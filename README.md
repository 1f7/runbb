
#RunBB forum
based on MyBB 1.8.7

# Now Dev Only! No support #

# Install
**1**
``` bash
$ composer create-project --dev runcmf/runcmf-skeleton my-app
```
**2** add to composer.json
```php
    "require": {
        ...
        ...
        "runcmf/runbb": "dev-master",
        "runcmf/runcli": "dev-master"
    },
```
**3** update
``` bash
$ composer update
```
**4** configure **db** connection app/Config/Settings.php
'prefix' - not necessary
example:
```php
    'db' => [// database configuration
        'default' => 'mysql',
        'connections' => [
            ...
            ...
            'mysql' => [
                'driver' => 'mysql',
//                'engine' => 'MyISAM',
                'engine' => 'InnoDB',
                'host' => '127.0.0.1',
                'database' => 'run',
                'username' => 'dbuser',
                'password' => '123',
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => 'mybb_'
            ],
            ...
            ...
        ],
    ],
```
**5** configure **modules** section app/Config/Settings.php
```php
    ...
    ...
    'modules' => [// register modules
        'forum' => 'RunBB\Init'
    ]
    ...
    ...
```
**6**
make dir 'forum' inside 'var' (var/forum). check writeble  
**7**
copy (or symlink) runbb folder from vendor/runcmf/runbb/web/assets  
to web/assets  
**8** migrate
``` bash
php bin/cli make:db
php bin/cli migrate:fill vendor/runcmf/runbb
php bin/cli seed:fill vendor/runcmf/runbb
```
**9** check config like site url, cookie and so on
``` php
$settings['bburl'] = "http://run.dev/forum";
$settings['homeurl'] = "http://run.dev/";
$settings['cookiedomain'] = ".run.dev";
```

---  
## Tests
```bash
$ cd vendor/runcmf/runbb
$ composer update
$ vendor/bin/phpunit
```
---  

## Security

If you discover any security related issues, please email to 1f7.wizard( at )gmail.com instead of using the issue tracker.

---  
### Who do I talk to? ###

* 1f7.wizard ( at ) gmail.com
* http://runcmf.ru

---  
## License

Apache License
Version 2.0. Please see [License File](LICENSE.md) for more information.