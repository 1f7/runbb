<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager as DB;
use RunCMF\Core\AbstractController;

class Install extends AbstractController
{

  private $err_msg = '';
  private $file_cache = '';
  private $filename = '';
  private $comp_method = 0;

  public function index(Request $req, Response $res)
  {
    $this->logger->info(__METHOD__ . ' action dispatched');

    $this->db_restore('run1.sql');

    $this->view->render($res, 'Install.html.twig', ['data' => $this->err_msg]);
  }

  /**
   * BEGIN Supex Dumper Restore Function
   */
  private function db_restore($file = '')
  {
    define('C_DEFAULT', 1);
    define('C_RESULT', 2);
    define('C_ERROR', 3);
    define('C_WARNING', 4);
    define('C_OK', 5);
    define('CHARSET', 'auto');
    // Путь и URL к файлам бекапа
    define('PATH', DIR . 'vendor/runcmf/runbb/src/RunBB/');
    $exhaust = '';

    define('RC_DBPREFIX', $this->settings['db']['prefix']);

    $db_link = mysqli_connect(
      $this->settings['db']['host'],
      $this->settings['db']['username'],
      $this->settings['db']['password'],
      $this->settings['db']['database']
    );

    $exhaust .= $this->tpl_l('Подключены к БД: `' . $this->settings['db']['database'] . '`.');
    // Версия MySQL вида 40101
    preg_match("/^(\d+)\.(\d+)\.(\d+)/", mysqli_get_server_info($db_link), $m);
    $this->mysqli_version = sprintf("%d%02d%02d", $m[1], $m[2], $m[3]);
    $this->forced_charset = false;

    $exhaust .= $this->tpl_l('Версия сервера БД: `' . $m[0] . '`.', C_OK);
    $exhaust .= $this->tpl_l('Версия клиента БД: `' . mysqli_get_client_info($db_link) . '`.', C_OK);

    set_error_handler(array($this, 'SXD_errorHandler'));
    error_reporting(-1);
    // Определение формата файла
    if (preg_match("/^(.+?)\.sql(\.(bz2|gz))?$/", $file, $matches)) {
      if (isset($matches[3]) && $matches[3] == 'bz2') {
        $this->comp_method = 2;
      } elseif (isset($matches[2]) && $matches[3] == 'gz') {
        $this->comp_method = 1;
      } else {
        $this->comp_method = 0;
      }
      //$this->comp_level = '';
      if (!file_exists(PATH . "/{$file}")) {
        $exhaust = $this->tpl_l("ОШИБКА! Файл не найден!", C_ERROR);
        //exit;
        tdie(PATH . "/{$file}" . $exhaust);
      }
      $exhaust .= $this->tpl_l("Чтение файла `{$file}`.");
      $file = $matches[1];
    } else {
      $exhaust = $this->tpl_l("ОШИБКА! Не выбран файл!", C_ERROR);
      //exit;
      tdie($exhaust);
    }

    $exhaust .= $this->tpl_l(str_repeat("-", 60), C_WARNING);

    $fp = $this->fn_open($file, "r");
    $this->file_cache = $sql = $table = $insert = '';
    $is_skd = $query_len = $execute = $q = $t = $i = $aff_rows = 0;
    $limit = 300;
    $index = 4;
    $tabs = 0;
    $cache = '';
    $info = array();

    // Установка кодировки соединения
    if ($this->mysqli_version > 40101 && (CHARSET != 'auto' || $this->forced_charset)) { // Кодировка по умолчанию, если в дампе не указана кодировка
      mysqli_query($db_link, "SET NAMES '" . $this->restore_charset . "'") or trigger_error("Неудается изменить кодировку соединения.<BR>" . mysqli_error($db_link), E_USER_ERROR);
      $exhaust .= $this->tpl_l("Установлена кодировка соединения `" . $this->restore_charset . "`.", C_WARNING);
      $last_charset = $this->restore_charset;
    } else {
      $last_charset = '';
    }
    $last_showed = '';

    while (($str = $this->fn_read_str($fp)) !== false) {
      // смена префикса таблиц
      if (preg_match("/{XPREFIX}/i", $str, $match)) {
        $str = preg_replace("/{XPREFIX}/i", RC_DBPREFIX, $str);
      }
      if (empty($str) || preg_match("/^(#|--)/", $str)) {
        if (!$is_skd && preg_match("/^#SKD101\|/", $str)) {
          $info = explode("|", $str);
          $exhaust .= $this->tpl_s(0, $t / $info[4]);
          $is_skd = 1;
        }
        continue;
      }
      $query_len += strlen($str);

      if (!$insert && preg_match("/^(INSERT INTO `?([^` ]+)`? .*?VALUES)(.*)$/i", $str, $m)) {
        if ($table != $m[2]) {
          $table = $m[2];
          $tabs++;
          $cache .= $this->tpl_l("Таблица `{$table}`.");
          $last_showed = $table;
          $i = 0;
          if ($is_skd)
            $exhaust .= $this->tpl_s(100, $t / $info[4]);
        }
        $insert = $m[1] . ' ';
        $sql .= $m[3];
        $index++;
        $info[$index] = isset($info[$index]) ? $info[$index] : 0;
        $limit = round($info[$index] / 20);
        $limit = $limit < 300 ? 300 : $limit;
        if ($info[$index] > $limit) {
          $exhaust .= $cache;
          $cache = '';
          $exhaust .= $this->tpl_s(0 / $info[$index], $t / $info[4]);
        }
      } else {
        $sql .= $str;
        if ($insert) {
          $i++;
          $t++;
          if ($is_skd && $info[$index] > $limit && $t % $limit == 0) {
            $exhaust .= $this->tpl_s($i / $info[$index], $t / $info[4]);
          }
        }
      }

      if (!$insert && preg_match("/^CREATE TABLE (IF NOT EXISTS )?`?([^` ]+)`?/i", $str, $m) && $table != $m[2]) {
        $table = $m[2];
        $insert = '';
        $tabs++;
        $is_create = true;
        $i = 0;
      }
      if ($sql) {
        if (preg_match("/;$/", $str)) {
          $sql = rtrim($insert . $sql, ";");
          if (empty($insert)) {
            if ($this->mysqli_version < 40101) {
              $sql = preg_replace("/ENGINE\s?=/", "TYPE=", $sql);
            } elseif (preg_match("/CREATE TABLE/i", $sql)) {
              // Выставляем кодировку соединения
              if (preg_match("/(CHARACTER SET|CHARSET)[=\s]+(\w+)/i", $sql, $charset)) {
                if (!$this->forced_charset && $charset[2] != $last_charset) {
                  if (CHARSET == 'auto') {
                    mysqli_query($db_link, "SET NAMES '" . $charset[2] . "'") or trigger_error("Неудается изменить кодировку соединения.<BR>{$sql}<BR>" . mysqli_error($db_link), E_USER_ERROR);
                    $cache .= $this->tpl_l("Установлена кодировка соединения `" . $charset[2] . "`.", C_WARNING);
                    $last_charset = $charset[2];
                  } else {
                    $cache .= $this->tpl_l('Кодировка соединения и таблицы не совпадает:', C_ERROR);
                    $cache .= $this->tpl_l('Таблица `' . $table . '` -> ' . $charset[2] . ' (соединение ' . $this->restore_charset . ')', C_ERROR);
                  }
                }
                // Меняем кодировку если указано форсировать кодировку
                if ($this->forced_charset) {
                  $sql = preg_replace("/(\/\*!\d+\s)?((COLLATE)[=\s]+)\w+(\s+\*\/)?/i", '', $sql);
                  $sql = preg_replace("/((CHARACTER SET|CHARSET)[=\s]+)\w+/i", "\\1" . $this->restore_charset . $this->restore_collate, $sql);
                }
              } elseif (CHARSET == 'auto') { // Вставляем кодировку для таблиц, если она не указана и установлена auto кодировка
                $sql .= ' DEFAULT CHARSET=' . $this->restore_charset . $this->restore_collate;
                if ($this->restore_charset != $last_charset) {
                  mysqli_query($db_link, "SET NAMES '" . $this->restore_charset . "'") or trigger_error("Неудается изменить кодировку соединения.<BR>{$sql}<BR>" . mysqli_error($db_link), E_USER_ERROR);
                  $cache .= $this->tpl_l("Установлена кодировка соединения `" . $this->restore_charset . "`.", C_WARNING);
                  $last_charset = $this->restore_charset;
                }
              }
            }
            if ($last_showed != $table) {
              $cache .= $this->tpl_l("Таблица `{$table}`.");
              $last_showed = $table;
            }
          } elseif ($this->mysqli_version > 40101 && empty($last_charset)) { // Устанавливаем кодировку на случай если отсутствует CREATE TABLE
            mysqli_query($db_link, "SET $this->restore_charset '" . $this->restore_charset . "'") or trigger_error("Неудается изменить кодировку соединения.<BR>{$sql}<BR>" . mysqli_error($db_link), E_USER_ERROR);
            $exhaust .= $this->tpl_l("Установлена кодировка соединения `" . $this->restore_charset . "`.", C_WARNING);
            $last_charset = $this->restore_charset;
          }
          $insert = '';
          $execute = 1;
        }
        if ($query_len >= 65536 && preg_match("/,$/", $str)) {
          $sql = rtrim($insert . $sql, ",");
          $execute = 1;
        }
        if ($execute) {
          $q++;
//$exhaust .= $this->tpl_l("sql: {$sql}", C_RESULT);
          mysqli_query($db_link, $sql) or trigger_error("Неправильный запрос.<BR>" . mysqli_error($db_link), E_USER_ERROR);
          if (preg_match("/^insert/i", $sql)) {
            $aff_rows += mysqli_affected_rows($db_link);
          }
          $sql = '';
          $query_len = 0;
          $execute = 0;
        }
      }
    }

    $exhaust .= $cache;
    $exhaust .= $this->tpl_s(1, 1);
    $exhaust .= $this->tpl_l(str_repeat("-", 60), C_WARNING);
    $exhaust .= $this->tpl_l("БД восстановлена из резервной копии.", C_RESULT);
    if (isset($info[3])) $exhaust .= $this->tpl_l("Дата создания копии: {$info[3]}", C_RESULT);
    $exhaust .= $this->tpl_l("Запросов к БД: {$q}", C_RESULT);
    $exhaust .= $this->tpl_l("Таблиц создано: {$tabs}", C_RESULT);
    $exhaust .= $this->tpl_l("Строк добавлено: {$aff_rows}", C_RESULT);
    $exhaust .= $this->tpl_l(str_repeat("-", 60), 4);

    $this->fn_close($fp);

    $this->err_msg = $exhaust;
  }

  private function fn_close($fp)
  {
    if ($this->comp_method == 2) {
      bzclose($fp);
    } elseif ($this->comp_method == 1) {
      gzclose($fp);
    } else {
      fclose($fp);
    }
    @chmod(PATH . $this->filename, 0666);
  }

  private function fn_open($name, $mode)
  {
    if ($this->comp_method == 2) {
      $this->filename = "{$name}.sql.bz2";
//      return bzopen(PATH . $this->filename, "{$mode}b{$this->comp_level}");
// если заебало ошипко wb7
      return bzopen(PATH . $this->filename, "{$mode}");
    } elseif ($this->comp_method == 1) {
      $this->filename = "{$name}.sql.gz";
      return gzopen(PATH . $this->filename, "{$mode}b{$this->comp_level}");
    } else {
      $this->filename = "{$name}.sql";
      return fopen(PATH . $this->filename, "{$mode}b");
    }
  }

  private function fn_read($fp)
  {
    if ($this->comp_method == 2) {
      return bzread($fp, 4096);
    } elseif ($this->comp_method == 1) {
      return gzread($fp, 4096);
    } else {
      return fread($fp, 4096);
    }
  }

  private function fn_read_str($fp)
  {
    $string = '';
    $this->file_cache = ltrim($this->file_cache);
    $pos = strpos($this->file_cache, "\n", 0);
    if ($pos < 1) {
      while (!$string && ($str = $this->fn_read($fp))) {
        $pos = strpos($str, "\n", 0);
        if ($pos === false) {
          $this->file_cache .= $str;
        } else {
          $string = $this->file_cache . substr($str, 0, $pos);
          $this->file_cache = substr($str, $pos + 1);
        }
      }
      if (!$str) {
        if ($this->file_cache) {
          $string = $this->file_cache;
          $this->file_cache = '';
          return trim($string);
        }
        return false;
      }
    } else {
      $string = substr($this->file_cache, 0, $pos);
      $this->file_cache = substr($this->file_cache, $pos + 1);
    }
    return trim($string);
  }

  private function SXD_errorHandler($errno, $errmsg, $filename, $linenum, $vars)
  {
    if ($errno == 2048) return true;
    if (preg_match("/chmod\(\).*?: Operation not permitted/", $errmsg)) return true;
    $dt = date("Y.m.d H:i:s");
    $errmsg = addslashes($errmsg);

    $this->err_msg .= $this->tpl_l("{$dt}<BR><B>Возникла ошибка!</B>: $filename, line: $linenum", C_ERROR);
    $this->err_msg .= $this->tpl_l("{$errmsg} ({$errno})", C_ERROR);
//  die();
  }

  private function tpl_l($str, $color = C_DEFAULT)
  {
    $str = preg_replace("/\s{2}/", " &nbsp;", $str);
    switch ($color) {
      case 2:
        $color = 'navy';
        break;
      case 3:
        $color = 'red';
        break;
      case 4:
        $color = 'maroon';
        break;
      case 5:
        $color = 'green';
        break;
      default:
        $color = 'black';
    }
    return '<div><font color="' . $color . '">' . $str . '</font></div>';
  }

  private function tpl_s($st, $so)
  {
    $st = round($st * 100);
    $st = $st > 100 ? 100 : $st;
    $so = round($so * 100);
    $so = $so > 100 ? 100 : $so;
    return '&nbsp; (' . $st . '%, ' . $so . '%)';
  }
  /**
   * END Supex Dumper Restore Function
   */
}