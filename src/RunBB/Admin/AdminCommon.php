<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin;

use RunCMF\Core\AbstractController;

class AdminCommon extends AbstractController
{
    public $admin = [];

    public $admin_options = [];

    public function init($module = 'home')
    {
        define('IN_ADMINCP', 1);

        $this->core->loadJs('metisMenu.min.js');
        $this->core->loadJs('sb-admin.js');

        $this->core->buildSidebarMenu();

        $this->run_module = $module;
        // Here you can change how much of an Admin CP IP address must match
        //in a previous session for the user is validated (defaults to 3 which matches a.b.c)
        define('ADMIN_IP_SEGMENTS', 0);

        $this->bb->shutdown_queries = $shutdown_functions = [];

        define('MYBB_ADMIN_DIR', MYBB_ROOT . $this->bb->config['admin_dir'] . '/');
        define('COPY_YEAR', $this->time->formatDate('Y', TIME_NOW));

        // Set cookie path to our admin dir temporarily, i.e. so that it affects the ACP only
//        $this->bb->settings['cookiepath'] = '/admin/forum/';//FIXME

//        if (!isset($cp_language)) {
//            if (!file_exists(MYBB_ROOT . 'Languages/' . $this->bb->settings['cplanguage'] . '/admin/home_dashboard.lang.php')) {
//                $this->bb->settings['cplanguage'] = 'english';
//            }
//            $this->lang->set_language($this->bb->settings['cplanguage'], 'admin');
//        }

        // Load global language phrases
        $this->lang->area = 'admin';
        $this->lang->load('global');
//        $this->lang->load('messages', true);//users

        if (function_exists('mb_internal_encoding') && !empty($this->lang->settings['charset'])) {
            @mb_internal_encoding($this->lang->settings['charset']);
        }

//        if (is_dir(MYBB_ROOT . 'install') && !file_exists(MYBB_ROOT . 'install/lock')) {
//            $this->bb->trigger_generic_error('install_directory');
//        }

//        $ip_address = $this->session->ipaddress;
        $this->user->uid = 0;
//        unset($this->user);
//        $this->user = [];
        // Load Admin CP style
        if (!isset($this->bb->cp_style)) {
            if (!empty($this->bb->settings['cpstyle']) && file_exists(DIR .'web/assets/runbb/admin/styles/' . $this->bb->settings['cpstyle'] . '/main.css')) {
                $this->bb->cp_style = $this->bb->settings['cpstyle'];
            } else {
                $this->bb->cp_style = 'default';
            }
        }
        $logged_out = false;
        $fail_check = 0;
        $post_verify = true;

        foreach (['action', 'do', 'module'] as $input) {
            if (!isset($this->bb->input[$input])) {
                $this->bb->input[$input] = '';
            }
        }
        if ($this->bb->input['action'] === 'unlock') {
            $this->user->uid = 0;
//            $this->bb->user = [];
//            unset($this->bb->user);
            $error = '';
            if ($this->bb->input['username']) {
                $user = $this->user->get_user_by_username($this->bb->input['username'], ['fields' => '*']);
                $this->user->load(0, $user);

                if (!$this->user->uid) {
                    $error = $this->lang->error_invalid_username;
                }
            } elseif ($this->bb->input['uid']) {
//                $this->bb->user = $this->user->get_user($this->bb->input['uid']);
                $this->user->load($this->bb->input['uid']);
                if (!$this->user->uid) {
                    $error = $this->lang->error_invalid_uid;
                }
            }

            // Do we have the token? If so let's process it
            if ($this->bb->input['token'] && $this->user->uid) {
                $query = $this->db->simple_select('awaitingactivation', 'COUNT(aid) AS num', "uid='" . (int)$this->user->uid . "' AND code='" . $this->db->escape_string($this->bb->input['token']) . "' AND type='l'");

                // If we're good to go
                if ($this->db->fetch_field($query, 'num') > 0) {
                    $this->db->delete_query('awaitingactivation', "uid='" . (int)$this->user->uid . "' AND code='" . $this->db->escape_string($this->bb->input['token']) . "' AND type='l'");
                    $this->db->update_query('adminoptions', ['loginlockoutexpiry' => 0, 'loginattempts' => 0], "uid='" . (int)$this->user->uid . "'");

                    $this->response->withRedirect((string)$this->request->getUri()->getPath());
                    exit;
                } else {
                    $error = $this->lang->error_invalid_token;
                }
            }

            $this->page->show_lockout_unlock($error, 'error');
        } elseif ($this->bb->input['do'] === 'login') {
            // We have an adminsid cookie?
            if (isset($this->bb->cookies['adminsid'])) {
                // Check admin session
                $query = $this->db->simple_select('adminsessions', 'sid', "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'");
                $this->session->admin_session = $this->db->fetch_field($query, 'sid');

                // Session found: redirect to index
                if ($this->session->admin_session) {
                    $this->response->withRedirect((string)$this->request->getUri()->getPath());
                    exit;
                }
            }

            $loginhandler = new \RunBB\Handlers\DataHandlers\LoginDataHandler($this->bb, 'get');

            // Validate PIN first
            if (!empty($this->bb->config['secret_pin']) &&
                (empty($this->bb->input['pin']) ||
                    $this->bb->input['pin'] != $this->bb->config['secret_pin'])) {
                $login_user = $this->user->get_user_by_username($this->bb->input['username'], ['fields' => ['email', 'username']]);
                $this->user->load(0, $login_user);

//                if ($login_user['uid'] > 0) {
                if ($this->user->uid > 0) {
                    $this->db->update_query('adminoptions', ['loginattempts' => 'loginattempts+1'], "uid='" . (int)$this->user->uid . "'", '', true);
                }

                $loginattempts = $this->login_attempt_check_acp($this->user->uid, true);

                // Have we attempted too many times?
                if ($loginattempts['loginattempts'] > 0) {
                    // Have we set an expiry yet?
                    if ($loginattempts['loginlockoutexpiry'] == 0) {
                        $this->db->update_query(
                            'adminoptions',
                            ['loginlockoutexpiry' => TIME_NOW + ((int)$this->bb->settings['loginattemptstimeout'] * 60)],
                            "uid='" . (int)$this->user->uid . "'"
                        );
                    }

                    // Did we hit lockout for the first time? Send the unlock email to the administrator
                    if ($loginattempts['loginattempts'] == $this->bb->settings['maxloginattempts']) {
                        $this->db->delete_query('awaitingactivation', "uid='" . (int)$this->user->uid . "' AND type='l'");
                        $lockout_array = [
                            'uid' => $this->user->uid,
                            'dateline' => TIME_NOW,
                            'code' => random_str(),
                            'type' => 'l'
                        ];
                        $this->db->insert_query('awaitingactivation', $lockout_array);

                        $subject = $this->lang->sprintf($this->lang->locked_out_subject, $this->bb->settings['bbname']);
                        $message = $this->lang->sprintf(
                            $this->lang->locked_out_message,
                            htmlspecialchars_uni($this->bb->input['username']),
                            $this->bb->settings['bbname'],
                            $this->bb->settings['maxloginattempts'],
                            $this->bb->settings['bburl'],
                            $this->bb->config['admin_dir'],
                            $lockout_array['code'],
                            $lockout_array['uid']
                        );
                        $this->mail->send($this->user->email, $subject, $message);
                    }

                    $this->bblogger->log_admin_action([
                            'type' => 'admin_locked_out',
                            'uid' => (int)$this->user->uid,
                            'username' => $this->user->username,
                        ]);

                    $this->page->show_lockedout();
                } else {
                    echo $this->page->show_login($this->lang->error_invalid_secret_pin, 'error');
                    exit;
                }
            }

            $loginhandler->set_data([
                'username' => $this->bb->input['username'],
                'password' => $this->bb->input['password']
            ]);

            if ($loginhandler->validate_login() == true) {
//                $this->bb->user = $this->user->get_user($loginhandler->login_data['uid']);
                $this->user->load($loginhandler->login_data['uid']);
            }
            if ($this->user->uid) {
                if ($this->login_attempt_check_acp($this->user->uid) == true) {
                    $this->bblogger->log_admin_action([
                            'type' => 'admin_locked_out',
                            'uid' => (int)$this->user->uid,
                            'username' => $this->user->username,
                        ]);

                    $this->page->show_lockedout();
                }

                $this->db->delete_query('adminsessions', "uid='{$this->user->uid}'");
                $sid = md5(random_str(50));
                $useragent = $_SERVER['HTTP_USER_AGENT'];
                if (my_strlen($useragent) > 200) {
                    $useragent = my_substr($useragent, 0, 200);
                }
                // Create a new admin session for this user
                $this->session->admin_session = [
                    'sid' => $sid,
                    'uid' => $this->user->uid,
                    'loginkey' => $this->user->loginkey,
                    'ip' => $this->session->ipaddress,
                    'dateline' => TIME_NOW,
                    'lastactive' => TIME_NOW,
                    'data' => my_serialize([]),
                    'useragent' => $this->db->escape_string($useragent),
                ];
                $this->db->insert_query('adminsessions', $this->session->admin_session);
                $this->session->admin_session['data'] = [];

                // Only reset the loginattempts when we're really logged in and the user doesn't need to enter a 2fa code
                $query = $this->db->simple_select('adminoptions', 'authsecret', "uid='{$this->user->uid}'");
                $this->admin_options = $this->db->fetch_array($query);
                if (empty($this->admin_options['authsecret'])) {
                    $this->db->update_query(
                        'adminoptions',
                        ['loginattempts' => 0, 'loginlockoutexpiry' => 0],
                        "uid='{$this->user->uid}'"
                    );
                }

                $this->bb->my_setcookie('adminsid', $sid, '', true);
                $this->bb->my_setcookie('acploginattempts', 0);
                $post_verify = false;
                $this->bb->request_method = 'get';
            } else {
                $login_user = $this->user->get_user_by_username($this->bb->input['username'], ['fields' => ['email', 'username']]);
                $this->user->load(0, $login_user);

                if ($this->user->uid > 0) {
                    $this->db->update_query('adminoptions', ['loginattempts' => 'loginattempts+1'], "uid='" . (int)$this->user->uid . "'", '', true);
                }

                $loginattempts = $this->login_attempt_check_acp($this->user->uid, true);

                // Have we attempted too many times?
                if ($loginattempts['loginattempts'] > 0) {
                    // Have we set an expiry yet?
                    if ($loginattempts['loginlockoutexpiry'] == 0) {
                        $this->db->update_query(
                            'adminoptions',
                            ['loginlockoutexpiry' => TIME_NOW + ((int)$this->bb->settings['loginattemptstimeout'] * 60)],
                            "uid='" . (int)$this->user->uid . "'"
                        );
                    }

                    // Did we hit lockout for the first time? Send the unlock email to the administrator
                    if ($loginattempts['loginattempts'] == $this->bb->settings['maxloginattempts']) {
                        $this->db->delete_query('awaitingactivation', "uid='" . (int)$this->user->uid . "' AND type='l'");
                        $lockout_array = [
                            'uid' => $this->user->uid,
                            'dateline' => TIME_NOW,
                            'code' => random_str(),
                            'type' => 'l'
                        ];
                        $this->db->insert_query('awaitingactivation', $lockout_array);

                        $subject = $this->lang->sprintf($this->lang->locked_out_subject, $this->bb->settings['bbname']);
                        $message = $this->lang->sprintf(
                            $this->lang->locked_out_message,
                            htmlspecialchars_uni($this->bb->input['username']),
                            $this->bb->settings['bbname'],
                            $this->bb->settings['maxloginattempts'],
                            $this->bb->settings['bburl'],
                            $this->bb->config['admin_dir'],
                            $lockout_array['code'],
                            $lockout_array['uid']
                        );
                        $this->mail->send($this->user->email, $subject, $message);
                    }

                    $this->bblogger->log_admin_action([
                            'type' => 'admin_locked_out',
                            'uid' => (int)$this->user->uid,
                            'username' => $this->user->username,
                        ]);

                    $this->page->show_lockedout();
                }

                $fail_check = 1;
            }
        } else {
            $login_message = '';
            // No admin session - show message on the login screen
            if (!isset($this->bb->cookies['adminsid'])) {
                $login_message = 'No admin session';
            } // Otherwise, check admin session
            else {
                $query = $this->db->simple_select('adminsessions', '*', "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'");
                $this->session->admin_session = $this->db->fetch_array($query);

                // No matching admin session found - show message on login screen
                if (!$this->session->admin_session['sid']) {
                    $login_message = $this->lang->error_invalid_admin_session;
                } else {
                    $this->session->admin_session['data'] = my_unserialize($this->session->admin_session['data']);

                    // Fetch the user from the admin session
//                    $this->bb->user = $this->user->get_user($this->session->admin_session['uid']);
                    $this->user->load($this->session->admin_session['uid']);


                    // Login key has changed - force logout
                    if (!$this->user->uid || $this->user->loginkey != $this->session->admin_session['loginkey']) {
                        $this->user->uid = 0;
//                        unset($this->bb->user);
//                        $this->bb->user = [];
                    } else {
                        // Admin CP sessions 1 hours old are expired
                        if ($this->session->admin_session['lastactive'] < TIME_NOW - 3600) {
                            $login_message = $this->lang->error_admin_session_expired;
                            $this->db->delete_query('adminsessions', "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'");
                            $this->user->uid = 0;
//                            unset($this->bb->user);
//                            $this->bb->user = [];
                        } // If IP matching is set - check IP address against the session IP
                        elseif (ADMIN_IP_SEGMENTS > 0) {
//                            $exploded_ip = explode('.', $ip_address);
                            $exploded_ip = explode('.', $this->session->ipaddress);
                            $exploded_admin_ip = explode('.', $this->session->admin_session['ip']);
                            $matches = 0;
                            $valid_ip = false;
                            for ($i = 0; $i < ADMIN_IP_SEGMENTS; ++$i) {
                                if ($exploded_ip[$i] == $exploded_admin_ip[$i]) {
                                    ++$matches;
                                }
                                if ($matches == ADMIN_IP_SEGMENTS) {
                                    $valid_ip = true;
                                    break;
                                }
                            }

                            // IP doesn't match properly - show message on logon screen
                            if (!$valid_ip) {
                                $login_message = $this->lang->error_invalid_ip;
                                $this->user->uid = 0;
//                                unset($this->bb->user);
//                                $this->bb->user = [];
                            }
                        }
                    }
                }
            }
        }

        if ($this->bb->input['action'] == 'logout' && $this->user->uid > 0) {
//            if ($this->bb->verify_post_check($this->bb->getInput('logoutkey', ''))) {
                $this->db->delete_query('adminsessions', "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'");
                $this->bb->my_unsetcookie('adminsid');
                $logged_out = true;

                return $this->response->withRedirect($this->bb->settings['bburl']);
//            }
        }
        if (!isset($this->user->usergroup)) {
            $mybbgroups = 1;
        } else {
            $mybbgroups = $this->user->usergroup . ',' . $this->user->additionalgroups;
        }
        $this->bb->usergroup = $this->group->usergroup_permissions($mybbgroups);
        $is_super_admin = $this->user->is_super_admin($this->user->uid);
        if ($this->user->uid == 0) {
            $this->bb->usergroup['cancp'] = 0;
        }
        if (($this->bb->usergroup['cancp'] != 1 && !$is_super_admin) || !$this->user->uid) {
            $this->db->delete_query('adminsessions', "uid = '{$this->user->uid}'");
//            unset($this->bb->user);
            $this->user->uid = 0;
            $this->bb->my_unsetcookie('adminsid');
        }
        if ($this->user->uid > 0) {
            $query = $this->db->simple_select('adminoptions', '*', "uid='" . $this->user->uid . "'");
            $this->admin_options = $this->db->fetch_array($query);
            if (!empty($this->admin_options['cplanguage'])
//                && file_exists(MYBB_ROOT . 'Languages/' . $this->admin_options['cplanguage'] . '/admin/home_dashboard.lang.php')
            ) {
//                $cp_language = $this->admin_options['cplanguage'];
                $this->lang->set_language($this->admin_options['cplanguage'], 'admin');
                $this->lang->load('global'); // Reload global language vars
//                $this->lang->load('messages', false);
            } else {
                $this->lang->set_language($this->settings['cplanguage'], 'admin');
                $this->lang->load('global'); // Reload global language vars
            }

            if (!empty($this->admin_options['cpstyle']) && file_exists(DIR .'web/assets/runbb/admin/styles/' . $this->admin_options['cpstyle'] . '/main.css')) {
                $this->bb->cp_style = $this->admin_options['cpstyle'];
            }

            // Update the session information in the DB
            if (isset($this->session->admin_session['sid'])) {
                $this->db->update_query(
                    'adminsessions',
                    ['lastactive' => TIME_NOW,
                        'ip' => $this->session->ipaddress],
                    "sid='" . $this->db->escape_string($this->session->admin_session['sid']) . "'"
                );
            }

            // Fetch administrator permissions
            $this->admin['permissions'] = $this->get_admin_permissions($this->user->uid);
        }
//    // Include the layout generation class overrides for this style
//    if(file_exists(MYBB_ADMIN_DIR."/styles/{$this->bb->cp_style}/style.php"))
//    {
//      require_once MYBB_ADMIN_DIR."/styles/{$this->bb->cp_style}/style.php";
//    }

//    // Check if any of the layout generation classes we can override exist in the style file
//    $classes = [
//      'Page' => 'DefaultPage',
//      'SidebarItem' => 'DefaultSidebarItem',
//      'PopupMenu' => 'DefaultPopupMenu',
//      'Table' => 'DefaultTable',
//      'Form' => 'DefaultForm',
//      'FormContainer' => 'DefaultFormContainer'
//    ];
//    foreach($classes as $style_name => $default_name)
//    {
//      // Style does not have this layout generation class, create it
//      if(!class_exists($style_name))
//      {
//        ev al("class {$style_name} extends {$default_name} { }");
//      }
//    }

        $this->page->style = $this->bb->cp_style;

        // Do not have a valid Admin user, throw back to login page.
        if ($this->user->uid === 0 || $logged_out == true) {
            if ($logged_out == true) {
                echo $this->page->show_login($this->lang->success_logged_out);
                exit;
            } elseif ($fail_check == 1) {
                $login_lang_string = $this->lang->error_invalid_username_password;

                switch ($this->bb->settings['username_method']) {
                    case 0: // Username only
                        $login_lang_string = $this->lang->sprintf($login_lang_string, $this->lang->login_username);
                        break;
                    case 1: // Email only
                        $login_lang_string = $this->lang->sprintf($login_lang_string, $this->lang->login_email);
                        break;
                    case 2: // Username and email
                    default:
                        $login_lang_string = $this->lang->sprintf($login_lang_string, $this->lang->login_username_and_password);
                        break;
                }

                echo $this->page->show_login($login_lang_string, 'error');
                exit;
            } else {
                // If we have this error while retreiving it from an AJAX request, then send back a nice error
                if (isset($this->bb->input['ajax']) && $this->bb->input['ajax'] == 1) {
                    echo json_encode(['errors' => ['login']]);
                    exit;
                }

                $msg = $this->lang->access_denied;
                if (!empty($login_message)) {
                    $msg = $msg . '<br />' . $login_message;
                }
                echo $this->page->show_login($msg, 'error');
                exit;
            }
        }

        // Time to check for Two-Factor Authentication
        // First: are we trying to verify a code?
        if ($this->bb->input['do'] == 'do_2fa' && $this->bb->request_method == 'post') {
            // Test whether it's a recovery code
            $recovery = false;
            $codes = my_unserialize($this->admin_options['recovery_codes']);
            if (!empty($codes) && in_array($this->bb->getInput('code', ''), $codes)) {
                $recovery = true;
                $ncodes = array_diff($codes, [$this->bb->input['code']]); // Removes our current code from the codes array
                $this->db->update_query(
                    'adminoptions',
                    ['recovery_codes' => $this->db->escape_string(my_serialize($ncodes))],
                    "uid='{$this->user->uid}'"
                );

                if (count($ncodes) == 0) {
                    $this->session->flash_message($this->lang->my2fa_no_codes, 'error');
                }
            }

            // Validate the code
            $auth = new \RunBB\Helpers\GoogleAuthenticator;

            $test = $auth->verifyCode($this->admin_options['authsecret'], $this->bb->getInput('code', ''));

            // Either the code was okay or it was a recovery code
            if ($test === true || $recovery === true) {
                // Correct code -> session authenticated
                $this->db->update_query('adminsessions', ['authenticated' => 1], "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'");
                $this->session->admin_session['authenticated'] = 1;
                $this->db->update_query('adminoptions', ['loginattempts' => 0, 'loginlockoutexpiry' => 0], "uid='{$this->user->uid}'");
                $this->bb->my_setcookie('acploginattempts', 0);
                // post would result in an authorization code mismatch error
                $this->bb->request_method = 'get';
            } else {
                // Wrong code -> close session (aka logout)
                $this->db->delete_query('adminsessions', "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'");
                $this->bb->my_unsetcookie('adminsid');

                // Now test whether we need to lock this guy completly
                $this->db->update_query('adminoptions', ['loginattempts' => 'loginattempts+1'], "uid='{$this->user->uid}'", '', true);

                $loginattempts = $this->login_attempt_check_acp($this->user->uid, true);

                // Have we attempted too many times?
                if ($loginattempts['loginattempts'] > 0) {
                    // Have we set an expiry yet?
                    if ($loginattempts['loginlockoutexpiry'] == 0) {
                        $this->db->update_query(
                            'adminoptions',
                            ['loginlockoutexpiry' => TIME_NOW + ((int)$this->bb->settings['loginattemptstimeout'] * 60)],
                            "uid='{$this->user->uid}'"
                        );
                    }

                    // Did we hit lockout for the first time? Send the unlock email to the administrator
                    if ($loginattempts['loginattempts'] == $this->bb->settings['maxloginattempts']) {
                        $this->db->delete_query('awaitingactivation', "uid='{$this->user->uid}' AND type='l'");
                        $lockout_array = [
                            'uid' => $this->user->uid,
                            'dateline' => TIME_NOW,
                            'code' => random_str(),
                            'type' => 'l'
                        ];
                        $this->db->insert_query('awaitingactivation', $lockout_array);

                        $subject = $this->lang->sprintf($this->lang->locked_out_subject, $this->bb->settings['bbname']);
                        $message = $this->lang->sprintf(
                            $this->lang->locked_out_message,
                            htmlspecialchars_uni($this->user->username),
                            $this->bb->settings['bbname'],
                            $this->bb->settings['maxloginattempts'],
                            $this->bb->settings['bburl'],
                            $this->bb->config['admin_dir'],
                            $lockout_array['code'],
                            $lockout_array['uid']
                        );
                        $this->mail->send($this->user->email, $subject, $message);
                    }

                    $this->bblogger->log_admin_action([
                            'type' => 'admin_locked_out',
                            'uid' => $this->user->uid,
                            'username' => $this->user->username,
                        ]);

                    $this->page->show_lockedout();
                }

                // Still here? Show a custom login page
                echo $this->page->show_login($this->lang->my2fa_failed, 'error');
                exit;
            }
        }

        // Show our 2FA page
        if (!empty($this->admin_options['authsecret']) && $this->session->admin_session['authenticated'] != 1) {
            $this->page->show_2fa();
        }

        $this->page->add_breadcrumb_item($this->lang->home, $this->bb->admin_url);

        // Begin dealing with the modules
        $this->modules_dir = MYBB_ADMIN_DIR . 'Modules';
        $dir = opendir($this->modules_dir);
        while (($module = readdir($dir)) !== false) {
            if (is_dir($this->modules_dir . '/' . $module) && !in_array($module, ['.', '..']) &&
                file_exists($this->modules_dir . '/' . $module . '/Meta.php')
            ) {
                //require_once $this->modules_dir.'/'.$module.'/Meta.php';
                $modfile = '\\RunBB\\Admin\\Modules\\' . $module . '\\Meta';
                $mod = strtolower($module);
                $this->$mod = new $modfile($this->bb);
                // Need to always load it for admin permissions / quick access
                $this->lang->load($mod . '_module_meta', false, true);

                $has_permission = false;
                if (method_exists($this->$mod, $mod . '_admin_permissions')) {
                    $has_permission = false;
                    if ($this->admin['permissions'][$mod] || $is_super_admin === true) {
                        $has_permission = true;
                    }
                } // This module doesn't support permissions
                else {
                    $has_permission = true;
                }
                // Do we have permissions to run this module (Note: home is accessible by all)
                if ($mod === 'home' || $has_permission === true) {
                    $method = $mod . '_meta';
//          $meta_function = $this->$module->$method;
//          $initialized = $meta_function();
                    $initialized = $this->$mod->$method();
                    if ($initialized == true) {
                        $modules[$module] = 1;
                    }
                } else {
                    $modules[$module] = 0;
                }
            }
        }

//        $modules =
        $this->plugins->runHooks('admin_tabs', $modules);

        closedir($dir);

//    if(strpos($this->bb->input['module'], '/') !== false)
//    {
//      $current_module = explode('/', $this->bb->input['module'], 2);
//    }
//    else
//    {
//      $current_module = explode('-', $this->bb->input['module'], 2);
//    }
//
//    if(!isset($current_module[1]))
//    {
//      $current_module[1] = 'home';
//    }

        $path = explode('/', $this->request->getUri()->getPath(), 5);
        $action = '';
        if (isset($path[4])) {
            $path = explode('?', $path[4]);
            if (!empty($path[0])) {
                $action = $path[0];
            }
        } else {
            // admin root - home or home action
            if (isset($path[3])) {
                $action = $path[3];
            } else {
                $action = 'home';
            }
        }
        $action_handler = $this->run_module . '_action_handler';
        $action_file = $this->{$this->run_module}->$action_handler($action);//$current_module[1]);

        // Set our POST validation code here
        $this->bb->post_code = $this->bb->generate_post_check();
        if ($this->run_module != 'home') {
            $this->check_admin_permissions(['module' => $this->page->active_module, 'action' => $this->page->active_action]);
        }

        // Only POST actions with a valid post code can modify information.
        // Here we check if the incoming request is a POST and if that key is valid.
        $post_check_ignores = [
            'example/page' => ['action']
        ]; // An array of modules/actions to ignore POST checks for.

        if ($this->bb->request_method == 'post') {
            if (in_array($this->bb->input['module'], $post_check_ignores)) {
                $k = array_search($this->bb->input['module'], $post_check_ignores);
                if (in_array($this->bb->input['action'], $post_check_ignores[$k])) {
                    $post_verify = false;
                }
            }

            if ($post_verify == true) {
                // If the post key does not match we switch the action to GET and set a message to show the user
                if (!isset($this->bb->input['my_post_key']) || $this->bb->post_code != $this->bb->input['my_post_key']) {
                    $this->bb->request_method = 'get';
                    $this->page->show_post_verify_error = true;
                }
            }
        }
        foreach ($this->core->data as $k => $v) {
            $this->view->offsetSet($k, $v);
        }
//        $this->view->offsetAdd($this->core->data);

        $this->plugins->runHooks('admin_load');
    }

    /**
     * Draw pagination for pages in the Admin CP.
     *
     * @param int $page The current page we're on
     * @param int $per_page The number of items per page
     * @param int $total_items The total number of items in this collection
     * @param string $url The URL for pagination of this collection
     * @return string The built pagination
     */
    public function draw_admin_pagination($page, $per_page, $total_items, $url)
    {
        if ($total_items <= $per_page) {
            return '';
        }

        $pages = ceil($total_items / $per_page);

        $pagination = "<div class=\"pagination\"><span class=\"pages\">{$this->lang->pages}: </span>\n";

        if ($page > 1) {
            $prev = $page - 1;
            $prev_page = $this->pagination->fetchPageUrl($url, $prev);
            $pagination .= "<a href=\"{$prev_page}\" class=\"pagination_previous\">&laquo; {$this->lang->previous}</a> \n";
        }

        // Maximum number of "page bits" to show
        if (!$this->bb->settings['maxmultipagelinks']) {
            $this->bb->settings['maxmultipagelinks'] = 5;
        }

        $max_links = $this->bb->settings['maxmultipagelinks'];

        $from = $page - floor($this->bb->settings['maxmultipagelinks'] / 2);
        $to = $page + floor($this->bb->settings['maxmultipagelinks'] / 2);

        if ($from <= 0) {
            $from = 1;
            $to = $from + $max_links - 1;
        }

        if ($to > $pages) {
            $to = $pages;
            $from = $pages - $max_links + 1;
            if ($from <= 0) {
                $from = 1;
            }
        }

        if ($to == 0) {
            $to = $pages;
        }

        if ($from > 2) {
            $first = $this->pagination->fetchPageUrl($url, 1);
            $pagination .= "<a href=\"{$first}\" title=\"{$this->lang->page} 1\" class=\"pagination_first\">1</a> ... ";
        }

        for ($i = $from; $i <= $to; ++$i) {
            $page_url = $this->pagination->fetchPageUrl($url, $i);
            if ($page == $i) {
                $pagination .= "<span class=\"pagination_current\">{$i}</span> \n";
            } else {
                $pagination .= "<a href=\"{$page_url}\" title=\"{$this->lang->page} {$i}\">{$i}</a> \n";
            }
        }

        if ($to < $pages) {
            $last = $this->pagination->fetchPageUrl($url, $pages);
            $pagination .= "... <a href=\"{$last}\" title=\"{$this->lang->page} {$pages}\" class=\"pagination_last\">{$pages}</a>";
        }

        if ($page < $pages) {
            $next = $page + 1;
            $next_page = $this->pagination->fetchPageUrl($url, $next);
            $pagination .= " <a href=\"{$next_page}\" class=\"pagination_next\">{$this->lang->next} &raquo;</a>\n";
        }
        $pagination .= "</div>\n";
        return $pagination;
    }

    /**
     * Builds a CSV parent list for a particular forum.
     *
     * @param int $fid The forum ID
     * @param string $navsep Optional separator - defaults to comma for CSV list
     * @return string The built parent list
     */
    public function make_parent_list($fid, $navsep = ',')
    {
        if (empty($this->bb->pforumcache)) {
            $query = $this->db->simple_select('forums', 'name, fid, pid', '', ['order_by' => 'disporder, pid']);
            while ($forum = $this->db->fetch_array($query)) {
                $this->bb->pforumcache[$forum['fid']][$forum['pid']] = $forum;
            }
        }

        reset($this->bb->pforumcache);
        reset($this->bb->pforumcache[$fid]);
        $navigation = '';
        foreach ($this->bb->pforumcache[$fid] as $key => $forum) {
            if ($fid == $forum['fid']) {
                if (isset($this->bb->pforumcache[$forum['pid']])) {
                    $navigation = $this->make_parent_list($forum['pid'], $navsep) . $navigation;
                }

                if ($navigation) {
                    $navigation .= $navsep;
                }
                $navigation .= $forum['fid'];
            }
        }
        return $navigation;
    }

    /**
     * Checks if a particular user has the necessary permissions to access a particular page.
     *
     * @param array $action Array containing module and action to check for
     * @param bool $error
     * @return bool
     */
    public function check_admin_permissions($action, $error = true)
    {
        if ($this->user->is_super_admin($this->user->uid)) {
            return true;
        }
        //if(class_exists($this->{$action['module']}))
        //require_once $this->modules_dir.'/'.$action['module'].'/module_meta.php';

        if (method_exists($this->{$action['module']}, $action['module'] . '_admin_permissions')) { //    if(function_exists($action['module'].'_admin_permissions'))
            $method = $action['module'] . '_admin_permissions';
            $permissions = $this->{$action['module']}->$method();

            if ($permissions['permissions'][$action['action']] &&
                (!isset($this->admin['permissions'][$action['module']][$action['action']]) ||
                $this->admin['permissions'][$action['module']][$action['action']] != 1)) {
                if ($error) {
                    $html = $this->page->output_header($this->lang->access_denied);
                    $this->page->add_breadcrumb_item($this->lang->access_denied, $this->bb->admin_url);
                    $html .= $this->page->output_error("<b>{$this->lang->access_denied}</b><ul><li style=\"list-style-type: none;\">{$this->lang->access_denied_desc}</li></ul>");
                    $this->page->output_footer();

                    $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
//                    echo $html;
                    // FIXME fuck
                    exit($this->view->render($this->response, '@forum/Admin/Deny.html.twig'));
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Fetches the list of administrator permissions for a particular user or group
     *
     * @param int $get_uid The user ID to fetch permissions for
     * @param int $get_gid The (optional) group ID to fetch permissions for
     * @return array Array of permissions for specified user or group
     */
    public function get_admin_permissions($get_uid = 0, $get_gid = 0)
    {
        // Set UID and GID if none
        $uid = $get_uid;
        $gid = $get_gid;

        $gid_array = [];

        if ($uid === 0) {
            $uid = $this->user->uid;
        }

        if (!$gid) {
            // Prepare user's groups since the group isn't specified
            $gid_array[] = (-1) * (int)$this->user->usergroup;

            if ($this->user->additionalgroups) {
                $additional_groups = explode(',', $this->user->additionalgroups);

                if (!empty($additional_groups)) {
                    // Make sure gids are negative
                    foreach ($additional_groups as $g) {
                        $gid_array[] = (-1) * abs($g);
                    }
                }
            }
        } else {
            // Group is specified
            // Make sure gid is negative
            $gid_array[] = (-1) * abs($gid);
        }

        // What are we trying to find?
        if ($get_gid && !$get_uid) {
            // A group only

            $options = [
                'order_by' => 'uid',
                'order_dir' => 'ASC',
                'limit' => '1'
            ];
            $query = $this->db->simple_select('adminoptions', 'permissions', "(uid='-{$get_gid}' OR uid='0') AND permissions != ''", $options);
            return my_unserialize($this->db->fetch_field($query, 'permissions'));
        } else {
            // A user and/or group

            $options = [
                'order_by' => 'uid',
                'order_dir' => 'DESC'
            ];

            // Prepare user's groups into SQL format
            $group_sql = '';
            foreach ($gid_array as $gid) {
                $group_sql .= " OR uid='{$gid}'";
            }

            $perms_group = $perms_def = [];
            $query = $this->db->simple_select('adminoptions', 'permissions, uid', "(uid='{$uid}'{$group_sql}) AND permissions != ''", $options);
            while ($perm = $this->db->fetch_array($query)) {
                $perm['permissions'] = my_unserialize($perm['permissions']);

                // Sorting out which permission is which
                if ($perm['uid'] > 0) {
                    $perms_user = $perm;
                    return $perms_user['permissions'];
                } elseif ($perm['uid'] < 0) {
                    $perms_group[] = $perm['permissions'];
                } else {
                    $perms_def = $perm['permissions'];
                }
            }

            // Figure out group permissions...ugh.
            foreach ($perms_group as $gperms) {
                if (!isset($final_group_perms)) {
                    // Use this group as the base for admin group permissions
                    $final_group_perms = $gperms;
                    continue;
                }

                // Loop through each specific permission to find the highest permission
                foreach ($gperms as $perm_name => $perm_value) {
                    if ($final_group_perms[$perm_name] != '1' && $perm_value == '1') {
                        $final_group_perms[$perm_name] = '1';
                    }
                }
            }

            // Send specific user, or group permissions before default.
            // If user's permission are explicitly set, they've already been returned above.
            if (isset($final_group_perms)) {
                return $final_group_perms;
            } else {
                return $perms_def;
            }
        }
    }

// FIXME delete???    /**
//     * Adds/Updates a Page/Tab to the permissions array in the adminoptions table
//     *
//     * @param string $tab The name of the tab that is being affected
//     * @param string $page The name of the page being affected (optional - if not specified, will affect everything under the specified tab)
//     * @param integer $default Default permissions for the page (1 for allowed - 0 for disallowed - -1 to remove)
//     */
//    function change_admin_permission($tab, $page = '', $default = 1)
//    {
//        $query = $this->db->simple_select('adminoptions', 'uid, permissions', "permissions != ''");
//        while ($adminoption = $this->db->fetch_array($query)) {
//            $adminoption['permissions'] = my_unserialize($adminoption['permissions']);
//
//            if ($default == -1) {
//                if (!empty($page)) {
//                    unset($adminoption['permissions'][$tab][$page]);
//                } else {
//                    unset($adminoption['permissions'][$tab]);
//                }
//            } else {
//                if (!empty($page)) {
//                    if ($adminoption['uid'] == 0) {
//                        $adminoption['permissions'][$tab][$page] = 0;
//                    } else {
//                        $adminoption['permissions'][$tab][$page] = $default;
//                    }
//                } else {
//                    if ($adminoption['uid'] == 0) {
//                        $adminoption['permissions'][$tab]['tab'] = 0;
//                    } else {
//                        $adminoption['permissions'][$tab]['tab'] = $default;
//                    }
//                }
//            }
//
//            $this->db->update_query('adminoptions', array('permissions' => $this->db->escape_string(my_serialize($adminoption['permissions']))), "uid='{$adminoption['uid']}'");
//        }
//    }

    /**
     * Checks if we have had too many attempts at logging into the ACP
     *
     * @param integer $uid The uid of the admin to check
     * @param boolean $return_num Return an array of the number of attempts and expiry time? (default false)
     * @return mixed Return an array if the second parameter is true, boolean otherwise.
     */
    private function login_attempt_check_acp($uid = 0, $return_num = false)
    {
        $attempts['loginattempts'] = 0;

        if ($uid > 0) {
            $query = $this->db->simple_select('adminoptions', 'loginattempts, loginlockoutexpiry', "uid='" . (int)$uid . "'", 1);
            $attempts = $this->db->fetch_array($query);
        }

        if ($attempts['loginattempts'] <= 0) {
            return false;
        }

        if ($this->bb->settings['maxloginattempts'] > 0 && $attempts['loginattempts'] >= $this->bb->settings['maxloginattempts']) {
            // Has the expiry dateline been set yet?
            if ($attempts['loginlockoutexpiry'] == 0 && $return_num == false) {
                $this->db->update_query(
                    'adminoptions',
                    ['loginlockoutexpiry' => TIME_NOW + ((int)$this->bb->settings['loginattemptstimeout'] * 60)],
                    "uid='" . (int)$uid . "'"
                );
            }

            // Are we returning the # of login attempts?
            if ($return_num == true) {
                return $attempts;
            } // Otherwise are we still locked out?
            elseif ($attempts['loginlockoutexpiry'] > TIME_NOW) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether there are any 'security' issues in templates via complex syntax
     *
     * @param string $template The template to be scanned
     * @return boolean A true/false depending on if an issue was detected
     */
    public function check_template($template)
    {
        // Check to see if our database password is in the template
        if (preg_match('#\$config\[(([\'|"]database[\'|"])|([^\'"].*?))\]\[(([\'|"](database|hostname|password|table_prefix|username)[\'|"])|([^\'"].*?))\]#i', $template)) {
            return true;
        }

        // System calls via backtick
        if (preg_match('#\$\s*\{#', $template)) {
            return true;
        }

        // Any other malicious acts?
        // Courtesy of ZiNgA BuRgA
        if (preg_match("~\\{\\$.+?\\}~s", preg_replace('~\\{\\$+[a-zA-Z_][a-zA-Z_0-9]*((?:-\\>|\\:\\:)\\$*[a-zA-Z_][a-zA-Z_0-9]*|\\[\s*\\$*([\'"]?)[a-zA-Z_ 0-9 ]+\\2\\]\s*)*\\}~', '', $template))) {
            return true;
        }

        return false;
    }

    /**
     * Prints a selection JavaScript code for selectable groups/forums fields.
     */
    public function print_selection_javascript()
    {
        static $already_printed = false;

        if ($already_printed) {
            return;
        }

        $already_printed = true;

        return "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'_forums_groups_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});

		$('.'+id+'_forums_groups').each(function(e)
		{
			$(this).hide();
		});

		if($('#'+id+'_forums_groups_'+checked))
		{
			$('#'+id+'_forums_groups_'+checked).show();
		}
	}
</script>";
    }
}
