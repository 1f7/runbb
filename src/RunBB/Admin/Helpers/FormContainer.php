<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Helpers;

class FormContainer
{
    /** @var Table */
    private $_container;
    /** @var string */
    public $_title;
    private $bb;

    /**
     * Initialise the new form container.
     *
     * @param string $title The title of the form container
     * @param string $extra_class An additional class to apply if we have one.
     */
    public function __construct($bb, $title = '', $extra_class = '')
    {
        $this->bb = $bb;
        $this->_container = new Table($bb);
        $this->extra_class = $extra_class;
        $this->_title = $title;
    }

    /**
     * Output a header row of the form container.
     *
     * @param string $title The header row label.
     * @param array $extra Array of extra information for this header cell (class, style, colspan, width)
     */
    public function output_row_header($title, $extra = [])
    {
        $this->_container->construct_header($title, $extra);
    }

    /**
     * Output a row of the form container.
     *
     * @param string $title The title of the row.
     * @param string $description The description of the row/field.
     * @param string $content The HTML content to show in the row.
     * @param string $label_for The ID of the control this row should be a label for.
     * @param array $options Array of options for the row cell.
     * @param array $row_options Array of options for the row container.
     */
    public function output_row(
        $title,
        $description = '',
        $content = '',
        $label_for = '',
        $options = [],
        $row_options = []
    ) {
    
        $pluginargs = [
            'title' => &$title,
            'description' => &$description,
            'content' => &$content,
            'label_for' => &$label_for,
            'options' => &$options,
            'row_options' => &$row_options,
            'this' => &$this
        ];

        $this->bb->plugins->runHooks("admin_formcontainer_output_row", $pluginargs);

        $row = $for = '';
        if ($label_for != '') {
            $for = " for=\"{$label_for}\"";
        }

        if ($title) {
            $row = "<label{$for}>{$title}</label>";
        }

        if ($description != '') {
            $row .= "\n<div class=\"description\">{$description}</div>\n";
        }

        $row .= "<div class=\"form_row\">{$content}</div>\n";

        $this->_container->construct_cell($row, $options);

        if (!isset($options['skip_construct'])) {
            $this->_container->construct_row($row_options);
        }
    }

    /**
     * Output a row cell for a table based form row.
     *
     * @param string $data The data to show in the cell.
     * @param array $options Array of options for the cell (optional).
     */
    public function output_cell($data, $options = [])
    {
        $this->_container->construct_cell($data, $options);
    }

    /**
     * Build a row for the table based form row.
     *
     * @param array $extra Array of extra options for the cell (optional).
     */
    public function construct_row($extra = [])
    {
        $this->_container->construct_row($extra);
    }

    /**
     * return the cells of a row for the table based form row.
     *
     * @param string $row_id The id of the row.
     * @param boolean $return Whether or not to return or echo the resultant contents.
     * @return string The output of the row cells (optional).
     */
    public function output_row_cells($row_id, $return = false)
    {
//        if (!$return) {
//            echo $this->_container->output_row_cells($row_id, $return);
//        } else {
            return $this->_container->output_row_cells($row_id, $return);
//        }
    }

    /**
     * Count the number of rows in the form container. Useful for displaying a 'no rows' message.
     *
     * @return int The number of rows in the form container.
     */
    function num_rows()
    {
        return $this->_container->num_rows();
    }

    /**
     * Output the end of the form container row.
     *
     * @param boolean $return Whether or not to return or echo the resultant contents.
     * @return string The output of the form container (optional).
     */
    public function end($return = false)
    {
        $hook = [
            'return' => &$return,
            'this' => &$this
        ];

        $this->bb->plugins->runHooks("admin_formcontainer_end", $hook);
//        if ($return == true) {
            return $this->_container->output($this->_title, 1, "general form_container {$this->extra_class}", true);
//        } else {
//            echo $this->_container->output($this->_title, 1, "general form_container {$this->extra_class}", false);
//        }
    }
}
