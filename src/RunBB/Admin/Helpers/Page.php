<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Helpers;

use RunCMF\Core\AbstractController;
use RunBB\Admin\Helpers\SidebarItem;

class Page extends AbstractController
{
    /**
     * @var string The current style in use.
     */
    public $style;

    /**
     * @var array The primary menu items.
     */
    public $menu = [];
    private $_menu = [];

    /**
     * @var string The side bar menu items.
     */
    public $submenu = '';

    /**
     * @var string The module we're currently in.
     */
    public $active_module;

    /**
     * @var string The action we're currently performing.
     */
    public $active_action;

    /**
     * @var string Content for the side bar of the page if we have one.
     */
    public $sidebar;

    /**
     * @var array The breadcrumb trail leading up to this page.
     */
    public $_breadcrumb_trail = [];

    /**
     * @var string Any additional information to add between the <head> tags.
     */
    public $extra_header = '';

    /**
     * @var string Any additional messages to add after the flash messages are shown.
     */
    public $extra_messages = [];

    /**
     * @var string Show a post verify error
     */
    public $show_post_verify_error = '';

    /**
     * Output the page header.
     *
     * @param string $title The title of the page.
     */
    public function output_header($title = '')
    {
        $args = [
            'this' => &$this,
            'title' => &$title,
        ];
        $this->bb->plugins->runHooks('admin_page_output_header', $args);
        if (!$title) {
            $title = $this->lang->mybb_admin_panel;
        }
//        $rtl = '';
//        if ($this->lang->settings['rtl'] == 1) {
//            $rtl = ' dir="rtl"';
//        }

        // Load stylesheet for this module if it has one
        $active_module = '';
        if (file_exists(DIR . "web/assets/runbb/admin/styles/{$this->style}/{$this->active_module}.css")) {
            $active_module = $this->active_module;
        }

        $flash_message = $err_message = [];
        if (isset($this->session->admin_session['data']['flash_message']) && $this->session->admin_session['data']['flash_message']) {
            $flash_message[]=[
                'type' => $this->session->admin_session['data']['flash_message']['type'],
                'message' => $this->session->admin_session['data']['flash_message']['message']
            ];
            $this->session->update_admin_session('flash_message', '');
        }

        if (!empty($this->extra_messages) && is_array($this->extra_messages)) {
            foreach ($this->extra_messages as $message) {
                switch ($message['type']) {
                    case 'success':
                    case 'error':
                        $flash_message[] = $message;
                        break;
                    default:
                        $err_message[] = $message['message'];
                        break;
                }
            }
        }

        if ($this->show_post_verify_error == true) {
            $err_message[] = $this->lang->invalid_post_verify_key;
        }
        $data = [
            'title' => $title,
            'copy_year' => COPY_YEAR,
            'style' => $this->style,
            'active_module' => $active_module,
            'extra_header' => $this->extra_header,
            'top_menu' => $this->_build_menu(),
            'submenu' => $this->submenu,
            'sidebar' => $this->sidebar,
            'breadcrumb' => $this->_generate_breadcrumb(),
            'flash_message' => $flash_message,
            'err_message' => $err_message,
        ];
//        return $this->view->fetch('@forum/Admin/header.html.twig', $data);
//        $this->view->fetch('@forum/Admin/header.html.twig', $data);
//        $this->view->offsetAdd($data);
        foreach ($data as $k => $v) {
            $this->view->offsetSet($k, $v);
        }
    }

    /**
     * Output the page footer.
     *
     * @param bool $quit
     */
    public function output_footer($quit = true)
    {
        $args = [
            'this' => &$this,
            'quit' => &$quit,
        ];

        $this->bb->plugins->runHooks('admin_page_output_footer', $args);

        $memory_usage = $this->parser->friendlySize(get_memory_usage());

        $totaltime = format_time_duration($this->bb->maintimer->stop());
        $querycount = $this->db->query_count;

        if (my_strpos(getenv('REQUEST_URI'), '?')) {
            $debuglink = htmlspecialchars_uni(getenv('REQUEST_URI')) . '&amp;debug=1#footer';
        } else {
            $debuglink = htmlspecialchars_uni(getenv('REQUEST_URI')) . '?debug=1#footer';
        }

        $data = [
            'generation' => $this->lang->sprintf($this->lang->generated_in, $totaltime, $debuglink, $querycount, $memory_usage),
            'explain' => $this->bb->debug_mode ? $this->db->explain : '',
        ];

//        return $this->view->fetch('@forum/Admin/footer.html.twig', $data);
//        return $this->view->fetch('@forum/Admin/Layout.html.twig', $data);
//        $this->view->offsetAdd($data);
        foreach ($data as $k => $v) {
            $this->view->offsetSet($k, $v);
        }
//FIXME !!!
//        if ($quit != false) {
//            exit;
//        }
    }

    /**
     * Add an item to the page breadcrumb trail.
     *
     * @param string $name The name of the item to add.
     * @param string $url The URL to the item we're adding (if there is one)
     */
    public function add_breadcrumb_item($name, $url = '')
    {
        $this->_breadcrumb_trail[] = ['name' => $name, 'url' => $url];
    }

    /**
     * Generate a breadcrumb trail.
     *
     * @return bool|string
     */
    private function _generate_breadcrumb()
    {
        if (!is_array($this->_breadcrumb_trail)) {
            return false;
        }
        $trail = '';
        foreach ($this->_breadcrumb_trail as $key => $crumb) {
            if (is_array($crumb['name'])) {// FIXME
                $crumb['name'] = $crumb['name']['name'];
            }

            if (isset($this->_breadcrumb_trail[$key + 1])) {
                $trail .= '<a href="' . $crumb['url'] . '">' . $crumb['name'] . '</a>';
                if (isset($this->_breadcrumb_trail[$key + 1])) {
//                    $trail .= ' &raquo; ';
                    $trail .= ' <i class="fa fa-arrow-right" aria-hidden="true"></i> ';
                }
            } else {
                $trail .= '<span class="active">' . $crumb['name'] . '</span>';
            }
        }
        return $trail;
    }

    /**
     * Output a success message.
     *
     * @param string $message The message to output.
     *
     * @return string success message div
     */
    public function output_success($message)
    {
        return "<div class=\"success\">{$message}</div>\n";
    }

    /**
     * Output an alert/warning message.
     *
     * @param string $message The message to output.
     * @param string $id The ID of the alert/warning (optional)
     *
     * @return string alert message div
     */
    public function output_alert($message, $id = '')
    {
        if ($id) {
            $id = " id=\"{$id}\"";
        }
        return "<div class=\"alert\"{$id}>{$message}</div>\n";
    }

    /**
     * Output an inline message.
     *
     * @param string $message The message to output.
     *
     * @return string error message div
     */
    public function output_inline_message($message)
    {
        return "<div class=\"inline_message\">{$message}</div>\n";
    }

    /**
     * Output a single error message.
     *
     * @param string $error The message to output.
     */
    public function output_error($error)
    {
        return '<div class="error">'. $error .'</div>';
    }

    /**
     * Output one or more inline error messages.
     *
     * @param array $errors Array of error messages to output.
     *
     * @return string error div
     */
    public function output_inline_error($errors)
    {
        if (!is_array($errors)) {
            $errors = [$errors];
        }
        $ret = "<div class=\"error\">\n";
        $ret .= "<p><em>{$this->lang->encountered_errors}</em></p>\n";
        $ret .= "<ul>\n";
        foreach ($errors as $error) {
            $ret .= "<li>{$error}</li>\n";
        }
        $ret .= "</ul>\n";
        $ret .= "</div>\n";

        return $ret;
    }

    /**
     * Generate the login page.
     *
     * @param string $message The any message to output on the page if there is one.
     * @param string $class The class name of the message (defaults to success)
     */
    public function show_login($message = '', $class = 'success')
    {
        $args = [
            'this' => &$this,
            'message' => &$message,
            'class' => &$class
        ];

        $this->bb->plugins->runHooks('admin_page_show_login_start', $args);

        $copy_year = COPY_YEAR;

        $login_container_width = '';
        $login_label_width = '';

        // If the language string for "Username" is too
        // cramped then use this to define how much
        // larger you want the gap to be (in px)
        if (isset($this->lang->login_field_width)) {
            $login_label_width = " style=\"width: " . ((int)$this->lang->login_field_width + 100) . "px;\"";
            $login_container_width = " style=\"width: " . (410 + ((int)$this->lang->login_field_width)) . "px;\"";
        }
/*
        $login_page = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head profile="http://gmpg.org/xfn/1">
<title>{{ title }}</title>
<meta name="author" content="MyBB Group" />
<meta name="copyright" content="Copyright {{ copy_year }} MyBB Group." />
<link rel="stylesheet" href="{{ bbasset_url }}/admin/styles/{{ cp_style }}/login.css" type="text/css" />
<script type="text/javascript" src="{{ bbasset_url }}/jscripts/jquery.js"></script>
<script type="text/javascript" src="{{ bbasset_url }}/jscripts/general.js?ver=1807"></script>
<script type="text/javascript" src="{{ bbasset_url }}/admin/jscripts/admincp.js"></script>
<script type="text/javascript">
//<![CDATA[
	loading_text = '{{ loading_text }}';
//]]>
</script>
</head>
<body>
<div id="container"{{ login_container_width }}>
	<div id="header">
		<div id="logo">
			<h1><a href="{{ bburl }}" title="{{ return }}"><span class="invisible">{{ bbacp }}</span></a></h1>
		</div>
	</div>
	<div id="content">
		<h2>{{ please_login }}</h2>
        {% if flash_message is not empty %}
        <p id="message" class="{{ flash_class }}"><span class="text">{{ flash_message }}</span></p>
        {% endif %}
EOF;
*/
//        if ($message) {
//            $login_page .= "<p id=\"message\" class=\"{$class}\"><span class=\"text\">{$message}</span></p>";
//        }
        // Make query string nice and pretty so that user can go to his/her preferred destination
        $query_string = '';
        if ($_SERVER['QUERY_STRING']) {
            $query_string = '?' . preg_replace('#adminsid=(.{32})#i', '', $_SERVER['QUERY_STRING']);
            $query_string = preg_replace('#my_post_key=(.{32})#i', '', $query_string);
            $query_string = str_replace('action=logout', '', $query_string);
            $query_string = preg_replace('#&+#', '&', $query_string);
            $query_string = str_replace('?&', '?', $query_string);
            $query_string = htmlspecialchars_uni($query_string);
        }
        switch ($this->bb->settings['username_method']) {
            case 0:
                $lang_username = $this->lang->username;
                break;
            case 1:
                $lang_username = $this->lang->username1;
                break;
            case 2:
                $lang_username = $this->lang->username2;
                break;
            default:
                $lang_username = $this->lang->username;
                break;
        }

        // Secret PIN
        //global $config;
        if (isset($config['secret_pin']) && $config['secret_pin'] != '') {
            $secret_pin = "<div class=\"label\"{$login_label_width}><label for=\"pin\">{$this->lang->secret_pin}</label></div>
            <div class=\"field\"><input type=\"password\" name=\"pin\" id=\"pin\" class=\"text_input\" /></div>";
        } else {
            $secret_pin = '';
        }

        $login_lang_string = $this->lang->enter_username_and_password;

        switch ($this->bb->settings['username_method']) {
            case 0: // Username only
                $login_lang_string = $this->lang->sprintf($login_lang_string, $this->lang->login_username);
                break;
            case 1: // Email only
                $login_lang_string = $this->lang->sprintf($login_lang_string, $this->lang->login_email);
                break;
            case 2: // Username and email
            default:
                $login_lang_string = $this->lang->sprintf($login_lang_string, $this->lang->login_username_and_password);
                break;
        }

        //$_SERVER['PHP_SELF'] = htmlspecialchars_uni($_SERVER['PHP_SELF']);
/*
        $login_page .= <<<EOF
		<p>{{ login_lang_string }}</p>
		<form method="post" action="{{ bburl }}/admin{{ query_string }}">
		<div class="form_container">

			<div class="label"{{ login_label_width }}><label for="username">{{ lang_username }}</label></div>

			<div class="field"><input type="text" name="username" id="username" class="text_input initial_focus" /></div>

			<div class="label"{{ login_label_width }}><label for="password">{{ lang_password }}</label></div>
			<div class="field"><input type="password" name="password" id="password" class="text_input" /></div>
            {{ secret_pin }}
		</div>
		<p class="submit">
			<span class="forgot_password">
				<a href="{{ bburl }}/lostpw">{{ lang_lost_password }}</a>
			</span>

			<input type="submit" value="{{ lang_login }}" />
			<input type="hidden" name="do" value="login" />
		</p>
		</form>
	</div>
</div>
</body>
</html>
EOF;
*/
        $data = [
            'title' => $this->lang->mybb_admin_login,
            'copy_year' => $copy_year,
            'bbasset_url' => $this->bb->asset_url,
            'cp_style' => $this->bb->cp_style,
            'loading_text' => $this->lang->loading_text,
            'login_label_width' => $login_label_width,
            'login_container_width' => $login_container_width,
            'bburl' => $this->bb->settings['bburl'],
            'return' => $this->lang->return_to_forum,
            'bbacp' => $this->lang->mybb_acp,
            'please_login' => $this->lang->please_login,
            'flash_class' => $class,
            'flash_message' => $message,
            'login_lang_string' => $login_lang_string,
            'query_string' => $query_string,
            'lang_username' => $lang_username,
            'lang_password' => $this->lang->password,
            'secret_pin' => $secret_pin,
            'lang_lost_password' => $this->lang->lost_password,
            'lang_login' => $this->lang->login
        ];

        $args = [
            'this' => &$this,
            'login_page' => &$data//&$login_page
        ];

        $this->bb->plugins->runHooks('admin_page_show_login_end', $args);

        return $this->view->fetch('@forum/Misc/login.html.twig', $data);
//        return $login_page;
    }

    public function show_2fa()
    {
        //global $lang, $cp_style, $mybb;
        $copy_year = COPY_YEAR;
        $mybb2fa_page = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head profile="http://gmpg.org/xfn/1">
<title>{$this->lang->my2fa}</title>
<meta name="author" content="MyBB Group" />
<meta name="copyright" content="Copyright {$copy_year} MyBB Group." />
<link rel="stylesheet" href="{$this->bb->asset_url}/admin/styles/{$this->bb->cp_style}/login.css" type="text/css" />
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/jquery.js"></script>
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/general.js?ver=1807"></script>
<script type="text/javascript" src="{$this->bb->asset_url}/admin/jscripts/admincp.js"></script>
<script type="text/javascript">
//<![CDATA[
	loading_text = '{$this->lang->loading_text}';
//]]>
</script>
</head>
<body>
<div id="container">
	<div id="header">
		<div id="logo">
			<h1><a href="../" title="{$this->lang->return_to_forum}"><span class="invisible">{$this->lang->mybb_acp}</span></a></h1>
		</div>
	</div>
	<div id="content">
		<h2>{$this->lang->my2fa}</h2>
EOF;
        // Make query string nice and pretty so that user can go to his/her preferred destination
        $query_string = '';
        if ($_SERVER['QUERY_STRING']) {
            $query_string = '?' . preg_replace('#adminsid=(.{32})#i', '', $_SERVER['QUERY_STRING']);
            $query_string = preg_replace('#my_post_key=(.{32})#i', '', $query_string);
            $query_string = str_replace('action=logout', '', $query_string);
            $query_string = preg_replace('#&+#', '&', $query_string);
            $query_string = str_replace('?&', '?', $query_string);
            $query_string = htmlspecialchars_uni($query_string);
        }
        $mybb2fa_page .= <<<EOF
		<p>{$this->lang->my2fa_code}</p>
		<form method="post" action="index.php{$query_string}">
		<div class="form_container">
			<div class="label"><label for="code">{$this->lang->my2fa_label}</label></div>
			<div class="field"><input type="text" name="code" id="code" class="text_input initial_focus" /></div>
		</div>
		<p class="submit">
			<input type="submit" value="{$this->lang->login}" />
			<input type="hidden" name="do" value="do_2fa" />
		</p>
		</form>
	</div>
</div>
</body>
</html>
EOF;
        echo $mybb2fa_page;
        exit;
    }

    /**
     * Generate the lockout page
     *
     */
    public function show_lockedout()
    {
        $copy_year = COPY_YEAR;
        $allowed_attempts = (int)$this->bb->settings['maxloginattempts'];
        $lockedout_message = $this->lang->sprintf($this->lang->error_mybb_admin_lockedout_message, $allowed_attempts);

        print <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head profile="http://gmpg.org/xfn/1">
<title>{$this->lang->mybb_admin_cp} - {$this->lang->error_mybb_admin_lockedout}</title>
<meta name="author" content="MyBB Group" />
<meta name="copyright" content="Copyright {$copy_year} MyBB Group." />
<link rel="stylesheet" href="{$this->bb->asset_url}/admin/styles/{$this->bb->cp_style}/login.css" type="text/css" />
</head>
<body>
<div id="container">
	<div id="header">
		<div id="logo">
			<h1><a href="../" title="{$this->lang->return_to_forum}"><span class="invisible">{$this->lang->mybb_acp}</span></a></h1>
		</div>
	</div>
	<div id="content">
		<h2>{$this->lang->error_mybb_admin_lockedout}</h2>
		<div class="alert">{$lockedout_message}</div>
	</div>
</div>
</body>
</html>
EOF;
        exit;
    }

    /**
     * Generate the lockout unlock page
     *
     * @param string $message The any message to output on the page if there is one.
     * @param string $class The class name of the message (defaults to success)
     */
    public function show_lockout_unlock($message = '', $class = 'success')
    {
        $copy_year = COPY_YEAR;
        switch ($this->bb->settings['username_method']) {
            case 0:
                $lang_username = $this->lang->username;
                break;
            case 1:
                $lang_username = $this->lang->username1;
                break;
            case 2:
                $lang_username = $this->lang->username2;
                break;
            default:
                $lang_username = $this->lang->username;
                break;
        }
        $login_label_width = '';
        // If the language string for "Username" is too cramped then use this to define how much larger you want the gap to be (in px)
        if (isset($this->lang->login_field_width)) {
            $login_label_width = " style=\"width: " . ((int)$this->lang->login_field_width + 100) . "px;\"";
        }
        if ($message) {
            $message = "<p id=\"message\" class=\"{$class}\"><span class=\"text\">{$message}</span></p>";
        }

        print <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head profile="http://gmpg.org/xfn/1">
<title>{$this->lang->mybb_admin_cp} - {$this->lang->lockout_unlock}</title>
<meta name="author" content="MyBB Group" />
<meta name="copyright" content="Copyright {$copy_year} MyBB Group." />
<link rel="stylesheet" href="{$this->bb->asset_url}/admin/styles/{$this->bb->cp_style}/login.css" type="text/css" />
</head>
<body>
<div id="container">
	<div id="header">
		<div id="logo">
			<h1><a href="../" title="{$this->lang->return_to_forum}"><span class="invisible">{$this->lang->mybb_acp}</span></a></h1>
		</div>
	</div>
	<div id="content">
		<h2>{$this->lang->lockout_unlock}</h2>
		{$message}
		<p>{$this->lang->enter_username_and_token}</p>
		<form method="post" action="index.php">
		<div class="form_container">

			<div class="label"{$login_label_width}><label for="username">{$lang_username}</label></div>

			<div class="field"><input type="text" name="username" id="username" class="text_input initial_focus" /></div>

			<div class="label"{$login_label_width}><label for="token">{$this->lang->unlock_token}</label></div>
			<div class="field"><input type="text" name="token" id="token" class="text_input" /></div>
		</div>
		<p class="submit">
			<span class="forgot_password">
				<a href="../member.php?action=lostpw">{$this->lang->lost_password}</a>
			</span>

			<input type="submit" value="{$this->lang->unlock_account}" />
			<input type="hidden" name="action" value="unlock" />
		</p>
		</form>
	</div>
</div>
</body>
</html>
EOF;
        exit;
    }

    /**
     * Add an item to the primary navigation menu.
     *
     * @param string $title The title of the menu item.
     * @param string $id The ID of the menu item. This should correspond with the module the menu will run.
     * @param string $link The link to follow when the menu item is clicked.
     * @param int $order The display order of the menu item. Lower display order means closer to start of the menu.
     * @param array $submenu Array of sub menu items if there are any.
     */
    public function add_menu_item($title, $id, $link, $order = 10, $submenu = [])
    {
        $this->_menu[$order][] = [
            'title' => $title,
            'id' => $id,
            'link' => $link,
            'submenu' => $submenu
        ];
    }

    /**
     * Build the actual navigation menu.
     *
     * @return bool|string
     */
    private function _build_menu($user_menu = '')
    {
        if (!is_array($this->_menu)) {
            return false;
        }
        //$build_menu = "<div id=\"menu\">\n<ul>\n";
        $sub_menu = '';
        $build_menu = '<ul class="nav navbar-nav">';
        ksort($this->_menu);
        foreach ($this->_menu as $items) {
            foreach ($items as $menu_item) {
                $menu_item['link'] = htmlspecialchars_uni($menu_item['link']);
                if ($menu_item['id'] == $this->active_module) {
                    $sub_menu = $menu_item['submenu'];
                    $sub_menu_title = $menu_item['title'];
                    $build_menu .= "<li class=\"active\"><a href=\"{$menu_item['link']}\">{$menu_item['title']}</a></li>\n";
                } else {
                    $build_menu .= "<li><a href=\"{$menu_item['link']}\">{$menu_item['title']}</a></li>\n";
                }
            }
        }
        $build_menu .= $user_menu;
        //$build_menu .= "</ul>\n</div>";
        $build_menu .= '</ul>';

        if ($sub_menu) {
            $this->_build_submenu($sub_menu_title, $sub_menu);
        }
        return $build_menu;
    }

    /**
     * Build a navigation sub menu if we have one.
     *
     * @param string $title A title for the sub menu.
     * @param array $items Array of items for the sub menu.
     */
    private function _build_submenu($title, $items)
    {
        if (is_array($items)) {
            $sidebar = new SideBarItem($this->bb, $title);
            $sidebar->add_menu_items($items, $this->active_action);
            $this->submenu .= $sidebar->get_markup();
        }
    }

    /**
     * Output a Javascript based tab control on to the page.
     *
     * @param array $tabs Array of tabs in name => title format. Name should correspond to the name of a DIV containing the tab content.
     * @param boolean $observe_onload Whether or not to run the event onload or instantly
     * @param string $id The ID to use for the tabs for if you run multiple instances of the tabbing control in one html page
     *
     * @return string tab control
     */
    public function output_tab_control($tabs = [], $observe_onload = true, $id = 'tabs')
    {
        $tabs = $this->bb->plugins->runHooks('admin_page_output_tab_control_start', $tabs);
        $ret = "<ul class=\"tabs\" id=\"{$id}\">\n";
        $tab_count = count($tabs);
        $done = 1;
        foreach ($tabs as $anchor => $title) {
            $class = '';
            if ($tab_count == $done) {
                $class .= ' last';
            }
            if ($done == 1) {
                $class .= ' first';
            }
            ++$done;
            $ret .= "<li class=\"{$class}\"><a href=\"#tab_{$anchor}\">{$title}</a></li>\n";
        }
        $ret .= "</ul>\n";
        $this->bb->plugins->runHooks('admin_page_output_tab_control_end', $tabs);
        return $ret;
    }

    /**
     * Output a series of primary navigation tabs for swithcing between items within a particular module/action.
     *
     * @param array $tabs Nested array of tabs containing possible keys of align, link_target, link, title.
     * @param string $active The name of the active tab. Corresponds with the key of each tab item.
     *
     * @return string nav tabs
     */
    public function output_nav_tabs($tabs = [], $active = '')
    {
        $tabs = $this->bb->plugins->runHooks("admin_page_output_nav_tabs_start", $tabs);
        $ret = "<div class=\"nav_tabs\">";
        $ret .= "\t<ul>\n";
        foreach ($tabs as $id => $tab) {
            $class = '';
            if ($id == $active) {
                $class = ' active';
            }
            if (isset($tab['align']) == "right") {
                $class .= " right";
            }
            $target = '';
            if (isset($tab['link_target'])) {
                $target = " target=\"{$tab['link_target']}\"";
            }
            if (!isset($tab['link'])) {
                $tab['link'] = '';
            }
            $ret .= "\t\t<li class=\"{$class}\"><a href=\"{$tab['link']}\"{$target}>{$tab['title']}</a></li>\n";
            $target = '';
        }
        $ret .= "\t</ul>\n";
        if ($tabs[$active]['description']) {
            $ret .= "\t<div class=\"tab_description\">{$tabs[$active]['description']}</div>\n";
        }
        $ret .= '</div>';
        $arguments = ['tabs' => $tabs, 'active' => $active];
        $this->bb->plugins->runHooks("admin_page_output_nav_tabs_end", $arguments);
        return $ret;
    }

    /**
     * Output a page asking if a user wishes to continue performing a specific action.
     *
     * @param string $url The URL to be forwarded to.
     * @param string $message The confirmation message to output.
     * @param string $title The title to use in the output header
     */
    public function output_confirm_action($url, $message = '', $title = '')
    {
        $args = [
            'this' => &$this,
            'url' => &$url,
            'message' => &$message,
            'title' => &$title,
        ];

        $this->bb->plugins->runHooks('admin_page_output_confirm_action', $args);

        if (!$message) {
            $message = $this->lang->confirm_action;
        }
        $html = $this->output_header($title);
        $form = new Form($this->bb, $url, 'post');
        $html .= $form->getForm();

        $html .= "<div class=\"confirm_action\">\n";
        $html .= "<p>{$message}</p>\n";
        $html .= "<br />\n";
        $html .= "<p class=\"buttons\">\n";
        $html .= $form->generate_submit_button($this->lang->yes, ['class' => 'button_yes']);
        $html .= $form->generate_submit_button($this->lang->no, ['name' => 'no', 'class' => 'button_no']);
        $html .= "</p>\n";
        $html .= "</div>\n";

        $html .= $form->end();
        $this->output_footer();

        $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
        return $this->view->render($this->response, '@forum/Admin/Confirm.html.twig');
    }
}
