<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Admin\Helpers;

/**
 * Generate a Javascript based popup menu.
 */
class PopupMenu
{
    /**
     * @var string The title of the popup menu to be shown on the button.
     */
    private $_title;

    /**
     * @var string The ID of this popup menu. Must be unique.
     */
    private $_id;

    /**
     * @var string Built HTML for the items in the popup menu.
     */
    private $_items;

    /**
     * Initialise a new popup menu.
     *
     * @var string $id The ID of the popup menu.
     * @var string $title The title of the popup menu.
     */
    public function __construct($id, $title = '')
    {
        $this->_id = $id;
        $this->_title = $title;
    }

    /**
     * Add an item to the popup menu.
     *
     * @param string $text The title of this item.
     * @param string $link The page this item should link to.
     * @param string $onclick The onclick event handler if we have one.
     */
    public function add_item($text, $link, $onclick = '')
    {
        if ($onclick) {
            $onclick = " onclick=\"{$onclick}\"";
        }
        $this->_items .= "<div class=\"popup_item_container\"><a href=\"{$link}\"{$onclick} class=\"popup_item\">{$text}</a></div>\n";
    }

    /**
     * Fetch the contents of the popup menu.
     *
     * @return string The popup menu.
     */
    public function fetch()
    {
        $popup = "<div class=\"popup_menu\" id=\"{$this->_id}_popup\">\n{$this->_items}</div>\n";
        if ($this->_title) {
            $popup .= "<a href=\"javascript:;\" id=\"{$this->_id}\" class=\"popup_button\">{$this->_title}</a>\n";
        }
        $popup .= "<script type=\"text/javascript\">\n";
        $popup .= "$(\"#{$this->_id}\").popupMenu();\n";
        $popup .= "</script>\n";
        return $popup;
    }

    /**
     * Outputs a popup menu to the browser.
     */
    public function output()
    {
        echo $this->fetch();
    }
}
