<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Helpers;

/**
 * A class for generating side bar blocks.
 */
class SidebarItem
{
    /**
     * @var string The title of the side bar block.
     */
    private $_title;

    /**
     * @var string The contents of the side bar block.
     */
    private $_contents;

    private $bb;

    /**
     * Constructor. Set the title of the side bar block.
     *
     * @param string $title The title of the side bar block.
     */
    public function __construct(& $bb, $title = '')
    {
        $this->bb = $bb;
        $this->_title = $title;
    }

    /**
     * Add menus item to the side bar block.
     *
     * @param array $items Array of menu items to add. Each menu item should be a nested array of id, link and title.
     * @param string $active The ID of the active menu item if there is one.
     */
    public function add_menu_items($items, $active)
    {
//        $this->_contents = '<ul class="menu">';
        $this->_contents = '<ul class="list-group">';
        foreach ($items as $item) {
            if (!$this->bb->adm->check_admin_permissions(['module' => $this->bb->adm->run_module, 'action' => $item['id']], false)) {
                continue;
            }

            $class = '';
            if ($item['id'] == $active) {
                $class = 'list-group-item-info';// active';
            }
            $item['link'] = htmlspecialchars_uni($item['link']);
            $this->_contents .= "<li class=\"list-group-item {$class}\"><a href=\"{$item['link']}\">{$item['title']}</a></li>\n";
        }
        $this->_contents .= '</ul>';
    }

    /**
     * Sets custom html to the contents variable
     *
     * @param string $html The custom html to set
     */
    public function set_contents($html)
    {
        $this->_contents = $html;
    }

    /**
     * Fetch the HTML markup for the side bar box.
     *
     * @return string
     */
    public function get_markup()
    {
//        $markup = "<div class=\"left_menu_box\">\n";
//        $markup .= "<div class=\"title\">{$this->_title}</div>\n";
        $markup = "<li class=\"dropdown-header\">{$this->_title}</li>\n";
        if ($this->_contents) {
            $markup .= $this->_contents;
        }
//        $markup .= "</div>\n";
        $markup .= "<li class=\"divider\"></li>\n";
        return $markup;
    }
}
