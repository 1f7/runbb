<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class AttachmentTypes extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_attachment_types', false, true);

        $this->page->add_breadcrumb_item($this->lang->attachment_types, $this->bb->admin_url . '/config/attachment_types');

        $this->plugins->runHooks('admin_config_attachment_types_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_attachment_types_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['mimetype']) && !trim($this->bb->input['extension'])) {
                    $errors[] = $this->lang->error_missing_mime_type;
                }

                if (!trim($this->bb->input['extension']) && !trim($this->bb->input['mimetype'])) {
                    $errors[] = $this->lang->error_missing_extension;
                }

                if (!isset($errors)) {
                    if ($this->bb->input['mimetype'] == 'images/attachtypes/') {
                        $this->bb->input['mimetype'] = '';
                    }

                    if (substr($this->bb->input['extension'], 0, 1) == '.') {
                        $this->bb->input['extension'] = substr($this->bb->input['extension'], 1);
                    }

                    $maxsize = $this->bb->getInput('maxsize', 0);

                    if ($maxsize == 0) {
                        $maxsize = '';
                    }

                    $new_type = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'mimetype' => $this->db->escape_string($this->bb->input['mimetype']),
                        'extension' => $this->db->escape_string($this->bb->input['extension']),
                        'maxsize' => $maxsize,
                        'icon' => $this->db->escape_string($this->bb->input['icon'])
                    ];

                    $atid = $this->db->insert_query('attachtypes', $new_type);

                    $this->plugins->runHooks('admin_config_attachment_types_add_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($atid, htmlspecialchars_uni($this->bb->input['extension']));

                    $this->cache->update_attachtypes();

                    $this->session->flash_message($this->lang->success_attachment_type_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/attachment_types');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_attachment_type);
            $html = $this->page->output_header($this->lang->attachment_types . ' - ' . $this->lang->add_new_attachment_type);

            $sub_tabs['attachment_types'] = [
                'title' => $this->lang->attachment_types,
                'link' => $this->bb->admin_url . '/config/attachment_types'
            ];

            $sub_tabs['add_attachment_type'] = [
                'title' => $this->lang->add_new_attachment_type,
                'link' => $this->bb->admin_url . '/config/attachment_types?action=add',
                'description' => $this->lang->add_attachment_type_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_attachment_type');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/attachment_types?action=add', 'post', 'add');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['maxsize'] = '1024';
                $this->bb->input['icon'] = 'images/attachtypes/';
            }

            // PHP settings
            $upload_max_filesize = @ini_get('upload_max_filesize');
            $post_max_size = @ini_get('post_max_size');
            $limit_string = '';
            if ($upload_max_filesize || $post_max_size) {
                $limit_string = '<br /><br />' . $this->lang->limit_intro;
                if ($upload_max_filesize) {
                    $limit_string .= '<br />' . $this->lang->sprintf($this->lang->limit_upload_max_filesize, $upload_max_filesize);
                }
                if ($post_max_size) {
                    $limit_string .= '<br />' . $this->lang->sprintf($this->lang->limit_post_max_size, $post_max_size);
                }
            }

            $form_container = new FormContainer($this, $this->lang->add_new_attachment_type);
            $form_container->output_row($this->lang->name, $this->lang->name_desc, $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->file_extension . ' <em>*</em>', $this->lang->file_extension_desc, $form->generate_text_box('extension', $this->bb->getInput('extension', ''), ['id' => 'extension']), 'extension');
            $form_container->output_row($this->lang->mime_type . ' <em>*</em>', $this->lang->mime_type_desc, $form->generate_text_box('mimetype', $this->bb->getInput('mimetype', ''), ['id' => 'mimetype']), 'mimetype');
            $form_container->output_row($this->lang->maximum_file_size, $this->lang->maximum_file_size_desc . $limit_string, $form->generate_numeric_field('maxsize', $this->bb->input['maxsize'], ['id' => 'maxsize', 'min' => 0]), 'maxsize');
            $form_container->output_row($this->lang->attachment_icon, $this->lang->attachment_icon_desc, $form->generate_text_box('icon', $this->bb->input['icon'], ['id' => 'icon']), 'icon');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_attachment_type);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/AttachmentTypesAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('attachtypes', '*', "atid='" . $this->bb->getInput('atid', 0) . "'");
            $attachment_type = $this->db->fetch_array($query);

            if (!$attachment_type['atid']) {
                $this->session->flash_message($this->lang->error_invalid_attachment_type, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/attachment_types');
            }

            $this->plugins->runHooks('admin_config_attachment_types_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['mimetype']) && !trim($this->bb->input['extension'])) {
                    $errors[] = $this->lang->error_missing_mime_type;
                }

                if (!trim($this->bb->input['extension']) && !trim($this->bb->input['mimetype'])) {
                    $errors[] = $this->lang->error_missing_extension;
                }

                if (!isset($errors)) {
                    if ($this->bb->input['mimetype'] == 'images/attachtypes/') {
                        $this->bb->input['mimetype'] = '';
                    }

                    if (substr($this->bb->input['extension'], 0, 1) == '.') {
                        $this->bb->input['extension'] = substr($this->bb->input['extension'], 1);
                    }

                    $updated_type = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'mimetype' => $this->db->escape_string($this->bb->input['mimetype']),
                        'extension' => $this->db->escape_string($this->bb->input['extension']),
                        'maxsize' => $this->bb->getInput('maxsize', 0),
                        'icon' => $this->db->escape_string($this->bb->input['icon'])
                    ];

                    $this->plugins->runHooks('admin_config_attachment_types_edit_commit');

                    $this->db->update_query('attachtypes', $updated_type, "atid='{$attachment_type['atid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($attachment_type['atid'], htmlspecialchars_uni($this->bb->input['extension']));

                    $this->cache->update_attachtypes();

                    $this->session->flash_message($this->lang->success_attachment_type_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/attachment_types');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_attachment_type);
            $html = $this->page->output_header($this->lang->attachment_types . ' - ' . $this->lang->edit_attachment_type);

            $sub_tabs['edit_attachment_type'] = [
                'title' => $this->lang->edit_attachment_type,
                'link' => $this->bb->admin_url . '/config/attachment_types?action=edit&atid=' . $attachment_type['atid'],
                'description' => $this->lang->edit_attachment_type_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_attachment_type');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/attachment_types?action=edit&atid=' . $attachment_type['atid'], 'post', 'add');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, $attachment_type);
            }

            // PHP settings
            $upload_max_filesize = @ini_get('upload_max_filesize');
            $post_max_size = @ini_get('post_max_size');
            $limit_string = '';
            if ($upload_max_filesize || $post_max_size) {
                $limit_string = '<br /><br />' . $this->lang->limit_intro;
                if ($upload_max_filesize) {
                    $limit_string .= '<br />' . $this->lang->sprintf($this->lang->limit_upload_max_filesize, $upload_max_filesize);
                }
                if ($post_max_size) {
                    $limit_string .= '<br />' . $this->lang->sprintf($this->lang->limit_post_max_size, $post_max_size);
                }
            }

            $form_container = new FormContainer($this, $this->lang->edit_attachment_type);
            $form_container->output_row($this->lang->name, $this->lang->name_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->file_extension . ' <em>*</em>', $this->lang->file_extension_desc, $form->generate_text_box('extension', $this->bb->input['extension'], ['id' => 'extension']), 'extension');
            $form_container->output_row($this->lang->mime_type . ' <em>*</em>', $this->lang->mime_type_desc, $form->generate_text_box('mimetype', $this->bb->input['mimetype'], ['id' => 'mimetype']), 'mimetype');
            $form_container->output_row($this->lang->maximum_file_size, $this->lang->maximum_file_size_desc . $limit_string, $form->generate_numeric_field('maxsize', $this->bb->input['maxsize'], ['id' => 'maxsize', 'min' => 0]), 'maxsize');
            $form_container->output_row($this->lang->attachment_icon, $this->lang->attachment_icon_desc, $form->generate_text_box('icon', $this->bb->input['icon'], ['id' => 'icon']), 'icon');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_attachment_type);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/AttachmentTypesEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/attachment_types');
            }

            $query = $this->db->simple_select('attachtypes', '*', "atid='" . $this->bb->getInput('atid', 0) . "'");
            $attachment_type = $this->db->fetch_array($query);

            if (!$attachment_type['atid']) {
                $this->session->flash_message($this->lang->error_invalid_attachment_type, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/attachment_types');
            }

            $this->plugins->runHooks('admin_config_attachment_types_delete');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('attachtypes', "atid='{$attachment_type['atid']}'");

                $this->plugins->runHooks('admin_config_attachment_types_delete_commit');

                $this->cache->update_attachtypes();

                // Log admin action
                $this->bblogger->log_admin_action($attachment_type['atid'], htmlspecialchars_uni($attachment_type['extension']));

                $this->session->flash_message($this->lang->success_attachment_type_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/attachment_types');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/attachment_types?action=delete&atid=' . $attachment_type['atid'], $this->lang->confirm_attachment_type_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->attachment_types);

            $sub_tabs['attachment_types'] = [
                'title' => $this->lang->attachment_types,
                'link' => $this->bb->admin_url . '/config/attachment_types',
                'description' => $this->lang->attachment_types_desc
            ];
            $sub_tabs['add_attachment_type'] = [
                'title' => $this->lang->add_new_attachment_type,
                'link' => $this->bb->admin_url . '/config/attachment_types?action=add',
            ];

            $this->plugins->runHooks('admin_config_attachment_types_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'attachment_types');

            $table = new Table;
            $table->construct_header($this->lang->extension, ['colspan' => 2]);
            $table->construct_header($this->lang->mime_type);
            $table->construct_header($this->lang->maximum_size, ['class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('attachtypes', '*', '', ['order_by' => 'extension']);
            while ($attachment_type = $this->db->fetch_array($query)) {
                // Just show default icons in ACP
                $attachment_type['icon'] = htmlspecialchars_uni(str_replace('{theme}', 'images', $attachment_type['icon']));
                if (my_strpos($attachment_type['icon'], 'p://') || substr($attachment_type['icon'], 0, 1) == '/') {
                    $image = $attachment_type['icon'];
                } else {
                    $image = $this->bb->asset_url . '/' . $attachment_type['icon'];
                }

                if (!$attachment_type['icon'] || $attachment_type['icon'] == 'images/attachtypes/') {
                    $attachment_type['icon'] = '&nbsp;';
                } else {
                    $attachment_type['name'] = htmlspecialchars_uni($attachment_type['name']);
                    $attachment_type['icon'] = "<img src=\"{$image}\" title=\"{$attachment_type['name']}\" alt=\"\" />";
                }

                $table->construct_cell($attachment_type['icon'], ['width' => 1]);
                $table->construct_cell("<strong>.{$attachment_type['extension']}</strong>");
                $table->construct_cell(htmlspecialchars_uni($attachment_type['mimetype']));
                $table->construct_cell($this->parser->friendlySize(($attachment_type['maxsize'] * 1024)), ['class' => 'align_center']);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/attachment_types?action=edit&amp;atid={$attachment_type['atid']}\">{$this->lang->edit}</a>", ['class' => 'align_center']);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/attachment_types?action=delete&amp;atid={$attachment_type['atid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_attachment_type_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_attachment_types, ['colspan' => 6]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->attachment_types);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/AttachmentTypes.html.twig');
        }
    }
}
