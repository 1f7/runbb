<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Badwords extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_badwords', false, true);

        $this->page->add_breadcrumb_item($this->lang->bad_words, $this->bb->admin_url . '/config/badwords');

        $this->plugins->runHooks('admin_config_badwords_begin');

        if ($this->bb->input['action'] == 'add' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_config_badwords_add');

            if (!trim($this->bb->input['badword'])) {
                $errors[] = $this->lang->error_missing_bad_word;
            }

            if (strlen(trim($this->bb->input['badword'])) > 100) {
                $errors[] = $this->lang->bad_word_max;
            }

            if (strlen($this->bb->input['replacement']) > 100) {
                $errors[] = $this->lang->replacement_word_max;
            }

            if (!isset($errors)) {
                $query = $this->db->simple_select('badwords', 'bid', "badword = '" . $this->db->escape_string($this->bb->input['badword']) . "'");

                if ($this->db->num_rows($query)) {
                    $errors[] = $this->lang->error_bad_word_filtered;
                }
            }

            $badword = str_replace('\*', '([a-zA-Z0-9_]{1})', preg_quote($this->bb->input['badword'], '#'));

            // Don't allow certain badword replacements to be added if it would cause an infinite recursive loop.
            if (strlen($this->bb->input['badword']) == strlen($this->bb->input['replacement']) && preg_match("#(^|\W)" . $badword . "(\W|$)#i", $this->bb->input['replacement'])) {
                $errors[] = $this->lang->error_replacement_word_invalid;
            }

            if (!isset($errors)) {
                $new_badword = [
                    'badword' => $this->db->escape_string($this->bb->input['badword']),
                    'replacement' => $this->db->escape_string($this->bb->input['replacement'])
                ];

                $bid = $this->db->insert_query('badwords', $new_badword);

                $this->plugins->runHooks('admin_config_badwords_add_commit');

                // Log admin action
                $this->bblogger->log_admin_action($bid, $this->bb->input['badword']);

                $this->cache->update_badwords();
                $this->session->flash_message($this->lang->success_added_bad_word, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/badwords');
            } else {
                $this->bb->input['action'] = '';
            }
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('badwords', '*', "bid='" . $this->bb->getInput('bid', 0) . "'");
            $badword = $this->db->fetch_array($query);

            // Does the bad word not exist?
            if (!$badword['bid']) {
                $this->session->flash_message($this->lang->error_invalid_bid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/badwords');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/badwords');
            }

            $this->plugins->runHooks('admin_config_badwords_delete');

            if ($this->bb->request_method == 'post') {
                // Delete the bad word
                $this->db->delete_query('badwords', "bid='{$badword['bid']}'");

                $this->plugins->runHooks('admin_config_badwords_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($badword['bid'], $badword['badword']);

                $this->cache->update_badwords();

                $this->session->flash_message($this->lang->success_deleted_bad_word, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/badwords');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/badwords?action=delete&bid=' . $badword['bid'], $this->lang->confirm_bad_word_deletion);
            }
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('badwords', '*', "bid='" . $this->bb->getInput('bid', 0) . "'");
            $badword = $this->db->fetch_array($query);

            // Does the bad word not exist?
            if (!$badword['bid']) {
                $this->session->flash_message($this->lang->error_invalid_bid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/badwords');
            }

            $this->plugins->runHooks('admin_config_badwords_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['badword'])) {
                    $errors[] = $this->lang->error_missing_bad_word;
                }

                if (strlen(trim($this->bb->input['badword'])) > 100) {
                    $errors[] = $this->lang->bad_word_max;
                }

                if (strlen($this->bb->input['replacement']) > 100) {
                    $errors[] = $this->lang->replacement_word_max;
                }

                if (!isset($errors)) {
                    $updated_badword = [
                        'badword' => $this->db->escape_string($this->bb->input['badword']),
                        'replacement' => $this->db->escape_string($this->bb->input['replacement'])
                    ];

                    $this->plugins->runHooks('admin_config_badwords_edit_commit');

                    $this->db->update_query('badwords', $updated_badword, "bid='{$badword['bid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($badword['bid'], $this->bb->input['badword']);

                    $this->cache->update_badwords();

                    $this->session->flash_message($this->lang->success_updated_bad_word, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/badwords');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_bad_word);
            $html = $this->page->output_header($this->lang->bad_words . ' - ' . $this->lang->edit_bad_word);

            $sub_tabs['editbadword'] = [
                'title' => $this->lang->edit_bad_word,
                'description' => $this->lang->edit_bad_word_desc,
                'link' => $this->bb->admin_url . '/config/badwords'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'editbadword');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/badwords?action=edit&bid=' . $badword['bid'], 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
                $badword_data = $this->bb->input;
            } else {
                $badword_data = $badword;
            }

            $form_container = new FormContainer($this, $this->lang->edit_bad_word);
            $form_container->output_row($this->lang->bad_word . ' <em>*</em>', $this->lang->bad_word_desc, $form->generate_text_box('badword', $badword_data['badword'], ['id' => 'badword']), 'badword');
            $form_container->output_row($this->lang->replacement, $this->lang->replacement_desc, $form->generate_text_box('replacement', $badword_data['replacement'], ['id' => 'replacement']), 'replacement');
            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->save_bad_word);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/BadwordsEdit.html.twig');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->bad_words);

            $sub_tabs['badwords'] = [
                'title' => $this->lang->bad_word_filters,
                'description' => $this->lang->bad_word_filters_desc,
                'link' => $this->bb->admin_url . '/config/badwords'
            ];

            $this->plugins->runHooks('admin_config_badwords_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'badwords');

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $table = new Table;
            $table->construct_header($this->lang->bad_word);
            $table->construct_header($this->lang->replacement, ['width' => '50%']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150, 'colspan' => 2]);

            $query = $this->db->simple_select('badwords', '*', '', ['order_by' => 'badword', 'order_dir' => 'asc']);
            while ($badword = $this->db->fetch_array($query)) {
                $badword['badword'] = htmlspecialchars_uni($badword['badword']);
                $badword['replacement'] = htmlspecialchars_uni($badword['replacement']);
                if (!$badword['replacement']) {
                    $badword['replacement'] = '*****';
                }
                $table->construct_cell($badword['badword']);
                $table->construct_cell($badword['replacement']);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/badwords?action=edit&amp;bid={$badword['bid']}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/badwords?action=delete&amp;bid={$badword['bid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_bad_word_deletion}');\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_bad_words, ['colspan' => 4]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->bad_word_filters);

            $form = new Form($this->bb, $this->bb->admin_url . '/config/badwords?action=add', 'post', 'add');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->add_bad_word);
            $form_container->output_row($this->lang->bad_word . ' <em>*</em>', $this->lang->bad_word_desc, $form->generate_text_box('badword', ($this->bb->getInput('badword', '')), ['id' => 'badword']), 'badword');
            $form_container->output_row($this->lang->replacement, $this->lang->replacement_desc, $form->generate_text_box('replacement', ($this->bb->getInput('replacement', '')), ['id' => 'replacement']), 'replacement');
            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->save_bad_word);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Badwords.html.twig');
        }
    }
}
