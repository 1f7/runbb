<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;
use Carbon\Carbon;

class Banning extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_banning', false, true);

        $this->page->add_breadcrumb_item($this->lang->banning, $this->bb->admin_url . '/config/banning');

        $this->plugins->runHooks('admin_config_banning_begin');

        if ($this->bb->input['action'] == 'add' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_config_banning_add');

            if (!trim($this->bb->input['filter'])) {
                $errors[] = $this->lang->error_missing_ban_input;
            }

            $query = $this->db->simple_select('banfilters', 'fid', "filter = '" . $this->db->escape_string($this->bb->input['filter']) . "' AND type = '" . $this->bb->getInput('type', 0) . "'");
            if ($this->db->num_rows($query)) {
                $errors[] = $this->lang->error_filter_already_banned;
            }

            if (!isset($errors)) {
                $new_filter = [
                    'filter' => $this->db->escape_string($this->bb->input['filter']),
                    'type' => $this->bb->getInput('type', 0),
                    'dateline' => TIME_NOW
                ];
                $fid = $this->db->insert_query('banfilters', $new_filter);

                $this->plugins->runHooks('admin_config_banning_add_commit');

                if ($this->bb->input['type'] == 1) {
                    $this->cache->update_bannedips();
                } elseif ($this->bb->input['type'] == 3) {
                    $this->cache->update_bannedemails();
                }

                // Log admin action
                $this->bblogger->log_admin_action($fid, htmlspecialchars_uni($this->bb->input['filter']), (int)$this->bb->input['type']);

                if ($this->bb->input['type'] == 1) {
                    $this->session->flash_message($this->lang->success_ip_banned, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/banning?');
                } elseif ($this->bb->input['type'] == 2) {
                    $this->session->flash_message($this->lang->success_username_disallowed, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/banning?type=usernames');
                } elseif ($this->bb->input['type'] == 3) {
                    $this->session->flash_message($this->lang->success_email_disallowed, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/banning?type=emails');
                }
            } else {
                if ($this->bb->input['type'] == 1) {
                    $this->bb->input['type'] = 'ips';
                } elseif ($this->bb->input['type'] == 2) {
                    $this->bb->input['type'] = 'usernames';
                } elseif ($this->bb->input['type'] == 3) {
                    $this->bb->input['type'] = 'emails';
                }
                $this->bb->input['action'] = '';
            }
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('banfilters', '*', "fid='" . $this->bb->getInput('fid', 0) . "'");
            $filter = $this->db->fetch_array($query);

            // Does the filter not exist?
            if (!$filter['fid']) {
                $this->session->flash_message($this->lang->error_invalid_filter, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/banning');
            }

            $this->plugins->runHooks('admin_config_banning_delete');

            if ($filter['type'] == 3) {
                $type = 'emails';
            } elseif ($filter['type'] == 2) {
                $type = 'usernames';
            } else {
                $type = 'ips';
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/banning?type=' . $type);
            }

            if ($this->bb->request_method == 'post') {
                // Delete the ban filter
                $this->db->delete_query('banfilters', "fid='{$filter['fid']}'");

                $this->plugins->runHooks('admin_config_banning_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($filter['fid'], htmlspecialchars_uni($filter['filter']), (int)$filter['type']);

                // Banned IP? Rebuild banned IP cache
                if ($filter['type'] == 1) {
                    $this->cache->update_bannedips();
                } elseif ($filter['type'] == 3) {
                    $this->cache->update_bannedemails();
                }

                $this->session->flash_message($this->lang->success_ban_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/banning?type=' . $type);
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/banning?action=delete&fid=' . $filter['fid'], $this->lang->confirm_ban_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_banning_start');

            $this->bb->input['type'] = isset($this->bb->input['type']) ? $this->bb->input['type'] : '';
            switch ($this->bb->input['type']) {
                case 'emails':
                    $type = '3';
                    $title = $this->lang->disallowed_email_addresses;
                    break;
                case 'usernames':
                    $type = '2';
                    $title = $this->lang->disallowed_usernames;
                    break;
                default:
                    $type = '1';
                    $title = $this->lang->banned_ip_addresses;
                    $this->bb->input['type'] = 'ips';
            }

            $html = $this->page->output_header($title);

            $sub_tabs['ips'] = [
                'title' => $this->lang->banned_ips,
                'link' => $this->bb->admin_url . '/config/banning',
                'description' => $this->lang->banned_ips_desc
            ];

            $sub_tabs['users'] = [
                'title' => $this->lang->banned_accounts,
                'link' => $this->bb->admin_url . '/users/banning'
            ];

            $sub_tabs['usernames'] = [
                'title' => $this->lang->disallowed_usernames,
                'link' => $this->bb->admin_url . '/config/banning?type=usernames',
                'description' => $this->lang->disallowed_usernames_desc
            ];

            $sub_tabs['emails'] = [
                'title' => $this->lang->disallowed_email_addresses,
                'link' => $this->bb->admin_url . '/config/banning?type=emails',
                'description' => $this->lang->disallowed_email_addresses_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, $this->bb->input['type']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $table = new Table;
            if ($this->bb->input['type'] == 'usernames') {
                $table->construct_header($this->lang->username);
                $table->construct_header($this->lang->date_disallowed, ['class' => 'align_center', 'width' => 200]);
                $table->construct_header($this->lang->last_attempted_use, ['class' => 'align_center', 'width' => 200]);
            } elseif ($this->bb->input['type'] == 'emails') {
                $table->construct_header($this->lang->email_address);
                $table->construct_header($this->lang->date_disallowed, ['class' => 'align_center', 'width' => 200]);
                $table->construct_header($this->lang->last_attempted_use, ['class' => 'align_center', 'width' => 200]);
            } else {
                $table->construct_header($this->lang->ip_address);
                $table->construct_header($this->lang->ban_date, ['class' => 'align_center', 'width' => 200]);
                $table->construct_header($this->lang->last_access, ['class' => 'align_center', 'width' => 200]);
            }
            $table->construct_header($this->lang->controls, ['width' => 1]);

            $query = $this->db->simple_select('banfilters', '*', "type='{$type}'", ['order_by' => 'filter', 'order_dir' => 'asc']);
            while ($filter = $this->db->fetch_array($query)) {
                $filter['filter'] = empty($filter['filter']) ? 'NoFilter' : htmlspecialchars_uni($filter['filter']);

                if ($filter['lastuse'] > 0) {
                    $last_use = Carbon::createFromTimestamp($filter['lastuse'])->toDayDateTimeString();
                } else {
                    $last_use = $this->lang->never;
                }

                if ($filter['dateline'] > 0) {
                    $date = Carbon::createFromTimestamp($filter['dateline'])->toDayDateTimeString();
                } else {
                    $date = $this->lang->na;
                }

                $table->construct_cell($filter['filter']);
                $table->construct_cell($date, ['class' => 'align_center']);
                $table->construct_cell($last_use, ['class' => 'align_center']);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/banning?action=delete&amp;fid={$filter['fid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_ban_deletion}');\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/delete.png\" title=\"{$this->lang->delete}\" alt=\"{$this->lang->delete}\" /></a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_bans, ['colspan' => 4]);
                $table->construct_row();
            }

            $html .= $table->output($title);

            $form = new Form($this->bb, $this->bb->admin_url . '/config/banning?action=add', 'post', 'add');
            $html .= $form->getForm();

            $this->bb->input['filter'] = isset($this->bb->input['filter']) ? $this->bb->input['filter'] : '';
            if ($this->bb->input['type'] == 'usernames') {
                $form_container = new FormContainer($this, $this->lang->add_disallowed_username);
                $form_container->output_row($this->lang->username . ' <em>*</em>', $this->lang->username_desc, $form->generate_text_box('filter', $this->bb->input['filter'], ['id' => 'filter']), 'filter');
                $buttons[] = $form->generate_submit_button($this->lang->disallow_username);
            } elseif ($this->bb->input['type'] == 'emails') {
                $form_container = new FormContainer($this, $this->lang->add_disallowed_email_address);
                $form_container->output_row($this->lang->email_address . ' <em>*</em>', $this->lang->email_address_desc, $form->generate_text_box('filter', $this->bb->input['filter'], ['id' => 'filter']), 'filter');
                $buttons[] = $form->generate_submit_button($this->lang->disallow_email_address);
            } else {
                $form_container = new FormContainer($this, $this->lang->ban_an_ip_address);
                $form_container->output_row($this->lang->ip_address . ' <em>*</em>', $this->lang->ip_address_desc, $form->generate_text_box('filter', $this->bb->input['filter'], ['id' => 'filter']), 'filter');
                $buttons[] = $form->generate_submit_button($this->lang->ban_ip_address);
            }

            $html .= $form_container->end();
            $html .= $form->generate_hidden_field('type', $type);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Ban.html.twig');
        }
    }
}
