<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Calendars extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_calendars', false, true);
        $this->lang->load('configs_profile_fields', false, true);

        $this->page->add_breadcrumb_item($this->lang->calendars, $this->bb->admin_url . '/config/calendars');

        if ($this->bb->input['action'] == 'add' || $this->bb->input['action'] == 'permissions' || !$this->bb->input['action']) {
            $sub_tabs['manage_calendars'] = [
                'title' => $this->lang->manage_calendars,
                'link' => $this->bb->admin_url . '/config/calendars',
                'description' => $this->lang->manage_calendars_desc
            ];
            $sub_tabs['add_calendar'] = [
                'title' => $this->lang->add_calendar,
                'link' => $this->bb->admin_url . '/config/calendars?action=add',
            ];
        }

        $this->plugins->runHooks('admin_config_calendars_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_calendars_add');

            if ($this->bb->request_method == 'post') {
                $this->plugins->runHooks('admin_config_calendars_add_commit');

                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!isset($this->bb->input['disporder'])) {
                    $errors[] = $this->lang->error_missing_order;
                }

                if (!isset($errors)) {
                    $calendar = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'startofweek' => $this->bb->getInput('startofweek', 0),
                        'eventlimit' => $this->bb->getInput('eventlimit', 0),
                        'showbirthdays' => $this->bb->getInput('showbirthdays', 0),
                        'moderation' => $this->bb->getInput('moderation', 0),
                        'allowhtml' => $this->bb->getInput('allowhtml', 0),
                        'allowmycode' => $this->bb->getInput('allowmycode', 0),
                        'allowimgcode' => $this->bb->getInput('allowimgcode', 0),
                        'allowvideocode' => $this->bb->getInput('allowvideocode', 0),
                        'allowsmilies' => $this->bb->getInput('allowsmilies', 0)
                    ];

                    $this->plugins->runHooks('admin_config_calendars_add_commit_start');

                    $cid = $this->db->insert_query('calendars', $calendar);

                    $this->plugins->runHooks('admin_config_calendars_add_commit_end');

                    // Log admin action
                    $this->bblogger->log_admin_action($cid, $this->bb->input['name']);

                    $this->session->flash_message($this->lang->success_calendar_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/calendars');
                }
            } else {
                $this->bb->input = array_merge($this->bb->input, [
                        'allowhtml' => 0,
                        'eventlimit' => 4,
                        'disporder' => 1,
                        'moderation' => 0
                    ]);
            }

            $this->page->add_breadcrumb_item($this->lang->add_calendar);
            $html = $this->page->output_header($this->lang->calendars . ' - ' . $this->lang->add_calendar);

            $sub_tabs['add_calendar'] = [
                'title' => $this->lang->add_calendar,
                'link' => $this->bb->admin_url . '/config/calendars?action=add',
                'description' => $this->lang->add_calendar_desc
            ];
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_calendar');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/calendars?action=add', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_calendar);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->display_order, $this->lang->display_order_desc, $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $select_list = [$this->lang->sunday, $this->lang->monday, $this->lang->tuesday, $this->lang->wednesday, $this->lang->thursday, $this->lang->friday, $this->lang->saturday];
            $form_container->output_row($this->lang->week_start, $this->lang->week_start_desc, $form->generate_select_box('startofweek', $select_list, $this->bb->getInput('startofweek', 0), ['id' => 'startofweek']), 'startofweek');
            $form_container->output_row($this->lang->event_limit, $this->lang->event_limit_desc, $form->generate_numeric_field('eventlimit', $this->bb->input['eventlimit'], ['id' => 'eventlimit', 'min' => 0]), 'eventlimit');
            $form_container->output_row($this->lang->show_birthdays, $this->lang->show_birthdays_desc, $form->generate_yes_no_radio('showbirthdays', $this->bb->getInput('showbirthdays', 0), true));
            $form_container->output_row($this->lang->moderate_events, $this->lang->moderate_events_desc, $form->generate_yes_no_radio('moderation', $this->bb->input['moderation'], true));
            $form_container->output_row($this->lang->allow_html, '', $form->generate_yes_no_radio('allowhtml', $this->bb->input['allowhtml']));
            $form_container->output_row($this->lang->allow_mycode, '', $form->generate_yes_no_radio('allowmycode', $this->bb->getInput('allowmycode', 0)));
            $form_container->output_row($this->lang->allow_img, '', $form->generate_yes_no_radio('allowimgcode', $this->bb->getInput('allowimgcode', 0)));
            $form_container->output_row($this->lang->allow_video, '', $form->generate_yes_no_radio('allowvideocode', $this->bb->getInput('allowvideocode', 0)));
            $form_container->output_row($this->lang->allow_smilies, '', $form->generate_yes_no_radio('allowsmilies', $this->bb->getInput('allowsmilies', 0)));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_calendar);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/CalendarsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'permissions') {
            $usergroups = [];

            $query = $this->db->simple_select('calendars', '*', "cid='" . $this->bb->getInput('cid', 0) . "'");
            $calendar = $this->db->fetch_array($query);

            // Does the calendar not exist?
            if (!$calendar['cid']) {
                $this->session->flash_message($this->lang->error_invalid_calendar, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            }

            $this->plugins->runHooks('admin_config_calendars_permissions');

            $query = $this->db->simple_select('usergroups', '*', '', ['order' => 'name']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = $usergroup;
            }

            $query = $this->db->simple_select('calendarpermissions', '*', "cid='{$calendar['cid']}'");
            while ($existing = $this->db->fetch_array($query)) {
                $existing_permissions[$existing['gid']] = $existing;
            }

            if ($this->bb->request_method == 'post') {
                foreach (array_keys($usergroups) as $group_id) {
                    $permissions = isset($this->bb->input['permissions'][$group_id]) ? $this->bb->input['permissions'][$group_id] : [];
                    $this->db->delete_query('calendarpermissions', "cid='{$calendar['cid']}' AND gid='" . (int)$group_id . "'");
                    if (!$this->bb->input['default_permissions'][$group_id]) {
                        foreach (['canviewcalendar', 'canaddevents', 'canbypasseventmod', 'canmoderateevents'] as $calendar_permission) {
                            if ($permissions[$calendar_permission] == 1) {
                                $permissions_array[$calendar_permission] = 1;
                            } else {
                                $permissions_array[$calendar_permission] = 0;
                            }
                        }
                        $permissions_array['gid'] = (int)$group_id;
                        $permissions_array['cid'] = $calendar['cid'];
                        $this->db->insert_query('calendarpermissions', $permissions_array);
                    }
                }
                $this->plugins->runHooks('admin_config_calendars_permissions_commit');
                // Log admin action
                $this->bblogger->log_admin_action($calendar['cid'], $calendar['name']);
                $this->session->flash_message($this->lang->success_calendar_permissions_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            }

            $calendar['name'] = htmlspecialchars_uni($calendar['name']);
            $this->page->add_breadcrumb_item($calendar['name'], $this->bb->admin_url . '/config/calendars?action=edit&cid=' . $calendar['cid']);
            $this->page->add_breadcrumb_item($this->lang->permissions);
            $html = $this->page->output_header($this->lang->calendars . ' - ' . $this->lang->edit_permissions);

            $form = new Form($this->bb, $this->bb->admin_url . '/config/calendars?action=permissions', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('cid', $calendar['cid']);

            $table = new Table;
            $table->construct_header($this->lang->permissions_group);
            $table->construct_header($this->lang->permissions_view, ['class' => 'align_center', 'width' => '10%']);
            $table->construct_header($this->lang->permissions_post_events, ['class' => 'align_center', 'width' => '10%']);
            $table->construct_header($this->lang->permissions_bypass_moderation, ['class' => 'align_center', 'width' => '10%']);
            $table->construct_header($this->lang->permissions_moderator, ['class' => 'align_center', 'width' => '10%']);
            $table->construct_header($this->lang->permissions_all, ['class' => 'align_center', 'width' => '10%']);

            foreach ($usergroups as $usergroup) {
                if (isset($existing_permissions[$usergroup['gid']])) {
                    $perms = $existing_permissions[$usergroup['gid']];
                    $default_checked = false;
                } else {
                    $perms = $usergroup;
                    $default_checked = true;
                }
                $perm_check = $all_check = '';
                $all_checked = true;
                foreach (['canviewcalendar', 'canaddevents', 'canbypasseventmod', 'canmoderateevents'] as $calendar_permission) {
                    if ($usergroup[$calendar_permission] == 1) {
                        $value = 'this.checked';
                    } else {
                        $value = 'false';
                    }
                    if ($perms[$calendar_permission] != 1) {
                        $all_checked = false;
                    }
                    if ($perms[$calendar_permission] == 1) {
                        $perms_checked[$calendar_permission] = 1;
                    } else {
                        $perms_checked[$calendar_permission] = 0;
                    }
                    $all_check .= "\$('#permissions_{$usergroup['gid']}_{$calendar_permission}').prop('checked', this.checked);\n";
                    $perm_check .= "\$('#permissions_{$usergroup['gid']}_{$calendar_permission}').prop('checked', $value);\n";
                }
                $default_click = "if(\$(this).is(':checked')) { $perm_check }";
                $reset_default = "if(!\$(this).is(':checked')) { \$('#permissions_{$usergroup['gid']}_all').prop('checked', false); }\n";
                $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);
                $table->construct_cell("<strong>{$usergroup['title']}</strong><br /><small style=\"vertical-align: middle;\">" . $form->generate_check_box("default_permissions[{$usergroup['gid']}];", 1, "", ["id" => "default_permissions_{$usergroup['gid']}", "checked" => $default_checked, "onclick" => $default_click]) . " <label for=\"default_permissions_{$usergroup['gid']}\">{$this->lang->permissions_use_group_default}</label></small>");
                $table->construct_cell($form->generate_check_box("permissions[{$usergroup['gid']}][canviewcalendar]", 1, "", ["id" => "permissions_{$usergroup['gid']}_canviewcalendar", "checked" => $perms_checked['canviewcalendar'], "onclick" => $reset_default]), ['class' => 'align_center']);
                $table->construct_cell($form->generate_check_box("permissions[{$usergroup['gid']}][canaddevents]", 1, "", ["id" => "permissions_{$usergroup['gid']}_canaddevents", "checked" => $perms_checked['canaddevents'], "onclick" => $reset_default]), ['class' => 'align_center']);
                $table->construct_cell($form->generate_check_box("permissions[{$usergroup['gid']}][canbypasseventmod]", 1, "", ["id" => "permissions_{$usergroup['gid']}_canbypasseventmod", "checked" => $perms_checked['canbypasseventmod'], "onclick" => $reset_default]), ['class' => 'align_center']);
                $table->construct_cell($form->generate_check_box("permissions[{$usergroup['gid']}][canmoderateevents]", 1, "", ["id" => "permissions_{$usergroup['gid']}_canmoderateevents", "checked" => $perms_checked['canmoderateevents'], "onclick" => $reset_default]), ['class' => 'align_center']);
                $table->construct_cell($form->generate_check_box("permissions[{$usergroup['gid']}][all]", 1, "", ["id" => "permissions_{$usergroup['gid']}_all", "checked" => $all_checked, "onclick" => $all_check]), ['class' => 'align_center']);
                $table->construct_row();
            }
            $html .= $table->output("{$this->lang->calendar_permissions_for} {$calendar['name']}");

            if (!isset($no_results)) {
                $buttons[] = $form->generate_submit_button($this->lang->save_permissions);
                $html .= $form->output_submit_wrapper($buttons);
            }
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/CalendarsPermissions.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('calendars', '*', "cid='" . $this->bb->getInput('cid', 0) . "'");
            $calendar = $this->db->fetch_array($query);

            // Does the calendar not exist?
            if (!$calendar['cid']) {
                $this->session->flash_message($this->lang->error_invalid_calendar, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            }

            $this->plugins->runHooks('admin_config_calendars_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!isset($this->bb->input['disporder'])) {
                    $errors[] = $this->lang->error_missing_order;
                }

                if (!isset($errors)) {
                    $updated_calendar = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'startofweek' => $this->bb->getInput('startofweek', 0),
                        'eventlimit' => $this->bb->getInput('eventlimit', 0),
                        'showbirthdays' => $this->bb->getInput('showbirthdays', 0),
                        'moderation' => $this->bb->getInput('moderation', 0),
                        'allowhtml' => $this->bb->getInput('allowhtml', 0),
                        'allowmycode' => $this->bb->getInput('allowmycode', 0),
                        'allowimgcode' => $this->bb->getInput('allowimgcode', 0),
                        'allowvideocode' => $this->bb->getInput('allowvideocode', 0),
                        'allowsmilies' => $this->bb->getInput('allowsmilies', 0)
                    ];

                    $this->plugins->runHooks('admin_config_calendars_edit_commit');

                    $this->db->update_query('calendars', $updated_calendar, "cid='{$calendar['cid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($calendar['cid'], $this->bb->input['name']);

                    $this->session->flash_message($this->lang->success_calendar_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/calendars');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_calendar);
            $html = $this->page->output_header($this->lang->calendars . ' - ' . $this->lang->edit_calendar);

            $sub_tabs['edit_calendar'] = [
                'title' => $this->lang->edit_calendar,
                'link' => $this->bb->admin_url . '/config/calendars?action=edit',
                'description' => $this->lang->edit_calendar_desc
            ];
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_calendar');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/calendars?action=edit', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('cid', $calendar['cid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = $calendar;
            }

            $form_container = new FormContainer($this, $this->lang->edit_calendar);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->display_order . ' <em>*</em>', $this->lang->display_order_desc, $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $select_list = [$this->lang->sunday, $this->lang->monday, $this->lang->tuesday, $this->lang->wednesday, $this->lang->thursday, $this->lang->friday, $this->lang->saturday];
            $form_container->output_row($this->lang->week_start, $this->lang->week_start_desc, $form->generate_select_box('startofweek', $select_list, $this->bb->input['startofweek'], ['id' => 'startofweek']), 'startofweek');
            $form_container->output_row($this->lang->event_limit, $this->lang->event_limit_desc, $form->generate_numeric_field('eventlimit', $this->bb->input['eventlimit'], ['id' => 'eventlimit', 'min' => 0]), 'eventlimit');
            $form_container->output_row($this->lang->show_birthdays, $this->lang->show_birthdays_desc, $form->generate_yes_no_radio('showbirthdays', $this->bb->input['showbirthdays'], true));
            $form_container->output_row($this->lang->moderate_events, $this->lang->moderate_events_desc, $form->generate_yes_no_radio('moderation', $this->bb->input['moderation'], true));
            $form_container->output_row($this->lang->allow_html, '', $form->generate_yes_no_radio('allowhtml', $this->bb->input['allowhtml']));
            $form_container->output_row($this->lang->allow_mycode, '', $form->generate_yes_no_radio('allowmycode', $this->bb->input['allowmycode']));
            $form_container->output_row($this->lang->allow_img, '', $form->generate_yes_no_radio('allowimgcode', $this->bb->input['allowimgcode']));
            $form_container->output_row($this->lang->allow_video, '', $form->generate_yes_no_radio('allowvideocode', $this->bb->input['allowvideocode']));
            $form_container->output_row($this->lang->allow_smilies, '', $form->generate_yes_no_radio('allowsmilies', $this->bb->input['allowsmilies']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_calendar);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/CalendarsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('calendars', '*', "cid='" . $this->bb->getInput('cid', 0) . "'");
            $calendar = $this->db->fetch_array($query);

            // Does the calendar not exist?
            if (!$calendar['cid']) {
                $this->session->flash_message($this->lang->error_invalid_calendar, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            }

            $this->plugins->runHooks('admin_config_calendars_delete');

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            }

            if ($this->bb->request_method == 'post') {
                // Delete the calendar
                $this->db->delete_query('calendars', "cid='{$calendar['cid']}'");
                $this->db->delete_query('calendarpermissions', "cid='{$calendar['cid']}'");
                $this->db->delete_query('events', "cid='{$calendar['cid']}'");

                $this->plugins->runHooks('admin_config_calendars_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($calendar['cid'], $calendar['name']);

                $this->session->flash_message($this->lang->success_calendar_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/calendars?action=delete&cid=' . $calendar['cid'], $this->lang->confirm_calendar_deletion);
            }
        }

        if ($this->bb->input['action'] == 'update_order' && $this->bb->request_method == 'post') {
            if (!is_array($this->bb->input['disporder'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/calendars');
            }

            $this->plugins->runHooks('admin_config_calendars_update_order');

            foreach ($this->bb->input['disporder'] as $cid => $order) {
                $update_query = [
                    'disporder' => (int)$order
                ];
                $this->db->update_query('calendars', $update_query, "cid='" . (int)$cid . "'");
            }

            $this->plugins->runHooks('admin_config_calendars_update_order_commit');

            // Log admin action
            $this->bblogger->log_admin_action();

            $this->session->flash_message($this->lang->success_calendar_orders_updated, 'success');
            return $response->withRedirect($this->bb->admin_url . '/config/calendars');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->manage_calendars);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_calendars');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/calendars?action=update_order', 'post');
            $html .= $form->getForm();
            $table = new Table;
            $table->construct_header($this->lang->calendar);
            $table->construct_header($this->lang->order, ['width' => '5%', 'class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 3, 'width' => 300]);

            $query = $this->db->simple_select('calendars', '*', '', ['order_by' => 'disporder']);
            while ($calendar = $this->db->fetch_array($query)) {
                $calendar['name'] = htmlspecialchars_uni($calendar['name']);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/calendars?action=edit&amp;cid={$calendar['cid']}\"><strong>{$calendar['name']}</strong></a>");
                $table->construct_cell($form->generate_numeric_field("disporder[{$calendar['cid']}]", $calendar['disporder'], ['id' => 'disporder', 'style' => 'width: 80%', 'class' => 'align_center', 'min' => 0]));
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/calendars?action=edit&amp;cid={$calendar['cid']}\">{$this->lang->edit}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/calendars?action=permissions&amp;cid={$calendar['cid']}\">{$this->lang->permissions}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/calendars?action=delete&amp;cid={$calendar['cid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_calendar_deletion}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_calendars, ['colspan' => 5]);
                $table->construct_row();
                $no_results = true;
            }

            $html .= $table->output($this->lang->manage_calendars);
            if (!isset($no_results)) {
                $buttons[] = $form->generate_submit_button($this->lang->save_calendar_orders);
                $html .= $form->output_submit_wrapper($buttons);
            }
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Calendars.html.twig');
        }
    }
}
