<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class HelpDocuments extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_help_documents', false, true);

        $this->page->add_breadcrumb_item($this->lang->help_documents, $this->bb->admin_url . '/config/help_documents');

        $this->plugins->runHooks('admin_config_help_documents_begin');

        // Add something
        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_help_documents_add');

            // Add section
            if ($this->bb->input['type'] == 'section') {
                $this->plugins->runHooks('admin_config_help_documents_add_section');

                // Do add?
                if ($this->bb->request_method == 'post') {
                    if (empty($this->bb->input['name'])) {
                        $errors[] = $this->lang->error_section_missing_name;
                    }

                    if (empty($this->bb->input['description'])) {
                        $errors[] = $this->lang->error_section_missing_description;
                    }

                    if (!isset($this->bb->input['enabled'])) {
                        $errors[] = $this->lang->error_section_missing_enabled;
                    }

                    if ($this->bb->input['enabled'] != 1) {
                        $this->bb->input['enabled'] = 0;
                    }

                    if (!isset($errors)) {
                        $sql_array = [
                            'name' => $this->db->escape_string($this->bb->input['name']),
                            'description' => $this->db->escape_string($this->bb->input['description']),
                            'usetranslation' => $this->bb->getInput('usetranslation', 0),
                            'enabled' => $this->bb->getInput('enabled', 0),
                            'disporder' => $this->bb->getInput('disporder', 0)
                        ];

                        $sid = $this->db->insert_query('helpsections', $sql_array);

                        $this->plugins->runHooks('admin_config_help_documents_add_section_commit');

                        // Log admin action
                        $this->bblogger->log_admin_action($sid, $this->bb->input['name'], 'section');

                        $this->session->flash_message($this->lang->success_help_section_added, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                    }
                }

                $this->page->add_breadcrumb_item($this->lang->add_new_section);
                $html = $this->page->output_header($this->lang->help_documents . ' - ' . $this->lang->add_new_section);

                $sub_tabs['manage_help_documents'] = [
                    'title' => $this->lang->manage_help_documents,
                    'link' => $this->bb->admin_url . '/config/help_documents'
                ];

                $sub_tabs['add_help_document'] = [
                    'title' => $this->lang->add_new_document,
                    'link' => $this->bb->admin_url . '/config/help_documents?action=add&type=document'
                ];

                $sub_tabs['add_help_section'] = [
                    'title' => $this->lang->add_new_section,
                    'link' => $this->bb->admin_url . '/config/help_documents?action=add&type=section',
                    'description' => $this->lang->add_new_section_desc
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'add_help_section');

                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                } else {
                    $query = $this->db->simple_select('helpsections', 'MAX(disporder) as maxdisp');
                    $this->bb->input['disporder'] = $this->db->fetch_field($query, 'maxdisp') + 1;
                    $this->bb->input['enabled'] = 1;
                    $this->bb->input['usetranslation'] = 1;
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/config/help_documents?action=add&type=section', 'post', 'add');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field('usetranslation', $this->bb->input['usetranslation']);

                $form_container = new FormContainer($this, $this->lang->add_new_section);
                $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
                $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->getInput('description', ''), ['id' => 'description']), 'description');
                $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
                $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->input['enabled']));
                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->save_section);

                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
            } // Add page
            else {
                $this->plugins->runHooks('admin_config_help_documents_add_page');

                // Do add?
                if ($this->bb->request_method == 'post') {
                    if (empty($this->bb->input['sid'])) {
                        $errors[] = $this->lang->error_missing_sid;
                    }

                    if (empty($this->bb->input['name'])) {
                        $errors[] = $this->lang->error_document_missing_name;
                    }

                    if (empty($this->bb->input['description'])) {
                        $errors[] = $this->lang->error_document_missing_description;
                    }

                    if (empty($this->bb->input['document'])) {
                        $errors[] = $this->lang->error_document_missing_document;
                    }

                    if (!isset($this->bb->input['enabled'])) {
                        $errors[] = $this->lang->error_document_missing_enabled;
                    }

                    if ($this->bb->input['enabled'] != 1) {
                        $this->bb->input['enabled'] = 0;
                    }

                    if (!isset($errors)) {
                        $sql_array = [
                            'sid' => $this->bb->getInput('sid', 0),
                            'name' => $this->db->escape_string($this->bb->input['name']),
                            'description' => $this->db->escape_string($this->bb->input['description']),
                            'document' => $this->db->escape_string($this->bb->input['document']),
                            'usetranslation' => $this->bb->getInput('usetranslation', 0),
                            'enabled' => $this->bb->getInput('enabled', 0),
                            'disporder' => $this->bb->getInput('disporder', 0)
                        ];

                        $hid = $this->db->insert_query('helpdocs', $sql_array);

                        $this->plugins->runHooks('admin_config_help_documents_add_page_commit');

                        // Log admin action
                        $this->bblogger->log_admin_action($hid, $this->bb->input['name'], 'document');

                        $this->session->flash_message($this->lang->success_help_document_added, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                    }
                }

                $this->page->add_breadcrumb_item($this->lang->add_new_document);
                $html = $this->page->output_header($this->lang->help_documents . ' - ' . $this->lang->add_new_document);

                $sub_tabs['manage_help_documents'] = [
                    'title' => $this->lang->manage_help_documents,
                    'link' => $this->bb->admin_url . '/config/help_documents'
                ];

                $sub_tabs['add_help_document'] = [
                    'title' => $this->lang->add_new_document,
                    'link' => $this->bb->admin_url . '/config/help_documents?action=add&type=document',
                    'description' => $this->lang->add_new_document_desc
                ];

                $sub_tabs['add_help_section'] = [
                    'title' => $this->lang->add_new_section,
                    'link' => $this->bb->admin_url . '/config/help_documents?action=add&type=section'
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'add_help_document');

                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                } else {
                    // Select the largest existing display order
                    $query = $this->db->simple_select('helpdocs', 'MAX(disporder) as maxdisp');
                    $this->bb->input['disporder'] = $this->db->fetch_field($query, 'maxdisp') + 1;
                    $this->bb->input['enabled'] = 1;
                    $this->bb->input['usetranslation'] = 1;
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/config/help_documents?action=add&type=document', 'post', 'add');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field('usetranslation', $this->bb->input['usetranslation']);

                $form_container = new FormContainer($this, $this->lang->add_new_document);
                $query = $this->db->simple_select('helpsections', 'sid, name');

                $sections = [];
                while ($section = $this->db->fetch_array($query)) {
                    $sections[$section['sid']] = $section['name'];
                }
                $form_container->output_row($this->lang->section . ' <em>*</em>', '', $form->generate_select_box('sid', $sections, $this->bb->getInput('sid', ''), ['id' => 'sid']), 'sid');
                $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
                $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->getInput('description', ''), ['id' => 'description']), 'description');
                $form_container->output_row($this->lang->document . ' <em>*</em>', '', $form->generate_text_area('document', $this->bb->getInput('document', ''), ['id' => 'document']), 'document');
                $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
                $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->input['enabled']));
                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->save_document);

                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/HelpDocumentsAdd.html.twig');
        }

        // Edit something
        if ($this->bb->input['action'] == 'edit') {
            $this->plugins->runHooks('admin_config_help_documents_edit');

            // Edit a section
            if (isset($this->bb->input['sid']) && !isset($this->bb->input['hid'])) {
                $query = $this->db->simple_select('helpsections', '*', "sid = '" . $this->bb->getInput('sid', 0) . "'");
                $section = $this->db->fetch_array($query);

                $this->plugins->runHooks('admin_config_help_documents_edit_section');

                // Do edit?
                if ($this->bb->request_method == 'post') {
                    $sid = $this->bb->getInput('sid', 0);

                    if (empty($sid)) {
                        $errors[] = $this->lang->error_invalid_sid;
                    }

                    if (empty($this->bb->input['name'])) {
                        $errors[] = $this->lang->error_section_missing_name;
                    }

                    if (empty($this->bb->input['description'])) {
                        $errors[] = $this->lang->error_section_missing_description;
                    }

                    if (!isset($this->bb->input['enabled'])) {
                        $errors[] = $this->lang->error_section_missing_enabled;
                    }

                    if ($this->bb->input['enabled'] != 1) {
                        $this->bb->input['enabled'] = 0;
                    }

                    if (!isset($errors)) {
                        $sql_array = [
                            'name' => $this->db->escape_string($this->bb->input['name']),
                            'description' => $this->db->escape_string($this->bb->input['description']),
                            'usetranslation' => $this->bb->getInput('usetranslation', 0),
                            'enabled' => $this->bb->getInput('enabled', 0),
                            'disporder' => $this->bb->getInput('disporder', 0)
                        ];

                        $this->plugins->runHooks('admin_config_help_documents_edit_section_commit');

                        $this->db->update_query('helpsections', $sql_array, "sid = '{$sid}'");

                        // Log admin action
                        $this->bblogger->log_admin_action($sid, $this->bb->input['name'], 'section');

                        $this->session->flash_message($this->lang->success_help_section_updated, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                    }
                }

                $this->page->add_breadcrumb_item($this->lang->edit_section);
                $html = $this->page->output_header($this->lang->help_documents . ' - ' . $this->lang->edit_section);


                $sub_tabs['edit_help_section'] = [
                    'title' => $this->lang->edit_section,
                    'link' => $this->bb->admin_url . '/config/help_documents?action=edit&sid=' . $section['sid'],
                    'description' => $this->lang->edit_section_desc
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_help_section');

                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                } else {
                    $this->bb->input['sid'] = $section['sid'];
                    $this->bb->input['name'] = $section['name'];
                    $this->bb->input['description'] = $section['description'];
                    $this->bb->input['disporder'] = $section['disporder'];
                    $this->bb->input['enabled'] = $section['enabled'];
                    $this->bb->input['usetranslation'] = $section['usetranslation'];
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/config/help_documents?action=edit', 'post', 'edit');
                $html .= $form->getForm();

                $html .= $form->generate_hidden_field('sid', $section['sid']);
                $html .= $form->generate_hidden_field('usetranslation', $this->bb->input['usetranslation']);

                $form_container = new FormContainer($this, $this->lang->edit_section . " ({$this->lang->id} " . $section['sid'] . ")");
                $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
                $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');
                $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
                $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->input['enabled']));
                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->edit_section);

                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
            } // Edit document
            else {
                $this->plugins->runHooks('admin_config_help_documents_edit_page');
                $hid = $this->bb->getInput('hid', 0);
                // Do edit?
                if ($this->bb->request_method == 'post') {
                    if (empty($hid)) {
                        $errors[] = $this->lang->error_invalid_sid;
                    }

                    if (empty($this->bb->input['name'])) {
                        $errors[] = $this->lang->error_document_missing_name;
                    }

                    if (empty($this->bb->input['description'])) {
                        $errors[] = $this->lang->error_document_missing_description;
                    }

                    if (empty($this->bb->input['document'])) {
                        $errors[] = $this->lang->error_document_missing_document;
                    }

                    if (!isset($this->bb->input['enabled'])) {
                        $errors[] = $this->lang->error_document_missing_enabled;
                    }

                    if ($this->bb->input['enabled'] != 1) {
                        $this->bb->input['enabled'] = 0;
                    }

                    if (!isset($errors)) {
                        $sql_array = [
                            'sid' => $this->bb->getInput('sid', 0),
                            'name' => $this->db->escape_string($this->bb->input['name']),
                            'description' => $this->db->escape_string($this->bb->input['description']),
                            'document' => $this->db->escape_string($this->bb->input['document']),
                            'usetranslation' => $this->bb->getInput('usetranslation', 0),
                            'enabled' => $this->bb->getInput('enabled', 0),
                            'disporder' => $this->bb->getInput('disporder', 0)
                        ];

                        $this->plugins->runHooks('admin_config_help_documents_edit_page_commit');

                        $this->db->update_query('helpdocs', $sql_array, "hid = '{$hid}'");

                        // Log admin action
                        $this->bblogger->log_admin_action($hid, $this->bb->input['name'], 'document');

                        $this->session->flash_message($this->lang->success_help_document_updated, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                    }
                }

                $this->page->add_breadcrumb_item($this->lang->edit_document);
                $html = $this->page->output_header($this->lang->help_documents . ' - ' . $this->lang->edit_document);


                $sub_tabs['edit_help_document'] = [
                    'title' => $this->lang->edit_document,
                    'link' => $this->bb->admin_url . '/config/help_documents?action=edit&hid=' . $hid,
                    'description' => $this->lang->edit_document_desc
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_help_document');

                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                } else {
                    $query = $this->db->simple_select('helpdocs', '*', "hid = '" . $this->bb->getInput('hid', 0) . "'");
                    $doc = $this->db->fetch_array($query);
                    $this->bb->input['hid'] = $doc['hid'];
                    $this->bb->input['sid'] = $doc['sid'];
                    $this->bb->input['name'] = $doc['name'];
                    $this->bb->input['description'] = $doc['description'];
                    $this->bb->input['document'] = $doc['document'];
                    $this->bb->input['disporder'] = $doc['disporder'];
                    $this->bb->input['enabled'] = $doc['enabled'];
                    $this->bb->input['usetranslation'] = $doc['usetranslation'];
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/config/help_documents?action=edit', 'post', 'edit');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field('hid', $doc['hid']);
                $html .= $form->generate_hidden_field('usetranslation', $this->bb->input['usetranslation']);

                $form_container = new FormContainer($this, $this->lang->edit_document . " ({$this->lang->id} " . $doc['hid'] . ")");

                $sections = [];
                $query = $this->db->simple_select('helpsections', 'sid, name');
                while ($section = $this->db->fetch_array($query)) {
                    $sections[$section['sid']] = $section['name'];
                }
                $form_container->output_row($this->lang->section . ' <em>*</em>', '', $form->generate_select_box('sid', $sections, $this->bb->input['sid']), 'sid');
                $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
                $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');
                $form_container->output_row($this->lang->document . ' <em>*</em>', '', $form->generate_text_area('document', $this->bb->input['document'], ['id' => 'document']), 'document');
                $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
                $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->input['enabled']));
                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->edit_document);

                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/HelpDocumentsEdit.html.twig');
        }

        // Delete something
        if ($this->bb->input['action'] == 'delete') {
            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
            }

            $this->plugins->runHooks('admin_config_help_documents_delete');

            // Do delete something?
            if ($this->bb->request_method == 'post') {
                // Delete section
                if (isset($this->bb->input['sid'])) {
                    $sid = $this->bb->getInput('sid', 0);

                    $query = $this->db->simple_select('helpsections', '*', "sid='{$sid}'");
                    $section = $this->db->fetch_array($query);

                    // Invalid section?
                    if (!$section['sid']) {
                        $this->session->flash_message($this->lang->error_missing_section_id, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                    }

                    // Delete section and its documents
                    $this->db->delete_query('helpsections', "sid = '{$section['sid']}'", 1);
                    $this->db->delete_query('helpdocs', "sid = '{$section['sid']}'");

                    $this->plugins->runHooks('admin_config_help_documents_delete_section_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($section['sid'], $section['name'], 'section');

                    $this->session->flash_message($this->lang->success_section_deleted, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                } // Delete document
                else {
                    $hid = $this->bb->getInput('hid', 0);

                    $query = $this->db->simple_select('helpdocs', '*', "hid='{$hid}'");
                    $doc = $this->db->fetch_array($query);

                    // Invalid document?
                    if (!$doc['hid']) {
                        $this->session->flash_message($this->lang->error_missing_hid, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                    }

                    $this->db->delete_query('helpdocs', "hid = '{$doc['hid']}'", 1);

                    $this->plugins->runHooks('admin_config_help_documents_delete_page_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($doc['hid'], $doc['name'], 'document');

                    $this->session->flash_message($this->lang->success_document_deleted, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/help_documents');
                }
            } // Show form for deletion
            else {
                // Section
                if (isset($this->bb->input['sid'])) {
                    $sid = $this->bb->getInput('sid', 0);
                    $this->page->output_confirm_action($this->bb->admin_url . '/config/help_documents?action=delete&sid=' . $sid, $this->lang->confirm_section_deletion);
                } // Document
                else {
                    $hid = $this->bb->getInput('hid', 0);
                    $this->page->output_confirm_action($this->bb->admin_url . '/config/help_documents?action=delete&hid=' . $hid, $this->lang->confirm_document_deletion);
                }
            }
        }

        // List document and sections
        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->help_documents);

            $sub_tabs['manage_help_documents'] = [
                'title' => $this->lang->manage_help_documents,
                'link' => $this->bb->admin_url . '/config/help_documents',
                'description' => $this->lang->manage_help_documents_desc
            ];

            $sub_tabs['add_help_document'] = [
                'title' => $this->lang->add_new_document,
                'link' => $this->bb->admin_url . '/config/help_documents?action=add&type=document'
            ];

            $sub_tabs['add_help_section'] = [
                'title' => $this->lang->add_new_section,
                'link' => $this->bb->admin_url . '/config/help_documents?action=add&type=section'
            ];

            $this->plugins->runHooks('admin_config_help_documents_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_help_documents');

            $table = new Table;
            $table->construct_header($this->lang->section_document);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => '150']);

            $query = $this->db->simple_select('helpsections', '*', '', ['order_by' => 'disporder']);
            while ($section = $this->db->fetch_array($query)) {
                $table->construct_cell('<div><strong><a href="'.$this->bb->admin_url."/config/help_documents?action=edit&amp;sid={$section['sid']}\">{$section['name']}</a></strong><br /><small>{$section['description']}</small></div>");
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/help_documents?action=edit&amp;sid={$section['sid']}\">{$this->lang->edit}</a>", ["class" => "align_center", "width" => '60']);
                $table->construct_cell('<a href="'.$this->bb->admin_url."/config/help_documents?action=delete&amp;sid={$section['sid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_section_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center", "width" => '90']);
                $table->construct_row();

                $query2 = $this->db->simple_select('helpdocs', '*', "sid='{$section['sid']}'", ['order_by' => 'disporder']);
                while ($doc = $this->db->fetch_array($query2)) {
                    $table->construct_cell("<div style=\"padding-left: 40px;\"><div><strong><a href=\"".$this->bb->admin_url."/config/help_documents?action=edit&amp;hid={$doc['hid']}\">{$doc['name']}</a></strong><br /><small>{$doc['description']}</small></div></div>");
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/help_documents?action=edit&amp;hid={$doc['hid']}\">{$this->lang->edit}</a>", ["class" => "align_center", "width" => '60']);
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/help_documents?action=delete&amp;hid={$doc['hid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_document_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center", "width" => '90']);
                    $table->construct_row();
                }
            }

            // No documents message
            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_help_documents, ['colspan' => 3]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->help_documents);
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/HelpDocuments.html.twig');
        }
    }
}
