<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Languages extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_languages', false, true);

        $languages = $this->lang->get_languages();

        $this->page->add_breadcrumb_item($this->lang->languages, $this->bb->admin_url . '/config/languages');

        $this->plugins->runHooks('admin_config_languages_begin');

        if ($this->bb->input['action'] == 'edit_properties') {
            $editlang = $this->bb->getInput('lang', '');

            if (!$editlang) {
                $this->session->flash_message($this->lang->error_invalid_file, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/languages');
            }

            $this->plugins->runHooks('admin_config_languages_edit_properties');

            if ($this->bb->request_method == 'post') {
//                if (!is_writable($file)) {
//                    $this->session->flash_message($this->lang->error_cannot_write_to_file, 'error');
//                    return $response->withRedirect($this->bb->admin_url . '/config/languages');
//                }

/*
                $this->lang->saveSection($editlang, 'langinfo', $this->bb->input['info']);
                foreach ($this->bb->input['info'] as $key => $info) {
                    $info = str_replace("\\", "\\\\", $info);
                    $info = str_replace('$', '\$', $info);

                    if ($key == 'admin' || $key == 'rtl') {
                        $info = (int)$info;
                    }

                    $newlanginfo[$key] = str_replace("\"", '\"', $info);
                }

                // Get contents of existing file
//                require $file;

                // Make the contents of the new file
                $newfile = "<?php
// The friendly name of the language
\$langinfo['name'] = \"{$newlanginfo['name']}\";

// The author of the language
\$langinfo['author'] = \"{$langinfo['author']}\";

// The language authors website
\$langinfo['website'] = \"{$langinfo['website']}\";

// Compatible version of MyBB
\$langinfo['version'] = \"{$langinfo['version']}\";

// Sets if the translation includes the Admin CP (1 = yes, 0 = no)
\$langinfo['admin'] = {$newlanginfo['admin']};

// Sets if the language is RTL (Right to Left) (1 = yes, 0 = no)
\$langinfo['rtl'] = {$newlanginfo['rtl']};

// Sets the lang in the <html> on all pages
\$langinfo['htmllang'] = \"{$newlanginfo['htmllang']}\";

// Sets the character set, blank uses the default.
\$langinfo['charset'] = \"{$newlanginfo['charset']}\";\n";

*/
                // Put it in!
//                if ($file = fopen($file, 'w')) {
                if ($this->lang->saveSection($editlang, 'langinfo', $this->bb->input['info'])) {
//                    fwrite($file, $newfile);
//                    fclose($file);

                    $this->plugins->runHooks('admin_config_languages_edit_properties_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($editlang);
                    //clear cache files FIXME now all maybe only current???
                    $this->lang->clearCache();

                    $this->session->flash_message($this->lang->success_langprops_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang);// . '&editwith=' . $editwith);
                } else {
                    $errors[] = $this->lang->error_cannot_write_to_file;
                }
            }

//            $this->page->add_breadcrumb_item(preg_replace("<\?|\? >", '<span>?</span>', $languages[$editlang]), $this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang);
            $this->page->add_breadcrumb_item($languages[$editlang], $this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang);
            $this->page->add_breadcrumb_item($this->lang->nav_editing_set);

            $html = $this->page->output_header($this->lang->languages);

            $sub_tabs['edit_properties'] = [
                'title' => $this->lang->edit_properties,
                'link' => $this->bb->admin_url . '/config/languages',
                'description' => $this->lang->edit_properties_desc
            ];
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_properties');

            // Get language info
//            require $file;
            $langinfo = $this->lang->getLangInfo($editlang);
            $form = new Form($this->bb, $this->bb->admin_url . '/config/languages?action=edit_properties', 'post', 'editset');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('lang', $editlang);
            $html .= $form->generate_hidden_field('info[author]', $langinfo['author']);
            $html .= $form->generate_hidden_field('info[website]', $langinfo['website']);
            $html .= $form->generate_hidden_field('info[version]', $langinfo['version']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                if ($langinfo['admin']) {
                    $this->bb->input['info']['admin'] = 1;
                } else {
                    $this->bb->input['info']['admin'] = 0;
                }

                if ($langinfo['rtl']) {
                    $this->bb->input['info']['rtl'] = 1;
                } else {
                    $this->bb->input['info']['rtl'] = 0;
                }

                $this->bb->input['info']['name'] = $langinfo['name'];
                $this->bb->input['info']['htmllang'] = $langinfo['htmllang'];
                $this->bb->input['info']['charset'] = $langinfo['charset'];
            }

            $form_container = new FormContainer($this, $this->lang->edit_properties);

            $form_container->output_row($this->lang->friendly_name . ' <em>*</em>', '', $form->generate_text_box('info[name]', $this->bb->input['info']['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->language_in_html . ' <em>*</em>', '', $form->generate_text_box('info[htmllang]', $this->bb->input['info']['htmllang'], ['id' => 'htmllang']), 'htmllang');
            $form_container->output_row($this->lang->charset . ' <em>*</em>', '', $form->generate_text_box('info[charset]', $this->bb->input['info']['charset'], ['id' => 'charset']), 'charset');
            $form_container->output_row($this->lang->rtl . ' <em>*</em>', '', $form->generate_yes_no_radio('info[rtl]', $this->bb->input['info']['rtl'], ['id' => 'rtl']), 'rtl');
            $form_container->output_row($this->lang->admin . ' <em>*</em>', '', $form->generate_yes_no_radio('info[admin]', $this->bb->input['info']['admin'], ['id' => 'admin']), 'admin');

            // Check if file is writable, before allowing submission
            $no_write = 0;
//            if (!is_writable($file)) {
//                $no_write = 1;
//                $html .= $this->page->output_alert($this->lang->alert_note_cannot_write);
//            }

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_language_file, ['disabled' => $no_write]);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/LanguagesEditProps.html.twig');
        }

        if ($this->bb->input['action'] == 'quick_phrases') {
            // Validate input
            $editlang = basename($this->bb->input['lang']);
//            $folder = MYBB_ROOT . 'Languages/' . $editlang . '/';

//            $this->page->add_breadcrumb_item(preg_replace("<\?|\? >", '<span>?</span>', $languages[$editlang]), $this->bb->admin_url . '/config/languages?action=quick_edit&lang=' . $editlang);
            $this->page->add_breadcrumb_item($languages[$editlang], $this->bb->admin_url . '/config/languages?action=quick_edit&lang=' . $editlang);

            // Validate that this language pack really exists
//            if (file_exists(MYBB_ROOT . 'Languages/' . $editlang . '.php')) {
//                // Then validate language pack folders (and try to fix them if missing)
//                @mkdir($folder);
//                @mkdir($folder . 'admin');
//            }

//            if (!file_exists($folder) || !file_exists($folder . 'admin')) {
//                $this->session->flash_message($this->lang->error_folders_fail, 'error');
//                return $response->withRedirect($this->bb->admin_url . '/config/languages');
//            }

            $this->plugins->runHooks('admin_config_languages_quick_phrases');
            $quick_phrases = [
//                'member.lang.php' => [
                'member' => [
                    'agreement' => $this->lang->quickphrases_agreement,
                    'agreement_1' => $this->lang->quickphrases_agreement_1,
                    'agreement_2' => $this->lang->quickphrases_agreement_2,
                    'agreement_3' => $this->lang->quickphrases_agreement_3,
                    'agreement_4' => $this->lang->quickphrases_agreement_4,
                    'agreement_5' => $this->lang->quickphrases_agreement_5
                ],
//                'messages.lang.php' => [
                'messages' => [
                    'error_nopermission_guest_1' => $this->lang->quickphrases_error_nopermission_guest_1,
                    'error_nopermission_guest_2' => $this->lang->quickphrases_error_nopermission_guest_2,
                    'error_nopermission_guest_3' => $this->lang->quickphrases_error_nopermission_guest_3,
                    'error_nopermission_guest_4' => $this->lang->quickphrases_error_nopermission_guest_4
                ]
            ];

            if ($this->bb->request_method == 'post') {
                // We have more than one file to edit, lets set flag for all of them.
                $editsuccess = true;
                foreach ($quick_phrases as $file => $phrases) {
//                        @include $folder . $file;
//                        $contents_file = (array)$l;
//                        unset($l);
                    $contents_file = [];
                    foreach ($phrases as $key => $value) {
                        // validation - we fetch from input only variables that are defined in $quick_phrases array
                        $contents_file[$key] = $this->bb->input['edit'][$key];
                    }

                    $editsuccess = $this->lang->saveSection($editlang, $file, $contents_file);
/*
                    // Save edited language file
                    if ($fp = @fopen($folder . $file, 'w')) {
                        // We need info about edited language files to generate credits for our file
                        require MYBB_ROOT . 'Languages/' . $editlang . '.php';

                        // Lets make nice credits header in language file
                        $lang_file_credits = "<?php\n/**\n";
                        $lang_file_credits .= " * MyBB Copyright 2014 MyBB Group, All Rights Reserved\n *\n";
                        $lang_file_credits .= " * Website: http://www.mybb.com\n";
                        $lang_file_credits .= " * License: http://www.mybb.com/about/license\n *\n * /\n\n";
                        $lang_file_credits .= "// " . str_repeat('-', 80) . "\n";
                        $lang_file_credits .= "// MyBB Language Pack File.\n";
                        $lang_file_credits .= "// This file has been generated by MyBB - buildin language pack editor.\n";
                        $lang_file_credits .= "// " . str_repeat('=', 80) . "\n";
                        $lang_file_credits .= "// Friendly name of the language : " . preg_replace("#<\?|\?>#i", " ", $langinfo['name']) . "\n";
                        $lang_file_credits .= "// Author of the language pack : " . preg_replace("#<\?|\?>#i", " ", $langinfo['author']) . "\n";
                        $lang_file_credits .= "// Language pack translators website : " . preg_replace("#<\?|\?>#i", " ", $langinfo['website']) . "\n";
                        $lang_file_credits .= "// Compatible version of MyBB : " . preg_replace("#<\?|\?>#i", " ", $langinfo['version']) . "\n";
                        $lang_file_credits .= "// Last edited in MyBB Editor by : " . preg_replace("#<\?|\?>#i", " ", $this->user->username) . "\n";
                        $lang_file_credits .= "// Last edited date : " . gmdate("r") . "\n";
                        $lang_file_credits .= "// " . str_repeat('-', 80) . "\n\n";

                        $contents_wfile = $lang_file_credits;
                        foreach ($contents_file as $key => $value) {
                            $contents_wfile .= "\$l['" . $key . "'] = " . var_export($value, true) . ";\n";
                        }

                        flock($fp, LOCK_EX);
                        fwrite($fp, $contents_wfile);
                        flock($fp, LOCK_UN);
                        fclose($fp);
                    } else {
                        // One of files failed
                        $editsuccess = false;
                    }
*/
                }

                if ($editsuccess == true) {
                    // Log admin action
                    $this->bblogger->log_admin_action($editlang);
                    //clear cache files FIXME now all maybe only current???
                    $this->lang->clearCache();

                    $this->session->flash_message($this->lang->success_quickphrases_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang);
                }
            }

            $html = $this->page->output_header($this->lang->languages);

            $sub_tabs['language_files'] = [
                'title' => $this->lang->language_files,
                'link' => $this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang,
                'description' => $this->lang->language_files_desc
            ];

            $sub_tabs['quick_phrases'] = [
                'title' => $this->lang->quick_phrases,
                'link' => $this->bb->admin_url . '/config/languages?action=quick_phrases&lang=' . $editlang,
                'description' => $this->lang->quick_phrases_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'quick_phrases');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/languages?action=quick_phrases&lang=' . $editlang, 'post', 'quick_phrases');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $table = new Table;

            // Check if files are writable, before allowing submission
            $no_write = null;
//            foreach ($quick_phrases as $file => $phrases) {
//                if (file_exists($folder . $file) && !is_writable($folder . $file) || !is_writable($folder)) {
//                    $no_write = 1;
//                }
//            }
//
//            if ($no_write) {
//                $html .= $this->page->output_alert($this->lang->alert_note_cannot_write);
//            }

            $form_container = new FormContainer($this, $this->lang->quick_phrases);
            $langinfo = $this->lang->getLangInfo($editlang);

            foreach ($quick_phrases as $file => $phrases) {
//                unset($langinfo);
//                @include MYBB_ROOT . 'Languages/' . $editlang . '.php';
                $quickphrases_dir_class = ' langeditor_ltr';
                if ((int)$langinfo['rtl'] > 0) {
                    $quickphrases_dir_class = ' langeditor_rtl';
                }
                $l= $this->lang->getSection($editlang, $file);
//                @include $folder . $file;
                foreach ($phrases as $phrase => $description) {
                    $value = $l[$phrase];
                    if (my_strtolower($langinfo['charset']) == 'utf-8') {
                        $value = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return dec_to_utf8(hexdec($matches[1]));'), $value);
                    } else {
                        $value = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return "&#".hexdec($matches[1]).";";'), $value);
                    }

                    $form_container->output_row($description, $phrase, $form->generate_text_area("edit[$phrase]", $value, ['id' => 'lang_' . $phrase, 'rows' => 2, 'class' => "langeditor_textarea_edit {$quickphrases_dir_class}"]), 'lang_' . $phrase, ['width' => '50%']);
                }
            }

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_language_file, ['disabled' => $no_write]);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/LanguagesQuickPhrases.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            // Validate input
//            if (isset($this->bb->input['lang'])) {
//                $editlang = basename($this->bb->input['lang']);
////                $folder = MYBB_ROOT . 'Languages/' . $editlang . '/';
//            } else {
//                $editlang = '';
////                $folder = '';
//            }
            $editlang = $this->bb->getInput('lang', '');

//            $this->page->add_breadcrumb_item(preg_replace("<\?|\? >", '<span>?</span>', $languages[$editlang]), $this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang);
            $this->page->add_breadcrumb_item($languages[$editlang], $this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang);

            $editwith = $this->bb->getInput('editwith', '');
            $editwithfolder = '';

//            if ($editwith) {
//                $editwithfolder = MYBB_ROOT . 'Languages/' . $editwith . '/';
//            }
            // Validate that edited language pack really exists
//            if (file_exists(MYBB_ROOT . 'Languages/' . $editlang . '.php')) {
                // Then validate edited language pack folders (and try to fix them if missing)
//                @mkdir($folder);
//                @mkdir($folder . 'admin');
//            }

//            if (!file_exists($folder) || !file_exists($folder . 'admin')) {
//                $this->session->flash_message($this->lang->error_folders_fail, 'error');
//                return $response->withRedirect($this->bb->admin_url . '/config/languages');
//            }

            // If we edit in compare mode, verify that at least folders of compared language exists
//            if ($editwithfolder && (!file_exists($editwithfolder) || !file_exists($editwithfolder))) {
//                $this->session->flash_message($this->lang->error_invalid_set, 'error');
//                return $response->withRedirect($this->bb->admin_url . '/config/languages');
//            }
            $this->plugins->runHooks('admin_config_languages_edit');

            if (isset($this->bb->input['file'])) {
                // Validate input
                $file = $this->bb->getInput('file', '');

                $isadmin = $this->bb->getInput('inadmin', 0);
//                if (isset($this->bb->input['inadmin']) && $this->bb->input['inadmin'] == 1) {
//                    $file = 'admin/' . $file;
//                    $isadmin = 1;
//                }
                $this->page->add_breadcrumb_item($file);

//                $editfile = $folder . $file;
                $withfile = '';


                if ($this->bb->request_method == 'post') {
                    // Save edited phrases to language file

                    // To validate input - build array of keys that allready exist in files
//                    @include $editfile;
//                    $valid_keys = (array)$l;
//                    unset($l);
//                    @include $editwithfile;
//
                    $valid_keys = $this->lang->getSection($editlang, $file, $isadmin);
                    $l=[];
                    if ($editwith) {
                        $l = $this->lang->getSection($editwith, $file, $isadmin);
                    }
                    $valid_keys = array_merge($valid_keys, (array)$l);

//                    unset($l);
//                    $langinfo = $this->lang->getLangInfo($editlang);

                    // Then fetch from input only valid keys
                    $contents_file = [];
                    foreach ($valid_keys as $key => $value) {
//                        $contents_wfile .= "\$l['" . $key . "'] = " . var_export($this->bb->input['edit'][$key], true) . ";\n";
                        $contents_file[$key] = $this->bb->input['edit'][$key];
                    }

//                    $editsuccess = $this->lang->saveSection($editlang, $file, $contents_file);

                    // Save edited language file
//                    if ($fp = @fopen($editfile, 'w')) {
                    if ($this->lang->saveSection($editlang, $file, $contents_file, $isadmin)) {
/*
                        // We need info about edited language files to generate credits for our file
                        require MYBB_ROOT . 'Languages/' . $editlang . '.php';

                        // Lets make nice credits header in language file
                        $lang_file_credits = "<?php\n/**\n";
                        $lang_file_credits .= " * MyBB Copyright 2014 MyBB Group, All Rights Reserved\n *\n";
                        $lang_file_credits .= " * Website: http://www.mybb.com\n";
                        $lang_file_credits .= " * License: http://www.mybb.com/about/license\n *\n * /\n\n";
                        $lang_file_credits .= "// " . str_repeat('-', 80) . "\n";
                        $lang_file_credits .= "// MyBB Language Pack File.\n";
                        $lang_file_credits .= "// This file has been generated by MyBB - buildin language pack editor.\n";
                        $lang_file_credits .= "// " . str_repeat('=', 80) . "\n";
                        $lang_file_credits .= "// Friendly name of the language : " . preg_replace("#<\?|\?>#i", " ", $langinfo['name']) . "\n";
                        $lang_file_credits .= "// Author of the language pack : " . preg_replace("#<\?|\?>#i", " ", $langinfo['author']) . "\n";
                        $lang_file_credits .= "// Language pack translators website : " . preg_replace("#<\?|\?>#i", " ", $langinfo['website']) . "\n";
                        $lang_file_credits .= "// Compatible version of MyBB : " . preg_replace("#<\?|\?>#i", " ", $langinfo['version']) . "\n";
                        $lang_file_credits .= "// Last edited in MyBB Editor by : " . preg_replace("#<\?|\?>#i", " ", $this->user->username) . "\n";
                        $lang_file_credits .= "// Last edited date : " . gmdate("r") . "\n";
                        $lang_file_credits .= "// " . str_repeat('-', 80) . "\n\n";

                        $contents_wfile = $lang_file_credits . $contents_wfile;

                        flock($fp, LOCK_EX);
                        fwrite($fp, $contents_wfile);
                        flock($fp, LOCK_UN);
                        fclose($fp);
*/
                        $this->plugins->runHooks('admin_config_languages_edit_commit');

                        // Log admin action
                        $this->bblogger->log_admin_action($editlang, $editlang, $this->bb->getInput('inadmin', 0));

                        $this->session->flash_message($this->lang->success_langfile_updated, 'success');
                        //clear cache files FIXME now all maybe only current???
                        $this->lang->clearCache();

                        return $response->withRedirect($this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang . '&editwith=' . $editwith);
                    } else {
                        $errors[] = $this->lang->error_cannot_write_to_file;
                    }
                }
                unset($langinfo);
//                @include MYBB_ROOT . 'Languages/' . $editwith . '.php';
                $langinfo = $this->lang->getLangInfo($editlang);
                $editlang_dir_class = ' langeditor_ltr';
                if (isset($langinfo['rtl']) && (int)$langinfo['rtl'] > 0) {
                    $editlang_dir_class = ' langeditor_rtl';
                }

                $editwith_dir_class = ' langeditor_ltr';
                if ($editwith) {
                    unset($langinfo);
//                @include MYBB_ROOT . 'Languages/' . $editlang . '.php';
                    $langinfo = $this->lang->getLangInfo($editwith);
                    if (isset($langinfo['rtl']) && (int)$langinfo['rtl'] > 0) {
                        $editwith_dir_class = ' langeditor_rtl';
                    }
                }

                // Build and output form with edited phrases

                // Get file being edited in an array
//                $l = [];
//                @include $editfile;
//                $editvars = (array)$l;
//                unset($l);
                $editvars = $this->lang->getSection($editlang, $file, $isadmin);
//                $withvars = [];
                // Get edit with file in an array if exists
                if ($editwith) {
                    // File we will compare to, may not exists, but dont worry we will auto switch to solo mode later if so
//                    @include $editwithfile;
//                    $withvars = (array)$l;
//                    unset($l);
                    $withvars = $this->lang->getSection($editwith, $file, $isadmin);
                }
                // Start output
                $html = $this->page->output_header($this->lang->languages);

                $sub_tabs['edit_language_variables'] = [
                    'title' => $this->lang->edit_language_variables,
                    'link' => $this->bb->admin_url . '/config/languages',
                    'description' => $this->lang->edit_language_variables_desc
                ];
                $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_language_variables');

                $form = new Form($this->bb, $this->bb->admin_url . '/config/languages?action=edit', 'post', 'edit');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field('file', htmlspecialchars_uni($file));
                $html .= $form->generate_hidden_field('lang', $editlang);
                $html .= $form->generate_hidden_field('editwith', $editwith);
                $html .= $form->generate_hidden_field('inadmin', $this->bb->getInput('inadmin', 0));
                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                }

                // Check if file is writable, before allowing submission
//                $no_write = null;
//                if (file_exists($editfile) && !is_writable($editfile) || !is_writable($folder)) {
//                    $no_write = 1;
//                    $html .= $this->page->output_alert($this->lang->alert_note_cannot_write);
//                }

                $form_container = new FormContainer($this, htmlspecialchars_uni($file));
                if ($editwith && $withvars) {
                    // Editing with another file

//                    $form_container->output_row_header(preg_replace("<\?|\? >", '<span>?</span>', $languages[$editwith]));
//                    $form_container->output_row_header(preg_replace("<\?|\? >", '<span>?</span>', $languages[$editlang]));
                    $form_container->output_row_header($languages[$editwith]);
                    $form_container->output_row_header($languages[$editlang]);

                    foreach ($withvars as $key => $value) {
                        if (my_strtolower($langinfo['charset']) == 'utf-8') {
                            $withvars[$key] = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return dec_to_utf8(hexdec($matches[1]));'), $withvars[$key]);
                            $editvars[$key] = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return dec_to_utf8(hexdec($matches[1]));'), $editvars[$key]);
                        } else {
                            $withvars[$key] = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return dec_to_utf8(hexdec($matches[1]));'), $withvars[$key]);
                            $editvars[$key] = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return "&#".hexdec($matches[1]).";";'), $editvars[$key]);
                        }

                        // Find problems and differences in editfile in comparision to editwithfile

                        // Count {x} in left and right variable
                        $withvars_value_cbvCount = preg_match_all("/{[ \t]*\d+[ \t]*}/", $withvars[$key], $matches);
                        $editvars_value_cbvCount = preg_match_all("/{[ \t]*\d+[ \t]*}/", $editvars[$key], $matches);

                        // If left contain something but right is empty or only spaces || count of {x} are different betwin left and right
                        if ($withvars[$key] && !$editvars[$key] || $withvars_value_cbvCount != $editvars_value_cbvCount) {
                            $textarea_issue_class = ' langeditor_textarea_issue';
                        } else {
                            $textarea_issue_class = '';
                        }

                        $form_container->output_row($key, '', $form->generate_text_area('', $withvars[$key], ['readonly' => true, 'rows' => 2, 'class' => "langeditor_textarea_editwith {$editwith_dir_class}"]), '', ['width' => '50%', 'skip_construct' => true]);
                        $form_container->output_row($key, '', $form->generate_text_area("edit[$key]", $editvars[$key], ['id' => 'lang_' . $key, 'rows' => 2, 'class' => "langeditor_textarea_edit {$textarea_issue_class} {$editlang_dir_class}"]), 'lang_' . $key, ['width' => '50%']);
                    }

                    // Create form fields for extra variables that are present only in edited file
                    $present_in_edit_vars_only = (array)array_diff_key($editvars, $withvars);
                    if ($present_in_edit_vars_only) {
                        foreach ($present_in_edit_vars_only as $key => $value) {
                            if (my_strtolower($langinfo['charset']) == 'utf-8') {
                                $editvars[$key] = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return dec_to_utf8(hexdec($matches[1]));'), $editvars[$key]);
                            } else {
                                $editvars[$key] = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return "&#".hexdec($matches[1]).";";'), $editvars[$key]);
                            }
                            $form_container->output_row('', '', '', '', ['width' => '50%', 'skip_construct' => true]);
                            $form_container->output_row($key, '', $form->generate_text_area("edit[$key]", $editvars[$key], ['id' => 'lang_' . $key, 'rows' => 2, 'class' => "langeditor_textarea_edit {$editlang_dir_class}"]), 'lang_' . $key, ['width' => '50%']);
                        }
                    }
                } else {
                    // Editing individually
                    $form_container->output_row_header(preg_replace("<\?|\?>", '<span>?</span>', $languages[$editlang]));

                    // Make each editing row from current file that we edit
                    foreach ($editvars as $key => $value) {
                        if (my_strtolower($langinfo['charset']) == 'utf-8') {
                            $value = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return dec_to_utf8(hexdec($matches[1]));'), $value);
                        } else {
                            $value = preg_replace_callback('#%u([0-9A-F]{1,4})#i', create_function('$matches', 'return "&#".hexdec($matches[1]).";";'), $value);
                        }
                        $form_container->output_row($key, "", $form->generate_text_area("edit[$key]", $value, ['id' => 'lang_' . $key, 'rows' => 2, 'class' => "langeditor_textarea_edit {$editlang_dir_class}"]), 'lang_' . $key, ['width' => '50%']);
                    }
                }
                $html .= $form_container->end();

//                if (!count($editvars)) {
//                    $no_write = 1;
//                }

//                $buttons[] = $form->generate_submit_button($this->lang->save_language_file, array('disabled' => $no_write));
                $buttons[] = $form->generate_submit_button($this->lang->save_language_file);

                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Config/LanguagesEditFile.html.twig');
            } else {
                // Build and output list of available language files
                $html = $this->page->output_header($this->lang->languages);

                $sub_tabs['language_files'] = [
                    'title' => $this->lang->language_files,
                    'link' => $this->bb->admin_url . '/config/languages?action=edit&lang=' . $editlang,
                    'description' => $this->lang->language_files_desc
                ];
                $sub_tabs['quick_phrases'] = [
                    'title' => $this->lang->quick_phrases,
                    'link' => $this->bb->admin_url . '/config/languages?action=quick_phrases&lang=' . $editlang,
                    'description' => $this->lang->quick_phrases_desc
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'language_files');

//                if (!file_exists(MYBB_ROOT . 'Languages/' . $editlang . '.php')) {
//                    $this->session->flash_message($this->lang->error_invalid_set, 'error');
//                    return $response->withRedirect($this->bb->admin_url . '/config/languages');
//                }
//                require MYBB_ROOT . 'Languages/' . $editlang . '.php';

                $table = new Table;
//                if ($editwithfolder) {
                if ($editwith) {
                    $table->construct_header(preg_replace("<\?|\?>", '<span>?</span>', $languages[$editwith]));
                    $table->construct_header($this->lang->phrases, ['class' => 'align_center', 'width' => 100]);
                    $table->construct_header(preg_replace('<\?|\?>', '<span>?</span>', $languages[$editlang]));
                    $table->construct_header($this->lang->issues, ['class' => 'align_center', 'width' => 100]);
                    $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 100]);
                } else {
                    $table->construct_header(preg_replace("<\?|\?>", '<span>?</span>', $languages[$editlang]));
                    $table->construct_header($this->lang->phrases, ['class' => 'align_center', 'width' => 100]);
                    $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 100]);
                }
//                $sections = $this->lang->getSections($languages[$editlang]);
                $sections = $this->lang->getSections($editlang);
                // Get files in main folder
                $filenames = [];
                foreach ($sections as $k => $v) {
                    $filenames[] = $v->section;
                }
//                if ($handle = opendir($folder)) {
//                    while (false !== ($file = readdir($handle))) {
//                        if (preg_match("#\.lang\.php$#", $file)) {
//                            $filenames[] = $file;
//                        }
//                    }
//                    closedir($handle);
//                    sort($filenames);
//                }

                $edit_colspan = 3;
                // Get files from folder we want to peek at (if possible)
                if ($editwith) {
                    $edit_colspan = 5;
                    $filenameswith = [];
                    $sections = $this->lang->getSections($editlang);
                    foreach ($sections as $k => $v) {
                        $filenameswith[] = $v->section;
                    }
//                    if ($handle = opendir($editwithfolder)) {
//                        while (false !== ($file = readdir($handle))) {
//                            if (preg_match("#\.lang\.php$#", $file)) {
//                                $filenameswith[] = $file;
//                            }
//                        }
//                        closedir($handle);
//                        sort($filenameswith);
//                    }
                }
                if ($editwith) {
                    $files_left = array_diff($filenameswith, $filenames);
                    $files_right = array_diff($filenames, $filenameswith);
                    $files_both = array_intersect($filenameswith, $filenames);
                    foreach ($files_left as $key => $file) {
//                        @include $editwithfolder . $file;
//                        $editvars_left = (array)$l;
//                        unset($l);
                        $editvars_left = $this->lang->getSection($editwith, $file);
                        $icon_issues = "<span class='langeditor_ok' title='" . $this->lang->issues_ok . "'></span>";
                        if (count($editvars_left) > 0) {
                            $icon_issues = "<span class='langeditor_warning' title='" . $this->lang->issues_warning . "'></span>";
                        }

                        $table->construct_cell($file, ['class' => 'langeditor_editwithfile']);
                        $table->construct_cell(count($editvars_left), ['class' => 'langeditor_phrases']);
                        $table->construct_cell('', ['class' => 'langeditor_editfile']);
                        $table->construct_cell($icon_issues, ['class' => 'langeditor_issues']);
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$file}\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                        $table->construct_row();
                    }
                    foreach ($files_right as $key => $file) {
//                        @include $folder . $file;
//                        $editvars_right = (array)$l;
//                        unset($l);
                        $editvars_right = $this->lang->getSection($editwith, $file);

                        $icon_issues = "<span class='langeditor_ok' title='" . $this->lang->issues_ok . "'></span>";
                        if (count($editvars_right > 0)) {
                            $icon_issues = "<span class='langeditor_nothingtocompare' title='" . $this->lang->issues_nothingtocompare . "'></span>";
                        }

                        $table->construct_cell('', ['class' => 'langeditor_editwithfile']);
                        $table->construct_cell('', ['class' => 'langeditor_phrases']);
                        $table->construct_cell($file, ['class' => 'langeditor_editfile']);
                        $table->construct_cell($icon_issues, ['class' => 'langeditor_issues']);
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$file}\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                        $table->construct_row();
                    }
                    foreach ($files_both as $key => $file) {
//                        $l = [];
//                        @include $editwithfolder . $file;
//                        $editvars_left = (array)$l;
//                        unset($l);
//                        $l = [];
//                        @include $folder . $file;
//                        $editvars_right = (array)$l;
//                        unset($l);
                        $editvars_left = $this->lang->getSection($editlang, $file);
//                        $editvars_left = $this->lang->sectionCount($editlang, $file, $admin);
                        $editvars_right = $this->lang->getSection($editwith, $file);
                        $table->construct_cell($file, ['class' => 'langeditor_editwithfile']);
                        $table->construct_cell(count($editvars_left), ['class' => 'langeditor_phrases']);
                        $table->construct_cell($file, ['class' => 'langeditor_editfile']);

                        $icon_issues = "<span class='langeditor_ok' title='" . $this->lang->issues_ok . "'></span>";

                        // Find problems and differences in editfile in comparision to editwithfile
                        foreach ($editvars_left as $editvars_left_key => $editvars_left_value) {
                            // Count {x} in left and right variable
                            $editvars_left_value_cbvCount = preg_match_all("/{[ \t]*\d+[ \t]*}/", $editvars_left_value, $matches);
                            $editvars_right_value_cbvCount = preg_match_all("/{[ \t]*\d+[ \t]*}/", $editvars_right[$editvars_left_key], $matches);
                            // If left contain something but right is empty || count of {x} are different betwin left and right
                            if ($editvars_left_value && !$editvars_right[$editvars_left_key] || $editvars_left_value_cbvCount != $editvars_right_value_cbvCount) {
                                $icon_issues = "<span class='langeditor_warning' title='" . $this->lang->issues_warning . "'></span>";
                                // One difference is enought, so lets abort checking for more.
                                break;
                            }
                        }

                        $table->construct_cell($icon_issues, ['class' => 'langeditor_issues']);
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$file}\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                        $table->construct_row();
                    }
                } else {
//                    foreach ($filenames as $key => $file) {
                    foreach ($sections as $key => $var) {
//                        $l=[];
//                        @include $folder . $file;
//                        $editvars_count = (array)$l;
//                        unset($l);
//                        $editvars_count = $this->lang->sectionCount($languages[$editlang]['name'], $var->section);
                        $editvars_count = $this->lang->sectionCount($editlang, $var->section);

                        $table->construct_cell($var->section, ['class' => 'langeditor_editfile']);
                        $table->construct_cell($editvars_count, ['class' => 'langeditor_phrases']);
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$var->section}\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                        $table->construct_row();
                    }
                }

                if ($table->num_rows() == 0) {
                    $table->construct_cell($this->lang->no_language_files_front_end, ['colspan' => $edit_colspan]);
                    $table->construct_row();
                }

                $html .= $table->output($this->lang->front_end);
                if (!isset($langinfo)) {
                    $langinfo = $this->lang->getLangInfo($editlang);
                }
                if ($langinfo['admin'] != 0) {
                    $table = new Table;
                    if ($editwith) {
                        $table->construct_header(preg_replace('<\?|\?>', '<span>?</span>', $languages[$editwith]));
                        $table->construct_header($this->lang->phrases, ['class' => 'align_center', 'width' => 100]);
                        $table->construct_header(preg_replace('<\?|\?>', '<span>?</span>', $languages[$editlang]));
                        $table->construct_header($this->lang->issues, ['class' => 'align_center', 'width' => 100]);
                        $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 100]);
                    } else {
                        $table->construct_header(preg_replace('<\?|\?>', '<span>?</span>', $languages[$editlang]));
                        $table->construct_header($this->lang->phrases, ['class' => 'align_center', 'width' => 100]);
                        $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 100]);
                    }

//                    $adminSections = $this->lang->getSections($languages[$editlang], 1);
                    $adminSections = $this->lang->getSections($editlang, 1);
                    // Get files in admin folder
                    $adminfilenames = [];
                    foreach ($adminSections as $k => $v) {
                        $adminfilenames[] = $v->section;
                    }
//                    $adminfilenames = array();
//                    if ($handle = opendir($folder . 'admin')) {
//                        while (false !== ($file = readdir($handle))) {
//                            if (preg_match("#\.lang\.php$#", $file)) {
//                                $adminfilenames[] = $file;
//                            }
//                        }
//                        closedir($handle);
//                        sort($adminfilenames);
//                    }
                    $edit_colspan = 3;
                    // Get files from admin folder we want to peek at (if possible)
                    if ($editwith) {
                        $edit_colspan = 5;
                        $adminSections = $this->lang->getSections($editwith, 1);
                        $adminfilenameswith = [];
                        foreach ($adminSections as $k => $v) {
                            $adminfilenameswith[] = $v->section;
//                        }
//                        if ($handle = opendir($editwithfolder . 'admin')) {
//                            while (false !== ($file = readdir($handle))) {
//                                if (preg_match("#\.lang\.php$#", $file)) {
//                                    $adminfilenameswith[] = $file;
//                                }
//                            }
//                            closedir($handle);
//                            sort($adminfilenameswith);
//                        }
                        }

//                    if ($editwith) {
                        $files_left = array_diff($adminfilenameswith, $adminfilenames);
                        $files_right = array_diff($adminfilenames, $adminfilenameswith);
                        $files_both = array_intersect($adminfilenameswith, $adminfilenames);
                        foreach ($files_left as $key => $file) {
//                            @include $editwithfolder . 'admin/' . $file;
//                            $editvars_left = (array)$l;
//                            unset($l);

                            $icon_issues = "<span class='langeditor_ok' title='" . $this->lang->issues_ok . "'></span>";
                            if (count($editvars_left) > 0) {
                                $icon_issues = "<span class='langeditor_warning' title='" . $this->lang->issues_warning . "'></span>";
                            }

                            $table->construct_cell($file, ['class' => 'langeditor_editwithfile']);
                            $table->construct_cell(count($editvars_left), ['class' => 'langeditor_phrases']);
                            $table->construct_cell('', ['class' => 'langeditor_editfile']);
                            $table->construct_cell($icon_issues, ['class' => 'langeditor_issues']);
                            $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$file}&amp;inadmin=1\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                            $table->construct_row();
                        }
                        foreach ($files_right as $key => $file) {
//                            @include $folder . 'admin/' . $file;
//                            $editvars_right = (array)$l;
//                            unset($l);

                            $icon_issues = "<span class='langeditor_ok' title='" . $this->lang->issues_ok . "'></span>";
                            if (count($editvars_right > 0)) {
                                $icon_issues = "<span class='langeditor_nothingtocompare' title='" . $this->lang->issues_nothingtocompare . "'></span>";
                            }

                            $table->construct_cell('', ['class' => 'langeditor_editwithfile']);
                            $table->construct_cell('', ['class' => 'langeditor_phrases']);
                            $table->construct_cell($file, ['class' => 'langeditor_editfile']);
                            $table->construct_cell($icon_issues, ['class' => 'langeditor_issues']);
                            $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$file}&amp;inadmin=1\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                            $table->construct_row();
                        }
                        foreach ($files_both as $key => $file) {
//                            @include $editwithfolder . 'admin/' . $file;
//                            $editvars_left = (array)$l;
//                            unset($l);
//                            @include $folder . 'admin/' . $file;
//                            $editvars_right = (array)$l;
//                            unset($l);
                            $editvars_left = $this->lang->getSection($editwith, $file, 1);
                            $editvars_right = $this->lang->getSection($editlang, $file, 1);

                            $table->construct_cell($file, ['class' => 'langeditor_editwithfile']);
                            $table->construct_cell(count($editvars_left), ['class' => 'langeditor_phrases']);
//                            $table->construct_cell(count($files_both), array('class' => 'langeditor_phrases'));
                            $table->construct_cell($file, ['class' => 'langeditor_editfile']);

                            $icon_issues = "<span class='langeditor_ok' title='" . $this->lang->issues_ok . "'></span>";

                            // Find problems and differences in editfile in comparision to editwithfile
                            foreach ($editvars_left as $editvars_left_key => $editvars_left_value) {
                                // Count {x} in left and right variable
                                $editvars_left_value_cbvCount = preg_match_all("/{[ \t]*\d+[ \t]*}/", $editvars_left_value, $matches);
                                $editvars_right_value_cbvCount = preg_match_all("/{[ \t]*\d+[ \t]*}/", $editvars_right[$editvars_left_key], $matches);
                                // If left contain something but right is empty || count of {x} are different betwin left and right
                                if ($editvars_left_value && !$editvars_right[$editvars_left_key] || $editvars_left_value_cbvCount != $editvars_right_value_cbvCount) {
                                    $icon_issues = "<span class='langeditor_warning' title='" . $this->lang->issues_warning . "'></span>";
                                    // One difference is enought.
                                    break;
                                }
                            }

                            $table->construct_cell($icon_issues, ['class' => 'langeditor_issues']);
                            $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$file}&amp;inadmin=1\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                            $table->construct_row();
                        }
                    } else {
//                        foreach ($adminfilenames as $key => $file) {
                        foreach ($adminSections as $key => $var) {
//                            @include $folder . 'admin/' . $file;
//                            $editvars_count = (array)$l;
//                            unset($l);
//                            $editvars_count = $this->lang->sectionCount($languages[$editlang]['name'], $var->section, 1);
                            $editvars_count = $this->lang->sectionCount($editlang, $var->section, 1);

                            $table->construct_cell($var->section, ['class' => 'langeditor_editfile']);
                            $table->construct_cell($editvars_count, ['class' => 'langeditor_phrases']);
                            $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/languages?action=edit&amp;lang={$editlang}&amp;editwith={$editwith}&amp;file={$var->section}&amp;inadmin=1\">{$this->lang->edit}</a>", ["class" => "langeditor_edit"]);
                            $table->construct_row();
                        }
                    }

                    if ($table->num_rows() == 0) {
                        $table->construct_cell($this->lang->no_language_files_admin_cp, ['colspan' => $edit_colspan]);
                        $table->construct_row();
                    }

                    $html .= $table->output($this->lang->admin_cp);
                }
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/LanguagesEdit.html.twig');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->languages);

            $sub_tabs['languages'] = [
                'title' => $this->lang->languages,
                'link' => $this->bb->admin_url . '/config/languages',
                'description' => $this->lang->languages_desc
            ];
            $sub_tabs['find_language'] = [
                'title' => $this->lang->find_language_packs,
                'link' => 'http://community.mybb.com/mods.php?action=browse&category=19',
                'target' => '_blank'
            ];

            $this->plugins->runHooks('admin_config_languages_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'languages');

            $table = new Table;
            $table->construct_header($this->lang->languagevar);
            $table->construct_header($this->lang->version, ['class' => 'align_center', 'width' => 100]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 155]);

            asort($languages);

            foreach ($languages as $key1 => $langname1) {
                $langselectlangs[$key1] = $this->lang->sprintf($this->lang->edit_with, preg_replace('<\?|\?>', '<span>?</span>', $langname1));
            }

            foreach ($languages as $key => $langname) {
//                include MYBB_ROOT . 'Languages/' . $key . '.php';
                $l = $this->lang->getLangInfo($key);
                if (!empty($l['website'])) {
                    $author = "<a href=\"{$l['website']}\" target=\"_blank\">{$l['author']}</a>";
                } else {
                    $author = $l['author'];
                }

                $table->construct_cell("<span class='langeditor_info_name'>" . preg_replace("<\?|\?>", "<span>?</span>", $l['name']) . "</span><br /><span class='langeditor_info_author'>{$author}</span>");
                $table->construct_cell($this->lang->settings['version'], ['class' => 'align_center']);

                $popup = new PopupMenu("language_{$key}", $this->lang->options);
                $popup->add_item($this->lang->edit_language_variables, $this->bb->admin_url . '/config/languages?action=edit&lang=' . $key);
                foreach ($langselectlangs as $key1 => $langname1) {
                    if ($key != $key1) {
                        $popup->add_item($langname1, $this->bb->admin_url . '/config/languages?action=edit&lang=' . $key . '&editwith=' . $key1);
                    }
                }
                $popup->add_item($this->lang->edit_properties, $this->bb->admin_url . '/config/languages?action=edit_properties&lang=' . $key);
                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_language, ['colspan' => 3]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->installed_language_packs);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Languages.html.twig');
        }
    }
}
