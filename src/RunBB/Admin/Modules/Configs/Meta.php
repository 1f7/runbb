<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

class Meta
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * @return bool true
     */
    public function configs_meta()
    {
        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'settings', 'title' => $this->bb->lang->bbsettings, 'link' => $this->bb->admin_url . '/config'];
        $sub_menu['20'] = ['id' => 'banning', 'title' => $this->bb->lang->banning, 'link' => $this->bb->admin_url . '/config/banning'];
        $sub_menu['30'] = ['id' => 'profile_fields', 'title' => $this->bb->lang->custom_profile_fields, 'link' => $this->bb->admin_url . '/config/profile_fields'];
        $sub_menu['40'] = ['id' => 'smilies', 'title' => $this->bb->lang->smilies, 'link' => $this->bb->admin_url . '/config/smilies'];
        $sub_menu['50'] = ['id' => 'badwords', 'title' => $this->bb->lang->word_filters, 'link' => $this->bb->admin_url . '/config/badwords'];
        $sub_menu['60'] = ['id' => 'mycode', 'title' => $this->bb->lang->mycode, 'link' => $this->bb->admin_url . '/config/mycode'];
        $sub_menu['70'] = ['id' => 'languages', 'title' => $this->bb->lang->languages, 'link' => $this->bb->admin_url . '/config/languages'];
        $sub_menu['80'] = ['id' => 'post_icons', 'title' => $this->bb->lang->post_icons, 'link' => $this->bb->admin_url . '/config/post_icons'];
        $sub_menu['90'] = ['id' => 'help_documents', 'title' => $this->bb->lang->help_documents, 'link' => $this->bb->admin_url . '/config/help_documents'];
        $sub_menu['100'] = ['id' => 'plugins', 'title' => $this->bb->lang->plugins, 'link' => $this->bb->admin_url . '/config/plugins'];
        $sub_menu['110'] = ['id' => 'attachment_types', 'title' => $this->bb->lang->attachment_types, 'link' => $this->bb->admin_url . '/config/attachment_types'];
        $sub_menu['120'] = ['id' => 'mod_tools', 'title' => $this->bb->lang->moderator_tools, 'link' => $this->bb->admin_url . '/config/mod_tools'];
        $sub_menu['130'] = ['id' => 'spiders', 'title' => $this->bb->lang->spiders_bots, 'link' => $this->bb->admin_url . '/config/spiders'];
        $sub_menu['140'] = ['id' => 'calendars', 'title' => $this->bb->lang->calendars, 'link' => $this->bb->admin_url . '/config/calendars'];
        $sub_menu['150'] = ['id' => 'warning', 'title' => $this->bb->lang->warning_system, 'link' => $this->bb->admin_url . '/config/warning'];
        $sub_menu['160'] = ['id' => 'thread_prefixes', 'title' => $this->bb->lang->thread_prefixes, 'link' => $this->bb->admin_url . '/config/thread_prefixes'];
        $sub_menu['170'] = ['id' => 'questions', 'title' => $this->bb->lang->security_questions, 'link' => $this->bb->admin_url . '/config/questions'];

        $sub_menu = $this->bb->plugins->runHooks('admin_config_menu', $sub_menu);

        $this->bb->page->add_menu_item($this->bb->lang->configuration, 'configs', $this->bb->admin_url . '/config', 10, $sub_menu);

        return true;
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function configs_action_handler($action)
    {
        $this->bb->page->active_module = 'configs';

        $actions = [
            'plugins' => ['active' => 'plugins', 'file' => 'plugins.php'],
            'smilies' => ['active' => 'smilies', 'file' => 'smilies.php'],
            'banning' => ['active' => 'banning', 'file' => 'banning.php'],
            'badwords' => ['active' => 'badwords', 'file' => 'badwords.php'],
            'profile_fields' => ['active' => 'profile_fields', 'file' => 'profile_fields.php'],
            'spiders' => ['active' => 'spiders', 'file' => 'spiders.php'],
            'attachment_types' => ['active' => 'attachment_types', 'file' => 'attachment_types.php'],
            'languages' => ['active' => 'languages', 'file' => 'languages.php'],
            'post_icons' => ['active' => 'post_icons', 'file' => 'post_icons.php'],
            'help_documents' => ['active' => 'help_documents', 'file' => 'help_documents.php'],
            'calendars' => ['active' => 'calendars', 'file' => 'calendars.php'],
            'warning' => ['active' => 'warning', 'file' => 'warning.php'],
            'mod_tools' => ['active' => 'mod_tools', 'file' => 'mod_tools.php'],
            'mycode' => ['active' => 'mycode', 'file' => 'mycode.php'],
            'settings' => ['active' => 'settings', 'file' => 'settings.php'],
            'thread_prefixes' => ['active' => 'thread_prefixes', 'file' => 'thread_prefixes.php'],
            'questions' => ['active' => 'questions', 'file' => 'questions.php']
        ];

        $actions = $this->bb->plugins->runHooks('admin_config_action_handler', $actions);

        //\RunBB\Init
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('forum-config');

        if (isset($actions[$action])) {
            $this->bb->page->active_action = $actions[$action]['active'];
            return $actions[$action]['file'];
        } else {
            $this->bb->page->active_action = 'settings';
            return 'settings';
        }
    }

    /**
     * @return array
     */
    public function configs_admin_permissions()
    {
        $admin_permissions = [
            'settings' => $this->bb->lang->can_manage_settings,
            'banning' => $this->bb->lang->can_manage_banned_accounts,
            'profile_fields' => $this->bb->lang->can_manage_custom_profile_fields,
            'smilies' => $this->bb->lang->can_manage_smilies,
            'badwords' => $this->bb->lang->can_manage_bad_words,
            'mycode' => $this->bb->lang->can_manage_custom_mycode,
            'languages' => $this->bb->lang->can_manage_language_packs,
            'post_icons' => $this->bb->lang->can_manage_post_icons,
            'help_documents' => $this->bb->lang->can_manage_help_documents,
            'plugins' => $this->bb->lang->can_manage_plugins,
            'attachment_types' => $this->bb->lang->can_manage_attachment_types,
            'spiders' => $this->bb->lang->can_manage_spiders_bots,
            'calendars' => $this->bb->lang->can_manage_calendars,
            'warning' => $this->bb->lang->can_manage_warning_system,
            'mod_tools' => $this->bb->lang->can_manage_mod_tools,
            'thread_prefixes' => $this->bb->lang->can_manage_thread_prefixes,
            'questions' => $this->bb->lang->can_manage_security_questions
        ];

        $admin_permissions = $this->bb->plugins->runHooks('admin_config_permissions', $admin_permissions);

        return ['name' => $this->bb->lang->configuration, 'permissions' => $admin_permissions, 'disporder' => 10];
    }
}
