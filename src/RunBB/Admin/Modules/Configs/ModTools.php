<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class ModTools extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_mod_tools', false, true);

        $this->page->add_breadcrumb_item($this->lang->mod_tools, $this->bb->admin_url . '/config/mod_tools');

        $this->plugins->runHooks('admin_config_mod_tools_begin');

        if ($this->bb->input['action'] == 'delete_post_tool') {
            $query = $this->db->simple_select('modtools', '*', "tid='{$this->bb->input['tid']}'");
            $tool = $this->db->fetch_array($query);

            // Does the post tool not exist?
            if (!$tool['tid']) {
                $this->session->flash_message($this->lang->error_invalid_post_tool, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            }

            $this->plugins->runHooks('admin_config_mod_tools_delete_post_tool');

            if ($this->bb->request_method == 'post') {
                // Delete the type
                $this->db->delete_query('modtools', "tid='{$tool['tid']}'");

                $this->plugins->runHooks('admin_config_mod_tools_delete_post_tool_commit');

                // Log admin action
                $this->bblogger->log_admin_action($tool['tid'], $tool['name']);
                $this->cache->update_forumsdisplay();

                $this->session->flash_message($this->lang->success_post_tool_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/mod_tools?action=post_tools&tid=' . $type['tid'], $this->lang->confirm_post_tool_deletion);
            }
        }

        if ($this->bb->input['action'] == 'delete_thread_tool') {
            $query = $this->db->simple_select('modtools', '*', "tid='{$this->bb->input['tid']}'");
            $tool = $this->db->fetch_array($query);

            // Does the post tool not exist?
            if (!$tool['tid']) {
                $this->session->flash_message($this->lang->error_invalid_thread_tool, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            }

            $this->plugins->runHooks('admin_config_mod_tools_delete_thread_tool');

            if ($this->bb->request_method == 'post') {
                // Delete the type
                $this->db->delete_query('modtools', "tid='{$tool['tid']}'");

                $this->plugins->runHooks('admin_config_mod_tools_delete_thread_tool_commit');

                // Log admin action
                $this->bblogger->log_admin_action($tool['tid'], $tool['name']);
                $this->cache->update_forumsdisplay();

                $this->session->flash_message($this->lang->success_thread_tool_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/mod_tools?action=delete_thread_tool&tid=' . $tool['tid'], $this->lang->confirm_thread_tool_deletion);
            }
        }

        if ($this->bb->input['action'] == 'post_tools') {
            $this->plugins->runHooks('admin_config_mod_tools_post_tools');

            $this->page->add_breadcrumb_item($this->lang->post_tools);
            $html = $this->page->output_header($this->lang->mod_tools . ' - ' . $this->lang->post_tools);

            $sub_tabs['thread_tools'] = [
                'title' => $this->lang->thread_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools'
            ];
            $sub_tabs['add_thread_tool'] = [
                'title' => $this->lang->add_thread_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_thread_tool'
            ];
            $sub_tabs['post_tools'] = [
                'title' => $this->lang->post_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=post_tools',
                'description' => $this->lang->post_tools_desc
            ];
            $sub_tabs['add_post_tool'] = [
                'title' => $this->lang->add_post_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_post_tool'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'post_tools');

            $table = new Table;
            $table->construct_header($this->lang->title);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('modtools', 'tid, name, description, type', "type='p'", ['order_by' => 'name']);
            while ($tool = $this->db->fetch_array($query)) {
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/mod_tools?action=edit_post_tool&amp;tid={$tool['tid']}\"><strong>" . htmlspecialchars_uni($tool['name']) . "</strong></a><br /><small>" . htmlspecialchars_uni($tool['description']) . "</small>");
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/mod_tools?action=edit_post_tool&amp;tid={$tool['tid']}\">{$this->lang->edit}</a>", ['width' => 100, 'class' => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/mod_tools?action=delete_post_tool&amp;tid={$tool['tid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_post_tool_deletion}')\">{$this->lang->delete}</a>", ['width' => 100, 'class' => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_post_tools, ['colspan' => 3]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->post_tools);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ModToolsPostTool.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_thread_tool') {
            $query = $this->db->simple_select('modtools', 'COUNT(tid) as tools', "tid = '{$this->bb->input['tid']}' AND type='t'");
            if ($this->db->fetch_field($query, 'tools') < 1) {
                $this->session->flash_message($this->lang->error_invalid_thread_tool, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
            }

            $this->plugins->runHooks('admin_config_mod_tools_edit_thread_tool');

            if ($this->bb->request_method == 'post') {
                if (trim($this->bb->input['title']) == '') {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (trim($this->bb->input['description']) == '') {
                    $errors[] = $this->lang->error_missing_description;
                }

                if ($this->bb->input['forum_type'] == 2) {
                    $forum_checked[1] = '';
                    $forum_checked[2] = 'checked="checked"';

                    if (count($this->bb->input['forum_1_forums']) < 1) {
                        $errors[] = $this->lang->error_no_forums_selected;
                    }
                } else {
                    $forum_checked[1] = 'checked="checked"';
                    $forum_checked[2] = '';

                    $this->bb->input['forum_1_forums'] = '';
                }

                if ($this->bb->input['group_type'] == 2) {
                    $group_checked[1] = '';
                    $group_checked[2] = 'checked="checked"';

                    if (count($this->bb->input['group_1_groups']) < 1) {
                        $errors[] = $this->lang->error_no_groups_selected;
                    }
                } else {
                    $group_checked[1] = 'checked="checked"';
                    $group_checked[2] = '';

                    $this->bb->input['group_1_groups'] = '';
                }

                if ($this->bb->input['approvethread'] != '' &&
                    $this->bb->input['approvethread'] != 'approve' &&
                    $this->bb->input['approvethread'] != 'unapprove' &&
                    $this->bb->input['approvethread'] != 'toggle'
                ) {
                    $this->bb->input['approvethread'] = '';
                }

                if ($this->bb->input['softdeletethread'] != '' &&
                    $this->bb->input['softdeletethread'] != 'softdelete' &&
                    $this->bb->input['softdeletethread'] != 'restore' &&
                    $this->bb->input['softdeletethread'] != 'toggle'
                ) {
                    $this->bb->input['softdeletethread'] = '';
                }

                if ($this->bb->input['openthread'] != '' &&
                    $this->bb->input['openthread'] != 'open' &&
                    $this->bb->input['openthread'] != 'close' &&
                    $this->bb->input['openthread'] != 'toggle'
                ) {
                    $this->bb->input['openthread'] = '';
                }

                if ($this->bb->input['stickthread'] != '' &&
                    $this->bb->input['stickthread'] != 'stick' &&

                    $this->bb->input['stickthread'] != 'unstick' &&
                    $this->bb->input['stickthread'] != 'toggle'
                ) {
                    $this->bb->input['stickthread'] = '';
                }

                if ($this->bb->input['move_type'] == 2) {
                    $move_checked[1] = '';
                    $move_checked[2] = 'checked="checked"';

                    if (!$this->bb->input['move_1_forum']) {
                        $errors[] = $this->lang->error_no_move_forum_selected;
                    } else {
                        // Check that the destination forum is not a category
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('move_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }

                    if ($this->bb->input['move_2_redirect'] != 1 && $this->bb->input['move_2_redirect'] != 0) {
                        $this->bb->input['move_2_redirect'] = 0;
                    }

                    if (!isset($this->bb->input['move_3_redirecttime'])) {
                        $this->bb->input['move_3_redirecttime'] = '';
                    }
                } else {
                    $move_checked[1] = 'checked="checked"';
                    $move_checked[2] = '';

                    $this->bb->input['move_1_forum'] = '';
                    $this->bb->input['move_2_redirect'] = 0;
                    $this->bb->input['move_3_redirecttime'] = '';
                }

                if ($this->bb->input['copy_type'] == 2) {
                    $copy_checked[1] = '';
                    $copy_checked[2] = 'checked="checked"';

                    if (!$this->bb->input['copy_1_forum']) {
                        $errors[] = $this->lang->error_no_copy_forum_selected;
                    } else {
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('copy_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $copy_checked[1] = 'checked="checked"';
                    $copy_checked[2] = '';

                    $this->bb->input['copy_1_forum'] = '';
                }

                if (!isset($errors)) {
                    $thread_options = [
                        'confirmation' => $this->bb->getInput('confirmation', 0),
                        'deletethread' => $this->bb->getInput('deletethread', 0),
                        'mergethreads' => $this->bb->getInput('mergethreads', 0),
                        'deletepoll' => $this->bb->getInput('deletepoll', 0),
                        'removeredirects' => $this->bb->getInput('removeredirects', 0),
                        'removesubscriptions' => $this->bb->getInput('removesubscriptions', 0),
                        'recountrebuild' => $this->bb->getInput('recountrebuild', 0),
                        'approvethread' => $this->bb->input['approvethread'],
                        'softdeletethread' => $this->bb->input['softdeletethread'],
                        'openthread' => $this->bb->input['openthread'],
                        'stickthread' => $this->bb->input['stickthread'],
                        'movethread' => $this->bb->getInput('move_1_forum', 0),
                        'movethreadredirect' => $this->bb->getInput('move_2_redirect', 0),
                        'movethreadredirectexpire' => $this->bb->getInput('move_3_redirecttime', 0),
                        'copythread' => $this->bb->getInput('copy_1_forum', 0),
                        'newsubject' => $this->bb->input['newsubject'],
                        'addreply' => $this->bb->input['newreply'],
                        'replysubject' => $this->bb->input['newreplysubject'],
                        'pm_subject' => $this->bb->input['pm_subject'],
                        'pm_message' => $this->bb->input['pm_message'],
                        'threadprefix' => $this->bb->getInput('threadprefix', 0)
                    ];

                    $update_tool['type'] = 't';
                    $update_tool['threadoptions'] = $this->db->escape_string(my_serialize($thread_options));
                    $update_tool['name'] = $this->db->escape_string($this->bb->input['title']);
                    $update_tool['description'] = $this->db->escape_string($this->bb->input['description']);
                    $update_tool['forums'] = '';
                    $update_tool['groups'] = '';

                    if ($this->bb->input['forum_type'] == 2) {
                        if (is_array($this->bb->input['forum_1_forums'])) {
                            $checked = [];

                            foreach ($this->bb->input['forum_1_forums'] as $fid) {
                                $checked[] = (int)$fid;
                            }

                            $update_tool['forums'] = implode(',', $checked);
                        }
                    } else {
                        $update_tool['forums'] = '-1';
                    }

                    if ($this->bb->input['group_type'] == 2) {
                        if (is_array($this->bb->input['group_1_groups'])) {
                            $checked = [];

                            foreach ($this->bb->input['group_1_groups'] as $gid) {
                                $checked[] = (int)$gid;
                            }

                            $update_tool['groups'] = implode(',', $checked);
                        }
                    } else {
                        $update_tool['groups'] = '-1';
                    }

                    $this->plugins->runHooks('admin_config_mod_tools_edit_thread_tool_commit');

                    $this->db->update_query('modtools', $update_tool, "tid='{$this->bb->input['tid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($this->bb->input['tid'], $this->bb->input['title']);
                    $this->cache->update_forumsdisplay();

                    $this->session->flash_message($this->lang->success_mod_tool_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_thread_tool);
            $html = $this->page->output_header($this->lang->mod_tools . ' - ' . $this->lang->edit_thread_tool);

            $sub_tabs['edit_thread_tool'] = [
                'title' => $this->lang->edit_thread_tool,
                'description' => $this->lang->edit_thread_tool_desc,
                'link' => $this->bb->admin_url . '/config/mod_tools'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_thread_tool');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/mod_tools?action=edit_thread_tool', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $this->bb->input['tid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $query = $this->db->simple_select('modtools', '*', "tid = '{$this->bb->input['tid']}'");
                $modtool = $this->db->fetch_array($query);
                $thread_options = my_unserialize($modtool['threadoptions']);

                $this->bb->input['title'] = $modtool['name'];
                $this->bb->input['description'] = $modtool['description'];
                $this->bb->input['forum_1_forums'] = explode(',', $modtool['forums']);
                $this->bb->input['group_1_groups'] = explode(',', $modtool['groups']);

                if (!$modtool['forums'] || $modtool['forums'] == -1) {
                    $forum_checked[1] = 'checked="checked"';
                    $forum_checked[2] = '';
                } else {
                    $forum_checked[1] = '';
                    $forum_checked[2] = 'checked="checked"';
                }

                if (!$modtool['groups'] || $modtool['groups'] == -1) {
                    $group_checked[1] = 'checked="checked"';
                    $group_checked[2] = '';
                } else {
                    $group_checked[1] = '';
                    $group_checked[2] = 'checked="checked"';
                }

                $this->bb->input['confirmation'] = $thread_options['confirmation'];
                $this->bb->input['approvethread'] = $thread_options['approvethread'];
                $this->bb->input['softdeletethread'] = $thread_options['softdeletethread'];
                $this->bb->input['openthread'] = $thread_options['openthread'];
                $this->bb->input['stickthread'] = $thread_options['stickthread'];
                $this->bb->input['move_1_forum'] = $thread_options['movethread'];
                $this->bb->input['move_2_redirect'] = $thread_options['movethreadredirect'];
                $this->bb->input['move_3_redirecttime'] = $thread_options['movethreadredirectexpire'];

                if (!$thread_options['movethread']) {
                    $move_checked[1] = 'checked="checked"';
                    $move_checked[2] = '';
                } else {
                    $move_checked[1] = '';
                    $move_checked[2] = 'checked="checked"';
                }

                if (!$thread_options['copythread']) {
                    $copy_checked[1] = 'checked="checked"';
                    $copy_checked[2] = '';
                } else {
                    $copy_checked[1] = '';
                    $copy_checked[2] = 'checked="checked"';
                }

                $this->bb->input['copy_1_forum'] = $thread_options['copythread'];
                $this->bb->input['deletethread'] = $thread_options['deletethread'];
                $this->bb->input['mergethreads'] = $thread_options['mergethreads'];
                $this->bb->input['deletepoll'] = $thread_options['deletepoll'];
                $this->bb->input['removeredirects'] = $thread_options['removeredirects'];
                $this->bb->input['removesubscriptions'] = $thread_options['removesubscriptions'];
                $this->bb->input['recountrebuild'] = $thread_options['recountrebuild'];
                $this->bb->input['threadprefix'] = $thread_options['threadprefix'];
                $this->bb->input['newsubject'] = $thread_options['newsubject'];
                $this->bb->input['newreply'] = $thread_options['addreply'];
                $this->bb->input['newreplysubject'] = $thread_options['replysubject'];
                $this->bb->input['pm_subject'] = $thread_options['pm_subject'];
                $this->bb->input['pm_message'] = $thread_options['pm_message'];
            }

            $form_container = new FormContainer($this, $this->lang->general_options);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"1\" {$forum_checked[1]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"2\" {$forum_checked[2]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"forum_2\" class=\"forums\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
					<td>" . $form->generate_forum_select('forum_1_forums[]', $this->bb->input['forum_1_forums'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('forum');
	</script>";
            $form_container->output_row($this->lang->available_in_forums . ' <em>*</em>', '', $actions);

            $actions = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"1\" {$group_checked[1]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"2\" {$group_checked[2]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"group_2\" class=\"groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('group_1_groups[]', $this->bb->input['group_1_groups'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('group');
	</script>";
            $form_container->output_row($this->lang->available_to_groups . ' <em>*</em>', '', $actions);
            $form_container->output_row($this->lang->show_confirmation . ' <em>*</em>', '', $form->generate_yes_no_radio('confirmation', $this->bb->input['confirmation'], ['style' => 'width: 2em;']));
            $html .= $form_container->end();

            $approve_unapprove = [
                '' => $this->lang->no_change,
                'approve' => $this->lang->approve,
                'unapprove' => $this->lang->unapprove,
                'toggle' => $this->lang->toggle
            ];

            $open_close = [
                '' => $this->lang->no_change,
                'open' => $this->lang->open,
                'close' => $this->lang->close,
                'toggle' => $this->lang->toggle
            ];

            $stick_unstick = [
                '' => $this->lang->no_change,
                'stick' => $this->lang->stick,
                'unstick' => $this->lang->unstick,
                'toggle' => $this->lang->toggle
            ];

            $form_container = new FormContainer($this, $this->lang->thread_moderation);
            $form_container->output_row($this->lang->approve_unapprove . ' <em>*</em>', '', $form->generate_select_box('approvethread', $approve_unapprove, $this->bb->input['approvethread'], ['id' => 'approvethread']), 'approvethread');
            $form_container->output_row($this->lang->open_close_thread . ' <em>*</em>', '', $form->generate_select_box('openthread', $open_close, $this->bb->input['openthread'], ['id' => 'openthread']), 'openthread');
            $form_container->output_row($this->lang->stick_unstick_thread . ' <em>*</em>', '', $form->generate_select_box('stickthread', $stick_unstick, $this->bb->input['stickthread'], ['id' => 'stickthread']), 'stickthread');


            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"1\" {$move_checked[1]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_move_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"2\" {$move_checked[2]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->move_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"move_2\" class=\"moves\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_move_to}</small></td>
					<td>" . $form->generate_forum_select('move_1_forum', $this->bb->input['move_1_forum']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->leave_redirect}</small></td>
					<td>" . $form->generate_yes_no_radio('move_2_redirect', $this->bb->input['move_2_redirect'], ['style' => 'width: 2em;']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->delete_redirect_after}</small></td>
					<td>" . $form->generate_numeric_field('move_3_redirecttime', $this->bb->input['move_3_redirecttime'], ['style' => 'width: 3em;', 'min' => 0]) . " {$this->lang->days}</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('move');
	</script>";
            $form_container->output_row($this->lang->move_thread . ' <em>*</em>', $this->lang->move_thread_desc, $actions);

            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"1\" {$copy_checked[1]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_copy_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"2\" {$copy_checked[2]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->copy_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"copy_2\" class=\"copys\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_copy_to}</small></td>
					<td>" . $form->generate_forum_select('copy_1_forum', $this->bb->input['copy_1_forum']) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('copy');
	</script>";
            $form_container->output_row($this->lang->copy_thread . ' <em>*</em>', '', $actions);

            $softdelete_restore = [
                '' => $this->lang->no_change,
                'restore' => $this->lang->restore,
                'softdelete' => $this->lang->softdelete,
                'toggle' => $this->lang->toggle
            ];

            $form_container->output_row($this->lang->softdelete_restore_thread . ' <em>*</em>', '', $form->generate_select_box('softdeletethread', $softdelete_restore, $this->bb->input['softdeletethread'], ['id' => 'softdeletethread']), 'softdeletethread');
            $form_container->output_row($this->lang->delete_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('deletethread', $this->bb->input['deletethread'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->merge_thread . ' <em>*</em>', $this->lang->merge_thread_desc, $form->generate_yes_no_radio('mergethreads', $this->bb->input['mergethreads'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->delete_poll . ' <em>*</em>', '', $form->generate_yes_no_radio('deletepoll', $this->bb->input['deletepoll'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->delete_redirects . ' <em>*</em>', '', $form->generate_yes_no_radio('removeredirects', $this->bb->input['removeredirects'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->remove_subscriptions . ' <em>*</em>', '', $form->generate_yes_no_radio('removesubscriptions', $this->bb->input['removesubscriptions'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->recount_rebuild . ' <em>*</em>', '', $form->generate_yes_no_radio('recountrebuild', $this->bb->input['recountrebuild'], ['style' => 'width: 2em;']));

            $threadprefixes = $this->thread->build_prefixes();
            if (!empty($threadprefixes)) {
                $thread_prefixes = [
                    '-1' => $this->lang->no_change,
                    '0' => $this->lang->no_prefix
                ];

                foreach ($threadprefixes as $prefix) {
                    $thread_prefixes[$prefix['pid']] = $prefix['prefix'];
                }

                $form_container->output_row($this->lang->apply_thread_prefix . ' <em>*</em>', '', $form->generate_select_box('threadprefix', $thread_prefixes, [$this->bb->getInput('threadprefix', 0)], ['id' => 'threadprefix']), 'threadprefix');
            }

            $form_container->output_row($this->lang->new_subject . ' <em>*</em>', $this->lang->new_subject_desc, $form->generate_text_box('newsubject', $this->bb->input['newsubject'], ['id' => 'newsubject']));
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->add_new_reply);
            $form_container->output_row($this->lang->add_new_reply, $this->lang->add_new_reply_desc, $form->generate_text_area('newreply', $this->bb->input['newreply'], ['id' => 'newreply']), 'newreply');
            $form_container->output_row($this->lang->reply_subject, $this->lang->reply_subject_desc, $form->generate_text_box('newreplysubject', $this->bb->input['newreplysubject'], ['id' => 'newreplysubject']), 'newreplysubject');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->send_private_message);
            $form_container->output_row($this->lang->private_message_message, $this->lang->private_message_message_desc, $form->generate_text_area('pm_message', $this->bb->input['pm_message'], ['id' => 'pm_message']), 'pm_message');
            $form_container->output_row($this->lang->private_message_subject, $this->lang->private_message_subject_desc, $form->generate_text_box('pm_subject', $this->bb->input['pm_subject'], ['id' => 'pm_subject']), 'pm_subject');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_thread_tool);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ModToolsThreadToolEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'add_thread_tool') {
            $this->plugins->runHooks('admin_config_mod_tools_add_thread_tool');

            if ($this->bb->request_method == 'post') {
                if (trim($this->bb->input['title']) == '') {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (trim($this->bb->input['description']) == '') {
                    $errors[] = $this->lang->error_missing_description;
                }

                if ($this->bb->input['forum_type'] == 2) {
                    $forum_checked[1] = '';
                    $forum_checked[2] = 'checked="checked"';

                    if (count($this->bb->input['forum_1_forums']) < 1) {
                        $errors[] = $this->lang->error_no_forums_selected;
                    }
                } else {
                    $forum_checked[1] = 'checked="checked"';
                    $forum_checked[2] = '';

                    $this->bb->input['forum_1_forums'] = '';
                }

                if ($this->bb->input['group_type'] == 2) {
                    $group_checked[1] = '';
                    $group_checked[2] = 'checked="checked"';

                    if (count($this->bb->input['group_1_groups']) < 1) {
                        $errors[] = $this->lang->error_no_groups_selected;
                    }
                } else {
                    $group_checked[1] = 'checked="checked"';
                    $group_checked[2] = '';

                    $this->bb->input['group_1_groups'] = '';
                }

                if ($this->bb->input['approvethread'] != '' &&
                    $this->bb->input['approvethread'] != 'approve' &&
                    $this->bb->input['approvethread'] != 'unapprove' &&
                    $this->bb->input['approvethread'] != 'toggle'
                ) {
                    $this->bb->input['approvethread'] = '';
                }

                if ($this->bb->input['softdeletethread'] != '' &&
                    $this->bb->input['softdeletethread'] != 'restore' &&
                    $this->bb->input['softdeletethread'] != 'softdelete' &&
                    $this->bb->input['softdeletethread'] != 'toggle'
                ) {
                    $this->bb->input['softdeletethread'] = '';
                }

                if ($this->bb->input['openthread'] != '' &&
                    $this->bb->input['openthread'] != 'open' &&
                    $this->bb->input['openthread'] != 'close' &&
                    $this->bb->input['openthread'] != 'toggle'
                ) {
                    $this->bb->input['openthread'] = '';
                }

                if ($this->bb->input['stickthread'] != '' &&
                    $this->bb->input['stickthread'] != 'stick' &&
                    $this->bb->input['stickthread'] != 'unstick' &&
                    $this->bb->input['stickthread'] != 'toggle'
                ) {
                    $this->bb->input['stickthread'] = '';
                }

                if (!$this->bb->getInput('threadprefix', 0)) {
                    $this->bb->input['threadprefix'] = '';
                }

                if ($this->bb->input['move_type'] == 2) {
                    $move_checked[1] = '';
                    $move_checked[2] = 'checked="checked"';

                    if (!$this->bb->input['move_1_forum']) {
                        $errors[] = $this->lang->error_no_move_forum_selected;
                    } else {
                        // Check that the destination forum is not a category
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('move_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $move_checked[1] = 'checked="checked"';
                    $move_checked[2] = '';

                    $this->bb->input['move_1_forum'] = '';
                    $this->bb->input['move_2_redirect'] = 0;
                    $this->bb->input['move_3_redirecttime'] = '';
                }

                if ($this->bb->input['copy_type'] == 2) {
                    $copy_checked[1] = '';
                    $copy_checked[2] = 'checked="checked"';

                    if (!$this->bb->input['copy_1_forum']) {
                        $errors[] = $this->lang->error_no_copy_forum_selected;
                    } else {
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('copy_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $copy_checked[1] = 'checked="checked"';
                    $copy_checked[2] = '';

                    $this->bb->input['copy_1_forum'] = '';
                }

                if (!isset($errors)) {
                    $thread_options = [
                        'confirmation' => $this->bb->getInput('confirmation', 0),
                        'deletethread' => $this->bb->getInput('deletethread', 0),
                        'mergethreads' => $this->bb->getInput('mergethreads', 0),
                        'deletepoll' => $this->bb->getInput('deletepoll', 0),
                        'removeredirects' => $this->bb->getInput('removeredirects', 0),
                        'removesubscriptions' => $this->bb->getInput('removesubscriptions', 0),
                        'recountrebuild' => $this->bb->getInput('recountrebuild', 0),
                        'approvethread' => $this->bb->input['approvethread'],
                        'softdeletethread' => $this->bb->input['softdeletethread'],
                        'openthread' => $this->bb->input['openthread'],
                        'stickthread' => $this->bb->input['stickthread'],
                        'movethread' => $this->bb->getInput('move_1_forum', 0),
                        'movethreadredirect' => $this->bb->getInput('move_2_redirect', 0),
                        'movethreadredirectexpire' => $this->bb->getInput('move_3_redirecttime', 0),
                        'copythread' => $this->bb->getInput('copy_1_forum', 0),
                        'newsubject' => $this->bb->input['newsubject'],
                        'addreply' => $this->bb->input['newreply'],
                        'replysubject' => $this->bb->input['newreplysubject'],
                        'pm_subject' => $this->bb->input['pm_subject'],
                        'pm_message' => $this->bb->input['pm_message'],
                        'threadprefix' => $this->bb->input['threadprefix'],
                    ];

                    $new_tool['type'] = 't';
                    $new_tool['threadoptions'] = $this->db->escape_string(my_serialize($thread_options));
                    $new_tool['name'] = $this->db->escape_string($this->bb->input['title']);
                    $new_tool['description'] = $this->db->escape_string($this->bb->input['description']);
                    $new_tool['forums'] = '';
                    $new_tool['groups'] = '';
                    $new_tool['postoptions'] = '';

                    if ($this->bb->input['forum_type'] == 2) {
                        if (is_array($this->bb->input['forum_1_forums'])) {
                            $checked = [];

                            foreach ($this->bb->input['forum_1_forums'] as $fid) {
                                $checked[] = (int)$fid;
                            }

                            $new_tool['forums'] = implode(',', $checked);
                        }
                    } else {
                        $new_tool['forums'] = '-1';
                    }

                    if ($this->bb->input['group_type'] == 2) {
                        if (is_array($this->bb->input['group_1_groups'])) {
                            $checked = [];

                            foreach ($this->bb->input['group_1_groups'] as $gid) {
                                $checked[] = (int)$gid;
                            }

                            $new_tool['groups'] = implode(',', $checked);
                        }
                    } else {
                        $new_tool['groups'] = '-1';
                    }

                    if ($this->bb->getInput('threadprefix', 0) >= 0) {
                        $thread_options['threadprefix'] = $this->bb->getInput('threadprefix', 0);
                    }

                    $tid = $this->db->insert_query('modtools', $new_tool);

                    $this->plugins->runHooks('admin_config_mod_tools_add_thread_tool_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($tid, $this->bb->input['title']);
                    $this->cache->update_forumsdisplay();

                    $this->session->flash_message($this->lang->success_mod_tool_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/mod_tools');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_thread_tool);
            $html = $this->page->output_header($this->lang->mod_tools . ' - ' . $this->lang->add_new_thread_tool);

            $sub_tabs['thread_tools'] = [
                'title' => $this->lang->thread_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools'
            ];
            $sub_tabs['add_thread_tool'] = [
                'title' => $this->lang->add_new_thread_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_thread_tool',
                'description' => $this->lang->add_thread_tool_desc
            ];
            $sub_tabs['post_tools'] = [
                'title' => $this->lang->post_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=post_tools',
            ];
            $sub_tabs['add_post_tool'] = [
                'title' => $this->lang->add_new_post_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_post_tool'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_thread_tool');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/mod_tools?action=add_thread_tool', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['title'] = '';
                $this->bb->input['description'] = '';
                $this->bb->input['forum_1_forums'] = '';
                $forum_checked[1] = 'checked="checked"';
                $forum_checked[2] = '';
                $this->bb->input['group_1_groups'] = '';
                $group_checked[1] = 'checked="checked"';
                $group_checked[2] = '';
                $this->bb->input['confirmation'] = '0';
                $this->bb->input['approvethread'] = '';
                $this->bb->input['softdeletethread'] = '';
                $this->bb->input['openthread'] = '';
                $this->bb->input['stickthread'] = '';
                $this->bb->input['move_1_forum'] = '';
                $this->bb->input['move_2_redirect'] = '0';
                $this->bb->input['move_3_redirecttime'] = '';
                $move_checked[1] = 'checked="checked"';
                $move_checked[2] = '';
                $copy_checked[1] = 'checked="checked"';
                $copy_checked[2] = '';
                $this->bb->input['copy_1_forum'] = '';
                $this->bb->input['deletethread'] = '0';
                $this->bb->input['mergethreads'] = '0';
                $this->bb->input['deletepoll'] = '0';
                $this->bb->input['removeredirects'] = '0';
                $this->bb->input['removesubscriptions'] = '0';
                $this->bb->input['recountrebuild'] = '0';
                $this->bb->input['threadprefix'] = '-1';
                $this->bb->input['newsubject'] = '{subject}';
                $this->bb->input['newreply'] = '';
                $this->bb->input['newreplysubject'] = '{subject}';
                $this->bb->input['pm_subject'] = '';
                $this->bb->input['pm_message'] = '';
            }

            $form_container = new FormContainer($this, $this->lang->general_options);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"1\" {$forum_checked[1]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"2\" {$forum_checked[2]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"forum_2\" class=\"forums\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
					<td>" . $form->generate_forum_select('forum_1_forums[]', $this->bb->input['forum_1_forums'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('forum');
	</script>";
            $form_container->output_row($this->lang->available_in_forums . ' <em>*</em>', '', $actions);

            $actions = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"1\" {$group_checked[1]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"2\" {$group_checked[2]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"group_2\" class=\"groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('group_1_groups[]', $this->bb->input['group_1_groups'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('group');
	</script>";
            $form_container->output_row($this->lang->available_to_groups . ' <em>*</em>', '', $actions);
            $form_container->output_row($this->lang->show_confirmation . ' <em>*</em>', '', $form->generate_yes_no_radio('confirmation', $this->bb->input['confirmation'], ['style' => 'width: 2em;']));
            $html .= $form_container->end();

            $approve_unapprove = [
                '' => $this->lang->no_change,
                'approve' => $this->lang->approve,
                'unapprove' => $this->lang->unapprove,
                'toggle' => $this->lang->toggle
            ];

            $open_close = [
                '' => $this->lang->no_change,
                'open' => $this->lang->open,
                'close' => $this->lang->close,
                'toggle' => $this->lang->toggle
            ];

            $stick_unstick = [
                '' => $this->lang->no_change,
                'stick' => $this->lang->stick,
                'unstick' => $this->lang->unstick,
                'toggle' => $this->lang->toggle
            ];

            $form_container = new FormContainer($this, $this->lang->thread_moderation);
            $form_container->output_row($this->lang->approve_unapprove . ' <em>*</em>', '', $form->generate_select_box('approvethread', $approve_unapprove, $this->bb->input['approvethread'], ['id' => 'approvethread']), 'approvethread');
            $form_container->output_row($this->lang->open_close_thread . ' <em>*</em>', '', $form->generate_select_box('openthread', $open_close, $this->bb->input['openthread'], ['id' => 'openthread']), 'openthread');
            $form_container->output_row($this->lang->stick_unstick_thread . ' <em>*</em>', '', $form->generate_select_box('stickthread', $stick_unstick, $this->bb->input['stickthread'], ['id' => 'stickthread']), 'stickthread');


            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"1\" {$move_checked[1]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_move_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"2\" {$move_checked[2]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->move_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"move_2\" class=\"moves\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_move_to}</small></td>
					<td>" . $form->generate_forum_select('move_1_forum', $this->bb->input['move_1_forum']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->leave_redirect}</small></td>
					<td>" . $form->generate_yes_no_radio('move_2_redirect', $this->bb->input['move_2_redirect'], ['style' => 'width: 2em;']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->delete_redirect_after}</small></td>
					<td>" . $form->generate_numeric_field('move_3_redirecttime', $this->bb->input['move_3_redirecttime'], ['style' => 'width: 3em;', 'min' => 0]) . " {$this->lang->days}</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('move');
	</script>";
            $form_container->output_row($this->lang->move_thread . " <em>*</em>", $this->lang->move_thread_desc, $actions);

            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"1\" {$copy_checked[1]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_copy_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"2\" {$copy_checked[2]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->copy_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"copy_2\" class=\"copys\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_copy_to}</small></td>
					<td>" . $form->generate_forum_select('copy_1_forum', $this->bb->input['copy_1_forum']) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('copy');
	</script>";
            $form_container->output_row($this->lang->copy_thread . ' <em>*</em>', '', $actions);

            $softdelete_restore = [
                '' => $this->lang->no_change,
                'restore' => $this->lang->restore,
                'softdelete' => $this->lang->softdelete,
                'toggle' => $this->lang->toggle
            ];

            $form_container->output_row($this->lang->softdelete_restore_thread . ' <em>*</em>', '', $form->generate_select_box('softdeletethread', $softdelete_restore, $this->bb->input['softdeletethread'], ['id' => 'softdeletethread']), 'softdeletethread');
            $form_container->output_row($this->lang->delete_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('deletethread', $this->bb->input['deletethread'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->merge_thread . ' <em>*</em>', $this->lang->merge_thread_desc, $form->generate_yes_no_radio('mergethreads', $this->bb->input['mergethreads'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->delete_poll . ' <em>*</em>', '', $form->generate_yes_no_radio('deletepoll', $this->bb->input['deletepoll'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->delete_redirects . ' <em>*</em>', '', $form->generate_yes_no_radio('removeredirects', $this->bb->input['removeredirects'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->remove_subscriptions . ' <em>*</em>', '', $form->generate_yes_no_radio('removesubscriptions', $this->bb->input['removesubscriptions'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->recount_rebuild . ' <em>*</em>', '', $form->generate_yes_no_radio('recountrebuild', $this->bb->input['recountrebuild'], ['style' => 'width: 2em;']));

            $threadprefixes = $this->thread->build_prefixes();
            if (!empty($threadprefixes)) {
                $thread_prefixes = [
                    '-1' => $this->lang->no_change,
                    '0' => $this->lang->no_prefix
                ];

                foreach ($threadprefixes as $prefix) {
                    $thread_prefixes[$prefix['pid']] = $prefix['prefix'];
                }

                $form_container->output_row($this->lang->apply_thread_prefix . ' <em>*</em>', '', $form->generate_select_box('threadprefix', $thread_prefixes, $this->bb->input['threadprefix'], ['id' => 'threadprefix']), 'threadprefix');
            }

            $form_container->output_row($this->lang->new_subject . ' <em>*</em>', $this->lang->new_subject_desc, $form->generate_text_box('newsubject', $this->bb->input['newsubject'], ['id' => 'newsubject']));
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->add_new_reply);
            $form_container->output_row($this->lang->add_new_reply, $this->lang->add_new_reply_desc, $form->generate_text_area('newreply', $this->bb->input['newreply'], ['id' => 'newreply']), 'newreply');
            $form_container->output_row($this->lang->reply_subject, $this->lang->reply_subject_desc, $form->generate_text_box('newreplysubject', $this->bb->input['newreplysubject'], ['id' => 'newreplysubject']), 'newreplysubject');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->send_private_message);
            $form_container->output_row($this->lang->private_message_message, $this->lang->private_message_message_desc, $form->generate_text_area('pm_message', $this->bb->input['pm_message'], ['id' => 'pm_message']), 'pm_message');
            $form_container->output_row($this->lang->private_message_subject, $this->lang->private_message_subject_desc, $form->generate_text_box('pm_subject', $this->bb->input['pm_subject'], ['id' => 'pm_subject']), 'pm_subject');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_thread_tool);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ModToolsThreadToolAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_post_tool') {
            $query = $this->db->simple_select('modtools', 'COUNT(tid) as tools', "tid = '{$this->bb->input['tid']}' AND type='p'");
            if ($this->db->fetch_field($query, 'tools') < 1) {
                $this->session->flash_message($this->lang->error_invalid_post_tool, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mod_tools?action=post_tools');
            }

            $this->plugins->runHooks('admin_config_mod_tools_edit_post_tool');

            if ($this->bb->request_method == 'post') {
                if (trim($this->bb->input['title']) == '') {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (trim($this->bb->input['description']) == '') {
                    $errors[] = $this->lang->error_missing_description;
                }

                if ($this->bb->input['forum_type'] == 2) {
                    if (count($this->bb->input['forum_1_forums']) < 1) {
                        $errors[] = $this->lang->error_no_forums_selected;
                    }
                } else {
                    $this->bb->input['forum_1_forums'] = '';
                }

                if ($this->bb->input['group_type'] == 2) {
                    if (count($this->bb->input['group_1_groups']) < 1) {
                        $errors[] = $this->lang->error_no_groups_selected;
                    }
                } else {
                    $this->bb->input['group_1_groups'] = '';
                }

                if ($this->bb->input['approvethread'] != '' &&
                    $this->bb->input['approvethread'] != 'approve' &&
                    $this->bb->input['approvethread'] != 'unapprove' &&
                    $this->bb->input['approvethread'] != 'toggle'
                ) {
                    $this->bb->input['approvethread'] = '';
                }

                if ($this->bb->input['softdeletethread'] != '' &&
                    $this->bb->input['softdeletethread'] != 'softdelete' &&
                    $this->bb->input['softdeletethread'] != 'restore' &&
                    $this->bb->input['softdeletethread'] != 'toggle'
                ) {
                    $this->bb->input['softdeletethread'] = '';
                }

                if ($this->bb->input['openthread'] != '' &&
                    $this->bb->input['openthread'] != 'open' &&
                    $this->bb->input['openthread'] != 'close' &&
                    $this->bb->input['openthread'] != 'toggle'
                ) {
                    $this->bb->input['openthread'] = '';
                }

                if ($this->bb->input['stickthread'] != '' &&
                    $this->bb->input['stickthread'] != 'stick' &&
                    $this->bb->input['stickthread'] != 'unstick' &&
                    $this->bb->input['stickthread'] != 'toggle'
                ) {
                    $this->bb->input['stickthread'] = '';
                }

                if ($this->bb->input['move_type'] == 2) {
                    if (!$this->bb->input['move_1_forum']) {
                        $errors[] = $this->lang->error_no_move_forum_selected;
                    } else {
                        // Check that the destination forum is not a category
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('move_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $this->bb->input['move_1_forum'] = '';
                    $this->bb->input['move_2_redirect'] = 0;
                    $this->bb->input['move_3_redirecttime'] = '';
                }

                if ($this->bb->input['copy_type'] == 2) {
                    if (!$this->bb->input['copy_1_forum']) {
                        $errors[] = $this->lang->error_no_copy_forum_selected;
                    } else {
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('copy_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $this->bb->input['copy_1_forum'] = '';
                }

                if ($this->bb->input['approveposts'] != '' &&
                    $this->bb->input['approveposts'] != 'approve' &&
                    $this->bb->input['approveposts'] != 'unapprove' &&
                    $this->bb->input['approveposts'] != 'toggle'
                ) {
                    $this->bb->input['approveposts'] = '';
                }

                if ($this->bb->input['softdeleteposts'] != '' &&
                    $this->bb->input['softdeleteposts'] != 'approve' &&
                    $this->bb->input['softdeleteposts'] != 'unapprove' &&
                    $this->bb->input['softdeleteposts'] != 'toggle'
                ) {
                    $this->bb->input['softdeleteposts'] = '';
                }

                if ($this->bb->input['splitposts'] < -2) {
                    $this->bb->input['splitposts'] = -1;
                }

                if ($this->bb->input['splitpostsclose'] == 1) {
                    $this->bb->input['splitpostsclose'] = 'close';
                } else {
                    $this->bb->input['splitpostsclose'] = '';
                }

                if ($this->bb->input['splitpostsstick'] == 1) {
                    $this->bb->input['splitpostsstick'] = 'stick';
                } else {
                    $this->bb->input['splitpostsstick'] = '';
                }

                if ($this->bb->input['splitpostsunapprove'] == 1) {
                    $this->bb->input['splitpostsunapprove'] = 'unapprove';
                } else {
                    $this->bb->input['splitpostsunapprove'] = '';
                }

                if (!isset($errors)) {
                    $thread_options = [
                        'confirmation' => $this->bb->getInput('confirmation', 0),
                        'deletethread' => $this->bb->getInput('deletethread', 0),
                        'softdeletethread' => $this->bb->input['softdeletethread'],
                        'approvethread' => $this->bb->input['approvethread'],
                        'openthread' => $this->bb->input['openthread'],
                        'stickthread' => $this->bb->input['stickthread'],
                        'movethread' => $this->bb->getInput('move_1_forum', 0),
                        'movethreadredirect' => $this->bb->getInput('move_2_redirect', 0),
                        'movethreadredirectexpire' => $this->bb->getInput('move_3_redirecttime', 0),
                        'copythread' => $this->bb->getInput('copy_1_forum', 0),
                        'newsubject' => $this->bb->input['newsubject'],
                        'addreply' => $this->bb->input['newreply'],
                        'replysubject' => $this->bb->input['newreplysubject'],
                        'pm_subject' => $this->bb->input['pm_subject'],
                        'pm_message' => $this->bb->input['pm_message'],
                        'threadprefix' => $this->bb->getInput('threadprefix', 0)
                    ];

                    if (stripos($this->bb->input['splitpostsnewsubject'], '{subject}') === false) {
                        $this->bb->input['splitpostsnewsubject'] = '{subject}' . $this->bb->input['splitpostsnewsubject'];
                    }

                    $post_options = [
                        'deleteposts' => $this->bb->getInput('deleteposts', 0),
                        'softdeleteposts' => $this->bb->input['softdeleteposts'],
                        'mergeposts' => $this->bb->getInput('mergeposts', 0),
                        'approveposts' => $this->bb->input['approveposts'],
                        'splitposts' => $this->bb->getInput('splitposts', 0),
                        'splitpostsclose' => $this->bb->input['splitpostsclose'],
                        'splitpostsstick' => $this->bb->input['splitpostsstick'],
                        'splitpostsunapprove' => $this->bb->input['splitpostsunapprove'],
                        'splitthreadprefix' => $this->bb->getInput('splitthreadprefix', 0),
                        'splitpostsnewsubject' => $this->bb->input['splitpostsnewsubject'],
                        'splitpostsaddreply' => $this->bb->input['splitpostsaddreply'],
                        'splitpostsreplysubject' => $this->bb->input['splitpostsreplysubject']
                    ];

                    $update_tool['type'] = 'p';
                    $update_tool['threadoptions'] = $this->db->escape_string(my_serialize($thread_options));
                    $update_tool['postoptions'] = $this->db->escape_string(my_serialize($post_options));
                    $update_tool['name'] = $this->db->escape_string($this->bb->input['title']);
                    $update_tool['description'] = $this->db->escape_string($this->bb->input['description']);
                    $update_tool['forums'] = '';
                    $update_tool['groups'] = '';

                    if ($this->bb->input['forum_type'] == 2) {
                        if (is_array($this->bb->input['forum_1_forums'])) {
                            $checked = [];

                            foreach ($this->bb->input['forum_1_forums'] as $fid) {
                                $checked[] = (int)$fid;
                            }

                            $update_tool['forums'] = implode(',', $checked);
                        }
                    } else {
                        $update_tool['forums'] = '-1';
                    }

                    if ($this->bb->input['group_type'] == 2) {
                        if (is_array($this->bb->input['group_1_groups'])) {
                            $checked = [];

                            foreach ($this->bb->input['group_1_groups'] as $gid) {
                                $checked[] = (int)$gid;
                            }

                            $update_tool['groups'] = implode(',', $checked);
                        }
                    } else {
                        $update_tool['groups'] = '-1';
                    }

                    $this->plugins->runHooks('admin_config_mod_tools_edit_post_tool_commit');

                    $this->db->update_query('modtools', $update_tool, "tid = '{$this->bb->input['tid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($this->bb->input['tid'], $this->bb->input['title']);
                    $this->cache->update_forumsdisplay();

                    $this->session->flash_message($this->lang->success_mod_tool_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/mod_tools?action=post_tools');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_post_tool);
            $html = $this->page->output_header($this->lang->mod_tools . ' - ' . $this->lang->edit_post_tool);

            $sub_tabs['edit_post_tool'] = [
                'title' => $this->lang->edit_post_tool,
                'description' => $this->lang->edit_post_tool_desc,
                'link' => $this->bb->admin_url . '/config/mod_tools'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_post_tool');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/mod_tools?action=edit_post_tool', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $this->bb->input['tid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $query = $this->db->simple_select('modtools', '*', "tid = '{$this->bb->input['tid']}'");
                $modtool = $this->db->fetch_array($query);
                $thread_options = my_unserialize($modtool['threadoptions']);
                $post_options = my_unserialize($modtool['postoptions']);

                $this->bb->input['title'] = $modtool['name'];
                $this->bb->input['description'] = $modtool['description'];
                $this->bb->input['forum_1_forums'] = explode(',', $modtool['forums']);
                $this->bb->input['group_1_groups'] = explode(',', $modtool['groups']);

                if (!$modtool['forums'] || $modtool['forums'] == -1) {
                    $forum_checked[1] = 'checked="checked"';
                    $forum_checked[2] = '';
                } else {
                    $forum_checked[1] = '';
                    $forum_checked[2] = 'checked="checked"';
                }

                if (!$modtool['groups'] || $modtool['groups'] == -1) {
                    $group_checked[1] = 'checked="checked"';
                    $group_checked[2] = '';
                } else {
                    $group_checked[1] = '';
                    $group_checked[2] = 'checked="checked"';
                }

                $this->bb->input['confirmation'] = $thread_options['confirmation'];
                $this->bb->input['approvethread'] = $thread_options['approvethread'];
                $this->bb->input['softdeletethread'] = $thread_options['softdeletethread'];
                $this->bb->input['openthread'] = $thread_options['openthread'];
                $this->bb->input['stickthread'] = $thread_options['stickthread'];
                $this->bb->input['move_1_forum'] = $thread_options['movethread'];
                $this->bb->input['move_2_redirect'] = $thread_options['movethreadredirect'];
                $this->bb->input['move_3_redirecttime'] = $thread_options['movethreadredirectexpire'];

                if (!$thread_options['movethread']) {
                    $move_checked[1] = 'checked="checked"';
                    $move_checked[2] = '';
                } else {
                    $move_checked[1] = '';
                    $move_checked[2] = 'checked="checked"';
                }

                if (!$thread_options['copythread']) {
                    $copy_checked[1] = 'checked="checked"';
                    $copy_checked[2] = '';
                } else {
                    $copy_checked[1] = '';
                    $copy_checked[2] = 'checked="checked"';
                }

                $this->bb->input['copy_1_forum'] = $thread_options['copythread'];
                $this->bb->input['deletethread'] = $thread_options['deletethread'];
                $this->bb->input['threadprefix'] = $thread_options['threadprefix'];
                $this->bb->input['newsubject'] = $thread_options['newsubject'];
                $this->bb->input['newreply'] = $thread_options['addreply'];
                $this->bb->input['newreplysubject'] = $thread_options['replysubject'];
                $this->bb->input['pm_subject'] = $thread_options['pm_subject'];
                $this->bb->input['pm_message'] = $thread_options['pm_message'];

                if ($post_options['splitposts'] == '-1') {
                    $do_not_split_checked = ' selected="selected"';
                    $split_same_checked = '';
                } elseif ($post_options['splitposts'] == '-2') {
                    $do_not_split_checked = '';
                    $split_same_checked = ' selected="selected"';
                }

                $this->bb->input['softdeleteposts'] = $post_options['softdeleteposts'];
                $this->bb->input['deleteposts'] = $post_options['deleteposts'];
                $this->bb->input['mergeposts'] = $post_options['mergeposts'];
                $this->bb->input['approveposts'] = $post_options['approveposts'];

                if ($post_options['splitpostsclose'] == 'close') {
                    $this->bb->input['splitpostsclose'] = '1';
                } else {
                    $this->bb->input['splitpostsclose'] = '0';
                }

                if ($post_options['splitpostsstick'] == 'stick') {
                    $this->bb->input['splitpostsstick'] = '1';
                } else {
                    $this->bb->input['splitpostsstick'] = '0';
                }

                if ($post_options['splitpostsunapprove'] == 'unapprove') {
                    $this->bb->input['splitpostsunapprove'] = '1';
                } else {
                    $this->bb->input['splitpostsunapprove'] = '0';
                }

                $this->bb->input['splitposts'] = $post_options['splitposts'];
                $this->bb->input['splitthreadprefix'] = $post_options['splitthreadprefix'];
                $this->bb->input['splitpostsnewsubject'] = $post_options['splitpostsnewsubject'];
                $this->bb->input['splitpostsaddreply'] = $post_options['splitpostsaddreply'];
                $this->bb->input['splitpostsreplysubject'] = $post_options['splitpostsreplysubject'];
            }

            $form_container = new FormContainer($this, $this->lang->general_options);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"1\" {$forum_checked[1]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"2\" {$forum_checked[2]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"forum_2\" class=\"forums\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
					<td>" . $form->generate_forum_select('forum_1_forums[]', $this->bb->input['forum_1_forums'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('forum');
	</script>";
            $form_container->output_row($this->lang->available_in_forums . ' <em>*</em>', '', $actions);

            $actions = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"1\" {$group_checked[1]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"2\" {$group_checked[2]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"group_2\" class=\"groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('group_1_groups[]', $this->bb->input['group_1_groups'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('group');
	</script>";
            $form_container->output_row($this->lang->available_to_groups . ' <em>*</em>', '', $actions);
            $form_container->output_row($this->lang->show_confirmation . ' <em>*</em>', '', $form->generate_yes_no_radio('confirmation', $this->bb->input['confirmation'], ['style' => 'width: 2em;']));
            $html .= $form_container->end();

            $approve_unapprove = [
                '' => $this->lang->no_change,
                'approve' => $this->lang->approve,
                'unapprove' => $this->lang->unapprove,
                'toggle' => $this->lang->toggle
            ];

            $form_container = new FormContainer($this, $this->lang->inline_post_moderation);

            $softdelete_restore = [
                '' => $this->lang->no_change,
                'restore' => $this->lang->restore,
                'softdelete' => $this->lang->softdelete,
                'toggle' => $this->lang->toggle
            ];

            $form_container->output_row($this->lang->softdelete_restore_posts . ' <em>*</em>', '', $form->generate_select_box('softdeleteposts', $softdelete_restore, $this->bb->input['softdeleteposts'], ['id' => 'softdeleteposts']), 'softdeleteposts');
            $form_container->output_row($this->lang->delete_posts . ' <em>*</em>', '', $form->generate_yes_no_radio('deleteposts', $this->bb->input['deleteposts']));
            $form_container->output_row($this->lang->merge_posts . ' <em>*</em>', $this->lang->merge_posts_desc, $form->generate_yes_no_radio('mergeposts', $this->bb->input['mergeposts']));
            $form_container->output_row($this->lang->approve_unapprove_posts . ' <em>*</em>', '', $form->generate_select_box('approveposts', $approve_unapprove, $this->bb->input['approveposts'], ['id' => 'approveposts']), 'approveposts');
            $html .= $form_container->end();

            $this->bb->selectoptions = "<option value=\"-1\"{$do_not_split_checked}>{$this->lang->do_not_split}</option>\n";
            $this->bb->selectoptions .= "<option value=\"-2\"{$split_same_checked} style=\"border-bottom: 1px solid #000;\">{$this->lang->split_to_same_forum}</option>\n";

            $form_container = new FormContainer($this, $this->lang->split_posts);
            $form_container->output_row($this->lang->split_posts2 . ' <em>*</em>', '', $form->generate_forum_select('splitposts', $this->bb->input['splitposts']));
            $form_container->output_row($this->lang->close_split_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('splitpostsclose', $this->bb->input['splitpostsclose']));
            $form_container->output_row($this->lang->stick_split_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('splitpostsstick', $this->bb->input['splitpostsstick']));
            $form_container->output_row($this->lang->unapprove_split_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('splitpostsunapprove', $this->bb->input['splitpostsunapprove']));

            $splitthreadprefix = $this->thread->build_prefixes();
            if (!empty($splitthreadprefix)) {
                $split_thread_prefixes = [
                    '0' => $this->lang->no_prefix
                ];

                foreach ($splitthreadprefix as $prefix) {
                    $split_thread_prefixes[$prefix['pid']] = $prefix['prefix'];
                }

                $form_container->output_row($this->lang->split_thread_prefix . ' <em>*</em>', '', $form->generate_select_box('splitthreadprefix', $split_thread_prefixes, [$this->bb->getInput('splitthreadprefix', 0)], ['id' => 'splitthreadprefix']), 'splitthreadprefix');
            }

            $form_container->output_row($this->lang->split_thread_subject, $this->lang->split_thread_subject_desc, $form->generate_text_box('splitpostsnewsubject', $this->bb->input['splitpostsnewsubject'], ['id' => 'splitpostsnewsubject ']), 'newreplysubject');
            $form_container->output_row($this->lang->add_new_split_reply, $this->lang->add_new_split_reply_desc, $form->generate_text_area('splitpostsaddreply', $this->bb->input['splitpostsaddreply'], ['id' => 'splitpostsaddreply']), 'splitpostsaddreply');
            $form_container->output_row($this->lang->split_reply_subject, $this->lang->split_reply_subject_desc, $form->generate_text_box('splitpostsreplysubject', $this->bb->input['splitpostsreplysubject'], ['id' => 'splitpostsreplysubject']), 'splitpostsreplysubject');
            $html .= $form_container->end();

            $open_close = [
                '' => $this->lang->no_change,
                'open' => $this->lang->open,
                'close' => $this->lang->close,
                'toggle' => $this->lang->toggle
            ];

            $stick_unstick = [
                '' => $this->lang->no_change,
                'stick' => $this->lang->stick,
                'unstick' => $this->lang->unstick,
                'toggle' => $this->lang->toggle
            ];

            $form_container = new FormContainer($this, $this->lang->thread_moderation);
            $form_container->output_row($this->lang->approve_unapprove . ' <em>*</em>', '', $form->generate_select_box('approvethread', $approve_unapprove, $this->bb->input['approvethread'], ['id' => 'approvethread']), 'approvethread');
            $form_container->output_row($this->lang->open_close_thread . ' <em>*</em>', '', $form->generate_select_box('openthread', $open_close, $this->bb->input['openthread'], ['id' => 'openthread']), 'openthread');
            $form_container->output_row($this->lang->stick_unstick_thread . ' <em>*</em>', '', $form->generate_select_box('stickthread', $stick_unstick, $this->bb->input['stickthread'], ['id' => 'stickthread']), 'stickthread');


            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"1\" {$move_checked[1]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_move_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"2\" {$move_checked[2]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->move_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"move_2\" class=\"moves\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_move_to}</small></td>
					<td>" . $form->generate_forum_select('move_1_forum', $this->bb->input['move_1_forum']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->leave_redirect}</small></td>
					<td>" . $form->generate_yes_no_radio('move_2_redirect', $this->bb->input['move_2_redirect']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->delete_redirect_after}</small></td>
					<td>" . $form->generate_numeric_field('move_3_redirecttime', $this->bb->input['move_3_redirecttime'], ['style' => 'width: 3em;', 'min' => 0]) . " {$this->lang->days}</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('move');
	</script>";
            $form_container->output_row($this->lang->move_thread . ' <em>*</em>', $this->lang->move_thread_desc, $actions);

            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"1\" {$copy_checked[1]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_copy_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"2\" {$copy_checked[2]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->copy_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"copy_2\" class=\"copys\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_copy_to}</small></td>
					<td>" . $form->generate_forum_select('copy_1_forum', $this->bb->input['copy_1_forum']) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('copy');
	</script>";
            $form_container->output_row($this->lang->copy_thread . ' <em>*</em>', '', $actions);
            $form_container->output_row($this->lang->softdelete_restore_thread . ' <em>*</em>', '', $form->generate_select_box('softdeletethread', $softdelete_restore, $this->bb->input['softdeletethread'], ['id' => 'softdeletethread']), 'softdeletethread');
            $form_container->output_row($this->lang->delete_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('deletethread', $this->bb->input['deletethread']));

            $threadprefixes = $this->thread->build_prefixes();
            if (!empty($threadprefixes)) {
                $thread_prefixes = [
                    '-1' => $this->lang->no_change,
                    '0' => $this->lang->no_prefix
                ];

                foreach ($threadprefixes as $prefix) {
                    $thread_prefixes[$prefix['pid']] = $prefix['prefix'];
                }

                $form_container->output_row($this->lang->apply_thread_prefix . ' <em>*</em>', '', $form->generate_select_box('threadprefix', $thread_prefixes, [$this->bb->getInput('threadprefix', 0)], ['id' => 'threadprefix']), 'threadprefix');
            }

            $form_container->output_row($this->lang->new_subject . ' <em>*</em>', $this->lang->new_subject_desc, $form->generate_text_box('newsubject', $this->bb->input['newsubject']));
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->add_new_reply);
            $form_container->output_row($this->lang->add_new_reply, $this->lang->add_new_reply_desc, $form->generate_text_area('newreply', $this->bb->input['newreply']), 'newreply');
            $form_container->output_row($this->lang->reply_subject, $this->lang->reply_subject_desc, $form->generate_text_box('newreplysubject', $this->bb->input['newreplysubject'], ['id' => 'newreplysubject']), 'newreplysubject');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->send_private_message);
            $form_container->output_row($this->lang->private_message_message, $this->lang->private_message_message_desc, $form->generate_text_area('pm_message', $this->bb->input['pm_message'], ['id' => 'pm_message']), 'pm_message');
            $form_container->output_row($this->lang->private_message_subject, $this->lang->private_message_subject_desc, $form->generate_text_box('pm_subject', $this->bb->input['pm_subject'], ['id' => 'pm_subject']), 'pm_subject');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_post_tool);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ModToolsPostToolEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'add_post_tool') {
            $this->plugins->runHooks('admin_config_mod_tools_add_post_tool');

            if ($this->bb->request_method == 'post') {
                if (trim($this->bb->input['title']) == '') {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (trim($this->bb->input['description']) == '') {
                    $errors[] = $this->lang->error_missing_description;
                }

                if ($this->bb->input['forum_type'] == 2) {
                    $forum_checked[1] = '';
                    $forum_checked[2] = 'checked="checked"';

                    if (count($this->bb->input['forum_1_forums']) < 1) {
                        $errors[] = $this->lang->error_no_forums_selected;
                    }
                } else {
                    $forum_checked[1] = 'checked="checked"';
                    $forum_checked[2] = '';

                    $this->bb->input['forum_1_forums'] = '';
                }

                if ($this->bb->input['group_type'] == 2) {
                    $group_checked[1] = '';
                    $group_checked[2] = 'checked="checked"';

                    if (count($this->bb->input['group_1_groups']) < 1) {
                        $errors[] = $this->lang->error_no_groups_selected;
                    }
                } else {
                    $group_checked[1] = 'checked="checked"';
                    $group_checked[2] = '';

                    $this->bb->input['group_1_groups'] = '';
                }

                if ($this->bb->input['approvethread'] != '' &&
                    $this->bb->input['approvethread'] != 'approve' &&
                    $this->bb->input['approvethread'] != 'unapprove' &&
                    $this->bb->input['approvethread'] != 'toggle'
                ) {
                    $this->bb->input['approvethread'] = '';
                }

                if ($this->bb->input['softdeletethread'] != '' &&
                    $this->bb->input['softdeletethread'] != 'softdelete' &&
                    $this->bb->input['softdeletethread'] != 'restore' &&
                    $this->bb->input['softdeletethread'] != 'toggle'
                ) {
                    $this->bb->input['softdeletethread'] = '';
                }

                if ($this->bb->input['openthread'] != '' &&
                    $this->bb->input['openthread'] != 'open' &&
                    $this->bb->input['openthread'] != 'close' &&
                    $this->bb->input['openthread'] != 'toggle'
                ) {
                    $this->bb->input['openthread'] = '';
                }

                if ($this->bb->input['stickthread'] != '' &&
                    $this->bb->input['stickthread'] != 'stick' &&
                    $this->bb->input['stickthread'] != 'unstick' &&
                    $this->bb->input['stickthread'] != 'toggle'
                ) {
                    $this->bb->input['stickthread'] = '';
                }

                if (!$this->bb->getInput('threadprefix', 0)) {
                    $this->bb->input['threadprefix'] = '';
                }

                if ($this->bb->input['move_type'] == 2) {
                    $move_checked[1] = '';
                    $move_checked[2] = 'checked="checked"';

                    if (!$this->bb->input['move_1_forum']) {
                        $errors[] = $this->lang->error_no_move_forum_selected;
                    } else {
                        // Check that the destination forum is not a category
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('move_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $move_checked[1] = 'checked="checked"';
                    $move_checked[2] = '';

                    $this->bb->input['move_1_forum'] = '';
                    $this->bb->input['move_2_redirect'] = 0;
                    $this->bb->input['move_3_redirecttime'] = '';
                }

                if ($this->bb->input['copy_type'] == 2) {
                    $copy_checked[1] = '';
                    $copy_checked[2] = 'checked="checked"';

                    if (!$this->bb->input['copy_1_forum']) {
                        $errors[] = $this->lang->error_no_copy_forum_selected;
                    } else {
                        $query = $this->db->simple_select('forums', 'type', "fid = '" . $this->bb->getInput('copy_1_forum', 0) . "'");
                        if ($this->db->fetch_field($query, 'type') == 'c') {
                            $errors[] = $this->lang->error_forum_is_category;
                        }
                    }
                } else {
                    $copy_checked[1] = 'checked="checked"';
                    $copy_checked[2] = '';

                    $this->bb->input['copy_1_forum'] = '';
                }

                if ($this->bb->input['approveposts'] != '' &&
                    $this->bb->input['approveposts'] != 'approve' &&
                    $this->bb->input['approveposts'] != 'unapprove' &&
                    $this->bb->input['approveposts'] != 'toggle'
                ) {
                    $this->bb->input['approveposts'] = '';
                }

                if ($this->bb->input['softdeleteposts'] != '' &&
                    $this->bb->input['softdeleteposts'] != 'softdelete' &&
                    $this->bb->input['softdeleteposts'] != 'restore' &&
                    $this->bb->input['softdeleteposts'] != 'toggle'
                ) {
                    $this->bb->input['softdeleteposts'] = '';
                }

                if ($this->bb->input['splitposts'] < -2) {
                    $this->bb->input['splitposts'] = -1;
                }

                if ($this->bb->input['splitpostsclose'] == 1) {
                    $this->bb->input['splitpostsclose'] = 'close';
                } else {
                    $this->bb->input['splitpostsclose'] = '';
                }

                if ($this->bb->input['splitpostsstick'] == 1) {
                    $this->bb->input['splitpostsstick'] = 'stick';
                } else {
                    $this->bb->input['splitpostsstick'] = '';
                }

                if ($this->bb->input['splitpostsunapprove'] == 1) {
                    $this->bb->input['splitpostsunapprove'] = 'unapprove';
                } else {
                    $this->bb->input['splitpostsunapprove'] = '';
                }

                if (!$this->bb->getInput('splitthreadprefix', 0)) {
                    $this->bb->input['splitthreadprefix'] = '';
                }

                if (!isset($errors)) {
                    $thread_options = [
                        'confirmation' => $this->bb->getInput('confirmation', 0),
                        'deletethread' => $this->bb->getInput('deletethread', 0),
                        'softdeletethread' => $this->bb->input['softdeletethread'],
                        'approvethread' => $this->bb->input['approvethread'],
                        'openthread' => $this->bb->input['openthread'],
                        'stickthread' => $this->bb->input['stickthread'],
                        'movethread' => $this->bb->getInput('move_1_forum', 0),
                        'movethreadredirect' => $this->bb->getInput('move_2_redirect', 0),
                        'movethreadredirectexpire' => $this->bb->getInput('move_3_redirecttime', 0),
                        'copythread' => $this->bb->getInput('copy_1_forum', 0),
                        'newsubject' => $this->bb->input['newsubject'],
                        'addreply' => $this->bb->input['newreply'],
                        'replysubject' => $this->bb->input['newreplysubject'],
                        'pm_subject' => $this->bb->input['pm_subject'],
                        'pm_message' => $this->bb->input['pm_message'],
                        'threadprefix' => $this->bb->getInput('threadprefix', 0)
                    ];

                    if (stripos($this->bb->input['splitpostsnewsubject'], '{subject}') === false) {
                        $this->bb->input['splitpostsnewsubject'] = '{subject}' . $this->bb->input['splitpostsnewsubject'];
                    }

                    $post_options = [
                        'deleteposts' => $this->bb->getInput('deleteposts', 0),
                        'softdeleteposts' => $this->bb->input['softdeleteposts'],
                        'mergeposts' => $this->bb->getInput('mergeposts', 0),
                        'approveposts' => $this->bb->input['approveposts'],
                        'splitposts' => $this->bb->getInput('splitposts', 0),
                        'splitpostsclose' => $this->bb->input['splitpostsclose'],
                        'splitpostsstick' => $this->bb->input['splitpostsstick'],
                        'splitpostsunapprove' => $this->bb->input['splitpostsunapprove'],
                        'splitthreadprefix' => $this->bb->getInput('splitthreadprefix', 0),
                        'splitpostsnewsubject' => $this->bb->input['splitpostsnewsubject'],
                        'splitpostsaddreply' => $this->bb->input['splitpostsaddreply'],
                        'splitpostsreplysubject' => $this->bb->input['splitpostsreplysubject']
                    ];

                    $new_tool['type'] = 'p';
                    $new_tool['threadoptions'] = $this->db->escape_string(my_serialize($thread_options));
                    $new_tool['postoptions'] = $this->db->escape_string(my_serialize($post_options));
                    $new_tool['name'] = $this->db->escape_string($this->bb->input['title']);
                    $new_tool['description'] = $this->db->escape_string($this->bb->input['description']);
                    $new_tool['forums'] = '';
                    $new_tool['groups'] = '';

                    if ($this->bb->input['forum_type'] == 2) {
                        if (is_array($this->bb->input['forum_1_forums'])) {
                            $checked = [];

                            foreach ($this->bb->input['forum_1_forums'] as $fid) {
                                $checked[] = (int)$fid;
                            }

                            $new_tool['forums'] = implode(',', $checked);
                        }
                    } else {
                        $new_tool['forums'] = '-1';
                    }

                    if ($this->bb->input['group_type'] == 2) {
                        if (is_array($this->bb->input['group_1_groups'])) {
                            $checked = [];

                            foreach ($this->bb->input['group_1_groups'] as $gid) {
                                $checked[] = (int)$gid;
                            }

                            $new_tool['groups'] = implode(',', $checked);
                        }
                    } else {
                        $new_tool['groups'] = '-1';
                    }

                    $tid = $this->db->insert_query('modtools', $new_tool);

                    $this->plugins->runHooks('admin_config_mod_tools_add_post_tool_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($tid, $this->bb->input['title']);
                    $this->cache->update_forumsdisplay();

                    $this->session->flash_message($this->lang->success_mod_tool_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/mod_tools?action=post_tools');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_post_tool);
            $html = $this->page->output_header($this->lang->mod_tools . ' - ' . $this->lang->add_new_post_tool);

            $sub_tabs['thread_tools'] = [
                'title' => $this->lang->thread_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools'
            ];
            $sub_tabs['add_thread_tool'] = [
                'title' => $this->lang->add_new_thread_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_thread_tool'
            ];
            $sub_tabs['post_tools'] = [
                'title' => $this->lang->post_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=post_tools',
            ];
            $sub_tabs['add_post_tool'] = [
                'title' => $this->lang->add_new_post_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_post_tool',
                'description' => $this->lang->add_post_tool_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_post_tool');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/mod_tools?action=add_post_tool', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['title'] = '';
                $this->bb->input['description'] = '';
                $this->bb->input['forum_1_forums'] = '';
                $forum_checked[1] = 'checked="checked"';
                $forum_checked[2] = '';
                $this->bb->input['group_1_groups'] = '';
                $group_checked[1] = 'checked="checked"';
                $group_checked[2] = '';
                $this->bb->input['confirmation'] = '0';
                $this->bb->input['approvethread'] = '';
                $this->bb->input['softdeletethread'] = '';
                $this->bb->input['openthread'] = '';
                $this->bb->input['stickthread'] = '';
                $this->bb->input['move_1_forum'] = '';
                $this->bb->input['move_2_redirect'] = '0';
                $this->bb->input['move_3_redirecttime'] = '';
                $move_checked[1] = 'checked="checked"';
                $move_checked[2] = '';
                $copy_checked[1] = 'checked="checked"';
                $copy_checked[2] = '';
                $this->bb->input['copy_1_forum'] = '';
                $this->bb->input['deletethread'] = '0';
                $this->bb->input['threadprefix'] = '-1';
                $this->bb->input['newsubject'] = '{subject}';
                $this->bb->input['newreply'] = '';
                $this->bb->input['newreplysubject'] = '{subject}';
                $do_not_split_checked = ' selected="selected"';
                $split_same_checked = '';
                $this->bb->input['deleteposts'] = '0';
                $this->bb->input['mergeposts'] = '0';
                $this->bb->input['approveposts'] = '';
                $this->bb->input['softdeleteposts'] = '';
                $this->bb->input['splitposts'] = '-1';
                $this->bb->input['splitpostsclose'] = '0';
                $this->bb->input['splitpostsstick'] = '0';
                $this->bb->input['splitpostsunapprove'] = '0';
                $this->bb->input['splitthreadprefix'] = '0';
                $this->bb->input['splitpostsnewsubject'] = '{subject}';
                $this->bb->input['splitpostsaddreply'] = '';
                $this->bb->input['splitpostsreplysubject'] = '{subject}';
            }

            $form_container = new FormContainer($this, $this->lang->general_options);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"1\" {$forum_checked[1]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"2\" {$forum_checked[2]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"forum_2\" class=\"forums\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
					<td>" . $form->generate_forum_select('forum_1_forums[]', $this->bb->input['forum_1_forums'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('forum');
	</script>";
            $form_container->output_row($this->lang->available_in_forums . ' <em>*</em>', '', $actions);

            $actions = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"1\" {$group_checked[1]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"2\" {$group_checked[2]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"group_2\" class=\"groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('group_1_groups[]', $this->bb->input['group_1_groups'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('group');
	</script>";
            $form_container->output_row($this->lang->available_to_groups . ' <em>*</em>', '', $actions);
            $form_container->output_row($this->lang->show_confirmation . ' <em>*</em>', '', $form->generate_yes_no_radio('confirmation', $this->bb->input['confirmation'], ['style' => 'width: 2em;']));
            $html .= $form_container->end();

            $approve_unapprove = [
                '' => $this->lang->no_change,
                'approve' => $this->lang->approve,
                'unapprove' => $this->lang->unapprove,
                'toggle' => $this->lang->toggle
            ];

            $form_container = new FormContainer($this, $this->lang->inline_post_moderation);

            $softdelete_restore = [
                '' => $this->lang->no_change,
                'restore' => $this->lang->restore,
                'softdelete' => $this->lang->softdelete,
                'toggle' => $this->lang->toggle
            ];

            $form_container->output_row($this->lang->softdelete_restore_posts . ' <em>*</em>', '', $form->generate_select_box('softdeleteposts', $softdelete_restore, $this->bb->input['softdeleteposts'], ['id' => 'softdeleteposts']), 'softdeleteposts');
            $form_container->output_row($this->lang->delete_posts . ' <em>*</em>', '', $form->generate_yes_no_radio('deleteposts', $this->bb->input['deleteposts']));
            $form_container->output_row($this->lang->merge_posts . ' <em>*</em>', $this->lang->merge_posts_desc, $form->generate_yes_no_radio('mergeposts', $this->bb->input['mergeposts']));
            $form_container->output_row($this->lang->approve_unapprove_posts . ' <em>*</em>', '', $form->generate_select_box('approveposts', $approve_unapprove, $this->bb->input['approveposts'], ['id' => 'approveposts']), 'approveposts');
            $html .= $form_container->end();

            $this->bb->selectoptions = "<option value=\"-1\"{$do_not_split_checked}>{$this->lang->do_not_split}</option>\n";
            $this->bb->selectoptions .= "<option value=\"-2\"{$split_same_checked} style=\"border-bottom: 1px solid #000;\">{$this->lang->split_to_same_forum}</option>\n";

            $form_container = new FormContainer($this, $this->lang->split_posts);
            $form_container->output_row($this->lang->split_posts2 . ' <em>*</em>', '', $form->generate_forum_select('splitposts', $this->bb->input['splitposts']));
            $form_container->output_row($this->lang->close_split_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('splitpostsclose', $this->bb->input['splitpostsclose']));
            $form_container->output_row($this->lang->stick_split_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('splitpostsstick', $this->bb->input['splitpostsstick']));
            $form_container->output_row($this->lang->unapprove_split_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('splitpostsunapprove', $this->bb->input['splitpostsunapprove']));

            $splitthreadprefix = $this->thread->build_prefixes();
            if (!empty($splitthreadprefix)) {
                $split_thread_prefixes = [
                    '0' => $this->lang->no_prefix
                ];

                foreach ($splitthreadprefix as $prefix) {
                    $split_thread_prefixes[$prefix['pid']] = $prefix['prefix'];
                }

                $form_container->output_row($this->lang->split_thread_prefix . ' <em>*</em>', '', $form->generate_select_box('splitthreadprefix', $split_thread_prefixes, [$this->bb->getInput('splitthreadprefix', 0)], ['id' => 'splitthreadprefix']), 'splitthreadprefix');
            }

            $form_container->output_row($this->lang->split_thread_subject, $this->lang->split_thread_subject_desc, $form->generate_text_box('splitpostsnewsubject', $this->bb->input['splitpostsnewsubject'], ['id' => 'splitpostsnewsubject ']), 'newreplysubject');
            $form_container->output_row($this->lang->add_new_split_reply, $this->lang->add_new_split_reply_desc, $form->generate_text_area('splitpostsaddreply', $this->bb->input['splitpostsaddreply'], ['id' => 'splitpostsaddreply']), 'splitpostsaddreply');
            $form_container->output_row($this->lang->split_reply_subject, $this->lang->split_reply_subject_desc, $form->generate_text_box('splitpostsreplysubject', $this->bb->input['splitpostsreplysubject'], ['id' => 'splitpostsreplysubject']), 'splitpostsreplysubject');
            $html .= $form_container->end();

            $open_close = [
                '' => $this->lang->no_change,
                'open' => $this->lang->open,
                'close' => $this->lang->close,
                'toggle' => $this->lang->toggle
            ];

            $stick_unstick = [
                '' => $this->lang->no_change,
                'stick' => $this->lang->stick,
                'unstick' => $this->lang->unstick,
                'toggle' => $this->lang->toggle
            ];

            $form_container = new FormContainer($this, $this->lang->thread_moderation);
            $form_container->output_row($this->lang->approve_unapprove . ' <em>*</em>', '', $form->generate_select_box('approvethread', $approve_unapprove, $this->bb->input['approvethread'], ['id' => 'approvethread']), 'approvethread');
            $form_container->output_row($this->lang->open_close_thread . ' <em>*</em>', '', $form->generate_select_box('openthread', $open_close, $this->bb->input['openthread'], ['id' => 'openthread']), 'openthread');
            $form_container->output_row($this->lang->stick_unstick_thread . ' <em>*</em>', '', $form->generate_select_box('stickthread', $stick_unstick, $this->bb->input['stickthread'], ['id' => 'stickthread']), 'stickthread');

            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"1\" {$move_checked[1]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_move_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"move_type\" value=\"2\" {$move_checked[2]} class=\"moves_check\" onclick=\"checkAction('move');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->move_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"move_2\" class=\"moves\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_move_to}</small></td>
					<td>" . $form->generate_forum_select('move_1_forum', $this->bb->input['move_1_forum']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->leave_redirect}</small></td>
					<td>" . $form->generate_yes_no_radio('move_2_redirect', $this->bb->input['move_2_redirect']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->delete_redirect_after}</small></td>
					<td>" . $form->generate_numeric_field('move_3_redirecttime', $this->bb->input['move_3_redirecttime'], ['style' => 'width: 3em;', 'min' => 0]) . " {$this->lang->days}</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('move');
	</script>";
            $form_container->output_row($this->lang->move_thread . ' <em>*</em>', $this->lang->move_thread_desc, $actions);

            $actions = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"1\" {$copy_checked[1]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->do_not_copy_thread}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"copy_type\" value=\"2\" {$copy_checked[2]} class=\"copys_check\" onclick=\"checkAction('copy');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->copy_thread}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"copy_2\" class=\"copys\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->forum_to_copy_to}</small></td>
					<td>" . $form->generate_forum_select('copy_1_forum', $this->bb->input['copy_1_forum']) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('copy');
	</script>";
            $form_container->output_row($this->lang->copy_thread . ' <em>*</em>', '', $actions);
            $form_container->output_row($this->lang->softdelete_restore_thread . ' <em>*</em>', '', $form->generate_select_box('softdeletethread', $softdelete_restore, $this->bb->input['softdeletethread'], ['id' => 'softdeletethread']), 'softdeletethread');
            $form_container->output_row($this->lang->delete_thread . ' <em>*</em>', '', $form->generate_yes_no_radio('deletethread', $this->bb->input['deletethread']));

            $threadprefixes = $this->thread->build_prefixes();
            if (!empty($threadprefixes)) {
                $thread_prefixes = [
                    '-1' => $this->lang->no_change,
                    '0' => $this->lang->no_prefix
                ];

                foreach ($threadprefixes as $prefix) {
                    $thread_prefixes[$prefix['pid']] = $prefix['prefix'];
                }

                $form_container->output_row($this->lang->apply_thread_prefix . ' <em>*</em>', '', $form->generate_select_box('threadprefix', $thread_prefixes, $this->bb->input['threadprefix'], ['id' => 'threadprefix']), 'threadprefix');
            }

            $form_container->output_row($this->lang->new_subject . ' <em>*</em>', $this->lang->new_subject_desc, $form->generate_text_box('newsubject', $this->bb->input['newsubject']));
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->add_new_reply);
            $form_container->output_row($this->lang->add_new_reply, $this->lang->add_new_reply_desc, $form->generate_text_area('newreply', $this->bb->input['newreply'], ['id' => 'newreply']), 'newreply');
            $form_container->output_row($this->lang->reply_subject, $this->lang->reply_subject_desc, $form->generate_text_box('newreplysubject', $this->bb->input['newreplysubject'], ['id' => 'newreplysubject']), 'newreplysubject');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->send_private_message);
            $form_container->output_row($this->lang->private_message_message, $this->lang->private_message_message_desc, $form->generate_text_area('pm_message', $this->bb->getInput('pm_message', ''), ['id' => 'pm_message']), 'pm_message');
            $form_container->output_row($this->lang->private_message_subject, $this->lang->private_message_subject_desc, $form->generate_text_box('pm_subject', $this->bb->getInput('pm_subject', ''), ['id' => 'pm_subject']), 'pm_subject');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_post_tool);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ModToolsPostToolAdd.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_mod_tools_start');

            $html = $this->page->output_header($this->lang->mod_tools . ' - ' . $this->lang->thread_tools);

            $sub_tabs['thread_tools'] = [
                'title' => $this->lang->thread_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools',
                'description' => $this->lang->thread_tools_desc
            ];
            $sub_tabs['add_thread_tool'] = [
                'title' => $this->lang->add_new_thread_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_thread_tool'
            ];
            $sub_tabs['post_tools'] = [
                'title' => $this->lang->post_tools,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=post_tools',
            ];
            $sub_tabs['add_post_tool'] = [
                'title' => $this->lang->add_new_post_tool,
                'link' => $this->bb->admin_url . '/config/mod_tools?action=add_post_tool'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'thread_tools');

            $table = new Table;
            $table->construct_header($this->lang->title);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('modtools', 'tid, name, description, type', "type='t'", ['order_by' => 'name']);
            while ($tool = $this->db->fetch_array($query)) {
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/mod_tools?action=edit_thread_tool&amp;tid={$tool['tid']}\"><strong>" . htmlspecialchars_uni($tool['name']) . "</strong></a><br /><small>" . htmlspecialchars_uni($tool['description']) . "</small>");
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/mod_tools?action=edit_thread_tool&amp;tid={$tool['tid']}\">{$this->lang->edit}</a>", ['width' => 100, 'class' => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/mod_tools?action=delete_thread_tool&amp;tid={$tool['tid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_thread_tool_deletion}')\">{$this->lang->delete}</a>", ['width' => 100, 'class' => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_thread_tools, ['colspan' => 3]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->thread_tools);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ModTools.html.twig');
        }
    }
}
