<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class MyCode extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_mycode', false, true);

        $this->page->add_breadcrumb_item($this->lang->mycode, $this->bb->admin_url . '/config/mycode');

        $this->plugins->runHooks('admin_config_mycode_begin');

        if ($this->bb->input['action'] == 'toggle_status') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mycode');
            }

            $query = $this->db->simple_select('mycode', '*', "cid='" . $this->bb->getInput('cid', 0) . "'");
            $mycode = $this->db->fetch_array($query);

            if (!$mycode['cid']) {
                $this->session->flash_message($this->lang->error_invalid_mycode, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mycode');
            }

            $this->plugins->runHooks('admin_config_mycode_toggle_status');

            if ($mycode['active'] == 1) {
                $new_status = 0;
                $phrase = $this->lang->success_deactivated_mycode;
            } else {
                $new_status = 1;
                $phrase = $this->lang->success_activated_mycode;
            }
            $mycode_update = [
                'active' => $new_status,
            ];

            $this->plugins->runHooks('admin_config_mycode_toggle_status_commit');

            $this->db->update_query('mycode', $mycode_update, "cid='{$mycode['cid']}'");

            $this->cache->update_mycode();

            // Log admin action
            $this->bblogger->log_admin_action($mycode['cid'], $mycode['title'], $new_status);

            $this->session->flash_message($phrase, 'success');
            return $response->withRedirect($this->bb->admin_url . '/config/mycode');
        }

        if ($this->bb->input['action'] == 'xmlhttp_test_mycode' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_config_mycode_xmlhttp_test_mycode_start');

            // Send no cache headers
            header('Expires: Sat, 1 Jan 2000 01:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-type: text/html');

            $sandbox = $this->test_regex($this->bb->input['regex'], $this->bb->input['replacement'], $this->bb->input['test_value']);

            $this->plugins->runHooks('admin_config_mycode_xmlhttp_test_mycode_end');

            echo $sandbox['actual'];
            exit;
        }

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_mycode_add');

            $sandbox = ['html' => '', 'actual' => ''];
            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['regex'])) {
                    $errors[] = $this->lang->error_missing_regex;
                }

                if (!trim($this->bb->input['replacement'])) {
                    $errors[] = $this->lang->error_missing_replacement;
                }

                if ($this->bb->input['test']) {
                    $errors[] = $this->lang->changes_not_saved;
                    $sandbox = $this->test_regex($this->bb->input['regex'], $this->bb->input['replacement'], $this->bb->input['test_value']);
                }

                if (!$errors) {
                    $new_mycode = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'regex' => $this->db->escape_string(str_replace("\x0", '', $this->bb->input['regex'])),
                        'replacement' => $this->db->escape_string($this->bb->input['replacement']),
                        'active' => $this->bb->getInput('active', 0),
                        'parseorder' => $this->bb->getInput('parseorder', 0)
                    ];

                    $cid = $this->db->insert_query('mycode', $new_mycode);

                    $this->plugins->runHooks('admin_config_mycode_add_commit');

                    $this->cache->update_mycode();

                    // Log admin action
                    $this->bblogger->log_admin_action($cid, htmlspecialchars_uni($this->bb->input['title']));

                    $this->session->flash_message($this->lang->success_added_mycode, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/mycode');
                }
            }

            $sub_tabs['mycode'] = [
                'title' => $this->lang->mycode,
                'link' => $this->bb->admin_url . '/config/mycode',
                'description' => $this->lang->mycode_desc
            ];

            $sub_tabs['add_new_mycode'] = [
                'title' => $this->lang->add_new_mycode,
                'link' => $this->bb->admin_url . '/config/mycode?action=add',
                'description' => $this->lang->add_new_mycode_desc
            ];

            $this->page->extra_header .= "
	<script type=\"text/javascript\">
	var my_post_key = '" . $this->bb->post_code . "';
	lang.mycode_sandbox_test_error = \"{$this->lang->mycode_sandbox_test_error}\";
	</script>";

            $this->page->add_breadcrumb_item($this->lang->add_new_mycode);
            $html = $this->page->output_header($this->lang->custom_mycode . ' - ' . $this->lang->add_new_mycode);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_new_mycode');

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['active'] = 1;
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/config/mycode?action=add', 'post', 'add');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->add_mycode);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description, '', $form->generate_text_box('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->regular_expression . ' <em>*</em>', $this->lang->regular_expression_desc . '<br /><strong>' . $this->lang->example . '</strong> \[b\](.*?)\[/b\]', $form->generate_text_area('regex', $this->bb->getInput('regex', ''), ['id' => 'regex']), 'regex');
            $form_container->output_row($this->lang->replacement . ' <em>*</em>', $this->lang->replacement_desc . '<br /><strong>' . $this->lang->example . '</strong> &lt;strong&gt;$1&lt;/strong&gt;', $form->generate_text_area('replacement', $this->bb->getInput('replacement', ''), ['id' => 'replacement']), 'replacement');
            $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('active', $this->bb->input['active']));
            $form_container->output_row($this->lang->parse_order, $this->lang->parse_order_desc, $form->generate_numeric_field('parseorder', $this->bb->getInput('parseorder', ''), ['id' => 'parseorder', 'min' => 0]), 'parseorder');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_mycode);
            $html .= $form->output_submit_wrapper($buttons);

            // Sandbox
            $html .= "<br />\n";
            $form_container = new FormContainer($this, $this->lang->sandbox);
            $form_container->output_row($this->lang->sandbox_desc);
            $form_container->output_row($this->lang->test_value, $this->lang->test_value_desc, $form->generate_text_area('test_value', $this->bb->getInput('test_value', ''), ['id' => 'test_value']) . "<br />" . $form->generate_submit_button($this->lang->test, ['id' => 'test', 'name' => 'test']), 'test_value');
            $form_container->output_row($this->lang->result_html, $this->lang->result_html_desc, $form->generate_text_area('result_html', $sandbox['html'], ['id' => 'result_html', 'disabled' => 1]), 'result_html');
            $form_container->output_row($this->lang->result_actual, $this->lang->result_actual_desc, "<div id=\"result_actual\">{$sandbox['actual']}</div>");
            $html .= $form_container->end();
//            $html .= '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/mycode_sandbox.js"></script>';
//            $html .= '<script type="text/javascript">
////<![CDATA[
//$(function(){
//    new MyCodeSandbox("' . $this->bb->settings['bburl'] . '/admin/config/mycode?action=xmlhttp_test_mycode", $("#test"), $("#regex"), $("#replacement"), $("#test_value"), $("#result_html"), $("#result_actual"));
//});
////]]>
//</script>';

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/MyCodeAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('mycode', '*', "cid='" . $this->bb->getInput('cid', 0) . "'");
            $mycode = $this->db->fetch_array($query);

            if (!$mycode['cid']) {
                $this->session->flash_message($this->lang->error_invalid_mycode, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mycode');
            }

            $this->plugins->runHooks('admin_config_mycode_edit');

            $sandbox = ['html' => '', 'actual' => ''];
            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['regex'])) {
                    $errors[] = $this->lang->error_missing_regex;
                }

                if (!trim($this->bb->input['replacement'])) {
                    $errors[] = $this->lang->error_missing_replacement;
                }

                if ($this->bb->input['test']) {
                    $errors[] = $this->lang->changes_not_saved;
                    $sandbox = $this->test_regex($this->bb->input['regex'], $this->bb->input['replacement'], $this->bb->input['test_value']);
                }

                if (!isset($errors)) {
                    $updated_mycode = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'regex' => $this->db->escape_string(str_replace("\x0", '', $this->bb->input['regex'])),
                        'replacement' => $this->db->escape_string($this->bb->input['replacement']),
                        'active' => $this->bb->getInput('active', 0),
                        'parseorder' => $this->bb->getInput('parseorder', 0)
                    ];

                    $this->plugins->runHooks('admin_config_mycode_edit_commit');

                    $this->db->update_query('mycode', $updated_mycode, "cid='{$mycode['cid']}'");

                    $this->cache->update_mycode();

                    // Log admin action
                    $this->bblogger->log_admin_action($mycode['cid'], htmlspecialchars_uni($this->bb->input['title']));

                    $this->session->flash_message($this->lang->success_updated_mycode, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/mycode');
                }
            }

            $sub_tabs['edit_mycode'] = [
                'title' => $this->lang->edit_mycode,
                'link' => $this->bb->admin_url . '/config/mycode?action=edit',
                'description' => $this->lang->edit_mycode_desc
            ];

            $this->page->extra_header .= "
	<script type=\"text/javascript\">
	var my_post_key = '" . $this->bb->post_code . "';
	lang.mycode_sandbox_test_error = \"{$this->lang->mycode_sandbox_test_error}\";
	</script>";

            $this->page->add_breadcrumb_item($this->lang->edit_mycode);
            $html = $this->page->output_header($this->lang->custom_mycode . ' - ' . $this->lang->edit_mycode);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_mycode');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/mycode?action=edit', 'post', 'edit');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('cid', $mycode['cid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, $mycode);
            }

            $form_container = new FormContainer($this, $this->lang->edit_mycode);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description, '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->regular_expression . ' <em>*</em>', $this->lang->regular_expression_desc . '<br /><strong>' . $this->lang->example . '</strong> \[b\](.*?)\[/b\]', $form->generate_text_area('regex', $this->bb->input['regex'], ['id' => 'regex']), 'regex');
            $form_container->output_row($this->lang->replacement . ' <em>*</em>', $this->lang->replacement_desc . '<br /><strong>' . $this->lang->example . '</strong> &lt;strong&gt;$1&lt;/strong&gt;', $form->generate_text_area('replacement', $this->bb->input['replacement'], ['id' => 'replacement']), 'replacement');
            $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('active', $this->bb->input['active']));
            $form_container->output_row($this->lang->parse_order, $this->lang->parse_order_desc, $form->generate_numeric_field('parseorder', $this->bb->input['parseorder'], ['id' => 'parseorder', 'min' => 0]), 'parseorder');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_mycode);

            $html .= $form->output_submit_wrapper($buttons);

            // Sandbox
            $html .= "<br />\n";
            $form_container = new FormContainer($this, $this->lang->sandbox);
            $form_container->output_row($this->lang->sandbox_desc);
            $form_container->output_row($this->lang->test_value, $this->lang->test_value_desc, $form->generate_text_area('test_value', $this->bb->getInput('test_value', ''), ['id' => 'test_value']) . "<br />" . $form->generate_submit_button($this->lang->test, ['id' => 'test', 'name' => 'test']), 'test_value');
            $form_container->output_row($this->lang->result_html, $this->lang->result_html_desc, $form->generate_text_area('result_html', $sandbox['html'], ['id' => 'result_html', 'disabled' => 1]), 'result_html');
            $form_container->output_row($this->lang->result_actual, $this->lang->result_actual_desc, "<div id=\"result_actual\">{$sandbox['actual']}</div>");
            $html .= $form_container->end();
//            echo '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/mycode_sandbox.js"></script>';
//            echo '<script type="text/javascript">
//
//$(function(){
////<![CDATA[
//    new MyCodeSandbox("' . $this->bb->settings['bburl'] . '/admin/config/mycode?action=xmlhttp_test_mycode", $("#test"), $("#regex"), $("#replacement"), $("#test_value"), $("#result_html"), $("#result_actual"));
//});
////]]>
//</script>';

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/MyCodeEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('mycode', '*', "cid='" . $this->bb->getInput('cid', 0) . "'");
            $mycode = $this->db->fetch_array($query);

            if (!$mycode['cid']) {
                $this->session->flash_message($this->lang->error_invalid_mycode, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/mycode');
            }

            $this->plugins->runHooks('admin_config_mycode_delete');

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/mycode');
            }

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('mycode', "cid='{$mycode['cid']}'");

                $this->plugins->runHooks('admin_config_mycode_delete_commit');

                $this->cache->update_mycode();

                // Log admin action
                $this->bblogger->log_admin_action($mycode['cid'], htmlspecialchars_uni($mycode['title']));

                $this->session->flash_message($this->lang->success_deleted_mycode, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/mycode');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/mycode?action=delete&cid=' . $mycode['cid'], $this->lang->confirm_mycode_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_mycode_start');

            $html = $this->page->output_header($this->lang->custom_mycode);

            $sub_tabs['mycode'] = [
                'title' => $this->lang->mycode,
                'link' => $this->bb->admin_url . '/config/mycode',
                'description' => $this->lang->mycode_desc
            ];

            $sub_tabs['add_new_mycode'] = [
                'title' => $this->lang->add_new_mycode,
                'link' => $this->bb->admin_url . '/config/mycode?action=add'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'mycode');

            $table = new Table;
            $table->construct_header($this->lang->title);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            $query = $this->db->simple_select('mycode', '*', '', ['order_by' => 'parseorder']);
            while ($mycode = $this->db->fetch_array($query)) {
                if ($mycode['active'] == 1) {
                    $phrase = $this->lang->deactivate_mycode;
                    $icon = "<img src=\"".$this->bb->admin_url."/styles/{$this->page->style}/images/icons/bullet_on.png\" alt=\"({$this->lang->alt_enabled})\" title=\"{$this->lang->alt_enabled}\"  style=\"vertical-align: middle;\" /> ";
                } else {
                    $phrase = $this->lang->activate_mycode;
                    $icon = "<img src=\"".$this->bb->admin_url."/styles/{$this->page->style}/images/icons/bullet_off.png\" alt=\"({$this->lang->alt_disabled})\" title=\"{$this->lang->alt_disabled}\"  style=\"vertical-align: middle;\" /> ";
                }

                if ($mycode['description']) {
                    $mycode['description'] = '<small>' . htmlspecialchars_uni($mycode['description']) . '</small>';
                }

                $table->construct_cell("<div>{$icon}<strong><a href=\"".$this->bb->admin_url."/config/mycode?action=edit&amp;cid={$mycode['cid']}\">" . htmlspecialchars_uni($mycode['title']) . "</a></strong><br />{$mycode['description']}</div>");

                $popup = new PopupMenu("mycode_{$mycode['cid']}", $this->lang->options);
                $popup->add_item($this->lang->edit_mycode, $this->bb->admin_url . '/config/mycode?action=edit&amp;cid=' . $mycode['cid']);
                $popup->add_item($phrase, $this->bb->admin_url . "/config/mycode?action=toggle_status&amp;cid={$mycode['cid']}&amp;my_post_key={$this->bb->post_code}");
                $popup->add_item($this->lang->delete_mycode, $this->bb->admin_url . "/config/mycode?action=delete&amp;cid={$mycode['cid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_mycode_deletion}')");
                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_mycode, ['colspan' => 2]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->custom_mycode);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/MyCode.html.twig');
        }
    }

    private function test_regex($regex, $replacement, $test)
    {
        $array = [];
        $array['actual'] = @preg_replace('#' . str_replace("\x0", '', $regex) . '#si', $replacement, $test);
        $array['html'] = htmlspecialchars_uni($array['actual']);
        return $array;
    }
}
