<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use RunBB\Core\XMLParser;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;

class Plugins extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_plugins', false, true);

        $this->page->add_breadcrumb_item($this->lang->plugins, $this->bb->admin_url . '/config/plugins');

        $this->plugins->runHooks('admin_config_plugins_begin');

        if ($this->bb->input['action'] == 'browse') {
            $this->page->add_breadcrumb_item($this->lang->browse_plugins);

            $html = $this->page->output_header($this->lang->browse_plugins);

            $sub_tabs['plugins'] = [
                'title' => $this->lang->plugins,
                'link' => $this->bb->admin_url . '/config/plugins',
                'description' => $this->lang->plugins_desc
            ];
            $sub_tabs['update_plugins'] = [
                'title' => $this->lang->plugin_updates,
                'link' => $this->bb->admin_url . '/config/plugins?action=check',
                'description' => $this->lang->plugin_updates_desc
            ];
            $sub_tabs['browse_plugins'] = [
                'title' => $this->lang->browse_plugins,
                'link' => $this->bb->admin_url . '/config/plugins?action=browse',
                'description' => $this->lang->browse_plugins_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'browse_plugins');

            $keywords = '';
            if (isset($this->bb->input['keywords'])) {
                $keywords = '&keywords=' . urlencode($this->bb->input['keywords']);
            }

            if (isset($this->bb->input['page'])) {
                $url_page = '&page=' . $this->bb->getInput('page', 0);
            } else {
                $this->bb->input['page'] = 1;
                $url_page = '';
            }

            // Gets the major version code. i.e. 1410 -> 1400 or 121 -> 1200
            $major_version_code = round($this->bb->version_code / 100, 0) * 100;
            // Convert to mods site version codes
            $search_version = ($major_version_code / 100) . 'x';

            $contents = fetch_remote_file("http://community.mybb.com/xmlbrowse.php?type=plugins&version={$search_version}{$keywords}{$url_page}");//, $post_data); //FIXME $post_data ???

            if (!$contents) {
                $html .= $this->page->output_inline_error($this->lang->error_communication_problem);
                $this->page->output_footer();
                tdie('What Here ???');
                exit;
            }

            $table = new Table;
            $table->construct_header($this->lang->plugin);
            $table->construct_header($this->lang->latest_version, ['class' => 'align_center', 'width' => 125]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 125]);

            $parser = new XMLParser($contents);
            $tree = $parser->get_tree();

            if (!is_array($tree) || !isset($tree['results'])) {
                $html .= $this->page->output_inline_error($this->lang->error_communication_problem);
                $this->page->output_footer();
                tdie('What Here ???');
                exit;
            }

            if (!empty($tree['results']['result'])) {
                if (array_key_exists('tag', $tree['results']['result'])) {
                    $only_plugin = $tree['results']['result'];
                    unset($tree['results']['result']);
                    $tree['results']['result'][0] = $only_plugin;
                }

                foreach ($tree['results']['result'] as $result) {
                    $result['name']['value'] = htmlspecialchars_uni($result['name']['value']);
                    $result['description']['value'] = htmlspecialchars_uni($result['description']['value']);
                    $result['author']['value'] = $this->parser->parse_message($result['author']['value'], [
                            'allow_html' => true
                        ]);
                    $result['version']['value'] = htmlspecialchars_uni($result['version']['value']);
                    $result['download_url']['value'] = htmlspecialchars_uni(html_entity_decode($result['download_url']['value']));

                    $table->construct_cell("<strong>{$result['name']['value']}</strong><br /><small>{$result['description']['value']}</small><br /><i><small>{$this->lang->created_by} {$result['author']['value']}</small></i>");
                    $table->construct_cell($result['version']['value'], ['class' => 'align_center']);
                    $table->construct_cell("<strong><a href=\"http://community.mybb.com/{$result['download_url']['value']}\" target=\"_blank\">{$this->lang->download}</a></strong>", ["class" => "align_center"]);
                    $table->construct_row();
                }
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->error_no_results_found, ['colspan' => 3]);
                $table->construct_row();
            }

            $search = new Form($this->bb, $this->bb->admin_url . '/config/plugins?action=browse', 'post', 'search_form');
            $html .= $search->getForm();
            $html .= "<div style=\"padding-bottom: 3px; margin-top: -9px; text-align: right;\">";
            if (isset($this->bb->input['keywords'])) {
                $default_class = '';
                $value = htmlspecialchars_uni($this->bb->input['keywords']);
            } else {
                $default_class = 'search_default';
                $value = $this->lang->search_for_plugins;
            }
            $html .= $search->generate_text_box('keywords', $value, ['id' => 'search_keywords', 'class' => "{$default_class} field150 field_small"]) . "\n";
            $html .= "<input type=\"submit\" class=\"search_button\" value=\"{$this->lang->search}\" />\n";
            $html .= "<script type=\"text/javascript\">
		var form = $(\"#search_form\");
		form.submit(function()
		{
			var search = $(\"#search_keywords\");
			if(search.val() == '' || search.val() == '{$this->lang->search_for_plugins}')
			{
				search.focus();
				return false;
			}
		});

		var search = $(\"#search_keywords\");
		search.focus(function()
		{
			var searched_focus = $(this);
			if(searched_focus.val() == '{$this->lang->search_for_plugins}')
			{
				searched_focus.removeClass(\"search_default\");
				searched_focus.val(\"\");
			}
		});

		search.blur(function()
		{
			var searched_blur = $(this);
			if(searched_blur.val() == \"\")
			{
				searched_blur.addClass('search_default');
				searched_blur.val('{$this->lang->search_for_plugins}');
			}
		});

		// fix the styling used if we have a different default value
        if(search.val() != '{$this->lang->search_for_plugins}')
        {
            search.removeClass('search_default');
        }
		</script>\n";
            $html .= "</div>\n";
            $html .= $search->end();

            // Recommended plugins = Default; Otherwise search results & pagination
            if ($this->bb->request_method == 'post') {
                $html .= $table->output("<span style=\"float: right;\"><small><a href=\"http://community.mybb.com/mods.php?action=browse&category=plugins\" target=\"_blank\">{$this->lang->browse_all_plugins}</a></small></span>" . $this->lang->sprintf($this->lang->browse_results_for_mybb, $this->bb->version));
            } else {
                $html .= $table->output("<span style=\"float: right;\"><small><a href=\"http://community.mybb.com/mods.php?action=browse&category=plugins\" target=\"_blank\">{$this->lang->browse_all_plugins}</a></small></span>" . $this->lang->sprintf($this->lang->recommended_plugins_for_mybb, $this->bb->version));
            }

            $html .= '<br />' . $this->adm->draw_admin_pagination($this->bb->input['page'], 15, $tree['results']['attributes']['total'], $this->bb->admin_url . "/config/plugins?action=browse{$keywords}&amp;page={page}");

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/PluginsBrowse.html.twig');
        }

        if ($this->bb->input['action'] == 'check') {
            $plugins_list = $this->get_plugins_list();

            $this->plugins->runHooks('admin_config_plugins_check');

            $info = [];

            if ($plugins_list) {
                $active_hooks = $this->plugins->hooks;
                foreach ($plugins_list as $plugin_file) {
                    require_once MYBB_ROOT . 'inc/plugins/' . $plugin_file;
                    $codename = str_replace('.php', '', $plugin_file);
                    $infofunc = $codename . '_info';
                    if (!function_exists($infofunc)) {
                        continue;
                    }
                    $plugininfo = $infofunc();
                    $plugininfo['guid'] = isset($plugininfo['guid']) ? trim($plugininfo['guid']) : '';
                    $plugininfo['codename'] = trim($plugininfo['codename']);

                    if ($plugininfo['codename'] != '') {
                        $info[] = $plugininfo['codename'];
                        $names[$plugininfo['codename']] = ['name' => $plugininfo['name'], 'version' => $plugininfo['version']];
                    } elseif ($plugininfo['guid'] != '') {
                        $info[] = $plugininfo['guid'];
                        $names[$plugininfo['guid']] = ['name' => $plugininfo['name'], 'version' => $plugininfo['version']];
                    }
                }
                $this->plugins->hooks = $active_hooks;
            }

            if (empty($info)) {
                $this->session->flash_message($this->lang->error_vcheck_no_supported_plugins, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            $url = 'http://community.mybb.com/version_check.php?';
            $url .= http_build_query(['info' => $info]) . '&';

            $contents = fetch_remote_file($url);

            if (!$contents) {
                $this->session->flash_message($this->lang->error_vcheck_communications_problem, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            $parser = new XMLParser($contents);
            $tree = $parser->get_tree();

            if (!is_array($tree) || !isset($tree['plugins'])) {
                $this->session->flash_message($this->lang->error_communication_problem, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            if (array_key_exists('error', $tree['plugins'])) {
                switch ($tree['plugins'][0]['error']) {
                    case '1':
                        $error_msg = $this->lang->error_no_input;
                        break;
                    case '2':
                        $error_msg = $this->lang->error_no_pids;
                        break;
                    default:
                        $error_msg = '';
                }
                $this->session->flash_message($this->lang->error_communication_problem . $error_msg, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            $table = new Table;
            $table->construct_header($this->lang->plugin);
            $table->construct_header($this->lang->your_version, ['class' => 'align_center', 'width' => 125]);
            $table->construct_header($this->lang->latest_version, ['class' => 'align_center', 'width' => 125]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 125]);

            if (!isset($tree['plugins']['plugin'])) {
                $this->session->flash_message($this->lang->success_plugins_up_to_date, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            if (array_key_exists('tag', $tree['plugins']['plugin'])) {
                $only_plugin = $tree['plugins']['plugin'];
                unset($tree['plugins']['plugin']);
                $tree['plugins']['plugin'][0] = $only_plugin;
            }

            foreach ($tree['plugins']['plugin'] as $plugin) {
                $compare_by = array_key_exists('codename', $plugin['attributes']) ? 'codename' : 'guid';
                $is_vulnerable = array_key_exists('vulnerable', $plugin) ? true : false;

                if (version_compare($names[$plugin['attributes'][$compare_by]]['version'], $plugin['version']['value'], '<')) {
                    $plugin['download_url']['value'] = htmlspecialchars_uni($plugin['download_url']['value']);
                    $plugin['vulnerable']['value'] = htmlspecialchars_uni($plugin['vulnerable']['value']);
                    $plugin['version']['value'] = htmlspecialchars_uni($plugin['version']['value']);

                    if ($is_vulnerable) {
                        $table->construct_cell("<div class=\"error\" id=\"flash_message\">
										{$this->lang->error_vcheck_vulnerable} {$names[$plugin['attributes'][$compare_by]]['name']}
										</div>
										<p>	<b>{$this->lang->error_vcheck_vulnerable_notes}</b> <br /><br /> {$plugin['vulnerable']['value']}</p>");
                    } else {
                        $table->construct_cell("<strong>{$names[$plugin['attributes'][$compare_by]]['name']}</strong>");
                    }
                    $table->construct_cell("{$names[$plugin['attributes'][$compare_by]]['version']}", ["class" => "align_center"]);
                    $table->construct_cell("<strong><span style=\"color: #C00\">{$plugin['version']['value']}</span></strong>", ["class" => "align_center"]);
                    if ($is_vulnerable) {
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/plugins\"><b>{$this->lang->deactivate}</b></a>", ["class" => "align_center", "width" => 150]);
                    } else {
                        $table->construct_cell("<strong><a href=\"http://community.mybb.com/{$plugin['download_url']['value']}\" target=\"_blank\">{$this->lang->download}</a></strong>", ["class" => "align_center"]);
                    }
                    $table->construct_row();
                }
            }

            if ($table->num_rows() == 0) {
                $this->session->flash_message($this->lang->success_plugins_up_to_date, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            $this->page->add_breadcrumb_item($this->lang->plugin_updates);

            $html = $this->page->output_header($this->lang->plugin_updates);

            $sub_tabs['plugins'] = [
                'title' => $this->lang->plugins,
                'link' => $this->bb->admin_url . '/config/plugins',
            ];

            $sub_tabs['update_plugins'] = [
                'title' => $this->lang->plugin_updates,
                'link' => $this->bb->admin_url . '/config/plugins?action=check',
                'description' => $this->lang->plugin_updates_desc
            ];

            $sub_tabs['browse_plugins'] = [
                'title' => $this->lang->browse_plugins,
                'link' => $this->bb->admin_url . '/config/plugins?action=browse',
                'description' => $this->lang->browse_plugins_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'update_plugins');

            $html .= $table->output($this->lang->plugin_updates);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/PluginsCheck.html.twig');
        }

        // Activates or deactivates a specific plugin
        if ($this->bb->input['action'] == 'activate' || $this->bb->input['action'] == 'deactivate') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            if ($this->bb->input['action'] == 'activate') {
                $this->plugins->runHooks('admin_config_plugins_activate');
            } else {
                $this->plugins->runHooks('admin_config_plugins_deactivate');
            }

            $codename = $this->bb->input['plugin'];
            $codename = str_replace([".", "/", "\\"], '', $codename);
            $file = basename($codename . '.php');

            // Check if the file exists and throw an error if it doesn't
            if (!file_exists(MYBB_ROOT . "inc/plugins/$file")) {
                $this->session->flash_message($this->lang->error_invalid_plugin, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/plugins');
            }

            $plugins_cache = $this->cache->read('plugins');
            $active_plugins = $plugins_cache['active'];

            require_once MYBB_ROOT . "inc/plugins/$file";

            $installed_func = "{$codename}_is_installed";
            $installed = true;
            if (function_exists($installed_func) && $installed_func() != true) {
                $installed = false;
            }

            $install_uninstall = false;

            if ($this->bb->input['action'] == 'activate') {
                $message = $this->lang->success_plugin_activated;

                // Plugin is compatible with this version?
                if ($this->plugins->is_compatible($codename) == false) {
                    $this->session->flash_message($this->lang->sprintf($this->lang->plugin_incompatible, $this->bb->version), 'error');
                    return $response->withRedirect($this->bb->admin_url . '/config/plugins');
                }

                // If not installed and there is a custom installation function
                if ($installed == false && function_exists("{$codename}_install")) {
                    call_user_func("{$codename}_install");
                    $message = $this->lang->success_plugin_installed;
                    $install_uninstall = true;
                }

                if (function_exists("{$codename}_activate")) {
                    call_user_func("{$codename}_activate");
                }

                $active_plugins[$codename] = $codename;
                $executed[] = 'activate';
            } elseif ($this->bb->input['action'] == 'deactivate') {
                $message = $this->lang->success_plugin_deactivated;

                if (function_exists("{$codename}_deactivate")) {
                    call_user_func("{$codename}_deactivate");
                }

                if ($this->bb->input['uninstall'] == 1 && function_exists("{$codename}_uninstall")) {
                    call_user_func("{$codename}_uninstall");
                    $message = $this->lang->success_plugin_uninstalled;
                    $install_uninstall = true;
                }

                unset($active_plugins[$codename]);
            }

            // Update plugin cache
            $plugins_cache['active'] = $active_plugins;
            $this->cache->update('plugins', $plugins_cache);

            // Log admin action
            $this->bblogger->log_admin_action($codename, $install_uninstall);

            if ($this->bb->input['action'] == 'activate') {
                $this->plugins->runHooks('admin_config_plugins_activate_commit');
            } else {
                $this->plugins->runHooks('admin_config_plugins_deactivate_commit');
            }

            $this->session->flash_message($message, 'success');
            return $response->withRedirect($this->bb->admin_url . '/config/plugins');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->plugins);

            $sub_tabs['plugins'] = [
                'title' => $this->lang->plugins,
                'link' => $this->bb->admin_url . '/config/plugins',
                'description' => $this->lang->plugins_desc
            ];
            $sub_tabs['update_plugins'] = [
                'title' => $this->lang->plugin_updates,
                'link' => $this->bb->admin_url . '/config/plugins?action=check',
                'description' => $this->lang->plugin_updates_desc
            ];

            $sub_tabs['browse_plugins'] = [
                'title' => $this->lang->browse_plugins,
                'link' => $this->bb->admin_url . '/config/plugins?action=browse',
                'description' => $this->lang->browse_plugins_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'plugins');

            // Let's make things easier for our user - show them active
            // and inactive plugins in different lists
            $plugins_cache = $this->cache->read('plugins');
            $active_plugins = isset($plugins_cache['active']) ? $plugins_cache['active'] : 'NoActivePlugin';

            $plugins_list = $this->get_plugins_list();

            $this->plugins->runHooks('admin_config_plugins_plugin_list');

            if (!empty($plugins_list)) {
                $a_plugins = $i_plugins = [];
                foreach ($plugins_list as $plugin_file) {
                    require_once MYBB_ROOT . 'inc/plugins/' . $plugin_file;
                    $codename = str_replace('.php', '', $plugin_file);
                    $infofunc = $codename . '_info';

                    if (!function_exists($infofunc)) {
                        continue;
                    }

                    $plugininfo = $infofunc();
                    $plugininfo['codename'] = $codename;

                    if (isset($active_plugins[$codename])) {
                        // This is an active plugin
                        $plugininfo['is_active'] = 1;

                        $a_plugins[] = $plugininfo;
                        continue;
                    }

                    // Either installed and not active or completely inactive
                    $i_plugins[] = $plugininfo;
                }

                $table = new Table;
                $table->construct_header($this->lang->plugin);
                $table->construct_header($this->lang->controls, ['colspan' => 2, 'class' => 'align_center', 'width' => 300]);

                if (empty($a_plugins)) {
                    $table->construct_cell($this->lang->no_active_plugins, ['colspan' => 3]);
                    $table->construct_row();
                } else {
                    $this->build_plugin_list($table, $a_plugins);
                }

                $html .= $table->output($this->lang->active_plugin);

                $table = new Table;
                $table->construct_header($this->lang->plugin);
                $table->construct_header($this->lang->controls, ['colspan' => 2, 'class' => 'align_center', 'width' => 300]);

                if (empty($i_plugins)) {
                    $table->construct_cell($this->lang->no_inactive_plugins, ['colspan' => 3]);
                    $table->construct_row();
                } else {
                    $this->build_plugin_list($table, $i_plugins);
                }

                $html .= $table->output($this->lang->inactive_plugin);
            } else {
                // No plugins
                $table = new Table;
                $table->construct_header($this->lang->plugin);
                $table->construct_header($this->lang->controls, ['colspan' => 2, 'class' => 'align_center', 'width' => 300]);

                $table->construct_cell($this->lang->no_plugins, ['colspan' => 3]);
                $table->construct_row();

                $html .= $table->output($this->lang->plugins);
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Plugins.html.twig');
        }
    }

    /**
     * @return array
     */
    private function get_plugins_list()
    {
        // Get a list of the plugin files which exist in the plugins directory
        $dir = @opendir(MYBB_ROOT . 'inc/plugins/');
        if ($dir) {
            while ($file = readdir($dir)) {
                $ext = get_extension($file);
                if ($ext == 'php') {
                    $plugins_list[] = $file;
                }
            }
            @sort($plugins_list);
        }
        @closedir($dir);

        return $plugins_list;
    }

    /**
     * @param array $plugin_list
     */
    private function build_plugin_list(& $table, $plugin_list)
    {
        foreach ($plugin_list as $plugininfo) {
            if ($plugininfo['website']) {
                $plugininfo['name'] = "<a href=\"" . $plugininfo['website'] . "\">" . $plugininfo['name'] . "</a>";
            }

            if ($plugininfo['authorsite']) {
                $plugininfo['author'] = "<a href=\"" . $plugininfo['authorsite'] . "\">" . $plugininfo['author'] . "</a>";
            }

            if ($this->plugins->is_compatible($plugininfo['codename']) == false) {
                $compatibility_warning = "<span style=\"color: red;\">" . $this->lang->sprintf($this->lang->plugin_incompatible, $this->bb->version) . "</span>";
            } else {
                $compatibility_warning = '';
            }

            $installed_func = "{$plugininfo['codename']}_is_installed";
            $install_func = "{$plugininfo['codename']}_install";
            $uninstall_func = "{$plugininfo['codename']}_uninstall";

            $installed = true;
            $install_button = false;
            $uninstall_button = false;

            if (function_exists($installed_func) && $installed_func() != true) {
                $installed = false;
            }

            if (function_exists($install_func)) {
                $install_button = true;
            }

            if (function_exists($uninstall_func)) {
                $uninstall_button = true;
            }

            $table->construct_cell("<strong>{$plugininfo['name']}</strong> ({$plugininfo['version']})<br /><small>{$plugininfo['description']}</small><br /><i><small>{$this->lang->created_by} {$plugininfo['author']}</small></i>");

            // Plugin is not installed at all
            if ($installed == false) {
                if ($compatibility_warning) {
                    $table->construct_cell("{$compatibility_warning}", ['class' => 'align_center', 'colspan' => 2]);
                } else {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/plugins?action=activate&amp;plugin={$plugininfo['codename']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->install_and_activate}</a>", ["class" => "align_center", "colspan" => 2]);
                }
            } // Plugin is activated and installed
            elseif ($plugininfo['is_active']) {
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/plugins?action=deactivate&amp;plugin={$plugininfo['codename']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->deactivate}</a>", ["class" => "align_center", "width" => 150]);
                if ($uninstall_button) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/plugins?action=deactivate&amp;uninstall=1&amp;plugin={$plugininfo['codename']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->uninstall}</a>", ["class" => "align_center", "width" => 150]);
                } else {
                    $table->construct_cell('&nbsp;', ['class' => 'align_center', 'width' => 150]);
                }
            } // Plugin is installed but not active
            elseif ($installed == true) {
                if ($compatibility_warning && !$uninstall_button) {
                    $table->construct_cell("{$compatibility_warning}", ['class' => 'align_center', 'colspan' => 2]);
                } else {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/plugins?action=activate&amp;plugin={$plugininfo['codename']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->activate}</a>", ["class" => "align_center", "width" => 150]);
                    if ($uninstall_button) {
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/plugins?action=deactivate&amp;uninstall=1&amp;plugin={$plugininfo['codename']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->uninstall}</a>", ["class" => "align_center", "width" => 150]);
                    } else {
                        $table->construct_cell('&nbsp;', ['class' => 'align_center', 'width' => 150]);
                    }
                }
            }
            $table->construct_row();
        }
    }
}
