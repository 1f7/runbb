<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class PostIcons extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_post_icons', false, true);

        $this->page->add_breadcrumb_item($this->lang->post_icons, $this->bb->admin_url . '/config/post_icons');

        $this->plugins->runHooks('admin_config_post_icons_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_post_icons_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['path'])) {
                    $errors[] = $this->lang->error_missing_path;
                }

                if (!isset($errors)) {
                    $new_icon = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'path' => $this->db->escape_string($this->bb->input['path'])
                    ];

                    $iid = $this->db->insert_query('icons', $new_icon);

                    $this->plugins->runHooks('admin_config_post_icons_add_commit');

                    $this->cache->update_posticons();

                    // Log admin action
                    $this->bblogger->log_admin_action($iid, htmlspecialchars_uni($this->bb->input['name']));

                    $this->session->flash_message($this->lang->success_post_icon_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_post_icon);
            $html = $this->page->output_header($this->lang->post_icons . ' - ' . $this->lang->add_post_icon);

            $sub_tabs['manage_icons'] = [
                'title' => $this->lang->manage_post_icons,
                'link' => $this->bb->admin_url . '/config/post_icons'
            ];

            $sub_tabs['add_icon'] = [
                'title' => $this->lang->add_post_icon,
                'link' => $this->bb->admin_url . '/config/post_icons?action=add',
                'description' => $this->lang->add_post_icon_desc
            ];

            $sub_tabs['add_multiple'] = [
                'title' => $this->lang->add_multiple_post_icons,
                'link' => $this->bb->admin_url . '/config/post_icons?action=add_multiple'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_icon');

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['path'] = 'images/icons/';
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/config/post_icons?action=add', 'post', 'add');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->add_post_icon);
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc, $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->image_path . ' <em>*</em>', $this->lang->image_path_desc, $form->generate_text_box('path', $this->bb->input['path'], ['id' => 'path']), 'path');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_post_icon);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/PostIconsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'add_multiple') {
            $this->plugins->runHooks('admin_config_post_icons_add_multiple');

            if ($this->bb->request_method == 'post') {
                if ($this->bb->input['step'] == 1) {
                    if (!trim($this->bb->input['pathfolder'])) {
                        $errors[] = $this->lang->error_missing_path_multiple;
                    }

                    $path = $this->bb->input['pathfolder'];
                    $dir = @opendir(MYBB_ROOT . $path);
                    if (!$dir) {
                        $errors[] = $this->lang->error_invalid_path;
                    }

                    if (substr($path, -1, 1) !== '/') {
                        $path .= '/';
                    }

                    $query = $this->db->simple_select('icons');

                    $aicons = [];
                    while ($icon = $this->db->fetch_array($query)) {
                        $aicons[$icon['path']] = 1;
                    }

                    $icons = [];
                    if (!$errors) {
                        while ($file = readdir($dir)) {
                            if ($file != '..' && $file != '.') {
                                $ext = get_extension($file);
                                if ($ext == 'gif' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'bmp') {
                                    if (!$aicons[$path . $file]) {
                                        $icons[] = $file;
                                    }
                                }
                            }
                        }
                        closedir($dir);

                        if (count($icons) == 0) {
                            $errors[] = $this->lang->error_no_images;
                        }
                    }

                    // Check for errors again (from above statement)!
                    if (!$errors) {
                        // We have no errors so let's proceed!
                        $this->page->add_breadcrumb_item($this->lang->add_multiple_post_icons);
                        $html = $this->page->output_header($this->lang->post_icons . ' - ' . $this->lang->add_multiple_post_icons);

                        $sub_tabs['manage_icons'] = [
                            'title' => $this->lang->manage_post_icons,
                            'link' => $this->bb->admin_url . '/config/post_icons'
                        ];

                        $sub_tabs['add_icon'] = [
                            'title' => $this->lang->add_post_icon,
                            'link' => $this->bb->admin_url . '/config/post_icons?action=add'
                        ];

                        $sub_tabs['add_multiple'] = [
                            'title' => $this->lang->add_multiple_post_icons,
                            'link' => $this->bb->admin_url . '/config/post_icons?action=add_multiple',
                            'description' => $this->lang->add_multiple_post_icons_desc
                        ];

                        $html .= $this->page->output_nav_tabs($sub_tabs, 'add_multiple');

                        $form = new Form($this->bb, $this->bb->admin_url . '/config/post_icons?action=add_multiple', 'post', 'add_multiple');
                        $html .= $form->getForm();
                        $html .= $form->generate_hidden_field('step', '2');
                        $html .= $form->generate_hidden_field('pathfolder', $path);

                        $form_container = new FormContainer($this, $this->lang->add_multiple_post_icons);
                        $form_container->output_row_header($this->lang->image, ['class' => 'align_center', 'width' => '10%']);
                        $form_container->output_row_header($this->lang->name);
                        $form_container->output_row_header($this->lang->add, ['class' => 'align_center', 'width' => '5%']);

                        foreach ($icons as $key => $file) {
                            $ext = get_extension($file);
                            $find = str_replace('.' . $ext, '', $file);
                            $name = ucfirst($find);

                            $form_container->output_cell("<img src=\"../" . $path . $file . "\" alt=\"\" /><br /><small>{$file}</small>", ["class" => "align_center", "width" => 1]);
                            $form_container->output_cell($form->generate_text_box("name[{$file}]", $name, ['id' => 'name', 'style' => 'width: 98%']));
                            $form_container->output_cell($form->generate_check_box("include[{$file}]", 1, "", ['checked' => 1]), ["class" => "align_center"]);
                            $form_container->construct_row();
                        }

                        if ($form_container->num_rows() == 0) {
                            $this->session->flash_message($this->lang->error_no_images, 'error');
                            return $response->withRedirect($this->bb->admin_url . '/config/post_icons?action=add_multiple');
                        }

                        $html .= $form_container->end();

                        $buttons[] = $form->generate_submit_button($this->lang->save_post_icons);
                        $html .= $form->output_submit_wrapper($buttons);
                        $html .= $form->end();

                        $this->page->output_footer();
                        tdie(' here ');
//                        $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
//                        return $this->view->render($response, '@forum/Admin/Config/PostIconsEdit.html.twig');
                    }
                } else {
                    $path = $this->bb->input['pathfolder'];
                    reset($this->bb->input['include']);
                    $name = $this->bb->input['name'];

                    if (empty($this->bb->input['include'])) {
                        $this->session->flash_message($this->lang->error_none_included, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/config/post_icons?action=add_multiple');
                    }

                    foreach ($this->bb->input['include'] as $image => $insert) {
                        if ($insert) {
                            $new_icon = [
                                'name' => $this->db->escape_string($name[$image]),
                                'path' => $this->db->escape_string($path . $image)
                            ];

                            $this->db->insert_query('icons', $new_icon);
                        }
                    }

                    $this->plugins->runHooks('admin_config_post_icons_add_multiple_commit');

                    $this->cache->update_posticons();

                    // Log admin action
                    $this->bblogger->log_admin_action();

                    $this->session->flash_message($this->lang->success_post_icons_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_multiple_post_icons);
            $html = $this->page->output_header($this->lang->post_icons . ' - ' . $this->lang->add_multiple_post_icons);

            $sub_tabs['manage_icons'] = [
                'title' => $this->lang->manage_post_icons,
                'link' => $this->bb->admin_url . '/config/post_icons'
            ];

            $sub_tabs['add_icon'] = [
                'title' => $this->lang->add_post_icon,
                'link' => $this->bb->admin_url . '/config/post_icons?action=add'
            ];

            $sub_tabs['add_multiple'] = [
                'title' => $this->lang->add_multiple_post_icons,
                'link' => $this->bb->admin_url . '/config/post_icons?action=add_multiple',
                'description' => $this->lang->add_multiple_post_icons_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_multiple');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/post_icons?action=add_multiple', 'post', 'add_multiple');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('step', '1');

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_multiple_post_icons);
            $form_container->output_row($this->lang->path_to_images . ' <em>*</em>', $this->lang->path_to_images_desc, $form->generate_text_box('pathfolder', $this->bb->getInput('pathfolder', ''), ['id' => 'pathfolder']), 'pathfolder');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->show_post_icons);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/PostIconsAddMultiple.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('icons', '*', "iid='" . $this->bb->getInput('iid', 0) . "'");
            $icon = $this->db->fetch_array($query);

            if (!$icon['iid']) {
                $this->session->flash_message($this->lang->error_invalid_post_icon, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
            }

            $this->plugins->runHooks('admin_config_post_icons_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['path'])) {
                    $errors[] = $this->lang->error_missing_path;
                }

                if (!isset($errors)) {
                    $updated_icon = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'path' => $this->db->escape_string($this->bb->input['path'])
                    ];

                    $this->plugins->runHooks('admin_config_post_icons_edit_commit');

                    $this->db->update_query('icons', $updated_icon, "iid='{$icon['iid']}'");

                    $this->cache->update_posticons();

                    // Log admin action
                    $this->bblogger->log_admin_action($icon['iid'], htmlspecialchars_uni($this->bb->input['name']));

                    $this->session->flash_message($this->lang->success_post_icon_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_post_icon);
            $html = $this->page->output_header($this->lang->post_icons . ' - ' . $this->lang->edit_post_icon);

            $sub_tabs['edit_icon'] = [
                'title' => $this->lang->edit_post_icon,
                'link' => $this->bb->admin_url . '/config/post_icons',
                'description' => $this->lang->edit_post_icon_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_icon');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/post_icons?action=edit', 'post', 'edit');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('iid', $icon['iid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, $icon);
            }

            $form_container = new FormContainer($this, $this->lang->edit_post_icon);
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->image_path . ' <em>*</em>', $this->lang->image_path_desc, $form->generate_text_box('path', $this->bb->input['path'], ['id' => 'path']), 'path');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_post_icon);
            $buttons[] = $form->generate_reset_button($this->lang->reset);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/PostIconsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('icons', '*', "iid='" . $this->bb->getInput('iid', 0) . "'");
            $icon = $this->db->fetch_array($query);

            if (!$icon['iid']) {
                $this->session->flash_message($this->lang->error_invalid_post_icon, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
            }

            $this->plugins->runHooks('admin_config_post_icons_delete');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('icons', "iid='{$icon['iid']}'");

                $this->plugins->runHooks('admin_config_post_icons_delete_commit');

                $this->cache->update_posticons();

                // Log admin action
                $this->bblogger->log_admin_action($icon['iid'], htmlspecialchars_uni($icon['name']));

                $this->session->flash_message($this->lang->success_post_icon_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/post_icons');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/post_icons?action=delete&iid=' . $icon['iid'], $this->lang->confirm_post_icon_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_post_icons_start');

            $html = $this->page->output_header($this->lang->post_icons);

            $sub_tabs['manage_icons'] = [
                'title' => $this->lang->manage_post_icons,
                'link' => $this->bb->admin_url . '/config/post_icons',
                'description' => $this->lang->manage_post_icons_desc
            ];

            $sub_tabs['add_icon'] = [
                'title' => $this->lang->add_post_icon,
                'link' => $this->bb->admin_url . '/config/post_icons?action=add'
            ];

            $sub_tabs['add_multiple'] = [
                'title' => $this->lang->add_multiple_post_icons,
                'link' => $this->bb->admin_url . '/config/post_icons?action=add_multiple'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_icons');

            $pagenum = $this->bb->getInput('page', 0);
            if ($pagenum) {
                $start = ($pagenum - 1) * 20;
            } else {
                $start = 0;
                $pagenum = 1;
            }

            $table = new Table;
            $table->construct_header($this->lang->image, ['class' => 'align_center', 'width' => 1]);
            $table->construct_header($this->lang->name, ['width' => '70%']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('icons', '*', '', ['limit_start' => $start, 'limit' => 20, 'order_by' => 'name']);
            while ($icon = $this->db->fetch_array($query)) {
                $icon['path'] = str_replace('{theme}', 'images', $icon['path']);
                if (my_strpos($icon['path'], 'p://') || substr($icon['path'], 0, 1) == '/') {
                    $image = $icon['path'];
                } else {
                    $image = $this->bb->asset_url . '/' . $icon['path'];
                }

                $table->construct_cell("<img src=\"" . htmlspecialchars_uni($image) . "\" alt=\"\" />", ["class" => "align_center"]);
                $table->construct_cell(htmlspecialchars_uni($icon['name']));

                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/post_icons?action=edit&amp;iid={$icon['iid']}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/post_icons?action=delete&amp;iid={$icon['iid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_post_icon_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_post_icons, ['colspan' => 4]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->manage_post_icons);

            $query = $this->db->simple_select('icons', 'COUNT(iid) AS icons');
            $total_rows = $this->db->fetch_field($query, 'icons');

            $html .= '<br />' . $this->adm->draw_admin_pagination($pagenum, '20', $total_rows, $this->bb->admin_url . '/config/post_icons?page={page}');

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/PostIcons.html.twig');
        }
    }
}
