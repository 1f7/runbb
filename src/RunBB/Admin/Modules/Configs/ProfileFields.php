<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class ProfileFields extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_profile_fields', false, true);

        $this->page->add_breadcrumb_item($this->lang->custom_profile_fields, $this->bb->admin_url . '/config/profile_fields');

        $this->plugins->runHooks('admin_config_profile_fields_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_profile_fields_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['description'])) {
                    $errors[] = $this->lang->error_missing_description;
                }

                if (!trim($this->bb->input['fieldtype'])) {
                    $errors[] = $this->lang->error_missing_fieldtype;
                }

                if (!isset($errors)) {
                    $type = $this->bb->input['fieldtype'];
                    $options = preg_replace("#(\r\n|\r|\n)#s", "\n", trim($this->bb->input['options']));
                    if ($type != 'text' && $type != 'textarea') {
                        $thing = "$type\n$options";
                    } else {
                        $thing = $type;
                    }

                    foreach (['viewableby', 'editableby'] as $key) {
                        if ($this->bb->input[$key] == 'all') {
                            $this->bb->input[$key] = -1;
                        } elseif ($this->bb->input[$key] == 'custom') {
                            if (isset($this->bb->input['select'][$key]) && is_array($this->bb->input['select'][$key])) {
                                foreach ($this->bb->input['select'][$key] as &$val) {
                                    $val = (int)$val;
                                }
                                unset($val);

                                $this->bb->input[$key] = implode(',', (array)$this->bb->input['select'][$key]);
                            } else {
                                $this->bb->input[$key] = '';
                            }
                        } else {
                            $this->bb->input[$key] = '';
                        }
                    }

                    $new_profile_field = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'type' => $this->db->escape_string($thing),
                        'regex' => $this->db->escape_string($this->bb->input['regex']),
                        'length' => $this->bb->getInput('length', 0),
                        'maxlength' => $this->bb->getInput('maxlength', 0),
                        'required' => $this->bb->getInput('required', 0),
                        'registration' => $this->bb->getInput('registration', 0),
                        'profile' => $this->bb->getInput('profile', 0),
                        'viewableby' => $this->db->escape_string($this->bb->input['viewableby']),
                        'editableby' => $this->db->escape_string($this->bb->input['editableby']),
                        'postbit' => $this->bb->getInput('postbit', 0),
                        'postnum' => $this->bb->getInput('postnum', 0),
                        'allowhtml' => $this->bb->getInput('allowhtml', 0),
                        'allowmycode' => $this->bb->getInput('allowmycode', 0),
                        'allowsmilies' => $this->bb->getInput('allowsmilies', 0),
                        'allowimgcode' => $this->bb->getInput('allowimgcode', 0),
                        'allowvideocode' => $this->bb->getInput('allowvideocode', 0)
                    ];

                    $fid = $this->db->insert_query('profilefields', $new_profile_field);

                    $this->db->write_query('ALTER TABLE ' . TABLE_PREFIX . "userfields ADD fid{$fid} TEXT");

                    $this->plugins->runHooks('admin_config_profile_fields_add_commit');

                    $this->cache->update_profilefields();

                    // Log admin action
                    $this->bblogger->log_admin_action($fid, htmlspecialchars_uni($this->bb->input['name']));

                    $this->session->flash_message($this->lang->success_profile_field_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/profile_fields');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_profile_field);
            $html = $this->page->output_header($this->lang->custom_profile_fields . ' - ' . $this->lang->add_new_profile_field);

            $sub_tabs['custom_profile_fields'] = [
                'title' => $this->lang->custom_profile_fields,
                'link' => $this->bb->admin_url . '/config/profile_fields'
            ];

            $sub_tabs['add_profile_field'] = [
                'title' => $this->lang->add_new_profile_field,
                'link' => $this->bb->admin_url . '/config/profile_fields?action=add',
                'description' => $this->lang->add_new_profile_field_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_profile_field');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/profile_fields?action=add', 'post', 'add');
            $html .= $form->getForm();

            if (isset($errors)) {
                switch ($this->bb->input['viewableby']) {
                    case 'all':
                        $this->bb->input['viewableby'] = -1;
                        break;
                    case 'custom':
                        $this->bb->input['viewableby'] = implode(',', (array)$this->bb->input['select']['viewableby']);
                        break;
                    default:
                        $this->bb->input['viewableby'] = '';
                        break;
                }

                switch ($this->bb->input['editableby']) {
                    case 'all':
                        $this->bb->input['editableby'] = -1;
                        break;
                    case 'custom':
                        $this->bb->input['editableby'] = implode(',', (array)$this->bb->input['select']['editableby']);
                        break;
                    default:
                        $this->bb->input['editableby'] = '';
                        break;
                }

                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['fieldtype'] = 'textbox';
                $this->bb->input['required'] = 0;
                $this->bb->input['registration'] = 0;
                $this->bb->input['editable'] = 1;
                $this->bb->input['hidden'] = 0;
                $this->bb->input['postbit'] = 0;
            }

            if (empty($this->bb->input['viewableby'])) {
                $this->bb->input['viewableby'] = '';
            }

            if (empty($this->bb->input['editableby'])) {
                $this->bb->input['editableby'] = '';
            }

            $form_container = new FormContainer($this, $this->lang->add_new_profile_field);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->getInput('name', '', true), ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');
            $select_list = [
                'text' => $this->lang->text,
                'textarea' => $this->lang->textarea,
                'select' => $this->lang->select,
                'multiselect' => $this->lang->multiselect,
                'radio' => $this->lang->radio,
                'checkbox' => $this->lang->checkbox
            ];
            $form_container->output_row($this->lang->field_type . ' <em>*</em>', $this->lang->field_type_desc, $form->generate_select_box('fieldtype', $select_list, $this->bb->input['fieldtype'], ['id' => 'fieldtype']), 'fieldtype');
            $form_container->output_row($this->lang->field_regex, $this->lang->field_regex_desc, $form->generate_text_box('regex', $this->bb->getInput('regex', ''), ['id' => 'regex']), 'regex', [], ['id' => 'row_regex']);
            $form_container->output_row($this->lang->maximum_length, $this->lang->maximum_length_desc, $form->generate_numeric_field('maxlength', $this->bb->getInput('maxlength', 0), ['id' => 'maxlength', 'min' => 0]), 'maxlength', [], ['id' => 'row_maxlength']);
            $form_container->output_row($this->lang->field_length, $this->lang->field_length_desc, $form->generate_numeric_field('length', $this->bb->getInput('length', 0), ['id' => 'length', 'min' => 0]), 'length', [], ['id' => 'row_fieldlength']);
            $form_container->output_row($this->lang->selectable_options, $this->lang->selectable_options_desc, $form->generate_text_area('options', $this->bb->getInput('options', ''), ['id' => 'options']), 'options', [], ['id' => 'row_options']);
            $form_container->output_row($this->lang->min_posts_enabled, $this->lang->min_posts_enabled_desc, $form->generate_numeric_field('postnum', $this->bb->getInput('postnum', ''), ['id' => 'postnum', 'min' => 0]), 'postnum');
            $form_container->output_row($this->lang->display_order . ' <em>*</em>', $this->lang->display_order_desc, $form->generate_numeric_field('disporder', $this->bb->getInput('disporder', ''), ['id' => 'disporder', 'min' => 0]), 'disporder');
            $form_container->output_row($this->lang->required . ' <em>*</em>', $this->lang->required_desc, $form->generate_yes_no_radio('required', $this->bb->input['required']));
            $form_container->output_row($this->lang->show_on_registration . ' <em>*</em>', $this->lang->show_on_registration_desc, $form->generate_yes_no_radio('registration', $this->bb->input['registration']));
            $form_container->output_row($this->lang->display_on_profile . ' <em>*</em>', $this->lang->display_on_profile_desc, $form->generate_yes_no_radio('profile', $this->bb->getInput('profile', '')));
            $form_container->output_row($this->lang->display_on_postbit . ' <em>*</em>', $this->lang->display_on_postbit_desc, $form->generate_yes_no_radio('postbit', $this->bb->input['postbit']));

            $selected_values = '';
            if ($this->bb->input['viewableby'] != '' && $this->bb->input['viewableby'] != -1) {
                $selected_values = explode(',', $this->bb->getInput('viewableby', ''));

                foreach ($selected_values as &$value) {
                    $value = (int)$value;
                }
                unset($value);
            }

            $group_checked = ['all' => '', 'custom' => '', 'none' => ''];
            if ($this->bb->input['viewableby'] == -1) {
                $group_checked['all'] = 'checked="checked"';
            } elseif ($this->bb->input['viewableby'] != '') {
                $group_checked['custom'] = 'checked="checked"';
            } else {
                $group_checked['none'] = 'checked="checked"';
            }

            $html .= $this->adm->print_selection_javascript();

            $select_code = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"viewableby\" value=\"all\" {$group_checked['all']} class=\"viewableby_forums_groups_check\" onclick=\"checkAction('viewableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"viewableby\" value=\"custom\" {$group_checked['custom']} class=\"viewableby_forums_groups_check\" onclick=\"checkAction('viewableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"viewableby_forums_groups_custom\" class=\"viewableby_forums_groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('select[viewableby][]', $selected_values, ['id' => 'viewableby', 'multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"viewableby\" value=\"none\" {$group_checked['none']} class=\"viewableby_forums_groups_check\" onclick=\"checkAction('viewableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->none}</strong></label></dt>
	</dl>
	<script type=\"text/javascript\">
		checkAction('viewableby');
	</script>";
            $form_container->output_row($this->lang->viewableby, $this->lang->viewableby_desc, $select_code, '', [], ['id' => 'row_viewableby']);

            $selected_values = '';
            if ($this->bb->input['editableby'] != '' && $this->bb->input['editableby'] != -1) {
                $selected_values = explode(',', $this->bb->getInput('editableby', ''));

                foreach ($selected_values as &$value) {
                    $value = (int)$value;
                }
                unset($value);
            }

            $group_checked = ['all' => '', 'custom' => '', 'none' => ''];
            if ($this->bb->input['editableby'] == -1) {
                $group_checked['all'] = 'checked="checked"';
            } elseif ($this->bb->input['editableby'] != '') {
                $group_checked['custom'] = 'checked="checked"';
            } else {
                $group_checked['none'] = 'checked="checked"';
            }

            $html .= $this->adm->print_selection_javascript();

            $select_code = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"editableby\" value=\"all\" {$group_checked['all']} class=\"editableby_forums_groups_check\" onclick=\"checkAction('editableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"editableby\" value=\"custom\" {$group_checked['custom']} class=\"editableby_forums_groups_check\" onclick=\"checkAction('editableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"editableby_forums_groups_custom\" class=\"editableby_forums_groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('select[editableby][]', $selected_values, ['id' => 'editableby', 'multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"editableby\" value=\"none\" {$group_checked['none']} class=\"editableby_forums_groups_check\" onclick=\"checkAction('editableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->none}</strong></label></dt>
	</dl>
	<script type=\"text/javascript\">
		checkAction('editableby');
	</script>";
            $form_container->output_row($this->lang->editableby, $this->lang->editableby_desc, $select_code, '', [], ['id' => 'row_editableby']);

            $parser_options = [
                $form->generate_check_box('allowhtml', 1, $this->lang->parse_allowhtml, ['checked' => $this->bb->getInput('allowhtml', 0), 'id' => 'allowhtml']),
                $form->generate_check_box('allowmycode', 1, $this->lang->parse_allowmycode, ['checked' => $this->bb->getInput('allowmycode', 0), 'id' => 'allowmycode']),
                $form->generate_check_box('allowsmilies', 1, $this->lang->parse_allowsmilies, ['checked' => $this->bb->getInput('allowsmilies', 0), 'id' => 'allowsmilies']),
                $form->generate_check_box('allowimgcode', 1, $this->lang->parse_allowimgcode, ['checked' => $this->bb->getInput('allowimgcode', 0), 'id' => 'allowimgcode']),
                $form->generate_check_box('allowvideocode', 1, $this->lang->parse_allowvideocode, ['checked' => $this->bb->getInput('allowvideocode', 0), 'id' => 'allowvideocode'])
            ];
            $form_container->output_row($this->lang->parser_options, '', implode('<br />', $parser_options), '', [], ['id' => 'row_parser_options']);
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_profile_field);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

//            echo '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/peeker.js?ver=1804"></script>
//	<script type="text/javascript">
//		$(document).ready(function() {
//				new Peeker($("#fieldtype"), $("#row_maxlength, #row_regex, #row_parser_options"), /text|textarea/, false);
//				new Peeker($("#fieldtype"), $("#row_fieldlength"), /select|multiselect/, false);
//				new Peeker($("#fieldtype"), $("#row_options"), /select|radio|checkbox/, false);
//				// Add a star to the extra row since the "extra" is required if the box is shown
//				add_star("row_maxlength");
//				add_star("row_fieldlength");
//				add_star("row_options");
//		});
//	</script>';

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ProfileFieldsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('profilefields', '*', "fid = '" . $this->bb->getInput('fid', 0) . "'");
            $profile_field = $this->db->fetch_array($query);

            if (!$profile_field['fid']) {
                $this->session->flash_message($this->lang->error_invalid_fid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/profile_fields');
            }

            $this->plugins->runHooks('admin_config_profile_fields_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['description'])) {
                    $errors[] = $this->lang->error_missing_description;
                }

                if (!trim($this->bb->input['fieldtype'])) {
                    $errors[] = $this->lang->error_missing_fieldtype;
                }

                $type = $this->bb->input['fieldtype'];
                $options = preg_replace("#(\r\n|\r|\n)#s", "\n", trim($this->bb->input['options']));
                if ($type != 'text' && $type != 'textarea') {
                    $type = "$type\n$options";
                }

                if (!isset($errors)) {
                    foreach (['viewableby', 'editableby'] as $key) {
                        if ($this->bb->input[$key] == 'all') {
                            $this->bb->input[$key] = -1;
                        } elseif ($this->bb->input[$key] == 'custom') {
                            if (isset($this->bb->input['select'][$key]) && is_array($this->bb->input['select'][$key])) {
                                foreach ($this->bb->input['select'][$key] as &$val) {
                                    $val = (int)$val;
                                }
                                unset($val);

                                $this->bb->input[$key] = implode(',', (array)$this->bb->input['select'][$key]);
                            } else {
                                $this->bb->input[$key] = '';
                            }
                        } else {
                            $this->bb->input[$key] = '';
                        }
                    }

                    $updated_profile_field = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'type' => $this->db->escape_string($type),
                        'regex' => $this->db->escape_string($this->bb->input['regex']),
                        'length' => $this->bb->getInput('length', 0),
                        'maxlength' => $this->bb->getInput('maxlength', 0),
                        'required' => $this->bb->getInput('required', 0),
                        'registration' => $this->bb->getInput('registration', 0),
                        'profile' => $this->bb->getInput('profile', 0),
                        'viewableby' => $this->db->escape_string($this->bb->input['viewableby']),
                        'editableby' => $this->db->escape_string($this->bb->input['editableby']),
                        'postbit' => $this->bb->getInput('postbit', 0),
                        'postnum' => $this->bb->getInput('postnum', 0),
                        'allowhtml' => $this->bb->getInput('allowhtml', 0),
                        'allowmycode' => $this->bb->getInput('allowmycode', 0),
                        'allowsmilies' => $this->bb->getInput('allowsmilies', 0),
                        'allowimgcode' => $this->bb->getInput('allowimgcode', 0),
                        'allowvideocode' => $this->bb->getInput('allowvideocode', 0)
                    ];

                    $this->plugins->runHooks('admin_config_profile_fields_edit_commit');

                    $this->db->update_query('profilefields', $updated_profile_field, "fid='{$profile_field['fid']}'");

                    $this->cache->update_profilefields();

                    // Log admin action
                    $this->bblogger->log_admin_action($profile_field['fid'], htmlspecialchars_uni($this->bb->input['name']));

                    $this->session->flash_message($this->lang->success_profile_field_saved, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/profile_fields');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_profile_field);
            $html = $this->page->output_header($this->lang->custom_profile_fields . ' - ' . $this->lang->edit_profile_field);

            $sub_tabs['edit_profile_field'] = [
                'title' => $this->lang->edit_profile_field,
                'link' => $this->bb->admin_url . '/config/profile_fields?action=edit&fid=' . $profile_field['fid'],
                'description' => $this->lang->edit_profile_field_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_profile_field');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/profile_fields?action=edit', 'post', 'edit');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('fid', $profile_field['fid']);

            if (isset($errors)) {
                switch ($this->bb->input['viewableby']) {
                    case 'all':
                        $this->bb->input['viewableby'] = -1;
                        break;
                    case 'custom':
                        $this->bb->input['viewableby'] = implode(',', (array)$this->bb->input['select']['viewableby']);
                        break;
                    default:
                        $this->bb->input['viewableby'] = '';
                        break;
                }

                switch ($this->bb->input['editableby']) {
                    case 'all':
                        $this->bb->input['editableby'] = -1;
                        break;
                    case 'custom':
                        $this->bb->input['editableby'] = implode(',', (array)$this->bb->input['select']['editableby']);
                        break;
                    default:
                        $this->bb->input['editableby'] = '';
                        break;
                }

                $html .= $this->page->output_inline_error($errors);
            } else {
                $type = explode("\n", $profile_field['type'], '2');

                $this->bb->input = $profile_field;
                $this->bb->input['fieldtype'] = $type[0];
                $this->bb->input['options'] = $type[1];
            }

            if (empty($this->bb->input['viewableby'])) {
                $this->bb->input['viewableby'] = '';
            }

            if (empty($this->bb->input['editableby'])) {
                $this->bb->input['editableby'] = '';
            }

            $form_container = new FormContainer($this, $this->lang->edit_profile_field);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');
            $select_list = [
                'text' => $this->lang->text,
                'textarea' => $this->lang->textarea,
                'select' => $this->lang->select,
                'multiselect' => $this->lang->multiselect,
                'radio' => $this->lang->radio,
                'checkbox' => $this->lang->checkbox
            ];
            $form_container->output_row($this->lang->field_type . ' <em>*</em>', $this->lang->field_type_desc, $form->generate_select_box('fieldtype', $select_list, $this->bb->input['fieldtype'], ['id' => 'fieldtype']), 'fieldtype');
            $form_container->output_row($this->lang->field_regex, $this->lang->field_regex_desc, $form->generate_text_box('regex', $this->bb->input['regex'], ['id' => 'regex']), 'regex', [], ['id' => 'row_regex']);
            $form_container->output_row($this->lang->maximum_length, $this->lang->maximum_length_desc, $form->generate_numeric_field('maxlength', $this->bb->input['maxlength'], ['id' => 'maxlength', 'min' => 0]), 'maxlength', [], ['id' => 'row_maxlength']);
            $form_container->output_row($this->lang->field_length, $this->lang->field_length_desc, $form->generate_numeric_field('length', $this->bb->input['length'], ['id' => 'length', 'min' => 0]), 'length', [], ['id' => 'row_fieldlength']);
            $form_container->output_row($this->lang->selectable_options, $this->lang->selectable_options_desc, $form->generate_text_area('options', $this->bb->input['options'], ['id' => 'options']), 'options', [], ['id' => 'row_options']);
            $form_container->output_row($this->lang->min_posts_enabled, $this->lang->min_posts_enabled_desc, $form->generate_numeric_field('postnum', $this->bb->input['postnum'], ['id' => 'postnum', 'min' => 0]), 'postnum');
            $form_container->output_row($this->lang->display_order . ' <em>*</em>', $this->lang->display_order_desc, $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $form_container->output_row($this->lang->required . ' <em>*</em>', $this->lang->required_desc, $form->generate_yes_no_radio('required', $this->bb->input['required']));
            $form_container->output_row($this->lang->show_on_registration . ' <em>*</em>', $this->lang->show_on_registration_desc, $form->generate_yes_no_radio('registration', $this->bb->input['registration']));
            $form_container->output_row($this->lang->display_on_profile . ' <em>*</em>', $this->lang->display_on_profile_desc, $form->generate_yes_no_radio('profile', $this->bb->input['profile']));
            $form_container->output_row($this->lang->display_on_postbit . ' <em>*</em>', $this->lang->display_on_postbit_desc, $form->generate_yes_no_radio('postbit', $this->bb->input['postbit']));

            $selected_values = '';
            if ($this->bb->input['viewableby'] != '' && $this->bb->input['viewableby'] != -1) {
                $selected_values = explode(',', $this->bb->getInput('viewableby'));

                foreach ($selected_values as &$value) {
                    $value = (int)$value;
                }
                unset($value);
            }

            $group_checked = ['all' => '', 'custom' => '', 'none' => ''];
            if ($this->bb->input['viewableby'] == -1) {
                $group_checked['all'] = 'checked="checked"';
            } elseif ($this->bb->input['viewableby'] != '') {
                $group_checked['custom'] = 'checked="checked"';
            } else {
                $group_checked['none'] = 'checked="checked"';
            }

            $html .= $this->adm->print_selection_javascript();

            $select_code = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"viewableby\" value=\"all\" {$group_checked['all']} class=\"viewableby_forums_groups_check\" onclick=\"checkAction('viewableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"viewableby\" value=\"custom\" {$group_checked['custom']} class=\"viewableby_forums_groups_check\" onclick=\"checkAction('viewableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"viewableby_forums_groups_custom\" class=\"viewableby_forums_groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('select[viewableby][]', $selected_values, ['id' => 'viewableby', 'multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"viewableby\" value=\"none\" {$group_checked['none']} class=\"viewableby_forums_groups_check\" onclick=\"checkAction('viewableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->none}</strong></label></dt>
	</dl>
	<script type=\"text/javascript\">
		checkAction('viewableby');
	</script>";
            $form_container->output_row($this->lang->viewableby, $this->lang->viewableby_desc, $select_code, '', [], ['id' => 'row_viewableby']);

            $selected_values = '';
            if ($this->bb->input['editableby'] != '' && $this->bb->input['editableby'] != -1) {
                $selected_values = explode(',', $this->bb->getInput('editableby'));

                foreach ($selected_values as &$value) {
                    $value = (int)$value;
                }
                unset($value);
            }

            $group_checked = ['all' => '', 'custom' => '', 'none' => ''];
            if ($this->bb->input['editableby'] == -1) {
                $group_checked['all'] = 'checked="checked"';
            } elseif ($this->bb->input['editableby'] != '') {
                $group_checked['custom'] = 'checked="checked"';
            } else {
                $group_checked['none'] = 'checked="checked"';
            }

            $html .= $this->adm->print_selection_javascript();

            $select_code = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"editableby\" value=\"all\" {$group_checked['all']} class=\"editableby_forums_groups_check\" onclick=\"checkAction('editableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"editableby\" value=\"custom\" {$group_checked['custom']} class=\"editableby_forums_groups_check\" onclick=\"checkAction('editableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"editableby_forums_groups_custom\" class=\"editableby_forums_groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('select[editableby][]', $selected_values, ['id' => 'editableby', 'multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"editableby\" value=\"none\" {$group_checked['none']} class=\"editableby_forums_groups_check\" onclick=\"checkAction('editableby');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->none}</strong></label></dt>
	</dl>
	<script type=\"text/javascript\">
		checkAction('editableby');
	</script>";
            $form_container->output_row($this->lang->editableby, $this->lang->editableby_desc, $select_code, '', [], ['id' => 'row_editableby']);

            $parser_options = [
                $form->generate_check_box('allowhtml', 1, $this->lang->parse_allowhtml, ['checked' => $this->bb->input['allowhtml'], 'id' => 'allowhtml']),
                $form->generate_check_box('allowmycode', 1, $this->lang->parse_allowmycode, ['checked' => $this->bb->input['allowmycode'], 'id' => 'allowmycode']),
                $form->generate_check_box('allowsmilies', 1, $this->lang->parse_allowsmilies, ['checked' => $this->bb->input['allowsmilies'], 'id' => 'allowsmilies']),
                $form->generate_check_box('allowimgcode', 1, $this->lang->parse_allowimgcode, ['checked' => $this->bb->input['allowimgcode'], 'id' => 'allowimgcode']),
                $form->generate_check_box('allowvideocode', 1, $this->lang->parse_allowvideocode, ['checked' => $this->bb->input['allowvideocode'], 'id' => 'allowvideocode'])
            ];
            $form_container->output_row($this->lang->parser_options, '', implode('<br />', $parser_options), '', [], ['id' => 'row_parser_options']);
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_profile_field);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

//            echo '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/peeker.js?ver=1804"></script>
//	<script type="text/javascript">
//		$(document).ready(function() {
//				new Peeker($("#fieldtype"), $("#row_maxlength, #row_regex, #row_parser_options"), /text|textarea/);
//				new Peeker($("#fieldtype"), $("#row_fieldlength"), /select|multiselect/);
//				new Peeker($("#fieldtype"), $("#row_options"), /select|radio|checkbox/);
//				// Add a star to the extra row since the "extra" is required if the box is shown
//				add_star("row_maxlength");
//				add_star("row_fieldlength");
//				add_star("row_options");
//		});
//	</script>';

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ProfileFieldsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('profilefields', '*', "fid='" . $this->bb->getInput('fid', 0) . "'");
            $profile_field = $this->db->fetch_array($query);

            // Does the profile field not exist?
            if (!$profile_field['fid']) {
                $this->session->flash_message($this->lang->error_invalid_fid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/profile_fields');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/profile_fields');
            }

            $this->plugins->runHooks('admin_config_profile_fields_delete');

            if ($this->bb->request_method == 'post') {
                // Delete the profile field
                $this->db->delete_query('profilefields', "fid='{$profile_field['fid']}'");
                $this->db->write_query('ALTER TABLE ' . TABLE_PREFIX . "userfields DROP fid{$profile_field['fid']}");

                $this->plugins->runHooks('admin_config_profile_fields_delete_commit');

                $this->cache->update_profilefields();

                // Log admin action
                $this->bblogger->log_admin_action($profile_field['fid'], htmlspecialchars_uni($profile_field['name']));

                $this->session->flash_message($this->lang->success_profile_field_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/profile_fields');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/profile_fields?action=delete&amp;fid=' . $profile_field['fid'], $this->lang->confirm_profile_field_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_profile_fields_start');

            $html = $this->page->output_header($this->lang->custom_profile_fields);

            $sub_tabs['custom_profile_fields'] = [
                'title' => $this->lang->custom_profile_fields,
                'link' => $this->bb->admin_url . '/config/profile_fields',
                'description' => $this->lang->custom_profile_fields_desc
            ];

            $sub_tabs['add_profile_field'] = [
                'title' => $this->lang->add_new_profile_field,
                'link' => $this->bb->admin_url . '/config/profile_fields?action=add',
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'custom_profile_fields');

            $table = new Table;
            $table->construct_header($this->lang->name);
            $table->construct_header($this->lang->required, ['class' => 'align_center']);
            $table->construct_header($this->lang->registration, ['class' => 'align_center']);
            $table->construct_header($this->lang->editable, ['class' => 'align_center']);
            $table->construct_header($this->lang->profile, ['class' => 'align_center']);
            $table->construct_header($this->lang->postbit, ['class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center']);

            $query = $this->db->simple_select('profilefields', '*', '', ['order_by' => 'disporder']);
            while ($field = $this->db->fetch_array($query)) {
                if ($field['required']) {
                    $required = $this->lang->yes;
                } else {
                    $required = $this->lang->no;
                }

                if ($field['registration']) {
                    $registration = $this->lang->yes;
                } else {
                    $registration = $this->lang->no;
                }

                if ($field['editableby'] == '') {
                    $editable = $this->lang->no;
                } else {
                    $editable = $this->lang->yes;
                }

                if ($field['profile']) {
                    $profile = $this->lang->yes;
                } else {
                    $profile = $this->lang->no;
                }

                if ($field['postbit']) {
                    $postbit = $this->lang->yes;
                } else {
                    $postbit = $this->lang->no;
                }

                $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/config/profile_fields?action=edit&amp;fid={$field['fid']}\">" . htmlspecialchars_uni($field['name']) . "</a></strong><br /><small>" . htmlspecialchars_uni($field['description']) . "</small>", ['width' => '35%']);
                $table->construct_cell($required, ['class' => 'align_center', 'width' => '10%']);
                $table->construct_cell($registration, ['class' => 'align_center', 'width' => '10%']);
                $table->construct_cell($editable, ['class' => 'align_center', 'width' => '10%']);
                $table->construct_cell($profile, ['class' => 'align_center', 'width' => '10%']);
                $table->construct_cell($postbit, ['class' => 'align_center', 'width' => '10%']);

                $popup = new PopupMenu('field_'.$field['fid'], $this->lang->options);
                $popup->add_item($this->lang->edit_field, $this->bb->admin_url . '/config/profile_fields?action=edit&amp;fid=' . $field['fid']);
                $popup->add_item($this->lang->delete_field, $this->bb->admin_url . "/config/profile_fields?action=delete&amp;fid={$field['fid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_profile_field_deletion}')");
                $table->construct_cell($popup->fetch(), ['class' => 'align_center', 'width' => '20%']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_profile_fields, ['colspan' => 7]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->custom_profile_fields);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ProfileFields.html.twig');
        }
    }
}
