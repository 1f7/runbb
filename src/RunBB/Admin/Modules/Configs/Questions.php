<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Questions extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_questions', false, true);

        $this->page->add_breadcrumb_item($this->lang->security_questions, $this->bb->admin_url . '/config/questions');

        $this->plugins->runHooks('admin_config_questions_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_questions_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['question'])) {
                    $errors[] = $this->lang->error_missing_question;
                }

                if (!trim($this->bb->input['answer'])) {
                    $errors[] = $this->lang->error_missing_answer;
                }

                if (!isset($errors)) {
                    $answer = preg_replace("#(\r\n|\r|\n)#s", "\n", trim($this->bb->input['answer']));
                    $new_question = [
                        'question' => $this->db->escape_string($this->bb->input['question']),
                        'answer' => $this->db->escape_string($answer),
                        'active' => $this->bb->getInput('active', 0)
                    ];
                    $qid = $this->db->insert_query('questions', $new_question);

                    $this->plugins->runHooks('admin_config_questions_add_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($qid, $this->bb->input['question']);

                    $this->session->flash_message($this->lang->success_question_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/questions');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_question);
            $html = $this->page->output_header($this->lang->security_questions . ' - ' . $this->lang->add_new_question);

            $sub_tabs['security_questions'] = [
                'title' => $this->lang->security_questions,
                'link' => $this->bb->admin_url . '/config/questions'
            ];

            $sub_tabs['add_new_question'] = [
                'title' => $this->lang->add_new_question,
                'link' => $this->bb->admin_url . '/config/questions?action=add',
                'description' => $this->lang->add_new_question_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_new_question');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/questions?action=add', 'post', 'add');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['active'] = '1';
            }

            $form_container = new FormContainer($this, $this->lang->add_new_question);
            $form_container->output_row($this->lang->question . ' <em>*</em>', $this->lang->question_desc, $form->generate_text_area('question', $this->bb->getInput('question', '', true), ['id' => 'question']), 'question');
            $form_container->output_row($this->lang->answers . ' <em>*</em>', $this->lang->answers_desc, $form->generate_text_area('answer', $this->bb->getInput('answer', '', true), ['id' => 'answer']), 'answer');
            $form_container->output_row($this->lang->active . ' <em>*</em>', '', $form->generate_yes_no_radio('active', $this->bb->input['active']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_question);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/QuestionsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('questions', '*', "qid='" . $this->bb->getInput('qid', 0) . "'");
            $question = $this->db->fetch_array($query);

            if (!$question['qid']) {
                $this->session->flash_message($this->lang->error_invalid_question, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/questions');
            }

            $this->plugins->runHooks('admin_config_questions_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['question'])) {
                    $errors[] = $this->lang->error_missing_question;
                }

                if (!trim($this->bb->input['answer'])) {
                    $errors[] = $this->lang->error_missing_answer;
                }

                if (!isset($errors)) {
                    $answer = preg_replace("#(\r\n|\r|\n)#s", "\n", trim($this->bb->input['answer']));

                    $updated_question = [
                        'question' => $this->db->escape_string($this->bb->input['question']),
                        'answer' => $this->db->escape_string($answer),
                        'active' => $this->bb->getInput('active', 0)
                    ];

                    $this->plugins->runHooks('admin_config_questions_edit_commit');

                    $this->db->update_query('questions', $updated_question, "qid='{$question['qid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($question['qid'], $this->bb->input['question']);

                    $this->session->flash_message($this->lang->success_question_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/questions');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_question);
            $html = $this->page->output_header($this->lang->security_questions . ' - ' . $this->lang->edit_question);

            $sub_tabs['edit_question'] = [
                'title' => $this->lang->edit_question,
                'link' => $this->bb->admin_url . '/config/questions?action=edit&qid=' . $question['qid'],
                'description' => $this->lang->edit_question_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_question');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/questions?action=edit&qid=' . $question['qid'], 'post', 'add');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = $question;
            }

            $form_container = new FormContainer($this, $this->lang->edit_question);
            $form_container->output_row($this->lang->question . ' <em>*</em>', $this->lang->question_desc, $form->generate_text_area('question', $this->bb->input['question'], ['id' => 'question']), 'question');
            $form_container->output_row($this->lang->answers . ' <em>*</em>', $this->lang->answers_desc, $form->generate_text_area('answer', $this->bb->input['answer'], ['id' => 'answer']), 'answer');
            $form_container->output_row($this->lang->active . ' <em>*</em>', '', $form->generate_yes_no_radio('active', $this->bb->input['active']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_question);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/QuestionsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/questions');
            }

            $query = $this->db->simple_select('questions', '*', "qid='" . $this->bb->getInput('qid', 0) . "'");
            $question = $this->db->fetch_array($query);

            if (!$question['qid']) {
                $this->session->flash_message($this->lang->error_invalid_question, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/questions');
            }

            $this->plugins->runHooks('admin_config_questions_delete');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('questions', "qid='{$question['qid']}'");
                $this->db->delete_query('questionsessions', "qid='{$question['qid']}'");

                $this->plugins->runHooks('admin_config_questions_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($question['qid'], $question['question']);

                $this->session->flash_message($this->lang->success_question_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/questions');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/questions?action=delete&qid=' . $question['qid'], $this->lang->confirm_question_deletion);
            }
        }

        if ($this->bb->input['action'] == 'disable') {
            $query = $this->db->simple_select('questions', '*', "qid='" . $this->bb->getInput('qid', 0) . "'");
            $question = $this->db->fetch_array($query);

            if (!$question['qid']) {
                $this->session->flash_message($this->lang->error_invalid_question, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/questions');
            }

            $this->plugins->runHooks('admin_config_questions_disable');

            $update_question = [
                'active' => 0
            ];

            $this->plugins->runHooks('admin_config_questions_disable_commit');

            $this->db->update_query('questions', $update_question, "qid = '{$question['qid']}'");

            // Log admin action
            $this->bblogger->log_admin_action($question['qid'], $question['question']);

            $this->session->flash_message($this->lang->success_question_disabled, 'success');
            return $response->withRedirect($this->bb->admin_url . '/config/questions');
        }

        if ($this->bb->input['action'] == 'enable') {
            $query = $this->db->simple_select('questions', '*', "qid='" . $this->bb->getInput('qid', 0) . "'");
            $question = $this->db->fetch_array($query);

            if (!$question['qid']) {
                $this->session->flash_message($this->lang->error_invalid_question, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/questions');
            }

            $this->plugins->runHooks('admin_config_questions_enable');

            $update_question = [
                'active' => 1
            ];

            $this->plugins->runHooks('admin_config_questions_enable_commit');

            $this->db->update_query('questions', $update_question, "qid = '{$question['qid']}'");

            // Log admin action
            $this->bblogger->log_admin_action($question['qid'], $question['question']);

            $this->session->flash_message($this->lang->success_question_enabled, 'success');
            return $response->withRedirect($this->bb->admin_url . '/config/questions');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_questions_start');

            $html = $this->page->output_header($this->lang->security_questions);

            $sub_tabs['security_questions'] = [
                'title' => $this->lang->security_questions,
                'link' => $this->bb->admin_url . '/config/questions',
                'description' => $this->lang->security_questions_desc
            ];
            $sub_tabs['add_new_question'] = [
                'title' => $this->lang->add_new_question,
                'link' => $this->bb->admin_url . '/config/questions?action=add',
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'security_questions');

            $table = new Table;
            $table->construct_header($this->lang->question);
            $table->construct_header($this->lang->answers, ['width' => '35%']);
            $table->construct_header($this->lang->correct, ['width' => '5%', 'class' => 'align_center']);
            $table->construct_header($this->lang->incorrect, ['width' => '5%', 'class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            $query = $this->db->simple_select('questions', '*', '', ['order_by' => 'question']);
            while ($questions = $this->db->fetch_array($query)) {
                $questions['question'] = htmlspecialchars_uni($questions['question']);
                $questions['answer'] = htmlspecialchars_uni($questions['answer']);
                $questions['answer'] = preg_replace("#(\n)#s", "<br />", trim($questions['answer']));
                $questions['correct'] = $this->bb->my_number_format($questions['correct']);
                $questions['incorrect'] = $this->bb->my_number_format($questions['incorrect']);

                if ($questions['active'] == 1) {
                    $icon = "<img src=\"".$this->bb->admin_url."/styles/{$this->page->style}/images/icons/bullet_on.png\" alt=\"({$this->lang->alt_enabled})\" title=\"{$this->lang->alt_enabled}\"  style=\"vertical-align: middle;\" /> ";
                } else {
                    $icon = "<img src=\"".$this->bb->admin_url."/styles/{$this->page->style}/images/icons/bullet_off.png\" alt=\"({$this->lang->alt_disabled})\" title=\"{$this->lang->alt_disabled}\"  style=\"vertical-align: middle;\" /> ";
                }

                $table->construct_cell("<div>{$icon}{$questions['question']}</div>");
                $table->construct_cell($questions['answer']);
                $table->construct_cell($questions['correct'], ['class' => 'align_center']);
                $table->construct_cell($questions['incorrect'], ['class' => 'align_center']);
                $popup = new PopupMenu("questions_{$questions['qid']}", $this->lang->options);
                $popup->add_item($this->lang->edit_question, $this->bb->admin_url . '/config/questions?action=edit&qid=' . $questions['qid']);
                if ($questions['active'] == 1) {
                    $popup->add_item($this->lang->disable_question, $this->bb->admin_url . "/config/questions?action=disable&amp;qid={$questions['qid']}&amp;my_post_key={$this->bb->post_code}");
                } else {
                    $popup->add_item($this->lang->enable_question, $this->bb->admin_url . "/config/questions?action=enable&amp;qid={$questions['qid']}&amp;my_post_key={$this->bb->post_code}");
                }
                $popup->add_item($this->lang->delete_question, $this->bb->admin_url . "/config/questions?action=delete&amp;qid={$questions['qid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_question_deletion}')");
                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_security_questions, ['colspan' => 5]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->security_questions);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Questions.html.twig');
        }
    }
}
