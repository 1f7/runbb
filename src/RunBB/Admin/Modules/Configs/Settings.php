<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Settings extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_settings');//, false);

        $this->page->add_breadcrumb_item($this->lang->board_settings, $this->bb->admin_url . '/config');

        $this->plugins->runHooks('admin_config_settings_begin');

        // Creating a new setting group
        if ($this->bb->input['action'] == 'addgroup') {
            $this->plugins->runHooks('admin_config_settings_addgroup');
            $errors = [];
            if ($this->bb->request_method == 'post') {
                // Validate title
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_group_title;
                }

                // Validate identifier
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_group_name;
                }
                $query = $this->db->simple_select('settinggroups', 'title', "name='" . $this->db->escape_string($this->bb->input['name']) . "'");
                if ($this->db->num_rows($query) > 0) {
                    $dup_group_title = $this->db->fetch_field($query, 'title');
                    $errors[] = $this->lang->sprintf($this->lang->error_duplicate_group_name, $dup_group_title);
                }

                if (!$errors) {
                    $new_setting_group = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'isdefault' => 0
                    ];
                    $gid = $this->db->insert_query('settinggroups', $new_setting_group);

                    $this->plugins->runHooks('admin_config_settings_addgroup_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($gid, $this->bb->input['name']);

                    $this->session->flash_message($this->lang->success_setting_group_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_setting_group);
            $html = $this->page->output_header($this->lang->board_settings . ' - ' . $this->lang->add_new_setting_group);

            $sub_tabs['change_settings'] = [
                'title' => $this->lang->change_settings,
                'link' => $this->bb->admin_url . '/config'
            ];

            $sub_tabs['add_setting'] = [
                'title' => $this->lang->add_new_setting,
                'link' => $this->bb->admin_url . '/config?action=add'
            ];

            $sub_tabs['add_setting_group'] = [
                'title' => $this->lang->add_new_setting_group,
                'link' => $this->bb->admin_url . '/config?action=addgroup',
                'description' => $this->lang->add_new_setting_group_desc
            ];

            $sub_tabs['modify_setting'] = [
                'title' => $this->lang->modify_existing_settings,
                'link' => $this->bb->admin_url . '/config?action=manage'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_setting_group');

            $form = new Form($this->bb, $this->bb->admin_url . '/config?action=addgroup', 'post', 'add');
            $html .= $form->getForm();

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_new_setting_group);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, '', $form->generate_text_area('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $this->bb->getInput('disporder', ''), ['id' => 'disporder', 'min' => 0]), 'disporder');
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->group_name_desc, $form->generate_text_box('name', $this->bb->getInput('name', '', true), ['id' => 'name']), 'name');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->insert_new_setting_group);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SettingsAddGroup.html.twig');
        }

        // Edit setting group
        if ($this->bb->input['action'] == 'editgroup') {
            $query = $this->db->simple_select('settinggroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $group = $this->db->fetch_array($query);

            // Does the setting not exist?
            if (!$group['gid']) {
                $this->session->flash_message($this->lang->error_invalid_gid2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }
            // Prevent editing of default
            if ($group['isdefault'] == 1) {
                $this->session->flash_message($this->lang->error_cannot_edit_default, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            $this->plugins->runHooks('admin_config_settings_editgroup');

            // Do edit?
            if ($this->bb->request_method == 'post') {
                // Validate title
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_group_title;
                }

                // Validate identifier
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_group_name;
                }
                $query = $this->db->simple_select('settinggroups', 'title', "name='" . $this->db->escape_string($this->bb->input['name']) . "' AND gid != '{$group['gid']}'");
                if ($this->db->num_rows($query) > 0) {
                    $dup_group_title = $this->db->fetch_field($query, 'title');
                    $errors[] = $this->lang->sprintf($this->lang->error_duplicate_group_name, $dup_group_title);
                }

                if (!isset($errors)) {
                    $update_setting_group = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                    ];

                    $this->plugins->runHooks('admin_config_settings_editgroup_commit');

                    $this->db->update_query('settinggroups', $update_setting_group, "gid='{$group['gid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($group['gid'], $this->bb->input['name']);

                    $this->session->flash_message($this->lang->success_setting_group_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_setting_group);
            $html = $this->page->output_header($this->lang->board_settings . ' - ' . $this->lang->edit_setting_group);

            $sub_tabs['edit_setting_group'] = [
                'title' => $this->lang->edit_setting_group,
                'link' => $this->bb->admin_url . '/config?action=editgroup&gid=' . $group['gid'],
                'description' => $this->lang->edit_setting_group_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_setting_group');

            $form = new Form($this->bb, $this->bb->admin_url . '/config?action=editgroup', 'post', 'editgroup');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('gid', $group['gid']);

            if (isset($errors)) {
                $group_data = $this->bb->input;
                $html .= $this->page->output_inline_error($errors);
            } else {
                $group_data = $group;
            }

            $form_container = new FormContainer($this, $this->lang->edit_setting_group);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $group_data['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, '', $form->generate_text_area('description', $group_data['description'], ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $group_data['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->group_name_desc, $form->generate_text_box('name', $group_data['name'], ['id' => 'name']), 'name');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->update_setting_group);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SettingsEditGroup.html.twig');
        }

        // Delete Setting Group
        if ($this->bb->input['action'] == 'deletegroup') {
            $query = $this->db->simple_select('settinggroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $group = $this->db->fetch_array($query);

            // Does the setting group not exist?
            if (!$group['gid']) {
                $this->session->flash_message($this->lang->error_invalid_gid2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }
            // Prevent deletion of default
            if ($group['isdefault'] == 1) {
                $this->session->flash_message($this->lang->error_cannot_edit_default, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            $this->plugins->runHooks('admin_config_settings_deletegroup');

            if ($this->bb->request_method == 'post') {
                // Delete the setting group and its settings
                $this->db->delete_query('settinggroups', "gid='{$group['gid']}'");
                $this->db->delete_query('settings', "gid='{$group['gid']}'");

                (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

                $this->plugins->runHooks('admin_config_settings_deletegroup_commit');

                // Log admin action
                $this->bblogger->log_admin_action($group['gid'], $group['name']);

                $this->session->flash_message($this->lang->success_setting_group_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config?action=deletegroup&gid=' . $group['gid'], $this->lang->confirm_setting_group_deletion);
            }
        }

        // Creating a new setting
        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_settings_add');
            $errors = [];
            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                $query = $this->db->simple_select('settinggroups', 'gid', "gid='" . $this->bb->getInput('gid', 0) . "'");
                $gid = $this->db->fetch_field($query, 'gid');
                if (!$gid) {
                    $errors[] = $this->lang->error_invalid_gid;
                }

                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }
                $query = $this->db->simple_select('settings', 'title', "name='" . $this->db->escape_string($this->bb->input['name']) . "'");
                if ($this->db->num_rows($query) > 0) {
                    $dup_setting_title = $this->db->fetch_field($query, 'title');
                    $errors[] = $this->lang->sprintf($this->lang->error_duplicate_name, $dup_setting_title);
                }

                // do some type filtering
                $this->bb->input['type'] = str_replace("\n", '', $this->bb->input['type']);
                if (strtolower(substr($this->bb->input['type'], 0, 3)) == 'php') {
                    $this->bb->input['type'] = '';
                }

                if (!$this->bb->input['type']) {
                    $errors[] = $this->lang->error_invalid_type;
                }

                if (empty($errors)) {
                    if ($this->bb->input['type'] == 'custom') {
                        $options_code = $this->bb->input['extra'];
                    } elseif ($this->bb->input['extra']) {
                        $options_code = "{$this->bb->input['type']}\n{$this->bb->input['extra']}";
                    } else {
                        $options_code = $this->bb->input['type'];
                    }

                    $this->bb->input['name'] = str_replace("\\", '', $this->bb->input['name']);
                    $this->bb->input['name'] = str_replace('$', '', $this->bb->input['name']);
                    $this->bb->input['name'] = str_replace("'", '', $this->bb->input['name']);

                    if ($options_code == 'numeric') {
                        $value = $this->bb->getInput('value', 0);
                    } else {
                        $value = $this->db->escape_string($this->bb->input['value']);
                    }

                    $new_setting = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'optionscode' => $this->db->escape_string($options_code),
                        'value' => $value,
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'gid' => $this->bb->getInput('gid', 0)
                    ];

                    $sid = $this->db->insert_query('settings', $new_setting);
                    (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

                    $this->plugins->runHooks('admin_config_settings_add_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($sid, $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_setting_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_setting);
            $html = $this->page->output_header($this->lang->board_settings . ' - ' . $this->lang->add_new_setting);

            $sub_tabs['change_settings'] = [
                'title' => $this->lang->change_settings,
                'link' => $this->bb->admin_url . '/config'
            ];

            $sub_tabs['add_setting'] = [
                'title' => $this->lang->add_new_setting,
                'link' => $this->bb->admin_url . '/config?action=add',
                'description' => $this->lang->add_new_setting_desc
            ];

            $sub_tabs['add_setting_group'] = [
                'title' => $this->lang->add_new_setting_group,
                'link' => $this->bb->admin_url . '/config?action=addgroup'
            ];

            $sub_tabs['modify_setting'] = [
                'title' => $this->lang->modify_existing_settings,
                'link' => $this->bb->admin_url . '/config?action=manage'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_setting');

            $form = new Form($this->bb, $this->bb->admin_url . '/config?action=add', 'post', 'add');
            $html .= $form->getForm();

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_new_setting);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, '', $form->generate_text_area('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');

            $query = $this->db->simple_select('settinggroups', '*', '', ['order_by' => 'disporder']);
            while ($group = $this->db->fetch_array($query)) {
                $group_lang_var = "setting_group_{$group['name']}";
                if (isset($this->lang->$group_lang_var)) {
                    $options[$group['gid']] = htmlspecialchars_uni($this->lang->$group_lang_var);
                } else {
                    $options[$group['gid']] = htmlspecialchars_uni($group['title']);
                }
            }
            $form_container->output_row($this->lang->group . ' <em>*</em>', '', $form->generate_select_box('gid', $options, $this->bb->getInput('gid', 0), ['id' => 'gid']), 'gid');
            $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $this->bb->getInput('disporder', ''), ['id' => 'disporder', 'min' => 0]), 'disporder');

            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc, $form->generate_text_box('name', $this->bb->getInput('name', '', true), ['id' => 'name']), 'name');

            $setting_types = [
                'text' => $this->lang->text,
                'numeric' => $this->lang->numeric_text,
                'textarea' => $this->lang->textarea,
                'yesno' => $this->lang->yesno,
                'onoff' => $this->lang->onoff,
                'select' => $this->lang->select,
                'forumselect' => $this->lang->forum_selection_box,
                'forumselectsingle' => $this->lang->forum_selection_single,
                'groupselect' => $this->lang->group_selection_box,
                'groupselectsingle' => $this->lang->group_selection_single,
                'radio' => $this->lang->radio,
                'checkbox' => $this->lang->checkbox,
                'language' => $this->lang->language_selection_box,
                'adminlanguage' => $this->lang->adminlanguage,
                'cpstyle' => $this->lang->cpstyle
                //'php' => $this->lang->php // Internal Use Only
            ];

            $form_container->output_row($this->lang->type . ' <em>*</em>', '', $form->generate_select_box('type', $setting_types, $this->bb->getInput('type', ''), ['id' => 'type']), 'type');
            $form_container->output_row($this->lang->extra, $this->lang->extra_desc, $form->generate_text_area('extra', $this->bb->getInput('extra', ''), ['id' => 'extra']), 'extra', [], ['id' => 'row_extra']);
            $form_container->output_row($this->lang->value, '', $form->generate_text_area('value', $this->bb->getInput('value', ''), ['id' => 'value']), 'value');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->insert_new_setting);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

//            echo '<script type="text/javascript" src="./jscripts/peeker.js?ver=1804"></script>
//	<script type="text/javascript">
//		$(document).ready(function() {
//			new Peeker($("#type"), $("#row_extra"), /^(select|radio|checkbox|php)$/, false);
//		});
//		// Add a star to the extra row since the "extra" is required if the box is shown
//		add_star("row_extra");
//	</script>';

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SettingsAdd.html.twig');
        }

        // Editing a particular setting
        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('settings', '*', "sid='" . $this->bb->getInput('sid', 0) . "'");
            $setting = $this->db->fetch_array($query);

            // Does the setting not exist?
            if (!$setting['sid']) {
                $this->session->flash_message($this->lang->error_invalid_sid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config');
            }

            // Prevent editing of default
            if ($setting['isdefault'] == 1) {
                $this->session->flash_message($this->lang->error_cannot_edit_default, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            $this->plugins->runHooks('admin_config_settings_edit');

            $type = explode("\n", $setting['optionscode'], 2);
            $type = trim($type[0]);
            if ($type == 'php') {
                $this->session->flash_message($this->lang->error_cannot_edit_php, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }
            $errors = [];
            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }
                $query = $this->db->simple_select('settings', 'title', "name='" . $this->db->escape_string($this->bb->input['name']) . "' AND sid != '{$setting['sid']}'");
                if ($this->db->num_rows($query) > 0) {
                    $dup_setting_title = $this->db->fetch_field($query, 'title');
                    $errors[] = $this->lang->sprintf($this->lang->error_duplicate_name, $dup_setting_title);
                }

                // do some type filtering
                $this->bb->input['type'] = str_replace("\n", '', $this->bb->input['type']);
                if (strtolower(substr($this->bb->input['type'], 0, 3)) == 'php') {
                    $this->bb->input['type'] = '';
                }

                if (!$this->bb->input['type']) {
                    $errors[] = $this->lang->error_invalid_type;
                }

                if (empty($errors)) {
                    if ($this->bb->input['type'] == 'custom') {
                        $options_code = $this->bb->input['extra'];
                    } elseif ($this->bb->input['extra']) {
                        $options_code = "{$this->bb->input['type']}\n{$this->bb->input['extra']}";
                    } else {
                        $options_code = $this->bb->input['type'];
                    }

                    $this->bb->input['name'] = str_replace("\\", '', $this->bb->input['name']);
                    $this->bb->input['name'] = str_replace('$', '', $this->bb->input['name']);
                    $this->bb->input['name'] = str_replace("'", '', $this->bb->input['name']);

                    if ($options_code == 'numeric') {
                        $value = $this->bb->getInput('value', 0);
                    } else {
                        $value = $this->db->escape_string($this->bb->input['value']);
                    }

                    $updated_setting = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'optionscode' => $this->db->escape_string($options_code),
                        'value' => $value,
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'gid' => $this->bb->getInput('gid', 0)
                    ];

                    $this->plugins->runHooks('admin_config_settings_edit_commit');

                    $this->db->update_query('settings', $updated_setting, "sid='{$setting['sid']}'");
                    (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

                    // Log admin action
                    $this->bblogger->log_admin_action($setting['sid'], $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_setting_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_setting);
            $html = $this->page->output_header($this->lang->board_settings . ' - ' . $this->lang->edit_setting);

            $sub_tabs['change_settings'] = [
                'title' => $this->lang->change_settings,
                'link' => $this->bb->admin_url . '/config',
            ];

            $sub_tabs['add_setting'] = [
                'title' => $this->lang->add_new_setting,
                'link' => $this->bb->admin_url . '/config?action=add'
            ];

            $sub_tabs['add_setting_group'] = [
                'title' => $this->lang->add_new_setting_group,
                'link' => $this->bb->admin_url . '/config?action=addgroup'
            ];

            $sub_tabs['modify_setting'] = [
                'title' => $this->lang->modify_existing_settings,
                'link' => $this->bb->admin_url . '/config?action=manage',
                'description' => $this->lang->modify_existing_settings_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'modify_setting');

            $form = new Form($this->bb, $this->bb->admin_url . '/config?action=edit', 'post', 'edit');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('sid', $setting['sid']);

            if (!empty($errors)) {
                $setting_data = $this->bb->input;
                $html .= $this->page->output_inline_error($errors);
            } else {
                $setting_data = $setting;
                $type = explode("\n", $setting['optionscode'], 2);
                $setting_data['type'] = trim($type[0]);
                $setting_data['extra'] = isset($type[1]) ? trim($type[1]) : '';
            }

            $form_container = new FormContainer($this, $this->lang->modify_setting);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $setting_data['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, '', $form->generate_text_area('description', $setting_data['description'], ['id' => 'description']), 'description');

            $query = $this->db->simple_select('settinggroups', '*', '', ['order_by' => 'disporder']);
            while ($group = $this->db->fetch_array($query)) {
                $group_lang_var = "setting_group_{$group['name']}";
                if (isset($this->lang->$group_lang_var)) {
                    $options[$group['gid']] = htmlspecialchars_uni($this->lang->$group_lang_var);
                } else {
                    $options[$group['gid']] = htmlspecialchars_uni($group['title']);
                }
            }
            $form_container->output_row($this->lang->group . ' <em>*</em>', '', $form->generate_select_box('gid', $options, $setting_data['gid'], ['id' => 'gid']), 'gid');
            $form_container->output_row($this->lang->display_order, '', $form->generate_numeric_field('disporder', $setting_data['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->setting_configuration, 1);
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc, $form->generate_text_box('name', $setting_data['name'], ['id' => 'name']), 'name');

            $setting_types = [
                'text' => $this->lang->text,
                'numeric' => $this->lang->numeric_text,
                'textarea' => $this->lang->textarea,
                'yesno' => $this->lang->yesno,
                'onoff' => $this->lang->onoff,
                'select' => $this->lang->select,
                'forumselect' => $this->lang->forum_selection_box,
                'forumselectsingle' => $this->lang->forum_selection_single,
                'groupselect' => $this->lang->group_selection_box,
                'groupselectsingle' => $this->lang->group_selection_single,
                'radio' => $this->lang->radio,
                'checkbox' => $this->lang->checkbox,
                'language' => $this->lang->language_selection_box,
                'adminlanguage' => $this->lang->adminlanguage,
                'cpstyle' => $this->lang->cpstyle
                //'php' => $this->lang->php // Internal Use Only
            ];

            $form_container->output_row($this->lang->type . ' <em>*</em>', '', $form->generate_select_box('type', $setting_types, $setting_data['type'], ['id' => 'type']), 'type');
            $form_container->output_row($this->lang->extra, $this->lang->extra_desc, $form->generate_text_area('extra', $setting_data['extra'], ['id' => 'extra']), 'extra', [], ['id' => 'row_extra']);
            $form_container->output_row($this->lang->value, '', $form->generate_text_area('value', $setting_data['value'], ['id' => 'value']), 'value');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->update_setting);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

//            echo '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/peeker.js?ver=1804"></script>
//	<script type="text/javascript">
//		$(document).ready(function() {
//			new Peeker($("#type"), $("#row_extra"), /^(select|radio|checkbox|php)$/, false);
//		});
//		// Add a star to the extra row since the "extra" is required if the box is shown
//		add_star("row_extra");
//	</script>';

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SettingsEdit.html.twig');
        }

        // Delete Setting
        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('settings', '*', "sid='" . $this->bb->getInput('sid', 0) . "'");
            $setting = $this->db->fetch_array($query);

            // Does the setting not exist?
            if (!$setting['sid']) {
                $this->session->flash_message($this->lang->error_invalid_sid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            // Prevent editing of default
            if ($setting['isdefault'] == 1) {
                $this->session->flash_message($this->lang->error_cannot_edit_default, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            $this->plugins->runHooks('admin_config_settings_delete');

            if ($this->bb->request_method == 'post') {
                // Delete the setting
                $this->db->delete_query('settings', "sid='{$setting['sid']}'");

                (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

                $this->plugins->runHooks('admin_config_settings_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($setting['sid'], $setting['title']);

                $this->session->flash_message($this->lang->success_setting_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config?action=delete&sid=' . $setting['sid'], $this->lang->confirm_setting_deletion);
            }
        }

        // Modify Existing Settings
        if ($this->bb->input['action'] == 'manage') {
            $this->plugins->runHooks('admin_config_settings_manage');

            // Update orders
            if ($this->bb->request_method == 'post') {
                if (is_array($this->bb->input['group_disporder'])) {
                    foreach ($this->bb->input['group_disporder'] as $gid => $new_order) {
                        $gid = (int)$gid;
                        $update_group = ['disporder' => (int)$new_order];
                        $this->db->update_query('settinggroups', $update_group, "gid={$gid}");
                    }
                }

                if (is_array($this->bb->input['setting_disporder'])) {
                    foreach ($this->bb->input['setting_disporder'] as $sid => $new_order) {
                        $sid = (int)$sid;
                        $update_setting = ['disporder' => (int)$new_order];
                        $this->db->update_query('settings', $update_setting, "sid={$sid}");
                    }
                }

                $this->plugins->runHooks('admin_config_settings_manage_commit');

                // Log admin action
                $this->bblogger->log_admin_action();

                $this->session->flash_message($this->lang->success_display_orders_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config?action=manage');
            }

            $this->page->add_breadcrumb_item($this->lang->modify_existing_settings);
            $html = $this->page->output_header($this->lang->board_settings . ' - ' . $this->lang->modify_existing_settings);

            $sub_tabs['change_settings'] = [
                'title' => $this->lang->change_settings,
                'link' => $this->bb->admin_url . '/config',
            ];

            $sub_tabs['add_setting'] = [
                'title' => $this->lang->add_new_setting,
                'link' => $this->bb->admin_url . '/config?action=add'
            ];

            $sub_tabs['add_setting_group'] = [
                'title' => $this->lang->add_new_setting_group,
                'link' => $this->bb->admin_url . '/config?action=addgroup'
            ];

            $sub_tabs['modify_setting'] = [
                'title' => $this->lang->modify_existing_settings,
                'link' => $this->bb->admin_url . '/config?action=manage',
                'description' => $this->lang->modify_existing_settings_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'modify_setting');

            // Cache settings
            $settings_cache = [];
            $query = $this->db->simple_select('settings', 'sid, name, title, disporder, gid, isdefault', '', ['order_by' => 'disporder', 'order_dir' => 'asc']);
            while ($setting = $this->db->fetch_array($query)) {
                $settings_cache[$setting['gid']][] = $setting;
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/config?action=manage', 'post', 'edit');
            $html .= $form->getForm();

            $table = new Table;

            $table->construct_header($this->lang->setting_group_setting);
            $table->construct_header($this->lang->order, ['class' => 'align_center', 'style' => 'width: 5%']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'style' => 'width: 200px']);

            // Generate table
            $query = $this->db->simple_select('settinggroups', '*', '', ['order_by' => 'disporder', 'order_dir' => 'asc']);
            while ($group = $this->db->fetch_array($query)) {
                // Make setting group row
                // Translated?
                $group_lang_var = "setting_group_{$group['name']}";
                if (isset($this->lang->$group_lang_var)) {
                    $group_title = htmlspecialchars_uni($this->lang->$group_lang_var);
                } else {
                    $group_title = htmlspecialchars_uni($group['title']);
                }
                $table->construct_cell("<strong>{$group_title}</strong>", ['id' => "group{$group['gid']}"]);
                $table->construct_cell($form->generate_numeric_field("group_disporder[{$group['gid']}]", $group['disporder'], ['style' => 'width: 50px; font-weight: bold', 'class' => 'align_center', 'min' => 0]));
                // Only show options if not a default setting group
                if ($group['isdefault'] != 1) {
                    $popup = new PopupMenu("group_{$group['gid']}", $this->lang->options);
                    $popup->add_item($this->lang->edit_setting_group, $this->bb->admin_url . '/config?action=editgroup&amp;gid=' . $group['gid']);
                    $popup->add_item($this->lang->delete_setting_group, $this->bb->admin_url . "/config?action=deletegroup&amp;gid={$group['gid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_setting_group_deletion}')");
                    $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                } else {
                    $table->construct_cell('');
                }
                $table->construct_row(['class' => 'alt_row', 'no_alt_row' => 1]);

                // Make rows for each setting in the group
//                if (is_array($settings_cache[$group['gid']])) {
                if (array_key_exists($group['gid'], $settings_cache)) {
                    foreach ($settings_cache[$group['gid']] as $setting) {
                        $setting_lang_var = "setting_{$setting['name']}";
                        if (isset($this->lang->$setting_lang_var)) {
                            $setting_title = htmlspecialchars_uni($this->lang->$setting_lang_var);
                        } else {
                            $setting_title = htmlspecialchars_uni($setting['title']);
                        }
                        $table->construct_cell($setting_title, ['style' => 'padding-left: 40px;']);
                        $table->construct_cell($form->generate_numeric_field("setting_disporder[{$setting['sid']}]", $setting['disporder'], ['style' => 'width: 50px', 'class' => 'align_center', 'min' => 0]));
                        // Only show options if not a default setting group or is a custom setting
                        if ($group['isdefault'] != 1 || $setting['isdefault'] != 1) {
                            $popup = new PopupMenu("setting_{$setting['sid']}", $this->lang->options);
                            $popup->add_item($this->lang->edit_setting, $this->bb->admin_url . "/config?action=edit&amp;sid={$setting['sid']}");
                            $popup->add_item($this->lang->delete_setting, $this->bb->admin_url . "/config?action=delete&amp;sid={$setting['sid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_setting_deletion}')");
                            $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                        } else {
                            $table->construct_cell('');
                        }
                        $table->construct_row(['no_alt_row' => 1, 'class' => "group{$group['gid']}"]);
                    }
                }
            }

            $html .= $table->output($this->lang->modify_existing_settings);

            $buttons[] = $form->generate_submit_button($this->lang->save_display_orders);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SettingsManage.html.twig');
        }

        // Change settings for a specified group.
        if ($this->bb->input['action'] == 'change') {
            $this->plugins->runHooks('admin_config_settings_change');

            if ($this->bb->request_method == 'post') {
                if (!is_writable(DIR . 'var/cache/forum/settings.php')) {
                    $this->session->flash_message($this->lang->error_chmod_settings_file, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/config');
                }

                // If we are changing the hidden captcha, make sure it doesn't conflict with another registration field
                if (isset($this->bb->input['upsetting']['hiddencaptchaimagefield'])) {
                    // Not allowed to be hidden captcha fields
                    $disallowed_fields = [
                        'username',
                        'password',
                        'password2',
                        'email',
                        'email2',
                        'imagestring',
                        'imagehash',
                        'answer',
                        'question_id',
                        'allownotices',
                        'hideemail',
                        'receivepms',
                        'pmnotice',
                        'emailpmnotify',
                        'invisible',
                        'subscriptionmethod',
                        'timezoneoffset',
                        'dstcorrection',
                        'language',
                        'step',
                        'action',
                        'agree',
                        'regtime',
                        'regcheck1',
                        'regcheck2',
                        'regsubmit'
                    ];

                    if (in_array($this->bb->input['upsetting']['hiddencaptchaimagefield'], $disallowed_fields)) {
                        // Whoopsies, you can't do that!
                        $error_message = $this->lang->sprintf($this->lang->error_hidden_captcha_conflict, htmlspecialchars_uni($this->bb->input['upsetting']['hiddencaptchaimagefield']));

                        $this->session->flash_message($error_message, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/config?action=change&gid=9');
                    }
                }

                // Get settings which optionscode is a forum/group select, checkbox or numeric
                // We cannot rely on user input to decide this
                $checkbox_settings = $forum_group_select = [];
                $query = $this->db->simple_select('settings', 'name, optionscode', "optionscode IN('forumselect', 'groupselect', 'numeric') OR optionscode LIKE 'checkbox%'");

                while ($multisetting = $this->db->fetch_array($query)) {
                    if (substr($multisetting['optionscode'], 0, 8) == 'checkbox') {
                        $checkbox_settings[] = $multisetting['name'];

                        // All checkboxes deselected = no $this->bb->input['upsetting'] for them, we need to initialize it manually then, but only on pages where the setting is shown
                        if (empty($this->bb->input['upsetting'][$multisetting['name']]) && isset($this->bb->input["isvisible_{$multisetting['name']}"])) {
                            $this->bb->input['upsetting'][$multisetting['name']] = [];
                        }
                    } elseif ($multisetting['optionscode'] == 'numeric') {
                        if (isset($this->bb->input['upsetting'][$multisetting['name']])) {
                            $this->bb->input['upsetting'][$multisetting['name']] = (int)$this->bb->input['upsetting'][$multisetting['name']];
                        }
                    } else {
                        $forum_group_select[] = $multisetting['name'];
                    }
                }

                if (is_array($this->bb->input['upsetting'])) {
                    foreach ($this->bb->input['upsetting'] as $name => $value) {
                        if ($forum_group_select && in_array($name, $forum_group_select)) {
                            if ($value == 'all') {
                                $value = -1;
                            } elseif ($value == 'custom') {
                                if (isset($this->bb->input['select'][$name]) && is_array($this->bb->input['select'][$name])) {
                                    foreach ($this->bb->input['select'][$name] as &$val) {
                                        $val = (int)$val;
                                    }
                                    unset($val);

                                    $value = implode(',', $this->bb->input['select'][$name]);
                                } else {
                                    $value = '';
                                }
                            } else {
                                $value = '';
                            }
                        } elseif ($checkbox_settings && in_array($name, $checkbox_settings)) {
                            $value = '';

                            if (is_array($this->bb->input['upsetting'][$name])) {
                                $value = implode(',', $this->bb->input['upsetting'][$name]);
                            }
                        }

                        $this->db->update_query('settings', ['value' => $this->db->escape_string($value)], "name='" . $this->db->escape_string($name) . "'");
                    }
                }

                // Check if we need to create our fulltext index after changing the search mode
                if (isset($this->bb->input['upsetting']['searchtype'])) {
                    if ($this->bb->settings['searchtype'] != $this->bb->input['upsetting']['searchtype'] &&
                        $this->bb->input['upsetting']['searchtype'] == 'fulltext'
                    ) {
                        if (!$this->db->is_fulltext('posts') && $this->db->supports_fulltext_boolean('posts')) {
                            $this->db->create_fulltext_index('posts', 'message');
                        }
                        if (!$this->db->is_fulltext('posts') && $this->db->supports_fulltext('threads')) {
                            $this->db->create_fulltext_index('threads', 'subject');
                        }
                    }
                }

                // If the delayedthreadviews setting was changed, enable or disable the tasks for it.
                if (isset($this->bb->input['upsetting']['delayedthreadviews']) && $this->bb->settings['delayedthreadviews'] != $this->bb->input['upsetting']['delayedthreadviews']) {
                    if ($this->bb->input['upsetting']['delayedthreadviews'] == 0) {
                        $updated_task = [
                            'enabled' => 0
                        ];
                    } else {
                        $updated_task = [
                            'enabled' => 1
                        ];
                    }
                    $this->db->update_query('tasks', $updated_task, "file='threadviews'");
                }

                // Have we changed our cookie prefix? If so, update our adminsid so we're not logged out
                if (isset($this->bb->input['upsetting']['cookieprefix']) &&
                    $this->bb->input['upsetting']['cookieprefix'] != $this->bb->settings['cookieprefix']
                ) {
                    $this->bb->my_unsetcookie('adminsid');
                    $this->bb->settings['cookieprefix'] = $this->bb->input['upsetting']['cookieprefix'];
                    $this->bb->my_setcookie('adminsid', $this->session->admin_session['sid'], '', true);
                }

                // Have we opted for a reCAPTCHA and not set a public/private key?
                if (isset($this->bb->input['upsetting']['captchaimage']) &&
                    $this->bb->input['upsetting']['captchaimage'] == 2 &&
                    !$this->bb->input['upsetting']['captchaprivatekey'] &&
                    !$this->bb->input['upsetting']['captchapublickey']
                ) {
                    $this->db->update_query('settings', ['value' => 1], "name = 'captchaimage'");
                }

                (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

                $this->plugins->runHooks('admin_config_settings_change_commit');

                // If we have changed our report reasons recache them
                if (isset($this->bb->input['upsetting']['reportreasons'])) {
                    $this->cache->update_reportedposts();
                }

                // Log admin action
                $this->bblogger->log_admin_action();

                $this->session->flash_message($this->lang->success_settings_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config');
            }

            // What type of page
            $cache_groups = $cache_settings = [];
            $html = '';
            if (isset($this->bb->input['search'])) {
                // Search

                // Search for settings
                $search = $this->db->escape_string_like($this->bb->input['search']);
                $query = $this->db->query('
			SELECT s.*
			FROM ' . TABLE_PREFIX . 'settings s
			LEFT JOIN ' . TABLE_PREFIX . "settinggroups g ON(s.gid=g.gid)
			WHERE s.name LIKE '%{$search}%' OR s.title LIKE '%{$search}%' OR s.description LIKE '%{$search}%' OR g.name LIKE '%{$search}%' OR g.title LIKE '%{$search}%' OR g.description LIKE '%{$search}%'
			ORDER BY s.disporder
		");
                while ($setting = $this->db->fetch_array($query)) {
                    $cache_settings[$setting['gid']][$setting['sid']] = $setting;
                }

                if (!$this->db->num_rows($query)) {
                    if (isset($this->bb->input['ajax_search'])) {
                        echo json_encode(['errors' => [$this->lang->error_no_settings_found]]);
                        exit;
                    } else {
                        $this->session->flash_message($this->lang->error_no_settings_found, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/config');
                    }
                }

                // Cache groups
                $groups = array_keys($cache_settings);
                $groups = implode(',', $groups);
                $query = $this->db->simple_select('settinggroups', '*', "gid IN ({$groups})", ['order_by' => 'disporder']);
                while ($group = $this->db->fetch_array($query)) {
                    $cache_groups[$group['gid']] = $group;
                }

                // Page header only if not AJAX
                if (!isset($this->bb->input['ajax_search'])) {
                    $this->page->add_breadcrumb_item($this->lang->settings_search);
                    $html .= $this->page->output_header($this->lang->board_settings . " - {$this->lang->settings_search}");
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/config?action=change', 'post', 'change');
                $html .= $form->getForm();

                $html .= $form->generate_hidden_field('gid', $group['gid']);
            } elseif (isset($this->bb->input['gid'])) {
                // Group listing
                // Cache groups
                $query = $this->db->simple_select('settinggroups', '*', "gid = '" . $this->bb->getInput('gid', 0) . "'");
                $groupinfo = $this->db->fetch_array($query);
                $cache_groups[$groupinfo['gid']] = $groupinfo;

                if (!$this->db->num_rows($query)) {
                    $html .= $this->page->output_error($this->lang->error_invalid_gid2);
                }

                // Cache settings
                $query = $this->db->simple_select('settings', '*', "gid='" . $this->bb->getInput('gid', 0) . "'", ['order_by' => 'disporder']);
                while ($setting = $this->db->fetch_array($query)) {
                    $cache_settings[$setting['gid']][$setting['sid']] = $setting;
                }

                if (!$this->db->num_rows($query)) {
                    $this->session->flash_message($this->lang->error_no_settings_found, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/config');
                }

                $group_lang_var = "setting_group_{$groupinfo['name']}";
                if (isset($this->lang->$group_lang_var)) {
                    $groupinfo['title'] = $this->lang->$group_lang_var;
                }

                // Page header
                $this->page->add_breadcrumb_item($groupinfo['title']);
                $html .= $this->page->output_header($this->lang->board_settings . " - {$groupinfo['title']}");

                $form = new Form($this->bb, $this->bb->admin_url . '/config?action=change', 'post', 'change');
                $html .= $form->getForm();

                $html .= $form->generate_hidden_field('gid', $groupinfo['gid']);
            } else {
                // All settings list
                // Cache groups
                $query = $this->db->simple_select('settinggroups', '*', '', ['order_by' => 'disporder']);
                while ($group = $this->db->fetch_array($query)) {
                    $cache_groups[$group['gid']] = $group;
                }

                if (!$this->db->num_rows($query)) {
                    $html .= $this->page->output_error($this->lang->error_invalid_gid2);
                }

                // Cache settings
                $query = $this->db->simple_select('settings', '*', '', ['order_by' => 'disporder']);
                while ($setting = $this->db->fetch_array($query)) {
                    $cache_settings[$setting['gid']][$setting['sid']] = $setting;
                }

                // Page header
                $this->page->add_breadcrumb_item($this->lang->show_all_settings);
                $html .= $this->page->output_header($this->lang->board_settings . " - {$this->lang->show_all_settings}");

                $form = new Form($this->bb, $this->bb->admin_url . '/config?action=change', 'post', 'change');
                $html .= $form->getForm();
            }

            // Build rest of page
            $buttons[] = $form->generate_submit_button($this->lang->save_settings);
            foreach ($cache_groups as $groupinfo) {
                $group_lang_var = "setting_group_{$groupinfo['name']}";
                if (isset($this->lang->$group_lang_var)) {
                    $groupinfo['title'] = $this->lang->$group_lang_var;
                }

                $form_container = new FormContainer($this, $groupinfo['title']);

                if (empty($cache_settings[$groupinfo['gid']])) {
                    $form_container->output_cell($this->lang->error_no_settings_found);
                    $form_container->construct_row();

                    $html .= $form_container->end();
                    $html .= '<br />';

                    continue;
                }

                foreach ($cache_settings[$groupinfo['gid']] as $setting) {
                    $options = '';
                    $type = explode("\n", $setting['optionscode']);
                    $type[0] = trim($type[0]);
                    $element_name = "upsetting[{$setting['name']}]";
                    $element_id = "setting_{$setting['name']}";
                    if ($type[0] == 'text' || $type[0] == '') {
                        $setting_code = $form->generate_text_box($element_name, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'numeric') {
                        $setting_code = $form->generate_numeric_field($element_name, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'textarea') {
                        $setting_code = $form->generate_text_area($element_name, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'yesno') {
                        $setting_code = $form->generate_yes_no_radio($element_name, $setting['value'], true, ['id' => $element_id . '_yes', 'class' => $element_id], ['id' => $element_id . '_no', 'class' => $element_id]);
                    } elseif ($type[0] == 'onoff') {
                        $setting_code = $form->generate_on_off_radio($element_name, $setting['value'], true, ['id' => $element_id . '_on', 'class' => $element_id], ['id' => $element_id . '_off', 'class' => $element_id]);
                    } elseif ($type[0] == 'cpstyle') {
//                        $dir = @opendir(MYBB_ROOT . $this->bb->config['admin_dir'] . '/styles');
                        $dir = @opendir(DIR . 'web/assets/runbb/admin/styles');

                        $folders = [];
                        while ($folder = readdir($dir)) {
//                            if ($file != '.' && $file != '..' && @file_exists(MYBB_ROOT . $this->bb->config['admin_dir'] . "/styles/$folder/main.css")) {
                            if ($folder != '.' && $folder != '..' && @file_exists(DIR . "web/assets/runbb/admin/styles/$folder/main.css")) {
                                $folders[$folder] = ucfirst($folder);
                            }
                        }
                        closedir($dir);
                        ksort($folders);
                        $setting_code = $form->generate_select_box($element_name, $folders, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'language') {
                        $languages = $this->lang->get_languages();
                        $setting_code = $form->generate_select_box($element_name, $languages, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'adminlanguage') {
                        $languages = $this->lang->get_languages(1);
                        $setting_code = $form->generate_select_box($element_name, $languages, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'passwordbox') {
                        $setting_code = $form->generate_password_box($element_name, $setting['value'], ['id' => $element_id]);
                    } elseif ($type[0] == 'php') {
                        $setting['optionscode'] = substr($setting['optionscode'], 3);
                        eval("\$setting_code = \"" . $setting['optionscode'] . "\";");
                    } elseif ($type[0] == 'forumselect') {
                        $selected_values = '';
                        if ($setting['value'] != '' && $setting['value'] != -1) {
                            $selected_values = explode(',', (string)$setting['value']);

                            foreach ($selected_values as &$value) {
                                $value = (int)$value;
                            }
                            unset($value);
                        }

                        $forum_checked = ['all' => '', 'custom' => '', 'none' => ''];
                        if ($setting['value'] == -1) {
                            $forum_checked['all'] = 'checked="checked"';
                        } elseif ($setting['value'] != '') {
                            $forum_checked['custom'] = 'checked="checked"';
                        } else {
                            $forum_checked['none'] = 'checked="checked"';
                        }

                        $html .= $this->adm->print_selection_javascript();

                        $setting_code = "
				<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
					<dt><label style=\"display: block;\"><input type=\"radio\" name=\"{$element_name}\" value=\"all\" {$forum_checked['all']} class=\"{$element_id}_forums_groups_check\" onclick=\"checkAction('{$element_id}');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
					<dt><label style=\"display: block;\"><input type=\"radio\" name=\"{$element_name}\" value=\"custom\" {$forum_checked['custom']} class=\"{$element_id}_forums_groups_check\" onclick=\"checkAction('{$element_id}');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
					<dd style=\"margin-top: 4px;\" id=\"{$element_id}_forums_groups_custom\" class=\"{$element_id}_forums_groups\">
						<table cellpadding=\"4\">
							<tr>
								<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
								<td>" . $form->generate_forum_select('select[' . $setting['name'] . '][]', $selected_values, ['id' => $element_id, 'multiple' => true, 'size' => 5]) . "</td>
							</tr>
						</table>
					</dd>
					<dt><label style=\"display: block;\"><input type=\"radio\" name=\"{$element_name}\" value=\"none\" {$forum_checked['none']} class=\"{$element_id}_forums_groups_check\" onclick=\"checkAction('{$element_id}');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->none}</strong></label></dt>
				</dl>
				<script type=\"text/javascript\">
					checkAction('{$element_id}');
				</script>";
                    } elseif ($type[0] == 'forumselectsingle') {
                        $selected_value = (int)$setting['value']; // No need to check if empty, int will give 0
                        $setting_code = $form->generate_forum_select($element_name, $selected_value, ['id' => $element_id, 'main_option' => $this->lang->none]);
                    } elseif ($type[0] == 'groupselect') {
                        $selected_values = '';
                        if ($setting['value'] != '' && $setting['value'] != -1) {
                            $selected_values = explode(',', (string)$setting['value']);

                            foreach ($selected_values as &$value) {
                                $value = (int)$value;
                            }
                            unset($value);
                        }

                        $group_checked = ['all' => '', 'custom' => '', 'none' => ''];
                        if ($setting['value'] == -1) {
                            $group_checked['all'] = 'checked="checked"';
                        } elseif ($setting['value'] != '') {
                            $group_checked['custom'] = 'checked="checked"';
                        } else {
                            $group_checked['none'] = 'checked="checked"';
                        }

                        $html .= $this->adm->print_selection_javascript();

                        $setting_code = "
				<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
					<dt><label style=\"display: block;\"><input type=\"radio\" name=\"{$element_name}\" value=\"all\" {$group_checked['all']} class=\"{$element_id}_forums_groups_check\" onclick=\"checkAction('{$element_id}');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
					<dt><label style=\"display: block;\"><input type=\"radio\" name=\"{$element_name}\" value=\"custom\" {$group_checked['custom']} class=\"{$element_id}_forums_groups_check\" onclick=\"checkAction('{$element_id}');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
					<dd style=\"margin-top: 4px;\" id=\"{$element_id}_forums_groups_custom\" class=\"{$element_id}_forums_groups\">
						<table cellpadding=\"4\">
							<tr>
								<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
								<td>" . $form->generate_group_select('select[' . $setting['name'] . '][]', $selected_values, ['id' => $element_id, 'multiple' => true, 'size' => 5]) . "</td>
							</tr>
						</table>
					</dd>
					<dt><label style=\"display: block;\"><input type=\"radio\" name=\"{$element_name}\" value=\"none\" {$group_checked['none']} class=\"{$element_id}_forums_groups_check\" onclick=\"checkAction('{$element_id}');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->none}</strong></label></dt>
				</dl>
				<script type=\"text/javascript\">
					checkAction('{$element_id}');
				</script>";
                    } elseif ($type[0] == 'groupselectsingle') {
                        $selected_value = (int)$setting['value']; // No need to check if empty, int will give 0
                        $setting_code = $form->generate_group_select($element_name, $selected_value, ['id' => $element_id, 'main_option' => $this->lang->none]);
                    } else {
                        $typecount = count($type);

                        if ($type[0] == 'checkbox') {
                            $multivalue = explode(',', $setting['value']);
                        }

                        for ($i = 0; $i < $typecount; $i++) {
                            $optionsexp = explode('=', $type[$i]);
                            if (!isset($optionsexp[1])) {
                                continue;
                            }
                            $title_lang = "setting_{$setting['name']}_{$optionsexp[0]}";
                            if (isset($this->lang->$title_lang)) {
                                $optionsexp[1] = $this->lang->$title_lang;
                            }

                            if ($type[0] == 'select') {
                                $option_list[$optionsexp[0]] = htmlspecialchars_uni($optionsexp[1]);
                            } elseif ($type[0] == 'radio') {
                                if ($setting['value'] == $optionsexp[0]) {
                                    $option_list[$i] = $form->generate_radio_button($element_name, $optionsexp[0], htmlspecialchars_uni($optionsexp[1]), ['id' => $element_id . '_' . $i, 'checked' => 1, 'class' => $element_id]);
                                } else {
                                    $option_list[$i] = $form->generate_radio_button($element_name, $optionsexp[0], htmlspecialchars_uni($optionsexp[1]), ['id' => $element_id . '_' . $i, 'class' => $element_id]);
                                }
                            } elseif ($type[0] == 'checkbox') {
                                if (in_array($optionsexp[0], $multivalue)) {
                                    $option_list[$i] = $form->generate_check_box("{$element_name}[]", $optionsexp[0], htmlspecialchars_uni($optionsexp[1]), ['id' => $element_id . '_' . $i, 'checked' => 1, 'class' => $element_id]);
                                } else {
                                    $option_list[$i] = $form->generate_check_box("{$element_name}[]", $optionsexp[0], htmlspecialchars_uni($optionsexp[1]), ['id' => $element_id . '_' . $i, 'class' => $element_id]);
                                }
                            }
                        }

                        if ($type[0] == 'select') {
                            $setting_code = $form->generate_select_box($element_name, $option_list, $setting['value'], ['id' => $element_id]);
                        } else {
                            $setting_code = implode('<br />', $option_list);

                            if ($type[0] == 'checkbox') {
                                $setting_code .= $form->generate_hidden_field("isvisible_{$setting['name']}", 1);
                            }
                        }
                        $option_list = [];
                    }

                    // Do we have a custom language variable for this title or description?
                    $title_lang = 'setting_' . $setting['name'];
                    $desc_lang = $title_lang . '_desc';
                    if (isset($this->lang->$title_lang)) {
                        $setting['title'] = $this->lang->$title_lang;
                    }
                    if (isset($this->lang->$desc_lang)) {
                        $setting['description'] = $this->lang->$desc_lang;
                    }
                    $form_container->output_row(htmlspecialchars_uni($setting['title']), $setting['description'], $setting_code, '', [], ['id' => 'row_' . $element_id]);
                }
                $html .= $form_container->end();

                $html .= $form->output_submit_wrapper($buttons);
                $html .= '<br />';
            }
            $html .= $form->end();

            $html .= $this->print_setting_peekers();

            if (!isset($this->bb->input['ajax_search'])) {
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Config/SettingsChange.html.twig');
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_settings_start');

            $this->page->extra_header .= <<<EOF
	<script type="text/javascript">
	<!--
	lang.searching = "{$this->lang->searching}";
	lang.search_error = "{$this->lang->search_error}";
	lang.search_done = "{$this->lang->search_done}";
	// -->
	</script>
EOF;

            $html = $this->page->output_header($this->lang->board_settings);
            if (isset($message)) {
                $html .= $this->page->output_inline_message($message);
            }

            $sub_tabs['change_settings'] = [
                'title' => $this->lang->change_settings,
                'link' => $this->bb->admin_url . '/config',
                'description' => $this->lang->change_settings_desc
            ];

            $sub_tabs['add_setting'] = [
                'title' => $this->lang->add_new_setting,
                'link' => $this->bb->admin_url . '/config?action=add'
            ];

            $sub_tabs['add_setting_group'] = [
                'title' => $this->lang->add_new_setting_group,
                'link' => $this->bb->admin_url . '/config?action=addgroup'
            ];

            $sub_tabs['modify_setting'] = [
                'title' => $this->lang->modify_existing_settings,
                'link' => $this->bb->admin_url . '/config?action=manage',
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'change_settings');

            // Search form
            $html .= "<div style=\"text-align: right; margin-bottom: 3px;\">";
            $search = new Form($this->bb, $this->bb->admin_url, 'get', 'settings_search', 0, 'settings_search');
            $html .= $search->getForm();
            $html .= $search->generate_hidden_field('module', 'config/settings');
            $html .= $search->generate_hidden_field('action', 'change');
            $html .= $search->generate_text_box('search', $this->lang->settings_search, ['id' => 'search', 'class' => 'search_default field150 field_small']);
            $html .= "<input type=\"submit\" class=\"search_button\" value=\"{$this->lang->search}\" />";
            $html .= $search->end();
            $html .= "</div>\n";

            $html .= '<div id="search_results">&nbsp;</div><div id="group_list">';
            $table = new Table;
            $table->construct_header($this->lang->setting_groups);

            switch ($this->db->type) {
                case 'pgsql':
                    $query = $this->db->query('
			SELECT g.*, COUNT(s.sid) AS settingcount
			FROM ' . TABLE_PREFIX . 'settinggroups g
			LEFT JOIN ' . TABLE_PREFIX . 'settings s ON (s.gid=g.gid)
			WHERE g.isdefault = 1
			GROUP BY ' . $this->db->build_fields_string('settinggroups', 'g.') . '
			ORDER BY g.disporder
		');
                    break;
                default:
                    $query = $this->db->query('
			SELECT g.*, COUNT(s.sid) AS settingcount
			FROM ' . TABLE_PREFIX . 'settinggroups g
			LEFT JOIN ' . TABLE_PREFIX . 'settings s ON (s.gid=g.gid)
			WHERE g.isdefault = 1
			GROUP BY g.gid
			ORDER BY g.disporder
		');
            }
            while ($group = $this->db->fetch_array($query)) {
                $group_lang_var = "setting_group_{$group['name']}";
                if (isset($this->lang->$group_lang_var)) {
                    $group_title = htmlspecialchars_uni($this->lang->$group_lang_var);
                } else {
                    $group_title = htmlspecialchars_uni($group['title']);
                }

                $group_desc_lang_var = "setting_group_{$group['name']}_desc";
                if (isset($this->lang->$group_desc_lang_var)) {
                    $group_desc = htmlspecialchars_uni($this->lang->$group_desc_lang_var);
                } else {
                    $group_desc = htmlspecialchars_uni($group['description']);
                }

                $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/config?action=change&amp;gid={$group['gid']}\">{$group_title}</a></strong> ({$group['settingcount']} {$this->lang->bbsettings})<br /><small>{$group_desc}</small>");
                $table->construct_row();
            }

            $html .= $table->output("<span style=\"float: right;\"><small><a href=\"".$this->bb->admin_url."/config?action=change\">{$this->lang->show_all_settings}</a></small></span>{$this->lang->board_settings}");

            // Plugin Settings
            switch ($this->db->type) {
                case 'pgsql':
                    $query = $this->db->query('
			SELECT g.*, COUNT(s.sid) AS settingcount
			FROM ' . TABLE_PREFIX . 'settinggroups g
			LEFT JOIN ' . TABLE_PREFIX . 'settings s ON (s.gid=g.gid)
			WHERE g.isdefault <> 1
			GROUP BY ' . $this->db->build_fields_string('settinggroups', 'g.') . '
			ORDER BY g.disporder
		');
                    break;
                default:
                    $query = $this->db->query('
			SELECT g.*, COUNT(s.sid) AS settingcount
			FROM ' . TABLE_PREFIX . 'settinggroups g
			LEFT JOIN ' . TABLE_PREFIX . 'settings s ON (s.gid=g.gid)
			WHERE g.isdefault <> 1
			GROUP BY g.gid
			ORDER BY g.disporder
		');
            }

            if ($this->db->num_rows($query)) {
                $table = new Table;
                $table->construct_header($this->lang->setting_groups);

                while ($group = $this->db->fetch_array($query)) {
                    $group_lang_var = "setting_group_{$group['name']}";
                    if (isset($this->lang->$group_lang_var)) {
                        $group_title = htmlspecialchars_uni($this->lang->$group_lang_var);
                    } else {
                        $group_title = htmlspecialchars_uni($group['title']);
                    }

                    $group_desc_lang_var = "setting_group_{$group['name']}_desc";
                    if (isset($this->lang->$group_desc_lang_var)) {
                        $group_desc = htmlspecialchars_uni($this->lang->$group_desc_lang_var);
                    } else {
                        $group_desc = htmlspecialchars_uni($group['description']);
                    }

                    $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/config?action=change&amp;gid={$group['gid']}\">{$group_title}</a></strong> ({$group['settingcount']} {$this->lang->bbsettings})<br /><small>{$group_desc}</small>");
                    $table->construct_row();
                }

                $html .= $table->output($this->lang->plugin_settings);
            }

            $html .= '</div>';

            $html .= '
<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/search.js"></script>
<script type="text/javascript">
//<!--
$(document).ready(function(){
	SettingSearch.init("' . $this->lang->settings_search . '","' . $this->lang->error_ajax_unknown . '");
});
//-->
</script>';

            $html .= $this->print_setting_peekers();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SettingsIndex.html.twig');
        }
    }

    /**
     * Print all the peekers for all of the default settings
     */
    private function print_setting_peekers()
    {
        $peekers = [
            'new Peeker($(".setting_boardclosed"), $("#row_setting_boardclosed_reason"), 1, true)',
            'new Peeker($(".setting_gzipoutput"), $("#row_setting_gziplevel"), 1, true)',
            'new Peeker($(".setting_useerrorhandling"), $("#row_setting_errorlogmedium, #row_setting_errortypemedium, #row_setting_errorloglocation"), 1, true)',
            'new Peeker($("#setting_subforumsindex"), $("#row_setting_subforumsstatusicons"), /[^0+|]/, false)',
            'new Peeker($(".setting_showsimilarthreads"), $("#row_setting_similarityrating, #row_setting_similarlimit"), 1, true)',
            'new Peeker($(".setting_disableregs"), $("#row_setting_regtype, #row_setting_securityquestion, #row_setting_regtime, #row_setting_allowmultipleemails, #row_setting_hiddencaptchaimage, #row_setting_betweenregstime"), 0, true)',
            'new Peeker($(".setting_hiddencaptchaimage"), $("#row_setting_hiddencaptchaimagefield"), 1, true)',
            'new Peeker($("#setting_failedlogincount"), $("#row_setting_failedlogintime, #row_setting_failedlogintext"), /[^0+|]/, false)',
            'new Peeker($(".setting_postfloodcheck"), $("#row_setting_postfloodsecs"), 1, true)',
            'new Peeker($("#setting_postmergemins"), $("#row_setting_postmergefignore, #row_setting_postmergeuignore, #row_setting_postmergesep"), /[^0+|]/, false)',
            'new Peeker($(".setting_enablememberlist"), $("#row_setting_membersperpage, #row_setting_default_memberlist_sortby, #row_setting_default_memberlist_order, #row_setting_memberlistmaxavatarsize"), 1, true)',
            'new Peeker($(".setting_enablereputation"), $("#row_setting_repsperpage, #row_setting_posrep, #row_setting_neurep, #row_setting_negrep, #row_setting_postrep, #row_setting_multirep, #row_setting_maxreplength, #row_setting_minreplength"), 1, true)',
            'new Peeker($(".setting_enablewarningsystem"), $("#row_setting_allowcustomwarnings, #row_setting_canviewownwarning, #row_setting_maxwarningpoints, #row_setting_allowanonwarningpms"), 1, true)',
            'new Peeker($(".setting_enablepms"), $("#row_setting_pmsallowhtml, #row_setting_pmsallowmycode, #row_setting_pmsallowsmilies, #row_setting_pmsallowimgcode, #row_setting_pmsallowvideocode, #row_setting_pmquickreply, #row_setting_pmfloodsecs, #row_setting_showpmip, #row_setting_maxpmquotedepth"), 1, true)',
            'new Peeker($(".setting_smilieinserter"), $("#row_setting_smilieinsertertot, #row_setting_smilieinsertercols"), 1, true)',
            'new Peeker($("#setting_mail_handler"), $("#row_setting_smtp_host, #row_setting_smtp_port, #row_setting_smtp_user, #row_setting_smtp_pass, #row_setting_secure_smtp"), "smtp", false)',
            'new Peeker($("#setting_mail_handler"), $("#row_setting_mail_parameters"), "mail", false)',
            'new Peeker($("#setting_captchaimage"), $("#row_setting_captchapublickey, #row_setting_captchaprivatekey"), /(2|4)/, false)',
            'new Peeker($(".setting_contact"), $("#row_setting_contact_guests, #row_setting_contact_badwords, #row_setting_contact_maxsubjectlength, #row_setting_contact_minmessagelength, #row_setting_contact_maxmessagelength"), 1, true)',
            'new Peeker($(".setting_enablepruning"), $("#row_setting_enableprunebyposts, #row_setting_pruneunactived, #row_setting_prunethreads"), 1, true)',
            'new Peeker($(".setting_enableprunebyposts"), $("#row_setting_prunepostcount, #row_setting_dayspruneregistered, #row_setting_prunepostcountall"), 1, true)',
            'new Peeker($(".setting_pruneunactived"), $("#row_setting_dayspruneunactivated"), 1, true)',
            'new Peeker($(".setting_statsenabled"), $("#row_setting_statscachetime, #row_setting_statslimit, #row_setting_statstopreferrer"), 1, true)',
            'new Peeker($(".setting_purgespammergroups_forums_groups_check"), $("#row_setting_purgespammerpostlimit, #row_setting_purgespammerbandelete, #row_setting_purgespammerapikey"), /^(?!none)/, true)',
            'new Peeker($(".setting_purgespammerbandelete"),$("#row_setting_purgespammerbangroup, #row_setting_purgespammerbanreason"), "ban", true)',
            'new Peeker($("#setting_maxloginattempts"), $("#row_setting_loginattemptstimeout"), /[^0+|]/, false)',
            'new Peeker($(".setting_bbcodeinserter"), $("#row_setting_partialmode, #row_setting_smilieinserter"), 1, true)',
            'new Peeker($(".setting_portal"), $("#row_setting_portal_announcementsfid, #row_setting_portal_showwelcome, #row_setting_portal_showpms, #row_setting_portal_showstats, #row_setting_portal_showwol, #row_setting_portal_showsearch, #row_setting_portal_showdiscussions"), 1, true)',
            'new Peeker($(".setting_portal_announcementsfid_forums_groups_check"), $("#row_setting_portal_numannouncements"), /^(?!none)/, true)',
            'new Peeker($(".setting_portal_showdiscussions"), $("#row_setting_portal_showdiscussionsnum, #row_setting_portal_excludediscussion"), 1, true)',
            'new Peeker($(".setting_enableattachments"), $("#row_setting_maxattachments, #row_setting_attachthumbnails"), 1, true)',
            'new Peeker($(".setting_attachthumbnails"), $("#row_setting_attachthumbh, #row_setting_attachthumbw"), "yes", true)',
            'new Peeker($(".setting_showbirthdays"), $("#row_setting_showbirthdayspostlimit"), 1, true)',
            'new Peeker($("#setting_betweenregstime"), $("#row_setting_maxregsbetweentime"), /[^0+|]/, false)',
            'new Peeker($(".setting_usecdn"), $("#row_setting_cdnurl, #row_setting_cdnpath"), 1, true)',
            'new Peeker($("#setting_errorlogmedium"), $("#row_setting_errortypemedium"), /^(log|email|both)/, false)',
            'new Peeker($("#setting_errorlogmedium"), $("#row_setting_errorloglocation"), /^(log|both)/, false)',
            'new Peeker($(".setting_sigmycode"), $("#row_setting_sigcountmycode, #row_setting_sigimgcode"), 1, true)',
            'new Peeker($(".setting_pmsallowmycode"), $("#row_setting_pmsallowimgcode, #row_setting_pmsallowvideocode"), 1, true)'
        ];

        $peekers = $this->plugins->runHooks('admin_settings_print_peekers', $peekers);

        $setting_peekers = implode("\n			", $peekers);

        return '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/peeker.js?ver=1804"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			' . $setting_peekers . '
		});
	</script>';
    }
}
