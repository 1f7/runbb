<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Smilies extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_smilies', false, true);

        $this->page->add_breadcrumb_item($this->lang->smilies, $this->bb->admin_url . '/config/smilies');

        $this->plugins->runHooks('admin_config_smilies_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_smilies_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['find'])) {
                    $errors[] = $this->lang->error_missing_text_replacement;
                }

                if (!trim($this->bb->input['image'])) {
                    $errors[] = $this->lang->error_missing_path;
                }

                if (!trim($this->bb->input['disporder'])) {
                    $errors[] = $this->lang->error_missing_order;
                } else {
                    $this->bb->input['disporder'] = $this->bb->getInput('disporder', 0);
                    $query = $this->db->simple_select('smilies', 'sid', 'disporder=\'' . $this->bb->input['disporder'] . '\'');
                    $duplicate_disporder = $this->db->fetch_field($query, 'sid');

                    if ($duplicate_disporder) {
                        $errors[] = $this->lang->error_duplicate_order;
                    }
                }

                if (!$errors) {
                    $this->bb->input['find'] = str_replace("\r\n", "\n", $this->bb->input['find']);
                    $this->bb->input['find'] = str_replace("\r", "\n", $this->bb->input['find']);
                    $this->bb->input['find'] = explode("\n", $this->bb->input['find']);
                    foreach (array_merge(array_keys($this->bb->input['find'], ''), array_keys($this->bb->input['find'], ' ')) as $key) {
                        unset($this->bb->input['find'][$key]);
                    }
                    $this->bb->input['find'] = implode("\n", $this->bb->input['find']);

                    $new_smilie = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'find' => $this->db->escape_string($this->bb->input['find']),
                        'image' => $this->db->escape_string($this->bb->input['image']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'showclickable' => $this->bb->getInput('showclickable', 0)
                    ];

                    $sid = $this->db->insert_query('smilies', $new_smilie);

                    $this->plugins->runHooks('admin_config_smilies_add_commit');

                    $this->cache->update_smilies();

                    // Log admin action
                    $this->bblogger->log_admin_action($sid, htmlspecialchars_uni($this->bb->input['name']));

                    $this->session->flash_message($this->lang->success_smilie_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/smilies');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_smilie);
            $html = $this->page->output_header($this->lang->smilies . ' - ' . $this->lang->add_smilie);

            $sub_tabs['manage_smilies'] = [
                'title' => $this->lang->manage_smilies,
                'link' => $this->bb->admin_url . '/config/smilies',
            ];
            $sub_tabs['add_smilie'] = [
                'title' => $this->lang->add_smilie,
                'link' => $this->bb->admin_url . '/config/smilies?action=add',
                'description' => $this->lang->add_smilie_desc
            ];
            $sub_tabs['add_multiple_smilies'] = [
                'title' => $this->lang->add_multiple_smilies,
                'link' => $this->bb->admin_url . '/config/smilies?action=add_multiple',
            ];
            $sub_tabs['mass_edit'] = [
                'title' => $this->lang->mass_edit,
                'link' => $this->bb->admin_url . '/config/smilies?action=mass_edit'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_smilie');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/smilies?action=add', 'post', 'add');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['image'] = 'images/smilies/';
                $this->bb->input['showclickable'] = 1;
            }

            if (!isset($this->bb->input['disporder'])) {
                $query = $this->db->simple_select('smilies', 'max(disporder) as dispordermax');
                $this->bb->input['disporder'] = $this->db->fetch_field($query, 'dispordermax') + 1;
            }

            $form_container = new FormContainer($this, $this->lang->add_smilie);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->text_replace . ' <em>*</em>', $this->lang->text_replace_desc, $form->generate_text_area('find', $this->bb->getInput('find', ''), ['id' => 'find']), 'find');
            $form_container->output_row($this->lang->image_path . ' <em>*</em>', $this->lang->image_path_desc, $form->generate_text_box('image', $this->bb->input['image'], ['id' => 'image']), 'image');
            $form_container->output_row($this->lang->display_order . ' <em>*</em>', $this->lang->display_order_desc, $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $form_container->output_row($this->lang->show_clickable . ' <em>*</em>', $this->lang->show_clickable_desc, $form->generate_yes_no_radio('showclickable', $this->bb->input['showclickable']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_smilie);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SmiliesAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('smilies', '*', "sid='" . $this->bb->getInput('sid', 0) . "'");
            $smilie = $this->db->fetch_array($query);

            // Does the smilie not exist?
            if (!$smilie['sid']) {
                $this->session->flash_message($this->lang->error_invalid_smilie, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/smilies');
            }

            $this->plugins->runHooks('admin_config_smilies_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['find'])) {
                    $errors[] = $this->lang->error_missing_text_replacement;
                }

                if (!trim($this->bb->input['image'])) {
                    $errors[] = $this->lang->error_missing_path;
                }

                if (!trim($this->bb->input['disporder'])) {
                    $errors[] = $this->lang->error_missing_order;
                } else {
                    $this->bb->input['disporder'] = $this->bb->getInput('disporder', 0);
                    $query = $this->db->simple_select('smilies', 'sid', "disporder= '" . $this->bb->input['disporder'] . "' AND sid != '" . $smilie['sid'] . "'");
                    $duplicate_disporder = $this->db->fetch_field($query, 'sid');

                    if ($duplicate_disporder) {
                        $errors[] = $this->lang->error_duplicate_order;
                    }
                }

                if (!isset($errors)) {
                    $this->bb->input['find'] = str_replace("\r\n", "\n", $this->bb->input['find']);
                    $this->bb->input['find'] = str_replace("\r", "\n", $this->bb->input['find']);
                    $this->bb->input['find'] = explode("\n", $this->bb->input['find']);
                    foreach (array_merge(array_keys($this->bb->input['find'], ''), array_keys($this->bb->input['find'], ' ')) as $key) {
                        unset($this->bb->input['find'][$key]);
                    }
                    $this->bb->input['find'] = implode("\n", $this->bb->input['find']);

                    $updated_smilie = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'find' => $this->db->escape_string($this->bb->input['find']),
                        'image' => $this->db->escape_string($this->bb->input['image']),
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'showclickable' => $this->bb->getInput('showclickable', 0)
                    ];

                    $this->plugins->runHooks('admin_config_smilies_edit_commit');

                    $this->db->update_query('smilies', $updated_smilie, "sid = '{$smilie['sid']}'");

                    $this->cache->update_smilies();

                    // Log admin action
                    $this->bblogger->log_admin_action($smilie['sid'], htmlspecialchars_uni($this->bb->input['name']));

                    $this->session->flash_message($this->lang->success_smilie_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/smilies');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_smilie);
            $html = $this->page->output_header($this->lang->smilies . ' - ' . $this->lang->edit_smilie);

            $sub_tabs['edit_smilie'] = [
                'title' => $this->lang->edit_smilie,
                'link' => $this->bb->admin_url . '/config/smilies?action=edit',
                'description' => $this->lang->edit_smilie_desc
            ];
            $sub_tabs['mass_edit'] = [
                'title' => $this->lang->mass_edit,
                'link' => $this->bb->admin_url . '/config/smilies?action=mass_edit',
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_smilie');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/smilies?action=edit', 'post', 'edit');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('sid', $smilie['sid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, $smilie);
            }

            $form_container = new FormContainer($this, $this->lang->edit_smilie);
            $form_container->output_row($this->lang->name . ' <em>*</em>', '', $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->text_replace . ' <em>*</em>', $this->lang->text_replace_desc, $form->generate_text_area('find', $this->bb->input['find'], ['id' => 'find']), 'find');
            $form_container->output_row($this->lang->image_path . ' <em>*</em>', $this->lang->image_path_desc, $form->generate_text_box('image', $this->bb->input['image'], ['id' => 'image']), 'image');
            $form_container->output_row($this->lang->display_order . ' <em>*</em>', $this->lang->display_order_desc, $form->generate_numeric_field('disporder', $this->bb->input['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $form_container->output_row($this->lang->show_clickable . ' <em>*</em>', $this->lang->show_clickable_desc, $form->generate_yes_no_radio('showclickable', $this->bb->input['showclickable']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_smilie);
            $buttons[] = $form->generate_reset_button($this->lang->reset);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SmiliesEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('smilies', '*', "sid='" . $this->bb->getInput('sid', 0) . "'");
            $smilie = $this->db->fetch_array($query);

            // Does the smilie not exist?
            if (!$smilie['sid']) {
                $this->session->flash_message($this->lang->error_invalid_smilie, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/smilies');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/smilies');
            }

            $this->plugins->runHooks('admin_config_smilies_delete');

            if ($this->bb->request_method == 'post') {
                // Delete the smilie
                $this->db->delete_query('smilies', "sid='{$smilie['sid']}'");

                $this->plugins->runHooks('admin_config_smilies_delete_commit');

                $this->cache->update_smilies();

                // Log admin action
                $this->bblogger->log_admin_action($smilie['sid'], htmlspecialchars_uni($smilie['name']));

                $this->session->flash_message($this->lang->success_smilie_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/smilies');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/smilies?action=delete&sid=' . $smilie['sid'], $this->lang->confirm_smilie_deletion);
            }
        }

        if ($this->bb->input['action'] == 'add_multiple') {
            $this->plugins->runHooks('admin_config_smilies_add_multiple');

            if ($this->bb->request_method == 'post') {
                if ($this->bb->input['step'] == 1) {
                    $this->plugins->runHooks('admin_config_smilies_add_multiple_step1');

                    if (!trim($this->bb->input['pathfolder'])) {
                        $errors[] = $this->lang->error_missing_path_multiple;
                    }

                    $path = $this->bb->input['pathfolder'];
                    $dir = @opendir(MYBB_ROOT . $path);

                    if (!$dir) {
                        $errors[] = $this->lang->error_invalid_path;
                    }

                    if ($path && !is_array($errors)) {
                        if (substr($path, -1, 1) !== '/') {
                            $path .= '/';
                        }

                        $query = $this->db->simple_select('smilies');

                        $asmilies = [];
                        while ($smilie = $this->db->fetch_array($query)) {
                            $asmilies[$smilie['image']] = 1;
                        }

                        $smilies = [];
                        while ($file = readdir($dir)) {
                            if ($file != '..' && $file != '.') {
                                $ext = get_extension($file);
                                if ($ext == 'gif' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'bmp') {
                                    if (!$asmilies[$path . $file]) {
                                        $smilies[] = $file;
                                    }
                                }
                            }
                        }
                        closedir($dir);

                        if (count($smilies) == 0) {
                            $errors[] = $this->lang->error_no_smilies;
                        }
                    }

                    if (!$errors) {
                        $this->page->add_breadcrumb_item($this->lang->add_multiple_smilies);
                        $html = $this->page->output_header($this->lang->smilies . ' - ' . $this->lang->add_multiple_smilies);

                        $sub_tabs['manage_smilies'] = [
                            'title' => $this->lang->manage_smilies,
                            'link' => $this->bb->admin_url . '/config/smilies',
                        ];
                        $sub_tabs['add_smilie'] = [
                            'title' => $this->lang->add_smilie,
                            'link' => $this->bb->admin_url . '/config/smilies?action=add'
                        ];
                        $sub_tabs['add_multiple_smilies'] = [
                            'title' => $this->lang->add_multiple_smilies,
                            'link' => $this->bb->admin_url . '/config/smilies?action=add_multiple',
                            'description' => $this->lang->add_multiple_smilies_desc
                        ];
                        $sub_tabs['mass_edit'] = [
                            'title' => $this->lang->mass_edit,
                            'link' => $this->bb->admin_url . '/config/smilies?action=mass_edit'
                        ];

                        $html .= $this->page->output_nav_tabs($sub_tabs, 'add_multiple_smilies');
                        $form = new Form($this->bb, $this->bb->admin_url . '/config/smilies?action=add_multiple', 'post', 'add_multiple');
                        $html .= $form->getForm();
                        $html .= $form->generate_hidden_field('step', '2');
                        $html .= $form->generate_hidden_field('pathfolder', $path);

                        $form_container = new FormContainer($this, $this->lang->add_multiple_smilies);
                        $form_container->output_row_header($this->lang->image, ['class' => 'align_center', 'width' => '10%']);
                        $form_container->output_row_header($this->lang->name);
                        $form_container->output_row_header($this->lang->text_replace, ['width' => '20%']);
                        $form_container->output_row_header($this->lang->include, ['class' => 'align_center', 'width' => '5%']);

                        foreach ($smilies as $key => $file) {
                            $ext = get_extension($file);
                            $find = str_replace('.' . $ext, '', $file);
                            $name = ucfirst($find);

                            $form_container->output_cell("<img src=\"../" . $path . $file . "\" alt=\"\" /><br /><small>{$file}</small>", ["class" => "align_center", "width" => 1]);
                            $form_container->output_cell($form->generate_text_box("name[{$file}]", $name, ['id' => 'name', 'style' => 'width: 98%']));
                            $form_container->output_cell($form->generate_text_box("find[{$file}]", ":" . $find . ":", ['id' => 'find', 'style' => 'width: 95%']));
                            $form_container->output_cell($form->generate_check_box("include[{$file}]", 1, "", ['checked' => 1]), ["class" => "align_center"]);
                            $form_container->construct_row();
                        }

                        if ($form_container->num_rows() == 0) {
                            $this->session->flash_message($this->lang->error_no_images, 'error');
                            return $response->withRedirect($this->bb->admin_url . '/config/smilies?action=add_multiple');
                        }

                        $html .= $form_container->end();

                        $buttons[] = $form->generate_submit_button($this->lang->save_smilies);

                        $html .= $form->output_submit_wrapper($buttons);
                        $html .= $form->end();

                        $this->page->output_footer();
                        tdie($html);
                        exit;
                    }
                } else {
                    $this->plugins->runHooks('admin_config_smilies_add_multiple_step2');

                    $path = $this->bb->input['pathfolder'];
                    reset($this->bb->input['include']);
                    $find = $this->bb->input['find'];
                    $name = $this->bb->input['name'];

                    if (empty($this->bb->input['include'])) {
                        $this->session->flash_message($this->lang->error_none_included, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/config/smilies?action=add_multiple');
                    }

                    $query = $this->db->simple_select('smilies', 'MAX(disporder) as max_disporder');
                    $disporder = $this->db->fetch_field($query, 'max_disporder');

                    foreach ($this->bb->input['include'] as $image => $insert) {
                        $find[$image] = str_replace("\r\n", "\n", $find[$image]);
                        $find[$image] = str_replace("\r", "\n", $find[$image]);
                        $find[$image] = explode("\n", $find[$image]);
                        foreach (array_merge(array_keys($find[$image], ''), array_keys($find[$image], ' ')) as $key) {
                            unset($find[$image][$key]);
                        }
                        $find[$image] = implode("\n", $find[$image]);

                        if ($insert) {
                            $new_smilie = [
                                'name' => $this->db->escape_string($name[$image]),
                                'find' => $this->db->escape_string($find[$image]),
                                'image' => $this->db->escape_string($path . $image),
                                'disporder' => ++$disporder,
                                'showclickable' => 1
                            ];

                            $this->db->insert_query('smilies', $new_smilie);
                        }
                    }

                    $this->plugins->runHooks('admin_config_smilies_add_multiple_commit');

                    $this->cache->update_smilies();

                    // Log admin action
                    $this->bblogger->log_admin_action();

                    $this->session->flash_message($this->lang->success_multiple_smilies_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/smilies');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_multiple_smilies);
            $html = $this->page->output_header($this->lang->smilies . ' - ' . $this->lang->add_multiple_smilies);

            $sub_tabs['manage_smilies'] = [
                'title' => $this->lang->manage_smilies,
                'link' => $this->bb->admin_url . '/config/smilies',
            ];
            $sub_tabs['add_smilie'] = [
                'title' => $this->lang->add_smilie,
                'link' => $this->bb->admin_url . '/config/smilies?action=add'
            ];
            $sub_tabs['add_multiple_smilies'] = [
                'title' => $this->lang->add_multiple_smilies,
                'link' => $this->bb->admin_url . '/config/smilies?action=add_multiple',
                'description' => $this->lang->add_multiple_smilies_desc
            ];
            $sub_tabs['mass_edit'] = [
                'title' => $this->lang->mass_edit,
                'link' => $this->bb->admin_url . '/config/smilies?action=mass_edit'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_multiple_smilies');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/smilies?action=add_multiple', 'post', 'add_multiple');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('step', '1');

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_multiple_smilies);
            $form_container->output_row($this->lang->path_to_images, $this->lang->path_to_images_desc, $form->generate_text_box('pathfolder', $this->bb->getInput('pathfolder', ''), ['id' => 'pathfolder']), 'pathfolder');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->show_smilies);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SmiliesAddMultiple.html.twig');
        }

        if ($this->bb->input['action'] == 'mass_edit') {
            $this->plugins->runHooks('admin_config_smilies_mass_edit');

            if ($this->bb->request_method == 'post') {
                foreach ($this->bb->input['name'] as $sid => $name) {
                    $disporder = (int)$this->bb->input['disporder'][$sid];

                    $sid = (int)$sid;
                    if ($this->bb->input['delete'][$sid] == 1) {
                        // Dirty hack to get the disporder working. Note: this doesn't work in every case
                        unset($this->bb->input['disporder'][$sid]);

                        $this->db->delete_query('smilies', "sid = '{$sid}'", 1);
                    } else {
                        $this->bb->input['find'][$sid] = str_replace("\r\n", "\n", $this->bb->input['find'][$sid]);
                        $this->bb->input['find'][$sid] = str_replace("\r", "\n", $this->bb->input['find'][$sid]);
                        $this->bb->input['find'][$sid] = explode("\n", $this->bb->input['find'][$sid]);
                        foreach (array_merge(array_keys($this->bb->input['find'][$sid], ''), array_keys($this->bb->input['find'][$sid], ' ')) as $key) {
                            unset($this->bb->input['find'][$sid][$key]);
                        }
                        $this->bb->input['find'][$sid] = implode("\n", $this->bb->input['find'][$sid]);

                        $smilie = [
                            'name' => $this->db->escape_string($this->bb->input['name'][$sid]),
                            'find' => $this->db->escape_string($this->bb->input['find'][$sid]),
                            'showclickable' => $this->db->escape_string($this->bb->input['showclickable'][$sid])
                        ];

                        // $test contains all disporders except the actual one so we can check whether we have multiple disporders
                        $test = $this->bb->input['disporder'];
                        unset($test[$sid]);
                        if (!in_array($disporder, $test)) {
                            $smilie['disporder'] = $disporder;
                        }

                        $this->db->update_query('smilies', $smilie, "sid = '{$sid}'");
                    }

                    $disporder_list[$disporder] = $disporder;
                }

                $this->plugins->runHooks('admin_config_smilies_mass_edit_commit');

                $this->cache->update_smilies();

                // Log admin action
                $this->bblogger->log_admin_action();

                $this->session->flash_message($this->lang->success_multiple_smilies_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/smilies');
            }

            $this->page->add_breadcrumb_item($this->lang->mass_edit);
            $html = $this->page->output_header($this->lang->smilies . ' - ' . $this->lang->mass_edit);

            $sub_tabs['manage_smilies'] = [
                'title' => $this->lang->manage_smilies,
                'link' => $this->bb->admin_url . '/config/smilies',
            ];
            $sub_tabs['add_smilie'] = [
                'title' => $this->lang->add_smilie,
                'link' => $this->bb->admin_url . '/config/smilies?action=add',
            ];
            $sub_tabs['add_multiple_smilies'] = [
                'title' => $this->lang->add_multiple_smilies,
                'link' => $this->bb->admin_url . '/config/smilies?action=add_multiple',
            ];
            $sub_tabs['mass_edit'] = [
                'title' => $this->lang->mass_edit,
                'link' => $this->bb->admin_url . '/config/smilies?action=mass_edit',
                'description' => $this->lang->mass_edit_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'mass_edit');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/smilies?action=mass_edit', 'post', 'mass_edit');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['path'] = 'images/smilies/';
                $this->bb->input['showclickable'] = 1;
            }

            if (!isset($this->bb->input['disporder'])) {
                $query = $this->db->simple_select('smilies', 'max(disporder) as dispordermax');
                $this->bb->input['disporder'] = $this->db->fetch_field($query, 'dispordermax') + 1;
            }

            $form_container = new FormContainer($this, $this->lang->manage_smilies);
            $form_container->output_row_header($this->lang->image, ['class' => 'align_center', 'width' => '1']);
            $form_container->output_row_header($this->lang->name);
            $form_container->output_row_header($this->lang->text_replace, ['width' => '20%']);
            $form_container->output_row_header($this->lang->order, ['width' => '5%']);
            $form_container->output_row_header($this->lang->mass_edit_show_clickable, ['width' => 165]);
            $form_container->output_row_header($this->lang->smilie_delete, ['class' => 'align_center', 'width' => '5%']);

            $query = $this->db->simple_select('smilies', '*', '', ['order_by' => 'disporder']);
            while ($smilie = $this->db->fetch_array($query)) {
                $smilie['image'] = str_replace('{theme}', 'images', $smilie['image']);
                if (my_strpos($smilie['image'], 'p://') || substr($smilie['image'], 0, 1) == '/') {
                    $image = $smilie['image'];
                } else {
                    $image = $this->bb->asset_url . '/' . $smilie['image'];
                }

                $form_container->output_cell("<img src=\"{$image}\" alt=\"\" />", ["class" => "align_center", "width" => 1]);
                $form_container->output_cell($form->generate_text_box("name[{$smilie['sid']}]", $smilie['name'], ['id' => 'name', 'style' => 'width: 98%']));
                $form_container->output_cell($form->generate_text_area("find[{$smilie['sid']}]", $smilie['find'], ['id' => 'find', 'style' => 'width: 95%', 'rows' => 1]));
                $form_container->output_cell($form->generate_numeric_field("disporder[{$smilie['sid']}]", $smilie['disporder'], ['id' => 'disporder', 'style' => 'width: 40px', 'min' => 0]));
                $form_container->output_cell($form->generate_yes_no_radio("showclickable[{$smilie['sid']}]", $smilie['showclickable']), ["class" => "align_center"]);
                $form_container->output_cell($form->generate_check_box("delete[{$smilie['sid']}]", 1, $this->bb->getInput('delete', '')), ["class" => "align_center"]);
                $form_container->construct_row();
            }

            if ($form_container->num_rows() == 0) {
                $form_container->output_cell($this->lang->no_smilies, ['colspan' => 6]);
                $form_container->construct_row();
            }

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_smilies);
            $buttons[] = $form->generate_reset_button($this->lang->reset);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SmiliesMassEdit.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_smilies_start');

            $html = $this->page->output_header($this->lang->manage_smilies);

            $sub_tabs['manage_smilies'] = [
                'title' => $this->lang->manage_smilies,
                'link' => $this->bb->admin_url . '/config/smilies',
                'description' => $this->lang->manage_smilies_desc
            ];
            $sub_tabs['add_smilie'] = [
                'title' => $this->lang->add_smilie,
                'link' => $this->bb->admin_url . '/config/smilies?action=add',
            ];
            $sub_tabs['add_multiple_smilies'] = [
                'title' => $this->lang->add_multiple_smilies,
                'link' => $this->bb->admin_url . '/config/smilies?action=add_multiple',
            ];
            $sub_tabs['mass_edit'] = [
                'title' => $this->lang->mass_edit,
                'link' => $this->bb->admin_url . '/config/smilies?action=mass_edit',
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_smilies');

            $pagenum = $this->bb->getInput('page', 0);
            if ($pagenum) {
                $start = ($pagenum - 1) * 20;
            } else {
                $start = 0;
                $pagenum = 1;
            }


            $table = new Table;
            $table->construct_header($this->lang->image, ['class' => 'align_center', 'width' => 1]);
            $table->construct_header($this->lang->name, ['width' => '35%']);
            $table->construct_header($this->lang->text_replace, ['width' => '35%']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('smilies', '*', '', ['limit_start' => $start, 'limit' => 20, 'order_by' => 'disporder']);
            while ($smilie = $this->db->fetch_array($query)) {
                $smilie['image'] = str_replace('{theme}', 'images', $smilie['image']);
                if (my_strpos($smilie['image'], 'p://') || substr($smilie['image'], 0, 1) == '/') {
                    $image = $smilie['image'];
                    $smilie['image'] = str_replace('{theme}', 'images', $smilie['image']);
                } else {
                    $image = $this->bb->asset_url . '/' . $smilie['image'];
                }

                $table->construct_cell("<img src=\"{$image}\" alt=\"\" class=\"smilie smilie_{$smilie['sid']}\" />", ["class" => "align_center"]);
                $table->construct_cell(htmlspecialchars_uni($smilie['name']));
                $table->construct_cell(nl2br(htmlspecialchars_uni($smilie['find'])));

                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/smilies?action=edit&amp;sid={$smilie['sid']}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/smilies?action=delete&amp;sid={$smilie['sid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_smilie_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_smilies, ['colspan' => 5]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->manage_smilies);

            $query = $this->db->simple_select('smilies', 'COUNT(sid) as smilies');
            $total_rows = $this->db->fetch_field($query, 'smilies');

            $html .= '<br />' . $this->adm->draw_admin_pagination($pagenum, '20', $total_rows, $this->bb->admin_url . '/config/smilies?page={page}');

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Smilies.html.twig');
        }
    }
}
