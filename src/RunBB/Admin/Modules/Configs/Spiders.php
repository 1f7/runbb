<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Spiders extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_spiders', false, true);

        $this->page->add_breadcrumb_item($this->lang->spiders_bots, $this->bb->admin_url . '/config/spiders');

        $this->plugins->runHooks('admin_config_spiders_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_config_spiders_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['useragent'])) {
                    $errors[] = $this->lang->error_missing_agent;
                }

                if (!isset($errors)) {
                    $new_spider = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'theme' => $this->bb->getInput('theme', 0),
                        'language' => $this->db->escape_string($this->bb->input['language']),
                        'usergroup' => $this->bb->getInput('usergroup', 0),
                        'useragent' => $this->db->escape_string($this->bb->input['useragent']),
                        'lastvisit' => 0
                    ];
                    $sid = $this->db->insert_query('spiders', $new_spider);

                    $this->plugins->runHooks('admin_config_spiders_add_commit');

                    $this->cache->update_spiders();

                    // Log admin action
                    $this->bblogger->log_admin_action($sid, $this->bb->input['name']);

                    $this->session->flash_message($this->lang->success_bot_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/spiders');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_bot);
            $html = $this->page->output_header($this->lang->spiders_bots . ' - ' . $this->lang->add_new_bot);

            $sub_tabs['spiders'] = [
                'title' => $this->lang->spiders_bots,
                'link' => $this->bb->admin_url . '/config/spiders',
            ];
            $sub_tabs['add_spider'] = [
                'title' => $this->lang->add_new_bot,
                'link' => $this->bb->admin_url . '/config/spiders?action=add',
                'description' => $this->lang->add_new_bot_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_spider');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/spiders?action=add', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_new_bot);
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc, $form->generate_text_box('name', $this->bb->getInput('name', ''), ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->user_agent . ' <em>*</em>', $this->lang->user_agent_desc, $form->generate_text_box('useragent', $this->bb->getInput('useragent', ''), ['id' => 'useragent']), 'useragent');

            $languages = ['' => $this->lang->use_board_default];
            $languages = array_merge($languages, $this->lang->get_languages());
            $form_container->output_row($this->lang->language_str, $this->lang->language_desc, $form->generate_select_box('language', $languages, $this->bb->getInput('language', ''), ['id' => 'language']), 'language');

            $form_container->output_row($this->lang->theme, $this->lang->theme_desc, $this->themes->build_theme_select('theme', $this->bb->getInput('theme', ''), 0, '', true, false, true));

            $query = $this->db->simple_select('usergroups', '*', '', ['order_by' => 'title', 'order_dir' => 'asc']);

            $usergroups = [];
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = $usergroup['title'];
            }
            if (!isset($this->bb->input['usergroup'])) {
                $this->bb->input['usergroup'] = 1;
            }
            $form_container->output_row($this->lang->user_group, $this->lang->user_group_desc, $form->generate_select_box('usergroup', $usergroups, $this->bb->input['usergroup'], ['id' => 'usergroup']), 'usergroup');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_bot);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SpidersAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('spiders', '*', "sid='" . $this->bb->getInput('sid', 0) . "'");
            $spider = $this->db->fetch_array($query);

            // Does the spider not exist?
            if (!$spider['sid']) {
                $this->session->flash_message($this->lang->error_invalid_bot, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/spiders');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/spiders');
            }

            $this->plugins->runHooks('admin_config_spiders_delete');

            if ($this->bb->request_method == 'post') {
                // Delete the spider
                $this->db->delete_query('spiders', "sid='{$spider['sid']}'");

                $this->plugins->runHooks('admin_config_spiders_delete_commit');

                $this->cache->update_spiders();

                // Log admin action
                $this->bblogger->log_admin_action($spider['sid'], $spider['name']);

                $this->session->flash_message($this->lang->success_bot_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/spiders');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/spiders?action=delete&sid=' . $spider['sid'], $this->lang->confirm_bot_deletion);
            }
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('spiders', '*', "sid='" . $this->bb->getInput('sid', 0) . "'");
            $spider = $this->db->fetch_array($query);

            // Does the spider not exist?
            if (!$spider['sid']) {
                $this->session->flash_message($this->lang->error_invalid_bot, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/spiders');
            }

            $this->plugins->runHooks('admin_config_spiders_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['name'])) {
                    $errors[] = $this->lang->error_missing_name;
                }

                if (!trim($this->bb->input['useragent'])) {
                    $errors[] = $this->lang->error_missing_agent;
                }

                if (!isset($errors)) {
                    $updated_spider = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'theme' => $this->bb->getInput('theme', 0),
                        'language' => $this->db->escape_string($this->bb->input['language']),
                        'usergroup' => $this->bb->getInput('usergroup', 0),
                        'useragent' => $this->db->escape_string($this->bb->input['useragent'])
                    ];

                    $this->plugins->runHooks('admin_config_spiders_edit_commit');

                    $this->db->update_query('spiders', $updated_spider, "sid='{$spider['sid']}'");

                    $this->cache->update_spiders();

                    // Log admin action
                    $this->bblogger->log_admin_action($spider['sid'], $this->bb->input['name']);

                    $this->session->flash_message($this->lang->success_bot_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/spiders');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_bot);
            $html = $this->page->output_header($this->lang->spiders_bots . ' - ' . $this->lang->edit_bot);

            $sub_tabs['edit_spider'] = [
                'title' => $this->lang->edit_bot,
                'link' => $this->bb->admin_url . '/config/spiders?action=edit&sid=' . $spider['sid'],
                'description' => $this->lang->edit_bot_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_spider');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/spiders?action=edit&sid=' . $spider['sid'], 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
                $spider_data = $this->bb->input;
            } else {
                $spider_data = $spider;
            }

            $form_container = new FormContainer($this, $this->lang->edit_bot);
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc, $form->generate_text_box('name', $spider_data['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->user_agent . ' <em>*</em>', $this->lang->user_agent_desc, $form->generate_text_box('useragent', $spider_data['useragent'], ['id' => 'useragent']), 'useragent');

            $languages = ['' => $this->lang->use_board_default];
            $languages = array_merge($languages, $this->lang->get_languages());
            $form_container->output_row($this->lang->language_str, $this->lang->language_desc, $form->generate_select_box('language', $languages, $spider_data['language'], ['id' => 'language']), 'language');

            $form_container->output_row($this->lang->theme, $this->lang->theme_desc, $this->themes->build_theme_select('theme', $spider_data['theme'], 0, '', true, false, true));

            $query = $this->db->simple_select('usergroups', '*', '', ['order_by' => 'title', 'order_dir' => 'asc']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = $usergroup['title'];
            }
            if (!$spider_data['usergroup']) {
                $spider_data['usergroup'] = 1;
            }
            $form_container->output_row($this->lang->user_group, $this->lang->user_group_desc, $form->generate_select_box('usergroup', $usergroups, $spider_data['usergroup'], ['id' => 'usergroup']), 'usergroup');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_bot);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/SpidersEdit.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_spiders_start');

            $html = $this->page->output_header($this->lang->spiders_bots);

            $sub_tabs['spiders'] = [
                'title' => $this->lang->spiders_bots,
                'link' => $this->bb->admin_url . '/config/spiders',
                'description' => $this->lang->spiders_bots_desc
            ];
            $sub_tabs['add_spider'] = [
                'title' => $this->lang->add_new_bot,
                'link' => $this->bb->admin_url . '/config/spiders?action=add'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'spiders');

            $table = new Table;
            $table->construct_header($this->lang->bot);
            $table->construct_header($this->lang->last_visit, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150, 'colspan' => 2]);

            $query = $this->db->simple_select('spiders', '*', '', ['order_by' => 'lastvisit', 'order_dir' => 'desc']);
            while ($spider = $this->db->fetch_array($query)) {
                $lastvisit = $this->lang->never;
                $spider['name'] = htmlspecialchars_uni($spider['name']);

                if ($spider['lastvisit']) {
                    $lastvisit = $this->time->formatDate('relative', $spider['lastvisit']);
                }

                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/spiders?action=edit&amp;sid={$spider['sid']}\"><strong>{$spider['name']}</strong></a>");
                $table->construct_cell($lastvisit, ['class' => 'align_center', 'width' => 200]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/spiders?action=edit&amp;sid={$spider['sid']}\">{$this->lang->edit}</a>", ["class" => "align_center", "width" => 75]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/spiders?action=delete&amp;sid={$spider['sid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_bot_deletion}');\">{$this->lang->delete}</a>", ["class" => "align_center", "width" => 75]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_bots, ['colspan' => 4]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->spiders_bots);
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Spiders.html.twig');
        }
    }
}
