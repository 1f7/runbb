<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class ThreadPrefixes extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_thread_prefixes', false, true);

        $this->page->add_breadcrumb_item($this->lang->thread_prefixes, $this->bb->admin_url . '/config/thread_prefixes');

        $sub_tabs = [
            'thread_prefixes' => [
                'title' => $this->lang->thread_prefixes,
                'link' => $this->bb->admin_url . '/config/thread_prefixes',
                'description' => $this->lang->thread_prefixes_desc
            ],
            'add_prefix' => [
                'title' => $this->lang->add_new_thread_prefix,
                'link' => $this->bb->admin_url . '/config/thread_prefixes?action=add_prefix',
                'description' => $this->lang->add_new_thread_prefix_desc
            ]
        ];

        $this->plugins->runHooks('admin_config_thread_prefixes_begin');

        if ($this->bb->input['action'] == 'add_prefix') {
            $this->plugins->runHooks('admin_config_thread_prefixes_add_prefix');

            if ($this->bb->request_method == 'post') {
                if (trim($this->bb->input['prefix']) == '') {
                    $errors[] = $this->lang->error_missing_prefix;
                }

                if (trim($this->bb->input['displaystyle']) == '') {
                    $errors[] = $this->lang->error_missing_display_style;
                }

                if ($this->bb->input['forum_type'] == 2) {
                    if (count($this->bb->input['forum_1_forums']) < 1) {
                        $errors[] = $this->lang->error_no_forums_selected;
                    }

                    $forum_checked[2] = 'checked="checked"';
                } else {
                    $forum_checked[1] = 'checked="checked"';
                    $this->bb->input['forum_1_forums'] = '';
                }

                if ($this->bb->input['group_type'] == 2) {
                    if (count($this->bb->input['group_1_groups']) < 1) {
                        $errors[] = $this->lang->error_no_groups_selected;
                    }

                    $group_checked[2] = 'checked="checked"';
                } else {
                    $group_checked[1] = 'checked="checked"';
                    $this->bb->input['group_1_forums'] = '';
                }

                if (!isset($errors)) {
                    $new_prefix = [
                        'prefix' => $this->db->escape_string($this->bb->input['prefix']),
                        'displaystyle' => $this->db->escape_string($this->bb->input['displaystyle'])
                    ];

                    if ($this->bb->input['forum_type'] == 2) {
                        if (is_array($this->bb->input['forum_1_forums'])) {
                            $checked = [];
                            foreach ($this->bb->input['forum_1_forums'] as $fid) {
                                $checked[] = (int)$fid;
                            }

                            $new_prefix['forums'] = implode(',', $checked);
                        }
                    } else {
                        $new_prefix['forums'] = '-1';
                    }

                    if ($this->bb->input['group_type'] == 2) {
                        if (is_array($this->bb->input['group_1_groups'])) {
                            $checked = [];
                            foreach ($this->bb->input['group_1_groups'] as $gid) {
                                $checked[] = (int)$gid;
                            }

                            $new_prefix['groups'] = implode(',', $checked);
                        }
                    } else {
                        $new_prefix['groups'] = '-1';
                    }

                    $pid = $this->db->insert_query('threadprefixes', $new_prefix);

                    $this->plugins->runHooks('admin_config_thread_prefixes_add_prefix_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($pid, htmlspecialchars_uni($this->bb->input['prefix']));
                    $this->cache->update_threadprefixes();

                    $this->session->flash_message($this->lang->success_thread_prefix_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/thread_prefixes');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_thread_prefix);
            $html = $this->page->output_header($this->lang->thread_prefixes . ' - ' . $this->lang->add_new_thread_prefix);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_prefix');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/thread_prefixes?action=add_prefix', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['prefix'] = '';
                $this->bb->input['displaystyle'] = '';
                $this->bb->input['forum_1_forums'] = '';
                $forum_checked[1] = 'checked="checked"';
                $forum_checked[2] = '';
                $this->bb->input['group_1_groups'] = '';
                $group_checked[1] = 'checked="checked"';
                $group_checked[2] = '';
            }

            $form_container = new FormContainer($this, $this->lang->prefix_options);
            $form_container->output_row($this->lang->prefix . ' <em>*</em>', $this->lang->prefix_desc, $form->generate_text_box('prefix', $this->bb->input['prefix'], ['id' => 'prefix']), 'prefix');
            $form_container->output_row($this->lang->display_style . ' <em>*</em>', $this->lang->display_style_desc, $form->generate_text_box('displaystyle', $this->bb->input['displaystyle'], ['id' => 'displaystyle']), 'displaystyle');

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"1\" {$forum_checked[1]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"2\" {$forum_checked[2]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"forum_2\" class=\"forums\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
					<td>" . $form->generate_forum_select('forum_1_forums[]', $this->bb->input['forum_1_forums'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('forum');
	</script>";
            $form_container->output_row($this->lang->available_in_forums . ' <em>*</em>', '', $actions);

            $group_select = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"1\" {$group_checked[1]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"2\" {$group_checked[2]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"group_2\" class=\"groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('group_1_groups[]', $this->bb->input['group_1_groups'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
		checkAction('group');
	</script>";
            $form_container->output_row($this->lang->available_to_groups . ' <em>*</em>', '', $group_select);
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_thread_prefix);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ThreadPrefixesAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_prefix') {
            $prefix = $this->thread->build_prefixes($this->bb->input['pid']);
            if (empty($prefix['pid'])) {
                $this->session->flash_message($this->lang->error_invalid_prefix, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/thread_prefixes');
            }

            $this->plugins->runHooks('admin_config_thread_prefixes_edit_prefix_start');

            if ($this->bb->request_method == 'post') {
                if (trim($this->bb->input['prefix']) == '') {
                    $errors[] = $this->lang->error_missing_prefix;
                }

                if (trim($this->bb->input['displaystyle']) == '') {
                    $errors[] = $this->lang->error_missing_display_style;
                }

                if ($this->bb->input['forum_type'] == 2) {
                    if (count($this->bb->input['forum_1_forums']) < 1) {
                        $errors[] = $this->lang->error_no_forums_selected;
                    }

                    $forum_checked[2] = 'checked="checked"';
                } else {
                    $forum_checked[1] = 'checked="checked"';
                    $this->bb->input['forum_1_forums'] = '';
                }

                if ($this->bb->input['group_type'] == 2) {
                    if (count($this->bb->input['group_1_groups']) < 1) {
                        $errors[] = $this->lang->error_no_groups_selected;
                    }

                    $group_checked[2] = 'checked="checked"';
                } else {
                    $group_checked[1] = 'checked="checked"';
                    $this->bb->input['group_1_forums'] = '';
                }

                if (!isset($errors)) {
                    $update_prefix = [
                        'prefix' => $this->db->escape_string($this->bb->input['prefix']),
                        'displaystyle' => $this->db->escape_string($this->bb->input['displaystyle'])
                    ];

                    if ($this->bb->input['forum_type'] == 2) {
                        if (is_array($this->bb->input['forum_1_forums'])) {
                            $checked = [];
                            foreach ($this->bb->input['forum_1_forums'] as $fid) {
                                $checked[] = (int)$fid;
                            }

                            $update_prefix['forums'] = implode(',', $checked);
                        }
                    } else {
                        $update_prefix['forums'] = '-1';
                    }

                    if ($this->bb->input['group_type'] == 2) {
                        if (is_array($this->bb->input['group_1_groups'])) {
                            $checked = [];
                            foreach ($this->bb->input['group_1_groups'] as $gid) {
                                $checked[] = (int)$gid;
                            }

                            $update_prefix['groups'] = implode(',', $checked);
                        }
                    } else {
                        $update_prefix['groups'] = '-1';
                    }

                    $this->plugins->runHooks('admin_config_thread_prefixes_edit_prefix_commit');

                    $this->db->update_query('threadprefixes', $update_prefix, "pid='{$prefix['pid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($prefix['pid'], htmlspecialchars_uni($this->bb->input['prefix']));
                    $this->cache->update_threadprefixes();

                    $this->session->flash_message($this->lang->success_thread_prefix_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/thread_prefixes');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_thread_prefix);
            $html = $this->page->output_header($this->lang->thread_prefixes . ' - ' . $this->lang->edit_thread_prefix);

            // Setup the edit prefix tab
            unset($sub_tabs);
            $sub_tabs['edit_prefix'] = [
                'title' => $this->lang->edit_prefix,
                'link' => $this->bb->admin_url . '/config/thread_prefixes',
                'description' => $this->lang->edit_prefix_desc
            ];
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_prefix');

            $form = new Form($this->bb, $this->bb->admin_url . '/config/thread_prefixes?action=edit_prefix', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('pid', $prefix['pid']);

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $query = $this->db->simple_select('threadprefixes', '*', "pid = '{$prefix['pid']}'");
                $threadprefix = $this->db->fetch_array($query);

                $this->bb->input['prefix'] = $threadprefix['prefix'];
                $this->bb->input['displaystyle'] = $threadprefix['displaystyle'];
                $this->bb->input['forum_1_forums'] = explode(',', $threadprefix['forums']);

                if (!$threadprefix['forums'] || $threadprefix['forums'] == -1) {
                    $forum_checked[1] = 'checked="checked"';
                    $forum_checked[2] = '';
                } else {
                    $forum_checked[1] = '';
                    $forum_checked[2] = 'checked="checked"';
                }

                $this->bb->input['group_1_groups'] = explode(',', $threadprefix['groups']);

                if (!$threadprefix['groups'] || $threadprefix['groups'] == -1) {
                    $group_checked[1] = 'checked="checked"';
                    $group_checked[2] = '';
                } else {
                    $group_checked[1] = '';
                    $group_checked[2] = 'checked="checked"';
                }
            }

            $form_container = new FormContainer($this, $this->lang->prefix_options);
            $form_container->output_row($this->lang->prefix . ' <em>*</em>', $this->lang->prefix_desc, $form->generate_text_box('prefix', $this->bb->input['prefix'], ['id' => 'prefix']), 'prefix');
            $form_container->output_row($this->lang->display_style . ' <em>*</em>', $this->lang->display_style_desc, $form->generate_text_box('displaystyle', $this->bb->input['displaystyle'], ['id' => 'displaystyle']), 'displaystyle');

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"1\" {$forum_checked[1]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_forums}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"forum_type\" value=\"2\" {$forum_checked[2]} class=\"forums_check\" onclick=\"checkAction('forum');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_forums}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"forum_2\" class=\"forums\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->forums_colon}</small></td>
					<td>" . $form->generate_forum_select('forum_1_forums[]', $this->bb->input['forum_1_forums'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('forum');
	</script>";
            $form_container->output_row($this->lang->available_in_forums . ' <em>*</em>', '', $actions);

            $group_select = "
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"1\" {$group_checked[1]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->all_groups}</strong></label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"group_type\" value=\"2\" {$group_checked[2]} class=\"groups_check\" onclick=\"checkAction('group');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->select_groups}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"group_2\" class=\"groups\">
			<table cellpadding=\"4\">
				<tr>
					<td valign=\"top\"><small>{$this->lang->groups_colon}</small></td>
					<td>" . $form->generate_group_select('group_1_groups[]', $this->bb->input['group_1_groups'], ['multiple' => true, 'size' => 5]) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
		checkAction('group');
	</script>";
            $form_container->output_row($this->lang->available_to_groups . ' <em>*</em>', '', $group_select);
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_thread_prefix);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ThreadPrefixesEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_prefix') {
            $prefix = $this->thread->build_prefixes($this->bb->input['pid']);
            if (empty($prefix['pid'])) {
                $this->session->flash_message($this->lang->error_invalid_thread_prefix, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/thread_prefixes');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/thread_prefixes');
            }

            $this->plugins->runHooks('admin_config_thread_prefixes_delete_prefix');

            if ($this->bb->request_method == 'post') {
                // Remove prefix from existing threads
                $update_threads = ['prefix' => 0];

                // Delete prefix
                $this->db->delete_query('threadprefixes', "pid='{$prefix['pid']}'");

                $this->plugins->runHooks('admin_config_thread_prefixes_delete_thread_prefix_commit');

                $this->db->update_query('threads', $update_threads, "prefix='{$prefix['pid']}'");

                // Log admin action
                $this->bblogger->log_admin_action($prefix['pid'], htmlspecialchars_uni($prefix['prefix']));
                $this->cache->update_threadprefixes();

                $this->session->flash_message($this->lang->success_thread_prefix_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/thread_prefixes');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/thread_prefixes?action=delete_prefix&pid=' . $prefix['pid'], $this->lang->confirm_thread_prefix_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_thread_prefixes_start');

            $html = $this->page->output_header($this->lang->thread_prefixes);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'thread_prefixes');

            $table = new Table;
            $table->construct_header($this->lang->prefix);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $prefixes = $this->thread->build_prefixes();
            if (!empty($prefixes)) {
                foreach ($prefixes as $prefix) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/thread_prefixes?action=edit_prefix&amp;pid={$prefix['pid']}\"><strong>" . htmlspecialchars_uni($prefix['prefix']) . "</strong></a>");
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/thread_prefixes?action=edit_prefix&amp;pid={$prefix['pid']}\">{$this->lang->edit}</a>", ['width' => 100, 'class' => "align_center"]);
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/thread_prefixes?action=delete_prefix&amp;pid={$prefix['pid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_thread_prefix_deletion}')\">{$this->lang->delete}</a>", ['width' => 100, 'class' => 'align_center']);
                    $table->construct_row();
                }
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_thread_prefixes, ['colspan' => 3]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->thread_prefixes);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/ThreadPrefixes.html.twig');
        }
    }
}
