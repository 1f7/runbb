<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Configs;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;
use RunBB\Helpers\WarnHelper;

class Warning extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('configs');// set active module
        $this->lang->load('configs_warning', false, true);

        $this->page->add_breadcrumb_item($this->lang->warning_system, $this->bb->admin_url . '/config/warning');

        if ($this->bb->input['action'] == 'levels' ||
            $this->bb->input['action'] == 'add_type' ||
            $this->bb->input['action'] == 'add_level' ||
            !$this->bb->input['action']
        ) {
            $sub_tabs['manage_types'] = [
                'title' => $this->lang->warning_types,
                'link' => $this->bb->admin_url . '/config/warning',
                'description' => $this->lang->warning_types_desc
            ];
            $sub_tabs['add_type'] = [
                'title' => $this->lang->add_warning_type,
                'link' => $this->bb->admin_url . '/config/warning?action=add_type',
                'description' => $this->lang->add_warning_type_desc
            ];
            $sub_tabs['manage_levels'] = [
                'title' => $this->lang->warning_levels,
                'link' => $this->bb->admin_url . '/config/warning?action=levels',
                'description' => $this->lang->warning_levels_desc,
            ];
            $sub_tabs['add_level'] = [
                'title' => $this->lang->add_warning_level,
                'link' => $this->bb->admin_url . '/config/warning?action=add_level',
                'description' => $this->lang->add_warning_level_desc
            ];
        }
        $warn = new WarnHelper($this->db);
        $this->plugins->runHooks('admin_config_warning_begin');

        if ($this->bb->input['action'] == 'add_level') {
            $this->plugins->runHooks('admin_config_warning_add_level');

            if ($this->bb->request_method == 'post') {
                if (!is_numeric($this->bb->input['percentage']) ||
                    $this->bb->input['percentage'] > 100 ||
                    $this->bb->input['percentage'] < 0) {
                    $errors[] = $this->lang->error_invalid_warning_percentage;
                }

                if (!$this->bb->input['action_type']) {
                    $errors[] = $this->lang->error_missing_action_type;
                }

                if (!isset($errors)) {
                    // Ban
                    if ($this->bb->input['action_type'] == 1) {
                        $action = [
                            'type' => 1,
                            'usergroup' => $this->bb->getInput('action_1_usergroup', 0),
                            'length' => $warn->fetch_time_length($this->bb->input['action_1_time'], $this->bb->input['action_1_period'])
                        ];
                    } // Suspend posting
                    elseif ($this->bb->input['action_type'] == 2) {
                        $action = [
                            'type' => 2,
                            'length' => $warn->fetch_time_length($this->bb->input['action_2_time'], $this->bb->input['action_2_period'])
                        ];
                    } // Moderate posts
                    elseif ($this->bb->input['action_type'] == 3) {
                        $action = [
                            'type' => 3,
                            'length' => $warn->fetch_time_length($this->bb->input['action_3_time'], $this->bb->input['action_3_period'])
                        ];
                    }
                    $new_level = [
                        'percentage' => $this->bb->getInput('percentage', 0),
                        'action' => my_serialize($action)
                    ];

                    $lid = $this->db->insert_query('warninglevels', $new_level);

                    $this->plugins->runHooks('admin_config_warning_add_level_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($lid, $this->bb->input['percentage']);

                    $this->session->flash_message($this->lang->success_warning_level_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/warning?action=levels');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_warning_level);
            $html = $this->page->output_header($this->lang->warning_levels . ' - ' . $this->lang->add_warning_level);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_level');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/warning?action=add_level', 'post');
            $html .= $form->getForm();

            $action_checked = [
                1 => '',
                2 => '',
                3 => ''
            ];
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
                $action_checked[$this->bb->input['action_type']] = 'checked="checked"';
            }

            $form_container = new FormContainer($this, $this->lang->add_warning_level);
            $form_container->output_row($this->lang->warning_points_percentage, $this->lang->warning_points_percentage_desc, $form->generate_numeric_field('percentage', $this->bb->getInput('percentage', 0), ['id' => 'percentage', 'min' => 0, 'max' => 100]), 'percentage');

            $query = $this->db->simple_select('usergroups', '*', 'isbannedgroup=1');
            while ($group = $this->db->fetch_array($query)) {
                $banned_groups[$group['gid']] = $group['title'];
            }

            $periods = [
                'hours' => $this->lang->expiration_hours,
                'days' => $this->lang->expiration_days,
                'weeks' => $this->lang->expiration_weeks,
                'months' => $this->lang->expiration_months,
                'never' => $this->lang->expiration_permanent
            ];

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
	</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_type\" value=\"1\" {$action_checked[1]} class=\"actions_check\" onclick=\"checkAction('action');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->ban_user}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"action_1\" class=\"actions\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->banned_group}</small></td>
					<td>" . $form->generate_select_box('action_1_usergroup', $banned_groups, $this->bb->getInput('action_1_usergroup', 0)) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->ban_length}</small></td>
					<td>" . $form->generate_numeric_field('action_1_time', $this->bb->getInput('action_1_time', 0), ['style' => 'width: 3em;', 'min' => 0]) . " " . $form->generate_select_box('action_1_period', $periods, $this->bb->getInput('action_1_period', 0)) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_type\" value=\"2\" {$action_checked[2]} class=\"actions_check\" onclick=\"checkAction('action');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->suspend_posting_privileges}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"action_2\" class=\"actions\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->suspension_length}</small></td>
					<td>" . $form->generate_numeric_field('action_2_time', $this->bb->getInput('action_2_time', 0), ['style' => 'width: 3em;', 'min' => 0]) . " " . $form->generate_select_box('action_2_period', $periods, $this->bb->getInput('action_2_period', 0)) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_type\" value=\"3\" {$action_checked[3]} class=\"actions_check\" onclick=\"checkAction('action');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->moderate_posts}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"action_3\" class=\"actions\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->moderation_length}</small></td>
					<td>" . $form->generate_numeric_field('action_3_time', $this->bb->getInput('action_3_time', 0), ['style' => 'width: 3em;', 'min' => 0]) . " " . $form->generate_select_box('action_3_period', $periods, $this->bb->getInput('action_3_period', 0)) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('action');
	</script>";
            $form_container->output_row($this->lang->action_to_be_taken, $this->lang->action_to_be_taken_desc, $actions);
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_warning_level);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/WarningAddLevel.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_level') {
            $query = $this->db->simple_select('warninglevels', '*', "lid='" . $this->bb->getInput('lid', 0) . "'");
            $level = $this->db->fetch_array($query);

            // Does the warning level not exist?
            if (!$level['lid']) {
                $this->session->flash_message($this->lang->error_invalid_warning_level, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            }

            $this->plugins->runHooks('admin_config_warning_edit_level');

            if ($this->bb->request_method == 'post') {
                if (!is_numeric($this->bb->input['percentage']) || $this->bb->input['percentage'] > 100 || $this->bb->input['percentage'] < 0) {
                    $errors[] = $this->lang->error_invalid_warning_percentage;
                }

                if (!$this->bb->input['action_type']) {
                    $errors[] = $this->lang->error_missing_action_type;
                }

                if (!isset($errors)) {
                    // Ban
                    if ($this->bb->input['action_type'] == 1) {
                        $action = [
                            'type' => 1,
                            'usergroup' => $this->bb->getInput('action_1_usergroup', 0),
                            'length' => $warn->fetch_time_length($this->bb->input['action_1_time'], $this->bb->input['action_1_period'])
                        ];
                    } // Suspend posting
                    elseif ($this->bb->input['action_type'] == 2) {
                        $action = [
                            'type' => 2,
                            'length' => $warn->fetch_time_length($this->bb->input['action_2_time'], $this->bb->input['action_2_period'])
                        ];
                    } // Moderate posts
                    elseif ($this->bb->input['action_type'] == 3) {
                        $action = [
                            'type' => 3,
                            'length' => $warn->fetch_time_length($this->bb->input['action_3_time'], $this->bb->input['action_3_period'])
                        ];
                    }
                    $updated_level = [
                        'percentage' => $this->bb->getInput('percentage', 0),
                        'action' => my_serialize($action)
                    ];

                    $this->plugins->runHooks('admin_config_warning_edit_level_commit');

                    $this->db->update_query('warninglevels', $updated_level, "lid='{$level['lid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($level['lid'], $this->bb->input['percentage']);

                    $this->session->flash_message($this->lang->success_warning_level_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/warning?action=levels');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_warning_level);
            $html = $this->page->output_header($this->lang->warning_levels . ' - ' . $this->lang->edit_warning_level);

            $sub_tabs['edit_level'] = [
                'link' => $this->bb->admin_url . '/config/warning?action=edit_level&lid=' . $level['lid'],
                'title' => $this->lang->edit_warning_level,
                'description' => $this->lang->edit_warning_level_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_level');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/warning?action=edit_level&lid=' . $level['lid'], 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, [
                        'percentage' => $level['percentage'],
                    ]);
                $action = my_unserialize($level['action']);
                $action_checked = [
                    1 => '',
                    2 => '',
                    3 => ''
                ];
                if ($action['type'] == 1) {
                    $this->bb->input['action_1_usergroup'] = $action['usergroup'];
                    $length = $warn->fetch_friendly_expiration($action['length']);
                    $this->bb->input['action_1_time'] = $length['time'];
                    $this->bb->input['action_1_period'] = $length['period'];
                } elseif ($action['type'] == 2) {
                    $length = $warn->fetch_friendly_expiration($action['length']);
                    $this->bb->input['action_2_time'] = $length['time'];
                    $this->bb->input['action_2_period'] = $length['period'];
                } elseif ($action['type'] == 3) {
                    $length = $warn->fetch_friendly_expiration($action['length']);
                    $this->bb->input['action_3_time'] = $length['time'];
                    $this->bb->input['action_3_period'] = $length['period'];
                }
                $action_checked[$action['type']] = 'checked="checked"';
            }

            $form_container = new FormContainer($this, $this->lang->edit_warning_level);
            $form_container->output_row(
                $this->lang->warning_points_percentage,
                $this->lang->warning_points_percentage_desc,
                $form->generate_numeric_field('percentage', $this->bb->input['percentage'], ['id' => 'percentage', 'min' => 0, 'max' => 100]),
                'percentage'
            );

            $query = $this->db->simple_select('usergroups', '*', 'isbannedgroup=1');
            while ($group = $this->db->fetch_array($query)) {
                $banned_groups[$group['gid']] = $group['title'];
            }

            $periods = [
                'hours' => $this->lang->expiration_hours,
                'days' => $this->lang->expiration_days,
                'weeks' => $this->lang->expiration_weeks,
                'months' => $this->lang->expiration_months,
                'never' => $this->lang->expiration_permanent
            ];

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
	</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_type\" value=\"1\" {$action_checked[1]} class=\"actions_check\" onclick=\"checkAction('action');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->ban_user}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"action_1\" class=\"actions\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->banned_group}</small></td>
					<td>" . $form->generate_select_box('action_1_usergroup', $banned_groups, $this->bb->input['action_1_usergroup']) . "</td>
				</tr>
				<tr>
					<td><small>{$this->lang->ban_length}</small></td>
					<td>" . $form->generate_numeric_field('action_1_time', $this->bb->input['action_1_time'], ['style' => 'width: 3em;', 'min' => 0]) . " " . $form->generate_select_box('action_1_period', $periods, $this->bb->input['action_1_period']) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_type\" value=\"2\" {$action_checked[2]} class=\"actions_check\" onclick=\"checkAction('action');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->suspend_posting_privileges}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"action_2\" class=\"actions\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->suspension_length}</small></td>
					<td>" . $form->generate_numeric_field('action_2_time', $this->bb->getInput('action_2_time', 0), ['style' => 'width: 3em;', 'min' => 0]) . " " . $form->generate_select_box('action_2_period', $periods, $this->bb->getInput('action_2_period', '')) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_type\" value=\"3\" {$action_checked[3]} class=\"actions_check\" onclick=\"checkAction('action');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->moderate_posts}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"action_3\" class=\"actions\">
			<table cellpadding=\"4\">
				<tr>
					<td><small>{$this->lang->moderation_length}</small></td>
					<td>" . $form->generate_numeric_field('action_3_time', $this->bb->getInput('action_3_time', 0), ['style' => 'width: 3em;', 'min' => 0]) . " " . $form->generate_select_box('action_3_period', $periods, $this->bb->getInput('action_3_period', '')) . "</td>
				</tr>
			</table>
		</dd>
	</dl>
	<script type=\"text/javascript\">
	checkAction('action');
	</script>";
            $form_container->output_row($this->lang->action_to_be_taken, $this->lang->action_to_be_taken_desc, $actions);
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_warning_level);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/WarningEditLevel.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_level') {
            $query = $this->db->simple_select('warninglevels', '*', "lid='" . $this->bb->getInput('lid', 0) . "'");
            $level = $this->db->fetch_array($query);

            // Does the warning level not exist?
            if (!$level['lid']) {
                $this->session->flash_message($this->lang->error_invalid_warning_level, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            }

            $this->plugins->runHooks('admin_config_warning_delete_level');

            if ($this->bb->request_method == 'post') {
                // Delete the level
                $this->db->delete_query('warninglevels', "lid='{$level['lid']}'");

                $this->plugins->runHooks('admin_config_warning_delete_level_commit');

                // Log admin action
                $this->bblogger->log_admin_action($level['lid'], $level['percentage']);

                $this->session->flash_message($this->lang->success_warning_level_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/warning?action=delete_level&lid=' . $level['lid'], $this->lang->confirm_warning_level_deletion);
            }
        }

        if ($this->bb->input['action'] == 'add_type') {
            $this->plugins->runHooks('admin_config_warning_add_type');

            if ($this->bb->request_method === 'POST') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_type_title;
                }

                if (!is_numeric($this->bb->input['points']) ||
                    $this->bb->input['points'] > $this->bb->settings['maxwarningpoints'] ||
                    $this->bb->input['points'] <= 0
                ) {
                    $errors[] = $this->lang->sprintf($this->lang->error_missing_type_points, $this->bb->settings['maxwarningpoints']);
                }

                if (!isset($errors)) {
                    $new_type = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'points' => $this->bb->getInput('points', ''),
                        'expirationtime' => $warn->fetch_time_length($this->bb->input['expire_time'], $this->bb->input['expire_period'])
                    ];

                    $tid = $this->db->insert_query('warningtypes', $new_type);

                    $this->plugins->runHooks('admin_config_warning_add_type_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($tid, $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_warning_type_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/warning');
                }
            } else {
                $this->bb->input = array_merge($this->bb->input, [
                        'points' => '2',
                        'expire_time' => 1,
                        'expire_period' => 'days'
                    ]);
            }

            $this->page->add_breadcrumb_item($this->lang->add_warning_type);
            $html = $this->page->output_header($this->lang->warning_types . ' - ' . $this->lang->add_warning_type);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_type');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/warning?action=add_type', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_warning_type);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->points_to_add . ' <em>*</em>', $this->lang->points_to_add_desc, $form->generate_numeric_field('points', $this->bb->input['points'], ['id' => 'points', 'min' => 0, 'max' => $this->bb->settings['maxwarningpoints']]), 'points');
            $expiration_periods = [
                'hours' => $this->lang->expiration_hours,
                'days' => $this->lang->expiration_days,
                'weeks' => $this->lang->expiration_weeks,
                'months' => $this->lang->expiration_months,
                'never' => $this->lang->expiration_never
            ];
            $form_container->output_row($this->lang->warning_expiry, $this->lang->warning_expiry_desc, $form->generate_numeric_field('expire_time', $this->bb->input['expire_time'], ['id' => 'expire_time', 'min' => 0]) . ' ' . $form->generate_select_box('expire_period', $expiration_periods, $this->bb->input['expire_period'], ['id' => 'expire_period']), 'expire_time');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_warning_type);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/WarningAddType.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_type') {
            $query = $this->db->simple_select('warningtypes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $type = $this->db->fetch_array($query);

            // Does the warning type not exist?
            if (!$type['tid']) {
                $this->session->flash_message($this->lang->error_invalid_warning_type, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            }

            $this->plugins->runHooks('admin_config_warning_edit_type');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_type_title;
                }

                if (!is_numeric($this->bb->input['points']) ||
                    $this->bb->input['points'] > $this->bb->settings['maxwarningpoints'] ||
                    $this->bb->input['points'] < 0
                ) {
                    $errors[] = $this->lang->sprintf($this->lang->error_missing_type_points, $this->bb->settings['maxwarningpoints']);
                }

                if (!isset($errors)) {
                    $updated_type = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'points' => $this->bb->getInput('points', 0),
                        'expirationtime' => $warn->fetch_time_length($this->bb->input['expire_time'], $this->bb->input['expire_period'])
                    ];
                    $this->plugins->runHooks('admin_config_warning_edit_type_commit');
                    $this->db->update_query('warningtypes', $updated_type, "tid='{$type['tid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($type['tid'], $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_warning_type_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/config/warning');
                }
            } else {
                $expiration = $warn->fetch_friendly_expiration($type['expirationtime']);
                $this->bb->input = array_merge($this->bb->input, [
                        'title' => $type['title'],
                        'points' => $type['points'],
                        'expire_time' => $expiration['time'],
                        'expire_period' => $expiration['period']
                    ]);
            }

            $this->page->add_breadcrumb_item($this->lang->edit_warning_type);
            $html = $this->page->output_header($this->lang->warning_types . ' - ' . $this->lang->edit_warning_type);

            $sub_tabs['edit_type'] = [
                'link' => $this->bb->admin_url . '/config/warning?action=edit_type&tid=' . $type['tid'],
                'title' => $this->lang->edit_warning_type,
                'description' => $this->lang->edit_warning_type_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_type');
            $form = new Form($this->bb, $this->bb->admin_url . '/config/warning?action=edit_type&tid=' . $type['tid'], 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->edit_warning_type);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->points_to_add . ' <em>*</em>', $this->lang->points_to_add_desc, $form->generate_numeric_field('points', $this->bb->input['points'], ['id' => 'points', 'min' => 0, 'max' => $this->bb->settings['maxwarningpoints']]), 'points');
            $expiration_periods = [
                'hours' => $this->lang->expiration_hours,
                'days' => $this->lang->expiration_days,
                'weeks' => $this->lang->expiration_weeks,
                'months' => $this->lang->expiration_months,
                'never' => $this->lang->expiration_never
            ];
            $form_container->output_row($this->lang->warning_expiry, $this->lang->warning_expiry_desc, $form->generate_numeric_field('expire_time', $this->bb->input['expire_time'], ['id' => 'expire_time', 'min' => 0]) . ' ' . $form->generate_select_box('expire_period', $expiration_periods, $this->bb->input['expire_period'], ['id' => 'expire_period']), 'expire_time');
            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->save_warning_type);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/WarningEditType.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_type') {
            $query = $this->db->simple_select('warningtypes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $type = $this->db->fetch_array($query);

            // Does the warning type not exist?
            if (!$type['tid']) {
                $this->session->flash_message($this->lang->error_invalid_warning_type, 'error');
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            }

            $this->plugins->runHooks('admin_config_warning_delete_type');

            if ($this->bb->request_method == 'post') {
                // Delete the type
                $this->db->delete_query('warningtypes', "tid='{$type['tid']}'");

                $this->plugins->runHooks('admin_config_warning_delete_type_commit');

                // Log admin action
                $this->bblogger->log_admin_action($type['tid'], $type['title']);

                $this->session->flash_message($this->lang->success_warning_type_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/config/warning');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/config/warning?action=delete_type&tid=' . $type['tid'], $this->lang->confirm_warning_type_deletion);
            }
        }

        if ($this->bb->input['action'] == 'levels') {
            $this->plugins->runHooks('admin_config_warning_levels');

            $html = $this->page->output_header($this->lang->warning_levels);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_levels');

            $table = new Table;
            $table->construct_header($this->lang->percentage, ['width' => '5%', 'class' => 'align_center']);
            $table->construct_header($this->lang->action_to_take);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('warninglevels', '*', '', ['order_by' => 'percentage']);
            while ($level = $this->db->fetch_array($query)) {
                $table->construct_cell("<strong>{$level['percentage']}%</strong>", ['class' => 'align_center']);
                $action = my_unserialize($level['action']);
                $period = $warn->fetch_friendly_expiration($action['length']);

                // Get the right language for the ban period
                $lang_str = 'expiration_' . $period['period'];
                $period_str = $this->lang->$lang_str;

                if ($action['type'] == 1) {
                    $type = 'move_banned_group';
                    $group_name = $this->bb->groupscache[$action['usergroup']]['title'];
                } elseif ($action['type'] == 2) {
                    $type = 'suspend_posting';
                } elseif ($action['type'] == 3) {
                    $type = 'moderate_new_posts';
                }

                if ($period['period'] == 'never') {
                    $type .= '_permanent';

                    if ($group_name) {
                        // Permanently banned? Oh noes... switch group to the first sprintf replacement...
                        $period['time'] = $group_name;
                    }
                }

                // If this level is permanently in place, then $period_str and $group_name do not apply below...
                $type = $this->lang->sprintf($this->lang->$type, $period['time'], $period_str, $group_name);

                $table->construct_cell($type);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/warning?action=edit_level&amp;lid={$level['lid']}\">{$this->lang->edit}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/warning?action=delete_level&amp;lid={$level['lid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_warning_level_deletion}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_warning_levels, ['colspan' => 4]);
                $table->construct_row();
                $no_results = true;
            }

            $html .= $table->output($this->lang->warning_levels);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/WarningLevels.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_config_warning_start');

            $html = $this->page->output_header($this->lang->warning_types);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_types');

            $table = new Table;
            $table->construct_header($this->lang->warning_type);
            $table->construct_header($this->lang->points, ['width' => '5%', 'class' => 'align_center']);
            $table->construct_header($this->lang->expires_after, ['width' => '25%', 'class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2]);

            $query = $this->db->simple_select('warningtypes', '*', '', ['order_by' => 'title']);
            while ($type = $this->db->fetch_array($query)) {
                $type['name'] = htmlspecialchars_uni($type['title']);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/warning?action=edit_type&amp;tid={$type['tid']}\"><strong>{$type['title']}</strong></a>");
                $table->construct_cell("{$type['points']}", ['class' => 'align_center']);
                $expiration = $warn->fetch_friendly_expiration($type['expirationtime']);
                $lang_str = 'expiration_' . $expiration['period'];
                if ($type['expirationtime'] > 0) {
                    $table->construct_cell("{$expiration['time']} {$this->lang->$lang_str}", ['class' => 'align_center']);
                } else {
                    $table->construct_cell($this->lang->never, ['class' => 'align_center']);
                }
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/warning?action=edit_type&amp;tid={$type['tid']}\">{$this->lang->edit}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/config/warning?action=delete_type&amp;tid={$type['tid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_warning_type_deletion}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_warning_types, ['colspan' => 5]);
                $table->construct_row();
                $no_results = true;
            }

            $html .= $table->output($this->lang->warning_types);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Config/Warning.html.twig');
        }
    }
}
