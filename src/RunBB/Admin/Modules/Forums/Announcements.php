<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Forums;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\Table;
use RunCMF\Core\AbstractController;

class Announcements extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('forums');// set active module
        $this->lang->load('forums_announcements', false, true);

        $this->page->add_breadcrumb_item($this->lang->forum_announcements, $this->bb->admin_url . '/forum/announcements');
        $errors = [];
        if ($this->bb->input['action'] == 'add' || !$this->bb->input['action']) {
            $sub_tabs['forum_announcements'] = [
                'title' => $this->lang->forum_announcements,
                'link' => $this->bb->admin_url . '/forum/announcements',
                'description' => $this->lang->forum_announcements_desc
            ];

            $sub_tabs['add_announcement'] = [
                'title' => $this->lang->add_announcement,
                'link' => $this->bb->admin_url . '/forum/announcements?action=add',
                'description' => $this->lang->add_announcement_desc
            ];
        } elseif ($this->bb->input['action'] == 'edit') {
            $sub_tabs['forum_announcements'] = [
                'title' => $this->lang->forum_announcements,
                'link' => $this->bb->admin_url . '/forum/announcements',
                'description' => $this->lang->forum_announcements_desc
            ];

            $sub_tabs['update_announcement'] = [
                'title' => $this->lang->update_announcement,
                'link' => $this->bb->admin_url . '/forum/announcements?action=add',
                'description' => $this->lang->update_announcement_desc
            ];
        }

        $this->plugins->runHooks('admin_forum_announcements_begin');
        //$months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $months = [1 => 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_forum_announcements_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['message'])) {
                    $errors[] = $this->lang->error_missing_message;
                }

                if (!trim($this->bb->input['fid'])) {
                    $errors[] = $this->lang->error_missing_forum;
                }

                if (!checkdate($this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0))) {
                    $errors[] = $this->lang->error_invalid_start_date;
                }

                // End before startdate?
                $startdate = @explode(' ', $this->bb->input['starttime_time']);
                $startdate = @explode(':', $startdate[0]);
                $enddate = @explode(' ', $this->bb->input['endtime_time']);
                $enddate = @explode(':', $enddate[0]);

                if (stristr($this->bb->input['starttime_time'], 'pm')) {
                    $startdate[0] = 12 + $startdate[0];
                    if ($startdate[0] >= 24) {
                        $startdate[0] = '00';
                    }
                }

                if (stristr($this->bb->input['endtime_time'], 'pm')) {
                    $enddate[0] = 12 + $enddate[0];
                    if ($enddate[0] >= 24) {
                        $enddate[0] = '00';
                    }
                }

                $startdate = gmmktime((int)$startdate[0], (int)$startdate[1], 0, $this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0));

                if ($this->bb->input['endtime_type'] != '2') {
                    $enddate = gmmktime((int)$enddate[0], (int)$enddate[1], 0, $this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0));
                    if (!checkdate($this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0))) {
                        $errors[] = $this->lang->error_invalid_end_date;
                    }
                    if ($enddate <= $startdate) {
                        $errors[] = $this->lang->error_end_before_start;
                    }
                }

                if (!$errors) {
                    if (isset($this->bb->input['preview'])) {
                        $parser_options = [];
                        $parser_options['allow_html'] = $this->bb->getInput('allowhtml', 0);
                        $parser_options['allow_mycode'] = $this->bb->getInput('allowmycode', 0);
                        $parser_options['allow_smilies'] = $this->bb->getInput('allowsmilies', 0);
                        $parser_options['allow_imgcode'] = 1;
                        $parser_options['allow_videocode'] = 1;
                        $parser_options['me_username'] = htmlspecialchars_uni($this->user->username);
                        $parser_options['filter_badwords'] = 1;

                        $preview['message'] = $this->parser->parse_message($this->bb->input['message'], $parser_options);
                        $preview['subject'] = htmlspecialchars_uni($this->bb->input['title']);
                    } else {
                        if (!in_array($this->bb->input['starttime_month'], $months)) {
                            $this->bb->input['starttime_month'] = 1;
                        }

                        if ($this->bb->input['endtime_type'] == '2') {
                            $enddate = '0';
                        } else {
                            if (!in_array($this->bb->input['endtime_month'], $months)) {
                                $this->bb->input['endtime_month'] = 1;
                            }
                        }

                        $insert_announcement = [
                            'fid' => $this->bb->input['fid'],
                            'uid' => $this->user->uid,
                            'subject' => $this->db->escape_string($this->bb->input['title']),
                            'message' => $this->db->escape_string($this->bb->input['message']),
                            'startdate' => $startdate,
                            'enddate' => $enddate,
                            'allowhtml' => $this->bb->getInput('allowhtml', 0),
                            'allowmycode' => $this->bb->getInput('allowmycode', 0),
                            'allowsmilies' => $this->bb->getInput('allowsmilies', 0)
                        ];

                        $aid = $this->db->insert_query('announcements', $insert_announcement);

                        $this->plugins->runHooks('admin_forum_announcements_add_commit');

                        // Log admin action
                        $this->bblogger->log_admin_action($aid, $this->bb->input['title']);
                        $this->cache->update_forumsdisplay();

                        $this->session->flash_message($this->lang->success_added_announcement, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
                    }
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_an_announcement);
            $html = $this->page->output_header($this->lang->add_an_announcement);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_announcement');

            $form = new Form($this->bb, $this->bb->admin_url . '/forum/announcements?action=add', 'post');
            $html .= $form->getForm();
            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            $default_options = [
                'starttime_time',
                'starttime_day',
                'starttime_month',
                'starttime_year',
                'endtime_type',
                'endtime_time',
                'endtime_day',
                'endtime_month',
                'endtime_year',
                'title',
                'message',
                'fid',
                'allowhtml',
                'allowmycode',
                'allowsmilies'
            ];

            foreach ($default_options as $option) {
                if (!isset($this->bb->input[$option])) {
                    $this->bb->input[$option] = '';
                }
            }

            if ($this->bb->input['endtime_type'] == '1') {
                $endtime_checked[1] = 'checked="checked"';
                $endtime_checked[2] = '';
            } else {
                $endtime_checked[1] = '';
                $endtime_checked[2] = 'checked="checked"';
            }

            if (!$this->bb->input['starttime_time']) {
                $start_time = explode('-', gmdate('g-i-a', TIME_NOW));
                $this->bb->input['starttime_time'] = $start_time[0] . ':' . $start_time[1] . ' ' . $start_time[2];
            }

            if (!$this->bb->input['endtime_time']) {
                $end_time = explode('-', gmdate('g-i-a', TIME_NOW));
                $this->bb->input['endtime_time'] = $end_time[0] . ':' . $end_time[1] . ' ' . $end_time[2];
            }

            if ($this->bb->input['starttime_day']) {
                $startday = $this->bb->getInput('starttime_day', 0);
            } else {
                $startday = gmdate('j', TIME_NOW);
            }

            if ($this->bb->input['endtime_day']) {
                $endday = $this->bb->getInput('endtime_day', 0);
            } else {
                $endday = gmdate('j', TIME_NOW);
            }

            $startdateday = $enddateday = $startdatemonth = $enddatemonth = '';

            // Days
            for ($i = 1; $i <= 31; ++$i) {
                if ($startday == $i) {
                    $startdateday .= "<option value=\"$i\" selected=\"selected\">$i</option>\n";
                } else {
                    $startdateday .= "<option value=\"$i\">$i</option>\n";
                }

                if ($endday == $i) {
                    $enddateday .= "<option value=\"$i\" selected=\"selected\">$i</option>\n";
                } else {
                    $enddateday .= "<option value=\"$i\">$i</option>\n";
                }
            }

//      // Months
//      for($i = 1; $i <= 12; ++$i)
//      {
//        $endmonthsel[$i] = $startmonthsel[$i] = '';
//      }
            $endmonthsel = $startmonthsel = $months;
            if ($this->bb->input['starttime_month']) {
                $startmonth = $this->bb->getInput('starttime_month', 0);
                $startmonthsel[$startmonth] = 'selected="selected"';
            } else {
                $startmonth = gmdate('m', TIME_NOW);
                $startmonthsel[$startmonth] = 'selected="selected"';
            }

            if ($this->bb->input['endtime_month']) {
                $endmonth = $this->bb->getInput('endtime_month', 0);
                $endmonthsel[$endmonth] = 'selected="selected"';
            } else {
                $endmonth = gmdate('m', TIME_NOW);
                $endmonthsel[$endmonth] = 'selected="selected"';
            }
            $startdatemonth .= "<option value=\"1\" {$startmonthsel[1]}>{$this->lang->january}</option>\n";
            $enddatemonth .= "<option value=\"1\" {$endmonthsel[1]}>{$this->lang->january}</option>\n";
            $startdatemonth .= "<option value=\"2\" {$startmonthsel[2]}>{$this->lang->february}</option>\n";
            $enddatemonth .= "<option value=\"2\" {$endmonthsel[2]}>{$this->lang->february}</option>\n";
            $startdatemonth .= "<option value=\"3\" {$startmonthsel[3]}>{$this->lang->march}</option>\n";
            $enddatemonth .= "<option value=\"3\" {$endmonthsel[3]}>{$this->lang->march}</option>\n";
            $startdatemonth .= "<option value=\"4\" {$startmonthsel[4]}>{$this->lang->april}</option>\n";
            $enddatemonth .= "<option value=\"4\" {$endmonthsel[4]}>{$this->lang->april}</option>\n";
            $startdatemonth .= "<option value=\"5\" {$startmonthsel[5]}>{$this->lang->may}</option>\n";
            $enddatemonth .= "<option value=\"5\" {$endmonthsel[5]}>{$this->lang->may}</option>\n";
            $startdatemonth .= "<option value=\"6\" {$startmonthsel[6]}>{$this->lang->june}</option>\n";
            $enddatemonth .= "<option value=\"6\" {$endmonthsel[6]}>{$this->lang->june}</option>\n";
            $startdatemonth .= "<option value=\"7\" {$startmonthsel[7]}>{$this->lang->july}</option>\n";
            $enddatemonth .= "<option value=\"7\" {$endmonthsel[7]}>{$this->lang->july}</option>\n";
            $startdatemonth .= "<option value=\"8\" {$startmonthsel[8]}>{$this->lang->august}</option>\n";
            $enddatemonth .= "<option value=\"8\" {$endmonthsel[9]}>{$this->lang->august}</option>\n";
            $startdatemonth .= "<option value=\"9\" {$startmonthsel[9]}>{$this->lang->september}</option>\n";
            $enddatemonth .= "<option value=\"9\" {$endmonthsel[9]}>{$this->lang->september}</option>\n";
            $startdatemonth .= "<option value=\"10\" {$startmonthsel[10]}>{$this->lang->october}</option>\n";
            $enddatemonth .= "<option value=\"10\" {$endmonthsel[10]}>{$this->lang->october}</option>\n";
            $startdatemonth .= "<option value=\"11\" {$startmonthsel[11]}>{$this->lang->november}</option>\n";
            $enddatemonth .= "<option value=\"11\" {$endmonthsel[11]}>{$this->lang->november}</option>\n";
            $startdatemonth .= "<option value=\"12\" {$startmonthsel[12]}>{$this->lang->december}</option>\n";
            $enddatemonth .= "<option value=\"12\" {$endmonthsel[12]}>{$this->lang->december}</option>\n";

            if ($this->bb->input['starttime_year']) {
                $startdateyear = $this->bb->getInput('starttime_year', 0);
            } else {
                $startdateyear = gmdate('Y', TIME_NOW);
            }

            if ($this->bb->input['endtime_year']) {
                $enddateyear = $this->bb->getInput('endtime_year', 0);
            } else {
                $enddateyear = gmdate('Y', TIME_NOW) + 1;
            }

            if (isset($preview)) {
                $form_container = new FormContainer($this, $this->lang->announcement_preview);
                $form_container->output_row($preview['subject'], '', $preview['message'], 'preview');
                $html .= $form_container->end();
            }

            $form_container = new FormContainer($this, $this->lang->add_an_announcement);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->start_date . ' <em>*</em>', $this->lang->start_date_desc, "<select name=\"starttime_day\">\n{$startdateday}</select>\n &nbsp; \n<select name=\"starttime_month\">\n{$startdatemonth}</select>\n &nbsp; \n<input type=\"text\" name=\"starttime_year\" value=\"{$startdateyear}\" size=\"4\" maxlength=\"4\" />\n - {$this->lang->time} " . $form->generate_text_box('starttime_time', $this->bb->input['starttime_time'], ['id' => 'starttime_time', 'style' => 'width: 50px;']));

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"endtime_type\" value=\"1\" {$endtime_checked[1]} class=\"endtimes_check\" onclick=\"checkAction('endtime');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->set_time}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"endtime_1\" class=\"endtimes\">
			<table cellpadding=\"4\">
				<tr>
					<td><select name=\"endtime_day\">\n{$enddateday}</select>\n &nbsp; \n<select name=\"endtime_month\">\n{$enddatemonth}</select>\n &nbsp; \n<input type=\"text\" name=\"endtime_year\" value=\"{$enddateyear}\" class=\"text_input\" size=\"4\" maxlength=\"4\" />\n - {$this->lang->time} " . $form->generate_text_box('endtime_time', $this->bb->input['endtime_time'], ['id' => 'endtime_time', 'style' => 'width: 50px;']) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"endtime_type\" value=\"2\" {$endtime_checked[2]} class=\"endtimes_check\" onclick=\"checkAction('endtime');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->never}</strong></label></dt>
	</dl>
	<script type=\"text/javascript\">
	checkAction('endtime');
	</script>";
            $form_container->output_row($this->lang->end_date . ' <em>*</em>', $this->lang->end_date_desc, $actions);
            $form_container->output_row($this->lang->message . ' <em>*</em>', '', $form->generate_text_area('message', $this->bb->input['message'], ['id' => 'message']), 'message');
            $form_container->output_row($this->lang->forums_to_appear_in . ' <em>*</em>', $this->lang->forums_to_appear_in_desc, $form->generate_forum_select('fid', $this->bb->input['fid'], ['size' => 5, 'main_option' => $this->lang->all_forums]));
            $form_container->output_row($this->lang->allow_html . ' <em>*</em>', '', $form->generate_yes_no_radio('allowhtml', $this->bb->input['allowhtml'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->allow_mycode . ' <em>*</em>', '', $form->generate_yes_no_radio('allowmycode', $this->bb->input['allowmycode'], ['style' => 'width: 2em;']));
            $form_container->output_row($this->lang->allow_smilies . ' <em>*</em>', '', $form->generate_yes_no_radio('allowsmilies', $this->bb->input['allowsmilies'], ['style' => 'width: 2em;']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_announcement);
            $buttons[] = $form->generate_submit_button($this->lang->preview_announcement, ['name' => 'preview']);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/AnnouncementsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            if (!trim($this->bb->input['aid'])) {
                $this->session->flash_message($this->lang->error_invalid_announcement, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
            }

            $this->plugins->runHooks('admin_forum_announcements_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['message'])) {
                    $errors[] = $this->lang->error_missing_message;
                }

                if (!trim($this->bb->input['fid'])) {
                    $errors[] = $this->lang->error_missing_forum;
                }

                if (!checkdate($this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0))) {
                    $errors[] = $this->lang->error_invalid_start_date;
                }

                // End before startdate?
                $startdate = @explode(' ', $this->bb->input['starttime_time']);
                $startdate = @explode(':', $startdate[0]);
                $enddate = @explode(' ', $this->bb->input['endtime_time']);
                $enddate = @explode(':', $enddate[0]);

                if (stristr($this->bb->input['starttime_time'], 'pm')) {
                    $startdate[0] = 12 + $startdate[0];
                    if ($startdate[0] >= 24) {
                        $startdate[0] = '00';
                    }
                }

                if (stristr($this->bb->input['endtime_time'], 'pm')) {
                    $enddate[0] = 12 + $enddate[0];
                    if ($enddate[0] >= 24) {
                        $enddate[0] = '00';
                    }
                }

                $startdate = gmmktime((int)$startdate[0], (int)$startdate[1], 0, $this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0));

                if ($this->bb->input['endtime_type'] != '2') {
                    $enddate = gmmktime((int)$enddate[0], (int)$enddate[1], 0, $this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0));
                    if (!checkdate($this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0))) {
                        $errors[] = $this->lang->error_invalid_end_date;
                    }
                    if ($enddate <= $startdate) {
                        $errors[] = $this->lang->error_end_before_start;
                    }
                }

                if (!$errors) {
                    if (isset($this->bb->input['preview'])) {
                        $parser_options = [];
                        $parser_options['allow_html'] = $this->bb->getInput('allowhtml', 0);
                        $parser_options['allow_mycode'] = $this->bb->getInput('allowmycode', 0);
                        $parser_options['allow_smilies'] = $this->bb->getInput('allowsmilies', 0);
                        $parser_options['allow_imgcode'] = 1;
                        $parser_options['allow_videocode'] = 1;
                        $parser_options['me_username'] = htmlspecialchars_uni($this->user->username);
                        $parser_options['filter_badwords'] = 1;

                        $preview = [];
                        $preview['message'] = $this->parser->parse_message($this->bb->input['message'], $parser_options);
                        $preview['subject'] = htmlspecialchars_uni($this->bb->input['title']);
                    } else {
                        //$months = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                        //$months = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
                        if (!in_array($this->bb->input['starttime_month'], $months)) {
                            $this->bb->input['starttime_month'] = 1;
                        }

                        if ($this->bb->input['endtime_type'] == '2') {
                            $enddate = '0';
                        } else {
                            if (!in_array($this->bb->input['endtime_month'], $months)) {
                                $this->bb->input['endtime_month'] = 1;
                            }
                        }

                        $update_announcement = [
                            'fid' => $this->bb->input['fid'],
                            'subject' => $this->db->escape_string($this->bb->input['title']),
                            'message' => $this->db->escape_string($this->bb->input['message']),
                            'startdate' => $startdate,
                            'enddate' => $enddate,
                            'allowhtml' => $this->bb->getInput('allowhtml', 0),
                            'allowmycode' => $this->bb->getInput('allowmycode', 0),
                            'allowsmilies' => $this->bb->getInput('allowsmilies', 0)
                        ];

                        $this->plugins->runHooks('admin_forum_announcements_edit_commit');

                        $this->db->update_query('announcements', $update_announcement, "aid='{$this->bb->input['aid']}'");

                        // Log admin action
                        $this->bblogger->log_admin_action($this->bb->input['aid'], $this->bb->input['title']);
                        $this->cache->update_forumsdisplay();

                        $this->session->flash_message($this->lang->success_updated_announcement, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
                    }
                }
            }

            $this->page->add_breadcrumb_item($this->lang->update_an_announcement);
            $html = $this->page->output_header($this->lang->update_an_announcement);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'update_announcement');

            $form = new Form($this->bb, $this->bb->admin_url . '/forum/announcements?action=edit', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('aid', $this->bb->input['aid']);
            $startmonthsel = $endmonthsel = $months;
            if ($errors || isset($this->bb->input['preview'])) {
                // Only show errors if we have any
                if ($errors) {
                    $html .= $this->page->output_inline_error($errors);
                }

                // Gather start and end date data
                $startday = $this->bb->input['starttime_day'];
                $start_time = $this->bb->input['starttime_time'];
                $startmonth = $this->bb->input['starttime_month'];
                $startmonthsel[$startmonth] = 'selected="selected"';
                $startdateyear = $this->bb->input['starttime_year'];

                if ($this->bb->input['endtime_type'] == 1) {
                    // Set time
                    $endtime_checked[1] = 'checked="checked"';
                    $endtime_checked[2] = '';

                    $endday = $this->bb->input['endtime_day'];
                    $endtime = $this->bb->input['endtime_time'];
                    $endmonth = $this->bb->input['endtime_month'];
                    $endmonthsel[$endmonth] = 'selected';
                    $enddateyear = $this->bb->input['endtime_year'];
                } else {
                    // Never
                    $endtime_checked[1] = '';
                    $endtime_checked[2] = 'checked="checked"';

                    $endday = $startday;
                    $endmonth = $startmonth;
                    $endmonthsel[$endmonth] = 'selected';
                    $enddateyear = $startdateyear + 1;
                }
            } elseif (!isset($this->bb->input['preview'])) {
                $query = $this->db->simple_select('announcements', '*', "aid='{$this->bb->input['aid']}'");
                $announcement = $this->db->fetch_array($query);

                if (!$announcement) {
                    $this->session->flash_message($this->lang->error_invalid_announcement, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
                }

                $start_time = explode('-', gmdate('g-i-a', $announcement['startdate']));
                $this->bb->input['starttime_time'] = $start_time[0] . ':' . $start_time[1] . ' ' . $start_time[2];

//        $startmonthsel = $endmonthsel = $months;
                $startday = gmdate('j', $announcement['startdate']);
                //$startmonth = gmdate('m', $announcement['startdate']);
                $startmonth = gmdate('n', $announcement['startdate']);
                $startmonthsel[$startmonth] = 'selected="selected"';
                $startdateyear = gmdate('Y', $announcement['startdate']);

                $this->bb->input['title'] = $announcement['subject'];
                $this->bb->input['message'] = $announcement['message'];
                $this->bb->input['allowhtml'] = $announcement['allowhtml'];
                $this->bb->input['allowsmilies'] = $announcement['allowsmilies'];
                $this->bb->input['allowmycode'] = $announcement['allowmycode'];
                $this->bb->input['fid'] = $announcement['fid'];

                if ($announcement['enddate']) {
                    $endtime_checked[1] = 'checked="checked"';
                    $endtime_checked[2] = '';

                    $end_time = explode('-', gmdate('g-i-a', $announcement['enddate']));
                    $this->bb->input['endtime_time'] = $end_time[0] . ':' . $end_time[1] . ' ' . $end_time[2];

                    $endday = gmdate('j', $announcement['enddate']);

                    //$endmonth = gmdate('m', $announcement['enddate']);
                    $endmonth = gmdate('n', $announcement['enddate']);
                    $endmonthsel[$endmonth] = 'selected';

                    $enddateyear = gmdate('Y', $announcement['enddate']);
                } else {
                    $endtime_checked[1] = '';
                    $endtime_checked[2] = 'checked="checked"';

                    $this->bb->input['endtime_time'] = $this->bb->input['starttime_time'];
                    $endday = $startday;
                    $endmonth = $startmonth;
                    $enddateyear = $startdateyear + 1;
                }
            }
            $startdateday = $enddateday = '';
            for ($i = 1; $i <= 31; ++$i) {
                if ($startday == $i) {
                    $startdateday .= "<option value=\"$i\" selected=\"selected\">$i</option>\n";
                } else {
                    $startdateday .= "<option value=\"$i\">$i</option>\n";
                }

                if ($endday == $i) {
                    $enddateday .= "<option value=\"$i\" selected=\"selected\">$i</option>\n";
                } else {
                    $enddateday .= "<option value=\"$i\">$i</option>\n";
                }
            }
            $startdatemonth = "<option value=\"1\" {$startmonthsel[1]}>{$this->lang->january}</option>\n";
            $enddatemonth = "<option value=\"1\" {$endmonthsel[1]}>{$this->lang->january}</option>\n";
            $startdatemonth .= "<option value=\"2\" {$startmonthsel[2]}>{$this->lang->february}</option>\n";
            $enddatemonth .= "<option value=\"2\" {$endmonthsel[2]}>{$this->lang->february}</option>\n";
            $startdatemonth .= "<option value=\"3\" {$startmonthsel[3]}>{$this->lang->march}</option>\n";
            $enddatemonth .= "<option value=\"3\" {$endmonthsel[3]}>{$this->lang->march}</option>\n";
            $startdatemonth .= "<option value=\"4\" {$startmonthsel[4]}>{$this->lang->april}</option>\n";
            $enddatemonth .= "<option value=\"4\" {$endmonthsel[4]}>{$this->lang->april}</option>\n";
            $startdatemonth .= "<option value=\"5\" {$startmonthsel[5]}>{$this->lang->may}</option>\n";
            $enddatemonth .= "<option value=\"5\" {$endmonthsel[5]}>{$this->lang->may}</option>\n";
            $startdatemonth .= "<option value=\"6\" {$startmonthsel[6]}>{$this->lang->june}</option>\n";
            $enddatemonth .= "<option value=\"6\" {$endmonthsel[6]}>{$this->lang->june}</option>\n";
            $startdatemonth .= "<option value=\"7\" {$startmonthsel[7]}>{$this->lang->july}</option>\n";
            $enddatemonth .= "<option value=\"7\" {$endmonthsel[7]}>{$this->lang->july}</option>\n";
            $startdatemonth .= "<option value=\"8\" {$startmonthsel[8]}>{$this->lang->august}</option>\n";
            $enddatemonth .= "<option value=\"8\" {$endmonthsel[8]}>{$this->lang->august}</option>\n";
            $startdatemonth .= "<option value=\"9\" {$startmonthsel[9]}>{$this->lang->september}</option>\n";
            $enddatemonth .= "<option value=\"9\" {$endmonthsel[9]}>{$this->lang->september}</option>\n";
            $startdatemonth .= "<option value=\"10\" {$startmonthsel[10]}>{$this->lang->october}</option>\n";
            $enddatemonth .= "<option value=\"10\" {$endmonthsel[10]}>{$this->lang->october}</option>\n";
            $startdatemonth .= "<option value=\"11\" {$startmonthsel[11]}>{$this->lang->november}</option>\n";
            $enddatemonth .= "<option value=\"11\" {$endmonthsel[11]}>{$this->lang->november}</option>\n";
            $startdatemonth .= "<option value=\"12\" {$startmonthsel[12]}>{$this->lang->december}</option>\n";
            $enddatemonth .= "<option value=\"12\" {$endmonthsel[12]}>{$this->lang->december}</option>\n";

            if (isset($preview)) {
                $form_container = new FormContainer($this, $this->lang->announcement_preview);
                $form_container->output_row($preview['subject'], '', $preview['message'], 'preview');
                $html .= $form_container->end();
            }

            $form_container = new FormContainer($this, $this->lang->add_an_announcement);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->start_date . ' <em>*</em>', $this->lang->start_date_desc, "<select name=\"starttime_day\">\n{$startdateday}</select>\n &nbsp; \n<select name=\"starttime_month\">\n{$startdatemonth}</select>\n &nbsp; \n<input type=\"text\" name=\"starttime_year\" value=\"{$startdateyear}\" size=\"4\" maxlength=\"4\" class=\"text_input\" />\n - {$this->lang->time} " . $form->generate_text_box('starttime_time', $this->bb->input['starttime_time'], ['id' => 'starttime_time', 'style' => 'width: 50px;']));

            $actions = "<script type=\"text/javascript\">
	function checkAction(id)
	{
		var checked = '';

		$('.'+id+'s_check').each(function(e, val)
		{
			if($(this).prop('checked') == true)
			{
				checked = $(this).val();
			}
		});
		$('.'+id+'s').each(function(e)
		{
			$(this).hide();
		});
		if($('#'+id+'_'+checked))
		{
			$('#'+id+'_'+checked).show();
		}
	}
</script>
	<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"endtime_type\" value=\"1\" {$endtime_checked[1]} class=\"endtimes_check\" onclick=\"checkAction('endtime');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->set_time}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"endtime_1\" class=\"endtimes\">
			<table cellpadding=\"4\">
				<tr>
					<td><select name=\"endtime_day\">\n{$enddateday}</select>\n &nbsp; \n<select name=\"endtime_month\">\n{$enddatemonth}</select>\n &nbsp; \n<input type=\"text\" name=\"endtime_year\" value=\"{$enddateyear}\" size=\"4\" maxlength=\"4\" />\n - {$this->lang->time} " . $form->generate_text_box('endtime_time', $this->bb->input['endtime_time'], ['id' => 'endtime_time', 'style' => 'width: 50px;']) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"endtime_type\" value=\"2\" {$endtime_checked[2]} class=\"endtimes_check\" onclick=\"checkAction('endtime');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->never}</strong></label></dt>
	</dl>
	<script type=\"text/javascript\">
	checkAction('endtime');
	</script>";
            $form_container->output_row($this->lang->end_date . ' <em>*</em>', $this->lang->end_date_desc, $actions);

            $form_container->output_row($this->lang->message . ' <em>*</em>', '', $form->generate_text_area('message', $this->bb->input['message'], ['id' => 'message']), 'message');

            $form_container->output_row($this->lang->forums_to_appear_in . ' <em>*</em>', $this->lang->forums_to_appear_in_desc, $form->generate_forum_select('fid', $this->bb->input['fid'], ['size' => 5, 'main_option' => $this->lang->all_forums]));

            $form_container->output_row($this->lang->allow_html . ' <em>*</em>', '', $form->generate_yes_no_radio('allowhtml', $this->bb->input['allowhtml'], ['style' => 'width: 2em;']));

            $form_container->output_row($this->lang->allow_mycode . ' <em>*</em>', '', $form->generate_yes_no_radio('allowmycode', $this->bb->input['allowmycode'], ['style' => 'width: 2em;']));

            $form_container->output_row($this->lang->allow_smilies . ' <em>*</em>', '', $form->generate_yes_no_radio('allowsmilies', $this->bb->input['allowsmilies'], ['style' => 'width: 2em;']));

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_announcement);
            $buttons[] = $form->generate_submit_button($this->lang->preview_announcement, ['name' => 'preview']);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/AnnouncementsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('announcements', '*', "aid='{$this->bb->input['aid']}'");
            $announcement = $this->db->fetch_array($query);

            // Does the announcement not exist?
            if (!$announcement['aid']) {
                $this->session->flash_message($this->lang->error_invalid_announcement, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
            }

            // User clicked no
            if ($this->bb->getInput('no', '')) {
                return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
            }

            $this->plugins->runHooks('admin_forum_announcements_delete');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('announcements', "aid='{$announcement['aid']}'");

                $this->plugins->runHooks('admin_forum_announcements_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($announcement['aid'], $announcement['subject']);
                $this->cache->update_forumsdisplay();

                $this->session->flash_message($this->lang->success_announcement_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/forum/announcements');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . "/forum/announcements?action=delete&amp;aid={$announcement['aid']}", $this->lang->confirm_announcement_deletion);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_forum_announcements_start');

            $html = $this->page->output_header($this->lang->forum_announcements);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'forum_announcements');

            // Fetch announcements into their proper arrays
            $global_announcements = $this->announcements = [];
            $query = $this->db->simple_select('announcements', 'aid, fid, subject, enddate');
            while ($announcement = $this->db->fetch_array($query)) {
                if ($announcement['fid'] == -1) {
                    $global_announcements[$announcement['aid']] = $announcement;
                    continue;
                }
                $this->announcements[$announcement['fid']][$announcement['aid']] = $announcement;
            }
            if (!empty($global_announcements)) {
                $table = new Table;
                $table->construct_header($this->lang->announcement);
                $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 150]);

                // Get the global announcements
                foreach ($global_announcements as $aid => $announcement) {
                    if ($announcement['enddate'] < TIME_NOW && $announcement['enddate'] != 0) {
                        $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/bullet_off.png\" alt=\"(Expired)\" title=\"Expired Announcement\"  style=\"vertical-align: middle;\" /> ";
                    } else {
                        $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/bullet_on.png\" alt=\"(Active)\" title=\"Active Announcement\"  style=\"vertical-align: middle;\" /> ";
                    }

                    $table->construct_cell($icon . "<a href=\"".$this->bb->admin_url."/forum/announcements?action=edit&amp;aid={$aid}\">" . htmlspecialchars_uni($announcement['subject']) . "</a>");
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/forum/announcements?action=edit&amp;aid={$aid}\">{$this->lang->edit}</a>", ["class" => "align_center", "width" => 75]);
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/forum/announcements?action=delete&amp;aid={$aid}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_announcement_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center", "width" => 75]);
                    $table->construct_row();
                }
                $html .= $table->output($this->lang->global_announcements);
            }

            $table = new Table;
            $table->construct_header($this->lang->announcement);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 200]);

            $this->fetch_forum_announcements($table);

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_forums, ['colspan' => '3']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->forum_announcements);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/Announcements.html.twig');
        }
    }

    /**
     * @param DefaultTable $table
     * @param int $pid
     * @param int $depth
     */
    private function fetch_forum_announcements(&$table, $pid = 0, $depth = 1)
    {
        static $forums_by_parent;

        if (!is_array($forums_by_parent)) {
            $this->forum->cache_forums();
            foreach ($this->bb->forum_cache as $forum) {
                $forums_by_parent[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
            }
        }

        if (!is_array($forums_by_parent[$pid])) {
            return;
        }

        foreach ($forums_by_parent[$pid] as $children) {
            foreach ($children as $forum) {
                $forum['name'] = htmlspecialchars_uni($forum['name']);
                if ($forum['active'] == 0) {
                    $forum['name'] = '<em>' . $forum['name'] . '</em>';
                }

                if ($forum['type'] == 'c') {
                    $forum['name'] = '<strong>' . $forum['name'] . '</strong>';
                }

                $table->construct_cell("<div style=\"padding-left: " . (40 * ($depth - 1)) . "px;\">{$forum['name']}</div>");
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/forum/announcements?action=add&amp;fid={$forum['fid']}\">{$this->lang->add_announcement}</a>", ["class" => "align_center", "colspan" => 2]);
                $table->construct_row();

                if (isset($this->announcements[$forum['fid']])) {
                    foreach ($this->announcements[$forum['fid']] as $aid => $announcement) {
                        if ($announcement['enddate'] < TIME_NOW && $announcement['enddate'] != 0) {
                            $icon = "<img src=\"".$this->bb->admin_url."/styles/{$this->page->style}/images/icons/bullet_off.png\" alt=\"(Expired)\" title=\"Expired Announcement\"  style=\"vertical-align: middle;\" /> ";
                        } else {
                            $icon = "<img src=\"".$this->bb->admin_url."/styles/{$this->page->style}/images/icons/bullet_on.png\" alt=\"(Active)\" title=\"Active Announcement\"  style=\"vertical-align: middle;\" /> ";
                        }

                        $table->construct_cell("<div style=\"padding-left: " . (40 * $depth) . "px;\">{$icon}<a href=\"".$this->bb->admin_url."/forum/announcements?action=edit&amp;aid={$aid}\">" . htmlspecialchars_uni($announcement['subject']) . "</a></div>");
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/forum/announcements?action=edit&amp;aid={$aid}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                        $table->construct_cell("<a href=\"".$this->bb->admin_url."/forum/announcements?action=delete&amp;aid={$aid}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_announcement_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                        $table->construct_row();
                    }
                }

                // Build the list for any sub forums of this forum
                if (isset($forums_by_parent[$forum['fid']])) {
                    $this->fetch_forum_announcements($table, $forum['fid'], $depth + 1);
                }
            }
        }
    }
}
