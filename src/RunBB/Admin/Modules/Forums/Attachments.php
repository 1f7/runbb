<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Forums;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Attachments extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('forums');// set active module
        $this->lang->load('forums_attachments', false, true);

        $this->page->add_breadcrumb_item($this->lang->attachments, $this->bb->admin_url . '/forum/attachments');

        if ($this->bb->input['action'] == 'stats' || $this->bb->input['action'] == 'orphans' || !$this->bb->input['action']) {
            $sub_tabs['find_attachments'] = [
                'title' => $this->lang->find_attachments,
                'link' => $this->bb->admin_url . '/forum/attachments',
                'description' => $this->lang->find_attachments_desc
            ];

            $sub_tabs['find_orphans'] = [
                'title' => $this->lang->find_orphans,
                'link' => $this->bb->admin_url . '/forum/attachments?action=orphans',
                'description' => $this->lang->find_orphans_desc
            ];

            $sub_tabs['stats'] = [
                'title' => $this->lang->attachment_stats,
                'link' => $this->bb->admin_url . '/forum/attachments?action=stats',
                'description' => $this->lang->attachment_stats_desc
            ];
        }

        $this->plugins->runHooks('admin_forum_attachments_begin');

        if ($this->bb->input['action'] == 'delete') {
            $this->plugins->runHooks('admin_forum_attachments_delete');

            if (!is_array($this->bb->input['aids'])) {
                $this->bb->input['aids'] = [$this->bb->getInput('aid', 0)];
            } else {
                $this->bb->input['aids'] = array_map('intval', $this->bb->input['aids']);
            }

            if (count($this->bb->input['aids']) < 1) {
                $this->session->flash_message($this->lang->error_nothing_selected, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum/attachments');
            }

            if ($this->bb->request_method == 'post') {
                $query = $this->db->simple_select('attachments', 'aid,pid,posthash, filename', 'aid IN (' . implode(',', $this->bb->input['aids']) . ')');
                while ($attachment = $this->db->fetch_array($query)) {
                    if (!$attachment['pid']) {
                        $this->upload->remove_attachment(null, $attachment['posthash'], $attachment['aid']);
                        // Log admin action
                        $this->bblogger->log_admin_action($attachment['aid'], $attachment['filename']);
                    } else {
                        $this->upload->remove_attachment($attachment['pid'], null, $attachment['aid']);
                        // Log admin action
                        $this->bblogger->log_admin_action($attachment['aid'], $attachment['filename'], $attachment['pid']);
                    }
                }

                $this->plugins->runHooks('admin_forum_attachments_delete_commit');

                $this->session->flash_message($this->lang->success_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/forum/attachments');
            } else {
                $aids = [];
                foreach ($this->bb->input['aids'] as $aid) {
                    $aids .= "&amp;aids[]=$aid";
                }
                $this->page->output_confirm_action($this->bb->admin_url . "/forum/attachments?action=delete&amp;aids={$aids}", $this->lang->confirm_delete);
            }
        }

        if ($this->bb->input['action'] == 'stats') {
            $this->plugins->runHooks('admin_forum_attachments_stats');

            $query = $this->db->simple_select('attachments', 'COUNT(*) AS total_attachments, SUM(filesize) as disk_usage, SUM(downloads*filesize) as bandwidthused', "visible='1'");
            $attachment_stats = $this->db->fetch_array($query);

            $this->page->add_breadcrumb_item($this->lang->stats);
            $html = $this->page->output_header($this->lang->stats_attachment_stats);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'stats');

            if ($attachment_stats['total_attachments'] == 0) {
                $html .= $this->page->output_inline_error([$this->lang->error_no_attachments]);
                $this->page->output_footer();
//                exit;
//FIXME move to flash message ???
                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/AttachmentsStats.html.twig');
            }

            $table = new Table;

            $table->construct_cell($this->lang->num_uploaded, ['width' => '25%']);
            $table->construct_cell($this->parser->formatNumber($attachment_stats['total_attachments']), ['width' => '25%']);
            $table->construct_cell($this->lang->space_used, ['width' => '200']);
            $table->construct_cell($this->parser->friendlySize($attachment_stats['disk_usage']), ['width' => '200']);
            $table->construct_row();

            $table->construct_cell($this->lang->bandwidth_used, ['width' => '25%']);
            $table->construct_cell($this->parser->friendlySize(round($attachment_stats['bandwidthused'])), ['width' => '25%']);
            $table->construct_cell($this->lang->average_size, ['width' => '25%']);
            $table->construct_cell($this->parser->friendlySize(round($attachment_stats['disk_usage'] / $attachment_stats['total_attachments'])), ['width' => '25%']);
            $table->construct_row();

            $html .= $table->output($this->lang->general_stats);

            // Fetch the most popular attachments
            $table = new Table;
            $table->construct_header($this->lang->attachments, ['colspan' => 2]);
            $table->construct_header($this->lang->size, ['width' => '10%', 'class' => 'align_center']);
            $table->construct_header($this->lang->posted_by, ['width' => '20%', 'class' => 'align_center']);
            $table->construct_header($this->lang->thread, ['width' => '25%', 'class' => 'align_center']);
            $table->construct_header($this->lang->downloads, ['width' => '10%', 'class' => 'align_center']);
            $table->construct_header($this->lang->date_uploaded, ['class' => 'align_center']);

            $query = $this->db->query('
		SELECT a.*, p.tid, p.fid, t.subject, p.uid, p.username, u.username AS user_username
		FROM ' . TABLE_PREFIX . 'attachments a
		LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=a.uid)
		ORDER BY a.downloads DESC
		LIMIT 5
	');
            while ($attachment = $this->db->fetch_array($query)) {
                $this->build_attachment_row($attachment, $table);
            }
            $html .= $table->output($this->lang->popular_attachments);

            // Fetch the largest attachments
            $table = new Table;
            $table->construct_header($this->lang->attachments, ['colspan' => 2]);
            $table->construct_header($this->lang->size, ['width' => '10%', 'class' => 'align_center']);
            $table->construct_header($this->lang->posted_by, ['width' => '20%', 'class' => 'align_center']);
            $table->construct_header($this->lang->thread, ['width' => '25%', 'class' => 'align_center']);
            $table->construct_header($this->lang->downloads, ['width' => '10%', 'class' => 'align_center']);
            $table->construct_header($this->lang->date_uploaded, ['class' => 'align_center']);

            $query = $this->db->query('
		SELECT a.*, p.tid, p.fid, t.subject, p.uid, p.username, u.username AS user_username
		FROM ' . TABLE_PREFIX . 'attachments a
		LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=a.uid)
		ORDER BY a.filesize DESC
		LIMIT 5
	');
            while ($attachment = $this->db->fetch_array($query)) {
                $this->build_attachment_row($attachment, $table);
            }
            $html .= $table->output($this->lang->largest_attachments);

            // Fetch users who've uploaded the most attachments
            $table = new Table;
            $table->construct_header($this->lang->username);
            $table->construct_header($this->lang->total_size, ['width' => '20%', 'class' => 'align_center']);

            switch ($this->db->type) {
                case 'pgsql':
                    $query = $this->db->query('
				SELECT a.*, u.uid AS useruid, u.username, SUM(a.filesize) as totalsize
				FROM ' . TABLE_PREFIX . 'attachments a
				LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=a.uid)
				GROUP BY " . $this->db->build_fields_string('attachments', 'a.') . ",u.uid,u.username
				ORDER BY totalsize DESC
				LIMIT 5
			");
                    break;
                default:
                    $query = $this->db->query('
				SELECT a.*, u.uid AS useruid, u.username, SUM(a.filesize) as totalsize
				FROM ' . TABLE_PREFIX . 'attachments a
				LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=a.uid)
				GROUP BY a.uid
				ORDER BY totalsize DESC
				LIMIT 5
			');
            }
            while ($user = $this->db->fetch_array($query)) {
                if (!$user['useruid']) {
                    $user['username'] = $this->lang->na;
                }
                $table->construct_cell($this->user->build_profile_link($user['username'], $user['useruid'], '_blank'));
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/forum/attachments?results=1&amp;username=" . urlencode($user['username']) . "\" target=\"_blank\">" . $this->parser->friendlySize($user['totalsize']) . "</a>", ['class' => 'align_center']);
                $table->construct_row();
            }
            $html .= $table->output($this->lang->users_diskspace);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/AttachmentsStats.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_orphans' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_forum_attachments_delete_orphans');
            $error = false;
            // Deleting specific attachments from uploads directory
            if (isset($this->bb->input['orphaned_files'])) {
                /**
                 * @param string $string
                 *
                 * @return string
                 */
                function clean_filename($string)
                {
                    return str_replace(['..'], '', $string);
                }

                $this->bb->input['orphaned_files'] = array_map('clean_filename', $this->bb->input['orphaned_files']);
                foreach ($this->bb->input['orphaned_files'] as $file) {
                    if (!@unlink(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $file)) {
                        $error = true;
                    }
                }
            }

            // Deleting physical attachments which exist in database
            if (is_array($this->bb->input['orphaned_attachments'])) {
                $this->bb->input['orphaned_attachments'] = array_map('intval', $this->bb->input['orphaned_attachments']);
                $query = $this->db->simple_select('attachments', 'aid,pid,posthash', 'aid IN (' . implode(',', $this->bb->input['orphaned_attachments']) . ')');
                while ($attachment = $this->db->fetch_array($query)) {
                    if (!$attachment['pid']) {
                        $this->upload->remove_attachment(null, $attachment['posthash'], $attachment['aid']);
                    } else {
                        $this->upload->remove_attachment($attachment['pid'], null, $attachment['aid']);
                    }
                }
            }

            $this->plugins->runHooks('admin_forum_attachments_delete_orphans_commit');

            // Log admin action
            $this->bblogger->log_admin_action();

            if ($error == true) {
                $this->session->flash_message($this->lang->error_not_all_removed, 'error');
            } else {
                $this->session->flash_message($this->lang->success_orphan_deleted, 'success');
            }
            return $response->withRedirect($this->bb->admin_url . '/forum/attachments');
        }

        if ($this->bb->input['action'] == 'orphans') {
            $this->plugins->runHooks('admin_forum_attachments_orphans');

            // Oprhans are defined as:
            // - Uploaded files in the uploads directory that don't exist in the database
            // - Attachments for which the uploaded file is missing
            // - Attachments for which the thread or post has been deleted
            // - Files uploaded > 24h ago not attached to a real post

            // This process is quite intensive so we split it up in to 2 steps, one which scans the file system and the other which scans the database.

            // Finished second step, show results
            if ($this->bb->input['step'] == 3) {
                $this->plugins->runHooks('admin_forum_attachments_step3');

                $results = 0;
                $bad_attachments = [];
                // Incoming attachments which exist as files but not in database
                if (isset($this->bb->input['bad_attachments'])) {
                    $bad_attachments = my_unserialize($this->bb->input['bad_attachments']);
                    $results = count($bad_attachments);
                }

                $aids = [];
                if (isset($this->bb->input['missing_attachment_files'])) {
                    $missing_attachment_files = my_unserialize($this->bb->input['missing_attachment_files']);
                    $aids = array_merge($aids, $missing_attachment_files);
                }

                if (isset($this->bb->input['missing_threads'])) {
                    $missing_threads = my_unserialize($this->bb->input['missing_threads']);
                    $aids = array_merge($aids, $missing_threads);
                }

                if (isset($this->bb->input['incomplete_attachments'])) {
                    $incomplete_attachments = my_unserialize($this->bb->input['incomplete_attachments']);
                    $aids = array_merge($aids, $incomplete_attachments);
                }

                foreach ($aids as $key => $aid) {
                    $aids[$key] = (int)$aid;
                }

                $results += count($aids);

                if ($results == 0) {
                    $this->session->flash_message($this->lang->success_no_orphans, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/forum/attachments');
                }

                $html = $this->page->output_header($this->lang->orphan_results);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'find_orphans');

                $form = new Form($this->bb, $this->bb->admin_url . '/forum/attachments?action=delete_orphans', 'post');
                $html .= $form->getForm();

                $table = new Table;
                $table->construct_header($form->generate_check_box('checkall', '1', '', ['class' => 'checkall']), ['width' => 1]);
                $table->construct_header($this->lang->size_attachments, ['colspan' => 2]);
                $table->construct_header($this->lang->reason_orphaned, ['width' => '20%', 'class' => 'align_center']);
                $table->construct_header($this->lang->date_uploaded, ['class' => 'align_center']);

                if (is_array($bad_attachments)) {
                    foreach ($bad_attachments as $file) {
                        $file_path = DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $file;
                        $filesize = $this->parser->friendlySize(filesize($file_path));
                        $table->construct_cell($form->generate_check_box('orphaned_files[]', $file, '', ['checked' => true]));
                        $table->construct_cell($this->upload->get_attachment_icon(get_extension($attachment['filename'])), ['width' => 1]);
                        $table->construct_cell("<span class=\"float_right\">{$filesize}</span>{$file}");
                        $table->construct_cell($this->lang->reason_not_in_table, ['class' => 'align_center']);
                        $table->construct_cell($this->time->formatDate('relative', filemtime($file_path)), ['class' => 'align_center']);
                        $table->construct_row();
                    }
                }

                if (count($aids) > 0) {
                    $query = $this->db->simple_select('attachments', '*', 'aid IN (' . implode(',', $aids) . ')');
                    while ($attachment = $this->db->fetch_array($query)) {
                        $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);

                        if ($missing_attachment_files[$attachment['aid']]) {
                            $reason = $this->lang->reason_file_missing;
                        } elseif ($missing_threads[$attachment['aid']]) {
                            $reason = $this->lang->reason_thread_deleted;
                        } elseif ($incomplete_attachments[$attachment['aid']]) {
                            $reason = $this->lang->reason_post_never_made;
                        }
                        $table->construct_cell($form->generate_check_box('orphaned_attachments[]', $attachment['aid'], '', ['checked' => true]));
                        $table->construct_cell($this->upload->get_attachment_icon(get_extension($attachment['filename'])), ['width' => 1]);
                        $table->construct_cell("<span class=\"float_right\">" . $this->parser->friendlySize($attachment['filesize']) . "</span>{$attachment['filename']}");//, array('class' => $this->cell_class));
                        $table->construct_cell($reason, ['class' => 'align_center']);
                        if ($attachment['dateuploaded']) {
                            $table->construct_cell($this->time->formatDate('relative', $attachment['dateuploaded']), ['class' => 'align_center']);
                        } else {
                            $table->construct_cell($this->lang->unknown, ['class' => 'align_center']);
                        }
                        $table->construct_row();
                    }
                }

                $html .= $table->output("{$this->lang->orphan_attachments_search} - {$results} {$this->lang->results}");

                $buttons[] = $form->generate_submit_button($this->lang->button_delete_orphans);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/AttachmentsOrphans.html.twig');
            } // Running second step - scan the database
            elseif ($this->bb->input['step'] == 2) {
                $this->plugins->runHooks('admin_forum_attachments_orphans_step2');

                $html = $this->page->output_header("{$this->lang->orphan_attachments_search} - {$this->lang->step2}");

                $html .= $this->page->output_nav_tabs($sub_tabs, 'find_orphans');
                $html .= "<h3>{$this->lang->step2of2}</h3>";
                $html .= "<p class=\"align_center\">{$this->lang->step2of2_line1}</p>";
                $html .= "<p class=\"align_center\">{$this->lang->step_line2}</p>";
                $html .= "<p class=\"align_center\"><img src=\"styles/{$this->page->style}/images/spinner_big.gif\" alt=\"{$this->lang->scanning}\" id=\"spinner\" /></p>";

                $this->page->output_footer(false);
                flush();

                $missing_attachment_files = [];
                $missing_threads = [];
                $incomplete_attachments = [];

                $query = $this->db->query('
			SELECT a.*, a.pid AS attachment_pid, p.pid
			FROM ' . TABLE_PREFIX . 'attachments a
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
			ORDER BY a.aid');
                while ($attachment = $this->db->fetch_array($query)) {
                    // Check if the attachment exists in the file system
                    if (!file_exists(DIR . 'web' . $this->bb->settings['uploadspath'] . "/{$attachment['attachname']}")) {
                        $missing_attachment_files[$attachment['aid']] = $attachment['aid'];
                    } // Check if the thread/post for this attachment is missing
                    elseif (!$attachment['pid'] && $attachment['attachment_pid']) {
                        $missing_threads[$attachment['aid']] = $attachment['aid'];
                    } // Check if the attachment was uploaded > 24 hours ago but not assigned to a thread
                    elseif (!$attachment['attachment_pid'] && $attachment['dateuploaded'] < TIME_NOW - 60 * 60 * 24 && $attachment['dateuploaded'] != 0) {
                        $incomplete_attachments[$attachment['aid']] = $attachment['aid'];
                    }
                }

                // Now send the user to the final page
                $form = new Form($this->bb, $this->bb->admin_url . '/forum/attachments?action=orphans&step=3', 'post', 'redirect_form', 0, '');
                $html .= $form->getForm();
                // Scan complete
                if ($this->bb->input['bad_attachments']) {
                    $html .= $form->generate_hidden_field('bad_attachments', $this->bb->input['bad_attachments']);
                }
                if (is_array($missing_attachment_files) && count($missing_attachment_files) > 0) {
                    $missing_attachment_files = my_serialize($missing_attachment_files);
                    $html .= $form->generate_hidden_field('missing_attachment_files', $missing_attachment_files);
                }
                if (is_array($missing_threads) && count($missing_threads) > 0) {
                    $missing_threads = my_serialize($missing_threads);
                    $html .= $form->generate_hidden_field('missing_threads', $missing_threads);
                }
                if (is_array($incomplete_attachments) && count($incomplete_attachments) > 0) {
                    $incomplete_attachments = my_serialize($incomplete_attachments);
                    $html .= $form->generate_hidden_field('incomplete_attachments', $incomplete_attachments);
                }
                $html .= $form->end();
                $html .= "<script type=\"text/javascript\">$(function() {
				window.setTimeout(
					function() {
						$(\"#redirect_form\").submit();
					}, 100
				);
			});</script>";
//                exit;

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/AttachmentsOrphans.html.twig');
            } // Running first step, scan the file system
            else {
                $this->plugins->runHooks('admin_forum_attachments_orphans_step1');

                $html = $this->page->output_header("{$this->lang->orphan_attachments_search} - {$this->lang->step1}");

                $html .= $this->page->output_nav_tabs($sub_tabs, 'find_orphans');
                $html .= "<h3>{$this->lang->step1of2}</h3>";
                $html .= "<p class=\"align_center\">{$this->lang->step1of2_line1}</p>";
                $html .= "<p class=\"align_center\">{$this->lang->step_line2}</p>";
                $html .= "<p class=\"align_center\"><img src=\"styles/{$this->page->style}/images/spinner_big.gif\" alt=\"{$this->lang->scanning}\" id=\"spinner\" /></p>";

                $this->page->output_footer(false);

                flush();

                $this->scan_attachments_directory();

                $form = new Form($this->bb, $this->bb->admin_url . '/forum/attachments?action=orphans&step=2', 'post', 'redirect_form', 0, '');
                $html .= $form->getForm();
                // Scan complete
                if (is_array($bad_attachments) && count($bad_attachments) > 0) {
                    $bad_attachments = my_serialize($bad_attachments);
                    $html .= $form->generate_hidden_field('bad_attachments', $bad_attachments);
                }
                $html .= $form->end();
                $html .= "<script type=\"text/javascript\">$(function() {
				window.setTimeout(
					function() {
						$(\"#redirect_form\").submit();
					}, 100
				);
			});</script>";
//                exit;
                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/AttachmentsOrphans.html.twig');
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_forum_attachments_start');
            $errors = [];
            if ($this->bb->request_method == 'post' || (isset($this->bb->input['results']) && $this->bb->input['results'] == 1)) {
                $search_sql = '1=1';

                // Build the search SQL for users

                // List of valid LIKE search fields
                $user_like_fields = ['filename', 'filetype'];
                foreach ($user_like_fields as $search_field) {
                    if (isset($this->bb->input[$search_field])) {
                        $search_sql .= " AND a.{$search_field} LIKE '%" . $this->db->escape_string_like($this->bb->input[$search_field]) . "%'";
                    }
                }

                // Username matching
                if ($this->bb->input['username']) {
                    $user = $this->user->get_user_by_username($this->bb->input['username']);

                    if (!$user['uid']) {
                        $errors[] = $this->lang->error_invalid_username;
                    } else {
                        $search_sql .= " AND a.uid='{$user['uid']}'";
                    }
                }

                $this->forum->cache_forums();

                // Searching for attachments in a specific forum, we need to fetch all child forums too
                if (isset($this->bb->input['forum'])) {
                    if (!is_array($this->bb->input['forum'])) {
                        $this->bb->input['forum'] = [$this->bb->input['forum']];
                    }

                    $fid_in = [];
                    foreach ($this->bb->input['forum'] as $fid) {
                        if (!$this->bb->forum_cache[$fid]) {
                            $errors[] = $this->lang->error_invalid_forums;
                            break;
                        }
                        $child_forums = $this->forum->get_child_list($fid);
                        $child_forums[] = $fid;
                        $fid_in = array_merge($fid_in, $child_forums);
                    }

                    if (count($fid_in) > 0) {
                        $search_sql .= ' AND p.fid IN (' . implode(',', $fid_in) . ')';
                    }
                }

                // LESS THAN or GREATER THAN
                $direction_fields = [
                    'dateuploaded' => $this->bb->getInput('dateuploaded', 0),
                    'filesize' => $this->bb->getInput('filesize', 0),
                    'downloads' => $this->bb->getInput('downloads', 0)
                ];

                if ($this->bb->input['dateuploaded'] && $this->bb->request_method == 'post') {
                    $direction_fields['dateuploaded'] = TIME_NOW - $direction_fields['dateuploaded'] * 60 * 60 * 24;
                }
                if ($this->bb->input['filesize'] && $this->bb->request_method == 'post') {
                    $direction_fields['filesize'] *= 1024;
                }

                foreach ($direction_fields as $field_name => $field_content) {
                    $direction_field = $field_name . '_dir';
                    if ($this->bb->input[$field_name] && $this->bb->input[$direction_field]) {
                        switch ($this->bb->input[$direction_field]) {
                            case 'greater_than':
                                $direction = '>';
                                break;
                            case 'less_than':
                                $direction = '<';
                                break;
                            default:
                                $direction = '=';
                        }
                        $search_sql .= " AND a.{$field_name}{$direction}'" . $field_content . "'";
                    }
                }
                if (!$errors) {
                    // Lets fetch out how many results we have
                    $query = $this->db->query('
				SELECT COUNT(a.aid) AS num_results
				FROM ' . TABLE_PREFIX . 'attachments a
				LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=a.pid)
				WHERE {$search_sql}
			");
                    $num_results = $this->db->fetch_field($query, 'num_results');

                    // No matching results then show an error
                    if (!$num_results) {
                        $errors[] = $this->lang->error_no_results;
                    }
                }

                // Now we fetch the results if there were 100% no errors
                if (empty($errors)) {
                    $this->bb->input['perpage'] = $this->bb->getInput('perpage', 0);
                    if (!$this->bb->input['perpage']) {
                        $this->bb->input['perpage'] = 20;
                    }

                    $this->bb->input['page'] = $this->bb->getInput('page', 0);
                    if ($this->bb->input['page']) {
                        $start = ($this->bb->input['page'] - 1) * $this->bb->input['perpage'];
                    } else {
                        $start = 0;
                        $this->bb->input['page'] = 1;
                    }

                    switch ($this->bb->input['sortby']) {
                        case 'filesize':
                            $sort_field = 'a.filesize';
                            break;
                        case 'downloads':
                            $sort_field = 'a.downloads';
                            break;
                        case 'dateuploaded':
                            $sort_field = 'a.dateuploaded';
                            break;
                        case 'username':
                            $sort_field = 'u.username';
                            break;
                        default:
                            $sort_field = 'a.filename';
                            $this->bb->input['sortby'] = 'filename';
                    }

                    if ($this->bb->input['order'] != 'desc') {
                        $this->bb->input['order'] = 'asc';
                    }

                    $this->page->add_breadcrumb_item($this->lang->results);
                    $html = $this->page->output_header($this->lang->index_find_attachments);

                    $html .= $this->page->output_nav_tabs($sub_tabs, 'find_attachments');

                    $form = new Form($this->bb, $this->bb->admin_url . '/forum/attachments?action=delete', 'post');
                    $html .= $form->getForm();

                    $table = new Table;
                    $table->construct_header($form->generate_check_box('checkall', '1', '', ['class' => 'checkall']), ['width' => 1]);
                    $table->construct_header($this->lang->attachments, ['colspan' => 2]);
                    $table->construct_header($this->lang->size, ['width' => '10%', 'class' => 'align_center']);
                    $table->construct_header($this->lang->posted_by, ['width' => '20%', 'class' => 'align_center']);
                    $table->construct_header($this->lang->thread, ['width' => '25%', 'class' => 'align_center']);
                    $table->construct_header($this->lang->downloads, ['width' => '10%', 'class' => 'align_center']);
                    $table->construct_header($this->lang->date_uploaded, ['class' => 'align_center']);

                    // Fetch matching attachments
                    $query = $this->db->query('
				SELECT a.*, p.tid, p.fid, t.subject, p.uid, p.username, u.username AS user_username
				FROM ' . TABLE_PREFIX . 'attachments a
				LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
				LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
				LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=a.uid)
				WHERE {$search_sql}
				ORDER BY {$sort_field} {$this->bb->input['order']}
				LIMIT {$start}, {$this->bb->input['perpage']}
			");
                    while ($attachment = $this->db->fetch_array($query)) {
                        $this->build_attachment_row($attachment, $table, $form);
                    }
                    $pagination = '';
                    // Need to draw pagination for this result set
                    if ($num_results > $this->bb->input['perpage']) {
                        $pagination_url = $this->bb->admin_url . '/forum/attachments?results=1';
                        $pagination_vars = ['perpage', 'sortby', 'order', 'filename', 'mimetype', 'username', 'fid', 'downloads', 'downloads_dir', 'dateuploaded', 'dateuploaded_dir', 'filesize', 'filesize_dir'];
                        foreach ($pagination_vars as $var) {
                            if ($this->bb->input[$var]) {
                                $pagination_url .= "&{$var}=" . urlencode($this->bb->input[$var]);
                            }
                        }
                        $pagination = $this->adm->draw_admin_pagination($this->bb->input['page'], $this->bb->input['perpage'], $num_results, $pagination_url);
                    }

                    $html .= $pagination;
                    $html .= $table->output($this->lang->results);
                    $html .= $pagination;

                    $buttons[] = $form->generate_submit_button($this->lang->button_delete_attachments);

                    $html .= $form->output_submit_wrapper($buttons);
                    $html .= $form->end();

                    $this->page->output_footer();

                    $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                    return $this->view->render($response, '@forum/Admin/Forums/Attachments.html.twig');
                }
            }

            $html = $this->page->output_header($this->lang->find_attachments);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'find_attachments');

            // If we have any error messages, show them
            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/forum/attachments', 'post');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->find_where);
            $form_container->output_row($this->lang->name_contains, $this->lang->name_contains_desc, $form->generate_text_box('filename', $this->bb->getInput('filename', ''), ['id' => 'filename']), 'filename');
            $form_container->output_row($this->lang->type_contains, '', $form->generate_text_box('mimetype', $this->bb->getInput('mimetype', ''), ['id' => 'mimetype']), 'mimetype');
            $form_container->output_row($this->lang->forum_is, '', $form->generate_forum_select('forum[]', $this->bb->getInput('forum', '', true), ['multiple' => true, 'size' => 5, 'id' => 'forum']), 'forum');
            $form_container->output_row($this->lang->username_is, '', $form->generate_text_box('username', $this->bb->getInput('username', '', true), ['id' => 'username']), 'username');

            $more_options = [
                'less_than' => $this->lang->more_than,
                'greater_than' => $this->lang->less_than
            ];

            $greater_options = [
                'greater_than' => $this->lang->greater_than,
                'is_exactly' => $this->lang->is_exactly,
                'less_than' => $this->lang->less_than
            ];

            $form_container->output_row($this->lang->date_posted_is, '', $form->generate_select_box('dateuploaded_dir', $more_options, $this->bb->getInput('dateuploaded_dir', ''), ['id' => 'dateuploaded_dir']) . ' ' . $form->generate_numeric_field('dateuploaded', $this->bb->getInput('dateuploaded', ''), ['id' => 'dateuploaded', 'min' => 0]) . " {$this->lang->days_ago}", 'dateuploaded');
            $form_container->output_row($this->lang->file_size_is, '', $form->generate_select_box('filesize_dir', $greater_options, $this->bb->getInput('filesize_dir', ''), ['id' => 'filesize_dir']) . ' ' . $form->generate_numeric_field('filesize', $this->bb->getInput('filesize', ''), ['id' => 'filesize', 'min' => 0]) . " {$this->lang->kb}", 'dateuploaded');
            $form_container->output_row($this->lang->download_count_is, '', $form->generate_select_box('downloads_dir', $greater_options, $this->bb->getInput('downloads_dir', ''), ['id' => 'downloads_dir']) . ' ' . $form->generate_numeric_field('downloads', $this->bb->getInput('downloads', ''), ['id' => 'downloads', 'min' => 0]), 'dateuploaded');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->display_options);
            $sort_options = [
                'filename' => $this->lang->filename,
                'filesize' => $this->lang->filesize,
                'downloads' => $this->lang->download_count,
                'dateuploaded' => $this->lang->date_uploaded,
                'username' => $this->lang->post_username
            ];
            $sort_directions = [
                'asc' => $this->lang->asc,
                'desc' => $this->lang->desc
            ];
            $form_container->output_row($this->lang->sort_results_by, '', $form->generate_select_box('sortby', $sort_options, $this->bb->getInput('sortby', ''), ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('order', $sort_directions, $this->bb->getInput('order', ''), ['id' => 'order']), 'sortby');
            $form_container->output_row($this->lang->results_per_page, '', $form->generate_numeric_field('perpage', $this->bb->getInput('perpage', ''), ['id' => 'perpage', 'min' => 1]), 'perpage');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->button_find_attachments);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/AttachmentsFind.html.twig');
        }
    }

    /**
     * @param string $dir
     */
    private function scan_attachments_directory($dir = '')
    {
        $real_dir = DIR . 'web' . $this->bb->settings['uploadspath'];
        $false_dir = '';
        if ($dir) {
            $real_dir .= '/' . $dir;
            $false_dir = $dir . '/';
        }
        $bad_attachments = [];
        if ($dh = opendir($real_dir)) {
            while (false !== ($file = readdir($dh))) {
                if ($file == '.' || $file == '..' || $file == '.svn') {
                    continue;
                }

                if (is_dir($real_dir . '/' . $file)) {
                    $this->scan_attachments_directory($false_dir . $file);
                } elseif (my_substr($file, -7, 7) == '.attach') {
                    $attachments_to_check["$false_dir$file"] = $false_dir . $file;
                    // In allotments of 20, query the database for these attachments
                    if (count($attachments_to_check) >= 20) {
                        $attachments_to_check = array_map([$this->db, 'escape_string'], $attachments_to_check);
                        $attachment_names = "'" . implode("','", $attachments_to_check) . "'";
                        $query = $this->db->simple_select('attachments', 'aid, attachname', "attachname IN ($attachment_names)");
                        while ($attachment = $this->db->fetch_array($query)) {
                            unset($attachments_to_check[$attachment['attachname']]);
                        }

                        // Now anything left is bad!
                        if (count($attachments_to_check) > 0) {
                            if ($bad_attachments) {
                                $bad_attachments = @array_merge($bad_attachments, $attachments_to_check);
                            } else {
                                $bad_attachments = $attachments_to_check;
                            }
                        }
                        $attachments_to_check = [];
                    }
                }
            }
            closedir($dh);
            // Any reamining to check?
            if (count($attachments_to_check) > 0) {
                $attachments_to_check = array_map([$this->db, 'escape_string'], $attachments_to_check);
                $attachment_names = "'" . implode("','", $attachments_to_check) . "'";
                $query = $this->db->simple_select('attachments', 'aid, attachname', "attachname IN ($attachment_names)");
                while ($attachment = $this->db->fetch_array($query)) {
                    unset($attachments_to_check[$attachment['attachname']]);
                }

                // Now anything left is bad!
                if (count($attachments_to_check) > 0) {
                    if ($bad_attachments) {
                        $bad_attachments = @array_merge($bad_attachments, $attachments_to_check);
                    } else {
                        $bad_attachments = $attachments_to_check;
                    }
                }
            }
        }
    }

    /**
     * @param array $attachment
     * @param DefaultTable $table
     * @param bool $use_form
     */
    private function build_attachment_row($attachment, &$table, &$form = '')
    {
        $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);

        // Here we do a bit of detection, we want to automatically check for removal any missing attachments and any not assigned to a post uploaded > 24hours ago
        // Check if the attachment exists in the file system
        $checked = false;
        $title = $this->cell_class = '';
        if (!file_exists(DIR . 'web' . $this->bb->settings['uploadspath'] . "/{$attachment['attachname']}")) {
            $this->cell_class = 'bad_attachment';
            $title = $this->lang->error_not_found;
            $checked = true;
        } elseif (!$attachment['pid'] && $attachment['dateuploaded'] < TIME_NOW - 60 * 60 * 24 && $attachment['dateuploaded'] != 0) {
            $this->cell_class = 'bad_attachment';
            $title = $this->lang->error_not_attached;
            $checked = true;
        } elseif (!$attachment['tid'] && $attachment['pid']) {
            $this->cell_class = 'bad_attachment';
            $title = $this->lang->error_does_not_exist;
            $checked = true;
        } elseif ($attachment['visible'] == 0) {
            $this->cell_class = 'invisible_attachment';
        }

        if ($this->cell_class) {
            $this->cell_class .= ' align_center';
        } else {
            $this->cell_class = 'align_center';
        }

        if (is_object($form)) {
            $table->construct_cell($form->generate_check_box('aids[]', $attachment['aid'], '', ['checked' => $checked]));
        }
        $table->construct_cell($this->upload->get_attachment_icon(get_extension($attachment['filename'])), ['width' => 1]);
        $table->construct_cell("<a href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" target=\"_blank\">{$attachment['filename']}</a>");
        $table->construct_cell($this->parser->friendlySize($attachment['filesize']), ['class' => $this->cell_class]);

        if ($attachment['user_username']) {
            $attachment['username'] = $attachment['username'];
        }
        $table->construct_cell($this->user->build_profile_link($attachment['username'], $attachment['uid'], "_blank"), ["class" => "align_center"]);
        $table->construct_cell("<a href=\"" . get_post_link($attachment['pid']) . "\" target=\"_blank\">" . htmlspecialchars_uni($attachment['subject']) . "</a>", ["class" => "align_center"]);
        $table->construct_cell($this->parser->formatNumber($attachment['downloads']), ["class" => "align_center"]);
        if ($attachment['dateuploaded'] > 0) {
            $date = $this->time->formatDate('relative', $attachment['dateuploaded']);
        } else {
            $date = $this->lang->unknown;
        }
        $table->construct_cell($date, ['class' => 'align_center']);
        $table->construct_row();
    }
}
