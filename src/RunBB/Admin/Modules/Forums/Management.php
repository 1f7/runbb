<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Forums;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Management extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('forums');// set active module
        $this->lang->load('forums_management', false, true);

        $this->page->add_breadcrumb_item($this->lang->forum_management, $this->bb->admin_url . '/forum');
        $errors = '';
        if ($this->bb->input['action'] == 'add' ||
            $this->bb->input['action'] == 'edit' ||
            $this->bb->input['action'] == 'copy' ||
            $this->bb->input['action'] == 'permissions' ||
            !$this->bb->input['action']
        ) {
            if (!empty($this->bb->input['fid']) &&
                ($this->bb->input['action'] == 'management' ||
                    $this->bb->input['action'] == 'edit' ||
                    $this->bb->input['action'] == 'copy' ||
                    !$this->bb->input['action'])
            ) {
                $sub_tabs['view_forum'] = [
                    'title' => $this->lang->view_forum,
                    'link' => $this->bb->admin_url . '/forum?fid=' . $this->bb->input['fid'],
                    'description' => $this->lang->view_forum_desc
                ];

                $sub_tabs['add_child_forum'] = [
                    'title' => $this->lang->add_child_forum,
                    'link' => $this->bb->admin_url . '/forum?action=add&pid=' . $this->bb->input['fid'],
                    'description' => $this->lang->view_forum_desc
                ];

                $sub_tabs['edit_forum_settings'] = [
                    'title' => $this->lang->edit_forum_settings,
                    'link' => $this->bb->admin_url . '/forum?action=edit&fid=' . $this->bb->input['fid'],
                    'description' => $this->lang->edit_forum_settings_desc
                ];

                $sub_tabs['copy_forum'] = [
                    'title' => $this->lang->copy_forum,
                    'link' => $this->bb->admin_url . '/forum?action=copy&amp;fid=' . $this->bb->input['fid'],
                    'description' => $this->lang->copy_forum_desc
                ];
            } else {
                $sub_tabs['forum_management'] = [
                    'title' => $this->lang->forum_management,
                    'link' => $this->bb->admin_url . '/forum',
                    'description' => $this->lang->forum_management_desc
                ];

                $sub_tabs['add_forum'] = [
                    'title' => $this->lang->add_forum,
                    'link' => $this->bb->admin_url . '/forum?action=add',
                    'description' => $this->lang->add_forum_desc
                ];
            }
        }

        $this->plugins->runHooks('admin_forum_management_begin');
        if ($this->bb->input['action'] == 'copy') {
            $this->plugins->runHooks('admin_forum_management_copy');

            if ($this->bb->request_method == 'post') {
                $from = $this->bb->getInput('from', 0);
                $to = $this->bb->getInput('to', 0);

                // Find the source forum
                $query = $this->db->simple_select('forums', '*', "fid='{$from}'");
                $from_forum = $this->db->fetch_array($query);
                if (!$this->db->num_rows($query)) {
                    $errors[] = $this->lang->error_invalid_source_forum;
                }

                if ($to == -1) {
                    // Create a new forum
                    if (empty($this->bb->input['title'])) {
                        $errors[] = $this->lang->error_new_forum_needs_name;
                    }

                    if ($this->bb->input['pid'] == -1 && $this->bb->input['type'] == 'f') {
                        $errors[] = $this->lang->error_no_parent;
                    }

                    if (!$errors) {
                        $new_forum = $from_forum;
                        unset($new_forum['fid'], $new_forum['threads'], $new_forum['posts'], $new_forum['lastpost'], $new_forum['lastposter'], $new_forum['lastposteruid'], $new_forum['lastposttid'], $new_forum['lastpostsubject'], $new_forum['unapprovedthreads'], $new_forum['unapprovedposts']);
                        $new_forum['name'] = $this->db->escape_string($this->bb->input['title']);
                        $new_forum['description'] = $this->db->escape_string($this->bb->input['description']);
                        $new_forum['type'] = $this->db->escape_string($this->bb->input['type']);
                        $new_forum['pid'] = $this->bb->getInput('pid', 0);
                        $new_forum['rulestitle'] = $this->db->escape_string($new_forum['rulestitle']);
                        $new_forum['rules'] = $this->db->escape_string($new_forum['rules']);
                        $new_forum['parentlist'] = '';

                        $to = $this->db->insert_query('forums', $new_forum);

                        // Generate parent list
                        $parentlist = $this->adm->make_parent_list($to);
                        $updatearray = [
                            'parentlist' => $parentlist
                        ];
                        $this->db->update_query('forums', $updatearray, "fid='{$to}'");
                    }
                } elseif ($this->bb->input['copyforumsettings'] == 1) {
                    // Copy settings to existing forum
                    $query = $this->db->simple_select('forums', '*', "fid='{$to}'");
                    $to_forum = $this->db->fetch_array($query);
                    if (!$this->db->num_rows($query)) {
                        $errors[] = $this->lang->error_invalid_destination_forum;
                    }

                    if (!$errors) {
                        $new_forum = $from_forum;
                        unset(
                            $new_forum['fid'],
                            $new_forum['threads'],
                            $new_forum['posts'],
                            $new_forum['lastpost'],
                            $new_forum['lastposter'],
                            $new_forum['lastposteruid'],
                            $new_forum['lastposttid'],
                            $new_forum['lastpostsubject'],
                            $new_forum['unapprovedthreads'],
                            $new_forum['unapprovedposts']
                        );

                        $new_forum['name'] = $this->db->escape_string($to_forum['name']);
                        $new_forum['description'] = $this->db->escape_string($to_forum['description']);
                        $new_forum['pid'] = $this->db->escape_string($to_forum['pid']);
                        $new_forum['parentlist'] = $this->db->escape_string($to_forum['parentlist']);
                        $new_forum['rulestitle'] = $this->db->escape_string($new_forum['rulestitle']);
                        $new_forum['rules'] = $this->db->escape_string($new_forum['rules']);

                        $this->db->update_query('forums', $new_forum, "fid='{$to}'");
                    }
                }

                if (!$errors) {
                    // Copy permissions
                    if (is_array($this->bb->input['copygroups']) && count($this->bb->input['copygroups'] > 0)) {
                        foreach ($this->bb->input['copygroups'] as $gid) {
                            $groups[] = (int)$gid;
                        }
                        $groups = implode(',', $groups);
                        $query = $this->db->simple_select('forumpermissions', '*', "fid='{$from}' AND gid IN ({$groups})");
                        $this->db->delete_query('forumpermissions', "fid='{$to}' AND gid IN ({$groups})", 1);
                        while ($permissions = $this->db->fetch_array($query)) {
                            unset($permissions['pid']);
                            $permissions['fid'] = $to;

                            $this->db->insert_query('forumpermissions', $permissions);
                        }

                        // Log admin action
                        $this->bblogger->log_admin_action($from, $from_forum['name'], $to, $new_forum['name'], $groups);
                    } else {
                        // Log admin action (no group permissions)
                        $this->bblogger->log_admin_action($from, $from_forum['name'], $to, $new_forum['name']);
                    }

                    $this->plugins->runHooks('admin_forum_management_copy_commit');

                    $this->cache->update_forums();
                    $this->cache->update_forumpermissions();

                    $this->session->flash_message($this->lang->success_forum_copied, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/forum?action=edit&fid=' . $to);
                }
            }

            $this->page->add_breadcrumb_item($this->lang->copy_forum);
            $html = $this->page->output_header($this->lang->copy_forum);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'copy_forum');

            $form = new Form($this->bb, $this->bb->admin_url . '/forum?action=copy', 'post');
            $html .= $form->getForm();

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
                $copy_data = $this->bb->input;
            } else {
                $copy_data['type'] = 'f';
                $copy_data['title'] = '';
                $copy_data['description'] = '';

                if (!isset($this->bb->input['pid'])) {
                    $copy_data['pid'] = '-1';
                } else {
                    $copy_data['pid'] = $this->bb->getInput('pid', 0);
                }
                $copy_data['disporder'] = '1';
                $copy_data['from'] = $this->bb->input['fid'];
                $copy_data['copyforumsettings'] = 0;
                $copy_data['pid'] = 0;
            }

            $types = [
                'f' => $this->lang->forum,
                'c' => $this->lang->category
            ];

            $create_a_options_f = [
                'id' => 'forum'
            ];

            $create_a_options_c = [
                'id' => 'category'
            ];

            if ($copy_data['type'] == 'f') {
                $create_a_options_f['checked'] = true;
            } else {
                $create_a_options_c['checked'] = true;
            }

            $usergroups = [];

            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
            }

            $form_container = new FormContainer($this, $this->lang->copy_forum);
            $form_container->output_row($this->lang->source_forum . ' <em>*</em>', $this->lang->source_forum_desc, $form->generate_forum_select('from', $copy_data['from'], ['id' => 'from']), 'from');
            $form_container->output_row($this->lang->destination_forum . ' <em>*</em>', $this->lang->destination_forum_desc, $form->generate_forum_select('to', isset($copy_data['to']) ? $copy_data['to'] : '', ['id' => 'to', 'main_option' => $this->lang->copy_to_new_forum]), 'to');
            $form_container->output_row($this->lang->copy_settings_and_properties, $this->lang->copy_settings_and_properties_desc, $form->generate_yes_no_radio('copyforumsettings', $copy_data['copyforumsettings']));
            $form_container->output_row($this->lang->copy_user_group_permissions, $this->lang->copy_user_group_permissions_desc, $form->generate_select_box('copygroups[]', $usergroups, $this->bb->getInput('copygroups', ''), ['id' => 'copygroups', 'multiple' => true, 'size' => 5]), 'copygroups');

            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->new_forum_settings);
            $form_container->output_row($this->lang->forum_type, $this->lang->forum_type_desc, $form->generate_radio_button('type', 'f', $this->lang->forum, $create_a_options_f) . "<br />\n" . $form->generate_radio_button('type', 'c', $this->lang->category, $create_a_options_c));
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $copy_data['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, '', $form->generate_text_area('description', $copy_data['description'], ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->parent_forum . ' <em>*</em>', $this->lang->parent_forum_desc, $form->generate_forum_select('pid', $copy_data['pid'], ['id' => 'pid', 'main_option' => $this->lang->none]), 'pid');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->copy_forum);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/ManagementCopy.html.twig');
        }

        if ($this->bb->input['action'] == 'editmod') {
            $query = $this->db->simple_select('moderators', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $mod_data = $this->db->fetch_array($query);

            if (!$mod_data['id']) {
                $this->session->flash_message($this->lang->error_incorrect_moderator, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum');
            }

            $this->plugins->runHooks('admin_forum_management_editmod');

            if ($mod_data['isgroup']) {
                $fieldname = 'title';
            } else {
                $fieldname = 'username';
            }

            if ($this->bb->request_method == 'post') {
                $mid = $this->bb->getInput('mid', 0);
                if (!$mid) {
                    $this->session->flash_message($this->lang->error_incorrect_moderator, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/forum');
                }

                if (!$errors) {
                    $fid = $this->bb->getInput('fid', 0);
                    $forum = $this->forum->get_forum($fid);
                    if ($mod_data['isgroup']) {
                        $mod = $this->bb->groupscache[$mod_data['id']];
                    } else {
                        $mod = $this->user->get_user($mod_data['id']);
                    }
                    $update_array = [
                        'fid' => (int)$fid,
                        'caneditposts' => $this->bb->getInput('caneditposts', 0),
                        'cansoftdeleteposts' => $this->bb->getInput('cansoftdeleteposts', 0),
                        'canrestoreposts' => $this->bb->getInput('canrestoreposts', 0),
                        'candeleteposts' => $this->bb->getInput('candeleteposts', 0),
                        'cansoftdeletethreads' => $this->bb->getInput('cansoftdeletethreads', 0),
                        'canrestorethreads' => $this->bb->getInput('canrestorethreads', 0),
                        'candeletethreads' => $this->bb->getInput('candeletethreads', 0),
                        'canviewips' => $this->bb->getInput('canviewips', 0),
                        'canviewunapprove' => $this->bb->getInput('canviewunapprove', 0),
                        'canviewdeleted' => $this->bb->getInput('canviewdeleted', 0),
                        'canopenclosethreads' => $this->bb->getInput('canopenclosethreads', 0),
                        'canstickunstickthreads' => $this->bb->getInput('canstickunstickthreads', 0),
                        'canapproveunapprovethreads' => $this->bb->getInput('canapproveunapprovethreads', 0),
                        'canapproveunapproveposts' => $this->bb->getInput('canapproveunapproveposts', 0),
                        'canapproveunapproveattachs' => $this->bb->getInput('canapproveunapproveattachs', 0),
                        'canmanagethreads' => $this->bb->getInput('canmanagethreads', 0),
                        'canmanagepolls' => $this->bb->getInput('canmanagepolls', 0),
                        'canpostclosedthreads' => $this->bb->getInput('canpostclosedthreads', 0),
                        'canmovetononmodforum' => $this->bb->getInput('canmovetononmodforum', 0),
                        'canusecustomtools' => $this->bb->getInput('canusecustomtools', 0),
                        'canmanageannouncements' => $this->bb->getInput('canmanageannouncements', 0),
                        'canmanagereportedposts' => $this->bb->getInput('canmanagereportedposts', 0),
                        'canviewmodlog' => $this->bb->getInput('canviewmodlog', 0)
                    ];

                    $this->plugins->runHooks('admin_forum_management_editmod_commit');

                    $this->db->update_query('moderators', $update_array, "mid='" . $this->bb->getInput('mid', 0) . "'");

                    $this->cache->update_moderators();

                    // Log admin action
                    $this->bblogger->log_admin_action($fid, $forum['name'], $mid, $mod[$fieldname]);

                    $this->session->flash_message($this->lang->success_moderator_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/forum?fid=' . $this->bb->getInput('fid', 0) . '#tab_moderators');
                }
            }

            if ($mod_data['isgroup']) {
                $query = $this->db->simple_select('usergroups', 'title', "gid='{$mod_data['id']}'");
                $mod_data[$fieldname] = $this->db->fetch_field($query, 'title');
            } else {
                $query = $this->db->simple_select('users', 'username', "uid='{$mod_data['id']}'");
                $mod_data[$fieldname] = $this->db->fetch_field($query, 'username');
            }

            $sub_tabs = [];

            $sub_tabs['edit_mod'] = [
                'title' => $this->lang->edit_mod,
                'link' => $this->bb->admin_url . '/forum?action=editmod&amp;mid=' . $this->bb->input['mid'],
                'description' => $this->lang->edit_mod_desc
            ];

            $this->page->add_breadcrumb_item($this->lang->forum_moderators, $this->bb->admin_url . "/forum?fid={$mod_data['fid']}#tab_moderators");
            $this->page->add_breadcrumb_item($this->lang->edit_forum);
            $html = $this->page->output_header($this->lang->edit_mod);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_mod');

            $form = new Form($this->bb, $this->bb->admin_url . '/forum?action=editmod', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('mid', $mod_data['mid']);

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
                $mod_data = $this->bb->input;
            }

            $form_container = new FormContainer($this, $this->lang->sprintf($this->lang->edit_mod_for, $mod_data[$fieldname]));
            $form_container->output_row($this->lang->forum, $this->lang->forum_desc, $form->generate_forum_select('fid', $mod_data['fid'], ['id' => 'fid']), 'fid');

            $moderator_permissions = [
                $form->generate_check_box('caneditposts', 1, $this->lang->can_edit_posts, ['checked' => $mod_data['caneditposts'], 'id' => 'caneditposts']),
                $form->generate_check_box('cansoftdeleteposts', 1, $this->lang->can_soft_delete_posts, ['checked' => $mod_data['cansoftdeleteposts'], 'id' => 'cansoftdeleteposts']),
                $form->generate_check_box('canrestoreposts', 1, $this->lang->can_restore_posts, ['checked' => $mod_data['canrestoreposts'], 'id' => 'canrestoreposts']),
                $form->generate_check_box('candeleteposts', 1, $this->lang->can_delete_posts, ['checked' => $mod_data['candeleteposts'], 'id' => 'candeleteposts']),
                $form->generate_check_box('cansoftdeletethreads', 1, $this->lang->can_soft_delete_threads, ['checked' => $mod_data['cansoftdeletethreads'], 'id' => 'cansoftdeletethreads']),
                $form->generate_check_box('canrestorethreads', 1, $this->lang->can_restore_threads, ['checked' => $mod_data['canrestorethreads'], 'id' => 'canrestorethreads']),
                $form->generate_check_box('candeletethreads', 1, $this->lang->can_delete_threads, ['checked' => $mod_data['candeletethreads'], 'id' => 'candeletethreads']),
                $form->generate_check_box('canviewips', 1, $this->lang->can_view_ips, ['checked' => $mod_data['canviewips'], 'id' => 'canviewips']),
                $form->generate_check_box('canviewunapprove', 1, $this->lang->can_view_unapprove, ['checked' => $mod_data['canviewunapprove'], 'id' => 'canviewunapprove']),
                $form->generate_check_box('canviewdeleted', 1, $this->lang->can_view_deleted, ['checked' => $mod_data['canviewdeleted'], 'id' => 'canviewdeleted']),
                $form->generate_check_box('canopenclosethreads', 1, $this->lang->can_open_close_threads, ['checked' => $mod_data['canopenclosethreads'], 'id' => 'canopenclosethreads']),
                $form->generate_check_box('canstickunstickthreads', 1, $this->lang->can_stick_unstick_threads, ['checked' => $mod_data['canstickunstickthreads'], 'id' => 'canstickunstickthreads']),
                $form->generate_check_box('canapproveunapprovethreads', 1, $this->lang->can_approve_unapprove_threads, ['checked' => $mod_data['canapproveunapprovethreads'], 'id' => 'canapproveunapprovethreads']),
                $form->generate_check_box('canapproveunapproveposts', 1, $this->lang->can_approve_unapprove_posts, ['checked' => $mod_data['canapproveunapproveposts'], 'id' => 'canapproveunapproveposts']),
                $form->generate_check_box('canapproveunapproveattachs', 1, $this->lang->can_approve_unapprove_attachments, ['checked' => $mod_data['canapproveunapproveattachs'], 'id' => 'canapproveunapproveattachs']),
                $form->generate_check_box('canmanagethreads', 1, $this->lang->can_manage_threads, ['checked' => $mod_data['canmanagethreads'], 'id' => 'canmanagethreads']),
                $form->generate_check_box('canmanagepolls', 1, $this->lang->can_manage_polls, ['checked' => $mod_data['canmanagepolls'], 'id' => 'canmanagepolls']),
                $form->generate_check_box('canpostclosedthreads', 1, $this->lang->can_post_closed_threads, ['checked' => $mod_data['canpostclosedthreads'], 'id' => 'canpostclosedthreads']),
                $form->generate_check_box('canmovetononmodforum', 1, $this->lang->can_move_to_other_forums, ['checked' => $mod_data['canmovetononmodforum'], 'id' => 'canmovetononmodforum']),
                $form->generate_check_box('canusecustomtools', 1, $this->lang->can_use_custom_tools, ['checked' => $mod_data['canusecustomtools'], 'id' => 'canusecustomtools'])
            ];
            $form_container->output_row($this->lang->moderator_permissions, '', "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $moderator_permissions) . "</div>");

            $moderator_cp_permissions = [
                $form->generate_check_box('canmanageannouncements', 1, $this->lang->can_manage_announcements, ['checked' => $mod_data['canmanageannouncements'], 'id' => 'canmanageannouncements']),
                $form->generate_check_box('canmanagereportedposts', 1, $this->lang->can_manage_reported_posts, ['checked' => $mod_data['canmanagereportedposts'], 'id' => 'canmanagereportedposts']),
                $form->generate_check_box('canviewmodlog', 1, $this->lang->can_view_mod_log, ['checked' => $mod_data['canviewmodlog'], 'id' => 'canviewmodlog'])
            ];
            $form_container->output_row($this->lang->moderator_cp_permissions, $this->lang->moderator_cp_permissions_desc, "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $moderator_cp_permissions) . "</div>");

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_mod);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/ManagementEditMod.html.twig');
        }

        if ($this->bb->input['action'] == 'clear_permission') {
            $pid = $this->bb->getInput('pid', 0);
            $fid = $this->bb->getInput('fid', 0);
            $gid = $this->bb->getInput('gid', 0);

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/forum?fid=' . $fid);
            }

            $this->plugins->runHooks('admin_forum_management_clear_permission');

            if ($this->bb->request_method == 'post') {
                if ((!$fid || !$gid) && $pid) {
                    $query = $this->db->simple_select('forumpermissions', 'fid, gid', "pid='{$pid}'");
                    $result = $this->db->fetch_array($query);
                    $fid = $result['fid'];
                    $gid = $result['gid'];
                }

                if ($pid) {
                    $this->db->delete_query('forumpermissions', "pid='{$pid}'");
                } else {
                    $this->db->delete_query('forumpermissions', "gid='{$gid}' AND fid='{$fid}'");
                }

                $this->plugins->runHooks('admin_forum_management_clear_permission_commit');

                $this->cache->update_forumpermissions();

                $this->session->flash_message($this->lang->success_custom_permission_cleared, 'success');
                return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_permissions");
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . "/forum?action=clear_permission&amp;pid={$pid}&amp;gid={$gid}&amp;fid={$fid}", $this->lang->confirm_clear_custom_permission);
            }
        }

        if ($this->bb->input['action'] == 'permissions') {
            $this->plugins->runHooks('admin_forum_management_permissions');

            if ($this->bb->request_method == 'post') {
                $pid = $this->bb->getInput('pid', 0);
                $fid = $this->bb->getInput('fid', 0);
                $gid = $this->bb->getInput('gid', 0);
                $forum = $this->forum->get_forum($fid);

                if ((!$fid || !$gid) && $pid) {
                    $query = $this->db->simple_select('forumpermissions', 'fid, gid', "pid='{$pid}'");
                    $result = $this->db->fetch_array($query);
                    $fid = $result['fid'];
                    $gid = $result['gid'];
                    $forum = $this->forum->get_forum($fid);
                }

                $field_list = [];
                $fields_array = $this->db->show_fields_from('forumpermissions');
                if (is_array($this->bb->input['permissions'])) {
                    // User has set permissions for this group...
                    foreach ($fields_array as $field) {
                        if (strpos($field['Field'], 'can') !== false || strpos($field['Field'], 'mod') !== false) {
                            if (array_key_exists($field['Field'], $this->bb->input['permissions'])) {
                                $update_array[$this->db->escape_string($field['Field'])] = (int)$this->bb->input['permissions'][$field['Field']];
                            } else {
                                $update_array[$this->db->escape_string($field['Field'])] = 0;
                            }
                        }
                    }
                } else {
                    // Else, we assume that the group has no permissions...
                    foreach ($fields_array as $field) {
                        if (strpos($field['Field'], 'can') !== false || strpos($field['Field'], 'mod') !== false) {
                            $update_array[$this->db->escape_string($field['Field'])] = 0;
                        }
                    }
                }

                if ($fid && !$pid) {
                    $update_array['fid'] = $fid;
                    $update_array['gid'] = $this->bb->getInput('gid', 0);
                    $this->db->insert_query('forumpermissions', $update_array);
                }

                $this->plugins->runHooks('admin_forum_management_permissions_commit');

                if (!($fid && !$pid)) {
                    $this->db->update_query('forumpermissions', $update_array, "pid='{$pid}'");
                }

                $this->cache->update_forumpermissions();

                // Log admin action
                $this->bblogger->log_admin_action($fid, $forum['name']);

                if ($this->bb->input['ajax'] == 1) {
                    echo json_encode("<script type=\"text/javascript\">$('#row_{$gid}').html('" . str_replace(["'", "\t", "\n"], ["\\'", '', ''], $this->retrieve_single_permissions_row($gid, $fid)) . "'); QuickPermEditor.init({$gid});</script>");
                    die;
                } else {
                    $this->session->flash_message($this->lang->success_forum_permissions_saved, 'success');
                    return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_permissions");
                }
            }

            if ($this->bb->input['ajax'] != 1) {
                $sub_tabs = [];

                if ($this->bb->input['fid'] && $this->bb->input['gid']) {
                    $sub_tabs['edit_permissions'] = [
                        'title' => $this->lang->forum_permissions,
                        'link' => $this->bb->admin_url . '/forum?action=permissions&amp;fid=' . $this->bb->input['fid'] . '&amp;gid=' . $this->bb->input['gid'],
                        'description' => $this->lang->forum_permissions_desc
                    ];

                    $this->page->add_breadcrumb_item($this->lang->forum_permissions2, $this->bb->admin_url . '/forum?fid=' . $this->bb->input['fid'] . '#tab_permissions');
                } else {
                    $query = $this->db->simple_select('forumpermissions', 'fid', "pid='" . $this->bb->getInput('pid', 0) . "'");
                    $this->bb->input['fid'] = $this->db->fetch_field($query, 'fid');

                    $sub_tabs['edit_permissions'] = [
                        'title' => $this->lang->forum_permissions,
                        'link' => $this->bb->admin_url . '/forum?action=permissions&amp;pid=' . $this->bb->getInput('pid', 0),
                        'description' => $this->lang->forum_permissions_desc
                    ];

                    $this->page->add_breadcrumb_item($this->lang->forum_permissions2, $this->bb->admin_url . '/forum?fid=' . $this->bb->input['fid'] . '#tab_permissions');
                }

                $this->page->add_breadcrumb_item($this->lang->forum_permissions);
                $html = $this->page->output_header($this->lang->forum_permissions);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_permissions');
            } else {
                // style="width: auto; max-width: 600px;"
                echo "
		
		<script src=\"{$this->bb->asset_url}/admin/jscripts/tabs.js\" type=\"text/javascript\"></script>\n
		<script type=\"text/javascript\">
<!--
$(document).ready(function() {
	$(\"#modal_form\").on(\"click\", \"#savePermissions\", function(e) {
		e.preventDefault();
		
		var datastring = $(\"#modal_form\").serialize();
		$.ajax({
			type: \"POST\",
			url: $(\"#modal_form\").attr('action'),
			data: datastring,
			dataType: \"json\",
			success: function(data) {
				$(data).filter(\"script\").each(function(e) {
					eval($(this).text());
				});
				$.modal.close();
			},
			error: function(){
			}
		});
	});
});
// -->
		</script>
		<div class=\"modal fade\" role=\"dialog\" style=\"width: auto;\">
		<div style=\"overflow-y: auto; max-height: 400px\">";
            }

            if ($this->bb->input['pid'] || ($this->bb->input['gid'] && $this->bb->input['fid'])) {
                if ($this->bb->input['ajax'] != 1) {
                    $form = new Form($this->bb, $this->bb->admin_url . '/forum?action=permissions', 'post');
                    $html .= $form->getForm();
                    $html .= $form->generate_hidden_field('usecustom', '1');
                } else {
                    $form = new Form($this->bb, $this->bb->admin_url . '/forum?action=permissions&amp;ajax=1&amp;pid=' . $this->bb->getInput('pid', 0) . '&amp;gid=' . $this->bb->getInput('gid', 0) . '&amp;fid=' . $this->bb->getInput('gid', 0), 'post', 'modal_form');
                    echo $form->getForm();
                    echo $form->generate_hidden_field('usecustom', '1');
                }

                if ($errors) {
                    if ($this->bb->input['ajax'] != 1) {
                        $html .= $this->page->output_inline_error($errors);
                    } else {
                        echo $this->page->output_inline_error($errors);
                    }
                    $permission_data = $this->bb->input;

                    $query = $this->db->simple_select('usergroups', '*', "gid='{$permission_data['gid']}'");
                    $usergroup = $this->db->fetch_array($query);

                    $query = $this->db->simple_select('forums', '*', "fid='{$permission_data['fid']}'");
                    $forum = $this->db->fetch_array($query);
                } else {
                    $pid = $this->bb->getInput('pid', 0);
                    $gid = $this->bb->getInput('gid', 0);
                    $fid = $this->bb->getInput('fid', 0);

                    if ($pid) {
                        $query = $this->db->simple_select('forumpermissions', '*', "pid='{$pid}'");
                    } else {
                        $query = $this->db->simple_select('forumpermissions', '*', "fid='{$fid}' AND gid='{$gid}'", ['limit' => 1]);
                    }

                    $permission_data = $this->db->fetch_array($query);

                    if (!$fid) {
                        $fid = $permission_data['fid'];
                    }

                    if (!$gid) {
                        $gid = $permission_data['gid'];
                    }

                    if (!$pid) {
                        $pid = $permission_data['pid'];
                    }

                    $query = $this->db->simple_select('usergroups', '*', "gid='$gid'");
                    $usergroup = $this->db->fetch_array($query);

                    $query = $this->db->simple_select('forums', '*', "fid='$fid'");
                    $forum = $this->db->fetch_array($query);

                    $sperms = $permission_data;

                    $sql = $this->forum->build_parent_list($fid);
                    $query = $this->db->simple_select('forumpermissions', '*', "$sql AND gid='$gid'");
                    $customperms = $this->db->fetch_array($query);

                    if ($permission_data['pid']) {
                        $permission_data['usecustom'] = 1;
                        $html .= $form->generate_hidden_field('pid', $pid);
                    } else {
                        $html .= $form->generate_hidden_field('fid', $fid);
                        $html .= $form->generate_hidden_field('gid', $gid);
                        if (!$customperms['pid']) {
                            $permission_data = $this->group->usergroup_permissions($gid);
                        } else {
                            $permission_data = $this->forum->forum_permissions($fid, 0, $gid);
                        }
                    }
                }

                $groups = [
                    'canviewthreads' => 'viewing',
                    'canview' => 'viewing',
                    'canonlyviewownthreads' => 'viewing',
                    'candlattachments' => 'viewing',

                    'canpostthreads' => 'posting_rating',
                    'canpostreplys' => 'posting_rating',
                    'canonlyreplyownthreads' => 'posting_rating',
                    'canpostattachments' => 'posting_rating',
                    'canratethreads' => 'posting_rating',

                    'caneditposts' => 'editing',
                    'candeleteposts' => 'editing',
                    'candeletethreads' => 'editing',
                    'caneditattachments' => 'editing',

                    'modposts' => 'moderate',
                    'modthreads' => 'moderate',
                    'modattachments' => 'moderate',
                    'mod_edit_posts' => 'moderate',

                    'canpostpolls' => 'polls',
                    'canvotepolls' => 'polls',
                    'cansearch' => 'misc',
                ];

                $groups = $this->plugins->runHooks('admin_forum_management_permission_groups', $groups);

                $tabs = [];
                foreach (array_unique(array_values($groups)) as $group) {
                    $lang_group = 'group_' . $group;
                    $tabs[$group] = $this->lang->$lang_group;
                }

                if ($this->bb->input['ajax'] == 1) {
                    echo $this->page->output_tab_control($tabs, false, 'tabs2');
                } else {
                    $html .= $this->page->output_tab_control($tabs);
                }

                $field_list = [];
                $fields_array = $this->db->show_fields_from('forumpermissions');
                foreach ($fields_array as $field) {
                    if (strpos($field['Field'], 'can') !== false || strpos($field['Field'], 'mod') !== false) {
                        if (array_key_exists($field['Field'], $groups)) {
                            $field_list[$groups[$field['Field']]][] = $field['Field'];
                        } else {
                            $field_list['misc'][] = $field['Field'];
                        }
                    }
                }

                $_tabs = '';
                foreach (array_unique(array_values($groups)) as $group) {
                    $lang_group = 'group_' . $group;
                    $_tabs .= "<div id=\"tab_" . $group . "\">\n";
                    $form_container = new FormContainer($this, "\"" . htmlspecialchars_uni($usergroup['title']) . "\" {$this->lang->custom_permissions_for} \"" . htmlspecialchars_uni($forum['name']) . "\"");
                    $fields = [];
                    foreach ($field_list[$group] as $field) {
                        $lang_field = $group . '_field_' . $field;
                        $fields[] = $form->generate_check_box("permissions[{$field}]", 1, $this->lang->$lang_field, ['checked' => $permission_data[$field], 'id' => $field]);
                    }
                    $form_container->output_row('', '', "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $fields) . "</div>");
                    $_tabs .= $form_container->end();
                    $_tabs .= '</div>';
                }

                if (isset($this->bb->input['ajax']) && $this->bb->input['ajax'] == 1) {
                    echo $_tabs;
                    $buttons[] = $form->generate_submit_button($this->lang->cancel, ['onclick' => '$.modal.close(); return false;']);
                    $buttons[] = $form->generate_submit_button($this->lang->save_permissions, ['id' => 'savePermissions']);
                    echo $form->output_submit_wrapper($buttons);
                    echo $form->end();
                    echo '</div>';
                    echo '</div>';
//                    echo '</div>';
                } else {
                    $html .= $_tabs;
                    $buttons[] = $form->generate_submit_button($this->lang->save_permissions);
                    $html .= $form->output_submit_wrapper($buttons);

                    $html .= $form->end();
                }
            }

            if ($this->bb->input['ajax'] != 1) {
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/ManagementPermissions.html.twig');
            } else {
                exit;
            }
        }

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_forum_management_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                $pid = $this->bb->getInput('pid', 0);
                $type = $this->bb->input['type'];

                if ($pid <= 0 && $type == 'f') {
                    $errors[] = $this->lang->error_no_parent;
                }

                if (!$errors) {
                    if ($pid < 0) {
                        $pid = 0;
                    }
                    $insert_array = [
                        'name' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'linkto' => $this->db->escape_string($this->bb->input['linkto']),
                        'type' => $this->db->escape_string($type),
                        'pid' => $pid,
                        'parentlist' => '',
                        'disporder' => $this->bb->getInput('disporder', 0),
                        'active' => $this->bb->getInput('active', 0),
                        'open' => $this->bb->getInput('open', 0),
                        'allowhtml' => $this->bb->getInput('allowhtml', 0),
                        'allowmycode' => $this->bb->getInput('allowmycode', 0),
                        'allowsmilies' => $this->bb->getInput('allowsmilies', 0),
                        'allowimgcode' => $this->bb->getInput('allowimgcode', 0),
                        'allowvideocode' => $this->bb->getInput('allowvideocode', 0),
                        'allowpicons' => $this->bb->getInput('allowpicons', 0),
                        'allowtratings' => $this->bb->getInput('allowtratings', 0),
                        'usepostcounts' => $this->bb->getInput('usepostcounts', 0),
                        'usethreadcounts' => $this->bb->getInput('usethreadcounts', 0),
                        'requireprefix' => $this->bb->getInput('requireprefix', 0),
                        'password' => $this->db->escape_string($this->bb->input['password']),
                        'showinjump' => $this->bb->getInput('showinjump', 0),
                        'style' => $this->bb->getInput('style', 0),
                        'overridestyle' => $this->bb->getInput('overridestyle', 0),
                        'rulestype' => $this->bb->getInput('rulestype', 0),
                        'rulestitle' => $this->db->escape_string($this->bb->input['rulestitle']),
                        'rules' => $this->db->escape_string($this->bb->input['rules']),
                        'defaultdatecut' => $this->bb->getInput('defaultdatecut', 0),
                        'defaultsortby' => $this->db->escape_string($this->bb->input['defaultsortby']),
                        'defaultsortorder' => $this->db->escape_string($this->bb->input['defaultsortorder']),
                    ];
                    $fid = $this->db->insert_query('forums', $insert_array);

                    $parentlist = $this->adm->make_parent_list($fid);
                    $this->db->update_query('forums', ['parentlist' => $parentlist], "fid='$fid'");

                    $this->inherit = $this->bb->input['default_permissions'];

                    foreach ($this->bb->input as $id => $permission) {
                        if (strpos($id, 'fields_') === false) {
                            continue;
                        }

                        list(, $gid) = explode('fields_', $id);

                        // If it isn't an array then it came from the javascript form
                        if (!is_array($permission)) {
                            $permission = explode(',', $permission);
                            $permission = array_flip($permission);
                            foreach ($permission as $name => $value) {
                                $permission[$name] = 1;
                            }
                        }

                        foreach (['canview', 'canpostthreads', 'canpostreplys', 'canpostpolls'] as $name) {
                            if (in_array($name, $permission) || isset($permission[$name])) {
                                $permissions[$name][$gid] = 1;
                            } else {
                                $permissions[$name][$gid] = 0;
                            }
                        }
                    }

                    $this->canview = $permissions['canview'];
                    $this->canpostthreads = $permissions['canpostthreads'];
                    $this->canpostpolls = $permissions['canpostpolls'];
                    $canpostattachments = $permissions['canpostattachments'];
                    $this->canpostreplies = $permissions['canpostreplys'];
                    $this->save_quick_perms($fid);

                    $this->plugins->runHooks('admin_forum_management_add_commit');

                    $this->cache->update_forums();

                    // Log admin action
                    $this->bblogger->log_admin_action($fid, $insert_array['name']);

                    $this->session->flash_message($this->lang->success_forum_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/forum');
                }
            }

            $this->page->extra_header .= "<script src=\"{$this->bb->asset_url}/admin/jscripts/quick_perm_editor.js\" type=\"text/javascript\"></script>\n";

            $this->page->add_breadcrumb_item($this->lang->add_forum);
            $html = $this->page->output_header($this->lang->add_forum);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_forum');

            $form = new Form($this->bb, $this->bb->admin_url . '/forum?action=add', 'post');
            $html .= $form->getForm();

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
                $forum_data = $this->bb->input;
            } else {
                $forum_data['type'] = 'f';
                $forum_data['title'] = '';
                $forum_data['description'] = '';

                if (!isset($this->bb->input['pid'])) {
                    $forum_data['pid'] = '-1';
                } else {
                    $forum_data['pid'] = $this->bb->getInput('pid', 0);
                }
                $forum_data['disporder'] = '1';
                $forum_data['linkto'] = '';
                $forum_data['password'] = '';
                $forum_data['active'] = 1;
                $forum_data['open'] = 1;
                $forum_data['overridestyle'] = '';
                $forum_data['style'] = '';
                $forum_data['rulestype'] = '';
                $forum_data['rulestitle'] = '';
                $forum_data['rules'] = '';
                $forum_data['defaultdatecut'] = '';
                $forum_data['defaultsortby'] = '';
                $forum_data['defaultsortorder'] = '';
                $forum_data['allowhtml'] = '';
                $forum_data['allowmycode'] = 1;
                $forum_data['allowsmilies'] = 1;
                $forum_data['allowimgcode'] = 1;
                $forum_data['allowvideocode'] = 1;
                $forum_data['allowpicons'] = 1;
                $forum_data['allowtratings'] = 1;
                $forum_data['showinjump'] = 1;
                $forum_data['usepostcounts'] = 1;
                $forum_data['usethreadcounts'] = 1;
                $forum_data['requireprefix'] = 0;
            }

            $types = [
                'f' => $this->lang->forum,
                'c' => $this->lang->category
            ];

            $create_a_options_f = [
                'id' => 'forum'
            ];

            $create_a_options_c = [
                'id' => 'category'
            ];

            if ($forum_data['type'] == 'f') {
                $create_a_options_f['checked'] = true;
            } else {
                $create_a_options_c['checked'] = true;
            }

            $form_container = new FormContainer($this, $this->lang->add_forum);
            $form_container->output_row($this->lang->forum_type, $this->lang->forum_type_desc, $form->generate_radio_button('type', 'f', $this->lang->forum, $create_a_options_f) . "<br />\n" . $form->generate_radio_button('type', 'c', $this->lang->category, $create_a_options_c));
            $form_container->output_row($this->lang->title . " <em>*</em>", "", $form->generate_text_box('title', $forum_data['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, "", $form->generate_text_area('description', $forum_data['description'], ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->parent_forum . " <em>*</em>", $this->lang->parent_forum_desc, $form->generate_forum_select('pid', $forum_data['pid'], ['id' => 'pid', 'main_option' => $this->lang->none]), 'pid');
            $form_container->output_row($this->lang->display_order, "", $form->generate_numeric_field('disporder', $forum_data['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $html .= $form_container->end();

            $html .= "<div id=\"additional_options_link\"><strong><a href=\"#\" onclick=\"$('#additional_options_link').toggle(); $('#additional_options').fadeToggle('fast'); return false;\">{$this->lang->show_additional_options}</a></strong><br /><br /></div>";
            $html .= "<div id=\"additional_options\" style=\"display: none;\">";
            $form_container = new FormContainer($this, "<div class=\"float_right\" style=\"font-weight: normal;\"><a href=\"#\" onclick=\"$('#additional_options_link').toggle(); $('#additional_options').fadeToggle('fast'); return false;\">{$this->lang->hide_additional_options}</a></div>" . $this->lang->additional_forum_options);
            $form_container->output_row($this->lang->forum_link, $this->lang->forum_link_desc, $form->generate_text_box('linkto', $forum_data['linkto'], ['id' => 'linkto']), 'linkto');
            $form_container->output_row($this->lang->forum_password, $this->lang->forum_password_desc, $form->generate_text_box('password', $forum_data['password'], ['id' => 'password']), 'password');

            $access_options = [
                $form->generate_check_box('active', 1, $this->lang->forum_is_active . "<br />\n<small>{$this->lang->forum_is_active_desc}</small>", ['checked' => $forum_data['active'], 'id' => 'active']),
                $form->generate_check_box('open', 1, $this->lang->forum_is_open . "<br />\n<small>{$this->lang->forum_is_open_desc}</small>", ['checked' => $forum_data['open'], 'id' => 'open'])
            ];

            $form_container->output_row($this->lang->access_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $access_options) . "</div>");

            $styles = [
                '0' => $this->lang->use_default
            ];

            $query = $this->db->simple_select('themes', 'tid,name', "name!='((master))' AND name!='((master-backup))'", ['order_by' => 'name']);
            while ($style = $this->db->fetch_array($query)) {
                $styles[$style['tid']] = htmlspecialchars_uni($style['name']);
            }

            $style_options = [
                $form->generate_check_box('overridestyle', 1, $this->lang->override_user_style, ['checked' => $forum_data['overridestyle'], 'id' => 'overridestyle']),
                $this->lang->forum_specific_style . "<br />\n" . $form->generate_select_box('style', $styles, $forum_data['style'], ['id' => 'style'])
            ];

            $form_container->output_row($this->lang->style_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $style_options) . "</div>");

            $display_methods = [
                '0' => $this->lang->dont_display_rules,
                '1' => $this->lang->display_rules_inline,
                '3' => $this->lang->display_rules_inline_new,
                '2' => $this->lang->display_rules_link
            ];

            $forum_rules = [
                $this->lang->display_method . "<br />\n" . $form->generate_select_box('rulestype', $display_methods, $forum_data['rulestype'], ['checked' => $forum_data['rulestype'], 'id' => 'rulestype']),
                $this->lang->title . "<br />\n" . $form->generate_text_box('rulestitle', $forum_data['rulestitle'], ['checked' => $forum_data['rulestitle'], 'id' => 'rulestitle']),
                $this->lang->rules . "<br />\n" . $form->generate_text_area('rules', $forum_data['rules'], ['checked' => $forum_data['rules'], 'id' => 'rules'])
            ];

            $form_container->output_row($this->lang->forum_rules, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $forum_rules) . "</div>");

            $default_date_cut = [
                0 => $this->lang->board_default,
                1 => $this->lang->datelimit_1day,
                5 => $this->lang->datelimit_5days,
                10 => $this->lang->datelimit_10days,
                20 => $this->lang->datelimit_20days,
                50 => $this->lang->datelimit_50days,
                75 => $this->lang->datelimit_75days,
                100 => $this->lang->datelimit_100days,
                365 => $this->lang->datelimit_lastyear,
                9999 => $this->lang->datelimit_beginning,
            ];

            $default_sort_by = [
                "" => $this->lang->board_default,
                "subject" => $this->lang->sort_by_subject,
                "lastpost" => $this->lang->sort_by_lastpost,
                "starter" => $this->lang->sort_by_starter,
                "started" => $this->lang->sort_by_started,
                "rating" => $this->lang->sort_by_rating,
                "replies" => $this->lang->sort_by_replies,
                "views" => $this->lang->sort_by_views,
            ];

            $default_sort_order = [
                "" => $this->lang->board_default,
                "asc" => $this->lang->sort_order_asc,
                "desc" => $this->lang->sort_order_desc,
            ];

            $view_options = [
                $this->lang->default_date_cut . "<br />\n" . $form->generate_select_box('defaultdatecut', $default_date_cut, $forum_data['defaultdatecut'], ['checked' => $forum_data['defaultdatecut'], 'id' => 'defaultdatecut']),
                $this->lang->default_sort_by . "<br />\n" . $form->generate_select_box('defaultsortby', $default_sort_by, $forum_data['defaultsortby'], ['checked' => $forum_data['defaultsortby'], 'id' => 'defaultsortby']),
                $this->lang->default_sort_order . "<br />\n" . $form->generate_select_box('defaultsortorder', $default_sort_order, $forum_data['defaultsortorder'], ['checked' => $forum_data['defaultsortorder'], 'id' => 'defaultsortorder']),
            ];

            $form_container->output_row($this->lang->default_view_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $view_options) . "</div>");

            $misc_options = [
                $form->generate_check_box('allowhtml', 1, $this->lang->allow_html, ['checked' => $forum_data['allowhtml'], 'id' => 'allowhtml']),
                $form->generate_check_box('allowmycode', 1, $this->lang->allow_mycode, ['checked' => $forum_data['allowmycode'], 'id' => 'allowmycode']),
                $form->generate_check_box('allowsmilies', 1, $this->lang->allow_smilies, ['checked' => $forum_data['allowsmilies'], 'id' => 'allowsmilies']),
                $form->generate_check_box('allowimgcode', 1, $this->lang->allow_img_code, ['checked' => $forum_data['allowimgcode'], 'id' => 'allowimgcode']),
                $form->generate_check_box('allowvideocode', 1, $this->lang->allow_video_code, ['checked' => $forum_data['allowvideocode'], 'id' => 'allowvideocode']),
                $form->generate_check_box('allowpicons', 1, $this->lang->allow_post_icons, ['checked' => $forum_data['allowpicons'], 'id' => 'allowpicons']),
                $form->generate_check_box('allowtratings', 1, $this->lang->allow_thread_ratings, ['checked' => $forum_data['allowtratings'], 'id' => 'allowtratings']),
                $form->generate_check_box('showinjump', 1, $this->lang->show_forum_jump, ['checked' => $forum_data['showinjump'], 'id' => 'showinjump']),
                $form->generate_check_box('usepostcounts', 1, $this->lang->use_postcounts, ['checked' => $forum_data['usepostcounts'], 'id' => 'usepostcounts']),
                $form->generate_check_box('usethreadcounts', 1, $this->lang->use_threadcounts, ['checked' => $forum_data['usethreadcounts'], 'id' => 'usethreadcounts']),
                $form->generate_check_box('requireprefix', 1, $this->lang->require_thread_prefix, ['checked' => $forum_data['requireprefix'], 'id' => 'requireprefix'])
            ];

            $form_container->output_row($this->lang->misc_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $misc_options) . "</div>");
            $html .= $form_container->end();
            $html .= "</div>";

            $query = $this->db->simple_select("usergroups", "*", "", ["order" => "name"]);
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = $usergroup;
            }

            $cached_forum_perms = $this->cache->read("forumpermissions");
            $field_list = [
                'canview' => $this->lang->permissions_canview,
                'canpostthreads' => $this->lang->permissions_canpostthreads,
                'canpostreplys' => $this->lang->permissions_canpostreplys,
                'canpostpolls' => $this->lang->permissions_canpostpolls,
            ];

            $field_list2 = [
                'canview' => $this->lang->perm_drag_canview,
                'canpostthreads' => $this->lang->perm_drag_canpostthreads,
                'canpostreplys' => $this->lang->perm_drag_canpostreplys,
                'canpostpolls' => $this->lang->perm_drag_canpostpolls,
            ];

            $ids = [];

            $form_container = new FormContainer($this, $this->lang->forum_permissions);
            $form_container->output_row_header($this->lang->permissions_group, ["class" => "align_center", 'style' => 'width: 40%']);
            $form_container->output_row_header($this->lang->overview_allowed_actions, ["class" => "align_center"]);
            $form_container->output_row_header($this->lang->overview_disallowed_actions, ["class" => "align_center"]);

            if ($this->bb->request_method == "post") {
                foreach ($usergroups as $usergroup) {
                    if (isset($this->bb->input['fields_' . $usergroup['gid']])) {
                        $input_permissions = $this->bb->input['fields_' . $usergroup['gid']];
                        if (!is_array($input_permissions)) {
                            // Convering the comma separated list from Javascript form into a variable
                            $input_permissions = explode(',', $input_permissions);
                        }
                        foreach ($input_permissions as $input_permission) {
                            $this->bb->input['permissions'][$usergroup['gid']][$input_permission] = 1;
                        }
                    }
                }
            }

            foreach ($usergroups as $usergroup) {
                $perms = [];
                if (isset($this->bb->input['default_permissions']) && $this->bb->input['default_permissions'][$usergroup['gid']]) {
                    if (is_array($existing_permissions) && $existing_permissions[$usergroup['gid']]) {
                        $perms = $existing_permissions[$usergroup['gid']];
                        $default_checked = false;
                    } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum_data['fid']][$usergroup['gid']]) {
                        $perms = $cached_forum_perms[$forum_data['fid']][$usergroup['gid']];
                        $default_checked = true;
                    } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum_data['pid']][$usergroup['gid']]) {
                        $perms = $cached_forum_perms[$forum_data['pid']][$usergroup['gid']];
                        $default_checked = true;
                    }
                }

                if (!$perms) {
                    $perms = $usergroup;
                    $default_checked = true;
                }

                foreach ($field_list as $forum_permission => $forum_perm_title) {
                    if (isset($this->bb->input['permissions'])) {
                        if ($this->bb->input['default_permissions'][$usergroup['gid']]) {
                            $default_checked = true;
                        } else {
                            $default_checked = false;
                        }

                        if ($this->bb->input['permissions'][$usergroup['gid']][$forum_permission]) {
                            $perms_checked[$forum_permission] = 1;
                        } else {
                            $perms_checked[$forum_permission] = 0;
                        }
                    } else {
                        if ($perms[$forum_permission] == 1) {
                            $perms_checked[$forum_permission] = 1;
                        } else {
                            $perms_checked[$forum_permission] = 0;
                        }
                    }
                }
                $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);

                if ($default_checked) {
                    $inherited_text = $this->lang->inherited_permission;
                } else {
                    $inherited_text = $this->lang->custom_permission;
                }
                //$default_click = "if(\$(this).is(':checked')) { ?? }";
                $form_container->output_cell("<strong>{$usergroup['title']}</strong><br />" .
                    $form->generate_check_box(
                        "default_permissions[{$usergroup['gid']}]",
                        1,
                        '',
                        ['id' => "default_permissions_{$usergroup['gid']}", "checked" => $default_checked/*, "onclick" => $default_click*/]
                    )
                    . " <small><label for=\"default_permissions_{$usergroup['gid']}\">{$this->lang->permissions_use_group_default}</label></small>");

                $field_select = "<div class=\"quick_perm_fields\">\n";
                $field_select .= "<div class=\"enabled\"><ul id=\"fields_enabled_{$usergroup['gid']}\">\n";
                foreach ($perms_checked as $perm => $value) {
                    if ($value == 1) {
                        $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
                    }
                }
                $field_select .= "</ul></div>\n";
                $field_select .= "<div class=\"disabled\"><ul id=\"fields_disabled_{$usergroup['gid']}\">\n";
                foreach ($perms_checked as $perm => $value) {
                    if ($value == 0) {
                        $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
                    }
                }
                $field_select .= "</ul></div></div>\n";
                $field_select .= $form->generate_hidden_field("fields_" . $usergroup['gid'], @implode(",", @array_keys($perms_checked, '1')), ['id' => 'fields_' . $usergroup['gid']]);
                $field_select = str_replace("'", "\\'", $field_select);
                $field_select = str_replace("\n", "", $field_select);

                $field_select = "<script type=\"text/javascript\">
//<![CDATA[
document.write('" . str_replace("/", "\/", $field_select) . "');
//]]>
</script>\n";

                $field_selected = [];
                foreach ($field_list as $forum_permission => $permission_title) {
                    $field_options[$forum_permission] = $permission_title;
                    if ($perms_checked[$forum_permission]) {
                        $field_selected[] = $forum_permission;
                    }
                }

                $field_select .= "<noscript>" . $form->generate_select_box('fields_' . $usergroup['gid'] . '[]', $field_options, $field_selected, ['id' => 'fields_' . $usergroup['gid'] . '[]', 'multiple' => true]) . "</noscript>\n";
                $form_container->output_cell($field_select, ['colspan' => 2]);

                $form_container->construct_row();

                $ids[] = $usergroup['gid'];
            }
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_forum);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            // Write in our JS based field selector
            $html .= "<script type=\"text/javascript\">\n<!--\n";
            foreach ($ids as $id) {
                $html .= "$(function() { QuickPermEditor.init(" . $id . ") });\n";
            }
            $html .= "// -->\n</script>\n";

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/ManagementAdd.html.twig');
        }

        if ($this->bb->input['action'] == "edit") {
            if (!$this->bb->input['fid']) {
                $this->session->flash_message($this->lang->error_invalid_fid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum');
            }

            $query = $this->db->simple_select("forums", "*", "fid='{$this->bb->input['fid']}'");
            $forum_data = $this->db->fetch_array($query);
            if (!$forum_data) {
                $this->session->flash_message($this->lang->error_invalid_fid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum');
            }

            $fid = $this->bb->getInput('fid', 0);

            $this->plugins->runHooks('admin_forum_management_edit');

            if ($this->bb->request_method == "post") {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                $pid = $this->bb->getInput('pid', 0);

                if ($pid == $this->bb->input['fid']) {
                    $errors[] = $this->lang->error_forum_parent_itself;
                } else {
                    $query = $this->db->simple_select('forums', 'parentlist', "fid='{$pid}'");
                    $parents = explode(',', $this->db->fetch_field($query, 'parentlist'));
                    if (in_array($this->bb->input['fid'], $parents)) {
                        $errors[] = $this->lang->error_forum_parent_child;
                    }
                }

                $type = $this->bb->input['type'];

                if ($pid <= 0 && $type == "f") {
                    $errors[] = $this->lang->error_no_parent;
                }

                if ($type == 'c' && $forum_data['type'] == 'f') {
                    $query = $this->db->simple_select('threads', 'COUNT(tid) as num_threads', "fid = '{$fid}'");
                    if ($this->db->fetch_field($query, "num_threads") > 0) {
                        $errors[] = $this->lang->error_not_empty;
                    }
                }

                if (!empty($this->bb->input['linkto']) && empty($forum_data['linkto'])) {
                    $query = $this->db->simple_select('threads', 'COUNT(tid) as num_threads', "fid = '{$fid}'", ["limit" => 1]);
                    if ($this->db->fetch_field($query, "num_threads") > 0) {
                        $errors[] = $this->lang->error_forum_link_not_empty;
                    }
                }

                if (!$errors) {
                    if ($pid < 0) {
                        $pid = 0;
                    }
                    $update_array = [
                        "name" => $this->db->escape_string($this->bb->input['title']),
                        "description" => $this->db->escape_string($this->bb->input['description']),
                        "linkto" => $this->db->escape_string($this->bb->input['linkto']),
                        "type" => $this->db->escape_string($type),
                        "pid" => $pid,
                        "disporder" => $this->bb->getInput('disporder', 0),
                        "active" => $this->bb->getInput('active', 0),
                        "open" => $this->bb->getInput('open', 0),
                        "allowhtml" => $this->bb->getInput('allowhtml', 0),
                        "allowmycode" => $this->bb->getInput('allowmycode', 0),
                        "allowsmilies" => $this->bb->getInput('allowsmilies', 0),
                        "allowimgcode" => $this->bb->getInput('allowimgcode', 0),
                        "allowvideocode" => $this->bb->getInput('allowvideocode', 0),
                        "allowpicons" => $this->bb->getInput('allowpicons', 0),
                        "allowtratings" => $this->bb->getInput('allowtratings', 0),
                        "usepostcounts" => $this->bb->getInput('usepostcounts', 0),
                        "usethreadcounts" => $this->bb->getInput('usethreadcounts', 0),
                        "requireprefix" => $this->bb->getInput('requireprefix', 0),
                        "password" => $this->db->escape_string($this->bb->input['password']),
                        "showinjump" => $this->bb->getInput('showinjump', 0),
                        "style" => $this->bb->getInput('style', 0),
                        "overridestyle" => $this->bb->getInput('overridestyle', 0),
                        "rulestype" => $this->bb->getInput('rulestype', 0),
                        "rulestitle" => $this->db->escape_string($this->bb->input['rulestitle']),
                        "rules" => $this->db->escape_string($this->bb->input['rules']),
                        "defaultdatecut" => $this->bb->getInput('defaultdatecut', 0),
                        "defaultsortby" => $this->db->escape_string($this->bb->input['defaultsortby']),
                        "defaultsortorder" => $this->db->escape_string($this->bb->input['defaultsortorder']),
                    ];
                    $this->db->update_query("forums", $update_array, "fid='{$fid}'");
                    if ($pid != $forum_data['pid']) {
                        // Update the parentlist of this forum.
                        $this->db->update_query("forums", ["parentlist" => $this->adm->make_parent_list($fid)], "fid='{$fid}'");

                        // Rebuild the parentlist of all of the subforums of this forum
                        switch ($this->db->type) {
                            case "sqlite":
                            case "pgsql":
                                $query = $this->db->simple_select("forums", "fid", "','||parentlist||',' LIKE '%,$fid,%'");
                                break;
                            default:
                                $query = $this->db->simple_select("forums", "fid", "CONCAT(',',parentlist,',') LIKE '%,$fid,%'");
                        }

                        while ($child = $this->db->fetch_array($query)) {
                            $this->db->update_query("forums", ["parentlist" => $this->adm->make_parent_list($child['fid'])], "fid='{$child['fid']}'");
                        }
                    }

                    $this->inherit = $this->bb->getInput('default_permissions', '');

                    foreach ($this->bb->input as $id => $permission) {
                        // Make sure we're only skipping inputs that don't start with "fields_" and aren't fields_default_ or fields_inherit_
                        if (strpos($id, 'fields_') === false || (strpos($id, 'fields_default_') !== false || strpos($id, 'fields_inherit_') !== false)) {
                            continue;
                        }

                        list(, $gid) = explode('fields_', $id);

                        if ($this->bb->input['fields_default_' . $gid] == $permission && $this->bb->input['fields_inherit_' . $gid] == 1) {
                            $this->inherit[$gid] = 1;
                            continue;
                        }
                        $this->inherit[$gid] = 0;

                        // If it isn't an array then it came from the javascript form
                        if (!is_array($permission)) {
                            $permission = explode(',', $permission);
                            $permission = array_flip($permission);
                            foreach ($permission as $name => $value) {
                                $permission[$name] = 1;
                            }
                        }

                        foreach (['canview', 'canpostthreads', 'canpostreplys', 'canpostpolls'] as $name) {
                            if (in_array($name, $permission) || $permission[$name]) {
                                $permissions[$name][$gid] = 1;
                            } else {
                                $permissions[$name][$gid] = 0;
                            }
                        }
                    }

                    $this->cache->update_forums();

                    $this->canview = $permissions['canview'];
                    $this->canpostthreads = $permissions['canpostthreads'];
                    $this->canpostpolls = $permissions['canpostpolls'];
                    $canpostattachments = $permissions['canpostattachments'];
                    $this->canpostreplies = $permissions['canpostreplys'];

                    $this->save_quick_perms($fid);

                    $this->plugins->runHooks("admin_forum_management_edit_commit");

                    // Log admin action
                    $this->bblogger->log_admin_action($fid, $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_forum_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/forum?fid=' . $fid);
                }
            }

            $this->page->extra_header .= "<script src=\"{$this->bb->asset_url}/admin/jscripts/quick_perm_editor.js\" type=\"text/javascript\"></script>\n";

            $this->page->add_breadcrumb_item($this->lang->edit_forum);
            $html = $this->page->output_header($this->lang->edit_forum);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_forum_settings');

            $form = new Form($this->bb, $this->bb->admin_url . '/forum?action=edit', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('fid', $fid);

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
                $forum_data = $this->bb->input;
            } else {
                $forum_data['title'] = $forum_data['name'];
            }

            $query = $this->db->simple_select("usergroups", "*", "", ["order_dir" => "name"]);
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = $usergroup;
            }

            $query = $this->db->simple_select("forumpermissions", "*", "fid='{$fid}'");
            while ($existing = $this->db->fetch_array($query)) {
                $existing_permissions[$existing['gid']] = $existing;
            }

            $types = [
                'f' => $this->lang->forum,
                'c' => $this->lang->category
            ];

            $create_a_options_f = [
                'id' => 'forum'
            ];

            $create_a_options_c = [
                'id' => 'category'
            ];

            if ($forum_data['type'] == "f") {
                $create_a_options_f['checked'] = true;
            } else {
                $create_a_options_c['checked'] = true;
            }

            $form_container = new FormContainer($this, $this->lang->edit_forum);
            $form_container->output_row($this->lang->forum_type, $this->lang->forum_type_desc, $form->generate_radio_button('type', 'f', $this->lang->forum, $create_a_options_f) . "<br />\n" . $form->generate_radio_button('type', 'c', $this->lang->category, $create_a_options_c));
            $form_container->output_row($this->lang->title . " <em>*</em>", "", $form->generate_text_box('title', $forum_data['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->description, "", $form->generate_text_area('description', $forum_data['description'], ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->parent_forum . " <em>*</em>", $this->lang->parent_forum_desc, $form->generate_forum_select('pid', $forum_data['pid'], ['id' => 'pid', 'main_option' => $this->lang->none]), 'pid');
            $form_container->output_row($this->lang->display_order, "", $form->generate_numeric_field('disporder', $forum_data['disporder'], ['id' => 'disporder', 'min' => 0]), 'disporder');
            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->additional_forum_options);
            $form_container->output_row($this->lang->forum_link, $this->lang->forum_link_desc, $form->generate_text_box('linkto', $forum_data['linkto'], ['id' => 'linkto']), 'linkto');
            $form_container->output_row($this->lang->forum_password, $this->lang->forum_password_desc, $form->generate_text_box('password', $forum_data['password'], ['id' => 'password']), 'password');

            $access_options = [
                $form->generate_check_box('active', 1, $this->lang->forum_is_active . "<br />\n<small>{$this->lang->forum_is_active_desc}</small>", ['checked' => $forum_data['active'], 'id' => 'active']),
                $form->generate_check_box('open', 1, $this->lang->forum_is_open . "<br />\n<small>{$this->lang->forum_is_open_desc}</small>", ['checked' => $forum_data['open'], 'id' => 'open'])
            ];

            $form_container->output_row($this->lang->access_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $access_options) . "</div>");

            $styles = [
                '0' => $this->lang->use_default
            ];

            $query = $this->db->simple_select("themes", "tid,name", "name!='((master))' AND name!='((master-backup))'", ['order_by' => 'name']);
            while ($style = $this->db->fetch_array($query)) {
                $styles[$style['tid']] = $style['name'];
            }

            $style_options = [
                $form->generate_check_box('overridestyle', 1, $this->lang->override_user_style, ['checked' => $forum_data['overridestyle'], 'id' => 'overridestyle']),
                $this->lang->forum_specific_style . "<br />\n" . $form->generate_select_box('style', $styles, $forum_data['style'], ['id' => 'style'])
            ];

            $form_container->output_row($this->lang->style_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $style_options) . "</div>");

            $display_methods = [
                '0' => $this->lang->dont_display_rules,
                '1' => $this->lang->display_rules_inline,
                '3' => $this->lang->display_rules_inline_new,
                '2' => $this->lang->display_rules_link
            ];

            $forum_rules = [
                $this->lang->display_method . "<br />\n" . $form->generate_select_box('rulestype', $display_methods, $forum_data['rulestype'], ['checked' => $forum_data['rulestype'], 'id' => 'rulestype']),
                $this->lang->title . "<br />\n" . $form->generate_text_box('rulestitle', $forum_data['rulestitle'], ['checked' => $forum_data['rulestitle'], 'id' => 'rulestitle']),
                $this->lang->rules . "<br />\n" . $form->generate_text_area('rules', $forum_data['rules'], ['checked' => $forum_data['rules'], 'id' => 'rules'])
            ];

            $form_container->output_row($this->lang->forum_rules, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $forum_rules) . "</div>");

            $default_date_cut = [
                0 => $this->lang->board_default,
                1 => $this->lang->datelimit_1day,
                5 => $this->lang->datelimit_5days,
                10 => $this->lang->datelimit_10days,
                20 => $this->lang->datelimit_20days,
                50 => $this->lang->datelimit_50days,
                75 => $this->lang->datelimit_75days,
                100 => $this->lang->datelimit_100days,
                365 => $this->lang->datelimit_lastyear,
                9999 => $this->lang->datelimit_beginning,
            ];

            $default_sort_by = [
                "" => $this->lang->board_default,
                "subject" => $this->lang->sort_by_subject,
                "lastpost" => $this->lang->sort_by_lastpost,
                "starter" => $this->lang->sort_by_starter,
                "started" => $this->lang->sort_by_started,
                "rating" => $this->lang->sort_by_rating,
                "replies" => $this->lang->sort_by_replies,
                "views" => $this->lang->sort_by_views,
            ];

            $default_sort_order = [
                "" => $this->lang->board_default,
                "asc" => $this->lang->sort_order_asc,
                "desc" => $this->lang->sort_order_desc,
            ];

            $view_options = [
                $this->lang->default_date_cut . "<br />\n" . $form->generate_select_box('defaultdatecut', $default_date_cut, $forum_data['defaultdatecut'], ['checked' => $forum_data['defaultdatecut'], 'id' => 'defaultdatecut']),
                $this->lang->default_sort_by . "<br />\n" . $form->generate_select_box('defaultsortby', $default_sort_by, $forum_data['defaultsortby'], ['checked' => $forum_data['defaultsortby'], 'id' => 'defaultsortby']),
                $this->lang->default_sort_order . "<br />\n" . $form->generate_select_box('defaultsortorder', $default_sort_order, $forum_data['defaultsortorder'], ['checked' => $forum_data['defaultsortorder'], 'id' => 'defaultsortorder']),
            ];

            $form_container->output_row($this->lang->default_view_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $view_options) . "</div>");

            $misc_options = [
                $form->generate_check_box('allowhtml', 1, $this->lang->allow_html, ['checked' => $forum_data['allowhtml'], 'id' => 'allowhtml']),
                $form->generate_check_box('allowmycode', 1, $this->lang->allow_mycode, ['checked' => $forum_data['allowmycode'], 'id' => 'allowmycode']),
                $form->generate_check_box('allowsmilies', 1, $this->lang->allow_smilies, ['checked' => $forum_data['allowsmilies'], 'id' => 'allowsmilies']),
                $form->generate_check_box('allowimgcode', 1, $this->lang->allow_img_code, ['checked' => $forum_data['allowimgcode'], 'id' => 'allowimgcode']),
                $form->generate_check_box('allowvideocode', 1, $this->lang->allow_video_code, ['checked' => $forum_data['allowvideocode'], 'id' => 'allowvideocode']),
                $form->generate_check_box('allowpicons', 1, $this->lang->allow_post_icons, ['checked' => $forum_data['allowpicons'], 'id' => 'allowpicons']),
                $form->generate_check_box('allowtratings', 1, $this->lang->allow_thread_ratings, ['checked' => $forum_data['allowtratings'], 'id' => 'allowtratings']),
                $form->generate_check_box('showinjump', 1, $this->lang->show_forum_jump, ['checked' => $forum_data['showinjump'], 'id' => 'showinjump']),
                $form->generate_check_box('usepostcounts', 1, $this->lang->use_postcounts, ['checked' => $forum_data['usepostcounts'], 'id' => 'usepostcounts']),
                $form->generate_check_box('usethreadcounts', 1, $this->lang->use_threadcounts, ['checked' => $forum_data['usethreadcounts'], 'id' => 'usethreadcounts']),
                $form->generate_check_box('requireprefix', 1, $this->lang->require_thread_prefix, ['checked' => $forum_data['requireprefix'], 'id' => 'requireprefix'])
            ];

            $form_container->output_row($this->lang->misc_options, "", "<div class=\"forum_settings_bit\">" . implode("</div><div class=\"forum_settings_bit\">", $misc_options) . "</div>");
            $html .= $form_container->end();

            $cached_forum_perms = $this->cache->read("forumpermissions");
            $field_list = [
                'canview' => $this->lang->permissions_canview,
                'canpostthreads' => $this->lang->permissions_canpostthreads,
                'canpostreplys' => $this->lang->permissions_canpostreplys,
                'canpostpolls' => $this->lang->permissions_canpostpolls,
            ];

            $field_list2 = [
                'canview' => $this->lang->perm_drag_canview,
                'canpostthreads' => $this->lang->perm_drag_canpostthreads,
                'canpostreplys' => $this->lang->perm_drag_canpostreplys,
                'canpostpolls' => $this->lang->perm_drag_canpostpolls,
            ];

            $ids = [];

            $form_container = new FormContainer($this, $this->lang->sprintf($this->lang->forum_permissions_in, $forum_data['name']));
            $form_container->output_row_header($this->lang->permissions_group, ["class" => "align_center", 'style' => 'width: 30%']);
            $form_container->output_row_header($this->lang->overview_allowed_actions, ["class" => "align_center"]);
            $form_container->output_row_header($this->lang->overview_disallowed_actions, ["class" => "align_center"]);
            $form_container->output_row_header($this->lang->controls, ["class" => "align_center", 'style' => 'width: 120px', 'colspan' => 2]);

            if ($this->bb->request_method == "post") {
                foreach ($usergroups as $usergroup) {
                    if (isset($this->bb->input['fields_' . $usergroup['gid']])) {
                        $input_permissions = $this->bb->input['fields_' . $usergroup['gid']];
                        if (!is_array($input_permissions)) {
                            // Convering the comma separated list from Javascript form into a variable
                            $input_permissions = explode(',', $input_permissions);
                        }
                        foreach ($input_permissions as $input_permission) {
                            $this->bb->input['permissions'][$usergroup['gid']][$input_permission] = 1;
                        }
                    }
                }
            }

            foreach ($usergroups as $usergroup) {
                $perms = [];
                if (isset($this->bb->input['default_permissions'])) {
                    if ($this->bb->input['default_permissions'][$usergroup['gid']]) {
                        if (is_array($existing_permissions) && $existing_permissions[$usergroup['gid']]) {
                            $perms = $existing_permissions[$usergroup['gid']];
                            $default_checked = false;
                        } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum_data['fid']][$usergroup['gid']]) {
                            $perms = $cached_forum_perms[$forum_data['fid']][$usergroup['gid']];
                            $default_checked = true;
                        } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum_data['pid']][$usergroup['gid']]) {
                            $perms = $cached_forum_perms[$forum_data['pid']][$usergroup['gid']];
                            $default_checked = true;
                        }
                    }

                    if (!$perms) {
                        $perms = $usergroup;
                        $default_checked = true;
                    }
                } else {
                    if (isset($existing_permissions) && isset($existing_permissions[$usergroup['gid']])) {
                        $perms = $existing_permissions[$usergroup['gid']];
                        $default_checked = false;
                    } elseif (is_array($cached_forum_perms) && isset($cached_forum_perms[$forum_data['fid']][$usergroup['gid']])) {
                        $perms = $cached_forum_perms[$forum_data['fid']][$usergroup['gid']];
                        $default_checked = true;
                    } elseif (is_array($cached_forum_perms) && isset($cached_forum_perms[$forum_data['pid']][$usergroup['gid']])) {
                        $perms = $cached_forum_perms[$forum_data['pid']][$usergroup['gid']];
                        $default_checked = true;
                    }

                    if (!$perms) {
                        $perms = $usergroup;
                        $default_checked = true;
                    }
                }

                foreach ($field_list as $forum_permission => $forum_perm_title) {
                    if (isset($this->bb->input['permissions'])) {
                        if ($this->bb->input['permissions'][$usergroup['gid']][$forum_permission]) {
                            $perms_checked[$forum_permission] = 1;
                        } else {
                            $perms_checked[$forum_permission] = 0;
                        }
                    } else {
                        if ($perms[$forum_permission] == 1) {
                            $perms_checked[$forum_permission] = 1;
                        } else {
                            $perms_checked[$forum_permission] = 0;
                        }
                    }
                }
                $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);

                if ($default_checked) {
                    $inherited_text = $this->lang->inherited_permission;
                } else {
                    $inherited_text = $this->lang->custom_permission;
                }

                $form_container->output_cell("<strong>{$usergroup['title']}</strong> <small style=\"vertical-align: middle;\">({$inherited_text})</small>");

                $field_select = "<div class=\"quick_perm_fields\">\n";
                $field_select .= "<div class=\"enabled\"><div class=\"fields_title\">{$this->lang->alt_enabled}</div><ul id=\"fields_enabled_{$usergroup['gid']}\">\n";
                foreach ($perms_checked as $perm => $value) {
                    if ($value == 1) {
                        $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
                    }
                }
                $field_select .= "</ul></div>\n";

                $field_select .= "<div class=\"disabled\"><div class=\"fields_title\">{$this->lang->alt_disabled}</div><ul id=\"fields_disabled_{$usergroup['gid']}\">\n";
                foreach ($perms_checked as $perm => $value) {
                    if ($value == 0) {
                        $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
                    }
                }
                $field_select .= "</ul></div></div>\n";
                $field_select .= $form->generate_hidden_field("fields_" . $usergroup['gid'], @implode(",", @array_keys($perms_checked, '1')), ['id' => 'fields_' . $usergroup['gid']]);
                $field_select .= $form->generate_hidden_field("fields_inherit_" . $usergroup['gid'], (int)$default_checked, ['id' => 'fields_inherit_' . $usergroup['gid']]);
                $field_select .= $form->generate_hidden_field("fields_default_" . $usergroup['gid'], @implode(",", @array_keys($perms_checked, '1')), ['id' => 'fields_default_' . $usergroup['gid']]);
                $field_select = str_replace("'", "\\'", $field_select);
                $field_select = str_replace("\n", "", $field_select);

                $field_select = "<script type=\"text/javascript\">
//<![CDATA[
document.write('" . str_replace("/", "\/", $field_select) . "');
//]]>
</script>\n";

                $field_selected = [];
                foreach ($field_list as $forum_permission => $permission_title) {
                    $field_options[$forum_permission] = $permission_title;
                    if ($perms_checked[$forum_permission]) {
                        $field_selected[] = $forum_permission;
                    }
                }

                $field_select .= "<noscript>" . $form->generate_select_box('fields_' . $usergroup['gid'] . '[]', $field_options, $field_selected, ['id' => 'fields_' . $usergroup['gid'] . '[]', 'multiple' => true]) . "</noscript>\n";
                $form_container->output_cell($field_select, ['colspan' => 2]);

                if (!$default_checked) {
                    // FIXME rebuild modal
//                    $form_container->output_cell("<a href=\"{$this->bb->settings['bburl']}/admin/forum?action=permissions&amp;pid={$perms['pid']}\" onclick=\"MyBB.popupWindow('{$this->bb->settings['bburl']}/admin/forum?action=permissions&pid={$perms['pid']}&ajax=1', null, true); return false;\">{$this->lang->edit_permissions}</a>", array("class" => "align_center"));
                    $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=permissions&amp;pid={$perms['pid']}\">{$this->lang->edit_permissions}</a>", ["class" => "align_center"]);
                    $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=clear_permission&amp;pid={$perms['pid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_clear_custom_permission}')\">{$this->lang->clear_custom_perms}</a>", ["class" => "align_center"]);
                } else {
//                    $form_container->output_cell("<a href=\"{$this->bb->settings['bburl']}/admin/forum?action=permissions&amp;gid={$usergroup['gid']}&amp;fid={$fid}\" onclick=\"MyBB.popupWindow('{$this->bb->settings['bburl']}/admin/forum?action=permissions&gid={$usergroup['gid']}&fid={$fid}&ajax=1', null, true); return false;\">{$this->lang->set_custom_perms}</a>", array("class" => "align_center", "colspan" => 2));
                    $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=permissions&amp;gid={$usergroup['gid']}&amp;fid={$fid}\">{$this->lang->set_custom_perms}</a>", ["class" => "align_center", "colspan" => 2]);
                }

                $form_container->construct_row(['id' => 'row_' . $usergroup['gid']]);

                $ids[] = $usergroup['gid'];
            }
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_forum);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            // Write in our JS based field selector
            $html .= "<script type=\"text/javascript\">\n<!--\n";
            foreach ($ids as $id) {
                $html .= "$(function() { QuickPermEditor.init(" . $id . "); });\n";
            }
            $html .= "// -->\n</script>\n";

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/ManagementEdit.html.twig');
        }

        if ($this->bb->input['action'] == "deletemod") {
            $modid = $this->bb->getInput('id', 0);
            $isgroup = $this->bb->getInput('isgroup', 0);
            $fid = $this->bb->getInput('fid', 0);

            $query = $this->db->simple_select("moderators", "*", "id='{$modid}' AND isgroup = '{$isgroup}' AND fid='{$fid}'");
            $mod = $this->db->fetch_array($query);

            // Does the forum not exist?
            if (!$mod['mid']) {
                $this->session->flash_message($this->lang->error_invalid_moderator, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum?fid=' . $fid);
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/forum?fid=' . $fid);
            }

            $this->plugins->runHooks("admin_forum_management_deletemod");

            if ($this->bb->request_method == "post") {
                $mid = $mod['mid'];
                if ($this->bb->input['isgroup']) {
                    $query = $this->db->query("
				SELECT m.*, g.title
				FROM " . TABLE_PREFIX . "moderators m
				LEFT JOIN " . TABLE_PREFIX . "usergroups g ON (g.gid=m.id)
				WHERE m.mid='{$mid}'
			");
                } else {
                    $query = $this->db->query("
				SELECT m.*, u.username, u.usergroup
				FROM " . TABLE_PREFIX . "moderators m
				LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=m.id)
				WHERE m.mid='{$mid}'
			");
                }
                $mod = $this->db->fetch_array($query);

                $this->db->delete_query("moderators", "mid='{$mid}'");

                $this->plugins->runHooks("admin_forum_management_deletemod_commit");

                $this->cache->update_moderators();

                $forum = $this->forum->get_forum($fid);

                // Log admin action
                if ($isgroup) {
                    $this->bblogger->log_admin_action($mid, $mod['title'], $forum['fid'], $forum['name']);
                } else {
                    $this->bblogger->log_admin_action($mid, $mod['username'], $forum['fid'], $forum['name']);
                }

                $this->session->flash_message($this->lang->success_moderator_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_moderators");
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . "/forum?action=deletemod&amp;fid={$mod['fid']}&amp;uid={$mod['uid']}", $this->lang->confirm_moderator_deletion);
            }
        }

        if ($this->bb->input['action'] == "delete") {
            $query = $this->db->simple_select("forums", "*", "fid='{$this->bb->input['fid']}'");
            $forum = $this->db->fetch_array($query);

            // Does the forum not exist?
            if (!$forum['fid']) {
                $this->session->flash_message($this->lang->error_invalid_forum, 'error');
                return $response->withRedirect($this->bb->admin_url . '/forum');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/forum');
            }

            $this->plugins->runHooks("admin_forum_management_delete");

            if ($this->bb->request_method == "post") {
                $fid = $this->bb->getInput('fid', 0);
                $forum_info = $this->forum->get_forum($fid);

                $query = $this->db->simple_select("forums", "posts,unapprovedposts,threads,unapprovedthreads", "fid='{$fid}'");
                $stats = $this->db->fetch_array($query);

                // Delete the forum
                $this->db->delete_query("forums", "fid='$fid'");
                $delquery = '';
                switch ($this->db->type) {
                    case "pgsql":
                    case "sqlite":
                        $query = $this->db->simple_select("forums", "*", "','|| parentlist|| ',' LIKE '%,$fid,%'");
                        break;
                    default:
                        $query = $this->db->simple_select("forums", "*", "CONCAT(',', parentlist, ',') LIKE '%,$fid,%'");
                }
                while ($forum = $this->db->fetch_array($query)) {
                    $fids[$forum['fid']] = $fid;
                    $delquery .= " OR fid='{$forum['fid']}'";

                    $stats['posts'] += $forum['posts'];
                    $stats['unapprovedposts'] += $forum['unapprovedposts'];
                    $stats['threads'] += $forum['threads'];
                    $stats['unapprovedthreads'] += $forum['unapprovedthreads'];
                }

                switch ($this->db->type) {
                    case "pgsql":
                    case "sqlite":
                        $this->db->delete_query("forums", "','||parentlist||',' LIKE '%,$fid,%'");
                        break;
                    default:
                        $this->db->delete_query("forums", "CONCAT(',',parentlist,',') LIKE '%,$fid,%'");
                }

                $this->db->delete_query("threads", "fid='{$fid}' {$delquery}");
                $this->db->delete_query("posts", "fid='{$fid}' {$delquery}");
                $this->db->delete_query("moderators", "fid='{$fid}' {$delquery}");
                $this->db->delete_query("forumsubscriptions", "fid='{$fid}' {$delquery}");
                $this->db->delete_query("forumpermissions", "fid='{$fid}' {$delquery}");

                $update_stats = [
                    'numthreads' => "-" . $stats['threads'],
                    'numunapprovedthreads' => "-" . $stats['unapprovedthreads'],
                    'numposts' => "-" . $stats['posts'],
                    'numunapprovedposts' => "-" . $stats['unapprovedposts']
                ];
                $this->cache->update_stats($update_stats);

                $this->plugins->runHooks("admin_forum_management_delete_commit");

                $this->cache->update_forums();
                $this->cache->update_moderators();
                $this->cache->update_forumpermissions();

                // Log admin action
                $this->bblogger->log_admin_action($forum_info['fid'], $forum_info['name']);

                $this->session->flash_message($this->lang->success_forum_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/forum');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . "/forum?action=delete&amp;fid={$forum['fid']}", $this->lang->confirm_forum_deletion);
            }
        }

        if (!$this->bb->input['action'] || $this->bb->input['action'] = 'management') {
            if (!isset($this->bb->input['fid'])) {
                $this->bb->input['fid'] = 0;
            }

            $fid = $this->bb->getInput('fid', 0);
            if ($fid) {
                $forum = $this->forum->get_forum($fid);
            }

            $this->plugins->runHooks('admin_forum_management_start');

            if ($this->bb->request_method == 'post') {
                if (isset($this->bb->input['update']) && $this->bb->input['update'] == 'permissions') {
                    $this->inherit = [];
                    foreach ($this->bb->input as $id => $permission) {
                        // Make sure we're only skipping inputs that don't start with "fields_" and aren't fields_default_ or fields_inherit_
                        if (strpos($id, 'fields_') === false || (strpos($id, 'fields_default_') !== false || strpos($id, 'fields_inherit_') !== false)) {
                            continue;
                        }

                        list(, $gid) = explode('fields_', $id);

                        if ($this->bb->input['fields_default_' . $gid] == $permission && $this->bb->input['fields_inherit_' . $gid] == 1) {
                            $this->inherit[$gid] = 1;
                            continue;
                        }
                        $this->inherit[$gid] = 0;

                        // If it isn't an array then it came from the javascript form
                        if (!is_array($permission)) {
                            $permission = explode(',', $permission);
                            $permission = array_flip($permission);
                            foreach ($permission as $name => $value) {
                                $permission[$name] = 1;
                            }
                        }

                        foreach (['canview', 'canpostthreads', 'canpostreplys', 'canpostpolls', 'canpostattachments'] as $name) {
                            if (isset($permission[$name])) {
                                $permissions[$name][$gid] = 1;
                            } else {
                                $permissions[$name][$gid] = 0;
                            }
                        }
                    }

                    $this->canview = $permissions['canview'];
                    $this->canpostthreads = $permissions['canpostthreads'];
                    $this->canpostpolls = $permissions['canpostpolls'];
                    $canpostattachments = $permissions['canpostattachments'];
                    $this->canpostreplies = $permissions['canpostreplys'];

                    $this->save_quick_perms($fid);

                    $this->plugins->runHooks("admin_forum_management_start_permissions_commit");

                    $this->cache->update_forums();

                    // Log admin action
                    $this->bblogger->log_admin_action('quickpermissions', $fid, $forum['name']);

                    $this->session->flash_message($this->lang->success_forum_permissions_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_permissions");
                } elseif (isset($this->bb->input['add']) && $this->bb->input['add'] == "moderators") {
                    $forum = $this->forum->get_forum($fid);
                    if (!$forum) {
                        $this->session->flash_message($this->lang->error_invalid_forum, 'error');
                        return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_moderators");
                    }
                    if (!empty($this->bb->input['usergroup'])) {
                        $isgroup = 1;
                        $gid = $this->bb->getInput('usergroup', 0);

                        if (!$this->bb->groupscache[$gid]) {
                            // Didn't select a valid moderator
                            $this->session->flash_message($this->lang->error_moderator_not_found, 'error');
                            return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_moderators");
                        }

                        $newmod = [
                            'id' => $gid,
                            'name' => $this->bb->groupscache[$gid]['title']
                        ];
                    } else {
                        $options = [
                            'fields' => ['uid AS id', 'username AS name']
                        ];
                        $newmod = $this->user->get_user_by_username($this->bb->input['username'], $options);

                        if (empty($newmod['id'])) {
                            $this->session->flash_message($this->lang->error_moderator_not_found, 'error');
                            return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_moderators");
                        }

                        $isgroup = 0;
                    }

                    if ($newmod['id']) {
                        $query = $this->db->simple_select("moderators", "id", "id='" . $newmod['id'] . "' AND fid='" . $fid . "' AND isgroup='{$isgroup}'", ['limit' => 1]);

                        if (!$this->db->num_rows($query)) {
                            $new_mod = [
                                'fid' => $fid,
                                'id' => $newmod['id'],
                                'isgroup' => $isgroup,
                                'caneditposts' => 1,
                                'cansoftdeleteposts' => 1,
                                'canrestoreposts' => 1,
                                'candeleteposts' => 1,
                                'cansoftdeletethreads' => 1,
                                'canrestorethreads' => 1,
                                'candeletethreads' => 1,
                                'canviewips' => 1,
                                'canviewunapprove' => 1,
                                'canviewdeleted' => 1,
                                'canopenclosethreads' => 1,
                                'canstickunstickthreads' => 1,
                                'canapproveunapprovethreads' => 1,
                                'canapproveunapproveposts' => 1,
                                'canapproveunapproveattachs' => 1,
                                'canmanagethreads' => 1,
                                'canmanagepolls' => 1,
                                'canpostclosedthreads' => 1,
                                'canmovetononmodforum' => 1,
                                'canusecustomtools' => 1,
                                'canmanageannouncements' => 1,
                                'canmanagereportedposts' => 1,
                                'canviewmodlog' => 1
                            ];

                            $mid = $this->db->insert_query('moderators', $new_mod);

                            if (!$isgroup) {
                                $this->db->update_query('users', ['usergroup' => 6], "uid='{$newmod['id']}' AND usergroup='2'");
                            }

                            $this->plugins->runHooks('admin_forum_management_start_moderators_commit');

                            $this->cache->update_moderators();

                            // Log admin action
                            $this->bblogger->log_admin_action('addmod', $mid, $newmod['name'], $fid, $forum['name']);

                            $this->session->flash_message($this->lang->success_moderator_added, 'success');
                            return $response->withRedirect($this->bb->admin_url . '/forum?action=editmod&mid='.$mid);
                        } else {
                            $this->session->flash_message($this->lang->error_moderator_already_added, 'error');
                            return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_moderators");
                        }
                    } else {
                        $this->session->flash_message($this->lang->error_moderator_not_found, 'error');
                        return $response->withRedirect($this->bb->admin_url . "/forum?fid={$fid}#tab_moderators");
                    }
                } else {
                    if (!empty($this->bb->input['disporder']) && is_array($this->bb->input['disporder'])) {
                        foreach ($this->bb->input['disporder'] as $update_fid => $order) {
                            $this->db->update_query("forums", ['disporder' => (int)$order], "fid='" . (int)$update_fid . "'");
                        }

                        $this->plugins->runHooks("admin_forum_management_start_disporder_commit");

                        $this->cache->update_forums();

                        // Log admin action
                        $this->bblogger->log_admin_action('orders', $forum['fid'], $forum['name']);

                        $this->session->flash_message($this->lang->success_forum_disporder_updated, 'success');
                        return $response->withRedirect($this->bb->admin_url . "/forum?fid=" . $this->bb->input['fid']);
                    }
                }
            }

            $this->lang->load('users_admin_permissions', false, true);

            $this->page->extra_header .= "<script src=\"{$this->bb->asset_url}/admin/jscripts/quick_perm_editor.js\" type=\"text/javascript\"></script>\n";

            if ($fid) {
                $this->page->add_breadcrumb_item($this->lang->view_forum, $this->bb->admin_url . '/forum');
            }

            $html = $this->page->output_header($this->lang->forum_management);

            if ($fid) {
                $html .= $this->page->output_nav_tabs($sub_tabs, 'view_forum');
            } else {
                $html .= $this->page->output_nav_tabs($sub_tabs, 'forum_management');
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/forum', 'post', 'management');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('fid', $this->bb->input['fid']);

            if ($fid) {
                $tabs = [
                    'subforums' => $this->lang->subforums,
                    'permissions' => $this->lang->forum_permissions,
                    'moderators' => $this->lang->moderators,
                ];
                $tabs = $this->plugins->runHooks("admin_forum_management_start_graph_tabs", $tabs);
                $html .= $this->page->output_tab_control($tabs);

                $html .= "<div id=\"tab_subforums\">\n";
                if (!isset($this->bb->forum_cache) || empty($this->bb->forum_cache)) {
                    $this->forum->cache_forums();
                }
                $form_container = new FormContainer($this, $this->lang->sprintf($this->lang->in_forums, $this->bb->forum_cache[$fid]['name']));
            } else {
                $form_container = new FormContainer($this, $this->lang->manage_forums);
            }
            $form_container->output_row_header($this->lang->forum);
            $form_container->output_row_header($this->lang->order, ["class" => "align_center", 'width' => '5%']);
            $form_container->output_row_header($this->lang->controls, ["class" => "align_center", 'style' => 'width: 200px']);

            $this->build_admincp_forums_list($form_container, $fid);

            $submit_options = [];

            if ($form_container->num_rows() == 0) {
                $form_container->output_cell($this->lang->no_forums, ['colspan' => 3]);
                $form_container->construct_row();
                $submit_options = ['disabled' => true];
            }

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->update_forum_orders, $submit_options);
            $buttons[] = $form->generate_reset_button($this->lang->reset);

            $html .= $form->output_submit_wrapper($buttons);

            if (!$fid) {
                $html .= $form->end();
            }

            if ($fid) {
                $html .= "</div>\n";
                $html .= $form->end();

                $query = $this->db->simple_select('usergroups', '*', '', ['order' => 'name']);
                while ($usergroup = $this->db->fetch_array($query)) {
                    $usergroups[$usergroup['gid']] = $usergroup;
                }

                $query = $this->db->simple_select("forumpermissions", "*", "fid='{$fid}'");
                while ($existing = $this->db->fetch_array($query)) {
                    $existing_permissions[$existing['gid']] = $existing;
                }

                $cached_forum_perms = $this->cache->read("forumpermissions");
                $field_list = [
                    'canview' => $this->lang->permissions_canview,
                    'canpostthreads' => $this->lang->permissions_canpostthreads,
                    'canpostreplys' => $this->lang->permissions_canpostreplys,
                    'canpostpolls' => $this->lang->permissions_canpostpolls,
                ];

                $field_list2 = [
                    'canview' => $this->lang->perm_drag_canview,
                    'canpostthreads' => $this->lang->perm_drag_canpostthreads,
                    'canpostreplys' => $this->lang->perm_drag_canpostreplys,
                    'canpostpolls' => $this->lang->perm_drag_canpostpolls,
                ];

                $ids = [];

                $form = new Form($this->bb, $this->bb->admin_url . '/forum', 'post', 'management');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field("fid", $this->bb->input['fid']);
                $html .= $form->generate_hidden_field("update", "permissions");

                $html .= "<div id=\"tab_permissions\">\n";

                $form_container = new FormContainer($this, $this->lang->sprintf($this->lang->forum_permissions_in, $this->bb->forum_cache[$fid]['name']));
                $form_container->output_row_header($this->lang->permissions_group, ["class" => "align_center", 'style' => 'width: 30%']);
                $form_container->output_row_header($this->lang->overview_allowed_actions, ["class" => "align_center"]);
                $form_container->output_row_header($this->lang->overview_disallowed_actions, ["class" => "align_center"]);
                $form_container->output_row_header($this->lang->controls, ["class" => "align_center", 'style' => 'width: 120px', 'colspan' => 2]);
                foreach ($usergroups as $usergroup) {
                    $perms = [];
                    if (isset($this->bb->input['default_permissions'])) {
                        if ($this->bb->input['default_permissions'][$usergroup['gid']]) {
                            if (is_array($existing_permissions) && $existing_permissions[$usergroup['gid']]) {
                                $perms = $existing_permissions[$usergroup['gid']];
                                $default_checked = false;
                            } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum['fid']][$usergroup['gid']]) {
                                $perms = $cached_forum_perms[$forum['fid']][$usergroup['gid']];
                                $default_checked = true;
                            } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum['pid']][$usergroup['gid']]) {
                                $perms = $cached_forum_perms[$forum['pid']][$usergroup['gid']];
                                $default_checked = true;
                            }
                        }

                        if (!$perms) {
                            $perms = $usergroup;
                            $default_checked = true;
                        }
                    } else {
                        if (isset($existing_permissions) &&
                            is_array($existing_permissions) &&
                            isset($existing_permissions[$usergroup['gid']])
                        ) {
                            $perms = $existing_permissions[$usergroup['gid']];
                            $default_checked = false;
                        } elseif (is_array($cached_forum_perms) &&
                            isset($cached_forum_perms[$forum['fid']]) &&
                            isset($cached_forum_perms[$forum['fid']][$usergroup['gid']])
                        ) {
                            $perms = $cached_forum_perms[$forum['fid']][$usergroup['gid']];
                            $default_checked = true;
                        } elseif (is_array($cached_forum_perms) &&
                            isset($cached_forum_perms[$forum['pid']][$usergroup['gid']])
                        ) {
                            $perms = $cached_forum_perms[$forum['pid']][$usergroup['gid']];
                            $default_checked = true;
                        }

                        if (!$perms) {
                            $perms = $usergroup;
                            $default_checked = true;
                        }
                    }
                    foreach ($field_list as $forum_permission => $forum_perm_title) {
                        if (isset($this->bb->input['permissions'])) {
                            if ($this->bb->input['permissions'][$usergroup['gid']][$forum_permission]) {
                                $perms_checked[$forum_permission] = 1;
                            } else {
                                $perms_checked[$forum_permission] = 0;
                            }
                        } else {
                            if ($perms[$forum_permission] == 1) {
                                $perms_checked[$forum_permission] = 1;
                            } else {
                                $perms_checked[$forum_permission] = 0;
                            }
                        }
                    }
                    $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);

                    if ($default_checked == 1) {
                        $inherited_text = $this->lang->inherited_permission;
                    } else {
                        $inherited_text = $this->lang->custom_permission;
                    }

                    $form_container->output_cell("<strong>{$usergroup['title']}</strong> <small style=\"vertical-align: middle;\">({$inherited_text})</small>");

                    $field_select = "<div class=\"quick_perm_fields\">\n";
                    $field_select .= "<div class=\"enabled\"><ul id=\"fields_enabled_{$usergroup['gid']}\">\n";
                    foreach ($perms_checked as $perm => $value) {
                        if ($value == 1) {
                            $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
                        }
                    }
                    $field_select .= "</ul></div>\n";
                    $field_select .= "<div class=\"disabled\"><ul id=\"fields_disabled_{$usergroup['gid']}\">\n";
                    foreach ($perms_checked as $perm => $value) {
                        if ($value == 0) {
                            $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
                        }
                    }
                    $field_select .= "</ul></div></div>\n";
                    $field_select .= $form->generate_hidden_field("fields_" . $usergroup['gid'], @implode(",", @array_keys($perms_checked, '1')), ['id' => 'fields_' . $usergroup['gid']]);
                    $field_select .= $form->generate_hidden_field("fields_inherit_" . $usergroup['gid'], (int)$default_checked, ['id' => 'fields_inherit_' . $usergroup['gid']]);
                    $field_select .= $form->generate_hidden_field("fields_default_" . $usergroup['gid'], @implode(",", @array_keys($perms_checked, '1')), ['id' => 'fields_default_' . $usergroup['gid']]);
                    $field_select = str_replace("'", "\\'", $field_select);
                    $field_select = str_replace("\n", "", $field_select);

                    $field_select = "<script type=\"text/javascript\">
//<![CDATA[
document.write('" . str_replace("/", "\/", $field_select) . "');
//]]>
</script>\n";

                    $field_selected = [];
                    foreach ($field_list as $forum_permission => $permission_title) {
                        $field_options[$forum_permission] = $permission_title;
                        if ($perms_checked[$forum_permission]) {
                            $field_selected[] = $forum_permission;
                        }
                    }

                    $field_select .= "<noscript>" . $form->generate_select_box('fields_' . $usergroup['gid'] . '[]', $field_options, $field_selected, ['id' => 'fields_' . $usergroup['gid'] . '[]', 'multiple' => true]) . "</noscript>\n";
                    $form_container->output_cell($field_select, ['colspan' => 2]);

                    if (!$default_checked) {
//                        $form_container->output_cell("<a href=\"{$this->bb->settings['bburl']}/admin/forum?action=permissions&amp;pid={$perms['pid']}\" onclick=\"MyBB.popupWindow('{$this->bb->settings['bburl']}/admin/forum?action=permissions&pid={$perms['pid']}&ajax=1', null, true); return false;\">{$this->lang->edit_permissions}</a>", array("class" => "align_center"));
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=permissions&amp;pid={$perms['pid']}\">{$this->lang->edit_permissions}</a>", ["class" => "align_center"]);
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=clear_permission&amp;pid={$perms['pid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_clear_custom_permission}')\">{$this->lang->clear_custom_perms}</a>", ["class" => "align_center"]);
                    } else {
//                        $form_container->output_cell("<a href=\"{$this->bb->settings['bburl']}/admin/forum?action=permissions&amp;gid={$usergroup['gid']}&amp;fid={$fid}\" onclick=\"MyBB.popupWindow('{$this->bb->settings['bburl']}/admin/forum?action=permissions&gid={$usergroup['gid']}&fid={$fid}&ajax=1', null, true); return false;\">{$this->lang->set_custom_perms}</a>", array("class" => "align_center", "colspan" => 2));
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=permissions&amp;gid={$usergroup['gid']}&amp;fid={$fid}\">{$this->lang->set_custom_perms}</a>", ["class" => "align_center", "colspan" => 2]);
                    }
                    $form_container->construct_row(['id' => 'row_' . $usergroup['gid']]);

                    $ids[] = $usergroup['gid'];
                }
                $html .= $form_container->end();

                $buttons = [];
                $buttons[] = $form->generate_submit_button($this->lang->update_forum_permissions);
                $buttons[] = $form->generate_reset_button($this->lang->reset);

                $html .= $form->output_submit_wrapper($buttons);

                // Write in our JS based field selector
                $html .= "<script type=\"text/javascript\">\n<!--\n";
                foreach ($ids as $id) {
                    $html .= "$(function() { QuickPermEditor.init(" . $id . ") });\n";
                }
                $html .= "// -->\n</script>\n";

                $html .= "</div>\n";
                $html .= $form->end();
                $html .= "<div id=\"tab_moderators\">\n";
                $form_container = new FormContainer($this, $this->lang->sprintf($this->lang->moderators_assigned_to, $this->bb->forum_cache[$fid]['name']));
                $form_container->output_row_header($this->lang->name, ['width' => '75%']);
                $form_container->output_row_header($this->lang->controls, ["class" => "align_center", 'style' => 'width: 200px', 'colspan' => 2]);
                $query = $this->db->query("
			SELECT m.mid, m.id, m.isgroup, u.username, g.title
			FROM " . TABLE_PREFIX . "moderators m
			LEFT JOIN " . TABLE_PREFIX . "users u ON (m.isgroup='0' AND m.id=u.uid)
			LEFT JOIN " . TABLE_PREFIX . "usergroups g ON (m.isgroup='1' AND m.id=g.gid)
			WHERE fid='{$fid}'
			ORDER BY m.isgroup DESC, u.username, g.title
		");
                while ($moderator = $this->db->fetch_array($query)) {
                    if ($moderator['isgroup']) {
                        $moderator['img'] = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/group.png\" alt=\"{$this->lang->group}\" title=\"{$this->lang->group}\" />";
                        $form_container->output_cell("{$moderator['img']} <a href=\"".$this->bb->admin_url."/users/groups?action=edit&amp;gid={$moderator['id']}\">" . htmlspecialchars_uni($moderator['title']) . " ({$this->lang->usergroup} {$moderator['id']})</a>");
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=editmod&amp;mid={$moderator['mid']}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=deletemod&amp;id={$moderator['id']}&amp;isgroup=1&amp;fid={$fid}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_moderator_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                    } else {
                        $moderator['img'] = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/user.png\" alt=\"{$this->lang->username}\" title=\"{$this->lang->username}\" />";
                        $form_container->output_cell("{$moderator['img']} <a href=\"".$this->bb->admin_url."/users?action=edit&amp;uid={$moderator['id']}\">" . htmlspecialchars_uni($moderator['username']) . "</a>");
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=editmod&amp;mid={$moderator['mid']}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                        $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=deletemod&amp;id={$moderator['id']}&amp;isgroup=0&amp;fid={$fid}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_moderator_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                    }
                    $form_container->construct_row();
                }

                if ($form_container->num_rows() == 0) {
                    $form_container->output_cell($this->lang->no_moderators, ['colspan' => 3]);
                    $form_container->construct_row();
                }
                $html .= $form_container->end();

                // Users
                $buttons = [];
                $form = new Form($this->bb, $this->bb->admin_url . '/forum', 'post', 'management');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field("fid", $this->bb->input['fid']);
                $html .= $form->generate_hidden_field("add", "moderators");

                // Usergroup Moderator
                if (!is_array($usergroups)) {
                    $usergroups = $this->bb->groupscache;
                }

                foreach ($usergroups as $group) {
                    $modgroups[$group['gid']] = $this->lang->usergroup . " " . $group['gid'] . ": " . htmlspecialchars_uni($group['title']);
                }

                if (!isset($this->bb->input['usergroup'])) {
                    $this->bb->input['usergroup'] = '';
                }

                if (!isset($this->bb->input['username'])) {
                    $this->bb->input['username'] = '';
                }

                $form_container = new FormContainer($this, $this->lang->add_usergroup_as_moderator);
                $form_container->output_row($this->lang->usergroup . " <em>*</em>", $this->lang->moderator_usergroup_desc, $form->generate_select_box('usergroup', $modgroups, $this->bb->input['usergroup'], ['id' => 'usergroup']), 'usergroup');
                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->add_usergroup_moderator);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
                $html .= '<br />';

                $form = new Form($this->bb, $this->bb->admin_url . '/forum', 'post', 'management');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field("fid", $this->bb->input['fid']);
                $html .= $form->generate_hidden_field("add", "moderators");
                $form_container = new FormContainer($this, $this->lang->add_user_as_moderator);
                $form_container->output_row($this->lang->username . " <em>*</em>", $this->lang->moderator_username_desc, $form->generate_text_box('username', $this->bb->input['username'], ['id' => 'username']), 'username');
                $html .= $form_container->end();

                // Autocompletion for usernames
                $html .= '
		<link rel="stylesheet" href="' . $this->bb->asset_url . '/jscripts/select2/select2.css">
		<script type="text/javascript" src="' . $this->bb->asset_url . '/jscripts/select2/select2.min.js?ver=1804"></script>
		<script type="text/javascript">
		<!--
		$("#username").select2({
			placeholder: "' . $this->lang->search_for_a_user . '",
			minimumInputLength: 2,
			multiple: false,
			ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
				url: "'.$this->bb->settings['bburl'].'/xmlhttp?action=get_users",
				dataType: "json",
				data: function (term, page) {
					return {
						query: term, // search term
					};
				},
				results: function (data, page) { // parse the results into the format expected by Select2.
					// since we are using custom formatting functions we do not need to alter remote JSON data
					return {results: data};
				}
			},
			initSelection: function(element, callback) {
				var query = $(element).val();
				if (query !== "") {
					$.ajax("'.$this->bb->settings['bburl'].'/xmlhttp?action=get_users&getone=1", {
						data: {
							query: query
						},
						dataType: "json"
					}).done(function(data) { callback(data); });
				}
			},
		});

		$(\'[for=username]\').click(function(){
			$("#username").select2(\'open\');
			return false;
		});
		// -->
		</script>';

                $buttons = [$form->generate_submit_button($this->lang->add_user_moderator)];
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
                $html .= "</div>\n";

                $this->plugins->runHooks("admin_forum_management_start_graph");
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Forums/Management.html.twig');
        }
    }

    /**
     * @param DefaultFormContainer $form_container
     * @param int $pid
     * @param int $depth
     */
    private function build_admincp_forums_list(&$form_container, $pid = 0, $depth = 1)
    {
        static $forums_by_parent;

        if (!is_array($forums_by_parent)) {
            if (empty($this->bb->forum_cache)) {
                $this->forum->cache_forums();
            }
            foreach ($this->bb->forum_cache as $forum) {
                $forums_by_parent[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
            }
        }

        if (!isset($forums_by_parent[$pid])) {
            return;
        }
        $donecount = 0;
        $this->sub_forums = $comma = '';
        foreach ($forums_by_parent[$pid] as $children) {
            foreach ($children as $forum) {
                $forum['name'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['name']); // Fix & but allow unicode

                if ($forum['active'] == 0) {
                    $forum['name'] = "<em>" . $forum['name'] . "</em>";
                }

                if ($forum['type'] == "c" && ($depth == 1 || $depth == 2)) {
                    $this->sub_forums = '';
                    if (isset($forums_by_parent[$forum['fid']]) && $depth == 2) {
                        $this->build_admincp_forums_list($form_container, $forum['fid'], $depth + 1);
                    }
                    if ($this->sub_forums) {
                        $this->sub_forums = "<br /><small>{$this->lang->sub_forums}: {$this->sub_forums}</small>";
                    }

                    $form_container->output_cell("<div style=\"padding-left: " . (40 * ($depth - 1)) . "px;\"><a href=\"".$this->bb->admin_url."/forum?fid={$forum['fid']}\"><strong>{$forum['name']}</strong></a>{$this->sub_forums}</div>");

                    $form_container->output_cell("<input type=\"text\" name=\"disporder[" . $forum['fid'] . "]\" value=\"" . $forum['disporder'] . "\" class=\"text_input align_center\" style=\"width: 80%; font-weight: bold;\" />", ["class" => "align_center"]);

                    $popup = new PopupMenu("forum_{$forum['fid']}", $this->lang->options);
                    $popup->add_item($this->lang->edit_forum, $this->bb->admin_url . '/forum?action=edit&fid=' . $forum['fid']);
                    $popup->add_item($this->lang->subforums, $this->bb->admin_url . '/forum?fid=' . $forum['fid']);
                    $popup->add_item($this->lang->moderators, $this->bb->admin_url . "/forum?fid={$forum['fid']}#tab_moderators");
                    $popup->add_item($this->lang->permissions, $this->bb->admin_url . "/forum?fid={$forum['fid']}#tab_permissions");
                    $popup->add_item($this->lang->add_child_forum, $this->bb->admin_url . '/forum?action=add&pid=' . $forum['fid']);
                    $popup->add_item($this->lang->copy_forum, $this->bb->admin_url . '/forum?action=copy&fid=' . $forum['fid']);
                    $popup->add_item($this->lang->delete_forum, $this->bb->admin_url . '/forum?action=delete&fid=' . $forum['fid'] . '&my_post_key=' . $this->bb->post_code, "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_forum_deletion}')");

                    $form_container->output_cell($popup->fetch(), ["class" => "align_center"]);

                    $form_container->construct_row();

                    // Does this category have any sub forums?
                    if ($forums_by_parent[$forum['fid']]) {
                        $this->build_admincp_forums_list($form_container, $forum['fid'], $depth + 1);
                    }
                } elseif ($forum['type'] == "f" && ($depth == 1 || $depth == 2)) {
                    if ($forum['description']) {
                        $forum['description'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['description']);
                        $forum['description'] = "<br /><small>" . $forum['description'] . "</small>";
                    }

                    $this->sub_forums = '';
                    if (isset($forums_by_parent[$forum['fid']]) && $depth == 2) {
                        $this->build_admincp_forums_list($form_container, $forum['fid'], $depth + 1);
                    }
                    if ($this->sub_forums) {
                        $this->sub_forums = "<br /><small>{$this->lang->sub_forums}: {$this->sub_forums}</small>";
                    }

                    $form_container->output_cell("<div style=\"padding-left: " . (40 * ($depth - 1)) . "px;\"><a href=\"".$this->bb->admin_url."/forum?fid={$forum['fid']}\">{$forum['name']}</a>{$forum['description']}{$this->sub_forums}</div>");

                    $form_container->output_cell("<input type=\"text\" name=\"disporder[" . $forum['fid'] . "]\" value=\"" . $forum['disporder'] . "\" class=\"text_input align_center\" style=\"width: 80%;\" />", ["class" => "align_center"]);

                    $popup = new PopupMenu("forum_{$forum['fid']}", $this->lang->options);
                    $popup->add_item($this->lang->edit_forum, $this->bb->admin_url . '/forum?action=edit&fid=' . $forum['fid']);
                    $popup->add_item($this->lang->subforums, $this->bb->admin_url . '/forum?fid=' . $forum['fid']);
                    $popup->add_item($this->lang->moderators, $this->bb->admin_url . "/forum?fid={$forum['fid']}#tab_moderators");
                    $popup->add_item($this->lang->permissions, $this->bb->admin_url . "/forum?fid={$forum['fid']}#tab_permissions");
                    $popup->add_item($this->lang->add_child_forum, $this->bb->admin_url . '/forum?action=add&pid=' . $forum['fid']);
                    $popup->add_item($this->lang->copy_forum, $this->bb->admin_url . '/forum?action=copy&fid=' . $forum['fid']);
                    $popup->add_item($this->lang->delete_forum, $this->bb->admin_url . "/forum?action=delete&amp;fid={$forum['fid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_forum_deletion}')");

                    $form_container->output_cell($popup->fetch(), ["class" => "align_center"]);

                    $form_container->construct_row();

                    if (isset($forums_by_parent[$forum['fid']]) && $depth == 1) {
                        $this->build_admincp_forums_list($form_container, $forum['fid'], $depth + 1);
                    }
                } elseif ($depth == 3) {
                    if ($donecount < $this->bb->settings['subforumsindex']) {
                        $this->sub_forums .= "{$comma} <a href=\"".$this->bb->admin_url."/forum?fid={$forum['fid']}\">{$forum['name']}</a>";
                        $comma = $this->lang->comma;
                    }

                    // Have we reached our max visible subforums? put a nice message and break out of the loop
                    ++$donecount;
                    if ($donecount == $this->bb->settings['subforumsindex']) {
                        if (subforums_count($forums_by_parent[$pid]) > $donecount) {
                            $this->sub_forums .= $comma . $this->lang->sprintf($this->lang->more_subforums, (subforums_count($forums_by_parent[$pid]) - $donecount));
                            return;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param int $gid
     * @param int $fid
     *
     * @return string
     */
    private function retrieve_single_permissions_row($gid, $fid)
    {
        $query = $this->db->simple_select("usergroups", "*", "gid='{$gid}'");
        $usergroup = $this->db->fetch_array($query);

        $query = $this->db->simple_select("forums", "*", "fid='{$fid}'");
        $forum_data = $this->db->fetch_array($query);

        $query = $this->db->simple_select("forumpermissions", "*", "fid='{$fid}'");
        while ($existing = $this->db->fetch_array($query)) {
            $existing_permissions[$existing['gid']] = $existing;
        }

        $cached_forum_perms = $this->cache->read("forumpermissions");
        $field_list = [
            'canview' => $this->lang->permissions_canview,
            'canpostthreads' => $this->lang->permissions_canpostthreads,
            'canpostreplys' => $this->lang->permissions_canpostreplys,
            'canpostpolls' => $this->lang->permissions_canpostpolls,
        ];

        $field_list2 = [
            'canview' => $this->lang->perm_drag_canview,
            'canpostthreads' => $this->lang->perm_drag_canpostthreads,
            'canpostreplys' => $this->lang->perm_drag_canpostreplys,
            'canpostpolls' => $this->lang->perm_drag_canpostpolls,
        ];

        $form = new Form($this->bb, '', '', '', 0, '', true);
        $frm = $form->getForm();
        $form_container = new FormContainer($this);

        $perms = [];

        if (is_array($existing_permissions) && $existing_permissions[$usergroup['gid']]) {
            $perms = $existing_permissions[$usergroup['gid']];
            $default_checked = false;
        } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum_data['fid']][$usergroup['gid']]) {
            $perms = $cached_forum_perms[$forum_data['fid']][$usergroup['gid']];
            $default_checked = true;
        } elseif (is_array($cached_forum_perms) && $cached_forum_perms[$forum_data['pid']][$usergroup['gid']]) {
            $perms = $cached_forum_perms[$forum_data['pid']][$usergroup['gid']];
            $default_checked = true;
        }

        if (!$perms) {
            $perms = $usergroup;
            $default_checked = true;
        }

        foreach ($field_list as $forum_permission => $forum_perm_title) {
            if ($perms[$forum_permission] == 1) {
                $perms_checked[$forum_permission] = 1;
            } else {
                $perms_checked[$forum_permission] = 0;
            }
        }

        $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);

        if ($default_checked == 1) {
            $inherited_text = $this->lang->inherited_permission;
        } else {
            $inherited_text = $this->lang->custom_permission;
        }

        $form_container->output_cell("<strong>{$usergroup['title']}</strong> <small style=\"vertical-align: middle;\">({$inherited_text})</small>");

        $field_select = "<div class=\"quick_perm_fields\">\n";
        $field_select .= "<div class=\"enabled\"><div class=\"fields_title\">{$this->lang->enabled}</div><ul id=\"fields_enabled_{$usergroup['gid']}\">\n";
        foreach ($perms_checked as $perm => $value) {
            if ($value == 1) {
                $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
            }
        }
        $field_select .= "</ul></div>\n";
        $field_select .= "<div class=\"disabled\"><div class=\"fields_title\">{$this->lang->disabled}</div><ul id=\"fields_disabled_{$usergroup['gid']}\">\n";
        foreach ($perms_checked as $perm => $value) {
            if ($value == 0) {
                $field_select .= "<li id=\"field-{$perm}\">{$field_list2[$perm]}</li>";
            }
        }
        $field_select .= "</ul></div></div>\n";
        $field_select .= $form->generate_hidden_field("fields_" . $usergroup['gid'], @implode(",", @array_keys($perms_checked, 1)), ['id' => 'fields_' . $usergroup['gid']]);
        $field_select = str_replace("\n", "", $field_select);

        foreach ($field_list as $forum_permission => $permission_title) {
            $field_options[$forum_permission] = $permission_title;
        }
        $form_container->output_cell($field_select, ['colspan' => 2]);

        if (!$default_checked) {
            $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=permissions&amp;pid={$perms['pid']}\" onclick=\"MyBB.popupWindow('".$this->bb->admin_url."/forum?action=permissions&pid={$perms['pid']}&ajax=1', null, true); return false;\">{$this->lang->edit_permissions}</a>", ["class" => "align_center"]);
            $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=clear_permission&amp;pid={$perms['pid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_clear_custom_permission}')\">{$this->lang->clear_custom_perms}</a>", ["class" => "align_center"]);
        } else {
            $form_container->output_cell("<a href=\"".$this->bb->admin_url."/forum?action=permissions&amp;gid={$usergroup['gid']}&amp;fid={$fid}\"  onclick=\"MyBB.popupWindow('".$this->bb->admin_url."/forum?action=permissions&gid={$usergroup['gid']}&fid={$fid}&ajax=1', null, true); return false;\">{$this->lang->set_custom_perms}</a>", ["class" => "align_center", "colspan" => 2]);
        }
        $form_container->construct_row();
        return $frm . $form_container->output_row_cells(0, true);
    }

    /**
     * @param int $fid
     */
    private function save_quick_perms($fid)
    {
        $permission_fields = [];

        $field_list = $this->db->show_fields_from('forumpermissions');
        foreach ($field_list as $field) {
            if (strpos($field['Field'], 'can') !== false || strpos($field['Field'], 'mod') !== false) {
                $permission_fields[$field['Field']] = 1;
            }
        }

        // "Can Only View Own Threads" and "Can Only Reply Own Threads" permissions are forum permission only options
        $usergroup_permission_fields = $permission_fields;
        unset($usergroup_permission_fields['canonlyviewownthreads']);
        unset($usergroup_permission_fields['canonlyreplyownthreads']);

        $query = $this->db->simple_select('usergroups', 'gid');
        while ($usergroup = $this->db->fetch_array($query)) {
            $query2 = $this->db->simple_select('forumpermissions', $this->db->escape_string(implode(',', array_keys($permission_fields))), "fid='{$fid}' AND gid='{$usergroup['gid']}'", ['limit' => 1]);
            $existing_permissions = $this->db->fetch_array($query2);

            if (!$existing_permissions) {
                $query2 = $this->db->simple_select('usergroups', $this->db->escape_string(implode(',', array_keys($usergroup_permission_fields))), "gid='{$usergroup['gid']}'", ['limit' => 1]);
                $existing_permissions = $this->db->fetch_array($query2);
            }

            // Delete existing permissions
            $this->db->delete_query('forumpermissions', "fid='{$fid}' AND gid='{$usergroup['gid']}'");

            // Only insert the new ones if we're using custom permissions
            if ($this->inherit[$usergroup['gid']] != 1) {
                if ($this->canview[$usergroup['gid']] == 1) {
                    $pview = 1;
                } else {
                    $pview = 0;
                }

                if ($this->canpostthreads[$usergroup['gid']] == 1) {
                    $pthreads = 1;
                } else {
                    $pthreads = 0;
                }

                if ($this->canpostreplies[$usergroup['gid']] == 1) {
                    $preplies = 1;
                } else {
                    $preplies = 0;
                }

                if ($this->canpostpolls[$usergroup['gid']] == 1) {
                    $ppolls = 1;
                } else {
                    $ppolls = 0;
                }

                if (!$preplies && !$pthreads) {
                    $ppost = 0;
                } else {
                    $ppost = 1;
                }

                $insertquery = [
                    'fid' => (int)$fid,
                    'gid' => (int)$usergroup['gid'],
                    'canview' => (int)$pview,
                    'canpostthreads' => (int)$pthreads,
                    'canpostreplys' => (int)$preplies,
                    'canpostpolls' => (int)$ppolls,
                ];

                foreach ($permission_fields as $field => $value) {
                    if (array_key_exists($field, $insertquery)) {
                        continue;
                    }

                    $insertquery[$this->db->escape_string($field)] = (int)$existing_permissions[$field];
                }

                $this->db->insert_query('forumpermissions', $insertquery);
            }
        }
        $this->cache->update_forumpermissions();
    }
}
