<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Forums;

class Meta
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * @return bool true
     */
    public function forums_meta()
    {
        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'management', 'title' => $this->bb->lang->forum_management, 'link' => $this->bb->admin_url . '/forum'];//?action=management');
        $sub_menu['20'] = ['id' => 'announcements', 'title' => $this->bb->lang->forum_announcements, 'link' => $this->bb->admin_url . '/forum/announcements'];
        $sub_menu['30'] = ['id' => 'moderation_queue', 'title' => $this->bb->lang->moderation_queue, 'link' => $this->bb->admin_url . '/forum/mod_queue'];
        $sub_menu['40'] = ['id' => 'attachments', 'title' => $this->bb->lang->attachments, 'link' => $this->bb->admin_url . '/forum/attachments'];

        $sub_menu = $this->bb->plugins->runHooks('admin_forum_menu', $sub_menu);

        $this->bb->page->add_menu_item($this->bb->lang->forums_and_posts, 'forums', $this->bb->admin_url . '/forum', 20, $sub_menu);

        return true;
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function forums_action_handler($action)
    {
        $this->bb->page->active_module = 'forums';

        $actions = [
            'mod_queue' => ['active' => 'moderation_queue', 'file' => 'moderation_queue.php'],
            'announcements' => ['active' => 'announcements', 'file' => 'announcements.php'],
            'attachments' => ['active' => 'attachments', 'file' => 'attachments.php'],
            'management' => ['active' => 'management', 'file' => 'management.php']
        ];

        $actions = $this->bb->plugins->runHooks('admin_forum_action_handler', $actions);

        //\RunBB\Init
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('forum-forums');

        if (isset($actions[$action])) {
            $this->bb->page->active_action = $actions[$action]['active'];
            return $actions[$action]['file'];
        } else {
            $this->bb->page->active_action = 'management';
            return 'management.php';
        }
    }

    /**
     * @return array
     */
    public function forums_admin_permissions()
    {
        $admin_permissions = [
            'management' => $this->bb->lang->can_manage_forums,
            'announcements' => $this->bb->lang->can_manage_forum_announcements,
            'moderation_queue' => $this->bb->lang->can_moderate,
            'attachments' => $this->bb->lang->can_manage_attachments,
        ];

        $admin_permissions = $this->bb->plugins->runHooks('admin_forum_permissions', $admin_permissions);

        return ['name' => $this->bb->lang->forums_and_posts, 'permissions' => $admin_permissions, 'disporder' => 20];
    }
}
