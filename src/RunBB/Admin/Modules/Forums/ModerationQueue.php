<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Forums;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\Table;
use RunCMF\Core\AbstractController;

class ModerationQueue extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('forums');// set active module
        $this->lang->load('forums_moderation_queue', false, true);


        $this->page->add_breadcrumb_item($this->lang->moderation_queue, $this->bb->admin_url . '/forum/mod_queue');

        $sub_tabs['threads'] = [
            'title' => $this->lang->threads,
            'link' => $this->bb->admin_url . '/forum/mod_queue?type=threads',
            'description' => $this->lang->threads_desc
        ];

        $sub_tabs['posts'] = [
            'title' => $this->lang->posts,
            'link' => $this->bb->admin_url . '/forum/mod_queue?type=posts',
            'description' => $this->lang->posts_desc
        ];

        $sub_tabs['attachments'] = [
            'title' => $this->lang->attachments,
            'link' => $this->bb->admin_url . '/forum/mod_queue?type=attachments',
            'description' => $this->lang->attachments_desc
        ];

        $this->plugins->runHooks('admin_forum_moderation_queue_begin');

        // Actually performing our moderation choices
        if ($this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_forum_moderation_queue_commit');

            if (is_array($this->bb->input['threads'])) {
                $threads_to_approve = $threads_to_delete = [];
                // Fetch threads
                $query = $this->db->simple_select('threads', 'tid', 'tid IN (' . implode(',', array_map('intval', array_keys($this->bb->input['threads']))) . "){$flist}");
                while ($thread = $this->db->fetch_array($query)) {
                    $action = $this->bb->input['threads'][$thread['tid']];
                    if ($action == 'approve') {
                        $threads_to_approve[] = $thread['tid'];
                    } elseif ($action == 'delete' && $this->bb->settings['soft_delete'] != 1) {
                        $this->moderation->delete_thread($thread['tid']);
                    } elseif ($action == 'delete') {
                        $threads_to_delete[] = $thread['tid'];
                    }
                }
                if (!empty($threads_to_approve)) {
                    $this->moderation->approve_threads($threads_to_approve);
                }
                if (!empty($threads_to_delete)) {
                    $this->moderation->soft_delete_threads($threads_to_delete);
                }

                $this->plugins->runHooks('admin_forum_moderation_queue_threads_commit');

                // Log admin action
                $this->bblogger->log_admin_action('threads');

                $this->session->flash_message($this->lang->success_threads, 'success');
                return $response->withRedirect($this->bb->admin_url . '/forum/mod_queue?type=threads');
            } elseif (is_array($this->bb->input['posts'])) {
                $posts_to_approve = $posts_to_delete = [];
                // Fetch posts
                $query = $this->db->simple_select('posts', 'pid', 'pid IN (' . implode(',', array_map('intval', array_keys($this->bb->input['posts']))) . "){$flist}");
                while ($post = $this->db->fetch_array($query)) {
                    $action = $this->bb->input['posts'][$post['pid']];
                    if ($action == 'approve') {
                        $posts_to_approve[] = $post['pid'];
                    } elseif ($action == 'delete' && $this->bb->settings['soft_delete'] != 1) {
                        $this->moderation->delete_post($post['pid']);
                    } elseif ($action == 'delete') {
                        $posts_to_delete[] = $post['pid'];
                    }
                }
                if (!empty($posts_to_approve)) {
                    $this->moderation->approve_posts($posts_to_approve);
                }
                if (!empty($posts_to_delete)) {
                    $this->moderation->soft_delete_posts($posts_to_delete);
                }

                $this->plugins->runHooks('admin_forum_moderation_queue_posts_commit');

                // Log admin action
                $this->bblogger->log_admin_action('posts');

                $this->session->flash_message($this->lang->success_posts, 'success');
                return $response->withRedirect($this->bb->admin_url . '/forum/mod_queue?type=posts');
            } elseif (is_array($this->bb->input['attachments'])) {
                $query = $this->db->simple_select('attachments', 'aid, pid', 'aid IN (' . implode(',', array_map('intval', array_keys($this->bb->input['attachments']))) . "){$flist}");
                while ($attachment = $this->db->fetch_array($query)) {
                    $action = $this->bb->input['attachments'][$attachment['aid']];
                    if ($action == 'approve') {
                        $this->db->update_query('attachments', ['visible' => 1], "aid='{$attachment['aid']}'");
                    } elseif ($action == 'delete') {
                        $this->upload->remove_attachment($attachment['pid'], '', $attachment['aid']);
                    }
                }

                $this->plugins->runHooks('admin_forum_moderation_queue_attachments_commit');

                // Log admin action
                $this->bblogger->log_admin_action('attachments');

                $this->session->flash_message($this->lang->success_attachments, 'success');
                return $response->withRedirect($this->bb->admin_url . '/forum/mod_queue?type=attachments');
            }
        }

        $all_options = "<ul class=\"modqueue_mass\">\n";
        $all_options .= "<li><a href=\"#\" class=\"mass_ignore\">{$this->lang->mark_as_ignored}</a></li>\n";
        $all_options .= "<li><a href=\"#\" class=\"mass_delete\">{$this->lang->mark_as_deleted}</a></li>\n";
        $all_options .= "<li><a href=\"#\" class=\"mass_approve\">{$this->lang->mark_as_approved}</a></li>\n";
        $all_options .= "</ul>\n";

        // Threads awaiting moderation
        if (!isset($this->bb->input['type']) || $this->bb->input['type'] == 'threads') {
            $this->plugins->runHooks('admin_forum_moderation_queue_threads');

            $this->cache->read('forums');

            $query = $this->db->simple_select('threads', 'COUNT(tid) AS unapprovedthreads', 'visible=0');
            $unapproved_threads = $this->db->fetch_field($query, 'unapprovedthreads');

            if ($unapproved_threads > 0) {
                // Figure out if we need to display multiple pages.
                $per_page = 15;
                if ($this->bb->input['page'] > 0) {
                    $current_page = $this->bb->getInput('page', 0);
                    $start = ($current_page - 1) * $per_page;
                    $pages = $unapproved_threads / $per_page;
                    $pages = ceil($pages);
                    if ($current_page > $pages) {
                        $start = 0;
                        $current_page = 1;
                    }
                } else {
                    $start = 0;
                    $current_page = 1;
                }

                $pagination = $this->adm->draw_admin_pagination($current_page, $per_page, $unapproved_threads, $this->bb->admin_url . '/forum/mod_queue?page={page}');

                $this->page->add_breadcrumb_item($this->lang->threads_awaiting_moderation);
                $html = $this->page->output_header($this->lang->threads_awaiting_moderation);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'threads');

                $form = new Form($this->bb, $this->bb->admin_url . '/forum/mod_queue', 'post');
                $html .= $form->getForm();

                $table = new Table;
                $table->construct_header($this->lang->subject);
                $table->construct_header($this->lang->author, ['class' => 'align_center', 'width' => '20%']);
                $table->construct_header($this->lang->posted, ['class' => 'align_center', 'width' => '20%']);

                $query = $this->db->query('
			SELECT t.tid, t.dateline, t.fid, t.subject, t.username AS threadusername, p.message AS postmessage, u.username AS username, t.uid
			FROM ' . TABLE_PREFIX . 'threads t
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=t.firstpost)
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=t.uid)
			WHERE t.visible='0'
			ORDER BY t.lastpost DESC
			LIMIT {$start}, {$per_page}
		");
                while ($thread = $this->db->fetch_array($query)) {
                    $thread['subject'] = htmlspecialchars_uni($thread['subject']);
                    $thread['threadlink'] = get_thread_link($thread['tid']);
                    $thread['forumlink'] = $this->forum->get_forum_link($thread['fid']);
                    $forum_name = $this->bb->forum_cache[$thread['fid']]['name'];
                    $threaddate = $this->time->formatDate('relative', $thread['dateline']);

                    if ($thread['username'] == '') {
                        if ($thread['threadusername'] != '') {
                            $profile_link = $thread['threadusername'];
                        } else {
                            $profile_link = $this->lang->guest;
                        }
                    } else {
                        $profile_link = $this->user->build_profile_link($thread['username'], $thread['uid'], '_blank');
                    }

                    $thread['postmessage'] = nl2br(htmlspecialchars_uni($thread['postmessage']));

                    $table->construct_cell("<a href=\"../{$thread['threadlink']}\" target=\"_blank\">{$thread['subject']}</a>");
                    $table->construct_cell($profile_link, ['class' => 'align_center']);
                    $table->construct_cell($threaddate, ['class' => 'align_center']);
                    $table->construct_row();

                    $controls = "<div class=\"modqueue_controls\">\n";
                    $controls .= $form->generate_radio_button("threads[{$thread['tid']}]", "ignore", $this->lang->ignore, ['class' => 'radio_ignore', 'checked' => true]) . " ";
                    $controls .= $form->generate_radio_button("threads[{$thread['tid']}]", "delete", $this->lang->delete, ['class' => 'radio_delete', 'checked' => false]) . " ";
                    $controls .= $form->generate_radio_button("threads[{$thread['tid']}]", "approve", $this->lang->approve, ['class' => 'radio_approve', 'checked' => false]);
                    $controls .= '</div>';

                    $forum = "<strong>{$this->lang->forum} <a href=\"../{$thread['forumlink']}\" target=\"_blank\">{$forum_name}</a></strong><br />";

                    $table->construct_cell("<div class=\"modqueue_message\">{$controls}<div class=\"modqueue_meta\">{$forum}</div>{$thread['postmessage']}</div>", ["colspan" => 3]);
                    $table->construct_row();
                }

                $html .= $table->output($this->lang->threads_awaiting_moderation);
                $html .= $all_options;
                $html .= $pagination;

                $buttons[] = $form->generate_submit_button($this->lang->perform_action);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();

                $html .= '<script type="text/javascript">
			$(".mass_ignore").on("click", function () {
				$("input.radio_ignore").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
			$(".mass_delete").on("click", function () {
				$("input.radio_delete").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
			$(".mass_approve").on("click", function () {
				$("input.radio_approve").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
		</script>';

                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/ModerationQueueThreads.html.twig');
            }
        }

        // Posts awaiting moderation
        if (!isset($this->bb->input['type']) || $this->bb->input['type'] == 'posts') {
            $this->plugins->runHooks('admin_forum_moderation_queue_posts');

            $this->cache->read('forums');

            $query = $this->db->query('
		SELECT COUNT(pid) AS unapprovedposts
		FROM  ' . TABLE_PREFIX . 'posts p
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
		WHERE p.visible='0' AND t.firstpost != p.pid
	");
            $unapproved_posts = $this->db->fetch_field($query, 'unapprovedposts');

            if ($unapproved_posts > 0) {
                // Figure out if we need to display multiple pages.
                $per_page = 15;
                if ($this->bb->input['page'] > 0) {
                    $current_page = $this->bb->getInput('page', 0);
                    $start = ($current_page - 1) * $per_page;
                    $pages = $unapproved_posts / $per_page;
                    $pages = ceil($pages);
                    if ($current_page > $pages) {
                        $start = 0;
                        $current_page = 1;
                    }
                } else {
                    $start = 0;
                    $current_page = 1;
                }

                $pagination = $this->adm->draw_admin_pagination($current_page, $per_page, $unapproved_posts, $this->bb->admin_url . '/forum/mod_queue?type=posts&amp;page={page}');


                $this->page->add_breadcrumb_item($this->lang->posts_awaiting_moderation);
                $html = $this->page->output_header($this->lang->posts_awaiting_moderation);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'posts');

                $form = new Form($this->bb, $this->bb->admin_url . '/forum/mod_queue', 'post');
                $html .= $form->getForm();

                $table = new Table;
                $table->construct_header($this->lang->subject);
                $table->construct_header($this->lang->author, ['class' => 'align_center', 'width' => '20%']);
                $table->construct_header($this->lang->posted, ['class' => 'align_center', 'width' => '20%']);

                $query = $this->db->query('
			SELECT p.pid, p.subject, p.message, p.dateline, p.username AS postusername, t.subject AS threadsubject, t.tid, u.username, p.uid, t.fid
			FROM  ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
			WHERE p.visible='0' AND t.firstpost != p.pid
			ORDER BY p.dateline DESC
			LIMIT {$start}, {$per_page}
		");
                while ($post = $this->db->fetch_array($query)) {
                    $altbg = alt_trow();
                    $post['threadsubject'] = htmlspecialchars_uni($post['threadsubject']);
                    $post['subject'] = htmlspecialchars_uni($post['subject']);

                    if (!$post['subject']) {
                        $post['subject'] = $this->lang->re . ' ' . $post['threadsubject'];
                    }

                    $post['postlink'] = get_post_link($post['pid'], $post['tid']);
                    $post['threadlink'] = get_thread_link($post['tid']);
                    $post['forumlink'] = $this->forum->get_forum_link($post['fid']);
                    $forum_name = $this->bb->forum_cache[$post['fid']]['name'];
                    $postdate = $this->time->formatDate('relative', $post['dateline']);

                    if ($post['username'] == '') {
                        if ($post['postusername'] != '') {
                            $profile_link = $post['postusername'];
                        } else {
                            $profile_link = $this->lang->guest;
                        }
                    } else {
                        $profile_link = $this->user->build_profile_link($post['username'], $post['uid'], '_blank');
                    }

                    $post['message'] = nl2br(htmlspecialchars_uni($post['message']));

                    $table->construct_cell("<a href=\"../{$post['postlink']}#pid{$post['pid']}\" target=\"_blank\">{$post['subject']}</a>");
                    $table->construct_cell($profile_link, ['class' => 'align_center']);
                    $table->construct_cell($postdate, ['class' => 'align_center']);
                    $table->construct_row();

                    $controls = "<div class=\"modqueue_controls\">\n";
                    $controls .= $form->generate_radio_button("posts[{$post['pid']}]", "ignore", $this->lang->ignore, ['class' => 'radio_ignore', 'checked' => true]) . " ";
                    $controls .= $form->generate_radio_button("posts[{$post['pid']}]", "delete", $this->lang->delete, ['class' => 'radio_delete', 'checked' => false]) . " ";
                    $controls .= $form->generate_radio_button("posts[{$post['pid']}]", "approve", $this->lang->approve, ['class' => 'radio_approve', 'checked' => false]);
                    $controls .= '</div>';

                    $thread = "<strong>{$this->lang->thread} <a href=\"../{$post['threadlink']}\" target=\"_blank\">{$post['threadsubject']}</a></strong>";
                    $forum = "<strong>{$this->lang->forum} <a href=\"../{$post['forumlink']}\" target=\"_blank\">{$forum_name}</a></strong><br />";

                    $table->construct_cell("<div class=\"modqueue_message\">{$controls}<div class=\"modqueue_meta\">{$forum}{$thread}</div>{$post['message']}</div>", ["colspan" => 3]);
                    $table->construct_row();
                }

                $html .= $table->output($this->lang->posts_awaiting_moderation);
                $html .= $all_options;
                $html .= $pagination;

                $buttons[] = $form->generate_submit_button($this->lang->perform_action);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();

                $html .= '<script type="text/javascript">
			$(".mass_ignore").on("click", function () {
				$("input.radio_ignore").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
			$(".mass_delete").on("click", function () {
				$("input.radio_delete").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
			$(".mass_approve").on("click", function () {
				$("input.radio_approve").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
		</script>';

                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/ModerationQueuePosts.html.twig');
            } elseif (isset($this->bb->input['type']) && $this->bb->input['type'] == 'posts') {
                $html = $this->page->output_header($this->lang->moderation_queue);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'posts');
                $html .= "<p class=\"notice\">{$this->lang->error_no_posts}</p>";
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/ModerationQueuePosts.html.twig');
            }
        }

        // Attachments awaiting moderation
        if (!isset($this->bb->input['type']) || $this->bb->input['type'] == 'attachments') {
            $this->plugins->runHooks('admin_forum_moderation_queue_attachments');

            $query = $this->db->query('
		SELECT COUNT(aid) AS unapprovedattachments
		FROM  ' . TABLE_PREFIX . 'attachments a
		LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
		WHERE a.visible='0'
	");
            $unapproved_attachments = $this->db->fetch_field($query, 'unapprovedattachments');

            if ($unapproved_attachments > 0) {
                // Figure out if we need to display multiple pages.
                $per_page = 15;
                if ($this->bb->input['page'] > 0) {
                    $current_page = $this->bb->getInput('page', 0);
                    $start = ($current_page - 1) * $per_page;
                    $pages = $unapproved_attachments / $per_page;
                    $pages = ceil($pages);
                    if ($current_page > $pages) {
                        $start = 0;
                        $current_page = 1;
                    }
                } else {
                    $start = 0;
                    $current_page = 1;
                }

                $pagination = $this->adm->draw_admin_pagination($current_page, $per_page, $unapproved_attachments, $this->bb->admin_url . '/forum/mod_queue?type=attachments&amp;page={page}');

                $this->page->add_breadcrumb_item($this->lang->attachments_awaiting_moderation);
                $html = $this->page->output_header($this->lang->attachments_awaiting_moderation);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'attachments');

                $form = new Form($this->bb, $this->bb->admin_url . '/forum/mod_queue', 'post');
                $html .= $form->getForm();

                $table = new Table;
                $table->construct_header($this->lang->filename);
                $table->construct_header($this->lang->uploadedby, ['class' => 'align_center', 'width' => '20%']);
                $table->construct_header($this->lang->posted, ['class' => 'align_center', 'width' => '20%']);
                $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 3]);

                $query = $this->db->query('
			SELECT a.*, p.subject AS postsubject, p.dateline, p.uid, u.username, t.tid, t.subject AS threadsubject
			FROM  ' . TABLE_PREFIX . 'attachments a
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
			WHERE a.visible='0'
			ORDER BY a.dateuploaded DESC
			LIMIT {$start}, {$per_page}
		");

                while ($attachment = $this->db->fetch_array($query)) {
                    if (!$attachment['dateuploaded']) {
                        $attachment['dateuploaded'] = $attachment['dateline'];
                    }
                    $attachdate = $this->time->formatDate('relative', $attachment['dateuploaded']);

                    $attachment['postsubject'] = htmlspecialchars_uni($attachment['postsubject']);
                    $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                    $attachment['threadsubject'] = htmlspecialchars_uni($attachment['threadsubject']);
                    $attachment['filesize'] = $this->parser->friendlySize($attachment['filesize']);

                    $link = get_post_link($attachment['pid'], $attachment['tid']) . "#pid{$attachment['pid']}";
                    $thread_link = get_thread_link($attachment['tid']);
                    $profile_link = $this->user->build_profile_link($attachment['username'], $attachment['uid'], '_blank');

                    $table->construct_cell("<a href=\"../attachment.php?aid={$attachment['aid']}\" target=\"_blank\">{$attachment['filename']}</a> ({$attachment['filesize']})<br /><small class=\"modqueue_meta\">{$this->lang->post} <a href=\"{$link}\" target=\"_blank\">{$attachment['postsubject']}</a></small>");
                    $table->construct_cell($profile_link, ['class' => 'align_center']);
                    $table->construct_cell($attachdate, ['class' => 'align_center']);

                    $table->construct_cell($form->generate_radio_button("attachments[{$attachment['aid']}]", "ignore", $this->lang->ignore, ['class' => 'radio_ignore', 'checked' => true]), ["class" => "align_center"]);
                    $table->construct_cell($form->generate_radio_button("attachments[{$attachment['aid']}]", "delete", $this->lang->delete, ['class' => 'radio_delete', 'checked' => false]), ["class" => "align_center"]);
                    $table->construct_cell($form->generate_radio_button("attachments[{$attachment['aid']}]", "approve", $this->lang->approve, ['class' => 'radio_approve', 'checked' => false]), ["class" => "align_center"]);
                    $table->construct_row();
                }
                $html .= $table->output($this->lang->attachments_awaiting_moderation);
                $html .= $all_options;
                $html .= $pagination;

                $buttons[] = $form->generate_submit_button($this->lang->perform_action);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();

                $html .= '<script type="text/javascript">
			$(".mass_ignore").on("click", function () {
				$("input.radio_ignore").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
			$(".mass_delete").on("click", function () {
				$("input.radio_delete").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
			$(".mass_approve").on("click", function () {
				$("input.radio_approve").each(function(e) {
					$(this).prop("checked", true);
				});
				return false;
			});
		</script>';

                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/ModerationQueueAttachments.html.twig');
            } elseif (isset($this->bb->input['type']) && $this->bb->input['type'] == 'attachments') {
                $html = $this->page->output_header($this->lang->moderation_queue);
                $html .= $this->page->output_nav_tabs($sub_tabs, 'attachments');
                $html .= "<p class=\"notice\">{$this->lang->error_no_attachments}</p>";
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Forums/ModerationQueueAttachments.html.twig');
            }
        }

        // Still nothing? All queues are empty! :-D
        $html = $this->page->output_header($this->lang->moderation_queue);
        $html .= "<p class=\"notice\">{$this->lang->error_no_threads}</p>";
        $this->page->output_footer();

        $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
        return $this->view->render($response, '@forum/Admin/Forums/ModerationQueue.html.twig');
    }
}
