<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Home;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunCMF\Core\AbstractController;

class Credits extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init();
        $this->lang->load('home_credits', false, true);

        $this->page->add_breadcrumb_item($this->lang->mybb_credits, $this->bb->admin_url . '/credits');//'index.php?module=home-credits');

        $this->plugins->runHooks('admin_home_credits_begin');

        if (!$this->bb->input['action']) {
            $this->page->output_header($this->lang->mybb_credits);

            $sub_tabs['credits'] = [
                'title' => $this->lang->mybb_credits,
                'link' => $this->bb->admin_url . '/credits',//'index.php?module=home-credits',
                'description' => $this->lang->mybb_credits_description
            ];

            $sub_tabs['credits_about'] = [
                'title' => $this->lang->about_the_team,
                'link' => 'http://www.mybb.com/about/team',
                'link_target' => '_blank',
            ];

            $sub_tabs['check_for_updates'] = [
                'title' => $this->lang->check_for_updates,
                'link' => $this->bb->admin_url . '/credits?fetch_new=1'//'index.php?module=home-credits&amp;fetch_new=1',
            ];

            $this->plugins->runHooks('admin_home_credits_start');

            $html = $this->page->output_nav_tabs($sub_tabs, 'credits');

            $mybb_credits = $this->cache->read('mybb_credits');

            if ($this->bb->getInput('fetch_new', 0) == 1 ||
                $this->bb->getInput('fetch_new', 0) == -2 ||
                ($this->bb->getInput('fetch_new', 0) != -1 &&
                    (!is_array($mybb_credits) ||
                        $mybb_credits['last_check'] <= TIME_NOW - 60 * 60 * 24 * 14))
            ) {
                $new_mybb_credits = [
                    'last_check' => TIME_NOW
                ];

                $contents = fetch_remote_file('http://www.mybb.com/mybb_team.xml');

                if (!$contents) {
                    $this->session->flash_message($this->lang->error_communication, 'error');
                    if ($this->bb->getInput('fetch_new', 0) == -2) {
                        return $response->withRedirect($this->bb->admin_url . '/tools/cache');
                    }
                    return $response->withRedirect($this->bb->admin_url . '/credits?fetch_new=-1');//'index.php?module=home-credits&amp;fetch_new=-1');
                }

                $parser = new \RunBB\Core\XMLParser($contents);
                $tree = $parser->get_tree();
                $mybbgroup = [];
                foreach ($tree['mybbgroup']['team'] as $team) {
                    $members = [];
                    foreach ($team['member'] as $member) {
                        $members[] = [
                            'name' => htmlspecialchars_uni($member['name']['value']),
                            'username' => htmlspecialchars_uni($member['username']['value']),
                            'profile' => htmlspecialchars_uni($member['profile']['value']),
                            'lead' => isset($member['attributes']['lead']) ? (bool)$member['attributes']['lead'] : false
                        ];
                    }
                    $mybbgroup[] = [
                        'title' => htmlspecialchars_uni($team['attributes']['title']),
                        'members' => $members
                    ];
                }
                $new_mybb_credits['credits'] = $mybbgroup;

                $this->cache->update('mybb_credits', $new_mybb_credits);

                if ($this->bb->getInput('fetch_new', 0) == -2) {
                    $this->lang->load('tools_cache');
                    $this->session->flash_message($this->lang->success_cache_reloaded, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/tools/cache');
                } else {
                    $this->session->flash_message($this->lang->success_credits_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/credits?fetch_new=-1');//'index.php?module=home-credits&amp;fetch_new=-1');
                }
            }

            if (empty($mybb_credits) || (is_array($mybb_credits) && empty($mybb_credits['credits']))) {
                $table = new Table;
                $table->construct_cell($this->lang->no_credits);
                $table->construct_row();
            } else {
                $largest_count = $i = 0;
                $team_max = [];
                foreach ($mybb_credits['credits'] as $team) {
                    $count = count($team['members']);
                    $team_max[$i++] = $count;
                    if ($largest_count < $count) {
                        $largest_count = $count;
                    }
                }
                $largest_count -= 1;

                $table = new Table;
                foreach ($mybb_credits['credits'] as $team) {
                    $table->construct_header($team['title'], ['width' => '16%']);
                }

                for ($i = 0; $i <= $largest_count; $i++) {
                    foreach ($team_max as $team => $max) {
                        if ($max < $i || !isset($mybb_credits['credits'][$team]['members'][$i])) {
                            $table->construct_cell('&nbsp;');
                        } else {
                            $table->construct_cell("<a href=\"{$mybb_credits['credits'][$team]['members'][$i]['profile']}\" title=\"{$mybb_credits['credits'][$team]['members'][$i]['username']}\" target=\"_blank\">{$mybb_credits['credits'][$team]['members'][$i]['name']}</a>");
                        }
                    }
                    $table->construct_row();
                }
            }

            $html .= $table->output($this->lang->mybb_credits);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Home/Credits.html.twig');
        }
    }
}
