<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Admin\Modules\Home;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunCMF\Core\AbstractController;
use RunBB\Core\XMLParser;
use RunBB\Core\FeedParser;
use Carbon\Carbon;

class Index extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init();

        $this->plugins->runHooks('admin_home_index_begin');

        $this->lang->load('home_dashboard', false, true);

        $sub_tabs['dashboard'] = [
            'title' => $this->lang->dashboard,
            'link' => $this->bb->admin_url,
            'description' => $this->lang->dashboard_description
        ];

        $sub_tabs['version_check'] = [
            'title' => $this->lang->version_check,
            'link' => $this->bb->admin_url . '?action=version_check',
            'description' => $this->lang->version_check_description
        ];

        if ($this->bb->input['action'] == 'version_check') {
            $this->plugins->runHooks('admin_home_version_check_start');

            $current_version = rawurlencode($this->bb->version_code);

            $updated_cache = [
                'last_check' => TIME_NOW
            ];

            $contents = fetch_remote_file('http://www.mybb.com/version_check.php');

            if (!$contents) {
                $this->session->flash_message($this->lang->error_communication, 'error');
                return $response->withRedirect($this->bb->admin_url);
            }

            $this->plugins->runHooks('admin_home_version_check');

            $this->page->add_breadcrumb_item($this->lang->version_check, $this->bb->admin_url . '?action=version_check');
            $this->page->output_header($this->lang->version_check);
            $html = $this->page->output_nav_tabs($sub_tabs, 'version_check');

            // We do this because there is some weird symbols that show up in the xml file for unknown reasons
            $pos = strpos($contents, '<');
            if ($pos > 1) {
                $contents = substr($contents, $pos);
            }

            $pos = strpos(strrev($contents), '>');
            if ($pos > 1) {
                $contents = substr($contents, 0, (-1) * ($pos - 1));
            }

            $parser = new XMLParser($contents);
            $tree = $parser->get_tree();

            $latest_code = (int)$tree['mybb']['version_code']['value'];
            $latest_version = '<strong>' . htmlspecialchars_uni($tree['mybb']['latest_version']['value']) . '</strong> (' . $latest_code . ')';
            if ($latest_code > $this->bb->version_code) {
                $latest_version = '<span style="color: #C00;">' . $latest_version . '</span>';
                $version_warn = 1;
                $updated_cache['latest_version'] = $latest_version;
                $updated_cache['latest_version_code'] = $latest_code;
            } else {
                $version_warn = 0;
                $latest_version = '<span style="color: green;">' . $latest_version . '</span>';
            }

            if ($version_warn) {
                $html .= $this->page->output_error("<p><em>{$this->lang->error_out_of_date}</em> {$this->lang->update_forum}</p>");
            } else {
                $html .= $this->page->output_success("<p><em>{$this->lang->success_up_to_date}</em></p>");
            }

            $table = new Table;
            $table->construct_header($this->lang->your_version);
            $table->construct_header($this->lang->latest_version);

            $table->construct_cell('<strong>' . $this->bb->version . '</strong> (' . $this->bb->version_code . ')');
            $table->construct_cell($latest_version);
            $table->construct_row();

            $html .= $table->output($this->lang->version_check);

            $feed_parser = new FeedParser();
            $feed_parser->parse_feed('http://feeds.feedburner.com/MyBBDevelopmentBlog');

            $updated_cache['news'] = [];

            if ($feed_parser->error == '') {
                foreach ($feed_parser->items as $item) {
                    if (!isset($updated_cache['news'][2])) {
                        $description = $item['description'];
                        $content = $item['content'];

                        $description = $this->parser->parse_message($description, [
                                'allow_html' => true,
                            ]);

                        $content = $this->parser->parse_message($content, [
                                'allow_html' => true,
                            ]);

                        $description = preg_replace('#<img(.*)/>#', '', $description);
                        $content = preg_replace('#<img(.*)/>#', '', $content);

                        $updated_cache['news'][] = [
                            'title' => htmlspecialchars_uni($item['title']),
                            'description' => $description,
                            'link' => htmlspecialchars_uni($item['link']),
                            'author' => htmlspecialchars_uni($item['author']),
                            'dateline' => $item['date_timestamp'],
                        ];
                    }

                    $stamp = '';
                    if ($item['date_timestamp']) {
                        $stamp = Carbon::createFromTimestamp($item['date_timestamp'])->toDayDateTimeString();
                    }

                    $link = htmlspecialchars_uni($item['link']);

                    $table->construct_cell('<span style="font-size: 16px;"><strong>' . htmlspecialchars_uni($item['title']) . "</strong></span><br /><br />{$content}<strong><span style=\"float: right;\">{$stamp}</span><br /><br /><a href=\"{$link}\" target=\"_blank\">&raquo; {$this->lang->read_more}</a></strong>");
                    $table->construct_row();
                }
            } else {
                $table->construct_cell("{$this->lang->error_fetch_news} <!-- error code: {$feed_parser->error} -->");
                $table->construct_row();
            }

            $this->cache->update('update_check', $updated_cache);

            $html .= $table->output($this->lang->latest_mybb_announcements);
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Home/ChkUpdates.html.twig');
        } elseif (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_home_index_start');

            if ($this->bb->request_method == 'post' && isset($this->bb->input['adminnotes'])) {
                // Update Admin Notes cache
                $update_cache = [
                    'adminmessage' => $this->bb->input['adminnotes']
                ];

                $this->cache->update('adminnotes', $update_cache);

                $this->plugins->runHooks('admin_home_index_start_begin');

                $this->session->flash_message($this->lang->success_notes_updated, 'success');
                return $response->withRedirect($this->bb->admin_url);
            }

            $this->page->add_breadcrumb_item($this->lang->dashboard);
//            $html = $this->page->output_header($this->lang->dashboard);
            $this->page->output_header($this->lang->dashboard);

            $sub_tabs['dashboard'] = [
                'title' => $this->lang->dashboard,
                'link' => $this->bb->admin_url,
                'description' => $this->lang->dashboard_description
            ];

//            $html .= $this->page->output_nav_tabs($sub_tabs, 'dashboard');
            $html = $this->page->output_nav_tabs($sub_tabs, 'dashboard');

            // Load stats cache
            $stats = $this->cache->read('stats');

            $serverload = get_server_load($this->lang->unknown);

            // Get the number of users
            $query = $this->db->simple_select('users', 'COUNT(uid) AS numusers');
            $users = $this->parser->formatNumber($this->db->fetch_field($query, 'numusers'));

            // Get the number of users awaiting validation
            $awaitingusers = $this->cache->read('awaitingactivation');

            if (!empty($awaitingusers['users'])) {
                $awaitingusers = (int)$awaitingusers['users'];
            } else {
                $awaitingusers = 0;
            }

            if ($awaitingusers < 1) {
                $awaitingusers = 0;
            } else {
                $awaitingusers = $this->parser->formatNumber($awaitingusers);
            }

            // Get the number of new users for today
            $timecut = TIME_NOW - 86400;
            $query = $this->db->simple_select('users', 'COUNT(uid) AS newusers', "regdate > '$timecut'");
            $newusers = $this->parser->formatNumber($this->db->fetch_field($query, 'newusers'));

            // Get the number of active users today
            $query = $this->db->simple_select('users', 'COUNT(uid) AS activeusers', "lastvisit > '$timecut'");
            $activeusers = $this->parser->formatNumber($this->db->fetch_field($query, 'activeusers'));

            // Get the number of threads
            $threads = $this->parser->formatNumber($stats['numthreads']);

            // Get the number of unapproved threads
            $unapproved_threads = $this->parser->formatNumber($stats['numunapprovedthreads']);

            // Get the number of new threads for today
            $query = $this->db->simple_select('threads', 'COUNT(*) AS newthreads', "dateline > '$timecut' AND visible='1' AND closed NOT LIKE 'moved|%'");
            $newthreads = $this->parser->formatNumber($this->db->fetch_field($query, 'newthreads'));

            // Get the number of posts
            $posts = $this->parser->formatNumber($stats['numposts']);

            // Get the number of unapproved posts
            if ($stats['numunapprovedposts'] < 0) {
                $stats['numunapprovedposts'] = 0;
            }

            $unapproved_posts = $this->parser->formatNumber($stats['numunapprovedposts']);

            // Get the number of new posts for today
            $query = $this->db->simple_select('posts', 'COUNT(*) AS newposts', "dateline > '$timecut' AND visible='1'");
            $newposts = $this->parser->formatNumber($this->db->fetch_field($query, 'newposts'));

            // Get the number of reported post
            $query = $this->db->simple_select('reportedcontent', 'COUNT(*) AS reported_posts', "type = 'post' OR type = ''");
            $reported_posts = $this->parser->formatNumber($this->db->fetch_field($query, 'reported_posts'));

            // Get the number of reported posts that haven't been marked as read yet
            $query = $this->db->simple_select('reportedcontent', 'COUNT(*) AS new_reported_posts', "reportstatus='0' AND (type = 'post' OR type = '')");
            $new_reported_posts = $this->parser->formatNumber($this->db->fetch_field($query, 'new_reported_posts'));

            // Get the number and total file size of attachments
            $query = $this->db->simple_select('attachments', 'COUNT(*) AS numattachs, SUM(filesize) as spaceused', "visible='1' AND pid > '0'");
            $attachs = $this->db->fetch_array($query);
            $attachs['spaceused'] = $this->parser->friendlySize($attachs['spaceused']);
            $approved_attachs = $this->parser->formatNumber($attachs['numattachs']);

            // Get the number of unapproved attachments
            $query = $this->db->simple_select('attachments', 'COUNT(*) AS numattachs', "visible='0' AND pid > '0'");
            $unapproved_attachs = $this->parser->formatNumber($this->db->fetch_field($query, 'numattachs'));

            // Fetch the last time an update check was run
            $update_check = $this->cache->read('update_check');

            // If last update check was greater than two weeks ago (14 days) show an alert
            if (isset($update_check['last_check']) && $update_check['last_check'] <= TIME_NOW - 60 * 60 * 24 * 14) {
                $this->lang->last_update_check_two_weeks = $this->lang->sprintf(
                    $this->lang->last_update_check_two_weeks,
                    $this->bb->admin_url . '?action=version_check'
                );
                $html .= $this->page->output_error("<p>{$this->lang->last_update_check_two_weeks}</p>");
            }

            // If the update check contains information about a newer version, show an alert
            if (isset($update_check['latest_version_code']) && $update_check['latest_version_code'] > $this->bb->version_code) {
                $this->lang->new_version_available = $this->lang->sprintf($this->lang->new_version_available, "MyBB {$this->bb->version}", "<a href=\"http://www.mybb.com/downloads\" target=\"_blank\">MyBB {$update_check['latest_version']}</a>");
                $html .= $this->page->output_error("<p><em>{$this->lang->new_version_available}</em></p>");
            }

            $this->plugins->runHooks('admin_home_index_output_message');

            $adminmessage = $this->cache->read('adminnotes');

            $table = new Table;
            $table->construct_header($this->lang->mybb_server_stats, ['colspan' => 2]);
            $table->construct_header($this->lang->forum_stats, ['colspan' => 2]);

            $table->construct_cell("<strong>{$this->lang->mybb_version}</strong>", ['width' => '25%']);
            $table->construct_cell($this->bb->version, ['width' => '25%']);
            $table->construct_cell("<strong>{$this->lang->threads}</strong>", ['width' => '25%']);
            $table->construct_cell("<strong>{$threads}</strong> {$this->lang->threads}<br /><strong>{$newthreads}</strong> {$this->lang->new_today}<br /><a href=\"".$this->bb->admin_url."/forum/mod_queue?type=threads\"><strong>{$unapproved_threads}</strong> {$this->lang->unapproved}</a>", ['width' => '25%']);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->php_version}</strong>", ['width' => '25%']);
            $table->construct_cell(PHP_VERSION, ['width' => '25%']);
            $table->construct_cell("<strong>{$this->lang->posts}</strong>", ['width' => '25%']);
            $table->construct_cell("<strong>{$posts}</strong> {$this->lang->posts}<br /><strong>{$newposts}</strong> {$this->lang->new_today}<br /><a href=\"".$this->bb->admin_url."/forum/mod_queue?type=posts\"><strong>{$unapproved_posts}</strong> {$this->lang->unapproved}</a><br /><strong>{$reported_posts}</strong> {$this->lang->reported_posts}<br /><strong>{$new_reported_posts}</strong> {$this->lang->unread_reports}", ['width' => '25%']);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->sql_engine}</strong>", ['width' => '25%']);
            $table->construct_cell($this->db->short_title . ' ' . $this->db->get_version(), ['width' => '25%']);
            $table->construct_cell("<strong>{$this->lang->users}</strong>", ['width' => '25%']);
            $table->construct_cell("<a href=\"".$this->bb->admin_url."/users\"><strong>{$users}</strong> {$this->lang->registered_users}</a><br /><strong>{$activeusers}</strong> {$this->lang->active_users}<br /><strong>{$newusers}</strong> {$this->lang->registrations_today}<br /><a href=\"".$this->bb->admin_url.'/users?action=search&results=1&conditions=' . urlencode(my_serialize(['usergroup' => '5'])) . "&from=home\"><strong>{$awaitingusers}</strong> {$this->lang->awaiting_activation}</a>", ['width' => '25%']);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->server_load}</strong>", ['width' => '25%']);
            $table->construct_cell($serverload, ['width' => '25%']);
            $table->construct_cell("<strong>{$this->lang->attachments}</strong>", ['width' => '25%']);
            $table->construct_cell("<strong>{$approved_attachs}</strong> {$this->lang->attachments}<br /><a href=\"".$this->bb->admin_url."/forum/mod_queue?type=attachments\"><strong>{$unapproved_attachs}</strong> {$this->lang->unapproved}</a><br /><strong>{$attachs['spaceused']}</strong> {$this->lang->used}", ['width' => '25%']);
            $table->construct_row();

            $html .= $table->output($this->lang->dashboard);

            $html .= '<div class="float_right" style="width: 48%;">';

            $table = new Table;
            $table->construct_header($this->lang->admin_notes_public);

            $form = new Form($this->bb, $this->bb->admin_url, 'post');
            $html .= $form->getForm();

            $table->construct_cell($form->generate_text_area('adminnotes', $adminmessage['adminmessage'], ['style' => 'width: 99%; height: 200px;']));
            $table->construct_row();

            $html .= $table->output($this->lang->admin_notes);

            $buttons[] = $form->generate_submit_button($this->lang->save_notes);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $html .= '</div><div class="float_left" style="width: 48%;">';

            // Latest news widget
            $table = new Table;
            $table->construct_header($this->lang->news_description);

            if (!empty($update_check['news']) && is_array($update_check['news'])) {
                foreach ($update_check['news'] as $news_item) {
                    $posted = Carbon::createFromTimestamp($news_item['dateline'])->toDayDateTimeString();
                    $table->construct_cell("<strong><a href=\"{$news_item['link']}\" target=\"_blank\">{$news_item['title']}</a></strong><br /><span class=\"smalltext\">{$posted}</span>");
                    $table->construct_row();

                    $table->construct_cell($news_item['description']);
                    $table->construct_row();
                }
            } else {
                $table->construct_cell($this->lang->no_announcements);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->latest_mybb_announcements);
            $html .= '</div>';

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Home/Index.html.twig');
        } else {
            $this->session->flash_message('No action found :(', 'error');
            return $response->withRedirect($this->bb->admin_url);
        }
    }
}
