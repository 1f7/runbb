<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Home;

class Meta
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * @return bool true
     */
    public function home_meta()
    {
        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'dashboard', 'title' => $this->bb->lang->dashboard, 'link' => $this->bb->admin_url];
        $sub_menu['20'] = ['id' => 'preferences', 'title' => $this->bb->lang->preferences, 'link' => $this->bb->admin_url . '/preferences'];
        $sub_menu['30'] = ['id' => 'docs', 'title' => $this->bb->lang->mybb_documentation, 'link' => 'http://docs.mybb.com'];
        $sub_menu['40'] = ['id' => 'credits', 'title' => $this->bb->lang->mybb_credits, 'link' => $this->bb->admin_url . '/credits'];
        $sub_menu = $this->bb->plugins->runHooks('admin_home_menu', $sub_menu);

        $this->bb->page->add_menu_item($this->bb->lang->home, 'home', $this->bb->admin_url, 1, $sub_menu);

        return true;
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function home_action_handler($action)
    {
        $this->bb->page->active_module = 'home';

        $actions = [
            'preferences' => ['active' => 'preferences', 'file' => 'preferences.php'],
            'credits' => ['active' => 'credits', 'file' => 'credits.php'],
            'version_check' => ['active' => 'version_check', 'file' => 'version_check.php'],
            'dashboard' => ['active' => 'dashboard', 'file' => $this->bb->admin_url]
        ];
        if (!isset($actions[$action])) {
            $this->bb->page->active_action = 'dashboard';
        } else {
            $this->bb->page->active_action = $actions[$action]['active'];
        }

        $actions = $this->bb->plugins->runHooks('admin_home_action_handler', $actions);

        if ($this->bb->page->active_action == 'dashboard') {
            // Quick Access
            $sub_menu = [];
            $sub_menu['10'] = ['id' => 'add_forum', 'title' => $this->bb->lang->add_new_forum, 'link' => $this->bb->admin_url . '/forum?action=add', 'module' => 'forums', 'action' => 'management'];
            $sub_menu['20'] = ['id' => 'search', 'title' => $this->bb->lang->search_for_users, 'link' => $this->bb->admin_url . '/users?action=search', 'module' => 'users', 'action' => 'users'];
            $sub_menu['30'] = ['id' => 'themes', 'title' => $this->bb->lang->themes, 'link' => $this->bb->admin_url . '/style', 'module' => 'style', 'action' => 'themes'];
            $sub_menu['40'] = ['id' => 'templates', 'title' => $this->bb->lang->templates, 'link' => $this->bb->admin_url . '/style/templates', 'module' => 'style', 'action' => 'templates'];
            $sub_menu['50'] = ['id' => 'plugins', 'title' => $this->bb->lang->plugins, 'link' => $this->bb->admin_url . '/config/plugins', 'module' => 'configs', 'action' => 'plugins'];
            $sub_menu['60'] = ['id' => 'backupdb', 'title' => $this->bb->lang->database_backups, 'link' => $this->bb->admin_url . '/tools/backupdb', 'module' => 'tools', 'action' => 'backupdb'];

            foreach ($sub_menu as $id => $sub) {
                if (!$this->bb->adm->check_admin_permissions(['module' => $sub['module'], 'action' => $sub['action']], false)) {
                    unset($sub_menu[$id]);
                }
            }

            $sub_menu = $this->bb->plugins->runHooks('admin_home_menu_quick_access', $sub_menu);
            if (!empty($sub_menu)) {
                $sidebar = new \RunBB\Admin\Helpers\SidebarItem($this->bb, $this->bb->lang->quick_access);
                $sidebar->add_menu_items($sub_menu, $this->bb->page->active_action, $this->bb->lang->quick_access);
                $this->bb->page->sidebar .= $sidebar->get_markup();
            }

            // Online Administrators in the last 30 minutes
            $timecut = TIME_NOW - 60 * 30;
            $query = $this->bb->db->simple_select('adminsessions', 'uid, ip, useragent', "lastactive > {$timecut}");
            $online_users = '<ul class="menu online_admins">';
            $online_admins = [];

            // If there's only 1 user online, it has to be us.
            if ($this->bb->db->num_rows($query) == 1) {
                $user = $this->bb->db->fetch_array($query);
                // Are we on a mobile device?
                // Stolen from http://stackoverflow.com/a/10989424
                $user_type = 'desktop';
                if ($this->bb->is_mobile($user['useragent'])) {
                    $user_type = 'mobile';
                }

                $online_admins[$this->bb->user->username] = [
                    'uid' => $this->bb->user->uid,
                    'username' => $this->bb->user->username,
                    'ip' => $user['ip'],
                    'type' => $user_type
                ];
            } else {
                $uid_in = [];
                while ($user = $this->bb->db->fetch_array($query)) {
                    $uid_in[] = $user['uid'];

                    $user_type = 'desktop';
                    if ($this->bb->is_mobile($user['useragent'])) {
                        $user_type = 'mobile';
                    }

                    $online_admins[$user['uid']] = [
                        'ip' => $user['ip'],
                        'type' => $user_type
                    ];
                }

//                if(!empty($uid_in)) {
                $uid_in = empty($uid_in) ? $uid_in = [0 => 0] : $uid_in;
                    $query = $this->bb->db->simple_select(
                        'users',
                        'uid, username',
                        'uid IN(' . implode(',', $uid_in) . ')',
                        ['order_by' => 'username']
                    );
                while ($user = $this->bb->db->fetch_array($query)) {
                    $online_admins[$user['username']] = [
                    'uid' => $user['uid'],
                    'username' => $user['username'],
                    'ip' => $online_admins[$user['uid']]['ip'],
                    'type' => $online_admins[$user['uid']]['type']
                    ];
                    unset($online_admins[$user['uid']]);
                }
//                }
            }

            $done_users = [];

            asort($online_admins);

            foreach ($online_admins as $user) {
                if (!isset($done_users["{$user['uid']}.{$user['ip']}"])) {
                    if ($user['type'] == 'mobile') {
                        $class = ' class="mobile_user"';
                    } else {
                        $class = '';
                    }
                    $ip_address = $user['ip'];
                    $online_users .= "<li title=\"{$this->bb->lang->ipaddress} {$ip_address}\"{$class}>" . $this->bb->user->build_profile_link($user['username'] . ' (' . $ip_address . ')', $user['uid'], "_blank") . "</li>";
                    $done_users["{$user['uid']}.{$user['ip']}"] = 1;
                }
            }
            $online_users .= '</ul>';
            if (!isset($sidebar)) {
                $sidebar = new \RunBB\Admin\Helpers\SidebarItem($this->bb, $this->bb->lang->online_admins);
            }
            $sidebar->set_contents($online_users);

            $this->bb->page->sidebar .= $sidebar->get_markup();
        }

        //forum-dashboard see in \RunBB\Init
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('forum-dashboard');

        if (isset($actions[$action])) {
            $this->bb->page->active_action = $actions[$action]['active'];
            return $actions[$action]['file'];
        } else {
            $this->bb->page->active_action = 'dashboard';
            return 'index';
        }
    }
}
