<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Home;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunCMF\Core\AbstractController;

class Preferences extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init();
        $this->lang->load('home_preferences', false, true);

        $this->page->add_breadcrumb_item($this->lang->preferences_and_personal_notes, $this->bb->admin_url . '/preferences');//'index.php?module=home-preferences');

        $this->plugins->runHooks('admin_home_preferences_begin');

        if ($this->bb->input['action'] == 'recovery_codes') {
            $this->page->add_breadcrumb_item(
                $this->lang->recovery_codes,
                $this->bb->admin_url . '/preferences?action=recovery_codes'
            );

            // First: regenerate the codes
            $codes = $this->generate_recovery_codes();
            $this->db->update_query('adminoptions', ['recovery_codes' => $this->db->escape_string(my_serialize($codes))], "uid='{$this->user->uid}'");

            // And now display them
            $this->page->output_header($this->lang->recovery_codes);

            $table = new Table;
            $table->construct_header($this->lang->recovery_codes);

            $table->construct_cell($this->lang->recovery_codes_warning);
            $table->construct_row();

            $table->construct_cell(implode('<br />', $codes));
            $table->construct_row();

            echo $table->output($this->lang->recovery_codes);

            $this->page->output_footer();
        }
        if (!$this->bb->input['action']) {
            $auth = new \RunBB\Helpers\GoogleAuthenticator;

            $this->plugins->runHooks('admin_home_preferences_start');

            if ($this->bb->request_method == 'post') {
                $query = $this->db->simple_select(
                    'adminoptions',
                    'permissions, defaultviews, authsecret, recovery_codes',
                    "uid='{$this->user->uid}'"
                );
                $adminopts = $this->db->fetch_array($query);

                $secret = $adminopts['authsecret'];
                // Was the option changed? empty = disabled so ==
                if ($this->bb->input['2fa'] == empty($secret)) {
                    // 2FA was enabled -> create secret and log
                    if ($this->bb->input['2fa']) {
                        $secret = $auth->createSecret();
                        // We don't want to close this session now
                        $this->db->update_query(
                            'adminsessions',
                            ['authenticated' => 1],
                            "sid='" . $this->db->escape_string($this->bb->cookies['adminsid']) . "'"
                        );
                        $this->bblogger->log_admin_action('enabled');
                    } // 2FA was disabled -> clear secret
                    else {
                        $secret = '';
                        $adminopts['recovery_codes'] = '';
                        $this->bblogger->log_admin_action('disabled');
                    }
                }

                $sqlarray = [
                    'notes' => $this->db->escape_string($this->bb->input['notes']),
                    'cpstyle' => $this->db->escape_string($this->bb->input['cpstyle']),
                    'cplanguage' => $this->db->escape_string($this->bb->input['cplanguage']),
                    'permissions' => $this->db->escape_string($adminopts['permissions']),
                    'defaultviews' => $this->db->escape_string($adminopts['defaultviews']),
                    'uid' => $this->user->uid,
                    'codepress' => $this->bb->getInput('codepress', 0), // It's actually CodeMirror but for compatibility purposes lets leave it codepress
                    'authsecret' => $this->db->escape_string($secret),
                    'recovery_codes' => $this->db->escape_string($adminopts['recovery_codes']),
                ];

                $this->db->replace_query('adminoptions', $sqlarray, 'uid');

                $this->plugins->runHooks('admin_home_preferences_start_commit');

                $this->session->flash_message($this->lang->success_preferences_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/preferences');//'index.php?module=home-preferences');
            }

            $this->page->output_header($this->lang->preferences_and_personal_notes);

            $sub_tabs['preferences'] = [
                'title' => $this->lang->preferences_and_personal_notes,
                'link' => $this->bb->admin_url . '/preferences',//'index.php?module=home-preferences',
                'description' => $this->lang->prefs_and_personal_notes_description
            ];

            $html = $this->page->output_nav_tabs($sub_tabs, 'preferences');

            $query = $this->db->simple_select('adminoptions', 'notes, cpstyle, cplanguage, codepress, authsecret', "uid='" . $this->user->uid . "'", ['limit' => 1]);
            $admin_options = $this->db->fetch_array($query);

            $form = new Form($this->bb, $this->bb->admin_url . '/preferences', 'post');
            $html .= $form->getForm();

            $dir = @opendir(DIR . 'web/assets/runbb/admin/styles');

            $folders = [];
            while ($folder = readdir($dir)) {
                if ($folder != '.' && $folder != '..' && @file_exists(DIR . 'web/assets/runbb/admin/styles/' . $folder . '/main.css')) {
                    $folders[$folder] = ucfirst($folder);
                }
            }
            closedir($dir);
            ksort($folders);
            $setting_code = $form->generate_select_box('cpstyle', $folders, $admin_options['cpstyle']);

            $languages = array_merge(['' => $this->lang->use_default], $this->lang->get_languages());
            $language_code = $form->generate_select_box('cplanguage', $languages, $admin_options['cplanguage']);

            $table = new Table;
            $table->construct_header($this->lang->global_preferences);

            $table->construct_cell("<strong>{$this->lang->acp_theme}</strong><br /><small>{$this->lang->select_acp_theme}</small><br /><br />{$setting_code}");
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->acp_language}</strong><br /><small>{$this->lang->select_acp_language}</small><br /><br />{$language_code}");
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->codemirror}</strong><br /><small>{$this->lang->use_codemirror_desc}</small><br /><br />" . $form->generate_on_off_radio('codepress', $admin_options['codepress']));
            $table->construct_row();

            // If 2FA is enabled we need to display a link to the recovery codes page
            if (!empty($admin_options['authsecret'])) {
                $this->lang->use_2fa_desc .= '<br />' . $this->lang->recovery_codes_desc . ' ' . $this->lang->recovery_codes_warning;
            }

            $table->construct_cell("<strong>{$this->lang->my2fa}</strong><br /><small>{$this->lang->use_2fa_desc}</small><br /><br />" . $form->generate_on_off_radio('2fa', (int)!empty($admin_options['authsecret'])));
            $table->construct_row();

            if (!empty($admin_options['authsecret'])) {
                $qr = $auth->getQRCodeGoogleUrl($this->user->username . '@' . str_replace(' ', '', $this->bb->settings['bbname']), $admin_options['authsecret']);
                $table->construct_cell("<strong>{$this->lang->my2fa_qr}</strong><br /><img src=\"{$qr}\"");
                $table->construct_row();
            }

            $html .= $table->output($this->lang->preferences);

            $table->construct_header($this->lang->notes_not_shared);

            $table->construct_cell($form->generate_text_area('notes', $admin_options['notes'], ['style' => 'width: 99%; height: 300px;']));
            $table->construct_row();

            $html .= $table->output($this->lang->personal_notes);

            $buttons[] = $form->generate_submit_button($this->lang->save_notes_and_prefs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Home/PrefIndex.html.twig');
        }
    }

    /**
     * Generate 10 random recovery codes, each with a length of 6 and without duplicates
     *
     * @return array
     */
    private function generate_recovery_codes()
    {
        $t = [];
        while (count($t) < 10) {
            $g = random_str(6);
            if (!in_array($g, $t)) {
                $t[] = $g;
            }
        }
        return $t;
    }
}
