<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Style;

class Meta
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * @return bool true
     */
    public function style_meta()
    {
        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'themes', 'title' => $this->bb->lang->themes, 'link' => $this->bb->admin_url . '/style'];
        $sub_menu['20'] = ['id' => 'templates', 'title' => $this->bb->lang->templates, 'link' => $this->bb->admin_url . '/style/templates'];

        $sub_menu = $this->bb->plugins->runHooks('admin_style_menu', $sub_menu);

        $this->bb->page->add_menu_item($this->bb->lang->templates, 'style', $this->bb->admin_url . '/style', 40, $sub_menu);
        return true;
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function style_action_handler($action)
    {
        $this->bb->page->active_module = 'style';

        $actions = [
            'templates' => ['active' => 'templates', 'file' => 'templates.php'],
            'themes' => ['active' => 'themes', 'file' => 'themes.php']
        ];

        $actions = $this->bb->plugins->runHooks('admin_style_action_handler', $actions);

        //\RunBB\Init
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('forum-style');

        if (isset($actions[$action])) {
            $this->bb->page->active_action = $actions[$action]['active'];
            return $actions[$action]['file'];
        } else {
            $this->bb->page->active_action = 'themes';
            return 'themes.php';
        }
    }

    /**
     * @return array
     */
    public function style_admin_permissions()
    {
        $admin_permissions = [
            'themes' => $this->bb->lang->can_manage_themes,
            'templates' => $this->bb->lang->can_manage_templates,
        ];

        $admin_permissions = $this->bb->plugins->runHooks('admin_style_permissions', $admin_permissions);

        return ['name' => $this->bb->lang->templates, 'permissions' => $admin_permissions, 'disporder' => 40];
    }
}
