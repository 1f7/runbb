<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Style;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Templates extends AbstractController
{
    private $errors = [];

    public function index(Request $request, Response $response)
    {
        $this->adm->init('style');// set active module
        $this->lang->load('style_templates', false, true);

        $this->page->add_breadcrumb_item($this->lang->template_sets, $this->bb->admin_url . '/style/templates');

        $sid = $this->bb->getInput('sid', 0);

        $expand_str = '';
        $expand_str2 = '';
        $expand_array = [];
        if (isset($this->bb->input['expand'])) {
            $expand_array = explode('|', $this->bb->input['expand']);
            $expand_array = array_map('intval', $expand_array);
            $expand_str = '&amp;expand=' . implode('|', $expand_array);
            $expand_str2 = '&expand=' . implode('|', $expand_array);
        }

        if ($this->bb->input['action'] == 'add_set' ||
            $this->bb->input['action'] == 'add_template' ||
            ($this->bb->input['action'] == 'add_template_group' && !$sid) ||
            $this->bb->input['action'] == 'search_replace' ||
            $this->bb->input['action'] == 'find_updated' ||
            (!$this->bb->input['action'] && !$sid)
        ) {
            $sub_tabs['templates'] = [
                'title' => $this->lang->manage_template_sets,
                'link' => $this->bb->admin_url . '/style/templates',
                'description' => $this->lang->manage_template_sets_desc
            ];

            $sub_tabs['add_set'] = [
                'title' => $this->lang->add_set,
                'link' => $this->bb->admin_url . '/style/templates?action=add_set' . $expand_str
            ];

            if ($this->bb->getInput('sid', 0) != -1) {
                $sub_tabs['add_template_group'] = [
                    'title' => $this->lang->add_template_group,
                    'link' => $this->bb->admin_url . '/style/templates?action=add_template_group' . $expand_str,
                    'description' => $this->lang->add_template_group_desc
                ];
            }

            $sub_tabs['search_replace'] = [
                'title' => $this->lang->search_replace,
                'link' => $this->bb->admin_url . '/style/templates?action=search_replace',
                'description' => $this->lang->search_replace_desc
            ];

            $sub_tabs['find_updated'] = [
                'title' => $this->lang->find_updated,
                'link' => $this->bb->admin_url . '/style/templates?action=find_updated',
                'description' => $this->lang->find_updated_desc
            ];
        } elseif (($sid && !$this->bb->input['action']) ||
            $this->bb->input['action'] == 'edit_set' ||
            $this->bb->input['action'] == 'check_set' ||
            $this->bb->input['action'] == 'edit_template' ||
            $this->bb->input['action'] == 'add_template_group'
        ) {
            $sub_tabs['manage_templates'] = [
                'title' => $this->lang->manage_templates,
                'link' => $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str,
                'description' => $this->lang->manage_templates_desc
            ];

            if ($sid > 0) {
                $sub_tabs['edit_set'] = [
                    'title' => $this->lang->edit_set,
                    'link' => $this->bb->admin_url . '/style/templates?action=edit_set&sid=' . $sid . $expand_str,
                    'description' => $this->lang->edit_set_desc
                ];
            }

            $sub_tabs['add_template'] = [
                'title' => $this->lang->add_template,
                'link' => $this->bb->admin_url . '/style/templates?action=add_template&sid=' . $sid . $expand_str,
                'description' => $this->lang->add_template_desc
            ];

            if ($this->bb->getInput('sid', 0) != -1) {
                $sub_tabs['add_template_group'] = [
                    'title' => $this->lang->add_template_group,
                    'link' => $this->bb->admin_url . '/style/templates?action=add_template_group&sid=' . $sid . $expand_str,
                    'description' => $this->lang->add_template_group_desc
                ];
            }
        }

        $template_sets = [];
        $template_sets[-1] = $this->lang->global_templates;

        $query = $this->db->simple_select('templatesets', '*', '', ['order_by' => 'title', 'order_dir' => 'ASC']);
        while ($template_set = $this->db->fetch_array($query)) {
            $template_sets[$template_set['sid']] = $template_set['title'];
        }

        $this->plugins->runHooks('admin_style_templates');

        if ($this->bb->input['action'] == 'add_set') {
            $this->plugins->runHooks('admin_style_templates_add_set');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $this->errors[] = $this->lang->error_missing_set_title;
                }

                if (empty($this->errors)) {
                    $sid = $this->db->insert_query('templatesets', ['title' => $this->db->escape_string($this->bb->input['title'])]);

                    $this->plugins->runHooks('admin_style_templates_add_set_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($sid, $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_template_set_saved, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid);
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_set);

            $html = $this->page->output_header($this->lang->add_set);

            $sub_tabs = [];
            $sub_tabs['add_set'] = [
                'title' => $this->lang->add_set,
                'link' => $this->bb->admin_url . '/style/templates?action=add_set',
                'description' => $this->lang->add_set_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_set');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            } else {
                $this->bb->input['title'] = '';
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=add_set', 'post', 'add_set');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->add_set);
            $form_container->output_row($this->lang->title, '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $html .= $form_container->end();

            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->save);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesAddSet.html.twig');
        }

        if ($this->bb->input['action'] == 'add_template') {
            $this->plugins->runHooks('admin_style_templates_add_template');

            if ($this->bb->request_method == 'post') {
                if (empty($this->bb->input['title'])) {
                    $this->errors[] = $this->lang->error_missing_set_title;
                } else {
                    $query = $this->db->simple_select('templates', 'COUNT(tid) as count', "title='" . $this->db->escape_string($this->bb->input['title']) . "' AND (sid = '-2' OR sid = '{$sid}')");
                    if ($this->db->fetch_field($query, 'count') > 0) {
                        $this->errors[] = $this->lang->error_already_exists;
                    }
                }

                if (!isset($template_sets[$sid])) {
                    $this->errors[] = $this->lang->error_invalid_set;
                }

                // Are we trying to do malicious things in our template?
                if ($this->adm->check_template($this->bb->input['template'])) {
                    $this->errors[] = $this->lang->error_security_problem;
                }

                if (empty($this->errors)) {
                    $template_array = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'sid' => $sid,
                        'template' => $this->db->escape_string(rtrim($this->bb->input['template'])),
                        'version' => $this->db->escape_string($this->bb->version_code),
                        'status' => '',
                        'dateline' => TIME_NOW
                    ];

                    $tid = $this->db->insert_query('templates', $template_array);

                    $this->plugins->runHooks('admin_style_templates_add_template_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($tid, $this->bb->input['title'], $sid, $template_sets[$sid]);

                    $this->session->flash_message($this->lang->success_template_saved, 'success');

                    if ($this->bb->input['continue']) {
                        return $response->withRedirect($this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($this->bb->input['title']) . '&sid=' . $sid . $expand_str2);
                    } else {
                        return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str2);
                    }
                }
            }

            if (!empty($this->errors)) {
                $template = $this->bb->input;
            } else {
                if (!$sid) {
                    $sid = -1;
                }

                $template['template'] = '';
                $template['sid'] = $sid;
            }

            if ($this->bb->input['sid']) {
                $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            }

            if ($this->adm->admin_options['codepress'] != 0) {
                $this->page->extra_header .= '
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.css" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/theme/mybb.css?ver=1804" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/xml/xml.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/javascript/javascript.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/css/css.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog-mybb.css" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/searchcursor.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/search.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldcode.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/xml-fold.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldgutter.js"></script>
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldgutter.css" rel="stylesheet">
';
            }

            $this->page->add_breadcrumb_item($this->lang->add_template);

            $html = $this->page->output_header($this->lang->add_template);

            $sub_tabs = [];
            $sub_tabs['add_template'] = [
                'title' => $this->lang->add_template,
                'link' => $this->bb->admin_url . '/style/templates?action=add_template&sid=' . $template['sid'] . $expand_str,
                'description' => $this->lang->add_template_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_template');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=add_template' . $expand_str, 'post', 'add_template');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->add_template, 'tfixed');
            $form_container->output_row($this->lang->template_name, $this->lang->template_name_desc, $form->generate_text_box('title', $template['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->template_set, $this->lang->template_set_desc, $form->generate_select_box('sid', $template_sets, $sid), 'sid');
            $form_container->output_row('', '', $form->generate_text_area('template', $template['template'], ['id' => 'template', 'class' => '', 'style' => 'width: 100%; height: 500px;']), 'template');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_continue, ['name' => 'continue']);
            $buttons[] = $form->generate_submit_button($this->lang->save_close, ['name' => 'close']);

            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();

            if ($this->adm->admin_options['codepress'] != 0) {
                $html .= '<script type="text/javascript">
			var editor = CodeMirror.fromTextArea(document.getElementById("template"), {
				lineNumbers: true,
				lineWrapping: true,
				foldGutter: true,
				gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
				viewportMargin: Infinity,
				indentWithTabs: true,
				indentUnit: 4,
				mode: "text/html",
				theme: "mybb"
			});
		</script>';
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesAddTemplate.html.twig');
        }

        if ($this->bb->input['action'] == 'add_template_group') {
            $this->plugins->runHooks('admin_style_templates_add_template_group');

            if ($this->bb->getInput('sid', 0) == -1) {
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str2);
            }

            $this->errors = [];
            if ($this->bb->request_method == 'post') {
                $prefix = trim($this->bb->input['prefix']);
                if (!$prefix) {
                    $this->errors[] = $this->lang->error_missing_group_prefix;
                }

                $title = trim($this->bb->input['title']);
                if (!$title) {
                    $this->errors[] = $this->lang->error_missing_group_title;
                }

                if (empty($this->errors)) {
                    $query = $this->db->simple_select('templategroups', 'COUNT(gid) AS gid', "prefix = '" . $this->db->escape_string($this->bb->input['prefix']) . "'");
                    $prefix_count = $this->db->fetch_field($query, 'gid');

                    if ($prefix_count >= 1) {
                        $this->errors[] = $this->lang->error_duplicate_group_prefix;
                    } else {
                        // Add template group
                        $insert_array = [
                            'prefix' => $this->db->escape_string($prefix),
                            'title' => $this->db->escape_string($title),
                            'isdefault' => 0
                        ];

                        $gid = $this->db->insert_query('templategroups', $insert_array);

                        $this->plugins->runHooks('admin_style_templates_add_template_group_commit');

                        $this->bblogger->log_admin_action($gid, $title);
                        $this->session->flash_message($this->lang->success_template_group_saved, 'success');

                        if ($sid) {
                            return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str2);
                        }

                        return $response->withRedirect($this->bb->admin_url . '/style/templates');
                    }
                }
            }

            if ($this->bb->input['sid']) {
                $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            }

            $this->page->add_breadcrumb_item($this->lang->add_template_group);
            $html = $this->page->output_header($this->lang->add_template_group);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_template_group');

            if (!empty($this->errors)) {
                $template_group = [
                    'prefix' => $prefix,
                    'title' => $title
                ];

                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=add_template_group' . $expand_str, 'post', 'add_template_group');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('sid', $sid);

            $form_container = new FormContainer($this, $this->lang->add_template_group);
            $form_container->output_row($this->lang->template_group_prefix, $this->lang->template_group_prefix_desc, $form->generate_text_box('prefix', $template_group['prefix'], ['id' => 'prefix']), 'prefix');
            $form_container->output_row($this->lang->template_group_title, $this->lang->template_group_title_desc, $form->generate_text_box('title', $template_group['title'], ['id' => 'title']), 'title');
            $html .= $form_container->end();

            $buttons = [
                $form->generate_submit_button($this->lang->add_template_group)
            ];

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesAddTemplateGroup.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_set') {
            $query = $this->db->simple_select('templatesets', '*', "sid='{$sid}'");
            $set = $this->db->fetch_array($query);
            if (!$set) {
                $this->session->flash_message($this->lang->error_invalid_input, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            $this->plugins->runHooks('admin_style_templates_edit_set');

            $sid = $set['sid'];

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $this->errors[] = $this->lang->error_missing_set_title;
                }

                if (empty($this->errors)) {
                    $query = $this->db->update_query('templatesets', ['title' => $this->db->escape_string($this->bb->input['title'])], "sid='{$sid}'");

                    $this->plugins->runHooks('admin_style_templates_edit_set_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($sid, $set['title']);

                    $this->session->flash_message($this->lang->success_template_set_saved, 'success');

                    return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str2);
                }
            }

            if ($sid) {
                $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            }

            $this->page->add_breadcrumb_item($this->lang->edit_set);

            $html = $this->page->output_header($this->lang->edit_set);

            $sub_tabs = [];
            $sub_tabs['edit_set'] = [
                'title' => $this->lang->edit_set,
                'link' => $this->bb->admin_url . '/style/templates?action=edit_set&sid=' . $sid,
                'description' => $this->lang->edit_set_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_set');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            } else {
                $query = $this->db->simple_select('templatesets', 'title', "sid='{$sid}'");
                $this->bb->input['title'] = $this->db->fetch_field($query, 'title');
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=edit_set' . $expand_str, 'post', 'edit_set');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('sid', $sid);

            $form_container = new FormContainer($this, $this->lang->edit_set);
            $form_container->output_row($this->lang->title, '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $html .= $form_container->end();

            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->save);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesEditSet.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_template') {
            if (!$this->bb->input['title'] || !$sid || !isset($template_sets[$sid])) {
                $this->session->flash_message($this->lang->error_missing_input, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            $this->plugins->runHooks('admin_style_templates_edit_template');

            if ($this->bb->request_method == 'post') {
                if (empty($this->bb->input['title'])) {
                    $this->errors[] = $this->lang->error_missing_title;
                }

                // Are we trying to do malicious things in our template?
                if ($this->adm->check_template($this->bb->input['template'])) {
                    $this->errors[] = $this->lang->error_security_problem;
                }

                if (empty($this->errors)) {
                    $query = $this->db->simple_select('templates', '*', "tid='{$this->bb->input['tid']}'");
                    $template = $this->db->fetch_array($query);

                    $template_array = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'sid' => $sid,
                        'template' => $this->db->escape_string(rtrim($this->bb->input['template'])),
                        'version' => $this->bb->version_code,
                        'status' => '',
                        'dateline' => TIME_NOW
                    ];

                    // Make sure we have the correct tid associated with this template. If the user double submits then the tid could originally be the master template tid, but because the form is sumbitted again, the tid doesn't get updated to the new modified template one. This then causes the master template to be overwritten
                    $query = $this->db->simple_select('templates', 'tid', "title='" . $this->db->escape_string($template['title']) . "' AND (sid = '-2' OR sid = '{$template['sid']}')", ['order_by' => 'sid', 'order_dir' => 'desc', 'limit' => 1]);
                    $template['tid'] = $this->db->fetch_field($query, 'tid');

                    $this->plugins->runHooks('admin_style_templates_edit_template_commit_start');

                    if ($sid > 0) {
                        // Check to see if it's never been edited before (i.e. master) or if this a new template (i.e. we've renamed it)  or if it's a custom template
                        $query = $this->db->simple_select('templates', 'sid', "title='" . $this->db->escape_string($this->bb->input['title']) . "' AND (sid = '-2' OR sid = '{$sid}' OR sid='{$template['sid']}')", ['order_by' => 'sid', 'order_dir' => 'desc']);
                        $existing_sid = $this->db->fetch_field($query, 'sid');
                        $existing_rows = $this->db->num_rows($query);

                        if (($existing_sid == -2 && $existing_rows == 1) || $existing_rows == 0) {
                            $template['tid'] = $this->db->insert_query('templates', $template_array);
                        } else {
                            $this->db->update_query('templates', $template_array, "tid='{$template['tid']}' AND sid != '-2'");
                        }
                    } else {
                        // Global template set
                        $this->db->update_query('templates', $template_array, "tid='{$template['tid']}' AND sid != '-2'");
                    }

                    $this->plugins->runHooks('admin_style_templates_edit_template_commit');

                    $query = $this->db->simple_select('templatesets', 'title', "sid='{$sid}'");
                    $set = $this->db->fetch_array($query);

                    $exploded = explode('_', $template_array['title'], 2);
                    $prefix = $exploded[0];

                    $query = $this->db->simple_select('templategroups', 'gid', "prefix = '" . $this->db->escape_string($prefix) . "'");
                    $group = $this->db->fetch_field($query, 'gid');

                    if (!$group) {
                        $group = '-1';
                    }

                    // Log admin action
                    $this->bblogger->log_admin_action($template['tid'], $this->bb->input['title'], $this->bb->input['sid'], $set['title']);

                    $this->session->flash_message($this->lang->success_template_saved, 'success');

                    if ($this->bb->input['continue']) {
                        if ($this->bb->input['from'] == 'diff_report') {
                            return $response->withRedirect($this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($this->bb->input['title']) . '&sid=' . $this->bb->getInput('sid', 0) . $expand_str2 . '&amp;from=diff_report');
                        } else {
                            return $response->withRedirect($this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($this->bb->input['title']) . '&sid=' . $this->bb->getInput('sid', 0) . $expand_str2);
                        }
                    } else {
                        if ($this->bb->input['from'] == 'diff_report') {
                            return $response->withRedirect($this->bb->admin_url . '/style/templates?action=find_updated');
                        } else {
                            return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $this->bb->getInput('sid', 0) . $expand_str2 . '#group_' . $group);
                        }
                    }
                }
            }

            if (!empty($this->errors)) {
                $template = $this->bb->input;
            } else {
                $query = $this->db->simple_select('templates', '*', "title='" . $this->db->escape_string($this->bb->input['title']) . "' AND (sid='-2' OR sid='{$sid}')", ['order_by' => 'sid', 'order_dir' => 'DESC', 'limit' => 1]);
                $template = $this->db->fetch_array($query);
            }
            $template['title'] = htmlspecialchars_uni($template['title']);

            if ($this->adm->admin_options['codepress'] != 0) {
                $this->page->extra_header .= '
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.css" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/theme/mybb.css?ver=1804" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/xml/xml.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/javascript/javascript.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/css/css.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog-mybb.css" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/searchcursor.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/search.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldcode.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/xml-fold.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldgutter.js"></script>
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldgutter.css" rel="stylesheet">
';
            }

            $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);

            if (!isset($this->bb->input['from'])) {
                $this->bb->input['from'] = '';
            }

            if ($this->bb->input['from'] == 'diff_report') {
                $this->page->add_breadcrumb_item($this->lang->find_updated, $this->bb->admin_url . '/style/templates?action=find_updated');
            }

            $this->page->add_breadcrumb_item($this->lang->edit_template_breadcrumb . $template['title'], $this->bb->admin_url . '/style/templates?sid=' . $sid);

            $html = $this->page->output_header($this->lang->sprintf($this->lang->editing_template, $template['title']));


            $sub_tabs = [];

            if ($this->bb->input['from'] == 'diff_report') {
                $sub_tabs['find_updated'] = [
                    'title' => $this->lang->find_updated,
                    'link' => $this->bb->admin_url . '/style/templates?action=find_updated'
                ];

                $sub_tabs['diff_report'] = [
                    'title' => $this->lang->diff_report,
                    'link' => $this->bb->admin_url . '/style/templates?action=diff_report&title=' . $this->db->escape_string($template['title']) . '&sid1=' . (int)$template['sid'] . '&sid2=-2',
                ];
            }

            $sub_tabs['edit_template'] = [
                'title' => $this->lang->edit_template,
                'link' => $this->bb->admin_url . '/style/templates?action=edit_template&title=' . htmlspecialchars_uni($template['title']) . $expand_str,
                'description' => $this->lang->edit_template_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_template');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=edit_template' . $expand_str, 'post', 'edit_template');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', isset($template['tid'])?$template['tid']:'') . "\n";

            if ($this->bb->input['from'] == 'diff_report') {
                $html .= $form->generate_hidden_field('from', 'diff_report');
            }

            $form_container = new FormContainer($this, $this->lang->edit_template_breadcrumb . $template['title'], 'tfixed');
            $form_container->output_row($this->lang->template_name, $this->lang->template_name_desc, $form->generate_text_box('title', $template['title'], ['id' => 'title']), 'title');

            // Force users to save the default template to a specific set, rather than the 'global' templates - where they can delete it
            if (isset($template['sid']) && $template['sid'] == '-2') {
                unset($template_sets[-1]);
            }

            $form_container->output_row($this->lang->template_set, $this->lang->template_set_desc, $form->generate_select_box('sid', $template_sets, $sid));

            $form_container->output_row('', '', $form->generate_text_area('template', (isset($template['template'])?$template['template']:'NoTemplate'), ['id' => 'template', 'class' => '', 'style' => 'width: 100%; height: 500px;']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_continue, ['name' => 'continue']);
            $buttons[] = $form->generate_submit_button($this->lang->save_close, ['name' => 'close']);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            if ($this->adm->admin_options['codepress'] != 0) {
                $html .= '<script type="text/javascript">
			var editor = CodeMirror.fromTextArea(document.getElementById("template"), {
				lineNumbers: true,
				lineWrapping: true,
				foldGutter: true,
				gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
				viewportMargin: Infinity,
				indentWithTabs: true,
				indentUnit: 4,
				mode: "text/html",
				theme: "mybb"
			});
		</script>';
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesEditTemplate.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_template_group') {
            $query = $this->db->simple_select('templategroups', '*', "gid = '" . $this->bb->getInput('gid', 0) . "'");

            if (!$this->db->num_rows($query)) {
                $this->session->flash_message($this->lang->error_missing_template_group, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            }

            $template_group = $this->db->fetch_array($query);
            if (isset($template_group['isdefault']) && $template_group['isdefault'] == 1) {
                $this->session->flash_message($this->lang->error_default_template_group, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            }

            $this->plugins->runHooks('admin_style_templates_edit_template_group');

            $this->errors = [];
            if ($this->bb->request_method == 'post') {
                $prefix = trim($this->bb->input['prefix']);
                if (!$prefix) {
                    $this->errors[] = $this->lang->error_missing_group_prefix;
                }

                $title = trim($this->bb->input['title']);
                if (!$title) {
                    $this->errors[] = $this->lang->error_missing_group_title;
                }

                if (empty($this->errors)) {
                    if ($prefix != $template_group['prefix']) {
                        $query = $this->db->simple_select('templategroups', 'COUNT(gid) AS gid', "prefix = '" . $this->db->escape_string($this->bb->input['prefix']) . "'");
                        $prefix_count = $this->db->fetch_field($query, 'gid');

                        if ($prefix_count >= 1) {
                            $this->errors[] = $this->lang->error_duplicate_group_prefix;
                        }
                    }

                    if (empty($this->errors)) {
                        // Add template group
                        $update_array = [
                            'prefix' => $this->db->escape_string($prefix),
                            'title' => $this->db->escape_string($title),
                            'isdefault' => 0
                        ];

                        $this->plugins->runHooks('admin_style_templates_edit_template_group_commit');

                        $this->db->update_query('templategroups', $update_array, "gid = '{$template_group['gid']}'");

                        $this->bblogger->log_admin_action($template_group['gid'], htmlspecialchars_uni($title));
                        $this->session->flash_message($this->lang->success_template_group_saved, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid);
                    }
                }
            }

            $this->lang->editing_template_group = $this->lang->sprintf($this->lang->editing_template_group, htmlspecialchars_uni($template_group['title']));

            $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            $this->page->add_breadcrumb_item($this->lang->editing_template_group, $this->bb->admin_url . '/style/templates?sid=' . $sid);

            $html = $this->page->output_header($this->lang->editing_template_group);

            if (!empty($this->errors)) {
                $template_group['prefix'] = $prefix;
                $template_group['title'] = $title;

                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=edit_template_group', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('sid', $sid);
            $html .= $form->generate_hidden_field('gid', $template_group['gid']);

            $form_container = new FormContainer($this, $this->lang->edit_template_group);
            $form_container->output_row($this->lang->template_group_prefix, $this->lang->template_group_prefix_desc, $form->generate_text_box('prefix', $template_group['prefix'], ['id' => 'prefix']), 'prefix');
            $form_container->output_row($this->lang->template_group_title, $this->lang->template_group_title_desc, $form->generate_text_box('title', $template_group['title'], ['id' => 'title']), 'title');
            $html .= $form_container->end();

            $buttons = [
                $form->generate_submit_button($this->lang->save_template_group)
            ];

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesEditTemplateGroup.html.twig');
        }

        if ($this->bb->input['action'] == 'search_replace') {
            $this->plugins->runHooks('admin_style_templates_search_replace');

            if ($this->bb->request_method == 'post') {
                if ($this->bb->input['type'] == 'templates') {
                    // Search and replace in templates

                    if (!$this->bb->input['find']) {
                        $this->session->flash_message($this->lang->search_noneset, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/style/templates?action=search_replace');
                    } else {
                        $this->page->add_breadcrumb_item($this->lang->search_replace);

                        $html = $this->page->output_header($this->lang->search_replace);

                        $this->plugins->runHooks('admin_style_templates_search_replace_find');

                        $html .= $this->page->output_nav_tabs($sub_tabs, 'search_replace');

                        $templates_list = [];
                        $table = new Table;

                        $template_sets = [];

                        // Get the names of all template sets
                        $template_sets[-2] = $this->lang->master_templates;
                        $template_sets[-1] = $this->lang->global_templates;

                        $query = $this->db->simple_select('templatesets', 'sid, title');
                        while ($set = $this->db->fetch_array($query)) {
                            $template_sets[$set['sid']] = $set['title'];
                        }

                        // Select all templates with that search term
                        $query = $this->db->query('
					SELECT t.tid, t.title, t.sid, t.template
					FROM ' . TABLE_PREFIX . 'templates t
					LEFT JOIN ' . TABLE_PREFIX . 'templatesets s ON (t.sid=s.sid)
					LEFT JOIN ' . TABLE_PREFIX . "templates t2 ON (t.title=t2.title AND t2.sid='1')
					WHERE t.template LIKE '%" . $this->db->escape_string_like($this->bb->input['find']) . "%' AND NOT (t.sid = -2 AND (t2.tid) IS NOT NULL)
					ORDER BY t.title ASC
				");
                        if ($this->db->num_rows($query) == 0) {
                            $table->construct_cell($this->lang->sprintf($this->lang->search_noresults, htmlspecialchars_uni($this->bb->input['find'])), ['class' => 'align_center']);

                            $table->construct_row();

                            $html .= $table->output($this->lang->search_results);
                        } else {
                            $template_list = [];
                            while ($template = $this->db->fetch_array($query)) {
                                $template_list[$template['sid']][$template['title']] = $template;
                            }

                            $count = 0;

                            foreach ($template_list as $sid => $templates) {
                                ++$count;

                                $search_header = $this->lang->sprintf($this->lang->search_header, htmlspecialchars_uni($this->bb->input['find']), $template_sets[$sid]);
                                $table->construct_header($search_header, ['colspan' => 2]);

                                foreach ($templates as $title => $template) {
                                    // Do replacement
                                    $newtemplate = str_ireplace($this->bb->input['find'], $this->bb->input['replace'], $template['template']);
                                    if ($newtemplate != $template['template'] && $this->adm->check_template($newtemplate) === false) {
                                        // If the template is different, that means the search term has been found.
                                        if (trim($this->bb->input['replace']) != '') {
                                            if ($template['sid'] == -2) {
                                                // The template is a master template.  We have to make a new custom template.
                                                $new_template = [
                                                    'title' => $this->db->escape_string($title),
                                                    'template' => $this->db->escape_string($newtemplate),
                                                    'sid' => 1,
                                                    'version' => $this->bb->version_code,
                                                    'status' => '',
                                                    'dateline' => TIME_NOW
                                                ];
                                                $new_tid = $this->db->insert_query('templates', $new_template);
                                                $label = $this->lang->sprintf($this->lang->search_created_custom, $template['title']);
                                                $url = $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . '&amp;sid=1';
                                            } else {
                                                // The template is a custom template.  Replace as normal.
                                                // Update the template if there is a replacement term
                                                $updatedtemplate = [
                                                    'template' => $this->db->escape_string($newtemplate)
                                                ];
                                                $this->db->update_query('templates', $updatedtemplate, "tid='" . $template['tid'] . "'");
                                                $label = $this->lang->sprintf($this->lang->search_updated, $template['title']);
                                                $url = $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$template['sid']}";
                                            }
                                        } else {
                                            // Just show that the term was found
                                            if ($template['sid'] == -2) {
                                                $label = $this->lang->sprintf($this->lang->search_found, $template['title']);
                                            } else {
                                                $label = $this->lang->sprintf($this->lang->search_found, $template['title']);
                                                $url = $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$template['sid']}";
                                            }
                                        }
                                    } else {
                                        // Just show that the term was found
                                        if ($template['sid'] == -2) {
                                            $label = $this->lang->sprintf($this->lang->search_found, $template['title']);
                                        } else {
                                            $label = $this->lang->sprintf($this->lang->search_found, $template['title']);
                                            $url = $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$template['sid']}";
                                        }
                                    }

                                    $table->construct_cell($label, ['width' => '85%']);

                                    if ($sid == -2) {
                                        $popup = new PopupMenu("template_{$template['tid']}", $this->lang->options);

                                        foreach ($template_sets as $set_sid => $title) {
                                            if ($set_sid > 0) {
                                                $popup->add_item($this->lang->edit_in . ' ' . htmlspecialchars_uni($title), $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$set_sid}");
                                            }
                                        }

                                        $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                                    } else {
                                        $table->construct_cell("<a href=\"{$url}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                                    }

                                    $table->construct_row();
                                }

                                if ($count == 1) {
                                    $html .= $table->output($this->lang->search_results);
                                } else {
                                    $html .= $table->output();
                                }
                            }
                        }

                        if (trim($this->bb->input['replace']) != '') {
                            // Log admin action - only if replace
                            $this->bblogger->log_admin_action($this->bb->input['find'], $this->bb->input['replace']);
                        }

                        $this->page->output_footer();
                        tdie('check this ->');
                        exit;
                    }
                } else {
                    if (!$this->bb->input['title']) {
                        $this->session->flash_message($this->lang->search_noneset, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/style/templates?action=search_replace');
                    } else {
                        // Search Template Titles

                        $templatessets = [];
                        $templates_sets = [];
                        // Get the names of all template sets
                        $template_sets[-2] = $this->lang->master_templates;
                        $template_sets[-1] = $this->lang->global_templates;

                        $this->plugins->runHooks('admin_style_templates_search_replace_title');

                        $query = $this->db->simple_select('templatesets', 'sid, title');
                        while ($set = $this->db->fetch_array($query)) {
                            $template_sets[$set['sid']] = $set['title'];
                        }

                        $table = new Table;

                        $query = $this->db->query('
					SELECT t.tid, t.title, t.sid, s.title as settitle, t2.tid as customtid
					FROM ' . TABLE_PREFIX . 'templates t
					LEFT JOIN ' . TABLE_PREFIX . 'templatesets s ON (t.sid=s.sid)
					LEFT JOIN ' . TABLE_PREFIX . "templates t2 ON (t.title=t2.title AND t2.sid='1')
					WHERE t.title LIKE '%" . $this->db->escape_string_like($this->bb->input['title']) . "%'
					ORDER BY t.title ASC
				");
                        while ($template = $this->db->fetch_array($query)) {
                            if ($template['sid'] == -2) {
                                if (!$template['customtid']) {
                                    $template['original'] = true;
                                } else {
                                    $template['modified'] = true;
                                }
                            } else {
                                $template['original'] = false;
                                $template['modified'] = false;
                            }
                            $templatessets[$template['sid']][$template['title']] = $template;
                        }

                        $this->page->add_breadcrumb_item($this->lang->search_replace);

                        $html = $this->page->output_header($this->lang->search_replace);
                        $html .= $this->page->output_nav_tabs($sub_tabs, 'search_replace');

                        if (empty($templatessets)) {
                            $table->construct_cell($this->lang->sprintf($this->lang->search_noresults_title, htmlspecialchars_uni($this->bb->input['title'])), ['class' => 'align_center']);

                            $table->construct_row();

                            $html .= $table->output($this->lang->search_results);
                        }

                        $count = 0;

                        foreach ($templatessets as $sid => $templates) {
                            ++$count;

                            $table->construct_header($template_sets[$sid], ['colspan' => 2]);

                            foreach ($templates as $template) {
                                $template['pretty_title'] = $template['title'];

                                $popup = new PopupMenu("template_{$template['tid']}", $this->lang->options);

                                if ($sid == -2) {
                                    foreach ($template_sets as $set_sid => $title) {
                                        if ($set_sid < 0) {
                                            continue;
                                        }

                                        $popup->add_item($this->lang->edit_in . ' ' . htmlspecialchars_uni($title), $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$set_sid}");
                                    }
                                } else {
                                    $popup->add_item($this->lang->full_edit, $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$sid}");
//                  $popup->add_item($this->lang->delete_template, $this->bb->settings['bburl'].'/admin/style/templates?action=delete_template&title='.urlencode($template['title'])."&amp;sid={$sid}&amp;my_post_key={$this->bb->post_code}{$expand_str}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_deletion}')");
                                }

                                if (isset($template['modified']) && $template['modified'] == true) {
                                    if ($sid > 0) {
                                        $popup->add_item($this->lang->diff_report, $this->bb->admin_url . '/style/templates?action=diff_report&title=' . urlencode($template['title']) . "&amp;sid2={$sid}");

                                        $popup->add_item($this->lang->revert_to_orig, $this->bb->admin_url . '/style/templates?action=revert&title=' . urlencode($template['title']) . "&amp;sid={$sid}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_revertion}')");
                                    }

                                    $template['pretty_title'] = "<span style=\"color: green;\">{$template['title']}</span>";
                                } // This template does not exist in the master list
                                elseif (!isset($template['original']) || $template['original'] == false) {
                                    $popup->add_item($this->lang->delete_template, $this->bb->admin_url . '/style/templates?action=delete_template&title=' . urlencode($template['title']) . "&amp;sid={$sid}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_deletion}')");

                                    $template['pretty_title'] = "<span style=\"color: blue;\">{$template['title']}</span>";
                                }

                                $table->construct_cell("<span style=\"padding: 20px;\">{$template['pretty_title']}</span>", ["width" => "85%"]);
                                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);

                                $table->construct_row();
                            }

                            if ($count == 1) {
                                $html .= $table->output($this->lang->sprintf($this->lang->search_names_header, htmlspecialchars_uni($this->bb->input['title'])));
                            } elseif ($count > 0) {
                                $html .= $table->output();
                            }
                        }

                        $this->page->output_footer();
                        tdie('check this');
                        exit;
                    }
                }
            }

            if ($this->adm->admin_options['codepress'] != 0) {
                $this->page->extra_header .= '
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.css" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/theme/mybb.css?ver=1804" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/xml/xml.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/javascript/javascript.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/css/css.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog-mybb.css" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/searchcursor.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/search.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldcode.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/xml-fold.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldgutter.js"></script>
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/fold/foldgutter.css" rel="stylesheet">
';
            }

            $this->page->add_breadcrumb_item($this->lang->search_replace);

            $html = $this->page->output_header($this->lang->search_replace);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'search_replace');

            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=search_replace', 'post', 'do_template');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('type', 'templates');

            $form_container = new FormContainer($this, $this->lang->search_replace, 'tfixed');
            $form_container->output_row($this->lang->search_for, '', $form->generate_text_area('find', $this->bb->input['find'], ['id' => 'find', 'class' => '', 'style' => 'width: 100%; height: 200px;']));

            $form_container->output_row($this->lang->replace_with, '', $form->generate_text_area('replace', $this->bb->input['replace'], ['id' => 'replace', 'class' => '', 'style' => 'width: 100%; height: 200px;']));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->find_and_replace);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();
            $html .= '<br />';


            $form = new Form($this->bb, $this->bb->admin_url . '/style/templates?action=search_replace', 'post', 'do_title');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('type', 'titles');

            $form_container = new FormContainer($this, $this->lang->search_template_names);
            $form_container->output_row($this->lang->search_for, '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $html .= $form_container->end();

            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->find_templates);
            $buttons[] = $form->generate_reset_button($this->lang->reset);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            if ($this->adm->admin_options['codepress'] != 0) {
                $html .= '<script type="text/javascript">
			var editor1 = CodeMirror.fromTextArea(document.getElementById("find"), {
				lineNumbers: true,
				lineWrapping: true,
				foldGutter: true,
				gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
				viewportMargin: Infinity,
				indentWithTabs: true,
				indentUnit: 4,
				mode: "text/html",
				theme: "mybb"
			});

			var editor2 = CodeMirror.fromTextArea(document.getElementById("replace"), {
				lineNumbers: true,
				lineWrapping: true,
				foldGutter: true,
				gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
				viewportMargin: Infinity,
				indentWithTabs: true,
				indentUnit: 4,
				mode: "text/html",
				theme: "mybb"
			});
		</script>';
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesSearchReplace.html.twig');
        }

        if ($this->bb->input['action'] == 'find_updated') {
            // Finds templates that are old and have been updated by BB
            $compare_version = $this->bb->version_code;
            $query = $this->db->query('
		SELECT COUNT(*) AS updated_count
		FROM ' . TABLE_PREFIX . 'templates t
		LEFT JOIN ' . TABLE_PREFIX . 'templates m ON (m.title=t.title AND m.sid=-2 AND m.version > t.version)
		WHERE t.sid > 0 AND m.template != t.template
	');
            $count = $this->db->fetch_array($query);

            if ($count['updated_count'] < 1) {
                $this->session->flash_message($this->lang->no_updated_templates, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            $this->plugins->runHooks('admin_style_templates_find_updated');

            $this->page->add_breadcrumb_item($this->lang->find_updated, $this->bb->admin_url . '/style/templates?action=find_updated');

            $html = $this->page->output_header($this->lang->find_updated);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'find_updated');

            $query = $this->db->simple_select('templatesets', '*', '', ['order_by' => 'title']);
            while ($templateset = $this->db->fetch_array($query)) {
                $templatesets[$templateset['sid']] = $templateset;
            }

            $html .= <<<LEGEND
	<fieldset>
<legend>{$this->lang->legend}</legend>
<ul>
<li>{$this->lang->updated_template_welcome1}</li>
<li>{$this->lang->updated_template_welcome2}</li>
<li>{$this->lang->updated_template_welcome3}</li>
</ul>
</fieldset>
LEGEND;

            $count = 0;
            $done_set = [];
            $done_output = [];
            $templates = [];
            $table = new Table;

            $query = $this->db->query('
		SELECT t.tid, t.title, t.sid, t.version
		FROM ' . TABLE_PREFIX . 'templates t
		LEFT JOIN ' . TABLE_PREFIX . 'templates m ON (m.title=t.title AND m.sid=-2 AND m.version > t.version)
		WHERE t.sid > 0 AND m.template != t.template
		ORDER BY t.sid ASC, title ASC
	');
            while ($template = $this->db->fetch_array($query)) {
                $templates[$template['sid']][] = $template;
            }

            foreach ($templates as $sid => $templates) {
                if (!$done_set[$sid]) {
                    $table->construct_header($templatesets[$sid]['title'], ['colspan' => 2]);

                    $done_set[$sid] = 1;
                    ++$count;
                }

                foreach ($templates as $template) {
                    $popup = new PopupMenu("template_{$template['tid']}", $this->lang->options);
                    $popup->add_item($this->lang->full_edit, $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&sid={$sid}&amp;from=diff_report");
                    $popup->add_item($this->lang->diff_report, $this->bb->admin_url . '/style/templates?action=diff_report&title=' . urlencode($template['title']) . '&sid1=' . $template['sid'] . '&sid2=-2&from=diff_report');
                    $popup->add_item($this->lang->revert_to_orig, $this->bb->admin_url . '/style/templates?action=revert&title=' . urlencode($template['title']) . "&sid={$sid}&amp;from=diff_report&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_revertion}')");

                    $table->construct_cell('<a href="' . $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&sid={$sid}&amp;from=diff_report\">{$template['title']}</a>", ['width' => '80%']);
                    $table->construct_cell($popup->fetch(), ['class' => 'align_center']);

                    $table->construct_row();
                }

                if ($done_set[$sid] && !$done_output[$sid]) {
                    $done_output[$sid] = 1;
                    if ($count == 1) {
                        $html .= $table->output($this->lang->find_updated);
                    } else {
                        $html .= $table->output();
                    }
                }
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesFindUpdated.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_template_group') {
            $gid = $this->bb->getInput('gid', 0);
            $query = $this->db->simple_select('templategroups', '*', "gid='{$gid}'");

            if (!$this->db->num_rows($query)) {
                $this->session->flash_message($this->lang->error_missing_template_group, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid);
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid);
            }

            $this->plugins->runHooks('admin_style_template_group_delete');

            $template_group = $this->db->fetch_array($query);

            if ($this->bb->request_method == 'post') {
                // Delete the group
                $this->db->delete_query('templategroups', "gid = '{$template_group['gid']}'");

                $this->plugins->runHooks('admin_style_template_group_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($template_group['gid'], htmlspecialchars_uni($template_group['title']));

                $this->session->flash_message($this->lang->success_template_group_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid);
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . "/style/templates?action=delete_template_group&amp;gid={$template_group['gid']}&amp;sid={$sid}", $this->lang->confirm_template_group_delete);
            }
        }

        if ($this->bb->input['action'] == 'delete_set') {
            $query = $this->db->simple_select('templatesets', '*', "sid='{$sid}' AND sid > 0");
            $set = $this->db->fetch_array($query);

            // Does the template not exist?
            if (!$set['sid']) {
                $this->session->flash_message($this->lang->error_invalid_template_set, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            $this->plugins->runHooks('admin_style_templates_delete_set');

            // Is there a theme attached to this set?
            $query = $this->db->simple_select('themes', 'properties');
            while ($theme = $this->db->fetch_array($query)) {
                $properties = my_unserialize($theme['properties']);
                if ($properties['templateset'] == $sid) {
                    $this->session->flash_message($this->lang->error_themes_attached_template_set, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/style/templates');
                }
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            if ($this->bb->request_method == 'post') {
                // Delete the templateset
                $this->db->delete_query('templatesets', "sid='{$set['sid']}'");
                // Delete all custom templates in this templateset
                $this->db->delete_query('templates', "sid='{$set['sid']}'");

                $this->plugins->runHooks('admin_style_templates_delete_set_commit');

                // Log admin action
                $this->bblogger->log_admin_action($set['sid'], $set['title']);

                $this->session->flash_message($this->lang->success_template_set_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/style/templates?action=delete_set&sid=' . $set['sid'], $this->lang->confirm_template_set_deletion);
            }
        }

        if ($this->bb->input['action'] == 'delete_template') {
            $query = $this->db->query('
		SELECT t.*, s.title as set_title
		FROM ' . TABLE_PREFIX . 'templates t
		LEFT JOIN ' . TABLE_PREFIX . "templatesets s ON(t.sid=s.sid)
		WHERE t.title='" . $this->db->escape_string($this->bb->input['title']) . "' AND t.sid > '-2' AND t.sid = '{$sid}'
	");
            $template = $this->db->fetch_array($query);

            // Does the template not exist?
            if (!$template) {
                $this->session->flash_message($this->lang->error_invalid_template, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $template['sid'] . $expand_str2);
            }

            $this->plugins->runHooks('admin_style_templates_delete_template');

            if ($this->bb->request_method == 'post') {
                // Delete the template
                $this->db->delete_query('templates', "tid='{$template['tid']}'");

                $this->plugins->runHooks('admin_style_templates_delete_template_commit');

                // Log admin action
                $this->bblogger->log_admin_action($template['tid'], $template['title'], $template['sid'], $template['set_title']);

                $this->session->flash_message($this->lang->success_template_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $template['sid'] . $expand_str2);
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/style/templates?action=delete_template&sid=' . $template['sid'] . $expand_str, $this->lang->confirm_template_deletion);
            }
        }

        if ($this->bb->input['action'] == 'diff_report') {
            // Compares a template of sid1 with that of sid2, if no sid1, it is assumed -2
            if (!$this->bb->input['sid1'] || !isset($template_sets[$this->bb->input['sid1']])) {
                $this->bb->input['sid1'] = -2;
            }

            if ($this->bb->input['sid2'] == -2) {
                $sub_tabs['find_updated'] = [
                    'title' => $this->lang->find_updated,
                    'link' => $this->bb->admin_url . '/style/templates?action=find_updated'
                ];
            }

            if ($this->bb->input['sid2'] != -2 && !isset($template_sets[$this->bb->input['sid2']])) {
                $this->session->flash_message($this->lang->error_invalid_input, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            if (!$this->bb->input['from']) {
                $this->bb->input['from'] = 0;
            }

            $sub_tabs['diff_report'] = [
                'title' => $this->lang->diff_report,
                'link' => $this->bb->admin_url . '/style/templates?action=diff_report&title=' . $this->db->escape_string($this->bb->input['title']) . '&from=' . htmlspecialchars_uni($this->bb->input['from']) . 'sid1=' . $this->bb->getInput('sid1', 0) . '&sid2=' . $this->bb->getInput('sid2', 0),
                'description' => $this->lang->diff_report_desc
            ];

            $this->plugins->runHooks('admin_style_templates_diff_report');

            $query = $this->db->simple_select('templates', '*', "title='" . $this->db->escape_string($this->bb->input['title']) . "' AND sid='" . $this->bb->getInput('sid1', 0) . "'");
            $template1 = $this->db->fetch_array($query);

            $query = $this->db->simple_select('templates', '*', "title='" . $this->db->escape_string($this->bb->input['title']) . "' AND sid='" . $this->bb->getInput('sid2', 0) . "'");
            $template2 = $this->db->fetch_array($query);

            if ($this->bb->input['sid2'] == -2) {
                $sub_tabs['full_edit'] = [
                    'title' => $this->lang->full_edit,
                    'link' => $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template1['title']) . '&sid=' . $this->bb->getInput('sid1', 0) . '&from=diff_report',
                ];
            }

            if ($template1['template'] == $template2['template']) {
                $this->session->flash_message($this->lang->templates_the_same, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $this->bb->getInput('sid2', 0) . $expand_str);
            }

            $template1['template'] = explode("\n", $template1['template']);
            $template2['template'] = explode("\n", $template2['template']);

            $this->plugins->runHooks('admin_style_templates_diff_report_run');

// FIXME change to package like https://github.com/chrisboulton/php-diff
//            require_once MYBB_ROOT . 'inc/3rdparty/diff/Diff.php';
//            require_once MYBB_ROOT . 'inc/3rdparty/diff/Diff/Renderer.php';
//            require_once MYBB_ROOT . 'inc/3rdparty/diff/Diff/Renderer/Inline.php';
//
//            $diff = new Horde_Text_Diff('auto', array($template1['template'], $template2['template']));
//            $renderer = new Horde_Text_Diff_Renderer_Inline();

            if ($sid) {
                $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid . $expand_str);
            }

            if ($this->bb->input['sid2'] == -2) {
                $this->page->add_breadcrumb_item($this->lang->find_updated, $this->bb->admin_url . '/style/templates?action=find_updated');
            }

            $this->page->add_breadcrumb_item($this->lang->diff_report . ': ' . htmlspecialchars_uni($template1['title']), $this->bb->admin_url . '/style/templates?action=diff_report&title=' . $this->db->escape_string($this->bb->input['title']) . '&from=' . htmlspecialchars_uni($this->bb->input['from']) . '&sid1=' . $this->bb->getInput('sid1', 0) . '&sid2=' . $this->bb->getInput('sid2', 0));

            $html = $this->page->output_header($this->lang->template_sets);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'diff_report');

            $table = new Table;

            if ($this->bb->input['from']) {
                $table->construct_header('<ins>' . $this->lang->master_updated_ins . '</ins><br /><del>' . $this->lang->master_updated_del . '</del>');
            } else {
                $table->construct_header('<ins>' . $this->lang->master_updated_del . '</ins><br /><del>' . $this->lang->master_updated_ins . '</del>');
            }

            $table->construct_cell('<pre class="differential">' . $renderer->render($diff) . '</pre>');
            $table->construct_row();

            $html .= $table->output($this->lang->template_diff_analysis . ': ' . $template1['title']);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesDiffReport.html.twig');
        }

        if ($this->bb->input['action'] == 'revert') {
            $query = $this->db->query('
		SELECT t.*, s.title as set_title
		FROM ' . TABLE_PREFIX . 'templates t
		LEFT JOIN ' . TABLE_PREFIX . "templatesets s ON(s.sid=t.sid)
		WHERE t.title='" . $this->db->escape_string($this->bb->input['title']) . "' AND t.sid > 0 AND t.sid = '" . $this->bb->getInput('sid', 0) . "'
	");
            $template = $this->db->fetch_array($query);

            // Does the template not exist?
            if (!$template) {
                $this->session->flash_message($this->lang->error_invalid_template, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $template['sid'] . $expand_str2);
            }

            $this->plugins->runHooks('admin_style_templates_revert');

            if ($this->bb->request_method == 'post') {
                // Revert the template
                $this->db->delete_query('templates', "tid='{$template['tid']}'");

                $this->plugins->runHooks('admin_style_templates_revert_commit');

                // Log admin action
                $this->bblogger->log_admin_action($template['tid'], $template['title'], $template['sid'], $template['set_title']);

                $this->session->flash_message($this->lang->success_template_reverted, 'success');

                if ($this->bb->input['from'] == 'diff_report') {
                    return $response->withRedirect($this->bb->admin_url . '/style/templates?action=find_updated');
                } else {
                    return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $template['sid'] . $expand_str2);
                }
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/style/templates?sid=' . $template['sid'] . $expand_str, $this->lang->confirm_template_revertion);
            }
        }

        if (isset($this->bb->input['sid']) && !$this->bb->input['action']) {
            if (!isset($template_sets[$this->bb->input['sid']])) {
                $this->session->flash_message($this->lang->error_invalid_input, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style/templates');
            }

            $this->plugins->runHooks('admin_style_templates_set');

            $table = new Table;

            $this->page->add_breadcrumb_item($template_sets[$sid], $this->bb->admin_url . '/style/templates?sid=' . $sid);

            $html = $this->page->output_header($this->lang->template_sets);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_templates');

            $table->construct_header($this->lang->template_set);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            // Global Templates
            if ($sid == -1) {
                $query = $this->db->simple_select('templates', 'tid,title', "sid='-1'", ['order_by' => 'title', 'order_dir' => 'ASC']);
                while ($template = $this->db->fetch_array($query)) {
                    $popup = new PopupMenu("template_{$template['tid']}", $this->lang->options);
                    $popup->add_item($this->lang->full_edit, $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . '&sid=-1');
                    $popup->add_item($this->lang->delete_template, $this->bb->admin_url . '/style/templates?action=delete_template&title=' . urlencode($template['title']) . "&amp;sid=-1&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_deletion}')");

                    $table->construct_cell('<a href="' . $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid=-1\">{$template['title']}</a>");
                    $table->construct_cell($popup->fetch(), ['class' => 'align_center']);

                    $table->construct_row();
                }

                if ($table->num_rows() == 0) {
                    $table->construct_cell($this->lang->no_global_templates, ['colspan' => 2]);
                    $table->construct_row();
                }

                $html .= $table->output($template_sets[$sid]);

                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Style/TemplatesManageGlobal.html.twig');
            }

            if (!isset($this->bb->input['expand'])) {
                $this->bb->input['expand'] = '';
            }
            if ($this->bb->input['expand'] == 'all') {
                // If we're expanding everything, stick in the ungrouped templates in the list as well
                $expand_array = [-1];
            }
            // Fetch Groups
            $query = $this->db->simple_select('templategroups', '*');

            $template_groups = [];
            while ($templategroup = $this->db->fetch_array($query)) {
                $templategroup['title'] = $this->lang->sprintf($this->lang->templates, htmlspecialchars_uni($this->lang->parse($templategroup['title'])));
                if ($this->bb->input['expand'] == 'all') {
                    $expand_array[] = $templategroup['gid'];
                }
                if (in_array($templategroup['gid'], $expand_array)) {
                    $templategroup['expanded'] = 1;
                }
                $template_groups[$templategroup['prefix']] = $templategroup;
            }

            uasort($template_groups, [$this, 'sort_template_groups']);

            // Add the ungrouped templates group at the bottom
            $template_groups['-1'] = [
                'prefix' => '',
                'title' => $this->lang->ungrouped_templates,
                'gid' => -1
            ];

            // Set the template group keys to lowercase for case insensitive comparison.
            $template_groups = array_change_key_case($template_groups, CASE_LOWER);

            // Load the list of templates
            $query = $this->db->simple_select('templates', '*', "sid='" . $this->bb->getInput('sid', 0) . "' OR sid='-2'", ['order_by' => 'sid DESC, title', 'order_dir' => 'ASC']);
            while ($template = $this->db->fetch_array($query)) {
                $exploded = explode('_', $template['title'], 2);

                // Set the prefix to lowercase for case insensitive comparison.
                $exploded[0] = strtolower($exploded[0]);

                if (isset($template_groups[$exploded[0]])) {
                    $group = $exploded[0];
                } else {
                    $group = -1;
                }

                $template['gid'] = -1;
                if (isset($template_groups[$exploded[0]]['gid'])) {
                    $template['gid'] = $template_groups[$exploded[0]]['gid'];
                }

                // If this template is not a master template, we simple add it to the list
                if ($template['sid'] != -2) {
                    $template['original'] = false;
                    $template['modified'] = false;
                    $template_groups[$group]['templates'][$template['title']] = $template;
                } elseif (!in_array($template['gid'], $expand_array) && !isset($expand_array[-1])) {
                    $template['original'] = true;
                    $template['modified'] = false;
                    $template_groups[$group]['templates'][$template['title']] = $template;

                    // Save some memory!
                    unset($template_groups[$group]['templates'][$template['title']]['template']);
                } // Otherwise, if we are down to master templates we need to do a few extra things
                else {
                    // Master template that hasn't been customised in the set we have expanded
                    if (!isset($template_groups[$group]['templates'][$template['title']]) || $template_groups[$group]['templates'][$template['title']]['template'] == $template['template']) {
                        $template['original'] = true;
                        $template_groups[$group]['templates'][$template['title']] = $template;
                    } // Template has been modified in the set we have expanded (it doesn't match the master)
                    elseif ($template_groups[$group]['templates'][$template['title']]['template'] != $template['template'] && $template_groups[$group]['templates'][$template['title']]['sid'] != -2) {
                        $template_groups[$group]['templates'][$template['title']]['modified'] = true;
                    }

                    // Save some memory!
                    unset($template_groups[$group]['templates'][$template['title']]['template']);
                }
            }

            foreach ($template_groups as $prefix => $group) {
                $tmp_expand = '';
                if (in_array($group['gid'], $expand_array)) {
                    $expand = $this->lang->collapse;
                    $expanded = true;

                    $tmp_expand = $expand_array;
                    $unsetgid = array_search($group['gid'], $tmp_expand);
                    unset($tmp_expand[$unsetgid]);
                    $group['expand_str'] = implode('|', $tmp_expand);
                } else {
                    $expand = $this->lang->expand;
                    $expanded = false;

                    $group['expand_str'] = implode('|', $expand_array);
                    if ($group['expand_str']) {
                        $group['expand_str'] .= '|';
                    }
                    $group['expand_str'] .= $group['gid'];
                }

                if ($group['expand_str']) {
                    $group['expand_str'] = "&amp;expand={$group['expand_str']}";
                }

                $set_popup = '';
                if (isset($group['isdefault']) && !$group['isdefault']) {
                    $popup = new PopupMenu("template_set_{$group['gid']}", $this->lang->options);
                    $popup->add_item($this->lang->edit_template_group, $this->bb->admin_url . "/style/templates?sid={$sid}&amp;action=edit_template_group&amp;gid={$group['gid']}{$group['expand_str']}");
                    $popup->add_item($this->lang->delete_template_group, $this->bb->admin_url . "/style/templates?sid={$sid}&amp;action=delete_template_group&amp;gid={$group['gid']}&amp;my_post_key={$this->bb->post_code}{$group['expand_str']}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_group_delete}')");

                    $set_popup = "<div class=\"float_right\">{$popup->fetch()}</div>";
                }

                if ($expanded == true) {
                    // Show templates in this group
                    $table->construct_cell("{$set_popup}<strong><a href=\"".$this->bb->admin_url."/style/templates?sid={$sid}{$group['expand_str']}#group_{$group['gid']}\">{$group['title']}</a></strong>");
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/style/templates?sid={$sid}{$group['expand_str']}#group_{$group['gid']}\">{$expand}</a>", ["class" => "align_center"]);
                    $table->construct_row(['class' => 'alt_row', 'id' => 'group_' . $group['gid'], 'name' => 'group_' . $group['gid']]);

                    if (isset($group['templates']) && count($group['templates']) > 0) {
                        $templates = $group['templates'];
                        ksort($templates);

                        foreach ($templates as $template) {
                            $template['pretty_title'] = $template['title'];

                            $popup = new PopupMenu("template_{$template['tid']}", $this->lang->options);
                            $popup->add_item($this->lang->full_edit, $this->bb->admin_url . '/style/templates?action=edit_template&amp;title=' . urlencode($template['title']) . "&amp;sid={$sid}{$expand_str}");
                            //FIXME need this delete???
                            $popup->add_item($this->lang->delete_template, $this->bb->admin_url . '/style/deltpl?title=' . urlencode($template['title']) . '&sid=' . $sid . '&my_post_key=' . $this->bb->post_code, "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_deletion}')");

                            if (isset($template['modified']) && $template['modified'] == true) {
                                if ($sid > 0) {
                                    $popup->add_item($this->lang->diff_report, $this->bb->admin_url . '/style/templates?action=diff_report&title=' . urlencode($template['title']) . "&amp;sid2={$sid}");

                                    $popup->add_item($this->lang->revert_to_orig, $this->bb->admin_url . '/style/templates?action=revert&title=' . urlencode($template['title']) . "&amp;sid={$sid}&amp;my_post_key={$this->bb->post_code}{$expand_str}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_revertion}')");
                                }

                                $template['pretty_title'] = "<span style=\"color: green;\">{$template['title']}</span>";
                            } // This template does not exist in the master list
                            elseif (isset($template['original']) && $template['original'] == false) {
                                $popup->add_item($this->lang->delete_template, $this->bb->admin_url . '/style/templates?action=delete_template&title=' . urlencode($template['title']) . "&amp;sid={$sid}&amp;my_post_key={$this->bb->post_code}{$expand_str}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_deletion}')");

                                $template['pretty_title'] = "<span style=\"color: blue;\">{$template['title']}</span>";
                            }

                            $table->construct_cell("<span style=\"padding: 20px;\"><a href=\"".$this->bb->admin_url."/style/templates?action=edit_template&amp;title=" . urlencode($template['title']) . "&amp;sid={$sid}{$expand_str}\" >{$template['pretty_title']}</a></span>");
                            $table->construct_cell($popup->fetch(), ['class' => 'align_center']);

                            $table->construct_row();
                        }
                    } else {
                        // No templates in this group
                        $table->construct_cell($this->lang->empty_template_set, ['colspan' => 2]);
                        $table->construct_row();
                    }
                } else {
                    // Collapse template set
                    $table->construct_cell("{$set_popup}<strong><a href=\"".$this->bb->admin_url."/style/templates?sid={$sid}{$group['expand_str']}#group_{$group['gid']}\">{$group['title']}</a></strong>");
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/style/templates?sid={$sid}{$group['expand_str']}#group_{$group['gid']}\">{$expand}</a>", ["class" => "align_center"]);
                    $table->construct_row(['class' => 'alt_row', 'id' => 'group_' . $group['gid'], 'name' => 'group_' . $group['gid']]);
                }
            }
            $cnt = \RunBB\Models\Template::count();
            $html .= $table->output('Total: '.$cnt.' &nbsp; '.$template_sets[$sid]);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/TemplatesManage.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_style_templates_start');

            $html = $this->page->output_header($this->lang->template_sets);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'templates');

            $themes = [];
            $query = $this->db->simple_select('themes', 'name,tid,properties', "tid != '1'");
            while ($theme = $this->db->fetch_array($query)) {
                $tbits = my_unserialize($theme['properties']);
                $themes[$tbits['templateset']][$theme['tid']] = htmlspecialchars_uni($theme['name']);
            }

            $template_sets = [];
            $template_sets[-1]['title'] = $this->lang->global_templates;
            $template_sets[-1]['sid'] = -1;

            $query = $this->db->simple_select('templatesets', '*', '', ['order_by' => 'title', 'order_dir' => 'ASC']);
            while ($template_set = $this->db->fetch_array($query)) {
                $template_sets[$template_set['sid']] = $template_set;
            }

            $table = new Table;
            $table->construct_header($this->lang->template_set);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            foreach ($template_sets as $set) {
                if ($set['sid'] == -1) {
                    $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/style/templates?sid=-1\">{$this->lang->global_templates}</a></strong><br /><small>{$this->lang->used_by_all_themes}</small>");
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/style/templates?sid=-1\">{$this->lang->expand_templates}</a>", ['class' => 'align_center']);
                    $table->construct_row();
                    continue;
                }

                if ($themes[$set['sid']]) {
                    $used_by_note = $this->lang->used_by;
                    $comma = '';
                    foreach ($themes[$set['sid']] as $theme_name) {
                        $used_by_note .= $comma . $theme_name;
                        $comma = $this->lang->comma;
                    }
                } else {
                    $used_by_note = $this->lang->not_used_by_any_themes;
                }

                if ($set['sid'] == 1) {
                    $actions = "<a href=\"".$this->bb->admin_url."/style/templates?sid={$set['sid']}\">{$this->lang->expand_templates}</a>";
                } else {
                    $popup = new PopupMenu("templateset_{$set['sid']}", $this->lang->options);
                    $popup->add_item($this->lang->expand_templates, $this->bb->admin_url . '/style/templates?sid=' . $set['sid']);

                    if ($set['sid'] != 1) {
                        $popup->add_item($this->lang->edit_template_set, $this->bb->admin_url . '/style/templates?action=edit_set&sid=' . $set['sid']);

                        if (!$themes[$set['sid']]) {
                            $popup->add_item($this->lang->delete_template_set, $this->bb->admin_url . "/style/templates?action=delete_set&amp;sid={$set['sid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_template_set_deletion}')");
                        }
                    }

                    $actions = $popup->fetch();
                }

                $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/style/templates?sid={$set['sid']}\">{$set['title']}</a></strong><br /><small>{$used_by_note}</small>");
                $table->construct_cell($actions, ['class' => 'align_center']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->template_sets);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/Templates.html.twig');
        }
    }

    //FIXME for debug only!!!
    public function delTpl(Request $request, Response $response)
    {
        $this->adm->init('style');// set active module
//    $this->lang->load('style_templates', false, true);

        if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
            $this->session->flash_message('Bad CSFR key', 'error');
            return $response->withRedirect($this->bb->admin_url . '/style/templates');
        } else {
            //delete_query($table, $where="", $limit="")
            $sid = $this->bb->getInput('sid', 0);
            $title = $this->bb->getInput('title', '');

            $this->db->delete_query('templates', "title='$title'", 1);
            $this->session->flash_message('Template "'.$title.'" deleted', 'success');
            return $response->withRedirect($this->bb->admin_url . '/style/templates?sid=' . $sid);
        }
    }

    /**
     * @param array $a
     * @param array $b
     *
     * @return int
     */
    private function sort_template_groups($a, $b)
    {
        return strcasecmp($a['title'], $b['title']);
    }
}
