<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Style;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use RunBB\Core\XMLParser;
use RunBB\Helpers\ThemeHelper;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;

class Themes extends AbstractController
{
    private $errors = [];

    public function index(Request $request, Response $response)
    {
        $this->adm->init('style');// set active module
        $this->lang->load('style_themes', false, true);

        $this->page->extra_header .= "
<script type=\"text/javascript\">
//<![CDATA[
var save_changes_lang_string = '{$this->lang->save_changes_js}';
var delete_lang_string = '{$this->lang->delete}';
var file_lang_string = '{$this->lang->file}';
var globally_lang_string = '{$this->lang->globally}';
var specific_actions_lang_string = '{$this->lang->specific_actions}';
var specific_actions_desc_lang_string = '{$this->lang->specific_actions_desc}';
var delete_confirm_lang_string = '{$this->lang->delete_confirm_js}';

lang.theme_info_fetch_error = \"{$this->lang->theme_info_fetch_error}\";
lang.theme_info_save_error = \"{$this->lang->theme_info_save_error}\";
//]]>
</script>";

        $themeHelper = new ThemeHelper($this->bb);

        if ($this->bb->input['action'] == 'xmlhttp_stylesheet' && $this->bb->request_method == 'post') {
            // Fetch the theme we want to edit this stylesheet in
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $parent_list = $themeHelper->make_parent_theme_list($theme['tid']);
            $parent_list = implode(',', $parent_list);
            if (!$parent_list) {
                $parent_list = 1;
            }

            $query = $this->db->simple_select('themestylesheets', '*', "name='" . $this->db->escape_string($this->bb->input['file']) . "' AND tid IN ({$parent_list})", ['order_by' => 'tid', 'order_dir' => 'desc', 'limit' => 1]);
            $stylesheet = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$stylesheet['sid']) {
                $this->session->flash_message($this->lang->error_invalid_stylesheet, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $css_array = $themeHelper->css_to_array($stylesheet['stylesheet']);
            $selector_list = $themeHelper->get_selectors_as_options($css_array, $this->bb->input['selector']);
            $editable_selector = $css_array[$this->bb->input['selector']];
            $properties = $themeHelper->parse_css_properties($editable_selector['values']);

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=stylesheet_properties', 'post', 'selector_form', 0, '', true);
            echo $form->getForm();
            echo $form->generate_hidden_field('tid', $this->bb->input['tid'], ['id' => 'tid']) . "\n";
            echo $form->generate_hidden_field('file', htmlspecialchars_uni($this->bb->input['file']), ['id' => 'file']) . "\n";
            echo $form->generate_hidden_field('selector', htmlspecialchars_uni($this->bb->input['selector']), ['id' => 'hidden_selector']) . "\n";

            $table = new Table;
            if ($this->lang->settings['rtl'] === true) {
                $div_align = 'left';
            } else {
                $div_align = 'right';
            }

            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[background]', $properties['background'], ['id' => 'css_bits[background]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->background}</strong></div>", ['style' => 'width: 20%;']);
            $table->construct_cell("<strong>{$this->lang->extra_css_atribs}</strong><br /><div style=\"align: center;\">" . $form->generate_text_area('css_bits[extra]', $properties['extra'], ['id' => 'css_bits[extra]', 'style' => 'width: 98%;', 'rows' => '19']) . "</div>", ['rowspan' => 8]);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[color]', $properties['color'], ['id' => 'css_bits[color]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->color}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[width]', $properties['width'], ['id' => 'css_bits[width]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->width}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[font_family]', $properties['font-family'], ['id' => 'css_bits[font_family]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_family}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[font_size]', $properties['font-size'], ['id' => 'css_bits[font_size]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_size}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[font_style]', $properties['font-style'], ['id' => 'css_bits[font_style]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_style}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[font_weight]', $properties['font-weight'], ['id' => 'css_bits[font_weight]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_weight}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: {$div_align};\">" . $form->generate_text_box('css_bits[text_decoration]', $properties['text-decoration'], ['id' => 'css_bits[text_decoration]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->text_decoration}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();

            echo $table->output(htmlspecialchars_uni($editable_selector['class_name']) . "<span id=\"saved\" style=\"color: #FEE0C6;\"></span>");
            exit;
        }

        $this->page->add_breadcrumb_item($this->lang->themes, $this->bb->admin_url . '/style');

        if ($this->bb->input['action'] == 'add' ||
            $this->bb->input['action'] == 'import' ||
            $this->bb->input['action'] == 'browse' ||
            !$this->bb->input['action']
        ) {
            $sub_tabs['themes'] = [
                'title' => $this->lang->themes,
                'link' => $this->bb->admin_url . '/style',
                'description' => $this->lang->themes_desc
            ];

            $sub_tabs['create_theme'] = [
                'title' => $this->lang->create_new_theme,
                'link' => $this->bb->admin_url . '/style?action=add',
                'description' => $this->lang->create_new_theme_desc
            ];

            $sub_tabs['import_theme'] = [
                'title' => $this->lang->import_a_theme,
                'link' => $this->bb->admin_url . '/style?action=import',
                'description' => $this->lang->import_a_theme_desc
            ];

            $sub_tabs['browse_themes'] = [
                'title' => $this->lang->browse_themes,
                'link' => $this->bb->admin_url . '/style?action=browse',
                'description' => $this->lang->browse_themes_desc
            ];
        }

        $this->plugins->runHooks('admin_style_themes_begin');

        if ($this->bb->input['action'] == 'browse') {
            $this->plugins->runHooks('admin_style_themes_browse');

            $this->page->add_breadcrumb_item($this->lang->browse_themes);

            $html = $this->page->output_header($this->lang->browse_themes);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'browse_themes');

            $keywords = '';
            if (isset($this->bb->input['keywords'])) {
                $keywords = '&keywords=' . urlencode($this->bb->input['keywords']);
            }

            if (isset($this->bb->input['page'])) {
                $url_page = '&page=' . $this->bb->getInput('page', 0);
            } else {
                $this->bb->input['page'] = 1;
                $url_page = '';
            }

            // Gets the major version code. i.e. 1410 -> 1400 or 121 -> 1200
            $major_version_code = round($this->bb->version_code / 100, 0) * 100;
            // Convert to mods site version codes
            $search_version = ($major_version_code / 100) . 'x';

            $contents = fetch_remote_file("http://community.mybb.com/xmlbrowse.php?type=themes&version={$search_version}{$keywords}{$url_page}");//, $post_data);

            if (!$contents) {
                $html .= $this->page->output_inline_error($this->lang->error_communication_problem);
                $this->page->output_footer();
                tdie('check this');
                exit;
            }

            $table = new Table;
            $table->construct_header($this->lang->themes, ['colspan' => 2]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 125]);

            $parser = new XMLParser($contents);
            $tree = $parser->get_tree();

            if (!is_array($tree) || !isset($tree['results'])) {
                $html .= $this->page->output_inline_error($this->lang->error_communication_problem);
                $this->page->output_footer();
                tdie('check this');
                exit;
            }

            if (!empty($tree['results']['result'])) {
                if (array_key_exists('tag', $tree['results']['result'])) {
                    $only_theme = $tree['results']['result'];
                    unset($tree['results']['result']);
                    $tree['results']['result'][0] = $only_theme;
                }

                foreach ($tree['results']['result'] as $result) {
                    $result['thumbnail']['value'] = htmlspecialchars_uni($result['thumbnail']['value']);
                    $result['name']['value'] = htmlspecialchars_uni($result['name']['value']);
                    $result['description']['value'] = htmlspecialchars_uni($result['description']['value']);
                    $result['author']['value'] = $this->parser->parse_message($result['author']['value'], [
                            'allow_html' => true
                        ]);
                    $result['download_url']['value'] = htmlspecialchars_uni(html_entity_decode($result['download_url']['value']));

                    $table->construct_cell("<img src=\"http://community.mybb.com/{$result['thumbnail']['value']}\" alt=\"{$this->lang->theme_thumbnail}\" title=\"{$this->lang->theme_thumbnail}\"/>", ["class" => "align_center", "width" => 100]);
                    $table->construct_cell("<strong>{$result['name']['value']}</strong><br /><small>{$result['description']['value']}</small><br /><i><small>{$this->lang->created_by} {$result['author']['value']}</small></i>");
                    $table->construct_cell("<strong><a href=\"http://community.mybb.com/{$result['download_url']['value']}\" target=\"_blank\">{$this->lang->download}</a></strong>", ["class" => "align_center"]);
                    $table->construct_row();
                }
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->error_no_results_found, ['colspan' => 3]);
                $table->construct_row();
            }

            $search = new Form($this->bb, $this->bb->admin_url . '/style?action=browse', 'post', 'search_form');
            $html .= $search->getForm();
            $html .= "<div style=\"padding-bottom: 3px; margin-top: -9px; text-align: right;\">";
            if (isset($this->bb->input['keywords'])) {
                $default_class = '';
                $value = htmlspecialchars_uni($this->bb->input['keywords']);
            } else {
                $default_class = 'search_default';
                $value = $this->lang->search_for_themes;
            }
            $html .= $search->generate_text_box('keywords', $value, ['id' => 'search_keywords', 'class' => "{$default_class} field150 field_small"]) . "\n";
            $html .= "<input type=\"submit\" class=\"search_button\" value=\"{$this->lang->search}\" />\n";
            $html .= "<script type=\"text/javascript\">
		var form = $(\"#search_form\");
		form.submit(function()
		{
			var search = $('#search_keywords');
			if(search.val() == '' || search.val() == '{$this->lang->search_for_themes}')
			{
				search.focus();
				return false;
			}
		});

		var search = $('#search_keywords');
		search.focus(function()
		{
			var search_focus = $(this);
			if(search_focus.val() == '{$this->lang->search_for_themes}')
			{
				search_focus.removeClass('search_default');
				search_focus.val('');
			}
		});

		search.blur(function()
		{
			var search_blur = $(this);
			if(search_blur.val() == '')
			{
				search_blur.addClass('search_default');
				search_blur.val('{$this->lang->search_for_themes}');
			}
		});

		// fix the styling used if we have a different default value
		if(search.val() != '{$this->lang->search_for_themes}')
		{
			search.removeClass('search_default');
		}
		</script>\n";
            $html .= "</div>\n";
            $html .= $search->end();

            // Recommended themes = Default; Otherwise search results & pagination
            if ($this->bb->request_method == 'post') {
                $html .= $table->output("<span style=\"float: right;\"><small><a href=\"http://community.mybb.com/mods.php?action=browse&category=themes\" target=\"_blank\">{$this->lang->browse_all_themes}</a></small></span>" . $this->lang->sprintf($this->lang->browse_results_for_mybb, $this->bb->version));
            } else {
                $html .= $table->output("<span style=\"float: right;\"><small><a href=\"http://community.mybb.com/mods.php?action=browse&category=themes\" target=\"_blank\">{$this->lang->browse_all_themes}</a></small></span>" . $this->lang->sprintf($this->lang->recommended_themes_for_mybb, $this->bb->version));
            }

            $html .= '<br />' . $this->adm->draw_admin_pagination($this->bb->input['page'], 15, $tree['results']['attributes']['total'], $this->bb->admin_url . "/style?action=browse{$keywords}&amp;page={page}");

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesBrowse.html.twig');
        }

        if ($this->bb->input['action'] == 'import') {
            $this->plugins->runHooks('admin_style_themes_import');

            if ($this->bb->request_method == 'post') {
                if (!$_FILES['local_file'] && !$this->bb->input['url']) {
                    $this->errors[] = $this->lang->error_missing_url;
                }

                if (empty($this->errors)) {
                    // Find out if there was an uploaded file
                    if ($_FILES['local_file']['error'] != 4) {
                        // Find out if there was an error with the uploaded file
                        if ($_FILES['local_file']['error'] != 0) {
                            $this->errors[] = $this->lang->error_uploadfailed . $this->lang->error_uploadfailed_detail;
                            switch ($_FILES['local_file']['error']) {
                                case 1: // UPLOAD_ERR_INI_SIZE
                                    $this->errors[] = $this->lang->error_uploadfailed_php1;
                                    break;
                                case 2: // UPLOAD_ERR_FORM_SIZE
                                    $this->errors[] = $this->lang->error_uploadfailed_php2;
                                    break;
                                case 3: // UPLOAD_ERR_PARTIAL
                                    $this->errors[] = $this->lang->error_uploadfailed_php3;
                                    break;
                                case 6: // UPLOAD_ERR_NO_TMP_DIR
                                    $this->errors[] = $this->lang->error_uploadfailed_php6;
                                    break;
                                case 7: // UPLOAD_ERR_CANT_WRITE
                                    $this->errors[] = $this->lang->error_uploadfailed_php7;
                                    break;
                                default:
                                    $this->errors[] = $this->lang->sprintf($this->lang->error_uploadfailed_phpx, $_FILES['local_file']['error']);
                                    break;
                            }
                        }

                        if (empty($this->errors)) {
                            // Was the temporary file found?
                            if (!is_uploaded_file($_FILES['local_file']['tmp_name'])) {
                                $this->errors[] = $this->lang->error_uploadfailed_lost;
                            }
                            // Get the contents
                            $contents = @file_get_contents($_FILES['local_file']['tmp_name']);
                            // Delete the temporary file if possible
                            @unlink($_FILES['local_file']['tmp_name']);
                            // Are there contents?
                            if (!trim($contents)) {
                                $this->errors[] = $this->lang->error_uploadfailed_nocontents;
                            }
                        }
                    } elseif (!empty($this->bb->input['url'])) {
                        // Get the contents
                        $contents = @fetch_remote_file($this->bb->input['url']);
                        if (!$contents) {
                            $this->errors[] = $this->lang->error_local_file;
                        }
                    } else {
                        // UPLOAD_ERR_NO_FILE
                        $this->errors[] = $this->lang->error_uploadfailed_php4;
                    }

                    if (empty($this->errors)) {
                        $options = [
                            'no_stylesheets' => ($this->bb->input['import_stylesheets'] ? 0 : 1),
                            'no_templates' => ($this->bb->input['import_templates'] ? 0 : 1),
                            'version_compat' => $this->bb->getInput('version_compat', 0),
                            'parent' => $this->bb->getInput('tid', 0),
                            'force_name_check' => true,
                        ];
                        $theme_id = $themeHelper->importThemeXml($contents, $options);

                        if ($theme_id > -1) {
                            $this->plugins->runHooks('admin_style_themes_import_commit');

                            // Log admin action
                            $this->bblogger->log_admin_action($theme_id);

                            $this->session->flash_message($this->lang->success_imported_theme, 'success');
                            return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme_id);
                        } else {
                            switch ($theme_id) {
                                case -1:
                                    $this->errors[] = $this->lang->error_uploadfailed_nocontents;
                                    break;
                                case -2:
                                    $this->errors[] = $this->lang->error_invalid_version;
                                    break;
                                case -3:
                                    $this->errors[] = $this->lang->error_theme_already_exists;
                                    break;
                                case -4:
                                    $this->errors[] = $this->lang->error_theme_security_problem;
                            }
                        }
                    }
                }
            }

            $query = $this->db->simple_select('themes', 'tid, name');
            while ($theme = $this->db->fetch_array($query)) {
                $themes[$theme['tid']] = $theme['name'];
            }

            $this->page->add_breadcrumb_item($this->lang->import_a_theme, $this->bb->admin_url . '/style?action=import');

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->import_a_theme}");

            $html .= $this->page->output_nav_tabs($sub_tabs, 'import_theme');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);

                if ($this->bb->input['import'] == 1) {
                    $import_checked[1] = '';
                    $import_checked[2] = 'checked="checked"';
                } else {
                    $import_checked[1] = 'checked="checked"';
                    $import_checked[2] = '';
                }
            } else {
                $import_checked[1] = 'checked="checked"';
                $import_checked[2] = '';

                $this->bb->input['import_stylesheets'] = true;
                $this->bb->input['import_templates'] = true;
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=import', 'post', '', 1);
            $html .= $form->getForm();

            $actions = '<script type="text/javascript">
	function checkAction(id)
	{
		var checked = \'\';

		$(\'.\'+id+\'s_check\').each(function(e, val)
		{
			if($(this).prop(\'checked\') == true)
			{
				checked = $(this).val();
			}
		});
		$(\'.\'+id+\'s\').each(function(e)
		{
			$(this).hide();
		});
		if($(\'#\'+id+\'_\'+checked))
		{
			$(\'#\'+id+\'_\'+checked).show();
		}
	}
</script>
	<dl style="margin-top: 0; margin-bottom: 0; width: 35%;">
	<dt><label style="display: block;"><input type="radio" name="import" value="0" ' . $import_checked[1] . ' class="imports_check" onclick="checkAction(\'import\');" style="vertical-align: middle;" /> ' . $this->lang->local_file . '</label></dt>
		<dd style="margin-top: 0; margin-bottom: 0; width: 100%;" id="import_0" class="imports">
	<table cellpadding="4">
				<tr>
					<td>' . $form->generate_file_upload_box("local_file", ['style' => 'width: 230px;']) . '</td>
				</tr>
		</table>
		</dd>
		<dt><label style="display: block;"><input type="radio" name="import" value="1" ' . $import_checked[2] . ' class="imports_check" onclick="checkAction(\'import\');" style="vertical-align: middle;" /> ' . $this->lang->url . '</label></dt>
		<dd style="margin-top: 0; margin-bottom: 0; width: 100%;" id="import_1" class="imports">
		<table cellpadding="4">
				<tr>
					<td>' . $form->generate_text_box("url", $this->bb->input['file']) . '</td>
				</tr>
		</table></dd>
	</dl>
	<script type="text/javascript">
	checkAction(\'import\');
	</script>';

            $form_container = new FormContainer($this, $this->lang->import_a_theme);
            $form_container->output_row($this->lang->import_from, $this->lang->import_from_desc, $actions, 'file');
            $form_container->output_row($this->lang->parent_theme, $this->lang->parent_theme_desc, $form->generate_select_box('tid', $themes, $this->bb->input['tid'], ['id' => 'tid']), 'tid');
            $form_container->output_row($this->lang->new_name, $this->lang->new_name_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->advanced_options, '', $form->generate_check_box('version_compat', '1', $this->lang->ignore_version_compatibility, ['checked' => $this->bb->input['version_compat'], 'id' => 'version_compat']) . "<br /><small>{$this->lang->ignore_version_compat_desc}</small><br />" . $form->generate_check_box('import_stylesheets', '1', $this->lang->import_stylesheets, ['checked' => $this->bb->input['import_stylesheets'], 'id' => 'import_stylesheets']) . "<br /><small>{$this->lang->import_stylesheets_desc}</small><br />" . $form->generate_check_box('import_templates', '1', $this->lang->import_templates, ['checked' => $this->bb->input['import_templates'], 'id' => 'import_templates']) . "<br /><small>{$this->lang->import_templates_desc}</small>");

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->import_theme);

            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesImport.html.twig');
        }

        if ($this->bb->input['action'] == 'export') {
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$theme['tid']) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_export');

            if ($this->bb->request_method == 'post') {
                $properties = my_unserialize($theme['properties']);

                $xml = "<?xml version=\"1.0\" encoding=\"{$this->lang->settings['charset']}\"?" . ">\r\n";
                $xml .= "<theme name=\"" . htmlspecialchars_uni($theme['name']) . "\" version=\"" . $this->bb->version_code . "\">\r\n";
                $xml .= "\t<properties>\r\n";
                foreach ($properties as $property => $value) {
                    if ($property == 'inherited') {
                        continue;
                    }

                    if (is_array($value)) {
                        $value = my_serialize($value);
                    }

                    $value = str_replace(']]>', ']]]]><![CDATA[>', $value);

                    $xml .= "\t\t<{$property}><![CDATA[{$value}]]></{$property}>\r\n";
                }
                $xml .= "\t</properties>\r\n";

                // Fetch list of all of the stylesheets for this theme
                $file_stylesheets = my_unserialize($theme['stylesheets']);

                $stylesheets = [];
                $inherited_load = [];

                // Now we loop through the list of stylesheets for each file
                foreach ($file_stylesheets as $file => $action_stylesheet) {
                    if ($file == 'inherited' || !is_array($action_stylesheet)) {
                        continue;
                    }

                    foreach ($action_stylesheet as $action => $style) {
                        foreach ($style as $stylesheet) {
                            $stylesheets[$stylesheet]['applied_to'][$file][] = $action;
                            if (is_array($file_stylesheets['inherited'][$file . '_' . $action]) && in_array($stylesheet, array_keys($file_stylesheets['inherited'][$file . '_' . $action]))) {
                                $stylesheets[$stylesheet]['inherited'] = $file_stylesheets['inherited'][$file . '_' . $action];
                                foreach ($file_stylesheets['inherited'][$file . '_' . $action] as $value) {
                                    $inherited_load[] = $value;
                                }
                            }
                        }
                    }
                }

                $inherited_load[] = $this->bb->input['tid'];
                $inherited_load = array_unique($inherited_load);

                $inherited_themes = [];
                if (count($inherited_load) > 0) {
                    $query = $this->db->simple_select('themes', 'tid, name', 'tid IN (' . implode(',', $inherited_load) . ')');
                    while ($inherited_theme = $this->db->fetch_array($query)) {
                        $inherited_themes[$inherited_theme['tid']] = $inherited_theme['name'];
                    }
                }

                $theme_stylesheets = [];

                if (count($inherited_load) > 0) {
                    $query = $this->db->simple_select('themestylesheets', '*', 'tid IN (' . implode(',', $inherited_load) . ')', ['order_by' => 'tid', 'order_dir' => 'desc']);
                    while ($theme_stylesheet = $this->db->fetch_array($query)) {
                        if (!$theme_stylesheets[$theme_stylesheet['cachefile']]) {
                            $theme_stylesheets[$theme_stylesheet['cachefile']] = $theme_stylesheet;
                            $theme_stylesheets[$theme_stylesheet['sid']] = $theme_stylesheet['cachefile'];
                        }
                    }
                }

                $xml .= "\t<stylesheets>\r\n";
                foreach ($stylesheets as $filename => $style) {
                    if (strpos($filename, 'css?stylesheet=') !== false) {
                        $style['sid'] = (int)str_replace('css?stylesheet=', '', $filename);
                        $filename = $theme_stylesheets[$style['sid']];
                    } else {
                        $filename = basename($filename);
                        $style['sid'] = $theme_stylesheets[$filename]['sid'];
                    }

                    $style['tid'] = $theme_stylesheets[$filename]['tid'];

                    if ($this->bb->input['custom_theme'] == 1 && $style['tid'] != $this->bb->input['tid']) {
                        continue;
                    }

                    // Has the file on the file system been modified?
                    $themeHelper->resyncStylesheet($theme_stylesheets[$filename]);

                    $style['sid'] = $theme_stylesheets[$filename]['sid'];

                    $attachedto = $theme_stylesheets[$filename]['attachedto'];
                    $stylesheet = $theme_stylesheets[$filename]['stylesheet'];
                    $stylesheet = str_replace(']]>', ']]]]><![CDATA[>', $stylesheet);

                    if ($attachedto) {
                        $attachedto = "attachedto=\"{$attachedto}\" ";
                    }

                    $filename = $theme_stylesheets[$filename]['name'];

                    $xml .= "\t\t<stylesheet name=\"{$filename}\" {$attachedto}version=\"{$this->bb->version_code}\"><![CDATA[{$stylesheet}]]>\r\n\t\t</stylesheet>\r\n";
                }
                $xml .= "\t</stylesheets>\r\n";

                if ($this->bb->input['include_templates'] != 0) {
                    $xml .= "\t<templates>\r\n";
                    $query = $this->db->simple_select('templates', '*', "sid='" . $properties['templateset'] . "'");
                    while ($template = $this->db->fetch_array($query)) {
                        $template['template'] = str_replace(']]>', ']]]]><![CDATA[>', $template['template']);
                        $xml .= "\t\t<template name=\"{$template['title']}\" version=\"{$template['version']}\"><![CDATA[{$template['template']}]]></template>\r\n";
                    }
                    $xml .= "\t</templates>\r\n";
                }
                $xml .= '</theme>';

                $this->plugins->runHooks('admin_style_themes_export_commit');

                // Log admin action
                $this->bblogger->log_admin_action($theme['tid'], htmlspecialchars_uni($theme['name']));

                $theme['name'] = rawurlencode($theme['name']);
                header('Content-disposition: attachment; filename=' . $theme['name'] . '-theme.xml');
                header('Content-type: application/octet-stream');
                header('Content-Length: ' . strlen($xml));
                header('Pragma: no-cache');
                header('Expires: 0');
                echo $xml;
                exit;
            }

            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);

            $this->page->add_breadcrumb_item($this->lang->export_theme, $this->bb->admin_url . '/style?action=export');

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->export_theme}");

            $sub_tabs['edit_stylesheets'] = [
                'title' => $this->lang->edit_stylesheets,
                'link' => $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid'],
            ];

            $sub_tabs['add_stylesheet'] = [
                'title' => $this->lang->add_stylesheet,
                'link' => $this->bb->admin_url . '/style?action=add_stylesheet&tid=' . $this->bb->input['tid'],
            ];

            $sub_tabs['export_theme'] = [
                'title' => $this->lang->export_theme,
                'link' => $this->bb->admin_url . '/style?action=export&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->export_theme_desc
            ];

            $sub_tabs['duplicate_theme'] = [
                'title' => $this->lang->duplicate_theme,
                'link' => $this->bb->admin_url . '/style?action=duplicate&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->duplicate_theme_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'export_theme');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=export', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $theme['tid']);

            $form_container = new FormContainer($this, $this->lang->export_theme . ': ' . htmlspecialchars_uni($theme['name']));
            $form_container->output_row($this->lang->include_custom_only, $this->lang->include_custom_only_desc, $form->generate_yes_no_radio('custom_theme', $this->bb->getInput('custom_theme', '')), 'custom_theme');
            $form_container->output_row($this->lang->include_templates, $this->lang->include_templates_desc, $form->generate_yes_no_radio('include_templates', $this->bb->getInput('include_templates', '')), 'include_templates');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->export_theme);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesExport.html.twig');
        }

        if ($this->bb->input['action'] == 'duplicate') {
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$theme['tid']) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_duplicate');

            if ($this->bb->request_method == 'post') {
                if ($this->bb->input['name'] == '') {
                    $this->errors[] = $this->lang->error_missing_name;
                } else {
                    $query = $this->db->simple_select('themes', 'COUNT(tid) as numthemes', "name = '" . $this->db->escape_string($this->bb->getInput('name', '')) . "'");
                    $numthemes = $this->db->fetch_field($query, 'numthemes');

                    if ($numthemes) {
                        $this->errors[] = $this->lang->error_theme_already_exists;
                    }
                }

                if (empty($this->errors)) {
                    $properties = my_unserialize($theme['properties']);
                    $sid = $properties['templateset'];
                    $nprops = null;
                    if ($this->bb->input['duplicate_templates']) {
                        $nsid = $this->db->insert_query('templatesets', ['title' => $this->db->escape_string($this->bb->input['name']) . ' Templates']);

                        // Copy all old Templates to our new templateset
                        $query = $this->db->simple_select('templates', '*', "sid='{$sid}'");
                        while ($template = $this->db->fetch_array($query)) {
                            $insert = [
                                'title' => $this->db->escape_string($template['title']),
                                'template' => $this->db->escape_string($template['template']),
                                'sid' => $nsid,
                                'version' => $this->db->escape_string($template['version']),
                                'dateline' => TIME_NOW
                            ];

                            if ($this->db->engine == 'pgsql') {
                                echo ' ';
                                flush();
                            }

                            $this->db->insert_query('templates', $insert);
                        }

                        // We need to change the templateset so we need to work out the others properties too
                        foreach ($properties as $property => $value) {
                            if ($property == 'inherited') {
                                continue;
                            }

                            $nprops[$property] = $value;
                            if ($properties['inherited'][$property]) {
                                $nprops['inherited'][$property] = $properties['inherited'][$property];
                            } else {
                                $nprops['inherited'][$property] = $theme['tid'];
                            }
                        }
                        $nprops['templateset'] = $nsid;
                    }
                    $tid = $themeHelper->build_new_theme($this->bb->input['name'], $nprops, $theme['tid']);

                    $themeHelper->update_theme_stylesheet_list($tid);

                    $this->plugins->runHooks('admin_style_themes_duplicate_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($tid, $theme['tid']);

                    $this->session->flash_message($this->lang->success_duplicated_theme, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $tid);
                }
            }

            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);

            $this->page->add_breadcrumb_item($this->lang->duplicate_theme, $this->bb->admin_url . '/style?action=duplicate&tid=' . $theme['tid']);

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->duplicate_theme}");

            $sub_tabs['edit_stylesheets'] = [
                'title' => $this->lang->edit_stylesheets,
                'link' => $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid'],
            ];

            $sub_tabs['add_stylesheet'] = [
                'title' => $this->lang->add_stylesheet,
                'link' => $this->bb->admin_url . '/style?action=add_stylesheet&tid=' . $this->bb->input['tid'],
            ];

            $sub_tabs['export_theme'] = [
                'title' => $this->lang->export_theme,
                'link' => $this->bb->admin_url . '/style?action=export&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->export_theme_desc
            ];

            $sub_tabs['duplicate_theme'] = [
                'title' => $this->lang->duplicate_theme,
                'link' => $this->bb->admin_url . '/style?action=duplicate&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->duplicate_theme_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'duplicate_theme');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            } else {
                $this->bb->input['duplicate_templates'] = true;
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=duplicate&tid=' . $theme['tid'], 'post');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->duplicate_theme);
            $form_container->output_row($this->lang->new_name, $this->lang->new_name_duplicate_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->advanced_options, '', $form->generate_check_box('duplicate_templates', '1', $this->lang->duplicate_templates, ['checked' => $this->bb->input['duplicate_templates'], 'id' => 'duplicate_templates']) . "<br /><small>{$this->lang->duplicate_templates_desc}</small>");

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->duplicate_theme);

            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesDuplicate.html.twig');
        }

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_style_themes_add');

            $query = $this->db->simple_select('themes', 'tid, name');
            while ($theme = $this->db->fetch_array($query)) {
                $themes[$theme['tid']] = $theme['name'];
            }

            if ($this->bb->request_method == 'post') {
                if (!$this->bb->input['name']) {
                    $this->errors[] = $this->lang->error_missing_name;
                } elseif (in_array($this->bb->input['name'], $themes)) {
                    $this->errors[] = $this->lang->error_theme_already_exists;
                }

                if (empty($this->errors)) {
                    $tid = $themeHelper->build_new_theme($this->bb->input['name'], null, $this->bb->input['tid']);

                    $this->plugins->runHooks('admin_style_themes_add_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action(htmlspecialchars_uni($this->bb->input['name']), $tid);

                    $this->session->flash_message($this->lang->success_theme_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $tid);
                }
            }

            $this->page->add_breadcrumb_item($this->lang->create_new_theme, $this->bb->admin_url . '/style?action=add');

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->create_new_theme}");

            $html .= $this->page->output_nav_tabs($sub_tabs, 'create_theme');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=add', 'post');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->create_a_theme);
            $form_container->output_row($this->lang->name, $this->lang->name_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name']), 'name');
            $form_container->output_row($this->lang->parent_theme, $this->lang->parent_theme_desc, $form->generate_select_box('tid', $themes, $this->bb->input['tid'], ['id' => 'tid']), 'tid');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->create_new_theme);

            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            // Does the theme not exist? or are we trying to delete the master?
            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_delete');

            if ($this->bb->request_method == 'post') {
                $inherited_theme_cache = [];

                $query = $this->db->simple_select('themes', 'tid,stylesheets', "tid != '{$theme['tid']}'", ['order_by' => 'pid, name']);
                while ($theme2 = $this->db->fetch_array($query)) {
                    $theme2['stylesheets'] = my_unserialize($theme2['stylesheets']);

                    if (!$theme2['stylesheets']['inherited']) {
                        continue;
                    }

                    $inherited_theme_cache[$theme2['tid']] = $theme2['stylesheets']['inherited'];
                }

                $inherited_stylesheets = false;

                // Are any other themes relying on stylesheets from this theme? Get a list and show an error
                foreach ($inherited_theme_cache as $tid => $inherited) {
                    foreach ($inherited as $file => $value) {
                        foreach ($value as $filepath => $val) {
                            if (strpos($filepath, "themes/theme{$theme['tid']}") !== false) {
                                $inherited_stylesheets = true;
                            }
                        }
                    }
                }

                if ($inherited_stylesheets == true) {
                    $this->session->flash_message($this->lang->error_inheriting_stylesheets, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/style');
                }

                $query = $this->db->simple_select('themestylesheets', 'cachefile', "tid='{$theme['tid']}'");
                while ($cachefile = $this->db->fetch_array($query)) {
                    @unlink(DIR . "web/themes/theme{$theme['tid']}/{$cachefile['cachefile']}");

                    $filename_min = str_replace('.css', '.min.css', $cachefile['cachefile']);
                    @unlink(DIR . "web/themes/theme{$theme['tid']}/{$filename_min}");
                }
                @unlink(DIR . "web/themes/theme{$theme['tid']}/index.html");

                $this->db->delete_query('themestylesheets', "tid='{$theme['tid']}'");

                // Update the CSS file list for this theme
                $themeHelper->update_theme_stylesheet_list($theme['tid'], $theme, true);

                $this->db->update_query('users', ['style' => 0], "style='{$theme['tid']}'");

                @rmdir(DIR . "web/themes/theme{$theme['tid']}/");

                $children = $themeHelper->make_child_theme_list($theme['tid']);
                $child_tid = $children[0];

                $this->db->update_query('themes', ['pid' => $theme['pid']], "tid='{$child_tid}'");

                $this->db->delete_query('themes', "tid='{$theme['tid']}'", 1);

                $this->plugins->runHooks('admin_style_themes_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($theme['tid'], htmlspecialchars_uni($theme['name']));

                $this->session->flash_message($this->lang->success_theme_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/style?action=delete&tid=' . $theme['tid'], $this->lang->confirm_theme_deletion);
            }
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_edit');

            if ($this->bb->request_method == 'post' && !$this->bb->input['do']) {
                $properties = [
                    'templateset' => $this->bb->getInput('templateset', 0),
                    'editortheme' => $this->bb->input['editortheme'],
                    'imgdir' => $this->bb->input['imgdir'],
                    'logo' => $this->bb->input['logo'],
                    'tablespace' => $this->bb->getInput('tablespace', 0),
                    'borderwidth' => $this->bb->getInput('borderwidth', 0),
                    'color' => $this->bb->input['color']
                ];

                if ($properties['color'] == 'none') {
                    unset($properties['color']);
                }

                if ($this->bb->input['colors']) {
                    $colors = explode("\n", $this->bb->input['colors']);
                    foreach ($colors as $color) {
                        $color = explode('=', $color);
                        $properties['colors'][$color[0]] = $color[1];
                    }
                }

                if ($properties['templateset'] <= 0) {
                    $this->errors[] = $this->lang->error_invalid_templateset;
                }

                $theme_properties = my_unserialize($theme['properties']);
                if (is_array($theme_properties['disporder'])) {
                    $properties['disporder'] = $theme_properties['disporder'];
                } else {
                    $this->errors[] = $this->lang->error_no_display_order;
                }

                $allowedgroups = [];
                if (is_array($this->bb->input['allowedgroups'])) {
                    foreach ($this->bb->input['allowedgroups'] as $gid) {
                        if ($gid == 'all') {
                            $allowedgroups = 'all';
                            break;
                        }
                        $gid = (int)$gid;
                        $allowedgroups[$gid] = $gid;
                    }
                }
                if (is_array($allowedgroups)) {
                    $allowedgroups = implode(',', $allowedgroups);
                }

                $update_array = [
                    'name' => $this->db->escape_string($this->bb->input['name']),
                    'pid' => $this->bb->getInput('pid', 0),
                    'allowedgroups' => $allowedgroups,
                    'properties' => $this->db->escape_string(my_serialize($properties))
                ];

                // perform validation
                if (!$update_array['name']) {
                    $this->errors[] = $this->lang->error_missing_name;
                } else {
                    $query = $this->db->simple_select('themes', 'COUNT(tid) as numthemes', "name = '" . $this->db->escape_string($update_array['name']) . "' AND tid != '{$theme['tid']}'");
                    $numthemes = $this->db->fetch_field($query, 'numthemes');

                    if ($numthemes) {
                        $this->errors[] = $this->lang->error_theme_already_exists;
                    }
                }

                if ($update_array['pid']) {
                    $query = $this->db->simple_select('themes', 'tid', "tid='" . $update_array['pid'] . "'");
                    $parent_check = $this->db->fetch_field($query, 'tid');
                    if (!$parent_check) {
                        $this->errors[] = $this->lang->error_invalid_parent_theme;
                    }
                }
                if ($properties['templateset']) {
                    $query = $this->db->simple_select('templatesets', 'sid', "sid='" . $properties['templateset'] . "'");
                    $ts_check = $this->db->fetch_field($query, 'sid');
                    if (!$ts_check) {
                        unset($properties['templateset']);
                    }
                }
                if (!$properties['templateset']) {
                    $this->errors[] = $this->lang->error_invalid_templateset;
                }

                if (!$properties['editortheme'] ||
                    !file_exists(DIR . 'web/assets/runbb/jscripts/sceditor/editor_themes/' . $properties['editortheme']) ||
                    is_dir(DIR . 'web/assets/runbb/jscripts/sceditor/editor_themes/' . $properties['editortheme'])
                ) {
                    $this->errors[] = $this->lang->error_invalid_editortheme;
                }

                if (empty($this->errors)) {
                    $this->plugins->runHooks('admin_style_themes_edit_commit');

                    $this->db->update_query('themes', $update_array, "tid='{$theme['tid']}'");
                    $themeHelper->update_theme_stylesheet_list($theme['tid']);

                    if ($theme['def'] == 1) {
                        $this->cache->update_default_theme();
                    }

                    // Log admin action
                    $this->bblogger->log_admin_action($theme['tid'], htmlspecialchars_uni($theme['name']));

                    $this->session->flash_message($this->lang->success_theme_properties_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
                }
            }

            // Fetch list of all of the stylesheets for this theme
            $file_stylesheets = my_unserialize($theme['stylesheets']);

            $stylesheets = [];
            $inherited_load = [];
            // Now we loop through the list of stylesheets for each file
            foreach ($file_stylesheets as $file => $action_stylesheet) {
                if ($file == 'inherited' || !is_array($action_stylesheet)) {
                    continue;
                }

                foreach ($action_stylesheet as $action => $style) {
                    foreach ($style as $stylesheet) {
                        $stylesheets[$stylesheet]['applied_to'][$file][] = $action;
                        //if(is_array($file_stylesheets['inherited'][$file.'_'.$action]) &&
                        if (isset($file_stylesheets['inherited'][$file . '_' . $action]) &&
                            in_array($stylesheet, array_keys($file_stylesheets['inherited'][$file . '_' . $action]))
                        ) {
                            $stylesheets[$stylesheet]['inherited'] = $file_stylesheets['inherited'][$file . '_' . $action];
                            foreach ($file_stylesheets['inherited'][$file . '_' . $action] as $value) {
                                $inherited_load[] = $value;
                            }
                        }
                    }
                }
            }

            $inherited_load[] = $this->bb->input['tid'];
            $inherited_load = array_unique($inherited_load);
            $inherited_themes = [];
            if (count($inherited_load) > 0) {
                $query = $this->db->simple_select('themes', 'tid, name', 'tid IN (' . implode(',', $inherited_load) . ')');
                while ($inherited_theme = $this->db->fetch_array($query)) {
                    $inherited_themes[$inherited_theme['tid']] = $inherited_theme['name'];
                }
            }

            $theme_stylesheets = [];

            if (count($inherited_load) > 0) {
                $query = $this->db->simple_select('themestylesheets', '*', '', ['order_by' => 'sid DESC, tid', 'order_dir' => 'desc']);
                while ($theme_stylesheet = $this->db->fetch_array($query)) {
                    if (!isset($theme_stylesheets[$theme_stylesheet['name']]) && in_array($theme_stylesheet['tid'], $inherited_load)) {
                        $theme_stylesheets[$theme_stylesheet['name']] = $theme_stylesheet;
                    }

                    $theme_stylesheets[$theme_stylesheet['sid']] = $theme_stylesheet['name'];
                }
            }
            // Save any stylesheet orders
            if ($this->bb->request_method == 'post' && $this->bb->input['do'] == 'save_orders') {
                if (!is_array($this->bb->input['disporder'])) {
                    // Error out
                    $this->session->flash_message($this->lang->error_no_display_order, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
                }

                $orders = [];
                foreach ($theme_stylesheets as $stylesheet => $properties) {
                    if (is_array($properties)) {
                        $order = (int)$this->bb->input['disporder'][$properties['sid']];

                        $orders[$properties['name']] = $order;
                    }
                }

                asort($orders, SORT_NUMERIC);

                // Save the orders in the theme properties
                $properties = my_unserialize($theme['properties']);
                $properties['disporder'] = $orders;

                $update_array = [
                    'properties' => $this->db->escape_string(my_serialize($properties))
                ];

                $this->db->update_query('themes', $update_array, "tid = '{$theme['tid']}'");

                if ($theme['def'] == 1) {
                    $this->cache->update_default_theme();
                }

                // normalize for consistency
                $themeHelper->update_theme_stylesheet_list($theme['tid'], false, true);

                $this->session->flash_message($this->lang->success_stylesheet_order_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
            }

            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->stylesheets}");

            $sub_tabs['edit_stylesheets'] = [
                'title' => $this->lang->edit_stylesheets,
                'link' => $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->edit_stylesheets_desc
            ];

            $sub_tabs['add_stylesheet'] = [
                'title' => $this->lang->add_stylesheet,
                'link' => $this->bb->admin_url . '/style?action=add_stylesheet&tid=' . $this->bb->input['tid'],
            ];

            $sub_tabs['export_theme'] = [
                'title' => $this->lang->export_theme,
                'link' => $this->bb->admin_url . '/style?action=export&tid=' . $this->bb->input['tid']
            ];

            $sub_tabs['duplicate_theme'] = [
                'title' => $this->lang->duplicate_theme,
                'link' => $this->bb->admin_url . '/style?action=duplicate&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->duplicate_theme_desc
            ];

            $properties = my_unserialize($theme['properties']);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_stylesheets');

            $table = new Table;
            $table->construct_header($this->lang->stylesheets);
            $table->construct_header($this->lang->display_order, ['class' => 'align_center', 'width' => 50]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            // Display Order form
            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=edit', 'post', 'edit');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $theme['tid']);
            $html .= $form->generate_hidden_field('do', 'save_orders');

            // Order the stylesheets
            $ordered_stylesheets = [];
            foreach ($properties['disporder'] as $style_name => $order) {
                foreach ($stylesheets as $filename => $style) {
                    if (strpos($filename, 'css?stylesheet=') !== false) {
                        $style['sid'] = (int)str_replace('css?stylesheet=', '', $filename);
                        $filename = $theme_stylesheets[$style['sid']];
                    }

                    if (basename($filename) != $style_name) {
                        continue;
                    }

                    $ordered_stylesheets[$filename] = $style;
                }
            }
            foreach ($ordered_stylesheets as $filename => $style) {
                if (strpos($filename, 'css?stylesheet=') !== false) {
                    $style['sid'] = (int)str_replace('css?stylesheet=', '', $filename);
                    $filename = $theme_stylesheets[$style['sid']];
                } else {
                    $filename = basename($filename);
                    $style['sid'] = $theme_stylesheets[$filename]['sid'];
                }

                // Has the file on the file system been modified?
                $themeHelper->resyncStylesheet($theme_stylesheets[$filename]);

                $filename = $theme_stylesheets[$filename]['name'];

                $inherited = '';
                $inherited_ary = [];
                //if(is_array($style['inherited']))
                if (isset($style['inherited'])) {
                    foreach ($style['inherited'] as $tid) {
                        if ($inherited_themes[$tid]) {
                            $inherited_ary[$tid] = $inherited_themes[$tid];
                        }
                    }
                }

                if (!empty($inherited_ary)) {
                    $inherited = " <small>({$this->lang->inherited_from}";
                    $sep = ' ';
                    $inherited_count = count($inherited_ary);
                    $count = 0;

                    foreach ($inherited_ary as $tid => $file) {
                        if (isset($applied_to_count) && $count == $applied_to_count && $count != 0) {
                            $sep = " {$this->lang->and} ";
                        }

                        $inherited .= $sep . $file;
                        $sep = $this->lang->comma;

                        ++$count;
                    }
                    $inherited .= ')</small>';
                }

                if (is_array($style['applied_to']) &&
                    (!isset($style['applied_to']['global']) ||
                        $style['applied_to']['global'][0] != 'global')
                ) {
                    $attached_to = '';

                    $applied_to_count = count($style['applied_to']);
                    $count = 0;
                    $sep = ' ';
                    $name = '';
                    $colors = [];

                    if (!isset($properties['colors'])) {
                        $properties['colors'] = [];
                    }

                    foreach ($style['applied_to'] as $name => $actions) {
                        if (!$name) {
                            continue;
                        }

                        if (array_key_exists($name, $properties['colors'])) {
                            $colors[] = $properties['colors'][$name];
                        }

                        if (count($colors)) {
                            // Colors override files and are handled below.
                            continue;
                        }

                        // It's a file:
                        ++$count;

                        if ($actions[0] != 'global') {
                            $name = "{$name} ({$this->lang->actions}: " . implode(',', $actions) . ')';
                        }

                        if ($count == $applied_to_count && $count > 1) {
                            $sep = " {$this->lang->and} ";
                        }
                        $attached_to .= $sep . $name;

                        $sep = $this->lang->comma;
                    }

                    if ($attached_to) {
                        $attached_to = "<small>{$this->lang->attached_to} {$attached_to}</small>";
                    }

                    if (count($colors)) {
                        // Attached to color instead of files.
                        $count = 1;
                        $color_list = $sep = '';

                        foreach ($colors as $color) {
                            if ($count == count($colors) && $count > 1) {
                                $sep = " {$this->lang->and} ";
                            }

                            $color_list .= $sep . trim($color);
                            ++$count;

                            $sep = ', ';
                        }

                        $attached_to = "<small>{$this->lang->attached_to} " . $this->lang->sprintf($this->lang->colors_attached_to) . " {$color_list}</small>";
                    }

                    if ($attached_to == '') {
                        // Orphaned! :(
                        $attached_to = "<small>{$this->lang->attached_to_nothing}</small>";
                    }
                } else {
                    $attached_to = "<small>{$this->lang->attached_to_all_pages}</small>";
                }

                $popup = new PopupMenu("style_{$style['sid']}", $this->lang->options);

                $popup->add_item($this->lang->edit_style, $this->bb->admin_url . '/style?action=edit_stylesheet&file=' . htmlspecialchars_uni($filename) . '&tid=' . $theme['tid']);
                $popup->add_item($this->lang->properties, $this->bb->admin_url . '/style?action=stylesheet_properties&file=' . htmlspecialchars_uni($filename) . '&tid=' . $theme['tid']);

                if ($inherited == '') {
                    $popup->add_item($this->lang->delete_revert, $this->bb->admin_url . '/style?action=delete_stylesheet&file=' . htmlspecialchars_uni($filename) . "&amp;tid={$theme['tid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_stylesheet_deletion}')");
                }

                $table->construct_cell('<strong><a href="'.$this->bb->admin_url.'/style?action=edit_stylesheet&file=' . htmlspecialchars_uni($filename) . "&amp;tid={$theme['tid']}\">{$filename}</a></strong>{$inherited}<br />{$attached_to}");
                $table->construct_cell($form->generate_numeric_field("disporder[{$theme_stylesheets[$filename]['sid']}]", $properties['disporder'][$filename], ['style' => 'width: 80%; text-align: center;', 'min' => 0]), ["class" => "align_center"]);
                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            $html .= $table->output("{$this->lang->stylesheets_in} " . htmlspecialchars_uni($theme['name']));

            $buttons = [$form->generate_submit_button($this->lang->save_stylesheet_order)];
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();
            $html .= '<br />';

            // Theme Properties table
            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=edit', 'post', 'edit');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $theme['tid']);
            $form_container = new FormContainer($this, $this->lang->edit_theme_properties);
            $form_container->output_row($this->lang->name . ' <em>*</em>', $this->lang->name_desc_edit, $form->generate_text_box('name', $theme['name'], ['id' => 'name']), 'name');

            $options = $themeHelper->build_theme_array($theme['tid']);
            $form_container->output_row($this->lang->parent_theme . ' <em>*</em>', $this->lang->parent_theme_desc, $form->generate_select_box('pid', $options, $theme['pid'], ['id' => 'pid']), 'pid');

            $options = [];
            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            $options['all'] = $this->lang->all_user_groups;
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[(int)$usergroup['gid']] = $usergroup['title'];
            }
            $form_container->output_row($this->lang->allowed_user_groups, $this->lang->allowed_user_groups_desc, $form->generate_select_box('allowedgroups[]', $options, explode(',', $theme['allowedgroups']), ['id' => 'allowedgroups', 'multiple' => true, 'size' => 5]), 'allowedgroups');

            $options = [];
            $query = $this->db->simple_select('templatesets', '*', '', ['order_by' => 'title']);
            while ($templateset = $this->db->fetch_array($query)) {
                $options[(int)$templateset['sid']] = $templateset['title'];
            }
            $form_container->output_row($this->lang->template_set . ' <em>*</em>', $this->lang->template_set_desc, $form->generate_select_box('templateset', $options, $properties['templateset'], ['id' => 'templateset']), 'templateset');

            $options = [];
            $editor_theme_root = DIR . 'web/assets/runbb/jscripts/sceditor/editor_themes/';
            if ($dh = @opendir($editor_theme_root)) {
                while ($dir = readdir($dh)) {
                    if ($dir == '.svn' || $dir == '.' || $dir == '..' ||
                        is_dir($editor_theme_root . $dir) ||
                        get_extension($editor_theme_root . $dir) != 'css'
                    ) {
                        continue;
                    }
                    $options[$dir] = ucfirst(str_replace(['_', '.css'], [' ', ''], $dir));
                }
            }
            $form_container->output_row($this->lang->editor_theme . ' <em>*</em>', $this->lang->editor_theme_desc, $form->generate_select_box('editortheme', $options, $properties['editortheme'], ['id' => 'editortheme']), 'editortheme');

            $form_container->output_row($this->lang->img_directory, $this->lang->img_directory_desc, $form->generate_text_box('imgdir', $properties['imgdir'], ['id' => 'imgdir']), 'imgdir');
            $form_container->output_row($this->lang->logo, $this->lang->logo_desc, $form->generate_text_box('logo', $properties['logo'], ['id' => 'boardlogo']), 'logo');
            $form_container->output_row($this->lang->table_spacing, $this->lang->table_spacing_desc, $form->generate_numeric_field('tablespace', $properties['tablespace'], ['id' => 'tablespace', 'min' => 0]), 'tablespace');
            $form_container->output_row($this->lang->inner_border, $this->lang->inner_border_desc, $form->generate_numeric_field('borderwidth', $properties['borderwidth'], ['id' => 'borderwidth', 'min' => 0]), 'borderwidth');

            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->colors_manage);

            if (!isset($properties['colors']) || !is_array($properties['colors'])) {
                $color_setting = $this->lang->colors_no_color_setting;
            } else {
                $colors = ['none' => $this->lang->colors_please_select];
                $colors = array_merge($colors, $properties['colors']);

                $color_setting = $form->generate_select_box(
                    'color',
                    $colors,
                    isset($properties['color']) ? $properties['color'] : '',
                    ['class' => "select\" style=\"width: 200px;"]
                );

                $this->bb->input['colors'] = '';
                foreach ($properties['colors'] as $key => $color) {
                    if ($this->bb->input['colors']) {
                        $this->bb->input['colors'] .= "\n";
                    }

                    $this->bb->input['colors'] .= "{$key}={$color}";
                }
            }

            $form_container->output_row($this->lang->colors_setting, $this->lang->colors_setting_desc, $color_setting, 'color');
            $form_container->output_row($this->lang->colors_add, $this->lang->colors_add_desc, $form->generate_text_area('colors', $this->bb->getInput('colors', ''), ['style' => 'width: 200px;', 'rows' => '5']));

            $html .= $form_container->end();

            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->save_theme_properties);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'stylesheet_properties') {
            // Fetch the theme we want to edit this stylesheet in
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_stylesheet_properties');

            $parent_list = $themeHelper->make_parent_theme_list($theme['tid']);
            $parent_list = implode(',', $parent_list);
            if (!$parent_list) {
                $parent_list = 1;
            }

            $query = $this->db->simple_select('themestylesheets', '*', "name='" . $this->db->escape_string($this->bb->input['file']) . "' AND tid IN ({$parent_list})", ['order_by' => 'tid', 'order_dir' => 'desc', 'limit' => 1]);
            $stylesheet = $this->db->fetch_array($query);
            // Does the theme not exist?
            if (!$stylesheet['sid']) {
                $this->session->flash_message($this->lang->error_invalid_stylesheet, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }
            // Fetch list of all of the stylesheets for this theme
            $stylesheets = $themeHelper->fetch_theme_stylesheets($theme);

            if (!array_key_exists($stylesheet['cachefile'], $stylesheets) &&
                array_key_exists('css?stylesheet=' . $stylesheet['tid'], $stylesheets)
            ) {
                $stylesheet['cachefile'] = 'css?stylesheet=' . $stylesheet['tid'];
            }

            $this_stylesheet = $stylesheets[$stylesheet['cachefile']];
            unset($stylesheets);

            if ($this->bb->request_method == 'post') {
                // Do we not have a name, or is it just an extension?
                if (!$this->bb->input['name'] || $this->bb->input['name'] == '.css') {
                    $this->errors[] = $this->lang->error_missing_stylesheet_name;
                }

                // Get 30 chars only because we don't want more than that
                $this->bb->input['name'] = my_substr($this->bb->input['name'], 0, 30);
                if (get_extension($this->bb->input['name']) != 'css') {
                    // Does not end with '.css'
                    $this->errors[] = $this->lang->sprintf($this->lang->error_missing_stylesheet_extension, $this->bb->input['name']);
                }

                if (empty($this->errors)) {
                    // Theme & stylesheet theme ID do not match, editing inherited - we copy to local theme
                    if ($theme['tid'] != $stylesheet['tid']) {
                        $stylesheet['sid'] = $themeHelper->copy_stylesheet_to_theme($stylesheet, $theme['tid']);
                    }

                    $attached = [];
                    if ($this->bb->input['attach'] == 1) {
                        // Our stylesheet is attached to custom pages in BB
                        foreach ($this->bb->input as $id => $value) {
                            $actions_list = '';
                            $attached_to = $value;

                            if (strpos($id, 'attached_') !== false) {
                                // We have a custom attached file
                                $attached_id = (int)str_replace('attached_', '', $id);

                                if ($this->bb->input['action_' . $attached_id] == 1) {
                                    // We have custom actions for attached files
                                    $actions_list = $this->bb->input['action_list_' . $attached_id];
                                }

                                if ($actions_list) {
                                    $attached_to .= '?' . $actions_list;
                                }

                                $attached[] = $attached_to;
                            }
                        }
                    } elseif ($this->bb->input['attach'] == 2) {
                        if (!is_array($this->bb->input['color'])) {
                            $this->errors[] = $this->lang->error_no_color_picked;
                        } else {
                            $attached = $this->bb->input['color'];
                        }
                    }

                    // Update Stylesheet
                    $update_array = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'attachedto' => $this->db->escape_string(implode('|', $attached))
                    ];

                    if ($stylesheet['name'] != $this->bb->input['name']) {
                        $update_array['cachefile'] = $this->db->escape_string(str_replace('/', '', $this->bb->input['name']));
                    }

                    $this->db->update_query('themestylesheets', $update_array, "sid='{$stylesheet['sid']}'", 1);

                    // If the name changed, re-cache our stylesheet
                    $theme_c = $update_d = false;
                    if ($stylesheet['name'] != $this->bb->input['name']) {
                        // Update the theme stylesheet list if the name is changed
                        $theme_c = $theme;
                        $update_d = true;

                        $this->db->update_query('themestylesheets', ['lastmodified' => TIME_NOW], "sid='{$stylesheet['sid']}'", 1);
                        if (!$themeHelper->cacheStylesheet($theme['tid'], str_replace('/', '', $this->bb->input['name']), $theme['stylesheet'])) {
                            $this->db->update_query('themestylesheets', ['cachefile' => "css?stylesheet={$stylesheet['sid']}"], "sid='{$stylesheet['sid']}'", 1);
                        }
                        @unlink(DIR . "web/themes/theme{$theme['tid']}/{$stylesheet['cachefile']}");

                        $filename_min = str_replace('.css', '.min.css', $stylesheet['cachefile']);
                        @unlink(DIR . "web/themes/theme{$theme['tid']}/{$filename_min}");
                    }

                    // Update the CSS file list for this theme
                    $themeHelper->update_theme_stylesheet_list($theme['tid'], $theme_c, $update_d);

                    $this->plugins->runHooks('admin_style_themes_stylesheet_properties_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($stylesheet['sid'], $this->bb->input['name'], $theme['tid'], htmlspecialchars_uni($theme['name']));

                    $this->session->flash_message($this->lang->success_stylesheet_properties_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
                }
            }

            $properties = my_unserialize($theme['properties']);
            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);
            $this->page->add_breadcrumb_item(htmlspecialchars_uni($stylesheet['name']) . " {$this->lang->properties}", $this->bb->admin_url . '/style?action=edit_properties&tid=' . $this->bb->input['tid']);

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->stylesheet_properties}");

            // If the stylesheet and theme do not match, we must be editing something that is inherited
            if ($this_stylesheet['inherited'][$stylesheet['name']]) {
                $query = $this->db->simple_select('themes', 'name', "tid='{$stylesheet['tid']}'");
                $stylesheet_parent = htmlspecialchars_uni($this->db->fetch_field($query, 'name'));

                // Show inherited warning
                if ($stylesheet['tid'] == 1) {
                    $html .= $this->page->output_alert($this->lang->sprintf($this->lang->stylesheet_inherited_default, $stylesheet_parent));
                } else {
                    $html .= $this->page->output_alert($this->lang->sprintf($this->lang->stylesheet_inherited, $stylesheet_parent));
                }
            }

            $applied_to = $this_stylesheet['applied_to'];
            unset($this_stylesheet);

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);

                foreach ($this->bb->input as $name => $value) {
                    if (strpos($name, 'attached') !== false) {
                        list(, $id) = explode('_', $name);
                        $id = (int)$id;

                        $applied_to[$value] = [0 => 'global'];

                        if ($this->bb->input['action_' . $id] == 1) {
                            $applied_to[$value] = explode(',', $this->bb->input['action_list_' . $id]);
                        }
                    }
                }
            } else {
                $this->bb->input['name'] = $stylesheet['name'];
            }

            $global_checked[1] = 'checked="checked"';
            $global_checked[2] = '';
            $global_checked[3] = '';

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=stylesheet_properties', 'post');
            $html .= $form->getForm();

            $specific_files = '<div id="attach_1" class="attachs">';
            $count = 0;
            if (is_array($applied_to) && $applied_to['global'][0] != 'global') {
                $check_actions = '';
                $stylesheet['colors'] = [];

                if (!is_array($properties['colors'])) {
                    $properties['colors'] = [];
                }

                foreach ($applied_to as $name => $actions) {
                    // Verify this is a color for this theme
                    if (array_key_exists($name, $properties['colors'])) {
                        $stylesheet['colors'][] = $name;
                    }

                    if (count($stylesheet['colors'])) {
                        // Colors override files and are handled below.
                        continue;
                    }

                    // It's a file:
                    $action_list = '';
                    if ($actions[0] != 'global') {
                        $action_list = implode(',', $actions);
                    }

                    if ($actions[0] == 'global') {
                        $global_action_checked[1] = 'checked="checked"';
                        $global_action_checked[2] = '';
                    } else {
                        $global_action_checked[2] = 'checked="checked"';
                        $global_action_checked[1] = '';
                    }

                    $specific_file = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_{$count}\" value=\"0\" {$global_action_checked[1]} class=\"action_{$count}s_check\" onclick=\"checkAction('action_{$count}');\" style=\"vertical-align: middle;\" /> {$this->lang->globally}</label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_{$count}\" value=\"1\" {$global_action_checked[2]} class=\"action_{$count}s_check\" onclick=\"checkAction('action_{$count}');\" style=\"vertical-align: middle;\" /> {$this->lang->specific_actions}</label></dt>
			<dd style=\"margin-top: 4px;\" id=\"action_{$count}_1\" class=\"action_{$count}s\">
			<small class=\"description\">{$this->lang->specific_actions_desc}</small>
			<table cellpadding=\"4\">
				<tr>
					<td>" . $form->generate_text_box('action_list_' . $count, $action_list, ['id' => 'action_list_' . $count, 'style' => 'width: 190px;']) . "</td>
				</tr>
			</table>
		</dd>
		</dl>";

                    $form_container = new FormContainer($this);
                    $form_container->output_row('', '', "<span style=\"float: right;\"><a href=\"\" id=\"delete_img_{$count}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/cross.png\" alt=\"{$this->lang->delete}\" title=\"{$this->lang->delete}\" /></a></span>{$this->lang->file} &nbsp;" . $form->generate_text_box("attached_{$count}", $name, ['id' => "attached_{$count}", 'style' => 'width: 200px;']), "attached_{$count}");

                    $form_container->output_row('', '', $specific_file);

                    $specific_files .= "<div id=\"attached_form_{$count}\">" . $form_container->end(true) . "</div><div id=\"attach_box_" . ($count + 1) . "\"></div>";

                    $check_actions .= "\n\tcheckAction('action_{$count}');";

                    ++$count;
                }

                if ($check_actions) {
                    $global_checked[3] = '';
                    $global_checked[2] = 'checked="checked"';
                    $global_checked[1] = '';
                }

                if (!empty($stylesheet['colors'])) {
                    $global_checked[3] = 'checked="checked"';
                    $global_checked[2] = '';
                    $global_checked[1] = '';
                }
            }

            $specific_files .= '</div>';

            // Colors
            $specific_colors = $specific_colors_option = '';

            if (is_array($properties['colors'])) {
                $specific_colors = "<div id=\"attach_2\" class=\"attachs\">";
                $specific_colors_option = '<dt><label style="display: block;"><input type="radio" name="attach" value="2" ' . $global_checked[3] . ' class="attachs_check" onclick="checkAction(\'attach\');" style="vertical-align: middle;" /> ' . $this->lang->colors_specific_color . '</label></dt><br />';

                $specific_color = "
			<small>{$this->lang->colors_add_edit_desc}</small>
			<br /><br />
			" . $form->generate_select_box('color[]', $properties['colors'], $stylesheet['colors'], ['multiple' => true, 'size' => "5\" style=\"width: 200px;"]) . "
		";

                $form_container = new FormContainer($this);
                $form_container->output_row('', '', $specific_color);
                $specific_colors .= $form_container->end(true) . '</div>';
            }

            $actions = '<script type="text/javascript">
	function checkAction(id)
	{
		var checked = \'\';

		$(\'.\'+id+\'s_check\').each(function(e, val)
		{
			if($(this).prop(\'checked\') == true)
			{
				checked = $(this).val();
			}
		});
		$(\'.\'+id+\'s\').each(function(e)
		{
			$(this).hide();
		});
		if($(\'#\'+id+\'_\'+checked))
		{
			$(\'#\'+id+\'_\'+checked).show();
		}
	}
</script>
	<dl style="margin-top: 0; margin-bottom: 0; width: 40%;">
		<dt><label style="display: block;"><input type="radio" name="attach" value="0" ' . $global_checked[1] . ' class="attachs_check" onclick="checkAction(\'attach\');" style="vertical-align: middle;" /> ' . $this->lang->globally . '</label></dt><br />
		<dt><label style="display: block;"><input type="radio" name="attach" value="1" ' . $global_checked[2] . ' class="attachs_check" onclick="checkAction(\'attach\');" style="vertical-align: middle;" /> ' . $this->lang->specific_files . ' (<a id="new_specific_file">' . $this->lang->add_another . '</a>)</label></dt><br />
		' . $specific_files . '
		' . $specific_colors_option . '
		' . $specific_colors . '
	</dl>
	<script type="text/javascript">
	checkAction(\'attach\');' . ($check_actions ) . '
	</script>';

            $html .= $form->generate_hidden_field('file', htmlspecialchars_uni($stylesheet['name'])) . "<br />\n";
            $html .= $form->generate_hidden_field('tid', $theme['tid']) . "<br />\n";

            $form_container = new FormContainer($this, "{$this->lang->edit_stylesheet_properties_for} " . htmlspecialchars_uni($stylesheet['name']));
            $form_container->output_row($this->lang->file_name, $this->lang->file_name_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name', 'style' => 'width: 200px;']), 'name');

            $form_container->output_row($this->lang->attached_to, $this->lang->attached_to_desc, $actions);

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_stylesheet_properties);

            $html .= $form->output_submit_wrapper($buttons);

            $html .= <<<EOF

	<script type="text/javascript" src="{$this->bb->asset_url}/admin/jscripts/theme_properties.js"></script>
	<script type="text/javascript">
	<!---
	themeProperties.setup('{$count}');
	// -->
	</script>
EOF;

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesStylesheetProperties.html.twig');
        }

        // Shows the page where you can actually edit a particular selector or the whole stylesheet
        if ($this->bb->input['action'] == 'edit_stylesheet' &&
            (!isset($this->bb->input['mode']) || $this->bb->input['mode'] == 'simple')) {
            // Fetch the theme we want to edit this stylesheet in
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_edit_stylesheet_simple');

            $parent_list = $themeHelper->make_parent_theme_list($theme['tid']);
            $parent_list = implode(',', $parent_list);
            if (!$parent_list) {
                $parent_list = 1;
            }

            $query = $this->db->simple_select('themestylesheets', '*', "name='" . $this->db->escape_string($this->bb->input['file']) . "' AND tid IN ({$parent_list})", ['order_by' => 'tid', 'order_dir' => 'desc', 'limit' => 1]);
            $stylesheet = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$stylesheet['sid']) {
                $this->session->flash_message($this->lang->error_invalid_stylesheet, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            if ($this->bb->request_method == 'post') {
                $sid = $stylesheet['sid'];

                // Theme & stylesheet theme ID do not match, editing inherited - we copy to local theme
                if ($theme['tid'] != $stylesheet['tid']) {
                    $sid = $themeHelper->copy_stylesheet_to_theme($stylesheet, $theme['tid']);
                }

                // Insert the modified CSS
                $new_stylesheet = $stylesheet['stylesheet'];

                if ($this->bb->input['serialized'] == 1) {
                    $this->bb->input['css_bits'] = my_unserialize($this->bb->input['css_bits']);
                }

                $css_to_insert = '';
                foreach ($this->bb->input['css_bits'] as $field => $value) {
                    if (!trim($value) || !trim($field)) {
                        continue;
                    }

                    if ($field == 'extra') {
                        $css_to_insert .= $value . "\n";
                    } else {
                        $field = str_replace('_', '-', $field);
                        $css_to_insert .= "{$field}: {$value};\n";
                    }
                }

                $new_stylesheet = $themeHelper->insert_into_css($css_to_insert, $this->bb->input['selector'], $new_stylesheet);

                // Now we have the new stylesheet, save it
                $updated_stylesheet = [
                    'cachefile' => $this->db->escape_string($stylesheet['name']),
                    'stylesheet' => $this->db->escape_string($themeHelper->unfix_css_urls($new_stylesheet)),
                    'lastmodified' => TIME_NOW
                ];
                $this->db->update_query('themestylesheets', $updated_stylesheet, "sid='{$sid}'");

                // Cache the stylesheet to the file
                if (!$themeHelper->cacheStylesheet($theme['tid'], $stylesheet['name'], $new_stylesheet)) {
                    $this->db->update_query('themestylesheets', ['cachefile' => "css?stylesheet={$sid}"], "sid='{$sid}'", 1);
                }

                // Update the CSS file list for this theme
                $themeHelper->update_theme_stylesheet_list($theme['tid']);

                $this->plugins->runHooks('admin_style_themes_edit_stylesheet_simple_commit');

                // Log admin action
                $this->bblogger->log_admin_action(htmlspecialchars_uni($theme['name']), $stylesheet['name']);

                if (!$this->bb->input['ajax']) {
                    $this->session->flash_message($this->lang->success_stylesheet_updated, 'success');

                    if ($this->bb->input['save_close']) {
                        return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
                    } else {
                        return $response->withRedirect($this->bb->admin_url . "/style?action=edit_stylesheet&tid={$theme['tid']}&file={$stylesheet['name']}");
                    }
                } else {
                    echo '1';
                    exit;
                }
            }

            // Has the file on the file system been modified?
            if ($themeHelper->resyncStylesheet($stylesheet)) {
                // Need to refetch new stylesheet as it was modified
                $query = $this->db->simple_select('themestylesheets', 'stylesheet', "sid='{$stylesheet['sid']}'");
                $stylesheet['stylesheet'] = $this->db->fetch_field($query, 'stylesheet');
            }

            $css_array = $themeHelper->css_to_array($stylesheet['stylesheet']);
            $selector_list = $themeHelper->get_selectors_as_options($css_array, $this->bb->getInput('selector', ''));

            // Do we not have any selectors? Send em to the full edit page
            if (!$selector_list) {
                $this->session->flash_message($this->lang->error_cannot_parse, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style?action=edit_stylesheet&tid=' . $theme['tid'] . '&file=' . htmlspecialchars_uni($stylesheet['name']) . '&mode=advanced');
            }

            // Fetch list of all of the stylesheets for this theme
            $stylesheets = $themeHelper->fetch_theme_stylesheets($theme);
            $this_stylesheet = $stylesheets[$stylesheet['name']];
            unset($stylesheets);

            $this->page->extra_header .= "
	<script type=\"text/javascript\">
	var my_post_key = '" . $this->bb->post_code . "';
	</script>";

            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);
            $this->page->add_breadcrumb_item("{$this->lang->editing} " . htmlspecialchars_uni($stylesheet['name']), $this->bb->admin_url . "/style?action=edit_stylesheet&amp;tid={$this->bb->input['tid']}&amp;file=" . htmlspecialchars_uni($this->bb->input['file']) . "&amp;mode=simple");

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->edit_stylesheets}");

            // If the stylesheet and theme do not match, we must be editing something that is inherited
            if ($this_stylesheet['inherited'][$stylesheet['name']]) {
                $query = $this->db->simple_select('themes', 'name', "tid='{$stylesheet['tid']}'");
                $stylesheet_parent = htmlspecialchars_uni($this->db->fetch_field($query, 'name'));

                // Show inherited warning
                if ($stylesheet['tid'] == 1) {
                    $html .= $this->page->output_alert($this->lang->sprintf($this->lang->stylesheet_inherited_default, $stylesheet_parent), 'ajax_alert');
                } else {
                    $html .= $this->page->output_alert($this->lang->sprintf($this->lang->stylesheet_inherited, $stylesheet_parent), 'ajax_alert');
                }
            }

            $sub_tabs['edit_stylesheet'] = [
                'title' => $this->lang->edit_stylesheet_simple_mode,
                'link' => $this->bb->admin_url . "/style?action=edit_stylesheet&amp;tid={$this->bb->input['tid']}&amp;file=" . htmlspecialchars_uni($this->bb->input['file']) . '&mode=simple',
                'description' => $this->lang->edit_stylesheet_simple_mode_desc
            ];

            $sub_tabs['edit_stylesheet_advanced'] = [
                'title' => $this->lang->edit_stylesheet_advanced_mode,
                'link' => $this->bb->admin_url . "/style?action=edit_stylesheet&amp;tid={$this->bb->input['tid']}&amp;file=" . htmlspecialchars_uni($this->bb->input['file']) . '&mode=advanced',
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_stylesheet');

            // Output the selection box
            $form = new Form($this->bb, $this->bb->admin_url, 'get', 'selector_form');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('module', 'style/themes') . "\n";
            $html .= $form->generate_hidden_field('action', 'edit_stylesheet') . "\n";
            $html .= $form->generate_hidden_field('tid', $this->bb->input['tid']) . "\n";
            $html .= $form->generate_hidden_field('file', htmlspecialchars_uni($this->bb->input['file'])) . "\n";

            $html .= "{$this->lang->selector}: <select id=\"selector\" name=\"selector\">\n{$selector_list}</select> <span id=\"mini_spinner\">" . $form->generate_submit_button($this->lang->go) . "</span><br /><br />\n";

            $html .= $form->end();

            // Haven't chosen a selector to edit, show the first one from the stylesheet
            if (!isset($this->bb->input['selector'])) {
                reset($css_array);
                uasort($css_array, [$themeHelper, 'css_selectors_sort_cmp']);
                $selector = key($css_array);
                $editable_selector = $css_array[$selector];
            } // Show a specific selector
            else {
                $editable_selector = $css_array[$this->bb->input['selector']];
                $selector = $this->bb->input['selector'];
            }

            // Get the properties from this item
            $properties = $themeHelper->parse_css_properties($editable_selector['values']);

            foreach (['background', 'color', 'width', 'font-family', 'font-size', 'font-style', 'font-weight', 'text-decoration'] as $_p) {
                if (!isset($properties[$_p])) {
                    $properties[$_p] = '';
                }
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=edit_stylesheet', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $this->bb->input['tid'], ['id' => 'tid']) . "\n";
            $html .= $form->generate_hidden_field('file', htmlspecialchars_uni($this->bb->input['file']), ['id' => 'file']) . "\n";
            $html .= $form->generate_hidden_field('selector', htmlspecialchars_uni($selector), ['id' => 'hidden_selector']) . "\n";

            $html .= '<div id="stylesheet">';
            $table = new Table;
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[background]', $properties['background'], ['id' => 'css_bits[background]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->background}</strong></div>", ['style' => 'width: 20%;']);
            $table->construct_cell("<strong>{$this->lang->extra_css_atribs}</strong><br /><div style=\"align: center;\">" . $form->generate_text_area('css_bits[extra]', $properties['extra'], ['id' => 'css_bits[extra]', 'style' => 'width: 98%;', 'rows' => '19']) . "</div>", ['rowspan' => 8]);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[color]', $properties['color'], ['id' => 'css_bits[color]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->color}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[width]', $properties['width'], ['id' => 'css_bits[width]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->width}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[font_family]', $properties['font-family'], ['id' => 'css_bits[font_family]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_family}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[font_size]', $properties['font-size'], ['id' => 'css_bits[font_size]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_size}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[font_style]', $properties['font-style'], ['id' => 'css_bits[font_style]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_style}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[font_weight]', $properties['font-weight'], ['id' => 'css_bits[font_weight]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->font_weight}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();
            $table->construct_cell("<div style=\"float: right;\">" . $form->generate_text_box('css_bits[text_decoration]', $properties['text-decoration'], ['id' => 'css_bits[text_decoration]', 'style' => 'width: 260px;']) . "</div><div><strong>{$this->lang->text_decoration}</strong></div>", ['style' => 'width: 40%;']);
            $table->construct_row();

            $html .= $table->output(htmlspecialchars_uni($editable_selector['class_name']) . '<span id="saved" style="color: #FEE0C6;"></span>');
            $html .= '</div>';

            $buttons[] = $form->generate_reset_button($this->lang->reset);
            $buttons[] = $form->generate_submit_button($this->lang->save_changes, ['id' => 'save', 'name' => 'save']);
            $buttons[] = $form->generate_submit_button($this->lang->save_changes_and_close, ['id' => 'save_close', 'name' => 'save_close']);

            $html .= $form->output_submit_wrapper($buttons);

            $html .= '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/themes.js?ver=1804"></script>';
            $html .= '<script type="text/javascript">

$(document).ready(function() {
//<![CDATA[
	ThemeSelector.init("' . $this->bb->admin_url . '/style?action=xmlhttp_stylesheet", "' . $this->bb->admin_url . '/style?action=edit_stylesheet", $("#selector"), $("#stylesheet"), "' . htmlspecialchars_uni($this->bb->input['file']) . '", $("#selector_form"), "' . $this->bb->input['tid'] . '");
	lang.saving = "' . $this->lang->saving . '";
});
//]]>
</script>';

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesEditStylesheet.html.twig');
        }

        if ($this->bb->input['action'] == 'edit_stylesheet' && $this->bb->input['mode'] == 'advanced') {
            // Fetch the theme we want to edit this stylesheet in
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_edit_stylesheet_advanced');

            $parent_list = $themeHelper->make_parent_theme_list($theme['tid']);
            $parent_list = implode(',', $parent_list);
            if (!$parent_list) {
                $parent_list = 1;
            }

            $query = $this->db->simple_select('themestylesheets', '*', "name='" . $this->db->escape_string($this->bb->input['file']) . "' AND tid IN ({$parent_list})", ['order_by' => 'tid', 'order_dir' => 'desc', 'limit' => 1]);
            $stylesheet = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$stylesheet['sid']) {
                $this->session->flash_message($this->lang->error_invalid_stylesheet, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            if ($this->bb->request_method == 'post') {
                $sid = $stylesheet['sid'];

                // Theme & stylesheet theme ID do not match, editing inherited - we copy to local theme
                if ($theme['tid'] != $stylesheet['tid']) {
                    $sid = $themeHelper->copy_stylesheet_to_theme($stylesheet, $theme['tid']);
                }

                // Now we have the new stylesheet, save it
                $updated_stylesheet = [
                    'cachefile' => $this->db->escape_string($stylesheet['name']),
                    'stylesheet' => $this->db->escape_string($themeHelper->unfix_css_urls($this->bb->input['stylesheet'])),
                    'lastmodified' => TIME_NOW
                ];
                $this->db->update_query('themestylesheets', $updated_stylesheet, "sid='{$sid}'");

                // Cache the stylesheet to the file
                if (!$themeHelper->cacheStylesheet($theme['tid'], $stylesheet['name'], $this->bb->input['stylesheet'])) {
                    $this->db->update_query('themestylesheets', ['cachefile' => $stylesheet['name']], "sid='{$sid}'", 1);
                    //$this->db->update_query('themestylesheets', array('cachefile' => "css?stylesheet={$sid}"), "sid='{$sid}'", 1);
                }
                // Update the CSS file list for this theme
                $themeHelper->update_theme_stylesheet_list($theme['tid']);

                $this->plugins->runHooks('admin_style_themes_edit_stylesheet_advanced_commit');

                // Log admin action
                $this->bblogger->log_admin_action(htmlspecialchars_uni($theme['name']), $stylesheet['name']);

                $this->session->flash_message($this->lang->success_stylesheet_updated, 'success');

                if (!isset($this->bb->input['save_close'])) {
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit_stylesheet&file=' . htmlspecialchars_uni($stylesheet['name']) . "&tid={$theme['tid']}&mode=advanced");
                } else {
                    return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
                }
            }
            // Fetch list of all of the stylesheets for this theme
            $stylesheets = $themeHelper->fetch_theme_stylesheets($theme);
            $this_stylesheet = $stylesheets[$stylesheet['name']];
            unset($stylesheets);

            if ($this->adm->admin_options['codepress'] != 0) {
                $this->page->extra_header .= '
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.css" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/theme/mybb.css?ver=1804" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog-mybb.css" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/css/css.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/searchcursor.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/search.js"></script>
';
            }

            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);
            $this->page->add_breadcrumb_item("{$this->lang->editing} " . htmlspecialchars_uni($stylesheet['name']), $this->bb->admin_url . "/style?action=edit_stylesheet&amp;tid={$this->bb->input['tid']}&amp;file=" . htmlspecialchars_uni($this->bb->input['file']) . "&amp;mode=advanced");

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->edit_stylesheet_advanced_mode}");

            // If the stylesheet and theme do not match, we must be editing something that is inherited
            if ($this_stylesheet['inherited'][$stylesheet['name']]) {
                $query = $this->db->simple_select('themes', 'name', "tid='{$stylesheet['tid']}'");
                $stylesheet_parent = htmlspecialchars_uni($this->db->fetch_field($query, 'name'));

                // Show inherited warning
                if ($stylesheet['tid'] == 1) {
                    $html .= $this->page->output_alert($this->lang->sprintf($this->lang->stylesheet_inherited_default, $stylesheet_parent));
                } else {
                    $html .= $this->page->output_alert($this->lang->sprintf($this->lang->stylesheet_inherited, $stylesheet_parent));
                }
            }

            $sub_tabs['edit_stylesheet'] = [
                'title' => $this->lang->edit_stylesheet_simple_mode,
                'link' => $this->bb->admin_url . "/style?action=edit_stylesheet&amp;tid={$this->bb->input['tid']}&amp;file=" . htmlspecialchars_uni($this->bb->input['file']) . "&amp;mode=simple"
            ];

            $sub_tabs['edit_stylesheet_advanced'] = [
                'title' => $this->lang->edit_stylesheet_advanced_mode,
                'link' => $this->bb->admin_url . "/style?action=edit_stylesheet&amp;tid={$this->bb->input['tid']}&amp;file=" . htmlspecialchars_uni($this->bb->input['file']) . "&amp;mode=advanced",
                'description' => $this->lang->edit_stylesheet_advanced_mode_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_stylesheet_advanced');

            // Has the file on the file system been modified?
            if ($themeHelper->resyncStylesheet($stylesheet)) {
                // Need to refetch new stylesheet as it was modified
                $query = $this->db->simple_select('themestylesheets', 'stylesheet', "sid='{$stylesheet['sid']}'");
                $stylesheet['stylesheet'] = $this->db->fetch_field($query, 'stylesheet');
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=edit_stylesheet&mode=advanced', 'post', 'edit_stylesheet');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('tid', $this->bb->input['tid']) . "\n";
            $html .= $form->generate_hidden_field('file', htmlspecialchars_uni($this->bb->input['file'])) . "\n";

            $table = new Table;
            $table->construct_cell($form->generate_text_area('stylesheet', $stylesheet['stylesheet'], ['id' => 'stylesheet', 'style' => 'width: 99%;', 'class' => '', 'rows' => '30']));
            $table->construct_row();
            $html .= $table->output($this->lang->full_stylesheet_for . ' ' . htmlspecialchars_uni($stylesheet['name']), 1, 'tfixed');

            $buttons[] = $form->generate_submit_button($this->lang->save_changes, ['id' => 'save', 'name' => 'save']);
            $buttons[] = $form->generate_submit_button($this->lang->save_changes_and_close, ['id' => 'save_close', 'name' => 'save_close']);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            if ($this->adm->admin_options['codepress'] != 0) {
                $html .= '<script type="text/javascript">
			var editor = CodeMirror.fromTextArea(document.getElementById("stylesheet"), {
				lineNumbers: true,
				lineWrapping: true,
				viewportMargin: Infinity,
				indentWithTabs: true,
				indentUnit: 4,
				mode: "text/css",
				theme: "mybb"
			});</script>';
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesEditStylesheetAdvanced.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_stylesheet') {
            // Fetch the theme we want to edit this stylesheet in
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_delete_stylesheet');

            $parent_list = $themeHelper->make_parent_theme_list($theme['tid']);
            $parent_list = implode(',', $parent_list);
            if (!$parent_list) {
                $parent_list = 1;
            }

            $query = $this->db->simple_select('themestylesheets', '*', "name='" . $this->db->escape_string($this->bb->input['file']) . "' AND tid IN ({$parent_list})", ['order_by' => 'tid', 'order_dir' => 'desc', 'limit' => 1]);
            $stylesheet = $this->db->fetch_array($query);

            // Does the theme not exist? or are we trying to delete the master?
            if (!$stylesheet['sid'] || $stylesheet['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_stylesheet, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('themestylesheets', "sid='{$stylesheet['sid']}'", 1);
                @unlink(MYBB_ROOT . "cache/themes/theme{$theme['tid']}/{$stylesheet['cachefile']}");

                $filename_min = str_replace('.css', '.min.css', $stylesheet['cachefile']);
                @unlink(MYBB_ROOT . "cache/themes/theme{$theme['tid']}/{$filename_min}");

                // Update the CSS file list for this theme
                $themeHelper->update_theme_stylesheet_list($theme['tid'], $theme, true);

                $this->plugins->runHooks('admin_style_themes_delete_stylesheet_commit');

                // Log admin action
                $this->bblogger->log_admin_action($stylesheet['sid'], $stylesheet['name'], $theme['tid'], htmlspecialchars_uni($theme['name']));

                $this->session->flash_message($this->lang->success_stylesheet_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style?action=edit&tid=' . $theme['tid']);
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/style?action=force&tid=' . $theme['tid'], $this->lang->confirm_stylesheet_deletion);
            }
        }

        if ($this->bb->input['action'] == 'add_stylesheet') {
            // Fetch the theme we want to edit this stylesheet in
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_add_stylesheet');

            // Fetch list of all of the stylesheets for this theme
            $stylesheets = $themeHelper->fetch_theme_stylesheets($theme);

            if ($this->bb->request_method == 'post') {
                // Remove special characters
                $this->bb->input['name'] = preg_replace('#([^a-z0-9-_\.]+)#i', '', $this->bb->input['name']);
                if (!$this->bb->input['name'] || $this->bb->input['name'] == '.css') {
                    $this->errors[] = $this->lang->error_missing_stylesheet_name;
                }

                // Get 30 chars only because we don't want more than that
                $this->bb->input['name'] = my_substr($this->bb->input['name'], 0, 30);
                if (get_extension($this->bb->input['name']) != 'css') {
                    // Does not end with '.css'
                    $this->errors[] = $this->lang->sprintf($this->lang->error_missing_stylesheet_extension, $this->bb->input['name']);
                }

                if (empty($this->errors)) {
                    if ($this->bb->input['add_type'] == 1) {
                        // Import from a current stylesheet
                        $parent_list = $themeHelper->make_parent_theme_list($theme['tid']);
                        $parent_list = implode(',', $parent_list);

                        $query = $this->db->simple_select('themestylesheets', 'stylesheet', "name='" . $this->db->escape_string($this->bb->input['import']) . "' AND tid IN ({$parent_list})", ['limit' => 1, 'order_by' => 'tid', 'order_dir' => 'desc']);
                        $stylesheet = $this->db->fetch_field($query, 'stylesheet');
                    } else {
                        // Custom stylesheet
                        $stylesheet = $this->bb->input['stylesheet'];
                    }

                    $attached = [];

                    if ($this->bb->input['attach'] == 1) {
                        // Our stylesheet is attached to custom pages in BB
                        foreach ($this->bb->input as $id => $value) {
                            $actions_list = '';
                            $attached_to = '';

                            if (strpos($id, 'attached_') !== false) {
                                // We have a custom attached file
                                $attached_id = (int)str_replace('attached_', '', $id);
                                $attached_to = $value;

                                if ($this->bb->input['action_' . $attached_id] == 1) {
                                    // We have custom actions for attached files
                                    $actions_list = $this->bb->input['action_list_' . $attached_id];
                                }

                                if ($actions_list) {
                                    $attached_to = $attached_to . '?' . $actions_list;
                                }

                                $attached[] = $attached_to;
                            }
                        }
                    } elseif ($this->bb->input['attach'] == 2) {
                        if (!is_array($this->bb->input['color'])) {
                            $this->errors[] = $this->lang->error_no_color_picked;
                        } else {
                            $attached = $this->bb->input['color'];
                        }
                    }

                    // Add Stylesheet
                    $insert_array = [
                        'name' => $this->db->escape_string($this->bb->input['name']),
                        'tid' => $this->bb->getInput('tid', 0),
                        'attachedto' => implode('|', array_map([$this->db, 'escape_string'], $attached)),
                        'stylesheet' => $this->db->escape_string($stylesheet),
                        'cachefile' => $this->db->escape_string(str_replace('/', '', $this->bb->input['name'])),
                        'lastmodified' => TIME_NOW
                    ];

                    $sid = $this->db->insert_query('themestylesheets', $insert_array);

                    if (!$themeHelper->cacheStylesheet($theme['tid'], str_replace('/', '', $this->bb->input['name']), $stylesheet)) {
                        $this->db->update_query('themestylesheets', ['cachefile' => "css?stylesheet={$sid}"], "sid='{$sid}'", 1);
                    }

                    // Update the CSS file list for this theme
                    $themeHelper->update_theme_stylesheet_list($theme['tid'], $theme, true);

                    $this->plugins->runHooks('admin_style_themes_add_stylesheet_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($sid, $this->bb->input['name'], $theme['tid'], htmlspecialchars_uni($theme['name']));

                    $this->session->flash_message($this->lang->success_stylesheet_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . "/style?action=edit_stylesheet&tid={$this->bb->input['tid']}&sid={$sid}&file=" . urlencode($this->bb->input['name']));
                }
            }

            if ($this->adm->admin_options['codepress'] != 0) {
                $this->page->extra_header .= '
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.css" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/theme/mybb.css?ver=1804" rel="stylesheet">
<link href="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog-mybb.css" rel="stylesheet">
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/lib/codemirror.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/mode/css/css.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/dialog/dialog.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/searchcursor.js"></script>
<script src="' . $this->bb->asset_url . '/admin/jscripts/codemirror/addon/search/search.js"></script>
';
            }

            $this->page->add_breadcrumb_item(htmlspecialchars_uni($theme['name']), $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']);
            $this->page->add_breadcrumb_item($this->lang->add_stylesheet);
            $properties = my_unserialize($theme['properties']);

            $html = $this->page->output_header("{$this->lang->themes} - {$this->lang->add_stylesheet}");

            $sub_tabs['edit_stylesheets'] = [
                'title' => $this->lang->edit_stylesheets,
                'link' => $this->bb->admin_url . '/style?action=edit&tid=' . $this->bb->input['tid']
            ];

            $sub_tabs['add_stylesheet'] = [
                'title' => $this->lang->add_stylesheet,
                'link' => $this->bb->admin_url . '/style?action=add_stylesheet&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->add_stylesheet_desc
            ];

            $sub_tabs['export_theme'] = [
                'title' => $this->lang->export_theme,
                'link' => $this->bb->admin_url . '/style?action=export&tid=' . $this->bb->input['tid']
            ];

            $sub_tabs['duplicate_theme'] = [
                'title' => $this->lang->duplicate_theme,
                'link' => $this->bb->admin_url . '/style?action=duplicate&tid=' . $this->bb->input['tid'],
                'description' => $this->lang->duplicate_theme_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_stylesheet');

            if (!empty($this->errors)) {
                $html .= $this->page->output_inline_error($this->errors);

                foreach ($this->bb->input as $name => $value) {
                    if (strpos($name, 'attached') !== false) {
                        list(, $id) = explode('_', $name);
                        $id = (int)$id;

                        $this->bb->input['applied_to'][$value] = [0 => 'global'];

                        if ($this->bb->input['action_' . $id] == 1) {
                            $this->bb->input['applied_to'][$value] = explode(',', $this->bb->input['action_list_' . $id]);
                        }
                    }
                }

                if ($this->bb->input['add_type'] == 1) {
                    $add_checked[1] = 'checked="checked"';
                    $add_checked[2] = '';
                } else {
                    $add_checked[2] = 'checked="checked"';
                    $add_checked[1] = '';
                }
            } else {
                $this->bb->input['name'] = $stylesheet['name'];
            }

            $global_checked[1] = 'checked="checked"';
            $global_checked[2] = '';
            $global_checked[3] = '';

            $form = new Form($this->bb, $this->bb->admin_url . '/style?action=add_stylesheet', 'post', 'add_stylesheet');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('tid', $this->bb->input['tid']) . "\n";

            $specific_files = '<div id="attach_1" class="attachs">';
            $count = 0;

            if ($this->bb->input['attach'] == 1 && is_array($this->bb->input['applied_to']) && (!isset($this->bb->input['applied_to']['global']) || $this->bb->input['applied_to']['global'][0] != "global")) {
                $check_actions = '';

                foreach ($this->bb->input['applied_to'] as $name => $actions) {
                    $action_list = '';
                    if ($actions[0] != 'global') {
                        $action_list = implode(',', $actions);
                    }

                    if ($actions[0] == 'global') {
                        $global_action_checked[1] = 'checked="checked"';
                        $global_action_checked[2] = '';
                    } else {
                        $global_action_checked[2] = 'checked="checked"';
                        $global_action_checked[1] = '';
                    }

                    $specific_file = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_{$count}\" value=\"0\" {$global_action_checked[1]} class=\"action_{$count}s_check\" onclick=\"checkAction('action_{$count}');\" style=\"vertical-align: middle;\" /> {$this->lang->globally}</label></dt>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"action_{$count}\" value=\"1\" {$global_action_checked[2]} class=\"action_{$count}s_check\" onclick=\"checkAction('action_{$count}');\" style=\"vertical-align: middle;\" /> {$this->lang->specific_actions}</label></dt>
			<dd style=\"margin-top: 4px;\" id=\"action_{$count}_1\" class=\"action_{$count}s\">
				<small class=\"description\">{$this->lang->specific_actions_desc}</small>
				<table cellpadding=\"4\">
					<tr>
						<td>" . $form->generate_text_box('action_list_' . $count, $action_list, ['id' => 'action_list_' . $count, 'style' => 'width: 190px;']) . "</td>
					</tr>
				</table>
			</dd>
		</dl>";

                    $form_container = new FormContainer($this);
                    $form_container->output_row('', '', "<span style=\"float: right;\"><a href=\"\" id=\"delete_img_{$count}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/cross.png\" alt=\"{$this->lang->delete}\" title=\"{$this->lang->delete}\" /></a></span>{$this->lang->file} &nbsp;" . $form->generate_text_box("attached_{$count}", $name, ['id' => "attached_{$count}", 'style' => 'width: 200px;']), "attached_{$count}");

                    $form_container->output_row('', '', $specific_file);

                    $specific_files .= "<div id=\"attached_form_{$count}\">" . $form_container->end(true) . "</div><div id=\"attach_box_{$count}\"></div>";

                    $check_actions .= "\n\tcheckAction('action_{$count}');";

                    ++$count;
                }

                if ($check_actions) {
                    $global_checked[3] = '';
                    $global_checked[2] = 'checked="checked"';
                    $global_checked[1] = '';
                }
            } elseif ($this->bb->input['attach'] == 2) {
                // Colors
                $stylesheet['colors'] = [];
                if (is_array($properties['colors'])) {
                    // We might have colors here...
                    foreach ($this->bb->input['color'] as $color) {
                        // Verify this is a color for this theme
                        if (array_key_exists($color, $properties['colors'])) {
                            $stylesheet['colors'][] = $color;
                        }
                    }

                    if (!empty($stylesheet['colors'])) {
                        $global_checked[3] = 'checked="checked"';
                        $global_checked[2] = '';
                        $global_checked[1] = '';
                    }
                }
            }

            $specific_files .= '</div>';

            // Colors
            $specific_colors = $specific_colors_option = '';

            if (is_array($properties['colors'])) {
                $specific_colors = '<br /><div id="attach_2" class="attachs">';
                $specific_colors_option = '<dt><label style="display: block;"><input type="radio" name="attach" value="2" ' . $global_checked[3] . ' class="attachs_check" onclick="checkAction(\'attach\');" style="vertical-align: middle;" /> ' . $this->lang->colors_specific_color . '</label></dt>';

                $specific_color = "
			<small>{$this->lang->colors_add_edit_desc}</small>
			<br /><br />
			" . $form->generate_select_box('color[]', $properties['colors'], $stylesheet['colors'], ['multiple' => true, 'size' => "5\" style=\"width: 200px;"]) . "
		";

                $form_container = new FormContainer($this);
                $form_container->output_row('', '', $specific_color);
                $specific_colors .= $form_container->end(true) . '</div>';
            }

            $actions = '<script type="text/javascript">
	function checkAction(id)
	{
		var checked = \'\';

		$(\'.\'+id+\'s_check\').each(function(e, val)
		{
			if($(this).prop(\'checked\') == true)
			{
				checked = $(this).val();
			}
		});
		$(\'.\'+id+\'s\').each(function(e)
		{
			$(this).hide();
		});
		if($(\'#\'+id+\'_\'+checked))
		{
			$(\'#\'+id+\'_\'+checked).show();
		}
	}
</script>
	<dl style="margin-top: 0; margin-bottom: 0; width: 40%;">
		<dt><label style="display: block;"><input type="radio" name="attach" value="0" ' . $global_checked[1] . ' class="attachs_check" onclick="checkAction(\'attach\');" style="vertical-align: middle;" /> ' . $this->lang->globally . '</label></dt><br />
		<dt><label style="display: block;"><input type="radio" name="attach" value="1" ' . $global_checked[2] . ' class="attachs_check" onclick="checkAction(\'attach\');" style="vertical-align: middle;" /> ' . $this->lang->specific_files . ' (<a id="new_specific_file">' . $this->lang->add_another . '</a>)</label></dt><br />
		' . $specific_files . '
		' . $specific_colors_option . '
		' . $specific_colors . '
	</dl>
	<script type="text/javascript">
	checkAction(\'attach\');' . $check_actions . '
	</script>';

            $html .= $form->generate_hidden_field('sid', $stylesheet['sid']) . "<br />\n";

            $form_container = new FormContainer($this, $this->lang->add_stylesheet_to . ' ' . htmlspecialchars_uni($theme['name']), 'tfixed');
            $form_container->output_row($this->lang->file_name, $this->lang->file_name_desc, $form->generate_text_box('name', $this->bb->input['name'], ['id' => 'name', 'style' => 'width: 200px;']), 'name');

            $form_container->output_row($this->lang->attached_to, $this->lang->attached_to_desc, $actions);

            $sheetnames = [];
            foreach ($stylesheets as $filename => $style) {
                $sheetnames[basename($filename)] = basename($filename);
            }

            $actions = "<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
	<dt><label style=\"display: block;\"><input type=\"radio\" name=\"add_type\" value=\"1\" {$add_checked[1]} class=\"adds_check\" onclick=\"checkAction('add');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->import_stylesheet_from}</strong></label></dt>
		<dd style=\"margin-top: 4px;\" id=\"add_1\" class=\"adds\">
			<table cellpadding=\"4\">
				<tr>
					<td>" . $form->generate_select_box('import', $sheetnames, $this->bb->input['import'], ['id' => 'import']) . "</td>
				</tr>
			</table>
		</dd>
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"add_type\" value=\"2\" {$add_checked[2]} class=\"adds_check\" onclick=\"checkAction('add');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->write_own}</strong></label></dt>
		<span id=\"add_2\" class=\"adds\"><br />" . $form->generate_text_area('stylesheet', $this->bb->input['stylesheet'], ['id' => 'stylesheet', 'style' => 'width: 99%;', 'class' => '', 'rows' => '30']) . "</span>
	</dl>";

            $form_container->output_row('', '', $actions);

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_stylesheet);

            $html .= $form->output_submit_wrapper($buttons);

            if ($this->adm->admin_options['codepress'] != 0) {
                $html .= '<script type="text/javascript">
			var editor = CodeMirror.fromTextArea(document.getElementById("stylesheet"), {
				lineNumbers: true,
				lineWrapping: true,
				viewportMargin: Infinity,
				indentWithTabs: true,
				indentUnit: 4,
				mode: "text/css",
				theme: "mybb"
			});</script>';
            }

            $html .= '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/themes.js?ver=1804"></script>';
            $html .= '<script type="text/javascript" src="' . $this->bb->asset_url . '/admin/jscripts/theme_properties.js"></script>';
            $html .= '<script type="text/javascript">
$(function() {
//<![CDATA[
	checkAction(\'add\');
	lang.saving = "' . $this->lang->saving . '";
});
//]]>
</script>';

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/ThemesAddStylesheet.html.twig');
        }

        if ($this->bb->input['action'] == 'set_default') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_set_default');

            $this->cache->update('default_theme', $theme);

            $this->db->update_query('themes', ['def' => 0]);
            $this->db->update_query('themes', ['def' => 1], "tid='" . $this->bb->getInput('tid', 0) . "'");

            $this->plugins->runHooks('admin_style_themes_set_default_commit');

            // Log admin action
            $this->bblogger->log_admin_action($theme['tid'], htmlspecialchars_uni($theme['name']));

            $this->session->flash_message($this->lang->success_theme_set_default, 'success');
            return $response->withRedirect($this->bb->admin_url . '/style');
        }

        if ($this->bb->input['action'] == 'force') {
            $query = $this->db->simple_select('themes', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $theme = $this->db->fetch_array($query);

            // Does the theme not exist?
            if (!$theme['tid'] || $theme['tid'] == 1) {
                $this->session->flash_message($this->lang->error_invalid_theme, 'error');
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            $this->plugins->runHooks('admin_style_themes_force');

            // User clicked no
//            if ($this->bb->input['no']) {
            if ($this->bb->getInput('no', false)) {
                return $response->withRedirect($this->bb->admin_url . '/style');
            }

            if ($this->bb->request_method === 'post') {
                $updated_users = [
                    'style' => $theme['tid']
                ];
                \Tracy\Debugger::dump('here');
                $this->plugins->runHooks('admin_style_themes_force_commit');

                $this->db->update_query('users', $updated_users);

                // Log admin action
                $this->bblogger->log_admin_action($theme['tid'], htmlspecialchars_uni($theme['name']));

                $this->session->flash_message($this->lang->success_theme_forced, 'success');
                return $response->withRedirect($this->bb->admin_url . '/style');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/style?action=force&tid=' . $theme['tid'], $this->lang->confirm_theme_forced);
            }
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->themes);

            $this->plugins->runHooks('admin_style_themes_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'themes');

            $this->bb->table = new Table;
            $this->bb->table->construct_header($this->lang->theme);
            $this->bb->table->construct_header($this->lang->num_users, ['class' => 'align_center', 'width' => 100]);
            $this->bb->table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            $themeHelper->build_theme_list();

            $html .= $this->bb->table->output($this->lang->themes);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Style/Themes.html.twig');
        }
    }
}
