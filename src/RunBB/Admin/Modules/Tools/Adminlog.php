<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Adminlog extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_adminlog', false, true);

        $this->page->add_breadcrumb_item($this->lang->admin_logs, $this->bb->admin_url . '/tools/adminlog');

        $sub_tabs['admin_logs'] = [
            'title' => $this->lang->admin_logs,
            'link' => $this->bb->admin_url . '/tools/adminlog',
            'description' => $this->lang->admin_logs_desc
        ];
        $sub_tabs['prune_admin_logs'] = [
            'title' => $this->lang->prune_admin_logs,
            'link' => $this->bb->admin_url . '/tools/adminlog?action=prune',
            'description' => $this->lang->prune_admin_logs_desc
        ];

        $this->plugins->runHooks('admin_tools_adminlog_begin');

        if ($this->bb->input['action'] == 'prune') {
            if (!$this->user->is_super_admin($this->user->uid)) {
                $this->session->flash_message($this->lang->cannot_perform_action_super_admin_general, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/adminlog');
            }

            $this->plugins->runHooks('admin_tools_adminlog_prune');

            if ($this->bb->request_method == 'post') {
                $is_today = false;
                $this->bb->input['older_than'] = $this->bb->getInput('older_than', 0);
                if ($this->bb->input['older_than'] <= 0) {
                    $is_today = true;
                    $this->bb->input['older_than'] = 1;
                }
                $where = 'dateline < ' . (TIME_NOW - ($this->bb->input['older_than'] * 86400));

                // Searching for entries by a particular user
                if (!empty($this->bb->input['uid'])) {
                    $where .= " AND uid='" . $this->bb->getInput('uid', 0) . "'";
                }

                // Searching for entries in a specific module
                if ($this->bb->input['filter_module']) {
                    $where .= " AND module='" . $this->db->escape_string($this->bb->input['filter_module']) . "'";
                }

                $query = $this->db->delete_query('adminlog', $where);
                $num_deleted = $this->db->affected_rows();

                $this->plugins->runHooks('admin_tools_adminlog_prune_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['older_than'], $this->bb->input['uid'], $this->bb->input['filter_module'], $num_deleted);

                $success = $this->lang->success_pruned_admin_logs;
                if ($is_today == true && $num_deleted > 0) {
                    $success .= ' ' . $this->lang->note_logs_locked;
                } elseif ($is_today == true && $num_deleted == 0) {
                    $this->session->flash_message($this->lang->note_logs_locked, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/adminlog');
                }
                $this->session->flash_message($success, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/adminlog');
            }
            $this->page->add_breadcrumb_item($this->lang->prune_admin_logs, $this->bb->admin_url . '/tools/adminlog?action=prune');
            $html = $this->page->output_header($this->lang->prune_admin_logs);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'prune_admin_logs');

            // Fetch filter options
            $sortbysel[$this->bb->input['sortby']] = 'selected="selected"';
            $ordersel[$this->bb->input['order']] = 'selected="selected"';

            $user_options[''] = $this->lang->all_administrators;
            $user_options['0'] = '----------';

            $query = $this->db->query('
		SELECT DISTINCT l.uid, u.username
		FROM ' . TABLE_PREFIX . 'adminlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (l.uid=u.uid)
		ORDER BY u.username ASC
	');
            while ($user = $this->db->fetch_array($query)) {
                $user_options[$user['uid']] = $user['username'];
            }

            $module_options = [];
            $module_options[''] = $this->lang->all_modules;
            $module_options['0'] = '----------';
            $query = $this->db->query('
		SELECT DISTINCT l.module
		FROM ' . TABLE_PREFIX . 'adminlog l
		ORDER BY l.module ASC
	');
            while ($module = $this->db->fetch_array($query)) {
                $module_options[$module['module']] = str_replace(' ', ' -&gt; ', ucwords(str_replace('/', ' ', $module['module'])));
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/adminlog?action=prune', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->prune_administrator_logs);
            $form_container->output_row($this->lang->module, '', $form->generate_select_box('filter_module', $module_options, $this->bb->input['filter_module'], ['id' => 'filter_module']), 'filter_module');
            $form_container->output_row($this->lang->administrator, '', $form->generate_select_box('uid', $user_options, $this->bb->input['uid'], ['id' => 'uid']), 'uid');
            if (!$this->bb->input['older_than']) {
                $this->bb->input['older_than'] = '30';
            }
            $form_container->output_row($this->lang->date_range, '', $this->lang->older_than . $form->generate_numeric_field('older_than', $this->bb->input['older_than'], ['id' => 'older_than', 'style' => 'width: 50px', 'min' => 0]) . " {$this->lang->days}", 'older_than');
            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->prune_administrator_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/AdminLogPrune.html.twig');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->admin_logs);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'admin_logs');

            $perpage = $this->bb->getInput('perpage', 0);
            if (!$perpage) {
                if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                    $this->bb->settings['threadsperpage'] = 20;
                }

                $perpage = $this->bb->settings['threadsperpage'];
            }

            $where = '';

            $this->plugins->runHooks('admin_tools_adminlog_start');

            // Searching for entries by a particular user
            if (!empty($this->bb->input['uid'])) {
                $where .= " AND l.uid='" . $this->bb->getInput('uid', 0) . "'";
            }

            // Searching for entries in a specific module
            if (!empty($this->bb->input['filter_module'])) {
                $where .= " AND module='" . $this->db->escape_string($this->bb->input['filter_module']) . "'";
            }

            // Order?
            $s_by = $this->bb->getInput('sortby', '');
            switch ($s_by) {
                case 'username':
                    $sortby = 'u.username';
                    break;
                default:
                    $sortby = 'l.dateline';
            }
            $order = $this->bb->getInput('order', '');
            if ($order != 'asc') {
                $order = 'desc';
            }

            $query = $this->db->query('
		SELECT COUNT(l.dateline) AS count
		FROM ' . TABLE_PREFIX . 'adminlog l
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=l.uid)
		WHERE 1=1 {$where}
	");
            $rescount = $this->db->fetch_field($query, 'count');

            // Figure out if we need to display multiple pages.
            if (!isset($this->bb->input['page']) || $this->bb->input['page'] != 'last') {
                $pagecnt = $this->bb->getInput('page', 0);
            }

            $postcount = (int)$rescount;
            $pages = $postcount / $perpage;
            $pages = ceil($pages);

            if (isset($this->bb->input['page']) && $this->bb->input['page'] == 'last') {
                $pagecnt = $pages;
            }

            if ($pagecnt > $pages) {
                $pagecnt = 1;
            }

            if ($pagecnt) {
                $start = ($pagecnt - 1) * $perpage;
            } else {
                $start = 0;
                $pagecnt = 1;
            }

            $table = new Table;
            $table->construct_header($this->lang->username, ['width' => '10%']);
            $table->construct_header($this->lang->date, ['class' => 'align_center', 'width' => '15%']);
            $table->construct_header($this->lang->information, ['class' => 'align_center', 'width' => '65%']);
            $table->construct_header($this->lang->ipaddress, ['class' => 'align_center', 'width' => '10%']);

            $query = $this->db->query('
		SELECT l.*, u.username, u.usergroup, u.displaygroup
		FROM ' . TABLE_PREFIX . 'adminlog l
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=l.uid)
		WHERE 1=1 {$where}
		ORDER BY {$sortby} {$order}
		LIMIT {$start}, {$perpage}
	");
            while ($logitem = $this->db->fetch_array($query)) {
                $information = '';
                $trow = alt_trow();
                $username = $this->user->format_name($logitem['username'], $logitem['usergroup'], $logitem['displaygroup']);

                $logitem['data'] = my_unserialize($logitem['data']);
                $logitem['profilelink'] = $this->user->build_profile_link($username, $logitem['uid'], '_blank');
                $logitem['dateline'] = $this->time->formatDate('relative', $logitem['dateline']);

                // Get detailed information from meta
                $information = $this->get_admin_log_action($logitem);

                $table->construct_cell($logitem['profilelink']);
                $table->construct_cell($logitem['dateline'], ['class' => 'align_center']);
                $table->construct_cell($information);
                $table->construct_cell($logitem['ipaddress'], ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_adminlogs, ['colspan' => '4']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->admin_logs);

            // Do we need to construct the pagination?
            if ($rescount > $perpage) {
                $html .= $this->adm->draw_admin_pagination(
                    $pagecnt,
                    $perpage,
                    $rescount,
                    $this->bb->admin_url . "/tools/adminlog?perpage=$perpage&amp;uid=".$this->bb->getInput('uid', 0).
                        "&amp;fid=".$this->bb->getInput('fid', 0)."&amp;sortby=".$this->bb->getInput('sortby', '')."
                        &amp;order={$order}&amp;filter_module=" . htmlspecialchars_uni($this->bb->getInput('filter_module', ''))
                ) . '<br />';
            }

            // Fetch filter options
            $sortbysel[$this->bb->getInput('sortby', '')] = 'selected="selected"';
            $ordersel[$this->bb->getInput('order', '')] = 'selected="selected"';

            $user_options[''] = $this->lang->all_administrators;
            $user_options['0'] = '----------';

            $query = $this->db->query('
		SELECT DISTINCT l.uid, u.username
		FROM ' . TABLE_PREFIX . 'adminlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (l.uid=u.uid)
		ORDER BY u.username ASC
	');
            while ($user = $this->db->fetch_array($query)) {
                $user_options[$user['uid']] = $user['username'];
            }

            $module_options = [];
            $module_options[''] = $this->lang->all_modules;
            $module_options['0'] = '----------';
            $query = $this->db->query('
		SELECT DISTINCT l.module
		FROM ' . TABLE_PREFIX . 'adminlog l
		ORDER BY l.module ASC
	');
            while ($module = $this->db->fetch_array($query)) {
                $module_options[$module['module']] = str_replace(' ', ' -&gt; ', ucwords(str_replace('/', ' ', $module['module'])));
            }

            $sort_by = [
                'dateline' => $this->lang->date,
                'username' => $this->lang->username
            ];

            $order_array = [
                'asc' => $this->lang->asc,
                'desc' => $this->lang->desc
            ];

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/adminlog', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->filter_administrator_logs);
            $form_container->output_row($this->lang->module, '', $form->generate_select_box('filter_module', $module_options, $this->bb->getInput('filter_module', ''), ['id' => 'filter_module']), 'filter_module');
            $form_container->output_row($this->lang->administrator, '', $form->generate_select_box('uid', $user_options, $this->bb->getInput('uid', 0), ['id' => 'uid']), 'uid');
            $form_container->output_row($this->lang->sort_by, '', $form->generate_select_box('sortby', $sort_by, $this->bb->getInput('sortby', 'desc'), ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('order', $order_array, $order, ['id' => 'order']) . " {$this->lang->order}", 'order');
            $form_container->output_row($this->lang->results_per_page, '', $form->generate_numeric_field('perpage', $perpage, ['id' => 'perpage', 'min' => 1]), 'perpage');

            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->filter_administrator_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/AdminLog.html.twig');
        }
    }

    /**
     * Returns language-friendly string describing $logitem
     * @param array $logitem The log item (one row from mybb_adminlogs)
     * @return string The description
     */
    private function get_admin_log_action($logitem)
    {
        $logitem['module'] = str_replace('/', '-', $logitem['module']);
        if ($logitem['module']) {
            list($module, $action) = explode('-', $logitem['module']);
        } else {
            $module=$action='NA';
        }
        $lang_string = 'admin_log_' . $module . '_' . $action . '_' . $logitem['action'];

        // Specific page overrides
        switch ($lang_string) {
            // == CONFIG ==
            case 'admin_log_config_banning_add': // Banning IP/Username/Email
            case 'admin_log_config_banning_delete': // Removing banned IP/username/emails
                switch ($logitem['data'][2]) {
                    case 1:
                        $lang_string = 'admin_log_config_banning_' . $logitem['action'] . '_ip';
                        break;
                    case 2:
                        $lang_string = 'admin_log_config_banning_' . $logitem['action'] . '_username';
                        break;
                    case 3:
                        $lang_string = 'admin_log_config_banning_' . $logitem['action'] . '_email';
                        break;
                }
                break;

            case 'admin_log_config_help_documents_add': // Help documents and sections
            case 'admin_log_config_help_documents_edit':
            case 'admin_log_config_help_documents_delete':
                $lang_string .= "_{$logitem['data'][2]}"; // adds _section or _document
                break;

            case 'admin_log_config_languages_edit': // Editing language variables
                $logitem['data'][1] = basename($logitem['data'][1]);
                if ($logitem['data'][2] == 1) {
                    $lang_string = 'admin_log_config_languages_edit_admin';
                }
                break;

            case 'admin_log_config_mycode_toggle_status': // Custom MyCode toggle activation
                if ($logitem['data'][2] == 1) {
                    $lang_string .= '_enabled';
                } else {
                    $lang_string .= '_disabled';
                }
                break;
            case 'admin_log_config_plugins_activate': // Installing plugin
                if ($logitem['data'][1]) {
                    $lang_string .= '_install';
                }
                break;
            case 'admin_log_config_plugins_deactivate': // Uninstalling plugin
                if ($logitem['data'][1]) {
                    $lang_string .= '_uninstall';
                }
                break;
            // == FORUM ==
            case 'admin_log_forum_attachments_delete': // Deleting attachments
                if ($logitem['data'][2]) {
                    $lang_string .= '_post';
                }
                break;
            case 'admin_log_forum_management_copy': // Forum copy
                if ($logitem['data'][4]) {
                    $lang_string .= '_with_permissions';
                }
                break;
            case 'admin_log_forum_management_': // add mod, permissions, forum orders
                // first parameter already set with action
                $lang_string .= $logitem['data'][0];
                if ($logitem['data'][0] == 'orders' && $logitem['data'][1]) {
                    $lang_string .= '_sub'; // updating forum orders in a subforum
                }
                break;
            case 'admin_log_forum_moderation_queue_': //moderation queue
                // first parameter already set with action
                $lang_string .= $logitem['data'][0];
                break;
            // == HOME ==
            case 'admin_log_home_preferences_': // 2FA
                $lang_string .= $logitem['data'][0]; // either 'enabled' or 'disabled'
                break;
            // == STYLE ==
            case 'admin_log_style_templates_delete_template': // deleting templates
                // global template set
                if ($logitem['data'][2] == -1) {
                    $lang_string .= '_global';
                }
                break;
            case 'admin_log_style_templates_edit_template': // editing templates
                // global template set
                if ($logitem['data'][2] == -1) {
                    $lang_string .= '_global';
                }
                break;
            // == TOOLS ==
            case 'admin_log_tools_adminlog_prune': // Admin Log Pruning
                if ($logitem['data'][1] && !$logitem['data'][2]) {
                    $lang_string = 'admin_log_tools_adminlog_prune_user';
                } elseif ($logitem['data'][2] && !$logitem['data'][1]) {
                    $lang_string = 'admin_log_tools_adminlog_prune_module';
                } elseif ($logitem['data'][1] && $logitem['data'][2]) {
                    $lang_string = 'admin_log_tools_adminlog_prune_user_module';
                }
                break;
            case 'admin_log_tools_modlog_prune': // Moderator Log Pruning
                if ($logitem['data'][1] && !$logitem['data'][2]) {
                    $lang_string = 'admin_log_tools_modlog_prune_user';
                } elseif ($logitem['data'][2] && !$logitem['data'][1]) {
                    $lang_string = 'admin_log_tools_modlog_prune_forum';
                } elseif ($logitem['data'][1] && $logitem['data'][2]) {
                    $lang_string = 'admin_log_tools_modlog_prune_user_forum';
                }
                break;
            case 'admin_log_tools_backupdb_backup': // Create backup
                if ($logitem['data'][0] == 'download') {
                    $lang_string = 'admin_log_tools_backupdb_backup_download';
                }
                $logitem['data'][1] = '...' . substr($logitem['data'][1], -20);
                break;
            case 'admin_log_tools_backupdb_dlbackup': // Download backup
                $logitem['data'][0] = '...' . substr($logitem['data'][0], -20);
                break;
            case 'admin_log_tools_backupdb_delete': // Delete backup
                $logitem['data'][0] = '...' . substr($logitem['data'][0], -20);
                break;
            case 'admin_log_tools_optimizedb_': // Optimize DB
                $logitem['data'][0] = @implode(', ', my_unserialize($logitem['data'][0]));
                break;
            case 'admin_log_tools_recount_rebuild_': // Recount and rebuild
                $detail_lang_string = $lang_string . $logitem['data'][0];
                if (isset($this->lang->$detail_lang_string)) {
                    $lang_string = $detail_lang_string;
                }
                break;
            // == USERS ==
            case 'admin_log_user_admin_permissions_edit': // editing default/group/user admin permissions
                if ($logitem['data'][0] > 0) {
                    // User
                    $lang_string .= '_user';
                } elseif ($logitem['data'][0] < 0) {
                    // Group
                    $logitem['data'][0] = abs($logitem['data'][0]);
                    $lang_string .= '_group';
                }
                break;
            case 'admin_log_user_admin_permissions_delete': // deleting group/user admin permissions
                if ($logitem['data'][0] > 0) {
                    // User
                    $lang_string .= '_user';
                } elseif ($logitem['data'][0] < 0) {
                    // Group
                    $logitem['data'][0] = abs($logitem['data'][0]);
                    $lang_string .= '_group';
                }
                break;
            case 'admin_log_user_banning_': // banning
                if ($logitem['data'][2] == 0) {
                    $lang_string = 'admin_log_user_banning_add_permanent';
                } else {
                    $logitem['data'][2] = $this->time->formatDate($this->bb->settings['dateformat'], $logitem['data'][2]);
                    $lang_string = 'admin_log_user_banning_add_temporary';
                }
                break;
            case 'admin_log_user_groups_join_requests':
                if ($logitem['data'][0] == 'approve') {
                    $lang_string = 'admin_log_user_groups_join_requests_approve';
                } else {
                    $lang_string = 'admin_log_user_groups_join_requests_deny';
                }
                break;
            case 'admin_log_user_users_inline_banned':
                if ($logitem['data'][1] == 0) {
                    $lang_string = 'admin_log_user_users_inline_banned_perm';
                } else {
                    $logitem['data'][1] = $this->time->formatDate($this->bb->settings['dateformat'], $logitem['data'][1]);
                    $lang_string = 'admin_log_user_users_inline_banned_temp';
                }
                break;
        }

        $plugin_array = ['logitem' => &$logitem, 'lang_string' => &$lang_string];
        $this->plugins->runHooks('admin_tools_get_admin_log_action', $plugin_array);

        if (isset($this->lang->$lang_string)) {
            array_unshift($logitem['data'], $this->lang->$lang_string); // First parameter for sprintf is the format string
            $string = call_user_func_array([$this->lang, 'sprintf'], $logitem['data']);
            if (!$string) {
                $string = $this->lang->$lang_string; // Fall back to the one in the language pack
            }
        } else {
            if (!empty($logitem['data']['type']) && $logitem['data']['type'] == 'admin_locked_out') {
                $string = $this->lang->sprintf($this->lang->admin_log_admin_locked_out, (int)$logitem['data']['uid'], htmlspecialchars_uni($logitem['data']['username']));
            } else {
                // Build a default string
                $string = $logitem['module'] . ' - ' . $logitem['action'];
                if (is_array($logitem['data']) && count($logitem['data']) > 0) {
                    $string .= '(' . implode(', ', $logitem['data']) . ')';
                }
            }
        }
        return $string;
    }
}
