<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunCMF\Core\AbstractController;

class Backupdb extends AbstractController
{
    private $dir;

    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_backupdb', false, true);

        $this->page->add_breadcrumb_item($this->lang->database_backups, $this->bb->admin_url . '/tools/backupdb');
        $this->dir = DIR . 'var/backups/';
        $this->plugins->runHooks('admin_tools_backupdb_begin');

        if ($this->bb->input['action'] == 'dlbackup') {
            if (empty($this->bb->input['file'])) {
                $this->session->flash_message($this->lang->error_file_not_specified, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
            }

            $this->plugins->runHooks('admin_tools_backupdb_dlbackup');

            $file = basename($this->bb->input['file']);
            $ext = get_extension($file);

            if (file_exists($this->dir . $file) &&
                filetype($this->dir . $file) == 'file' &&
                ($ext == 'gz' || $ext == 'sql')
            ) {
                $this->plugins->runHooks('admin_tools_backupdb_dlbackup_commit');

                // Log admin action
                $this->bblogger->log_admin_action($file);

                header('Content-disposition: attachment; filename=' . $file);
                header('Content-type: ' . $ext);
                header('Content-length: ' . filesize($this->dir . $file));

                $handle = fopen($this->dir . $file, 'rb');
                while (!feof($handle)) {
                    echo fread($handle, 8192);
                }
                fclose($handle);
            } else {
                $this->session->flash_message($this->lang->error_invalid_backup, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
            }
        }

        if ($this->bb->input['action'] == 'delete') {
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
            }

            $file = basename($this->bb->input['file']);

            if (!trim($this->bb->input['file']) || !file_exists($this->dir . $file)) {
                $this->session->flash_message($this->lang->error_backup_doesnt_exist, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
            }

            $this->plugins->runHooks('admin_tools_backupdb_delete');

            if ($this->bb->request_method == 'post') {
                $delete = @unlink($this->dir . $file);

                if ($delete) {
                    $this->plugins->runHooks('admin_tools_backupdb_delete_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($file);

                    $this->session->flash_message($this->lang->success_backup_deleted, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
                } else {
                    $this->session->flash_message($this->lang->error_backup_not_deleted, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
                }
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/tools/backupdb?action=delete&file=' . $this->bb->input['file'], $this->lang->confirm_backup_deletion);
            }
        }

        if ($this->bb->input['action'] == 'backup') {
            $this->plugins->runHooks('admin_tools_backupdb_backup');

            if ($this->bb->request_method == 'post') {
                if (!is_array($this->bb->input['tables'])) {
                    $this->session->flash_message($this->lang->error_tables_not_selected, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/backupdb?action=backup');
                }

                @set_time_limit(0);

                if ($this->bb->input['method'] == 'disk') {
                    $file = $this->dir . 'backup' . date('_Ymd_His_') . random_str(16);

                    if ($this->bb->input['filetype'] == 'gzip') {
                        if (!function_exists('gzopen')) { // check zlib-ness
                            $this->session->flash_message($this->lang->error_no_zlib, 'error');
                            return $response->withRedirect($this->bb->admin_url . '/tools/backupdb?action=backup');
                        }

                        $fp = gzopen($file . '.incomplete.sql.gz', 'w9');
                    } else {
                        $fp = fopen($file . '.incomplete.sql', 'w');
                    }
                } else {
                    $file = 'backup_' . substr(md5($this->user->uid . TIME_NOW), 0, 10) . random_str(54);
                    if ($this->bb->input['filetype'] == 'gzip') {
                        if (!function_exists('gzopen')) { // check zlib-ness
                            $this->session->flash_message($this->lang->error_no_zlib, 'error');
                            return $response->withRedirect($this->bb->admin_url . '/tools/backupdb?action=backup');
                        }

                        // Send headers for gzip file
                        header('Content-Encoding: gzip');
                        header('Content-Type: application/x-gzip');
                        header('Content-Disposition: attachment; filename="' . $file . '.sql.gz"');
                    } else {
                        // Send standard headers for .sql
                        header('Content-Type: text/x-sql');
                        header('Content-Disposition: attachment; filename="' . $file . '.sql"');
                    }
                }
                $this->db->set_table_prefix('');

                $time = date('dS F Y \a\t H:i', TIME_NOW);
                $header = "-- MyBB Database Backup\n-- Generated: {$time}\n-- -------------------------------------\n\n";
                $contents = $header;
                foreach ($this->bb->input['tables'] as $table) {
                    if (!$this->db->table_exists($this->db->escape_string($table))) {
                        continue;
                    }
                    if ($this->bb->input['analyzeoptimize'] == 1) {
                        $this->db->optimize_table($table);
                        $this->db->analyze_table($table);
                    }

                    $field_list = [];
                    $fields_array = $this->db->show_fields_from($table);
                    foreach ($fields_array as $field) {
                        $field_list[] = $field['Field'];
                    }

                    $fields = '`' . implode('`,`', $field_list) . '`';
                    if ($this->bb->input['contents'] != 'data') {
                        $structure = $this->db->show_create_table($table) . ";\n";
                        $contents .= $structure;
                        $this->clear_overflow($fp, $contents);
                    }

                    if ($this->bb->input['contents'] != 'structure') {
                        if ($this->db->engine == 'mysqli') {
                            $query = mysqli_query($this->db->read_link, "SELECT * FROM {$this->db->table_prefix}{$table}", MYSQLI_USE_RESULT);
                        } else {
                            $query = $this->db->simple_select($table);
                        }

                        while ($row = $this->db->fetch_array($query)) {
                            $insert = "INSERT INTO {$table} ($fields) VALUES (";
                            $comma = '';
                            foreach ($field_list as $field) {
                                if (!isset($row[$field]) || is_null($row[$field])) {
                                    $insert .= $comma . 'NULL';
                                } elseif ($this->db->engine == 'mysqli') {
                                    $insert .= $comma . "'" . mysqli_real_escape_string($this->db->read_link, $row[$field]) . "'";
                                } else {
                                    $insert .= $comma . "'" . $this->db->escape_string($row[$field]) . "'";
                                }
                                $comma = ',';
                            }
                            $insert .= ");\n";
                            $contents .= $insert;
                            $this->clear_overflow($fp, $contents);
                        }
                        $this->db->free_result($query);
                    }
                }

                $this->db->set_table_prefix(TABLE_PREFIX);

                if ($this->bb->input['method'] == 'disk') {
                    if ($this->bb->input['filetype'] == 'gzip') {
                        gzwrite($fp, $contents);
                        gzclose($fp);
                        rename($file . '.incomplete.sql.gz', $file . '.sql.gz');
                    } else {
                        fwrite($fp, $contents);
                        fclose($fp);
                        rename($file . '.incomplete.sql', $file . '.sql');
                    }

                    if ($this->bb->input['filetype'] == 'gzip') {
                        $ext = '.sql.gz';
                    } else {
                        $ext = '.sql';
                    }

                    $this->plugins->runHooks('admin_tools_backupdb_backup_disk_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action('disk', $file . $ext);

                    $file_from_admindir = $this->bb->admin_url . '/tools/backupdb?action=dlbackup&file=' . basename($file) . $ext;
                    $this->session->flash_message("<span><em>{$this->lang->success_backup_created}</em></span><p>{$this->lang->backup_saved_to}<br />{$file}{$ext} (<a href=\"{$file_from_admindir}\">{$this->lang->download}</a>)</p>", 'success');
                    return $response->withRedirect($this->bb->admin_url . '/tools/backupdb');
                } else {
                    $this->plugins->runHooks('admin_tools_backupdb_backup_download_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action('download');

                    if ($this->bb->input['filetype'] == 'gzip') {
                        echo gzencode($contents);
                    } else {
                        echo $contents;
                    }
                }

                exit;
            }

            $this->page->extra_header = "	<script type=\"text/javascript\">
	function changeSelection(action, prefix)
	{
		var select_box = document.getElementById('table_select');

		for(var i = 0; i < select_box.length; i++)
		{
			if(action == 'select')
			{
				select_box[i].selected = true;
			}
			else if(action == 'deselect')
			{
				select_box[i].selected = false;
			}
			else if(action == 'forum' && prefix != 0)
			{
				select_box[i].selected = false;
				var row = select_box[i].value;
				var subString = row.substring(prefix.length, 0);
				if(subString == prefix)
				{
					select_box[i].selected = true;
				}
			}
		}
	}
	</script>\n";

            $this->page->add_breadcrumb_item($this->lang->new_database_backup);
            $html = $this->page->output_header($this->lang->new_database_backup);

            $sub_tabs['database_backup'] = [
                'title' => $this->lang->database_backups,
                'link' => $this->bb->admin_url . '/tools/backupdb'
            ];

            $sub_tabs['new_backup'] = [
                'title' => $this->lang->new_backup,
                'link' => $this->bb->admin_url . '/tools/backupdb?action=backup',
                'description' => $this->lang->new_backup_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'new_backup');

            // Check if file is writable, before allowing submission
            if (!is_writable($this->dir)) {
                $this->lang->update_button = '';
                echo $this->page->output_alert($this->lang->alert_not_writable);
                $cannot_write = true;
            }

            $table = new Table;
            $table->construct_header($this->lang->table_selection);
            $table->construct_header($this->lang->backup_options);

            $table_selects = [];
            $table_list = $this->db->list_tables($this->bb->config['database']['database']);
            foreach ($table_list as $id => $table_name) {
                $table_selects[$table_name] = $table_name;
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/backupdb?action=backup', 'post', 'table_selection', 0, 'table_selection');
            $html .= $form->getForm();

            $table->construct_cell("{$this->lang->table_select_desc}\n<br /><br />\n<a href=\"javascript:changeSelection('select', 0);\">{$this->lang->select_all}</a><br />\n<a href=\"javascript:changeSelection('deselect', 0);\">{$this->lang->deselect_all}</a><br />\n<a href=\"javascript:changeSelection('forum', '" . TABLE_PREFIX . "');\">{$this->lang->select_forum_tables}</a>\n<br /><br />\n<div class=\"form_row\">" . $form->generate_select_box("tables[]", $table_selects, false, ['multiple' => true, 'id' => 'table_select', 'size' => 20]) . "</div>", ['rowspan' => 5, 'width' => '50%', 'style' => 'border-bottom: 0px']);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->file_type}</strong><br />\n{$this->lang->file_type_desc}<br />\n<div class=\"form_row\">" . $form->generate_radio_button("filetype", "gzip", $this->lang->gzip_compressed, ['checked' => 1]) . "<br />\n" . $form->generate_radio_button("filetype", "plain", $this->lang->plain_text) . "</div>", ['width' => '50%']);
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->save_method}</strong><br />\n{$this->lang->save_method_desc}<br /><div class=\"form_row\">" . $form->generate_radio_button("method", "disk", $this->lang->backup_directory) . "<br />\n" . $form->generate_radio_button("method", "download", $this->lang->download, ['checked' => 1]) . "</div>", ['width' => '50%']);
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->backup_contents}</strong><br />\n{$this->lang->backup_contents_desc}<br /><div class=\"form_row\">" . $form->generate_radio_button("contents", "both", $this->lang->structure_and_data, ['checked' => 1]) . "<br />\n" . $form->generate_radio_button("contents", "structure", $this->lang->structure_only) . "<br />\n" . $form->generate_radio_button("contents", "data", $this->lang->data_only) . "</div>", ['width' => '50%']);
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->analyze_and_optimize}</strong><br />\n{$this->lang->analyze_and_optimize_desc}<br /><div class=\"form_row\">" . $form->generate_yes_no_radio("analyzeoptimize") . "</div>", ['width' => '50%']);
            $table->construct_row();

            $html .= $table->output($this->lang->new_database_backup);

            $buttons[] = $form->generate_submit_button($this->lang->perform_backup);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/BackupDBBackup.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->page->add_breadcrumb_item($this->lang->backups);
            $html = $this->page->output_header($this->lang->database_backups);

            $sub_tabs['database_backup'] = [
                'title' => $this->lang->database_backups,
                'link' => $this->bb->admin_url . '/tools/backupdb',
                'description' => $this->lang->database_backups_desc
            ];

            $sub_tabs['new_backup'] = [
                'title' => $this->lang->new_backup,
                'link' => $this->bb->admin_url . '/tools/backupdb?action=backup',
            ];

            $this->plugins->runHooks('admin_tools_backupdb_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'database_backup');

            $backups = [];
            $handle = opendir($this->dir);

            if ($handle !== false) {
                while (($file = readdir($handle)) !== false) {
                    if (filetype($this->dir . $file) == 'file') {
                        $ext = get_extension($file);
                        if ($ext == 'gz' || $ext == 'sql') {
                            $backups[@filemtime($this->dir . $file)] = [
                                'file' => $file,
                                'time' => @filemtime($this->dir . $file),
                                'type' => $ext
                            ];
                        }
                    }
                }
                closedir($handle);
            }

            $count = count($backups);
            krsort($backups);

            $table = new Table;
            $table->construct_header($this->lang->backup_filename);
            $table->construct_header($this->lang->file_size, ['class' => 'align_center']);
            $table->construct_header($this->lang->creation_date);
            $table->construct_header($this->lang->controls, ['class' => 'align_center']);

            foreach ($backups as $backup) {
                $time = '-';
                if ($backup['time']) {
                    $time = $this->time->formatDate('relative', $backup['time']);
                }

                $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/backupdb?action=dlbackup&amp;file={$backup['file']}\">{$backup['file']}</a>");
                $table->construct_cell($this->parser->friendlySize(filesize($this->dir . $backup['file'])), ["class" => "align_center"]);
                $table->construct_cell($time);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/backupdb?action=backup&amp;action=delete&amp;file={$backup['file']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_backup_deletion}')\">{$this->lang->delete}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($count == 0) {
                $table->construct_cell($this->lang->no_backups, ['colspan' => 4]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->existing_database_backups);
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/BackupDB.html.twig');
        }
    }

    /**
     * Allows us to refresh cache to prevent over flowing
     *
     * @param resource $fp
     * @param string $contents
     */
    private function clear_overflow($fp, &$contents)
    {
        if ($this->bb->input['method'] == 'disk') {
            if ($this->bb->input['filetype'] == 'gzip') {
                gzwrite($fp, $contents);
            } else {
                fwrite($fp, $contents);
            }
        } else {
            if ($this->bb->input['filetype'] == 'gzip') {
                echo gzencode($contents);
            } else {
                echo $contents;
            }
        }

        $contents = '';
    }
}
