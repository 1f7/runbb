<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunCMF\Core\AbstractController;

class Cache extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_cache', false, true);

        $this->page->add_breadcrumb_item($this->lang->cache_manager, $this->bb->admin_url . '/tools/cache');

        $this->plugins->runHooks('admin_tools_cache_begin');

        if ($this->bb->input['action'] == 'view') {
            if (!trim($this->bb->input['title'])) {
                $this->session->flash_message($this->lang->error_no_cache_specified, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            }

            $this->plugins->runHooks('admin_tools_cache_view');

            // Rebuilds forum settings
            if ($this->bb->input['title'] == 'settings') {
                $this->cachedsettings = (array)$this->settings;
                if (isset($this->cachedsettings['internal'])) {
                    unset($this->cachedsettings['internal']);
                }

                $this->cacheitem = [
                    'title' => 'settings',
                    'cache' => my_serialize($this->cachedsettings)
                ];
            } else {
                $query = $this->db->simple_select('datacache', '*', "title = '" . $this->db->escape_string($this->bb->input['title']) . "'");
                $this->cacheitem = $this->db->fetch_array($query);
            }

            if (!$this->cacheitem) {
                $this->session->flash_message($this->lang->error_incorrect_cache, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            }

            $this->cachecontents = unserialize($this->cacheitem['cache']);
            if (empty($this->cachecontents)) {
                $this->cachecontents = $this->lang->error_empty_cache;
            }
            ob_start();
            print_r($this->cachecontents);
            $this->cachecontents = htmlspecialchars_uni(ob_get_contents());
            ob_end_clean();

            $this->page->add_breadcrumb_item($this->lang->view);
            $html = $this->page->output_header($this->lang->cache_manager);

            $table = new Table;

            $table->construct_cell("<pre>\n{$this->cachecontents}\n</pre>");
            $table->construct_row();
            $html .= $table->output($this->lang->cache . " {$this->cacheitem['title']}");

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/CacheView.html.twig');
        }

        if ($this->bb->input['action'] == 'rebuild' || $this->bb->input['action'] == 'reload') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            }

            $this->plugins->runHooks('admin_tools_cache_rebuild');

            // Rebuilds forum settings
            if ($this->bb->input['title'] == 'settings') {
                (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

                $this->plugins->runHooks('admin_tools_cache_rebuild_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['title']);

                $this->session->flash_message($this->lang->success_cache_reloaded, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            }

            if (method_exists($this->cache, "update_{$this->bb->input['title']}")) {
                $func = "update_{$this->bb->input['title']}";
                $this->cache->$func();

                $this->plugins->runHooks('admin_tools_cache_rebuild_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['title']);

                $this->session->flash_message($this->lang->success_cache_rebuilt, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            } elseif (method_exists($this->cache, "reload_{$this->bb->input['title']}")) {
                $func = "reload_{$this->bb->input['title']}";
                $this->cache->$func();

                $this->plugins->runHooks('admin_tools_cache_rebuild_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['title']);

                $this->session->flash_message($this->lang->success_cache_reloaded, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            } elseif (function_exists("update_{$this->bb->input['title']}")) {
                $func = "update_{$this->bb->input['title']}";
                $func();

                $this->plugins->runHooks('admin_tools_cache_rebuild_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['title']);

                $this->session->flash_message($this->lang->success_cache_rebuilt, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            } elseif (function_exists("reload_{$this->bb->input['title']}")) {
                $func = "reload_{$this->bb->input['title']}";
                $func();

                $this->plugins->runHooks('admin_tools_cache_rebuild_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['title']);

                $this->session->flash_message($this->lang->success_cache_reloaded, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            } else {
                $this->session->flash_message($this->lang->error_cannot_rebuild, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            }
        }

        if ($this->bb->input['action'] == 'rebuild_all') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/cache');
            }

            $this->plugins->runHooks('admin_tools_cache_rebuild_all');

            $query = $this->db->simple_select('datacache');
            while ($this->cacheitem = $this->db->fetch_array($query)) {
                if (method_exists($this->cache, "update_{$this->cacheitem['title']}")) {
                    $func = "update_{$this->cacheitem['title']}";
                    $this->cache->$func();
                } elseif (method_exists($this->cache, "reload_{$this->cacheitem['title']}")) {
                    $func = "reload_{$this->cacheitem['title']}";
                    $this->cache->$func();
                } elseif (function_exists("update_{$this->cacheitem['title']}")) {
                    $func = "update_{$this->cacheitem['title']}";
                    $func();
                } elseif (function_exists("reload_{$this->cacheitem['title']}")) {
                    $func = "reload_{$this->cacheitem['title']}";
                    $func();
                }
            }

            // Rebuilds forum settings
            (new \RunBB\Helpers\Restorer($this->bb))->rebuild_settings();

            $this->plugins->runHooks('admin_tools_cache_rebuild_all_commit');

            // Log admin action
            $this->bblogger->log_admin_action();

            $this->session->flash_message($this->lang->success_cache_reloaded, 'success');
            return $response->withRedirect($this->bb->admin_url . '/tools/cache');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->cache_manager);

            $sub_tabs['cache_manager'] = [
                'title' => $this->lang->cache_manager,
                'link' => $this->bb->admin_url . '/tools/cache',
                'description' => $this->lang->cache_manager_description
            ];

            $this->plugins->runHooks('admin_tools_cache_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'cache_manager');

            $table = new Table;
            $table->construct_header($this->lang->name);
            $table->construct_header($this->lang->size, ['class' => 'align_center', 'width' => 100]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            $query = $this->db->simple_select('datacache');
            while ($this->cacheitem = $this->db->fetch_array($query)) {
                $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/tools/cache?action=view&amp;title=" . urlencode($this->cacheitem['title']) . "\">{$this->cacheitem['title']}</a></strong>");
                $table->construct_cell($this->parser->friendlySize(strlen($this->cacheitem['cache'])), ['class' => 'align_center']);

                if (method_exists($this->cache, 'update_' . $this->cacheitem['title'])) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/cache?action=rebuild&amp;title=" . urlencode($this->cacheitem['title']) . "&amp;my_post_key={$this->bb->post_code}\">" . $this->lang->rebuild_cache . "</a>", ["class" => "align_center"]);
                } elseif (method_exists($this->cache, "reload_" . $this->cacheitem['title'])) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/cache?action=reload&amp;title=" . urlencode($this->cacheitem['title']) . "&amp;my_post_key={$this->bb->post_code}\">" . $this->lang->reload_cache . "</a>", ["class" => "align_center"]);
                } elseif (function_exists("update_" . $this->cacheitem['title'])) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/cache?action=rebuild&amp;title=" . urlencode($this->cacheitem['title']) . "&amp;my_post_key={$this->bb->post_code}\">" . $this->lang->rebuild_cache . "</a>", ["class" => "align_center"]);
                } elseif (function_exists("reload_" . $this->cacheitem['title'])) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/cache?action=reload&amp;title=" . urlencode($this->cacheitem['title']) . "&amp;my_post_key={$this->bb->post_code}\">" . $this->lang->reload_cache . "</a>", ["class" => "align_center"]);
                } else {
                    $table->construct_cell('');
                }

                $table->construct_row();
            }

            // Rebuilds forum settings
            $this->cachedsettings = (array)$this->settings;
            if (isset($this->cachedsettings['internal'])) {
                unset($this->cachedsettings['internal']);
            }

            $table->construct_cell("<strong><a href=\"".$this->bb->admin_url."/tools/cache?action=view&amp;title=settings\">settings</a></strong>");
            $table->construct_cell($this->parser->friendlySize(strlen(my_serialize($this->cachedsettings))), ["class" => "align_center"]);
            $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/cache?action=reload&amp;title=settings&amp;my_post_key={$this->bb->post_code}\">" . $this->lang->reload_cache . "</a>", ["class" => "align_center"]);

            $table->construct_row();

            $html .= $table->output("<div style=\"float: right;\"><small><a href=\"".$this->bb->admin_url."/tools/cache?action=rebuild_all&amp;my_post_key={$this->bb->post_code}\">" . $this->lang->rebuild_reload_all . "</a></small></div>" . $this->lang->cache_manager);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/Cache.html.twig');
        }
    }
}
