<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunCMF\Core\AbstractController;

class FileVerification extends AbstractController
{
    private $checksums = [];

    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_file_verification', false, true);

        @set_time_limit(0);

        $this->page->add_breadcrumb_item($this->lang->file_verification, $this->bb->admin_url . '/tools/file_verification');

        $this->plugins->runHooks('admin_tools_file_verification_begin');

        $this->plugins->runHooks('admin_tools_file_verification_check');

        if ($this->bb->request_method == 'post') {
            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/tools');
            }

            $this->page->add_breadcrumb_item($this->lang->checking, $this->bb->admin_url . '/tools/file_verification');

            $html = $this->page->output_header($this->lang->file_verification . ' - ' . $this->lang->checking);

            $file = explode("\n", fetch_remote_file("http://www.mybb.com/checksums/release_mybb_{$this->bb->version_code}.txt"));

            if (strstr($file[0], '<?xml') !== false || empty($file[0])) {
                $html .= $this->page->output_inline_error($this->lang->error_communication);
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Tools/FileVerification.html.twig');
                exit;
            }

            // Parser-up our checksum file from the MyBB Server
            foreach ($file as $line) {
                $parts = explode(' ', $line, 2);
                if (empty($parts[0]) || empty($parts[1])) {
                    continue;
                }

                if (substr($parts[1], 0, 7) == './admin') {
                    $parts[1] = "./{$this->bb->config['admin_dir']}" . substr($parts[1], 7);
                }

                if (file_exists(MYBB_ROOT . 'forums.php') && !file_exists(MYBB_ROOT . 'portal.php')) {
                    if (trim($parts[1]) == './index.php') {
                        $parts[1] = './forums.php';
                    } elseif ($parts[1] == './portal.php') {
                        $parts[1] = './index.php';
                    }
                }

                $this->checksums[trim($parts[1])][] = $parts[0];
            }

            $bad_files = $this->verify_files();

            $this->plugins->runHooks('admin_tools_file_verification_check_commit_start');

            $table = new Table;
            $table->construct_header($this->lang->file);
            $table->construct_header($this->lang->status, ['class' => 'align_center', 'width' => 100]);

            foreach ($bad_files as $file) {
                switch ($file['status']) {
                    case 'changed':
                        $file['status'] = $this->lang->changed;
                        $color = '#F22B48';
                        break;
                    case 'missing':
                        $file['status'] = $this->lang->missing;
                        $color = '#5B5658';
                        break;
                }

                $table->construct_cell("<strong><span style=\"color: {$color};\">" . htmlspecialchars_uni(substr($file['path'], 2)) . "</span></strong>");

                $table->construct_cell("<strong><span style=\"color: {$color};\">{$file['status']}</span></strong>", ['class' => 'align_center']);
                $table->construct_row();
            }

            $no_errors = false;
            if ($table->num_rows() == 0) {
                $no_errors = true;
                $table->construct_cell($this->lang->no_corrupt_files_found, ['colspan' => 3]);
                $table->construct_row();
            }

            if ($no_errors) {
                $html .= $table->output($this->lang->file_verification . ': ' . $this->lang->no_problems_found);
            } else {
                $html .= $table->output($this->lang->file_verification . ': ' . $this->lang->found_problems);
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/FileVerification.html.twig');
        }

        $this->page->output_confirm_action($this->bb->admin_url . '/tools/file_verification', $this->lang->file_verification_message, $this->lang->file_verification);
    }

    /**
     * Processes a checksum list on BB files and returns a result set
     *
     * @param string $path The base path
     * @param int $count The count of files
     * @return array The bad files
     */
    private function verify_files($path = MYBB_ROOT, $count = 0)
    {
        // We don't need to check these types of files
        $ignore = ['.', '..', '.svn', 'config.php', 'settings.php', 'Thumb.db', 'config.default.php', 'lock', 'htaccess.txt', 'logo.gif', 'logo.png'];
        $ignore_ext = ['attach'];

        if (substr($path, -1, 1) == '/') {
            $path = substr($path, 0, -1);
        }

//    if (!is_array($bad_verify_files)) {
        $bad_verify_files = [];
//    }

        // Make sure that we're in a directory and it's not a symbolic link
        if (@is_dir($path) && !@is_link($path)) {
            if ($dh = @opendir($path)) {
                // Loop through all the files/directories in this directory
                while (($file = @readdir($dh)) !== false) {
                    if (in_array($file, $ignore) || in_array(get_extension($file), $ignore_ext)) {
                        continue;
                    }

                    // Recurse through the directory tree
                    if (is_dir($path . '/' . $file)) {
                        $this->verify_files($path . '/' . $file, ($count + 1));
                        continue;
                    }

                    // We only need the last part of the path (from the BB directory to the file. i.e. inc/functions.php)
                    $file_path = '.' . str_replace(substr(MYBB_ROOT, 0, -1), '', $path) . '/' . $file;

                    // Does this file even exist in our official list? Perhaps it's a plugin
                    if (array_key_exists($file_path, $this->checksums)) {
                        $filename = $path . '/' . $file;
                        $handle = fopen($filename, 'rb');
                        $contents = '';
                        while (!feof($handle)) {
                            $contents .= fread($handle, 8192);
                        }
                        fclose($handle);

                        $md5 = md5($contents);

                        // Does it match any of our hashes (unix/windows new lines taken into consideration with the hashes)
                        if (!in_array($md5, $this->checksums[$file_path])) {
                            $bad_verify_files[] = ['status' => 'changed', 'path' => $file_path];
                        }
                    }
                    unset($this->checksums[$file_path]);
                }
                @closedir($dh);
            }
        }

        if ($count == 0) {
            if (!empty($this->checksums)) {
                foreach ($this->checksums as $file_path => $hashes) {
                    if (in_array(basename($file_path), $ignore)) {
                        continue;
                    }
                    $bad_verify_files[] = ['status' => 'missing', 'path' => $file_path];
                }
            }
        }

        // uh oh
        if ($count == 0) {
            return $bad_verify_files;
        }
    }
}
