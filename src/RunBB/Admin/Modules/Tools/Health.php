<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Health extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_system_health', false, true);

        $this->page->add_breadcrumb_item($this->lang->system_health, $this->bb->admin_url . '/tools');

        $sub_tabs['system_health'] = [
            'title' => $this->lang->system_health,
            'link' => $this->bb->admin_url . '/tools',
            'description' => $this->lang->system_health_desc
        ];

        $sub_tabs['utf8_conversion'] = [
            'title' => $this->lang->utf8_conversion,
            'link' => $this->bb->admin_url . '/tools?action=utf8_conversion',
            'description' => $this->lang->utf8_conversion_desc2
        ];

        $sub_tabs['template_check'] = [
            'title' => $this->lang->check_templates,
            'link' => $this->bb->admin_url . '/tools?action=check_templates',
            'description' => $this->lang->check_templates_desc
        ];

        $this->plugins->runHooks('admin_tools_system_health_begin');

        if ($this->bb->input['action'] == 'do_check_templates' && $this->bb->request_method == 'post') {
            $query = $this->db->simple_select('templates', '*', '', ['order_by' => 'sid, title', 'order_dir' => 'ASC']);

            if (!$this->db->num_rows($query)) {
                $this->session->flash_message($this->lang->error_invalid_input, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools');
            }

            $this->plugins->runHooks('admin_tools_system_health_template_do_check_start');

            $t_cache = [];
            while ($template = $this->db->fetch_array($query)) {
                if ($this->adm->check_template($template['template']) == true) {
                    $t_cache[$template['sid']][] = $template;
                }
            }

            if (empty($t_cache)) {
                $this->session->flash_message($this->lang->success_templates_checked, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools');
            }

            $this->plugins->runHooks('admin_tools_system_health_template_do_check');

            $this->page->add_breadcrumb_item($this->lang->check_templates);
            $html = $this->page->output_header($this->lang->check_templates);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'template_check');
            $html .= $this->page->output_inline_error([$this->lang->check_templates_info_desc]);

            $templatesets = [
                -2 => [
                    'title' => 'MyBB Master Templates'
                ]
            ];
            $query = $this->db->simple_select('templatesets', '*');
            while ($set = $this->db->fetch_array($query)) {
                $templatesets[$set['sid']] = $set;
            }

            $count = 0;
            foreach ($t_cache as $sid => $templates) {
                if (!$done_set[$sid]) {
                    $table = new Table();
                    $table->construct_header($templatesets[$sid]['title'], ['colspan' => 2]);

                    $done_set[$sid] = 1;
                    ++$count;
                }

                if ($sid == -2) {
                    // Some cheeky clown has altered the master templates!
                    $table->construct_cell($this->lang->error_master_templates_altered, ['colspan' => 2]);
                    $table->construct_row();
                }

                foreach ($templates as $template) {
                    if ($sid == -2) {
                        $table->construct_cell($template['title'], ['colspan' => 2]);
                    } else {
                        $popup = new PopupMenu("template_{$template['tid']}", $this->lang->options);
                        $popup->add_item($this->lang->full_edit, $this->bb->admin_url . '/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$sid}");

                        $table->construct_cell('<a href="'.$this->bb->admin_url.'/style/templates?action=edit_template&title=' . urlencode($template['title']) . "&amp;sid={$sid}&amp;from=diff_report\">{$template['title']}</a>", ['width' => '80%']);
                        $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                    }

                    $table->construct_row();
                }

                if ($done_set[$sid] && !$done_output[$sid]) {
                    $done_output[$sid] = 1;
                    if ($count == 1) {
                        $html .= $table->output($this->lang->check_templates);
                    } else {
                        $html .= $table->output();
                    }
                }
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/HealthCheckTemplatesResult.html.twig');
        }

        if ($this->bb->input['action'] == 'check_templates') {
            $this->page->add_breadcrumb_item($this->lang->check_templates);
            $html = $this->page->output_header($this->lang->check_templates);

            $this->plugins->runHooks('admin_tools_system_health_template_check');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'template_check');

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/tools', 'post', 'check_set');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('action', 'do_check_templates');

            $form_container = new FormContainer($this, $this->lang->check_templates);
            $form_container->output_row($this->lang->check_templates_title, '', $this->lang->check_templates_info);
            $html .= $form_container->end();

            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->proceed);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/HealthCheckTemplates.html.twig');
        }

        if ($this->bb->input['action'] == 'utf8_conversion') {
            if ($this->db->type == 'sqlite' || $this->db->type == 'pgsql') {
                $this->session->flash_message($this->lang->error_not_supported, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools');
            }

            $this->plugins->runHooks('admin_tools_system_health_utf8_conversion');

            if ($this->bb->request_method == 'post' || ($this->bb->input['do'] == 'all' && !empty($this->bb->input['table']))) {
                if (!empty($this->bb->input['mb4']) && version_compare($this->db->get_version(), '5.5.3', '<')) {
                    $this->session->flash_message($this->lang->error_utf8mb4_version, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools?action=utf8_conversion');
                }
                @set_time_limit(0);

                $old_table_prefix = $this->db->table_prefix;
                $this->db->set_table_prefix('');

                if (!$this->db->table_exists($this->db->escape_string($this->bb->input['table']))) {
                    $this->db->set_table_prefix($old_table_prefix);
                    $this->session->flash_message($this->lang->error_invalid_table, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools?action=utf8_conversion');
                }

                $this->db->set_table_prefix($old_table_prefix);

                $this->page->add_breadcrumb_item($this->lang->utf8_conversion, $this->bb->admin_url . '/tools?action=utf8_conversion');

                $html = $this->page->output_header($this->lang->system_health . ' - ' . $this->lang->utf8_conversion);

                $sub_tabs['system_health'] = [
                    'title' => $this->lang->system_health,
                    'link' => $this->bb->admin_url . '/tools',
                    'description' => $this->lang->system_health_desc
                ];

                $sub_tabs['utf8_conversion'] = [
                    'title' => $this->lang->utf8_conversion,
                    'link' => $this->bb->admin_url . '/tools?action=utf8_conversion',
                    'description' => $this->lang->utf8_conversion_desc2
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'utf8_conversion');

                $old_table_prefix = $this->db->table_prefix;
                $this->db->set_table_prefix('');

                $table = new Table;

                $table1 = $this->db->show_create_table($this->db->escape_string($this->bb->input['table']));
                preg_match('#CHARSET=([a-zA-Z0-9_]+)\s?#i', $table1, $matches);
                $charset = $matches[1];

                if (!empty($this->bb->input['mb4'])) {
                    $table->construct_cell('<strong>' . $this->lang->sprintf($this->lang->converting_to_utf8mb4, $this->bb->input['table'], $charset) . '</strong>');
                } else {
                    $table->construct_cell('<strong>' . $this->lang->sprintf($this->lang->converting_to_utf8, $this->bb->input['table'], $charset) . '</strong>');
                }
                $table->construct_row();

                $table->construct_cell($this->lang->please_wait);
                $table->construct_row();

                $html .= $table->output($this->lang->converting_table . " {$this->bb->input['table']}");

                $this->db->set_table_prefix($old_table_prefix);

                $this->page->output_footer(false);

                $old_table_prefix = $this->db->table_prefix;
                $this->db->set_table_prefix('');

                flush();

                $types = [
                    'text' => 'blob',
                    'mediumtext' => 'mediumblob',
                    'longtext' => 'longblob',
                    'char' => 'varbinary',
                    'varchar' => 'varbinary',
                    'tinytext' => 'tinyblob'
                ];

                $blob_types = ['blob', 'tinyblob', 'mediumblog', 'longblob', 'text', 'tinytext', 'mediumtext', 'longtext'];

                // Get next table in list
                $convert_to_binary = '';
                $convert_to_utf8 = '';
                $comma = '';

                if (!empty($this->bb->input['mb4'])) {
                    $character_set = 'utf8mb4';
                    $collation = 'utf8mb4_general_ci';
                } else {
                    $character_set = 'utf8';
                    $collation = 'utf8_general_ci';
                }

                // Set table default charset
                $this->db->write_query("ALTER TABLE {$this->bb->input['table']} DEFAULT CHARACTER SET {$character_set} COLLATE {$collation}");

                // Fetch any fulltext keys
                if ($this->db->supports_fulltext($this->bb->input['table'])) {
                    $table_structure = $this->db->show_create_table($this->bb->input['table']);
                    switch ($this->db->type) {
                        case 'mysql':
                        case 'mysqli':
                            preg_match_all("#FULLTEXT KEY `?([a-zA-Z0-9_]+)`? \(([a-zA-Z0-9_`,']+)\)#i", $table_structure, $matches);
                            if (is_array($matches)) {
                                foreach ($matches[0] as $key => $matched) {
                                    $this->db->write_query("ALTER TABLE {$this->bb->input['table']} DROP INDEX {$matches[1][$key]}");
                                    $fulltext_to_create[$matches[1][$key]] = $matches[2][$key];
                                }
                            }
                    }
                }

                // Find out which columns need converting and build SQL statements
                $query = $this->db->query("SHOW FULL COLUMNS FROM {$this->bb->input['table']}");
                while ($column = $this->db->fetch_array($query)) {
                    list($type) = explode('(', $column['Type']);
                    if (array_key_exists($type, $types)) {
                        // Build the actual strings for converting the columns
                        $names = "CHANGE `{$column['Field']}` `{$column['Field']}` ";

                        if (($this->db->type == 'mysql' || $this->db->type == 'mysqli') && in_array($type, $blob_types)) {
                            if ($column['Null'] == 'YES') {
                                $attributes = 'NULL';
                            } else {
                                $attributes = 'NOT NULL';
                            }
                        } else {
                            $attributes = ' DEFAULT ';
                            if ($column['Default'] == 'NULL') {
                                $attributes .= 'NULL ';
                            } else {
                                $attributes .= "'" . $this->db->escape_string($column['Default']) . "' ";

                                if ($column['Null'] == 'YES') {
                                    $attributes .= 'NULL';
                                } else {
                                    $attributes .= 'NOT NULL';
                                }
                            }
                        }

                        $convert_to_binary .= $comma . $names . preg_replace('/' . $type . '/i', $types[$type], $column['Type']) . ' ' . $attributes;
                        $convert_to_utf8 .= "{$comma}{$names}{$column['Type']} CHARACTER SET {$character_set} COLLATE {$collation} {$attributes}";

                        $comma = ',';
                    }
                }

                if (!empty($convert_to_binary)) {
                    // This converts the columns to UTF-8 while also doing the same for data
                    $this->db->write_query("ALTER TABLE {$this->bb->input['table']} {$convert_to_binary}");
                    $this->db->write_query("ALTER TABLE {$this->bb->input['table']} {$convert_to_utf8}");
                }

                // Any fulltext indexes to recreate?
                if (is_array($fulltext_to_create)) {
                    foreach ($fulltext_to_create as $name => $fields) {
                        $this->db->create_fulltext_index($this->bb->input['table'], $fields, $name);
                    }
                }

                $this->db->set_table_prefix($old_table_prefix);

                $this->plugins->runHooks('admin_tools_system_health_utf8_conversion_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['table']);

                $this->session->flash_message($this->lang->sprintf($this->lang->success_table_converted, $this->bb->input['table']), 'success');

                if ($this->bb->input['do'] == 'all') {
                    $old_table_prefix = $this->db->table_prefix;
                    $this->db->set_table_prefix('');

                    $tables = $this->db->list_tables($this->bb->config['database']['database']);
                    foreach ($tables as $key => $tablename) {
                        if (substr($tablename, 0, strlen(TABLE_PREFIX)) == TABLE_PREFIX) {
                            $table = $this->db->show_create_table($tablename);
                            preg_match("#CHARSET=([a-zA-Z0-9_]+)\s?#i", $table, $matches);
                            if (empty($this->bb->input['mb4']) &&
                                ($this->fetch_iconv_encoding($matches[1]) == 'utf-8' ||
                                    $matches[1] == 'utf8mb4') &&
                                $this->bb->input['table'] != $tablename
                            ) {
                                continue;
                            } elseif (!empty($this->bb->input['mb4']) &&
                                $this->fetch_iconv_encoding($matches[1]) != 'utf-8' &&
                                $this->bb->input['table'] != $tablename
                            ) {
                                continue;
                            }

                            $mybb_tables[$key] = $tablename;
                        }
                    }

                    asort($mybb_tables);
                    reset($mybb_tables);

                    $is_next = false;
                    $nexttable = '';

                    foreach ($mybb_tables as $key => $tablename) {
                        if ($is_next == true) {
                            $nexttable = $tablename;
                            break;
                        } elseif ($this->bb->input['table'] == $tablename) {
                            $is_next = true;
                        }
                    }

                    $this->db->set_table_prefix($old_table_prefix);

                    if ($nexttable) {
                        $nexttable = $this->db->escape_string($nexttable);
                        $mb4 = '';
                        if (!empty($this->bb->input['mb4'])) {
                            $mb4 = '&amp;mb4=1';
                        }
                        return $response->withRedirect($this->bb->admin_url . '/tools?action=utf8_conversion&do=all&table=' . $nexttable . $mb4);
                    }
                }

                return $response->withRedirect($this->bb->admin_url . '/tools?action=utf8_conversion');
            }

            if (isset($this->bb->input['table']) || $this->bb->input['do'] == 'all') {
                if (!empty($this->bb->input['mb4']) && version_compare($this->db->get_version(), '5.5.3', '<')) {
                    $this->session->flash_message($this->lang->error_utf8mb4_version, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools?action=utf8_conversion');
                }

                $old_table_prefix = $this->db->table_prefix;
                $this->db->set_table_prefix('');

                if ($this->bb->input['do'] != 'all' && !$this->db->table_exists($this->db->escape_string($this->bb->input['table']))) {
                    $this->db->set_table_prefix($old_table_prefix);
                    $this->session->flash_message($this->lang->error_invalid_table, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools?action=utf8_conversion');
                }

                if ($this->bb->input['do'] == 'all') {
                    $tables = $this->db->list_tables($this->bb->config['database']['database']);
                    foreach ($tables as $key => $tablename) {
                        if (substr($tablename, 0, strlen(TABLE_PREFIX)) == TABLE_PREFIX) {
                            $table = $this->db->show_create_table($tablename);
                            preg_match("#CHARSET=([a-zA-Z0-9_]+)\s?#i", $table, $matches);
                            if (empty($this->bb->input['mb4']) && ($this->fetch_iconv_encoding($matches[1]) == 'utf-8' || $matches[1] == 'utf8mb4')) {
                                continue;
                            } elseif (!empty($this->bb->input['mb4']) && $this->fetch_iconv_encoding($matches[1]) != 'utf-8') {
                                continue;
                            }
                            $mybb_tables[$key] = $tablename;
                        }
                    }

                    if (is_array($mybb_tables)) {
                        asort($mybb_tables);
                        reset($mybb_tables);
                        $nexttable = current($mybb_tables);
                        $table = $this->db->show_create_table($this->db->escape_string($nexttable));
                        $this->bb->input['table'] = $nexttable;
                    } else {
                        $this->db->set_table_prefix($old_table_prefix);
                        $this->session->flash_message($this->lang->success_all_tables_already_converted, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/tools');
                    }
                } else {
                    $table = $this->db->show_create_table($this->db->escape_string($this->bb->input['table']));
                }

                $this->page->add_breadcrumb_item($this->lang->utf8_conversion, $this->bb->admin_url . '/tools?action=utf8_conversion');

                $this->db->set_table_prefix($old_table_prefix);

                $html = $this->page->output_header($this->lang->system_health . ' - ' . $this->lang->utf8_conversion);

                $sub_tabs['system_health'] = [
                    'title' => $this->lang->system_health,
                    'link' => $this->bb->admin_url . '/tools',
                    'description' => $this->lang->system_health_desc
                ];

                $sub_tabs['utf8_conversion'] = [
                    'title' => $this->lang->utf8_conversion,
                    'link' => $this->bb->admin_url . '/tools?action=utf8_conversion',
                    'description' => $this->lang->utf8_conversion_desc2
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'utf8_conversion');

                $old_table_prefix = $this->db->table_prefix;
                $this->db->set_table_prefix('');

                preg_match("#CHARSET=([a-zA-Z0-9_]+)\s?#i", $table, $matches);
                $charset = $matches[1];

                $mb4 = '';
                if (!empty($this->bb->input['mb4'])) {
                    $mb4 = '&amp;mb4=1';
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/tools?action=utf8_conversion' . $mb4, 'post', 'utf8_conversion');
                $html .= $form->getForm();
                $html .= $form->generate_hidden_field('table', $this->bb->input['table']);

                if ($this->bb->input['do'] == 'all') {
                    $html .= $form->generate_hidden_field('do', 'all');
                }

                $table = new Table;

                if (!empty($this->bb->input['mb4'])) {
                    $table->construct_cell('<strong>' . $this->lang->sprintf($this->lang->convert_all_to_utf8mb4, $charset) . '</strong>');
                    $this->lang->notice_process_long_time .= "<br /><br /><strong>{$this->lang->notice_mb4_warning}</strong>";
                } else {
                    if ($this->bb->input['do'] == 'all') {
                        $table->construct_cell('<strong>' . $this->lang->sprintf($this->lang->convert_all_to_utf, $charset) . '</strong>');
                    } else {
                        $table->construct_cell('<strong>' . $this->lang->sprintf($this->lang->convert_to_utf8, $this->bb->input['table'], $charset) . '</strong>');
                    }
                }

                $table->construct_row();

                $table->construct_cell($this->lang->notice_process_long_time);
                $table->construct_row();

                if ($this->bb->input['do'] == 'all') {
                    $html .= $table->output($this->lang->convert_tables);
                    $buttons[] = $form->generate_submit_button($this->lang->convert_database_tables);
                } else {
                    $html .= $table->output($this->lang->convert_table . ": {$this->bb->input['table']}");
                    $buttons[] = $form->generate_submit_button($this->lang->convert_database_table);
                }

                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();

                $this->db->set_table_prefix($old_table_prefix);

                $this->page->output_footer();
                tdie('check this');
                exit;
            }

            if (!$this->bb->config['database']['encoding']) {
                $this->session->flash_message($this->lang->error_db_encoding_not_set, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools');
            }

            $tables = $this->db->list_tables($this->bb->config['database']['database']);

            $old_table_prefix = $this->db->table_prefix;
            $this->db->set_table_prefix('');

            $encodings = [];

            foreach ($tables as $key => $tablename) {
                if (substr($tablename, 0, strlen($old_table_prefix)) == $old_table_prefix) {
                    $table = $this->db->show_create_table($tablename);
                    preg_match("#CHARSET=([a-zA-Z0-9_]+)\s?#i", $table, $matches);
                    $encodings[$key] = $this->fetch_iconv_encoding($matches[1]);
                    $mybb_tables[$key] = $tablename;
                }
            }

            $this->db->set_table_prefix($old_table_prefix);
            $this->page->add_breadcrumb_item($this->lang->utf8_conversion, $this->bb->admin_url . '/tools?action=utf8_conversion');
            $html = $this->page->output_header($this->lang->system_health . ' - ' . $this->lang->utf8_conversion);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'utf8_conversion');

            asort($mybb_tables);

            $unique = array_unique($encodings);
            $convert_utf8 = $convert_utf8mb4 = false;
            foreach ($unique as $encoding) {
                if ($encoding == 'utf-8') {
                    $convert_utf8mb4 = true;
                } elseif ($encoding != 'utf8mb4') {
                    $convert_utf8 = true;
                }
            }

            if (count($unique) > 1) {
                $html .= $this->page->output_error("<p><em>{$this->lang->warning_multiple_encodings}</em></p>");
            }

            if (in_array('utf8mb4', $unique) && $this->bb->config['database']['encoding'] != 'utf8mb4') {
                $html .= $this->page->output_error("<p><em>{$this->lang->warning_utf8mb4_config}</em></p>");
            }

            $table = new Table;
            $table->construct_header($this->lang->table);
            $table->construct_header($this->lang->status_utf8, ['class' => 'align_center']);
            $table->construct_header($this->lang->status_utf8mb4, ['class' => 'align_center']);

            $all_utf8 = $all_utf8mb4 = '-';
            if ($convert_utf8) {
                $all_utf8 = "<strong><a href=\"".$this->bb->admin_url."/tools?action=utf8_conversion&amp;do=all\">{$this->lang->convert_all}</a></strong>";
            }
            if ($convert_utf8mb4) {
                $all_utf8mb4 = "<strong><a href=\"".$this->bb->admin_url."/tools?action=utf8_conversion&amp;do=all&amp;mb4=1\">{$this->lang->convert_all}</a></strong>";
            }
            $table->construct_cell("<strong>{$this->lang->all_tables}</strong>");
            $table->construct_cell($all_utf8, ['class' => 'align_center', 'width' => '15%']);
            $table->construct_cell($all_utf8mb4, ['class' => 'align_center', 'width' => '25%']);
            $table->construct_row();

            $db_version = $this->db->get_version();

            foreach ($mybb_tables as $key => $tablename) {
                if ($encodings[$key] != 'utf-8' && $encodings[$key] != 'utf8mb4') {
                    $status = "<a href=\"".$this->bb->admin_url."/tools?action=utf8_conversion&amp;table={$tablename}\" style=\"background: url({$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/cross.png) no-repeat; padding-left: 20px;\">{$this->lang->convert_now}</a>";
                } else {
                    $status = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/tick.png\" alt=\"{$this->lang->ok}\" />";
                }
                if (version_compare($db_version, '5.5.3', '<')) {
                    $utf8mb4 = $this->lang->not_available;
                } elseif ($encodings[$key] == 'utf8mb4') {
                    $utf8mb4 = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/tick.png\" alt=\"{$this->lang->ok}\" />";
                } elseif ($encodings[$key] == 'utf-8') {
                    $utf8mb4 = "<a href=\"".$this->bb->admin_url."/tools?action=utf8_conversion&amp;table={$tablename}&amp;mb4=1\" style=\"background: url({$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/cross.png) no-repeat; padding-left: 20px;\">{$this->lang->convert_now}</a>";
                } else {
                    $utf8mb4 = '-';
                }
                $table->construct_cell("<strong>{$tablename}</strong>");
                $table->construct_cell($status, ['class' => 'align_center', 'width' => '15%']);
                $table->construct_cell($utf8mb4, ['class' => 'align_center', 'width' => '25%']);
                $table->construct_row();
            }

            $html .= $table->output("<div>{$this->lang->utf8_conversion}</div>");

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/HealthUtf8Conversion.html.twig');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->system_health);

            $this->plugins->runHooks('admin_tools_system_health_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'system_health');

            $table = new Table;
            $table->construct_header($this->lang->totals, ['colspan' => 2]);
            $table->construct_header($this->lang->attachments, ['colspan' => 2]);

            $query = $this->db->simple_select('attachments', 'COUNT(*) AS numattachs, SUM(filesize) as spaceused, SUM(downloads*filesize) as bandwidthused', "visible='1' AND pid > '0'");
            $attachs = $this->db->fetch_array($query);

            $table->construct_cell("<strong>{$this->lang->total_database_size}</strong>", ['width' => '25%']);
            $table->construct_cell($this->parser->friendlySize($this->db->fetch_size()), ['width' => '25%']);
            $table->construct_cell("<strong>{$this->lang->attachment_space_used}</strong>", ['width' => '200']);
            $table->construct_cell($this->parser->friendlySize((int)$attachs['spaceused']), ['width' => '200']);
            $table->construct_row();

            if ($attachs['spaceused'] > 0) {
                $attach_average_size = round($attachs['spaceused'] / $attachs['numattachs']);
                $bandwidth_average_usage = round($attachs['bandwidthused']);
            } else {
                $attach_average_size = 0;
                $bandwidth_average_usage = 0;
            }

            $table->construct_cell("<strong>{$this->lang->total_cache_size}</strong>", ['width' => '25%']);
            $table->construct_cell($this->parser->friendlySize($this->cache->sizeOf()), ['width' => '25%']);
            $table->construct_cell("<strong>{$this->lang->estimated_attachment_bandwidth_usage}</strong>", ['width' => '25%']);
            $table->construct_cell($this->parser->friendlySize($bandwidth_average_usage), ['width' => '25%']);
            $table->construct_row();


            $table->construct_cell("<strong>{$this->lang->max_upload_post_size}</strong>", ['width' => '200']);
            $table->construct_cell(@ini_get('upload_max_filesize') . ' / ' . @ini_get('post_max_size'), ['width' => '200']);
            $table->construct_cell("<strong>{$this->lang->average_attachment_size}</strong>", ['width' => '25%']);
            $table->construct_cell($this->parser->friendlySize($attach_average_size), ['width' => '25%']);
            $table->construct_row();

            $html .= $table->output($this->lang->stats);

            $table->construct_header($this->lang->task);
            $table->construct_header($this->lang->run_time, ['width' => 200, 'class' => 'align_center']);

            $task_cache = $this->cache->read('tasks');
            $nextrun = $task_cache['nextrun'];

            $query = $this->db->simple_select('tasks', '*', "nextrun >= '{$nextrun}' AND enabled='1'", ['order_by' => 'nextrun', 'order_dir' => 'asc', 'limit' => 3]);
            while ($task = $this->db->fetch_array($query)) {
                $task['title'] = htmlspecialchars_uni($task['title']);
                $next_run = date($this->bb->settings['dateformat'], $task['nextrun']) . ', ' . date($this->bb->settings['timeformat'], $task['nextrun']);
                $table->construct_cell("<strong>{$task['title']}</strong>");
                $table->construct_cell($next_run, ['class' => 'align_center']);

                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_tasks, ['colspan' => 2]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->next_3_tasks);

            if (isset($this->adm->admin['permissions']['tools']['backupdb']) && $this->adm->admin['permissions']['tools']['backupdb'] == 1) {
                $backups = [];
                $dir = DIR . 'var/backups/';
                $handle = opendir($dir);
                while (($file = readdir($handle)) !== false) {
                    if (filetype($dir . $file) == 'file') {
                        $ext = get_extension($file);
                        if ($ext == 'gz' || $ext == 'sql') {
                            $backups[@filemtime($dir . $file)] = [
                                'file' => $file,
                                'time' => @filemtime($dir . $file),
                                'type' => $ext
                            ];
                        }
                    }
                }

                $count = count($backups);
                krsort($backups);

                $table = new Table;
                $table->construct_header($this->lang->name);
                $table->construct_header($this->lang->backup_time, ['width' => 200, 'class' => 'align_center']);

                $backupscnt = 0;
                foreach ($backups as $backup) {
                    ++$backupscnt;

                    if ($backupscnt == 4) {
                        break;
                    }

                    $time = '-';
                    if ($backup['time']) {
                        $time = $this->time->formatDate('relative', $backup['time']);
                    }

                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/backupdb?action=dlbackup&amp;file={$backup['file']}\">{$backup['file']}</a>");
                    $table->construct_cell($time, ['class' => 'align_center']);
                    $table->construct_row();
                }

                if ($count == 0) {
                    $table->construct_cell($this->lang->no_backups, ['colspan' => 2]);
                    $table->construct_row();
                }

                $html .= $table->output($this->lang->existing_db_backups);
            }
            $errors = 0;
            if (is_writable(DIR . 'var/cache/forum/settings.php')) {
                $message_settings = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_settings = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

            if (is_writable(MYBB_ROOT . 'inc/config.php')) {
                $message_config = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_config = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

//      $uploadspath = $this->bb->settings['uploadspath'];
//      if(my_substr($uploadspath, 0, 1) == '.')
//      {
            $uploadspath = DIR . 'web' . $this->bb->settings['uploadspath'];
//      }
            if (is_writable($uploadspath)) {
                $message_upload = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_upload = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

//      $avataruploadpath = $this->bb->settings['avataruploadpath'];
//      if(my_substr($avataruploadpath, 0, 1) == '.')
//      {
            $avataruploadpath = DIR . 'web' . $this->bb->settings['avataruploadpath'];
//      }
            if (is_writable($avataruploadpath)) {
                $message_avatar = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_avatar = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

            if (is_writable(DIR . 'var/cache/languages')) {
                $message_language = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_language = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

            if (is_writable(DIR . 'var/backups')) {
                $message_backup = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_backup = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

            if (is_writable(DIR . 'var/cache/forum')) {
                $message_cache = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_cache = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

            if (is_writable(DIR . 'web/themes')) {
                $message_themes = "<span style=\"color: green;\">{$this->lang->writable}</span>";
            } else {
                $message_themes = "<strong><span style=\"color: #C00\">{$this->lang->not_writable}</span></strong><br />{$this->lang->please_chmod_777}";
                ++$errors;
            }

            if (isset($errors)) {
                $html .= $this->page->output_error("<p><em>{$errors} {$this->lang->error_chmod}</span></strong> {$this->lang->chmod_info} <a href=\"http://docs.mybb.com/HowTo_Chmod.html\" target=\"_blank\">MyBB Docs</a>.</em></p>");
            } else {
                $html .= $this->page->output_success("<p><em>{$this->lang->success_chmod}</em></p>");
            }

            $table = new Table;
            $table->construct_header($this->lang->file);
            $table->construct_header($this->lang->location, ['colspan' => 2, 'width' => 250]);

            $table->construct_cell("<strong>{$this->lang->config_file}</strong>");
            $table->construct_cell('./inc/config.php FIXME hardcoded config');
            $table->construct_cell($message_config);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->settings_file}</strong>");
            $table->construct_cell(DIR . 'var/cache/forum/settings.php');
            $table->construct_cell($message_settings);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->file_upload_dir}</strong>");
            $table->construct_cell(DIR. 'web' . $this->bb->settings['uploadspath']);
            $table->construct_cell($message_upload);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->avatar_upload_dir}</strong>");
            $table->construct_cell(DIR . 'web' . $this->bb->settings['avataruploadpath']);
            $table->construct_cell($message_avatar);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->language_files}</strong>");
            $table->construct_cell(DIR . 'var/cache/languages');
            $table->construct_cell($message_language);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->backup_dir}</strong>");
            $table->construct_cell(DIR . 'var/backups');
            $table->construct_cell($message_backup);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->cache_dir}</strong>");
            $table->construct_cell(DIR . 'var/cache/forum');
            $table->construct_cell($message_cache);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->themes_dir}</strong>");
            $table->construct_cell(DIR .'web/themes');
            $table->construct_cell($message_themes);
            $table->construct_row();

            $this->plugins->runHooks('admin_tools_system_health_output_chmod_list');

            $html .= $table->output($this->lang->chmod_files_and_dirs);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/Health.html.twig');
        }
    }

    /**
     * Fetch the iconv/mb encoding for a particular MySQL encoding
     *
     * @param string $mysql_encoding The MySQL encoding
     * @return string The iconv/mb encoding
     */
    public function fetch_iconv_encoding($mysql_encoding)
    {
        $mysql_encoding = explode('_', $mysql_encoding);
        switch ($mysql_encoding[0]) {
            case 'utf8':
                return 'utf-8';
                break;
            case 'latin1':
                return 'iso-8859-1';
                break;
            default:
                return $mysql_encoding[0];
        }
    }
}
