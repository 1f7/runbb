<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Mailerrors extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_mailerrors', false, true);

        $this->page->add_breadcrumb_item($this->lang->system_email_log, $this->bb->admin_url . '/tools/mailerrors');

        $this->plugins->runHooks('admin_tools_mailerrors_begin');

        if ($this->bb->input['action'] == 'prune' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_tools_mailerrors_prune');

            if (isset($this->bb->input['delete_all'])) {
                $this->db->delete_query('mailerrors');
                $num_deleted = $this->db->affected_rows();

                $this->plugins->runHooks('admin_tools_mailerrors_prune_delete_all_commit');

                // Log admin action
                $this->bblogger->log_admin_action($num_deleted);

                $this->session->flash_message($this->lang->all_logs_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/mailerrors');
            } elseif (is_array($this->bb->input['log'])) {
                $log_ids = implode(',', array_map('intval', $this->bb->input['log']));
                if ($log_ids) {
                    $this->db->delete_query('mailerrors', "eid IN ({$log_ids})");
                    $num_deleted = $this->db->affected_rows();
                }
            }

            $this->plugins->runHooks('admin_tools_mailerrors_prune_commit');

            // Log admin action
            $this->bblogger->log_admin_action($num_deleted);

            $this->session->flash_message($this->lang->selected_logs_deleted, 'success');
            return $response->withRedirect($this->bb->admin_url . '/tools/mailerrors');
        }

        if ($this->bb->input['action'] == 'view') {
            $query = $this->db->simple_select('mailerrors', '*', "eid='" . $this->bb->getInput('eid', 0) . "'");
            $log = $this->db->fetch_array($query);

            if (!$log['eid']) {
                exit;
            }

            $this->plugins->runHooks('admin_tools_mailerrors_view');

            $log['toaddress'] = htmlspecialchars_uni($log['toaddress']);
            $log['fromaddress'] = htmlspecialchars_uni($log['fromaddress']);
            $log['subject'] = htmlspecialchars_uni($log['subject']);
            $log['error'] = htmlspecialchars_uni($log['error']);
            $log['smtperror'] = htmlspecialchars_uni($log['smtpcode']);
            $log['dateline'] = date($this->bb->settings['dateformat'], $log['dateline']) . ', ' . date($this->bb->settings['timeformat'], $log['dateline']);
            $log['message'] = nl2br(htmlspecialchars_uni($log['message']));

            ?>

            <div class="modal">
                <div style="overflow-y: auto; max-height: 400px;">

                    <?php
                    $table = new Table();

                    $table->construct_cell($log['error'], ['colspan' => 2]);
                    $table->construct_row();

                    if ($log['smtpcode']) {
                        $table->construct_cell($this->lang->smtp_code);
                        $table->construct_cell($log['smtpcode']);
                        $table->construct_row();
                    }

                    if ($log['smtperror']) {
                        $table->construct_cell($this->lang->smtp_server_response);
                        $table->construct_cell($log['smtperror']);
                        $table->construct_row();
                    }
                    echo $table->output($this->lang->error);

                    $table = new Table();

                    $table->construct_cell($this->lang->to . ':');
                    $table->construct_cell("<a href=\"mailto:{$log['toaddress']}\">{$log['toaddress']}</a>");
                    $table->construct_row();

                    $table->construct_cell($this->lang->from . ':');
                    $table->construct_cell("<a href=\"mailto:{$log['fromaddress']}\">{$log['fromaddress']}</a>");
                    $table->construct_row();

                    $table->construct_cell($this->lang->subject . ':');
                    $table->construct_cell($log['subject']);
                    $table->construct_row();

                    $table->construct_cell($this->lang->date . ':');
                    $table->construct_cell($log['dateline']);
                    $table->construct_row();

                    $table->construct_cell($log['message'], ['colspan' => 2]);
                    $table->construct_row();

                    echo $table->output($this->lang->email);

                    ?>
                </div>
            </div>
            <?php

            exit;
        }

        if (!$this->bb->input['action']) {
            $per_page = 20;

            if ($this->bb->getInput('page', 0) > 1) {
                $this->bb->input['page'] = $this->bb->getInput('page', 0);
                $start = ($this->bb->input['page'] * $per_page) - $per_page;
            } else {
                $this->bb->input['page'] = 1;
                $start = 0;
            }

            $additional_criteria = [];

            $this->plugins->runHooks('admin_tools_mailerrors_start');

            $html = $this->page->output_header($this->lang->system_email_log);

            $sub_tabs['mailerrors'] = [
                'title' => $this->lang->system_email_log,
                'link' => $this->bb->admin_url . '/tools/mailerrors',
                'description' => $this->lang->system_email_log_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'mailerrors');

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/mailerrors?action=prune', 'post');
            $html .= $form->getForm();

            $additional_sql_criteria = '';
            // Begin criteria filtering
            if ($this->bb->getInput('subject', '')) {
                $additional_sql_criteria .= " AND subject LIKE '%" . $this->db->escape_string_like($this->bb->input['subject']) . "%'";
                $additional_criteria[] = 'subject=' . htmlspecialchars_uni($this->bb->input['subject']);
                $form->generate_hidden_field('subject', $this->bb->input['subject']);
            }

            if ($this->bb->getInput('fromaddress', '')) {
                $additional_sql_criteria .= " AND fromaddress LIKE '%" . $this->db->escape_string_like($this->bb->input['fromaddress']) . "%'";
                $additional_criteria[] = 'fromaddress=' . urlencode($this->bb->input['fromaddress']);
                $form->generate_hidden_field('fromaddress', $this->bb->input['fromaddress']);
            }

            if ($this->bb->getInput('toaddress', '')) {
                $additional_sql_criteria .= " AND toaddress LIKE '%" . $this->db->escape_string_like($this->bb->input['toaddress']) . "%'";
                $additional_criteria[] = 'toaddress=' . urlencode($this->bb->input['toaddress']);
                $form->generate_hidden_field('toaddress', $this->bb->input['toaddress']);
            }

            if ($this->bb->getInput('error', '')) {
                $additional_sql_criteria .= " AND error LIKE '%" . $this->db->escape_string_like($this->bb->input['error']) . "%'";
                $additional_criteria[] = 'error=' . urlencode($this->bb->input['error']);
                $form->generate_hidden_field('error', $this->bb->input['error']);
            }

            if ($additional_criteria) {
                $additional_criteria = '&amp;' . implode('&amp;', $additional_criteria);
            } else {
                $additional_criteria = '';
            }

            $table = new Table;
            $table->construct_header($form->generate_check_box('allbox', 1, '', ['class' => 'checkall']));
            $table->construct_header($this->lang->subject);
            $table->construct_header($this->lang->to, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->error_message, ['class' => 'align_center', 'width' => '30%']);
            $table->construct_header($this->lang->date_sent, ['class' => 'align_center', 'width' => '20%']);

            $query = $this->db->simple_select('mailerrors', '*', "1=1 $additional_sql_criteria", ['order_by' => 'dateline', 'order_dir' => 'DESC', 'limit_start' => $start, 'limit' => $per_page]);

            while ($log = $this->db->fetch_array($query)) {
                $log['subject'] = htmlspecialchars_uni($log['subject']);
                $log['toemail'] = htmlspecialchars_uni($log['toemail']);
                $log['error'] = htmlspecialchars_uni($log['error']);
                $log['dateline'] = date($this->bb->settings['dateformat'], $log['dateline']) . ', ' . date($this->bb->settings['timeformat'], $log['dateline']);

                $table->construct_cell($form->generate_check_box("log[{$log['eid']}]", $log['eid'], ''));
                $table->construct_cell("<a href=\"javascript:MyBB.popupWindow('".$this->bb->admin_url."/tools/mailerrors?action=view&amp;eid={$log['eid']}', null, true);\">{$log['subject']}</a>");
                $find_from = "<div class=\"float_right\"><a href=\"".$this->bb->admin_url."/tools/mailerrors?toaddress={$log['toaddress']}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/find.png\" title=\"{$this->lang->find_emails_to_addr}\" alt=\"{$this->lang->fine}\" /></a></div>";
                $table->construct_cell("{$find_from}<div>{$log['toaddress']}</div>");
                $table->construct_cell($log['error']);
                $table->construct_cell($log['dateline'], ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_logs, ['colspan' => 5]);
                $table->construct_row();
                $html .= $table->output($this->lang->system_email_log);
            } else {
                $html .= $table->output($this->lang->system_email_log);
                $buttons[] = $form->generate_submit_button($this->lang->delete_selected, ['onclick' => "return confirm('{$this->lang->confirm_delete_logs}');"]);
                $buttons[] = $form->generate_submit_button($this->lang->delete_all, ['name' => 'delete_all', 'onclick' => "return confirm('{$this->lang->confirm_delete_all_logs}');"]);
                $html .= $form->output_submit_wrapper($buttons);
            }

            $html .= $form->end();

            $query = $this->db->simple_select('mailerrors l', 'COUNT(eid) AS logs', "1=1 {$additional_sql_criteria}");
            $total_rows = $this->db->fetch_field($query, 'logs');

            $html .= '<br />' . $this->adm->draw_admin_pagination($this->bb->input['page'], $per_page, $total_rows, $this->bb->admin_url . "/tools/mailerrors?page={page}{$additional_criteria}");

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/mailerrors', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->filter_system_email_log);
            $form_container->output_row($this->lang->subject_contains, '', $form->generate_text_box('subject', $this->bb->getInput('subject', ''), ['id' => 'subject']), 'subject');
            $form_container->output_row($this->lang->error_message_contains, '', $form->generate_text_box('error', $this->bb->getInput('error', ''), ['id' => 'error']), 'error');
            $form_container->output_row($this->lang->to_address_contains, '', $form->generate_text_box('toaddress', $this->bb->getInput('toaddress', ''), ['id' => 'toaddress']), 'toaddress');
            $form_container->output_row($this->lang->from_address_contains, '', $form->generate_text_box('fromaddress', $this->bb->getInput('fromaddress', ''), ['id' => 'fromaddress']), 'fromaddress');

            $html .= $form_container->end();
            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->filter_system_email_log);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/MailErrors.html.twig');
        }
    }
}