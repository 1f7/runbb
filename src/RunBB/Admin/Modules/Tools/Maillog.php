<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Maillog extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_maillogs', false, true);

        $this->page->add_breadcrumb_item($this->lang->user_email_log, $this->bb->admin_url . '/tools/maillogs');

        $this->plugins->runHooks('admin_tools_maillogs_begin');

        if ($this->bb->input['action'] == 'prune' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_tools_maillogs_prune');

            if (isset($this->bb->input['delete_all'])) {
                $this->db->delete_query('maillogs');
                $num_deleted = $this->db->affected_rows();

                $this->plugins->runHooks('admin_tools_maillogs_prune_delete_all_commit');

                // Log admin action
                $this->bblogger->log_admin_action($num_deleted);

                $this->session->flash_message($this->lang->all_logs_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/maillogs');
            } elseif (is_array($this->bb->input['log'])) {
                $log_ids = implode(',', array_map('intval', $this->bb->input['log']));
                if ($log_ids) {
                    $this->db->delete_query('maillogs', "mid IN ({$log_ids})");
                    $num_deleted = $this->db->affected_rows();
                }
            }

            $this->plugins->runHooks('admin_tools_maillogs_prune_commit');

            // Log admin action
            $this->bblogger->log_admin_action($num_deleted);

            $this->session->flash_message($this->lang->selected_logs_deleted, 'success');
            return $response->withRedirect($this->bb->admin_url . '/tools/maillogs');
        }

        if ($this->bb->input['action'] == 'view') {
            $query = $this->db->simple_select('maillogs', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $log = $this->db->fetch_array($query);

            if (!$log['mid']) {
                exit;
            }

            $this->plugins->runHooks('admin_tools_maillogs_view');

            $log['toemail'] = htmlspecialchars_uni($log['toemail']);
            $log['fromemail'] = htmlspecialchars_uni($log['fromemail']);
            $log['subject'] = htmlspecialchars_uni($log['subject']);
            $log['dateline'] = date($this->bb->settings['dateformat'], $log['dateline']) . ', ' . date($this->bb->settings['timeformat'], $log['dateline']);
            if ($this->bb->settings['mail_logging'] == 1) {
                $log['message'] = $this->lang->na;
            } else {
                $log['message'] = nl2br(htmlspecialchars_uni($log['message']));
            }

            ?>
            <div class="modal">
                <div style="overflow-y: auto; max-height: 600px;">

                    <?php
                    $table = new Table();

                    $table->construct_cell($this->lang->to . ':');
                    $table->construct_cell("<a href=\"mailto:{$log['toemail']}\">{$log['toemail']}</a>");
                    $table->construct_row();

                    $table->construct_cell($this->lang->from . ':');
                    $table->construct_cell("<a href=\"mailto:{$log['fromemail']}\">{$log['fromemail']}</a>");
                    $table->construct_row();

                    $table->construct_cell($this->lang->ip_address . ':');
                    $table->construct_cell($log['ipaddress']);
                    $table->construct_row();

                    $table->construct_cell($this->lang->subject . ':');
                    $table->construct_cell($log['subject']);
                    $table->construct_row();

                    $table->construct_cell($this->lang->date . ':');
                    $table->construct_cell($log['dateline']);
                    $table->construct_row();

                    $table->construct_cell($log['message'], ['colspan' => 2]);
                    $table->construct_row();

                    echo $table->output($this->lang->user_email_log_viewer);

                    ?>
                </div>
            </div>
            <?php
        }

        if (!$this->bb->input['action']) {
            if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                $this->bb->settings['threadsperpage'] = 20;
            }

            $per_page = $this->bb->settings['threadsperpage'];

            if (!$per_page) {
                $per_page = 20;
            }

            if (isset($this->bb->input['page']) && $this->bb->input['page'] > 1) {
                $this->bb->input['page'] = $this->bb->getInput('page', 0);
                $start = ($this->bb->input['page'] * $per_page) - $per_page;
            } else {
                $this->bb->input['page'] = 1;
                $start = 0;
            }

            $additional_criteria = [];

            $this->plugins->runHooks('admin_tools_maillogs_start');

            // Filter form was submitted - play around with the values
            if ($this->bb->request_method == 'post') {
                if ($this->bb->input['from_type'] == 'user') {
                    $this->bb->input['fromname'] = $this->bb->input['from_value'];
                } elseif ($this->bb->input['from_type'] == 'email') {
                    $this->bb->input['fromemail'] = $this->bb->input['from_value'];
                }

                if ($this->bb->input['to_type'] == 'user') {
                    $this->bb->input['toname'] = $this->bb->input['to_value'];
                } elseif ($this->bb->input['to_type'] == 'email') {
                    $this->bb->input['toemail'] = $this->bb->input['to_value'];
                }
            }

            $touid = $this->bb->getInput('touid', 0);
            $toname = $this->db->escape_string($this->bb->getInput('toname', ''));
            $toemail = $this->db->escape_string_like($this->bb->getInput('toemail', ''));

            $fromuid = $this->bb->getInput('fromuid', 0);
            $fromemail = $this->db->escape_string_like($this->bb->getInput('fromemail', ''));

            $subject = $this->db->escape_string_like($this->bb->getInput('subject', ''));

            // Begin criteria filtering
            $additional_sql_criteria = '';
            if ($this->bb->getInput('subject', '', true)) {
                $additional_sql_criteria .= " AND l.subject LIKE '%{$subject}%'";
                $additional_criteria[] = 'subject=' . urlencode($this->bb->getInput('subject', '', true));
            }

            $from_filter = $to_filter = '';
            if ($fromuid) {
                $query = $this->db->simple_select('users', 'uid, username', "uid = '{$fromuid}'");
                $user = $this->db->fetch_array($query);
                $from_filter = $user['username'];

                $additional_sql_criteria .= " AND l.fromuid = '{$fromuid}'";
                $additional_criteria[] = "fromuid={$fromuid}";
            } elseif ($this->bb->getInput('fromname', '', true)) {
                $user = $this->user->get_user_by_username($this->bb->getInput('fromname', '', true), ['fields' => 'uid, username']);
                $from_filter = $user['username'];

                if (!$user['uid']) {
                    $this->session->flash_message($this->lang->error_invalid_user, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/maillogs');
                }

                $additional_sql_criteria .= "AND l.fromuid = '{$user['uid']}'";
                $additional_criteria[] = "fromuid={$user['uid']}";
            }

            if ($this->bb->getInput('fromemail', '')) {
                $additional_sql_criteria .= " AND l.fromemail LIKE '%{$fromemail}%'";
                $additional_criteria[] = 'fromemail=' . urlencode($this->bb->getInput('fromemail', ''));
                $from_filter = $this->bb->getInput('fromemail', '');
            }

            if ($touid) {
                $query = $this->db->simple_select('users', 'uid, username', "uid = '{$touid}'");
                $user = $this->db->fetch_array($query);
                $to_filter = $user['username'];

                $additional_sql_criteria .= " AND l.touid = '{$touid}'";
                $additional_criteria[] = "touid={$touid}";
            } elseif ($toname) {
                $user = $this->user->get_user_by_username($toname, ['fields' => 'username']);
                $to_filter = $user['username'];

                if (!$user['uid']) {
                    $this->session->flash_message($this->lang->error_invalid_user, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/maillogs');
                }

                $additional_sql_criteria .= "AND l.touid='{$user['uid']}'";
                $additional_criteria[] = "touid={$user['uid']}";
            }

            if ($toemail) {
                $additional_sql_criteria .= " AND l.toemail LIKE '%{$toemail}%'";
                $additional_criteria[] = 'toemail=' . urlencode($this->bb->input['toemail']);
                $to_filter = $this->bb->input['toemail'];
            }

            if (!empty($additional_criteria)) {
                $additional_criteria = '&amp;' . implode('&amp;', $additional_criteria);
            } else {
                $additional_criteria = '';
            }

            $html = $this->page->output_header($this->lang->user_email_log);

            $sub_tabs['maillogs'] = [
                'title' => $this->lang->user_email_log,
                'link' => $this->bb->admin_url . '/tools/maillogs',
                'description' => $this->lang->user_email_log_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'maillogs');

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/maillogs?action=prune', 'post');
            $html .= $form->getForm();

            $table = new Table;
            $table->construct_header($form->generate_check_box('allbox', 1, '', ['class' => 'checkall']));
            $table->construct_header($this->lang->subject, ['colspan' => 2]);
            $table->construct_header($this->lang->from, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->to, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->date_sent, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->ip_address, ['class' => 'align_center', 'width' => '10%']);

            $query = $this->db->query('
		SELECT l.*, r.username AS to_username, f.username AS from_username, t.subject AS thread_subject
		FROM ' . TABLE_PREFIX . 'maillogs l
		LEFT JOIN ' . TABLE_PREFIX . 'users r ON (r.uid=l.touid)
		LEFT JOIN ' . TABLE_PREFIX . 'users f ON (f.uid=l.fromuid)
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=l.tid)
		WHERE 1=1 {$additional_sql_criteria}
		ORDER BY l.dateline DESC
		LIMIT {$start}, {$per_page}
	");
            while ($log = $this->db->fetch_array($query)) {
                $table->construct_cell($form->generate_check_box("log[{$log['mid']}]", $log['mid'], ''), ['width' => 1]);
                $log['subject'] = htmlspecialchars_uni($log['subject']);
                $log['dateline'] = date($this->bb->settings['dateformat'], $log['dateline']) . ', ' . date($this->bb->settings['timeformat'], $log['dateline']);

                if ($log['type'] == 2) {
                    if ($log['thread_subject']) {
                        $log['thread_subject'] = htmlspecialchars_uni($log['thread_subject']);
                        $thread_link = "<a href=\"" . get_thread_link($log['tid']) . "\">" . $log['thread_subject'] . '</a>';
                    } else {
                        $thread_link = $this->lang->deleted;
                    }
                    $table->construct_cell("<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/maillogs_thread.png\" title=\"{$this->lang->sent_using_send_thread_feature}\" alt=\"\" />", ["width" => 1]);
                    $table->construct_cell("<a href=\"javascript:BB.popupWindow('".$this->bb->admin_url."/tools/maillogs?action=view&amp;mid={$log['mid']}', null, true);\">{$log['subject']}</a><br /><small>{$this->lang->thread} {$thread_link}</small>");

                    if ($log['fromuid'] > 0) {
                        $find_from = "<div class=\"float_right\"><a href=\"".$this->bb->admin_url."/tools/maillogs?fromuid={$log['fromuid']}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/find.png\" title=\"{$this->lang->find_emails_by_user}\" alt=\"{$this->lang->find}\" /></a></div>";
                    }

                    if (!$log['from_username'] && $log['fromuid'] > 0) {
                        $table->construct_cell("{$find_from}<div>{$this->lang->deleted_user}</div>");
                    } elseif ($log['fromuid'] == 0) {
                        $log['fromemail'] = htmlspecialchars_uni($log['fromemail']);
                        $table->construct_cell("{$find_from}<div>{$log['fromemail']}</div>");
                    } else {
                        $table->construct_cell("{$find_from}<div><a href=\"" . get_profile_link($log['fromuid']) . "\">{$log['from_username']}</a></div>");
                    }

                    $log['toemail'] = htmlspecialchars_uni($log['toemail']);
                    $table->construct_cell($log['toemail']);
                } elseif ($log['type'] == 1) {
                    $table->construct_cell("<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/maillogs_user.png\" title=\"{$this->lang->email_sent_to_user}\" alt=\"\" />", ["width" => 1]);
                    $table->construct_cell("<a href=\"javascript:MyBB.popupWindow('".$this->bb->admin_url."/tools/maillogs?action=view&amp;mid={$log['mid']}', null, true);\">{$log['subject']}</a>");

                    if ($log['fromuid'] > 0) {
                        $find_from = "<div class=\"float_right\"><a href=\"".$this->bb->admin_url."/tools/maillogs?fromuid={$log['fromuid']}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/find.png\" title=\"{$this->lang->find_emails_by_user}\" alt=\"{$this->lang->find}\" /></a></div>";
                    }

                    if (!$log['from_username'] && $log['fromuid'] > 0) {
                        $table->construct_cell("{$find_from}<div>{$this->lang->deleted_user}</div>");
                    } elseif ($log['fromuid'] == 0) {
                        $log['fromemail'] = htmlspecialchars_uni($log['fromemail']);
                        $table->construct_cell("{$find_from}<div>{$log['fromemail']}</div>");
                    } else {
                        $table->construct_cell("{$find_from}<div><a href=\"" . get_profile_link($log['fromuid']) . "\">{$log['from_username']}</a></div>");
                    }

                    $find_to = "<div class=\"float_right\"><a href=\"".$this->bb->admin_url."/tools/maillogs?touid={$log['touid']}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/find.png\" title=\"{$this->lang->find_emails_to_user}\" alt=\"{$this->lang->find}\" /></a></div>";
                    if (!$log['to_username']) {
                        $table->construct_cell("{$find_to}<div>{$this->lang->deleted_user}</div>");
                    } else {
                        $table->construct_cell("{$find_to}<div><a href=\"" . get_profile_link($log['touid']) . "\">{$log['to_username']}</a></div>");
                    }
                } elseif ($log['type'] == 3) {
                    $table->construct_cell("<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/maillogs_contact.png\" title=\"{$this->lang->email_sent_using_contact_form}\" alt=\"\" />", ["width" => 1]);
                    $table->construct_cell("<a href=\"javascript:MyBB.popupWindow('".$this->bb->admin_url."/tools/maillogs?action=view&amp;mid={$log['mid']}', null, true);\">{$log['subject']}</a>");

                    if ($log['fromuid'] > 0) {
                        $find_from = "<div class=\"float_right\"><a href=\"".$this->bb->admin_url."/tools/maillogs?fromuid={$log['fromuid']}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/find.png\" title=\"{$this->lang->find_emails_by_user}\" alt=\"{$this->lang->find}\" /></a></div>";
                    }

                    if (!$log['from_username'] && $log['fromuid'] > 0) {
                        $table->construct_cell("{$find_from}<div>{$this->lang->deleted_user}</div>");
                    } elseif ($log['fromuid'] == 0) {
                        $log['fromemail'] = htmlspecialchars_uni($log['fromemail']);
                        $table->construct_cell("{$find_from}<div>{$log['fromemail']}</div>");
                    } else {
                        $table->construct_cell("{$find_from}<div><a href=\"" . get_profile_link($log['fromuid']) . "\">{$log['from_username']}</a></div>");
                    }

                    $log['toemail'] = htmlspecialchars_uni($log['toemail']);
                    $table->construct_cell($log['toemail']);
                }

                $table->construct_cell($log['dateline'], ['class' => 'align_center']);
                $table->construct_cell($log['ipaddress'], ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_logs, ['colspan' => '7']);
                $table->construct_row();
                $html .= $table->output($this->lang->user_email_log);
            } else {
                $html .= $table->output($this->lang->user_email_log);
                $buttons[] = $form->generate_submit_button($this->lang->delete_selected, ['onclick' => "return confirm('{$this->lang->confirm_delete_logs}');"]);
                $buttons[] = $form->generate_submit_button($this->lang->delete_all, ['name' => 'delete_all', 'onclick' => "return confirm('{$this->lang->confirm_delete_all_logs}');"]);
                $html .= $form->output_submit_wrapper($buttons);
            }

            $html .= $form->end();

            $query = $this->db->simple_select('maillogs l', 'COUNT(l.mid) as logs', "1=1 {$additional_sql_criteria}");
            $total_rows = $this->db->fetch_field($query, 'logs');

            $html .= '<br />' . $this->adm->draw_admin_pagination($this->bb->input['page'], $per_page, $total_rows, $this->bb->admin_url . "/tools/maillogs?page={page}{$additional_criteria}");

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/maillogs', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->filter_user_email_log);
            $user_email = [
                'user' => $this->lang->username_is,
                'email' => $this->lang->email_contains
            ];
            $form_container->output_row($this->lang->subject_contains, '', $form->generate_text_box('subject', $subject, ['id' => 'subject']), 'subject');
            $from_type = 'email';
            if ($log['from_username']) {
                $from_type = 'user';
            } elseif ($this->bb->getInput('fromemail', '')) {
                $from_type = 'email';
            }
            $form_container->output_row($this->lang->from, '', $form->generate_select_box('from_type', $user_email, $from_type) . ' ' . $form->generate_text_box('from_value', $from_filter, ['id' => 'from_value']), 'from_value');
            $to_type = 'email';
            if ($log['to_username']) {
                $to_type = 'user';
            } elseif ($this->bb->getInput('toemail', '')) {
                $to_type = 'email';
            }
            $form_container->output_row($this->lang->to, '', $form->generate_select_box('to_type', $user_email, $to_type) . ' ' . $form->generate_text_box('to_value', $to_filter, ['id' => 'to_value']), 'to_value');
            $html .= $form_container->end();
            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->filter_user_email_log);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/MailLog.html.twig');
        }
    }
}