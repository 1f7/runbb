<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use RunBB\Admin\Helpers\SidebarItem;

class Meta// extends AbstractController
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * @return bool true
     */
    public function tools_meta()
    {
        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'system_health', 'title' => $this->bb->lang->system_health, 'link' => $this->bb->admin_url . '/tools'];
        $sub_menu['20'] = ['id' => 'cache', 'title' => $this->bb->lang->cache_manager, 'link' => $this->bb->admin_url . '/tools/cache'];
        $sub_menu['30'] = ['id' => 'tasks', 'title' => $this->bb->lang->task_manager, 'link' => $this->bb->admin_url . '/tools/tasks'];
        $sub_menu['40'] = ['id' => 'recount_rebuild', 'title' => $this->bb->lang->recount_and_rebuild, 'link' => $this->bb->admin_url . '/tools/recount_rebuild'];
        $sub_menu['50'] = ['id' => 'php_info', 'title' => $this->bb->lang->view_php_info, 'link' => $this->bb->admin_url . '/tools/php_info'];
        $sub_menu['60'] = ['id' => 'backupdb', 'title' => $this->bb->lang->database_backups, 'link' => $this->bb->admin_url . '/tools/backupdb'];
        $sub_menu['70'] = ['id' => 'optimizedb', 'title' => $this->bb->lang->optimize_database, 'link' => $this->bb->admin_url . '/tools/optimizedb'];
        $sub_menu['80'] = ['id' => 'file_verification', 'title' => $this->bb->lang->file_verification, 'link' => $this->bb->admin_url . '/tools/file_verification'];

        $sub_menu = $this->bb->plugins->runHooks('admin_tools_menu', $sub_menu);

        $this->bb->page->add_menu_item($this->bb->lang->maintenance, 'tools', $this->bb->admin_url . '/tools', 50, $sub_menu);

        return true;
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function tools_action_handler($action)
    {
        $this->bb->page->active_module = 'tools';

        $actions = [
            'tools' => ['active' => 'system_health', 'file' => ''],
            'php_info' => ['active' => 'php_info', 'file' => 'php_info.php'],
            'tasks' => ['active' => 'tasks', 'file' => 'tasks.php'],
            'backupdb' => ['active' => 'backupdb', 'file' => 'backupdb.php'],
            'optimizedb' => ['active' => 'optimizedb', 'file' => 'optimizedb.php'],
            'cache' => ['active' => 'cache', 'file' => 'cache.php'],
            'recount_rebuild' => ['active' => 'recount_rebuild', 'file' => 'recount_rebuild.php'],
            'maillogs' => ['active' => 'maillogs', 'file' => 'maillogs.php'],
            'mailerrors' => ['active' => 'mailerrors', 'file' => 'mailerrors.php'],
            'adminlog' => ['active' => 'adminlog', 'file' => 'adminlog.php'],
            'modlog' => ['active' => 'modlog', 'file' => 'modlog.php'],
            'warninglog' => ['active' => 'warninglog', 'file' => 'warninglog.php'],
            'spamlog' => ['active' => 'spamlog', 'file' => 'spamlog.php'],
            'system_health' => ['active' => 'system_health', 'file' => 'system_health.php'],
            'file_verification' => ['active' => 'file_verification', 'file' => 'file_verification.php'],
            'statistics' => ['active' => 'statistics', 'file' => 'statistics.php'],
        ];

        $actions = $this->bb->plugins->runHooks('admin_tools_action_handler', $actions);

        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'adminlog', 'title' => $this->bb->lang->administrator_log, 'link' => $this->bb->admin_url . '/tools/adminlog'];
        $sub_menu['20'] = ['id' => 'modlog', 'title' => $this->bb->lang->moderator_log, 'link' => $this->bb->admin_url . '/tools/modlog'];
        $sub_menu['30'] = ['id' => 'maillogs', 'title' => $this->bb->lang->user_email_log, 'link' => $this->bb->admin_url . '/tools/maillogs'];
        $sub_menu['40'] = ['id' => 'mailerrors', 'title' => $this->bb->lang->system_mail_log, 'link' => $this->bb->admin_url . '/tools/mailerrors'];
        $sub_menu['50'] = ['id' => 'warninglog', 'title' => $this->bb->lang->user_warning_log, 'link' => $this->bb->admin_url . '/tools/warninglog'];
        $sub_menu['60'] = ['id' => 'spamlog', 'title' => $this->bb->lang->spam_log, 'link' => $this->bb->admin_url . '/tools/spamlog'];
        $sub_menu['70'] = ['id' => 'statistics', 'title' => $this->bb->lang->statistics, 'link' => $this->bb->admin_url . '/tools/statistics'];

        $sub_menu = $this->bb->plugins->runHooks('admin_tools_menu_logs', $sub_menu);

        if (!isset($actions[$action])) {
            $this->bb->page->active_action = 'system_health';
        }

        $sidebar = new SidebarItem($this->bb, $this->bb->lang->logs);
        $sidebar->add_menu_items($sub_menu, $actions[$action]['active']);

        $this->bb->page->sidebar .= $sidebar->get_markup();

        //\RunBB\Init
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('forum-tools');

        if (isset($actions[$action])) {
            $this->bb->page->active_action = $actions[$action]['active'];
            return $actions[$action]['file'];
        } else {
            return 'system_health.php';
        }
    }

    /**
     * @return array
     */
    public function tools_admin_permissions()
    {
        $admin_permissions = [
            'system_health' => $this->bb->lang->can_access_system_health,
            'cache' => $this->bb->lang->can_manage_cache,
            'tasks' => $this->bb->lang->can_manage_tasks,
            'backupdb' => $this->bb->lang->can_manage_db_backup,
            'optimizedb' => $this->bb->lang->can_optimize_db,
            'recount_rebuild' => $this->bb->lang->can_recount_and_rebuild,
            'adminlog' => $this->bb->lang->can_manage_admin_logs,
            'modlog' => $this->bb->lang->can_manage_mod_logs,
            'maillogs' => $this->bb->lang->can_manage_user_mail_log,
            'mailerrors' => $this->bb->lang->can_manage_system_mail_log,
            'warninglog' => $this->bb->lang->can_manage_user_warning_log,
            'spamlog' => $this->bb->lang->can_manage_spam_log,
            'php_info' => $this->bb->lang->can_view_php_info,
            'file_verification' => $this->bb->lang->can_manage_file_verification,
            'statistics' => $this->bb->lang->can_view_statistics,
        ];

        $admin_permissions = $this->bb->plugins->runHooks('admin_tools_permissions', $admin_permissions);

        return ['name' => $this->bb->lang->maintenance, 'permissions' => $admin_permissions, 'disporder' => 50];
    }
}
