<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Modlog extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_modlog', false, true);

        $this->page->add_breadcrumb_item($this->lang->mod_logs, $this->bb->admin_url . '/tools/modlog');

        $sub_tabs['mod_logs'] = [
            'title' => $this->lang->mod_logs,
            'link' => $this->bb->admin_url . '/tools/modlog',
            'description' => $this->lang->mod_logs_desc
        ];
        $sub_tabs['prune_mod_logs'] = [
            'title' => $this->lang->prune_mod_logs,
            'link' => $this->bb->admin_url . '/tools/modlog?action=prune',
            'description' => $this->lang->prune_mod_logs_desc
        ];

        $this->plugins->runHooks('admin_tools_modlog_begin');

        if ($this->bb->input['action'] == 'prune') {
            $this->plugins->runHooks('admin_tools_modlog_prune');

            if ($this->bb->request_method == 'post') {
                $is_today = false;
                $this->bb->input['older_than'] = $this->bb->getInput('older_than', 0);
                if ($this->bb->input['older_than'] <= 0) {
                    $is_today = true;
                    $this->bb->input['older_than'] = 1;
                }
                $where = 'dateline < ' . (TIME_NOW - ($this->bb->input['older_than'] * 86400));

                // Searching for entries by a particular user
                if ($this->bb->input['uid']) {
                    $where .= " AND uid='" . $this->bb->getInput('uid', 0) . "'";
                }

                // Searching for entries in a specific module
                if ($this->bb->input['fid'] > 0) {
                    $where .= " AND fid='" . $this->db->escape_string($this->bb->input['fid']) . "'";
                } else {
                    $this->bb->input['fid'] = 0;
                }

                $this->db->delete_query('moderatorlog', $where);
                $num_deleted = $this->db->affected_rows();

                $this->plugins->runHooks('admin_tools_modlog_prune_commit');

                if (empty($this->bb->forum_cache)) {
                    $this->forum->cache_forums();
                }

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['older_than'], $this->bb->input['uid'], $this->bb->input['fid'], $num_deleted, $this->bb->forum_cache[$this->bb->input['fid']]['name']);

                $success = $this->lang->success_pruned_mod_logs;
                if ($is_today == true && $num_deleted > 0) {
                    $success .= ' ' . $this->lang->note_logs_locked;
                } elseif ($is_today == true && $num_deleted == 0) {
                    $this->session->flash_message($this->lang->note_logs_locked, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/modlog');
                }
                $this->session->flash_message($success, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/modlog');
            }
            $this->page->add_breadcrumb_item($this->lang->prune_mod_logs, $this->bb->admin_url . '/tools/modlog?action=prune');
            $html = $this->page->output_header($this->lang->prune_mod_logs);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'prune_mod_logs');

            // Fetch filter options
            $sortbysel[$this->bb->input['sortby']] = 'selected="selected"';
            $ordersel[$this->bb->input['order']] = 'selected="selected"';

            $user_options[''] = $this->lang->all_moderators;
            $user_options['0'] = '----------';

            $query = $this->db->query('
		SELECT DISTINCT l.uid, u.username
		FROM ' . TABLE_PREFIX . 'moderatorlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (l.uid=u.uid)
		ORDER BY u.username ASC
	');
            while ($user = $this->db->fetch_array($query)) {
                $user_options[$user['uid']] = $user['username'];
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/modlog?action=prune', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->prune_moderator_logs);
            $form_container->output_row($this->lang->forum, '', $form->generate_forum_select('fid', $this->bb->input['fid'], ['id' => 'fid', 'main_option' => $this->lang->all_forums]), 'fid');
            $form_container->output_row($this->lang->forum_moderator, '', $form->generate_select_box('uid', $user_options, $this->bb->input['uid'], ['id' => 'uid']), 'uid');
            if (!$this->bb->input['older_than']) {
                $this->bb->input['older_than'] = '30';
            }
            $form_container->output_row($this->lang->date_range, '', $this->lang->older_than . $form->generate_numeric_field('older_than', $this->bb->input['older_than'], ['id' => 'older_than', 'style' => 'width: 50px', 'min' => 0]) . ' ' . $this->lang->days, 'older_than');
            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->prune_moderator_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/ModLogPrune.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_tools_modlog_start');

            $html = $this->page->output_header($this->lang->mod_logs);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'mod_logs');

            $perpage = $this->bb->getInput('perpage', 0);
            if (!$perpage) {
                if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                    $this->bb->settings['threadsperpage'] = 20;
                }

                $perpage = $this->bb->settings['threadsperpage'];
            }

            $where = 'WHERE 1=1';

            // Searching for entries by a particular user
            if ($this->bb->input['uid']) {
                $where .= " AND l.uid='" . $this->bb->getInput('uid', 0) . "'";
            }

            // Searching for entries in a specific forum
            if ($this->bb->input['fid'] > 0) {
                $where .= " AND l.fid='" . $this->bb->getInput('fid', 0) . "'";
            }

            // Order?
            switch ($this->bb->input['sortby']) {
                case 'username':
                    $sortby = 'u.username';
                    break;
                case 'forum':
                    $sortby = 'f.name';
                    break;
                case 'thread':
                    $sortby = 't.subject';
                    break;
                default:
                    $sortby = 'l.dateline';
            }
            $order = $this->bb->input['order'];
            if ($order != 'asc') {
                $order = 'desc';
            }

            $query = $this->db->query('
		SELECT COUNT(l.dateline) AS count
		FROM ' . TABLE_PREFIX . "moderatorlog l
		{$where}
	");
            $rescount = $this->db->fetch_field($query, 'count');

            // Figure out if we need to display multiple pages.
            if ($this->bb->input['page'] != 'last') {
                $pagecnt = $this->bb->getInput('page', 0);
            }

            $postcount = (int)$rescount;
            $pages = $postcount / $perpage;
            $pages = ceil($pages);

            if ($this->bb->input['page'] == 'last') {
                $pagecnt = $pages;
            }

            if ($pagecnt > $pages) {
                $pagecnt = 1;
            }

            if ($pagecnt) {
                $start = ($pagecnt - 1) * $perpage;
            } else {
                $start = 0;
                $pagecnt = 1;
            }

            $table = new Table;
            $table->construct_header($this->lang->username, ['width' => '10%']);
            $table->construct_header($this->lang->date, ['class' => 'align_center', 'width' => '15%']);
            $table->construct_header($this->lang->action, ['class' => 'align_center', 'width' => '35%']);
            $table->construct_header($this->lang->information, ['class' => 'align_center', 'width' => '30%']);
            $table->construct_header($this->lang->ipaddress, ['class' => 'align_center', 'width' => '10%']);

            $query = $this->db->query('
		SELECT l.*, u.username, u.usergroup, u.displaygroup, t.subject AS tsubject, f.name AS fname, p.subject AS psubject
		FROM ' . TABLE_PREFIX . 'moderatorlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=l.uid)
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=l.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'forums f ON (f.fid=l.fid)
		LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=l.pid)
		{$where}
		ORDER BY {$sortby} {$order}
		LIMIT {$start}, {$perpage}
	");
            while ($logitem = $this->db->fetch_array($query)) {
                $information = '';
                $logitem['action'] = htmlspecialchars_uni($logitem['action']);
                $logitem['dateline'] = $this->time->formatDate('relative', $logitem['dateline']);
                $trow = alt_trow();
                $username = $this->user->format_name($logitem['username'], $logitem['usergroup'], $logitem['displaygroup']);
                $logitem['profilelink'] = $this->user->build_profile_link($username, $logitem['uid'], '_blank');
                if ($logitem['tsubject']) {
                    $information = "<strong>{$this->lang->thread}</strong> <a href=\"" . get_thread_link($logitem['tid']) . "\" target=\"_blank\">" . htmlspecialchars_uni($logitem['tsubject']) . "</a><br />";
                }
                if ($logitem['fname']) {
                    $information .= "<strong>{$this->lang->forum}</strong> <a href=\"" . $this->forum->get_forum_link($logitem['fid']) . "\" target=\"_blank\">" . htmlspecialchars_uni($logitem['fname']) . "</a><br />";
                }
                if ($logitem['psubject']) {
                    $information .= "<strong>{$this->lang->post}</strong> <a href=\"" . get_post_link($logitem['pid']) . "#pid{$logitem['pid']}\" target=\"_blank\">" . htmlspecialchars_uni($logitem['psubject']) . "</a>";
                }

                if (!$logitem['tsubject'] || !$logitem['fname'] || !$logitem['psubject']) {
                    $data = my_unserialize($logitem['data']);
                    if ($data['uid']) {
                        $information = "<strong>{$this->lang->user_info}</strong> <a href=\"" . get_profile_link($data['uid']) . "\" target=\"_blank\">" . htmlspecialchars_uni($data['username']) . "</a>";
                    }
                    if ($data['aid']) {
                        $information = "<strong>{$this->lang->announcement}</strong> <a href=\"" . get_announcement_link($data['aid']) . "\" target=\"_blank\">" . htmlspecialchars_uni($data['subject']) . "</a>";
                    }
                }

                $table->construct_cell($logitem['profilelink']);
                $table->construct_cell($logitem['dateline'], ['class' => 'align_center']);
                $table->construct_cell($logitem['action'], ['class' => 'align_center']);
                $table->construct_cell($information);
                $table->construct_cell($logitem['ipaddress'], ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_modlogs, ['colspan' => '5']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->mod_logs);

            // Do we need to construct the pagination?
            if ($rescount > $perpage) {
                $html .= $this->adm->draw_admin_pagination($pagecnt, $perpage, $rescount, $this->bb->admin_url . "/tools/modlog?perpage=$perpage&amp;uid={$this->bb->input['uid']}&amp;fid={$this->bb->input['fid']}&amp;sortby={$this->bb->input['sortby']}&amp;order={$order}") . "<br />";
            }

            // Fetch filter options
            $sortbysel[$this->bb->input['sortby']] = 'selected="selected"';
            $ordersel[$this->bb->input['order']] = 'selected="selected"';

            $user_options[''] = $this->lang->all_moderators;
            $user_options['0'] = '----------';

            $query = $this->db->query('
		SELECT DISTINCT l.uid, u.username
		FROM ' . TABLE_PREFIX . 'moderatorlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (l.uid=u.uid)
		ORDER BY u.username ASC
	');
            while ($user = $this->db->fetch_array($query)) {
                $selected = '';
                if ($this->bb->input['uid'] == $user['uid']) {
                    $selected = 'selected="selected"';
                }
                $user_options[$user['uid']] = $user['username'];
            }

            $sort_by = [
                'dateline' => $this->lang->date,
                'username' => $this->lang->username,
                'forum' => $this->lang->forum_name,
                'thread' => $this->lang->thread_subject
            ];

            $order_array = [
                'asc' => $this->lang->asc,
                'desc' => $this->lang->desc
            ];

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/modlog', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->filter_moderator_logs);
            $form_container->output_row($this->lang->forum, '', $form->generate_forum_select('fid', $this->bb->input['fid'], ['id' => 'fid', 'main_option' => $this->lang->all_forums]), 'fid');
            $form_container->output_row($this->lang->forum_moderator, '', $form->generate_select_box('uid', $user_options, $this->bb->input['uid'], ['id' => 'uid']), 'uid');
            $form_container->output_row($this->lang->sort_by, '', $form->generate_select_box('sortby', $sort_by, $this->bb->input['sortby'], ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('order', $order_array, $order, ['id' => 'order']) . " {$this->lang->order}", 'order');
            $form_container->output_row($this->lang->results_per_page, '', $form->generate_numeric_field('perpage', $perpage, ['id' => 'perpage', 'min' => 1]), 'perpage');

            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->filter_moderator_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/ModLog.html.twig');
        }
    }
}
