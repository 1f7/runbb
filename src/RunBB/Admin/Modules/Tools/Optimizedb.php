<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunCMF\Core\AbstractController;

class Optimizedb extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_optimizedb', false, true);

        $this->page->add_breadcrumb_item($this->lang->optimize_database, $this->bb->admin_url . '/tools/optimizedb');

        $this->plugins->runHooks('admin_tools_optimizedb_begin');

        $this->plugins->runHooks('admin_tools_optimizedb_start');

        if ($this->bb->request_method == 'post') {
            if (!is_array($this->bb->input['tables'])) {
                $this->session->flash_message($this->lang->error_no_tables_selected, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/optimizedb');
            }

            @set_time_limit(0);

            $this->db->set_table_prefix('');

            foreach ($this->bb->input['tables'] as $table) {
                if ($this->db->table_exists($this->db->escape_string($table))) {
                    $this->db->optimize_table($table);
                    $this->db->analyze_table($table);
                }
            }

            $this->db->set_table_prefix(TABLE_PREFIX);

            $this->plugins->runHooks('admin_tools_optimizedb_start_begin');

            // Log admin action
            $this->bblogger->log_admin_action(my_serialize($this->bb->input['tables']));

            $this->session->flash_message($this->lang->success_tables_optimized, 'success');
            return $response->withRedirect($this->bb->admin_url . '/tools/optimizedb');
        }

        $this->page->extra_header = "	<script type=\"text/javascript\">
function changeSelection(action, prefix)
{
    var select_box = document.getElementById('table_select');

    for(var i = 0; i < select_box.length; i++)
    {
        if(action == 'select')
        {
            select_box[i].selected = true;
        }
        else if(action == 'deselect')
        {
            select_box[i].selected = false;
        }
        else if(action == 'forum' && prefix != 0)
        {
            select_box[i].selected = false;
            var row = select_box[i].value;
            var subString = row.substring(prefix.length, 0);
            if(subString == prefix)
            {
                select_box[i].selected = true;
            }
        }
    }
}
</script>\n";

        $html = $this->page->output_header($this->lang->optimize_database);

        $table = new Table;
        $table->construct_header($this->lang->table_selection);

        $table_selects = [];
        $table_list = $this->db->list_tables($this->bb->config['database']['database']);
        foreach ($table_list as $id => $table_name) {
            $table_selects[$table_name] = $table_name;
        }

        $form = new Form($this->bb, $this->bb->admin_url . '/tools/optimizedb', 'post', 'table_selection', 0, 'table_selection');
        $html .= $form->getForm();

        $table->construct_cell("{$this->lang->tables_select_desc}\n<br /><br />\n<a href=\"javascript:changeSelection('select', 0);\">{$this->lang->select_all}</a><br />\n<a href=\"javascript:changeSelection('deselect', 0);\">{$this->lang->deselect_all}</a><br />\n<a href=\"javascript:changeSelection('forum', '" . TABLE_PREFIX . "');\">{$this->lang->select_forum_tables}</a>\n<br /><br />\n<div class=\"form_row\">" . $form->generate_select_box("tables[]", $table_selects, false, ['multiple' => true, 'id' => 'table_select', 'size' => 20]) . "</div>", ['rowspan' => 5, 'width' => '50%']);
        $table->construct_row();

        $html .= $table->output($this->lang->optimize_database);

        $buttons[] = $form->generate_submit_button($this->lang->optimize_selected_tables);
        $html .= $form->output_submit_wrapper($buttons);
        $html .= $form->end();

        $this->page->output_footer();

        $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
        return $this->view->render($response, '@forum/Admin/Tools/OptimizeDB.html.twig');
    }
}
