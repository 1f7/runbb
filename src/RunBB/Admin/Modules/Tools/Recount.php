<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Recount extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_recount_rebuild', false, true);

        $this->page->add_breadcrumb_item($this->lang->recount_rebuild, $this->bb->admin_url . '/tools/recount_rebuild');

        $this->plugins->runHooks('admin_tools_recount_rebuild');


        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_tools_recount_rebuild_start');

            if ($this->bb->request_method === 'post') {
                if ($this->bb->getInput('page', 0) < 1) {
                    $this->bb->input['page'] = 1;
                }
                $this->rebuilder = new \RunBB\Helpers\Restorer($this->bb);

                if (isset($this->bb->input['do_rebuildforumcounters'])) {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_forum_counters');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('forum');
                    }
                    if (!$this->bb->getInput('forumcounters', 0)) {
                        $this->bb->input['forumcounters'] = 50;
                    }

                    return $this->acp_rebuild_forum_counters();
                } elseif (isset($this->bb->input['do_rebuildthreadcounters'])) {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_thread_counters');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('thread');
                    }
                    if (!$this->bb->getInput('threadcounters', 0)) {
                        $this->bb->input['threadcounters'] = 500;
                    }

                    $this->acp_rebuild_thread_counters();
                } elseif (isset($this->bb->input['do_recountuserposts'])) {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_user_posts');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('userposts');
                    }
                    if (!$this->bb->getInput('userposts', 0)) {
                        $this->bb->input['userposts'] = 500;
                    }

                    $this->acp_recount_user_posts();
                } elseif (isset($this->bb->input['do_recountuserthreads'])) {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_user_threads');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('userthreads');
                    }
                    if (!$this->bb->getInput('userthreads', 0)) {
                        $this->bb->input['userthreads'] = 500;
                    }

                    $this->acp_recount_user_threads();
                } elseif (isset($this->bb->input['do_rebuildattachmentthumbs'])) {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_attachment_thumbs');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('attachmentthumbs');
                    }

                    if (!$this->bb->getInput('attachmentthumbs', 0)) {
                        $this->bb->input['attachmentthumbs'] = 500;
                    }

                    $this->acp_rebuild_attachment_thumbnails();
                } elseif (isset($this->bb->input['do_recountreputation'])) {
                    $this->plugins->runHooks('admin_tools_recount_recount_reputation');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('reputation');
                    }

                    if (!$this->bb->getInput('reputation', 0)) {
                        $this->bb->input['reputation'] = 500;
                    }

                    $this->acp_recount_reputation();
                } elseif (isset($this->bb->input['do_recountwarning'])) {
                    $this->plugins->runHooks('admin_tools_recount_recount_warning');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('warning');
                    }

                    if (!$this->bb->getInput('warning', 0)) {
                        $this->bb->input['warning'] = 500;
                    }

                    $this->acp_recount_warning();
                } elseif (isset($this->bb->input['do_recountprivatemessages'])) {
                    $this->plugins->runHooks('admin_tools_recount_recount_private_messages');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('privatemessages');
                    }

                    if (!$this->bb->getInput('privatemessages', 0)) {
                        $this->bb->input['privatemessages'] = 500;
                    }

                    $this->acp_recount_private_messages();
                } elseif (isset($this->bb->input['do_recountreferral'])) {
                    $this->plugins->runHooks('admin_tools_recount_recount_referral');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('referral');
                    }

                    if (!$this->bb->getInput('referral', 0)) {
                        $this->bb->input['referral'] = 500;
                    }

                    $this->acp_recount_referrals();
                } elseif (isset($this->bb->input['do_recountthreadrating'])) {
                    $this->plugins->runHooks('admin_tools_recount_recount_thread_ratings');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('threadrating');
                    }

                    if (!$this->bb->getInput('threadrating', 0)) {
                        $this->bb->input['threadrating'] = 500;
                    }

                    $this->acp_recount_thread_ratings();
                } elseif (isset($this->bb->input['do_rebuildpollcounters'])) {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_poll_counters');

                    if ($this->bb->input['page'] == 1) {
                        // Log admin action
                        $this->bblogger->log_admin_action('poll');
                    }

                    if (!$this->bb->getInput('pollcounters', 0)) {
                        $this->bb->input['pollcounters'] = 500;
                    }
                    $this->acp_rebuild_poll_counters();
                } else {
                    $this->plugins->runHooks('admin_tools_recount_rebuild_stats');

                    $this->cache->update_stats();

                    // Log admin action
                    $this->bblogger->log_admin_action('stats');

                    $this->session->flash_message($this->lang->success_rebuilt_forum_stats, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/tools/recount_rebuild');
                }
            }

            $html = $this->page->output_header($this->lang->recount_rebuild);

            $sub_tabs['recount_rebuild'] = [
                'title' => $this->lang->recount_rebuild,
                'link' => $this->bb->admin_url . '/tools/recount_rebuild',
                'description' => $this->lang->recount_rebuild_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'recount_rebuild');

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/recount_rebuild', 'post');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->recount_rebuild);
            $form_container->output_row_header($this->lang->name);
            $form_container->output_row_header($this->lang->data_per_page, ['width' => 50]);
            $form_container->output_row_header('&nbsp;');

            $form_container->output_cell("<label>{$this->lang->rebuild_forum_counters}</label><div class=\"description\">{$this->lang->rebuild_forum_counters_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('forumcounters', 50, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_rebuildforumcounters']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->rebuild_thread_counters}</label><div class=\"description\">{$this->lang->rebuild_thread_counters_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('threadcounters', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_rebuildthreadcounters']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->rebuild_poll_counters}</label><div class=\"description\">{$this->lang->rebuild_poll_counters_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('pollcounters', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_rebuildpollcounters']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_user_posts}</label><div class=\"description\">{$this->lang->recount_user_posts_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('userposts', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountuserposts']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_user_threads}</label><div class=\"description\">{$this->lang->recount_user_threads_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('userthreads', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountuserthreads']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->rebuild_attachment_thumbs}</label><div class=\"description\">{$this->lang->rebuild_attachment_thumbs_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('attachmentthumbs', 20, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_rebuildattachmentthumbs']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_stats}</label><div class=\"description\">{$this->lang->recount_stats_desc}</div>");
            $form_container->output_cell($this->lang->na);
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountstats']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_reputation}</label><div class=\"description\">{$this->lang->recount_reputation_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('reputation', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountreputation']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_warning}</label><div class=\"description\">{$this->lang->recount_warning_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('warning', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountwarning']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_private_messages}</label><div class=\"description\">{$this->lang->recount_private_messages_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('privatemessages', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountprivatemessages']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_referrals}</label><div class=\"description\">{$this->lang->recount_referrals_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('referral', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountreferral']));
            $form_container->construct_row();

            $form_container->output_cell("<label>{$this->lang->recount_thread_ratings}</label><div class=\"description\">{$this->lang->recount_thread_ratings_desc}</div>");
            $form_container->output_cell($form->generate_numeric_field('threadrating', 500, ['style' => 'width: 150px;', 'min' => 0]));
            $form_container->output_cell($form->generate_submit_button($this->lang->go, ['name' => 'do_recountthreadrating']));
            $form_container->construct_row();

            $this->plugins->runHooks('admin_tools_recount_rebuild_output_list');

            $html .= $form_container->end();

            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/Recount.html.twig');
        }
    }

    /**
     * Rebuild forum counters
     */
    private function acp_rebuild_forum_counters()
    {
        $query = $this->db->simple_select('forums', 'COUNT(*) as num_forums');
        $num_forums = $this->db->fetch_field($query, 'num_forums');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('forumcounters', 0);
        if ($per_page <= 0) {
            $per_page = 50;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('forums', 'fid', '', ['order_by' => 'fid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($forum = $this->db->fetch_array($query)) {
            $update['parentlist'] = $this->adm->make_parent_list($forum['fid']);
            $this->db->update_query('forums', $update, "fid='{$forum['fid']}'");
            $this->rebuilder->rebuild_forum_counters($forum['fid']);
        }

        return $this->check_proceed($num_forums, $end, ++$page, $per_page, 'forumcounters', 'do_rebuildforumcounters', $this->lang->success_rebuilt_forum_counters);
    }

    /**
     * Rebuild thread counters
     */
    private function acp_rebuild_thread_counters()
    {
        $query = $this->db->simple_select('threads', 'COUNT(*) as num_threads');
        $num_threads = $this->db->fetch_field($query, 'num_threads');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('threadcounters', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('threads', 'tid', '', ['order_by' => 'tid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($thread = $this->db->fetch_array($query)) {
            $this->rebuilder->rebuild_thread_counters($thread['tid']);
        }

        $this->check_proceed($num_threads, $end, ++$page, $per_page, 'threadcounters', 'do_rebuildthreadcounters', $this->lang->success_rebuilt_thread_counters);
    }

    /**
     * Rebuild poll counters
     */
    private function acp_rebuild_poll_counters()
    {
        $query = $this->db->simple_select('polls', 'COUNT(*) as num_polls');
        $num_polls = $this->db->fetch_field($query, 'num_polls');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('pollcounters', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('polls', 'pid', '', ['order_by' => 'pid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($poll = $this->db->fetch_array($query)) {
            $this->rebuilder->rebuild_poll_counters($poll['pid']);
        }

        $this->check_proceed($num_polls, $end, ++$page, $per_page, 'pollcounters', 'do_rebuildpollcounters', $this->lang->success_rebuilt_poll_counters);
    }

    /**
     * Recount user posts
     */
    private function acp_recount_user_posts()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) as num_users');
        $num_users = $this->db->fetch_field($query, 'num_users');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('userposts', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('forums', 'fid', 'usepostcounts = 0');
        $fids = [];
        while ($forum = $this->db->fetch_array($query)) {
            $fids[] = $forum['fid'];
        }
        if (!empty($fids)) {
            $fids = implode(',', $fids);
        }
        if ($fids) {
            $fids = " AND p.fid NOT IN($fids)";
        } else {
            $fids = '';
        }

        $query = $this->db->simple_select('users', 'uid', '', ['order_by' => 'uid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($user = $this->db->fetch_array($query)) {
            $query2 = $this->db->query('
			SELECT COUNT(p.pid) AS post_count
			FROM ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
			WHERE p.uid='{$user['uid']}' AND t.visible > 0 AND p.visible > 0{$fids}
		");
            $num_posts = $this->db->fetch_field($query2, 'post_count');

            $this->db->update_query('users', ['postnum' => (int)$num_posts], "uid='{$user['uid']}'");
        }

        $this->check_proceed($num_users, $end, ++$page, $per_page, 'userposts', 'do_recountuserposts', $this->lang->success_rebuilt_user_post_counters);
    }

    /**
     * Recount user threads
     */
    private function acp_recount_user_threads()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) as num_users');
        $num_users = $this->db->fetch_field($query, 'num_users');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('userthreads', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('forums', 'fid', 'usethreadcounts = 0');
        $fids = [];
        while ($forum = $this->db->fetch_array($query)) {
            $fids[] = $forum['fid'];
        }
        if (!empty($fids)) {
            $fids = implode(',', $fids);
        }
        if ($fids) {
            $fids = " AND t.fid NOT IN($fids)";
        } else {
            $fids = '';
        }

        $query = $this->db->simple_select('users', 'uid', '', ['order_by' => 'uid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($user = $this->db->fetch_array($query)) {
            $query2 = $this->db->query('
			SELECT COUNT(t.tid) AS thread_count
			FROM ' . TABLE_PREFIX . "threads t
			WHERE t.uid='{$user['uid']}' AND t.visible > 0 AND t.closed NOT LIKE 'moved|%'{$fids}
		");
            $num_threads = $this->db->fetch_field($query2, 'thread_count');

            $this->db->update_query('users', ['threadnum' => (int)$num_threads], "uid='{$user['uid']}'");
        }

        $this->check_proceed($num_users, $end, ++$page, $per_page, 'userthreads', 'do_recountuserthreads', $this->lang->success_rebuilt_user_thread_counters);
    }

    /**
     * Recount reputation values
     */
    private function acp_recount_reputation()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) as num_users');
        $num_users = $this->db->fetch_field($query, 'num_users');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('reputation', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('users', 'uid', '', ['order_by' => 'uid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($user = $this->db->fetch_array($query)) {
            $query2 = $this->db->query('
			SELECT SUM(reputation) as total_rep
			FROM ' . TABLE_PREFIX . "reputation
			WHERE `uid`='{$user['uid']}'
		");
            $total_rep = $this->db->fetch_field($query2, 'total_rep');

            $this->db->update_query('users', ['reputation' => (int)$total_rep], "uid='{$user['uid']}'");
        }

        $this->check_proceed($num_users, $end, ++$page, $per_page, 'reputation', 'do_recountreputation', $this->lang->success_rebuilt_reputation);
    }

    /**
     * Recount warnings for users
     */
    private function acp_recount_warning()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) as num_users');
        $num_users = $this->db->fetch_field($query, 'num_users');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('warning', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('users', 'uid', '', ['order_by' => 'uid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($user = $this->db->fetch_array($query)) {
            $query2 = $this->db->query('
			SELECT SUM(points) as warn_lev
			FROM ' . TABLE_PREFIX . "warnings
			WHERE uid='{$user['uid']}' AND expired='0'
		");
            $warn_lev = $this->db->fetch_field($query2, 'warn_lev');

            $this->db->update_query('users', ['warningpoints' => (int)$warn_lev], "uid='{$user['uid']}'");
        }

        $this->check_proceed($num_users, $end, ++$page, $per_page, 'warning', 'do_recountwarning', $this->lang->success_rebuilt_warning);
    }

    /**
     * Recount private messages (total and unread) for users
     */
    private function acp_recount_private_messages()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) as num_users');
        $num_users = $this->db->fetch_field($query, 'num_users');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('privatemessages', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('users', 'uid', '', ['order_by' => 'uid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($user = $this->db->fetch_array($query)) {
            $this->user->update_pm_count($user['uid']);
        }

        $this->check_proceed($num_users, $end, ++$page, $per_page, 'privatemessages', 'do_recountprivatemessages', $this->lang->success_rebuilt_private_messages);
    }

    /**
     * Recount referrals for users
     */
    private function acp_recount_referrals()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) as num_users');
        $num_users = $this->db->fetch_field($query, 'num_users');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('referral', 0);
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('users', 'uid', '', ['order_by' => 'uid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($user = $this->db->fetch_array($query)) {
            $query2 = $this->db->query('
			SELECT COUNT(uid) as num_referrers
			FROM ' . TABLE_PREFIX . "users
			WHERE referrer='{$user['uid']}'
		");
            $num_referrers = $this->db->fetch_field($query2, 'num_referrers');

            $this->db->update_query('users', ['referrals' => (int)$num_referrers], "uid='{$user['uid']}'");
        }

        $this->check_proceed($num_users, $end, ++$page, $per_page, 'referral', 'do_recountreferral', $this->lang->success_rebuilt_referral);
    }

    /**
     * Recount thread ratings
     */
    private function acp_recount_thread_ratings()
    {
        $query = $this->db->simple_select('threads', 'COUNT(*) as num_threads');
        $num_threads = $this->db->fetch_field($query, 'num_threads');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('threadrating', 0);
        if ($per_page <= 0) {
            $per_page = 500;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

        $query = $this->db->simple_select('threads', 'tid', '', ['order_by' => 'tid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($thread = $this->db->fetch_array($query)) {
            $query2 = $this->db->query('
			SELECT COUNT(tid) as num_ratings, SUM(rating) as total_rating
			FROM ' . TABLE_PREFIX . "threadratings
			WHERE tid='{$thread['tid']}'
		");
            $recount = $this->db->fetch_array($query2);

            $this->db->update_query('threads', ['numratings' => (int)$recount['num_ratings'], 'totalratings' => (int)$recount['total_rating']], "tid='{$thread['tid']}'");
        }

        $this->check_proceed($num_threads, $end, ++$page, $per_page, 'threadrating', 'do_recountthreadrating', $this->lang->success_rebuilt_thread_ratings);
    }

    /**
     * Rebuild thumbnails for attachments
     */
    private function acp_rebuild_attachment_thumbnails()
    {
        $query = $this->db->simple_select('attachments', 'COUNT(aid) as num_attachments');
        $num_attachments = $this->db->fetch_field($query, 'num_attachments');

        $page = $this->bb->getInput('page', 0);
        $per_page = $this->bb->getInput('attachmentthumbs', 0);
        if ($per_page <= 0) {
            $per_page = 20;
        }
        $start = ($page - 1) * $per_page;
        $end = $start + $per_page;

//    $uploadspath = $this->bb->settings['uploadspath'];
//    if(my_substr($uploadspath, 0, 1) == '.')
//    {
        $uploadspath = DIR . 'web' . $this->bb->settings['uploadspath'];
//    }

        $img = new \RunBB\Core\Image();

        $query = $this->db->simple_select('attachments', '*', '', ['order_by' => 'aid', 'order_dir' => 'asc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($attachment = $this->db->fetch_array($query)) {
            $ext = my_strtolower(my_substr(strrchr($attachment['filename'], '.'), 1));
            if ($ext == 'gif' || $ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'jpe') {
                $thumbname = str_replace('.attach', "_thumb.$ext", $attachment['attachname']);
                $thumbnail = $img->generate_thumbnail($uploadspath . '/' . $attachment['attachname'], $uploadspath, $thumbname, $this->bb->settings['attachthumbh'], $this->bb->settings['attachthumbw']);
                if ($thumbnail['code'] == 4) {
                    $thumbnail['filename'] = 'SMALL';
                }
                $this->db->update_query('attachments', ['thumbnail' => $thumbnail['filename']], "aid='{$attachment['aid']}'");
            }
        }

        $this->check_proceed($num_attachments, $end, ++$page, $per_page, 'attachmentthumbs', 'do_rebuildattachmentthumbs', $this->lang->success_rebuilt_attachment_thumbnails);
    }

    /**
     * @param int $current
     * @param int $finish
     * @param int $next_page
     * @param int $per_page
     * @param string $name
     * @param string $name2
     * @param string $message
     */
    private function check_proceed($current, $finish, $next_page, $per_page, $name, $name2, $message)
    {
        if ($finish >= $current) {
            $this->session->flash_message($message, 'success');
            return $this->response->withRedirect($this->bb->admin_url . '/tools/recount_rebuild');
        } else {
            $html = $this->page->output_header();

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/recount_rebuild', 'post');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('page', $next_page);
            $html .= $form->generate_hidden_field($name, $per_page);
            $html .= $form->generate_hidden_field($name2, $this->lang->go);
            $html .= "<div class=\"confirm_action\">\n";
            $html .= "<p>{$this->lang->confirm_proceed_rebuild}</p>\n";
            $html .= "<br />\n";
            $html .= "<script type=\"text/javascript\">$(function() { var button = $(\"#proceed_button\"); if(button.length > 0) { button.val(\"{$this->lang->automatically_redirecting}\"); button.attr(\"disabled\", true); button.css(\"color\", \"#aaa\"); button.css(\"borderColor\", \"#aaa\"); document.forms[0].submit(); }})</script>";
            $html .= "<p class=\"buttons\">\n";
            $html .= $form->generate_submit_button($this->lang->proceed, ['class' => 'button_yes', 'id' => 'proceed_button']);
            $html .= "</p>\n";
            $html .= "</div>\n";

            $html .= $form->end();

            $this->page->output_footer();
//            exit;
            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($this->response, '@forum/Admin/Tools/RecountProceed.html.twig');
        }
    }
}
