<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Spamlog extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_spamlog', false, true);

        $this->page->add_breadcrumb_item($this->lang->spam_logs, $this->bb->admin_url . '/tools/spamlog');

        $sub_tabs['spam_logs'] = [
            'title' => $this->lang->spam_logs,
            'link' => $this->bb->admin_url . '/tools/spamlog',
            'description' => $this->lang->spam_logs_desc
        ];
        $sub_tabs['prune_spam_logs'] = [
            'title' => $this->lang->prune_spam_logs,
            'link' => $this->bb->admin_url . '/tools/spamlog?action=prune',
            'description' => $this->lang->prune_spam_logs_desc
        ];

        $this->plugins->runHooks('admin_tools_spamlog_begin');

        if ($this->bb->input['action'] == 'prune') {
            if (!$this->user->is_super_admin($this->user->uid)) {
                $this->session->flash_message($this->lang->cannot_perform_action_super_admin_general, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/spamlog');
            }

            $this->plugins->runHooks('admin_tools_spamlog_prune');

            if ($this->bb->request_method == 'post') {
                $is_today = false;
                $this->bb->input['older_than'] = $this->bb->getInput('older_than', 0);
                if ($this->bb->input['older_than'] <= 0) {
                    $is_today = true;
                    $this->bb->input['older_than'] = 1;
                }
                $where = 'dateline < ' . (TIME_NOW - ($this->bb->input['older_than'] * 86400));

                // Searching for entries in a specific module
                if ($this->bb->input['filter_username']) {
                    $where .= " AND username='" . $this->db->escape_string($this->bb->input['filter_username']) . "'";
                }

                // Searching for entries in a specific module
                if ($this->bb->input['filter_email']) {
                    $where .= " AND email='" . $this->db->escape_string($this->bb->input['filter_email']) . "'";
                }

                $query = $this->db->delete_query('spamlog', $where);
                $num_deleted = $this->db->affected_rows();

                $this->plugins->runHooks('admin_tools_spamlog_prune_commit');

                // Log admin action
                $this->bblogger->log_admin_action($this->bb->input['older_than'], $this->bb->input['filter_username'], $this->bb->input['filter_email'], $num_deleted);

                $success = $this->lang->success_pruned_spam_logs;
                if ($is_today == true && $num_deleted > 0) {
                    $success .= ' ' . $this->lang->note_logs_locked;
                } elseif ($is_today == true && $num_deleted == 0) {
                    $this->session->flash_message($this->lang->note_logs_locked, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/tools/spamlog');
                }
                $this->session->flash_message($success, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/spamlog');
            }
            $this->page->add_breadcrumb_item($this->lang->prune_spam_logs, $this->bb->admin_url . '/tools/spamlog?action=prune');
            $html = $this->page->output_header($this->lang->prune_spam_logs);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'prune_spam_logs');

            // Fetch filter options
            $sortbysel[$this->bb->input['sortby']] = 'selected="selected"';
            $ordersel[$this->bb->input['order']] = 'selected="selected"';

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/spamlog?action=prune', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->prune_spam_logs);
            $form_container->output_row($this->lang->spam_username, '', $form->generate_text_box('filter_username', $this->bb->input['filter_username'], ['id' => 'filter_username']), 'filter_username');
            $form_container->output_row($this->lang->spam_email, '', $form->generate_text_box('filter_email', $this->bb->input['filter_email'], ['id' => 'filter_email']), 'filter_email');
            if (!$this->bb->input['older_than']) {
                $this->bb->input['older_than'] = '30';
            }
            $form_container->output_row($this->lang->date_range, '', $this->lang->older_than . $form->generate_numeric_field('older_than', $this->bb->input['older_than'], ['id' => 'older_than', 'style' => 'width: 50px', 'min' => 0]) . " {$this->lang->days}", 'older_than');
            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->prune_spam_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/SpamLogPrune.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_tools_spamlog_start');

            $html = $this->page->output_header($this->lang->spam_logs);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'spam_logs');

            $perpage = $this->bb->getInput('perpage', 0);
            if (!$perpage) {
                $perpage = 20;
            }

            $where = '1=1';

            $additional_criteria = [];

            // Searching for entries witha  specific username
            if ($this->bb->input['username']) {
                $where .= " AND username='" . $this->db->escape_string($this->bb->input['username']) . "'";
                $additional_criteria[] = 'username=' . urlencode($this->bb->input['username']);
            }

            // Searching for entries with a specific email
            if ($this->bb->input['email']) {
                $where .= " AND email='" . $this->db->escape_string($this->bb->input['email']) . "'";
                $additional_criteria[] = 'email=' . urlencode($this->bb->input['email']);
            }

            // Searching for entries with a specific IP
            if ($this->bb->input['ipaddress'] > 0) {
                $where .= ' AND ipaddress=' . $this->bb->input['ipaddress'];
                $additional_criteria[] = 'ipaddress=' . urlencode($this->bb->input['ipaddress']);
            }

            if ($additional_criteria) {
                $additional_criteria = "&amp;" . implode("&amp;", $additional_criteria);
            } else {
                $additional_criteria = '';
            }

            // Order?
            switch ($this->bb->input['sortby']) {
                case 'username':
                    $sortby = 'username';
                    break;
                case 'email':
                    $sortby = 'email';
                    break;
                case 'ipaddress':
                    $sortby = 'ipaddress';
                    break;
                default:
                    $sortby = 'dateline';
            }
            $order = $this->bb->input['order'];
            if ($order != 'asc') {
                $order = 'desc';
            }

            $query = $this->db->simple_select('spamlog', 'COUNT(sid) AS count', $where);
            $rescount = $this->db->fetch_field($query, 'count');

            // Figure out if we need to display multiple pages.
            if ($this->bb->input['page'] != 'last') {
                $pagecnt = $this->bb->getInput('page', 0);
            }

            $logcount = (int)$rescount;
            $pages = $logcount / $perpage;
            $pages = ceil($pages);

            if ($this->bb->input['page'] == 'last') {
                $pagecnt = $pages;
            }

            if ($pagecnt > $pages) {
                $pagecnt = 1;
            }

            if ($pagecnt) {
                $start = ($pagecnt - 1) * $perpage;
            } else {
                $start = 0;
                $pagecnt = 1;
            }

            $table = new Table;
            $table->construct_header($this->lang->spam_username, ['width' => '20%']);
            $table->construct_header($this->lang->spam_email, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->spam_ip, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->spam_date, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->spam_confidence, ['class' => 'align_center', 'width' => '20%']);

            $query = $this->db->simple_select('spamlog', '*', $where, ['order_by' => $sortby, 'order_dir' => $order, 'limit_start' => $start, 'limit' => $perpage]);
            while ($row = $this->db->fetch_array($query)) {
                $username = htmlspecialchars_uni($row['username']);
                $email = htmlspecialchars_uni($row['email']);
                $ip_address = $row['ipaddress'];

                $dateline = '';
                if ($row['dateline'] > 0) {
                    $dateline = $this->time->formatDate('relative', $row['dateline']);
                }

                $confidence = '0%';
                $data = @my_unserialize($row['data']);
                if (is_array($data) && !empty($data)) {
                    if (isset($data['confidence'])) {
                        $confidence = (double)$data['confidence'] . '%';
                    }
                }

                $table->construct_cell($username);
                $table->construct_cell($email);
                $table->construct_cell($ip_address);
                $table->construct_cell($dateline);
                $table->construct_cell($confidence);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_spam_logs, ['colspan' => '5']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->spam_logs);

            // Do we need to construct the pagination?
            if ($rescount > $perpage) {
                $html .= $this->adm->draw_admin_pagination($pagecnt, $perpage, $rescount, $this->bb->admin_url . "/tools/spamlog?perpage={$perpage}{$additional_criteria}&amp;sortby={$this->bb->input['sortby']}&amp;order={$order}") . '<br />';
            }

            // Fetch filter options
            $sortbysel[$this->bb->input['sortby']] = 'selected="selected"';
            $ordersel[$this->bb->input['order']] = 'selected="selected"';

            $sort_by = [
                'dateline' => $this->lang->spam_date,
                'username' => $this->lang->spam_username,
                'email' => $this->lang->spam_email,
                'ipaddress' => $this->lang->spam_ip,
            ];

            $order_array = [
                'asc' => $this->lang->asc,
                'desc' => $this->lang->desc
            ];

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/spamlog', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->filter_spam_logs);
            $form_container->output_row($this->lang->spam_username, '', $form->generate_text_box('username', $this->bb->input['username'], ['id' => 'username']), 'suername');
            $form_container->output_row($this->lang->spam_email, '', $form->generate_text_box('email', $this->bb->input['email'], ['id' => 'email']), 'email');
            $form_container->output_row($this->lang->spam_ip, '', $form->generate_text_box('ipaddress', $this->bb->input['ipaddress'], ['id' => 'ipaddress']), 'ipaddress');
            $form_container->output_row($this->lang->sort_by, '', $form->generate_select_box('sortby', $sort_by, $this->bb->input['sortby'], ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('order', $order_array, $order, ['id' => 'order']) . " {$this->lang->order}", 'order');
            $form_container->output_row($this->lang->results_per_page, '', $form->generate_numeric_field('perpage', $perpage, ['id' => 'perpage', 'min' => 1]), 'perpage');

            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->filter_spam_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/SpamLog.html.twig');
        }
    }
}
