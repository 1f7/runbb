<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunCMF\Core\AbstractController;

class Statistics extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_statistics', false, true);

        if ($this->bb->input['action'] == 'do_graph') {
            $range = [
                'start' => $this->bb->getInput('start', 0),
                'end' => $this->bb->getInput('end', 0)
            ];
            $this->create_graph($this->bb->input['type'], $range);
            die;
        }

        $this->page->add_breadcrumb_item($this->lang->statistics, $this->bb->admin_url . '/tools/statistics');

        $sub_tabs['overall_statistics'] = [
            'title' => $this->lang->overall_statistics,
            'link' => $this->bb->admin_url . '/tools/statistics',
            'description' => $this->lang->overall_statistics_desc
        ];

        $this->plugins->runHooks('admin_tools_statistics_begin');

        $query = $this->db->simple_select('stats', 'COUNT(*) as total');
        if ($this->db->fetch_field($query, 'total') == 0) {
            $this->session->flash_message($this->lang->error_no_statistics_available_yet, 'error');
            return $response->withRedirect($this->bb->admin_url . '/tools');
        }

        $per_page = 20;

        $this->plugins->runHooks('admin_tools_statistics_overall_begin');

        // Do we have date range criteria?
        if (isset($this->bb->input['from_year'])) {
            $start_dateline = mktime(0, 0, 0, $this->bb->getInput('from_month', 0), $this->bb->getInput('from_day', 0), $this->bb->getInput('from_year', 0));
            $end_dateline = mktime(23, 59, 59, $this->bb->getInput('to_month', 0), $this->bb->getInput('to_day', 0), $this->bb->getInput('to_year', 0));
            $range = "&amp;start={$start_dateline}&amp;end={$end_dateline}";
        }

        // Otherwise default to the last 30 days
        if (!isset($this->bb->input['from_year']) || $start_dateline > TIME_NOW || $end_dateline > mktime(23, 59, 59)) {
            $start_dateline = TIME_NOW - (60 * 60 * 24 * 30);
            $end_dateline = TIME_NOW;

            list($this->bb->input['from_day'], $this->bb->input['from_month'], $this->bb->input['from_year']) = explode('-', date('j-n-Y', $start_dateline));
            list($this->bb->input['to_day'], $this->bb->input['to_month'], $this->bb->input['to_year']) = explode('-', date('j-n-Y', $end_dateline));

            $range = "&amp;start={$start_dateline}&amp;end={$end_dateline}";
        }

        $last_dateline = 0;

        if (isset($this->bb->input['page']) && $this->bb->input['page'] > 1) {
            $this->bb->input['page'] = $this->bb->getInput('page', 0);
            $start = ($this->bb->input['page'] * $per_page) - $per_page;
        } else {
            $this->bb->input['page'] = 1;
            $start = 0;
        }

        $query = $this->db->simple_select('stats', '*', "dateline >= '" . (int)$start_dateline . "' AND dateline <= '" . (int)$end_dateline . "'", ['order_by' => 'dateline', 'order_dir' => 'asc']);

        $stats = [];
        while ($stat = $this->db->fetch_array($query)) {
            if ($last_dateline) {
                $stat['change_users'] = ($stat['numusers'] - $stats[$last_dateline]['numusers']);
                $stat['change_threads'] = ($stat['numthreads'] - $stats[$last_dateline]['numthreads']);
                $stat['change_posts'] = ($stat['numposts'] - $stats[$last_dateline]['numposts']);
            }

            $stats[$stat['dateline']] = $stat;

            $last_dateline = $stat['dateline'];
        }

        if (empty($stats)) {
            $this->session->flash_message($this->lang->error_no_results_found_for_criteria, 'error');
            return $response->withRedirect($this->bb->admin_url . '/tools');
        }

        krsort($stats, SORT_NUMERIC);

        $this->page->add_breadcrumb_item($this->lang->overall_statistics, $this->bb->admin_url . '/tools/statistics');

        $html = $this->page->output_header($this->lang->statistics . ' - ' . $this->lang->overall_statistics);

        $html .= $this->page->output_nav_tabs($sub_tabs, 'overall_statistics');

        // Date range fields
        $form = new Form($this->bb, $this->bb->admin_url . '/tools/statistics', 'post', 'overall');
        $html .= $form->getForm();
        $html .= "<fieldset><legend>{$this->lang->date_range}</legend>\n";
        $html .= "{$this->lang->from} " . $form->generate_date_select('from', $this->bb->input['from_day'], $this->bb->input['from_month'], $this->bb->input['from_year']);
        $html .= " {$this->lang->to} " . $form->generate_date_select('to', $this->bb->input['to_day'], $this->bb->input['to_month'], $this->bb->input['to_year']);
        $html .= ' ' . $form->generate_submit_button($this->lang->view);
        $html .= "</fieldset>\n";
        $html .= $form->end();

        $html .= "<fieldset><legend>{$this->lang->users}</legend>\n";
        $html .= "<img src=\"".$this->bb->admin_url."/tools/statistics?action=do_graph&amp;type=users{$range}\" />\n";
        $html .= "</fieldset>\n";

        $html .= "<fieldset><legend>{$this->lang->threads}</legend>\n";
        $html .= "<img src=\"".$this->bb->admin_url."/tools/statistics?action=do_graph&amp;type=threads{$range}\" />\n";
        $html .= "</fieldset>\n";

        $html .= "<fieldset><legend>{$this->lang->posts}</legend>\n";
        $html .= "<img src=\"".$this->bb->admin_url."/tools/statistics?action=do_graph&amp;type=posts{$range}\" />\n";
        $html .= "</fieldset>\n";

        $total_rows = count($stats);

        $table = new Table;
        $table->construct_header($this->lang->date);
        $table->construct_header($this->lang->users);
        $table->construct_header($this->lang->threads);
        $table->construct_header($this->lang->posts);
        $query = $this->db->simple_select('stats', '*', "dateline >= '" . (int)$start_dateline . "' AND dateline <= '" . (int)$end_dateline . "'", ['order_by' => 'dateline', 'order_dir' => 'desc', 'limit_start' => $start, 'limit' => $per_page]);
        while ($stat = $this->db->fetch_array($query)) {
            $table->construct_cell('<strong>' . date($this->bb->settings['dateformat'], $stat['dateline']) . '</strong>');
            $table->construct_cell($this->bb->my_number_format($stat['numusers']) . ' <small>' . $this->generate_growth_string($stats[$stat['dateline']]['change_users']) . '</small>');
            $table->construct_cell($this->bb->my_number_format($stat['numthreads']) . ' <small>' . $this->generate_growth_string($stats[$stat['dateline']]['change_threads']) . '</small>');
            $table->construct_cell($this->bb->my_number_format($stat['numposts']) . ' <small>' . $this->generate_growth_string($stats[$stat['dateline']]['change_posts']) . '</small>');
            $table->construct_row();
        }
        $html .= $table->output($this->lang->overall_statistics);

        $url_range = '&from_month=' . $this->bb->getInput('from_month', 0) . '&from_day=' . $this->bb->getInput('from_day', 0) . '&from_year=' . $this->bb->getInput('from_year', 0);
        $url_range .= '&to_month=' . $this->bb->getInput('to_month', 0) . '&to_day=' . $this->bb->getInput('to_day', 0) . '&to_year=' . $this->bb->getInput('to_year', 0);

        $html .= $this->adm->draw_admin_pagination($this->bb->input['page'], $per_page, $total_rows, $this->bb->admin_url . '/tools/statistics' . $url_range . '&page={page}');

        $this->page->output_footer();

        $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
        return $this->view->render($response, '@forum/Admin/Tools/Statistics.html.twig');
    }

    /**
     * @param int $number
     *
     * @return string
     */
    private function generate_growth_string($number)
    {
        if ($number === null) {
            return '';
        }

        $number = (int)$number;
        $friendly_number = $this->bb->my_number_format(abs($number));

        if ($number > 0) {
            $growth_string = "(<img src=\"{$this->bb->asset_url}/admin/styles/{$this->bb->cp_style}/images/icons/increase.png\" alt=\"{$this->lang->increase}\" title=\"{$this->lang->increase}\" style=\"vertical-align: middle; margin-top: -2px;\" /> {$friendly_number})";
        } elseif ($number == 0) {
            $growth_string = "(<img src=\"{$this->bb->asset_url}/admin/styles/{$this->bb->cp_style}/images/icons/no_change.png\" alt=\"{$this->lang->no_change}\" title=\"{$this->lang->no_change}\" style=\"vertical-align: middle; margin-top: -2px;\" /> {$friendly_number})";
        } else {
            $growth_string = "(<img src=\"{$this->bb->asset_url}/admin/styles/{$this->bb->cp_style}/images/icons/decrease.png\" alt=\"{$this->lang->decrease}\" title=\"{$this->lang->decrease}\" style=\"vertical-align: middle; margin-top: -2px;\" /> {$friendly_number})";
        }

        return $growth_string;
    }

    /**
     * @param string $type users, threads, posts
     * @param array $range
     */
    private function create_graph($type, $range = null)
    {
        // Do we have date range criteria?
        if ($range['end'] || $range['start']) {
            $start = (int)$range['start'];
            $end = (int)$range['end'];
        } // Otherwise default to the last 30 days
        else {
            $start = TIME_NOW - (60 * 60 * 24 * 30);
            $end = TIME_NOW;
        }

        $allowed_types = ['users', 'threads', 'posts'];
        if (!in_array($type, $allowed_types)) {
            die;
        }

        $points = $stats = $datelines = [];
        if ($start == 0) {
            $query = $this->db->simple_select('stats', "dateline,num{$type}", "dateline <= '" . (int)$end . "'", ['order_by' => 'dateline', 'order_dir' => 'desc', 'limit' => 2]);
            while ($stat = $this->db->fetch_array($query)) {
                $stats[] = $stat['num' . $type];
                $datelines[] = $stat['dateline'];
                $x_labels[] = date('m/j', $stat['dateline']);
            }
            $points[$datelines[0]] = 0;
            $points[$datelines[1]] = $stats[0] - $stats[1];
            ksort($points, SORT_NUMERIC);
        } elseif ($end == 0) {
            $query = $this->db->simple_select('stats', "dateline,num{$type}", "dateline >= '" . (int)$start . "'", ['order_by' => 'dateline', 'order_dir' => 'asc', 'limit' => 2]);
            while ($stat = $this->db->fetch_array($query)) {
                $stats[] = $stat['num' . $type];
                $datelines[] = $stat['dateline'];
                $x_labels[] = date('m/j', $stat['dateline']);
            }
            $points[$datelines[0]] = 0;
            $points[$datelines[1]] = $stats[1] - $stats[0];
            ksort($points, SORT_NUMERIC);
        } else {
            $query = $this->db->simple_select('stats', "dateline,num{$type}", "dateline >= '" . (int)$start . "' AND dateline <= '" . (int)$end . "'", ['order_by' => 'dateline', 'order_dir' => 'asc']);
            while ($stat = $this->db->fetch_array($query)) {
                $points[$stat['dateline']] = $stat['num' . $type];
                $datelines[] = $stat['dateline'];
                $x_labels[] = date('m/j', $stat['dateline']);
            }
        }

        sort($datelines, SORT_NUMERIC);

        // Find our year(s) label
        $start_year = date('Y', $datelines[0]);
        $last_year = date('Y', $datelines[count($datelines) - 1]);
        if (($last_year - $start_year) == 0) {
            $bottom_label = $start_year;
        } else {
            $bottom_label = $start_year . ' - ' . $last_year;
        }

        // Create the graph outline
        $graph = new \RunBB\Core\Graph($this->bb);
        $graph->add_points(array_values($points));
        $graph->add_x_labels($x_labels);
        $graph->set_bottom_label($bottom_label);
        $graph->render();
        $graph->output();
    }
}
