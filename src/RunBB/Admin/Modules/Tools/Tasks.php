<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class Tasks extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_tasks', false, true);

        $this->page->add_breadcrumb_item($this->lang->task_manager, $this->bb->admin_url . '/tools/tasks');
        $this->plugins->runHooks('admin_tools_tasks_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_tools_tasks_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['description'])) {
                    $errors[] = $this->lang->error_missing_description;
                }

                if (!file_exists(MYBB_ROOT . 'Tasks/' . $this->bb->input['file'] . '.php')) {
                    $errors[] = $this->lang->error_invalid_task_file;
                }

                $this->bb->input['minute'] = $this->check_time_values($this->bb->input['minute'], 0, 59, 'string');
                if ($this->bb->input['minute'] === false) {
                    $errors[] = $this->lang->error_invalid_minute;
                }

                $this->bb->input['hour'] = $this->check_time_values($this->bb->input['hour'], 0, 59, 'string');
                if ($this->bb->input['hour'] === false) {
                    $errors[] = $this->lang->error_invalid_hour;
                }

                if ($this->bb->input['day'] != '*' && $this->bb->input['day'] != '') {
                    $this->bb->input['day'] = $this->check_time_values($this->bb->input['day'], 1, 31, 'string');
                    if ($this->bb->input['day'] === false) {
                        $errors[] = $this->lang->error_invalid_day;
                    }
                    $this->bb->input['weekday'] = ['*'];
                } else {
                    $this->bb->input['weekday'] = $this->check_time_values($this->bb->input['weekday'], 0, 6, 'array');
                    if ($this->bb->input['weekday'] === false) {
                        $errors[] = $this->lang->error_invalid_weekday;
                    }
                    $this->bb->input['day'] = '*';
                }

                $this->bb->input['month'] = $this->check_time_values($this->bb->input['month'], 1, 12, 'array');
                if ($this->bb->input['month'] === false) {
                    $errors[] = $this->lang->error_invalid_month;
                }

                if (!isset($errors)) {
                    $new_task = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'file' => $this->db->escape_string($this->bb->input['file']),
                        'minute' => $this->db->escape_string($this->bb->input['minute']),
                        'hour' => $this->db->escape_string($this->bb->input['hour']),
                        'day' => $this->db->escape_string($this->bb->input['day']),
                        'month' => $this->db->escape_string(implode(',', $this->bb->input['month'])),
                        'weekday' => $this->db->escape_string(implode(',', $this->bb->input['weekday'])),
                        'enabled' => $this->bb->getInput('enabled', 0),
                        'logging' => $this->bb->getInput('logging', 0)
                    ];

                    $new_task['nextrun'] = $this->task->fetch_next_run($new_task);
                    $tid = $this->db->insert_query('tasks', $new_task);

                    $this->plugins->runHooks('admin_tools_tasks_add_commit');

                    $this->cache->update_tasks();

                    // Log admin action
                    $this->bblogger->log_admin_action($tid, htmlspecialchars_uni($this->bb->input['title']));

                    $this->session->flash_message($this->lang->success_task_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
                }
            }
            $this->page->add_breadcrumb_item($this->lang->add_new_task);
            $html = $this->page->output_header($this->lang->scheduled_tasks . ' - ' . $this->lang->add_new_task);


            $sub_tabs['scheduled_tasks'] = [
                'title' => $this->lang->scheduled_tasks,
                'link' => $this->bb->admin_url . '/tools/tasks'
            ];

            $sub_tabs['add_task'] = [
                'title' => $this->lang->add_new_task,
                'link' => $this->bb->admin_url . '/tools/tasks?action=add',
                'description' => $this->lang->add_new_task_desc
            ];

            $sub_tabs['task_logs'] = [
                'title' => $this->lang->view_task_logs,
                'link' => $this->bb->admin_url . '/tools/tasks?action=logs'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_task');
            $form = new Form($this->bb, $this->bb->admin_url . '/tools/tasks?action=add', 'post', 'add');
            $html .= $form->getForm();
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['minute'] = '*';
                $this->bb->input['hour'] = '*';
                $this->bb->input['day'] = '*';
                $this->bb->input['weekday'] = '*';
                $this->bb->input['month'] = '*';
            }
            $form_container = new FormContainer($this, $this->lang->add_new_task);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');

            $task_list = [];
            $task_files = scandir(MYBB_ROOT . 'Tasks/');
            foreach ($task_files as $task_file) {
                if (is_file(MYBB_ROOT . "Tasks/{$task_file}") && get_extension($task_file) == 'php') {
                    $file_id = preg_replace("#\." . get_extension($task_file) . "$#i", "$1", $task_file);
                    $task_list[$file_id] = $task_file;
                }
            }
            $form_container->output_row($this->lang->task_file . ' <em>*</em>', $this->lang->task_file_desc, $form->generate_select_box('file', $task_list, $this->bb->getInput('file', ''), ['id' => 'file']), 'file');
            $form_container->output_row($this->lang->time_minutes, $this->lang->time_minutes_desc, $form->generate_text_box('minute', $this->bb->input['minute'], ['id' => 'minute']), 'minute');
            $form_container->output_row($this->lang->time_hours, $this->lang->time_hours_desc, $form->generate_text_box('hour', $this->bb->input['hour'], ['id' => 'hour']), 'hour');
            $form_container->output_row($this->lang->time_days_of_month, $this->lang->time_days_of_month_desc, $form->generate_text_box('day', $this->bb->input['day'], ['id' => 'day']), 'day');

            $options = [
                '*' => $this->lang->every_weekday,
                '0' => $this->lang->sunday,
                '1' => $this->lang->monday,
                '2' => $this->lang->tuesday,
                '3' => $this->lang->wednesday,
                '4' => $this->lang->thursday,
                '5' => $this->lang->friday,
                '6' => $this->lang->saturday
            ];
            $form_container->output_row($this->lang->time_weekdays, $this->lang->time_weekdays_desc, $form->generate_select_box('weekday[]', $options, $this->bb->input['weekday'], ['id' => 'weekday', 'multiple' => true, 'size' => 8]), 'weekday');

            $options = [
                '*' => $this->lang->every_month,
                '1' => $this->lang->january,
                '2' => $this->lang->february,
                '3' => $this->lang->march,
                '4' => $this->lang->april,
                '5' => $this->lang->may,
                '6' => $this->lang->june,
                '7' => $this->lang->july,
                '8' => $this->lang->august,
                '9' => $this->lang->september,
                '10' => $this->lang->october,
                '11' => $this->lang->november,
                '12' => $this->lang->december
            ];
            $form_container->output_row($this->lang->time_months, $this->lang->time_months_desc, $form->generate_select_box('month[]', $options, $this->bb->input['month'], ['id' => 'month', 'multiple' => true, 'size' => 13]), 'month');
            $form_container->output_row($this->lang->enable_logging . ' <em>*</em>', '', $form->generate_yes_no_radio('logging', $this->bb->getInput('logging', ''), true));
            $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->getInput('enabled', ''), true));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_task);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/TasksAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('tasks', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $task = $this->db->fetch_array($query);

            // Does the task not exist?
            if (!$task['tid']) {
                $this->session->flash_message($this->lang->error_invalid_task, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            $this->plugins->runHooks('admin_tools_tasks_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!trim($this->bb->input['description'])) {
                    $errors[] = $this->lang->error_missing_description;
                }

                if (!file_exists(MYBB_ROOT . 'Tasks/' . $this->bb->input['file'] . '.php')) {
                    $errors[] = $this->lang->error_invalid_task_file;
                }

                $this->bb->input['minute'] = $this->check_time_values($this->bb->input['minute'], 0, 59, 'string');
                if ($this->bb->input['minute'] === false) {
                    $errors[] = $this->lang->error_invalid_minute;
                }

                $this->bb->input['hour'] = $this->check_time_values($this->bb->input['hour'], 0, 59, 'string');
                if ($this->bb->input['hour'] === false) {
                    $errors[] = $this->lang->error_invalid_hour;
                }

                if ($this->bb->input['day'] != '*' && $this->bb->input['day'] != '') {
                    $this->bb->input['day'] = $this->check_time_values($this->bb->input['day'], 1, 31, 'string');
                    if ($this->bb->input['day'] === false) {
                        $errors[] = $this->lang->error_invalid_day;
                    }
                    $this->bb->input['weekday'] = ['*'];
                } else {
                    $this->bb->input['weekday'] = $this->check_time_values($this->bb->input['weekday'], 0, 6, 'array');
                    if ($this->bb->input['weekday'] === false) {
                        $errors[] = $this->lang->error_invalid_weekday;
                    }
                    $this->bb->input['day'] = '*';
                }

                $this->bb->input['month'] = $this->check_time_values($this->bb->input['month'], 1, 12, 'array');
                if ($this->bb->input['month'] === false) {
                    $errors[] = $this->lang->error_invalid_month;
                }

                if (!isset($errors)) {
                    $enable_confirmation = false;
                    // Check if we need to ask the user to confirm turning on the task
                    if (($task['file'] == 'backupdb' ||
                            $task['file'] == 'checktables') &&
                        $task['enabled'] == 0 &&
                        $this->bb->input['enabled'] == 1
                    ) {
                        $this->bb->input['enabled'] = 0;
                        $enable_confirmation = true;
                    }

                    $updated_task = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'file' => $this->db->escape_string($this->bb->input['file']),
                        'minute' => $this->db->escape_string($this->bb->input['minute']),
                        'hour' => $this->db->escape_string($this->bb->input['hour']),
                        'day' => $this->db->escape_string($this->bb->input['day']),
                        'month' => $this->db->escape_string(implode(',', $this->bb->input['month'])),
                        'weekday' => $this->db->escape_string(implode(',', $this->bb->input['weekday'])),
                        'enabled' => $this->bb->getInput('enabled', 0),
                        'logging' => $this->bb->getInput('logging', 0)
                    ];

                    $updated_task['nextrun'] = $this->task->fetch_next_run($updated_task);

                    $this->plugins->runHooks('admin_tools_tasks_edit_commit');

                    $this->db->update_query('tasks', $updated_task, "tid='{$task['tid']}'");

                    $this->cache->update_tasks();

                    // Log admin action
                    $this->bblogger->log_admin_action($task['tid'], htmlspecialchars_uni($this->bb->input['title']));

                    $this->session->flash_message($this->lang->success_task_updated, 'success');

                    if ($enable_confirmation == true) {
                        return $response->withRedirect($this->bb->admin_url . "/tools/tasks?action=enable&amp;tid={$task['tid']}&amp;my_post_key={$this->bb->post_code}");
                    } else {
                        return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
                    }
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_task);
            $html = $this->page->output_header($this->lang->scheduled_tasks . ' - ' . $this->lang->edit_task);

            $sub_tabs['edit_task'] = [
                'title' => $this->lang->edit_task,
                'description' => $this->lang->edit_task_desc,
                'link' => $this->bb->admin_url . '/tools/tasks?action=edit&tid=' . $task['tid']
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_task');

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/tasks?action=edit', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
                $task_data = $this->bb->input;
            } else {
                $task_data = $task;
                $task_data['weekday'] = explode(',', $task['weekday']);
                $task_data['month'] = explode(',', $task['month']);
            }

            $form_container = new FormContainer($this, $this->lang->edit_task);
            $html .= $form->generate_hidden_field('tid', $task['tid']);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $task_data['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description . ' <em>*</em>', '', $form->generate_text_box('description', $task_data['description'], ['id' => 'description']), 'description');

            $task_list = [];
            $task_files = scandir(MYBB_ROOT . 'Tasks/');
            foreach ($task_files as $task_file) {
                if (is_file(MYBB_ROOT . "Tasks/{$task_file}") && get_extension($task_file) == 'php') {
                    $file_id = preg_replace("#\." . get_extension($task_file) . "$#i", "$1", $task_file);
                    $task_list[$file_id] = $task_file;
                }
            }
            $form_container->output_row($this->lang->task . ' <em>*</em>', $this->lang->task_file_desc, $form->generate_select_box('file', $task_list, $task_data['file'], ['id' => 'file']), 'file');
            $form_container->output_row($this->lang->time_minutes, $this->lang->time_minutes_desc, $form->generate_text_box('minute', $task_data['minute'], ['id' => 'minute']), 'minute');
            $form_container->output_row($this->lang->time_hours, $this->lang->time_hours_desc, $form->generate_text_box('hour', $task_data['hour'], ['id' => 'hour']), 'hour');
            $form_container->output_row($this->lang->time_days_of_month, $this->lang->time_days_of_month_desc, $form->generate_text_box('day', $task_data['day'], ['id' => 'day']), 'day');

            $options = [
                '*' => $this->lang->every_weekday,
                '0' => $this->lang->sunday,
                '1' => $this->lang->monday,
                '2' => $this->lang->tuesday,
                '3' => $this->lang->wednesday,
                '4' => $this->lang->thursday,
                '5' => $this->lang->friday,
                '6' => $this->lang->saturday
            ];
            $form_container->output_row($this->lang->time_weekdays, $this->lang->time_weekdays_desc, $form->generate_select_box('weekday[]', $options, $task_data['weekday'], ['id' => 'weekday', 'multiple' => true]), 'weekday');

            $options = [
                '*' => $this->lang->every_month,
                '1' => $this->lang->january,
                '2' => $this->lang->february,
                '3' => $this->lang->march,
                '4' => $this->lang->april,
                '5' => $this->lang->may,
                '6' => $this->lang->june,
                '7' => $this->lang->july,
                '8' => $this->lang->august,
                '9' => $this->lang->september,
                '10' => $this->lang->october,
                '11' => $this->lang->november,
                '12' => $this->lang->december
            ];
            $form_container->output_row($this->lang->time_months, $this->lang->time_months_desc, $form->generate_select_box('month[]', $options, $task_data['month'], ['id' => 'month', 'multiple' => true]), 'month');
            $form_container->output_row($this->lang->enable_logging . ' <em>*</em>', '', $form->generate_yes_no_radio('logging', $task_data['logging'], true));
            $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $task_data['enabled'], true));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_task);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/TasksEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('tasks', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $task = $this->db->fetch_array($query);

            // Does the task not exist?
            if (!$task['tid']) {
                $this->session->flash_message($this->lang->error_invalid_task, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            $this->plugins->runHooks('admin_tools_tasks_delete');

            if ($this->bb->request_method == 'post') {
                // Delete the task & any associated task log entries
                $this->db->delete_query('tasks', "tid='{$task['tid']}'");
                $this->db->delete_query('tasklog', "tid='{$task['tid']}'");

                // Fetch next task run

                $this->plugins->runHooks('admin_tools_tasks_delete_commit');

                $this->cache->update_tasks();

                // Log admin action
                $this->bblogger->log_admin_action($task['tid'], htmlspecialchars_uni($task['title']));

                $this->session->flash_message($this->lang->success_task_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/tools/tasks?action=delete&tid=' . $task['tid'], $this->lang->confirm_task_deletion);
            }
        }

        if ($this->bb->input['action'] == 'enable' || $this->bb->input['action'] == 'disable') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            $query = $this->db->simple_select('tasks', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $task = $this->db->fetch_array($query);

            // Does the task not exist?
            if (!$task['tid']) {
                $this->session->flash_message($this->lang->error_invalid_task, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            if ($this->bb->input['action'] == 'enable') {
                $this->plugins->runHooks('admin_tools_tasks_enable');
            } else {
                $this->plugins->runHooks('admin_tools_tasks_disable');
            }

            if ($this->bb->input['action'] == 'enable') {
                if ($task['file'] == 'backupdb' || $task['file'] == 'checktables') {
                    // User clicked no
                    if ($this->bb->input['no']) {
                        return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
                    }

                    if ($this->bb->request_method == 'post') {
                        $nextrun = $this->task->fetch_next_run($task);
                        $this->db->update_query('tasks', ['nextrun' => $nextrun, 'enabled' => 1], "tid='{$task['tid']}'");

                        $this->plugins->runHooks('admin_tools_tasks_enable_commit');

                        $this->cache->update_tasks();

                        // Log admin action
                        $this->bblogger->log_admin_action($task['tid'], htmlspecialchars_uni($task['title']), $this->bb->input['action']);

                        $this->session->flash_message($this->lang->success_task_enabled, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
                    } else {
                        $this->page->output_confirm_action($this->bb->admin_url . '/tools/tasks?action=enable&tid=' . $task['tid'], $this->lang->confirm_task_enable);
                    }
                } else {
                    $nextrun = $this->task->fetch_next_run($task);
                    $this->db->update_query('tasks', ['nextrun' => $nextrun, 'enabled' => 1], "tid='{$task['tid']}'");

                    $this->plugins->runHooks('admin_tools_tasks_enable_commit');

                    $this->cache->update_tasks();

                    // Log admin action
                    $this->bblogger->log_admin_action($task['tid'], htmlspecialchars_uni($task['title']), $this->bb->input['action']);

                    $this->session->flash_message($this->lang->success_task_enabled, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
                }
            } else {
                $this->db->update_query('tasks', ['enabled' => 0], "tid='{$task['tid']}'");

                $this->plugins->runHooks('admin_tools_tasks_disable_commit');

                $this->cache->update_tasks();

                // Log admin action
                $this->bblogger->log_admin_action($task['tid'], htmlspecialchars_uni($task['title']), htmlspecialchars_uni($this->bb->input['action']));

                $this->session->flash_message($this->lang->success_task_disabled, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }
        }

        if ($this->bb->input['action'] == 'run') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            ignore_user_abort(true);
            @set_time_limit(0);

            $this->plugins->runHooks('admin_tools_tasks_run');

            $query = $this->db->simple_select('tasks', '*', "tid='" . $this->bb->getInput('tid', 0) . "'");
            $task = $this->db->fetch_array($query);

            // Does the task not exist?
            if (!$task['tid']) {
                $this->session->flash_message($this->lang->error_invalid_task, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
            }

            $this->task->run_task($task['tid']);

            $this->plugins->runHooks('admin_tools_tasks_run_commit');

            // Log admin action
            $this->bblogger->log_admin_action($task['tid'], htmlspecialchars_uni($task['title']));

            $this->session->flash_message($this->lang->success_task_run, 'success');
            return $response->withRedirect($this->bb->admin_url . '/tools/tasks');
        }

        if ($this->bb->input['action'] == 'logs') {
            $this->plugins->runHooks('admin_tools_tasks_logs');

            $html = $this->page->output_header($this->lang->task_logs);

            $sub_tabs['scheduled_tasks'] = [
                'title' => $this->lang->scheduled_tasks,
                'link' => $this->bb->admin_url . '/tools/tasks'
            ];

            $sub_tabs['add_task'] = [
                'title' => $this->lang->add_new_task,
                'link' => $this->bb->admin_url . '/tools/tasks?action=add'
            ];

            $sub_tabs['task_logs'] = [
                'title' => $this->lang->view_task_logs,
                'link' => $this->bb->admin_url . '/tools/tasks?action=logs',
                'description' => $this->lang->view_task_logs_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'task_logs');

            $table = new Table;
            $table->construct_header($this->lang->task);
            $table->construct_header($this->lang->date, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->data, ['width' => '60%']);

            $query = $this->db->simple_select('tasklog', 'COUNT(*) AS log_count');
            $log_count = $this->db->fetch_field($query, 'log_count');

            $start = 0;
            $per_page = 50;
            $current_page = 1;

            if (isset($this->bb->input['page']) && $this->bb->input['page'] > 0) {
                $current_page = $this->bb->getInput('page', 0);
                $start = ($current_page - 1) * $per_page;
                $pages = $log_count / $per_page;
                $pages = ceil($pages);
                if ($current_page > $pages) {
                    $start = 0;
                    $current_page = 1;
                }
            }

            $pagination = $this->adm->draw_admin_pagination($current_page, $per_page, $log_count, $this->bb->admin_url . '/tools/tasks?action=logs&amp;page={page}');

            $query = $this->db->query('
		SELECT l.*, t.title
		FROM ' . TABLE_PREFIX . 'tasklog l
		LEFT JOIN ' . TABLE_PREFIX . "tasks t ON (t.tid=l.tid)
		ORDER BY l.dateline DESC
		LIMIT {$start}, {$per_page}
	");
            while ($log_entry = $this->db->fetch_array($query)) {
                $log_entry['title'] = htmlspecialchars_uni($log_entry['title']);
                $log_entry['data'] = htmlspecialchars_uni($log_entry['data']);

                $date = $this->time->formatDate('relative', $log_entry['dateline']);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/tasks?action=edit&amp;tid={$log_entry['tid']}\">{$log_entry['title']}</a>");
                $table->construct_cell($date, ['class' => 'align_center']);
                $table->construct_cell($log_entry['data']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_task_logs, ['colspan' => '3']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->task_logs);
            $html .= $pagination;

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/TasksLogs.html.twig');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->task_manager);

            $sub_tabs['scheduled_tasks'] = [
                'title' => $this->lang->scheduled_tasks,
                'link' => $this->bb->admin_url . '/tools/tasks',
                'description' => $this->lang->scheduled_tasks_desc
            ];

            $sub_tabs['add_task'] = [
                'title' => $this->lang->add_new_task,
                'link' => $this->bb->admin_url . '/tools/tasks?action=add'
            ];

            $sub_tabs['task_logs'] = [
                'title' => $this->lang->view_task_logs,
                'link' => $this->bb->admin_url . '/tools/tasks?action=logs'
            ];

            $this->plugins->runHooks('admin_tools_tasks_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'scheduled_tasks');

            $table = new Table;
            $table->construct_header($this->lang->task);
            $table->construct_header($this->lang->next_run, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            $query = $this->db->simple_select('tasks', '*', '', ['order_by' => 'title', 'order_dir' => 'asc']);
            while ($task = $this->db->fetch_array($query)) {
                $task['title'] = htmlspecialchars_uni($task['title']);
                $task['description'] = htmlspecialchars_uni($task['description']);
                $next_run = date($this->bb->settings['dateformat'], $task['nextrun']) . ', ' . date($this->bb->settings['timeformat'], $task['nextrun']);
                if ($task['enabled'] == 1) {
                    $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/bullet_on.png\" alt=\"({$this->lang->alt_enabled})\" title=\"{$this->lang->alt_enabled}\"  style=\"vertical-align: middle;\" /> ";
                } else {
                    $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/bullet_off.png\" alt=\"({$this->lang->alt_disabled})\" title=\"{$this->lang->alt_disabled}\"  style=\"vertical-align: middle;\" /> ";
                }
                $table->construct_cell("<div class=\"float_right\"><a href=\"".$this->bb->admin_url."/tools/tasks?action=run&amp;tid={$task['tid']}&amp;my_post_key={$this->bb->post_code}\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/run_task.png\" title=\"{$this->lang->run_task_now}\" alt=\"{$this->lang->run_task}\" /></a></div><div>{$icon}<strong><a href=\"".$this->bb->admin_url."/tools/tasks?action=edit&amp;tid={$task['tid']}\">{$task['title']}</a></strong><br /><small>{$task['description']}</small></div>");
                $table->construct_cell($next_run, ['class' => 'align_center']);

                $popup = new PopupMenu("task_{$task['tid']}", $this->lang->options);
                $popup->add_item($this->lang->edit_task, $this->bb->admin_url . "/tools/tasks?action=edit&amp;tid={$task['tid']}");
                if ($task['enabled'] == 1) {
                    $popup->add_item($this->lang->run_task, $this->bb->admin_url . "/tools/tasks?action=run&amp;tid={$task['tid']}&amp;my_post_key={$this->bb->post_code}");
                    $popup->add_item($this->lang->disable_task, $this->bb->admin_url . "/tools/tasks?action=disable&amp;tid={$task['tid']}&amp;my_post_key={$this->bb->post_code}");
                } else {
                    $popup->add_item($this->lang->enable_task, $this->bb->admin_url . "/tools/tasks?action=enable&amp;tid={$task['tid']}&amp;my_post_key={$this->bb->post_code}");
                }
                $popup->add_item($this->lang->delete_task, $this->bb->admin_url . "/tools/tasks?action=delete&amp;tid={$task['tid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_task_deletion}')");
                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_tasks, ['colspan' => 3]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->scheduled_tasks);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/Tasks.html.twig');
        }
    }

    /**
     * Validates a string or array of values
     *
     * @param string|array $value Comma-separated list or array of values
     * @param int $min Minimum value
     * @param int $max Maximum value
     * @param string $return_type Set 'string' to return in a comma-separated list, or 'array' to return in an array
     * @return string|array String or array of valid values OR false if string/array is invalid
     */
    private function check_time_values($value, $min, $max, $return_type)
    {
        // If the values aren't in an array form, make them into an array
        if (!is_array($value)) {
            // Empty value == *
            if ($value === '') {
                return ($return_type == 'string') ? '*' : ['*'];
            }
            $implode = 1;
            $value = explode(',', $value);
        }
        // If * is in the array, always return with * because it overrides all
        if (in_array('*', $value)) {
            return ($return_type == 'string') ? '*' : ['*'];
        }
        // Validate each value in array
        foreach ($value as $time) {
            if ($time < $min || $time > $max) {
                return false;
            }
        }
        // Return based on return type
        if ($return_type == 'string') {
            $value = implode(',', $value);
        }
        return $value;
    }
}
