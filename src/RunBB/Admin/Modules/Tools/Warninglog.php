<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Warninglog extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_warninglog', false, true);

        $this->page->add_breadcrumb_item($this->lang->warning_logs, $this->bb->admin_url . '/tools/warninglog');

        $this->plugins->runHooks('admin_tools_warninglog_begin');

        // Revoke a warning
        if ($this->bb->input['action'] == 'do_revoke' && $this->bb->request_method == 'post') {
            $query = $this->db->simple_select('warnings', '*', "wid='" . $this->bb->getInput('wid', 0) . "'");
            $warning = $this->db->fetch_array($query);

            if (!$warning['wid']) {
                $this->bb->flash_message($this->lang->error_invalid_warning, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/warninglog');
            } elseif ($warning['daterevoked']) {
                $this->bb->flash_message($this->lang->error_already_revoked, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/warninglog?action=view&wid=' . $warning['wid']);
            }

            $user = $this->user->get_user($warning['uid']);

            $this->plugins->runHooks('admin_tools_warninglog_do_revoke');

            if (!trim($this->bb->input['reason'])) {
                $warn_errors[] = $this->lang->error_no_revoke_reason;
                $this->bb->input['action'] = 'view';
            } else {
                // Warning is still active, lower users point count
                if ($warning['expired'] != 1) {
                    $new_warning_points = $user['warningpoints'] - $warning['points'];
                    if ($new_warning_points < 0) {
                        $new_warning_points = 0;
                    }

                    // Update user
                    $updated_user = [
                        'warningpoints' => $new_warning_points
                    ];
                }

                // Update warning
                $updated_warning = [
                    'expired' => 1,
                    'daterevoked' => TIME_NOW,
                    'revokedby' => $this->user->uid,
                    'revokereason' => $this->db->escape_string($this->bb->input['reason'])
                ];

                $this->plugins->runHooks('admin_tools_warninglog_do_revoke_commit');

                if ($warning['expired'] != 1) {
                    $this->db->update_query('users', $updated_user, "uid='{$warning['uid']}'");
                }

                $this->db->update_query('warnings', $updated_warning, "wid='{$warning['wid']}'");

                $this->bb->flash_message($this->lang->redirect_warning_revoked, 'success');
                return $response->withRedirect($this->bb->admin_url . '/tools/warninglog?action=view&wid=' . $warning['wid']);
            }
        }

        // Detailed view of a warning
        if ($this->bb->input['action'] == 'view') {
            $query = $this->db->query('
		SELECT w.*, t.title AS type_title, u.username, p.subject AS post_subject
		FROM ' . TABLE_PREFIX . 'warnings w
		LEFT JOIN ' . TABLE_PREFIX . 'warningtypes t ON (t.tid=w.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=w.issuedby)
		LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=w.pid)
		WHERE w.wid='" . $this->bb->getInput('wid', 0) . "'
	");
            $warning = $this->db->fetch_array($query);

            if (!$warning['wid']) {
                $this->bb->flash_message($this->lang->error_invalid_warning, 'error');
                return $response->withRedirect($this->bb->admin_url . '/tools/warninglog');
            }

            $user = $this->user->get_user((int)$warning['uid']);

            $this->plugins->runHooks('admin_tools_warninglog_view');

            $this->page->add_breadcrumb_item($this->lang->warning_details, $this->bb->admin_url . '/tools/warninglog?action=view&wid=' . $warning['wid']);

            $html = $this->page->output_header($this->lang->warning_details);

            $user_link = $this->user->build_profile_link($user['username'], $user['uid'], '_blank');

            if (isset($warn_errors)) {
                $html .= $this->page->output_inline_error($warn_errors);
                $this->bb->input['reason'] = htmlspecialchars_uni($this->bb->input['reason']);
            }

            $table = new Table;

            $post_link = '';
            if ($warning['post_subject']) {
                $warning['post_subject'] = $this->parser->parse_badwords($warning['post_subject']);
                $warning['post_subject'] = htmlspecialchars_uni($warning['post_subject']);
                $post_link = get_post_link($warning['pid']);
                $table->construct_cell("<strong>{$this->lang->warned_user}</strong><br /><br />{$user_link}");
                $table->construct_cell("<strong>{$this->lang->post}</strong><br /><br /><a href=\"{$post_link}\" target=\"_blank\">{$warning['post_subject']}</a>");
                $table->construct_row();
            } else {
                $table->construct_cell("<strong>{$this->lang->warned_user}</strong><br /><br />{$user_link}", ['colspan' => 2]);
                $table->construct_row();
            }

            $issuedby = $this->user->build_profile_link($warning['username'], $warning['issuedby'], '_blank');
            $notes = nl2br(htmlspecialchars_uni($warning['notes']));

            $date_issued = $this->time->formatDate('relative', $warning['dateline']);
            if ($warning['type_title']) {
                $warning_type = $warning['type_title'];
            } else {
                $warning_type = $warning['title'];
            }
            $warning_type = htmlspecialchars_uni($warning_type);
            if ($warning['points'] > 0) {
                $warning['points'] = "+{$warning['points']}";
            }

            $points = $this->lang->sprintf($this->lang->warning_points, $warning['points']);
            if ($warning['expired'] != 1) {
                if ($warning['expires'] == 0) {
                    $expires = $this->lang->never;
                } else {
                    $expires = $this->time->formatDate('relative', $warning['expires']);
                }
                $status = $this->lang->warning_active;
            } else {
                if ($warning['daterevoked']) {
                    $expires = $status = $this->lang->warning_revoked;
                } elseif ($warning['expires']) {
                    $expires = $status = $this->lang->already_expired;
                }
            }

            $table->construct_cell("<strong>{$this->lang->warning}</strong><br /><br />{$warning_type} {$points}", ['width' => '50%']);
            $table->construct_cell("<strong>{$this->lang->date_issued}</strong><br /><br />{$date_issued}", ['width' => '50%']);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->issued_by}</strong><br /><br />{$issuedby}", ['width' => '50%']);
            $table->construct_cell("<strong>{$this->lang->expires}</strong><br /><br />{$expires}", ['width' => '50%']);
            $table->construct_row();

            $table->construct_cell("<strong>{$this->lang->warning_note}</strong><br /><br />{$notes}", ['colspan' => 2]);
            $table->construct_row();

            $html .= $table->output("<div class=\"float_right\" style=\"font-weight: normal;\">{$status}</div>" . $this->lang->warning_details);

            if (!$warning['daterevoked']) {
                $form = new Form($this->bb, $this->bb->admin_url . '/tools/warninglog', 'post');
                $html .= $form->getForm();
                $form_container = new FormContainer($this, $this->lang->revoke_warning);
                $html .= $form->generate_hidden_field('action', 'do_revoke');
                $html .= $form->generate_hidden_field('wid', $warning['wid']);
                $form_container->output_row('', $this->lang->revoke_warning_desc, $form->generate_text_area('reason', $this->bb->getInput('reason', '', true), ['id' => 'reason']), 'reason');

                $html .= $form_container->end();
                $buttons[] = $form->generate_submit_button($this->lang->revoke_warning);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
            } else {
                $date_revoked = $this->time->formatDate('relative', $warning['daterevoked']);
                $revoked_user = $this->user->get_user($warning['revokedby']);
                $revoked_by = $this->user->build_profile_link($revoked_user['username'], $revoked_user['uid'], '_blank');
                $revoke_reason = nl2br(htmlspecialchars_uni($warning['revokereason']));

                $revoke_table = new Table;
                $revoke_table->construct_cell("<strong>{$this->lang->revoked_by}</strong><br /><br />{$revoked_by}", ['width' => '50%']);
                $revoke_table->construct_cell("<strong>{$this->lang->date_revoked}</strong><br /><br />{$date_revoked}", ['width' => '50%']);
                $revoke_table->construct_row();

                $revoke_table->construct_cell("<strong>{$this->lang->reason}</strong><br /><br />{$revoke_reason}", ['colspan' => 2]);
                $revoke_table->construct_row();

                $html .= $revoke_table->output($this->lang->warning_is_revoked);
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/WarningLogView.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_tools_warninglog_start');

            $html = $this->page->output_header($this->lang->warning_logs);

            $sub_tabs['warning_logs'] = [
                'title' => $this->lang->warning_logs,
                'link' => $this->bb->admin_url . '/tools/warninglog',
                'description' => $this->lang->warning_logs_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'warning_logs');

            // Filter options
            $where_sql = '';
            if (!empty($this->bb->input['filter']['username'])) {
                $search_user = $this->user->get_user_by_username($this->bb->input['filter']['username']);

                $this->bb->input['filter']['uid'] = (int)$search_user['uid'];
                $this->bb->input['filter']['uid'] = $this->db->fetch_field($query, 'uid');
            }
            if (isset($this->bb->input['filter']['uid'])) {
                $search['uid'] = (int)$this->bb->input['filter']['uid'];
                $where_sql .= " AND w.uid='{$search['uid']}'";
                if (!isset($this->bb->input['search']['username'])) {
                    $user = $this->user->get_user($this->bb->input['search']['uid']);
                    $this->bb->input['search']['username'] = $user['username'];
                }
            }
            if (!empty($this->bb->input['filter']['mod_username'])) {
                $mod_user = $this->user->get_user_by_username($this->bb->input['filter']['mod_username']);

                $this->bb->input['filter']['mod_uid'] = (int)$mod_user['uid'];
            }
            if (isset($this->bb->input['filter']['mod_uid'])) {
                $search['mod_uid'] = (int)$this->bb->input['filter']['mod_uid'];
                $where_sql .= " AND w.issuedby='{$search['mod_uid']}'";
                if (!isset($this->bb->input['search']['mod_username'])) {
                    $mod_user = $this->user->get_user($this->bb->input['search']['uid']);
                    $this->bb->input['search']['mod_username'] = $mod_user['username'];
                }
            }
            if (isset($this->bb->input['filter']['reason'])) {
                $search['reason'] = $this->db->escape_string_like($this->bb->input['filter']['reason']);
                $where_sql .= " AND (w.notes LIKE '%{$search['reason']}%' OR t.title LIKE '%{$search['reason']}%' OR w.title LIKE '%{$search['reason']}%')";
            }
            $sortbysel = [];
            switch ($this->bb->input['filter']['sortby']) {
                case 'username':
                    $sortby = 'u.username';
                    $sortbysel['username'] = ' selected="selected"';
                    break;
                case 'expires':
                    $sortby = 'w.expires';
                    $sortbysel['expires'] = ' selected="selected"';
                    break;
                case 'issuedby':
                    $sortby = 'i.username';
                    $sortbysel['issuedby'] = ' selected="selected"';
                    break;
                default: // 'dateline'
                    $sortby = 'w.dateline';
                    $sortbysel['dateline'] = ' selected="selected"';
            }
            $order = $this->bb->input['filter']['order'];
            $ordersel = [];
            if ($order != 'asc') {
                $order = 'desc';
                $ordersel['desc'] = ' selected="selected"';
            } else {
                $ordersel['asc'] = ' selected="selected"';
            }

            // Expire any warnings past their expiration date
            $warningshandler = new \RunBB\Handlers\DataHandlers\WarningsHandler($this->bb, 'update');

            $warningshandler->expire_warnings();

            // Pagination stuff
            $sql = '
		SELECT COUNT(wid) as count
		FROM
			' . TABLE_PREFIX . 'warnings w
			LEFT JOIN ' . TABLE_PREFIX . "warningtypes t ON (w.tid=t.tid)
		WHERE 1=1
			{$where_sql}
	";
            $query = $this->db->query($sql);
            $total_warnings = $this->db->fetch_field($query, 'count');
            $view_page = 1;
            if (isset($this->bb->input['page']) && $this->bb->getInput('page', 0) > 0) {
                $view_page = $this->bb->getInput('page', 0);
            }
            $per_page = 20;
            if (isset($this->bb->input['filter']['per_page']) && (int)$this->bb->input['filter']['per_page'] > 0) {
                $per_page = (int)$this->bb->input['filter']['per_page'];
            }
            $start = ($view_page - 1) * $per_page;
            // Build the base URL for pagination links
            $url = $this->bb->admin_url . '/tools/warninglog';
            if (isset($this->bb->input['filter']) && count($this->bb->input['filter'])) {
                foreach ($this->bb->input['filter'] as $field => $value) {
                    $value = urlencode($value);
                    $url .= "&amp;filter[{$field}]={$value}";
                }
            }

            // The actual query
            $sql = '
		SELECT
			w.wid, w.title as custom_title, w.points, w.dateline, w.issuedby, w.expires, w.expired, w.daterevoked, w.revokedby,
			t.title,
			u.uid, u.username, u.usergroup, u.displaygroup,
			i.uid as mod_uid, i.username as mod_username, i.usergroup as mod_usergroup, i.displaygroup as mod_displaygroup
		FROM ' . TABLE_PREFIX . 'warnings w
		LEFT JOIN ' . TABLE_PREFIX . 'users u on (w.uid=u.uid)
			LEFT JOIN ' . TABLE_PREFIX . 'warningtypes t ON (w.tid=t.tid)
			LEFT JOIN ' . TABLE_PREFIX . "users i ON (i.uid=w.issuedby)
		WHERE 1=1
			{$where_sql}
		ORDER BY {$sortby} {$order}
		LIMIT {$start}, {$per_page}
	";
            $query = $this->db->query($sql);


            $table = new Table;
            $table->construct_header($this->lang->warned_user, ['width' => '15%']);
            $table->construct_header($this->lang->warning, ['class' => 'align_center', 'width' => '25%']);
            $table->construct_header($this->lang->date_issued, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->expires, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->issued_by, ['class' => 'align_center', 'width' => '15%']);
            $table->construct_header($this->lang->options, ['class' => 'align_center', 'width' => '5%']);

            while ($row = $this->db->fetch_array($query)) {
                if (!$row['username']) {
                    $row['username'] = $this->lang->guest;
                }

                $trow = alt_trow();
                $username = $this->user->format_name($row['username'], $row['usergroup'], $row['displaygroup']);
                if (!$row['uid']) {
                    $username_link = $username;
                } else {
                    $username_link = $this->user->build_profile_link($username, $row['uid'], '_blank');
                }
                $mod_username = $this->user->format_name($row['mod_username'], $row['mod_usergroup'], $row['mod_displaygroup']);
                $mod_username_link = $this->user->build_profile_link($mod_username, $row['mod_uid'], '_blank');
                $issued_date = $this->time->formatDate('relative', $row['dateline']);
                $revoked_text = '';
                if ($row['daterevoked'] > 0) {
                    $revoked_date = $this->time->formatDate('relative', $row['daterevoked']);
                    $revoked_text = "<br /><small><strong>{$this->lang->revoked}</strong> {$revoked_date}</small>";
                }
                if ($row['expires'] > 0) {
                    $expire_date = $this->time->formatDate('relative', $row['expires']);
                } else {
                    $expire_date = $this->lang->never;
                }
                $title = $row['title'];
                if (empty($row['title'])) {
                    $title = $row['custom_title'];
                }
                $title = htmlspecialchars_uni($title);
                if ($row['points'] > 0) {
                    $points = '+' . $row['points'];
                }

                $table->construct_cell($username_link);
                $table->construct_cell("{$title} ({$points})");
                $table->construct_cell($issued_date, ['class' => 'align_center']);
                $table->construct_cell($expire_date . $revoked_text, ['class' => 'align_center']);
                $table->construct_cell($mod_username_link);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/tools/warninglog?action=view&amp;wid={$row['wid']}\">{$this->lang->view}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_warning_logs, ['colspan' => '6']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->warning_logs);

            // Do we need to construct the pagination?
            if ($total_warnings > $per_page) {
                $html .= $this->adm->draw_admin_pagination($view_page, $per_page, $total_warnings, $url) . '<br />';
            }

            $sort_by = [
                'expires' => $this->lang->expiry_date,
                'dateline' => $this->lang->issued_date,
                'username' => $this->lang->warned_user,
                'issuedby' => $this->lang->issued_by
            ];

            $order_array = [
                'asc' => $this->lang->asc,
                'desc' => $this->lang->desc
            ];

            $form = new Form($this->bb, $this->bb->admin_url . '/tools/warninglog', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->filter_warning_logs);
            $form_container->output_row($this->lang->filter_warned_user, '', $form->generate_text_box('filter[username]', $this->bb->input['filter']['username'], ['id' => 'filter_username']), 'filter_username');
            $form_container->output_row($this->lang->filter_issued_by, '', $form->generate_text_box('filter[mod_username]', $this->bb->input['filter']['mod_username'], ['id' => 'filter_mod_username']), 'filter_mod_username');
            $form_container->output_row($this->lang->filter_reason, '', $form->generate_text_box('filter[reason]', $this->bb->input['filter']['reason'], ['id' => 'filter_reason']), 'filter_reason');
            $form_container->output_row($this->lang->sort_by, '', $form->generate_select_box('filter[sortby]', $sort_by, $this->bb->input['filter']['sortby'], ['id' => 'filter_sortby']) . " {$this->lang->in} " . $form->generate_select_box('filter[order]', $order_array, $order, ['id' => 'filter_order']) . " {$this->lang->order}", 'filter_order');
            $form_container->output_row($this->lang->results_per_page, '', $form->generate_numeric_field('filter[per_page]', $per_page, ['id' => 'filter_per_page', 'min' => 1]), 'filter_per_page');

            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->filter_warning_logs);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/WarningLog.html.twig');
        }
    }
}
