<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Tools;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class phpInfo extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('tools');// set active module
        $this->lang->load('tools_php_info', false, true);

        if ($this->bb->input['action'] == 'phpinfo') {
            $this->plugins->runHooks('admin_tools_php_info_phpinfo');

            // Log admin action
            $this->bblogger->log_admin_action();

            phpinfo();
            exit;
        }

        $this->page->add_breadcrumb_item($this->lang->php_info, $this->bb->admin_url . '/tools/php_info');

        $this->plugins->runHooks('admin_tools_php_info_begin');

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_tools_php_info_start');

            $html = $this->page->output_header($this->lang->php_info);

            $html .= "<iframe src=\"".$this->bb->admin_url."/tools/php_info?action=phpinfo\" width=\"100%\" height=\"500\" frameborder=\"0\">{$this->lang->browser_no_iframe_support}</iframe>";

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Tools/PhpInfo.html.twig');
        }
    }
}
