<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\PopupMenu;
use RunCMF\Core\AbstractController;

class AdminPermissions extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_admin_permissions', false, true);

        $this->page->add_breadcrumb_item($this->lang->admin_permissions, $this->bb->admin_url . '/users/adminperm');

        if (($this->bb->input['action'] == 'edit' && $this->bb->input['uid'] == 0) ||
            $this->bb->input['action'] == 'group' || !$this->bb->input['action']
        ) {
            $sub_tabs['user_permissions'] = [
                'title' => $this->lang->user_permissions,
                'link' => $this->bb->admin_url . '/users/adminperm',
                'description' => $this->lang->user_permissions_desc
            ];

            $sub_tabs['group_permissions'] = [
                'title' => $this->lang->group_permissions,
                'link' => $this->bb->admin_url . '/users/adminperm?action=group',
                'description' => $this->lang->group_permissions_desc
            ];

            $sub_tabs['default_permissions'] = [
                'title' => $this->lang->default_permissions,
                'link' => $this->bb->admin_url . '/users/adminperm?action=edit&uid=0',
                'description' => $this->lang->default_permissions_desc
            ];
        }

        $uid = $this->bb->getInput('uid', 0);

        $this->plugins->runHooks('admin_user_admin_permissions_begin');

        if ($this->bb->input['action'] == 'delete') {
            if ($this->user->is_super_admin($uid)) {
                $this->session->flash_message($this->lang->error_super_admin, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            }

            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            }

            if (!trim($this->bb->input['uid'])) {
                $this->session->flash_message($this->lang->error_delete_no_uid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            }

            $query = $this->db->simple_select('adminoptions', 'COUNT(uid) as adminoptions', "uid = '{$this->bb->input['uid']}'");
            if ($this->db->fetch_field($query, 'adminoptions') == 0) {
                $this->session->flash_message($this->lang->error_delete_invalid_uid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            }

            $this->plugins->runHooks('admin_user_admin_permissions_delete');

            if ($this->bb->request_method == 'post') {
                $newperms = [
                    'permissions' => ''
                ];

                $this->plugins->runHooks('admin_user_admin_permissions_delete_commit');

                $this->db->update_query('adminoptions', $newperms, "uid = '{$uid}'");

                // Log admin action
                if ($uid < 0) {
                    $gid = abs($uid);
                    $query = $this->db->simple_select('usergroups', 'title', "gid='{$gid}'");
                    $group = $this->db->fetch_array($query);
                    $this->bblogger->log_admin_action($uid, $group['title']);
                } elseif ($uid == 0) {
                    // Default
                    $this->bblogger->log_admin_action(0, $this->lang->default);
                } else {
                    $user = $this->user->get_user($uid);
                    $this->bblogger->log_admin_action($uid, $user['username']);
                }

                $this->session->flash_message($this->lang->success_perms_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/adminperm?action=delete&uid='.$this->bb->input['uid'], $this->lang->confirm_perms_deletion);
            }
        }

        if ($this->bb->input['action'] == 'edit') {
            if ($this->user->is_super_admin($uid)) {
                $this->session->flash_message($this->lang->error_super_admin, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            }

            $this->plugins->runHooks('admin_user_admin_permissions_edit');

            if ($this->bb->request_method == 'post') {
                foreach ($this->bb->input['permissions'] as $module => $actions) {
                    $no_access = 0;
                    foreach ($actions as $action => $access) {
                        if ($access == 0) {
                            ++$no_access;
                        }
                    }
                    // User can't access any actions in this module - just disallow it completely
                    if ($no_access == count($actions)) {
                        unset($this->bb->input['permissions'][$module]);
                    }
                }

                // Does an options row exist for this admin already?
                $query = $this->db->simple_select('adminoptions', 'COUNT(uid) AS existing_options', "uid='" . $this->bb->getInput('uid', 0) . "'");
                $existing_options = $this->db->fetch_field($query, 'existing_options');
                if ($existing_options > 0) {
                    $this->db->update_query('adminoptions', ['permissions' => $this->db->escape_string(my_serialize($this->bb->input['permissions']))], "uid = '" . $this->bb->getInput('uid', 0) . "'");
                } else {
                    $insert_array = [
                        'uid' => $this->bb->getInput('uid', 0),
                        'permissions' => $this->db->escape_string(my_serialize($this->bb->input['permissions'])),
                        'notes' => '',
                        'defaultviews' => ''
                    ];
                    $this->db->insert_query('adminoptions', $insert_array);
                }

                $this->plugins->runHooks('admin_user_admin_permissions_edit_commit');

                // Log admin action
                if ($uid > 0) {
                    // Users
                    $user = $this->user->get_user($uid);
                    $this->bblogger->log_admin_action($uid, $user['username']);
                } elseif ($uid < 0) {
                    // Groups
                    $gid = abs($uid);
                    $query = $this->db->simple_select('usergroups', 'title', "gid='{$gid}'");
                    $group = $this->db->fetch_array($query);
                    $this->bblogger->log_admin_action($uid, $group['title']);
                } else {
                    // Default
                    $this->bblogger->log_admin_action(0);
                }

                $this->session->flash_message($this->lang->admin_permissions_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/adminperm');
            }

            if ($uid > 0) {
                switch ($this->db->type) {
                    case 'pgsql':
                    case 'sqlite':
                        $query = $this->db->query('
					SELECT u.uid, u.username, g.cancp, g.gid
					FROM ' . TABLE_PREFIX . 'users u
					LEFT JOIN ' . TABLE_PREFIX . "usergroups g ON (((','|| u.additionalgroups|| ',' LIKE '%,'|| g.gid|| ',%') OR u.usergroup = g.gid))
					WHERE u.uid='$uid'
					AND g.cancp=1
					LIMIT 1
				");
                        break;
                    default:
                        $query = $this->db->query('
				SELECT u.uid, u.username, g.cancp, g.gid
				FROM ' . TABLE_PREFIX . 'users u
				LEFT JOIN ' . TABLE_PREFIX . "usergroups g ON (((CONCAT(',', u.additionalgroups, ',') LIKE CONCAT('%,', g.gid, ',%')) OR u.usergroup = g.gid))
				WHERE u.uid='$uid'
				AND g.cancp=1
				LIMIT 1
			");
                }

                $admin = $this->db->fetch_array($query);
                $permission_data = $this->adm->get_admin_permissions($uid, $admin['gid']);
                $title = $admin['username'];
                $this->page->add_breadcrumb_item($this->lang->user_permissions, $this->bb->admin_url . '/users/adminperm');
            } elseif ($uid < 0) {
                $gid = abs($uid);
                $query = $this->db->simple_select('usergroups', 'title', "gid='$gid'");
                $group = $this->db->fetch_array($query);
                $permission_data = $this->adm->get_admin_permissions('', $gid);
                $title = $group['title'];
                $this->page->add_breadcrumb_item($this->lang->group_permissions, $this->bb->admin_url . '/users/adminperm?action=group');
            } else {
                $query = $this->db->simple_select('adminoptions', 'permissions', "uid='0'");
                $permission_data = my_unserialize($this->db->fetch_field($query, 'permissions'));
                $this->page->add_breadcrumb_item($this->lang->default_permissions);
                $title = $this->lang->default;
            }

            if ($uid != 0) {
                $this->page->add_breadcrumb_item($this->lang->edit_permissions . ": {$title}");
            }

            $html = $this->page->output_header($this->lang->edit_permissions);

            if ($uid != 0) {
                $sub_tabs['edit_permissions'] = [
                    'title' => $this->lang->edit_permissions,
                    'link' => $this->bb->admin_url . '/users/adminperm?action=edit&uid=' . $uid,
                    'description' => $this->lang->edit_permissions_desc
                ];

                $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_permissions');
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/users/adminperm?action=edit', 'post', 'edit');
            $html .= $form->getForm();

            $html .= $form->generate_hidden_field('uid', $uid);

            // Fetch all of the modules we have
            $modules_dir = MYBB_ADMIN_DIR . 'Modules';
            $dir = opendir($modules_dir);
            $modules = [];
            while (($module = readdir($dir)) !== false) {
                if (is_dir($modules_dir . '/' . $module) &&
                    !in_array($module, ['.', '..']) &&
                    file_exists($modules_dir . '/' . $module . '/Meta.php')
                ) {
                    $mod = strtolower($module);
                    $meta_function = $mod . '_admin_permissions';
//          // Module has no permissions, skip it
//          if(function_exists($meta_function) && is_array($meta_function()))
                    if (method_exists($this->adm->$mod, $meta_function)) {
                        $permission_modules[$mod] = $this->adm->$mod->$meta_function();
                        $modules[$permission_modules[$mod]['disporder']][] = $mod;
                    }
                }
            }
            closedir($dir);
            ksort($modules);
            foreach ($modules as $disp_order => $mod) {
                if (!is_array($mod)) {
                    continue;
                }

                foreach ($mod as $module) {
                    $module_tabs[$module] = $permission_modules[$module]['name'];
                }
            }
            $html .= $this->page->output_tab_control($module_tabs);
            foreach ($permission_modules as $key => $module) {
                $html .= "<div id=\"tab_{$key}\">\n";
                $form_container = new FormContainer($this->bb, "{$module['name']}");
                foreach ($module['permissions'] as $action => $title) {
                    $form_container->output_row(
                        $title,
                        '',
                        $form->generate_yes_no_radio(
                            'permissions[' . $key . '][' . $action . ']',
                            (int)$permission_data[$key][$action],
                            ['yes' => 1, 'no' => 0]
                        ),
                        'permissions[' . $key . '][' . $action . ']'
                    );
                }
                $html .= $form_container->end();
                $html .= "</div>\n";
            }

            $buttons[] = $form->generate_submit_button($this->lang->update_permissions);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/AdminPermissionsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'group') {
            $this->plugins->runHooks('admin_user_admin_permissions_group');

            $this->page->add_breadcrumb_item($this->lang->group_permissions);
            $html = $this->page->output_header($this->lang->group_permissions);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'group_permissions');

            $table = new Table;
            $table->construct_header($this->lang->group);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            // Get usergroups with ACP access
            $query = $this->db->query('
		SELECT g.title, g.cancp, a.permissions, g.gid
		FROM ' . TABLE_PREFIX . 'usergroups g
		LEFT JOIN ' . TABLE_PREFIX . 'adminoptions a ON (a.uid = -g.gid)
		WHERE g.cancp = 1
		ORDER BY g.title ASC
	');
            while ($group = $this->db->fetch_array($query)) {
                if ($group['permissions'] != '') {
                    $perm_type = 'group';
                } else {
                    $perm_type = 'default';
                }
                $uid = -$group['gid'];
                $table->construct_cell("<div class=\"float_right\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/{$perm_type}.png\" title=\"{$this->lang->permissions_type_group}\" alt=\"{$perm_type}\" /></div><div><strong><a href=\"".$this->bb->admin_url."/users/adminperm?action=edit&amp;uid={$uid}\" title=\"{$this->lang->edit_group}\">{$group['title']}</a></strong><br /></div>");

                if ($group['permissions'] != '') {
                    $popup = new PopupMenu("groupperm_{$uid}", $this->lang->options);
                    $popup->add_item($this->lang->edit_permissions, $this->bb->admin_url . '/users/adminperm?action=edit&uid=' . $uid);

                    // Check permissions for Revoke
                    $popup->add_item($this->lang->revoke_permissions, $this->bb->admin_url . "/users/adminperm?action=delete&amp;uid={$uid}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, 'Are you sure you wish to revoke this group\'s permissions?')");
                    $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                } else {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/adminperm?action=edit&amp;uid={$uid}\">{$this->lang->set_permissions}</a>", ["class" => "align_center"]);
                }
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_group_perms, ['colspan' => '3']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->group_permissions);

            $html .= <<<LEGEND
<br />
<fieldset>
<legend>{$this->lang->legend}</legend>
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/group.png" alt="{$this->lang->using_custom_perms}" style="vertical-align: middle;" /> {$this->lang->using_custom_perms}<br />
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/default.png" alt="{$this->lang->using_default_perms}" style="vertical-align: middle;" /> {$this->lang->using_default_perms}</fieldset>
LEGEND;

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/AdminPermissionsGroup.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_user_admin_permissions_start');

            $this->page->add_breadcrumb_item($this->lang->user_permissions);
            $html = $this->page->output_header($this->lang->user_permissions);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'user_permissions');

            $table = new Table;
            $table->construct_header($this->lang->user);
            $table->construct_header($this->lang->last_active, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            // Get usergroups with ACP access
            $usergroups = [];
            $query = $this->db->simple_select('usergroups', '*', 'cancp = 1');
            while ($usergroup = $this->db->fetch_array($query)) {
                $usergroups[$usergroup['gid']] = $usergroup;
            }

            if (!empty($usergroups)) {
                // Get users whose primary or secondary usergroup has ACP access
                $comma = $primary_group_list = $secondary_group_list = '';
                foreach ($usergroups as $gid => $group_info) {
                    $primary_group_list .= $comma . $gid;
                    switch ($this->db->type) {
                        case 'pgsql':
                        case 'sqlite':
                            $secondary_group_list .= " OR ','|| u.additionalgroups||',' LIKE '%,{$gid},%'";
                            break;
                        default:
                            $secondary_group_list .= " OR CONCAT(',', u.additionalgroups,',') LIKE '%,{$gid},%'";
                    }

                    $comma = ',';
                }

                $group_list = implode(',', array_keys($usergroups));
                $secondary_groups = ',' . $group_list . ',';

                // Get usergroups with ACP access
                $query = $this->db->query('
			SELECT g.title, g.cancp, a.permissions, g.gid
			FROM ' . TABLE_PREFIX . 'usergroups g
			LEFT JOIN ' . TABLE_PREFIX . 'adminoptions a ON (a.uid = -g.gid)
			WHERE g.cancp = 1
			ORDER BY g.title ASC
		');
                while ($group = $this->db->fetch_array($query)) {
                    $group_permissions[$group['gid']] = $group['permissions'];
                }

                $query = $this->db->query('
			SELECT u.uid, u.username, u.lastactive, u.usergroup, u.additionalgroups, a.permissions
			FROM ' . TABLE_PREFIX . 'users u
			LEFT JOIN ' . TABLE_PREFIX . "adminoptions a ON (a.uid=u.uid)
			WHERE u.usergroup IN ({$primary_group_list}) {$secondary_group_list}
			ORDER BY u.username ASC
		");
                while ($admin = $this->db->fetch_array($query)) {
                    if ($admin['permissions'] != '') {
                        $perm_type = 'user';
                    } else {
                        $groups = explode(',', $admin['additionalgroups'] . ',' . $admin['usergroup']);
                        foreach ($groups as $group) {
                            if ($group == '') {
                                continue;
                            }
                            if (isset($group_permissions[$group]) && $group_permissions[$group] != '') {
                                $perm_type = 'group';
                                break;
                            }
                        }

                        if (!$group_permissions) {
                            $perm_type = 'default';
                        }
                    }

                    $usergroup_list = [];

                    // Build a list of group memberships that have access to the Admin CP
                    // Primary usergroup?
                    if (isset($usergroups[$admin['usergroup']]['cancp']) &&
                        $usergroups[$admin['usergroup']]['cancp'] == 1) {
                        $usergroup_list[] = '<i>' . $usergroups[$admin['usergroup']]['title'] . '</i>';
                    }

                    // Secondary usergroups?
                    $additional_groups = explode(',', $admin['additionalgroups']);
                    if (is_array($additional_groups)) {
                        foreach ($additional_groups as $gid) {
                            if (isset($usergroups[$gid]['cancp']) && $usergroups[$gid]['cancp'] == 1) {
                                $usergroup_list[] = $usergroups[$gid]['title'];
                            }
                        }
                    }
                    $usergroup_list = implode($this->lang->comma, $usergroup_list);

                    $table->construct_cell("<div class=\"float_right\"><img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/{$perm_type}.png\" title=\"{$this->lang->permissions_type_user}\" alt=\"{$perm_type}\" /></div><div><strong><a href=\"".$this->bb->admin_url."/users/adminperm?action=edit&amp;uid={$admin['uid']}\" title=\"{$this->lang->edit_user}\">{$admin['username']}</a></strong><br /><small>{$usergroup_list}</small></div>");

                    $table->construct_cell($this->time->formatDate('relative', $admin['lastactive']), ["class" => "align_center"]);

                    $popup = new PopupMenu("adminperm_{$admin['uid']}", $this->lang->options);
                    if (!$this->user->is_super_admin($admin['uid'])) {
                        if ($admin['permissions'] != '') {
                            $popup->add_item($this->lang->edit_permissions, $this->bb->admin_url . '/users/adminperm?action=edit&uid=' . $admin['uid']);
                            $popup->add_item($this->lang->revoke_permissions, $this->bb->admin_url . "/users/adminperm?action=delete&amp;uid={$admin['uid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_perms_deletion2}')");
                        } else {
                            $popup->add_item($this->lang->set_permissions, $this->bb->admin_url . '/users/adminperm?action=edit&uid=' . $admin['uid']);
                        }
                    }
                    $popup->add_item($this->lang->view_log, $this->bb->admin_url . '/tools/adminlog?uid=' . $admin['uid']);
                    $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                    $table->construct_row();
                }
            }

            if (empty($usergroups) || $table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_user_perms, ['colspan' => '3']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->user_permissions);

            $html .= <<<LEGEND
<br />
<fieldset>
<legend>{$this->lang->legend}</legend>
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/user.png" alt="{$this->lang->using_individual_perms}" style="vertical-align: middle;" /> {$this->lang->using_individual_perms}<br />
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/group.png" alt="{$this->lang->using_group_perms}" style="vertical-align: middle;" /> {$this->lang->using_group_perms}<br />
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/default.png" alt="{$this->lang->using_default_perms}" style="vertical-align: middle;" /> {$this->lang->using_default_perms}</fieldset>
LEGEND;
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/AdminPermissions.html.twig');
        }
    }
}
