<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Banning extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_banning', false, true);


        $this->page->add_breadcrumb_item($this->lang->banning, $this->bb->admin_url . '/users/banning');


        $sub_tabs['ips'] = [
            'title' => $this->lang->banned_ips,
            'link' => $this->bb->admin_url . '/config/banning',
        ];

        $sub_tabs['bans'] = [
            'title' => $this->lang->banned_accounts,
            'link' => $this->bb->admin_url . '/users/banning',
            'description' => $this->lang->banned_accounts_desc
        ];

        $sub_tabs['usernames'] = [
            'title' => $this->lang->disallowed_usernames,
            'link' => $this->bb->admin_url . '/config/banning?type=usernames',
        ];

        $sub_tabs['emails'] = [
            'title' => $this->lang->disallowed_email_addresses,
            'link' => $this->bb->admin_url . '/config/banning?type=emails',
        ];

        // Fetch banned groups
        $query = $this->db->simple_select('usergroups', 'gid,title', 'isbannedgroup=1', ['order_by' => 'title']);
        while ($group = $this->db->fetch_array($query)) {
            $banned_groups[$group['gid']] = $group['title'];
        }

        // Fetch ban times
        $ban_times = $this->ban->fetch_ban_times();

        $this->plugins->runHooks('admin_user_banning_begin');

        if ($this->bb->input['action'] == 'prune') {
            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $query = $this->db->simple_select('banned', '*', "uid='{$this->bb->input['uid']}'");
            $ban = $this->db->fetch_array($query);

            if (!$ban['uid']) {
                $this->session->flash_message($this->lang->error_invalid_ban, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $user = $this->user->get_user($ban['uid']);

            if ($this->user->is_super_admin($user['uid']) &&
                ($this->user->uid != $user['uid'] &&
                    !$this->user->is_super_admin($this->user->uid))
            ) {
                $this->session->flash_message($this->lang->cannot_perform_action_super_admin_general, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $this->plugins->runHooks('admin_user_banning_prune');

            if ($this->bb->request_method == 'post') {
                $query = $this->db->simple_select('threads', 'tid', "uid='{$user['uid']}'");
                while ($thread = $this->db->fetch_array($query)) {
                    $this->moderation->delete_thread($thread['tid']);
                }

                $query = $this->db->simple_select('posts', 'pid', "uid='{$user['uid']}'");
                while ($post = $this->db->fetch_array($query)) {
                    $this->moderation->delete_post($post['pid']);
                }

                $this->plugins->runHooks('admin_user_banning_prune_commit');

                $this->cache->update_reportedcontent();

                // Log admin action
                $this->bblogger->log_admin_action($user['uid'], $user['username']);

                $this->session->flash_message($this->lang->success_pruned, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/banning?action=prune&uid=' . $user['uid'], $this->lang->confirm_prune);
            }
        }

        if ($this->bb->input['action'] == 'lift') {
            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $query = $this->db->simple_select('banned', '*', "uid='{$this->bb->input['uid']}'");
            $ban = $this->db->fetch_array($query);

            if (!$ban['uid']) {
                $this->session->flash_message($this->lang->error_invalid_ban, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $user = $this->user->get_user($ban['uid']);

            if ($this->user->is_super_admin($user['uid']) &&
                ($this->user->uid != $user['uid'] &&
                    !$this->user->is_super_admin($this->user->uid))
            ) {
                $this->session->flash_message($this->lang->cannot_perform_action_super_admin_general, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $this->plugins->runHooks('admin_user_banning_lift');

            if ($this->bb->request_method == 'post') {
                $updated_group = [
                    'usergroup' => $ban['oldgroup'],
                    'additionalgroups' => $ban['oldadditionalgroups'],
                    'displaygroup' => $ban['olddisplaygroup']
                ];
                $this->db->delete_query('banned', "uid='{$ban['uid']}'");

                $this->plugins->runHooks('admin_user_banning_lift_commit');

                $this->db->update_query('users', $updated_group, "uid='{$ban['uid']}'");

                $this->cache->update_banned();
                $this->cache->update_moderators();

                // Log admin action
                $this->bblogger->log_admin_action($ban['uid'], $user['username']);

                $this->session->flash_message($this->lang->success_ban_lifted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/banning?action=lift&uid=' . $ban['uid'], $this->lang->confirm_lift_ban);
            }
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('banned', '*', "uid='{$this->bb->input['uid']}'");
            $ban = $this->db->fetch_array($query);

            $user = $this->user->get_user($ban['uid']);

            if (!$ban['uid']) {
                $this->session->flash_message($this->lang->error_invalid_ban, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/banning');
            }

            $this->plugins->runHooks('admin_user_banning_edit');

            if ($this->bb->request_method == 'post') {
                if (!$ban['uid']) {
                    $errors[] = $this->lang->error_invalid_username;
                } // Is the user we're trying to ban a super admin and we're not?
                elseif ($this->user->is_super_admin($ban['uid']) && !$this->user->is_super_admin($ban['uid'])) {
                    $errors[] = $this->lang->error_no_perm_to_ban;
                }

                if ($ban['uid'] == $this->user->uid) {
                    $errors[] = $this->lang->error_ban_self;
                }

                // No errors? Update
                if (!isset($errors)) {
                    // Ban the user
                    if ($this->bb->input['bantime'] == '---') {
                        $lifted = 0;
                    } else {
                        $lifted = $this->ban->ban_date2timestamp($this->bb->input['bantime'], $ban['dateline']);
                    }

                    $reason = my_substr($this->bb->input['reason'], 0, 255);

                    if (count($banned_groups) == 1) {
                        $group = array_keys($banned_groups);
                        $this->bb->input['usergroup'] = $group[0];
                    }

                    $update_array = [
                        'gid' => $this->bb->getInput('usergroup', 0),
                        'dateline' => TIME_NOW,
                        'bantime' => $this->db->escape_string($this->bb->input['bantime']),
                        'lifted' => $this->db->escape_string($lifted),
                        'reason' => $this->db->escape_string($reason)
                    ];

                    $this->db->update_query('banned', $update_array, "uid='{$ban['uid']}'");

                    // Move the user to the banned group
                    $update_array = [
                        'usergroup' => $this->bb->getInput('usergroup', 0),
                        'displaygroup' => 0,
                        'additionalgroups' => '',
                    ];
                    $this->db->update_query('users', $update_array, "uid = {$ban['uid']}");

                    $this->plugins->runHooks('admin_user_banning_edit_commit');

                    $this->cache->update_banned();

                    // Log admin action
                    $this->bblogger->log_admin_action($ban['uid'], $user['username']);

                    $this->session->flash_message($this->lang->success_ban_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/banning');
                }
            }
            $this->page->add_breadcrumb_item($this->lang->edit_ban);
            $html = $this->page->output_header($this->lang->edit_ban);

            $sub_tabs = [];
            $sub_tabs['edit'] = [
                'title' => $this->lang->edit_ban,
                'description' => $this->lang->edit_ban_desc
            ];
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit');

            $form = new Form($this->bb, $this->bb->admin_url . '/users/banning?action=edit&uid=' . $ban['uid'], 'post');
            $html .= $form->getForm();
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, $ban);
            }

            $form_container = new FormContainer($this, $this->lang->edit_ban);
            $form_container->output_row($this->lang->ban_username, '', $user['username']);
            $form_container->output_row($this->lang->ban_reason, '', $form->generate_text_area('reason', $this->bb->input['reason'], ['id' => 'reason', 'maxlength' => '255']), 'reason');
            if (count($banned_groups) > 1) {
                $form_container->output_row($this->lang->ban_group, $this->lang->ban_group_desc, $form->generate_select_box('usergroup', $banned_groups, $this->bb->input['usergroup'], ['id' => 'usergroup']), 'usergroup');
            }

            if ($this->bb->input['bantime'] == 'perm' ||
                $this->bb->input['bantime'] == '' ||
                $this->bb->input['lifted'] == 'perm' ||
                $this->bb->input['lifted'] == ''
            ) {
                $this->bb->input['bantime'] = '---';
                $this->bb->input['lifted'] = '---';
            }

            foreach ($ban_times as $time => $period) {
                if ($time != '---') {
                    $friendly_time = $this->time->formatDate('D, jS M Y @ g:ia', $this->ban->ban_date2timestamp($time));
                    $period = "{$period} ({$friendly_time})";
                }
                $length_list[$time] = $period;
            }
            $form_container->output_row($this->lang->ban_time, '', $form->generate_select_box('bantime', $length_list, $this->bb->input['bantime'], ['id' => 'bantime']), 'bantime');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->update_ban);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/BanningEdit.html.twig');
        }

        if (!$this->bb->input['action']) {
            $where_sql_full = $where_sql = '';
            $errors = [];
            $this->plugins->runHooks('admin_user_banning_start');

            if ($this->bb->request_method == 'post') {
                $options = [
                    'fields' => ['username', 'usergroup', 'additionalgroups', 'displaygroup']
                ];

                $user = $this->user->get_user_by_username($this->bb->input['username'], $options);

                // Are we searching a user?
                if ($this->bb->getInput('search', '') !== '') {
                    $where_sql = 'uid=\'' . (int)$user['uid'] . '\'';
                    $where_sql_full = 'WHERE b.uid=\'' . (int)$user['uid'] . '\'';
                } else {
                    if (!$user['uid']) {
                        $errors[] = $this->lang->error_invalid_username;
                    } // Is the user we're trying to ban a super admin and we're not?
                    elseif ($this->user->is_super_admin($user['uid']) && !$this->user->is_super_admin($this->user->uid)) {
                        $errors[] = $this->lang->error_no_perm_to_ban;
                    } else {
                        $query = $this->db->simple_select('banned', 'uid', "uid='{$user['uid']}'");
                        if ($this->db->fetch_field($query, 'uid')) {
                            $errors[] = $this->lang->error_already_banned;
                        }

                        // Get PRIMARY usergroup information
                        $usergroups = $this->cache->read('usergroups');
                        if (!empty($usergroups[$user['usergroup']]) && $usergroups[$user['usergroup']]['isbannedgroup'] == 1) {
                            $errors[] = $this->lang->error_already_banned;
                        }
                    }

                    if ($user['uid'] == $this->user->uid) {
                        $errors[] = $this->lang->error_ban_self;
                    }

                    // No errors? Insert
                    if (!$errors) {
                        // Ban the user
                        if ($this->bb->input['bantime'] == '---') {
                            $lifted = 0;
                        } else {
                            $lifted = $this->ban->ban_date2timestamp($this->bb->input['bantime']);
                        }

                        $reason = my_substr($this->bb->input['reason'], 0, 255);

                        if (count($banned_groups) == 1) {
                            $group = array_keys($banned_groups);
                            $this->bb->input['usergroup'] = $group[0];
                        }

                        $insert_array = [
                            'uid' => $user['uid'],
                            'gid' => $this->bb->getInput('usergroup', 0),
                            'oldgroup' => $user['usergroup'],
                            'oldadditionalgroups' => $user['additionalgroups'],
                            'olddisplaygroup' => $user['displaygroup'],
                            'admin' => (int)$this->user->uid,
                            'dateline' => TIME_NOW,
                            'bantime' => $this->db->escape_string($this->bb->input['bantime']),
                            'lifted' => $this->db->escape_string($lifted),
                            'reason' => $this->db->escape_string($reason)
                        ];
                        $this->db->insert_query('banned', $insert_array);

                        // Move the user to the banned group
                        $update_array = [
                            'usergroup' => $this->bb->getInput('usergroup', 0),
                            'displaygroup' => 0,
                            'additionalgroups' => '',
                        ];

                        $this->db->delete_query('forumsubscriptions', "uid = '{$user['uid']}'");
                        $this->db->delete_query('threadsubscriptions', "uid = '{$user['uid']}'");

                        $this->plugins->runHooks('admin_user_banning_start_commit');

                        $this->db->update_query('users', $update_array, "uid = '{$user['uid']}'");

                        $this->cache->update_banned();

                        // Log admin action
                        $this->bblogger->log_admin_action($user['uid'], $user['username'], $lifted);

                        $this->session->flash_message($this->lang->success_banned, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/users/banning');
                    }
                }
            }

            $html = $this->page->output_header($this->lang->banned_accounts);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'bans');

            $query = $this->db->simple_select('banned', 'COUNT(*) AS ban_count', $where_sql);
            $ban_count = $this->db->fetch_field($query, 'ban_count');

            $per_page = 20;

            if (isset($this->bb->input['page']) && $this->bb->input['page'] > 0) {
                $current_page = $this->bb->getInput('page', 0);
                $start = ($current_page - 1) * $per_page;
                $pages = $ban_count / $per_page;
                $pages = ceil($pages);
                if ($current_page > $pages) {
                    $start = 0;
                    $current_page = 1;
                }
            } else {
                $start = 0;
                $current_page = 1;
            }

            $pagination = $this->adm->draw_admin_pagination($current_page, $per_page, $ban_count, $this->bb->admin_url . '/users/banning?page={page}');

            $table = new Table;
            $table->construct_header($this->lang->user);
            $table->construct_header($this->lang->ban_lifts_on, ['class' => 'align_center', 'width' => 150]);
            $table->construct_header($this->lang->time_left, ['class' => 'align_center', 'width' => 150]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 200]);
            $table->construct_header($this->lang->moderation, ['class' => 'align_center', 'colspan' => 1, 'width' => 200]);

            // Fetch bans
            $query = $this->db->query('
		SELECT b.*, a.username AS adminuser, u.username
		FROM ' . TABLE_PREFIX . 'banned b
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (b.uid=u.uid)
		LEFT JOIN ' . TABLE_PREFIX . "users a ON (b.admin=a.uid)
		{$where_sql_full}
		ORDER BY dateline DESC
		LIMIT {$start}, {$per_page}
	");

            // Get the banned users
            while ($ban = $this->db->fetch_array($query)) {
                $profile_link = $this->user->build_profile_link($ban['username'], $ban['uid'], '_blank');
                $ban_date = $this->time->formatDate($this->bb->settings['dateformat'], $ban['dateline']);
                if ($ban['lifted'] == 'perm' || $ban['lifted'] == '' || $ban['bantime'] == 'perm' || $ban['bantime'] == '---') {
                    $ban_period = $this->lang->permenantly;
                    $time_remaining = $lifts_on = $this->lang->na;
                } else {
                    $ban_period = $this->lang->for . ' ' . $ban_times[$ban['bantime']];

                    $remaining = $ban['lifted'] - TIME_NOW;
                    $time_remaining = $this->time->niceTime($remaining, ['short' => 1, 'seconds' => false]) . '';

                    if ($remaining < 3600) {
                        $time_remaining = "<span style=\"color: red;\">{$time_remaining}</span>";
                    } elseif ($remaining < 86400) {
                        $time_remaining = "<span style=\"color: maroon;\">{$time_remaining}</span>";
                    } elseif ($remaining < 604800) {
                        $time_remaining = "<span style=\"color: green;\">{$time_remaining}</span>";
                    }

                    $lifts_on = $this->time->formatDate($this->bb->settings['dateformat'], $ban['lifted']);
                }

                if (!$ban['adminuser']) {
                    if ($ban['admin'] == 0) {
                        $ban['adminuser'] = 'BB System';
                    } else {
                        $ban['adminuser'] = $ban['admin'];
                    }
                }

                $table->construct_cell($this->lang->sprintf($this->lang->bannedby_x_on_x, $profile_link, htmlspecialchars_uni($ban['adminuser']), $ban_date, $ban_period));
                $table->construct_cell($lifts_on, ['class' => 'align_center']);
                $table->construct_cell($time_remaining, ['class' => 'align_center']);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/banning?action=edit&amp;uid={$ban['uid']}\">{$this->lang->edit}</a>", ["class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/banning?action=lift&amp;uid={$ban['uid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_lift_ban}');\">{$this->lang->lift}</a>", ["class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/banning?action=prune&amp;uid={$ban['uid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_prune}');\">{$this->lang->prune_threads_and_posts}</a>", ["class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_banned_users, ['colspan' => '6']);
                $table->construct_row();
            }
            $html .= $table->output($this->lang->banned_accounts);
            $html .= $pagination;

            $form = new Form($this->bb, $this->bb->admin_url . '/users/banning', 'post');
            $html .= $form->getForm();
            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            if (isset($this->bb->input['uid']) && !isset($this->bb->input['username'])) {
                $user = $this->user->get_user($this->bb->input['uid']);
                $this->bb->input['username'] = $user['username'];
            }

            $form_container = new FormContainer($this, $this->lang->ban_a_user);
            $banusername = isset($this->bb->input['username']) ? $this->bb->input['username'] : '';
            $form_container->output_row($this->lang->ban_username, $this->lang->autocomplete_enabled, $form->generate_text_box('username', $banusername, ['id' => 'username']), 'username');
            $banreason = isset($this->bb->input['reason']) ? $this->bb->input['reason'] : '';
            $form_container->output_row($this->lang->ban_reason, '', $form->generate_text_area('reason', $banreason, ['id' => 'reason', 'maxlength' => '255']), 'reason');
            if (count($banned_groups) > 1) {
                $form_container->output_row($this->lang->ban_group, $this->lang->add_ban_group_desc, $form->generate_select_box('usergroup', $banned_groups, $this->bb->input['usergroup'], ['id' => 'usergroup']), 'usergroup');
            }
            foreach ($ban_times as $time => $period) {
                if ($time != '---') {
                    $friendly_time = $this->time->formatDate('D, jS M Y @ g:ia', $this->ban->ban_date2timestamp($time));
                    $period = "{$period} ({$friendly_time})";
                }
                $length_list[$time] = $period;
            }
            $bantime = isset($this->bb->input['bantime']) ? $this->bb->input['bantime'] : '';
            $form_container->output_row($this->lang->ban_time, '', $form->generate_select_box('bantime', $length_list, $bantime, ['id' => 'bantime']), 'bantime');

            $html .= $form_container->end();

            // Autocompletion for usernames
            $html .= '
	<link rel="stylesheet" href="' . $this->bb->asset_url . '/jscripts/select2/select2.css">
	<script type="text/javascript" src="' . $this->bb->asset_url . '/jscripts/select2/select2.min.js?ver=1804"></script>
	<script type="text/javascript">
	<!--
	  var rootpath = "' . $this->bb->settings['bburl'] . '";
	$("#username").select2({
		placeholder: "' . $this->lang->search_for_a_user . '",
		minimumInputLength: 2,
		multiple: false,
		ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
			url: "' . $this->bb->settings['bburl'] . '/xmlhttp?action=get_users",
			dataType: "json",
			data: function (term, page) {
				return {
					query: term, // search term
				};
			},
			results: function (data, page) { // parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to alter remote JSON data
				return {results: data};
			}
		},
		initSelection: function(element, callback) {
			var query = $(element).val();
			if (query !== "") {
				$.ajax("' . $this->bb->settings['bburl'] . '/xmlhttp?action=get_users&getone=1", {
					data: {
						query: query
					},
					dataType: "json"
				}).done(function(data) { callback(data); });
			}
		},
	});

  	$(\'[for=username]\').click(function(){
		$("#username").select2(\'open\');
		return false;
	});
	// -->
	</script>';

            $buttons[] = $form->generate_submit_button($this->lang->ban_user);
            $buttons[] = $form->generate_submit_button($this->lang->search_user, ['name' => 'search']);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/Banning.html.twig');
        }
    }
}
