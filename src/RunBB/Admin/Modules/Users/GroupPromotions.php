<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\PopupMenu;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class GroupPromotions extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_group_promotions', false, true);

        $this->page->add_breadcrumb_item($this->lang->user_group_promotions, $this->bb->admin_url . '/users/group_promotions');

        $sub_tabs['usergroup_promotions'] = [
            'title' => $this->lang->user_group_promotions,
            'link' => $this->bb->admin_url . '/users/group_promotions',
            'description' => $this->lang->user_group_promotions_desc
        ];

        $sub_tabs['add_promotion'] = [
            'title' => $this->lang->add_new_promotion,
            'link' => $this->bb->admin_url . '/users/group_promotions?action=add',
            'description' => $this->lang->add_new_promotion_desc
        ];

        $sub_tabs['promotion_logs'] = [
            'title' => $this->lang->view_promotion_logs,
            'link' => $this->bb->admin_url . '/users/group_promotions?action=logs',
            'description' => $this->lang->view_promotion_logs_desc
        ];

        $this->plugins->runHooks('admin_user_group_promotions_begin');

        if ($this->bb->input['action'] == 'disable') {
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            if (!trim($this->bb->input['pid'])) {
                $this->session->flash_message($this->lang->error_no_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $query = $this->db->simple_select('promotions', '*', "pid='" . $this->bb->getInput('pid', 0) . "'");
            $promotion = $this->db->fetch_array($query);

            if (!$promotion['pid']) {
                $this->session->flash_message($this->lang->error_invalid_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $this->plugins->runHooks('admin_user_group_promotions_disable');

            if ($this->bb->request_method == 'post') {
                $update_promotion = [
                    'enabled' => 0
                ];

                $this->plugins->runHooks('admin_user_group_promotions_disable_commit');

                $this->db->update_query('promotions', $update_promotion, "pid = '{$promotion['pid']}'");

                // Log admin action
                $this->bblogger->log_admin_action($promotion['pid'], $promotion['title']);

                $this->session->flash_message($this->lang->success_promo_disabled, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/group_promotions?action=disable&pid=' . $promotion['pid'], $this->lang->confirm_promo_disable);
            }
        }

        if ($this->bb->input['action'] == 'delete') {
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            if (!trim($this->bb->input['pid'])) {
                $this->session->flash_message($this->lang->error_no_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $query = $this->db->simple_select('promotions', '*', "pid='" . $this->bb->getInput('pid', 0) . "'");
            $promotion = $this->db->fetch_array($query);

            if (!$promotion['pid']) {
                $this->session->flash_message($this->lang->error_invalid_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $this->plugins->runHooks('admin_user_group_promotions_delete');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('promotions', "pid = '{$promotion['pid']}'");

                $this->plugins->runHooks('admin_user_group_promotions_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($promotion['pid'], $promotion['title']);

                $this->session->flash_message($this->lang->success_promo_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/group_promotions?action=delete&pid=' . $this->bb->input['pid'], $this->lang->confirm_promo_deletion);
            }
        }

        if ($this->bb->input['action'] == 'enable') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            if (!trim($this->bb->input['pid'])) {
                $this->session->flash_message($this->lang->error_no_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $query = $this->db->simple_select('promotions', '*', "pid='" . $this->bb->getInput('pid', 0) . "'");
            $promotion = $this->db->fetch_array($query);

            if (!$promotion['pid']) {
                $this->session->flash_message($this->lang->error_invalid_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $this->plugins->runHooks('admin_user_group_promotions_enable');

            $update_promotion = [
                'enabled' => 1
            ];

            $this->plugins->runHooks('admin_user_group_promotions_enable_commit');

            $this->db->update_query('promotions', $update_promotion, "pid = '{$promotion['pid']}'");

            // Log admin action
            $this->bblogger->log_admin_action($promotion['pid'], $promotion['title']);

            $this->session->flash_message($this->lang->success_promo_enabled, 'success');
            return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
        }

        if ($this->bb->input['action'] == 'edit') {
            if (!trim($this->bb->input['pid'])) {
                $this->session->flash_message($this->lang->error_no_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $query = $this->db->simple_select('promotions', '*', "pid = '{$this->bb->input['pid']}'");
            $promotion = $this->db->fetch_array($query);

            if (!$promotion) {
                $this->session->flash_message($this->lang->error_invalid_promo_id, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
            }

            $this->plugins->runHooks('admin_user_group_promotions_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_no_title;
                }

                if (!trim($this->bb->input['description'])) {
                    $errors[] = $this->lang->error_no_desc;
                }

                if (empty($this->bb->input['requirements'])) {
                    $errors[] = $this->lang->error_no_requirements;
                }

                if (empty($this->bb->input['originalusergroup'])) {
                    $errors[] = $this->lang->error_no_orig_usergroup;
                }

                if (!trim($this->bb->input['newusergroup'])) {
                    $errors[] = $this->lang->error_no_new_usergroup;
                }

                if (!trim($this->bb->input['usergroupchangetype'])) {
                    $errors[] = $this->lang->error_no_usergroup_change_type;
                }

                if (!isset($errors)) {
                    if (in_array('*', $this->bb->input['originalusergroup'])) {
                        $this->bb->input['originalusergroup'] = '*';
                    } else {
                        $this->bb->input['originalusergroup'] = implode(',', array_map('intval', $this->bb->input['originalusergroup']));
                    }

                    $allowed_operators = ['>', '>=', '=', '<=', '<'];
                    $operator_fields = ['posttype', 'threadtype', 'timeregisteredtype', 'reputationtype', 'referralstype', 'warningstype'];

                    foreach ($operator_fields as $field) {
                        if (!in_array($this->bb->getInput($field, ''), $allowed_operators)) {
                            $this->bb->input[$field] = '=';
                        }
                    }

                    $update_promotion = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'posts' => $this->bb->getInput('postcount', 0),
                        'posttype' => $this->db->escape_string($this->bb->input['posttype']),
                        'threads' => $this->bb->getInput('threadcount', 0),
                        'threadtype' => $this->db->escape_string($this->bb->input['threadtype']),
                        'registered' => $this->bb->getInput('timeregistered', 0),
                        'registeredtype' => $this->db->escape_string($this->bb->input['timeregisteredtype']),
                        'online' => $this->bb->getInput('timeonline', 0),
                        'onlinetype' => $this->db->escape_string($this->bb->input['timeonlinetype']),
                        'reputations' => $this->bb->getInput('reputationcount', 0),
                        'reputationtype' => $this->db->escape_string($this->bb->input['reputationtype']),
                        'referrals' => $this->bb->getInput('referrals', 0),
                        'referralstype' => $this->db->escape_string($this->bb->input['referralstype']),
                        'warnings' => $this->bb->getInput('warnings', 0),
                        'warningstype' => $this->db->escape_string($this->bb->input['warningstype']),
                        'requirements' => $this->db->escape_string(implode(',', $this->bb->input['requirements'])),
                        'originalusergroup' => $this->db->escape_string($this->bb->input['originalusergroup']),
                        'newusergroup' => $this->bb->getInput('newusergroup', 0),
                        'usergrouptype' => $this->db->escape_string($this->bb->input['usergroupchangetype']),
                        'enabled' => $this->bb->getInput('enabled', 0),
                        'logging' => $this->bb->getInput('logging', 0)
                    ];

                    $this->plugins->runHooks('admin_user_group_promotions_edit_commit');

                    $this->db->update_query('promotions', $update_promotion, "pid = '{$promotion['pid']}'");

                    // Log admin action
                    $this->bblogger->log_admin_action($promotion['pid'], $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_promo_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_promotion);
            $html = $this->page->output_header($this->lang->user_group_promotions . ' - ' . $this->lang->edit_promotion);

            $sub_tabs = [];
            $sub_tabs['edit_promotion'] = [
                'title' => $this->lang->edit_promotion,
                'link' => $this->bb->admin_url . '/users/group_promotions?action=edit',
                'description' => $this->lang->edit_promotion_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_promotion');
            $form = new Form($this->bb, $this->bb->admin_url . '/users/group_promotions?action=edit', 'post', 'edit');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('pid', $promotion['pid']);
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['title'] = $promotion['title'];
                $this->bb->input['description'] = $promotion['description'];
                $this->bb->input['requirements'] = explode(',', $promotion['requirements']);
                $this->bb->input['reputationcount'] = $promotion['reputations'];
                $this->bb->input['reputationtype'] = $promotion['reputationtype'];
                $this->bb->input['postcount'] = $promotion['posts'];
                $this->bb->input['posttype'] = $promotion['posttype'];
                $this->bb->input['threadcount'] = $promotion['threads'];
                $this->bb->input['threadtype'] = $promotion['threadtype'];
                $this->bb->input['referrals'] = $promotion['referrals'];
                $this->bb->input['referralstype'] = $promotion['referralstype'];
                $this->bb->input['warnings'] = $promotion['warnings'];
                $this->bb->input['warningstype'] = $promotion['warningstype'];
                $this->bb->input['timeregistered'] = $promotion['registered'];
                $this->bb->input['timeregisteredtype'] = $promotion['registeredtype'];
                $this->bb->input['timeonline'] = $promotion['online'];
                $this->bb->input['timeonlinetype'] = $promotion['onlinetype'];
                $this->bb->input['originalusergroup'] = explode(',', $promotion['originalusergroup']);
                $this->bb->input['usergroupchangetype'] = $promotion['usergrouptype'];
                $this->bb->input['newusergroup'] = $promotion['newusergroup'];
                $this->bb->input['enabled'] = $promotion['enabled'];
                $this->bb->input['logging'] = $promotion['logging'];
            }

            $form_container = new FormContainer($this, $this->lang->edit_promotion);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_desc . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');

            $options = [
                'postcount' => $this->lang->post_count,
                'threadcount' => $this->lang->thread_count,
                'reputation' => $this->lang->reputation,
                'referrals' => $this->lang->referrals,
                'warnings' => $this->lang->warning_points,
                'timeregistered' => $this->lang->time_registered,
                'timeonline' => $this->lang->time_online
            ];
            $form_container->output_row($this->lang->promo_requirements . ' <em>*</em>', $this->lang->promo_requirements_desc, $form->generate_select_box('requirements[]', $options, $this->bb->input['requirements'], ['id' => 'requirements', 'multiple' => true, 'size' => 5]), 'requirements');
            $options_type = [
                '>' => $this->lang->greater_than,
                '>=' => $this->lang->greater_than_or_equal_to,
                '=' => $this->lang->equal_to,
                '<=' => $this->lang->less_than_or_equal_to,
                '<' => $this->lang->less_than
            ];
            $form_container->output_row($this->lang->post_count, $this->lang->post_count_desc, $form->generate_numeric_field('postcount', $this->bb->input['postcount'], ['id' => 'postcount', 'min' => 0]) . ' ' . $form->generate_select_box('posttype', $options_type, $this->bb->input['posttype'], ['id' => 'posttype']), 'postcount');
            $form_container->output_row($this->lang->thread_count, $this->lang->thread_count_desc, $form->generate_numeric_field('threadcount', $this->bb->input['threadcount'], ['id' => 'threadcount', 'min' => 0]) . ' ' . $form->generate_select_box('threadtype', $options_type, $this->bb->input['threadtype'], ['id' => 'threadtype']), 'threadcount');
            $form_container->output_row($this->lang->reputation_count, $this->lang->reputation_count_desc, $form->generate_numeric_field('reputationcount', $this->bb->input['reputationcount'], ['id' => 'reputationcount', 'min' => 0]) . ' ' . $form->generate_select_box('reputationtype', $options_type, $this->bb->input['reputationtype'], ['id' => 'reputationtype']), 'reputationcount');
            $options = [
                'hours' => $this->lang->hours,
                'days' => $this->lang->days,
                'weeks' => $this->lang->weeks,
                'months' => $this->lang->months,
                'years' => $this->lang->years
            ];
            $form_container->output_row($this->lang->referral_count, $this->lang->referral_count_desc, $form->generate_numeric_field('referrals', $this->bb->input['referrals'], ['id' => 'referrals', 'min' => 0]) . ' ' . $form->generate_select_box('referralstype', $options_type, $this->bb->input['referralstype'], ['id' => 'referralstype']), 'referrals');
            $form_container->output_row($this->lang->warning_points, $this->lang->warning_points_desc, $form->generate_numeric_field('warnings', $this->bb->input['warnings'], ['id' => 'warnings', 'min' => 0]) . ' ' . $form->generate_select_box('warningstype', $options_type, $this->bb->input['warningstype'], ['id' => 'warningstype']), 'warnings');
            $form_container->output_row($this->lang->time_registered, $this->lang->time_registered_desc, $form->generate_numeric_field('timeregistered', $this->bb->input['timeregistered'], ['id' => 'timeregistered', 'min' => 0]) . ' ' . $form->generate_select_box('timeregisteredtype', $options, $this->bb->input['timeregisteredtype'], ['id' => 'timeregisteredtype']), 'timeregistered');
            $form_container->output_row($this->lang->time_online, $this->lang->time_online_desc, $form->generate_numeric_field('timeonline', $this->bb->input['timeonline'], ['id' => 'timeonline', 'min' => 0]) . ' ' . $form->generate_select_box('timeonlinetype', $options, $this->bb->input['timeonlinetype'], ['id' => 'timeonlinetype']), 'timeonline');
            $options = [];

            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[(int)$usergroup['gid']] = $usergroup['title'];
            }

            $form_container->output_row($this->lang->orig_user_group . ' <em>*</em>', $this->lang->orig_user_group_desc, $form->generate_select_box('originalusergroup[]', $options, $this->bb->input['originalusergroup'], ['id' => 'originalusergroup', 'multiple' => true, 'size' => 5]), 'originalusergroup');

            unset($options['*']); // Remove the all usergroups option
            $form_container->output_row($this->lang->new_user_group . ' <em>*</em>', $this->lang->new_user_group_desc, $form->generate_select_box('newusergroup', $options, $this->bb->input['newusergroup'], ['id' => 'newusergroup']), 'newusergroup');

            $options = [
                'primary' => $this->lang->primary_user_group,
                'secondary' => $this->lang->secondary_user_group
            ];
            $form_container->output_row($this->lang->user_group_change_type . ' <em>*</em>', $this->lang->user_group_change_type_desc, $form->generate_select_box('usergroupchangetype', $options, $this->bb->input['usergroupchangetype'], ['id' => 'usergroupchangetype']), 'usergroupchangetype');
            $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->input['enabled'], true));
            $form_container->output_row($this->lang->enable_logging . ' <em>*</em>', '', $form->generate_yes_no_radio('logging', $this->bb->input['logging'], true));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->update_promotion);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupPromotionsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_user_group_promotions_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_no_title;
                }

                if (!trim($this->bb->input['description'])) {
                    $errors[] = $this->lang->error_no_desc;
                }

                if (empty($this->bb->input['requirements'])) {
                    $errors[] = $this->lang->error_no_requirements;
                }

                if (empty($this->bb->input['originalusergroup'])) {
                    $errors[] = $this->lang->error_no_orig_usergroup;
                }

                if (!trim($this->bb->input['newusergroup'])) {
                    $errors[] = $this->lang->error_no_new_usergroup;
                }

                if (!trim($this->bb->input['usergroupchangetype'])) {
                    $errors[] = $this->lang->error_no_usergroup_change_type;
                }

                if (!isset($errors)) {
                    if (in_array('*', $this->bb->input['originalusergroup'])) {
                        $this->bb->input['originalusergroup'] = '*';
                    } else {
                        $this->bb->input['originalusergroup'] = implode(',', array_map('intval', $this->bb->input['originalusergroup']));
                    }

                    $allowed_operators = ['>', '>=', '=', '<=', '<'];
                    $operator_fields = ['posttype', 'threadtype', 'timeregisteredtype', 'reputationtype', 'referralstype', 'warningstype'];

                    foreach ($operator_fields as $field) {
                        if (!in_array($this->bb->getInput($field, ''), $allowed_operators)) {
                            $this->bb->input[$field] = '=';
                        }
                    }

                    $new_promotion = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'posts' => $this->bb->getInput('postcount', 0),
                        'posttype' => $this->db->escape_string($this->bb->input['posttype']),
                        'threads' => $this->bb->getInput('threadcount', 0),
                        'threadtype' => $this->db->escape_string($this->bb->input['threadtype']),
                        'registered' => $this->bb->getInput('timeregistered', 0),
                        'registeredtype' => $this->db->escape_string($this->bb->input['timeregisteredtype']),
                        'online' => $this->bb->getInput('timeonline', 0),
                        'onlinetype' => $this->db->escape_string($this->bb->input['timeonlinetype']),
                        'reputations' => $this->bb->getInput('reputationcount', 0),
                        'reputationtype' => $this->db->escape_string($this->bb->input['reputationtype']),
                        'referrals' => $this->bb->getInput('referrals', 0),
                        'referralstype' => $this->db->escape_string($this->bb->input['referralstype']),
                        'warnings' => $this->bb->getInput('warnings', 0),
                        'warningstype' => $this->db->escape_string($this->bb->input['warningstype']),
                        'requirements' => $this->db->escape_string(implode(',', $this->bb->input['requirements'])),
                        'originalusergroup' => $this->db->escape_string($this->bb->input['originalusergroup']),
                        'usergrouptype' => $this->db->escape_string($this->bb->input['usergroupchangetype']),
                        'newusergroup' => $this->bb->getInput('newusergroup', 0),
                        'enabled' => $this->bb->getInput('enabled', 0),
                        'logging' => $this->bb->getInput('logging', 0)
                    ];

                    $pid = $this->db->insert_query('promotions', $new_promotion);

                    $this->plugins->runHooks('admin_user_group_promotions_add_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($pid, $this->bb->input['title']);

                    $this->session->flash_message($this->lang->success_promo_added, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/group_promotions');
                }
            }
            $this->page->add_breadcrumb_item($this->lang->add_new_promotion);
            $html = $this->page->output_header($this->lang->user_group_promotions . ' - ' . $this->lang->add_new_promotion);

            $sub_tabs['usergroup_promotions'] = [
                'title' => $this->lang->user_group_promotions,
                'link' => $this->bb->admin_url . '/users/group_promotions'
            ];

            $sub_tabs['add_promotion'] = [
                'title' => $this->lang->add_new_promotion,
                'link' => $this->bb->admin_url . '/users/group_promotions?action=add',
                'description' => $this->lang->add_new_promotion_desc
            ];

            $sub_tabs['promotion_logs'] = [
                'title' => $this->lang->view_promotion_logs,
                'link' => $this->bb->admin_url . '/users/group_promotions?action=logs'
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_promotion');
            $form = new Form($this->bb, $this->bb->admin_url . '/users/group_promotions?action=add', 'post', 'add');
            $html .= $form->getForm();
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input['reputationcount'] = '0';
                $this->bb->input['referrals'] = '0';
                $this->bb->input['warnings'] = '0';
                $this->bb->input['postcount'] = '0';
                $this->bb->input['threadcount'] = '0';
                $this->bb->input['timeregistered'] = '0';
                $this->bb->input['timeregisteredtype'] = 'days';
                $this->bb->input['timeonline'] = '0';
                $this->bb->input['timeonlinetype'] = 'days';
                $this->bb->input['originalusergroup'] = '*';
                $this->bb->input['newusergroup'] = '2';
                $this->bb->input['enabled'] = '1';
                $this->bb->input['logging'] = '1';
            }
            $form_container = new FormContainer($this, $this->lang->add_new_promotion);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_desc . ' <em>*</em>', '', $form->generate_text_box('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');

            $options = [
                'postcount' => $this->lang->post_count,
                'threadcount' => $this->lang->thread_count,
                'reputation' => $this->lang->reputation,
                'referrals' => $this->lang->referrals,
                'warnings' => $this->lang->warning_points,
                'timeregistered' => $this->lang->time_registered,
                'timeonline' => $this->lang->time_online
            ];

            $form_container->output_row($this->lang->promo_requirements . ' <em>*</em>', $this->lang->promo_requirements_desc, $form->generate_select_box('requirements[]', $options, $this->bb->getInput('requirements', ['']), ['id' => 'requirements', 'multiple' => true, 'size' => 5]), 'requirements');

            $options_type = [
                '>' => $this->lang->greater_than,
                '>=' => $this->lang->greater_than_or_equal_to,
                '=' => $this->lang->equal_to,
                '<=' => $this->lang->less_than_or_equal_to,
                '<' => $this->lang->less_than
            ];

            $form_container->output_row($this->lang->post_count, $this->lang->post_count_desc, $form->generate_numeric_field('postcount', $this->bb->input['postcount'], ['id' => 'postcount', 'min' => 0]) . ' ' . $form->generate_select_box('posttype', $options_type, $this->bb->getInput('posttype', ''), ['id' => 'posttype']), 'postcount');
            $form_container->output_row($this->lang->thread_count, $this->lang->thread_count_desc, $form->generate_numeric_field('threadcount', $this->bb->input['threadcount'], ['id' => 'threadcount', 'min' => 0]) . ' ' . $form->generate_select_box('threadtype', $options_type, $this->bb->getInput('threadtype', ''), ['id' => 'threadtype']), 'threadcount');
            $form_container->output_row($this->lang->reputation_count, $this->lang->reputation_count_desc, $form->generate_numeric_field('reputationcount', $this->bb->input['reputationcount'], ['id' => 'reputationcount', 'min' => 0]) . ' ' . $form->generate_select_box('reputationtype', $options_type, $this->bb->getInput('reputationtype', ''), ['id' => 'reputationtype']), 'reputationcount');
            $options = [
                'hours' => $this->lang->hours,
                'days' => $this->lang->days,
                'weeks' => $this->lang->weeks,
                'months' => $this->lang->months,
                'years' => $this->lang->years
            ];
            $form_container->output_row($this->lang->referral_count, $this->lang->referral_count_desc, $form->generate_numeric_field('referrals', $this->bb->input['referrals'], ['id' => 'referrals', 'min' => 0]) . ' ' . $form->generate_select_box('referralstype', $options_type, $this->bb->getInput('referralstype', ''), ['id' => 'referralstype']), 'referrals');
            $form_container->output_row($this->lang->warning_points, $this->lang->warning_points_desc, $form->generate_numeric_field('warnings', $this->bb->input['warnings'], ['id' => 'warnings', 'min' => 0]) . ' ' . $form->generate_select_box('warningstype', $options_type, $this->bb->getInput('warningstype', ''), ['id' => 'warningstype']), 'warnings');
            $form_container->output_row($this->lang->time_registered, $this->lang->time_registered_desc, $form->generate_numeric_field('timeregistered', $this->bb->input['timeregistered'], ['id' => 'timeregistered', 'min' => 0]) . ' ' . $form->generate_select_box('timeregisteredtype', $options, $this->bb->input['timeregisteredtype'], ['id' => 'timeregisteredtype']), 'timeregistered');
            $form_container->output_row($this->lang->time_online, $this->lang->time_online_desc, $form->generate_numeric_field('timeonline', $this->bb->input['timeonline'], ['id' => 'timeonline', 'min' => 0]) . ' ' . $form->generate_select_box('timeonlinetype', $options, $this->bb->input['timeonlinetype'], ['id' => 'timeonlinetype']), 'timeonline');
            $options = [];

            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[(int)$usergroup['gid']] = $usergroup['title'];
            }

            $form_container->output_row($this->lang->orig_user_group . ' <em>*</em>', $this->lang->orig_user_group_desc, $form->generate_select_box('originalusergroup[]', $options, $this->bb->getInput('originalusergroup', ['']), ['id' => 'originalusergroup', 'multiple' => true, 'size' => 5]), 'originalusergroup');

            unset($options['*']);
            $form_container->output_row($this->lang->new_user_group . ' <em>*</em>', $this->lang->new_user_group_desc, $form->generate_select_box('newusergroup', $options, $this->bb->input['newusergroup'], ['id' => 'newusergroup']), 'newusergroup');

            $options = [
                'primary' => $this->lang->primary_user_group,
                'secondary' => $this->lang->secondary_user_group
            ];

            $form_container->output_row($this->lang->user_group_change_type . ' <em>*</em>', $this->lang->user_group_change_type_desc, $form->generate_select_box('usergroupchangetype', $options, $this->bb->getInput('usergroupchangetype', ''), ['id' => 'usergroupchangetype']), 'usergroupchangetype');

            $form_container->output_row($this->lang->enabled . ' <em>*</em>', '', $form->generate_yes_no_radio('enabled', $this->bb->input['enabled'], true));

            $form_container->output_row($this->lang->enable_logging . ' <em>*</em>', '', $form->generate_yes_no_radio('logging', $this->bb->input['logging'], true));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->update_promotion);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupPromotionsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'logs') {
            $this->plugins->runHooks('admin_user_group_promotions_logs');

            if ($this->bb->getInput('page', 0) > 1) {
                $this->bb->input['page'] = $this->bb->getInput('page', 0);
                $start = ($this->bb->input['page'] * 20) - 20;
            } else {
                $this->bb->input['page'] = 1;
                $start = 0;
            }

            $this->page->add_breadcrumb_item($this->lang->promotion_logs);
            $html = $this->page->output_header($this->lang->user_group_promotions . ' - ' . $this->lang->promotion_logs);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'promotion_logs');

            $table = new Table;
            $table->construct_header($this->lang->promoted_user, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->user_group_change_type, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->orig_user_group, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->new_user_group, ['class' => 'align_center', 'width' => '20%']);
            $table->construct_header($this->lang->time_promoted, ['class' => 'align_center', 'width' => '20%']);

            $query = $this->db->query('
		SELECT pl.*,u.username
		FROM ' . TABLE_PREFIX . 'promotionlogs pl
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=pl.uid)
		ORDER BY dateline DESC
		LIMIT {$start}, 20
	");
            while ($log = $this->db->fetch_array($query)) {
                $log['username'] = "<a href=\"index.php?module=user-view&amp;action=edit&amp;uid={$log['uid']}\">" . htmlspecialchars_uni($log['username']) . "</a>";

                if ($log['type'] == 'secondary' || (!empty($log['oldusergroup']) && strstr(',', $log['oldusergroup']))) {
                    $log['oldusergroup'] = '<i>' . $this->lang->multiple_usergroups . '</i>';
                    $log['newusergroup'] = htmlspecialchars_uni($this->bb->groupscache[$log['newusergroup']]['title']);
                } else {
                    $log['oldusergroup'] = htmlspecialchars_uni($this->bb->groupscache[$log['oldusergroup']]['title']);
                    $log['newusergroup'] = htmlspecialchars_uni($this->bb->groupscache[$log['newusergroup']]['title']);
                }

                if ($log['type'] == 'secondary') {
                    $log['type'] = $this->lang->secondary;
                } else {
                    $log['type'] = $this->lang->primary;
                }

                $log['dateline'] = date($this->bb->settings['dateformat'], $log['dateline']) . ', ' . date($this->bb->settings['timeformat'], $log['dateline']);
                $table->construct_cell($log['username']);
                $table->construct_cell($log['type'], ['style' => 'text-align: center;']);
                $table->construct_cell($log['oldusergroup'], ['style' => 'text-align: center;']);
                $table->construct_cell($log['newusergroup'], ['style' => 'text-align: center;']);
                $table->construct_cell($log['dateline'], ['style' => 'text-align: center;']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_promotion_logs, ['colspan' => '5']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->promotion_logs);

            $query = $this->db->simple_select('promotionlogs', 'COUNT(plid) as promotionlogs');
            $total_rows = $this->db->fetch_field($query, 'promotionlogs');

            $html .= '<br />' . $this->adm->draw_admin_pagination($this->bb->input['page'], '20', $total_rows, $this->bb->admin_url . '/users/group_promotions?action=logs&amp;page={page}');

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupPromotionsLogs.html.twig');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_user_group_promotions_start');

            $html = $this->page->output_header($this->lang->promotion_manager);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'usergroup_promotions');

            $table = new Table;
            $table->construct_header($this->lang->promotion);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 150]);

            $query = $this->db->simple_select('promotions', '*', '', ['order_by' => 'title', 'order_dir' => 'asc']);
            while ($promotion = $this->db->fetch_array($query)) {
                $promotion['title'] = htmlspecialchars_uni($promotion['title']);
                $promotion['description'] = htmlspecialchars_uni($promotion['description']);
                if ($promotion['enabled'] == 1) {
                    $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/bullet_on.png\" alt=\"({$this->lang->alt_enabled})\" title=\"{$this->lang->alt_enabled}\"  style=\"vertical-align: middle;\" /> ";
                } else {
                    $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/bullet_off.png\" alt=\"({$this->lang->alt_disabled})\" title=\"{$this->lang->alt_disabled}\"  style=\"vertical-align: middle;\" /> ";
                }

                $table->construct_cell("<div>{$icon}<strong><a href=\"".$this->bb->admin_url."/users/group_promotions?action=edit&amp;pid={$promotion['pid']}\">{$promotion['title']}</a></strong><br /><small>{$promotion['description']}</small></div>");

                $popup = new PopupMenu("promotion_{$promotion['pid']}", $this->lang->options);
                $popup->add_item($this->lang->edit_promotion, $this->bb->admin_url . '/users/group_promotions?action=edit&amp;pid=' . $promotion['pid']);
                if ($promotion['enabled'] == 1) {
                    $popup->add_item($this->lang->disable_promotion, $this->bb->admin_url . "/users/group_promotions?action=disable&amp;pid={$promotion['pid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_promo_disable}')");
                } else {
                    $popup->add_item($this->lang->enable_promotion, $this->bb->admin_url . "/users/group_promotions?action=enable&amp;pid={$promotion['pid']}&amp;my_post_key={$this->bb->post_code}");
                }
                $popup->add_item($this->lang->delete_promotion, $this->bb->admin_url . "/users/group_promotions?action=delete&amp;pid={$promotion['pid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_promo_deletion}')");
                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_promotions_set, ['colspan' => '2']);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->user_group_promotions);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupPromotions.html.twig');
        }
    }
}
