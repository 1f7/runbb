<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\PopupMenu;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Groups extends AbstractController
{
    // Array of usergroup permission fields and their default values.
    private $usergroup_permissions = [
        'isbannedgroup' => 0,
        'canview' => 1,
        'canviewthreads' => 1,
        'canviewprofiles' => 1,
        'candlattachments' => 1,
        'canviewboardclosed' => 1,
        'canpostthreads' => 1,
        'canpostreplys' => 1,
        'canpostattachments' => 1,
        'canratethreads' => 1,
        'modposts' => 0,
        'modthreads' => 0,
        'modattachments' => 0,
        'mod_edit_posts' => 0,
        'caneditposts' => 1,
        'candeleteposts' => 1,
        'candeletethreads' => 1,
        'caneditattachments' => 1,
        'canpostpolls' => 1,
        'canvotepolls' => 1,
        'canundovotes' => 0,
        'canusepms' => 1,
        'cansendpms' => 1,
        'cantrackpms' => 1,
        'candenypmreceipts' => 1,
        'pmquota' => 100,
        'maxpmrecipients' => 5,
        'cansendemail' => 1,
        'cansendemailoverride' => 0,
        'maxemails' => 4,
        'emailfloodtime' => 5,
        'canviewmemberlist' => 1,
        'canviewcalendar' => 1,
        'canaddevents' => 1,
        'canbypasseventmod' => 0,
        'canmoderateevents' => 0,
        'canviewonline' => 1,
        'canviewwolinvis' => 0,
        'canviewonlineips' => 0,
        'cancp' => 0,
        'issupermod' => 0,
        'cansearch' => 1,
        'canusercp' => 1,
        'canuploadavatars' => 1,
        'canratemembers' => 1,
        'canchangename' => 0,
        'canbereported' => 0,
        'canchangewebsite' => 1,
        'showforumteam' => 0,
        'usereputationsystem' => 1,
        'cangivereputations' => 1,
        'candeletereputations' => 1,
        'reputationpower' => 1,
        'maxreputationsday' => 5,
        'maxreputationsperuser' => 5,
        'maxreputationsperthread' => 5,
        'candisplaygroup' => 0,
        'attachquota' => 5000,
        'cancustomtitle' => 0,
        'canwarnusers' => 0,
        'canreceivewarnings' => 1,
        'maxwarningsday' => 0,
        'canmodcp' => 0,
        'showinbirthdaylist' => 0,
        'canoverridepm' => 0,
        'canusesig' => 0,
        'canusesigxposts' => 0,
        'signofollow' => 0,
        'edittimelimit' => 0,
        'maxposts' => 0,
        'showmemberlist' => 1,
        'canmanageannounce' => 0,
        'canmanagemodqueue' => 0,
        'canmanagereportedcontent' => 0,
        'canviewmodlogs' => 0,
        'caneditprofiles' => 0,
        'canbanusers' => 0,
        'canviewwarnlogs' => 0,
        'canuseipsearch' => 0
    ];

    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_groups', false, true);

        $this->page->add_breadcrumb_item($this->lang->user_groups, $this->bb->admin_url . '/users/groups');
        $errors = [];
        if ($this->bb->input['action'] == 'add' || !$this->bb->input['action']) {
            $sub_tabs['manage_groups'] = [
                'title' => $this->lang->manage_user_groups,
                'link' => $this->bb->admin_url . '/users/groups',
                'description' => $this->lang->manage_user_groups_desc
            ];
            $sub_tabs['add_group'] = [
                'title' => $this->lang->add_user_group,
                'link' => $this->bb->admin_url . '/users/groups?action=add',
                'description' => $this->lang->add_user_group_desc
            ];
        }

        $this->plugins->runHooks('admin_user_groups_begin');

        if ($this->bb->input['action'] == 'approve_join_request') {
            $query = $this->db->simple_select('joinrequests', '*', "rid='" . $this->bb->input['rid'] . "'");
            $request = $this->db->fetch_array($query);

            if (!$request['rid']) {
                $this->session->flash_message($this->lang->error_invalid_join_request, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $request['gid']);
            }

            $this->plugins->runHooks('admin_user_groups_approve_join_request');

            // Add the user to the group
            $this->group->join_usergroup($request['uid'], $request['gid']);

            // Delete the join request
            $this->db->delete_query('joinrequests', "rid='{$request['rid']}'");

            $this->plugins->runHooks('admin_user_groups_approve_join_request_commit');

            $this->session->flash_message($this->lang->success_join_request_approved, 'success');
            return $response->withRedirect($this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $request['gid']);
        }

        if ($this->bb->input['action'] == 'deny_join_request') {
            $query = $this->db->simple_select('joinrequests', '*', "rid='" . $this->bb->input['rid'] . "'");
            $request = $this->db->fetch_array($query);

            if (!$request['rid']) {
                $this->session->flash_message($this->lang->error_invalid_join_request, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $request['gid']);
            }

            $this->plugins->runHooks('admin_user_groups_deny_join_request');

            // Delete the join request
            $this->db->delete_query('joinrequests', "rid='{$request['rid']}'");

            $this->plugins->runHooks('admin_user_groups_deny_join_request_commit');

            $this->session->flash_message($this->lang->success_join_request_denied, 'success');
            return $response->withRedirect($this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $request['gid']);
        }

        if ($this->bb->input['action'] == 'join_requests') {
            $query = $this->db->simple_select('usergroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $group = $this->db->fetch_array($query);

            if (!$group['gid'] || $group['type'] != 4) {
                $this->session->flash_message($this->lang->error_invalid_user_group, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            $this->plugins->runHooks('admin_user_groups_join_requests_start');

            if ($this->bb->request_method == 'post' && is_array($this->bb->input['users'])) {
                $uid_in = implode(',', array_map('intval', $this->bb->input['users']));

                if (isset($this->bb->input['approve'])) {
                    foreach ($this->bb->input['users'] as $uid) {
                        $uid = (int)$uid;
                        $this->group->join_usergroup($uid, $group['gid']);
                    }
                    // Log admin action
                    $this->bblogger->log_admin_action('approve', htmlspecialchars_uni($group['title']), $group['gid']);
                    $message = $this->lang->success_selected_requests_approved;
                } else {
                    // Log admin action
                    $this->bblogger->log_admin_action('deny', htmlspecialchars_uni($group['title']), $group['gid']);
                    $message = $this->lang->success_selected_requests_denied;
                }

                $this->plugins->runHooks('admin_user_groups_join_requests_commit');

                // Go through and delete the join requests from the database
                $this->db->delete_query('joinrequests', "uid IN ({$uid_in}) AND gid='{$group['gid']}'");

                $this->plugins->runHooks('admin_user_groups_join_requests_commit_end');

                $this->session->flash_message($message, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $group['gid']);
            }

            $this->page->add_breadcrumb_item($this->lang->join_requests_for . ' ' . htmlspecialchars_uni($group['title']));
            $html = $this->page->output_header($this->lang->join_requests_for . ' ' . htmlspecialchars_uni($group['title']));

            $sub_tabs = [];
            $sub_tabs['join_requests'] = [
                'title' => $this->lang->group_join_requests,
                'link' => $this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $group['gid'],
                'description' => $this->lang->group_join_requests_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'join_requests');

            $query = $this->db->simple_select('joinrequests', 'COUNT(*) AS num_requests', "gid='{$group['gid']}'");
            $num_requests = $this->db->fetch_field($query, 'num_requests');

            $per_page = 20;

            if ($this->bb->input['page'] > 0) {
                $current_page = $this->bb->getInput('page', 0);
                $start = ($current_page - 1) * $per_page;
                $pages = $num_requests / $per_page;
                $pages = ceil($pages);
                if ($current_page > $pages) {
                    $start = 0;
                    $current_page = 1;
                }
            } else {
                $start = 0;
                $current_page = 1;
            }

            // Do we need to construct the pagination?
            $pagination = '';
            if ($num_requests > $per_page) {
                $pagination = $this->adm->draw_admin_pagination($this->page, $per_page, $num_requests, $this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $group['gid']);
                $html .= $pagination;
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/users/groups?action=join_requests&gid=' . $group['gid'], 'post');
            $html .= $form->getForm();
            $table = new Table;
            $table->construct_header($form->generate_check_box('checkall', 1, '', ['class' => 'checkall']), ['width' => 1]);
            $table->construct_header($this->lang->users);
            $table->construct_header($this->lang->reason);
            $table->construct_header($this->lang->date_requested, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'width' => 200]);

            $query = $this->db->query('
		SELECT j.*, u.username
		FROM ' . TABLE_PREFIX . 'joinrequests j
		INNER JOIN ' . TABLE_PREFIX . "users u ON (u.uid=j.uid)
		WHERE j.gid='{$group['gid']}'
		ORDER BY dateline ASC
		LIMIT {$start}, {$per_page}
	");

            while ($request = $this->db->fetch_array($query)) {
                $table->construct_cell($form->generate_check_box('users[]', $request['uid'], ''));
                $table->construct_cell('<strong>' . $this->user->build_profile_link($request['username'], $request['uid'], '_blank') . '</strong>');
                $table->construct_cell(htmlspecialchars_uni($request['reason']));
                $table->construct_cell($this->time->formatDate('relative', $request['dateline']), ['class' => 'align_center']);

                $popup = new PopupMenu("join_{$request['rid']}", $this->lang->options);
                $popup->add_item($this->lang->approve, $this->bb->admin_url . "/users/groups?action=approve_join_request&amp;rid={$request['rid']}&amp;my_post_key={$this->bb->post_code}");
                $popup->add_item($this->lang->deny, $this->bb->admin_url . "/users/groups?action=deny_join_request&amp;rid={$request['rid']}&amp;my_post_key={$this->bb->post_code}");

                $table->construct_cell($popup->fetch(), ['class' => 'align_center']);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_join_requests, ['colspan' => 6]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->join_requests_for . ' ' . htmlspecialchars_uni($group['title']));
            $html .= $pagination;

            $buttons[] = $form->generate_submit_button($this->lang->approve_selected_requests, ['name' => 'approve']);
            $buttons[] = $form->generate_submit_button($this->lang->deny_selected_requests, ['name' => 'deny']);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupsJoinRequests.html.twig');
        }

        if ($this->bb->input['action'] == 'add_leader' && $this->bb->request_method == 'post') {
            $query = $this->db->simple_select('usergroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $group = $this->db->fetch_array($query);

            if (!$group['gid']) {
                $this->session->flash_message($this->lang->error_invalid_user_group, 'error');
                return $response->withRedirect('index.php?module=user-group');
            }

            $this->plugins->runHooks('admin_user_groups_add_leader');

            $user = $this->user->get_user_by_username($this->bb->input['username'], ['fields' => 'username']);
            if (!$user['uid']) {
                $errors[] = $this->lang->error_invalid_username;
            } else {
                // Is this user already a leader of this group?
                $query = $this->db->simple_select('groupleaders', 'uid', "uid='{$user['uid']}' AND gid='{$group['gid']}'");
                $existing_leader = $this->db->fetch_field($query, 'uid');
                if ($existing_leader) {
                    $errors[] = $this->lang->error_already_leader;
                }
            }

            // No errors, insert
            if (!$errors) {
                $new_leader = [
                    'gid' => $group['gid'],
                    'uid' => $user['uid'],
                    'canmanagemembers' => $this->bb->getInput('canmanagemembers', 0),
                    'canmanagerequests' => $this->bb->getInput('canmanagerequests', 0),
                    'caninvitemembers' => $this->bb->getInput('caninvitemembers', 0)
                ];

                $makeleadermember = $this->bb->getInput('makeleadermember', 0);
                if ($makeleadermember == 1) {
                    $this->group->join_usergroup($user['uid'], $group['gid']);
                }

                $this->plugins->runHooks('admin_user_groups_add_leader_commit');

                $this->db->insert_query('groupleaders', $new_leader);

                $this->cache->update_groupleaders();

                // Log admin action
                $this->bblogger->log_admin_action($user['uid'], $user['username'], $group['gid'], htmlspecialchars_uni($group['title']));

                $this->session->flash_message("{$user['username']} " . $this->lang->success_user_made_leader, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/groups?action=leaders&gid=' . $group['gid']);
            } else {
                // Errors, show leaders page
                $this->bb->input['action'] = 'leaders';
            }
        }

        // Show a listing of group leaders
        if ($this->bb->input['action'] == 'leaders') {
            $query = $this->db->simple_select('usergroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $group = $this->db->fetch_array($query);

            if (!$group['gid']) {
                $this->session->flash_message($this->lang->error_invalid_user_group, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            $this->plugins->runHooks('admin_user_groups_leaders');

            $this->page->add_breadcrumb_item($this->lang->group_leaders_for . ' ' . htmlspecialchars_uni($group['title']));
            $html = $this->page->output_header($this->lang->group_leaders_for . ' ' . htmlspecialchars_uni($group['title']));

            $sub_tabs = [];
            $sub_tabs['group_leaders'] = [
                'title' => $this->lang->manage_group_leaders,
                'link' => $this->bb->admin_url . '/users/groups?action=leaders&gid=' . $group['gid'],
                'description' => $this->lang->manage_group_leaders_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'group_leaders');

            $table = new Table;
            $table->construct_header($this->lang->user);
            $table->construct_header($this->lang->can_manage_members, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->can_manage_join_requests, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->can_invite_members, ['class' => 'align_center', 'width' => 200]);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 200]);

            $query = $this->db->query('
		SELECT g.*, u.username
		FROM ' . TABLE_PREFIX . 'groupleaders g
		INNER JOIN ' . TABLE_PREFIX . "users u ON (u.uid=g.uid)
		WHERE g.gid='{$group['gid']}'
		ORDER BY u.username ASC
	");
            while ($leader = $this->db->fetch_array($query)) {
                $leader['username'] = htmlspecialchars_uni($leader['username']);
                if ($leader['canmanagemembers']) {
                    $canmanagemembers = $this->lang->yes;
                } else {
                    $canmanagemembers = $this->lang->no;
                }

                if ($leader['canmanagerequests']) {
                    $canmanagerequests = $this->lang->yes;
                } else {
                    $canmanagerequests = $this->lang->no;
                }

                if ($leader['caninvitemembers']) {
                    $caninvitemembers = $this->lang->yes;
                } else {
                    $caninvitemembers = $this->lang->no;
                }

                $table->construct_cell('<strong>' . $this->user->build_profile_link($leader['username'], $leader['uid'], '_blank') . '</strong>');
                $table->construct_cell($canmanagemembers, ['class' => 'align_center']);
                $table->construct_cell($canmanagerequests, ['class' => 'align_center']);
                $table->construct_cell($caninvitemembers, ['class' => 'align_center']);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/groups?action=edit_leader&lid={$leader['lid']}\">{$this->lang->edit}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/groups?action=delete_leader&amp;lid={$leader['lid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_group_leader_deletion}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_assigned_leaders, ['colspan' => 5]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->group_leaders_for . ' ' . htmlspecialchars_uni($group['title']));

            $form = new Form($this->bb, $this->bb->admin_url . '/users/groups?action=add_leader&gid=' . $group['gid'], 'post');
            $html .= $form->getForm();

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, [
                        'canmanagemembers' => 1,
                        'canmanagerequests' => 1,
                        'caninvitemembers' => 1,
                        'makeleadermember' => 0
                    ]);
            }
            $uname = isset($this->bb->input['username']) ? $this->bb->input['username'] : '';
            $form_container = new FormContainer($this, $this->lang->add_group_leader . ' ' . htmlspecialchars_uni($group['title']));
            $form_container->output_row($this->lang->username . ' <em>*</em>', '', $form->generate_text_box('username', $uname, ['id' => 'username']), 'username');
            $form_container->output_row($this->lang->can_manage_group_members, $this->lang->can_manage_group_members_desc, $form->generate_yes_no_radio('canmanagemembers', $this->bb->input['canmanagemembers']));
            $form_container->output_row($this->lang->can_manage_group_join_requests, $this->lang->can_manage_group_join_requests_desc, $form->generate_yes_no_radio('canmanagerequests', $this->bb->input['canmanagerequests']));
            $form_container->output_row($this->lang->can_invite_group_members, $this->lang->can_invite_group_members_desc, $form->generate_yes_no_radio('caninvitemembers', $this->bb->input['caninvitemembers']));
            $form_container->output_row($this->lang->make_user_member, $this->lang->make_user_member_desc, $form->generate_yes_no_radio('makeleadermember', $this->bb->input['makeleadermember']));
            $html .= $form_container->end();

            // Autocompletion for usernames
            $html .= '
	<link rel="stylesheet" href="' . $this->bb->asset_url . '/jscripts/select2/select2.css">
	<script type="text/javascript" src="' . $this->bb->asset_url . '/jscripts/select2/select2.min.js?ver=1804"></script>
	<script type="text/javascript">
	<!--
	$("#username").select2({
		placeholder: "' . $this->lang->search_for_a_user . '",
		minimumInputLength: 2,
		multiple: false,
		ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
			url: "' . $this->bb->settings['bburl'] . '/xmlhttp?action=get_users",
			dataType: "json",
			data: function (term, page) {
				return {
					query: term // search term
				};
			},
			results: function (data, page) { // parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to alter remote JSON data
				return {results: data};
			}
		},
		initSelection: function(element, callback) {
			var query = $(element).val();
			if (query !== "") {
				$.ajax("' . $this->bb->settings['bburl'] . '/xmlhttp?action=get_users&getone=1", {
					data: {
						query: query
					},
					dataType: "json"
				}).done(function(data) { callback(data); });
			}
		}
	});
	// -->
	</script>';

            $buttons[] = $form->generate_submit_button($this->lang->save_group_leader);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupsLeaders.html.twig');
        }

        if ($this->bb->input['action'] == 'delete_leader') {
            $query = $this->db->query('
		SELECT l.*, u.username
		FROM ' . TABLE_PREFIX . 'groupleaders l
		INNER JOIN ' . TABLE_PREFIX . "users u ON (u.uid=l.uid)
		WHERE l.lid='" . $this->bb->getInput('lid', 0) . "'");
            $leader = $this->db->fetch_array($query);

            if (!$leader['lid']) {
                $this->session->flash_message($this->lang->error_invalid_group_leader, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            $query = $this->db->simple_select('usergroups', '*', "gid='{$leader['gid']}'");
            $group = $this->db->fetch_array($query);

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            $this->plugins->runHooks('admin_user_groups_delete_leader');

            if ($this->bb->request_method == 'post') {
                $this->plugins->runHooks('admin_user_groups_delete_leader_commit');

                // Delete the leader
                $this->db->delete_query('groupleaders', "lid='{$leader['lid']}'");

                $this->plugins->runHooks('admin_user_groups_delete_leader_commit_end');

                $this->cache->update_groupleaders();

                // Log admin action
                $this->bblogger->log_admin_action($leader['uid'], $leader['username'], $group['gid'], htmlspecialchars_uni($group['title']));

                $this->session->flash_message($this->lang->success_group_leader_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/groups?action=leaders&gid='.$group['gid']);
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/groups?action=delete_leader&lid='.$leader['lid'], $this->lang->confirm_group_leader_deletion);
            }
        }

        if ($this->bb->input['action'] == 'edit_leader') {
            $query = $this->db->query('
		SELECT l.*, u.username
		FROM ' . TABLE_PREFIX . 'groupleaders l
		INNER JOIN ' . TABLE_PREFIX . "users u ON (u.uid=l.uid)
		WHERE l.lid='" . $this->bb->getInput('lid', 0) . "'
	");
            $leader = $this->db->fetch_array($query);

            if (!$leader['lid']) {
                $this->session->flash_message($this->lang->error_invalid_group_leader, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            $query = $this->db->simple_select('usergroups', '*', "gid='{$leader['gid']}'");
            $group = $this->db->fetch_array($query);

            $this->plugins->runHooks('admin_user_groups_edit_leader');

            if ($this->bb->request_method == 'post') {
                $updated_leader = [
                    'canmanagemembers' => $this->bb->getInput('canmanagemembers', 0),
                    'canmanagerequests' => $this->bb->getInput('canmanagerequests', 0),
                    'caninvitemembers' => $this->bb->getInput('caninvitemembers', 0)
                ];

                $this->plugins->runHooks('admin_user_groups_edit_leader_commit');

                $this->db->update_query('groupleaders', $updated_leader, "lid={$leader['lid']}");

                $this->cache->update_groupleaders();

                // Log admin action
                $this->bblogger->log_admin_action($leader['uid'], $leader['username'], $group['gid'], htmlspecialchars_uni($group['title']));

                $this->session->flash_message($this->lang->success_group_leader_updated, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/groups?action=leaders&gid='.$group['gid']);
            }

            if (!$errors) {
                $this->bb->input = array_merge($this->bb->input, $leader);
            }

            $this->page->add_breadcrumb_item($this->lang->group_leaders_for . ' ' . htmlspecialchars_uni($group['title']), $this->bb->admin_url . '/users/groups?action=leaders&gid='.$group['gid']);
            $this->page->add_breadcrumb_item($this->lang->edit_leader . " {$leader['username']}");

            $html = $this->page->output_header($this->lang->edit_group_leader);

            $sub_tabs = [];
            $sub_tabs['group_leaders'] = [
                'title' => $this->lang->edit_group_leader,
                'link' => $this->bb->admin_url . '/users/groups?action=edit_leader&lid=' . $leader['lid'],
                'description' => $this->lang->edit_group_leader_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'group_leaders');

            $form = new Form($this->bb, $this->bb->admin_url . '/users/groups?action=edit_leader&lid='.$leader['lid'], 'post');
            $html .= $form->getForm();

            $form_container = new FormContainer($this, $this->lang->edit_group_leader);
            $form_container->output_row($this->lang->username . ' <em>*</em>', '', $leader['username']);

            $form_container->output_row($this->lang->can_manage_group_members, $this->lang->can_manage_group_members_desc, $form->generate_yes_no_radio('canmanagemembers', $this->bb->input['canmanagemembers']));
            $form_container->output_row($this->lang->can_manage_group_join_requests, $this->lang->can_manage_group_join_requests_desc, $form->generate_yes_no_radio('canmanagerequests', $this->bb->input['canmanagerequests']));
            $form_container->output_row($this->lang->can_invite_group_members, $this->lang->can_invite_group_members_desc, $form->generate_yes_no_radio('caninvitemembers', $this->bb->input['caninvitemembers']));
            $buttons[] = $form->generate_submit_button($this->lang->save_group_leader);

            $html .= $form_container->end();
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupsEditLeader.html.twig');
        }

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_user_groups_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (my_strpos($this->bb->input['namestyle'], '{username}') === false) {
                    $errors[] = $this->lang->error_missing_namestyle_username;
                }

                if (!$errors) {
                    if (isset($this->bb->input['stars']) && $this->bb->input['stars'] < 1) {
                        $this->bb->input['stars'] = 0;
                    }

                    if (!isset($this->bb->input['starimage'])) {
                        $this->bb->input['starimage'] = 'images/star.png';
                    }

                    $new_usergroup = [
                        'type' => 2,
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'namestyle' => $this->db->escape_string($this->bb->input['namestyle']),
                        'usertitle' => $this->db->escape_string($this->bb->input['usertitle']),
                        'stars' => $this->bb->getInput('stars', 0),
                        'starimage' => $this->db->escape_string($this->bb->input['starimage']),
                        'disporder' => 0
                    ];

                    // Set default permissions
                    if ($this->bb->input['copyfrom'] == 0) {
                        $new_usergroup = array_merge($new_usergroup, $this->usergroup_permissions);
                    } // Copying permissions from another group
                    else {
                        $query = $this->db->simple_select('usergroups', '*', "gid='" . $this->bb->getInput('copyfrom', 0) . "'");
                        $existing_usergroup = $this->db->fetch_array($query);
                        foreach (array_keys($this->usergroup_permissions) as $field) {
                            $new_usergroup[$field] = $existing_usergroup[$field];
                        }
                    }

                    $this->plugins->runHooks('admin_user_groups_add_commit');

                    $gid = $this->db->insert_query('usergroups', $new_usergroup);

                    $this->plugins->runHooks('admin_user_groups_add_commit_end');

                    // Are we copying permissions? If so, copy all forum permissions too
                    if ($this->bb->input['copyfrom'] > 0) {
                        $query = $this->db->simple_select('forumpermissions', '*', "gid='" . $this->bb->getInput('copyfrom', 0) . "'");
                        while ($forum_permission = $this->db->fetch_array($query)) {
                            unset($forum_permission['pid']);
                            $forum_permission['gid'] = $gid;
                            $this->db->insert_query('forumpermissions', $forum_permission);
                        }
                    }

                    // Update the caches
                    $this->cache->update_usergroups();
                    $this->cache->update_forumpermissions();

                    // Log admin action
                    $this->bblogger->log_admin_action($gid, htmlspecialchars_uni($this->bb->input['title']));

                    $this->session->flash_message($this->lang->success_group_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/groups?action=edit&gid='.$gid);
                }
            }

            $this->page->add_breadcrumb_item($this->lang->add_user_group);
            $html = $this->page->output_header($this->lang->add_user_group);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_group');
            $form = new Form($this->bb, $this->bb->admin_url . '/users/groups?action=add', 'post');
            $html .= $form->getForm();

            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge(
                    $this->bb->input,
                    [
                        'namestyle' => '{username}'
                    ]
                );
            }

            $form_container = new FormContainer($this, $this->lang->add_user_group);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description, '', $form->generate_text_box('description', $this->bb->getInput('description', '', true), ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->username_style, $this->lang->username_style_desc, $form->generate_text_box('namestyle', $this->bb->getInput('namestyle', ''), ['id' => 'namestyle']), 'namestyle');
            $form_container->output_row($this->lang->user_title, $this->lang->user_title_desc, $form->generate_text_box('usertitle', $this->bb->getInput('usertitle', ''), ['id' => 'usertitle']), 'usertitle');

            $options[0] = $this->lang->do_not_copy_permissions;
            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
            }
            $form_container->output_row($this->lang->copy_permissions_from, $this->lang->copy_permissions_from_desc, $form->generate_select_box('copyfrom', $options, $this->bb->getInput('copyfrom', ''), ['id' => 'copyfrom']), 'copyfrom');

            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->save_user_group);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupsAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('usergroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $usergroup = $this->db->fetch_array($query);

            if (!$usergroup['gid']) {
                $this->session->flash_message($this->lang->error_invalid_user_group, 'error');
                return $response->withRedirect('index.php?module=user-group');
            } elseif (isset($this->bb->input['namestyle'])) {
                if (preg_match('#<((m[^a])|(b[^diloru>])|(s[^aemptu>]))(\s*[^>]*)>#si', $this->bb->input['namestyle'])) {
                    $errors[] = $this->lang->error_disallowed_namestyle_username;
                    $this->bb->input['namestyle'] = $usergroup['namestyle'];
                }
            }

            $this->plugins->runHooks('admin_user_groups_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (my_strpos($this->bb->input['namestyle'], '{username}') === false) {
                    $errors[] = $this->lang->error_missing_namestyle_username;
                }

                if (isset($this->bb->input['moderate']) && $this->bb->input['moderate'] == 1 && $this->bb->input['invite'] == 1) {
                    $errors[] = $this->lang->error_cannot_have_both_types;
                }

                if (!$errors) {
                    if (isset($this->bb->input['joinable']) && $this->bb->input['joinable'] == 1) {
                        if ($this->bb->input['moderate'] == 1) {
                            $this->bb->input['type'] = '4';
                        } elseif ($this->bb->input['invite'] == 1) {
                            $this->bb->input['type'] = '5';
                        } else {
                            $this->bb->input['type'] = '3';
                        }
                    } else {
                        $this->bb->input['type'] = '2';
                    }

                    if ($usergroup['type'] == 1) {
                        $this->bb->input['type'] = 1;
                    }

                    if ($this->bb->input['stars'] < 1) {
                        $this->bb->input['stars'] = 0;
                    }

                    $updated_group = [
                        'type' => $this->bb->getInput('type', 0),
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'description' => $this->db->escape_string($this->bb->input['description']),
                        'namestyle' => $this->db->escape_string($this->bb->input['namestyle']),
                        'usertitle' => $this->db->escape_string($this->bb->input['usertitle']),
                        'stars' => $this->bb->getInput('stars', 0),
                        'starimage' => $this->db->escape_string($this->bb->input['starimage']),
                        'image' => $this->db->escape_string($this->bb->input['image']),
                        'isbannedgroup' => $this->bb->getInput('isbannedgroup', 0),
                        'canview' => $this->bb->getInput('canview', 0),
                        'canviewthreads' => $this->bb->getInput('canviewthreads', 0),
                        'canviewprofiles' => $this->bb->getInput('canviewprofiles', 0),
                        'candlattachments' => $this->bb->getInput('candlattachments', 0),
                        'canviewboardclosed' => $this->bb->getInput('canviewboardclosed', 0),
                        'canpostthreads' => $this->bb->getInput('canpostthreads', 0),
                        'canpostreplys' => $this->bb->getInput('canpostreplys', 0),
                        'canpostattachments' => $this->bb->getInput('canpostattachments', 0),
                        'canratethreads' => $this->bb->getInput('canratethreads', 0),
                        'modposts' => $this->bb->getInput('modposts', 0),
                        'modthreads' => $this->bb->getInput('modthreads', 0),
                        'mod_edit_posts' => $this->bb->getInput('mod_edit_posts', 0),
                        'modattachments' => $this->bb->getInput('modattachments', 0),
                        'caneditposts' => $this->bb->getInput('caneditposts', 0),
                        'candeleteposts' => $this->bb->getInput('candeleteposts', 0),
                        'candeletethreads' => $this->bb->getInput('candeletethreads', 0),
                        'caneditattachments' => $this->bb->getInput('caneditattachments', 0),
                        'canpostpolls' => $this->bb->getInput('canpostpolls', 0),
                        'canvotepolls' => $this->bb->getInput('canvotepolls', 0),
                        'canundovotes' => $this->bb->getInput('canundovotes', 0),
                        'canusepms' => $this->bb->getInput('canusepms', 0),
                        'cansendpms' => $this->bb->getInput('cansendpms', 0),
                        'cantrackpms' => $this->bb->getInput('cantrackpms', 0),
                        'candenypmreceipts' => $this->bb->getInput('candenypmreceipts', 0),
                        'pmquota' => $this->bb->getInput('pmquota', 0),
                        'maxpmrecipients' => $this->bb->getInput('maxpmrecipients', 0),
                        'cansendemail' => $this->bb->getInput('cansendemail', 0),
                        'cansendemailoverride' => $this->bb->getInput('cansendemailoverride', 0),
                        'maxemails' => $this->bb->getInput('maxemails', 0),
                        'emailfloodtime' => $this->bb->getInput('emailfloodtime', 0),
                        'canviewmemberlist' => $this->bb->getInput('canviewmemberlist', 0),
                        'canviewcalendar' => $this->bb->getInput('canviewcalendar', 0),
                        'canaddevents' => $this->bb->getInput('canaddevents', 0),
                        'canbypasseventmod' => $this->bb->getInput('canbypasseventmod', 0),
                        'canmoderateevents' => $this->bb->getInput('canmoderateevents', 0),
                        'canviewonline' => $this->bb->getInput('canviewonline', 0),
                        'canviewwolinvis' => $this->bb->getInput('canviewwolinvis', 0),
                        'canviewonlineips' => $this->bb->getInput('canviewonlineips', 0),
                        'cancp' => $this->bb->getInput('cancp', 0),
                        'issupermod' => $this->bb->getInput('issupermod', 0),
                        'cansearch' => $this->bb->getInput('cansearch', 0),
                        'canusercp' => $this->bb->getInput('canusercp', 0),
                        'canuploadavatars' => $this->bb->getInput('canuploadavatars', 0),
                        'canchangename' => $this->bb->getInput('canchangename', 0),
                        'canbereported' => $this->bb->getInput('canbereported', 0),
                        'canchangewebsite' => $this->bb->getInput('canchangewebsite', 0),
                        'showforumteam' => $this->bb->getInput('showforumteam', 0),
                        'usereputationsystem' => $this->bb->getInput('usereputationsystem', 0),
                        'cangivereputations' => $this->bb->getInput('cangivereputations', 0),
                        'candeletereputations' => $this->bb->getInput('candeletereputations', 0),
                        'reputationpower' => $this->bb->getInput('reputationpower', 0),
                        'maxreputationsday' => $this->bb->getInput('maxreputationsday', 0),
                        'maxreputationsperuser' => $this->bb->getInput('maxreputationsperuser', 0),
                        'maxreputationsperthread' => $this->bb->getInput('maxreputationsperthread', 0),
                        'attachquota' => $this->bb->getInput('attachquota', 0),
                        'cancustomtitle' => $this->bb->getInput('cancustomtitle', 0),
                        'canwarnusers' => $this->bb->getInput('canwarnusers', 0),
                        'canreceivewarnings' => $this->bb->getInput('canreceivewarnings', 0),
                        'maxwarningsday' => $this->bb->getInput('maxwarningsday', 0),
                        'canmodcp' => $this->bb->getInput('canmodcp', 0),
                        'showinbirthdaylist' => $this->bb->getInput('showinbirthdaylist', 0),
                        'canoverridepm' => $this->bb->getInput('canoverridepm', 0),
                        'canusesig' => $this->bb->getInput('canusesig', 0),
                        'canusesigxposts' => $this->bb->getInput('canusesigxposts', 0),
                        'signofollow' => $this->bb->getInput('signofollow', 0),
                        'edittimelimit' => $this->bb->getInput('edittimelimit', 0),
                        'maxposts' => $this->bb->getInput('maxposts', 0),
                        'showmemberlist' => $this->bb->getInput('showmemberlist', 0),
                        'canmanageannounce' => $this->bb->getInput('canmanageannounce', 0),
                        'canmanagemodqueue' => $this->bb->getInput('canmanagemodqueue', 0),
                        'canmanagereportedcontent' => $this->bb->getInput('canmanagereportedcontent', 0),
                        'canviewmodlogs' => $this->bb->getInput('canviewmodlogs', 0),
                        'caneditprofiles' => $this->bb->getInput('caneditprofiles', 0),
                        'canbanusers' => $this->bb->getInput('canbanusers', 0),
                        'canviewwarnlogs' => $this->bb->getInput('canviewwarnlogs', 0),
                        'canuseipsearch' => $this->bb->getInput('canuseipsearch', 0)
                    ];

                    // Only update the candisplaygroup setting if not a default user group
                    if ($usergroup['type'] != 1) {
                        $updated_group['candisplaygroup'] = $this->bb->getInput('candisplaygroup', 0);
                    }

                    $this->plugins->runHooks('admin_user_groups_edit_commit');

                    $this->db->update_query('usergroups', $updated_group, "gid='{$usergroup['gid']}'");

                    // Update the caches
                    $this->cache->update_usergroups();
                    $this->cache->update_forumpermissions();

                    // Log admin action
                    $this->bblogger->log_admin_action($usergroup['gid'], htmlspecialchars_uni($this->bb->input['title']));

                    $this->session->flash_message($this->lang->success_group_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/groups');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_user_group);
            $html = $this->page->output_header($this->lang->edit_user_group);

            $sub_tabs = [];
            $sub_tabs['edit_group'] = [
                'title' => $this->lang->edit_user_group,
                'description' => $this->lang->edit_user_group_desc
            ];

            $form = new Form($this->bb, $this->bb->admin_url . '/users/groups?action=edit&gid='.$usergroup['gid'], 'post');
            $html .= $form->getForm();

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_group');

            // If we have any error messages, show them
            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                if ($usergroup['type'] == '3') {
                    $usergroup['joinable'] = 1;
                    $usergroup['moderate'] = 0;
                    $usergroup['invite'] = 0;
                } elseif ($usergroup['type'] == '4') {
                    $usergroup['joinable'] = 1;
                    $usergroup['moderate'] = 1;
                    $usergroup['invite'] = 0;
                } elseif ($usergroup['type'] == '5') {
                    $usergroup['joinable'] = 1;
                    $usergroup['moderate'] = 0;
                    $usergroup['invite'] = 1;
                } else {
                    $usergroup['joinable'] = 0;
                    $usergroup['moderate'] = 0;
                    $usergroup['invite'] = 0;
                }
                $this->bb->input = array_merge($this->bb->input, $usergroup);
            }
            $tabs = [
                'general' => $this->lang->general,
                'forums_posts' => $this->lang->forums_posts,
                'users_permissions' => $this->lang->users_permissions,
                'misc' => $this->lang->misc,
                'modcp' => $this->lang->mod_cp
            ];
            $tabs = $this->plugins->runHooks('admin_user_groups_edit_graph_tabs', $tabs);
            $html .= $this->page->output_tab_control($tabs);

            $html .= '<div id="tab_general">';
            $form_container = new FormContainer($this, $this->lang->general);
            $form_container->output_row($this->lang->title . ' <em>*</em>', '', $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->short_description, '', $form->generate_text_box('description', $this->bb->input['description'], ['id' => 'description']), 'description');
            $form_container->output_row($this->lang->username_style, $this->lang->username_style_desc, $form->generate_text_box('namestyle', $this->bb->input['namestyle'], ['id' => 'namestyle']), 'namestyle');
            $form_container->output_row($this->lang->user_title, $this->lang->user_title_desc, $form->generate_text_box('usertitle', $this->bb->input['usertitle'], ['id' => 'usertitle']), 'usertitle');

            $stars = '<table cellpadding="3"><tr><td>' . $form->generate_numeric_field('stars', $this->bb->input['stars'], ['class' => 'field50', 'id' => 'stars', 'min' => 0]) . '</td><td>' . $form->generate_text_box('starimage', $this->bb->input['starimage'], ['id' => 'starimage']) . '</td></tr>';
            $stars .= "<tr><td><small>{$this->lang->stars}</small></td><td><small>{$this->lang->star_image}</small></td></tr></table>";
            $form_container->output_row($this->lang->user_stars, $this->lang->user_stars_desc, $stars, 'stars');

            $form_container->output_row($this->lang->group_image, $this->lang->group_image_desc, $form->generate_text_box('image', $this->bb->input['image'], ['id' => 'image']), 'image');

            $general_options = [];
            $general_options[] = $form->generate_check_box('showmemberlist', 1, $this->lang->member_list, ['checked' => $this->bb->input['showmemberlist']]);
            if ($usergroup['gid'] != '1' && $usergroup['gid'] != '5') {
                $general_options[] = $form->generate_check_box('showforumteam', 1, $this->lang->forum_team, ['checked' => $this->bb->input['showforumteam']]);
            }
            $general_options[] = $form->generate_check_box('isbannedgroup', 1, $this->lang->is_banned_group, ['checked' => $this->bb->input['isbannedgroup']]);

            $form_container->output_row($this->lang->general_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $general_options) . '</div>');

            if ($usergroup['type'] != 1) {
                $public_options = [
                    $form->generate_check_box('joinable', 1, $this->lang->user_joinable, ['checked' => $this->bb->input['joinable']]),
                    $form->generate_check_box('moderate', 1, $this->lang->moderate_join_requests, ['checked' => $this->bb->input['moderate']]),
                    $form->generate_check_box('invite', 1, $this->lang->invite_only, ['checked' => $this->bb->input['invite']]),
                    $form->generate_check_box('candisplaygroup', 1, $this->lang->can_set_as_display_group, ['checked' => $this->bb->input['candisplaygroup']]),
                ];
                $form_container->output_row($this->lang->publicly_joinable_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $public_options) . "</div>");
            }

            $admin_options = [
                $form->generate_check_box('issupermod', 1, $this->lang->is_super_mod, ['checked' => $this->bb->input['issupermod']]),
                $form->generate_check_box('canmodcp', 1, $this->lang->can_access_mod_cp, ['checked' => $this->bb->input['canmodcp']]),
                $form->generate_check_box('cancp', 1, $this->lang->can_access_admin_cp, ['checked' => $this->bb->input['cancp']])
            ];
            $form_container->output_row($this->lang->moderation_administration_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $admin_options) . "</div>");

            $html .= $form_container->end();
            $html .= '</div>';

            //
            // FORUMS AND POSTS
            //
            $html .= '<div id="tab_forums_posts">';
            $form_container = new FormContainer($this, $this->lang->forums_posts);

            $viewing_options = [
                $form->generate_check_box('canview', 1, $this->lang->can_view_board, ['checked' => $this->bb->input['canview']]),
                $form->generate_check_box('canviewthreads', 1, $this->lang->can_view_threads, ['checked' => $this->bb->input['canviewthreads']]),
                $form->generate_check_box('cansearch', 1, $this->lang->can_search_forums, ['checked' => $this->bb->input['cansearch']]),
                $form->generate_check_box('canviewprofiles', 1, $this->lang->can_view_profiles, ['checked' => $this->bb->input['canviewprofiles']]),
                $form->generate_check_box('candlattachments', 1, $this->lang->can_download_attachments, ['checked' => $this->bb->input['candlattachments']]),
                $form->generate_check_box('canviewboardclosed', 1, $this->lang->can_view_board_closed, ['checked' => $this->bb->input['canviewboardclosed']])
            ];
            $form_container->output_row($this->lang->viewing_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $viewing_options) . '</div>');

            $posting_options = [
                $form->generate_check_box('canpostthreads', 1, $this->lang->can_post_threads, ['checked' => $this->bb->input['canpostthreads']]),
                $form->generate_check_box('canpostreplys', 1, $this->lang->can_post_replies, ['checked' => $this->bb->input['canpostreplys']]),
                $form->generate_check_box('canratethreads', 1, $this->lang->can_rate_threads, ['checked' => $this->bb->input['canratethreads']]),
                "{$this->lang->max_posts_per_day}<br /><small class=\"input\">{$this->lang->max_posts_per_day_desc}</small><br />" . $form->generate_numeric_field('maxposts', $this->bb->input['maxposts'], ['id' => 'maxposts', 'class' => 'field50', 'min' => 0])
            ];
            $form_container->output_row($this->lang->posting_rating_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $posting_options) . '</div>');

            $moderator_options = [
                $form->generate_check_box('modposts', 1, $this->lang->mod_new_posts, ['checked' => $this->bb->input['modposts']]),
                $form->generate_check_box('modthreads', 1, $this->lang->mod_new_threads, ['checked' => $this->bb->input['modthreads']]),
                $form->generate_check_box('modattachments', 1, $this->lang->mod_new_attachments, ['checked' => $this->bb->input['modattachments']]),
                $form->generate_check_box('mod_edit_posts', 1, $this->lang->mod_after_edit, ['checked' => $this->bb->input['mod_edit_posts']])
            ];
            $form_container->output_row($this->lang->moderation_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $moderator_options) . '</div>');

            $poll_options = [
                $form->generate_check_box('canpostpolls', 1, $this->lang->can_post_polls, ['checked' => $this->bb->input['canpostpolls']]),
                $form->generate_check_box('canvotepolls', 1, $this->lang->can_vote_polls, ['checked' => $this->bb->input['canvotepolls']]),
                $form->generate_check_box('canundovotes', 1, $this->lang->can_undo_votes, ['checked' => $this->bb->input['canundovotes']])
            ];
            $form_container->output_row($this->lang->poll_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $poll_options) . '</div>');

            $attachment_options = [
                $form->generate_check_box('canpostattachments', 1, $this->lang->can_post_attachments, ['checked' => $this->bb->input['canpostattachments']]),
                "{$this->lang->attach_quota}<br /><small class=\"input\">{$this->lang->attach_quota_desc}</small><br />" . $form->generate_numeric_field('attachquota', $this->bb->input['attachquota'], ['id' => 'attachquota', 'class' => 'field50', 'min' => 0]) . "KB"
            ];
            $form_container->output_row($this->lang->attachment_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $attachment_options) . '</div>');

            $editing_options = [
                $form->generate_check_box('caneditposts', 1, $this->lang->can_edit_posts, ['checked' => $this->bb->input['caneditposts']]),
                $form->generate_check_box('candeleteposts', 1, $this->lang->can_delete_posts, ['checked' => $this->bb->input['candeleteposts']]),
                $form->generate_check_box('candeletethreads', 1, $this->lang->can_delete_threads, ['checked' => $this->bb->input['candeletethreads']]),
                $form->generate_check_box('caneditattachments', 1, $this->lang->can_edit_attachments, ['checked' => $this->bb->input['caneditattachments']]),
                "{$this->lang->edit_time_limit}<br /><small class=\"input\">{$this->lang->edit_time_limit_desc}</small><br />" . $form->generate_numeric_field('edittimelimit', $this->bb->input['edittimelimit'], ['id' => 'edittimelimit', 'class' => 'field50', 'min' => 0])
            ];
            $form_container->output_row($this->lang->editing_deleting_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $editing_options) . '</div>');

            $html .= $form_container->end();
            $html .= '</div>';

            //
            // USERS AND PERMISSIONS
            //
            $html .= '<div id="tab_users_permissions">';
            $form_container = new FormContainer($this, $this->lang->users_permissions);

            $account_options = [
                $form->generate_check_box('canbereported', 1, $this->lang->can_be_reported, ['checked' => $this->bb->input['canbereported']]),
                $form->generate_check_box('canusercp', 1, $this->lang->can_access_usercp, ['checked' => $this->bb->input['canusercp']]),
                $form->generate_check_box('canchangename', 1, $this->lang->can_change_username, ['checked' => $this->bb->input['canchangename']]),
                $form->generate_check_box('cancustomtitle', 1, $this->lang->can_use_usertitles, ['checked' => $this->bb->input['cancustomtitle']]),
                $form->generate_check_box('canuploadavatars', 1, $this->lang->can_upload_avatars, ['checked' => $this->bb->input['canuploadavatars']]),
                $form->generate_check_box('canusesig', 1, $this->lang->can_use_signature, ['checked' => $this->bb->input['canusesig']]),
                $form->generate_check_box('signofollow', 1, $this->lang->uses_no_follow, ['checked' => $this->bb->input['signofollow']]),
                $form->generate_check_box('canchangewebsite', 1, $this->lang->can_change_website, ['checked' => $this->bb->input['canchangewebsite']]),
                "{$this->lang->required_posts}<br /><small class=\"input\">{$this->lang->required_posts_desc}</small><br />" . $form->generate_numeric_field('canusesigxposts', $this->bb->input['canusesigxposts'], ['id' => 'canusesigxposts', 'class' => 'field50', 'min' => 0])
            ];
            $form_container->output_row($this->lang->account_management, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $account_options) . '</div>');

            $reputation_options = [
                $form->generate_check_box('usereputationsystem', 1, $this->lang->show_reputations, ['checked' => $this->bb->input['usereputationsystem']]),
                $form->generate_check_box('cangivereputations', 1, $this->lang->can_give_reputation, ['checked' => $this->bb->input['cangivereputations']]),
                $form->generate_check_box('candeletereputations', 1, $this->lang->can_delete_own_reputation, ['checked' => $this->bb->input['candeletereputations']]),
                "{$this->lang->points_to_award_take}<br /><small class=\"input\">{$this->lang->points_to_award_take_desc}</small><br />" . $form->generate_numeric_field('reputationpower', $this->bb->input['reputationpower'], ['id' => 'reputationpower', 'class' => 'field50', 'min' => 0]),
                "{$this->lang->max_reputations_perthread}<br /><small class=\"input\">{$this->lang->max_reputations_perthread_desc}</small><br />" . $form->generate_numeric_field('maxreputationsperthread', $this->bb->input['maxreputationsperthread'], ['id' => 'maxreputationsperthread', 'class' => 'field50', 'min' => 0]),
                "{$this->lang->max_reputations_daily}<br /><small class=\"input\">{$this->lang->max_reputations_daily_desc}</small><br />" . $form->generate_numeric_field('maxreputationsday', $this->bb->input['maxreputationsday'], ['id' => 'maxreputationsday', 'class' => 'field50', 'min' => 0])
            ];
            $form_container->output_row($this->lang->reputation_system, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $reputation_options) . '</div>');

            $warning_options = [
                $form->generate_check_box('canwarnusers', 1, $this->lang->can_send_warnings, ['checked' => $this->bb->input['canwarnusers']]),
                $form->generate_check_box('canreceivewarnings', 1, $this->lang->can_receive_warnings, ['checked' => $this->bb->input['canreceivewarnings']]),
                "{$this->lang->warnings_per_day}<br />" . $form->generate_numeric_field('maxwarningsday', $this->bb->input['maxwarningsday'], ['id' => 'maxwarningsday', 'class' => 'field50'])
            ];
            $form_container->output_row($this->lang->warning_system, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $warning_options) . '</div>');

            $pm_options = [
                $form->generate_check_box('canusepms', 1, $this->lang->can_use_pms, ['checked' => $this->bb->input['canusepms']]),
                $form->generate_check_box('cansendpms', 1, $this->lang->can_send_pms, ['checked' => $this->bb->input['cansendpms']]),
                $form->generate_check_box('canoverridepm', 1, $this->lang->can_override_pms, ['checked' => $this->bb->input['canoverridepm']]),
                $form->generate_check_box('cantrackpms', 1, $this->lang->can_track_pms, ['checked' => $this->bb->input['cantrackpms']]),
                $form->generate_check_box('candenypmreceipts', 1, $this->lang->can_deny_reciept, ['checked' => $this->bb->input['candenypmreceipts']]),
                "{$this->lang->message_quota}<br /><small>{$this->lang->message_quota_desc}</small><br />" . $form->generate_numeric_field('pmquota', $this->bb->input['pmquota'], ['id' => 'pmquota', 'class' => 'field50', 'min' => 0]),
                "{$this->lang->max_recipients}<br /><small>{$this->lang->max_recipients_desc}</small><br />" . $form->generate_numeric_field('maxpmrecipients', $this->bb->input['maxpmrecipients'], ['id' => 'maxpmrecipients', 'class' => 'field50', 'min' => 0])
            ];
            $form_container->output_row($this->lang->private_messaging, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $pm_options) . '</div>');

            $html .= $form_container->end();
            $html .= '</div>';

            //
            // MISC
            //
            $html .= '<div id="tab_misc">';
            $form_container = new FormContainer($this, $this->lang->misc);

            $calendar_options = [
                $form->generate_check_box('canviewcalendar', 1, $this->lang->can_view_calendar, ['checked' => $this->bb->input['canviewcalendar']]),
                $form->generate_check_box('canaddevents', 1, $this->lang->can_post_events, ['checked' => $this->bb->input['canaddevents']]),
                $form->generate_check_box('canbypasseventmod', 1, $this->lang->can_bypass_event_moderation, ['checked' => $this->bb->input['canbypasseventmod']]),
                $form->generate_check_box('canmoderateevents', 1, $this->lang->can_moderate_events, ['checked' => $this->bb->input['canmoderateevents']])
            ];
            $form_container->output_row($this->lang->calendar, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $calendar_options) . '</div>');

            $wol_options = [
                $form->generate_check_box('canviewonline', 1, $this->lang->can_view_whos_online, ['checked' => $this->bb->input['canviewonline']]),
                $form->generate_check_box('canviewwolinvis', 1, $this->lang->can_view_invisible, ['checked' => $this->bb->input['canviewwolinvis']]),
                $form->generate_check_box('canviewonlineips', 1, $this->lang->can_view_ips, ['checked' => $this->bb->input['canviewonlineips']])
            ];
            $form_container->output_row($this->lang->whos_online, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $wol_options) . '</div>');

            $misc_options = [
                $form->generate_check_box('canviewmemberlist', 1, $this->lang->can_view_member_list, ['checked' => $this->bb->input['canviewmemberlist']]),
                $form->generate_check_box('showinbirthdaylist', 1, $this->lang->show_in_birthday_list, ['checked' => $this->bb->input['showinbirthdaylist']]),
                $form->generate_check_box('cansendemail', 1, $this->lang->can_email_users, ['checked' => $this->bb->input['cansendemail']]),
                $form->generate_check_box('cansendemailoverride', 1, $this->lang->can_email_users_override, ['checked' => $this->bb->input['cansendemailoverride']]),
                "{$this->lang->max_emails_per_day}<br /><small class=\"input\">{$this->lang->max_emails_per_day_desc}</small><br />" . $form->generate_numeric_field('maxemails', $this->bb->input['maxemails'], ['id' => 'maxemails', 'class' => 'field50', 'min' => 0]),
                "{$this->lang->email_flood_time}<br /><small class=\"input\">{$this->lang->email_flood_time_desc}</small><br />" . $form->generate_numeric_field('emailfloodtime', $this->bb->input['emailfloodtime'], ['id' => 'emailfloodtime', 'class' => 'field50', 'min' => 0])
            ];
            $form_container->output_row($this->lang->misc, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $misc_options) . '</div>');

            $html .= $form_container->end();
            $html .= '</div>';

            //
            // MODERATOR CP
            //
            $html .= '<div id="tab_modcp">';
            $form_container = new FormContainer($this, $this->lang->mod_cp);

            $forum_post_options = [
                $form->generate_check_box('canmanageannounce', 1, $this->lang->can_manage_announce, ['checked' => $this->bb->input['canmanageannounce']]),
                $form->generate_check_box('canmanagemodqueue', 1, $this->lang->can_manage_mod_queue, ['checked' => $this->bb->input['canmanagemodqueue']]),
                $form->generate_check_box('canmanagereportedcontent', 1, $this->lang->can_manage_reported_content, ['checked' => $this->bb->input['canmanagereportedcontent']]),
                $form->generate_check_box('canviewmodlogs', 1, $this->lang->can_view_mod_logs, ['checked' => $this->bb->input['canviewmodlogs']])
            ];
            $form_container->output_row($this->lang->forum_post_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $forum_post_options) . '</div>');

            $user_options = [
                $form->generate_check_box('caneditprofiles', 1, $this->lang->can_edit_profiles, ['checked' => $this->bb->input['caneditprofiles']]),
                $form->generate_check_box('canbanusers', 1, $this->lang->can_ban_users, ['checked' => $this->bb->input['canbanusers']]),
                $form->generate_check_box('canviewwarnlogs', 1, $this->lang->can_view_warnlogs, ['checked' => $this->bb->input['canviewwarnlogs']]),
                $form->generate_check_box('canuseipsearch', 1, $this->lang->can_use_ipsearch, ['checked' => $this->bb->input['canuseipsearch']])
            ];
            $form_container->output_row($this->lang->user_options, '', "<div class=\"group_settings_bit\">" . implode("</div><div class=\"group_settings_bit\">", $user_options) . '</div>');

            $html .= $form_container->end();
            $html .= '</div>';

            $this->plugins->runHooks('admin_user_groups_edit_graph');

            $buttons[] = $form->generate_submit_button($this->lang->save_user_group);
            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/GroupsEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('usergroups', '*', "gid='" . $this->bb->getInput('gid', 0) . "'");
            $usergroup = $this->db->fetch_array($query);

            if (!$usergroup['gid']) {
                $this->session->flash_message($this->lang->error_invalid_user_group, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }
            if ($usergroup['type'] == 1) {
                $this->session->flash_message($this->lang->error_default_group_delete, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            }

            $this->plugins->runHooks('admin_user_groups_delete');

            if ($this->bb->request_method == 'post') {
                if ($usergroup['isbannedgroup'] == 1) {
                    // If banned group, move users to default banned group
                    $updated_users = ['usergroup' => 7];
                } else {
                    // Move any users back to the registered group
                    $updated_users = ['usergroup' => 2];
                }

                $this->db->update_query('users', $updated_users, "usergroup='{$usergroup['gid']}'");

                $updated_users = ['displaygroup' => 'usergroup'];
                $this->plugins->runHooks('admin_user_groups_delete_commit');

                $this->db->update_query('users', $updated_users, "displaygroup='{$usergroup['gid']}'", '', true); // No quotes = displaygroup=usergroup

                switch ($this->db->type) {
                    case 'pgsql':
                    case 'sqlite':
                        $query = $this->db->simple_select('users', 'uid', "','||additionalgroups||',' LIKE '%,{$usergroup['gid']},%'");
                        break;
                    default:
                        $query = $this->db->simple_select('users', 'uid', "CONCAT(',',additionalgroups,',') LIKE '%,{$usergroup['gid']},%'");
                }
                while ($user = $this->db->fetch_array($query)) {
                    $this->group->leave_usergroup($user['uid'], $usergroup['gid']);
                }

                $this->db->update_query('banned', ['gid' => 7], "gid='{$usergroup['gid']}'");
                $this->db->update_query('banned', ['oldgroup' => 2], "oldgroup='{$usergroup['gid']}'");
                $this->db->update_query('banned', ['olddisplaygroup' => 'oldgroup'], "olddisplaygroup='{$usergroup['gid']}'", '', true); // No quotes = displaygroup=usergroup

                $this->db->delete_query('forumpermissions', "gid='{$usergroup['gid']}'");
                $this->db->delete_query('calendarpermissions', "gid='{$usergroup['gid']}'");
                $this->db->delete_query('joinrequests', "gid='{$usergroup['gid']}'");
                $this->db->delete_query('moderators', "id='{$usergroup['gid']}' AND isgroup='1'");
                $this->db->delete_query('groupleaders', "gid='{$usergroup['gid']}'");
                $this->db->delete_query('usergroups', "gid='{$usergroup['gid']}'");

                $this->plugins->runHooks('admin_user_groups_delete_commit_end');

                $this->cache->update_groupleaders();
                $this->cache->update_moderators();
                $this->cache->update_usergroups();
                $this->cache->update_forumpermissions();
                $this->cache->update_banned();

                // Log admin action
                $this->bblogger->log_admin_action($usergroup['gid'], htmlspecialchars_uni($usergroup['title']));

                $this->session->flash_message($this->lang->success_group_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/groups');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/groups?action=delete&gid='.$usergroup['gid'], $this->lang->confirm_group_deletion);
            }
        }

        if ($this->bb->input['action'] == 'disporder' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('admin_user_groups_disporder');

            foreach ($this->bb->input['disporder'] as $gid => $order) {
                $gid = (int)$gid;
                $order = (int)$order;
                if ($gid != 0 && $order != 0) {
                    $sql_array = [
                        'disporder' => $order,
                    ];
                    $this->db->update_query('usergroups', $sql_array, "gid = '{$gid}'");
                }
            }

            // Log admin action
            $this->bblogger->log_admin_action();

            $this->plugins->runHooks('admin_user_groups_disporder_commit');

            $this->session->flash_message($this->lang->success_group_disporders_updated, 'success');
            return $response->withRedirect($this->bb->admin_url . '/users/groups');
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_user_groups_start');

            if ($this->bb->request_method == 'post') {
                if (!empty($this->bb->input['disporder'])) {
                    foreach ($this->bb->input['disporder'] as $gid => $order) {
                        $this->db->update_query('usergroups', ['disporder' => (int)$order], "gid='" . (int)$gid . "'");
                    }

                    $this->plugins->runHooks('admin_user_groups_start_commit');

                    $this->cache->update_usergroups();

                    $this->session->flash_message($this->lang->success_groups_disporder_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/groups');
                }
            }

            $html = $this->page->output_header($this->lang->manage_user_groups);
            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_groups');

            $form = new Form($this->bb, $this->bb->admin_url . '/users/groups', 'post', 'groups');
            $html .= $form->getForm();

            $query = $this->db->query('
		SELECT g.gid, COUNT(u.uid) AS users
		FROM ' . TABLE_PREFIX . 'users u
		LEFT JOIN ' . TABLE_PREFIX . 'usergroups g ON (g.gid=u.usergroup)
		GROUP BY g.gid
	');
            while ($groupcount = $this->db->fetch_array($query)) {
                $primaryusers[$groupcount['gid']] = $groupcount['users'];
            }

            switch ($this->db->type) {
                case 'pgsql':
                case 'sqlite':
                    $query = $this->db->query('
				SELECT g.gid, COUNT(u.uid) AS users
				FROM ' . TABLE_PREFIX . 'users u
				LEFT JOIN ' . TABLE_PREFIX . "usergroups g ON (','|| u.additionalgroups|| ',' LIKE '%,'|| g.gid|| ',%')
				WHERE g.gid != '0' AND g.gid is not NULL GROUP BY g.gid
			");
                    break;
                default:
                    $query = $this->db->query('
				SELECT g.gid, COUNT(u.uid) AS users
				FROM ' . TABLE_PREFIX . 'users u
				LEFT JOIN ' . TABLE_PREFIX . "usergroups g ON (CONCAT(',', u.additionalgroups, ',') LIKE CONCAT('%,', g.gid, ',%'))
				WHERE g.gid != '0' AND g.gid is not NULL GROUP BY g.gid
			");
            }
            while ($groupcount = $this->db->fetch_array($query)) {
                $secondaryusers[$groupcount['gid']] = $groupcount['users'];
            }

            $query = $this->db->query('
		SELECT g.gid, COUNT(r.uid) AS users
		FROM ' . TABLE_PREFIX . 'joinrequests r
		LEFT JOIN ' . TABLE_PREFIX . 'usergroups g ON (g.gid=r.gid)
		GROUP BY g.gid
	');

            $joinrequests = [];
            while ($joinrequest = $this->db->fetch_array($query)) {
                $joinrequests[$joinrequest['gid']] = $joinrequest['users'];
            }

            // Fetch group leaders
            $leaders = [];
            $query = $this->db->query('
		SELECT u.username, u.uid, l.gid
		FROM ' . TABLE_PREFIX . 'groupleaders l
		INNER JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=l.uid)
		ORDER BY u.username ASC
	');
            while ($leader = $this->db->fetch_array($query)) {
                $leaders[$leader['gid']][] = $this->user->build_profile_link($leader['username'], $leader['uid'], '_blank');
            }

            $form_container = new FormContainer($this, $this->lang->user_groups);
            $form_container->output_row_header($this->lang->group);
            $form_container->output_row_header($this->lang->number_of_users, ['class' => 'align_center', 'width' => '75']);
            $form_container->output_row_header($this->lang->order, ['class' => 'align_center', 'width' => '5%']);
            $form_container->output_row_header($this->lang->controls, ['class' => 'align_center']);

            $query = $this->db->simple_select('usergroups', '*', '', ['order_by' => 'disporder']);
            while ($usergroup = $this->db->fetch_array($query)) {
                if ($usergroup['type'] > 1) {
                    $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/default/images/icons/custom.png\" alt=\"{$this->lang->custom_user_group}\" style=\"vertical-align: middle;\" />";
                } else {
                    $icon = "<img src=\"{$this->bb->asset_url}/admin/styles/default/images/icons/default.png\" alt=\"{$this->lang->default_user_group}\" style=\"vertical-align: middle;\" />";
                }

                $leaders_list = '';
                if (isset($leaders[$usergroup['gid']])) {
                    $leaders_list = "<br />{$this->lang->group_leaders}: " . implode($this->lang->comma, $leaders[$usergroup['gid']]);
                }

                $join_requests = '';
                if (isset($joinrequests[$usergroup['gid']])) {
                    if ($joinrequests[$usergroup['gid']] > 1 && $usergroup['type'] == 4) {
                        $join_requests = " <small><a href=\"".$this->bb->admin_url."/users/groups?action=join_requests&amp;gid={$usergroup['gid']}\"><span style=\"color: red;\">({$joinrequests[$usergroup['gid']]} {$this->lang->outstanding_join_request})</span></a></small>";
                    } elseif ($joinrequests[$usergroup['gid']] == 1 && $usergroup['type'] == 4) {
                        $join_requests = " <small><a href=\"".$this->bb->admin_url."/users/groups?action=join_requests&amp;gid={$usergroup['gid']}\"><span style=\"color: red;\">({$joinrequests[$usergroup['gid']]} {$this->lang->outstanding_join_request})</span></a></small>";
                    }
                }

                $form_container->output_cell("<div class=\"float_right\">{$icon}</div><div><strong><a href=\"".$this->bb->admin_url."/users/groups?action=edit&amp;gid={$usergroup['gid']}\">" . htmlspecialchars_uni($usergroup['title']) . "</a></strong>{$join_requests}<br /><small>" . htmlspecialchars_uni($usergroup['description']) . "{$leaders_list}</small></div>");

                if (!isset($primaryusers[$usergroup['gid']])) {
                    $primaryusers[$usergroup['gid']] = 0;
                }
                $numusers = $primaryusers[$usergroup['gid']];
                if (isset($secondaryusers[$usergroup['gid']])) {
                    $numusers += $secondaryusers[$usergroup['gid']];
                }

                $form_container->output_cell($this->parser->formatNumber($numusers), ['class' => 'align_center']);

                if ($usergroup['showforumteam'] == 1) {
                    $form_container->output_cell("<input type=\"text\" name=\"disporder[{$usergroup['gid']}]\" value=\"{$usergroup['disporder']}\" class=\"text_input align_center\" style=\"width: 80%;\" />", ["class" => "align_center"]);
                } else {
                    $form_container->output_cell('&nbsp;', ['class' => 'align_center']);
                }

                $popup = new PopupMenu("usergroup_{$usergroup['gid']}", $this->lang->options);
                $popup->add_item($this->lang->edit_group, $this->bb->admin_url . '/users/groups?action=edit&gid='.$usergroup['gid']);
                $popup->add_item($this->lang->list_users, $this->bb->admin_url . "/users?action=search&results=1&amp;conditions[usergroup]={$usergroup['gid']}");
                if (isset($joinrequests[$usergroup['gid']]) && $joinrequests[$usergroup['gid']] > 0 && $usergroup['type'] == 4) {
                    $popup->add_item($this->lang->join_requests, $this->bb->admin_url . '/users/groups?action=join_requests&gid='.$usergroup['gid']);
                }
                $popup->add_item($this->lang->group_leaders, $this->bb->admin_url . '/users/groups?action=leaders&gid='.$usergroup['gid']);
                if ($usergroup['type'] > 1) {
                    $popup->add_item($this->lang->delete_group, $this->bb->admin_url . "/users/groups?action=delete&amp;gid={$usergroup['gid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_group_deletion}')");
                }
                $form_container->output_cell($popup->fetch(), ['class' => 'align_center']);
                $form_container->construct_row();
            }

            if ($form_container->num_rows() == 0) {
                $form_container->output_cell($this->lang->no_groups, ['colspan' => 4]);
                $form_container->construct_row();
            }

            $html .= $form_container->end();

            $buttons = [];
            $buttons[] = $form->generate_submit_button($this->lang->update_groups_order);
            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();

            $html .= <<<LEGEND
	<br />
	<fieldset>
<legend>{$this->lang->legend}</legend>
<img src="{$this->bb->asset_url}/admin/styles/default/images/icons/custom.png" alt="{$this->lang->custom_user_group}" style="vertical-align: middle;" /> {$this->lang->custom_user_group}<br />
<img src="{$this->bb->asset_url}/admin/styles/default/images/icons/default.png" alt="{$this->lang->default_user_group}" style="vertical-align: middle;" /> {$this->lang->default_user_group}
</fieldset>
LEGEND;

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/Groups.html.twig');
        }
    }
}
