<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\Table;
use RunCMF\Core\AbstractController;

class MassMail extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_mass_mail', false, true);
        $this->page->add_breadcrumb_item($this->lang->mass_mail, $this->bb->admin_url . '/users/mass_mail');

        if ($this->bb->input['action'] == 'send' || $this->bb->input['action'] == 'archive' || !$this->bb->input['action']) {
            $sub_tabs['mail_queue'] = [
                'title' => $this->lang->mass_mail_queue,
                'link' => $this->bb->admin_url . '/users/mass_mail',
                'description' => $this->lang->mass_mail_queue_desc
            ];

            $sub_tabs['send_mass_mail'] = [
                'title' => $this->lang->create_mass_mail,
                'link' => $this->bb->admin_url . '/users/mass_mail?action=send',
                'description' => $this->lang->create_mass_mail_desc
            ];

            $sub_tabs['archive'] = [
                'title' => $this->lang->mass_mail_archive,
                'link' => $this->bb->admin_url . '/users/mass_mail?action=archive',
                'description' => $this->lang->mass_mail_archive_desc
            ];
        }

        $this->plugins->runHooks('admin_user_mass_email');

        if ($this->bb->input['action'] == 'edit') {
            $this->page->add_breadcrumb_item($this->lang->edit_mass_mail);

            $query = $this->db->simple_select('massemails', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $email = $this->db->fetch_array($query);
            if (!$email['mid']) {
                $this->session->flash_message($this->lang->error_invalid_mid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
            }

            $this->plugins->runHooks('admin_user_mass_email_edit_start');

            if ($email['conditions'] != '') {
                $email['conditions'] = my_unserialize($email['conditions']);
            }

            $sub_tabs['edit_mass_mail'] = [
                'title' => $this->lang->edit_mass_mail,
                'link' => $this->bb->admin_url . '/users/mass_mail?action=edit&mid=' . $email['mid'],
                'description' => $this->lang->edit_mass_mail_desc
            ];

            $replacement_fields = [
                '{username}' => $this->lang->username,
                '{email}' => $this->lang->email_addr,
                '{bbname}' => $this->lang->board_name,
                '{bburl}' => $this->lang->board_url
            ];

            $html_personalisation = $text_personalisation = "<script type=\"text/javascript\">\n<!--\ndocument.write('{$this->lang->personalize_message} ";
            foreach ($replacement_fields as $value => $name) {
                $html_personalisation .= " [<a href=\"#\" onclick=\"insertText(\'{$value}\', \'htmlmessage\'); return false;\">{$name}</a>], ";
                $text_personalisation .= " [<a href=\"#\" onclick=\"insertText(\'{$value}\', \'message\'); return false;\">{$name}</a>], ";
            }
            $html_personalisation = substr($html_personalisation, 0, -2) . "');\n// --></script>\n";
            $text_personalisation = substr($text_personalisation, 0, -2) . "');\n// --></script>\n";

            // All done here
            if ($this->bb->request_method == 'post') {
                // Sending this message now
                if ($this->bb->input['delivery_type'] == 'now') {
                    $delivery_date = TIME_NOW;
                } // Delivering in the future
                else {
                    if (strstr($this->bb->input['deliverytime_time'], 'pm')) {
                        $this->bb->input['deliveryhour'] += 12;
                    }

                    $exploded = explode(':', $this->bb->input['endtime_time']);
                    $this->bb->input['deliveryhour'] = (int)$exploded[0];

                    $exploded = explode(' ', $exploded[1]);
                    $this->bb->input['deliveryminute'] = (int)$exploded[0];

                    $delivery_date = gmmktime($this->bb->input['deliveryhour'], $this->bb->input['deliveryminute'], 0, $this->bb->input['endtime_month'], $this->bb->input['endtime_day'], $this->bb->input['endtime_year']) + $this->user->timezone * 3600;
                    if ($delivery_date <= TIME_NOW) {
                        $errors[] = $this->lang->error_only_in_future;
                    }
                }

                // Need to perform the search to fetch the number of users we're emailing
                $member_query = $this->mail->buildMassMailQuery($this->bb->input['conditions']);
                $query = $this->db->simple_select('users u', 'COUNT(uid) AS num', $member_query);
                $num = $this->db->fetch_field($query, 'num');

                if ($num == 0) {
                    $errors[] = $this->lang->error_no_users;
                }

                if (!trim($this->bb->input['subject'])) {
                    $errors[] = $this->lang->error_missing_subject;
                }

                if ($this->bb->input['type'] == 1) {
                    if (!$this->bb->input['message']) {
                        $errors[] = $this->lang->error_missing_message;
                    }
                } else {
                    if ($this->bb->input['format'] == 2 && $this->bb->input['automatic_text'] == 0 && !$this->bb->input['message']) {
                        $errors[] = $this->lang->error_missing_plain_text;
                    }

                    if (($this->bb->input['format'] == 1 || $this->bb->input['format'] == 2) && !$this->bb->input['htmlmessage']) {
                        $errors[] = $this->lang->error_missing_html;
                    } elseif ($this->bb->input['format'] == 0 && !$this->bb->input['message']) {
                        $errors[] = $this->lang->error_missing_plain_text;
                    }
                }

                if (!isset($errors)) {
                    // Sending via a PM
                    if ($this->bb->input['type'] == 1) {
                        $this->bb->input['format'] = 0;
                        $this->bb->input['htmlmessage'] = '';
                    } // Sending via email
                    else {
                        // Do we need to generate a text based version?
                        if ($this->bb->input['format'] == 2 && $this->bb->input['automatic_text']) {
                            $this->bb->input['message'] = $this->mail->createTextMessage($this->bb->input['htmlmessage']);
                        } elseif ($this->bb->input['format'] == 1) {
                            $this->bb->input['message'] = '';
                        } elseif ($this->bb->input['format'] == 0) {
                            $this->bb->input['htmlmessage'] = '';
                        }
                    }

                    // Mark as queued for delivery
                    $updated_email = [
                        'status' => 1,
                        'senddate' => $delivery_date,
                        'totalcount' => $num,
                        'conditions' => $this->db->escape_string(my_serialize($this->bb->input['conditions'])),
                        'message' => $this->db->escape_string($this->bb->input['message']),
                        'subject' => $this->db->escape_string($this->bb->input['subject']),
                        'htmlmessage' => $this->db->escape_string($this->bb->input['htmlmessage']),
                        'format' => $this->bb->getInput('format', 0),
                        'type' => $this->bb->getInput('type', 0),
                        'perpage' => $this->bb->getInput('perpage', 0)
                    ];

                    $this->plugins->runHooks('admin_user_mass_email_edit_commit');

                    $this->db->update_query('massemails', $updated_email, "mid='{$email['mid']}'");

                    $this->session->flash_message($this->lang->success_mass_mail_saved, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
                }
            }

            $html = $this->page->output_header($this->lang->edit_mass_mail);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_mass_mail');

            // If we have any error messages, show them
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
                $input = $this->bb->input;
            } else {
                $input = $email;

                if ($email['senddate'] != 0) {
                    if ($email['senddate'] <= TIME_NOW) {
                        $input['delivery_type'] = 'now';
                        $delivery_type_checked['now'] = ' checked="checked"';
                    } else {
                        $input['delivery_type'] = 'future';
                        $time = date('d-n-Y-h-i-a', $email['senddate']);
                        $time = explode('-', $time);
                        $input['deliveryhour'] = (int)$time[3];
                        $input['deliveryminute'] = (int)$time[4];
                        $input['deliverymonth'] = (int)$time[1];
                        $input['deliveryday'] = (int)$time[0];
                        $input['deliveryyear'] = (int)$time[2];
                        $input['deliverymeridiem'] = $time[5];
                        $delivery_type_checked['future'] = ' checked="checked"';
                    }
                } else {
                    $input['delivery_type'] = 'now';
                    $delivery_type_checked['now'] = ' checked="checked"';
                }
            }

            if (isset($input['deliveryhour'])) {
                $input['endtime_time'] = (int)$input['deliveryhour'] . ':';
            } else {
                $input['endtime_time'] = '12:';
            }

            if (isset($input['deliveryminute'])) {
                $input['endtime_time'] .= (int)$input['deliveryminute'] . ' ';
            } else {
                $input['endtime_time'] .= '00 ';
            }

            if (isset($input['deliverymeridiem'])) {
                $input['endtime_time'] .= $input['deliverymeridiem'];
            } else {
                $input['endtime_time'] .= 'am';
            }

            if (!isset($input['deliveryyear'])) {
                $enddateyear = gmdate('Y', TIME_NOW);
            } else {
                $enddateyear = (int)$input['deliveryyear'];
            }

            if (!isset($input['deliverymonth'])) {
                $input['enddatemonth'] = gmdate('n', TIME_NOW);
            } else {
                $input['enddatemonth'] = (int)$input['deliverymonth'];
            }

            if (!isset($input['deliveryday'])) {
                $input['enddateday'] = gmdate('j', TIME_NOW);
            } else {
                $input['enddateday'] = (int)$input['deliveryday'];
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/users/mass_mail?action=edit', 'post');
            $html .= $form->getForm();
            $html .= $form->generate_hidden_field('mid', $email['mid']);

            $mid_add = '';
            if ($email['mid']) {
                $mid_add = "&amp;mid={$email['mid']}";
            }

            $form_container = new FormContainer($this, "{$this->lang->edit_mass_mail}: {$this->lang->message_settings}");

            $form_container->output_row("{$this->lang->subject}: <em>*</em>", $this->lang->subject_desc, $form->generate_text_box('subject', $input['subject'], ['id' => 'subject']), 'subject');

            if ($input['type'] == 0) {
                $type_email_checked = true;
                $type_pm_checked = false;
            } elseif ($input['type'] == 1) {
                $type_email_checked = false;
                $type_pm_checked = true;
            }

            $type_options = [
                $form->generate_radio_button('type', 0, $this->lang->send_via_email, ['id' => 'type_email', 'checked' => $type_email_checked]),
                $form->generate_radio_button('type', 1, $this->lang->send_via_pm, ['id' => 'type_pm', 'checked' => $type_pm_checked])
            ];
            $form_container->output_row("{$this->lang->message_type}: <em>*</em>", '', implode('<br />', $type_options));

            $monthnames = [
                'offset',
                $this->lang->january,
                $this->lang->february,
                $this->lang->march,
                $this->lang->april,
                $this->lang->may,
                $this->lang->june,
                $this->lang->july,
                $this->lang->august,
                $this->lang->september,
                $this->lang->october,
                $this->lang->november,
                $this->lang->december,
            ];

            $enddatemonth = '';
            foreach ($monthnames as $key => $month) {
                if ($month == 'offset') {
                    continue;
                }

                if ($key == $input['enddatemonth']) {
                    $enddatemonth .= "<option value=\"{$key}\" selected=\"selected\">{$month}</option>\n";
                } else {
                    $enddatemonth .= "<option value=\"{$key}\">{$month}</option>\n";
                }
            }

            $enddateday = '';

            // Construct option list for days
            for ($i = 1; $i <= 31; ++$i) {
                if ($i == $input['enddateday']) {
                    $enddateday .= "<option value=\"{$i}\" selected=\"selected\">{$i}</option>\n";
                } else {
                    $enddateday .= "<option value=\"{$i}\">{$i}</option>\n";
                }
            }

            $actions = "<script type=\"text/javascript\">
		function checkAction(id)
		{
			var checked = '';

			$('.'+id+'s_check').each(function(e, val)
			{
				if($(this).prop('checked') == true)
				{
					checked = $(this).val();
				}
			});
			$('.'+id+'s').each(function(e)
			{
				$(this).hide();
			});
			if($('#'+id+'_'+checked))
			{
				$('#'+id+'_'+checked).show();
			}
		}
	</script>
		<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"delivery_type\" value=\"now\" {$delivery_type_checked['now']} class=\"delivery_types_check\" onclick=\"checkAction('delivery_type');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->deliver_immediately}</strong></label></dt>

		<dt><label style=\"display: block;\"><input type=\"radio\" name=\"delivery_type\" value=\"future\" ".($delivery_type_checked['future'])." class=\"delivery_types_check\" onclick=\"checkAction('delivery_type');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->deliver_specific}</strong></label></dt>
			<dd style=\"margin-top: 4px;\" id=\"delivery_type_future\" class=\"delivery_types\">
				<table cellpadding=\"4\">
					<tr>
						<td><select name=\"endtime_day\">\n{$enddateday}</select>\n &nbsp; \n<select name=\"endtime_month\">\n{$enddatemonth}</select>\n &nbsp; \n<input type=\"text\" name=\"endtime_year\" value=\"{$enddateyear}\" class=\"text_input\" size=\"4\" maxlength=\"4\" />\n - {$this->lang->time} " . $form->generate_text_box('endtime_time', $input['endtime_time'], ['id' => 'endtime_time', 'style' => 'width: 60px;']) . "</td>
					</tr>
				</table>
			</dd>
		</dl>
		<script type=\"text/javascript\">
		checkAction('delivery_type');
		</script>";
            $form_container->output_row("{$this->lang->delivery_date}: <em>*</em>", $this->lang->delivery_date_desc, $actions);

            $form_container->output_row("{$this->lang->per_page}: <em>*</em>", $this->lang->per_page_desc, $form->generate_numeric_field('perpage', $input['perpage'], ['id' => 'perpage', 'min' => 1]), 'perpage');

            $format_options = [
                0 => $this->lang->plain_text_only,
                1 => $this->lang->html_only,
                2 => $this->lang->html_and_plain_text
            ];

            $form_container->output_row(
                "{$this->lang->message_format}: <em>*</em>",
                '',
                $form->generate_select_box(
                    'format',
                    $format_options,
                    $input['format'],
                    ['id' => 'format']
                ),
                'format',
                null,
                ['id' => 'format_container']
            );

            $html .= $form_container->end();

            if ($input['format'] == 2) {
                if ($input['automatic_text'] && !$email['mid']) {
                    $automatic_text_check = true;
                    $text_display = 'display: none';
                    $automatic_display = 'display: none;';
                }
            } elseif ($input['format'] == 1 && $input['type'] != 1) {
                $text_display = 'display: none;';
            } elseif ($input['format'] == 0 || $input['type'] == 1) {
                $html_display = 'display: none';
            }

            $html .= "<div id=\"message_html\" style=\"{$html_display}\">";
            $form_container = new FormContainer($this, "{$this->lang->edit_mass_mail}: {$this->lang->define_html_message}");
            $form_container->output_row("{$this->lang->define_html_message_desc}:", $html_personalisation, $form->generate_text_area('htmlmessage', $input['htmlmessage'], ['id' => 'htmlmessage', 'rows' => 15, 'cols ' => 70, 'style' => 'width: 95%']) . "<div id=\"automatic_display\" style=\"".($automatic_display)."\">" . $form->generate_check_box('automatic_text', 1, $this->lang->auto_gen_plain_text, ['checked'=> ($automatic_text_check), 'id' => 'automatic_text']) . '</div>');
            $html .= $form_container->end();
            $html .= '</div>';

            $html .= '<div id="message_text" style="'.($text_display).'">';
            $form_container = new FormContainer($this, "{$this->lang->edit_mass_mail}: {$this->lang->define_text_version}");
            $form_container->output_row("{$this->lang->define_text_version_desc}:", $text_personalisation, $form->generate_text_area('message', $input['message'], ['id' => 'message', 'rows' => 15, 'cols ' => 70, 'style' => 'width: 95%']));
            $html .= $form_container->end();
            $html .= '</div>';

            $html .= "
	<script type=\"text/javascript\">
		function ToggleFormat()
		{
			var v = $('#format option:selected').val();
			if(v == 2)
			{
				$('#automatic_display').show();
				$('#message_html').show();
				if($('#automatic_text').checked)
				{
					$('#message_text').hide();
				}
				else
				{
					$('#message_text').show();
				}
			}
			else if(v == 1)
			{
				$('#message_text').hide();
				$('#message_html').show();
				$('#automatic_display').hide();
			}
			else
			{
				$('#message_text').show();
				$('#message_html').hide();
			}
		}
		$(document).on('change', '#format', function() {
			ToggleFormat();
		});

		function ToggleType()
		{
			var v = $('#type_pm').prop('checked');
			if(v == true)
			{
				$('#message_html').hide();
				$('#message_text').show();
				$('#format_container').hide();
			}
			else
			{
				$('#message_html').show();
				$('#format_container').show();
				ToggleFormat();
			}
		}
		$('#type_pm').on('click', function() {
			ToggleType();
		});
		$('#type_email').on('click', function() {
			ToggleType();
		});
		ToggleType();

		function ToggleAutomatic()
		{
			var v = $('#automatic_text').prop('checked');
			if(v == true)
			{
				$('#message_text').hide();
			}
			else
			{
				$('#message_text').show();
			}
		}

		$('#automatic_text').on('click', function() {
			ToggleAutomatic();
		});

		function insertText(value, textarea)
		{
			textarea = document.getElementById(textarea);
			// Internet Explorer
			if(document.selection)
			{
				textarea.focus();
				var selection = document.selection.createRange();
				selection.text = value;
			}
			// Firefox
			else if(textarea.selectionStart || textarea.selectionStart == '0')
			{
				var start = textarea.selectionStart;
				var end = textarea.selectionEnd;
				textarea.value = textarea.value.substring(0, start)	+ value	+ textarea.value.substring(end, textarea.value.length);
			}
			else
			{
				textarea.value += value;
			}
		}

	</script>";

            $form_container = new FormContainer($this, "{$this->lang->edit_mass_mail}: {$this->lang->define_the_recipients}");

            $form_container->output_row($this->lang->username_contains, '', $form->generate_text_box('conditions[username]', $input['conditions']['username'], ['id' => 'username']), 'username');
            $form_container->output_row($this->lang->email_addr_contains, '', $form->generate_text_box('conditions[email]', $input['conditions']['email'], ['id' => 'email']), 'email');

            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);

            $options = [];
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[$usergroup['gid']] = $usergroup['title'];
            }

            $form_container->output_row($this->lang->members_of, $this->lang->additional_user_groups_desc, $form->generate_select_box('conditions[usergroup][]', $options, $input['conditions']['usergroup'], ['id' => 'usergroups', 'multiple' => true, 'size' => 5]), 'usergroups');

            $greater_options = [
                'greater_than' => $this->lang->greater_than,
                'is_exactly' => $this->lang->is_exactly,
                'less_than' => $this->lang->less_than
            ];
            $form_container->output_row($this->lang->post_count_is, '', $form->generate_select_box(
                'conditions[postnum_dir]',
                $greater_options,
                $input['conditions']['postnum_dir'],
                ['id' => 'postnum_dir']
            ) . ' ' . $form->generate_numeric_field('conditions[postnum]', $input['conditions']['postnum'], ['id' => 'postnum', 'min' => 0]), 'postnum');

            $more_options = [
                'more_than' => $this->lang->more_than,
                'less_than' => $this->lang->less_than
            ];

            $date_options = [
                'hours' => $this->lang->hours,
                'days' => $this->lang->days,
                'weeks' => $this->lang->weeks,
                'months' => $this->lang->months,
                'years' => $this->lang->years
            ];
            $form_container->output_row($this->lang->user_registered, '', $form->generate_select_box(
                'conditions[regdate_dir]',
                $more_options,
                $input['conditions']['regdate_dir'],
                ['id' => 'regdate_dir']
            ) . ' ' . $form->generate_numeric_field(
                'conditions[regdate]',
                $input['conditions']['regdate'],
                ['id' => 'regdate', 'min' => 0]
            ) . ' ' . $form->generate_select_box(
                'conditions[regdate_date]',
                $date_options,
                $input['conditions']['regdate_date'],
                ['id' => 'regdate_date']
            ) . " {$this->lang->ago}", 'regdate');

            $form_container->output_row($this->lang->user_last_active, '', $form->generate_select_box(
                'conditions[lastactive_dir]',
                $more_options,
                $input['conditions']['lastactive_dir'],
                ['id' => 'lastactive_dir']
            ) . ' ' . $form->generate_numeric_field(
                'conditions[lastactive]',
                $input['conditions']['lastactive'],
                ['id' => 'lastactive', 'min' => 0]
            ) . ' ' . $form->generate_select_box(
                'conditions[lastactive_date]',
                $date_options,
                $input['conditions']['lastactive_date'],
                ['id' => 'lastactive_date']
            ) . " {$this->lang->ago}", 'lastactive');

            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_mass_mail);
            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/MassMailEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'send') {
            $this->page->add_breadcrumb_item($this->lang->send_mass_mail);

            if (isset($this->bb->input['step'])) {
                $query = $this->db->simple_select('massemails', '*', "status=0 and mid='" . $this->bb->getInput('mid', 0) . "'");
                $email = $this->db->fetch_array($query);
                if (!$email['mid'] && $this->bb->input['step'] != 1) {
                    $this->session->flash_message($this->lang->error_invalid_mid, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
                }
            }

            $replacement_fields = [
                '{username}' => $this->lang->username,
                '{email}' => $this->lang->email_addr,
                '{bbname}' => $this->lang->board_name,
                '{bburl}' => $this->lang->board_url
            ];

            $html_personalisation = $text_personalisation = "<script type=\"text/javascript\">\n<!--\ndocument.write('{$this->lang->personalize_message} ";
            foreach ($replacement_fields as $value => $name) {
                $html_personalisation .= " [<a href=\"#\" onclick=\"insertText(\'{$value}\', \'htmlmessage\'); return false;\">{$name}</a>], ";
                $text_personalisation .= " [<a href=\"#\" onclick=\"insertText(\'{$value}\', \'message\'); return false;\">{$name}</a>], ";
            }
            $html_personalisation = substr($html_personalisation, 0, -2) . "');\n// --></script>\n";
            $text_personalisation = substr($text_personalisation, 0, -2) . "');\n// --></script>\n";

            $this->plugins->runHooks('admin_user_mass_email_send_start');

            if (isset($this->bb->input['step']) && $this->bb->input['step'] == 4) {
                // All done here
                if ($this->bb->request_method == 'post') {
                    // Sending this message now
                    if ($this->bb->input['delivery_type'] == 'now') {
                        $delivery_date = TIME_NOW;
                    } // Delivering in the future
                    else {
                        if (strstr($this->bb->input['deliverytime_time'], 'pm')) {
                            $this->bb->input['deliveryhour'] += 12;
                        }

                        $exploded = explode(':', $this->bb->input['endtime_time']);
                        $this->bb->input['deliveryhour'] = (int)$exploded[0];

                        $exploded = explode(' ', $exploded[1]);
                        $this->bb->input['deliveryminute'] = (int)$exploded[0];

                        $delivery_date = gmmktime($this->bb->input['deliveryhour'], $this->bb->input['deliveryminute'], 0, $this->bb->input['endtime_month'], $this->bb->input['endtime_day'], $this->bb->input['endtime_year']) + $this->user->timezone * 3600;
                        if ($delivery_date <= TIME_NOW) {
                            $errors[] = $this->lang->error_only_in_future;
                        }
                    }

                    if (!isset($errors)) {
                        // Mark as queued for delivery
                        $updated_email = [
                            'status' => 1,
                            'senddate' => $delivery_date
                        ];

                        $this->plugins->runHooks('admin_user_mass_email_send_finalize_commit');

                        $this->db->update_query('massemails', $updated_email, "mid='{$email['mid']}'");

                        $this->session->flash_message($this->lang->success_mass_mail_saved, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
                    }
                }

                // Show summary of the mass email we've just been creating and allow the user to specify the delivery date
                $html = $this->page->output_header("{$this->lang->send_mass_mail}: {$this->lang->step_four}");

                $html .= $this->page->output_nav_tabs($sub_tabs, 'send_mass_mail');

                // If we have any error messages, show them
                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                    $input = $this->bb->input;
                } else {
                    $input = [];
                    if ($email['senddate'] != 0) {
                        if ($email['senddate'] <= TIME_NOW) {
                            $input['delivery_type'] = 'now';
                            $delivery_type_checked['now'] = ' checked="checked"';
                        } else {
                            $input['delivery_type'] = 'future';
                            $time = date('d-n-Y-h-i-a', $email['senddate']);
                            $time = explode('-', $time);
                            $input['deliveryhour'] = (int)$time[3];
                            $input['deliveryminute'] = (int)$time[4];
                            $input['deliverymonth'] = (int)$time[1];
                            $input['deliveryday'] = (int)$time[0];
                            $input['deliveryyear'] = (int)$time[2];
                            $input['deliverymeridiem'] = $time[5];
                            $delivery_type_checked['future'] = ' checked="checked"';
                        }
                    } else {
                        $input['delivery_type'] = 'now';
                        $delivery_type_checked['now'] = ' checked="checked"';
                    }
                }

                $table = new Table;
                $table->construct_cell("<strong>{$this->lang->delivery_method}:</strong>", ['width' => '25%']);
                if ($email['type'] == 1) {
                    $delivery_type = $this->lang->private_message;
                } elseif ($email['type'] == 0) {
                    $delivery_type = $this->lang->email;
                }
                $table->construct_cell($delivery_type);
                $table->construct_row();

                $table->construct_cell("<strong>{$this->lang->subject}:</strong>");
                $table->construct_cell(htmlspecialchars_uni($email['subject']));
                $table->construct_row();

                $table->construct_cell("<strong>{$this->lang->message}:</strong>");
                $format_preview = '';
                if ($email['format'] == 0 || $email['format'] == 2) {
                    $format_preview .= "{$this->lang->text_based} - <a href=\"#\" onclick=\"javascript:MyBB.popupWindow('".$this->bb->admin_url."/users/mass_mail?action=preview&amp;mid={$email['mid']}&amp;format=text', null, true);\">{$this->lang->preview}</a>";
                }
                if ($email['format'] == 2) {
                    $format_preview .= " {$this->lang->and} <br />";
                }
                if ($email['format'] == 1 || $email['format'] == 2) {
                    $format_preview .= "{$this->lang->html_based} - <a href=\"#\" onclick=\"javascript:MyBB.popupWindow('".$this->bb->admin_url."/users/mass_mail?action=preview&mid={$email['mid']}', null, true);\">{$this->lang->preview}</a>";
                }
                $table->construct_cell($format_preview);
                $table->construct_row();

                // Recipient counts & details
                $table->construct_cell("<strong>{$this->lang->total_recipients}:</strong>");
                $table->construct_cell($this->parser->formatNumber($email['totalcount']) . " - <a href=\"".$this->bb->admin_url."/users/mass_mail?action=send&amp;step=3&amp;mid={$email['mid']}\">{$this->lang->change_recipient_conds}</a>");
                $table->construct_row();

                $html .= $table->output("{$this->lang->send_mass_mail}: {$this->lang->step_four} - {$this->lang->review_message}");

                if (isset($input['deliveryhour'])) {
                    $input['endtime_time'] = (int)$input['deliveryhour'] . ':';
                } else {
                    $input['endtime_time'] = '12:';
                }

                if (isset($input['deliveryminute'])) {
                    $input['endtime_time'] .= (int)$input['deliveryminute'] . ' ';
                } else {
                    $input['endtime_time'] .= '00 ';
                }

                if (isset($input['deliverymeridiem'])) {
                    $input['endtime_time'] .= $input['deliverymeridiem'];
                } else {
                    $input['endtime_time'] .= 'am';
                }

                if (!isset($input['deliveryyear'])) {
                    $enddateyear = gmdate('Y', TIME_NOW);
                } else {
                    $enddateyear = (int)$input['deliveryyear'];
                }

                if (!isset($input['deliverymonth'])) {
                    $input['enddatemonth'] = gmdate('n', TIME_NOW);
                } else {
                    $input['enddatemonth'] = (int)$input['deliverymonth'];
                }

                if (!isset($input['deliveryday'])) {
                    $input['enddateday'] = gmdate('j', TIME_NOW);
                } else {
                    $input['enddateday'] = (int)$input['deliveryday'];
                }

                $monthnames = [
                    'offset',
                    $this->lang->january,
                    $this->lang->february,
                    $this->lang->march,
                    $this->lang->april,
                    $this->lang->may,
                    $this->lang->june,
                    $this->lang->july,
                    $this->lang->august,
                    $this->lang->september,
                    $this->lang->october,
                    $this->lang->november,
                    $this->lang->december,
                ];

                $enddatemonth = '';
                foreach ($monthnames as $key => $month) {
                    if ($month == 'offset') {
                        continue;
                    }

                    if ($key == $input['enddatemonth']) {
                        $enddatemonth .= "<option value=\"{$key}\" selected=\"selected\">{$month}</option>\n";
                    } else {
                        $enddatemonth .= "<option value=\"{$key}\">{$month}</option>\n";
                    }
                }

                $enddateday = '';

                // Construct option list for days
                for ($i = 1; $i <= 31; ++$i) {
                    if ($i == $input['enddateday']) {
                        $enddateday .= "<option value=\"{$i}\" selected=\"selected\">{$i}</option>\n";
                    } else {
                        $enddateday .= "<option value=\"{$i}\">{$i}</option>\n";
                    }
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/users/mass_mail?action=send&step=4&mid=' . $email['mid'], 'post');
                $html .= $form->getForm();
                $form_container = new FormContainer($this, "{$this->lang->send_mass_mail}: {$this->lang->step_four} - {$this->lang->define_delivery_date}");

                $actions = "<script type=\"text/javascript\">
			function checkAction(id)
			{
				var checked = '';

				$('.'+id+'s_check').each(function(e, val)
				{
					if($(this).prop('checked') == true)
					{
						checked = $(this).val();
					}
				});
				$('.'+id+'s').each(function(e)
				{
					$(this).hide();
				});
				if($('#'+id+'_'+checked))
				{
					$('#'+id+'_'+checked).show();
				}
			}
		</script>
			<dl style=\"margin-top: 0; margin-bottom: 0; width: 100%;\">
			<dt><label style=\"display: block;\"><input type=\"radio\" name=\"delivery_type\" value=\"now\" {$delivery_type_checked['now']} class=\"delivery_types_check\" onclick=\"checkAction('delivery_type');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->deliver_immediately}</strong></label></dt>

			<dt><label style=\"display: block;\"><input type=\"radio\" name=\"delivery_type\" value=\"future\" ".($delivery_type_checked['future'])." class=\"delivery_types_check\" onclick=\"checkAction('delivery_type');\" style=\"vertical-align: middle;\" /> <strong>{$this->lang->deliver_specific}</strong></label></dt>
				<dd style=\"margin-top: 4px;\" id=\"delivery_type_future\" class=\"delivery_types\">
					<table cellpadding=\"4\">
						<tr>
							<td><select name=\"endtime_day\">\n{$enddateday}</select>\n &nbsp; \n<select name=\"endtime_month\">\n{$enddatemonth}</select>\n &nbsp; \n<input type=\"text\" name=\"endtime_year\" class=\"text_input\" value=\"{$enddateyear}\" size=\"4\" maxlength=\"4\" />\n - {$this->lang->time} " . $form->generate_text_box('endtime_time', $input['endtime_time'], ['id' => 'endtime_time', 'style' => 'width: 60px;']) . "</td>
						</tr>
					</table>
				</dd>
			</dl>
			<script type=\"text/javascript\">
			checkAction('delivery_type');
			</script>";
                $form_container->output_row("{$this->lang->delivery_date}: <em>*</em>", $this->lang->delivery_date_desc, $actions);

                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->schedule_for_delivery);
                $html .= $form->output_submit_wrapper($buttons);
                $html .= $form->end();
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Users/MassMailSendStep4.html.twig');
            } elseif (isset($this->bb->input['step']) && $this->bb->input['step'] == 3) {
                // Define the recipients/conditions
                if ($this->bb->request_method == 'post') {
                    // Need to perform the search to fetch the number of users we're emailing
                    $member_query = $this->mail->buildMassMailQuery($this->bb->input['conditions']);
                    $query = $this->db->simple_select('users u', 'COUNT(uid) AS num', $member_query);
                    $num = $this->db->fetch_field($query, 'num');

                    if ($num == 0) {
                        $errors[] = $this->lang->error_no_users;
                    } // Got one or more results
                    else {
                        $updated_email = [
                            'totalcount' => $num,
                            'conditions' => $this->db->escape_string(my_serialize($this->bb->input['conditions']))
                        ];

                        $this->plugins->runHooks('admin_user_mass_email_send_define_commit');

                        $this->db->update_query('massemails', $updated_email, "mid='{$email['mid']}'");

                        // Take the user to the next step
                        return $response->withRedirect($this->bb->admin_url . '/users/mass_mail?action=send&step=4&mid=' . $email['mid']);
                    }
                }

                $html = $this->page->output_header("{$this->lang->send_mass_mail}: {$this->lang->step_three}");

                $form = new Form($this->bb, $this->bb->admin_url . '/users/mass_mail?action=send&step=3&mid=' . $email['mid'], 'post');
                $html .= $form->getForm();
                $html .= $this->page->output_nav_tabs($sub_tabs, 'send_mass_mail');

                // If we have any error messages, show them
                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                    $input = $this->bb->input;
                } else {
                    if ($email['conditions'] != '') {
                        $input = [
                            'conditions' => my_unserialize($email['conditions'])
                        ];
                    } else {
                        $input = [];
                    }
                }

                $options = [
                    'username', 'email', 'postnum_dir', 'postnum', 'regdate', 'regdate_date',
                    'regdate_dir', 'lastactive', 'lastactive_date', 'lastactive_dir'
                ];

                foreach ($options as $option) {
                    if (!isset($input['conditions'][$option])) {
                        $input['conditions'][$option] = '';
                    }
                }
                if (!isset($input['conditions']['usergroup']) || !is_array($input['conditions']['usergroup'])) {
                    $input['conditions']['usergroup'] = [];
                }

                $form_container = new FormContainer($this, "{$this->lang->send_mass_mail}: {$this->lang->step_three} - {$this->lang->define_the_recipients}");

                $form_container->output_row($this->lang->username_contains, '', $form->generate_text_box('conditions[username]', $input['conditions']['username'], ['id' => 'username']), 'username');
                $form_container->output_row($this->lang->email_addr_contains, '', $form->generate_text_box('conditions[email]', $input['conditions']['email'], ['id' => 'email']), 'email');

                $options = [];
                $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
                while ($usergroup = $this->db->fetch_array($query)) {
                    $options[$usergroup['gid']] = $usergroup['title'];
                }

                $form_container->output_row($this->lang->members_of, $this->lang->additional_user_groups_desc, $form->generate_select_box('conditions[usergroup][]', $options, $input['conditions']['usergroup'], ['id' => 'usergroups', 'multiple' => true, 'size' => 5]), 'usergroups');

                $greater_options = [
                    'greater_than' => $this->lang->greater_than,
                    'is_exactly' => $this->lang->is_exactly,
                    'less_than' => $this->lang->less_than
                ];
                $form_container->output_row($this->lang->post_count_is, '', $form->generate_select_box(
                    'conditions[postnum_dir]',
                    $greater_options,
                    $input['conditions']['postnum_dir'],
                    ['id' => 'postnum_dir']
                ) . ' ' . $form->generate_numeric_field('conditions[postnum]', $input['conditions']['postnum'], ['id' => 'postnum', 'min' => 0]), 'postnum');

                $more_options = [
                    'more_than' => $this->lang->more_than,
                    'less_than' => $this->lang->less_than
                ];

                $date_options = [
                    'hours' => $this->lang->hours,
                    'days' => $this->lang->days,
                    'weeks' => $this->lang->weeks,
                    'months' => $this->lang->months,
                    'years' => $this->lang->years
                ];
                $form_container->output_row($this->lang->user_registered, '', $form->generate_select_box(
                    'conditions[regdate_dir]',
                    $more_options,
                    $input['conditions']['regdate_dir'],
                    ['id' => 'regdate_dir']
                ) . ' ' . $form->generate_numeric_field(
                    'conditions[regdate]',
                    $input['conditions']['regdate'],
                    ['id' => 'regdate', 'min' => 0]
                ) . ' ' . $form->generate_select_box(
                    'conditions[regdate_date]',
                    $date_options,
                    $input['conditions']['regdate_date'],
                    ['id' => 'regdate_date']
                ) . " {$this->lang->ago}", 'regdate');

                $form_container->output_row($this->lang->user_last_active, '', $form->generate_select_box(
                    'conditions[lastactive_dir]',
                    $more_options,
                    $input['conditions']['lastactive_dir'],
                    ['id' => 'lastactive_dir']
                ) . ' ' . $form->generate_numeric_field(
                    'conditions[lastactive]',
                    $input['conditions']['lastactive'],
                    ['id' => 'lastactive', 'min' => 0]
                ) . ' ' . $form->generate_select_box(
                    'conditions[lastactive_date]',
                    $date_options,
                    $input['conditions']['lastactive_date'],
                    ['id' => 'lastactive_date']
                ) . " {$this->lang->ago}", 'lastactive');

                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->next_step);
                $html .= $form->output_submit_wrapper($buttons);

                $html .= $form->end();
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Users/MassMailSendStep3.html.twig');
            } // Reviewing the automatic text based version of the message.
            elseif (isset($this->bb->input['step']) && $this->bb->input['step'] == 2) {
                // Update text based version
                if ($this->bb->request_method == 'post') {
                    if (!trim($this->bb->input['message'])) {
                        $errors[] = $this->lang->error_missing_plain_text;
                    } else {
                        $updated_email = [
                            'message' => $this->db->escape_string($this->bb->input['message'])
                        ];

                        $this->plugins->runHooks('admin_user_mass_email_send_review_commit');

                        $this->db->update_query('massemails', $updated_email, "mid='{$email['mid']}'");

                        // Take the user to the next step
                        return $response->withRedirect($this->bb->admin_url . '/users/mass_mail?action=send&step=3&mid=' . $email['mid']);
                    }
                }

                $html = $this->page->output_header("{$this->lang->send_mass_mail}: {$this->lang->step_two}");

                $form = new Form($this->bb, $this->bb->admin_url . '/users/mass_mail?action=send&step=2&mid='.$email['mid'], 'post');
                $html .= $form->getForm();
                $html .= $this->page->output_nav_tabs($sub_tabs, 'send_mass_mail');

                // If we have any error messages, show them
                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                }

                $form_container = new FormContainer($this, "{$this->lang->send_mass_mail}: {$this->lang->step_two} - {$this->lang->review_text_version}");
                $form_container->output_row("{$this->lang->review_text_version_desc}:", $text_personalisation, $form->generate_text_area('message', $email['message'], ['id' => 'message', 'rows' => 15, 'cols ' => 70, 'style' => 'width: 95%']));
                $html .= $form_container->end();

                $buttons[] = $form->generate_submit_button($this->lang->next_step);
                $html .= $form->output_submit_wrapper($buttons);

                $html .= $form->end();
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Users/MassMailSendStep2.html.twig');
            } elseif (!isset($this->bb->input['step']) || $this->bb->input['step'] == 1) {
                if ($this->bb->request_method == 'post') {
                    if (!trim($this->bb->input['subject'])) {
                        $errors[] = $this->lang->error_missing_subject;
                    }

                    if ($this->bb->input['type'] == 1) {
                        if (!$this->bb->input['message']) {
                            $errors[] = $this->lang->error_missing_message;
                        }
                    } else {
                        if ($this->bb->input['format'] == 2 && $this->bb->input['automatic_text'] == 0 && !$this->bb->input['message']) {
                            $errors[] = $this->lang->error_missing_plain_text;
                        }

                        if (($this->bb->input['format'] == 1 || $this->bb->input['format'] == 2) && !$this->bb->input['htmlmessage']) {
                            $errors[] = $this->lang->error_missing_html;
                        } elseif ($this->bb->input['format'] == 0 && !$this->bb->input['message']) {
                            $errors[] = $this->lang->error_missing_plain_text;
                        }
                    }

                    // No errors, insert away
                    if (!isset($errors)) {
                        if (!$new_email['mid']) {
                            // Sending via a PM
                            if ($this->bb->input['type'] == 1) {
                                $this->bb->input['format'] = 0;
                                $this->bb->input['htmlmessage'] = '';
                            } // Sending via email
                            else {
                                // Do we need to generate a text based version?
                                if ($this->bb->input['format'] == 2 && $this->bb->input['automatic_text']) {
                                    $this->bb->input['message'] = $this->mail->createTextMessage($this->bb->input['htmlmessage']);
                                } elseif ($this->bb->input['format'] == 1) {
                                    $this->bb->input['message'] = '';
                                } elseif ($this->bb->input['format'] == 0) {
                                    $this->bb->input['htmlmessage'] = '';
                                }
                            }

                            $new_email = [
                                'uid' => $this->user->uid,
                                'subject' => $this->db->escape_string($this->bb->input['subject']),
                                'message' => $this->db->escape_string($this->bb->input['message']),
                                'htmlmessage' => $this->db->escape_string($this->bb->input['htmlmessage']),
                                'format' => $this->bb->getInput('format', 0),
                                'type' => $this->bb->getInput('type', 0),
                                'dateline' => TIME_NOW,
                                'senddate' => 0,
                                'status' => 0,
                                'sentcount' => 0,
                                'totalcount' => 0,
                                'conditions' => '',
                                'perpage' => $this->bb->getInput('perpage', 0)
                            ];

                            $mid = $this->db->insert_query('massemails', $new_email);

                            $this->plugins->runHooks('admin_user_mass_email_send_insert_commit');
                        } // Updating an existing one
                        else {
                            $updated_email = [
                                'subject' => $this->db->escape_string($this->bb->input['subject']),
                                'message' => $this->db->escape_string($this->bb->input['message']),
                                'htmlmessage' => $this->db->escape_string($this->bb->input['htmlmessage']),
                                'format' => $this->bb->getInput('format', 0),
                                'type' => $this->bb->getInput('type', 0),
                                'perpage' => $this->bb->getInput('perpage', 0)
                            ];

                            $this->plugins->runHooks('admin_user_mass_email_send_update_commit');

                            $this->db->update_query('massemails', $updated_email, "mid='{$email['mid']}'");
                            $mid = $email['mid'];
                        }

                        if ($this->bb->input['format'] == 2 && $this->bb->input['automatic_text'] == 1) {
                            $next = 2;
                        } else {
                            $next = 3;
                        }
                        return $response->withRedirect($this->bb->admin_url . "/users/mass_mail?action=send&step={$next}&mid={$mid}");
                    }
                }

                $html = $this->page->output_header("{$this->lang->send_mass_mail}: {$this->lang->step_one}");

                $mid_add = '';
                if (isset($email['mid'])) {
                    $mid_add = "&amp;mid={$email['mid']}";
                }

                $form = new Form($this->bb, $this->bb->admin_url . '/users/mass_mail?action=send' . $mid_add, 'post');
                $html .= $form->getForm();
                $html .= $this->page->output_nav_tabs($sub_tabs, 'send_mass_mail');

                // If we have any error messages, show them
                if (isset($errors)) {
                    $html .= $this->page->output_inline_error($errors);
                    $input = $this->bb->input;
                } elseif (!isset($email)) {
                    $input = [
                        'type' => 0,
                        'format' => 2,
                        'automatic_text' => 1,
                        'perpage' => 50,
                    ];
                } else {
                    $input = $email;
                }

                $form_container = new FormContainer($this, "{$this->lang->send_mass_mail}: {$this->lang->step_one} - {$this->lang->message_settings}");

                $form_container->output_row("{$this->lang->subject}: <em>*</em>", $this->lang->subject_desc, $form->generate_text_box('subject', $input['subject'], ['id' => 'subject']), 'subject');

//        if(isset($this->bb->input['type'])) {
                if ($this->bb->input['type'] == 0) {
                    $type_email_checked = true;
                    $type_pm_checked = false;
                } elseif ($this->bb->input['type'] == 1) {
                    $type_email_checked = false;
                    $type_pm_checked = true;
                }
//        }

                $type_options = [
                    $form->generate_radio_button('type', 0, $this->lang->send_via_email, ['id' => 'type_email', 'checked' => $type_email_checked]),
                    $form->generate_radio_button('type', 1, $this->lang->send_via_pm, ['id' => 'type_pm', 'checked' => $type_pm_checked])
                ];
                $form_container->output_row("{$this->lang->message_type}:", '', implode('<br />', $type_options));

                $format_options = [
                    0 => $this->lang->plain_text_only,
                    1 => $this->lang->html_only,
                    2 => $this->lang->html_and_plain_text
                ];

                $form_container->output_row("{$this->lang->message_format}:", '', $form->generate_select_box(
                    'format',
                    $format_options,
                    $input['format'],
                    ['id' => 'format']
                ), 'format', null, ['id' => 'format_container']);

                $form_container->output_row("{$this->lang->per_page}: <em>*</em>", $this->lang->per_page_desc, $form->generate_numeric_field('perpage', $input['perpage'], ['id' => 'perpage', 'min' => 1]), 'perpage');

                $html .= $form_container->end();

                if (isset($this->bb->input['format'])) {
                    if ($this->bb->input['format'] == 2) {
                        if ($this->bb->input['automatic_text'] && !$email['mid']) {
                            $automatic_text_check = true;
                            $text_display = 'display: none';
                            $automatic_display = 'display: none;';
                        }
                    } elseif ($this->bb->input['format'] == 1 && $this->bb->input['type'] != 1) {
                        $text_display = 'display: none;';
                    } elseif ($this->bb->input['format'] == 0 || $this->bb->input['type'] == 1) {
                        $html_display = 'display: none';
                    }
                }

                $html .= '<div id="message_html" style="'.($html_display).'">';
                $form_container = new FormContainer($this, "{$this->lang->send_mass_mail}: {$this->lang->step_one} - {$this->lang->define_html_message}");
                $form_container->output_row(
                    "{$this->lang->define_html_message_desc}:",
                    $html_personalisation,
                    $form->generate_text_area(
                        'htmlmessage',
                        $input['htmlmessage'],
                        ['id' => 'htmlmessage', 'rows' => 15, 'cols ' => 70, 'style' => 'width: 95%']
                    ) .
                    '<div id="automatic_display" style="'.($automatic_display).'">' .
                    $form->generate_check_box('automatic_text', 1, $this->lang->auto_gen_plain_text, ['checked' => $automatic_text_check, 'id' => 'automatic_text']) . '</div>'
                );
                $html .= $form_container->end();
                $html .= '</div>';

                $html .= '<div id="message_text" style="'.($text_display).'">';
                $form_container = new FormContainer($this, "{$this->lang->send_mass_mail}: {$this->lang->step_one} - {$this->lang->define_text_version}");
                $form_container->output_row(
                    "{$this->lang->define_text_version_desc}:",
                    $text_personalisation,
                    $form->generate_text_area(
                        'message',
                        $input['message'],
                        ['id' => 'message', 'rows' => 15, 'cols ' => 70, 'style' => 'width: 95%']
                    )
                );
                $html .= $form_container->end();
                $html .= '</div>';

                $html .= "
		<script type=\"text/javascript\">
		function ToggleFormat()
		{
			var v = $('#format option:selected').val();
			if(v == 2)
			{
				$('#automatic_display').show();
				$('#message_html').show();
				if($('#automatic_text').checked)
				{
					$('#message_text').hide();
				}
				else
				{
					$('#message_text').show();
				}
			}
			else if(v == 1)
			{
				$('#message_text').hide();
				$('#message_html').show();
				$('#automatic_display').hide();
			}
			else
			{
				$('#message_text').show();
				$('#message_html').hide();
			}
		}
		$(document).on('change', '#format', function() {
			ToggleFormat();
		});

		function ToggleType()
		{
			var v = $('#type_pm').prop('checked');
			if(v == true)
			{
				$('#message_html').hide();
				$('#message_text').show();
				$('#format_container').hide();
			}
			else
			{
				$('#message_html').show();
				$('#format_container').show();
				ToggleFormat();
			}
		}
		$('#type_pm').on('click', function() {
			ToggleType();
		});
		$('#type_email').on('click', function() {
			ToggleType();
		});
		ToggleType();

		function ToggleAutomatic()
		{
			var v = $('#automatic_text').prop('checked');
			if(v == true)
			{
				$('#message_text').hide();
			}
			else
			{
				$('#message_text').show();
			}
		}

		$('#automatic_text').on('click', function() {
			ToggleAutomatic();
		});

		function insertText(value, textarea)
		{
			textarea = document.getElementById(textarea);
			// Internet Explorer
			if(document.selection)
			{
				textarea.focus();
				var selection = document.selection.createRange();
				selection.text = value;
			}
			// Firefox
			else if(textarea.selectionStart || textarea.selectionStart == '0')
			{
				var start = textarea.selectionStart;
				var end = textarea.selectionEnd;
				textarea.value = textarea.value.substring(0, start)	+ value	+ textarea.value.substring(end, textarea.value.length);
			}
			else
			{
				textarea.value += value;
			}
		}

		</script>";

                $buttons[] = $form->generate_submit_button($this->lang->next_step);
                $html .= $form->output_submit_wrapper($buttons);

                $html .= $form->end();
                $this->page->output_footer();

                $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                return $this->view->render($response, '@forum/Admin/Users/MassMailSend.html.twig');
            }

            $this->plugins->runHooks('admin_user_mass_email_preview_end');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('massemails', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $mass_email = $this->db->fetch_array($query);

            if (!$mass_email['mid']) {
                $this->session->flash_message($this->lang->error_delete_invalid_mid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
            }

            $this->plugins->runHooks('admin_user_mass_email_delete_start');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('massemails', "mid='{$mass_email['mid']}'");

                $this->plugins->runHooks('admin_user_mass_email_delete_commit');

                // Log admin action
                $this->bblogger->log_admin_action($mass_email['mid'], $mass_email['subject']);

                if ($this->bb->input['archive'] == 1) {
                    $this->session->flash_message($this->lang->success_mass_mail_deleted, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/mass_mail?action=archive');
                } else {
                    $this->session->flash_message($this->lang->success_mass_mail_deleted, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
                }
            } else {
                if ($this->bb->input['archive'] == 1) {
                    $this->page->output_confirm_action($this->bb->admin_url . "/users/mass_mail?action=delete&amp;mid={$mass_email['mid']}&amp;archive=1", $this->lang->mass_mail_deletion_confirmation);
                } else {
                    $this->page->output_confirm_action($this->bb->admin_url . '/users/mass_mail?action=delete&mid=' . $mass_email['mid'], $this->lang->mass_mail_deletion_confirmation);
                }
            }
        }

        if ($this->bb->input['action'] == 'preview') {
            $query = $this->db->simple_select('massemails', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $mass_email = $this->db->fetch_array($query);

            if (!$mass_email['mid']) {
                $this->session->flash_message($this->lang->error_invalid_mid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
            }

            $this->plugins->runHooks('admin_user_mass_email_preview_start');

            echo '<div class="modal">
	<div style="overflow-y: auto; max-height: 400px;">';

            $table = new Table();

            if ($this->bb->input['format'] == 'text' || !$mass_email['htmlmessage']) {
                // Show preview of the text version
                $table->construct_cell(nl2br($mass_email['message']));
            } else {
                // Preview the HTML version
                $table->construct_cell($mass_email['htmlmessage']);
            }

            $this->plugins->runHooks('admin_user_mass_email_preview_end');

            $table->construct_row();

            echo $table->output($this->lang->mass_mail_preview);

            echo '</div>
</div>';
            exit;
        }

        if ($this->bb->input['action'] == 'resend') {
            // Copy and resend an email
            $query = $this->db->simple_select('massemails', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $mass_email = $this->db->fetch_array($query);

            if (!$mass_email['mid']) {
                $this->session->flash_message($this->lang->error_invalid_mid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
            }

            $this->plugins->runHooks('admin_user_mass_email_resend_start');

            // Need to perform the search to fetch the number of users we're emailing
            $member_query = $this->mail->buildMassMailQuery(my_unserialize($mass_email['conditions']));
            $query = $this->db->simple_select('users u', 'COUNT(uid) AS num', $member_query);
            $total_recipients = $this->db->fetch_field($query, 'num');

            // Create the new email based off the old one.
            $new_email = [
                'uid' => $mass_email['uid'],
                'subject' => $this->db->escape_string($mass_email['subject']),
                'message' => $this->db->escape_string($mass_email['message']),
                'htmlmessage' => $this->db->escape_string($mass_email['htmlmessage']),
                'type' => $this->db->escape_string($mass_email['type']),
                'format' => $this->db->escape_string($mass_email['format']),
                'dateline' => TIME_NOW,
                'senddate' => '0',
                'status' => 0,
                'sentcount' => 0,
                'totalcount' => $total_recipients,
                'conditions' => $this->db->escape_string($mass_email['conditions']),
                'perpage' => $mass_email['perpage']
            ];

            $mid = $this->db->insert_query('massemails', $new_email);

            $this->plugins->runHooks('admin_user_mass_email_resend_end');

            // Redirect the user to the summary page so they can select when to deliver this message
            $this->session->flash_message($this->lang->success_mass_mail_resent, 'success');
            return $response->withRedirect($this->bb->admin_url . '/users/mass_mail?action=send&step=4&mid=' . $mid);
        }

        if ($this->bb->input['action'] == 'cancel') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            // Cancel the delivery of a mass-email.
            $query = $this->db->simple_select('massemails', '*', "mid='" . $this->bb->getInput('mid', 0) . "'");
            $mass_email = $this->db->fetch_array($query);

            if (!$mass_email['mid']) {
                $this->session->flash_message($this->lang->error_invalid_mid, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
            }

            $updated_email = [
                'status' => 4
            ];

            $this->plugins->runHooks('admin_user_mass_email_cancel');

            $this->db->update_query('massemails', $updated_email, "mid='{$mass_email['mid']}'");

            $this->session->flash_message($this->lang->success_mass_mail_canceled, 'success');
            return $response->withRedirect($this->bb->admin_url . '/users/mass_mail');
        }

        if ($this->bb->input['action'] == 'archive') {
            // View a list of archived email messages
            $html = $this->page->output_header($this->lang->mass_mail_archive);

            $this->plugins->runHooks('admin_user_mass_email_archive_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'archive');

            $table = new Table;
            $table->construct_header($this->lang->subject);
            $table->construct_header($this->lang->status, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->delivery_date, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->recipients, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 200]);

            $query = $this->db->simple_select('massemails', '*', 'status NOT IN (0, 1, 2)', ['order_by' => 'senddate']);
            while ($email = $this->db->fetch_array($query)) {
                $email['subject'] = htmlspecialchars_uni($email['subject']);
                if ($email['senddate'] < TIME_NOW) {
                    $table->construct_cell("<strong>{$email['subject']}</strong>");
                }
                if ($email['status'] == 3) {
                    $status = $this->lang->delivered;
                } elseif ($email['status'] == 4) {
                    $status = $this->lang->canceled;
                }
                $table->construct_cell($status, ['class' => 'align_center']);

                $delivery_date = $this->time->formatDate($this->bb->settings['dateformat'], $email['senddate']);

                $table->construct_cell($delivery_date, ['class' => 'align_center']);
                $table->construct_cell($this->parser->formatNumber($email['totalcount']), ['class' => 'align_center']);

                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/mass_mail?action=resend&amp;mid={$email['mid']}\">{$this->lang->resend}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/mass_mail?action=delete&amp;mid={$email['mid']}&amp;my_post_key={$this->bb->post_code}&amp;archive=1\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->mass_mail_deletion_confirmation}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);

                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_archived_messages, ['colspan' => 6]);
                $table->construct_row();
                $no_results = true;
            }

            $this->plugins->runHooks('admin_user_mass_email_archive_end');

            $html .= $table->output($this->lang->mass_mail_archive);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/MassMailArchive.html.twig');
        }

        if (!$this->bb->input['action']) {
            $html = $this->page->output_header($this->lang->mass_mail_queue);

            $this->plugins->runHooks('admin_user_mass_email_start');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'mail_queue');

            $table = new Table;
            $table->construct_header($this->lang->subject);
            $table->construct_header($this->lang->status, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->delivery_date, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->recipients, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 200]);

            $query = $this->db->simple_select('massemails', '*', 'status IN (0, 1, 2)', ['order_by' => 'senddate']);
            while ($email = $this->db->fetch_array($query)) {
                $email['subject'] = htmlspecialchars_uni($email['subject']);
                if (TIME_NOW >= $email['senddate'] && $email['status'] > 1) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/mass_mail?action=edit&amp;mid={$email['mid']}\"><strong>{$email['subject']}</strong></a>");
                } else {
                    $table->construct_cell("<strong>{$email['subject']}</strong>");
                }
                if ($email['status'] == 0) {
                    $status = $this->lang->draft;
                } elseif ($email['status'] == 1) {
                    $status = $this->lang->queued;
                } elseif ($email['status'] == 2) {
                    $progress = ceil($email['sentcount'] / $email['totalcount'] * 100);
                    if ($progress > 100) {
                        $progress = 100;
                    }
                    $status = "{$this->lang->delivering} ({$progress}%)";
                }
                $table->construct_cell($status, ['class' => 'align_center']);

                if ($email['status'] != 0) {
                    $delivery_date = $this->time->formatDate($this->bb->settings['dateformat'], $email['senddate']);
                } else {
                    $delivery_date = $this->lang->na;
                }

                $table->construct_cell($delivery_date, ['class' => 'align_center']);
                $table->construct_cell($this->parser->formatNumber($email['totalcount']), ['class' => 'align_center']);
                if (TIME_NOW >= $email['senddate'] && $email['status'] > 1) {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/mass_mail?action=cancel&amp;mid={$email['mid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->mass_mail_cancel_confirmation}')\">{$this->lang->cancel}</a>", ["width" => 100, "colspan" => 2, "class" => "align_center"]);
                } else {
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/mass_mail?action=edit&amp;mid={$email['mid']}\">{$this->lang->edit}</a>", ["width" => 100, "class" => "align_center"]);
                    $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/mass_mail?action=delete&amp;mid={$email['mid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->mass_mail_deletion_confirmation}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);
                }
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_unsent_messages, ['colspan' => 6]);
                $table->construct_row();
                $no_results = true;
            }

            $this->plugins->runHooks('admin_user_mass_email_end');

            $html .= $table->output($this->lang->mass_mail_queue);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/MassMail.html.twig');
        }
    }
}
