<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

class Meta
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * @return bool true
     */
    public function users_meta()
    {
        $sub_menu = [];
        $sub_menu['10'] = ['id' => 'users', 'title' => $this->bb->lang->users, 'link' => $this->bb->admin_url . '/users'];
        $sub_menu['20'] = ['id' => 'groups', 'title' => $this->bb->lang->groups, 'link' => $this->bb->admin_url . '/users/groups'];
        $sub_menu['30'] = ['id' => 'titles', 'title' => $this->bb->lang->user_titles, 'link' => $this->bb->admin_url . '/users/titles'];
        $sub_menu['40'] = ['id' => 'banning', 'title' => $this->bb->lang->banning, 'link' => $this->bb->admin_url . '/users/banning'];
        $sub_menu['50'] = ['id' => 'adminperm', 'title' => $this->bb->lang->admin_permissions, 'link' => $this->bb->admin_url . '/users/adminperm'];
        $sub_menu['60'] = ['id' => 'mass_mail', 'title' => $this->bb->lang->mass_mail, 'link' => $this->bb->admin_url . '/users/mass_mail'];
        $sub_menu['70'] = ['id' => 'group_promotions', 'title' => $this->bb->lang->group_promotions, 'link' => $this->bb->admin_url . '/users/group_promotions'];

        $sub_menu = $this->bb->plugins->runHooks('admin_user_menu', $sub_menu);

        $this->bb->page->add_menu_item($this->bb->lang->users, 'users', $this->bb->admin_url . '/users', 30, $sub_menu);
        return true;
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public function users_action_handler($action)
    {
        $this->bb->page->active_module = 'users';

        $actions = [
            'group_promotions' => ['active' => 'group_promotions', 'file' => 'group_promotions.php'],
            'adminperm' => ['active' => 'adminperm', 'file' => 'admin_permissions.php'],
            'titles' => ['active' => 'titles', 'file' => 'titles.php'],
            'banning' => ['active' => 'banning', 'file' => 'banning.php'],
            'groups' => ['active' => 'groups', 'file' => 'groups.php'],
            'mass_mail' => ['active' => 'mass_mail', 'file' => 'mass_mail.php'],
            'users' => ['active' => 'users', 'file' => 'users.php']
        ];

        $actions = $this->bb->plugins->runHooks('admin_user_action_handler', $actions);

        //\RunBB\Init
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('forum-users');

        if (isset($actions[$action])) {
            $this->bb->page->active_action = $actions[$action]['active'];
            return $actions[$action]['file'];
        } else {
            $this->bb->page->active_action = 'users';
            return 'users.php';
        }
    }

    /**
     * @return array
     */
    public function users_admin_permissions()
    {
        $admin_permissions = [
            'users' => $this->bb->lang->can_manage_users,
            'groups' => $this->bb->lang->can_manage_user_groups,
            'titles' => $this->bb->lang->can_manage_user_titles,
            'banning' => $this->bb->lang->can_manage_user_bans,
            'adminperm' => $this->bb->lang->can_manage_admin_permissions,
            'mass_mail' => $this->bb->lang->can_send_mass_mail,
            'group_promotions' => $this->bb->lang->can_manage_group_promotions
        ];

        $admin_permissions = $this->bb->plugins->runHooks('admin_user_permissions', $admin_permissions);

        return ['name' => $this->bb->lang->users, 'permissions' => $admin_permissions, 'disporder' => 30];
    }
}
