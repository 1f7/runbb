<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\FormContainer;
use RunCMF\Core\AbstractController;

class Titles extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_titles', false, true);

        $this->page->add_breadcrumb_item($this->lang->user_titles, $this->bb->admin_url . '/users/titles');

        if ($this->bb->input['action'] == 'add' || !$this->bb->input['action']) {
            $sub_tabs['manage_titles'] = [
                'title' => $this->lang->user_titles,
                'link' => $this->bb->admin_url . '/users/titles',
                'description' => $this->lang->user_titles_desc
            ];
            $sub_tabs['add_title'] = [
                'title' => $this->lang->add_new_user_title,
                'link' => $this->bb->admin_url . '/users/titles?action=add',
                'description' => $this->lang->add_new_user_title_desc
            ];
        }

        $this->plugins->runHooks('admin_user_titles_begin');

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_user_titles_add');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!isset($this->bb->input['posts'])) {
                    $errors[] = $this->lang->error_missing_posts;
                }

                $query = $this->db->simple_select('usertitles', 'utid', "posts= '" . $this->bb->getInput('posts', 0) . "'");
                if ($this->db->num_rows($query)) {
                    $errors[] = $this->lang->error_cannot_have_same_posts;
                }

                if (!isset($errors)) {
                    $new_title = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'posts' => $this->bb->getInput('posts', 0),
                        'stars' => $this->bb->getInput('stars', 0),
                        'starimage' => $this->db->escape_string($this->bb->input['starimage'])
                    ];

                    $utid = $this->db->insert_query('usertitles', $new_title);

                    $this->plugins->runHooks('admin_user_titles_add_commit');

                    $this->cache->update_usertitles();

                    // Log admin action
                    $this->bblogger->log_admin_action($utid, htmlspecialchars_uni($this->bb->input['title']), $this->bb->input['posts']);

                    $this->session->flash_message($this->lang->success_user_title_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/titles');
                }
            } else {
                $this->bb->input = array_merge(
                    $this->bb->input,
                    [
                        'stars' => '1',
                        'starimage' => '{theme}/star.png',
                    ]
                );
            }

            $this->page->add_breadcrumb_item($this->lang->add_new_user_title);
            $html = $this->page->output_header($this->lang->user_titles . ' - ' . $this->lang->add_new_user_title);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'add_title');
            $form = new Form($this->bb, $this->bb->admin_url . '/users/titles?action=add', 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->add_new_user_title);
            $form_container->output_row($this->lang->title_to_assign . '<em>*</em>', $this->lang->title_to_assign_desc, $form->generate_text_box('title', $this->bb->getInput('title', '', true), ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->minimum_posts, $this->lang->minimum_posts_desc, $form->generate_numeric_field('posts', $this->bb->getInput('posts', ''), ['id' => 'posts', 'min' => 0]), 'posts');
            $form_container->output_row($this->lang->number_of_stars, $this->lang->number_of_stars_desc, $form->generate_numeric_field('stars', $this->bb->input['stars'], ['id' => 'stars', 'min' => 0]), 'stars');
            $form_container->output_row($this->lang->star_image, $this->lang->star_image_desc, $form->generate_text_box('starimage', $this->bb->input['starimage'], ['id' => 'starimage']), 'starimage');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_user_title);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/TitlesAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $query = $this->db->simple_select('usertitles', '*', "utid='" . $this->bb->getInput('utid', 0) . "'");
            $usertitle = $this->db->fetch_array($query);

            if (!$usertitle['utid']) {
                $this->session->flash_message($this->lang->error_invalid_user_title, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/titles');
            }

            $this->plugins->runHooks('admin_user_titles_edit');

            if ($this->bb->request_method == 'post') {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_title;
                }

                if (!isset($this->bb->input['posts'])) {
                    $errors[] = $this->lang->error_missing_posts;
                }

                $query = $this->db->simple_select('usertitles', 'utid', "posts= '" . $this->bb->getInput('posts', 0) . "' AND utid!= '" . $this->bb->getInput('utid', 0) . "'");
                if ($this->db->num_rows($query)) {
                    $errors[] = $this->lang->error_cannot_have_same_posts;
                }

                if (!isset($errors)) {
                    $updated_title = [
                        'title' => $this->db->escape_string($this->bb->input['title']),
                        'posts' => $this->bb->getInput('posts', 0),
                        'stars' => $this->bb->getInput('stars', 0),
                        'starimage' => $this->db->escape_string($this->bb->input['starimage'])
                    ];

                    $this->plugins->runHooks('admin_user_titles_edit_commit');

                    $this->db->update_query('usertitles', $updated_title, "utid='{$usertitle['utid']}'");

                    $this->cache->update_usertitles();

                    // Log admin action
                    $this->bblogger->log_admin_action($usertitle['utid'], htmlspecialchars_uni($this->bb->input['title']), $this->bb->input['posts']);

                    $this->session->flash_message($this->lang->success_user_title_updated, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users/titles');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_user_title);
            $html = $this->page->output_header($this->lang->user_titles . ' - ' . $this->lang->edit_user_title);

            $sub_tabs['edit_title'] = [
                'title' => $this->lang->edit_user_title,
                'link' => $this->bb->admin_url . '/users/titles?action=edit&utid=' . $usertitle['utid'],
                'description' => $this->lang->edit_user_title_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_title');
            $form = new Form($this->bb, $this->bb->admin_url . '/users/titles?action=edit&utid=' . $usertitle['utid'], 'post');
            $html .= $form->getForm();

            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, $usertitle);
            }

            $form_container = new FormContainer($this, $this->lang->edit_user_title);
            $form_container->output_row($this->lang->title_to_assign . '<em>*</em>', $this->lang->title_to_assign_desc, $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');
            $form_container->output_row($this->lang->minimum_posts, $this->lang->minimum_posts_desc, $form->generate_numeric_field('posts', $this->bb->input['posts'], ['id' => 'posts', 'min' => 0]), 'posts');
            $form_container->output_row($this->lang->number_of_stars, $this->lang->number_of_stars_desc, $form->generate_numeric_field('stars', $this->bb->input['stars'], ['id' => 'stars', 'min' => 0]), 'stars');
            $form_container->output_row($this->lang->star_image, $this->lang->star_image_desc, $form->generate_text_box('starimage', $this->bb->input['starimage'], ['id' => 'starimage']), 'starimage');
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->save_user_title);

            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/TitlesEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $query = $this->db->simple_select('usertitles', '*', "utid='" . $this->bb->getInput('utid', 0) . "'");
            $usertitle = $this->db->fetch_array($query);

            if (!$usertitle['utid']) {
                $this->session->flash_message($this->lang->error_invalid_user_title, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users/titles');
            }

            // User clicked no
            if (isset($this->bb->input['no'])) {
                return $response->withRedirect($this->bb->admin_url . '/users/titles');
            }

            $this->plugins->runHooks('admin_user_titles_delete');

            if ($this->bb->request_method == 'post') {
                $this->db->delete_query('usertitles', "utid='{$usertitle['utid']}'");

                $this->plugins->runHooks('admin_user_titles_delete_commit');

                $this->cache->update_usertitles();

                // Log admin action
                $this->bblogger->log_admin_action($usertitle['utid'], htmlspecialchars_uni($usertitle['title']), $usertitle['posts']);

                $this->session->flash_message($this->lang->success_user_title_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users/titles');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users/titles?action=delete&utid=' . $usertitle['utid'], $this->lang->user_title_deletion_confirmation);
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks('admin_user_titles_start');

            $html = $this->page->output_header($this->lang->manage_user_titles);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'manage_titles');

            $table = new Table;
            $table->construct_header($this->lang->user_title);
            $table->construct_header($this->lang->minimum_posts, ['width' => '130', 'class' => 'align_center']);
            $table->construct_header($this->lang->controls, ['class' => 'align_center', 'colspan' => 2, 'width' => 200]);

            $query = $this->db->simple_select('usertitles', '*', '', ['order_by' => 'posts']);
            while ($usertitle = $this->db->fetch_array($query)) {
                $usertitle['title'] = htmlspecialchars_uni($usertitle['title']);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/titles?action=edit&amp;utid={$usertitle['utid']}\"><strong>{$usertitle['title']}</strong></a>");
                $table->construct_cell($usertitle['posts'], ['class' => 'align_center']);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/titles?action=edit&amp;utid={$usertitle['utid']}\">{$this->lang->edit}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_cell("<a href=\"".$this->bb->admin_url."/users/titles?action=delete&amp;utid={$usertitle['utid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"return AdminCP.deleteConfirmation(this, '{$this->lang->user_title_deletion_confirmation}')\">{$this->lang->delete}</a>", ["width" => 100, "class" => "align_center"]);
                $table->construct_row();
            }

            if ($table->num_rows() == 0) {
                $table->construct_cell($this->lang->no_user_titles, ['colspan' => 4]);
                $table->construct_row();
                $no_results = true;
            }

            $html .= $table->output($this->lang->manage_user_titles);

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/Titles.html.twig');
        }
    }
}
