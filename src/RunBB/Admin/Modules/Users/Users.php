<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Admin\Modules\Users;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use RunBB\Core\Image;
use RunBB\Admin\Helpers\Table;
use RunBB\Admin\Helpers\Form;
use RunBB\Admin\Helpers\FormContainer;
use RunBB\Admin\Helpers\PopupMenu;

class Users extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        $this->adm->init('users');// set active module
        $this->lang->load('users_users', false, true);

        $this->page->add_breadcrumb_item($this->lang->users, $this->bb->admin_url . '/users');

        if ($this->bb->input['action'] == 'add' ||
            $this->bb->input['action'] == 'merge' ||
            $this->bb->input['action'] == 'search' ||
            !$this->bb->input['action']
        ) {
            $sub_tabs['browse_users'] = [
                'title' => $this->lang->browse_users,
                'link' => $this->bb->admin_url . '/users',
                'description' => $this->lang->browse_users_desc
            ];

            $sub_tabs['find_users'] = [
                'title' => $this->lang->find_users,
                'link' => $this->bb->admin_url . '/users?action=search',
                'description' => $this->lang->find_users_desc
            ];

            $sub_tabs['create_user'] = [
                'title' => $this->lang->create_user,
                'link' => $this->bb->admin_url . '/users?action=add',
                'description' => $this->lang->create_user_desc
            ];

            $sub_tabs['merge_users'] = [
                'title' => $this->lang->merge_users,
                'link' => $this->bb->admin_url . '/users?action=merge',
                'description' => $this->lang->merge_users_desc
            ];
        }

        $this->user_view_fields = [
            'avatar' => [
                'title' => $this->lang->avatar,
                'width' => '24',
                'align' => ''
            ],
            'username' => [
                'title' => $this->lang->username,
                'width' => '',
                'align' => ''
            ],
            'email' => [
                'title' => $this->lang->email,
                'width' => '',
                'align' => 'center'
            ],
            'usergroup' => [
                'title' => $this->lang->primary_group,
                'width' => '',
                'align' => 'center'
            ],
            'additionalgroups' => [
                'title' => $this->lang->additional_groups,
                'width' => '',
                'align' => 'center'
            ],
            'regdate' => [
                'title' => $this->lang->registered,
                'width' => '',
                'align' => 'center'
            ],
            'lastactive' => [
                'title' => $this->lang->last_active,
                'width' => '',
                'align' => 'center'
            ],
            'postnum' => [
                'title' => $this->lang->post_count,
                'width' => '',
                'align' => 'center'
            ],
            'threadnum' => [
                'title' => $this->lang->thread_count,
                'width' => '',
                'align' => 'center'
            ],
            'reputation' => [
                'title' => $this->lang->reputation,
                'width' => '',
                'align' => 'center'
            ],
            'warninglevel' => [
                'title' => $this->lang->warning_level,
                'width' => '',
                'align' => 'center'
            ],
            'regip' => [
                'title' => $this->lang->registration_ip,
                'width' => '',
                'align' => 'center'
            ],
            'lastip' => [
                'title' => $this->lang->last_known_ip,
                'width' => '',
                'align' => 'center'
            ],
            'controls' => [
                'title' => $this->lang->controls,
                'width' => '',
                'align' => 'center'
            ]
        ];

        $sort_options = [
            'username' => $this->lang->username,
            'regdate' => $this->lang->registration_date,
            'lastactive' => $this->lang->last_active,
            'numposts' => $this->lang->post_count,
            'reputation' => $this->lang->reputation,
            'warninglevel' => $this->lang->warning_level
        ];

        $this->plugins->runHooks('admin_user_users_begin');


        if ($this->bb->input['action'] == 'views') {
            return $this->view_manager($this->bb->admin_url . '/users', 'user', $this->user_view_fields, $sort_options, 'user_search_conditions');
        }

        if ($this->bb->input['action'] == 'iplookup') {
            $this->bb->input['ipaddress'] = $this->bb->getInput('ipaddress', '');
            $this->lang->ipaddress_misc_info = $this->lang->sprintf($this->lang->ipaddress_misc_info, htmlspecialchars_uni($this->bb->input['ipaddress']));
            $ipaddress_location = $this->lang->na;
            $ipaddress_host_name = $this->lang->na;
            $modcp_ipsearch_misc_info = '';
            if (!strstr($this->bb->input['ipaddress'], '*')) {
                // Return GeoIP information if it is available to us
                if (function_exists('geoip_record_by_name')) {
                    $ip_record = @geoip_record_by_name($this->bb->input['ipaddress']);
                    if ($ip_record) {
                        $ipaddress_location = htmlspecialchars_uni(utf8_encode($ip_record['country_name']));
                        if ($ip_record['city']) {
                            $ipaddress_location .= $this->lang->comma . htmlspecialchars_uni(utf8_encode($ip_record['city']));
                        }
                    }
                }

                $ipaddress_host_name = htmlspecialchars_uni(@gethostbyaddr($this->bb->input['ipaddress']));

                // gethostbyaddr returns the same ip on failure
                if ($ipaddress_host_name == $this->bb->input['ipaddress']) {
                    $ipaddress_host_name = $this->lang->na;
                }
            }

            ?>
            <div class="modal">
                <div style="overflow-y: auto; max-height: 400px;">

                    <?php

                    $table = new Table();

                    $table->construct_cell($this->lang->ipaddress_host_name . ':');
                    $table->construct_cell($ipaddress_host_name);
                    $table->construct_row();

                    $table->construct_cell($this->lang->ipaddress_location . ':');
                    $table->construct_cell($ipaddress_location);
                    $table->construct_row();

                    echo $table->output($this->lang->ipaddress_misc_info);

                    ?>
                </div>
            </div>
            <?php
        }

        if ($this->bb->input['action'] == 'activate_user') {
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            $user = $this->user->get_user($this->bb->input['uid']);

            // Does the user not exist?
            if (!$user['uid'] || $user['usergroup'] != 5) {
                $this->session->flash_message($this->lang->error_invalid_user, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            $this->plugins->runHooks('admin_user_users_coppa_activate');

            $updated_user['usergroup'] = $user['usergroup'];

            // Update
            if ($user['coppauser']) {
                $updated_user = [
                    'coppauser' => 0
                ];
            } else {
                $this->db->delete_query('awaitingactivation', "uid='{$user['uid']}'");
            }

            // Move out of awaiting activation if they're in it.
            if ($user['usergroup'] == 5) {
                $updated_user['usergroup'] = 2;
            }

            $this->plugins->runHooks('admin_user_users_coppa_activate_commit');

            $this->db->update_query('users', $updated_user, "uid='{$user['uid']}'");

            $this->cache->update_awaitingactivation();

            $message = $this->lang->sprintf($this->lang->email_adminactivateaccount, $user['username'], $this->bb->settings['bbname'], $this->bb->settings['bburl']);
            $this->mail->send($user['email'], $this->lang->sprintf($this->lang->emailsubject_activateaccount, $this->bb->settings['bbname']), $message);

            // Log admin action
            $this->bblogger->log_admin_action($user['uid'], $user['username']);

            if ($this->bb->input['from'] == 'home') {
                if ($user['coppauser']) {
                    $message = $this->lang->success_coppa_activated;
                } else {
                    $message = $this->lang->success_activated;
                }

                $this->session->update_admin_session('flash_message2', ['message' => $message, 'type' => 'success']);
            } else {
                if ($user['coppauser']) {
                    $this->session->flash_message($this->lang->success_coppa_activated, 'success');
                } else {
                    $this->session->flash_message($this->lang->success_activated, 'success');
                }
            }

            if ($this->session->admin_session['data']['last_users_url']) {
                $url = $this->session->admin_session['data']['last_users_url'];
                $this->session->update_admin_session('last_users_url', '');

                if ($this->bb->input['from'] == 'home') {
                    $this->session->update_admin_session('from', 'home');
                }
            } else {
                $url = $this->bb->admin_url . '/users?action=edit&uid=' . $user['uid'];
            }

            $this->plugins->runHooks('admin_user_users_coppa_end');

            return $response->withRedirect($url);
        }

        if ($this->bb->input['action'] == 'add') {
            $this->plugins->runHooks('admin_user_users_add');
            if ($this->bb->request_method == 'post') {
                // Determine the usergroup stuff
                if (isset($this->bb->input['additionalgroups']) && is_array($this->bb->input['additionalgroups'])) {
                    foreach ($this->bb->input['additionalgroups'] as $key => $gid) {
                        if ($gid == $this->bb->input['usergroup']) {
                            unset($this->bb->input['additionalgroups'][$key]);
                        }
                    }
                    $additionalgroups = implode(',', $this->bb->input['additionalgroups']);
                } else {
                    $additionalgroups = '';
                }

                $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'insert');

                // Set the data for the new user.
                $new_user = [
                    'uid' => $this->bb->getInput('uid', 0),
                    'username' => $this->bb->getInput('username', '', true),
                    'password' => $this->bb->getInput('password', '', true),
                    'password2' => $this->bb->getInput('confirm_password', '', true),
                    'email' => $this->bb->getInput('email', ''),
                    'email2' => $this->bb->getInput('email', ''),
                    'usergroup' => $this->bb->getInput('usergroup', ''),
                    'additionalgroups' => $additionalgroups,
                    'displaygroup' => $this->bb->getInput('displaygroup', ''),
                    'profile_fields' => $this->bb->getInput('profile_fields', ''),
                    'profile_fields_editable' => true,
                ];

                // Set the data of the user in the datahandler.
                $userhandler->set_data($new_user);
                $errors = '';

                // Validate the user and get any errors that might have occurred.
                if (!$userhandler->validate_user()) {
                    $errors = $userhandler->get_friendly_errors();
                } else {
                    $user_info = $userhandler->insert_user();

                    $this->plugins->runHooks('admin_user_users_add_commit');

                    // Log admin action
                    $this->bblogger->log_admin_action($user_info['uid'], $user_info['username']);

                    $this->session->flash_message($this->lang->success_user_created, 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users?action=edit&uid=' . $user_info['uid']);
                }
            }

            // Fetch custom profile fields - only need required profile fields here
            $query = $this->db->simple_select('profilefields', '*', 'required=1', ['order_by' => 'disporder']);

            $profile_fields = [];
            while ($profile_field = $this->db->fetch_array($query)) {
                $profile_fields['required'][] = $profile_field;
            }

            $this->page->add_breadcrumb_item($this->lang->create_user);
            $html = $this->page->output_header($this->lang->create_user);

            $form = new Form($this->bb, $this->bb->admin_url . '/users?action=add', 'post');
            $html .= $form->getForm();

            $html .= $this->page->output_nav_tabs($sub_tabs, 'create_user');

            // If we have any error messages, show them
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $this->bb->input = array_merge($this->bb->input, ['usergroup' => 2]);
            }

            $form_container = new FormContainer($this, $this->lang->required_profile_info);
            $form_container->output_row($this->lang->username . ' <em>*</em>', '', $form->generate_text_box('username', $this->bb->getInput('username', '', true), ['id' => 'username']), 'username');
            $form_container->output_row($this->lang->password . ' <em>*</em>', '', $form->generate_password_box('password', $this->bb->getInput('password', '', true), ['id' => 'password', 'autocomplete' => 'off']), 'password');
            $form_container->output_row($this->lang->confirm_password . ' <em>*</em>', '', $form->generate_password_box('confirm_password', $this->bb->getInput('confirm_password', '', true), ['id' => 'confirm_new_password']), 'confirm_new_password');
            $form_container->output_row($this->lang->email_address . ' <em>*</em>', '', $form->generate_text_box('email', $this->bb->getInput('email', ''), ['id' => 'email']), 'email');

            $display_group_options[0] = $this->lang->use_primary_user_group;
            $options = [];
            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
                $display_group_options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
            }

            $form_container->output_row($this->lang->primary_user_group . ' <em>*</em>', '', $form->generate_select_box('usergroup', $options, $this->bb->input['usergroup'], ['id' => 'usergroup']), 'usergroup');
            $form_container->output_row($this->lang->additional_user_groups, $this->lang->additional_user_groups_desc, $form->generate_select_box('additionalgroups[]', $options, $this->bb->getInput('additionalgroups', ''), ['id' => 'additionalgroups', 'multiple' => true, 'size' => 5]), 'additionalgroups');
            $form_container->output_row($this->lang->display_user_group . ' <em>*</em>', '', $form->generate_select_box('displaygroup', $display_group_options, $this->bb->getInput('displaygroup', ''), ['id' => 'displaygroup']), 'displaygroup');

            // Output custom profile fields - required
            $this->output_custom_profile_fields($profile_fields['required'], $this->bb->getInput('profile_fields', ''), $form_container, $form);

            $html .= $form_container->end();
            $buttons[] = $form->generate_submit_button($this->lang->save_user);
            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/UsersAdd.html.twig');
        }

        if ($this->bb->input['action'] == 'edit') {
            $user = $this->user->get_user($this->bb->input['uid']);
            $errors = [];
            // Does the user not exist?
            if (!$user->uid) {
                $this->session->flash_message($this->lang->error_invalid_user, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            $this->plugins->runHooks('admin_user_users_edit');

            if ($this->bb->request_method == 'post') {
                if ($this->user->is_super_admin($this->bb->input['uid']) &&
                    $this->user->uid != $this->bb->input['uid'] &&
                    !$this->user->is_super_admin($this->user->uid)
                ) {
                    $this->session->flash_message($this->lang->error_no_perms_super_admin, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/users');
                }

                // Determine the usergroup stuff
                if (is_array($this->bb->input['additionalgroups'])) {
                    foreach ($this->bb->input['additionalgroups'] as $key => $gid) {
                        if ($gid == $this->bb->input['usergroup']) {
                            unset($this->bb->input['additionalgroups'][$key]);
                        }
                    }
                    $additionalgroups = implode(',', $this->bb->input['additionalgroups']);
                } else {
                    $additionalgroups = '';
                }

                $returndate = '';
                if (!empty($this->bb->input['away_day'])) {
                    $awaydate = TIME_NOW;
                    // If the user has indicated that they will return on a specific day, but not month or year, assume it is current month and year
                    if (!$this->bb->input['away_month']) {
                        $this->bb->input['away_month'] = $this->time->formatDate('n', $awaydate);
                    }
                    if (!$this->bb->input['away_year']) {
                        $this->bb->input['away_year'] = $this->time->formatDate('Y', $awaydate);
                    }

                    $return_month = (int)substr($this->bb->input['away_month'], 0, 2);
                    $return_day = (int)substr($this->bb->input['away_day'], 0, 2);
                    $return_year = min($this->bb->getInput('away_year', 0), 9999);

                    // Check if return date is after the away date.
                    $returntimestamp = gmmktime(0, 0, 0, $return_month, $return_day, $return_year);
                    $awaytimestamp = gmmktime(0, 0, 0, $this->bb->time->formatDate('n', $awaydate), $this->time->formatDate('j', $awaydate), $this->time->formatDate('Y', $awaydate));
                    if ($return_year < $this->time->formatDate('Y', $awaydate) || ($returntimestamp < $awaytimestamp && $return_year == $this->time->formatDate('Y', $awaydate))) {
                        $away_in_past = true;
                    }

                    $returndate = "{$return_day}-{$return_month}-{$return_year}";
                }

                // Set up user handler.
                $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

                // Set the data for the new user.
                $updated_user = [
                    'uid' => $this->bb->input['uid'],
                    'username' => $this->bb->input['username'],
                    'email' => $this->bb->input['email'],
                    'email2' => $this->bb->input['email'],
                    'usergroup' => $this->bb->input['usergroup'],
                    'additionalgroups' => $additionalgroups,
                    'displaygroup' => $this->bb->input['displaygroup'],
                    'postnum' => $this->bb->input['postnum'],
                    'threadnum' => $this->bb->input['threadnum'],
                    'usertitle' => $this->bb->input['usertitle'],
                    'timezone' => $this->bb->input['timezone'],
                    'language' => $this->bb->input['language'],
                    'profile_fields' => $this->bb->input['profile_fields'],
                    'profile_fields_editable' => true,
                    'website' => $this->bb->input['website'],
                    'icq' => $this->bb->input['icq'],
                    'aim' => $this->bb->input['aim'],
                    'yahoo' => $this->bb->input['yahoo'],
                    'skype' => $this->bb->input['skype'],
                    'google' => $this->bb->input['google'],
                    'birthday' => [
                        'day' => $this->bb->input['bday1'],
                        'month' => $this->bb->input['bday2'],
                        'year' => $this->bb->input['bday3']
                    ],
                    'style' => $this->bb->input['style'],
                    'signature' => $this->bb->input['signature'],
                    'dateformat' => $this->bb->getInput('dateformat', 0),
                    'timeformat' => $this->bb->getInput('timeformat', 0),
                    'usernotes' => $this->bb->input['usernotes'],
                    'away' => [
                        'away' => $this->bb->input['away'],
                        'date' => TIME_NOW,
                        'returndate' => $returndate,
                        'awayreason' => $this->bb->input['awayreason']
                    ]
                ];

                if ($user->usergroup == 5 && $this->bb->input['usergroup'] != 5) {
                    if ($user->coppauser == 1) {
                        $updated_user['coppa_user'] = 0;
                    }
                }
                if ($this->bb->input['new_password']) {
                    $updated_user['password'] = $this->bb->input['new_password'];
                    $updated_user['password2'] = $this->bb->input['confirm_new_password'];
                }

                $updated_user['options'] = [
                    'allownotices' => $this->bb->input['allownotices'],
                    'hideemail' => $this->bb->input['hideemail'],
                    'subscriptionmethod' => $this->bb->input['subscriptionmethod'],
                    'invisible' => $this->bb->input['invisible'],
                    'dstcorrection' => $this->bb->input['dstcorrection'],
                    'threadmode' => $this->bb->input['threadmode'],
                    'classicpostbit' => $this->bb->input['classicpostbit'],
                    'showimages' => $this->bb->input['showimages'],
                    'showvideos' => $this->bb->input['showvideos'],
                    'showsigs' => $this->bb->input['showsigs'],
                    'showavatars' => $this->bb->input['showavatars'],
                    'showquickreply' => $this->bb->input['showquickreply'],
                    'receivepms' => $this->bb->input['receivepms'],
                    'receivefrombuddy' => $this->bb->input['receivefrombuddy'],
                    'pmnotice' => $this->bb->input['pmnotice'],
                    'daysprune' => $this->bb->input['daysprune'],
                    'showcodebuttons' => $this->bb->input['showcodebuttons'],
                    'sourceeditor' => $this->bb->input['sourceeditor'],
                    'pmnotify' => $this->bb->input['pmnotify'],
                    'buddyrequestspm' => $this->bb->input['buddyrequestspm'],
                    'buddyrequestsauto' => $this->bb->input['buddyrequestsauto'],
                    'showredirect' => $this->bb->input['showredirect']
                ];

                if ($this->bb->settings['usertppoptions']) {
                    $updated_user['options']['tpp'] = $this->bb->getInput('tpp', 0);
                }

                if ($this->bb->settings['userpppoptions']) {
                    $updated_user['options']['ppp'] = $this->bb->getInput('ppp', 0);
                }

                // Set the data of the user in the datahandler.
                $userhandler->set_data($updated_user);
                $errors = '';

                // Validate the user and get any errors that might have occurred.
                if (!$userhandler->validate_user()) {
                    $errors = $userhandler->get_friendly_errors();
                } else {
                    // Are we removing an avatar from this user?
                    if ($this->bb->input['remove_avatar']) {
                        $extra_user_updates = [
                            'avatar' => '',
                            'avatardimensions' => '',
                            'avatartype' => ''
                        ];
                        $this->upload->remove_avatars($user->uid);
                    }

                    // Are we uploading a new avatar?
                    if ($_FILES['avatar_upload']['name']) {
                        $avatar = $this->upload->upload_avatar($_FILES['avatar_upload'], $user->uid);
                        if ($avatar['error']) {
                            $errors = [$avatar['error']];
                        } else {
                            if ($avatar['width'] > 0 && $avatar['height'] > 0) {
                                $avatar_dimensions = $avatar['width'] . '|' . $avatar['height'];
                            }
                            $extra_user_updates = [
                                'avatar' => $avatar['avatar'] . '?dateline=' . TIME_NOW,
                                'avatardimensions' => $avatar_dimensions,
                                'avatartype' => 'upload'
                            ];
                        }
                    } // Are we setting a new avatar from a URL?
                    elseif ($this->bb->input['avatar_url'] && $this->bb->input['avatar_url'] != $user->avatar) {
                        if (filter_var($this->bb->input['avatar_url'], FILTER_VALIDATE_EMAIL) !== false) {
                            // Gravatar
                            $email = md5(strtolower(trim($this->bb->input['avatar_url'])));

                            $s = '';
                            if (!$this->bb->settings['maxavatardims']) {
                                $this->bb->settings['maxavatardims'] = '100x100'; // Hard limit of 100 if there are no limits
                            }

                            // Because Gravatars are square, hijack the width
                            list($maxwidth, $maxheight) = explode('x', my_strtolower($this->bb->settings['maxavatardims']));

                            $s = "?s={$maxwidth}";
                            $maxheight = (int)$maxwidth;

                            $extra_user_updates = [
                                'avatar' => "https://www.gravatar.com/avatar/{$email}{$s}",
                                'avatardimensions' => "{$maxheight}|{$maxheight}",
                                'avatartype' => 'gravatar'
                            ];
                        } else {
                            $this->bb->input['avatar_url'] = preg_replace('#script:#i', '', $this->bb->input['avatar_url']);
                            $ext = get_extension($this->bb->input['avatar_url']);

                            // Copy the avatar to the local server (work around remote URL access disabled for getimagesize)
                            $file = fetch_remote_file($this->bb->input['avatar_url']);
                            if (!$file) {
                                $avatar_error = $this->lang->error_invalidavatarurl;
                            } else {
                                $tmp_name = '../' . $this->bb->settings['avataruploadpath'] . '/remote_' . md5(random_str());
                                $fp = @fopen($tmp_name, 'wb');
                                if (!$fp) {
                                    $avatar_error = $this->lang->error_invalidavatarurl;
                                } else {
                                    fwrite($fp, $file);
                                    fclose($fp);
                                    list($width, $height, $type) = @getimagesize($tmp_name);
                                    @unlink($tmp_name);
                                    echo $type;
                                    if (!$type) {
                                        $avatar_error = $this->lang->error_invalidavatarurl;
                                    }
                                }
                            }

                            if (empty($avatar_error)) {
                                if ($width && $height && $this->bb->settings['maxavatardims'] != '') {
                                    list($maxwidth, $maxheight) = explode('x', my_strtolower($this->bb->settings['maxavatardims']));
                                    if (($maxwidth && $width > $maxwidth) || ($maxheight && $height > $maxheight)) {
                                        $this->lang->error_avatartoobig = $this->lang->sprintf($this->lang->error_avatartoobig, $maxwidth, $maxheight);
                                        $avatar_error = $this->lang->error_avatartoobig;
                                    }
                                }
                            }

                            if (empty($avatar_error)) {
                                if ($width > 0 && $height > 0) {
                                    $avatar_dimensions = (int)$width . '|' . (int)$height;
                                }
                                $extra_user_updates = [
                                    'avatar' => $this->db->escape_string($this->bb->input['avatar_url'] . '?dateline=' . TIME_NOW),
                                    'avatardimensions' => $avatar_dimensions,
                                    'avatartype' => 'remote'
                                ];
                                $this->upload->remove_avatars($user->uid);
                            } else {
                                $errors = [$avatar_error];
                            }
                        }
                    }

                    // Moderator 'Options' (suspend signature, suspend/moderate posting)
                    $moderator_options = [
                        1 => [
                            'action' => 'suspendsignature', // The moderator action we're performing
                            'period' => 'action_period', // The time period we've selected from the dropdown box
                            'time' => 'action_time', // The time we've entered
                            'update_field' => 'suspendsignature', // The field in the database to update if true
                            'update_length' => 'suspendsigtime' // The length of suspension field in the database
                        ],
                        2 => [
                            'action' => 'moderateposting',
                            'period' => 'modpost_period',
                            'time' => 'modpost_time',
                            'update_field' => 'moderateposts',
                            'update_length' => 'moderationtime'
                        ],
                        3 => [
                            'action' => 'suspendposting',
                            'period' => 'suspost_period',
                            'time' => 'suspost_time',
                            'update_field' => 'suspendposting',
                            'update_length' => 'suspensiontime'
                        ]
                    ];

                    foreach ($moderator_options as $option) {
                        if (!$this->bb->input[$option['action']]) {
                            if ($user->{$option['update_field']} == 1) {
                                // We're revoking the suspension
                                $extra_user_updates[$option['update_field']] = 0;
                                $extra_user_updates[$option['update_length']] = 0;
                            }

                            // Skip this option if we haven't selected it
                            continue;
                        }

                        if ($this->bb->input[$option['action']]) {
                            if ((int)$this->bb->input[$option['time']] == 0 &&
                                $this->bb->input[$option['period']] != 'never' &&
                                $user->{$option['update_field']} != 1) {
                                // User has selected a type of ban, but not entered a valid time frame
                                $string = $option['action'] . '_error';
                                $errors[] = $this->lang->$string;
                            }

                            if (!is_array($errors)) {
                                $suspend_length = (new WarnHelper($this->db))->fetch_time_length((int)$this->bb->input[$option['time']], $this->bb->input[$option['period']]);

                                if ($user->{$option['update_field']} == 1 && ($this->bb->input[$option['time']] || $this->bb->input[$option['period']] == 'never')) {
                                    // We already have a suspension, but entered a new time
                                    if ($suspend_length == '-1') {
                                        // Permanent ban on action
                                        $extra_user_updates[$option['update_length']] = 0;
                                    } elseif ($suspend_length && $suspend_length != '-1') {
                                        // Temporary ban on action
                                        $extra_user_updates[$option['update_length']] = TIME_NOW + $suspend_length;
                                    }
                                } elseif (!$user->{$option['update_field']}) {
                                    // New suspension for this user... bad user!
                                    $extra_user_updates[$option['update_field']] = 1;
                                    if ($suspend_length == '-1') {
                                        $extra_user_updates[$option['update_length']] = 0;
                                    } else {
                                        $extra_user_updates[$option['update_length']] = TIME_NOW + $suspend_length;
                                    }
                                }
                            }
                        }
                    }

                    if ($extra_user_updates['moderateposts'] && $extra_user_updates['suspendposting']) {
                        $errors[] = $this->lang->suspendmoderate_error;
                    }

                    if (isset($away_in_past)) {
                        $errors[] = $this->lang->error_acp_return_date_past;
                    }

                    if (!$errors) {
                        $user_info = $userhandler->update_user();

                        $this->plugins->runHooks('admin_user_users_edit_commit_start');

                        $this->db->update_query('users', $extra_user_updates, "uid='{$user->uid}'");

                        // if we're updating the user's signature preferences, do so now
                        if ($this->bb->input['update_posts'] == 'enable' || $this->bb->input['update_posts'] == 'disable') {
                            $update_signature = [
                                'includesig' => ($this->bb->input['update_posts'] == 'enable' ? 1 : 0)
                            ];
                            $this->db->update_query('posts', $update_signature, "uid='{$user->uid}'");
                        }

                        $this->plugins->runHooks('admin_user_users_edit_commit');

                        if ($user->usergroup == 5 && $this->bb->input['usergroup'] != 5) {
                            $this->cache->update_awaitingactivation();
                        }

                        // Log admin action
                        $this->bblogger->log_admin_action($user->uid, $this->bb->input['username']);

                        $this->session->flash_message($this->lang->success_user_updated, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/users');
                    }
                }
            }

            if (!$errors) {
                $user->usertitle = htmlspecialchars_decode($user->usertitle);

                $options = [
                    'bday1', 'bday2', 'bday3',
                    'new_password', 'confirm_new_password',
                    'action_time', 'action_period',
                    'modpost_period', 'moderateposting', 'modpost_time', 'suspost_period', 'suspost_time'
                ];

                foreach ($options as $option) {
                    if (!isset($input_user[$option])) {
                        $this->bb->input[$option] = '';
                    }
                }

                // We need to fetch this users profile field values
                $query = $this->db->simple_select('userfields', '*', "ufid='{$user->uid}'");
                $this->bb->input['profile_fields'] = $this->db->fetch_array($query);
            }

            if ($this->bb->input['bday1'] || $this->bb->input['bday2'] || $this->bb->input['bday3']) {
                $this->bb->input['bday'][0] = $this->bb->input['bday1'];
                $this->bb->input['bday'][1] = $this->bb->input['bday2'];
                $this->bb->input['bday'][2] = $this->bb->getInput('bday3', 0);
            } else {
                $this->bb->input['bday'] = [0, 0, ''];

                if ($user->birthday) {
                    $this->bb->input['bday'] = explode('-', $user->birthday);
                }
            }

            if (isset($this->bb->input['away_day']) || isset($this->bb->input['away_month']) || isset($this->bb->input['away_year'])) {
                $this->bb->input['away_year'] = $this->bb->getInput('away_year', 0);
            } else {
                $this->bb->input['away_day'] = 0;
                $this->bb->input['away_month'] = 0;
                $this->bb->input['away_year'] = '';

                if ($user->returndate) {
                    list($this->bb->input['away_day'], $this->bb->input['away_month'], $this->bb->input['away_year']) = explode('-', $user->returndate);
                }
            }

            // Fetch custom profile fields
            $query = $this->db->simple_select('profilefields', '*', '', ['order_by' => 'disporder']);

            $profile_fields = [];
            while ($profile_field = $this->db->fetch_array($query)) {
                if ($profile_field['required'] == 1) {
                    $profile_fields['required'][] = $profile_field;
                } else {
                    $profile_fields['optional'][] = $profile_field;
                }
            }

            $this->page->add_breadcrumb_item($this->lang->edit_user . ': ' . htmlspecialchars_uni($user->username));

            $this->page->extra_header .= <<<EOF

	<link rel="stylesheet" href="{$this->bb->asset_url}/jscripts/sceditor/editor_themes/mybb.css" type="text/css" media="all" />
	<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/sceditor/jquery.sceditor.bbcode.min.js?ver=1805"></script>
	<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/bbcodes_sceditor.js?ver=1804"></script>
	<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/sceditor/editor_plugins/undo.js?ver=1805"></script>
EOF;
            $html = $this->page->output_header($this->lang->edit_user);

            $sub_tabs['edit_user'] = [
                'title' => $this->lang->edit_user,
                'description' => $this->lang->edit_user_desc
            ];

            $form = new Form($this->bb, $this->bb->admin_url . '/users?action=edit&uid=' . $user->uid, 'post', '', 1);
            $html .= $form->getForm();
            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_user');

            // If we have any error messages, show them
            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            // Is this user a COPPA user? We show a warning & activate link
            if ($user->coppauser) {
                $html .= $this->lang->sprintf($this->lang->warning_coppa_user, $user->uid);
            }

            $tabs = [
                'overview' => $this->lang->overview,
                'profile' => $this->lang->profile,
                'settings' => $this->lang->account_settings,
                'signature' => $this->lang->signature,
                'avatar' => $this->lang->avatar,
                'modoptions' => $this->lang->mod_options
            ];
            $tabs = $this->plugins->runHooks('admin_user_users_edit_graph_tabs', $tabs);
            $html .= $this->page->output_tab_control($tabs);

            //
            // OVERVIEW
            //
            $html .= "<div id=\"tab_overview\">\n";
            $table = new Table;
            $table->construct_header($this->lang->avatar, ['class' => 'align_center']);
            $table->construct_header($this->lang->general_account_stats, ['colspan' => '2', 'class' => 'align_center']);

            // Avatar
            $avatar_dimensions = explode('|', $user->avatardimensions);
            if ($user->avatar) {
                if ($user->avatardimensions) {
                    $img = new \RunBB\Core\Image();
                    list($width, $height) = explode('|', $user->avatardimensions);
                    $scaled_dimensions = $img->scale_image($width, $height, 120, 120);
                } else {
                    $scaled_dimensions = [
                        'width' => 120,
                        'height' => 120
                    ];
                }
                if (my_substr($user->avatar, 0, 7) !== 'http://' && my_substr($user->avatar, 0, 8) !== 'https://') {
                    $user->avatar = $this->bb->asset_url . '/' . $user->avatar;
                }
            } else {
                $user->avatar = $this->bb->asset_url . '/' . $this->bb->settings['useravatar'];
                $scaled_dimensions = [
                    'width' => 120,
                    'height' => 120
                ];
            }
            $avatar_top = ceil((126 - $scaled_dimensions['height']) / 2);
            if ($user->lastactive) {
                $last_active = $this->time->formatDate('relative', $user->lastactive);
            } else {
                $last_active = $this->lang->never;
            }
            $reg_date = $this->time->formatDate('relative', $user->regdate);
            if ($user->dst == 1) {
                $timezone = $user->timezone + 1;
            } else {
                $timezone = $user->timezone;
            }
            $local_date = gmdate($this->bb->settings['dateformat'], TIME_NOW + ($timezone * 3600));
            $local_time = gmdate($this->bb->settings['timeformat'], TIME_NOW + ($timezone * 3600));

            $localtime = $this->lang->sprintf($this->lang->local_time_format, $local_date, $local_time);
            $days_registered = (TIME_NOW - $user->regdate) / (24 * 3600);
            $posts_per_day = 0;
            if ($days_registered > 0) {
                $posts_per_day = round($user->postnum / $days_registered, 2);
                if ($posts_per_day > $user->postnum) {
                    $posts_per_day = $user->postnum;
                }
            }
            $posts_per_day = $this->parser->formatNumber($posts_per_day);

            $stats = $this->cache->read('stats');
            $posts = $stats['numposts'];
            if ($posts == 0) {
                $percent_posts = '0';
            } else {
                $percent_posts = round($user->postnum * 100 / $posts, 2);
            }

            $user_permissions = $this->user->user_permissions($user->uid);

            // Fetch the reputation for this user
            if ($user_permissions['usereputationsystem'] == 1 && $this->bb->settings['enablereputation'] == 1) {
                $reputation = $this->user->get_reputation($user->reputation);
            } else {
                $reputation = '-';
            }

            $warning_level = '';
            if ($this->bb->settings['enablewarningsystem'] != 0 && $user_permissions['canreceivewarnings'] != 0) {
                if ($this->bb->settings['maxwarningpoints'] < 1) {
                    $this->bb->settings['maxwarningpoints'] = 10;
                }

                $warning_level = round($user->warningpoints / $this->bb->settings['maxwarningpoints'] * 100);
                if ($warning_level > 100) {
                    $warning_level = 100;
                }
                $warning_level = get_colored_warning_level($warning_level);
            }

            $age = $this->lang->na;
            if ($user->birthday) {
                $age = $this->user->get_age($user->birthday);
            }

            $postnum = $this->parser->formatNumber($user->postnum);

            $table->construct_cell("<div style=\"width: 126px; height: 126px;\" class=\"user_avatar\"><img src=\"" . htmlspecialchars_uni($user->avatar) . "\" style=\"margin-top: {$avatar_top}px\" width=\"{$scaled_dimensions['width']}\" height=\"{$scaled_dimensions['height']}\" alt=\"\" /></div>", ['rowspan' => 6, 'width' => 1]);
            $table->construct_cell("<strong>{$this->lang->email_address}:</strong> <a href=\"mailto:" . htmlspecialchars_uni($user->email) . "\">" . htmlspecialchars_uni($user->email) . "</a>");
            $table->construct_cell("<strong>{$this->lang->last_active}:</strong> {$last_active}");
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->registration_date}:</strong> {$reg_date}");
            $table->construct_cell("<strong>{$this->lang->local_time}:</strong> {$localtime}");
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->posts}:</strong> {$postnum}");
            $table->construct_cell("<strong>{$this->lang->age}:</strong> {$age}");
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->posts_per_day}:</strong> {$posts_per_day}");
            $table->construct_cell("<strong>{$this->lang->reputation}:</strong> {$reputation}");
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->percent_of_total_posts}:</strong> {$percent_posts}");
            $table->construct_cell("<strong>{$this->lang->warning_level}:</strong> {$warning_level}");
            $table->construct_row();
            $table->construct_cell("<strong>{$this->lang->registration_ip}:</strong> " . $user->regip);
            $table->construct_cell("<strong>{$this->lang->last_known_ip}:</strong> " . $user->lastip);
            $table->construct_row();

            $html .= $table->output("{$this->lang->user_overview}: {$user->username}");
            $html .= "</div>\n";

            //
            // PROFILE
            //
            $html .= "<div id=\"tab_profile\">\n";

            $form_container = new FormContainer($this, $this->lang->required_profile_info . ": {$user->username}");
            $form_container->output_row($this->lang->username . ' <em>*</em>', '', $form->generate_text_box('username', $this->bb->getInput('username', $user->username, true), ['id' => 'username']), 'username');
            $form_container->output_row($this->lang->new_password, $this->lang->new_password_desc, $form->generate_password_box('new_password', $this->bb->input['new_password'], ['id' => 'new_password', 'autocomplete' => 'off']), 'new_password');
            $form_container->output_row($this->lang->confirm_new_password, $this->lang->new_password_desc, $form->generate_password_box('confirm_new_password', $this->bb->input['confirm_new_password'], ['id' => 'confirm_new_password']), 'confirm_new_password');
            $form_container->output_row($this->lang->email_address . ' <em>*</em>', '', $form->generate_text_box('email', $this->bb->getInput('email', $user->email), ['id' => 'email']), 'email');

            $display_group_options[0] = $this->lang->use_primary_user_group;
            $options = [];
            $query = $this->db->simple_select('usergroups', 'gid, title', "gid != '1'", ['order_by' => 'title']);
            while ($usergroup = $this->db->fetch_array($query)) {
                $options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
                $display_group_options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
            }

            $this->bb->getInput('additionalgroups', $user->additionalgroups);
            if (isset($this->bb->input['additionalgroups']) && !is_array($this->bb->input['additionalgroups'])) {
                $this->bb->input['additionalgroups'] = explode(',', $this->bb->input['additionalgroups']);
            }

            $form_container->output_row($this->lang->primary_user_group . ' <em>*</em>', '', $form->generate_select_box('usergroup', $options, $this->bb->getInput('usergroup', $user->usergroup), ['id' => 'usergroup']), 'usergroup');
            $form_container->output_row($this->lang->additional_user_groups, $this->lang->additional_user_groups_desc, $form->generate_select_box('additionalgroups[]', $options, $this->bb->getInput('additionalgroups', $user->additionalgroups), ['id' => 'additionalgroups', 'multiple' => true, 'size' => 5]), 'additionalgroups');
            $form_container->output_row($this->lang->display_user_group . ' <em>*</em>', '', $form->generate_select_box('displaygroup', $display_group_options, $this->bb->getInput('displaygroup', $user->displaygroup), ['id' => 'displaygroup']), 'displaygroup');
            $form_container->output_row($this->lang->post_count . ' <em>*</em>', '', $form->generate_numeric_field('postnum', $this->bb->getInput('postnum', $user->postnum), ['id' => 'postnum', 'min' => 0]), 'postnum');
            $form_container->output_row($this->lang->thread_count . ' <em>*</em>', '', $form->generate_numeric_field('threadnum', $this->bb->getInput('threadnum', $user->threadnum), ['id' => 'threadnum', 'min' => 0]), 'threadnum');

            // Output custom profile fields - required
            if (!isset($profile_fields['required'])) {
                $profile_fields['required'] = [];
            }
            $this->output_custom_profile_fields($profile_fields['required'], $this->bb->input['profile_fields'], $form_container, $form);

            $html .= $form_container->end();

            $form_container = new FormContainer($this, $this->lang->optional_profile_info . ": {$user->username}");
            $form_container->output_row($this->lang->custom_user_title, $this->lang->custom_user_title_desc, $form->generate_text_box('usertitle', $this->bb->getInput('usertitle', $user->usertitle, true), ['id' => 'usertitle']), 'usertitle');
            $form_container->output_row($this->lang->website, '', $form->generate_text_box('website', $this->bb->getInput('website', $user->website), ['id' => 'website']), 'website');
            $form_container->output_row($this->lang->icq_number, '', $form->generate_numeric_field('icq', $this->bb->getInput('icq', $user->icq), ['id' => 'icq', 'min' => 0]), 'icq');
            $form_container->output_row($this->lang->aim_handle, '', $form->generate_text_box('aim', $this->bb->getInput('aim', $user->aim), ['id' => 'aim']), 'aim');
            $form_container->output_row($this->lang->yahoo_messanger_handle, '', $form->generate_text_box('yahoo', $this->bb->getInput('yahoo', $user->yahoo), ['id' => 'yahoo']), 'yahoo');
            $form_container->output_row($this->lang->skype_handle, '', $form->generate_text_box('skype', $this->bb->getInput('skype', $user->skype), ['id' => 'skype']), 'skype');
            $form_container->output_row($this->lang->google_handle, '', $form->generate_text_box('google', $this->bb->getInput('google', $user->google), ['id' => 'google']), 'google');

            // Birthday
            $birthday_days = [0 => ''];
            for ($i = 1; $i <= 31; $i++) {
                $birthday_days[$i] = $i;
            }

            $birthday_months = [
                0 => '',
                1 => $this->lang->january,
                2 => $this->lang->february,
                3 => $this->lang->march,
                4 => $this->lang->april,
                5 => $this->lang->may,
                6 => $this->lang->june,
                7 => $this->lang->july,
                8 => $this->lang->august,
                9 => $this->lang->september,
                10 => $this->lang->october,
                11 => $this->lang->november,
                12 => $this->lang->december
            ];

            $birthday_row = $form->generate_select_box('bday1', $birthday_days, $this->bb->input['bday'][0], ['id' => 'bday_day']);
            $birthday_row .= ' ' . $form->generate_select_box('bday2', $birthday_months, $this->bb->input['bday'][1], ['id' => 'bday_month']);
            $birthday_row .= ' ' . $form->generate_numeric_field('bday3', $this->bb->input['bday'][2], ['id' => 'bday_year', 'style' => 'width: 4em;', 'min' => 0]);

            $form_container->output_row($this->lang->birthday, '', $birthday_row, 'birthday');

            // Output custom profile fields - optional
            $this->output_custom_profile_fields($profile_fields['optional'], $this->bb->input['profile_fields'], $form_container, $form);

            $html .= $form_container->end();


            if ($this->bb->settings['allowaway'] != 0) {
                $form_container = new FormContainer($this, $this->lang->away_information . ": {$user->username}");
                $awaycheck = [false, true];
                $this->bb->input['away'] = isset($this->bb->input['away']) ? $this->bb->input['away'] : $user->away;
                if ($this->bb->input['away'] == 1) {
                    $awaycheck = [true, false];
                }
                $form_container->output_row($this->lang->away_status, $this->lang->away_status_desc, $form->generate_radio_button('away', 1, $this->lang->im_away, ['id' => 'away', "checked" => $awaycheck[0]]) . " " . $form->generate_radio_button('away', 0, $this->lang->im_here, ['id' => 'away2', "checked" => $awaycheck[1]]), 'away');
                $form_container->output_row($this->lang->away_reason, $this->lang->away_reason_desc, $form->generate_text_box('awayreason', $this->bb->getInput('awayreason', $user->awayreason, true), ['id' => 'awayreason']), 'awayreason');

                //Return date (we can use the arrays from birthday)
                $return_row = $form->generate_select_box('away_day', $birthday_days, $this->bb->input['away_day'], ['id' => 'away_day']);
                $return_row .= ' ' . $form->generate_select_box('away_month', $birthday_months, $this->bb->input['away_month'], ['id' => 'away_month']);
                $return_row .= ' ' . $form->generate_numeric_field('away_year', $this->bb->input['away_year'], ['id' => 'away_year', 'style' => 'width: 4em;', 'min' => 0]);

                $form_container->output_row($this->lang->return_date, $this->lang->return_date_desc, $return_row, 'away_date');

                $html .= $form_container->end();
            }

            $html .= "</div>\n";

            //
            // ACCOUNT SETTINGS
            //

            // Plugin hook note - we should add hooks in above each output_row for the below so users can add their own options to each group :>

            $html .= "<div id=\"tab_settings\">\n";
            $form_container = new FormContainer($this, $this->lang->account_settings . ": {$user->username}");
            $login_options = [
                $form->generate_check_box('invisible', 1, $this->lang->hide_from_whos_online, ['checked' => $this->bb->getInput('invisible', $user->invisible)]),
            ];
            $form_container->output_row($this->lang->login_cookies_privacy, '', "<div class=\"user_settings_bit\">" . implode("</div><div class=\"user_settings_bit\">", $login_options) . "</div>");

            $this->bb->input['pmnotice'] = isset($this->bb->input['pmnotice']) ? $this->bb->input['pmnotice'] : $user->pmnotice;
            if ($this->bb->input['pmnotice'] > 1) {
                $this->bb->input['pmnotice'] = 1;
            }

            $messaging_options = [
                $form->generate_check_box('allownotices', 1, $this->lang->recieve_admin_emails, ['checked' => $this->bb->getInput('allownotices', $user->allownotices)]),
                $form->generate_check_box('hideemail', 1, $this->lang->hide_email_from_others, ['checked' => $this->bb->getInput('hideemail', $user->hideemail)]),
                $form->generate_check_box('receivepms', 1, $this->lang->recieve_pms_from_others, ['checked' => $this->bb->getInput('receivepms', $user->receivepms)]),
                $form->generate_check_box('receivefrombuddy', 1, $this->lang->recieve_pms_from_buddy, ['checked' => $this->bb->getInput('receivefrombuddy', $user->receivefrombuddy)]),
                $form->generate_check_box('pmnotice', 1, $this->lang->alert_new_pms, ['checked' => $this->bb->getInput('pmnotice', $user->pmnotice)]),
                $form->generate_check_box('pmnotify', 1, $this->lang->email_notify_new_pms, ['checked' => $this->bb->getInput('pmnotify', $user->pmnotify)]),
                $form->generate_check_box('buddyrequestspm', 1, $this->lang->buddy_requests_pm, ['checked' => $this->bb->getInput('buddyrequestspm', $user->buddyrequestspm)]),
                $form->generate_check_box('buddyrequestsauto', 1, $this->lang->buddy_requests_auto, ['checked' => $this->bb->getInput('buddyrequestsauto', $user->buddyrequestsauto)]),
                "<label for=\"subscriptionmethod\">{$this->lang->default_thread_subscription_mode}:</label><br />" . $form->generate_select_box("subscriptionmethod", [$this->lang->do_not_subscribe, $this->lang->no_email_notification, $this->lang->instant_email_notification], $this->bb->getInput('subscriptionmethod', $user->subscriptionmethod), ['id' => 'subscriptionmethod'])
            ];
            $form_container->output_row($this->lang->messaging_and_notification, '', "<div class=\"user_settings_bit\">" . implode("</div><div class=\"user_settings_bit\">", $messaging_options) . "</div>");

            $date_format_options = [$this->lang->use_default];
            foreach ($this->bb->date_formats as $key => $format) {
                $date_format_options[$key] = $this->time->formatDate($format, TIME_NOW, '', 0);
            }

            $time_format_options = [$this->lang->use_default];
            foreach ($this->bb->time_formats as $key => $format) {
                $time_format_options[$key] = $this->time->formatDate($format, TIME_NOW, '', 0);
            }
            $date_options = [
                "<label for=\"dateformat\">{$this->lang->date_format}:</label><br />" . $form->generate_select_box("dateformat", $date_format_options, $this->bb->getInput('dateformat', $user->dateformat), ['id' => 'dateformat']),
                "<label for=\"dateformat\">{$this->lang->time_format}:</label><br />" . $form->generate_select_box("timeformat", $time_format_options, $this->bb->getInput('timeformat', $user->timeformat), ['id' => 'timeformat']),
                "<label for=\"timezone\">{$this->lang->time_zone}:</label><br />" . $this->time->buildTimezoneSelect('timezone', $this->bb->getInput('timezone', $user->timezone)),
                "<label for=\"dstcorrection\">{$this->lang->daylight_savings_time_correction}:</label><br />" . $form->generate_select_box("dstcorrection", [2 => $this->lang->automatically_detect, 1 => $this->lang->always_use_dst_correction, 0 => $this->lang->never_use_dst_correction], $this->bb->getInput('dstcorrection', $user->dstcorrection), ['id' => 'dstcorrection'])
            ];
            $form_container->output_row($this->lang->date_and_time_options, '', "<div class=\"user_settings_bit\">" . implode("</div><div class=\"user_settings_bit\">", $date_options) . "</div>");


            $tpp_options = [$this->lang->use_default];
            if ($this->bb->settings['usertppoptions']) {
                $explodedtpp = explode(',', $this->bb->settings['usertppoptions']);
                if (is_array($explodedtpp)) {
                    foreach ($explodedtpp as $tpp) {
                        if ($tpp <= 0) {
                            continue;
                        }
                        $tpp_options[$tpp] = $tpp;
                    }
                }
            }

            $thread_age_options = [
                0 => $this->lang->use_default,
                1 => $this->lang->show_threads_last_day,
                5 => $this->lang->show_threads_last_5_days,
                10 => $this->lang->show_threads_last_10_days,
                20 => $this->lang->show_threads_last_20_days,
                50 => $this->lang->show_threads_last_50_days,
                75 => $this->lang->show_threads_last_75_days,
                100 => $this->lang->show_threads_last_100_days,
                365 => $this->lang->show_threads_last_year,
                9999 => $this->lang->show_all_threads
            ];

            $forum_options = [
                "<label for=\"tpp\">{$this->lang->threads_per_page}:</label><br />" . $form->generate_select_box("tpp", $tpp_options, $this->bb->getInput('tpp', $user->tpp), ['id' => 'tpp']),
                "<label for=\"daysprune\">{$this->lang->default_thread_age_view}:</label><br />" . $form->generate_select_box("daysprune", $thread_age_options, $this->bb->getInput('daysprune', $user->daysprune), ['id' => 'daysprune'])
            ];
            $form_container->output_row($this->lang->forum_display_options, '', "<div class=\"user_settings_bit\">" . implode("</div><div class=\"user_settings_bit\">", $forum_options) . "</div>");

            $ppp_options = [$this->lang->use_default];
            if ($this->bb->settings['userpppoptions']) {
                $explodedppp = explode(',', $this->bb->settings['userpppoptions']);
                if (is_array($explodedppp)) {
                    foreach ($explodedppp as $ppp) {
                        if ($ppp <= 0) {
                            continue;
                        }
                        $ppp_options[$ppp] = $ppp;
                    }
                }
            }

            $thread_options = [
                $form->generate_check_box('classicpostbit', 1, $this->lang->show_classic_postbit, ['checked' => $this->bb->getInput('classicpostbit', $user->classicpostbit)]),
                $form->generate_check_box('showimages', 1, $this->lang->display_images, ['checked' => $this->bb->getInput('showimages', $user->showimages)]),
                $form->generate_check_box('showvideos', 1, $this->lang->display_videos, ['checked' => $this->bb->getInput('showvideos', $user->showvideos)]),
                $form->generate_check_box('showsigs', 1, $this->lang->display_users_sigs, ['checked' => $this->bb->getInput('showsigs', $user->showsigs)]),
                $form->generate_check_box('showavatars', 1, $this->lang->display_users_avatars, ['checked' => $this->bb->getInput('showavatars', $user->showavatars)]),
                $form->generate_check_box('showquickreply', 1, $this->lang->show_quick_reply, ['checked' => $this->bb->getInput('showquickreply', $user->showquickreply)]),
                "<label for=\"ppp\">{$this->lang->posts_per_page}:</label><br />" . $form->generate_select_box('ppp', $ppp_options, $this->bb->getInput('ppp', $user->ppp), ['id' => 'ppp']),
                "<label for=\"threadmode\">{$this->lang->default_thread_view_mode}:</label><br />" . $form->generate_select_box('threadmode', ['' => $this->lang->use_default, 'linear' => $this->lang->linear_mode, 'threaded' => $this->lang->threaded_mode], $this->bb->getInput('threadmode', $user->threadmode), ['id' => 'threadmode'])
            ];
            $form_container->output_row($this->lang->thread_view_options, '', "<div class=\"user_settings_bit\">" . implode("</div><div class=\"user_settings_bit\">", $thread_options) . "</div>");

            $languages = array_merge(['' => $this->lang->use_default], $this->lang->get_languages());

            $other_options = [
                $form->generate_check_box('showredirect', 1, $this->lang->show_redirect, ['checked' => $this->bb->getInput('showredirect', $user->showredirect)]),
                $form->generate_check_box('showcodebuttons', '1', $this->lang->show_code_buttons, ['checked' => $this->bb->getInput('showcodebuttons', $user->showcodebuttons)]),
                $form->generate_check_box('sourceeditor', '1', $this->lang->source_editor, ['checked' => $this->bb->getInput('sourceeditor', $user->sourceeditor)]),
                "<label for=\"style\">{$this->lang->theme}:</label><br />" . $this->themes->build_theme_select('style', $this->bb->getInput('style', $user->style), 0, '', true, false, true),
                "<label for=\"language\">{$this->lang->board_language}:</label><br />" . $form->generate_select_box("language", $languages, $this->bb->getInput('language', $user->language), ['id' => 'language'])
            ];
            $form_container->output_row($this->lang->other_options, '', '<div class="user_settings_bit">' . implode('</div><div class="user_settings_bit">', $other_options) . '</div>');

            $html .= $form_container->end();
            $html .= "</div>\n";

            //
            // SIGNATURE EDITOR
            //
            $signature_editor = $form->generate_text_area('signature', $this->bb->getInput('signature', $user->signature, true), ['id' => 'signature', 'rows' => 15, 'cols' => '70', 'style' => 'height: 250px; width: 95%']);
            $sig_smilies = $this->lang->off;
            if ($this->bb->settings['sigsmilies'] == 1) {
                $sig_smilies = $this->lang->on;
            }
            $sig_mycode = $this->lang->off;
            if ($this->bb->settings['sigmycode'] == 1) {
                $sig_mycode = $this->lang->on;
                $signature_editor .= $this->editor->build_mycode_inserter('signature', $this->bb->settings['smilieinserter']);
            }
            $sig_html = $this->lang->off;
            if ($this->bb->settings['sightml'] == 1) {
                $sig_html = $this->lang->on;
            }
            $sig_imgcode = $this->lang->off;
            if ($this->bb->settings['sigimgcode'] == 1) {
                $sig_imgcode = $this->lang->on;
            }
            $html .= "<div id=\"tab_signature\">\n";
            $form_container = new FormContainer($this, "{$this->lang->signature}: {$user->username}");
            $form_container->output_row($this->lang->signature, $this->lang->sprintf($this->lang->signature_desc, $sig_mycode, $sig_smilies, $sig_imgcode, $sig_html), $signature_editor, 'signature');

            $periods = [
                'hours' => $this->lang->expire_hours,
                'days' => $this->lang->expire_days,
                'weeks' => $this->lang->expire_weeks,
                'months' => $this->lang->expire_months,
                'never' => $this->lang->expire_permanent
            ];

            // Are we already suspending the signature?
            if (isset($this->bb->input['suspendsignature'])) {
                $sig_checked = 1;

                // Display how much time is left on the ban for the user to extend it
                if ($user->suspendsigtime == '0') {
                    // Permanent
                    $this->lang->suspend_expire_info = $this->lang->suspend_sig_perm;
                } else {
                    // There's a limit to the suspension!
                    $remaining = $user->suspendsigtime - TIME_NOW;
                    $expired = $this->time->niceTime($remaining, ['seconds' => false]);

                    $color = 'inherit';
                    if ($remaining < 3600) {
                        $color = 'red';
                    } elseif ($remaining < 86400) {
                        $color = 'maroon';
                    } elseif ($remaining < 604800) {
                        $color = 'green';
                    }

                    $this->lang->suspend_expire_info = $this->lang->sprintf($this->lang->suspend_expire_info, $expired, $color);
                }
                $user_suspend_info = '
				<tr>
					<td colspan="2">' . $this->lang->suspend_expire_info . '<br />' . $this->lang->suspend_sig_extend . '</td>
				</tr>';
            } else {
                $sig_checked = 0;
                $user_suspend_info = '';
            }

            $actions = '
	<script type="text/javascript">
	<!--
		var sig_checked = "' . $sig_checked . '";

		function toggleAction()
		{
			if($("#suspend_action").is(\':visible\'))
			{
				$("#suspend_action").hide();
			}
			else
			{
				$("#suspend_action").show();
			}
		}
	// -->
	</script>

	<dl style="margin-top: 0; margin-bottom: 0; width: 100%;">
		<dt>' . $form->generate_check_box("suspendsignature", 1, $this->lang->suspend_sig_box, ['checked' => $sig_checked, 'onclick' => 'toggleAction();']) . '</dt>
		<dd style="margin-top: 4px;" id="suspend_action" class="actions">
			<table cellpadding="4">' . $user_suspend_info . '
				<tr>
					<td width="30%"><small>' . $this->lang->expire_length . '</small></td>
					<td>' . $form->generate_numeric_field('action_time', $this->bb->input['action_time'], ['style' => 'width: 3em;', 'min' => 0]) . ' ' . $form->generate_select_box('action_period', $periods, $this->bb->input['action_period']) . '</td>
				</tr>
			</table>
		</dd>
	</dl>

	<script type="text/javascript">
	<!--
		if(sig_checked == 0)
		{
			$("#suspend_action").hide();
		}
	// -->
	</script>';

            $form_container->output_row($this->lang->suspend_sig, $this->lang->suspend_sig_info, $actions);

            $signature_options = [
                $form->generate_radio_button('update_posts', 'enable', $this->lang->enable_sig_in_all_posts, ['checked' => 0]),
                $form->generate_radio_button('update_posts', 'disable', $this->lang->disable_sig_in_all_posts, ['checked' => 0]),
                $form->generate_radio_button('update_posts', 'no', $this->lang->do_nothing, ['checked' => 1])
            ];

            $form_container->output_row($this->lang->signature_preferences, '', implode('<br />', $signature_options));

            $html .= $form_container->end();
            $html .= "</div>\n";

            //
            // AVATAR MANAGER
            //
            $html .= "<div id=\"tab_avatar\">\n";
            $table = new Table;
            $table->construct_header($this->lang->current_avatar, ['colspan' => 2]);

            $table->construct_cell("<div style=\"width: 126px; height: 126px;\" class=\"user_avatar\"><img src=\"" . htmlspecialchars_uni($user->avatar) . "\" width=\"{$scaled_dimensions['width']}\" style=\"margin-top: {$avatar_top}px\" height=\"{$scaled_dimensions['height']}\" alt=\"\" /></div>", ['width' => 1]);

            $avatar_url = '';
            if ($user->avatartype == 'upload' || stristr($user->avatar, $this->bb->settings['avataruploadpath'])) {
                $current_avatar_msg = "<br /><strong>{$this->lang->user_current_using_uploaded_avatar}</strong>";
            } elseif ($user->avatartype == 'remote' || my_strpos(my_strtolower($user->avatar), 'http://') !== false) {
                $current_avatar_msg = "<br /><strong>{$this->lang->user_current_using_remote_avatar}</strong>";
                $avatar_url = $user->avatar;
            }

            if ($errors) {
                $avatar_url = htmlspecialchars_uni($this->bb->input['avatar_url']);
            }

            if ($this->bb->settings['maxavatardims'] != "") {
                list($max_width, $max_height) = explode("x", my_strtolower($this->bb->settings['maxavatardims']));
                $max_size = "<br />{$this->lang->max_dimensions_are} {$max_width}x{$max_height}";
            }

            if ($this->bb->settings['avatarsize']) {
                $maximum_size = $this->parser->friendlySize($this->bb->settings['avatarsize'] * 1024);
                $max_size .= "<br />{$this->lang->avatar_max_size} {$maximum_size}";
            }

            if ($user->avatar) {
                $remove_avatar = "<br /><br />" . $form->generate_check_box("remove_avatar", 1, "<strong>{$this->lang->remove_avatar}</strong>");
            }

            $table->construct_cell($this->lang->avatar_desc . "{$remove_avatar}<br /><small>{$max_size}</small>");
            $table->construct_row();

            $html .= $table->output($this->lang->avatar . ": {$user->username}");

            // Custom avatar
            if ($this->bb->settings['avatarresizing'] == "auto") {
                $auto_resize = $this->lang->avatar_auto_resize;
            } elseif ($this->bb->settings['avatarresizing'] == "user") {
                $auto_resize = "<input type=\"checkbox\" name=\"auto_resize\" value=\"1\" checked=\"checked\" id=\"auto_resize\" /> <label for=\"auto_resize\">{$this->lang->attempt_to_auto_resize}</label></span>";
            }
            $form_container = new FormContainer($this, $this->lang->specify_custom_avatar);
            $form_container->output_row($this->lang->upload_avatar, $auto_resize, $form->generate_file_upload_box('avatar_upload', ['id' => 'avatar_upload']), 'avatar_upload');
            $form_container->output_row($this->lang->or_specify_avatar_url, "", $form->generate_text_box('avatar_url', $avatar_url, ['id' => 'avatar_url']), 'avatar_url');
            $html .= $form_container->end();
            $html .= "</div>\n";

            //
            // MODERATOR OPTIONS
            //
            $periods = [
                'hours' => $this->lang->expire_hours,
                'days' => $this->lang->expire_days,
                'weeks' => $this->lang->expire_weeks,
                'months' => $this->lang->expire_months,
                'never' => $this->lang->expire_permanent
            ];

            $html .= "<div id=\"tab_modoptions\">\n";
            $form_container = new FormContainer($this, $this->lang->mod_options . ": {$user->username}");
            $form_container->output_row($this->lang->user_notes, '', $form->generate_text_area('usernotes', $this->bb->getInput('usernotes', $user->usernotes, true), ['id' => 'usernotes']), 'usernotes');

            // Mod posts
            // Generate check box
            $modpost_options = $form->generate_select_box('modpost_period', $periods, $this->bb->input['modpost_period'], ['id' => 'modpost_period']);

            // Do we have any existing suspensions here?
            $existing_info = '';
            if ($user->moderateposts || ($this->bb->input['moderateposting'] && !empty($errors))) {
                $this->bb->input['moderateposting'] = 1;
                if ($user->moderationtime != 0) {
                    $remaining = $user->moderationtime - TIME_NOW;
                    $expired = $this->time->niceTime($remaining, ['seconds' => false]);

                    $color = 'inherit';
                    if ($remaining < 3600) {
                        $color = 'red';
                    } elseif ($remaining < 86400) {
                        $color = 'maroon';
                    } elseif ($remaining < 604800) {
                        $color = 'green';
                    }

                    $existing_info = $this->lang->sprintf($this->lang->moderate_length, $expired, $color);
                } else {
                    $existing_info = $this->lang->moderated_perm;
                }
            }

            $modpost_div = '<div id="modpost">' . $existing_info . '' . $this->lang->moderate_for . ' ' . $form->generate_numeric_field("modpost_time", $this->bb->input['modpost_time'], ['style' => 'width: 3em;', 'min' => 0]) . ' ' . $modpost_options . '</div>';
            $this->lang->moderate_posts_info = $this->lang->sprintf($this->lang->moderate_posts_info, $user->username);
            $form_container->output_row($form->generate_check_box("moderateposting", 1, $this->lang->moderate_posts, ["id" => "moderateposting", "onclick" => "toggleBox('modpost');", "checked" => $this->bb->input['moderateposting']]), $this->lang->moderate_posts_info, $modpost_div);

            // Suspend posts
            // Generate check box
            $suspost_options = $form->generate_select_box('suspost_period', $periods, $this->bb->input['suspost_period'], ['id' => 'suspost_period']);

            // Do we have any existing suspensions here?
//            if ($user->suspendposting || ($this->bb->input['suspendposting'] && !empty($errors))) {
            if ($this->bb->getInput('suspendposting', $user->suspendposting) && !empty($errors)) {
                $this->bb->input['suspendposting'] = 1;

                if ($user->suspensiontime == 0 || $this->bb->input['suspost_period'] == 'never') {
                    $existing_info = $this->lang->suspended_perm;
                } else {
                    $remaining = $user->suspensiontime - TIME_NOW;
                    $suspost_date = $this->time->niceTime($remaining, ['seconds' => false]);

                    $color = 'inherit';
                    if ($remaining < 3600) {
                        $color = 'red';
                    } elseif ($remaining < 86400) {
                        $color = 'maroon';
                    } elseif ($remaining < 604800) {
                        $color = 'green';
                    }

                    $existing_info = $this->lang->sprintf($this->lang->suspend_length, $suspost_date, $color);
                }
            }

            $suspost_div = '<div id="suspost">' . $existing_info . '' . $this->lang->suspend_for . ' ' . $form->generate_numeric_field("suspost_time", $this->bb->input['suspost_time'], ['style' => 'width: 3em;', 'min' => 0]) . ' ' . $suspost_options . '</div>';
            $this->lang->suspend_posts_info = $this->lang->sprintf($this->lang->suspend_posts_info, $user->username);
            $form_container->output_row($form->generate_check_box('suspendposting', 1, $this->lang->suspend_posts, ['id' => 'suspendposting', 'onclick' => "toggleBox('suspost');", 'checked' => $this->bb->getInput('suspendposting', $user->suspendposting)]), $this->lang->suspend_posts_info, $suspost_div);


            $html .= $form_container->end();
            $html .= "</div>\n";

            $this->plugins->runHooks("admin_user_users_edit_graph");

            $buttons[] = $form->generate_submit_button($this->lang->save_user);
            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();

            $html .= '<script type="text/javascript">
<!--

function toggleBox(action)
{
	if(action == "modpost")
	{
		$("#suspendposting").attr("checked", false);
		$("#suspost").hide();

		if($("#moderateposting").is(":checked") == true)
		{
			$("#modpost").show();
		}
		else if($("#moderateposting").is(":checked") == false)
		{
			$("#modpost").hide();
		}
	}
	else if(action == "suspost")
	{
		$("#moderateposting").attr("checked", false);
		$("#modpost").hide();

		if($("#suspendposting").is(":checked") == true)
		{
			$("#suspost").show();
		}
		else if($("#suspendposting").is(":checked") == false)
		{
			$("#suspost").hide();
		}
	}
}

if($("#moderateposting").is(":checked") == false)
{
	$("#modpost").hide();
}
else
{
	$("#modpost").show();
}

if($("#suspendposting").is(":checked") == false)
{
	$("#suspost").hide();
}
else
{
	$("#suspost").show();
}

// -->
</script>';

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/UsersEdit.html.twig');
        }

        if ($this->bb->input['action'] == 'delete') {
            $user = $this->user->get_user($this->bb->input['uid']);

            // Does the user not exist?
            if (!$user['uid']) {
                $this->session->flash_message($this->lang->error_invalid_user, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            if ($this->user->is_super_admin($this->bb->input['uid']) &&
                $this->user->uid != $this->bb->input['uid'] &&
                !$this->user->is_super_admin($this->user->uid)
            ) {
                $this->session->flash_message($this->lang->error_no_perms_super_admin, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            // User clicked no
            if ($this->bb->input['no']) {
                return $response->withRedirect($this->bb->admin_url . '/users');
            }

            $this->plugins->runHooks('admin_user_users_delete');

            if ($this->bb->request_method == 'post') {
                $this->plugins->runHooks('admin_user_users_delete_commit');

                // Set up user handler.
                $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'delete');

                // Delete the user
                if (!$userhandler->delete_user($user['uid'])) {
                    $this->session->flash_message($this->lang->error_cannot_delete_user, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/users');
                }

                $this->cache->update_awaitingactivation();

                $this->plugins->runHooks('admin_user_users_delete_commit_end');

                $this->bblogger->log_admin_action($user['uid'], $user['username']);

                $this->session->flash_message($this->lang->success_user_deleted, 'success');
                return $response->withRedirect($this->bb->admin_url . '/users');
            } else {
                $this->page->output_confirm_action($this->bb->admin_url . '/users?action=delete&uid=' . $user['uid'], $this->lang->user_deletion_confirmation);
            }
        }

        if ($this->bb->input['action'] == 'referrers') {
            $this->page->add_breadcrumb_item($this->lang->show_referrers);
            $html = $this->page->output_header($this->lang->show_referrers);

            $sub_tabs['referrers'] = [
                'title' => $this->lang->show_referrers,
                'link' => $this->bb->admin_url . '/users?action=referrers&uid=' . $this->bb->input['uid'],
                'description' => $this->lang->show_referrers_desc
            ];

            $this->plugins->runHooks('admin_user_users_referrers');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'referrers');

            // Fetch default admin view
            $default_view = $this->fetch_default_view('users');
            if (!$default_view) {
                $default_view = '0';
            }
            $query = $this->db->simple_select('adminviews', '*', "type='user' AND (vid='{$default_view}' OR uid=0)", ['order_by' => 'uid', 'order_dir' => 'desc']);
            $admin_view = $this->db->fetch_array($query);

            if (isset($this->bb->input['type'])) {
                $admin_view['view_type'] = $this->bb->input['type'];
            }

            $admin_view['conditions'] = my_unserialize($admin_view['conditions']);
            $admin_view['conditions']['referrer'] = $this->bb->input['uid'];

            $view = $this->build_users_view($admin_view);

            // No referred users
            if (!$view) {
                $table = new Table;
                $table->construct_cell($this->lang->error_no_referred_users);
                $table->construct_row();
                $html .= $table->output($this->lang->show_referrers);
            } else {
                $html .= $view;
            }

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/UsersReferrers.html.twig');
        }

        if ($this->bb->input['action'] == 'ipaddresses') {
            $this->page->add_breadcrumb_item($this->lang->ip_addresses);
            $html = $this->page->output_header($this->lang->ip_addresses);

            $sub_tabs['ipaddresses'] = [
                'title' => $this->lang->show_ip_addresses,
                'link' => $this->bb->admin_url . '/users?action=ipaddresses&uid=' . $this->bb->input['uid'],
                'description' => $this->lang->show_ip_addresses_desc
            ];

            $this->plugins->runHooks('admin_user_users_ipaddresses');

            $html .= $this->page->output_nav_tabs($sub_tabs, 'ipaddresses');

            $query = $this->db->simple_select('users', 'uid, regip, username, lastip', "uid='{$this->bb->input['uid']}'", ['limit' => 1]);
            $user = $this->db->fetch_array($query);

            // Log admin action
            $this->bblogger->log_admin_action($user['uid'], $user['username']);

            $table = new Table;

            $table->construct_header($this->lang->ip_address);
            $table->construct_header($this->lang->controls, ['width' => 200, 'class' => "align_center"]);

            if (empty($user['lastip'])) {
                $user['lastip'] = $this->lang->unknown;
                $controls = '';
            } else {
                $popup = new PopupMenu('user_last', $this->lang->options);
                $popup->add_item(
                    $this->lang->show_users_regged_with_ip,
                    $this->bb->admin_url . '/users?action=search&results=1&conditions=' . urlencode(my_serialize(['regip' => $user['lastip']]))
                );
                $popup->add_item($this->lang->show_users_posted_with_ip, $this->bb->admin_url . '/users?results=1&action=search&conditions=' . urlencode(my_serialize(["postip" => $user['lastip']])));
                $popup->add_item($this->lang->info_on_ip, $this->bb->admin_url . '/users?action=iplookup&ipaddress='.$user['lastip'], "BB.popupWindow('{$this->bb->settings['bburl']}admin/users?action=iplookup&ipaddress={$user['lastip']}', null, true); return false;");
                $popup->add_item($this->lang->ban_ip, $this->bb->admin_url . '/config/banning?filter=' . $user['lastip']);
                $controls = $popup->fetch();
            }
            $table->construct_cell("<strong>{$this->lang->last_known_ip}:</strong> " . $user['lastip']);
            $table->construct_cell($controls, ['class' => 'align_center']);
            $table->construct_row();

            if (empty($user['regip'])) {
                $user['regip'] = $this->lang->unknown;
                $controls = '';
            } else {
                $popup = new PopupMenu("user_reg", $this->lang->options);
                $popup->add_item($this->lang->show_users_regged_with_ip, $this->bb->admin_url . '/users?results=1&action=search&conditions=' . urlencode(my_serialize(["regip" => $user['regip']])));
                $popup->add_item($this->lang->show_users_posted_with_ip, $this->bb->admin_url . '/users?results=1&action=search&conditions=' . urlencode(my_serialize(["postip" => $user['regip']])));
                $popup->add_item($this->lang->info_on_ip, $this->bb->admin_url . '/users?action=iplookup&ipaddress='.$user['regip'], "BB.popupWindow('".$this->bb->admin_url."/users?action=iplookup&ipaddress={$user['regip']}', null, true); return false;");
                $popup->add_item($this->lang->ban_ip, $this->bb->admin_url . '/config/banning?filter=' . $user['regip']);
                $controls = $popup->fetch();
            }
            $table->construct_cell("<strong>{$this->lang->registration_ip}:</strong> " . $user['regip']);
            $table->construct_cell($controls, ['class' => 'align_center']);
            $table->construct_row();

            $counter = 0;

            $query = $this->db->simple_select('posts', 'DISTINCT ipaddress', "uid='{$this->bb->input['uid']}'");
            while ($ip = $this->db->fetch_array($query)) {
                ++$counter;
                $popup = new PopupMenu("id_{$counter}", $this->lang->options);
                $popup->add_item($this->lang->show_users_regged_with_ip, $this->bb->admin_url . '/users?results=1&action=search&conditions=' . urlencode(my_serialize(["regip" => $ip['ipaddress']])));
                $popup->add_item($this->lang->show_users_posted_with_ip, $this->bb->admin_url . '/users?results=1&action=search&conditions=' . urlencode(my_serialize(["postip" => $ip['ipaddress']])));
                $popup->add_item($this->lang->info_on_ip, $this->bb->admin_url . '/users?action=iplookup&ipaddress='.$ip['ipaddress'], "BB.popupWindow('".$this->bb->admin_url."/users?action=iplookup&ipaddress={$ip['ipaddress']}', null, true); return false;");
                $popup->add_item($this->lang->ban_ip, $this->bb->admin_url . '/config/banning?filter=' . $ip['ipaddress']);
                $controls = $popup->fetch();

                $table->construct_cell($ip['ipaddress']);
                $table->construct_cell($controls, ['class' => "align_center"]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->ip_address_for . " {$user['username']}");

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/UsersIpaddresses.html.twig');
        }

        if ($this->bb->input['action'] == 'merge') {
            $this->plugins->runHooks('admin_user_users_merge');
            $errors = [];
            if ($this->bb->request_method == 'post') {
                $source_user = $this->user->get_user_by_username($this->bb->input['source_username'], ['fields' => '*']);
                if (!$source_user['uid']) {
                    $errors[] = $this->lang->error_invalid_user_source;
                }

                $destination_user = $this->user->get_user_by_username($this->bb->input['destination_username'], ['fields' => '*']);
                if (!$destination_user['uid']) {
                    $errors[] = $this->lang->error_invalid_user_destination;
                }

                // If we're not a super admin and we're merging a source super admin or a destination super admin then dissallow this action
                if (!$this->user->is_super_admin($this->user->uid) &&
                    ($this->user->is_super_admin($source_user['uid']) ||
                        $this->user->is_super_admin($destination_user['uid']))
                ) {
                    $this->session->flash_message($this->lang->error_no_perms_super_admin, 'error');
                    return $response->withRedirect($this->bb->admin_url . '/users');
                }

                if ($source_user['uid'] == $destination_user['uid']) {
                    $errors[] = $this->lang->error_cannot_merge_same_account;
                }

                if (empty($errors)) {
                    // Begin to merge the accounts
                    $uid_update = [
                        'uid' => $destination_user['uid']
                    ];
                    $query = $this->db->simple_select('adminoptions', 'uid', "uid='{$destination_user['uid']}'");
                    $existing_admin_options = $this->db->fetch_field($query, 'uid');

                    // Only carry over admin options/permissions if we don't already have them
                    if (!$existing_admin_options) {
                        $this->db->update_query('adminoptions', $uid_update, "uid='{$source_user['uid']}'");
                    }

                    $this->db->update_query('adminlog', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('announcements', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('events', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('threadsubscriptions', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('forumsubscriptions', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('joinrequests', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('moderatorlog', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('pollvotes', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('posts', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('privatemessages', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('reportedcontent', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('threadratings', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('threads', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('warnings', $uid_update, "uid='{$source_user['uid']}'");
                    $this->db->update_query('warnings', ['revokedby' => $destination_user['uid']], "revokedby='{$source_user['uid']}'");
                    $this->db->update_query('warnings', ['issuedby' => $destination_user['uid']], "issuedby='{$source_user['uid']}'");

                    // Banning
                    $this->db->update_query('banned', ['admin' => $destination_user['uid']], "admin = '{$source_user['uid']}'");

                    // Merging Reputation
                    // First, let's change all the details over to our new user...
                    $this->db->update_query('reputation', ['adduid' => $destination_user['uid']], "adduid = '" . $source_user['uid'] . "'");
                    $this->db->update_query('reputation', ['uid' => $destination_user['uid']], "uid = '" . $source_user['uid'] . "'");

                    // Now that all the repuation is merged, figure out what to do with this user's comments...
                    $options = [
                        'order_by' => 'uid',
                        'order_dir' => 'ASC'
                    ];

                    $to_remove = [];
                    $query = $this->db->simple_select('reputation', '*', "adduid = '" . $destination_user['uid'] . "'");
                    while ($rep = $this->db->fetch_array($query)) {
                        if ($rep['pid'] == 0 && $this->bb->settings['multirep'] == 0 && $last_result['uid'] == $rep['uid']) {
                            // Multiple reputation is disallowed, and this isn't a post, so let's remove this comment
                            $to_remove[] = $rep['rid'];
                        }

                        // Remove comments or posts liked by 'me'
                        if ($last_result['uid'] == $destination_user['uid'] || $rep['uid'] == $destination_user['uid']) {
                            if (!in_array($rep['rid'], $to_remove)) {
                                $to_remove[] = $rep['rid'];
                                continue;
                            }
                        }

                        $last_result = [
                            'rid' => $rep['rid'],
                            'uid' => $rep['uid']
                        ];
                    }

                    // Remove any reputations we've selected to remove...
                    if (!empty($to_remove)) {
                        $imp = implode(',', $to_remove);
                        $this->db->delete_query('reputation', 'rid IN (' . $imp . ')');
                    }

                    // Calculate the new reputation for this user...
                    $query = $this->db->simple_select('reputation', 'SUM(reputation) as total_rep', "uid='{$destination_user['uid']}'");
                    $total_reputation = $this->db->fetch_field($query, 'total_rep');

                    $this->db->update_query('users', ['reputation' => (int)$total_reputation], "uid='{$destination_user['uid']}'");

                    // Calculate warning points
                    $query = $this->db->query('
				SELECT SUM(points) as warn_lev
				FROM ' . TABLE_PREFIX . "warnings
				WHERE uid='{$source_user['uid']}' AND expired='0'
			");
                    $original_warn_level = $this->db->fetch_field($query, 'warn_lev');

                    $query = $this->db->query('
				SELECT SUM(points) as warn_lev
				FROM ' . TABLE_PREFIX . "warnings
				WHERE uid='{$destination_user['uid']}' AND expired='0'
			");
                    $new_warn_level = $this->db->fetch_field($query, 'warn_lev');
                    $this->db->update_query('users', ['warningpoints' => (int)$original_warn_level + $new_warn_level], "uid='{$destination_user['uid']}'");

                    // Additional updates for non-uid fields
                    $last_poster = [
                        'lastposteruid' => $destination_user['uid'],
                        'lastposter' => $this->db->escape_string($destination_user['username'])
                    ];
                    $this->db->update_query('forums', $last_poster, "lastposteruid='{$source_user['uid']}'");
                    $this->db->update_query('threads', $last_poster, "lastposteruid='{$source_user['uid']}'");
                    $edit_uid = [
                        'edituid' => $destination_user['uid']
                    ];
                    $this->db->update_query('posts', $edit_uid, "edituid='{$source_user['uid']}'");

                    $from_uid = [
                        'fromid' => $destination_user['uid']
                    ];
                    $this->db->update_query('privatemessages', $from_uid, "fromid='{$source_user['uid']}'");
                    $to_uid = [
                        'toid' => $destination_user['uid']
                    ];
                    $this->db->update_query('privatemessages', $to_uid, "toid='{$source_user['uid']}'");

                    // Buddy/ignore lists
                    $destination_buddies = explode(',', $destination_user['buddylist']);
                    $source_buddies = explode(',', $source_user['buddylist']);
                    $buddies = array_unique(array_merge($source_buddies, $destination_buddies));
                    // Make sure the new buddy list doesn't contain either users
                    $buddies_array = array_diff($buddies, [$destination_user['uid'], $source_user['uid']]);

                    $destination_ignored = explode(',', $destination_user['ignorelist']);
                    $source_ignored = explode(',', $destination_user['ignorelist']);
                    $ignored = array_unique(array_merge($source_ignored, $destination_ignored));
                    // ... and the same for the new ignore list
                    $ignored_array = array_diff($ignored, [$destination_user['uid'], $source_user['uid']]);

                    // Remove any ignored users from the buddy list
                    $buddies = array_diff($buddies_array, $ignored_array);
                    // implode the arrays so we get a nice neat list for each
                    $buddies = trim(implode(',', $buddies), ',');
                    $ignored = trim(implode(',', $ignored_array), ',');

                    $lists = [
                        'buddylist' => $buddies,
                        'ignorelist' => $ignored
                    ];
                    $this->db->update_query('users', $lists, "uid='{$destination_user['uid']}'");

                    // Set up user handler.
                    $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'delete');

                    // Delete the old user
                    $userhandler->delete_user($source_user['uid']);

                    // Get a list of forums where post count doesn't apply
                    $fids = [];
                    $query = $this->db->simple_select('forums', 'fid', 'usepostcounts=0');
                    while ($fid = $this->db->fetch_field($query, 'fid')) {
                        $fids[] = $fid;
                    }

                    $fids_not_in = '';
                    if (!empty($fids)) {
                        $fids_not_in = 'AND fid NOT IN(' . implode(',', $fids) . ')';
                    }

                    // Update user post count
                    $query = $this->db->simple_select('posts', 'COUNT(*) AS postnum', "uid='" . $destination_user['uid'] . "' {$fids_not_in}");
                    $num = $this->db->fetch_array($query);
                    $updated_count = [
                        'postnum' => $num['postnum']
                    ];
                    $this->db->update_query('users', $updated_count, "uid='{$destination_user['uid']}'");

                    // Update user thread count
                    $query = $this->db->simple_select('threads', 'COUNT(*) AS threadnum', "uid='" . $destination_user['uid'] . "' {$fids_not_in}");
                    $num = $this->db->fetch_array($query);
                    $updated_count = [
                        'threadnum' => $num['threadnum']
                    ];
                    $this->db->update_query('users', $updated_count, "uid='{$destination_user['uid']}'");

                    // Use the earliest registration date
                    if ($destination_user['regdate'] > $source_user['regdate']) {
                        $this->db->update_query('users', ['regdate' => $source_user['regdate']], "uid='{$destination_user['uid']}'");
                    }

                    $this->plugins->runHooks('admin_user_users_merge_commit');

                    $this->cache->update_awaitingactivation();

                    // Log admin action
                    $this->bblogger->log_admin_action($source_user['uid'], $source_user['username'], $destination_user['uid'], $destination_user['username']);

                    // Redirect!
                    $this->session->flash_message("<strong>{$source_user['username']}</strong> {$this->lang->success_merged} {$destination_user['username']}", 'success');
                    return $response->withRedirect($this->bb->admin_url . '/users');
                }
            }

            $this->page->add_breadcrumb_item($this->lang->merge_users);
            $html = $this->page->output_header($this->lang->merge_users);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'merge_users');

            // If we have any error messages, show them
            if ($errors) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/users?action=merge', 'post');
            $html .= $form->getForm();
            $form_container = new FormContainer($this, $this->lang->merge_users);
            $form_container->output_row($this->lang->source_account . ' <em>*</em>', $this->lang->source_account_desc, $form->generate_text_box('source_username', $this->bb->getInput('source_username', '', true), ['id' => 'source_username']), 'source_username');
            $form_container->output_row($this->lang->destination_account . ' <em>*</em>', $this->lang->destination_account_desc, $form->generate_text_box('destination_username', $this->bb->getInput('destination_username', '', true), ['id' => 'destination_username']), 'destination_username');
            $html .= $form_container->end();

            // Autocompletion for usernames
            $html .= '
	<link rel="stylesheet" href="' . $this->bb->asset_url . '/jscripts/select2/select2.css">
	<script type="text/javascript" src="' . $this->bb->asset_url . '/jscripts/select2/select2.min.js?ver=1804"></script>
	<script type="text/javascript">
	<!--
	$("#source_username").select2({
		placeholder: "' . $this->lang->search_for_a_user . '",
		minimumInputLength: 2,
		multiple: false,
		ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
			url: "'.$this->bb->settings['bburl'].'/xmlhttp?action=get_users",
			dataType: "json",
			data: function (term, page) {
				return {
					query: term // search term
				};
			},
			results: function (data, page) { // parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to alter remote JSON data
				return {results: data};
			}
		},
		initSelection: function(element, callback) {
			var query = $(element).val();
			if (query !== "") {
				$.ajax("'.$this->bb->settings['bburl'].'/xmlhttp?action=get_users&getone=1", {
					data: {
						query: query
					},
					dataType: "json"
				}).done(function(data) { callback(data); });
			}
		}
	});
	$("#destination_username").select2({
		placeholder: "' . $this->lang->search_for_a_user . '",
		minimumInputLength: 2,
		multiple: false,
		ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
			url: "'.$this->bb->settings['bburl'].'/xmlhttp?action=get_users",
			dataType: "json",
			data: function (term, page) {
				return {
					query: term // search term
				};
			},
			results: function (data, page) { // parse the results into the format expected by Select2.
				// since we are using custom formatting functions we do not need to alter remote JSON data
				return {results: data};
			}
		},
		initSelection: function(element, callback) {
			var query = $(element).val();
			if (query !== "") {
				$.ajax("'.$this->bb->settings['bburl'].'/xmlhttp?action=get_users&getone=1", {
					data: {
						query: query
					},
					dataType: "json"
				}).done(function(data) { callback(data); });
			}
		}
	});
	// -->
	</script>';

            $buttons[] = $form->generate_submit_button($this->lang->merge_user_accounts);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/UsersMerge.html.twig');
        }

        if ($this->bb->input['action'] == 'search') {
            $this->plugins->runHooks('admin_user_users_search');

            if ($this->bb->request_method == 'post' || (isset($this->bb->input['results']) && $this->bb->input['results'] == 1)) {
                // Build view options from incoming search options
                if (isset($this->bb->input['vid'])) {
                    $query = $this->db->simple_select("adminviews", "*", "vid='" . $this->bb->getInput('vid', 0) . "'");
                    $admin_view = $this->db->fetch_array($query);
                    // View does not exist or this view is private and does not belong to the current user
                    if (!$admin_view['vid'] || ($admin_view['visibility'] == 1 && $admin_view['uid'] != $this->user->uid)) {
                        unset($admin_view);
                    }
                }

                if (isset($this->bb->input['search_id']) && $this->session->admin_session['data']['user_views'][$this->bb->input['search_id']]) {
                    $admin_view = $this->session->admin_session['data']['user_views'][$this->bb->input['search_id']];
                    unset($admin_view['extra_sql']);
                } else {
                    // Don't have a view? Fetch the default
                    if (!isset($admin_view['vid'])) {
                        $default_view = $this->fetch_default_view('users');
                        if (!$default_view) {
                            $default_view = '0';
                        }
                        $query = $this->db->simple_select("adminviews", "*", "type='user' AND (vid='{$default_view}' OR uid=0)", ["order_by" => "uid", "order_dir" => "desc"]);
                        $admin_view = $this->db->fetch_array($query);
                    }
                }

                // Override specific parts of the view
                unset($admin_view['vid']);

                if (isset($this->bb->input['type'])) {
                    $admin_view['view_type'] = $this->bb->input['type'];
                }

                if ($this->bb->input['conditions']) {
                    $admin_view['conditions'] = $this->bb->input['conditions'];
                }

                if (isset($this->bb->input['sortby'])) {
                    $admin_view['sortby'] = $this->bb->input['sortby'];
                }

                if ($this->bb->getInput('perpage', 0)) {
                    $admin_view['perpage'] = $this->bb->input['perpage'];
                }

                if (isset($this->bb->input['order'])) {
                    $admin_view['sortorder'] = $this->bb->input['order'];
                }

                if (isset($this->bb->input['displayas'])) {
                    $admin_view['view_type'] = $this->bb->input['displayas'];
                }

                if (isset($this->bb->input['profile_fields'])) {
                    $admin_view['custom_profile_fields'] = $this->bb->input['profile_fields'];
                }

                $this->plugins->runHooks("admin_user_users_search_commit");

                $results = $this->build_users_view($admin_view);

                if ($results) {
                    $html = $this->page->output_header($this->lang->find_users);
                    $html .= "<script type=\"text/javascript\" src=\"{$this->bb->asset_url}/admin/jscripts/users.js\"></script>";
                    $html .= $this->page->output_nav_tabs($sub_tabs, 'find_users');
                    $html .= $results;
                    $this->page->output_footer();

                    $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
                    return $this->view->render($response, '@forum/Admin/Users/UsersSearchResult.html.twig');
                } else {
                    if ($this->bb->input['from'] == "home") {
                        $this->session->flash_message($this->lang->error_no_users_found, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/users');
                        exit;
                    } else {
                        $errors[] = $this->lang->error_no_users_found;
                    }
                }
            }

            $this->page->add_breadcrumb_item($this->lang->find_users);
            $html = $this->page->output_header($this->lang->find_users);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'find_users');

            // If we have any error messages, show them
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            if (!isset($this->bb->input['displayas'])) {
                $this->bb->input['displayas'] = 'card';
            }

            $form = new Form($this->bb, $this->bb->admin_url . '/users?action=search', "post");
            $html .= $form->getForm();
            $html .= $this->user_search_conditions($this->bb->input, $form);

            $form_container = new FormContainer($this, $this->lang->display_options);
            $sort_directions = [
                'asc' => $this->lang->ascending,
                'desc' => $this->lang->descending
            ];
            $form_container->output_row($this->lang->sort_results_by, '', $form->generate_select_box('sortby', $sort_options, $this->bb->getInput('sortby', ''), ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('order', $sort_directions, $this->bb->getInput('order', ''), ['id' => 'order']), 'sortby');
            $form_container->output_row($this->lang->results_per_page, '', $form->generate_numeric_field('perpage', $this->bb->getInput('perpage', 15), ['id' => 'perpage', 'min' => 1]), 'perpage');
            $form_container->output_row($this->lang->display_results_as, '', $form->generate_radio_button('displayas', 'table', $this->lang->table, ['checked' => ($this->bb->input['displayas'] != "card" ? true : false)]) . "<br />" . $form->generate_radio_button('displayas', 'card', $this->lang->business_card, ['checked' => ($this->bb->input['displayas'] == "card" ? true : false)]));
            $html .= $form_container->end();

            $buttons[] = $form->generate_submit_button($this->lang->find_users);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/UsersSearch.html.twig');
        }

        if ($this->bb->input['action'] == "inline_edit") {
            $this->plugins->runHooks("admin_user_users_inline");

            if ($this->bb->input['vid'] || $this->bb->cookies['acp_view']) {
                // We have a custom view
                if (!$this->bb->cookies['acp_view']) {
                    // Set a cookie
                    $this->bb->my_setcookie("acp_view", $this->bb->input['vid'], 60);
                } elseif ($this->bb->cookies['acp_view']) {
                    // We already have a cookie, so let's use it...
                    $this->bb->input['vid'] = $this->bb->cookies['acp_view'];
                }

                $vid_url = "?vid=" . $this->bb->input['vid'];
            }

            // First, collect the user IDs that we're performing the moderation on
            $ids = explode("|", $this->bb->cookies['inlinemod_useracp']);
            foreach ($ids as $id) {
                if ($id != '') {
                    $selected[] = (int)$id;
                }
            }

            // Verify incoming POST request
            if (!$this->bb->verify_post_check($this->bb->input['my_post_key'])) {
                $this->session->flash_message($this->lang->invalid_post_verify_key2, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users');
            }
            $sub_tabs['manage_users'] = [
                "title" => $this->lang->manage_users,
                "link" => "./",
                "description" => $this->lang->manage_users_desc
            ];
            $this->page->add_breadcrumb_item($this->lang->manage_users);

            if (!is_array($selected)) {
                // Not selected any users, show error
                $this->session->flash_message($this->lang->error_inline_no_users_selected, 'error');
                return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
            }

            switch ($this->bb->input['inline_action']) {
                case 'multiactivate':
                    // Run through the activating users, so that users already registered (but have been selected) aren't affected
                    if (is_array($selected)) {
                        $sql_array = implode(",", $selected);
                        $query = $this->db->simple_select("users", "uid, username, email", "usergroup = '5' AND uid IN (" . $sql_array . ")");
                        $user_mail_data = [];
                        while ($user = $this->db->fetch_array($query)) {
                            $to_update[] = $user['uid'];
                            $user_mail_data[] = ['username' => $user['username'], 'email' => $user['email']];
                        }
                    }

                    if (is_array($to_update)) {
                        $sql_array = implode(",", $to_update);
                        $this->db->write_query("UPDATE " . TABLE_PREFIX . "users SET usergroup = '2' WHERE uid IN (" . $sql_array . ")");

                        $this->cache->update_awaitingactivation();

                        // send activation mail
                        foreach ($user_mail_data as $mail_data) {
                            $message = $this->lang->sprintf($this->lang->email_adminactivateaccount, $mail_data['username'], $this->bb->settings['bbname'], $this->bb->settings['bburl']);
                            $this->mail->send($mail_data['email'], $this->lang->sprintf($this->lang->emailsubject_activateaccount, $this->bb->settings['bbname']), $message);
                        }

                        // Action complete, grab stats and show success message - redirect user
                        $to_update_count = count($to_update);
                        $this->lang->inline_activated = $this->lang->sprintf($this->lang->inline_activated, $this->parser->formatNumber($to_update_count));

                        if ($to_update_count != count($selected)) {
                            // The update count is different to how many we selected!
                            $not_updated_count = count($selected) - $to_update_count;
                            $this->lang->inline_activated_more = $this->lang->sprintf($this->lang->inline_activated_more, $this->parser->formatNumber($not_updated_count));
                            $this->lang->inline_activated = $this->lang->inline_activated . "<br />" . $this->lang->inline_activated_more; // Add these stats to the message
                        }

                        $this->bb->input['action'] = "inline_activated"; // Force a change to the action so we can add it to the adminlog
                        $this->bblogger->log_admin_action($to_update_count); // Add to adminlog
                        $this->bb->my_unsetcookie("inlinemod_useracp"); // Unset the cookie, so that the users aren't still selected when we're redirected

                        $this->session->flash_message($this->lang->inline_activated, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                    } else {
                        // Nothing was updated, show an error
                        $this->session->flash_message($this->lang->inline_activated_failed, 'error');
                        return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                    }
                    break;
                case 'multilift':
                    // Get the users that are banned, and check that they have been selected
                    if ($this->bb->input['no']) {
                        return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url); // User clicked on 'No'
                    }

                    if ($this->bb->request_method == "post") {
                        $sql_array = implode(",", $selected);
                        $query = $this->db->simple_select("banned", "*", "uid IN (" . $sql_array . ")");
                        $to_be_unbanned = $this->db->num_rows($query);
                        while ($ban = $this->db->fetch_array($query)) {
                            $updated_group = [
                                "usergroup" => $ban['oldgroup'],
                                "additionalgroups" => $ban['oldadditionalgroups'],
                                "displaygroup" => $ban['olddisplaygroup']
                            ];
                            $this->db->update_query("users", $updated_group, "uid = '" . $ban['uid'] . "'");
                            $this->db->delete_query("banned", "uid = '" . $ban['uid'] . "'");
                        }

                        $this->cache->update_banned();
                        $this->cache->update_moderators();

                        $this->bb->input['action'] = "inline_lift";
                        $this->bblogger->log_admin_action($to_be_unbanned);
                        $this->bb->my_unsetcookie("inlinemod_useracp");

                        $this->lang->success_ban_lifted = $this->lang->sprintf($this->lang->success_ban_lifted, $this->parser->formatNumber($to_be_unbanned));
                        $this->session->flash_message($this->lang->success_ban_lifted, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                    } else {
                        $this->page->output_confirm_action($this->bb->admin_url . '/users?action=inline_edit&inline_action=multilift', $this->lang->confirm_multilift);
                    }

                    break;
                case 'multiban':
                    if ($this->bb->input['processed'] == 1) {
                        // We've posted ban information!
                        // Build an array of users to ban, =D
                        $sql_array = implode(",", $selected);
                        // Build a cache array for this users that have been banned already
                        $query = $this->db->simple_select("banned", "uid", "uid IN (" . $sql_array . ")");
                        while ($user = $this->db->fetch_array($query)) {
                            $bannedcache[] = "u_" . $user['uid'];
                        }

                        // Collect the users
                        $query = $this->db->simple_select("users", "uid, username, usergroup, additionalgroups, displaygroup", "uid IN (" . $sql_array . ")");

                        if ($this->bb->input['bantime'] == '---') {
                            $lifted = 0;
                        } else {
                            $lifted = $this->ban->ban_date2timestamp($this->bb->input['bantime']);
                        }

                        $reason = my_substr($this->bb->input['reason'], 0, 255);

                        $banned_count = 0;
                        while ($user = $this->db->fetch_array($query)) {
                            if ($user['uid'] == $this->user->uid || $this->user->is_super_admin($user['uid'])) {
                                // We remove ourselves and Super Admins from the mix
                                continue;
                            }

                            if (is_array($bannedcache) && in_array("u_" . $user['uid'], $bannedcache)) {
                                // User already has a ban, update it!
                                $update_array = [
                                    "admin" => (int)$this->user->uid,
                                    "dateline" => TIME_NOW,
                                    "bantime" => $this->db->escape_string($this->bb->input['bantime']),
                                    "lifted" => $this->db->escape_string($lifted),
                                    "reason" => $this->db->escape_string($reason)
                                ];
                                $this->db->update_query("banned", $update_array, "uid = '" . $user['uid'] . "'");
                            } else {
                                // Not currently banned - insert the ban
                                $insert_array = [
                                    'uid' => $user['uid'],
                                    'gid' => $this->bb->getInput('usergroup', 0),
                                    'oldgroup' => $user['usergroup'],
                                    'oldadditionalgroups' => $user['additionalgroups'],
                                    'olddisplaygroup' => $user['displaygroup'],
                                    'admin' => (int)$this->user->uid,
                                    'dateline' => TIME_NOW,
                                    'bantime' => $this->db->escape_string($this->bb->input['bantime']),
                                    'lifted' => $this->db->escape_string($lifted),
                                    'reason' => $this->db->escape_string($reason)
                                ];
                                $this->db->insert_query('banned', $insert_array);
                            }

                            // Moved the user to the 'Banned' Group
                            $update_array = [
                                'usergroup' => 7,
                                'displaygroup' => 0,
                                'additionalgroups' => '',
                            ];
                            $this->db->update_query('users', $update_array, "uid = '{$user['uid']}'");

                            $this->db->delete_query("forumsubscriptions", "uid = '{$user['uid']}'");
                            $this->db->delete_query("threadsubscriptions", "uid = '{$user['uid']}'");

                            $this->cache->update_banned();
                            ++$banned_count;
                        }
                        $this->bb->input['action'] = "inline_banned";
                        $this->bblogger->log_admin_action($banned_count, $lifted);
                        $this->bb->my_unsetcookie("inlinemod_useracp"); // Remove the cookie of selected users as we've finished with them

                        $this->lang->users_banned = $this->lang->sprintf($this->lang->users_banned, $banned_count);
                        $this->session->flash_message($this->lang->users_banned, 'success');
                        return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                    }

                    echo $this->page->output_header($this->lang->manage_users);
                    echo $this->page->output_nav_tabs($sub_tabs, 'manage_users');

                    // Provide the user with a warning of what they're about to do
                    $table = new Table;
                    $this->lang->mass_ban_info = $this->lang->sprintf($this->lang->mass_ban_info, count($selected));
                    $table->construct_cell($this->lang->mass_ban_info);
                    $table->construct_row();
                    echo $table->output($this->lang->important);

                    // If there's any errors, display inline
                    if ($errors) {
                        echo $this->page->output_inline_error($errors);
                    }

                    $form = new Form($this->bb, $this->bb->admin_url . '/users', 'post');
                    echo $form->getForm();
                    echo $form->generate_hidden_field('action', 'inline_edit');
                    echo $form->generate_hidden_field('inline_action', 'multiban');
                    echo $form->generate_hidden_field('processed', '1');

                    $form_container = new FormContainer($this, '<div class="float_right"><a href="' . $this->bb->admin_url . '/users?action=inline_edit&amp;inline_action=multilift&amp;my_post_key=' . $this->bb->post_code . '">' . $this->lang->lift_bans . '</a></div>' . $this->lang->mass_ban);
                    $form_container->output_row($this->lang->ban_reason, "", $form->generate_text_area('reason', $this->bb->input['reason'], ['id' => 'reason', 'maxlength' => '255']), 'reason');
                    $ban_times = $this->ban->fetch_ban_times();
                    foreach ($ban_times as $time => $period) {
                        if ($time != '---') {
                            $friendly_time = $this->time->formatDate("D, jS M Y @ g:ia", $this->ban->ban_date2timestamp($time));
                            $period = "{$period} ({$friendly_time})";
                        }
                        $length_list[$time] = $period;
                    }
                    $form_container->output_row($this->lang->ban_time, "", $form->generate_select_box('bantime', $length_list, $this->bb->input['bantime'], ['id' => 'bantime']), 'bantime');
                    echo $form_container->end();

                    $buttons[] = $form->generate_submit_button($this->lang->ban_users);
                    echo $form->output_submit_wrapper($buttons);
                    echo $form->end();
                    $this->page->output_footer();
                    break;
                case 'multidelete':
                    if ($this->bb->input['no']) {
                        return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url); // User clicked on 'No
                    } else {
                        if ($this->bb->input['processed'] == 1) {
                            // Set up user handler.
                            $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'delete');

                            // Delete users
                            $deleted = $userhandler->delete_user($selected);
                            $to_be_deleted = $deleted['deleted_users']; // Get the correct number of deleted users

                            // Update forum stats, remove the cookie and redirect the user
                            $this->bb->my_unsetcookie("inlinemod_useracp");
                            $this->bb->input['action'] = "inline_delete";
                            $this->bblogger->log_admin_action($to_be_deleted);

                            $this->lang->users_deleted = $this->lang->sprintf($this->lang->users_deleted, $to_be_deleted);

                            $this->cache->update_awaitingactivation();

                            $this->session->flash_message($this->lang->users_deleted, 'success');
                            return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                        }

                        $to_be_deleted = count($selected);
                        $this->lang->confirm_multidelete = $this->lang->sprintf($this->lang->confirm_multidelete, $this->parser->formatNumber($to_be_deleted));
                        $this->page->output_confirm_action($this->bb->admin_url . "/users?action=inline_edit&amp;inline_action=multidelete&amp;my_post_key={$this->bb->post_code}&amp;processed=1", $this->lang->confirm_multidelete);
                    }
                    break;
                case 'multiprune':
                    if ($this->bb->input['processed'] == 1) {
                        if (($this->bb->input['day'] || $this->bb->input['month'] || $this->bb->input['year']) && $this->bb->input['set']) {
                            $errors[] = $this->lang->multi_selected_dates;
                        }

                        $day = $this->bb->getInput('day', 0);
                        $month = $this->bb->getInput('month', 0);
                        $year = $this->bb->getInput('year', 0);

                        // Selected a date - check if the date the user entered is valid
                        if ($this->bb->input['day'] || $this->bb->input['month'] || $this->bb->input['year']) {
                            // Is the date sort of valid?
                            if ($day < 1 || $day > 31 || $month < 1 || $month > 12 || ($month == 2 && $day > 29)) {
                                $errors[] = $this->lang->incorrect_date;
                            }

                            // Check the month
                            $months = get_bdays($year);
                            if ($day > $months[$month - 1]) {
                                $errors[] = $this->lang->incorrect_date;
                            }

                            // Check the year
                            if ($year != 0 && ($year < (date("Y") - 100)) || $year > date("Y")) {
                                $errors[] = $this->lang->incorrect_date;
                            }

                            if (!$errors) {
                                // No errors, so let's continue and set the date to delete from
                                $date = mktime(date('H'), date('i'), date('s'), $month, $day, $year); // Generate a unix time stamp
                            }
                        } elseif ($this->bb->input['set'] > 0) {
                            // Set options
                            // For this purpose, 1 month = 31 days
                            $base_time = 24 * 60 * 60;

                            switch ($this->bb->input['set']) {
                                case '1':
                                    $threshold = $base_time * 31; // 1 month = 31 days, in the standard terms
                                    break;
                                case '2':
                                    $threshold = $base_time * 93; // 3 months = 31 days * 3
                                    break;
                                case '3':
                                    $threshold = $base_time * 183; // 6 months = 365 days / 2
                                    break;
                                case '4':
                                    $threshold = $base_time * 365; // 1 year = 365 days
                                    break;
                                case '5':
                                    $threshold = $base_time * 548; // 18 months = 365 + 183
                                    break;
                                case '6':
                                    $threshold = $base_time * 730; // 2 years = 365 * 2
                                    break;
                            }

                            if (!$threshold) {
                                // An option was entered that isn't in the dropdown box
                                $errors[] = $this->lang->no_set_option;
                            } else {
                                $date = TIME_NOW - $threshold;
                            }
                        } else {
                            $errors[] = $this->lang->no_prune_option;
                        }

                        if (!$errors) {
                            $sql_array = implode(",", $selected);
                            $prune_array = [];
                            $query = $this->db->simple_select("users", "uid", "uid IN (" . $sql_array . ")");
                            while ($user = $this->db->fetch_array($query)) {
                                // Protect Super Admins
                                if ($this->user->is_super_admin($user['uid']) && !$this->user->is_super_admin($this->user->uid)) {
                                    continue;
                                }

                                $return_array = $this->delete_user_posts($user['uid'], $date); // Delete user posts, and grab a list of threads to delete
                                if ($return_array && is_array($return_array)) {
                                    $prune_array = array_merge_recursive($prune_array, $return_array);
                                }
                            }

                            // No posts were found for the user, return error
                            if (!is_array($prune_array) || count($prune_array) == 0) {
                                $this->session->flash_message($this->lang->prune_fail, 'error');
                                return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                            }

                            // We've finished deleting user's posts, so let's delete the threads
                            if (is_array($prune_array['to_delete']) && count($prune_array['to_delete']) > 0) {
                                foreach ($prune_array['to_delete'] as $tid) {
                                    $this->db->delete_query("threads", "tid='$tid'");
                                    $this->db->delete_query("threads", "closed='moved|$tid'");
                                    $this->db->delete_query("threadsubscriptions", "tid='$tid'");
                                    $this->db->delete_query("polls", "tid='$tid'");
                                    $this->db->delete_query("threadsread", "tid='$tid'");
                                    $this->db->delete_query("threadratings", "tid='$tid'");
                                }
                            }

                            // After deleting threads, rebuild the thread counters for the affected threads
                            if (is_array($prune_array['thread_update']) && count($prune_array['thread_update']) > 0) {
                                $sql_array = implode(",", $prune_array['thread_update']);
                                $query = $this->db->simple_select("threads", "tid", "tid IN (" . $sql_array . ")", ['order_by' => 'tid', 'order_dir' => 'asc']);
                                while ($thread = $this->db->fetch_array($query)) {
                                    rebuild_thread_counters($thread['tid']);
                                }
                            }

                            // After updating thread counters, update the affected forum counters
                            if (is_array($prune_array['forum_update']) && count($prune_array['forum_update']) > 0) {
                                $sql_array = implode(",", $prune_array['forum_update']);
                                $query = $this->db->simple_select("forums", "fid", "fid IN (" . $sql_array . ")", ['order_by' => 'fid', 'order_dir' => 'asc']);
                                while ($forum = $this->db->fetch_array($query)) {
                                    // Because we have a recursive array merge, check to see if there isn't a duplicated forum to update
                                    if ($looped_forum == $forum['fid']) {
                                        continue;
                                    }
                                    $looped_forum = $forum['fid'];
                                    (new \RunBB\Helpers\Restorer($this))->rebuild_forum_counters($forum['fid']);
                                }
                            }

                            $this->bblogger->log_admin_action();
                            $this->bb->my_unsetcookie("inlinemod_useracp"); // We've got our users, remove the cookie
                            $this->session->flash_message($this->lang->prune_complete, 'success');
                            return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                        }
                    }

                    echo $this->page->output_header($this->lang->manage_users);
                    echo $this->page->output_nav_tabs($sub_tabs, 'manage_users');

                    // Display a table warning
                    $table = new Table;
                    $this->lang->mass_prune_info = $this->lang->sprintf($this->lang->mass_prune_info, count($selected));
                    $table->construct_cell($this->lang->mass_prune_info);
                    $table->construct_row();
                    echo $table->output($this->lang->important);

                    if ($errors) {
                        echo $this->page->output_inline_error($errors);
                    }

                    // Display the prune options
                    $form = new Form($this->bb, $this->bb->admin_url . '/users', "post");
                    echo $form->getForm();
                    echo $form->generate_hidden_field('action', 'inline_edit');
                    echo $form->generate_hidden_field('inline_action', 'multiprune');
                    echo $form->generate_hidden_field('processed', '1');

                    $form_container = new FormContainer($this, $this->lang->mass_prune_posts);

                    // Generate a list of days (1 - 31)
                    $day_options = [];
                    $day_options[] = "&nbsp;";
                    for ($i = 1; $i <= 31; ++$i) {
                        $day_options[] = $i;
                    }

                    // Generate a list of months (1 - 12)
                    $month_options = [];
                    $month_options[] = "&nbsp;";
                    for ($i = 1; $i <= 12; ++$i) {
                        $string = "month_{$i}";
                        $month_options[] = $this->lang->$string;
                    }
                    $date_box = $form->generate_select_box('day', $day_options, $this->bb->input['day']);
                    $month_box = $form->generate_select_box('month', $month_options, $this->bb->input['month']);
                    $year_box = $form->generate_numeric_field('year', $this->bb->input['year'], ['id' => 'year', 'style' => 'width: 50px;', 'min' => 0]);

                    $prune_select = $date_box . $month_box . $year_box;
                    $form_container->output_row($this->lang->manual_date, "", $prune_select, 'date');

                    // Generate the set date box
                    $set_options = [];
                    $set_options[] = $this->lang->set_an_option;
                    for ($i = 1; $i <= 6; ++$i) {
                        $string = "option_{$i}";
                        $set_options[] = $this->lang->$string;
                    }

                    $form_container->output_row($this->lang->relative_date, "", $this->lang->delete_posts . " " . $form->generate_select_box('set', $set_options, $this->bb->input['set']), 'set');
                    echo $form_container->end();

                    $buttons[] = $form->generate_submit_button($this->lang->prune_posts);
                    echo $form->output_submit_wrapper($buttons);
                    echo $form->end();
                    $this->page->output_footer();
                    break;
                case 'multiusergroup':
                    if ($this->bb->input['processed'] == 1) {
                        // Determine additional usergroups
                        if (is_array($this->bb->input['additionalgroups'])) {
                            foreach ($this->bb->input['additionalgroups'] as $key => $gid) {
                                if ($gid == $this->bb->input['usergroup']) {
                                    unset($this->bb->input['additionalgroups'][$key]);
                                }
                            }

                            $additionalgroups = implode(",", array_map('intval', $this->bb->input['additionalgroups']));
                        } else {
                            $additionalgroups = '';
                        }

                        // Create an update array
                        $update_array = [
                            "usergroup" => $this->bb->getInput('usergroup', 0),
                            "additionalgroups" => $additionalgroups,
                            "displaygroup" => $this->bb->getInput('displaygroup', 0)
                        ];

                        // Do the usergroup update for all those selected
                        // If the a selected user is a super admin, don't update that user
                        foreach ($selected as $user) {
                            if (!$this->user->is_super_admin($user)) {
                                $users_to_update[] = $user;
                            }
                        }

                        $to_update_count = count($users_to_update);
                        if ($to_update_count > 0 && is_array($users_to_update)) {
                            // Update the users in the database
                            $sql = implode(",", $users_to_update);
                            $this->db->update_query("users", $update_array, "uid IN (" . $sql . ")");

                            // Redirect the admin...
                            $this->bb->input['action'] = "inline_usergroup";
                            $this->bblogger->log_admin_action($to_update_count);
                            $this->bb->my_unsetcookie("inlinemod_useracp");
                            $this->session->flash_message($this->lang->success_mass_usergroups, 'success');
                            return $response->withRedirect($this->bb->admin_url . '/users' . $vid_url);
                        } else {
                            // They tried to edit super admins! Uh-oh!
                            $errors[] = $this->lang->no_usergroup_changed;
                        }
                    }

                    echo $this->page->output_header($this->lang->manage_users);
                    echo $this->page->output_nav_tabs($sub_tabs, 'manage_users');

                    // Display a table warning
                    $table = new Table;
                    $this->lang->usergroup_info = $this->lang->sprintf($this->lang->usergroup_info, count($selected));
                    $table->construct_cell($this->lang->usergroup_info);
                    $table->construct_row();
                    echo $table->output($this->lang->important);

                    if ($errors) {
                        echo $this->page->output_inline_error($errors);
                    }

                    // Display the usergroup options
                    $form = new Form($this->bb, $this->bb->admin_url . '/users', "post");
                    echo $form->getForm();
                    echo $form->generate_hidden_field('action', 'inline_edit');
                    echo $form->generate_hidden_field('inline_action', 'multiusergroup');
                    echo $form->generate_hidden_field('processed', '1');

                    $form_container = new FormContainer($this, $this->lang->mass_usergroups);

                    // Usergroups
                    $display_group_options[0] = $this->lang->use_primary_user_group;
                    $options = [];
                    $query = $this->db->simple_select("usergroups", "gid, title", "gid != '1'", ['order_by' => 'title']);
                    while ($usergroup = $this->db->fetch_array($query)) {
                        $options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
                        $display_group_options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
                    }

                    if (!is_array($this->bb->input['additionalgroups'])) {
                        $this->bb->input['additionalgroups'] = explode(',', $this->bb->input['additionalgroups']);
                    }

                    $form_container->output_row($this->lang->primary_user_group, "", $form->generate_select_box('usergroup', $options, $this->bb->input['usergroup'], ['id' => 'usergroup']), 'usergroup');
                    $form_container->output_row($this->lang->additional_user_groups, $this->lang->additional_user_groups_desc, $form->generate_select_box('additionalgroups[]', $options, $this->bb->input['additionalgroups'], ['id' => 'additionalgroups', 'multiple' => true, 'size' => 5]), 'additionalgroups');
                    $form_container->output_row($this->lang->display_user_group, "", $form->generate_select_box('displaygroup', $display_group_options, $this->bb->input['displaygroup'], ['id' => 'displaygroup']), 'displaygroup');

                    echo $form_container->end();

                    $buttons[] = $form->generate_submit_button($this->lang->alter_usergroups);
                    echo $form->output_submit_wrapper($buttons);
                    echo $form->end();
                    $this->page->output_footer();
                    break;
            }
        }

        if (!$this->bb->input['action']) {
            $this->plugins->runHooks("admin_user_users_start");

            $html = $this->page->output_header($this->lang->browse_users);
            $html .= "<script type=\"text/javascript\" src=\"{$this->bb->asset_url}/admin/jscripts/users.js\"></script>";

            $html .= $this->page->output_nav_tabs($sub_tabs, 'browse_users');

            if (isset($this->bb->input['search_id']) && $this->session->admin_session['data']['user_views'][$this->bb->input['search_id']]) {
                $admin_view = $this->session->admin_session['data']['user_views'][$this->bb->input['search_id']];
                unset($admin_view['extra_sql']);
            } else {
                // Showing a specific view
                if (isset($this->bb->input['vid'])) {
                    $query = $this->db->simple_select("adminviews", "*", "vid='" . $this->bb->getInput('vid', 0) . "'");
                    $admin_view = $this->db->fetch_array($query);
                    // View does not exist or this view is private and does not belong to the current user
                    if (!$admin_view['vid'] || ($admin_view['visibility'] == 1 && $admin_view['uid'] != $this->user->uid)) {
                        unset($admin_view);
                    }
                }

                // Don't have a view? Fetch the default
                if (!isset($admin_view)) {
                    $default_view = $this->fetch_default_view('users');
                    if (!$default_view) {
                        $default_view = "0";
                    }
                    $query = $this->db->simple_select("adminviews", "*", "type='user' AND (vid='{$default_view}' OR uid=0)", ["order_by" => "uid", "order_dir" => "desc"]);
                    $admin_view = $this->db->fetch_array($query);
                }
            }

            // Fetch a list of all of the views for this user
            $popup = new PopupMenu('views', $this->lang->views);

            $query = $this->db->simple_select("adminviews", "*", "type='user' AND (visibility=2 OR uid={$this->user->uid})", ["order_by" => "title"]);
            while ($view = $this->db->fetch_array($query)) {
                $popup->add_item(htmlspecialchars_uni($view['title']), $this->bb->admin_url . '/users?vid=' . $view['vid']);
            }
            $popup->add_item("<em>{$this->lang->manage_views}</em>", $this->bb->admin_url . '/users?action=views');
            $admin_view['popup'] = $popup->fetch();

            if (isset($this->bb->input['type'])) {
                $admin_view['view_type'] = $this->bb->input['type'];
            }

            $results = $this->build_users_view($admin_view);

            if (!$results) {
                // If we came from the home page and clicked on the "Activate Users" link, send them back to here
                if ($this->session->admin_session['data']['from'] == "home") {
                    $this->session->flash_message($this->session->admin_session['data']['flash_message2']['message'], $this->session->admin_session['data']['flash_message2']['type']);
                    $this->session->update_admin_session('flash_message2', '');
                    $this->session->update_admin_session('from', '');
                    return $response->withRedirect($this->bb->admin_url . '/users');
                    exit;
                } else {
                    $errors[] = $this->lang->error_no_users_found;
                }
            }

            // If we have any error messages, show them
            if (!empty($errors)) {
                if ($inline != true) {
                    $html .= "<div style=\"display: inline; float: right;\">{$admin_view['popup']}</div><br />\n";
                }
                $html .= $this->page->output_inline_error($errors);
            }

            $html .= $results;

            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($response, '@forum/Admin/Users/Users.html.twig');
        }
    }

    /**
     * @param array $view
     *
     * @return string
     */
    private function build_users_view($view)
    {
        $view_title = '';
        if ($view['title']) {
            if (!isset($view['vid'])) {
                $view['vid'] = 1;
            }
            $title_string = "view_title_{$view['vid']}";

            if ($this->lang->$title_string) {
                $view['title'] = $this->lang->$title_string;
            }

            $view_title .= " (" . htmlspecialchars_uni($view['title']) . ")";
        }

        // Build the URL to this view
        if (!isset($view['url'])) {
            $view['url'] = $this->bb->admin_url . '/users';
        }
        if (!is_array($view['conditions'])) {
            $view['conditions'] = my_unserialize($view['conditions']);
        }
        if (!is_array($view['fields'])) {
            $view['fields'] = my_unserialize($view['fields']);
        }
        if (!is_array($view['custom_profile_fields'])) {
            $view['custom_profile_fields'] = my_unserialize($view['custom_profile_fields']);
        }
        if (isset($this->bb->input['username'])) {
            $view['conditions']['username'] = $this->bb->input['username'];
        }
        if ($view['vid']) {
            $view['url'] .= "?vid={$view['vid']}";
        } else {
            // If this is a custom view we need to save everything ready to pass it on from page to page
            if (!$this->bb->input['search_id']) {
                $search_id = md5(random_str());
                $this->session->admin_session['data']['user_views'][$search_id] = $view;
                $this->session->update_admin_session('user_views', $this->session->admin_session['data']['user_views']);
                $this->bb->input['search_id'] = $search_id;
            }
            $view['url'] .= "&amp;search_id=" . htmlspecialchars_uni($this->bb->input['search_id']);
        }

        if (isset($this->bb->input['username'])) {
            $view['url'] .= "&amp;username=" . urlencode(htmlspecialchars_uni($this->bb->input['username']));
        }

        if (!isset($this->session->admin_session['data']['last_users_view']) ||
            $this->session->admin_session['data']['last_users_view'] != str_replace("&amp;", "&", $view['url'])
        ) {
            $this->session->update_admin_session('last_users_url', str_replace("&amp;", "&", $view['url']));
        }

        if (isset($view['conditions']['referrer'])) {
            $view['url'] .= "&amp;action=referrers&amp;uid=" . htmlspecialchars_uni($view['conditions']['referrer']);
        }

        // Do we not have any views?
        if (empty($view)) {
            return false;
        }

        $table = new Table;

        // Build header for table based view
        if ($view['view_type'] != "card") {
            foreach ($view['fields'] as $field) {
                if (!$this->user_view_fields[$field]) {
                    continue;
                }
                $view_field = $this->user_view_fields[$field];
                $field_options = [];
                if ($view_field['width']) {
                    $field_options['width'] = $view_field['width'];
                }
                if ($view_field['align']) {
                    $field_options['class'] = "align_" . $view_field['align'];
                }
                $table->construct_header($view_field['title'], $field_options);
            }
            $table->construct_header("<input type=\"checkbox\" name=\"allbox\" onclick=\"inlineModeration.checkAll(this);\" />"); // Create a header for the "select" boxes
        }

        $search_sql = '1=1';

        // Build the search SQL for users

        // List of valid LIKE search fields
        $user_like_fields = ['username', 'email', 'website', 'icq', 'aim', 'yahoo', 'skype', 'google', 'signature', 'usertitle'];
        foreach ($user_like_fields as $search_field) {
            if (!empty($view['conditions'][$search_field]) && !$view['conditions'][$search_field . '_blank']) {
                $search_sql .= " AND u.{$search_field} LIKE '%" . $this->db->escape_string_like($view['conditions'][$search_field]) . "%'";
            } elseif (!empty($view['conditions'][$search_field . '_blank'])) {
                $search_sql .= " AND u.{$search_field} != ''";
            }
        }

        // EXACT matching fields
        $user_exact_fields = ["referrer"];
        foreach ($user_exact_fields as $search_field) {
            if (!empty($view['conditions'][$search_field])) {
                $search_sql .= " AND u.{$search_field}='" . $this->db->escape_string($view['conditions'][$search_field]) . "'";
            }
        }

        // LESS THAN or GREATER THAN
        $direction_fields = ["postnum", "threadnum"];
        foreach ($direction_fields as $search_field) {
            $direction_field = $search_field . "_dir";
            if (isset($view['conditions'][$search_field]) && ($view['conditions'][$search_field] || $view['conditions'][$search_field] === '0') && $view['conditions'][$direction_field]) {
                switch ($view['conditions'][$direction_field]) {
                    case "greater_than":
                        $direction = ">";
                        break;
                    case "less_than":
                        $direction = "<";
                        break;
                    default:
                        $direction = "=";
                }
                $search_sql .= " AND u.{$search_field}{$direction}'" . $this->db->escape_string($view['conditions'][$search_field]) . "'";
            }
        }

        // Registration searching
        $reg_fields = ["regdate"];
        foreach ($reg_fields as $search_field) {
            if (!empty($view['conditions'][$search_field]) && (int)$view['conditions'][$search_field]) {
                $threshold = TIME_NOW - ((int)$view['conditions'][$search_field] * 24 * 60 * 60);

                $search_sql .= " AND u.{$search_field} >= '{$threshold}'";
            }
        }

        // IP searching
        $ip_fields = ["regip", "lastip"];
        foreach ($ip_fields as $search_field) {
            if (!empty($view['conditions'][$search_field])) {
                $ip_range = fetch_ip_range($view['conditions'][$search_field]);
                if (!is_array($ip_range)) {
                    $ip_sql = "{$search_field}=" . $ip_range;
                } else {
                    $ip_sql = "{$search_field} BETWEEN " . $ip_range[0] . " AND " . $ip_range[1];
                }
                $search_sql .= " AND {$ip_sql}";
            }
        }

        // Post IP searching
        if (!empty($view['conditions']['postip'])) {
            $ip_range = fetch_ip_range($view['conditions']['postip']);
            if (!is_array($ip_range)) {
                $ip_sql = "ipaddress=" . $ip_range;
            } else {
                $ip_sql = "ipaddress BETWEEN " . $ip_range[0] . " AND " . $ip_range[1];
            }
            $ip_uids = [0];
            $query = $this->db->simple_select("posts", "uid", $ip_sql);
            while ($uid = $this->db->fetch_field($query, "uid")) {
                $ip_uids[] = $uid;
            }
            $search_sql .= " AND u.uid IN(" . implode(',', $ip_uids) . ")";
            unset($ip_uids);
        }

        // Custom Profile Field searching
        if ($view['custom_profile_fields']) {
            $userfield_sql = '1=1';
            foreach ($view['custom_profile_fields'] as $column => $input) {
                if (is_array($input)) {
                    foreach ($input as $value => $text) {
                        if ($value == $column) {
                            $value = $text;
                        }

                        if ($value == $this->lang->na) {
                            continue;
                        }

                        if (strpos($column, '_blank') !== false) {
                            $column = str_replace('_blank', '', $column);
                            $userfield_sql .= ' AND ' . $this->db->escape_string($column) . " != ''";
                        } else {
                            $userfield_sql .= ' AND ' . $this->db->escape_string($column) . "='" . $this->db->escape_string($value) . "'";
                        }
                    }
                } elseif (!empty($input)) {
                    if ($input == $this->lang->na) {
                        continue;
                    }

                    if (strpos($column, '_blank') !== false) {
                        $column = str_replace('_blank', '', $column);
                        $userfield_sql .= ' AND ' . $this->db->escape_string($column) . " != ''";
                    } else {
                        $userfield_sql .= ' AND ' . $this->db->escape_string($column) . " LIKE '%" . $this->db->escape_string_like($input) . "%'";
                    }
                }
            }

            if ($userfield_sql != '1=1') {
                $userfield_uids = [0];
                $query = $this->db->simple_select("userfields", "ufid", $userfield_sql);
                while ($userfield = $this->db->fetch_array($query)) {
                    $userfield_uids[] = $userfield['ufid'];
                }
                $search_sql .= " AND u.uid IN(" . implode(',', $userfield_uids) . ")";
                unset($userfield_uids);
            }
        }

        // Usergroup based searching
        if (isset($view['conditions']['usergroup'])) {
            if (!is_array($view['conditions']['usergroup'])) {
                $view['conditions']['usergroup'] = [$view['conditions']['usergroup']];
            }

            foreach ($view['conditions']['usergroup'] as $usergroup) {
                $usergroup = (int)$usergroup;

                if (!$usergroup) {
                    continue;
                }

                $additional_sql = '';

                switch ($this->db->type) {
                    case "pgsql":
                    case "sqlite":
                        $additional_sql .= " OR ','||additionalgroups||',' LIKE '%,{$usergroup},%'";
                        break;
                    default:
                        $additional_sql .= "OR CONCAT(',',additionalgroups,',') LIKE '%,{$usergroup},%'";
                }
            }

            $search_sql .= " AND (u.usergroup IN (" . implode(",", array_map('intval', $view['conditions']['usergroup'])) . ") {$additional_sql})";
        }

        // COPPA users only?
        if (isset($view['conditions']['coppa'])) {
            $search_sql .= " AND u.coppauser=1 AND u.usergroup=5";
        }

        // Extra SQL?
        if (isset($view['extra_sql'])) {
            $search_sql .= $view['extra_sql'];
        }

        // Lets fetch out how many results we have
        $query = $this->db->query("
		SELECT COUNT(u.uid) AS num_results
		FROM " . TABLE_PREFIX . "users u
		WHERE {$search_sql}
	");
        $num_results = $this->db->fetch_field($query, "num_results");

        // No matching results then return false
        if (!$num_results) {
            return false;
        } // Generate the list of results
        else {
            if (!$view['perpage']) {
                $view['perpage'] = 20;
            }
            $view['perpage'] = (int)$view['perpage'];

            // Establish which page we're viewing and the starting index for querying
            if (!isset($this->bb->input['page'])) {
                $this->bb->input['page'] = 1;
            } else {
                $this->bb->input['page'] = $this->bb->getInput('page', 0);
            }

            if ($this->bb->input['page']) {
                $start = ($this->bb->input['page'] - 1) * $view['perpage'];
            } else {
                $start = 0;
                $this->bb->input['page'] = 1;
            }

            $from_bit = "";
            if (isset($this->bb->input['from']) && $this->bb->input['from'] == "home") {
                $from_bit = "&amp;from=home";
            }

            switch ($view['sortby']) {
                case "regdate":
                case "lastactive":
                case "postnum":
                case "reputation":
                    $view['sortby'] = $this->db->escape_string($view['sortby']);
                    break;
                case "numposts":
                    $view['sortby'] = "postnum";
                    break;
                case "numthreads":
                    $view['sortby'] = "threadnum";
                    break;
                case "warninglevel":
                    $view['sortby'] = "warningpoints";
                    break;
                default:
                    $view['sortby'] = "username";
            }

            if ($view['sortorder'] != "desc") {
                $view['sortorder'] = "asc";
            }

            $usergroups = $this->cache->read("usergroups");

            // Fetch matching users
            $query = $this->db->query("
			SELECT u.*
			FROM " . TABLE_PREFIX . "users u
			WHERE {$search_sql}
			ORDER BY {$view['sortby']} {$view['sortorder']}
			LIMIT {$start}, {$view['perpage']}
		");
            $users = '';
            while ($user = $this->db->fetch_array($query)) {
                $comma = $groups_list = '';
                $user['view']['username'] = "<a href=\"".$this->bb->admin_url."/users?action=edit&amp;uid={$user['uid']}\">" .
                    $this->user->format_name($user['username'], $user['usergroup'], $user['displaygroup']) . "</a>";
                $user['view']['usergroup'] = htmlspecialchars_uni($usergroups[$user['usergroup']]['title']);
                if ($user['additionalgroups']) {
                    $additional_groups = explode(",", $user['additionalgroups']);

                    foreach ($additional_groups as $group) {
                        $groups_list .= $comma . htmlspecialchars_uni($usergroups[$group]['title']);
                        $comma = $this->lang->comma;
                    }
                }
                if (!$groups_list) {
                    $groups_list = $this->lang->none;
                }
                $user['view']['additionalgroups'] = "<small>{$groups_list}</small>";
                $user['view']['email'] = "<a href=\"mailto:" . htmlspecialchars_uni($user['email']) . "\">" . htmlspecialchars_uni($user['email']) . "</a>";
                $user['view']['regdate'] = $this->time->formatDate('relative', $user['regdate']);
                $user['view']['lastactive'] = $this->time->formatDate('relative', $user['lastactive']);

                // Build popup menu
                $popup = new PopupMenu("user_{$user['uid']}", $this->lang->options);
                $popup->add_item($this->lang->view_profile, get_profile_link($user['uid']));
                $popup->add_item($this->lang->edit_profile_and_settings, $this->bb->admin_url.'/users?action=edit&uid='.$user['uid']);

                // Banning options... is this user banned?
                if ($usergroups[$user['usergroup']]['isbannedgroup'] == 1) {
                    // Yes, so do we want to edit the ban or pardon his crime?
                    $popup->add_item($this->lang->edit_ban, $this->bb->admin_url . "/users/banning?uid={$user['uid']}#username");
                    $popup->add_item($this->lang->lift_ban, $this->bb->admin_url . "/users/banning?action=lift&uid={$user['uid']}&my_post_key={$this->bb->post_code}");
                } else {
                    // Not banned... but soon maybe!
                    $popup->add_item($this->lang->ban_user, $this->bb->admin_url . "/users/banning?uid={$user['uid']}#username");
                }

                if ($user['usergroup'] == 5) {
                    if ($user['coppauser']) {
                        $popup->add_item($this->lang->approve_coppa_user, $this->bb->admin_url . "/users?action=activate_user&amp;uid={$user['uid']}&amp;my_post_key={$this->bb->post_code}{$from_bit}");
                    } else {
                        $popup->add_item($this->lang->approve_user, $this->bb->admin_url . "/users?action=activate_user&amp;uid={$user['uid']}&amp;my_post_key={$this->bb->post_code}{$from_bit}");
                    }
                }

                $popup->add_item($this->lang->delete_user, $this->bb->admin_url . "/users?action=delete&amp;uid={$user['uid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->user_deletion_confirmation}')");
                $popup->add_item($this->lang->show_referred_users, $this->bb->admin_url .'/users?action=referrers&uid='.$user['uid']);
                $popup->add_item($this->lang->show_ip_addresses, $this->bb->admin_url . '/users?action=ipaddresses&uid='.$user['uid']);
                $popup->add_item($this->lang->show_attachments, $this->bb->admin_url . '/forum/attachments?results=1&username=' . urlencode(htmlspecialchars_uni($user['username'])));
                $user['view']['controls'] = $popup->fetch();

                // Fetch the reputation for this user
                if ($usergroups[$user['usergroup']]['usereputationsystem'] == 1 && $this->bb->settings['enablereputation'] == 1) {
                    $user['view']['reputation'] = $this->user->get_reputation($user['reputation']);
                } else {
                    $reputation = "-";
                }

                if ($this->bb->settings['enablewarningsystem'] != 0 && $usergroups[$user['usergroup']]['canreceivewarnings'] != 0) {
                    if ($this->bb->settings['maxwarningpoints'] < 1) {
                        $this->bb->settings['maxwarningpoints'] = 10;
                    }

                    $warning_level = round($user['warningpoints'] / $this->bb->settings['maxwarningpoints'] * 100);
                    if ($warning_level > 100) {
                        $warning_level = 100;
                    }
                    $user['view']['warninglevel'] = get_colored_warning_level($warning_level);
                }

                if ($user['avatar'] && my_substr($user['avatar'], 0, 7) !== 'http://' && my_substr($user['avatar'], 0, 8) !== 'https://') {
                    $user['avatar'] = $this->bb->asset_url . '/' . $user['avatar'];
                }
                if ($view['view_type'] == "card") {
                    $scaled_avatar = $this->fetch_scaled_avatar($user, 80, 80);
                } else {
                    $scaled_avatar = $this->fetch_scaled_avatar($user, 34, 34);
                }
                if (!$user['avatar']) {
                    $user['avatar'] = $this->bb->asset_url . '/' . $this->bb->settings['useravatar'];
                }
                $user['view']['avatar'] = "<img src=\"" . htmlspecialchars_uni($user['avatar']) . "\" alt=\"\" width=\"{$scaled_avatar['width']}\" height=\"{$scaled_avatar['height']}\" />";

                if ($view['view_type'] == "card") {
                    $users .= $this->build_user_view_card($user, $view, $i);
                } else {
                    $this->build_user_view_table($user, $view, $table);
                }
            }

            // If card view, we need to output the results
            if ($view['view_type'] == "card") {
                $table->construct_cell($users);
                $table->construct_row();
            }
        }

        if (!isset($view['table_id'])) {
            $view['table_id'] = "users_list";
        }

        $switch_view = "<div class=\"float_right\">";
        $switch_url = $view['url'];
        if ($this->bb->input['page'] > 0) {
            $switch_url .= "&amp;page=" . $this->bb->getInput('page', 0);
        }
        if ($view['view_type'] != "card") {
            $switch_view .= "<strong>{$this->lang->table_view}</strong> | <a href=\"{$switch_url}&amp;type=card\" style=\"font-weight: normal;\">{$this->lang->card_view}</a>";
        } else {
            $switch_view .= "<a href=\"{$switch_url}&amp;type=table\" style=\"font-weight: normal;\">{$this->lang->table_view}</a> | <strong>{$this->lang->card_view}</strong>";
        }
        $switch_view .= "</div>";

        // Do we need to construct the pagination?
        if ($num_results > $view['perpage']) {
            $pagination = $this->adm->draw_admin_pagination($this->bb->input['page'], $view['perpage'], $num_results, $view['url'] . "&amp;type={$view['view_type']}");
            $search_class = 'float_right';
            $search_style = '';
        } else {
            $search_class = '';
            $search_style = 'text-align: right;';
        }

        $search_action = $view['url'];
        // stop &username= in the query string
        if ($view_upos = strpos($search_action, '&amp;username=')) {
            $search_action = substr($search_action, 0, $view_upos);
        }
        $search_action = str_replace("&amp;", "&", $search_action);
        $search = new Form($this->bb, htmlspecialchars_uni($search_action), 'post', 'search_form', 0, '', true);

        $built_view = $search->getForm();
        $built_view .= "<div class=\"{$search_class}\" style=\"padding-bottom: 3px; margin-top: -9px; {$search_style}\">";
        $built_view .= $search->generate_hidden_field('action', 'search') . "\n";
        if (isset($view['conditions']['username'])) {
            $default_class = '';
            $value = $view['conditions']['username'];
        } else {
            $default_class = "search_default";
            $value = $this->lang->search_for_user;
        }
        $built_view .= $search->generate_text_box('username', $value, ['id' => 'search_keywords', 'class' => "{$default_class} field150 field_small"]) . "\n";
        $built_view .= "<input type=\"submit\" class=\"search_button\" value=\"{$this->lang->search}\" />\n";
        if (isset($view['popup'])) {
            $built_view .= " <div style=\"display: inline\">{$view['popup']}</div>\n";
        }
        $built_view .= "<script type=\"text/javascript\">
		var form = $(\"#search_form\");
		form.submit(function() {
			var search = $('#search_keywords');
			if(search.val() == '' || search.val() == '" . addcslashes($this->lang->search_for_user, "'") . "')
			{
				search.focus();
				return false;
			}
		});

		var search = $(\"#search_keywords\");
		search.focus(function()
		{
			var searched_focus = $(this);
			if(searched_focus.val() == '" . addcslashes($this->lang->search_for_user, "'") . "')
			{
				searched_focus.removeClass(\"search_default\");
				searched_focus.val(\"\");
			}
		});

		search.blur(function()
		{
			var searched_blur = $(this);
			if(searched_blur.val() == \"\")
			{
				searched_blur.addClass('search_default');
				searched_blur.val('" . addcslashes($this->lang->search_for_user, "'") . "');
			}
		});

		// fix the styling used if we have a different default value
		if(search.val() != '" . addcslashes($this->lang->search_for_user, "'") . "')
		{
			$(search).removeClass('search_default');
		}
		</script>\n";
        $built_view .= "</div>\n";

        // Autocompletion for usernames
        // TODO Select2

        $built_view .= $search->end();

        if (isset($pagination)) {
            $built_view .= $pagination;
        }
        if ($view['view_type'] != "card") {
            $checkbox = '';
        } else {
            $checkbox = "<input type=\"checkbox\" name=\"allbox\" onclick=\"inlineModeration.checkAll(this)\" /> ";
        }
        $built_view .= $table->construct_html("{$switch_view}<div>{$checkbox}{$this->lang->users}{$view_title}</div>", 1, "", $view['table_id']);
        if (isset($pagination)) {
            $built_view .= $pagination;
        }

        $built_view .= '
<script type="text/javascript" src="' . $this->bb->asset_url . '/jscripts/inline_moderation.js?ver=1800"></script>
<form action="' . $this->bb->admin_url . '/users" method="post">
<input type="hidden" name="my_post_key" value="' . $this->bb->post_code . '" />
<input type="hidden" name="action" value="inline_edit" />
<div class="float_right"><span class="smalltext"><strong>' . $this->lang->inline_edit . '</strong></span>
<select name="inline_action">
	<option value="multiactivate">' . $this->lang->inline_activate . '</option>
	<option value="multiban">' . $this->lang->inline_ban . '</option>
	<option value="multiusergroup">' . $this->lang->inline_usergroup . '</option>
	<option value="multidelete">' . $this->lang->inline_delete . '</option>
	<option value="multiprune">' . $this->lang->inline_prune . '</option>
</select>
<input type="submit" class="submit_button inline_element" name="go" value="' . $this->lang->go . ' (0)" id="inline_go" />&nbsp;
<input type="button" onclick="javascript:inlineModeration.clearChecked();" value="' . $this->lang->clear . '" class="submit_button inline_element" />
</div>
</form>
<br style="clear: both;" />
<script type="text/javascript">
<!--
	var go_text = "' . $this->lang->go . '";
	var all_text = "1";
	var inlineType = "user";
	var inlineId = "acp";
// -->
</script>';

        return $built_view;
    }

    /**
     * @param array $user
     * @param array $view
     * @param int $i
     *
     * @return string
     */
    private function build_user_view_card($user, $view, &$i)
    {
        ++$i;
        if ($i == 3) {
            $i = 1;
        }

        // Loop through fields user wants to show
        foreach ($view['fields'] as $field) {
            if (!$this->user_view_fields[$field]) {
                continue;
            }

            $view_field = $this->user_view_fields[$field];

            // Special conditions for avatar
            if ($field == "avatar") {
                $avatar = $user['view']['avatar'];
            } elseif ($field == "controls") {
                $controls = $user['view']['controls'];
            } // Otherwise, just user data
            elseif ($field != "username") {
                if (isset($user['view'][$field])) {
                    $value = $user['view'][$field];
                } else {
                    $value = $user[$field];
                }

                if ($field == "postnum") {
                    $value = $this->parser->formatNumber($value);
                }

                $user_details[] = "<strong>{$view_field['title']}:</strong> {$value}";
            }
        }
        // Floated to the left or right?
        if ($i == 1) {
            $float = 'left';
        } else {
            $float = 'right';
        }

        // And build the final card
        $card = "<fieldset id=\"uid_{$user['uid']}\" style=\"width: 48%; float: {$float};\" class=\"form-inline\">\n";
        $card .= "<span><legend><input type=\"checkbox\" class=\"checkbox\" name=\"inlinemod_{$user['uid']}\" id=\"inlinemod_{$user['uid']}\" value=\"1\" onclick=\"$('#uid_{$user['uid']}').toggleClass('inline_selected');\" /> {$user['view']['username']}</legend></span>\n";
        if ($avatar) {
            $card .= "<div class=\"user_avatar\">{$avatar}</div>\n";
        }
        if ($user_details) {
            $card .= "<div class=\"user_details\">" . implode("<br />", $user_details) . "</div>\n";
        }
        if ($controls) {
            $card .= "<div class=\"float_right\" style=\"padding: 4px;\">{$controls}</div>\n";
        }
        $card .= '</fieldset>';
        return $card;
    }

    /**
     * @param array $user
     * @param array $view
     * @param DefaultTable $table
     */
    private function build_user_view_table($user, $view, &$table)
    {
        foreach ($view['fields'] as $field) {
            if (!$this->user_view_fields[$field]) {
                continue;
            }
            $view_field = $this->user_view_fields[$field];
            $field_options = [];
            if ($view_field['align']) {
                $field_options['class'] = "align_" . $view_field['align'];
            }
            if (isset($user['view'][$field])) {
                $value = $user['view'][$field];
            } else {
                $value = $user[$field];
            }

            if ($field == "postnum") {
                $value = $this->parser->formatNumber($user[$field]);
            }
            $table->construct_cell($value, $field_options);
        }

        $table->construct_cell("<input type=\"checkbox\" class=\"checkbox\" name=\"inlinemod_{$user['uid']}\" id=\"inlinemod_{$user['uid']}\" value=\"1\" onclick=\"$('#uid_{$user['uid']}').toggleClass('inline_selected');\" />");

        $table->construct_row();
    }

    /**
     * @param array $user
     * @param int $max_width
     * @param int $max_height
     *
     * @return array
     */
    private function fetch_scaled_avatar($user, $max_width = 80, $max_height = 80)
    {
        $scaled_dimensions = [
            'width' => $max_width,
            'height' => $max_height,
        ];

        if ($user['avatar']) {
            if ($user['avatardimensions']) {
                $img = new Image();
                list($width, $height) = explode('|', $user['avatardimensions']);
                $scaled_dimensions = $img->scale_image($width, $height, $max_width, $max_height);
            }
        }

        return ['width' => $scaled_dimensions['width'], 'height' => $scaled_dimensions['height']];
    }

    private function output_custom_profile_fields($fields, $values, &$form_container, &$form, $search = false)
    {
        if (!is_array($fields)) {
            return;
        }
        foreach ($fields as $profile_field) {
            $profile_field['name'] = htmlspecialchars_uni($profile_field['name']);
            $profile_field['description'] = htmlspecialchars_uni($profile_field['description']);
            if (strpos($profile_field['type'], "\n")) {// проверим наличие разделителя
                list($type, $options) = explode("\n", $profile_field['type'], 2);
                $type = trim($type);
            } else {
                $type = $profile_field['type'];
            }
            $field_name = "fid{$profile_field['fid']}";

            switch ($type) {
                case "multiselect":
                    if (isset($values[$field_name]) && !is_array($values[$field_name])) {
                        $user_options = explode("\n", $values[$field_name]);
                    } else {
                        $user_options = $values[$field_name];
                    }

                    $selected_options = [];
                    foreach ($user_options as $val) {
                        $selected_options[$val] = htmlspecialchars_uni($val);
                    }

                    $select_options = explode("\n", $options);
                    $options = [];
                    if ($search == true) {
                        $select_options[''] = $this->lang->na;
                    }

                    foreach ($select_options as $val) {
                        $val = htmlspecialchars_uni(trim($val));
                        $options[$val] = $val;
                    }
                    if (!$profile_field['length']) {
                        $profile_field['length'] = 3;
                    }
                    $code = $form->generate_select_box("profile_fields[{$field_name}][]", $options, $selected_options, ['id' => "profile_field_{$field_name}", 'multiple' => true, 'size' => $profile_field['length']]);
                    break;
                case "select":
                    $select_options = [];
                    if ($search == true) {
                        $select_options[''] = $this->lang->na;
                    }
                    $select_options += explode("\n", $options);
                    $options = [];
                    foreach ($select_options as $val) {
                        $val = htmlspecialchars_uni(trim($val));
                        $options[$val] = $val;
                    }
                    if (!$profile_field['length']) {
                        $profile_field['length'] = 1;
                    }
                    if ($search == true) {
                        $code = $form->generate_select_box("profile_fields[{$field_name}][{$field_name}]", $options, htmlspecialchars_uni($values[$field_name]), ['id' => "profile_field_{$field_name}", 'size' => $profile_field['length']]);
                    } else {
                        $code = $form->generate_select_box("profile_fields[{$field_name}]", $options, htmlspecialchars_uni($values[$field_name]), ['id' => "profile_field_{$field_name}", 'size' => $profile_field['length']]);
                    }
                    break;
                case "radio":
                    $radio_options = [];
                    if ($search == true) {
                        $radio_options[''] = $this->lang->na;
                    }
                    $radio_options += explode("\n", $options);
                    $code = '';
                    foreach ($radio_options as $val) {
                        $val = trim($val);
                        $code .= $form->generate_radio_button("profile_fields[{$field_name}]", $val, htmlspecialchars_uni($val), ['id' => "profile_field_{$field_name}", 'checked' => ($val == $values[$field_name] ? true : false)]) . "<br />";
                    }
                    break;
                case "checkbox":
                    if (!is_array($values[$field_name])) {
                        $user_options = explode("\n", $values[$field_name]);
                    } else {
                        $user_options = $values[$field_name];
                    }
                    foreach ($user_options as $val) {
                        $selected_options[$val] = $val;
                    }
                    $select_options = [];
                    if ($search == true) {
                        $select_options[''] = $this->lang->na;
                    }
                    $select_options += explode("\n", $options);
                    $code = '';
                    foreach ($select_options as $val) {
                        $val = trim($val);
                        $code .= $form->generate_check_box("profile_fields[{$field_name}][]", $val, htmlspecialchars_uni($val), ['id' => "profile_field_{$field_name}", 'checked' => ($val == $selected_options[$val] ? true : false)]) . "<br />";
                    }
                    break;
                case "textarea":
                    $extra = '';
                    if (isset($this->bb->input['action']) && $this->bb->input['action'] == "search") {
                        $extra = " {$this->lang->or} " . $form->generate_check_box("profile_fields[{$field_name}_blank]", 1, $this->lang->is_not_blank, ['id' => "{$field_name}_blank", 'checked' => $values[$field_name . '_blank']]);
                    }

                    $code = $form->generate_text_area("profile_fields[{$field_name}]", $values[$field_name], ['id' => "profile_field_{$field_name}", 'rows' => 6, 'cols' => 50]) . $extra;
                    break;
                default:
                    $extra = '';
                    if (isset($this->bb->input['action']) && $this->bb->input['action'] == "search") {
                        $extra = " {$this->lang->or} " . $form->generate_check_box("profile_fields[{$field_name}_blank]", 1, $this->lang->is_not_blank, ['id' => "{$field_name}_blank", 'checked' => $values[$field_name . '_blank']]);
                    }

                    $code = $form->generate_text_box("profile_fields[{$field_name}]", $values[$field_name], ['id' => "profile_field_{$field_name}", 'maxlength' => $profile_field['maxlength'], 'length' => $profile_field['length']]) . $extra;
                    break;
            }

            $form_container->output_row($profile_field['name'], $profile_field['description'], $code, '', ['id' => "profile_field_{$field_name}"]);
            $code = $user_options = $selected_options = $radio_options = $val = $options = '';
        }
    }

    private function user_search_conditions($input = [], &$form)
    {
        if (!$input) {
            $input = $this->bb->input;
        }
        if (isset($input['conditions']) && !is_array($input['conditions'])) {
            $input['conditions'] = my_unserialize($input['conditions']);
        } else {
            $input['conditions'] = null;
        }

        if (isset($input['profile_fields']) && !is_array($input['profile_fields'])) {
            $input['profile_fields'] = my_unserialize($input['profile_fields']);
        } else {
            $input['profile_fields'] = [];
        }

        if (isset($input['fields']) && !is_array($input['fields'])) {
            $input['fields'] = my_unserialize($input['fields']);
        } else {
            $input['fields'] = [];
        }

        $form_container = new FormContainer($this, $this->lang->find_users_where);
        $form_container->output_row($this->lang->username_contains, "", $form->generate_text_box('conditions[username]', $input['conditions']['username'], ['id' => 'username']), 'username');
        $form_container->output_row($this->lang->email_address_contains, "", $form->generate_text_box('conditions[email]', $input['conditions']['email'], ['id' => 'email']), 'email');

        $options = [];
        $query = $this->db->simple_select("usergroups", "gid, title", "gid != '1'", ['order_by' => 'title']);
        while ($usergroup = $this->db->fetch_array($query)) {
            $options[$usergroup['gid']] = htmlspecialchars_uni($usergroup['title']);
        }

        $form_container->output_row($this->lang->is_member_of_groups, $this->lang->additional_user_groups_desc, $form->generate_select_box('conditions[usergroup][]', $options, $input['conditions']['usergroup'], ['id' => 'usergroups', 'multiple' => true, 'size' => 5]), 'usergroups');

        $form_container->output_row($this->lang->website_contains, '', $form->generate_text_box('conditions[website]', $input['conditions']['website'], ['id' => 'website']) . " {$this->lang->or} " . $form->generate_check_box('conditions[website_blank]', 1, $this->lang->is_not_blank, ['id' => 'website_blank', 'checked' => $input['conditions']['website_blank']]), 'website');
        $form_container->output_row($this->lang->icq_number_contains, '', $form->generate_text_box('conditions[icq]', $input['conditions']['icq'], ['id' => 'icq']) . " {$this->lang->or} " . $form->generate_check_box('conditions[icq_blank]', 1, $this->lang->is_not_blank, ['id' => 'icq_blank', 'checked' => $input['conditions']['icq_blank']]), 'icq');
        $form_container->output_row($this->lang->aim_handle_contains, '', $form->generate_text_box('conditions[aim]', $input['conditions']['aim'], ['id' => 'aim']) . " {$this->lang->or} " . $form->generate_check_box('conditions[aim_blank]', 1, $this->lang->is_not_blank, ['id' => 'aim_blank', 'checked' => $input['conditions']['aim_blank']]), 'aim');
        $form_container->output_row($this->lang->yahoo_contains, '', $form->generate_text_box('conditions[yahoo]', $input['conditions']['yahoo'], ['id' => 'yahoo']) . " {$this->lang->or} " . $form->generate_check_box('conditions[yahoo_blank]', 1, $this->lang->is_not_blank, ['id' => 'yahoo_blank', 'checked' => $input['conditions']['yahoo_blank']]), 'yahoo');
        $form_container->output_row($this->lang->skype_contains, '', $form->generate_text_box('conditions[skype]', $input['conditions']['skype'], ['id' => 'skype']) . " {$this->lang->or} " . $form->generate_check_box('conditions[skype_blank]', 1, $this->lang->is_not_blank, ['id' => 'skype_blank', 'checked' => $input['conditions']['skype_blank']]), 'skype');
        $form_container->output_row($this->lang->google_contains, '', $form->generate_text_box('conditions[google]', $input['conditions']['google'], ['id' => 'google']) . " {$this->lang->or} " . $form->generate_check_box('conditions[google_blank]', 1, $this->lang->is_not_blank, ['id' => 'google_blank', 'checked' => $input['conditions']['google_blank']]), 'google');
        $form_container->output_row($this->lang->signature_contains, '', $form->generate_text_box('conditions[signature]', $input['conditions']['signature'], ['id' => 'signature']) . " {$this->lang->or} " . $form->generate_check_box('conditions[signature_blank]', 1, $this->lang->is_not_blank, ['id' => 'signature_blank', 'checked' => $input['conditions']['signature_blank']]), 'signature');
        $form_container->output_row($this->lang->user_title_contains, '', $form->generate_text_box('conditions[usertitle]', $input['conditions']['usertitle'], ['id' => 'usertitle']) . " {$this->lang->or} " . $form->generate_check_box('conditions[usertitle_blank]', 1, $this->lang->is_not_blank, ['id' => 'usertitle_blank', 'checked' => $input['conditions']['usertitle_blank']]), 'usertitle');
        $greater_options = [
            'greater_than' => $this->lang->greater_than,
            'is_exactly' => $this->lang->is_exactly,
            'less_than' => $this->lang->less_than
        ];
        $form_container->output_row($this->lang->post_count_is, "", $form->generate_select_box('conditions[postnum_dir]', $greater_options, $input['conditions']['postnum_dir'], ['id' => 'numposts_dir']) . " " . $form->generate_text_box('conditions[postnum]', $input['conditions']['postnum'], ['id' => 'numposts']), 'numposts');
        $form_container->output_row($this->lang->thread_count_is, "", $form->generate_select_box('conditions[threadnum_dir]', $greater_options, $input['conditions']['threadnum_dir'], ['id' => 'numthreads_dir']) . " " . $form->generate_text_box('conditions[threadnum]', $input['conditions']['threadnum'], ['id' => 'numthreads']), 'numthreads');

        $form_container->output_row($this->lang->reg_in_x_days, '', $form->generate_text_box('conditions[regdate]', $input['conditions']['regdate'], ['id' => 'regdate']) . ' ' . $this->lang->days, 'regdate');
        $form_container->output_row($this->lang->reg_ip_matches, $this->lang->wildcard, $form->generate_text_box('conditions[regip]', $input['conditions']['regip'], ['id' => 'regip']), 'regip');
        $form_container->output_row($this->lang->last_known_ip, $this->lang->wildcard, $form->generate_text_box('conditions[lastip]', $input['conditions']['lastip'], ['id' => 'lastip']), 'lastip');
        $form_container->output_row($this->lang->posted_with_ip, $this->lang->wildcard, $form->generate_text_box('conditions[postip]', $input['conditions']['postip'], ['id' => 'postip']), 'postip');

        $ret = $form_container->end();

        // Custom profile fields go here
        $form_container = new FormContainer($this, $this->lang->custom_profile_fields_match);

        // Fetch custom profile fields
        $query = $this->db->simple_select("profilefields", "*", "", ['order_by' => 'disporder']);

        $profile_fields = [];
        while ($profile_field = $this->db->fetch_array($query)) {
            if ($profile_field['required'] == 1) {
                $profile_fields['required'][] = $profile_field;
            } else {
                $profile_fields['optional'][] = $profile_field;
            }
        }

        $this->output_custom_profile_fields($profile_fields['required'], $input['profile_fields'], $form_container, $form, true);
        $this->output_custom_profile_fields($profile_fields['optional'], $input['profile_fields'], $form_container, $form, true);

        $ret .= $form_container->end();

        // Autocompletion for usernames
        $ret .= '
<link rel="stylesheet" href="' . $this->bb->asset_url . '/jscripts/select2/select2.css">
<script type="text/javascript" src="' . $this->bb->asset_url . '/jscripts/select2/select2.min.js?ver=1804"></script>
<script type="text/javascript">
<!--
$("#username").select2({
	placeholder: "' . $this->lang->search_for_a_user . '",
	minimumInputLength: 2,
	multiple: false,
	ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
		url: "'. $this->bb->settings['bburl'] .'/xmlhttp?action=get_users",
		dataType: \'json\',
		data: function (term, page) {
			return {
				query: term // search term
			};
		},
		results: function (data, page) { // parse the results into the format expected by Select2.
			// since we are using custom formatting functions we do not need to alter remote JSON data
			return {results: data};
		}
	},
	initSelection: function(element, callback) {
		var query = $(element).val();
		if (query !== "") {
			$.ajax("'. $this->bb->settings['bburl'] .'/xmlhttp?action=get_users&getone=1", {
				data: {
					query: query
				},
				dataType: "json"
			}).done(function(data) { callback(data); });
		}
	}
});
// -->
</script>';
        return $ret;
    }

    /**
     * Builds the "view management" interface allowing administrators to edit their custom designed "views"
     *
     * @param string $base_url The base URL to this instance of the view manager
     * @param string $type The internal type identifier for this view
     * @param array $fields Array of fields this view supports
     * @param array $sort_options Array of possible sort options this view supports if any
     * @param string $conditions_callback Optional callback function which generates list of "conditions" for this view
     */
    private function view_manager($base_url, $type, $fields, $sort_options = [], $conditions_callback = '')
    {
        $sub_tabs['views'] = [
            'title' => $this->lang->views,
            'link' => "{$base_url}?action=views",
            'description' => $this->lang->views_desc
        ];

        $sub_tabs['create_view'] = [
            'title' => $this->lang->create_new_view,
            'link' => "{$base_url}?action=views&amp;do=add",
            'description' => $this->lang->create_new_view_desc
        ];

        $this->page->add_breadcrumb_item($this->lang->view_manager, $this->bb->admin_url . '/users?action=views');

        // Lang strings should be in global lang file
        if ($this->bb->input['do'] == "set_default") {
            $query = $this->db->simple_select(
                "adminviews",
                "vid, uid, visibility",
                "vid='" . $this->bb->getInput('vid', 0) . "'"
            );
            $admin_view = $this->db->fetch_array($query);

            if (!$admin_view['vid'] ||
                $admin_view['visibility'] == 1 &&
                $this->user->uid != $admin_view['uid']
            ) {
                $this->session->flash_message($this->lang->error_invalid_admin_view, 'error');
                return $this->response->withRedirect($base_url . "?action=views");
            }
            $this->set_default_view($type, $admin_view['vid']);
            $this->session->flash_message($this->lang->succuss_view_set_as_default, 'success');
            return $this->response->withRedirect($base_url . "?action=views");
        }

        if ($this->bb->input['do'] == "add") {
            if ($this->bb->request_method == "post") {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_view_title;
                }
                if ($this->bb->input['fields_js']) {
                    $this->bb->input['fields'] = explode(",", $this->bb->input['fields_js']);
                }
                if (count($this->bb->input['fields']) <= 0) {
                    $errors[] = $this->lang->error_no_view_fields;
                }

                if ($this->bb->getInput('perpage', 0) <= 0) {
                    $errors[] = $this->lang->error_invalid_view_perpage;
                }

                if (!in_array($this->bb->input['sortby'], array_keys($sort_options))) {
                    $errors[] = $this->lang->error_invalid_view_sortby;
                }

                if ($this->bb->input['sortorder'] != "asc" && $this->bb->input['sortorder'] != "desc") {
                    $errors[] = $this->lang->error_invalid_view_sortorder;
                }

                if ($this->bb->input['visibility'] == 0) {
                    $this->bb->input['visibility'] = 2;
                }

                if (!$errors) {
                    $new_view = [
                        "uid" => $this->user->uid,
                        "title" => $this->db->escape_string($this->bb->input['title']),
                        "type" => $type,
                        "visibility" => $this->bb->getInput('visibility', 0),
                        "fields" => $this->db->escape_string(my_serialize($this->bb->input['fields'])),
                        "conditions" => $this->db->escape_string(my_serialize($this->bb->input['conditions'])),
                        "custom_profile_fields" => $this->db->escape_string(my_serialize($this->bb->input['profile_fields'])),
                        "sortby" => $this->db->escape_string($this->bb->input['sortby']),
                        "sortorder" => $this->db->escape_string($this->bb->input['sortorder']),
                        "perpage" => $this->bb->getInput('perpage', 0),
                        "view_type" => $this->db->escape_string($this->bb->input['view_type'])
                    ];

                    $vid = $this->db->insert_query("adminviews", $new_view);

                    if ($this->bb->input['isdefault']) {
                        $this->set_default_view($type, $vid);
                    }
                    $this->session->flash_message($this->lang->success_view_created, "success");
                    return $this->response->withRedirect($base_url . '?vid='.$vid);
                }
            } else {
                $this->bb->input = array_merge($this->bb->input, ['perpage' => 20]);
            }

            // Write in our JS based field selector
            $this->page->extra_header .= "<script src=\"{$this->bb->asset_url}/admin/jscripts/view_manager.js\" type=\"text/javascript\"></script>\n";

            $this->page->add_breadcrumb_item($this->lang->create_new_view);
            $html = $this->page->output_header($this->lang->create_new_view);

            $form = new Form($this->bb, $base_url . "?action=views&amp;do=add", "post");
            $html .= $form->getForm();
            $html .= $this->page->output_nav_tabs($sub_tabs, 'create_view');

            // If we have any error messages, show them
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            }

            $form_container = new FormContainer($this, $this->lang->create_new_view);
            $form_container->output_row($this->lang->title . " <em>*</em>", "", $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');

            if ($this->bb->input['visibility'] == 2) {
                $visibility_public_checked = true;
            } else {
                $visibility_private_checked = true;
            }

            $visibility_options = [
                $form->generate_radio_button("visibility", "1", "<strong>{$this->lang->private}</strong> - {$this->lang->private_desc}", ["checked" => $visibility_private_checked]),
                $form->generate_radio_button("visibility", "2", "<strong>{$this->lang->public}</strong> - {$this->lang->public_desc}", ["checked" => $visibility_public_checked])
            ];
            $form_container->output_row($this->lang->visibility, "", implode("<br />", $visibility_options));

            $form_container->output_row($this->lang->set_as_default_view, "", $form->generate_yes_no_radio("isdefault", $this->bb->input['isdefault'], ['yes' => 1, 'no' => 0]));

            if (count($sort_options) > 0) {
                $sort_directions = [
                    "asc" => $this->lang->ascending,
                    "desc" => $this->lang->descending
                ];
                $form_container->output_row($this->lang->sort_results_by, "", $form->generate_select_box('sortby', $sort_options, $this->bb->input['sortby'], ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('sortorder', $sort_directions, $this->bb->input['sortorder'], ['id' => 'sortorder']), 'sortby');
            }

            $form_container->output_row($this->lang->results_per_page, "", $form->generate_numeric_field('perpage', $this->bb->input['perpage'], ['id' => 'perpage', 'min' => 1]), 'perpage');

            if ($type == "user") {
                $form_container->output_row($this->lang->display_results_as, "", $form->generate_radio_button('view_type', 'table', $this->lang->table, ['checked' => ($this->bb->input['view_type'] != "card" ? true : false)]) . "<br />" . $form->generate_radio_button('view_type', 'card', $this->lang->business_card, ['checked' => ($this->bb->input['view_type'] == "card" ? true : false)]));
            }

            $html .= $form_container->end();

            $field_select = "<div class=\"view_fields\">\n";
            $field_select .= "<div class=\"enabled\"><div class=\"fields_title\">{$this->lang->enabled}</div><ul id=\"fields_enabled\">\n";
            if (is_array($this->bb->input['fields'])) {
                foreach ($this->bb->input['fields'] as $field) {
                    if ($fields[$field]) {
                        $field_select .= "<li id=\"field-{$field}\">&#149; {$fields[$field]['title']}</li>";
                        $active[$field] = 1;
                    }
                }
            }
            $field_select .= "</ul></div>\n";
            $field_select .= "<div class=\"disabled\"><div class=\"fields_title\">{$this->lang->disabled}</div><ul id=\"fields_disabled\">\n";
            foreach ($fields as $key => $field) {
                if ($active[$key]) {
                    continue;
                }
                $field_select .= "<li id=\"field-{$key}\">&#149; {$field['title']}</li>";
            }
            $field_select .= "</div></ul>\n";
            $field_select .= $form->generate_hidden_field("fields_js", @implode(",", @array_keys($active)), ['id' => 'fields_js']);
            $field_select = str_replace("'", "\\'", $field_select);
            $field_select = str_replace("\n", "", $field_select);

            $field_select = "<script type=\"text/javascript\">
//<![CDATA[
document.write('" . str_replace("/", "\/", $field_select) . "');
//]]>
</script>\n";

            foreach ($fields as $key => $field) {
                $field_options[$key] = $field['title'];
            }

            $field_select .= "<noscript>" . $form->generate_select_box('fields[]', $field_options, $this->bb->input['fields'], ['id' => 'fields', 'multiple' => true]) . "</noscript>\n";

            $form_container = new FormContainer($this, $this->lang->fields_to_show);
            $form_container->output_row($this->lang->fields_to_show_desc, $description, $field_select);
            $html .= $form_container->end();

            // Build the search conditions
            if (function_exists($conditions_callback)) {
                $conditions_callback($this->bb->input, $form);
            }

            $buttons[] = $form->generate_submit_button($this->lang->save_view);
            $html .= $form->output_submit_wrapper($buttons);
            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($this->response, '@forum/Admin/Users/UsersViewAdd.html.twig');
        } elseif ($this->bb->input['do'] == "edit") {
            $query = $this->db->simple_select("adminviews", "*", "vid='" . $this->bb->getInput('vid', 0) . "'");
            $admin_view = $this->db->fetch_array($query);

            // Does the view not exist?
            if (!$admin_view['vid'] || $admin_view['visibility'] == 1 && $this->user->uid != $admin_view['uid']) {
                $this->session->flash_message($this->lang->error_invalid_admin_view, 'error');
                return $this->response->withRedirect($base_url . "?action=views");
            }

            if ($this->bb->request_method == "post") {
                if (!trim($this->bb->input['title'])) {
                    $errors[] = $this->lang->error_missing_view_title;
                }
                if ($this->bb->input['fields_js']) {
                    $this->bb->input['fields'] = explode(",", $this->bb->input['fields_js']);
                }

                if (count($this->bb->input['fields']) <= 0) {
                    $errors[] = $this->lang->error_no_view_fields;
                }

                if ($this->bb->getInput('perpage', 0) <= 0) {
                    $errors[] = $this->lang->error_invalid_view_perpage;
                }

                if (!in_array($this->bb->input['sortby'], array_keys($sort_options))) {
                    $errors[] = $this->lang->error_invalid_view_sortby;
                }

                if ($this->bb->input['sortorder'] != "asc" && $this->bb->input['sortorder'] != "desc") {
                    $errors[] = $this->lang->error_invalid_view_sortorder;
                }

                if ($this->bb->input['visibility'] == 0) {
                    $this->bb->input['visibility'] = 2;
                }

                if (!isset($errors)) {
                    $updated_view = [
                        "title" => $this->db->escape_string($this->bb->input['title']),
                        "type" => $type,
                        "visibility" => $this->bb->getInput('visibility', 0),
                        "fields" => $this->db->escape_string(my_serialize($this->bb->input['fields'])),
                        "conditions" => $this->db->escape_string(my_serialize($this->bb->input['conditions'])),
                        "custom_profile_fields" => $this->db->escape_string(my_serialize($this->bb->input['profile_fields'])),
                        "sortby" => $this->db->escape_string($this->bb->input['sortby']),
                        "sortorder" => $this->db->escape_string($this->bb->input['sortorder']),
                        "perpage" => $this->bb->getInput('perpage', 0),
                        "view_type" => $this->db->escape_string($this->bb->input['view_type'])
                    ];
                    $this->db->update_query("adminviews", $updated_view, "vid='{$admin_view['vid']}'");

                    if ($this->bb->input['isdefault']) {
                        $this->set_default_view($type, $admin_view['vid']);
                    }

                    $this->session->flash_message($this->lang->success_view_updated, "success");
                    return $this->response->withRedirect($base_url . '?vid='.$admin_view['vid']);
                }
            }

            // Write in our JS based field selector
            $this->page->extra_header .= "<script src=\"{$this->bb->asset_url}/admin/jscripts/view_manager.js\" type=\"text/javascript\"></script>\n";

            $this->page->add_breadcrumb_item($this->lang->edit_view);
            $html = $this->page->output_header($this->lang->edit_view);

            $form = new Form($this->bb, $base_url . "?action=views&amp;do=edit&amp;vid={$admin_view['vid']}", "post");
            $html .= $form->getForm();
            $sub_tabs = [];
            $sub_tabs['edit_view'] = [
                'title' => $this->lang->edit_view,
                'link' => $base_url . "?action=views&amp;do=edit&amp;vid={$admin_view['vid']}",
                'description' => $this->lang->edit_view_desc
            ];

            $html .= $this->page->output_nav_tabs($sub_tabs, 'edit_view');

            // If we have any error messages, show them
            if (isset($errors)) {
                $html .= $this->page->output_inline_error($errors);
            } else {
                $admin_view['conditions'] = my_unserialize($admin_view['conditions']);
                $admin_view['fields'] = my_unserialize($admin_view['fields']);
                $admin_view['profile_fields'] = my_unserialize($admin_view['custom_profile_fields']);
                $this->bb->input = array_merge($this->bb->input, $admin_view);

                $this->bb->input['isdefault'] = 0;
                $default_view = $this->fetch_default_view($type);

                if ($default_view == $admin_view['vid']) {
                    $this->bb->input['isdefault'] = 1;
                }
            }

            $form_container = new FormContainer($this, $this->lang->edit_view);
            $form_container->output_row($this->lang->view . " <em>*</em>", "", $form->generate_text_box('title', $this->bb->input['title'], ['id' => 'title']), 'title');

            if ($this->bb->input['visibility'] == 2) {
                $visibility_public_checked = true;
            } else {
                $visibility_private_checked = true;
            }

            $visibility_options = [
                $form->generate_radio_button("visibility", "1", "<strong>{$this->lang->private}</strong> - {$this->lang->private_desc}", ["checked" => $visibility_private_checked]),
                $form->generate_radio_button("visibility", "2", "<strong>{$this->lang->public}</strong> - {$this->lang->public_desc}", ["checked" => $visibility_public_checked])
            ];
            $form_container->output_row($this->lang->visibility, "", implode("<br />", $visibility_options));

            $form_container->output_row($this->lang->set_as_default_view, "", $form->generate_yes_no_radio("isdefault", $this->bb->input['isdefault'], ['yes' => 1, 'no' => 0]));

            if (count($sort_options) > 0) {
                $sort_directions = [
                    "asc" => $this->lang->ascending,
                    "desc" => $this->lang->descending
                ];
                $form_container->output_row($this->lang->sort_results_by, "", $form->generate_select_box('sortby', $sort_options, $this->bb->input['sortby'], ['id' => 'sortby']) . " {$this->lang->in} " . $form->generate_select_box('sortorder', $sort_directions, $this->bb->input['sortorder'], ['id' => 'sortorder']), 'sortby');
            }

            $form_container->output_row($this->lang->results_per_page, "", $form->generate_numeric_field('perpage', $this->bb->input['perpage'], ['id' => 'perpage', 'min' => 1]), 'perpage');

            if ($type == "user") {
                $form_container->output_row($this->lang->display_results_as, "", $form->generate_radio_button('view_type', 'table', $this->lang->table, ['checked' => ($this->bb->input['view_type'] != "card" ? true : false)]) . "<br />" . $form->generate_radio_button('view_type', 'card', $this->lang->business_card, ['checked' => ($this->bb->input['view_type'] == "card" ? true : false)]));
            }

            $html .= $form_container->end();

            $field_select = "<div class=\"view_fields\">\n";
            $field_select .= "<div class=\"enabled\"><div class=\"fields_title\">{$this->lang->enabled}</div><ul id=\"fields_enabled\">\n";
            if (is_array($this->bb->input['fields'])) {
                foreach ($this->bb->input['fields'] as $field) {
                    if ($fields[$field]) {
                        $field_select .= "<li id=\"field-{$field}\">&#149; {$fields[$field]['title']}</li>";
                        $active[$field] = 1;
                    }
                }
            }
            $field_select .= "</ul></div>\n";
            $field_select .= "<div class=\"disabled\"><div class=\"fields_title\">{$this->lang->disabled}</div><ul id=\"fields_disabled\">\n";
            if (is_array($fields)) {
                foreach ($fields as $key => $field) {
                    if ($active[$key]) {
                        continue;
                    }
                    $field_select .= "<li id=\"field-{$key}\">&#149; {$field['title']}</li>";
                }
            }
            $field_select .= "</div></ul>\n";
            $field_select .= $form->generate_hidden_field("fields_js", @implode(",", @array_keys($active)), ['id' => 'fields_js']);
            $field_select = str_replace("'", "\\'", $field_select);
            $field_select = str_replace("\n", "", $field_select);

            $field_select = "<script type=\"text/javascript\">
//<![CDATA[
document.write('" . str_replace("/", "\/", $field_select) . "');
//]]></script>\n";

            foreach ($fields as $key => $field) {
                $field_options[$key] = $field['title'];
            }

            $field_select .= "<noscript>" . $form->generate_select_box('fields[]', $field_options, $this->bb->input['fields'], ['id' => 'fields', 'multiple' => true]) . "</noscript>\n";

            $form_container = new FormContainer($this, $this->lang->fields_to_show);
            $form_container->output_row($this->lang->fields_to_show_desc, $description, $field_select);
            $html .= $form_container->end();

            // Build the search conditions
            if (function_exists($conditions_callback)) {
                $conditions_callback($this->bb->input, $form);
            }

            $buttons[] = $form->generate_submit_button($this->lang->save_view);
            $html .= $form->output_submit_wrapper($buttons);

            $html .= $form->end();
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($this->response, '@forum/Admin/Users/UsersViewEdit.html.twig');
        } elseif ($this->bb->input['do'] == "delete") {
            if ($this->bb->input['no']) {
                return $this->response->withRedirect($base_url . "?action=views");
            }

            $query = $this->db->simple_select("adminviews", "COUNT(vid) as views");
            $views = $this->db->fetch_field($query, "views");

            if ($views == 0) {
                $this->session->flash_message($this->lang->error_cannot_delete_view, 'error');
                return $this->response->withRedirect($base_url . "?action=views");
            }

            $vid = $this->bb->getInput('vid', 0);
            $query = $this->db->simple_select("adminviews", "vid, uid, visibility", "vid = '{$vid}'");
            $admin_view = $this->db->fetch_array($query);

            if ($vid == 1 || !$admin_view['vid'] || $admin_view['visibility'] == 1 && $this->user->uid != $admin_view['uid']) {
                $this->session->flash_message($this->lang->error_invalid_view_delete, 'error');
                return $this->response->withRedirect($base_url . "?action=views");
            }

            if ($this->bb->request_method == "post") {
                $this->db->delete_query("adminviews", "vid='{$admin_view['vid']}'");
                $this->session->flash_message($this->lang->success_view_deleted, 'success');
                return $this->response->withRedirect($base_url . "?action=views");
            } else {
                $this->page->output_confirm_action($base_url . "?action=views&amp;do=delete&amp;vid={$admin_view['vid']}", $this->lang->confirm_view_deletion);
            }
        } // Export views
        elseif ($this->bb->input['do'] == "export") {
            $xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?" . ">\n";
            $xml = "<adminviews version=\"" . $this->bb->version_code . "\" exported=\"" . TIME_NOW . "\">\n";

            if ($this->bb->input['type']) {
                $type_where = "type='" . $this->db->escape_string($this->bb->input['type']) . "'";
            }

            $query = $this->db->simple_select("adminviews", "*", $type_where);
            while ($admin_view = $this->db->fetch_array($query)) {
                $fields = my_unserialize($admin_view['fields']);
                $conditions = my_unserialize($admin_view['conditions']);

                $admin_view['title'] = str_replace(']]>', ']]]]><![CDATA[>', $admin_view['title']);
                $admin_view['sortby'] = str_replace(']]>', ']]]]><![CDATA[>', $admin_view['sortby']);
                $admin_view['sortorder'] = str_replace(']]>', ']]]]><![CDATA[>', $admin_view['sortorder']);
                $admin_view['view_type'] = str_replace(']]>', ']]]]><![CDATA[>', $admin_view['view_type']);

                $xml .= "\t<view vid=\"{$admin_view['vid']}\" uid=\"{$admin_view['uid']}\" type=\"{$admin_view['type']}\" visibility=\"{$admin_view['visibility']}\">\n";
                $xml .= "\t\t<title><![CDATA[{$admin_view['title']}]]></title>\n";
                $xml .= "\t\t<fields>\n";
                foreach ($fields as $field) {
                    $xml .= "\t\t\t<field name=\"{$field}\" />\n";
                }
                $xml .= "\t\t</fields>\n";
                $xml .= "\t\t<conditions>\n";
                foreach ($conditions as $name => $condition) {
                    if (!$conditions) {
                        continue;
                    }
                    if (is_array($condition)) {
                        $condition = my_serialize($condition);
                        $is_serialized = " is_serialized=\"1\"";
                    }
                    $condition = str_replace(']]>', ']]]]><![CDATA[>', $condition);
                    $xml .= "\t\t\t<condition name=\"{$name}\"{$is_serialized}><![CDATA[{$condition}]]></condition>\n";
                }
                $xml .= "\t\t</conditions>\n";
                $xml .= "\t\t<sortby><![CDATA[{$admin_view['sortby']}]]></sortby>\n";
                $xml .= "\t\t<sortorder><![CDATA[{$admin_view['sortorder']}]]></sortorder>\n";
                $xml .= "\t\t<perpage><![CDATA[{$admin_view['perpage']}]]></perpage>\n";
                $xml .= "\t\t<view_type><![CDATA[{$admin_view['view_type']}]]></view_type>\n";
                $xml .= "\t</view>\n";
            }
            $xml .= "</adminviews>\n";
            $this->bb->settings['bbname'] = urlencode($this->bb->settings['bbname']);
            header("Content-disposition: filename=" . $this->bb->settings['bbname'] . "-views.xml");
            header("Content-Length: " . my_strlen($xml));
            header("Content-type: unknown/unknown");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $xml;
            exit;
        } // Generate a listing of all current views
        else {
            $html = $this->page->output_header($this->lang->view_manager);

            $html .= $this->page->output_nav_tabs($sub_tabs, 'views');

            $table = new Table;
            $table->construct_header($this->lang->view);
            $table->construct_header($this->lang->controls, ["class" => "align_center", "width" => 150]);

            $default_view = $this->fetch_default_view($type);

            $query = $this->db->simple_select("adminviews", "COUNT(vid) as views");
            $views = $this->db->fetch_field($query, "views");

            $query = $this->db->query("
			SELECT v.*, u.username
			FROM " . TABLE_PREFIX . "adminviews v
			LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=v.uid)
			WHERE v.visibility='2' OR (v.visibility='1' AND v.uid='{$this->user->uid}')
			ORDER BY title
		");
            while ($view = $this->db->fetch_array($query)) {
                $created = "";
                if ($view['uid'] == 0) {
                    $view_type = "default";
                    $default_class = "grey";
                } elseif ($view['visibility'] == 2) {
                    $view_type = "group";
                    if ($view['username']) {
                        $created = "<br /><small>{$this->lang->created_by} {$view['username']}</small>";
                    }
                } else {
                    $view_type = "user";
                }

                $default_add = '';
                if ($default_view == $view['vid']) {
                    $default_add = " ({$this->lang->default})";
                }

                $title_string = "view_title_{$view['vid']}";

                if ($this->lang->$title_string) {
                    $view['title'] = $this->lang->$title_string;
                }

                $table->construct_cell("<div class=\"float_right\"><img src=\"styles/{$this->page->style}/images/icons/{$view_type}.png\" title=\"" . $this->lang->sprintf($this->lang->this_is_a_view, $view_type) . "\" alt=\"{$view_type}\" /></div><div class=\"{$default_class}\"><strong><a href=\"{$base_url}?action=views&amp;do=edit&amp;vid={$view['vid']}\" >{$view['title']}</a></strong>{$default_add}{$created}</div>");

                $popup = new PopupMenu("view_{$view['vid']}", $this->lang->options);
                $popup->add_item($this->lang->edit_view, "{$base_url}?action=views&amp;do=edit&amp;vid={$view['vid']}");
                if ($view['vid'] != $default_view) {
                    $popup->add_item($this->lang->set_as_default, "{$base_url}?action=views&amp;do=set_default&amp;vid={$view['vid']}");
                }

                if ($views > 1 && $view['vid'] != 1) {
                    $popup->add_item($this->lang->delete_view, "{$base_url}?action=views&amp;do=delete&amp;vid={$view['vid']}&amp;my_post_key={$this->bb->post_code}", "return AdminCP.deleteConfirmation(this, '{$this->lang->confirm_view_deletion}')");
                }
                $controls = $popup->fetch();
                $table->construct_cell($controls, ["class" => "align_center"]);
                $table->construct_row();
            }

            $html .= $table->output($this->lang->view);

            $html .= <<<LEGEND
<br />
<fieldset>
<legend>{$this->lang->legend}</legend>
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/default.png" alt="{$this->lang->default}" style="vertical-align: middle;" /> {$this->lang->default_view_desc}<br />
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/group.png" alt="{$this->lang->public}" style="vertical-align: middle;" /> {$this->lang->public_view_desc}<br />
<img src="{$this->bb->asset_url}/admin/styles/{$this->page->style}/images/icons/user.png" alt="{$this->lang->private}" style="vertical-align: middle;" /> {$this->lang->private_view_desc}</fieldset>
LEGEND;
            $this->page->output_footer();

            $this->view->offsetSet('content', $html);//FIXME rebuild!!!!
            return $this->view->render($this->response, '@forum/Admin/Users/UsersView.html.twig');
        }
    }

    private function set_default_view($type, $vid)
    {
        $query = $this->db->simple_select("adminoptions", "defaultviews", "uid='{$this->user->uid}'");
        $default_views = my_unserialize($this->db->fetch_field($query, "defaultviews"));
        if (!$this->db->num_rows($query)) {
            $create = true;
        }
        $default_views[$type] = $vid;
        $default_views = my_serialize($default_views);
        $updated_admin = ["defaultviews" => $this->db->escape_string($default_views)];

        if ($create == true) {
            $updated_admin['uid'] = $this->user->uid;
            $updated_admin['notes'] = '';
            $updated_admin['permissions'] = '';
            $this->db->insert_query("adminoptions", $updated_admin);
        } else {
            $this->db->update_query("adminoptions", $updated_admin, "uid='{$this->user->uid}'");
        }
    }

    private function fetch_default_view($type)
    {
        $query = $this->db->simple_select("adminoptions", "defaultviews", "uid='{$this->user->uid}'");
        $default_views = my_unserialize($this->db->fetch_field($query, "defaultviews"));
        if (!is_array($default_views)) {
            return false;
        }
        return $default_views[$type];
    }

    /**
     * Provides a function to entirely delete a user's posts, and find the threads attached to them
     *
     * @param integer $uid The uid of the user
     * @param int $date A UNIX timestamp to delete posts that are older
     * @return array An array of threads to delete, threads/forums to recount
     */
    public function delete_user_posts($uid, $date)
    {
        $uid = (int)$uid;

        // Build an array of posts to delete
        $postcache = [];
        $query = $this->db->simple_select('posts', 'pid', "uid = '" . $uid . "' AND dateline < '" . $date . "'");
        while ($post = $this->db->fetch_array($query)) {
            $postcache[] = $post['pid'];
        }

        if (!$this->db->num_rows($query)) {
            return false;
        } elseif (!empty($postcache)) {
            // Let's start deleting posts
            $user_posts = implode(',', $postcache);
            $query = $this->db->query('
			SELECT p.pid, p.visible, f.usepostcounts, t.tid AS thread, t.firstpost, t.fid AS forum
			FROM ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . 'forums f ON (f.fid=p.fid)
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
			WHERE p.pid IN ({$user_posts})
		");

            $post_count = 0; // Collect the post number to deduct from the user's postcount
            $thread_list = [];
            $forum_list = [];
            $delete_thread_list = [];
            if (!$this->db->num_rows($query)) {
                return false;
            } else {
                while ($post = $this->db->fetch_array($query)) {
                    if ($post['usepostcounts'] != 0 && $post['visible'] != 0) {
                        ++$post_count;
                    }

                    if ($post['pid'] == $post['firstpost']) {
                        $delete_thread_list[] = $post['thread'];
                    }

                    if (!in_array($post['thread'], $thread_list) && !in_array($post['thread'], $delete_thread_list)) {
                        $thread_list[] = $post['thread']; // Threads that have been affected by this action, that aren't marked to be deleted
                    }
                    if (!in_array($post['forum'], $forum_list)) {
                        $forum_list[] = $post['forum']; // Forums that have been affected, too
                    }

                    // Remove the attachments to this post, then delete the post
                    $this->upload->remove_attachments($post['pid']);
                    $this->db->delete_query("posts", "pid = '" . $post['pid'] . "'");
                    $this->db->delete_query("pollvotes", "pid = '" . $post['pid'] . "'"); // Delete pollvotes attached to this post
                }

                $this->db->update_query("users", ["postnum" => "postnum-" . $post_count . ""], "uid='" . $uid . "'", 1, true);

                $to_return = [
                    'to_delete' => $delete_thread_list,
                    'thread_update' => $thread_list,
                    'forum_update' => $forum_list
                ];

                return $to_return;
            }
        }
    }
}