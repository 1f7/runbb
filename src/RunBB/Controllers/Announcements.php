<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Announcements extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('announcements');

        $aid = $this->bb->getInput('aid', 0);

        // Get announcement fid
        $query = $this->db->simple_select('announcements', 'fid', "aid='$aid'");
        $announcement = $this->db->fetch_array($query);

        $this->plugins->runHooks('announcements_start');

        if (!$announcement) {
            $this->bb->error($this->lang->error_invalidannouncement);
        }

        // Get forum info
        $this->bb->fid = $announcement['fid'];
        if ($this->bb->fid > 0) {
            $forum = $this->forum->get_forum($this->bb->fid);

            if (!$forum) {
                $this->bb->error($this->lang->error_invalidforum);
            }

            // Make navigation
            $this->bb->build_forum_breadcrumb($forum['fid']);

            // Permissions
            $forumpermissions = $this->forum->forum_permissions($forum['fid']);

            if ($forumpermissions['canview'] == 0 || $forumpermissions['canviewthreads'] == 0) {
                return $this->bb->error_no_permission();
            }

            // Check if this forum is password protected and we have a valid password
            $this->forum->check_forum_password($forum['fid']);
        }
        $this->bb->add_breadcrumb($this->lang->nav_announcements);

        // Get announcement info
        $time = TIME_NOW;

        $query = $this->db->query('
	SELECT u.*, u.username AS userusername, a.*, f.*
	FROM ' . TABLE_PREFIX . 'announcements a
	LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=a.uid)
	LEFT JOIN ' . TABLE_PREFIX . "userfields f ON (f.ufid=u.uid)
	WHERE a.startdate<='$time' AND (a.enddate>='$time' OR a.enddate='0') AND a.aid='$aid'
");

        $this->bb->announcementarray = $this->db->fetch_array($query);

        if (!$this->bb->announcementarray) {
            $this->bb->error($this->lang->error_invalidannouncement);
        }

        // Gather usergroup data from the cache
        // Field => Array Key
        $data_key = [
            'title' => 'grouptitle',
            'usertitle' => 'groupusertitle',
            'stars' => 'groupstars',
            'starimage' => 'groupstarimage',
            'image' => 'groupimage',
            'namestyle' => 'namestyle',
            'usereputationsystem' => 'usereputationsystem'
        ];

        foreach ($data_key as $field => $key) {
            $this->bb->announcementarray[$key] =
                $this->bb->groupscache[$this->bb->announcementarray['usergroup']][$field];
        }

        $this->bb->announcementarray['dateline'] = $this->bb->announcementarray['startdate'];
        $this->bb->announcementarray['userusername'] = $this->bb->announcementarray['username'];

        $announcement = $this->post->build_postbit($this->bb->announcementarray, 3);
        $this->view->offsetSet('announcement', $announcement);

        $this->bb->announcementarray['subject'] =
            $this->parser->parse_badwords($this->bb->announcementarray['subject']);
        $this->lang->forum_announcement =
            $this->lang->sprintf(
                $this->lang->forum_announcement,
                htmlspecialchars_uni($this->bb->announcementarray['subject'])
            );

        if ($this->bb->announcementarray['startdate'] > $this->user->lastvisit) {
            $setcookie = true;
            if (isset($this->bb->cookies['mybb']['announcements']) &&
                is_scalar($this->bb->cookies['mybb']['announcements'])) {
                $cookie = my_unserialize(stripslashes($this->bb->cookies['mybb']['announcements']));

                if (isset($cookie[$this->bb->announcementarray['aid']])) {
                    $setcookie = false;
                }
            }

            if ($setcookie) {
                $this->bb->my_set_array_cookie(
                    'announcements',
                    $this->bb->announcementarray['aid'],
                    $this->bb->announcementarray['startdate'],
                    -1
                );
            }
        }

        $this->plugins->runHooks('announcements_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/announcements.html.twig');
    }
}
