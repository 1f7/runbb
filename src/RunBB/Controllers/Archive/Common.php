<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Archive;

use RunCMF\Core\AbstractController;
use RunBB\Core\Archive;

class Common extends AbstractController
{
    protected function init()
    {
        define('IN_ARCHIVE', 1);

        $this->bb->shutdown_queries = $shutdown_functions = [];

        $groupscache = $this->cache->read('usergroups');
        if (!is_array($groupscache)) {
            $this->cache->update_usergroups();
            $groupscache = $this->cache->read('usergroups');
        }
        $fpermissioncache = $this->cache->read('forumpermissions');

        // Send headers before anything else.
        // send_page_headers();

        // If the installer has not been removed and no lock exists, die.
        if (is_dir(MYBB_ROOT . 'install') && !file_exists(MYBB_ROOT . 'install/lock')) {
            echo "Please remove the install directory from your server, or create a file called 'lock' in the install directory. Until you do so, your board will remain unaccessable";
            exit;
        }

        // If the server OS is not Windows and not Apache or the PHP is running as a CGI or we have defined ARCHIVE_QUERY_STRINGS, use query strings - DIRECTORY_SEPARATOR checks if running windows
        //if((DIRECTORY_SEPARATOR != '\\' && stripos($_SERVER['SERVER_SOFTWARE'], 'apache') == false) || stripos(SAPI_NAME, 'cgi') !== false || defined("ARCHIVE_QUERY_STRINGS"))
        // http://dev.mybb.com/issues/1489 - remove automatic detection and rely on users to set the right option here
        if ($this->bb->settings['seourls_archive'] == 1) {
            if ($_SERVER['REQUEST_URI']) {
                $url = $_SERVER['REQUEST_URI'];
            } elseif ($_SERVER['REDIRECT_URL']) {
                $url = $_SERVER['REDIRECT_URL'];
            } elseif ($_SERVER['PATH_INFO']) {
                $url = $_SERVER['PATH_INFO'];
            } else {
                $url = $this->bb->currentRoute;
            }
            $this->bb->base_url = $this->bb->settings['bburl'] . '/archive';
            $endpart = my_substr(strrchr($url, '/'), 1);
        } else {
            $url = $_SERVER['QUERY_STRING'];
            $this->bb->base_url = $this->bb->settings['bburl'] . '/archive?';
            $endpart = $url;
        }

        $this->action = 'index';
//    $this->archive = new Archive($this);
        // This seems to work the same as the block below except without the css bugs O_o
        $archiveurl = $this->bb->settings['bburl'] . '/archive';

        if ($endpart != 'index') {
            $endpart = str_replace('.html', '', $endpart);
            $todo = explode('-', $endpart, 3);
            if ($todo[0]) {
                $this->action = $this->action2 = $todo[0];
            }
            if (!empty($todo[2])) {
                $this->page = (int)$todo[2];
            } else {
                $this->page = 1;
            }
            if (!empty($todo[1])) {
                $this->id = (int)$todo[1];
            } else {
                $this->id = 0;
            }

            // Get the thread, announcement or forum information.
            if ($this->action == 'announcement') {
                $time = TIME_NOW;
                $query = $this->db->query('
			SELECT a.*, u.username
			FROM ' . TABLE_PREFIX . 'announcements a
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=a.uid)
			WHERE a.aid='{$this->id}' AND startdate < '{$time}'  AND (enddate > '{$time}' OR enddate = 0)
		");
                $announcement = $this->db->fetch_array($query);
                if (!$announcement['aid']) {
                    $this->action = '404';
                }
            } elseif ($this->action == 'thread') {
                $query = $this->db->simple_select('threads', '*', "tid='{$this->id}' AND closed NOT LIKE 'moved|%'");
                $this->threads = $this->db->fetch_array($query);
                if (!$this->threads['tid']) {
                    $this->action = '404';
                }
            } elseif ($this->action == 'forum') {
                $query = $this->db->simple_select('forums', '*', "fid='{$this->id}' AND active!=0 AND password=''");
                $this->forums = $this->db->fetch_array($query);
                if (!$this->forums['fid']) {
                    $this->action = '404';
                }
            } elseif ($this->action != 'index') {
                $this->action = '404';
            }
        }

        // Define the full MyBB version location of this page.
        if ($this->action == 'thread') {
            define('MYBB_LOCATION', get_thread_link($this->id));
        } elseif ($this->action == 'forum') {
            define('MYBB_LOCATION', $this->forum->get_forum_link($this->id));
        } elseif ($this->action == 'announcement') {
            define('MYBB_LOCATION', get_announcement_link($this->id));
        } else {
            define('MYBB_LOCATION', $this->bb->settings['bburl']);
        }


        if (!$this->bb->settings['bblanguage']) {
            $this->bb->settings['bblanguage'] = 'english';
        }
        $this->lang->set_language($this->bb->settings['bblanguage']);

        // Load global language phrases
        $this->lang->load('global');
        $this->lang->load('messages');
        $this->lang->load('archive');

        // Draw up the basic part of our naviagation
        $this->bb->navbits[0]['name'] = $this->bb->settings['bbname_orig'];
        $this->bb->navbits[0]['url'] = $this->bb->settings['bburl'] . '/archive';

        // Check banned ip addresses
        if ($this->ban->is_banned_ip($this->session->ipaddress)) {
            $this->archive->archive_error($this->lang->error_banned);
            return 'exit';
        }

        // If our board is closed..
        if ($this->bb->settings['boardclosed'] == 1) {
            if ($this->bb->usergroup['canviewboardclosed'] != 1) {
                if (!$this->bb->settings['boardclosed_reason']) {
                    $this->bb->settings['boardclosed_reason'] = $this->lang->boardclosed_reason;
                }

                $this->lang->error_boardclosed .= '<blockquote>' . $this->bb->settings['boardclosed_reason'] . '</blockquote>';
                $this->archive->archive_error($this->lang->error_boardclosed);
                return 'exit';
            }
        }

        // Do we require users to login?
        if ($this->bb->settings['forcelogin'] == 1) {
            if ($this->user->uid == 0) {
                $this->archive->archive_error($this->lang->error_mustlogin);
                return 'exit';
            }
        }

//FIXME ???    // Load Limiting - DIRECTORY_SEPARATOR checks if running windows
//    if(DIRECTORY_SEPARATOR != '\\')
//    {
//      if($uptime = @exec('uptime'))
//      {
//        //preg_match("/averages?: ([0-9\.]+),[\s]+([0-9\.]+),[\s]+([0-9\.]+)/", $uptime, $regs);
//        preg_match("/average?: ([0-9\.]+),[\s]+([0-9\.]+),[\s]+([0-9\.]+)/", $uptime, $regs);
//        $load = $regs[1];
//        if($this->bb->usergroup['cancp'] != 1 && $load > $this->bb->settings['load'] && $this->bb->settings['load'] > 0)
//        {
//          $this->archive->archive_error($this->lang->error_loadlimit);
//          return 'exit';
//        }
//      }
//    }

        if ($this->bb->usergroup['canview'] == 0) {
            $this->archive->archive_error_no_permission();
            return 'exit';
        }
    }
}
