<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Archive;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Load global language phrases
        $this->lang->load('index');

        $this->plugins->runHooks('archive_start');

        switch ($this->action) {
            // Display an announcement.
            case 'announcement':
                // Fetch the forum this thread is in
                if ($announcement['fid'] != -1) {
                    $forum = $this->forum->get_forum($announcement['fid']);
                    if (!$forum['fid'] || $forum['password'] != '') {
                        return $this->archive->archive_error($this->lang->error_invalidforum);
                    }

                    // Check if we have permission to view this thread
                    $this->forumpermissions = $this->forum->forum_permissions($forum['fid']);
                    if ($this->forumpermissions['canview'] != 1 || $this->forumpermissions['canviewthreads'] != 1) {
                        return $this->archive->archive_error_no_permission();
                    }

                    $this->archive->check_forum_password_archive($forum['fid']);
                }

                $announcement['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($announcement['subject']));

                $parser_options = [
                    'allow_html' => $announcement['allowhtml'],
                    'allow_mycode' => $announcement['allowmycode'],
                    'allow_smilies' => $announcement['allowsmilies'],
                    'allow_imgcode' => 1,
                    'allow_videocode' => 1,
                    'me_username' => $announcement['username'],
                    'filter_badwords' => 1
                ];

                $announcement['message'] = $this->parser->parse_message($announcement['message'], $parser_options);

                $profile_link = $this->user->build_profile_link($announcement['username'], $announcement['uid']);

                // Build the navigation
                $this->bb->add_breadcrumb($announcement['subject']);
                $this->archive->archive_header($announcement['subject'], $announcement['subject'], $this->bb->settings['bburl'] . "/announcements?aid={$this->id}");

                // Format announcement contents.
                $announcement['startdate'] = $this->time->formatDate('relative', $announcement['startdate']);

                $this->plugins->runHooks('archive_announcement_start');

                echo "<div class=\"post\">\n<div class=\"header\">\n<h2>{$announcement['subject']} - {$profile_link}</h2>";
                echo "<div class=\"dateline\">{$announcement['startdate']}</div>\n</div>\n<div class=\"message\">{$announcement['message']}</div>\n</div>\n";

                $this->plugins->runHooks('archive_announcement_end');

                $this->archive->archive_footer();
                break;

            // Display a thread.
            case 'thread':
                $thread['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($this->threads['subject']));

                // Fetch the forum this thread is in
                $forum = $this->forum->get_forum($this->threads['fid']);
                if (!$forum['fid'] || $forum['password'] != '') {
                    return $this->archive->archive_error($this->lang->error_invalidforum);
                }

                // Check if we have permission to view this thread
                $this->forumpermissions = $this->forum->forum_permissions($forum['fid']);
                if ($this->forumpermissions['canview'] != 1 || $this->forumpermissions['canviewthreads'] != 1) {
                    return $this->archive->archive_error_no_permission();
                }

                if ($this->threads['visible'] != 1) {
                    if ($this->user->is_moderator($forum['fid'], 'canviewunapprove')) {
                        return $this->archive->archive_error(
                            $this->lang->sprintf(
                                $this->lang->error_unapproved_thread,
                                $this->bb->settings['bburl'] . '/' . get_thread_link($this->threads['tid'], $this->page)
                            )
                        );
                    } else {
                        return $this->archive->archive_error($this->lang->error_invalidthread);
                    }
                }

                if (isset($this->forumpermissions['canonlyviewownthreads']) &&
                    $this->forumpermissions['canonlyviewownthreads'] == 1 &&
                    $this->threads['uid'] != $this->user->uid
                ) {
                    return $this->archive->archive_error_no_permission();
                }

                $this->archive->check_forum_password_archive($forum['fid']);

                // Build the navigation
                $this->bb->build_forum_breadcrumb($forum['fid']);//, 1);
                $this->bb->add_breadcrumb($this->threads['subject']);

                $this->archive->archive_header(
                    $this->threads['subject'],
                    $this->threads['subject'],
                    $this->bb->settings['homeurl'] . get_thread_link($this->threads['tid'], $this->page)
                );

                $this->plugins->runHooks('archive_thread_start');

                // Paginate this thread
                if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                    $this->bb->settings['postsperpage'] = 20;
                }
                $perpage = $this->bb->settings['postsperpage'];
                $postcount = (int)$this->threads['replies'] + 1;
                $pages = ceil($postcount / $perpage);

                if ($this->page > $pages) {
                    $this->page = 1;
                }
                if ($this->page) {
                    $start = ($this->page - 1) * $perpage;
                } else {
                    $start = 0;
                    $this->page = 1;
                }

                $pids = [];
                // Fetch list of post IDs to be shown
                $query = $this->db->simple_select('posts', 'pid', "tid='{$this->id}' AND visible='1'", ['order_by' => 'dateline', 'limit_start' => $start, 'limit' => $perpage]);
                while ($post = $this->db->fetch_array($query)) {
                    $pids[$post['pid']] = $post['pid'];
                }

                if (empty($pids)) {
                    return $this->archive->archive_error($this->lang->error_invalidthread);
                }

                $this->archive->archive_multipage($postcount, $perpage, $this->page, "{$this->bb->base_url}thread-$this->id");

                $pids = implode(',', $pids);

                if ($pids && $this->bb->settings['enableattachments'] == 1) {
                    // Build attachments cache
                    $query = $this->db->simple_select('attachments', '*', "pid IN ({$pids})");
                    while ($attachment = $this->db->fetch_array($query)) {
                        $acache[$attachment['pid']][$attachment['aid']] = $attachment;
                    }
                }

                // Start fetching the posts
                $query = $this->db->query('
			SELECT u.*, u.username AS userusername, p.*
			FROM ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
			WHERE p.pid IN ({$pids})
			ORDER BY p.dateline
		");
                while ($post = $this->db->fetch_array($query)) {
                    $post['date'] = $this->time->formatDate('relative', $post['dateline']);
                    if ($post['userusername']) {
                        $post['username'] = $post['userusername'];
                    }

                    // Parse the message
                    $parser_options = [
                        'allow_html' => $forum['allowhtml'],
                        'allow_mycode' => $forum['allowmycode'],
                        'allow_smilies' => $forum['allowsmilies'],
                        'allow_imgcode' => $forum['allowimgcode'],
                        'allow_videocode' => $forum['allowvideocode'],
                        'me_username' => $post['username'],
                        'filter_badwords' => 1
                    ];
                    if ($post['smilieoff'] == 1) {
                        $parser_options['allow_smilies'] = 0;
                    }

                    $post['message'] = $this->parser->parse_message($post['message'], $parser_options);

                    // Is there an attachment in this post?
                    if ($this->bb->settings['enableattachments'] == 1 && isset($acache[$post['pid']]) && is_array($acache[$post['pid']])) {
                        foreach ($acache[$post['pid']] as $aid => $attachment) {
                            $post['message'] = str_replace("[attachment={$attachment['aid']}]", "[<a href=\"" . $this->bb->settings['bburl'] . "/attachment?aid={$attachment['aid']}\">attachment={$attachment['aid']}</a>]", $post['message']);
                        }
                    }

                    // Damn thats a lot of parsing, now to determine which username to show..
                    if ($post['userusername']) {
                        $post['username'] = $post['userusername'];
                    }
                    $post['username'] = $this->user->build_profile_link($post['username'], $post['uid']);

                    $this->plugins->runHooks('archive_thread_post');

                    // Finally show the post
                    echo "<div class=\"post\">\n<div class=\"header\">\n<div class=\"author\"><h2>{$post['username']}</h2></div>";
                    echo "<div class=\"dateline\">{$post['date']}</div>\n</div>\n<div class=\"message\">{$post['message']}</div>\n</div>\n";
                }
                $this->archive->archive_multipage($postcount, $perpage, $this->page, "{$this->bb->base_url}thread-$this->id");

                $this->plugins->runHooks('archive_thread_end');
                $this->archive->archive_footer();
                break;

            // Display a category or a forum.
            case 'forum':
                // Check if we have permission to view this forum
                $this->forumpermissions = $this->forum->forum_permissions($this->forums['fid']);
                if ($this->forumpermissions['canview'] != 1) {
                    return $this->archive->archive_error_no_permission();
                }

                $this->archive->check_forum_password_archive($this->forums['fid']);

                $useronly = '';
                if (isset($this->forumpermissions['canonlyviewownthreads']) &&
                    $this->forumpermissions['canonlyviewownthreads'] == 1
                ) {
                    $useronly = "AND uid={$this->user->uid}";
                }

                // Paginate this forum
                $query = $this->db->simple_select('threads', 'COUNT(tid) AS threads', "fid='{$this->id}' AND visible='1' {$useronly}");
                $threadcount = $this->db->fetch_field($query, 'threads');

                // Build the navigation
                $this->bb->build_forum_breadcrumb($this->forums['fid']);//, 1);

                // No threads and not a category? Error!
                if ($this->forums['type'] != 'c') {
                    if ($this->forumpermissions['canviewthreads'] != 1) {
                        $this->archive->archive_header(strip_tags($this->forums['name']), $this->forums['name'], $this->forum->get_forum_link($this->id, $this->page));
                        return $this->archive->archive_error($this->lang->error_nopermission);
                    }

                    if ($threadcount < 1 && $this->forumpermissions['canviewthreads'] == 1) {
                        $this->archive->archive_header(strip_tags($this->forums['name']), $this->forums['name'], $this->forum->get_forum_link($this->id, $this->page));
                        return $this->archive->archive_error($this->lang->error_nothreads);
                    }
                }

                // Build the archive header.
                $this->archive->archive_header(strip_tags($this->forums['name']), $this->forums['name'], $this->forum->get_forum_link($this->id, $this->page), 1);

                $this->plugins->runHooks('archive_forum_start');

                if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                    $this->bb->settings['threadsperpage'] = 20;
                }

                $perpage = $this->bb->settings['threadsperpage'];
                $pages = ceil($threadcount / $perpage);
                if ($this->page > $pages) {
                    $this->page = 1;
                }

                if ($this->page > 0) {
                    $start = ($this->page - 1) * $perpage;
                } else {
                    $start = 0;
                    $this->page = 1;
                }

                // Decide what type of listing to show.
                if ($this->forums['type'] == 'f') {
                    echo "<div class=\"listing\">\n<div class=\"header\"><h2>{$this->forums['name']}</h2></div>\n";
                } elseif ($this->forums['type'] == 'c') {
                    echo "<div class=\"listing\">\n<div class=\"header\"><h2>{$this->forums['name']}</h2></div>\n";
                }

                // Show subforums.
                $query = $this->db->simple_select('forums', 'COUNT(fid) AS subforums', "pid='{$this->id}'");
                $subforumcount = $this->db->fetch_field($query, 'subforums');
                if ($subforumcount > 0) {
                    echo "<div class=\"forumlist\">\n";
                    echo "<h3>{$this->lang->subforums}</h3>\n";
                    echo "<ol>\n";
                    $forums = $this->build_archive_forumbits($this->forums['fid']);
                    echo $forums;
                    echo "</ol>\n</div>\n";
                }

                $this->archive->archive_multipage($threadcount, $perpage, $this->page, "{$this->bb->base_url}forum-$this->id");

                // Get the announcements if the forum is not a category.
                if ($this->forums['type'] == 'f') {
                    $sql = $this->forum->build_parent_list($this->forums['fid'], 'fid', 'OR', $this->forums['parentlist']);
                    $time = TIME_NOW;
                    $query = $this->db->simple_select('announcements', '*', "startdate < '{$time}' AND (enddate > '{$time}' OR enddate=0) AND ({$sql} OR fid='-1')");
                    if ($this->db->num_rows($query) > 0) {
                        echo "<div class=\"announcementlist\">\n";
                        echo "<h3>{$this->lang->forumbit_announcements}</h3>";
                        echo "<ol>\n";
                        while ($announcement = $this->db->fetch_array($query)) {
                            $announcement['subject'] = $this->parser->parse_badwords($announcement['subject']);
                            echo "<li><a href=\"{$this->bb->base_url}announcement-{$announcement['aid']}.html\">" . htmlspecialchars_uni($announcement['subject']) . "</a></li>";
                        }
                        echo "</ol>\n</div>\n";
                    }
                }

                // Get the stickies if the forum is not a category.
                if ($this->forums['type'] == 'f') {
                    $options = [
                        'order_by' => 'sticky, lastpost',
                        'order_dir' => 'desc',
                        'limit_start' => $start,
                        'limit' => $perpage
                    ];
                    $query = $this->db->simple_select('threads', '*', "fid='{$this->id}' AND visible='1' AND sticky='1' AND closed NOT LIKE 'moved|%' {$useronly}", $options);
                    if ($this->db->num_rows($query) > 0) {
                        echo "<div class=\"threadlist\">\n";
                        echo "<h3>{$this->lang->forumbit_stickies}</h3>";
                        echo "<ol>\n";
                        while ($sticky = $this->db->fetch_array($query)) {
                            $sticky['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($sticky['subject']));
                            if ($sticky['replies'] != 1) {
                                $lang_reply_text = $this->lang->archive_replies;
                            } else {
                                $lang_reply_text = $this->lang->archive_reply;
                            }

                            $this->plugins->runHooks('archive_forum_thread');

                            $sticky['replies'] = $this->parser->formatNumber($sticky['replies']);

                            echo "<li><a href=\"{$this->bb->base_url}thread-{$sticky['tid']}.html\">{$sticky['subject']}</a>";
                            echo "<span class=\"replycount\"> ({$sticky['replies']} {$lang_reply_text})</span></li>";
                        }
                        echo "</ol>\n</div>\n";
                    }
                }

                // Get the threads if the forum is not a category.
                if ($this->forums['type'] == 'f') {
                    $options = [
                        'order_by' => 'sticky, lastpost',
                        'order_dir' => 'desc',
                        'limit_start' => $start,
                        'limit' => $perpage
                    ];
                    $query = $this->db->simple_select('threads', '*', "fid='{$this->id}' AND visible='1' AND sticky='0' AND closed NOT LIKE 'moved|%' {$useronly}", $options);
                    if ($this->db->num_rows($query) > 0) {
                        echo "<div class=\"threadlist\">\n";
                        echo "<h3>{$this->lang->forumbit_threads}</h3>";
                        echo "<ol>\n";
                        while ($thread = $this->db->fetch_array($query)) {
                            $thread['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($thread['subject']));
                            if ($thread['replies'] != 1) {
                                $lang_reply_text = $this->lang->archive_replies;
                            } else {
                                $lang_reply_text = $this->lang->archive_reply;
                            }

                            $this->plugins->runHooks('archive_forum_thread');

                            $thread['replies'] = $this->parser->formatNumber($thread['replies']);

                            echo "<li><a href=\"{$this->bb->base_url}thread-{$thread['tid']}.html\">{$thread['subject']}</a>";
                            echo "<span class=\"replycount\"> ({$thread['replies']} {$lang_reply_text})</span></li>";
                        }
                        echo "</ol>\n</div>\n";
                    }
                }

                echo "</div>\n";

                $this->archive->archive_multipage($threadcount, $perpage, $this->page, "{$this->bb->base_url}forum-$this->id");

                $this->plugins->runHooks('archive_forum_end');

                $this->archive->archive_footer();
                break;

            // Display the board home.
            case 'index':
                // Build our forum listing
                $forums = $this->build_archive_forumbits(0);

                $this->archive->archive_header('', $this->bb->settings['bbname_orig'], $this->bb->settings['bburl']);

                $this->plugins->runHooks('archive_index_start');

                echo "<div class=\"listing forumlist\">\n<div class=\"header\">{$this->bb->settings['bbname']}</div>\n<div class=\"forums\">\n<ul>\n";
                echo $forums;
                echo "\n</ul>\n</div>\n</div>";

                $this->plugins->runHooks('archive_index_end');

                $this->archive->archive_footer();
                break;
            default:
                header('HTTP/1.0 404 Not Found');
                switch ($this->action2) {
                    case 'announcement':
                        return $this->archive->archive_error($this->lang->error_invalidannouncement);
                    case 'thread':
                        return $this->archive->archive_error($this->lang->error_invalidthread);
                    case 'forum':
                        return $this->archive->archive_error($this->lang->error_invalidforum);
                    default:
                        return $this->archive->archive_error($this->lang->archive_not_found);
                }
        }

        $this->plugins->runHooks('archive_end');
    }

    /**
     * Gets a list of forums and possibly subforums.
     *
     * @param int $pid The parent forum to get the childforums for.
     * @return array Array of information regarding the child forums of this parent forum
     */
    private function build_archive_forumbits($pid = 0)
    {
        //global $db, $forumpermissions, $mybb, $base_url;

        // Sort out the forum cache first.
        static $fcache;
        if (!is_array($fcache)) {
            // Fetch forums
            $query = $this->db->simple_select('forums', '*', "active!=0 AND password=''", ['order_by' => 'pid, disporder']);
            while ($forum = $this->db->fetch_array($query)) {
                $fcache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
            }
            $this->forumpermissions = $this->forum->forum_permissions();
        }

        $forums = '';

        // Start the process.
        if (is_array($fcache[$pid])) {
            foreach ($fcache[$pid] as $key => $main) {
                foreach ($main as $key => $forum) {
                    $perms = $this->forumpermissions[$forum['fid']];
                    if (($perms['canview'] == 1 || $this->bb->settings['hideprivateforums'] == 0) && $forum['active'] != 0) {
                        if ($forum['linkto']) {
                            $forums .= "<li><a href=\"{$forum['linkto']}\">{$forum['name']}</a>";
                        } elseif ($forum['type'] == 'c') {
                            $forums .= "<li><strong><a href=\"{$this->bb->base_url}forum-{$forum['fid']}.html\">{$forum['name']}</a></strong>";
                        } else {
                            $forums .= "<li><a href=\"{$this->bb->base_url}forum-{$forum['fid']}.html\">{$forum['name']}</a>";
                        }
                        if (!empty($fcache[$forum['fid']])) {
                            $forums .= "\n<ol>\n";
                            $forums .= $this->build_archive_forumbits($forum['fid']);
                            $forums .= "</ol>\n";
                        }
                        $forums .= "</li>\n";
                    }
                }
            }
        }
        return $forums;
    }
}
