<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Attachment extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        if ($this->bb->settings['enableattachments'] != 1) {
            $this->bb->error($this->lang->attachments_disabled);
        }

        // Find the AID we're looking for
        if (isset($this->bb->input['thumbnail'])) {
            $aid = $this->bb->getInput('thumbnail', 0);
        } else {
            $aid = $this->bb->getInput('aid', 0);
        }

        $pid = $this->bb->getInput('pid', 0);

        // Select attachment data from database
        if ($aid) {
//            $query = $this->db->simple_select('attachments', '*', "aid='{$aid}'");
            $attachment = \RunBB\Models\Attachment::where('aid', '=', $aid)->first()->toArray();
        } else {
            $attachment = \RunBB\Models\Attachment::where('pid', '=', $pid)->first()->toArray();
//            $query = $this->db->simple_select('attachments', '*', "pid='{$pid}'");
        }
//        $attachment = $this->db->fetch_array($query);

        $this->plugins->runHooks('attachment_start');

        if (!$attachment) {
            $this->bb->error($this->lang->error_invalidattachment);
        }

        if ($attachment['thumbnail'] == '' && isset($this->bb->input['thumbnail'])) {
            $this->bb->error($this->lang->error_invalidattachment);
        }

        $pid = $attachment['pid'];

        // Don't check the permissions on preview
        if ($pid || $attachment['uid'] != $this->user->uid) {
            $post = $this->post->get_post($pid);
            $thread = $this->thread->get_thread($post['tid']);

            if (!$thread && !isset($this->bb->input['thumbnail'])) {
                $this->bb->error($this->lang->error_invalidthread);
            }
            $fid = $thread['fid'];

            // Get forum info
            //$forum = $this->forum->get_forum($fid);

            // Permissions
            $forumpermissions = $this->forum->forum_permissions($fid);

            if ($forumpermissions['canview'] == 0 ||
                $forumpermissions['canviewthreads'] == 0 ||
                (isset($forumpermissions['canonlyviewownthreads']) &&
                    $forumpermissions['canonlyviewownthreads'] != 0 &&
                    $thread['uid'] != $this->user->uid) ||
                ($forumpermissions['candlattachments'] == 0 && !isset($this->bb->input['thumbnail']))
            ) {
//                if($this->request->isXhr()) {
                    $attachment = [
                        'aid' => 0,
                        'pid' => 0,
//                        'posthash' => '',
                        'uid' => 0,
                        'filename' => 'noperm.png',
                        'filetype' => 'image/png',
                        'filesize' => 5586,
                        'attachname' => 'noperm.png',
//                        'downloads' => 3,
//                        'dateuploaded' => 1480723883,
                        'visible' => 1,
//                        'thumbnail' => "noperm.png",
                        'noupdate' => true
                    ];
//                } else {
//                    return $this->bb->error_no_permission();
//                }
            }

            // Error if attachment is invalid or not visible
            if (!$attachment['attachname'] ||
                (!$this->user->is_moderator($fid, 'canviewunapprove') &&
                    ($attachment['visible'] != 1 ||
                        $thread['visible'] != 1 ||
                        $post['visible'] != 1))
            ) {
                $this->bb->error($this->lang->error_invalidattachment);
            }
        }
        // Only increment the download count if this is not a thumbnail
        if (!isset($this->bb->input['thumbnail']) && !isset($attachment['noupdate'])) {
            \RunBB\Models\Attachment::where('aid', $attachment['aid'])
                ->update(['downloads' => $attachment['downloads'] + 1]);
        }

        // basename isn't UTF-8 safe. This is a workaround.
        $attachment['filename'] = ltrim(basename(' ' . $attachment['filename']));

        $this->plugins->runHooks('attachment_end');

        if (isset($this->bb->input['thumbnail'])) {
            if (!file_exists(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $attachment['thumbnail'])) {
                $this->bb->error($this->lang->error_invalidattachment);
            }

            $ext = get_extension($attachment['thumbnail']);
            switch ($ext) {
                case 'gif':
                    $type = 'image/gif';
                    break;
                case 'bmp':
                    $type = 'image/bmp';
                    break;
                case 'png':
                    $type = 'image/png';
                    break;
                case 'jpg':
                case 'jpeg':
                case 'jpe':
                    $type = 'image/jpeg';
                    break;
                default:
                    $type = 'image/unknown';
                    break;
            }

            header("Content-disposition: filename=\"{$attachment['filename']}\"");
            header('Content-type: ' . $type);
            $thumb = DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $attachment['thumbnail'];
            header('Content-length: ' . @filesize($thumb));
            $handle = fopen($thumb, 'rb');
            while (!feof($handle)) {
                echo fread($handle, 8192);
            }
            fclose($handle);
        } else {
            if (!file_exists(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $attachment['attachname'])) {
                $this->bb->error($this->lang->error_invalidattachment);
            }

            switch ($attachment['filetype']) {
                case 'application/pdf':
                case 'image/bmp':
                case 'image/gif':
                case 'image/jpeg':
                case 'image/pjpeg':
                case 'image/png':
                case 'text/plain':
                    header('Content-type: ' . $attachment['filetype']);
                    $disposition = 'inline';
                    //$disposition = 'attachment';
                    break;

                default:
                    $filetype = $attachment['filetype'];

                    if (!$filetype) {
                        $filetype = 'application/force-download';
                    }

                    header('Content-type: ' . $filetype);
                    $disposition = 'attachment';
            }

            if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie') !== false) {
                header('Content-disposition: attachment; filename=' . $attachment['filename']);
            } else {
                header("Content-disposition: {$disposition}; filename=\"{$attachment['filename']}\"");
            }

            if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'msie 6.0') !== false) {
                header('Expires: -1');
            }

            header("Content-length: {$attachment['filesize']}");
            header('Content-range: bytes=0-' . ($attachment['filesize'] - 1) . '/' . $attachment['filesize']);
            $handle = fopen(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $attachment['attachname'], 'rb');
            while (!feof($handle)) {
                echo fread($handle, 8192);
            }
            fclose($handle);
            exit;
        }
    }
}
