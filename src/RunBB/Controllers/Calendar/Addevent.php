<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Addevent extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() === 'exit') {
                return;
            }
        }
        $query = $this->db->simple_select('calendars', '*', "cid='".$this->bb->input['calendar']."'");
        $calendar = $this->db->fetch_array($query);

        // Invalid calendar?
        if (!$calendar['cid']) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar or post events?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);
        if ($calendar_permissions['canviewcalendar']  != 1 || $calendar_permissions['canaddevents']  != 1) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb(htmlspecialchars_uni($calendar['name']), get_calendar_link($calendar['cid']));
        $this->bb->add_breadcrumb($this->lang->nav_addevent);

        $this->plugins->runHooks('calendar_addevent_start');

        // If MyCode is on for this forum and the MyCode editor is enabled inthe Admin CP,
        // draw the code buttons and smilie inserter.
        if ($this->bb->settings['bbcodeinserter'] != 0 &&
        (!$this->user->uid || $this->user->showcodebuttons != 0) &&
        $calendar['allowmycode'] == 1) {
            $this->bb->codebuttons = $this->editor->build_mycode_inserter('message', $calendar['allowsmilies']);
            if ($calendar['allowsmilies'] == 1) {
                $this->bb->smilieinserter = $this->editor->build_clickable_smilies();
            }
        }

        // Previous selections
        $name = $description = '';
        if (isset($this->bb->input['name'])) {
            $name = htmlspecialchars_uni($this->bb->getInput('name'));
        }
        $this->view->offsetSet('name', $name);

        if (isset($this->bb->input['description'])) {
            $description = htmlspecialchars_uni($this->bb->getInput('description'));
        }
        $this->view->offsetSet('description', $description);

        $single_month = $start_month = $end_month = $repeats_sel = $repeats_3_days =
        $repeats_4_occurance = $repeats_4_weekday = $repeats_5_month = $repeats_5_occurance =
        $repeats_5_weekday = $repeats_5_month2 = [];
        foreach (range(1, 12) as $number) {
            $single_month[$number] = $start_month[$number] =
            $end_month[$number] = $repeats_5_month[$number] =
            $repeats_5_month2[$number] = '';
        }
        foreach (range(1, 5) as $number) {
            $repeats_sel[$number] = '';
        }
        foreach (range(0, 6) as $number) {
            $repeats_3_days[$number] = $repeats_4_weekday[$number] = $repeats_5_weekday[$number] = '';
        }
        foreach (range(1, 4) as $number) {
            $repeats_4_occurance[$number] = $repeats_5_occurance[$number] = '';
        }
        $repeats_4_occurance['last'] = $repeats_5_occurance['last'] = '';
        $repeats_4_type = [1 => '', 2 => ''];
        $repeats_5_type = [1 => '', 2 => ''];

        if ($this->bb->request_method == 'post') {
            $single_day = $this->bb->getInput('single_day', 0);
            $single_month[$this->bb->getInput('single_month', 0)] = ' selected="selected"';
            $single_year = $this->bb->getInput('single_year', 0);
            $start_day = $this->bb->getInput('start_day', 0);
            $start_month[$this->bb->getInput('start_month', 0)] = ' selected="selected"';
            $start_year = $this->bb->getInput('start_year', 0);
            $start_time = htmlspecialchars_uni($this->bb->getInput('start_time'));
            $end_day = $this->bb->getInput('end_day', 0);
            $end_month[$this->bb->getInput('end_month', 0)] = ' selected="selected"';
            $end_year = $this->bb->getInput('end_year', 0);
            $end_time = htmlspecialchars_uni($this->bb->getInput('end_time'));
            if ($this->bb->getInput('type') == 'single') {
                $type_single = ' checked="checked"';
                $type_ranged = '';
                $type = 'single';
            } else {
                $type_ranged = ' checked="checked"';
                $type_single = '';
                $type = 'ranged';
            }
            if (!empty($this->bb->input['repeats'])) {
                $repeats_sel[$this->bb->getInput('repeats', 0)] = ' selected="selected"';
            }
            $repeats_1_days = $this->bb->getInput('repeats_1_days', 0);
            $repeats_3_weeks = $this->bb->getInput('repeats_3_weeks', 0);
            foreach ($this->bb->getInput('repeats_3_days', []) as $day => $val) {
                if ($val != 1) {
                    continue;
                }
                $day = (int)$day;
                $repeats_3_days[$day] = ' checked="checked"';
            }
            $repeats_4_type = [];
            if ($this->bb->getInput('repeats_4_type', 0) == 1) {
                $repeats_4_type[1] = ' checked="checked"';
                $repeats_4_type[2] = '';
            } else {
                $repeats_4_type[2] = ' checked="checked"';
                $repeats_4_type[1] = '';
            }
            $repeats_4_day = $this->bb->getInput('repeats_4_day', 0);
            $repeats_4_months = $this->bb->getInput('repeats_4_months', 0);
            $repeats_4_occurance[$this->bb->getInput('repeats_4_occurance')] = ' selected="selected"';
            $repeats_4_weekday[$this->bb->getInput('repeats_4_weekday', 0)] = ' selected="selected"';
            $repeats_4_months2 = $this->bb->getInput('repeats_4_months2', 0);
            if ($this->bb->getInput('repeats_5_type', 0) == 1) {
                $repeats_5_type[1] = ' checked="checked"';
            } else {
                $repeats_5_type[2] = ' checked="checked"';
            }
            $repeats_5_day = $this->bb->getInput('repeats_5_day', 0);
            $repeats_5_month[$this->bb->getInput('repeats_5_month', 0)] = ' selected="selected"';
            $repeats_5_years = $this->bb->getInput('repeats_5_years', 0);
            $repeats_5_occurance[$this->bb->getInput('repeats_5_occurance')] = ' selected="selected"';
            $repeats_5_weekday[$this->bb->getInput('repeats_5_weekday', 0)] = ' selected="selected"';
            $repeats_5_month2[$this->bb->getInput('repeats_5_month2', 0)] = ' selected="selected"';
            $repeats_5_years2 = $this->bb->getInput('repeats_5_years2', 0);

            $timezone = $this->bb->getInput('timezone', 0);
        } else {
            if (!empty($this->bb->input['day'])) {
                $single_day = $start_day = $end_day = $this->bb->getInput('day', 0);
            } else {
                $single_day = $start_day = $end_day = $this->time->formatDate('j');
            }
            if (!empty($this->bb->input['month'])) {
                $month = $this->bb->getInput('month', 0);
            } else {
                $month = $this->time->formatDate('n');
            }
            $single_month[$month] = $start_month[$month] = $end_month[$month] = ' selected="selected"';
            if (!empty($this->bb->input['year'])) {
                $single_year = $start_year = $end_year = $this->bb->getInput('year', 0);
            } else {
                $single_year = $start_year = $end_year = $this->time->formatDate('Y');
            }
            $start_time = $end_time = '';
            $type_single = ' checked="checked"';
            $type_ranged = '';
            $type = 'single';
            $repeats_1_days = 1;
            $repeats_3_weeks = 1;
            $repeats_4_type[1] = ' checked="checked"';
            $repeats_4_day = 1;
            $repeats_4_months = 1;
            $repeats_4_occurance[1] = ' selected="selected"';
            $repeats_4_weekday[0] = ' selected="selected"';
            $repeats_4_months2 = 1;
            $repeats_5_type[1] = ' checked="checked"';
            $repeats_5_day = 1;
            $repeats_5_month[1] = ' selected="selected"';
            $repeats_5_years = 1;
            $repeats_5_occurance[1] = ' selected="selected"';
            $repeats_5_weekday[0] = ' selected="selected"';
            $repeats_5_month2[1] = ' selected="selected"';
            $repeats_5_years2 = 1;
            $timezone = $this->user->timezone;
        }
        $this->view->offsetSet('type_single', $type_single);
        $this->view->offsetSet('type_ranged', $type_ranged);
        $this->view->offsetSet('start_time', $start_time);
        $this->view->offsetSet('end_time', $end_time);
        $this->view->offsetSet('single_month', $single_month);
        $this->view->offsetSet('start_month', $start_month);
        $this->view->offsetSet('end_month', $end_month);
        $this->view->offsetSet('repeats_sel', $repeats_sel);
        $this->view->offsetSet('repeats_1_days', $repeats_1_days);
        $this->view->offsetSet('repeats_3_weeks', $repeats_3_weeks);
        $this->view->offsetSet('repeats_3_days', $repeats_3_days);
        $this->view->offsetSet('repeats_4_type', $repeats_4_type);
        $this->view->offsetSet('repeats_4_day', $repeats_4_day);
        $this->view->offsetSet('repeats_4_months', $repeats_4_months);
        $this->view->offsetSet('repeats_4_occurance', $repeats_4_occurance);
        $this->view->offsetSet('repeats_4_weekday', $repeats_4_weekday);
        $this->view->offsetSet('repeats_4_months2', $repeats_4_months2);
        $this->view->offsetSet('repeats_5_type', $repeats_5_type);
        $this->view->offsetSet('repeats_5_day', $repeats_5_day);
        $this->view->offsetSet('repeats_5_month', $repeats_5_month);
        $this->view->offsetSet('repeats_5_years', $repeats_5_years);
        $this->view->offsetSet('repeats_5_occurance', $repeats_5_occurance);
        $this->view->offsetSet('repeats_5_weekday', $repeats_5_weekday);
        $this->view->offsetSet('repeats_5_month2', $repeats_5_month2);
        $this->view->offsetSet('repeats_5_years2', $repeats_5_years2);

        $single_years = $start_years = $end_years = '';
        // Construct option list for years
        for ($year = $this->time->formatDate('Y'); $year < ($this->time->formatDate('Y') + 5); ++$year) {
            if ($year == $single_year) {
                $selected = 'selected="selected"';
                $single_years .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
                //ev al("\$single_years .= \"".$this->templates->get('calendar_year')."\";");
            } else {
                $selected = '';
                $single_years .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
                //ev al("\$single_years .= \"".$this->templates->get('calendar_year')."\";");
            }

            if ($year == $start_year) {
                $selected = 'selected="selected"';
                $start_years .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
                //ev al("\$start_years .= \"".$this->templates->get('calendar_year')."\";");
            } else {
                $selected = '';
                $start_years .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
                //ev al("\$start_years .= \"".$this->templates->get('calendar_year')."\";");
            }

            if ($year == $end_year) {
                $selected = 'selected="selected"';
                $end_years .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
                //ev al("\$end_years .= \"".$this->templates->get('calendar_year')."\";");
            } else {
                $selected = '';
                $end_years .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
                //ev al("\$end_years .= \"".$this->templates->get('calendar_year')."\";");
            }
        }
        $this->view->offsetSet('single_years', $single_years);
        $this->view->offsetSet('start_years', $start_years);
        $this->view->offsetSet('end_years', $end_years);

        $single_days = $start_days = $end_days = '';
        // Construct option list for days
        for ($day = 1; $day <= 31; ++$day) {
            if ($day == $single_day) {
                $selected = 'selected="selected"';
                $single_days .= "<option value=\"{$day}\" {$selected}>{$day}</option>";
                //ev al("\$single_days .= \"".$this->templates->get('calendar_day')."\";");
            } else {
                $selected = '';
                $single_days .= "<option value=\"{$day}\" {$selected}>{$day}</option>";
                //ev al("\$single_days .= \"".$this->templates->get('calendar_day')."\";");
            }

            if ($day == $start_day) {
                $selected = 'selected="selected"';
                $start_days .= "<option value=\"{$day}\" {$selected}>{$day}</option>";
                //ev al("\$start_days .= \"".$this->templates->get('calendar_day')."\";");
            } else {
                $selected = '';
                $start_days .= "<option value=\"{$day}\" {$selected}>{$day}</option>";
                //ev al("\$start_days .= \"".$this->templates->get('calendar_day')."\";");
            }

            if ($day == $end_day) {
                $selected = 'selected="selected"';
                $end_days .= "<option value=\"{$day}\" {$selected}>{$day}</option>";
                //ev al("\$end_days .= \"".$this->templates->get('calendar_day')."\";");
            } else {
                $selected = '';
                $end_days .= "<option value=\"{$day}\" {$selected}>{$day}</option>";
                //ev al("\$end_days .= \"".$this->templates->get('calendar_day')."\";");
            }
        }
        $this->view->offsetSet('single_days', $single_days);
        $this->view->offsetSet('start_days', $start_days);
        $this->view->offsetSet('end_days', $end_days);
        $this->view->offsetSet('timezones', $this->time->buildTimezoneSelect('timezone', $timezone));

        if ($this->bb->getInput('ignoretimezone', 0) == 1) {
            $ignore_timezone = 'checked="checked"';
        } else {
            $ignore_timezone = '';
        }
        $this->view->offsetSet('ignore_timezone', $ignore_timezone);

        if ($this->bb->getInput('private', 0) == 1) {
            $privatecheck = 'checked="checked"';
        } else {
            $privatecheck = '';
        }
        $this->view->offsetSet('privatecheck', $privatecheck);

        $select_calendar = [];
        // Build calendar select
        $calendar_permissions = $this->calendar->get_calendar_permissions();
        $query = $this->db->simple_select('calendars', '*', '', ['order_by' => 'name', 'order_dir' => 'asc']);
        while ($calendar_option = $this->db->fetch_array($query)) {
            if ($calendar_permissions[$calendar['cid']]['canviewcalendar'] == 1) {
                $calendar_option['name'] = htmlspecialchars_uni($calendar_option['name']);
                if ($calendar_option['cid'] == $this->bb->input['calendar']) {
                    $selected = ' selected="selected"';
                } else {
                    $selected = '';
                }
                $select_calendar[]=[
                'cid' => $calendar_option['cid'],
                'selected' => $selected,
                'name' => $calendar_option['name']
                ];
                //ev al("\$select_calendar .= \"".$this->templates->get('calendar_select')."\";");
            }
        }
        $this->view->offsetSet('select_calendar', $select_calendar);

    //    if(count($select_calendar) > 1)
    //    {
    //      //ev al("\$calendar_select .= \"".$this->templates->get('calendar_addevent_calendarselect')."\";");
    //    }
    //    else
    //    {
    //      //ev al("\$calendar_select .= \"".$this->templates->get('calendar_addevent_calendarselect_hidden')."\";");
    //    }

        if (!empty($this->event_errors)) {
            $this->event_errors = '';
        }
        $this->view->offsetSet('event_errors', $this->event_errors);

        $this->plugins->runHooks('calendar_addevent_end');

        //ev al("\$addevent = \"".$this->templates->get('calendar_addevent')."\";");
        $this->bb->output_page();
        $this->view->render($response, '@forum/Calendar/addevent.html.twig');
    }

    public function doAddevent(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $query = $this->db->simple_select('calendars', '*', "cid='{$this->bb->input['calendar']}'");
        $calendar = $this->db->fetch_array($query);

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar or post events?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);
        if ($calendar_permissions['canviewcalendar'] != 1 || $calendar_permissions['canaddevents'] != 1) {
            return $this->bb->error_no_permission();
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key'));

        $this->plugins->runHooks('calendar_do_addevent_start');

        // Set up event handler.
        $eventhandler = new \RunBB\Handlers\DataHandlers\EventDataHandler($this->bb, 'insert');

        $this->bb->input['type'] = $this->bb->getInput('type');

        // Prepare an array for the eventhandler.
        $event = [
        'cid' => $calendar['cid'],
        'uid' => $this->user->uid,
        'name' => $this->bb->getInput('name'),
        'description' => $this->bb->getInput('description'),
        'private' => $this->bb->getInput('private', 0),
        'type' => $this->bb->input['type']
        ];

        // Now we add in our date/time info depending on the type of event
        if ($this->bb->input['type'] == 'single') {
            $event['start_date'] = [
            'day' => $this->bb->getInput('single_day', 0),
            'month' => $this->bb->getInput('single_month', 0),
            'year' => $this->bb->getInput('single_year', 0)
            ];
            $event['repeats'] = '';
        } elseif ($this->bb->input['type'] == 'ranged') {
            $event['start_date'] = [
            'day' => $this->bb->getInput('start_day', 0),
            'month' => $this->bb->getInput('start_month', 0),
            'year' => $this->bb->getInput('start_year', 0),
            'time' => $this->bb->getInput('start_time')
            ];
            $event['end_date'] = [
            'day' => $this->bb->getInput('end_day', 0),
            'month' => $this->bb->getInput('end_month', 0),
            'year' => $this->bb->getInput('end_year', 0),
            'time' => $this->bb->getInput('end_time')
            ];
            $event['timezone'] = $this->bb->getInput('timezone');
            $event['ignoretimezone'] =    $this->bb->getInput('ignoretimezone', 0);
            $repeats = [];
            switch ($this->bb->input['repeats']) {
                case 1:
                    $repeats['repeats'] = 1;
                    $repeats['days'] = $this->bb->getInput('repeats_1_days', 0);
                    break;
                case 2:
                    $repeats['repeats'] = 2;
                    break;
                case 3:
                    $repeats['repeats'] = 3;
                    $repeats['weeks'] = $this->bb->getInput('repeats_3_weeks', 0);
                    $this->bb->input['repeats_3_days'] = $this->bb->getInput('repeats_3_days', []);
                    ksort($this->bb->input['repeats_3_days']);
                    $days = [];
                    foreach ($this->bb->input['repeats_3_days'] as $weekday => $value) {
                        if ($value != 1) {
                            continue;
                        }
                        $days[] = $weekday;
                    }
                    $repeats['days'] = $days;
                    break;
                case 4:
                    $repeats['repeats'] = 4;
                    if ($this->bb->getInput('repeats_4_type', 0) == 1) {
                        $repeats['day'] = $this->bb->getInput('repeats_4_day', 0);
                        $repeats['months'] = $this->bb->getInput('repeats_4_months', 0);
                    } else {
                        $repeats['months'] = $this->bb->getInput('repeats_4_months2', 0);
                        $repeats['occurance'] = $this->bb->getInput('repeats_4_occurance');
                        $repeats['weekday'] = $this->bb->getInput('repeats_4_weekday', 0);
                    }
                    break;
                case 5:
                    $repeats['repeats'] = 5;
                    if ($this->bb->getInput('repeats_5_type', 0) == 1) {
                        $repeats['day'] = $this->bb->getInput('repeats_5_day', 0);
                        $repeats['month'] = $this->bb->getInput('repeats_5_month', 0);
                        $repeats['years'] = $this->bb->getInput('repeats_5_years', 0);
                    } else {
                        $repeats['occurance'] = $this->bb->getInput('repeats_5_occurance');
                        $repeats['weekday'] = $this->bb->getInput('repeats_5_weekday', 0);
                        $repeats['month'] = $this->bb->getInput('repeats_5_month2', 0);
                        $repeats['years'] = $this->bb->getInput('repeats_5_years', 0);
                    }
                    break;
                default:
                    $repeats['repeats'] = 0;
            }
            $event['repeats'] = $repeats;
        }

        $eventhandler->set_data($event);

        // Now let the eventhandler do all the hard work.
        if (!$eventhandler->validateEvent()) {
            $this->event_errors = $eventhandler->get_friendly_errors();
            $this->event_errors = $this->bb->inline_error($this->event_errors);
            //$this->bb->input['action'] = 'addevent';
            $this->index($request, $response, true);
        } else {
            $details = $eventhandler->insertEvent();
            $this->plugins->runHooks('calendar_do_addevent_end');
            if ($details['visible'] == 1) {
                $this->bb->redirect(get_event_link($details['eid']), $this->lang->redirect_eventadded);
            } else {
                $this->bb->redirect(get_calendar_link($event['cid']), $this->lang->redirect_eventadded_moderation);
            }
        }
    }
}
