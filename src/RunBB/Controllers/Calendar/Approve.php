<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Approve extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $query = $this->db->simple_select('events', '*', "eid='{$this->bb->input['eid']}'");
        $event = $this->db->fetch_array($query);

        if (!$event) {
            $this->bb->error($this->lang->error_invalidevent);
        }

        $query = $this->db->simple_select('calendars', '*', "cid='{$event['cid']}'");
        $calendar = $this->db->fetch_array($query);

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);
        if ($calendar_permissions['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }

        if ($calendar_permissions['canmoderateevents'] != 1) {
            return $this->bb->error_no_permission();
        }

        $updated_event = [
        'visible' => 1
        ];

        $this->plugins->runHooks('calendar_approve_start');

        $this->db->update_query('events', $updated_event, "eid='{$event['eid']}'");

        $this->plugins->runHooks('calendar_approve_end');

        return $this->bb->redirect(get_event_link($event['eid']), $this->lang->redirect_eventapproved);
    }
}
