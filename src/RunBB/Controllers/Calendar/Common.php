<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{

    protected function init()
    {
        // Load global language phrases
        $this->lang->load('calendar');

        if ($this->bb->settings['enablecalendar'] == 0) {
            $this->bb->error($this->lang->calendar_disabled);
            return 'exit';
        }

        if ($this->bb->usergroup['canviewcalendar'] == 0) {
            $this->bb->error_no_permission();
            return 'exit';
        }
        $this->event_errors = '';

        $this->plugins->runHooks('calendar_start');
        $this->view->offsetSet('monthnames', $this->calendar->monthnames);

        // Make navigation
        $this->bb->add_breadcrumb($this->lang->nav_calendar, $this->bb->settings['bburl'] . '/calendar');

        $this->bb->input['calendar'] = $this->bb->getInput('calendar', 0);
        $calendars = $this->calendar->cache_calendars();

        //$calendar_jump = '';
        if (count($calendars) > 1) {
            //$calendar_jump =
            $this->calendar->build_calendar_jump($this->bb->input['calendar']);
        }
    }
}
