<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Dayview extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Showing a particular calendar
        if ($this->bb->input['calendar']) {
//            $query = $this->db->simple_select('calendars', '*', "cid='{$this->bb->input['calendar']}'");
//            $calendar = $this->db->fetch_array($query);
            $calendar = \RunBB\Models\Calendar::where('cid', '=', $this->bb->input['calendar'])
                ->first()->toArray();
        } // Showing the default calendar
        else {
//            $query = $this->db->simple_select('calendars', '*', "disporder='1'");
//            $calendar = $this->db->fetch_array($query);
            $calendar = \RunBB\Models\Calendar::where('disporder', '=', '1')
                ->first()->toArray();
        }

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);
        if ($calendar_permissions['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }
        $this->view->offsetSet('calendar', $calendar);

        // Incoming year?
        $this->bb->input['year'] = $this->bb->getInput('year', 0);
        if ($this->bb->input['year'] && $this->bb->input['year'] <= $this->time->formatDate('Y') + 5) {
            $year = $this->bb->input['year'];
        } else {
            $year = $this->time->formatDate('Y');
        }
        $this->view->offsetSet('year', $year);

        // Then the month
        $this->bb->input['month'] = $this->bb->getInput('month', 0);
        if ($this->bb->input['month'] >= 1 && $this->bb->input['month'] <= 12) {
            $month = $this->bb->input['month'];
        } else {
            $month = $this->time->formatDate('n');
        }
        $this->view->offsetSet('month', $month);

        // And day?
        $this->bb->input['day'] = $this->bb->getInput('day', 0);
        if ($this->bb->input['day'] && $this->bb->input['day'] <= gmdate('t', gmmktime(0, 0, 0, $month, 1, $year))) {
            $day = $this->bb->input['day'];
        } else {
            $day = $this->time->formatDate('j');
        }

        $this->bb->add_breadcrumb(htmlspecialchars_uni($calendar['name']), get_calendar_link($calendar['cid']));
        $this->bb->add_breadcrumb($day . ' ' . $this->calendar->monthnames[$month] . ' ' . $year, get_calendar_link($calendar['cid'], $year, $month, $day));
        $this->view->offsetSet('curr_month', $this->calendar->monthnames[$month]);

        $this->plugins->runHooks('calendar_dayview_start');

        // Load Birthdays for this day
        $birthday_list = $birthdays = '';
        if ($calendar['showbirthdays']) {
            $birthdays2 = $this->calendar->get_birthdays($month, $day);
            $bdayhidden = 0;
            if (is_array($birthdays2)) {
                $comma = '';
                foreach ($birthdays2 as $birthday) {
                    if ($birthday['birthdayprivacy'] == 'all') {
                        $bday = explode('-', $birthday['birthday']);
                        if ($bday[2] && $bday[2] < $year) {
                            $age = $year - $bday[2];
                            $age = ' (' . $this->lang->sprintf($this->lang->years_old, $age) . ')';
                        } else {
                            $age = '';
                        }

                        $birthday['username'] = $this->user->format_name($birthday['username'], $birthday['usergroup'], $birthday['displaygroup']);
                        $birthday['profilelink'] = $this->user->build_profile_link($birthday['username'], $birthday['uid']);
                        $birthday_list .= "{$comma}{$birthday['profilelink']}{$age}";
                        //ev al('\$birthday_list .= \''.$this->templates->get('calendar_dayview_birthdays_bday', 1, 0).'\';');
                        $comma = $this->lang->comma;
                    } else {
                        ++$bdayhidden;
                    }
                }
            }
            if ($bdayhidden > 0) {
                if ($birthday_list) {
                    $birthday_list .= ' - ';
                }
                $birthday_list .= $bdayhidden . ' ' . $this->lang->birthdayhidden;
            }
            if ($birthday_list) {
                $bdaydate = $this->time->formatDate($this->bb->settings['dateformat'], gmmktime(0, 0, 0, $month, $day, $year), 0, 0);
                $this->lang->birthdays_on_day = $this->lang->sprintf($this->lang->birthdays_on_day, $bdaydate);
                //ev al('\$birthdays = \''.$this->templates->get('calendar_dayview_birthdays', 1, 0).'\';');
            }
        }
        $this->view->offsetSet('birthday_list', $birthday_list);

        // So now we fetch events for this month
        $start_timestamp = gmmktime(0, 0, 0, $month, $day, $year);
        $end_timestamp = gmmktime(23, 59, 59, $month, $day, $year);

        $events_cache = $this->calendar->get_events(
            $calendar,
            $start_timestamp,
            $end_timestamp,
            $calendar_permissions['canmoderateevents']
        );

        $events = [];
        if (isset($events_cache["$day-$month-$year"]) && is_array($events_cache["$day-$month-$year"])) {
            foreach ($events_cache["$day-$month-$year"] as $event) {
                $event['name'] = htmlspecialchars_uni($event['name']);

                $event_parser_options = [
                    'allow_html' => $calendar['allowhtml'],
                    'allow_mycode' => $calendar['allowmycode'],
                    'allow_smilies' => $calendar['allowsmilies'],
                    'allow_imgcode' => $calendar['allowimgcode'],
                    'allow_videocode' => $calendar['allowvideocode']
                ];

                if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                    ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
                ) {
                    $event_parser_options['allow_imgcode'] = 0;
                }

                if (($this->user->showvideos != 1 && $this->user->uid != 0) ||
                    ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
                ) {
                    $event_parser_options['allow_videocode'] = 0;
                }

                $event['description'] = $this->parser->parse_message($event['description'], $event_parser_options);

                // Get the usergroup
                if ($event['username']) {
                    if (!$event['displaygroup']) {
                        $event['displaygroup'] = $event['usergroup'];
                    }
                    $user_usergroup = $this->bb->groupscache[$event['displaygroup']];
                } else {
                    $user_usergroup = $this->bb->groupscache[1];
                }

                $titles_cache = $this->cache->read('usertitles');

                // Event made by registered user
                if ($event['uid'] > 0 && $event['username']) {
                    $event['profilelink'] = $this->user->build_profile_link(
                        $this->user->format_name($event['username'], $event['usergroup'], $event['displaygroup']),
                        $event['uid']
                    );

                    if (trim($event['usertitle']) != '') {
                        // Do nothing, no need for an extra variable..
                    } elseif ($user_usergroup['usertitle'] != '') {
                        $event['usertitle'] = $user_usergroup['usertitle'];
                    } elseif (is_array($titles_cache) && !$user_usergroup['usertitle']) {
                        reset($titles_cache);
                        foreach ($titles_cache as $title) {
                            if ($event['postnum'] >= $title['posts']) {
                                $event['usertitle'] = $title['title'];
                                $event['stars'] = $title['stars'];
                                $event['starimage'] = $title['starimage'];
                                break;
                            }
                        }
                    }

                    if ($user_usergroup['stars']) {
                        $event['stars'] = $user_usergroup['stars'];
                    }

                    if (empty($event['starimage'])) {
                        $event['starimage'] = $user_usergroup['starimage'];
                    }

                    $event['userstars'] = '';
                    for ($i = 0; $i < $event['stars']; ++$i) {
                        $event['userstars'] .= "<img src=\"{$this->bb->asset_url}/{$event['starimage']}\" border=\"0\" alt=\"*\" />";
                    }

                    if ($event['userstars'] && $event['starimage'] && $event['stars']) {
                        $event['userstars'] .= '<br />';
                    }
                } // Created by a guest or an unknown user
                else {
                    if (!$event['username']) {
                        $event['username'] = $this->lang->guest;
                    }

                    $event['username'] = $event['username'];
                    $event['profilelink'] = $this->user->format_name($event['username'], 1);

                    if ($user_usergroup['usertitle']) {
                        $event['usertitle'] = $user_usergroup['usertitle'];
                    } else {
                        $event['usertitle'] = $this->lang->guest;
                    }
                    $event['userstars'] = '';
                }

                $event['usertitle'] = htmlspecialchars_uni($event['usertitle']);

                if ($event['ignoretimezone'] == 0) {
                    $offset = $event['timezone'];
                } else {
                    $offset = $this->user->timezone;
                }

                $event['starttime_user'] = $event['starttime'] + $offset * 3600;

                // Events over more than one day
                $time_period = '';
                if ($event['endtime'] > 0 && $event['endtime'] != $event['starttime']) {
                    $event['endtime_user'] = $event['endtime'] + $offset * 3600;
                    $start_day = gmmktime(0, 0, 0, gmdate('n', $event['starttime_user']), gmdate('j', $event['starttime_user']), gmdate('Y', $event['starttime_user']));
                    $end_day = gmmktime(0, 0, 0, gmdate('n', $event['endtime_user']), gmdate('j', $event['endtime_user']), gmdate('Y', $event['endtime_user']));
                    $start_time = gmdate('Hi', $event['starttime_user']);
                    $end_time = gmdate('Hi', $event['endtime_user']);

                    // Event only runs over one day
                    if ($start_day == $end_day && $event['repeats']['repeats'] == 0) {
                        $time_period = gmdate($this->bb->settings['dateformat'], $event['starttime_user']);
                        // Event runs all day
                        if ($start_time != 0000 && $end_time != 2359) {
                            $time_period .= $this->lang->comma . gmdate(
                                $this->bb->settings['timeformat'],
                                $event['starttime_user']
                            ) . ' - ' . gmdate($this->bb->settings['timeformat'], $event['endtime_user']);
                        } else {
                            $time_period .= $this->lang->comma . $this->lang->all_day;
                        }
                    } else {
                        $time_period = gmdate(
                            $this->bb->settings['dateformat'],
                            $event['starttime_user']
                        ) . ', ' . gmdate($this->bb->settings['timeformat'], $event['starttime_user']);
                        $time_period .= ' - ';
                        $time_period .= gmdate(
                            $this->bb->settings['dateformat'],
                            $event['endtime_user']
                        ) . ', ' . gmdate($this->bb->settings['timeformat'], $event['endtime_user']);
                    }
                } else {
                    $time_period = gmdate($this->bb->settings['dateformat'], $event['starttime_user']);
                }

                $repeats = $this->calendar->fetch_friendly_repetition($event);

                $edit_event = $event_class = '';
                $showModeratorOptions = false;
                if ($calendar_permissions['canmoderateevents'] == 1 ||
                    ($this->user->uid > 0 && $this->user->uid == $event['uid'])
                ) {
                    $showModeratorOptions = true;
                    if ($calendar_permissions['canmoderateevents'] == 1) {
                        if ($event['visible'] == 1) {
                            $approve = $this->lang->unapprove_event;
                            $approve_value = 'unapprove';
                        } else {
                            $approve = $this->lang->approve_event;
                            $approve_value = 'approve';
                        }
                    }
                    if ($event['visible'] == 0) {
                        $event_class = ' trow_shaded';
                    }
                }
                $events[] = [
                    'event' => $event,
                    'time_period' => $time_period,
                    'event_class' => $event_class,
                    'showModeratorOptions' => $showModeratorOptions,
                    'post_code' => $this->bb->post_code,
                    'approve_value' => $approve_value,
                    'approve' => $approve,
                    'repeats' => $repeats
                ];
            }
        }
        $this->view->offsetSet('events', $events);

        $yearsel = [];
        for ($year_sel = $this->time->formatDate('Y'); $year_sel < ($this->time->formatDate('Y') + 5); ++$year_sel) {
            $yearsel[] = [
                'year_sel' => $year_sel
            ];
        }
        $this->view->offsetSet('yearsel', $yearsel);

        if (!$events) {
            $this->lang->no_events = $this->lang->sprintf($this->lang->no_events, $this->bb->settings['bburl'], $calendar['cid'], $day, $month, $year);
        }

        // Now output the page
        $this->plugins->runHooks('calendar_dayview_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Calendar/dayview.html.twig');
    }
}
