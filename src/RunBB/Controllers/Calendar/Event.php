<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Event extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
//        $query = $this->db->query('
//		SELECT u.*, e.*
//		FROM ' . TABLE_PREFIX . 'events e
//		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=e.uid)
//		WHERE e.eid='{$this->bb->input['eid']}'
//	");
//        $event = $this->db->fetch_array($query);
        $event = \RunBB\Models\Event::where('events.eid', '=', $this->bb->input['eid'])
            ->leftJoin('users', 'users.uid', '=', 'events.uid')
            ->first([
                'users.*',
                'events.*'
            ])
            ->toArray();

        if (!$event || ($event['private'] == 1 && $event['uid'] != $this->user->uid)) {
            $this->bb->error($this->lang->error_invalidevent);
        }

//        $query = $this->db->simple_select('calendars', '*', "cid='{$event['cid']}'");
//        $calendar = $this->db->fetch_array($query);
        $calendar = \RunBB\Models\Calendar::where('cid', '=', $event['cid'])
            ->first()
            ->toArray();

        $this->view->offsetSet('calendar', $calendar);

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);
        if ($calendar_permissions['canviewcalendar'] != 1 ||
            ($calendar_permissions['canmoderateevents'] != 1 &&
                $event['visible'] == 0)
        ) {
            return $this->bb->error_no_permission();
        }

        $event['name'] = htmlspecialchars_uni($event['name']);

        $this->bb->add_breadcrumb(htmlspecialchars_uni($calendar['name']), get_calendar_link($calendar['cid']));
        $this->bb->add_breadcrumb($event['name'], get_event_link($event['eid']));

        $this->plugins->runHooks('calendar_event_start');

        $event_parser_options = [
            'allow_html' => $calendar['allowhtml'],
            'allow_mycode' => $calendar['allowmycode'],
            'allow_smilies' => $calendar['allowsmilies'],
            'allow_imgcode' => $calendar['allowimgcode'],
            'allow_videocode' => $calendar['allowvideocode']
        ];

        if (($this->user->showimages != 1 && $this->user->uid != 0) ||
            ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
        ) {
            $event_parser_options['allow_imgcode'] = 0;
        }

        if (($this->user->showvideos != 1 && $this->user->uid != 0) ||
            ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
        ) {
            $event_parser_options['allow_videocode'] = 0;
        }

        $event['description'] = $this->parser->parse_message($event['description'], $event_parser_options);

        // Get the usergroup
        if ($event['username']) {
            if (!$event['displaygroup']) {
                $event['displaygroup'] = $event['usergroup'];
            }
            $user_usergroup = $this->bb->groupscache[$event['displaygroup']];
        } else {
            $user_usergroup = $this->bb->groupscache[1];
        }

        $titles_cache = $this->cache->read('usertitles');

        // Event made by registered user
        if ($event['uid'] > 0 && $event['username']) {
            $event['profilelink'] = $this->user->build_profile_link($this->user->format_name(
                $event['username'],
                $event['usergroup'],
                $event['displaygroup']
            ), $event['uid']);

            if (trim($event['usertitle']) != '') {
                // Do nothing, no need for an extra variable..
            } elseif ($user_usergroup['usertitle'] != '') {
                $event['usertitle'] = $user_usergroup['usertitle'];
            } elseif (is_array($titles_cache) && !$user_usergroup['usertitle']) {
                reset($titles_cache);
                foreach ($titles_cache as $title) {
                    if ($event['postnum'] >= $title['posts']) {
                        $event['usertitle'] = $title['title'];
                        $event['stars'] = $title['stars'];
                        $event['starimage'] = $title['starimage'];
                        break;
                    }
                }
            }

            if ($user_usergroup['stars']) {
                $event['stars'] = $user_usergroup['stars'];
            }

            if (empty($event['starimage'])) {
                $event['starimage'] = $user_usergroup['starimage'];
            }
            $event['starimage'] = str_replace('{theme}', $this->bb->theme['imgdir'], $event['starimage']);

            $event['userstars'] = '';
            for ($i = 0; $i < $event['stars']; ++$i) {
                $event['userstars'] .= "<img src=\"{$this->bb->asset_url}/{$event['starimage']}\" border=\"0\" alt=\"*\" />";
            }

            if ($event['userstars'] && $event['starimage'] && $event['stars']) {
                $event['userstars'] .= '<br />';
            }
        } // Created by a guest or an unknown user
        else {
            if (!$event['username']) {
                $event['username'] = $this->lang->guest;
            }

            $event['profilelink'] = $this->user->format_name($event['username'], 1);

            if ($user_usergroup['usertitle']) {
                $event['usertitle'] = $user_usergroup['usertitle'];
            } else {
                $event['usertitle'] = $this->lang->guest;
            }
            $event['userstars'] = '';
        }

        $event['usertitle'] = htmlspecialchars_uni($event['usertitle']);

        if ($event['ignoretimezone'] == 0) {
            $offset = $event['timezone'];
        } else {
            $offset = $this->user->timezone;
        }

        $event['starttime_user'] = $event['starttime'] + $offset * 3600;

        // Events over more than one day
        $time_period = '';
        if ($event['endtime'] > 0 && $event['endtime'] != $event['starttime']) {
            $event['endtime_user'] = $event['endtime'] + $offset * 3600;
            $start_day = gmmktime(0, 0, 0, gmdate('n', $event['starttime_user']), gmdate('j', $event['starttime_user']), gmdate('Y', $event['starttime_user']));
            $end_day = gmmktime(0, 0, 0, gmdate('n', $event['endtime_user']), gmdate('j', $event['endtime_user']), gmdate('Y', $event['endtime_user']));
            $start_time = gmdate('Hi', $event['starttime_user']);
            $end_time = gmdate('Hi', $event['endtime_user']);

            $event['repeats'] = my_unserialize($event['repeats']);

            // Event only runs over one day
            if ($start_day == $end_day && $event['repeats']['repeats'] == 0) {
                $time_period = gmdate($this->bb->settings['dateformat'], $event['starttime_user']);
                // Event runs all day
                if ($start_time != 0000 && $end_time != 2359) {
                    $time_period .= $this->lang->comma . gmdate($this->bb->settings['timeformat'], $event['starttime_user']) . ' - ' . gmdate($this->bb->settings['timeformat'], $event['endtime_user']);
                } else {
                    $time_period .= $this->lang->comma . $this->lang->all_day;
                }
            } else {
                $time_period = gmdate($this->bb->settings['dateformat'], $event['starttime_user']) . ', ' . gmdate($this->bb->settings['timeformat'], $event['starttime_user']);
                $time_period .= ' - ';
                $time_period .= gmdate($this->bb->settings['dateformat'], $event['endtime_user']) . ', ' . gmdate($this->bb->settings['timeformat'], $event['endtime_user']);
            }
        } else {
            $time_period = gmdate($this->bb->settings['dateformat'], $event['starttime_user']);
        }
        $this->view->offsetSet('time_period', $time_period);

        $repeats = $this->calendar->fetch_friendly_repetition($event);
        $this->view->offsetSet('repeats', $repeats);

        $event_class = $approve_value = $approve = '';
        $this->bb->showEditButton = $this->bb->showModOptions = false;
        if ($calendar_permissions['canmoderateevents'] == 1 || ($this->user->uid > 0 && $this->user->uid == $event['uid'])) {
            $this->bb->showEditButton = true;
            if ($calendar_permissions['canmoderateevents'] == 1) {
                $this->bb->showModOptions = true;
                if ($event['visible'] == 1) {
                    $approve = $this->lang->unapprove_event;
                    $approve_value = 'unapprove';
                } else {
                    $approve = $this->lang->approve_event;
                    $approve_value = 'approve';
                }
            }

            if ($event['visible'] == 0) {
                $event_class = ' trow_shaded';
            }
        }
        $this->view->offsetSet('event_class', $event_class);
        $this->view->offsetSet('approve_value', $approve_value);
        $this->view->offsetSet('approve', $approve);
        $this->view->offsetSet('event', $event);


        $month = $this->time->formatDate('n');
        $this->view->offsetSet('month', $month);
        $this->view->offsetSet('curr_month', $this->calendar->monthnames[$month]);

        $yearsel = [];
        for ($year_sel = $this->time->formatDate('Y'); $year_sel < ($this->time->formatDate('Y') + 5); ++$year_sel) {
            $yearsel[] = [
                'year_sel' => $year_sel
            ];
        }
        $this->view->offsetSet('yearsel', $yearsel);

        $this->plugins->runHooks('calendar_event_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Calendar/event.html.twig');
    }
}
