<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Showing a particular calendar
        if ($this->bb->input['calendar']) {
//            $query = $this->db->simple_select('calendars', '*', "cid='{$this->bb->input['calendar']}'");
//            $calendar = $this->db->fetch_array($query);
            $calendar = \RunBB\Models\Calendar::where('cid', '=', $this->bb->input['calendar'])
                ->first()
                ->toArray();
        } // Showing the default calendar
        else {
//            $query = $this->db->simple_select('calendars', '*', '', array('order_by' => 'disporder', 'limit' => 1));
//            $calendar = $this->db->fetch_array($query);
            //FIXME check this !!!!
            $calendar = \RunBB\Models\Calendar::orderBy('disporder', 'asc')
                ->first()
                ->toArray();
        }

        // Invalid calendar?
        if (!$calendar['cid']) {
            $this->bb->error($this->lang->invalid_calendar);
        }
        $this->view->offsetSet('calendar', $calendar);

        // Do we have permission to view this calendar?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);

        if ($calendar_permissions['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('calendar_main_view');

        // Incoming year?
        $this->bb->input['year'] = $this->bb->getInput('year', 0);
        if ($this->bb->input['year'] && $this->bb->input['year'] <= $this->time->formatDate('Y') + 5) {
            $year = $this->bb->input['year'];
        } else {
            $year = $this->time->formatDate('Y');
        }
        $this->view->offsetSet('year', $year);

        // Then the month
        $this->bb->input['month'] = $this->bb->getInput('month', 0);
        if ($this->bb->input['month'] >= 1 && $this->bb->input['month'] <= 12) {
            $month = $this->bb->input['month'];
        } else {
            $month = $this->time->formatDate('n');
        }
        $this->view->offsetSet('month', $month);

        $this->bb->add_breadcrumb(htmlspecialchars_uni($calendar['name']), get_calendar_link($calendar['cid']));
        $this->bb->add_breadcrumb($this->calendar->monthnames[$month] . ' ' . $year, get_calendar_link($calendar['cid'], $year, $month));

        $this->view->offsetSet('curr_month', $this->calendar->monthnames[$month]);

        $next_month = $this->calendar->get_next_month($month, $year);
        $prev_month = $this->calendar->get_prev_month($month, $year);
        $this->view->offsetSet('next_month', $this->calendar->get_next_month($month, $year));
        $this->view->offsetSet('prev_month', $this->calendar->get_prev_month($month, $year));

        $this->view->offsetSet('prev_link', get_calendar_link($calendar['cid'], $prev_month['year'], $prev_month['month']));
        $this->view->offsetSet('next_link', get_calendar_link($calendar['cid'], $next_month['year'], $next_month['month']));

        // Start constructing the calendar
        $weekdays = $this->calendar->fetch_weekday_structure($calendar['startofweek']);
        $month_start_weekday = gmdate('w', gmmktime(0, 0, 0, $month, $calendar['startofweek'] + 1, $year));
        $prev_month_days = gmdate('t', gmmktime(0, 0, 0, $prev_month['month'], 1, $prev_month['year']));

        // This is if we have days in the previous month to show
        if ($month_start_weekday != $weekdays[0] || $calendar['startofweek'] != 0) {
            $prev_days = $day = gmdate('t', gmmktime(0, 0, 0, $prev_month['month'], 1, $prev_month['year']));
            $day -= array_search(($month_start_weekday), $weekdays);
            $day += $calendar['startofweek'] + 1;
            if ($day > $prev_month_days + 1) {
                // Go one week back
                $day -= 7;
            }
            $calendar_month = $prev_month['month'];
            $calendar_year = $prev_month['year'];
        } else {
            $day = $calendar['startofweek'] + 1;
            $calendar_month = $month;
            $calendar_year = $year;
        }

        // So now we fetch events for this month (nb, cache events for past month,
        // current month and next month for mini calendars too)
        $start_timestamp = gmmktime(0, 0, 0, $calendar_month, $day, $calendar_year);
        $num_days = gmdate('t', gmmktime(0, 0, 0, $month, 1, $year));

        $month_end_weekday = gmdate('w', gmmktime(0, 0, 0, $month, $num_days, $year));
        $next_days = 6 - $month_end_weekday + $calendar['startofweek'];

        // More than a week? Go one week back
        if ($next_days >= 7) {
            $next_days -= 7;
        }
        if ($next_days > 0) {
            $end_timestamp = gmmktime(23, 59, 59, $next_month['month'], $next_days, $next_month['year']);
        } else {
            // We don't need days from the next month
            $end_timestamp = gmmktime(23, 59, 59, $month, $num_days, $year);
        }

        $events_cache = $this->calendar->get_events($calendar, $start_timestamp, $end_timestamp, $calendar_permissions['canmoderateevents']);

        // Fetch birthdays
        if ($calendar['showbirthdays']) {
            $bday_months = [$month, $prev_month['month'], $next_month['month']];
            $birthdays = $this->calendar->get_birthdays($bday_months);
        }

        $today = $this->time->formatDate('dnY');
        $weekday_headers = [];
        // Build weekday headers
        foreach ($weekdays as $weekday) {
            //$weekday_name = $this->calendar->fetch_weekday_name($weekday);
            $weekday_headers[] = [
                'weekday_name' => $this->calendar->fetch_weekday_name($weekday)
            ];
            //ev al("\$weekday_headers .= \"".$this->templates->get('calendar_weekdayheader')."\";");
        }
        $this->view->offsetSet('weekday_headers', $weekday_headers);

        $in_month = 0;
        $day_bits = $calendar_rows = [];
        for ($row = 0; $row < 6; ++$row) { // Iterate weeks (each week gets a row)
            foreach ($weekdays as $weekday_id => $weekday) {
                // Current month always starts on 1st row
                if ($row == 0 && $day == $calendar['startofweek'] + 1) {
                    $in_month = 1;
                    $calendar_month = $month;
                    $calendar_year = $year;
                } elseif ($calendar_month == $prev_month['month'] && $day > $prev_month_days) {
                    $day = 1;
                    $in_month = 1;
                    $calendar_month = $month;
                    $calendar_year = $year;
                } elseif ($day > $num_days && $calendar_month != $prev_month['month']) {
                    $in_month = 0;
                    $calendar_month = $next_month['month'];
                    $calendar_year = $next_month['year'];
                    $day = 1;
                    if ($calendar_month == $month) {
                        $in_month = 1;
                    }
                }

                if ($weekday_id == 0) {
                    $week_stamp = gmmktime(0, 0, 0, $calendar_month, $day, $calendar_year);
                    $week_link = get_calendar_week_link($calendar['cid'], $week_stamp);
                }

                if ($weekday_id == 0 && $calendar_month == $next_month['month']) {
                    break;
                }

                    $day_events = '';
                    // Any events on this specific day?
                if (is_array($events_cache) && array_key_exists("{$day}-{$calendar_month}-{$calendar_year}", $events_cache)) {
                    $total_events = count($events_cache["$day-$calendar_month-$calendar_year"]);
                    if ($total_events > $calendar['eventlimit'] && $calendar['eventlimit'] != 0) {
                        if ($total_events > 1) {
                            $day_events = "<div style=\"margin-bottom: 4px;\"><a href=\"" . get_calendar_link($calendar['cid'], $calendar_year, $calendar_month, $day) . "\" class=\"smalltext\">{$total_events} {$this->lang->events}</a></div>\n";
                        } else {
                            $day_events = "<div style=\"margin-bottom: 4px;\"><a href=\"" . get_calendar_link($calendar['cid'], $calendar_year, $calendar_month, $day) . "\" class=\"smalltext\">1 {$this->lang->event}</a></div>\n";
                        }
                    } else {
                        foreach ($events_cache["$day-$calendar_month-$calendar_year"] as $event) {
                            $event['eventlink'] = get_event_link($event['eid']);
                            $event['fullname'] = htmlspecialchars_uni($event['name']);
                            if (my_strlen($event['name']) > 15) {
                                $event['name'] = my_substr($event['name'], 0, 15) . '...';
                            }
                            $event['name'] = htmlspecialchars_uni($event['name']);
                            if ($event['private'] == 1) {
                                $event_class = ' private_event';
                            } else {
                                $event_class = ' public_event';
                            }
                            if ($event['visible'] == 0) {
                                $event_class .= ' trow_shaded';
                            }
                            $day_events .= "<div style=\"margin-bottom: 4px;\" class=\"smalltext {$event_class}\"><a href=\"{$event['eventlink']}\" title=\"{$event['fullname']}\">{$event['name']}</a></div>";
                            //ev al("\$day_events .= \"".$this->templates->get('calendar_eventbit')."\";");
                        }
                    }
                }

                    // Birthdays on this day?
                    $birthday_lang = '';
                    $bday_count = '';
                if ($calendar['showbirthdays'] && is_array($birthdays) && array_key_exists($day - $calendar_month, $birthdays)) {
                    $bday_count = count($birthdays[$day - $calendar_month]);
                    if ($bday_count > 1) {
                        $birthday_lang = $this->lang->birthdays;
                    } else {
                        $birthday_lang = $this->lang->birthday;
                    }
                    $calendar['link'] = get_calendar_link($calendar['cid'], $calendar_year, $calendar_month, $day);
                    //ev al("\$day_birthdays = \"".$this->templates->get('calendar_weekrow_day_birthdays')."\";");
                }

                    $day_link = get_calendar_link($calendar['cid'], $calendar_year, $calendar_month, $day);

                    // Is the current day
                if ($day . $calendar_month . $year == $today && $month == $calendar_month) {
                    $day_class = 'trow_sep';
                } // Not in this month
                elseif ($in_month == 0) {
                    $day_class = 'trow1';
                } // Just a normal day in this month
                else {
                    $day_class = 'trow2';
                }
                    $day_bits[] = [
                    'class' => $day_class,
                    'link' => $day_link,
                    'day' => $day,
                    'bday_count' => $bday_count,
                    'birthday_lang' => $birthday_lang,
                    'events' => $day_events
                    ];
                    //ev al("\$day_bits .= \"".$this->templates->get('calendar_weekrow_day')."\";");
                    $day_events = '';
                    ++$day;
            }
            if ($day_bits) {
                $calendar_rows[] = [
                    'week_link' => $week_link,
                    'day_bits' => $day_bits,
                    'calendarlink' => isset($calendar['link']) ? $calendar['link'] : ''
                ];
                //ev al("\$calendar_rows .= \"".$this->templates->get('calendar_weekrow')."\";");
            }
            $day_bits = [];
        }
        $this->view->offsetSet('calendar_rows', $calendar_rows);

        $yearsel = [];
        for ($year_sel = $this->time->formatDate('Y'); $year_sel < ($this->time->formatDate('Y') + 5); ++$year_sel) {
            $yearsel[] = [
                'year_sel' => $year_sel
            ];
        }
        $this->view->offsetSet('yearsel', $yearsel);

        $this->plugins->runHooks('calendar_end');

        //ev al('\$calendar = \''.$this->templates->get('calendar').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Calendar/index.html.twig');
    }
}
