<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Move extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

//        $query = $this->db->simple_select('events', '*', "eid='{$this->bb->input['eid']}'");
//        $event = $this->db->fetch_array($query);
        $event = \RunBB\Models\Event::where('eid', '=', $this->bb->input['eid'])
            ->first()
            ->toArray();

        if (!$event) {
            $this->bb->error($this->lang->error_invalidevent);
        }

//        $query = $this->db->simple_select('calendars', '*', "cid='{$event['cid']}'");
//        $calendar = $this->db->fetch_array($query);
        $calendar = \RunBB\Models\Calendar::where('cid', '=', $event['cid'])
            ->first()
            ->toArray();

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar or post events?
        $calendar_permissions = $this->calendar->get_calendar_permissions();
        if ($calendar_permissions[$calendar['cid']]['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }

        if ($calendar_permissions[$calendar['cid']]['canmoderateevents'] != 1) {
            return $this->bb->error_no_permission();
        }

        $event['name'] = htmlspecialchars_uni($event['name']);

        $this->bb->add_breadcrumb(htmlspecialchars_uni($calendar['name']), get_calendar_link($calendar['cid']));
        $this->bb->add_breadcrumb($event['name'], get_event_link($event['eid']));
        $this->bb->add_breadcrumb($this->lang->nav_move_event);

        $this->bb->plugins->runHooks('calendar_move_start');

        $calendar_select = $selected = '';

        // Build calendar select
//        $query = $this->db->simple_select('calendars', '*', '', array('order_by' => 'name', 'order_dir' => 'asc'));
        $co = \RunBB\Models\Calendar::orderBy('name', 'asc')
            ->get()
            ->toArray();

//        while ($calendar_option = $this->db->fetch_array($query)) {
        foreach ($co as $calendar_option) {
            if ($calendar_permissions[$calendar['cid']]['canviewcalendar'] == 1) {
                $calendar_option['name'] = htmlspecialchars_uni($calendar_option['name']);
                $calendar_select .= "<option value=\"{$calendar_option['cid']}\"{$selected}>{$calendar_option['name']}</option>";
                //ev al('\$calendar_select .= \''.$this->bb->templates->get('calendar_select').'\';');
            }
        }
        $this->view->offsetSet('calendar_select', $calendar_select);
        $this->view->offsetSet('event', $event);

        $this->bb->plugins->runHooks('calendar_move_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Calendar/move.html.twig');
    }

    public function doMove(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $query = $this->db->simple_select('events', '*', "eid='{$this->bb->input['eid']}'");
        $event = $this->db->fetch_array($query);

        if (!$event) {
            $this->bb->error($this->lang->error_invalidevent);
        }

        $query = $this->db->simple_select('calendars', '*', "cid='{$event['cid']}'");
        $calendar = $this->db->fetch_array($query);

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar?
        $calendar_permissions = $this->calendar->get_calendar_permissions();
        if ($calendar_permissions[$calendar['cid']]['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }

        if ($calendar_permissions[$calendar['cid']]['canmoderateevents'] != 1) {
            return $this->bb->error_no_permission();
        }

        $query = $this->db->simple_select('calendars', '*', "cid='" . $this->bb->getInput('new_calendar', 0) . "'");
        $new_calendar = $this->db->fetch_array($query);

        if (!$new_calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        if ($calendar_permissions[$this->bb->input['new_calendar']]['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }

        $updated_event = [
            'cid' => $new_calendar['cid']
        ];

        $this->bb->plugins->runHooks('calendar_do_move_start');

        $this->db->update_query('events', $updated_event, "eid='{$event['eid']}'");

        $this->bb->plugins->runHooks('calendar_do_move_end');

        return $this->bb->redirect(get_event_link($event['eid']), $this->lang->redirect_eventmoved);
    }
}
