<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Calendar;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Weekview extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Showing a particular calendar
        if ($this->bb->input['calendar']) {
            $query = $this->db->simple_select('calendars', '*', "cid='{$this->bb->input['calendar']}'");
            $calendar = $this->db->fetch_array($query);
        } // Showing the default calendar
        else {
            $query = $this->db->simple_select('calendars', '*', "disporder='1'");
            $calendar = $this->db->fetch_array($query);
        }

        // Invalid calendar?
        if (!$calendar) {
            $this->bb->error($this->lang->invalid_calendar);
        }

        // Do we have permission to view this calendar?
        $calendar_permissions = $this->calendar->get_calendar_permissions($calendar['cid']);
        if ($calendar_permissions['canviewcalendar'] != 1) {
            return $this->bb->error_no_permission();
        }

        $weekdays = $this->calendar->fetch_weekday_structure($calendar['startofweek']);

        $yearsel = [];
        for ($year_sel = $this->time->formatDate('Y'); $year_sel < ($this->time->formatDate('Y') + 5); ++$year_sel) {
            $yearsel[] = [
                'year_sel' => $year_sel
            ];
        }
        $this->view->offsetSet('yearsel', $yearsel);

        // No incoming week, show THIS week
        if (empty($this->bb->input['week'])) {
            list($day, $month, $year) = explode('-', $this->time->formatDate('j-n-Y'));
            $php_weekday = gmdate('w', gmmktime(0, 0, 0, $month, $day, $year));
            $my_weekday = array_search($php_weekday, $weekdays);
            // So now we have the start day of this week to show
            $start_day = $day - $my_weekday;
            $this->bb->input['week'] = gmmktime(0, 0, 0, $month, $start_day, $year);
        } else {
            $this->bb->input['week'] = (int)str_replace('n', '-', $this->bb->getInput('week', ''));
            // No negative years please ;)
            if ($this->bb->input['week'] < -62167219200) {
                $this->bb->input['week'] = -62167219200;
            }
        }

        // This is where we've come from and where we're headed
        $week_from = explode('-', gmdate('j-n-Y', $this->bb->input['week']));
        $this->view->offsetSet('week_from', $week_from);

        $week_from_one = $week_from[1];
        //$friendly_week_from = gmdate($this->bb->settings['dateformat'], $this->bb->input['week']);
        $this->view->offsetSet('friendly_week_from', gmdate($this->bb->settings['dateformat'], $this->bb->input['week']));
        $week_to_stamp = gmmktime(0, 0, 0, $week_from[1], $week_from[0] + 6, $week_from[2]);
        $week_to = explode('-', gmdate('j-n-Y-t', $week_to_stamp));
        //$friendly_week_to = gmdate($this->bb->settings['dateformat'], $week_to_stamp);
        $this->view->offsetSet('friendly_week_to', gmdate($this->bb->settings['dateformat'], $week_to_stamp));
        $this->view->offsetSet('week_from_one', $this->calendar->monthnames[$week_from_one]);

        $this->bb->add_breadcrumb(htmlspecialchars_uni($calendar['name']), get_calendar_link($calendar['cid']));
        $this->bb->add_breadcrumb("{$this->calendar->monthnames[$week_from[1]]} {$week_from[2]}", get_calendar_link($calendar['cid'], $week_from[2], $week_from[1]));
        $this->bb->add_breadcrumb($this->lang->weekly_overview);

        $this->plugins->runHooks('calendar_weekview_start');

        // Establish if we have a month ending in this week
        if ($week_from[1] != $week_to[1]) {
            $different_months = true;
            $week_months = [[$week_from[1], $week_from[2]], [$week_to[1], $week_to[2]]];
            $bday_months = [$week_from[1], $week_to[1]];
        } else {
            $week_months = [[$week_from[1], $week_from[2]]];
            $bday_months = [$week_from[1]];
        }

        // Load Birthdays for this month
        if ($calendar['showbirthdays'] == 1) {
            $birthdays = $this->calendar->get_birthdays($bday_months);
        }

        // We load events for the entire month date range - for our mini calendars too
        $events_from = gmmktime(0, 0, 0, $week_from[1], 1, $week_from[2]);
        $events_to = gmmktime(0, 0, 0, $week_to[1], $week_to[3], $week_to[2]);

        $events_cache = $this->calendar->get_events($calendar, $events_from, $events_to, $calendar_permissions['canmoderateevents']);

        $today = $this->time->formatDate('dnY');

        $next_week = $this->bb->input['week'] + 604800;
        $this->view->offsetSet('next_link', get_calendar_week_link($calendar['cid'], $next_week));
        $prev_week = $this->bb->input['week'] - 604800;
        $this->view->offsetSet('prev_link', get_calendar_week_link($calendar['cid'], $prev_week));

        $weekday_date = $this->bb->input['week'];
        $day_bits = [];
        while ($weekday_date <= $week_to_stamp) {
            $weekday = gmdate('w', $weekday_date);
            $weekday_name = $this->calendar->fetch_weekday_name($weekday);
            $weekday_month = gmdate('n', $weekday_date);
            $weekday_year = gmdate('Y', $weekday_date);
            $weekday_day = gmdate('j', $weekday_date);

            // Special shading for today
            $day_shaded = '';
            if (gmdate('dnY', $weekday_date) == $today) {
                $day_shaded = ' trow_shaded';
            }

            $day_events = '';

            // Any events on this specific day?
            if (is_array($events_cache) && array_key_exists("{$weekday_day}-{$weekday_month}-{$weekday_year}", $events_cache)) {
                foreach ($events_cache["$weekday_day-$weekday_month-$weekday_year"] as $event) {
                    $event['eventlink'] = get_event_link($event['eid']);
                    $event['name'] = htmlspecialchars_uni($event['name']);
                    $event['fullname'] = $event['name'];
                    if (my_strlen($event['name']) > 50) {
                        $event['name'] = my_substr($event['name'], 0, 50) . '...';
                    }
                    // Events over more than one day
                    $time_period = '';
                    if ($event['endtime'] > 0 && $event['endtime'] != $event['starttime']) {
                        $start_day = gmmktime(0, 0, 0, gmdate('n', $event['starttime_user']), gmdate('j', $event['starttime_user']), gmdate('Y', $event['starttime_user']));
                        $end_day = gmmktime(0, 0, 0, gmdate('n', $event['endtime_user']), gmdate('j', $event['endtime_user']), gmdate('Y', $event['endtime_user']));
                        $start_time = gmdate('Hi', $event['starttime_user']);
                        $end_time = gmdate('Hi', $event['endtime_user']);
                        // Event only runs over one day
                        if ($start_day == $end_day || $event['repeats'] > 0) {
                            // Event runs all day
                            if ($start_time == 0000 && $end_time == 2359) {
                                $time_period = $this->lang->all_day;
                            } else {
                                $time_period = gmdate($this->bb->settings['timeformat'], $event['starttime_user']) . ' - ' . gmdate($this->bb->settings['timeformat'], $event['endtime_user']);
                            }
                        } // Event starts on this day
                        elseif ($start_day == $weekday_date) {
                            // Event runs all day
                            if ($start_time == 0000) {
                                $time_period = $this->lang->all_day;
                            } else {
                                $time_period = $this->lang->starts . gmdate($this->bb->settings['timeformat'], $event['starttime_user']);
                            }
                        } // Event finishes on this day
                        elseif ($end_day == $weekday_date) {
                            // Event runs all day
                            if ($end_time == 2359) {
                                $time_period = $this->lang->all_day;
                            } else {
                                $time_period = $this->lang->finishes . gmdate($this->bb->settings['timeformat'], $event['endtime_user']);
                            }
                        } // Event is in the middle
                        else {
                            $time_period = $this->lang->all_day;
                        }
                    }
                    $event_time = '';
                    if ($time_period) {
                        eval("\$event_time = \"" . $this->templates->get('calendar_weekview_day_event_time') . "\";");
                    }
                    if ($event['private'] == 1) {
                        $event_class = ' private_event';
                    } else {
                        $event_class = ' public_event';
                    }
                    if ($event['visible'] == 0) {
                        $event_class .= ' trow_shaded';
                    }
                    eval("\$day_events .= \"" . $this->templates->get('calendar_weekview_day_event') . "\";");
                }
            }

            // Birthdays on this day?
            $day_birthdays = $calendar_link = $birthday_lang = '';
            if ($calendar['showbirthdays'] && is_array($birthdays) && array_key_exists("{$weekday_day}-{$weekday_month}", $birthdays)) {
                $bday_count = count($birthdays["$weekday_day-$weekday_month"]);
                if ($bday_count > 1) {
                    $birthday_lang = $this->lang->birthdays;
                } else {
                    $birthday_lang = $this->lang->birthday;
                }

                $calendar_link = get_calendar_link($calendar['cid'], $weekday_year, $weekday_month, $weekday_day);
                $day_birthdays = "<a href=\"{$calendar_link}\">{$birthday_lang}: {$bday_count}</a><br />";
            }

            $day_link = get_calendar_link($calendar['cid'], $weekday_year, $weekday_month, $weekday_day);
            if (!isset($day_bits[$weekday_month])) {
                $day_bits[$weekday_month] = '';
            }
            $day_bits[$weekday_month][]=[
                'day_shaded' => $day_shaded,
                'weekday_day' => $weekday_day,
                'weekday_name' => $weekday_name,
                'day_birthdays' => $day_birthdays,
                'day_events' => $day_events
            ];
//            $day_bits[$weekday_month] .= "
//            <div class=\"list-group-item\">
//                <div class=\"media\">
//                    <div class=\"media-left $day_shaded\">
//                        <h2 class=\"media-heading\" style=\"text-align: center; padding: 8px;\">{$weekday_day}</h2>
//                    </div>
//                    <div class=\"media-body\"  style=\"padding-left: 15px\">
//                        <h4 class=\"media-heading\">{$weekday_name}</h4>
//                        <div class=\"media-bottom small\">{$day_birthdays}{$day_events}</div>
//                    </div>
//                </div>
//            </div>";
            //ev al("\$day_bits[$weekday_month] .= \"" . $this->templates->get('calendar_weekview_day') . "\";");
            $day_events = $day_birthdays = '';
            $weekday_date = gmmktime(0, 0, 0, $weekday_month, $weekday_day + 1, $weekday_year);
        }

        // Now we build our month headers
        $mini_calendars = [];
        $weekday_bits = [];
        foreach ($week_months as $month) {
            $weekday_month = $this->calendar->monthnames[$month[0]];
            $weekday_year = $month[1];
            // Fetch mini calendar for each month in this week
            $mini_calendars[] = $this->calendar->build_mini_calendar($calendar, $month[0], $weekday_year, $events_cache);
            // Fetch out the days for this month
            //$days = $day_bits[$month[0]];
            $weekday_bits[] = [
                'weekday_month' => $weekday_month,
                'weekday_year' => $weekday_year,
                'days' => $day_bits[$month[0]]
            ];
        }
        $this->view->offsetSet('mini_calendars', $mini_calendars);
        $this->view->offsetSet('weekday_bits', $weekday_bits);
        $this->view->offsetSet('calendar', $calendar);

        // Now output the page
        $this->plugins->runHooks('calendar_weekview_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Calendar/weekview.html.twig');
    }
}
