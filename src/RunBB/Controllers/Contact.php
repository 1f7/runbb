<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Contact extends AbstractController
{

    private function common()
    {
        // Load global language phrases
        $this->lang->load('contact');

        $this->plugins->runHooks('contact_start');

        // Make navigation
        $this->bb->add_breadcrumb($this->lang->contact, 'contact');

        if ($this->bb->settings['contact'] != 1 ||
            (!$this->user->uid && $this->bb->settings['contact_guests'] == 1)) {
            $this->bb->error_no_permission();
        }

        if ($this->bb->settings['contactemail']) {
            $this->contactemail = $this->bb->settings['contactemail'];
        } else {
            $this->contactemail = $this->bb->settings['adminemail'];
        }

        // Check group limits
        if ($this->bb->usergroup['maxemails'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $query = $this->db->simple_select(
                'maillogs',
                'COUNT(mid) AS sent_count',
                "{$user_check} AND dateline >= " . (TIME_NOW - (60 * 60 * 24))
            );
            $sent_count = $this->db->fetch_field($query, 'sent_count');
            if ($sent_count >= $this->bb->usergroup['maxemails']) {
                $this->lang->error_max_emails_day = $this->lang->sprintf(
                    $this->lang->error_max_emails_day,
                    $this->bb->usergroup['maxemails']
                );
                $this->bb->error($this->lang->error_max_emails_day, $this->lang->error);
                return false;
            }
        }

        // Check email flood control
        if ($this->bb->usergroup['emailfloodtime'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $timecut = TIME_NOW - $this->bb->usergroup['emailfloodtime'] * 60;

            $query = $this->db->simple_select(
                'maillogs',
                'mid, dateline',
                "{$user_check} AND dateline > '{$timecut}'",
                ['order_by' => 'dateline', 'order_dir' => 'DESC']
            );
            $last_email = $this->db->fetch_array($query);

            // Users last email was within the flood time, show the error
            if ($last_email['mid']) {
                $remaining_time = ($this->bb->usergroup['emailfloodtime'] * 60) - (TIME_NOW - $last_email['dateline']);

                if ($remaining_time == 1) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_second,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } elseif ($remaining_time < 60) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_seconds,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time
                    );
                } elseif ($remaining_time > 60 && $remaining_time < 120) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_minute,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } else {
                    $remaining_time_minutes = ceil($remaining_time / 60);
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_minutes,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time_minutes
                    );
                }

                $this->bb->error($this->lang->error_emailflooding, $this->lang->error);
                return false;
            }
        }

        $this->errors = [];

        $this->bb->input['message'] = trim_blank_chrs($this->bb->getInput('message', '', true));
        $this->bb->input['subject'] = trim_blank_chrs($this->bb->getInput('subject', '', true));
        $this->bb->input['email'] = trim_blank_chrs($this->bb->getInput('email', ''));
    }

    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->common() === false) {
                return;
            }
        }

        if (empty($this->errors)) {
            $this->errors = '';
        }

        // Generate CAPTCHA?
        $captcha = '';
        if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
            $post_captcha = new \RunBB\Core\Captcha($this->bb, true, 'post_captcha');

            if ($post_captcha->html) {
                $captcha = $post_captcha->html;
            }
        }
        $this->view->offsetSet('captcha', $captcha);

        $this->bb->input['subject'] = htmlspecialchars_uni($this->bb->input['subject']);
        $this->bb->input['message'] = htmlspecialchars_uni($this->bb->input['message']);

        if ($this->user->uid && !$this->bb->getInput('email', '')) {
            $this->bb->input['email'] = htmlspecialchars_uni($this->user->email);
        } else {
            $this->bb->input['email'] = htmlspecialchars_uni($this->bb->getInput('email', ''));
        }

        $this->view->offsetSet('errors', $this->errors);

        $this->plugins->runHooks('contact_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/contact.html.twig');
    }

    public function send(Request $request, Response $response)
    {
        $this->common();
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('contact_do_start');

        // Validate input
        if (empty($this->bb->input['subject'])) {
            $this->errors[] = $this->lang->contact_no_subject;
        }

        if (strlen($this->bb->input['subject']) > $this->bb->settings['contact_maxsubjectlength'] &&
            $this->bb->settings['contact_maxsubjectlength'] > 0) {
            $this->errors[] = $this->lang->sprintf(
                $this->lang->subject_too_long,
                $this->bb->settings['contact_maxsubjectlength'],
                strlen($this->bb->input['subject'])
            );
        }

        if (empty($this->bb->input['message'])) {
            $this->errors[] = $this->lang->contact_no_message;
        }

        if (strlen($this->bb->input['message']) > $this->bb->settings['contact_maxmessagelength'] &&
            $this->bb->settings['contact_maxmessagelength'] > 0) {
            $this->errors[] = $this->lang->sprintf(
                $this->lang->message_too_long,
                $this->bb->settings['contact_maxmessagelength'],
                strlen($this->bb->input['message'])
            );
        }

        if (strlen($this->bb->input['message']) < $this->bb->settings['contact_minmessagelength'] &&
            $this->bb->settings['contact_minmessagelength'] > 0) {
            $this->errors[] = $this->lang->sprintf(
                $this->lang->message_too_short,
                $this->bb->settings['contact_minmessagelength'],
                strlen($this->bb->input['message'])
            );
        }

        if (empty($this->bb->input['email'])) {
            $this->errors[] = $this->lang->contact_no_email;
        } else {
            // Validate email
            if (!validate_email_format($this->bb->input['email'])) {
                $this->errors[] = $this->lang->contact_no_email;
            }
        }

        // Should we have a CAPTCHA? Perhaps yes, but only for guests like in other pages...
        if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
            $captcha = new \RunBB\Core\Captcha($this);

            if ($captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($captcha->get_errors() as $error) {
                    $this->errors[] = $error;
                }
            }
        }

        if (!$this->user->uid && $this->bb->settings['stopforumspam_on_contact']) {
            $stop_forum_spam_checker = new \RunBB\Core\StopForumSpamChecker(
                $this->plugins,
                $this->bb->settings['stopforumspam_min_weighting_before_spam'],
                $this->bb->settings['stopforumspam_check_usernames'],
                $this->bb->settings['stopforumspam_check_emails'],
                $this->bb->settings['stopforumspam_check_ips'],
                $this->bb->settings['stopforumspam_log_blocks']
            );

            try {
                if ($stop_forum_spam_checker->is_user_a_spammer(
                    '',
                    $this->bb->input['email'],
                    $this->session->ipaddress
                )) {
                    $this->errors[] = $this->lang->sprintf(
                        $this->lang->error_stop_forum_spam_spammer,
                        $stop_forum_spam_checker->getErrorText([
                            'stopforumspam_check_emails',
                        'stopforumspam_check_ips'])
                    );
                }
            } catch (\Exception $e) {
                if ($this->bb->settings['stopforumspam_block_on_error']) {
                    $this->errors[] = $this->lang->error_stop_forum_spam_fetching;
                }
            }
        }

        if (empty($this->errors)) {
            if ($this->bb->settings['contact_badwords'] == 1) {
                $parser_options = [
                    'filter_badwords' => 1
                ];

                $this->bb->input['subject'] = $this->parser->parse_message(
                    $this->bb->input['subject'],
                    $parser_options
                );
                $this->bb->input['message'] = $this->parser->parse_message(
                    $this->bb->input['message'],
                    $parser_options
                );
            }

            $user = $this->lang->na;
            if ($this->user->uid) {
                $user = $this->user->username . ' - ' . $this->bb->settings['bburl'] . '/' .
                    get_profile_link($this->user->uid);
            }

            $subject = $this->lang->sprintf($this->lang->email_contact_subject, $this->bb->input['subject']);
            $message = $this->lang->sprintf(
                $this->lang->email_contact,
                $this->bb->input['email'],
                $user,
                $this->session->ipaddress,
                $this->bb->input['message']
            );

            // Email the administrator
            $this->mail->send($this->contactemail, $subject, $message, $this->bb->input['email']);

            $this->plugins->runHooks('contact_do_end');

            if ($this->bb->settings['mail_logging'] > 0) {
                // Log the message
                $log_entry = [
                    'subject' => $this->db->escape_string($subject),
                    'message' => $this->db->escape_string($message),
                    'dateline' => TIME_NOW,
                    'fromuid' => $this->user->uid,
                    'fromemail' => $this->db->escape_string($this->bb->input['email']),
                    'touid' => 0,
                    'toemail' => $this->db->escape_string($this->contactemail),
                    'tid' => 0,
                    'ipaddress' => $this->session->ipaddress,
                    'type' => 3
                ];
                $this->db->insert_query('maillogs', $log_entry);
            }

            if ($this->bb->usergroup['emailfloodtime'] > 0 || (isset($sent_count) &&
                    $sent_count + 1 >= $this->bb->usergroup['maxemails'])) {
                $this->bb->redirect($this->bb->settings['bburl'], $this->lang->contact_success_message, '', true);
            } else {
                $this->bb->redirect(
                    $this->bb->settings['bburl'] . '/contact',
                    $this->lang->contact_success_message,
                    '',
                    true
                );
            }
        } else {
            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        }
    }
}
