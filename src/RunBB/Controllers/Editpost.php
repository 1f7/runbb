<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Editpost extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('editpost');

        $this->plugins->runHooks('editpost_start');

        // No permission for guests
        if (!$this->user->uid) {
            return $this->bb->error_no_permission();
        }

        // Get post info
        $this->bb->pid = $this->bb->getInput('pid', 0);

        // if we already have the post information...
        if (isset($style) && $style['pid'] == $this->bb->pid && $style['type'] != 'f') {
            $post = &$style;
        } else {
            $post = $this->post->get_post($this->bb->pid);
        }

        if (!$post) {
            $this->bb->error($this->lang->error_invalidpost);
        }

        // Get thread info
        $this->tid = $post['tid'];
        $this->bb->tid = $post['tid'];// to attach ???
        $thread = $this->thread->get_thread($this->tid);

        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $thread['subject'] = htmlspecialchars_uni($thread['subject']);

        // Get forum info
        $this->fid = $post['fid'];
        $this->bb->forums = $this->forum->get_forum($this->fid);

        if (($thread['visible'] == 0 && !$this->user->is_moderator($this->fid, 'canviewunapprove')) ||
            ($thread['visible'] == -1 && !$this->user->is_moderator($this->fid, 'canviewdeleted')) ||
            ($thread['visible'] < -1 && $thread['uid'] != $this->user->uid)
        ) {
            $this->bb->error($this->lang->error_invalidthread);
        }
        if (!$this->bb->forums || $this->bb->forums['type'] != 'f') {
            $this->bb->error($this->lang->error_closedinvalidforum);
        }
        if (($this->bb->forums['open'] == 0 &&
                !$this->user->is_moderator($this->fid, 'caneditposts')) ||
            $this->user->suspendposting == 1
        ) {
            return $this->bb->error_no_permission();
        }

        // Add prefix to breadcrumb
        $breadcrumbprefix = '';
        if ($thread['prefix']) {
            $threadprefixes = $this->thread->build_prefixes();
            if (!empty($threadprefixes[$thread['prefix']])) {
                $breadcrumbprefix = $threadprefixes[$thread['prefix']]['displaystyle'] . '&nbsp;';
            }
        }

        // Make navigation
        $this->bb->build_forum_breadcrumb($this->fid);
        $this->bb->add_breadcrumb($breadcrumbprefix . $thread['subject'], get_thread_link($thread['tid']));
        $this->bb->add_breadcrumb($this->lang->nav_editpost);

        $forumpermissions = $this->forum->forum_permissions($this->fid);

        if ($this->bb->settings['bbcodeinserter'] != 0 && $this->bb->forums['allowmycode'] != 0 && $this->user->showcodebuttons != 0) {
            $this->bb->codebuttons = $this->editor->build_mycode_inserter('message', $this->bb->settings['smilieinserter']);
        }
        if ($this->bb->settings['smilieinserter'] != 0) {
            $this->bb->smilieinserter = $this->editor->build_clickable_smilies();
        }

        $this->bb->input['action'] = $this->bb->getInput('action', '');
        if (!$this->bb->input['action'] || isset($this->bb->input['previewpost'])) {
            $this->bb->input['action'] = 'editpost';
        }

        if ($this->bb->input['action'] == 'deletepost' && $this->bb->request_method == 'post') {
            if ((!$this->user->is_moderator($this->fid, 'candeleteposts') &&
                    !$this->user->is_moderator($this->fid, 'cansoftdeleteposts') &&
                    $this->bb->pid != $thread['firstpost']) ||
                (!$this->user->is_moderator($this->fid, 'candeletethreads') &&
                    !$this->user->is_moderator($this->fid, 'cansoftdeletethreads') &&
                    $this->bb->pid == $thread['firstpost'])
            ) {
                if ($thread['closed'] == 1) {
                    $this->bb->error($this->lang->redirect_threadclosed);
                }
                if (($forumpermissions['candeleteposts'] == 0 && $this->bb->pid != $thread['firstpost']) ||
                    ($forumpermissions['candeletethreads'] == 0 && $this->bb->pid == $thread['firstpost'])
                ) {
                    return $this->bb->error_no_permission();
                }
                if ($this->user->uid != $post['uid']) {
                    return $this->bb->error_no_permission();
                }
                // User can't delete unapproved post
                if ($post['visible'] == 0) {
                    return $this->bb->error_no_permission();
                }
            }
            if ($post['visible'] == -1 && $this->bb->settings['soft_delete'] == 1) {
                $this->bb->error($this->lang->error_already_deleted);
            }
        } elseif ($this->bb->input['action'] == 'restorepost' && $this->bb->request_method == 'post') {
            if ((!$this->user->is_moderator($this->fid, 'canrestoreposts') && $this->bb->pid != $thread['firstpost']) ||
                (!$this->user->is_moderator($this->fid, 'canrestorethreads') && $this->bb->pid == $thread['firstpost']) || $post['visible'] != -1
            ) {
                return $this->bb->error_no_permission();
            }
        } else {
            if (!$this->user->is_moderator($this->fid, 'caneditposts')) {
                if ($thread['closed'] == 1) {
                    $this->bb->error($this->lang->redirect_threadclosed);
                }
                if ($forumpermissions['caneditposts'] == 0) {
                    return $this->bb->error_no_permission();
                }
                if ($this->user->uid != $post['uid']) {
                    return $this->bb->error_no_permission();
                }
                // Edit time limit
                $time = TIME_NOW;
                if ($this->bb->usergroup['edittimelimit'] != 0 && $post['dateline'] < ($time - ($this->bb->usergroup['edittimelimit'] * 60))) {
                    $this->lang->edit_time_limit = $this->lang->sprintf($this->lang->edit_time_limit, $this->bb->usergroup['edittimelimit']);
                    $this->bb->error($this->lang->edit_time_limit);
                }
                // User can't edit unapproved post
                if ($post['visible'] == 0 || $post['visible'] == -1) {
                    return $this->bb->error_no_permission();
                }
            }
        }

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($this->bb->forums['fid']);

        if ((empty($_POST) && empty($_FILES)) && $this->bb->getInput('processed', 0) == '1') {
            $this->bb->error($this->lang->error_cannot_upload_php_post);
        }

        $attachedfile = [];
        if ($this->bb->settings['enableattachments'] == 1 &&
            !$this->bb->getInput('attachmentaid', 0) &&
            ($this->bb->getInput('newattachment', '') || $this->bb->getInput('updateattachment', '') ||
                ($this->bb->input['action'] == 'do_editpost' && isset($this->bb->input['submit']) &&
                    $_FILES['attachment']))
        ) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            // If there's an attachment, check it and upload it
            if ($_FILES['attachment']['size'] > 0 && $forumpermissions['canpostattachments'] != 0) {
                $query = $this->db->simple_select(
                    'attachments',
                    'aid',
                    "filename='" . $this->db->escape_string($_FILES['attachment']['name']) . "' AND pid='{$this->bb->pid}'"
                );
                $updateattach = $this->db->fetch_field($query, 'aid');

                $update_attachment = false;
                if ($updateattach > 0 && $this->bb->getInput('updateattachment', '') &&
                    ($this->bb->usergroup['caneditattachments'] ||
                        $forumpermissions['caneditattachments'])
                ) {
                    $update_attachment = true;
                }
                $attachedfile = $this->upload->upload_attachment($_FILES['attachment'], $update_attachment);
            }
            if (!empty($attachedfile['error'])) {
                $this->bb->input['action'] = 'editpost';
            }
            if (!isset($this->bb->input['submit'])) {
                $this->bb->input['action'] = 'editpost';
            }
        }
        $this->view->offsetSet('attachedfile', $attachedfile);

        if ($this->bb->settings['enableattachments'] == 1 &&
            $this->bb->getInput('attachmentaid', 0) &&
            isset($this->bb->input['attachmentact']) &&
            $this->bb->input['action'] == 'do_editpost' &&
            $this->bb->request_method == 'post'
        ) { // Lets remove/approve/unapprove the attachment
        // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->bb->input['attachmentaid'] = $this->bb->getInput('attachmentaid', 0);
            if ($this->bb->input['attachmentact'] == 'remove') {
                $this->upload->remove_attachment($this->bb->pid, '', $this->bb->input['attachmentaid']);
            } elseif ($this->bb->getInput('attachmentact', '') === 'approve' &&
                $this->user->is_moderator($this->fid, 'canapproveunapproveattachs')
            ) {
                $update_sql = ['visible' => 1];
                $this->db->update_query('attachments', $update_sql, "aid='{$this->bb->input['attachmentaid']}'");
                $this->thread->update_thread_counters($post['tid'], ['attachmentcount' => '+1']);
            } elseif ($this->bb->getInput('attachmentact', '') === 'unapprove' &&
                $this->user->is_moderator($this->fid, 'canapproveunapproveattachs')
            ) {
                $update_sql = ['visible' => 0];
                $this->db->update_query('attachments', $update_sql, "aid='{$this->bb->input['attachmentaid']}'");
                $this->thread->update_thread_counters($post['tid'], ['attachmentcount' => '-1']);
            }
            if (!isset($this->bb->input['submit'])) {
                $this->bb->input['action'] = 'editpost';
            }
        }

        if ($this->bb->input['action'] == 'deletepost' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->plugins->runHooks('editpost_deletepost');

            if ($this->bb->getInput('delete', 0) == 1) {
                $query = $this->db->simple_select(
                    'posts',
                    'pid',
                    "tid='{$this->tid}'",
                    ['limit' => 1, 'order_by' => 'dateline', 'order_dir' => 'asc']
                );
                $firstcheck = $this->db->fetch_array($query);
                if ($firstcheck['pid'] == $this->bb->pid) {
                    $firstpost = 1;
                } else {
                    $firstpost = 0;
                }

                $modlogdata['fid'] = $this->fid;
                $modlogdata['tid'] = $this->tid;
                if ($firstpost) {
                    if ($forumpermissions['candeletethreads'] == 1 ||
                        $this->user->is_moderator($this->fid, 'candeletethreads') ||
                        $this->user->is_moderator($this->fid, 'cansoftdeletethreads')
                    ) {
                        if ($this->bb->settings['soft_delete'] == 1 ||
                            $this->user->is_moderator($this->fid, 'cansoftdeletethreads')
                        ) {
                            $modlogdata['pid'] = $this->bb->pid;

                            $this->moderation->soft_delete_threads([$this->tid]);
                            $this->bblogger->log_moderator_action($modlogdata, $this->lang->thread_soft_deleted);
                        } else {
                            $this->moderation->delete_thread($this->tid);
                            $this->moderation->mark_reports($this->tid, 'thread');
                            $this->bblogger->log_moderator_action($modlogdata, $this->lang->thread_deleted);
                        }

                        if ($this->bb->input['ajax'] == 1) {
                            header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                            if ($this->user->is_moderator($this->fid, 'canviewdeleted')) {
                                echo json_encode(['data' => '1']);
                            } else {
                                echo json_encode(['data' => '3', 'url' => get_forum_link($this->fid)]);
                            }
                        } else {
                            return $this->bb->redirect($this->forum->get_forum_link($this->fid), $this->lang->redirect_threaddeleted);
                        }
                    } else {
                        return $this->bb->error_no_permission();
                    }
                } else {
                    if ($forumpermissions['candeleteposts'] == 1 ||
                        $this->user->is_moderator($this->fid, 'candeleteposts') ||
                        $this->user->is_moderator($this->fid, 'cansoftdeleteposts')
                    ) {
                        // Select the first post before this
                        if ($this->bb->settings['soft_delete'] == 1 ||
                            $this->user->is_moderator($this->fid, 'cansoftdeleteposts')
                        ) {
                            $modlogdata['pid'] = $this->bb->pid;

                            $this->moderation->soft_delete_posts([$this->bb->pid]);
                            $this->bblogger->log_moderator_action($modlogdata, $this->lang->post_soft_deleted);
                        } else {
                            $this->moderation->delete_post($this->bb->pid);
                            $this->moderation->mark_reports($this->bb->pid, 'post');
                            $this->bblogger->log_moderator_action($modlogdata, $this->lang->post_deleted);
                        }

                        $query = $this->db->simple_select(
                            'posts',
                            'pid',
                            "tid='{$this->tid}' AND dateline <= '{$post['dateline']}'",
                            ['limit' => 1, 'order_by' => 'dateline', 'order_dir' => 'desc']
                        );
                        $next_post = $this->db->fetch_array($query);
                        if ($next_post['pid']) {
                            $redirect = get_post_link($next_post['pid'], $this->tid) . "#pid{$next_post['pid']}";
                        } else {
                            $redirect = get_thread_link($this->tid);
                        }

                        if ($this->bb->input['ajax'] == 1) {
                            header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                            if ($this->user->is_moderator($this->fid, 'canviewdeleted')) {
                                echo json_encode(['data' => '1']);
                            } else {
                                echo json_encode(['data' => '2']);
                            }
                        } else {
                            return $this->bb->redirect($redirect, $this->lang->redirect_postdeleted);
                        }
                    } else {
                        return $this->bb->error_no_permission();
                    }
                }
            } else {
                $this->bb->error($this->lang->redirect_nodelete);
            }
        }

        if ($this->bb->input['action'] == 'restorepost' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->plugins->runHooks('editpost_restorepost');

            if ($this->bb->getInput('restore', 0) == 1) {
                $query = $this->db->simple_select(
                    'posts',
                    'pid',
                    "tid='{$this->tid}'",
                    ['limit' => 1, 'order_by' => 'dateline', 'order_dir' => 'asc']
                );
                $firstcheck = $this->db->fetch_array($query);
                if ($firstcheck['pid'] == $this->bb->pid) {
                    $firstpost = 1;
                } else {
                    $firstpost = 0;
                }

                $modlogdata['fid'] = $this->fid;
                $modlogdata['tid'] = $this->tid;
                $modlogdata['pid'] = $this->bb->pid;
                if ($firstpost) {
                    if ($this->user->is_moderator($this->fid, 'canrestorethreads')) {
                        $this->moderation->restore_threads([$this->tid]);
                        $this->bblogger->log_moderator_action($modlogdata, $this->lang->thread_restored);
                        if ($this->bb->input['ajax'] == 1) {
                            header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                            echo json_encode(['data' => '1']);
                        } else {
                            return $this->bb->redirect($this->forum->get_forum_link($this->fid), $this->lang->redirect_threadrestored);
                        }
                    } else {
                        return $this->bb->error_no_permission();
                    }
                } else {
                    if ($this->user->is_moderator($this->fid, 'canrestoreposts')) {
                        // Select the first post before this
                        $this->moderation->restore_posts([$this->bb->pid]);
                        $this->bblogger->log_moderator_action($modlogdata, $this->lang->post_restored);
                        $redirect = get_post_link($this->bb->pid, $this->tid) . "#pid{$this->bb->pid}";

                        if ($this->bb->input['ajax'] == 1) {
                            header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                            echo json_encode(['data' => '1']);
                        } else {
                            return $this->bb->redirect($redirect, $this->lang->redirect_postrestored);
                        }
                    } else {
                        return $this->bb->error_no_permission();
                    }
                }
            } else {
                $this->bb->error($this->lang->redirect_norestore);
            }
        }

        if ($this->bb->input['action'] == 'do_editpost' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->plugins->runHooks('editpost_do_editpost_start');

            // Set up posthandler.
            $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'update');
            $posthandler->action = 'post';

            // Set the post data that came from the input to the $post array.
            $post = [
                'pid' => $this->bb->input['pid'],
                'prefix' => $this->bb->getInput('threadprefix', 0),
                'subject' => $this->bb->getInput('subject', ''),
                'icon' => $this->bb->getInput('icon', 0),
                'uid' => $post['uid'],
                'username' => $post['username'],
                'edit_uid' => $this->user->uid,
                'message' => $this->bb->getInput('message', '', true),
                'editreason' => $this->bb->getInput('editreason', '', true),
            ];

            $postoptions = $this->bb->getInput('postoptions', []);
            if (!isset($postoptions['signature'])) {
                $postoptions['signature'] = 0;
            }
            if (!isset($postoptions['subscriptionmethod'])) {
                $postoptions['subscriptionmethod'] = 0;
            }
            if (!isset($postoptions['disablesmilies'])) {
                $postoptions['disablesmilies'] = 0;
            }

            // Set up the post options from the input.
            $post['options'] = [
                'signature' => $postoptions['signature'],
                'subscriptionmethod' => $postoptions['subscriptionmethod'],
                'disablesmilies' => $postoptions['disablesmilies']
            ];

            $posthandler->set_data($post);

            // Now let the post handler do all the hard work.
            if (!$posthandler->validatePost()) {
                $post_errors = $posthandler->get_friendly_errors();
                $post_errors = $this->bb->inline_error($post_errors);
                $this->bb->input['action'] = 'editpost';
            } // No errors were found, we can call the update method.
            else {
                $postinfo = $posthandler->updatePost();
                $visible = $postinfo['visible'];
                $first_post = $postinfo['first_post'];

                // Help keep our attachments table clean.
                $this->db->delete_query('attachments', "filename='' OR filesize<1");

                // Did the user choose to post a poll? Redirect them to the poll posting page.
                if ($this->bb->getInput('postpoll', 0) && $forumpermissions['canpostpolls']) {
                    $url = "polls.php?action=newpoll&tid=$this->tid&polloptions=" . $this->bb->getInput('numpolloptions', 0);
                    $this->lang->redirect_postedited = $this->lang->redirect_postedited_poll;
                } elseif ($visible == 0 && $first_post &&
                    !$this->user->is_moderator($this->fid, 'canviewunapprove', $this->user->uid)
                ) {
                    // Moderated post
                    $this->lang->redirect_postedited .= $this->lang->redirect_thread_moderation;
                    $url = $this->forum->get_forum_link($this->fid);
                } elseif ($visible == 0 && !$this->user->is_moderator($this->fid, 'canviewunapprove', $this->user->uid)) {
                    $this->lang->redirect_postedited .= $this->lang->redirect_post_moderation;
                    $url = $this->thread->get_thread_link($this->tid);
                } // Otherwise, send them back to their post
                else {
                    $this->lang->redirect_postedited .= $this->lang->redirect_postedited_redirect;
                    $url = get_post_link($this->bb->pid, $this->tid) . "#pid{$this->bb->pid}";
                }
                $this->plugins->runHooks('editpost_do_editpost_end');

                return $this->bb->redirect($url, $this->lang->redirect_postedited);
            }
        }

        if (!$this->bb->input['action'] || $this->bb->input['action'] == 'editpost') {
            $this->plugins->runHooks('editpost_action_start');

            if (!isset($this->bb->input['previewpost'])) {
                $icon = $post['icon'];
            }

            $posticons = [];
            if ($this->bb->forums['allowpicons'] != 0) {
                $posticons = $this->post->get_post_icons();
            }
            $this->view->offsetSet('posticons', $posticons);

            $this->bb->showDeleteBox = false;
            // Can we delete posts?
            if ($post['visible'] != -1 && ($this->user->is_moderator($this->fid, 'candeleteposts') ||
                    $forumpermissions['candeleteposts'] == 1 && $this->user->uid == $post['uid'])
            ) {
                $this->bb->showDeleteBox = true;
            }

            $bgcolor = 'trow1';
            $this->bb->showAttachBox = false;
            if ($this->bb->settings['enableattachments'] != 0 && $forumpermissions['canpostattachments'] != 0) {
                $this->bb->showAttachBox = true;
                // Get a listing of the current attachments, if there are any
                $attachcount = 0;
                $query = $this->db->simple_select('attachments', '*', "pid='{$this->bb->pid}'");
                $attachments = '';
                while ($attachment = $this->db->fetch_array($query)) {
                    $attachment['size'] = $this->parser->friendlySize($attachment['filesize']);
                    $attachment['icon'] = $this->upload->get_attachment_icon(get_extension($attachment['filename']));
                    $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                    $postinsert = false;
                    if ($this->bb->settings['bbcodeinserter'] != 0 &&
                        $this->bb->forums['allowmycode'] != 0 &&
                        $this->user->showcodebuttons != 0
                    ) {
                        $postinsert = true;
                    }
                    // Moderating options
                    $attach_mod_options = false;
                    if ($this->user->is_moderator($this->fid)) {
                        $attach_mod_options = true;
                    }
                    $attachments[] = [
                        'attachment' => $attachment,
                        'postinsert' => $postinsert,
                        'attach_mod_options' => $attach_mod_options
                    ];
                    $attachcount++;
                }
                $this->view->offsetSet('attachments', $attachments);

                $query = $this->db->simple_select('attachments', 'SUM(filesize) AS ausage', "uid='" . $this->user->uid . "'");
                $usage = $this->db->fetch_array($query);
                if ($usage['ausage'] > ($this->bb->usergroup['attachquota'] * 1024) && $this->bb->usergroup['attachquota'] != 0) {
                    $noshowattach = 1;
                } else {
                    $noshowattach = 0;
                }
                if ($this->bb->usergroup['attachquota'] == 0) {
                    $friendlyquota = $this->lang->unlimited;
                } else {
                    $friendlyquota = $this->parser->friendlySize($this->bb->usergroup['attachquota'] * 1024);
                }
                $friendlyusage = $this->parser->friendlySize($usage['ausage']);
                $this->lang->attach_quota = $this->lang->sprintf($this->lang->attach_quota, $friendlyusage, $friendlyquota);
                $this->bb->attach_add = false;
                if ($this->bb->settings['maxattachments'] == 0 ||
                    ($this->bb->settings['maxattachments'] != 0 &&
                        $attachcount < $this->bb->settings['maxattachments']) && !$noshowattach
                ) {
                    $this->bb->attach_add = true;
                }
                $this->bb->attach_update = false;
                if (($this->bb->usergroup['caneditattachments'] || $forumpermissions['caneditattachments']) && $attachcount > 0) {
                    $this->bb->attach_update = true;
                }
            }

            if (!$this->bb->getInput('attachmentaid', 0) &&
                !$this->bb->getInput('newattachment', '') &&
                !$this->bb->getInput('updateattachment', '') &&
                !isset($this->bb->input['previewpost'])
            ) {
                $message = $post['message'];
                $subject = $post['subject'];
                $reason = htmlspecialchars_uni($post['editreason']);
            } else {
                $message = $this->bb->getInput('message', '', true);
                $subject = $this->bb->getInput('subject', '', true);
                $reason = htmlspecialchars_uni($this->bb->getInput('editreason', '', true));
            }
            $this->view->offsetSet('reason', $reason);

            if (!isset($post_errors)) {
                $post_errors = '';
            }

            $postoptions_subscriptionmethod_dont = $postoptions_subscriptionmethod_none =
            $postoptions_subscriptionmethod_email = $postoptions_subscriptionmethod_pm = '';
            $postoptionschecked = ['signature' => '', 'disablesmilies' => ''];

            if (!empty($this->bb->input['previewpost']) || $post_errors) {
                // Set up posthandler.
                $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'update');
                $posthandler->action = 'post';

                // Set the post data that came from the input to the $post array.
                $post = [
                    'pid' => $this->bb->input['pid'],
                    'prefix' => $this->bb->getInput('threadprefix', 0),
                    'subject' => $this->bb->getInput('subject', '', true),
                    'icon' => $this->bb->getInput('icon', 0),
                    'uid' => $post['uid'],
                    'edit_uid' => $this->user->uid,
                    'message' => $this->bb->getInput('message', '', true),
                ];

                $postoptions = $this->bb->getInput('postoptions', []);
                if (!isset($postoptions['signature'])) {
                    $postoptions['signature'] = 0;
                }
                if (!isset($postoptions['emailnotify'])) {
                    $postoptions['emailnotify'] = 0;
                }
                if (!isset($postoptions['disablesmilies'])) {
                    $postoptions['disablesmilies'] = 0;
                }

                // Set up the post options from the input.
                $post['options'] = [
                    'signature' => $postoptions['signature'],
                    'emailnotify' => $postoptions['emailnotify'],
                    'disablesmilies' => $postoptions['disablesmilies']
                ];

                $posthandler->set_data($post);

                // Now let the post handler do all the hard work.
                if (!$posthandler->validatePost()) {
                    $post_errors = $posthandler->get_friendly_errors();
                    $post_errors = $this->bb->inline_error($post_errors);
                    $this->bb->input['action'] = 'editpost';
                    $this->bb->input['previewpost'] = 0;
                } else {
                    $previewmessage = $message;
                    $previewsubject = $subject;
                    $message = htmlspecialchars_uni($message);
                    $subject = htmlspecialchars_uni($subject);

                    $postoptions = $this->bb->getInput('postoptions', []);

                    if (isset($postoptions['signature']) && $postoptions['signature'] == 1) {
                        $postoptionschecked['signature'] = ' checked="checked"';
                    }

                    if (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'none') {
                        $postoptions_subscriptionmethod_none = 'checked="checked"';
                    } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'email') {
                        $postoptions_subscriptionmethod_email = 'checked="checked"';
                    } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'pm') {
                        $postoptions_subscriptionmethod_pm = 'checked="checked"';
                    } else {
                        $postoptions_subscriptionmethod_dont = 'checked="checked"';
                    }

                    if (isset($postoptions['disablesmilies']) && $postoptions['disablesmilies'] == 1) {
                        $postoptionschecked['disablesmilies'] = ' checked="checked"';
                    }
                }
            }
            $postbit = '';
            if (!empty($this->bb->input['previewpost'])) {
                if (!$post['uid']) {
                    $query = $this->db->simple_select('posts', 'username, dateline', "pid='{$this->bb->pid}'");
                    $postinfo = $this->db->fetch_array($query);
                } else {
                    // Figure out the poster's other information.
                    $query = $this->db->query('
				SELECT u.*, f.*, p.dateline
				FROM ' . TABLE_PREFIX . 'users u
				LEFT JOIN ' . TABLE_PREFIX . 'userfields f ON (f.ufid=u.uid)
				LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.uid=u.uid)
				WHERE u.uid='{$post['uid']}' AND p.pid='{$this->bb->pid}'
				LIMIT 1
			");
                    $postinfo = $this->db->fetch_array($query);
                    $postinfo['userusername'] = $postinfo['username'];
                }

                $query = $this->db->simple_select('attachments', '*', "pid='{$this->bb->pid}'");
                while ($attachment = $this->db->fetch_array($query)) {
                    $this->bb->attachcache[0][$attachment['aid']] = $attachment;
                }

                if (!isset($postoptions['disablesmilies'])) {
                    $postoptions['disablesmilies'] = 0;
                }

                // Set the values of the post info array.
                $postinfo['message'] = $previewmessage;
                $postinfo['subject'] = $previewsubject;
                $postinfo['icon'] = isset($icon) ? $icon : $post['icon'];
                $postinfo['smilieoff'] = $postoptions['disablesmilies'];

                $postbit = $this->post->build_postbit($postinfo, 1);
            } elseif (!$post_errors) {
                $message = htmlspecialchars_uni($message);
                $subject = htmlspecialchars_uni($subject);

                //$preview = '';

                if ($post['includesig'] != 0) {
                    $postoptionschecked['signature'] = ' checked="checked"';
                }

                if ($post['smilieoff'] == 1) {
                    $postoptionschecked['disablesmilies'] = ' checked="checked"';
                }

                $query = $this->db->simple_select(
                    'threadsubscriptions',
                    'notification',
                    "tid='{$this->tid}' AND uid='{$this->user->uid}'"
                );
                if ($this->db->num_rows($query) > 0) {
                    $notification = $this->db->fetch_field($query, 'notification');

                    if ($notification == 0) {
                        $postoptions_subscriptionmethod_none = 'checked="checked"';
                    } elseif ($notification == 1) {
                        $postoptions_subscriptionmethod_email = 'checked="checked"';
                    } elseif ($notification == 2) {
                        $postoptions_subscriptionmethod_pm = 'checked="checked"';
                    } else {
                        $postoptions_subscriptionmethod_dont = 'checked="checked"';
                    }
                }
            }
            $this->view->offsetSet('postbit', $postbit);
            $this->view->offsetSet('post_errors', $post_errors);
            $this->view->offsetSet('message', $message);
            $this->view->offsetSet('subject', $subject);

            // Generate thread prefix selector if this is the first post of the thread
            $prefixselect = '';
            if ($thread['firstpost'] == $this->bb->pid) {
                if (!$this->bb->getInput('threadprefix', 0)) {
                    $this->bb->input['threadprefix'] = $thread['prefix'];
                }

                $prefixselect = $this->thread->build_prefix_select(
                    $this->bb->forums['fid'],
                    $this->bb->getInput('threadprefix', 0),
                    0,
                    $thread['prefix']
                );
            }
            $this->view->offsetSet('prefixselect', $prefixselect);

            $editreason = '';
            if ($this->bb->settings['alloweditreason'] == 1) {
                $bgcolor = 'trow2';
                $bgcolor2 = 'trow1';
            } else {
                $bgcolor = 'trow1';
                $bgcolor2 = 'trow2';
            }
            $this->view->offsetSet('bgcolor', $bgcolor);
            $this->view->offsetSet('bgcolor2', $bgcolor2);

            $query = $this->db->simple_select(
                'posts',
                '*',
                "tid='{$this->tid}'",
                ['limit' => 1, 'order_by' => 'dateline', 'order_dir' => 'asc']
            );
            $firstcheck = $this->db->fetch_array($query);

            $time = TIME_NOW;
            $this->bb->showPollBox = false;
            $postpollchecked = '';
            $numpolloptions = 2;
            if ($firstcheck['pid'] == $this->bb->pid &&
                $forumpermissions['canpostpolls'] != 0 && $thread['poll'] < 1 &&
                ($this->user->is_moderator($this->fid, 'canmanagepolls') ||
                    $thread['dateline'] > ($time - ($this->bb->settings['polltimelimit'] * 60 * 60)) ||
                    $this->bb->settings['polltimelimit'] == 0)
            ) {
                $this->bb->showPollBox = true;
                $this->lang->max_options = $this->lang->sprintf($this->lang->max_options, $this->bb->settings['maxpolloptions']);
                $numpolloptions = $this->bb->getInput('numpolloptions', 0);

                if ($numpolloptions < 1) {
                    $numpolloptions = 2;
                }
                if ($this->bb->getInput('postpoll', 0) == 1) {
                    $postpollchecked = 'checked="checked"';
                }
            }

            $this->view->offsetSet('postoptionschecked', $postoptionschecked);
            $this->view->offsetSet('postoptions', [
                'dont' => $postoptions_subscriptionmethod_dont,
                'none' => $postoptions_subscriptionmethod_none,
                'email' => $postoptions_subscriptionmethod_email,
                'pm' => $postoptions_subscriptionmethod_pm
            ]);
            $this->view->offsetSet('postpollchecked', $postpollchecked);
            $this->view->offsetSet('numpolloptions', $numpolloptions);

            // Can we disable smilies or are they disabled already?
            $this->bb->showDisableSmilies = false;
            if ($this->bb->forums['allowsmilies'] != 0) {
                $this->bb->showDisableSmilies = true;
            }

            $moderation_text = '';
            if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveattachs')) {
                if ($forumpermissions['modattachments'] == 1 && $forumpermissions['canpostattachments'] != 0) {
                    $moderation_text = $this->lang->moderation_forum_attachments;
                }
            }

            if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveposts')) {
                if ($forumpermissions['mod_edit_posts'] == 1) {
                    $moderation_text = $this->lang->moderation_forum_edits;
                }
            }
            $this->view->offsetSet('moderation_text', $moderation_text);
            $this->view->offsetSet('pid', $this->bb->pid);

            $this->plugins->runHooks('editpost_end');

            $this->bb->forums['name'] = strip_tags($this->bb->forums['name']);

            $this->bb->output_page();
            $this->view->render($response, '@forum/editpost.html.twig');
        }
    }
}
