<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Forumdisplay extends AbstractController
{
    private $inlinecount = 0;

    public function index(Request $request, Response $response)
    {
        $orderarrow = $sortsel = ['rating' => '', 'subject' => '', 'starter' => '', 'started' => '', 'replies' => '', 'views' => '', 'lastpost' => ''];
        $ordersel = ['asc' => '', 'desc' => ''];
        $datecutsel = [1 => '', 5 => '', 10 => '', 20 => '', 50 => '', 75 => '', 100 => '', 365 => '', 9999 => ''];
        $rules = '';

        // Load global language phrases
        $this->lang->load('forumdisplay');

        $this->plugins->runHooks('forumdisplay_start');

        $fid = $this->bb->getInput('fid', 0);
        if ($fid < 0) {
            switch ($fid) {
                case '-1':
                    $location = 'index';//'index.php';
                    break;
                case '-2':
                    $location = 'search';//'search.php';
                    break;
                case '-3':
                    $location = 'usercp';//'usercp.php';
                    break;
                case '-4':
                    $location = 'private';//'private.php';
                    break;
                case '-5':
                    $location = 'online';//'online.php';
                    break;
            }
            if ($location) {
                return $response->withRedirect($this->bb->settings['bburl'] . '/' . $location);
            }
        }

        // Get forum info
        $foruminfo = $this->forum->get_forum($fid);
        if (!$foruminfo) {
            $this->bb->error($this->lang->error_invalidforum);
        }

        $currentitem = $fid;
        $this->bb->build_forum_breadcrumb($fid);
        $parentlist = $foruminfo['parentlist'];

        // To validate, turn & to &amp; but support unicode
        $foruminfo['name'] = preg_replace('#&(?!\#[0-9]+;)#si', '&amp;', $foruminfo['name']);

        $this->bb->forumpermissions = $this->forum->forum_permissions();
        $fpermissions = $this->bb->forumpermissions[$fid];

        if ($fpermissions['canview'] != 1) {
            return $this->bb->error_no_permission();
        }
        if ($this->user->uid == 0) {
            // Cookie'd forum read time
            $forumsread = [];
            if (isset($this->bb->cookies['mybb']['forumread'])) {
                $forumsread = my_unserialize($this->bb->cookies['mybb']['forumread']);
            }

            if (is_array($forumsread) && empty($forumsread)) {
                if (isset($this->bb->cookies['mybb']['readallforums'])) {
                    $forumsread[$fid] = $this->bb->cookies['mybb']['lastvisit'];
                } else {
                    $forumsread = [];
                }
            }

//            $query = $this->db->simple_select('forums', '*', 'active != 0', array('order_by' => 'pid, disporder'));
            $f = \RunBB\Models\Forum::getActiveForums();
        } else {
            // Build a forum cache from the database
//            $query = $this->db->query('
//		SELECT f.*, fr.dateline AS lastread
//		FROM ' . TABLE_PREFIX . 'forums f
//		LEFT JOIN ' . TABLE_PREFIX . "forumsread fr ON (fr.fid=f.fid AND fr.uid='{$this->user->uid}')
//		WHERE f.active != 0
//		ORDER BY pid, disporder
//	");
            $f = \RunBB\Models\Forum::getActiveForumsOnForumsread($this->user->uid);
        }

//        while ($forum = $this->db->fetch_array($query)) {
        foreach ($f as $forum) {
            if ($this->user->uid == 0 && isset($forumsread[$forum['fid']])) {
                $forum['lastread'] = $forumsread[$forum['fid']];
            }

            $this->bb->fcache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
        }
        // Get the forum moderators if the setting is enabled.
        if ($this->bb->settings['modlist'] != 0) {
            $this->bb->moderatorcache = $this->cache->read('moderators');
        }

        $bgcolor = 'trow1';
        if ($this->bb->settings['subforumsindex'] != 0) {
            $this->bb->showdepth = 3;
        } else {
            $this->bb->showdepth = 2;
        }

        $child_forums = $this->forum->build_forumbits($fid, 2);//no cat, forums only
        //$forums = $child_forums['forum_list'];
        $forums = $child_forums['forum_forums'];

        if ($forums) {
            $this->lang->sub_forums_in = $this->lang->sprintf($this->lang->sub_forums_in, $foruminfo['name']);
        }
        $this->view->offsetSet('forums', $forums);
        // header row vars
        $this->view->offsetSet('forumsvars', $child_forums['forum_vars']);

        $excols = 'forumdisplay';

        // Password protected forums
        $this->forum->check_forum_password($foruminfo['fid']);

        if ($foruminfo['linkto']) {
//            header("Location: {$foruminfo['linkto']}");
            return $response->withRedirect($foruminfo['linkto']);
//            exit;
        }

        // Make forum jump...
        $forumjump = '';
        if ($this->bb->settings['enableforumjump'] != 0) {
            $forumjump = $this->forum->build_forum_jump('', $fid, 1);
        }
        $this->view->offsetSet('forumjump', $forumjump);

        $newthread = '';
        if ($foruminfo['type'] == 'f' &&
            $foruminfo['open'] != 0 &&
            $fpermissions['canpostthreads'] != 0 &&
            $this->user->suspendposting == 0
        ) {
            $newthread = "<a href=\"{$this->bb->settings['bburl']}/newthread?fid={$fid}\" class=\"button new_thread_button\"><span>{$this->lang->post_thread}</span></a>";
        }
        $this->view->offsetSet('newthread', $newthread);

        $this->bb->showSearchForm = false;
        if ($fpermissions['cansearch'] != 0 && $foruminfo['type'] == 'f') {
            $this->bb->showSearchForm = true;
        }

        // Gather forum stats
        $has_announcements = $has_modtools = false;
        $forum_stats = $this->cache->read('forumsdisplay');

        if (is_array($forum_stats)) {
            if (!empty($forum_stats[-1]['modtools']) || !empty($forum_stats[$fid]['modtools'])) {
                // Mod tools are specific to forums, not parents
                $has_modtools = true;
            }

            if (!empty($forum_stats[-1]['announcements']) || !empty($forum_stats[$fid]['announcements'])) {
                // Global or forum-specific announcements
                $has_announcements = true;
            }
        }

        $done_moderators = [
            'users' => [],
            'groups' => []
        ];

        $moderators = $comma = '';
        $parentlistexploded = explode(',', $parentlist);

        foreach ($parentlistexploded as $mfid) {
            // This forum has moderators
            if (is_array($this->bb->moderatorcache[$mfid])) {
                // Fetch each moderator from the cache and format it, appending it to the list
                foreach ($this->bb->moderatorcache[$mfid] as $modtype) {
                    foreach ($modtype as $moderator) {
                        if ($moderator['isgroup']) {
                            if (in_array($moderator['id'], $done_moderators['groups'])) {
                                continue;
                            }

                            $moderator['title'] = htmlspecialchars_uni($moderator['title']);
                            $moderators .= $comma . $moderator['title'];
                            $done_moderators['groups'][] = $moderator['id'];
                        } else {
                            if (in_array($moderator['id'], $done_moderators['users'])) {
                                continue;
                            }

                            $moderator['profilelink'] = get_profile_link($moderator['id']);
                            $moderator['username'] = $this->user->format_name(
                                htmlspecialchars_uni($moderator['username']),
                                $moderator['usergroup'],
                                $moderator['displaygroup']
                            );
                            $moderators .= "{$comma}<a href=\"{$moderator['profilelink']}\">{$moderator['username']}</a>";
                            $done_moderators['users'][] = $moderator['id'];
                        }
                        $comma = $this->lang->comma;
                    }
                }
            }

            if (!empty($forum_stats[$mfid]['announcements'])) {
                $has_announcements = true;
            }
        }
        $comma = '';

        $this->view->offsetSet('moderators', $moderators);

        // Get the users browsing this forum.
        $usersbrowsing = '';
        if ($this->bb->settings['browsingthisforum'] != 0) {
            $timecut = TIME_NOW - $this->bb->settings['wolcutoff'];

            $comma = '';
            $guestcount = 0;
            $membercount = 0;
            $inviscount = 0;
            $onlinemembers = '';
            $doneusers = [];

//            $query = $this->db->query('
//		SELECT s.ip, s.uid, u.username, s.time, u.invisible, u.usergroup, u.usergroup, u.displaygroup
//		FROM ' . TABLE_PREFIX . 'sessions s
//		LEFT JOIN ' . TABLE_PREFIX . "users u ON (s.uid=u.uid)
//		WHERE s.time > '$timecut' AND location1='$fid' AND nopermission != 1
//		ORDER BY u.username ASC, s.time DESC
//	");
            $u = \RunBB\Models\Session::where('sessions.time', '>', "'$timecut'")
                ->where('sessions.location1', '=', $fid)
                ->where('sessions.nopermission', '!=', 1)
//                ->select('sessions.ip', 'sessions.uid', 'sessions.time',
//                    'users.username', 'users.invisible', 'users.usergroup', 'users.usergroup', 'users.displaygroup')
                ->leftJoin('users', function ($join) {
                    $join->on('sessions.uid', '=', 'users.uid');
                })
                ->orderBy('users.username', 'asc')
                ->orderBy('sessions.time', 'desc')
//                ->get()
                ->get(['sessions.ip', 'sessions.uid', 'sessions.time',
                    'users.username', 'users.invisible',
                    'users.usergroup', 'users.displaygroup'
                ])
                ->toArray();

//            while ($user = $this->db->fetch_array($query)) {
            foreach ($u as $user) {
                if ($user['uid'] == 0) {
                    ++$guestcount;
                } else {
                    if (empty($doneusers[$user['uid']]) || $doneusers[$user['uid']] < $user['time']) {
                        $doneusers[$user['uid']] = $user['time'];
                        ++$membercount;
                        if ($user['invisible'] == 1) {
                            $invisiblemark = '*';
                            ++$inviscount;
                        } else {
                            $invisiblemark = '';
                        }

                        if ($user['invisible'] != 1 || $this->bb->usergroup['canviewwolinvis'] == 1 || $user['uid'] == $this->user->uid) {
                            $user['username'] = $this->user->format_name($user['username'], $user['usergroup'], $user['displaygroup']);
                            $user['profilelink'] = $this->user->build_profile_link($user['username'], $user['uid']);
                            $onlinemembers .= "{$comma}{$user['profilelink']}{$invisiblemark}";
                            $comma = $this->lang->comma;
                        }
                    }
                }
            }

            $guestsonline = '';
            if ($guestcount) {
                $guestsonline = $this->lang->sprintf($this->lang->users_browsing_forum_guests, $guestcount);
            }

            $invisonline = '';
            if (isset($this->user->invisible) && $this->user->invisible == 1) {
                // the user was counted as invisible user --> correct the inviscount
                $inviscount -= 1;
            }
            if ($inviscount && $this->bb->usergroup['canviewwolinvis'] != 1) {
                $invisonline = $this->lang->sprintf($this->lang->users_browsing_forum_invis, $inviscount);
            }


            $onlinesep = '';
            if ($invisonline != '' && $onlinemembers) {
                $onlinesep = $this->lang->comma;
            }

            $onlinesep2 = '';
            if ($invisonline != '' && $guestcount || $onlinemembers && $guestcount) {
                $onlinesep2 = $this->lang->comma;
            }
            $usersbrowsing = "<span class=\"smalltext\">{$this->lang->users_browsing_forum} {$onlinemembers}{$onlinesep}{$invisonline}{$onlinesep2}{$guestsonline}</span><br />";
        }
        $this->view->offsetSet('usersbrowsing', $usersbrowsing);

        // Do we have any forum rules to show for this forum?
        $rules = $forumrules = '';
        if ($foruminfo['rulestype'] != 0 && $foruminfo['rules']) {
            if (!$foruminfo['rulestitle']) {
                $foruminfo['rulestitle'] = $this->lang->sprintf($this->lang->forum_rules, $foruminfo['name']);
            }

            $rules_parser = [
                'allow_html' => 1,
                'allow_mycode' => 1,
                'allow_smilies' => 1,
                'allow_imgcode' => 1
            ];

            $foruminfo['rules'] = $this->parser->parse_message($foruminfo['rules'], $rules_parser);
            if ($foruminfo['rulestype'] == 1 || $foruminfo['rulestype'] == 3) {
                eval("\$rules = \"" . $this->templates->get('forumdisplay_rules') . "\";");
            } elseif ($foruminfo['rulestype'] == 2) {
                eval("\$rules = \"" . $this->templates->get('forumdisplay_rules_link') . "\";");
            }
        }
        $this->view->offsetSet('rules', $rules);

        $bgcolor = 'trow1';

        // Set here to fetch only approved topics (and then below for a moderator we change this).
        $visibleonly = "visible='1'";
        $tvisibleonly = "AND t.visible='1'";

        // Check if the active user is a moderator and get the inline moderation tools.
        if ($this->user->is_moderator($fid)) {
            $ismod = true;
            $inlinemod = '';
            $this->bb->inlinecookie = 'inlinemod_forum' . $fid;

            if ($this->user->is_moderator($fid, 'canviewdeleted') == true ||
                $this->user->is_moderator($fid, 'canviewunapprove') == true
            ) {
                if ($this->user->is_moderator($fid, 'canviewunapprove') == true &&
                    $this->user->is_moderator($fid, 'canviewdeleted') == false
                ) {
                    $visibleonly = 'visible IN (0,1)';
                    $tvisibleonly = 'AND t.visible IN (0,1)';
                } elseif ($this->user->is_moderator($fid, 'canviewdeleted') == true &&
                    $this->user->is_moderator($fid, 'canviewunapprove') == false
                ) {
                    $visibleonly = 'visible IN (-1,1)';
                    $tvisibleonly = 'AND t.visible IN (-1,1)';
                } else {
                    $visibleonly = 'visible IN (-1,0,1)';
                    $tvisibleonly = ' AND t.visible IN (-1,0,1)';
                }
            }
        } else {
            $inlinemod = $inlinemodcol = '';
            $ismod = false;
        }
        $this->view->offsetSet('ismod', $ismod);

        if ($this->user->is_moderator($fid, 'caneditposts') || $fpermissions['caneditposts'] == 1) {
            $can_edit_titles = 1;
        } else {
            $can_edit_titles = 0;
        }

        unset($rating);

        // Pick out some sorting options.
        // First, the date cut for the threads.
        $datecut = 9999;
        if (empty($this->bb->input['datecut'])) {
            // If the user manually set a date cut, use it.
            if (!empty($this->user->daysprune)) {
                $datecut = $this->user->daysprune;
            } else {
                // If the forum has a non-default date cut, use it.
                if (!empty($foruminfo['defaultdatecut'])) {
                    $datecut = $foruminfo['defaultdatecut'];
                }
            }
        } // If there was a manual date cut override, use it.
        else {
            $datecut = $this->bb->getInput('datecut', 0);
        }

        $datecutsel[(int)$datecut] = ' selected="selected"';
        $this->view->offsetSet('datecutsel', $datecutsel);

        if ($datecut > 0 && $datecut != 9999) {
            $checkdate = TIME_NOW - ($datecut * 86400);
            $datecutsql = "AND (lastpost >= '$checkdate' OR sticky = '1')";
            $datecutsql2 = "AND (t.lastpost >= '$checkdate' OR t.sticky = '1')";
        } else {
            $datecutsql = '';
            $datecutsql2 = '';
        }

        // Sort by thread prefix
        $tprefix = $this->bb->getInput('prefix', 0);
        if ($tprefix > 0) {
            $prefixsql = "AND prefix = {$tprefix}";
            $prefixsql2 = "AND t.prefix = {$tprefix}";
        } elseif ($tprefix == -1) {
            $prefixsql = 'AND prefix = 0';
            $prefixsql2 = 'AND t.prefix = 0';
        } elseif ($tprefix == -2) {
            $prefixsql = 'AND prefix != 0';
            $prefixsql2 = 'AND t.prefix != 0';
        } else {
            $prefixsql = $prefixsql2 = '';
        }

        // Pick the sort order.
        if (!isset($this->bb->input['order']) && !empty($foruminfo['defaultsortorder'])) {
            $this->bb->input['order'] = $foruminfo['defaultsortorder'];
        } else {
            $this->bb->input['order'] = $this->bb->getInput('order', '');
        }

        $this->bb->input['order'] = htmlspecialchars_uni($this->bb->getInput('order', ''));

        switch (my_strtolower($this->bb->input['order'])) {
            case 'asc':
                $sortordernow = 'asc';
                $ordersel['asc'] = ' selected="selected"';
                $oppsort = $this->lang->desc;
                $oppsortnext = 'desc';
                break;
            default:
                $sortordernow = 'desc';
                $ordersel['desc'] = ' selected="selected"';
                $oppsort = $this->lang->asc;
                $oppsortnext = 'asc';
                break;
        }
        $this->view->offsetSet('ordersel', $ordersel);

        // Sort by which field?
        if (!isset($this->bb->input['sortby']) && !empty($foruminfo['defaultsortby'])) {
            $this->bb->input['sortby'] = $foruminfo['defaultsortby'];
        } else {
            $this->bb->input['sortby'] = $this->bb->getInput('sortby', '');
        }

        $t = 't.';
        $sortfield2 = '';

        $sortby = htmlspecialchars_uni($this->bb->input['sortby']);

        switch ($this->bb->input['sortby']) {
            case 'subject':
                $sortfield = 'subject';
                break;
            case 'replies':
                $sortfield = 'replies';
                break;
            case 'views':
                $sortfield = 'views';
                break;
            case 'starter':
                $sortfield = 'username';
                break;
            case 'rating':
                $t = '';
                $sortfield = 'averagerating';
                $sortfield2 = ', t.totalratings DESC';
                break;
            case 'started':
                $sortfield = 'dateline';
                break;
            default:
                $sortby = 'lastpost';
                $sortfield = 'lastpost';
                $this->bb->input['sortby'] = 'lastpost';
                break;
        }

        $sortsel['rating'] = ''; // Needs to be initialized in order to speed-up things. Fixes #2031
        $sortsel[$this->bb->input['sortby']] = ' selected="selected"';

        // Pick the right string to join the sort URL
        if ($this->bb->seo_support == true) {
            $string = '?';
        } else {
            $string = '&amp;';
        }

        // Are we viewing a specific page?
        $this->bb->input['page'] = $this->bb->getInput('page', 0);
        if ($this->bb->input['page'] > 1) {
            $sorturl = $this->forum->get_forum_link($fid, $this->bb->input['page']) . $string . "datecut=$datecut&amp;prefix=$tprefix";
        } else {
            $sorturl = $this->forum->get_forum_link($fid) . $string . "datecut=$datecut&amp;prefix=$tprefix";
        }
        $this->view->offsetSet('sorturl', $sorturl);

        $orderarrow['$sortby'] = "<span class=\"smalltext\">[<a href=\"{$sorturl}&sortby={$sortby}&order={$oppsortnext}\">{$oppsort}</a>]</span>";

        $threadcount = 0;
        $useronly = $tuseronly = '';
        if (isset($fpermissions['canonlyviewownthreads']) && $fpermissions['canonlyviewownthreads'] == 1) {
            $useronly = "AND uid={$this->user->uid}";
            $tuseronly = "AND t.uid={$this->user->uid}";
        }

        if ($fpermissions['canviewthreads'] != 0) {
            // How many posts are there?
            if (($datecut > 0 && $datecut != 9999) ||
                isset($fpermissions['canonlyviewownthreads']) &&
                $fpermissions['canonlyviewownthreads'] == 1
            ) {
//                $query = $this->db->simple_select('threads', 'COUNT(tid) AS threads',
//                    "fid = '$fid' $useronly $visibleonly $datecutsql $prefixsql");
//                $threadcount = $this->db->fetch_field($query, 'threads');
                $threadcount = \RunBB\Models\Thread::whereRaw(
                    "fid = '$fid' $useronly AND $visibleonly $datecutsql $prefixsql"
                )->count('tid');
            } else {
//                $query = $this->db->simple_select('forums', 'threads, unapprovedthreads, deletedthreads',
//                    "fid = '{$fid}'", array('limit' => 1));
//                $forum_threads = $this->db->fetch_array($query);
                $f = \RunBB\Models\Forum::where('fid', $fid)
                    ->take(1)
                    ->get(['threads', 'unapprovedthreads', 'deletedthreads'])
                    ->toArray();

                $forum_threads = $f[0];

                $threadcount = $forum_threads['threads'];
                if ($ismod == true) {
                    $threadcount += $forum_threads['unapprovedthreads'] + $forum_threads['deletedthreads'];
                }

                // If we have 0 threads double check there aren't any "moved" threads
                if ($threadcount == 0) {
//                    $query = $this->db->simple_select('threads', 'COUNT(tid) AS threads',
//                        "fid = '$fid' $useronly $visibleonly", array('limit' => 1));
//                    $threadcount = $this->db->fetch_field($query, 'threads');
                    $threadcount = \RunBB\Models\Thread::whereRaw("fid = '$fid' $useronly AND $visibleonly")
                        ->take(1)
                        ->count('tid');
                }
            }
        }

        // How many pages are there?
        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        $perpage = $this->bb->settings['threadsperpage'];

        if ($this->bb->input['page'] > 0) {
            $page = $this->bb->input['page'];
            $start = ($page - 1) * $perpage;
            $pages = $threadcount / $perpage;
            $pages = ceil($pages);
            if ($page > $pages || $page <= 0) {
                $start = 0;
                $page = 1;
            }
        } else {
            $start = 0;
            $page = 1;
        }

        $end = $start + $perpage;
        $lower = $start + 1;
        $upper = $end;

        if ($upper > $threadcount) {
            $upper = $threadcount;
        }

        // Assemble page URL
        if ($this->bb->input['sortby'] || $this->bb->input['order'] || $this->bb->input['datecut'] || $this->bb->input['prefix']) { // Ugly URL
            $page_url = str_replace('{fid}', $fid, FORUM_URL_PAGED);

            if ($this->bb->seo_support == true) {
                $q = '?';
                $and = '';
            } else {
                $q = '';
                $and = '&';
            }

            if ((!empty($foruminfo['defaultsortby']) &&
                    $sortby != $foruminfo['defaultsortby']) ||
                (empty($foruminfo['defaultsortby']) && $sortby != 'lastpost')
            ) {
                $page_url .= "{$q}{$and}sortby={$sortby}";
                $q = '';
                $and = '&';
            }

            if ($sortordernow != 'desc') {
                $page_url .= "{$q}{$and}order={$sortordernow}";
                $q = '';
                $and = '&';
            }

            if ($datecut > 0 && $datecut != 9999) {
                $page_url .= "{$q}{$and}datecut={$datecut}";
                $q = '';
                $and = '&';
            }

            if ($tprefix != 0) {
                $page_url .= "{$q}{$and}prefix={$tprefix}";
            }
        } else {
            $page_url = str_replace('{fid}', $fid, FORUM_URL_PAGED);
        }
        $multipage = $this->pagination->multipage($threadcount, $perpage, $page, $page_url);
        $this->view->offsetSet('multipage', $multipage);

        $ratingsort = '';
        if ($this->bb->settings['allowthreadratings'] != 0 &&
            $foruminfo['allowtratings'] != 0 &&
            $fpermissions['canviewthreads'] != 0
        ) {
            $this->lang->load('ratethread');

            switch ($this->db->type) {
                case 'pgsql':
                    $ratingadd = 'CASE WHEN t.numratings=0 THEN 0 ELSE t.totalratings/t.numratings::numeric END AS averagerating, ';
                    break;
                default:
                    $ratingadd = '(t.totalratings/t.numratings) AS averagerating, ';
            }

            $lpbackground = 'trow2';
            //FIXME add lang ratings_update_error !!!
            $ratingsort = "<option value=\"rating\" {$sortsel['rating']}>{$this->lang->sort_by_rating}</option>";
            $colspan = '7';
        } else {
            if ($sortfield == 'averagerating') {
                $t = 't.';
                $sortfield = 'lastpost';
            }
            $ratingadd = '';
            $lpbackground = 'trow1';
            $colspan = '6';
        }
        $this->view->offsetSet('ratingsort', $ratingsort);

        if ($ismod) {
            ++$colspan;
        }
        $this->view->offsetSet('colspan', $colspan);

        // Get Announcements
        $announcementlist = '';
        if ($has_announcements == true) {
            $limit = '';
            $announcements = [];
            // def val $this->bb->settings['announcementlimit'] = 2
//            if ($this->bb->settings['announcementlimit']) {
//                $limit = 'LIMIT 0, ' . $this->bb->settings['announcementlimit'];
//            }

            $sql = $this->forum->build_parent_list($fid, 'fid', 'OR', $parentlist);

            $time = TIME_NOW;
//            $query = $this->db->query('
//		SELECT a.*, u.username
//		FROM ' . TABLE_PREFIX . 'announcements a
//		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=a.uid)
//		WHERE a.startdate<='$time' AND (a.enddate>='$time' OR a.enddate='0') AND ($sql OR fid='-1')
//		ORDER BY a.startdate DESC $limit
//	");
            $a = \RunBB\Models\Announcement::whereRaw(
                "startdate<='$time' AND (enddate>='$time' OR enddate='0') AND ($sql OR fid='-1')"
            )
                ->leftJoin('users', 'users.uid', '=', 'announcements.uid')
                ->orderBy('announcements.startdate', 'desc')
//                ->skip(0)
                ->take($this->bb->settings['announcementlimit'])
                ->get(['announcements.*', 'users.username'])
                ->toArray();
            // See if this announcement has been read in our announcement array
            $cookie = [];
            if (isset($this->bb->cookies['mybb']['announcements'])) {
                $cookie = my_unserialize(stripslashes($this->bb->cookies['mybb']['announcements']));
            }

            $announcementlist = '';
            $bgcolor = alt_trow(true); // Reset the trow colors
//            while ($announcement = $this->db->fetch_array($query)) {
            foreach ($a as $announcement) {
                if ($announcement['startdate'] > $this->user->lastvisit && !isset($cookie[$announcement['aid']])) {
                    $new_class = ' class="subject_new"';
                    $folder = 'newfolder';
                } else {
                    $new_class = ' class="subject_old"';
                    $folder = 'folder';
                }

                // Mmm, eat those announcement cookies if they're older than our last visit
                if (isset($cookie[$announcement['aid']]) && $cookie[$announcement['aid']] < $this->user->lastvisit) {
                    unset($cookie[$announcement['aid']]);
                }

                $announcement['announcementlink'] = get_announcement_link($announcement['aid']);
                $announcement['subject'] = $this->parser->parse_badwords($announcement['subject']);
                $announcement['subject'] = htmlspecialchars_uni($announcement['subject']);
                $postdate = $this->time->formatDate('relative', $announcement['startdate']);
                $announcement['profilelink'] = $this->user->build_profile_link($announcement['username'], $announcement['uid']);

                if ($this->bb->settings['allowthreadratings'] != 0 &&
                    $foruminfo['allowtratings'] != 0 &&
                    $fpermissions['canviewthreads'] != 0
                ) {
//          $rating = "	<td class=\"{$bgcolor} forumdisplay_announcement\" align=\"center\">-</td>";
                    $lpbackground = 'trow2';
                } else {
//          $rating = '';
                    $lpbackground = 'trow1';
                }

                $this->plugins->runHooks('forumdisplay_announcement');
                $announcements[] = [
                    'bgcolor' => $bgcolor,
                    'folder' => $folder,
                    'lpbackground' => $lpbackground,
                    'announcementlink' => $announcement['announcementlink'],
                    'new_class' => $new_class,
                    'subject' => $announcement['subject'],
                    'profilelink' => $announcement['profilelink'],
//          'rating' => $rating,
                    'postdate' => $postdate,
//          'modann' => $modann
                ];
                $bgcolor = alt_trow();
            }
            $this->view->offsetSet('announcements', $announcements);

            if ($announcements) {
                $shownormalsep = true;
            }

            if (empty($cookie)) {
                // Clean up cookie crumbs
                $this->bb->my_setcookie('mybb[announcements]', 0, (TIME_NOW - (60 * 60 * 24 * 365)));
            } elseif (!empty($cookie)) {
                $this->bb->my_setcookie('mybb[announcements]', addslashes(my_serialize($cookie)), -1);
            }
        }

        $tids = $threadcache = [];
        $icon_cache = $this->cache->read('posticons');

        if ($fpermissions['canviewthreads'] != 0) {
            $this->plugins->runHooks('forumdisplay_get_threads');

            // Start Getting Threads
            $q = "SELECT t.*, {$ratingadd}t.username AS threadusername, u.username
		        FROM " . TABLE_PREFIX . 'threads t
		        LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid = t.uid)
		        WHERE t.fid='$fid' $tuseronly $tvisibleonly $datecutsql2 $prefixsql2
		        ORDER BY t.sticky DESC, {$t}{$sortfield} $sortordernow $sortfield2
		        LIMIT $start, $perpage";
            $query = $this->db->query($q);

            $ratings = false;
            $moved_threads = [];
            while ($thread = $this->db->fetch_array($query)) {
                $threadcache[$thread['tid']] = $thread;

                if ($thread['numratings'] > 0 && $ratings == false) {
                    $ratings = true; // Looks for ratings in the forum
                }

                // If this is a moved thread - set the tid for participation marking
                // and thread read marking to that of the moved thread
                if (substr($thread['closed'], 0, 5) == 'moved') {
                    $tid = substr($thread['closed'], 6);
                    if (!isset($tids[$tid])) {
                        $moved_threads[$tid] = $thread['tid'];
                        $tids[$thread['tid']] = $tid;
                    }
                } // Otherwise - set it to the plain thread ID
                else {
                    $tids[$thread['tid']] = $thread['tid'];
                    if (isset($moved_threads[$thread['tid']])) {
                        unset($moved_threads[$thread['tid']]);
                    }
                }
            }

            if ($this->bb->settings['allowthreadratings'] != 0 &&
                $foruminfo['allowtratings'] != 0 && $this->user->uid &&
                !empty($threadcache) && $ratings == true
            ) {
                // Check if we've rated threads on this page
                // Guests get the pleasure of not being ID'd, but will be checked when they try and rate
//                $imp = implode(',', array_keys($threadcache));
//                $query = $this->db->simple_select('threadratings', 'tid, uid',
//                    "tid IN ({$imp}) AND uid = '{$this->user->uid}'");
                $r = \RunBB\Models\Threadrating::where('uid', '=', $this->user->uid)
                    ->whereIn('tid', array_keys($threadcache))
                    ->get(['tid', 'uid'])
                    ->toArray();
//                while ($rating = $this->db->fetch_array($query)) {
                foreach ($r as $rating) {
                    $threadcache[$rating['tid']]['rated'] = 1;
                }
            }
        }

        // If user has moderation tools available, prepare the Select All feature
        $this->bb->showSelectall = false;
        if ($this->user->is_moderator($fid) && $threadcount > $perpage) {
            $this->bb->showSelectall = true;
            $this->lang->page_selected = $this->lang->sprintf($this->lang->page_selected, count($threadcache));
            $this->lang->select_all = $this->lang->sprintf($this->lang->select_all, (int)$threadcount);
            $this->lang->all_selected = $this->lang->sprintf($this->lang->all_selected, (int)$threadcount);
        }

//        if (!empty($tids)) {
//            $tids = implode(',', $tids);
//        }
        // Check participation by the current user in any of these threads - for 'dot' folder icons
        if ($this->bb->settings['dotfolders'] != 0 && $this->user->uid && !empty($threadcache)) {
//            $query = $this->db->simple_select('posts', 'DISTINCT tid,uid',
//                "uid='{$this->user->uid}' AND tid IN (".implode(',', $tids).") AND {$visibleonly}");
            $p = \RunBB\Models\Post::where('uid', '=', $this->user->uid)
                ->whereIn('tid', $tids)
                ->whereRaw($visibleonly)
                ->distinct()
                ->get(['tid', 'uid'])
                ->toArray();

//            while ($post = $this->db->fetch_array($query)) {
            foreach ($p as $post) {
                if (!empty($moved_threads[$post['tid']])) {
                    $post['tid'] = $moved_threads[$post['tid']];
                }
                if ($threadcache[$post['tid']]) {
                    $threadcache[$post['tid']]['doticon'] = 1;
                }
            }
        }

        // Read threads
        if ($this->user->uid && $this->bb->settings['threadreadcut'] > 0 && !empty($threadcache)) {
//            $query = $this->db->simple_select('threadsread', '*', "uid='{$this->user->uid}' AND tid IN (".implode(',', $tids).")");
            $p = \RunBB\Models\Threadsread::where('uid', '=', $this->user->uid)
                ->whereIn('tid', $tids)
//                ->distinct()
                ->get()
                ->toArray();

//            while ($readthread = $this->db->fetch_array($query)) {
            foreach ($p as $readthread) {
                if (!empty($moved_threads[$readthread['tid']])) {
                    $readthread['tid'] = $moved_threads[$readthread['tid']];
                }
                if ($threadcache[$readthread['tid']]) {
                    $threadcache[$readthread['tid']]['lastread'] = $readthread['dateline'];
                }
            }
        }

        if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
//            $query = $this->db->simple_select('forumsread', 'dateline', "fid='{$fid}' AND uid='{$this->user->uid}'");
//            $forum_read = $this->db->fetch_field($query, 'dateline');
            $forum_read = \RunBB\Models\Forumsread::where([
                    ['fid', '=', "'$fid'"], ['uid', '=', "'{$this->user->uid}'"]
                ])
                ->count();

            $read_cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
            if ($forum_read == 0 || $forum_read < $read_cutoff) {
                $forum_read = $read_cutoff;
            }
        } else {
            $forum_read = $this->bb->my_get_array_cookie('forumread', $fid);

            if (isset($this->bb->cookies['mybb']['readallforums']) && !$forum_read) {
                $forum_read = $this->bb->cookies['mybb']['lastvisit'];
            }
        }

        $unreadpost = 0;
        $threads = '';
        $this->bb->showInlineModeration = false;
        if (!empty($threadcache) && is_array($threadcache)) {
            if (!$this->bb->settings['maxmultipagelinks']) {
                $this->bb->settings['maxmultipagelinks'] = 5;
            }

            if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                $this->bb->settings['postsperpage'] = 20;
            }

            foreach ($threadcache as $thread) {
                $this->plugins->runHooks('forumdisplay_thread');

                $moved = explode('|', $thread['closed']);

                if ($thread['visible'] == 0) {
                    $bgcolor = 'trow_shaded';
                } elseif ($thread['visible'] == -1) {
                    $bgcolor = 'trow_shaded trow_deleted';
                } else {
                    $bgcolor = alt_trow();
                }

                if ($thread['sticky'] == 1) {
                    $thread_type_class = ' forumdisplay_sticky';
                } else {
                    $thread_type_class = ' forumdisplay_regular';
                }

                $folder = '';
                $prefix = '';

                $thread['author'] = $thread['uid'];
                if (!$thread['username']) {
                    $thread['username'] = $thread['threadusername'];
                    $thread['profilelink'] = $thread['threadusername'];
                } else {
                    $thread['profilelink'] = $this->user->build_profile_link($thread['username'], $thread['uid']);
                }

                // If this thread has a prefix, insert a space between prefix and subject
                $thread['threadprefix'] = $threadprefix = '';
                if ($thread['prefix'] != 0) {
                    $threadprefix = $this->thread->build_prefixes($thread['prefix']);
                    if (!empty($threadprefix)) {
                        $thread['threadprefix'] = $threadprefix['displaystyle'] . '&nbsp;';
                    }
                }

                $thread['subject'] = $this->parser->parse_badwords($thread['subject']);
                $thread['subject'] = htmlspecialchars_uni($thread['subject']);

                if ($thread['icon'] > 0 && $icon_cache[$thread['icon']]) {
                    $icon = $icon_cache[$thread['icon']];
                    $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
                    $icon['path'] = htmlspecialchars_uni($icon['path']);
                    $icon['name'] = htmlspecialchars_uni($icon['name']);
                    $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" title=\"{$icon['name']}\" />";
                } else {
                    $icon = '&nbsp;';
                }

                $prefix = '';
                if ($thread['poll']) {
                    $prefix = $this->lang->poll_prefix;
                }

                if ($thread['sticky'] == '1' && !isset($donestickysep)) {
                    $threads .= '<div class="list-group-item inline_row trow1 forumdisplay_sticky">' . $this->lang->sticky_threads . '</div>';
                    $shownormalsep = true;
                    $donestickysep = true;
                } elseif ($thread['sticky'] == 0 && !empty($shownormalsep)) {
                    $threads .= '<div class="list-group-item list-group-item-info">' . $this->lang->normal_threads . '</div>';
                    $shownormalsep = false;
                }

                $rating = '';
                if ($this->bb->settings['allowthreadratings'] != 0 && $foruminfo['allowtratings'] != 0) {
                    if ($moved[0] == 'moved') {
                        $rating = "<td class=\"{$bgcolor}\" style=\"text-align: center;\">-</td>";
                    } else {
                        $thread['averagerating'] = (float)round($thread['averagerating'], 2);
                        $thread['width'] = (int)round($thread['averagerating']) * 20;
                        $thread['numratings'] = (int)$thread['numratings'];

                        $not_rated = '';
                        if (!isset($thread['rated']) || empty($thread['rated'])) {
                            $not_rated = ' star_rating_notrated';
                        }

                        $ratingvotesav = $this->lang->sprintf($this->lang->rating_votes_average, $thread['numratings'], $thread['averagerating']);
//<td align=\"center\" class=\"{$bgcolor}{$thread_type_class}\" id=\"rating_table_{$thread['tid']}\">
                        $rating = '
                        <div class="hidden-xs col-sm-2 text-center text-nowrap">
                            <div id="rating_table_' . $thread['tid'] . '">
                                <ul class="star_rating' . $not_rated . '" id="rating_thread_' . $thread['tid'] . '">
                                  <li style="width: ' . $thread['width'] . '%" class="current_rating" id="current_rating_' . $thread['tid'] . '">' . $ratingvotesav . '</li>
                                </ul>
                                <script type="text/javascript">
                                <!--
                                  Rating.build_forumdisplay(' . $thread['tid'] . ', { width: "' . $thread['width'] . '", extra_class: "' . $not_rated . '", current_average: "' . $ratingvotesav . '" });
                                // -->
                                </script>
                            </div>
                        </div>';
                    }
                }

                $thread['pages'] = 0;
                $thread['multipage'] = '';
                $threadpages = '';
                $morelink = '';
                $thread['posts'] = $thread['replies'] + 1;

                if ($thread['unapprovedposts'] > 0 && $ismod) {
                    $thread['posts'] += $thread['unapprovedposts'] + $thread['deletedposts'];
                }

                if ($thread['posts'] > $this->bb->settings['postsperpage']) {
                    $thread['pages'] = $thread['posts'] / $this->bb->settings['postsperpage'];
                    $thread['pages'] = ceil($thread['pages']);

                    if ($thread['pages'] > $this->bb->settings['maxmultipagelinks']) {
                        $pagesstop = $this->bb->settings['maxmultipagelinks'] - 1;
                        $page_link = get_thread_link($thread['tid'], $thread['pages']);
                        $morelink = "... <a href=\"{$page_link}\">{$thread['pages']}</a>";
                    } else {
                        $pagesstop = $thread['pages'];
                    }

                    for ($i = 1; $i <= $pagesstop; ++$i) {
                        $page_link = get_thread_link($thread['tid'], $i);
                        $threadpages .= "<a href=\"{$page_link}\">{$i}</a> ";
                    }
                    $thread['multipage'] = " <span class=\"smalltext\">({$this->lang->pages} {$threadpages}{$morelink})</span>";
                } else {
                    $threadpages = '';
                    $morelink = '';
                    $thread['multipage'] = '';
                }

                if ($ismod) {
                    if (isset($this->bb->cookies[$this->bb->inlinecookie]) && my_strpos($this->bb->cookies[$this->bb->inlinecookie], "|{$thread['tid']}|")) {
                        $inlinecheck = 'checked="checked"';
                        ++$this->inlinecount;
                    } else {
                        $inlinecheck = '';
                    }

                    $multitid = $thread['tid'];
                    $modbit = "<div class=\"hidden-xs col-sm-1 text-center\"><input type=\"checkbox\" name=\"inlinemod_{$multitid}\" id=\"inlinemod_{$multitid}\" value=\"1\" {$inlinecheck}  /></div>";
                } else {
                    $modbit = '';
                }

                if ($moved[0] == 'moved') {
                    $prefix = $this->lang->moved_prefix;
                    $thread['tid'] = $moved[1];
                    $thread['replies'] = '-';
                    $thread['views'] = '-';
                }

                $thread['threadlink'] = get_thread_link($thread['tid']);
                $thread['lastpostlink'] = get_thread_link($thread['tid'], 0, 'lastpost');

                // Determine the folder
                $folder = '';
                $folder_label = '';

                if (isset($thread['doticon'])) {
                    $folder = 'dot_';
                    $folder_label .= $this->lang->icon_dot;
                }

                $gotounread = '';
                $isnew = 0;
                $donenew = 0;

                if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid && $thread['lastpost'] > $forum_read) {
                    if (!empty($thread['lastread'])) {
                        $last_read = $thread['lastread'];
                    } else {
                        $last_read = $read_cutoff;
                    }
                } else {
                    $last_read = $this->bb->my_get_array_cookie('threadread', $thread['tid']);
                }

                if ($forum_read > $last_read) {
                    $last_read = $forum_read;
                }

                if ($thread['lastpost'] > $last_read && $moved[0] != 'moved') {
                    $folder .= 'new';
                    $folder_label .= $this->lang->icon_new;
                    $new_class = 'subject_new';
                    $thread['newpostlink'] = get_thread_link($thread['tid'], 0, 'newpost');
                    $gotounread = "<a href=\"{$thread['newpostlink']}\"><img src=\"{$this->bb->theme['imgdir']}/jump.png\" alt=\"{$this->lang->goto_first_unread}\" title=\"{$this->lang->goto_first_unread}\" /></a> ";
                    $unreadpost = 1;
                } else {
                    $folder_label .= $this->lang->icon_no_new;
                    $new_class = 'subject_old';
                }

                if ($thread['replies'] >= $this->bb->settings['hottopic'] || $thread['views'] >= $this->bb->settings['hottopicviews']) {
                    $folder .= 'hot';
                    $folder_label .= $this->lang->icon_hot;
                }

                if ($thread['closed'] == 1) {
                    $folder .= 'lock';
                    $folder_label .= $this->lang->icon_lock;
                }

                if ($moved[0] == 'moved') {
                    $folder = 'move';
                    $gotounread = '';
                }

                $folder .= 'folder';

                $inline_edit_tid = $thread['tid'];

                // If this user is the author of the thread and it is not closed or they are a moderator, they can edit
                $inline_edit_class = '';
                if (($thread['uid'] == $this->user->uid && $thread['closed'] != 1 &&
                        $this->user->uid != 0 && $can_edit_titles == 1) || $ismod == true
                ) {
                    $inline_edit_class = 'subject_editable';
                }

                $lastposter = $thread['lastposter'];
                $lastposteruid = $thread['lastposteruid'];
                $lastpostdate = $this->time->formatDate('relative', $thread['lastpost']);

                // Don't link to guest's profiles (they have no profile).
                if ($lastposteruid == 0) {
                    $lastposterlink = $lastposter;
                } else {
                    $lastposterlink = $this->user->build_profile_link($lastposter, $lastposteruid);
                }

                $thread['replies'] = $this->parser->formatNumber($thread['replies']);
                $thread['views'] = $this->parser->formatNumber($thread['views']);

                // Threads and posts requiring moderation
                if ($thread['unapprovedposts'] > 0 && $this->user->is_moderator($fid, 'canviewunapprove')) {
                    if ($thread['unapprovedposts'] > 1) {
                        $unapproved_posts_count = $this->lang->sprintf($this->lang->thread_unapproved_posts_count, $thread['unapprovedposts']);
                    } else {
                        $unapproved_posts_count = $this->lang->sprintf($this->lang->thread_unapproved_post_count, 1);
                    }

                    $thread['unapprovedposts'] = $this->parser->formatNumber($thread['unapprovedposts']);
                    $unapproved_posts = " <span title=\"{$unapproved_posts_count}\">({$thread['unapprovedposts']})</span>";
                } else {
                    $unapproved_posts = '';
                }

                // If this thread has 1 or more attachments show the papperclip
                if ($this->bb->settings['enableattachments'] == 1 && $thread['attachmentcount'] > 0) {
                    if ($thread['attachmentcount'] > 1) {
                        $attachment_count = $this->lang->sprintf($this->lang->attachment_count_multiple, $thread['attachmentcount']);
                    } else {
                        $attachment_count = $this->lang->attachment_count;
                    }
                    $attachment_count = "<div class=\"float_right\"><img src=\"{$this->bb->theme['imgdir']}/paperclip.png\" alt=\"\" title=\"{$attachment_count}\" /></div>";
                } else {
                    $attachment_count = '';
                }

                $this->plugins->runHooks('forumdisplay_thread_end');
                $threads .= '
                <div class="list-group-item inline_row ' . $bgcolor . $thread_type_class . '">
                    <div class="row">
                        <div class="col-xs-11 col-sm-5">
                            <div class="media">
                                <div class="media-left" style="padding-right: 1em;">
                                    <span class="thread_status ' . $folder . '" title="' . $folder_label . '">&nbsp;</span>
                                </div>
                                <div class="media-body">
                                    ' . $attachment_count . '
                                    <span class="' . $bgcolor . $thread_type_class . '">' . $icon . '</span>&nbsp;
                                    <span>' . $prefix . ' ' . $gotounread . $thread['threadprefix'] . '
                                      <span class="' . $inline_edit_class . ' ' . $new_class . '" id="tid_' . $inline_edit_tid . '"><a href="' . $thread['threadlink'] . '">' . $thread['subject'] . '</a></span>
                                      ' . $thread['multipage'] . '
                                    </span>
                                    <div class="small">' . $thread['profilelink'] . '</div>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-xs col-sm-1 text-center" style="text-overflow: ellipsis; overflow: hidden;">
                          <a href=\"javascript:MyBB.whoPosted(' . $thread['tid'] . ');">' . $thread['replies'] . '</a>' . $unapproved_posts . '
                        </div>
                        <div class="hidden-xs col-sm-1 text-center" style="text-overflow: ellipsis; overflow: hidden;">
                          ' . $thread['views'] . '
                        </div>
                        ' . $rating . '
                        <div class="hidden-xs col-sm-2">
                          <span class="small">' . $lastpostdate . '<br />
                          <a href="' . $thread['lastpostlink'] . '">' . $this->lang->lastpost . '</a>: ' . $lastposterlink . '</span>
                        </div>
                        ' . $modbit . '
                    </div>
                </div>';
            }

            $customthreadtools = $standardthreadtools = '';
            if ($ismod) {
                if ($this->user->is_moderator($fid, 'canusecustomtools') && $has_modtools == true) {
                    $gids = explode(',', $this->user->additionalgroups);
                    $gids[] = $this->user->usergroup;
                    $gids = array_filter(array_unique($gids));

                    $gidswhere = '';
                    switch ($this->db->type) {
                        case 'pgsql':
                        case 'sqlite':
                            foreach ($gids as $gid) {
                                $gid = (int)$gid;
                                $gidswhere .= " OR ','||groups||',' LIKE '%,{$gid},%'";
                            }
                            $query = $this->db->simple_select(
                                'modtools',
                                'tid, name',
                                "(','||forums||',' LIKE '%,$fid,%' OR ','||forums||',' LIKE '%,-1,%' OR forums='') AND (groups='' OR ','||groups||',' LIKE '%,-1,%'{$gidswhere}) AND type = 't'"
                            );
                            break;
                        default:
                            foreach ($gids as $gid) {
                                $gid = (int)$gid;
                                $gidswhere .= " OR CONCAT(',',groups,',') LIKE '%,{$gid},%'";
                            }
                            $query = $this->db->simple_select(
                                'modtools',
                                'tid, name',
                                "(CONCAT(',',forums,',') LIKE '%,$fid,%' OR CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='') AND (groups='' OR CONCAT(',',groups,',') LIKE '%,-1,%'{$gidswhere}) AND type = 't'"
                            );
                            break;
                    }

                    while ($tool = $this->db->fetch_array($query)) {
                        $customthreadtools .= "<option value=\"{$tool['tid']}\">{$tool['name']}</option>";
                    }
                }

                $inlinemodopenclose = $inlinemodstickunstick = $inlinemodsoftdelete =
                $inlinemodrestore = $inlinemoddelete = $inlinemodmanage = $inlinemodapproveunapprove = '';

                if ($this->user->is_moderator($fid, 'canopenclosethreads')) {
                    $inlinemodopenclose = "
                    <option value=\"multiclosethreads\">{$this->lang->close_threads}</option>
                    <option value=\"multiopenthreads\">{$this->lang->open_threads}</option>";
                }
                $this->view->offsetSet('inlinemodopenclose', $inlinemodopenclose);

                if ($this->user->is_moderator($fid, 'canstickunstickthreads')) {
                    $inlinemodstickunstick = "
                    <option value=\"multistickthreads\">{$this->lang->stick_threads}</option>
                    <option value=\"multiunstickthreads\">{$this->lang->unstick_threads}</option>";
                }
                $this->view->offsetSet('inlinemodstickunstick', $inlinemodstickunstick);

                if ($this->user->is_moderator($fid, 'cansoftdeletethreads')) {
                    $inlinemodsoftdelete = "
                    <option value=\"multisoftdeletethreads\">{$this->lang->soft_delete_threads}</option>";
                }
                $this->view->offsetSet('inlinemodsoftdelete', $inlinemodsoftdelete);

                if ($this->user->is_moderator($fid, 'canrestorethreads')) {
                    $inlinemodrestore = "
                    <option value=\"multirestorethreads\">{$this->lang->restore_threads}</option>";
                }
                $this->view->offsetSet('inlinemodrestore', $inlinemodrestore);

                if ($this->user->is_moderator($fid, 'candeletethreads')) {
                    $inlinemoddelete = "
                    <option value=\"multideletethreads\">{$this->lang->delete_threads}</option>";
                }
                $this->view->offsetSet('inlinemoddelete', $inlinemoddelete);

                if ($this->user->is_moderator($fid, 'canmanagethreads')) {
                    $inlinemodmanage = "
                    <option value=\"multimovethreads\">{$this->lang->move_threads}</option>";
                }
                $this->view->offsetSet('inlinemodmanage', $inlinemodmanage);

                if ($this->user->is_moderator($fid, 'canapproveunapproveposts')) {
                    $inlinemodapproveunapprove = "
                    <option value=\"multiapprovethreads\">{$this->lang->approve_threads}</option>
                    <option value=\"multiunapprovethreads\">{$this->lang->unapprove_threads}</option>";
                }
                $this->view->offsetSet('inlinemodapproveunapprove', $inlinemodapproveunapprove);

                $this->bb->showStandardthreadtools = false;
                if (!empty($inlinemodopenclose) || !empty($inlinemodstickunstick) ||
                    !empty($inlinemodsoftdelete) || !empty($inlinemodrestore) || !empty($inlinemoddelete) ||
                    !empty($inlinemodmanage) || !empty($inlinemodapproveunapprove)
                ) {
                    $this->bb->showStandardthreadtools = true;
                }

                // Only show inline mod menu if there's options to show
                if ($this->bb->showStandardthreadtools || !empty($customthreadtools)) {
                    $this->bb->showInlineModeration = true;
                }
            }
            $this->view->offsetSet('customthreadtools', $customthreadtools);
        }

        // If there are no unread threads in this forum and no unread child forums - mark it as read
        $unread_threads = $this->indicator->fetch_unread_count($fid);
        if ($unread_threads !== false && $unread_threads == 0 && empty($unread_forums)) {//FIXME $unread_forums inside Forum
            $this->indicator->mark_forum_read($fid);
        }

        // Subscription status
        $add_remove_subscription = 'add';
        $add_remove_subscription_text = $this->lang->subscribe_forum;

        if ($this->user->uid) {
            $r = \RunBB\Models\Forumsubscription::where([
                ['fid', $fid],
                ['uid', $this->user->uid]
            ])
                ->count();

            if ($r > 0) {
                $add_remove_subscription = 'remove';
                $add_remove_subscription_text = $this->lang->unsubscribe_forum;
            }
        }
        $this->view->offsetSet('add_remove_subscription', $add_remove_subscription);
        $this->view->offsetSet('add_remove_subscription_text', $add_remove_subscription_text);

        $inline_edit_js = $clearstoredpass = '';

        // Is this a real forum with threads?
        $this->bb->showThreads = false;
        if ($foruminfo['type'] != 'c') {
            $this->bb->showThreads = true;
            if ($fpermissions['canviewthreads'] != 1) {
                $threads = "
                <div class=\"row\">
                    <div class=\"text-center trow1\"><p>{$this->lang->nopermission}</p></div>
                </div>";
            }

            if (!$threadcount && $fpermissions['canviewthreads'] == 1) {
                $threads = "
                <div class=\"row\">
                    <div class=\"text-center\"><p>{$this->lang->nothreads}</p></div>
                </div>";
            }

            $post_code_string = '';
            if ($this->user->uid) {
                $post_code_string = '&my_post_key=' . $this->bb->post_code;
            }
            $this->view->offsetSet('post_code_string', $post_code_string);

            $prefixselect = $this->thread->build_forum_prefix_select($fid, $tprefix);
            $this->view->offsetSet('prefixselect', $prefixselect);

            $this->lang->rss_discovery_forum = $this->lang->sprintf(
                $this->lang->rss_discovery_forum,
                htmlspecialchars_uni(strip_tags($foruminfo['name']))
            );
        } else {
            if (empty($forums)) {
                $this->bb->error($this->lang->error_containsnoforums);
            }
        }
        $this->view->offsetSet('fid', $fid);
        $this->view->offsetSet('sortsel', $sortsel);
        $this->view->offsetSet('foruminfo', $foruminfo);
        $this->view->offsetSet('orderarrow', $orderarrow);
        $this->view->offsetSet('threads', $threads);
        $this->view->offsetSet('inlinecount', $this->inlinecount);
        $this->view->offsetSet('threadcount', $threadcount);


        $this->plugins->runHooks('forumdisplay_end');

        $foruminfo['name'] = strip_tags($foruminfo['name']);

        $this->bb->output_page();
        $this->view->render($response, '@forum/forumdisplay.html.twig');
    }
}
