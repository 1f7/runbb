<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;

class Index
{
    protected $c;

    public function __construct(Container $c)
    {
        $this->c = $c;
    }

    public function index(Request $request, Response $response)
    {
        $this->c['plugins']->runHooks('index_start');

        // Load global language phrases
        $this->c['lang']->load('index');

//tdie($this);
        $this->c['bb']->showWhosOnline = false;
        if ($this->c['bb']->settings['showwol'] != 0 && $this->c['bb']->usergroup['canviewonline'] != 0) {
            // Get the online users.
            if ($this->c['bb']->settings['wolorder'] == 'username') {
//                $order_by = 'u.username ASC';
//                $order_by2 = 's.time DESC';
                $order_e = [0 => 'users.username', 1 => 'ASC'];
                $order_e2 = [0 => 'sessions.time', 1 => 'DESC'];
            } else {
//                $order_by = 's.time DESC';
//                $order_by2 = 'u.username ASC';
                $order_e = [0 => 'sessions.time', 1 => 'DESC'];
                $order_e2 = [0 => 'users.username', 1 => 'ASC'];
            }

            $timesearch = TIME_NOW - (int)$this->c['bb']->settings['wolcutoff'];
//            $comma = '';
//            $query = $this->db->query("
//		SELECT s.sid, s.ip, s.uid, s.time, s.location, s.location1, u.username, u.invisible, u.usergroup, u.displaygroup
//		FROM " . TABLE_PREFIX . "sessions s
//		LEFT JOIN " . TABLE_PREFIX . "users u ON (s.uid=u.uid)
//		WHERE s.time > '" . $timesearch . "'
//		ORDER BY {$order_by}, {$order_by2}
//	");
            $u = \RunBB\Models\Session::where('sessions.time', '>', $timesearch)
                ->orderBy($order_e[0], $order_e[1])
                ->orderBy($order_e2[0], $order_e2[1])
                ->select(
                    'sessions.sid',
                    'sessions.ip',
                    'sessions.uid',
                    'sessions.time',
                    'sessions.location',
                    'sessions.location1',
                    'users.username',
                    'users.invisible',
                    'users.usergroup',
                    'users.displaygroup'
                )
                ->leftJoin('users', 'users.uid', '=', 'sessions.uid')
                ->get()
                ->toArray();

            $this->c['bb']->forum_viewers = $doneusers = [];
            $membercount = $guestcount = $anoncount = $botcount = 0;
            $onlinemembers = $comma = '';

            // Fetch spiders
            $spiders = $this->c['cache']->read('spiders');

            // Loop through all users.
//            while ($user = $this->db->fetch_array($query)) {
            foreach ($u as $user) {
                // Create a key to test if this user is a search bot.
                $botkey = my_strtolower(str_replace('bot=', '', $user['sid']));

                // Decide what type of user we are dealing with.
                if ($user['uid'] > 0) {
                    // The user is registered.
                    if (empty($doneusers[$user['uid']]) || $doneusers[$user['uid']] < $user['time']) {
                        // If the user is logged in anonymously, update the count for that.
                        if ($user['invisible'] == 1) {
                            ++$anoncount;
                        }
                        ++$membercount;
                        if ($user['invisible'] != 1 ||
                            $this->c['bb']->usergroup['canviewwolinvis'] == 1 ||
                            $user['uid'] == $this->c['user']->uid) {
                            // If this usergroup can see anonymously logged-in users, mark them.
                            if ($user['invisible'] == 1) {
                                $invisiblemark = '*';
                            } else {
                                $invisiblemark = '';
                            }

                            // Properly format the username and assign the template.
                            $user['username'] = $this->c['user']->format_name(
                                $user['username'],
                                $user['usergroup'],
                                $user['displaygroup']
                            );
                            $user['profilelink'] = $this->c['user']->build_profile_link(
                                $user['username'],
                                $user['uid']
                            );
                            $onlinemembers .= $comma . $user['profilelink'] . $invisiblemark;
                            $comma = $this->c['lang']->comma;
                        }
                        // This user has been handled.
                        $doneusers[$user['uid']] = $user['time'];
                    }
                } elseif (my_strpos($user['sid'], 'bot=') !== false && $spiders[$botkey]) {
                    // The user is a search bot.
                    $onlinemembers .= $comma . $this->c['user']->format_name(
                        $spiders[$botkey]['name'],
                        $spiders[$botkey]['usergroup']
                    );
                    $comma = $this->c['lang']->comma;
                    ++$botcount;
                } else {
                    // The user is a guest.
                    ++$guestcount;
                }

                if ($user['location1']) {
                    ++$this->c['bb']->forum_viewers[$user['location1']];
                }
            }

            // Build the who's online bit on the index page.
            $onlinecount = $membercount + $guestcount + $botcount;

            if ($onlinecount != 1) {
                $onlinebit = $this->c['lang']->online_online_plural;
            } else {
                $onlinebit = $this->c['lang']->online_online_singular;
            }
            if ($membercount != 1) {
                $memberbit = $this->c['lang']->online_member_plural;
            } else {
                $memberbit = $this->c['lang']->online_member_singular;
            }
            if ($anoncount != 1) {
                $anonbit = $this->c['lang']->online_anon_plural;
            } else {
                $anonbit = $this->c['lang']->online_anon_singular;
            }
            if ($guestcount != 1) {
                $guestbit = $this->c['lang']->online_guest_plural;
            } else {
                $guestbit = $this->c['lang']->online_guest_singular;
            }
            $this->c['lang']->online_note = $this->c['lang']->sprintf(
                $this->c['lang']->online_note,
                $this->c['parser']->formatNumber($onlinecount),
                $onlinebit,
                $this->c['bb']->settings['wolcutoffmins'],
                $this->c['parser']->formatNumber($membercount),
                $memberbit,
                $this->c['parser']->formatNumber($anoncount),
                $anonbit,
                $this->c['parser']->formatNumber($guestcount),
                $guestbit
            );
            $this->c['bb']->showWhosOnline = true;
            $this->c['view']->offsetSet('onlinemembers', $onlinemembers);
        }

        // Build the birthdays for to show on the index page.
        $bdays = $birthdays = '';
        $this->c['bb']->showBirthDays = false;
        if ($this->c['bb']->settings['showbirthdays'] != 0) {
            // First, see what day this is.
            $bdaycount = $bdayhidden = 0;
            $bdaydate = $this->c['time']->formatDate('j-n', TIME_NOW, '', 0);
            $year = $this->c['time']->formatDate('Y', TIME_NOW, '', 0);

            $bdaycache = $this->c['cache']->read('birthdays');

            if (!is_array($bdaycache)) {
                $this->c['cache']->update_birthdays();
                $bdaycache = $this->c['cache']->read('birthdays');
            }

            $hiddencount = $today_bdays = 0;
            if (isset($bdaycache[$bdaydate])) {
                $hiddencount = isset($bdaycache[$bdaydate]['hiddencount']) ? $bdaycache[$bdaydate]['hiddencount'] : 0;
                $today_bdays = $bdaycache[$bdaydate]['users'];
            }

            $comma = '';
            if (!empty($today_bdays)) {
                if ((int)$this->c['bb']->settings['showbirthdayspostlimit'] > 0) {
                    $bdayusers = [];
                    foreach ($today_bdays as $key => $bdayuser_pc) {
                        $bdayusers[$bdayuser_pc['uid']] = $key;
                    }

                    if (!empty($bdayusers)) {
                        // Find out if our users have enough posts to be seen on our birthday list
                        $bday_sql = implode(',', array_keys($bdayusers));
                        $query = $this->c['db']->simple_select('users', 'uid, postnum', "uid IN ({$bday_sql})");

                        while ($bdayuser = $this->c['db']->fetch_array($query)) {
                            if ($bdayuser['postnum'] < $this->c['bb']->settings['showbirthdayspostlimit']) {
                                unset($today_bdays[$bdayusers[$bdayuser['uid']]]);
                            }
                        }
                    }
                }

                // We still have birthdays - display them in our list!
                if (!empty($today_bdays)) {
                    foreach ($today_bdays as $bdayuser) {
                        if ($bdayuser['displaygroup'] == 0) {
                            $bdayuser['displaygroup'] = $bdayuser['usergroup'];
                        }

                        // If this user's display group can't be seen in the birthday list, skip it
                        if ($this->c['bb']->groupscache[$bdayuser['displaygroup']] &&
                            $this->bb->groupscache[$bdayuser['displaygroup']]['showinbirthdaylist'] != 1
                        ) {
                            continue;
                        }

                        $age = '';
                        $bday = explode('-', $bdayuser['birthday']);
                        if ($year > $bday['2'] && $bday['2'] != '') {
                            $age = ' (' . ($year - $bday['2']) . ')';
                        }

                        $bdayuser['username'] = $this->c['user']->format_name(
                            $bdayuser['username'],
                            $bdayuser['usergroup'],
                            $bdayuser['displaygroup']
                        );
                        $bdayuser['profilelink'] = $this->c['user']->build_profile_link(
                            $bdayuser['username'],
                            $bdayuser['uid']
                        );
                        $bdays .= $comma . $bdayuser['profilelink'] . $age;
                        ++$bdaycount;
                        $comma = $this->c['lang']->comma;
                    }
                }
            }

            if ($hiddencount > 0) {
                if ($bdaycount > 0) {
                    $bdays .= ' - ';
                }

                $bdays .= "{$hiddencount} {$this->c['lang']->birthdayhidden}";
            }
            $this->c['view']->offsetSet('bdays', $bdays);

            // If there are one or more birthdays, show them.
            if ($bdaycount > 0 || $hiddencount > 0) {
                $this->c['bb']->showBirthDays = true;
            }
        }

        // Build the forum statistics to show on the index page.
        if ($this->c['bb']->settings['showindexstats'] != 0) {
            // First, load the stats cache.
            $stats = $this->c['cache']->read('stats');

            // Check who's the newest member.
            if (!$stats['lastusername']) {
                $newestmember = $this->c['lang']->nobody;
                ;
            } else {
                $newestmember = $this->c['user']->build_profile_link($stats['lastusername'], $stats['lastuid']);
            }

            // Format the stats language.
            $this->c['lang']->stats_posts_threads = $this->c['lang']->sprintf(
                $this->c['lang']->stats_posts_threads,
                $this->c['parser']->formatNumber($stats['numposts']),
                $this->c['parser']->formatNumber($stats['numthreads'])
            );
            $this->c['lang']->stats_numusers = $this->c['lang']->sprintf(
                $this->c['lang']->stats_numusers,
                $this->c['parser']->formatNumber($stats['numusers'])
            );
            $this->c['lang']->stats_newestuser = $this->c['lang']->sprintf(
                $this->c['lang']->stats_newestuser,
                $newestmember
            );

            // Find out what the highest users online count is.
            $mostonline = $this->c['cache']->read('mostonline');
            if ($onlinecount > $mostonline['numusers']) {
                $time = TIME_NOW;
                $mostonline['numusers'] = $onlinecount;
                $mostonline['time'] = $time;
                $this->c['cache']->update('mostonline', $mostonline);
            }
            $recordcount = $mostonline['numusers'];
            $recorddate = $this->c['time']->formatDate($this->c['bb']->settings['dateformat'], $mostonline['time']);
            $recordtime = $this->c['time']->formatDate($this->c['bb']->settings['timeformat'], $mostonline['time']);

            // Then format that language string.
            $this->c['lang']->stats_mostonline = $this->c['lang']->sprintf(
                $this->c['lang']->stats_mostonline,
                $this->c['parser']->formatNumber($recordcount),
                $recorddate,
                $recordtime
            );
        }

        // Show the board statistics table only if one or more index statistics are enabled.
        $this->c['bb']->showBoardStats = false;
        if (($this->c['bb']->settings['showwol'] != 0 && $this->c['bb']->usergroup['canviewonline'] != 0) ||
            $this->c['bb']->settings['showindexstats'] != 0 ||
            ($this->c['bb']->settings['showbirthdays'] != 0 && $bdaycount > 0)
        ) {
            $this->c['bb']->showBoardStats = true;
            if (!isset($stats) || isset($stats) && !is_array($stats)) {
                // Load the stats cache.
                $stats = $this->c['cache']->read('stats');
            }

            $post_code_string = '';
            if ($this->c['user']->uid) {
                $post_code_string = 'my_post_key=' . $this->c['bb']->post_code;
            }
            $this->c['view']->offsetSet('post_code_string', $post_code_string);
        }

        if ($this->c['user']->uid == 0) {
            // Build a forum cache.
//            $query = $this->db->simple_select('forums', '*', 'active!=0', array('order_by' => 'pid, disporder'));
//            $f = \RunBB\Models\Forum::where('forums.active', '!=', '0')
//                ->orderBy('pid')
//                ->orderBy('disporder')
//                ->get()
//                ->toArray();
            $f = \RunBB\Models\Forum::getActiveForums();

            $forumsread = [];
            if (isset($this->c['bb']->cookies['mybb']['forumread'])) {
                $forumsread = my_unserialize($this->c['bb']->cookies['mybb']['forumread']);
            }
        } else {
            // Build a forum cache.
//            $query = $this->db->query("
//		SELECT f.*, fr.dateline AS lastread
//		FROM " . TABLE_PREFIX . "forums f
//		LEFT JOIN " . TABLE_PREFIX . "forumsread fr ON (fr.fid = f.fid AND fr.uid = '{$this->user->uid}')
//		WHERE f.active != 0
//		ORDER BY pid, disporder
//	");
//            $f = \RunBB\Models\Forum::where('forums.active', '!=', '0')
//                ->select('forums.*', 'forumsread.dateline AS lastread')
//                ->leftJoin('forumsread', function ($j) {
//                    $j->on(function ($q) {
//                        $q->on('forumsread.fid', '=', 'forums.fid');
//                        $q->on('forumsread.uid', '=', DB::raw($this->user->uid));
//                    });
//                })
//                ->orderBy('pid')
//                ->orderBy('disporder')
//                ->get()
//                ->toArray();
            $f = \RunBB\Models\Forum::getActiveForumsOnForumsread($this->c['user']->uid);
        }

//        while ($forum = $this->db->fetch_array($query)) {
        foreach ($f as $forum) {
            if ($this->c['user']->uid == 0) {
                if (!empty($forumsread[$forum['fid']])) {
                    $forum['lastread'] = $forumsread[$forum['fid']];
                }
            }
            $this->c['bb']->fcache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
        }
        $this->c['bb']->forumpermissions = $this->c['forum']->forum_permissions();
        // Get the forum moderators if the setting is enabled.
        $this->c['bb']->moderatorcache = [];
        if ($this->c['bb']->settings['modlist'] != 0 && $this->c['bb']->settings['modlist'] != 'off') {
            $this->c['bb']->moderatorcache = $this->c['cache']->read('moderators');
        }

        $this->c['bb']->permissioncache['-1'] = '1';

        // Decide if we're showing first-level subforums on the index page.
        $this->c['bb']->showdepth = 2;
        if ($this->c['bb']->settings['subforumsindex'] != 0) {
            $this->c['bb']->showdepth = 3;
        }

        $forum_list = $this->c['forum']->build_forumbits();

        $this->c['view']->offsetSet('forums', $forum_list['forum_cats']);

        $this->c['plugins']->runHooks('index_end');

        $this->c['bb']->output_page();
        $this->c['view']->render($response, '@forum/index.html.twig');
    }
}
