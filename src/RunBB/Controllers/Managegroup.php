<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Managegroup extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load language files
        $this->lang->load('managegroup');

        $gid = $this->bb->getInput('gid', 0);
        if (!isset($this->bb->groupscache[$gid])) {
            $this->bb->error($this->lang->invalid_group);
        }
        $this->view->offsetSet('gid', $gid);

        $usergroup = $this->bb->groupscache[$gid];
        $this->lang->nav_group_management = $this->lang->sprintf($this->lang->nav_group_management, htmlspecialchars_uni($usergroup['title']));
        $this->bb->add_breadcrumb($this->lang->nav_group_memberships, 'usercp/usergroups');
        $this->bb->add_breadcrumb($this->lang->nav_group_management, 'managegroup?gid=' . $gid);

        $this->bb->input['action'] = $this->bb->getInput('action', '');

        if ($this->bb->input['action'] == 'joinrequests') {
            $this->bb->add_breadcrumb($this->lang->nav_join_requests);
        }

        // Check that this user is actually a leader of this group
        $query = $this->db->simple_select('groupleaders', '*', "uid='{$this->user->uid}' AND gid='{$gid}'");
        $groupleader = $this->db->fetch_array($query);

        if (!$groupleader['uid'] && $this->bb->usergroup['cancp'] != 1) {
            $this->bb->error($this->lang->not_leader_of_this_group);
        }

        if ($this->bb->input['action'] == 'do_add' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            if ($groupleader['canmanagemembers'] == 0) {
                return $this->bb->error_no_permission();
            }

            $this->plugins->runHooks('managegroup_do_add_start');

            $options = [
                'fields' => ['additionalgroups', 'usergroup']
            ];

            $user = $this->user->get_user_by_username($this->bb->getInput('username', ''), $options);

            if ($user['uid']) {
                $additionalgroups = explode(',', $user['additionalgroups']);
                if ($user['usergroup'] != $gid && !in_array($gid, $additionalgroups)) {
                    $this->group->join_usergroup($user['uid'], $gid);
                    $this->db->delete_query('joinrequests', "uid='{$user['uid']}' AND gid='{$gid}'");
                    $this->plugins->runHooks('managegroup_do_add_end');
                    return $this->bb->redirect($this->bb->settings['bburl'] . '/managegroup?gid=' . $gid, $this->lang->user_added);
                } else {
                    $this->bb->error($this->lang->error_alreadyingroup);
                }
            } else {
                $this->bb->error($this->lang->error_invalidusername);
            }
        } elseif ($this->bb->input['action'] == 'do_invite' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            if ($groupleader['caninvitemembers'] == 0) {
                return $this->bb->error_no_permission();
            }

            $this->plugins->runHooks('managegroup_do_invite_start');

            $options = [
                'fields' => ['additionalgroups', 'usergroup', 'language']
            ];

            $user = $this->user->get_user_by_username($this->bb->getInput('inviteusername', ''), $options);

            if ($user['uid']) {
                $additionalgroups = explode(',', $user['additionalgroups']);
                if ($user['usergroup'] != $gid && !in_array($gid, $additionalgroups)) {
                    $query = $this->db->simple_select(
                        'joinrequests',
                        'rid',
                        "uid = '" . (int)$user['uid'] . "' AND gid = '" . (int)$gid . "'",
                        ['limit' => 1]
                    );
                    $pendinginvite = $this->db->fetch_array($query);
                    if ($pendinginvite['rid']) {
                        $this->bb->error($this->lang->error_alreadyinvited);
                    } else {
                        $usergroups_cache = $this->cache->read('usergroups');
                        $usergroup = $usergroups_cache[$gid];

                        $joinrequest = [
                            'uid' => $user['uid'],
                            'gid' => $usergroup['gid'],
                            'dateline' => TIME_NOW,
                            'invite' => 1
                        ];
                        $this->db->insert_query('joinrequests', $joinrequest);

                        $lang_var = 'invite_pm_message';
                        if ($this->bb->settings['deleteinvites'] != 0) {
                            $lang_var .= '_expires';
                        }

                        $pm = [
                            'subject' => ['invite_pm_subject', $usergroup['title']],
                            'message' => [$lang_var, $usergroup['title'], $this->bb->settings['bburl'], $this->bb->settings['deleteinvites']],
                            'touid' => $user['uid'],
                            'language' => $user['language'],
                            'language_file' => 'managegroup'
                        ];

                        $this->pm->send_pm($pm, $this->user->uid, true);

                        $this->plugins->runHooks('managegroup_do_invite_end');

                        return $this->bb->redirect($this->bb->settings['bburl'] . '/managegroup?gid=' . $gid, $this->lang->user_invited);
                    }
                } else {
                    $this->bb->error($this->lang->error_alreadyingroup);
                }
            } else {
                $this->bb->error($this->lang->error_invalidusername);
            }
        } elseif ($this->bb->input['action'] == 'do_joinrequests' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            if ($groupleader['canmanagerequests'] == 0) {
                return $this->bb->error_no_permission();
            }

            $this->plugins->runHooks('managegroup_do_joinrequests_start');

            $uidin = null;
            if (is_array($this->bb->getInput('request', []))) {
                $uidin = [];
                foreach ($this->bb->getInput('request', []) as $uid => $what) {
                    if ($what == 'accept') {
                        $this->group->join_usergroup($uid, $gid);
                        $uidin[] = (int)$uid;
                    } elseif ($what == 'decline') {
                        $uidin[] = (int)$uid;
                    }
                }
            }
            if (is_array($uidin) && !empty($uidin)) {
                $uids = implode(',', $uidin);
                $this->db->delete_query('joinrequests', "uid IN ({$uids}) AND gid='{$gid}'");
            }

            $this->plugins->runHooks('managegroup_do_joinrequests_end');

            return $this->bb->redirect($this->bb->settings['bburl'] . '/managegroup?gid=' . $gid, $this->lang->join_requests_moderated);
        } elseif ($this->bb->input['action'] == 'joinrequests') {
            $users = [];
            $this->plugins->runHooks('managegroup_joinrequests_start');

            $query = $this->db->query('
		SELECT j.*, u.uid, u.username, u.postnum, u.regdate
		FROM ' . TABLE_PREFIX . 'joinrequests j
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=j.uid)
		WHERE j.gid='{$gid}' AND j.uid != 0
		ORDER BY u.username ASC
	");
            while ($user = $this->db->fetch_array($query)) {
                //$regdate = $this->time->formatDate($this->bb->settings['dateformat'], $user['regdate']);
                $users[] = [
                    'altbg' => alt_trow(),
                    'profilelink' => $this->user->build_profile_link($user['username'], $user['uid']),
                    'reason' => htmlspecialchars_uni($user['reason']),
                    'uid' => $user['uid']
                ];
            }
            if (!$users) {
                $this->bb->error($this->lang->no_requests);
            }
            $this->view->offsetSet('users', $users);

            $this->lang->join_requests = $this->lang->sprintf($this->lang->join_requests_title, htmlspecialchars_uni($usergroup['title']));
            $this->plugins->runHooks('managegroup_joinrequests_end');

            $this->bb->output_page();
            return $this->view->render($response, '@forum/managegroup_joinreq.html.twig');
        } elseif ($this->bb->input['action'] == 'do_manageusers' && $this->bb->request_method == 'post') {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            if ($groupleader['canmanagemembers'] == 0) {
                return $this->bb->error_no_permission();
            }

            $this->plugins->runHooks('managegroup_do_manageusers_start');

            if (is_array($this->bb->getInput('removeuser', []))) {
                foreach ($this->bb->getInput('removeuser', []) as $uid) {
                    $this->group->leave_usergroup($uid, $gid);
                }
            } else {
                $this->bb->error($this->lang->no_users_selected);
            }

            $this->plugins->runHooks('managegroup_do_manageusers_end');

            return $this->bb->redirect($this->bb->settings['bburl'] . '/managegroup?gid=' . $gid, $this->lang->users_removed);
        } else {
            $this->plugins->runHooks('managegroup_start');

            $this->lang->members_of = $this->lang->sprintf($this->lang->members_of, htmlspecialchars_uni($usergroup['title']));
            $this->lang->add_member = $this->lang->sprintf($this->lang->add_member, htmlspecialchars_uni($usergroup['title']));
            $this->lang->invite_member = $this->lang->sprintf($this->lang->invite_member, htmlspecialchars_uni($usergroup['title']));
            $this->bb->showJoinRequests = false;
            if ($usergroup['type'] == 5) {
                $usergrouptype = $this->lang->group_public_invite;
            } elseif ($usergroup['type'] == 4) {
                $query = $this->db->simple_select('joinrequests', 'COUNT(*) AS req', "gid='{$gid}'");
                $numrequests = $this->db->fetch_array($query);
                if ($numrequests['req']) {
                    $this->bb->showJoinRequests = true;
                    $this->lang->num_requests_pending = $this->lang->sprintf($this->lang->num_requests_pending, $numrequests['req']);
                }
                $usergrouptype = $this->lang->group_public_moderated;
            } elseif ($usergroup['type'] == 3) {
                $usergrouptype = $this->lang->group_public_not_moderated;
            } elseif ($usergroup['type'] == 2) {
                $usergrouptype = $this->lang->group_private;
            } else {
                $usergrouptype = $this->lang->group_default;
            }
            $this->view->offsetSet('usergrouptype', $usergrouptype);

            $leaders = '';
            // Display group leaders (if there is any)
            $query = $this->db->query('
		SELECT g.*, u.username, u.usergroup, u.displaygroup
		FROM ' . TABLE_PREFIX . 'groupleaders g
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=g.uid)
		WHERE g.gid = '{$gid}'
	");

            $leaders_array = [];

            if ($this->db->num_rows($query)) {
                $loop = 1;
                $leader_count = $this->db->num_rows($query);
                while ($leader = $this->db->fetch_array($query)) {
                    $leader_name = $this->user->format_name(htmlspecialchars_uni($leader['username']), $leader['usergroup'], $leader['displaygroup']);
                    $leader_profile_link = $this->user->build_profile_link($leader_name, $leader['uid']);

                    $leaders_array[] = $leader['uid'];
                    // Get commas...
                    if ($loop != $leader_count) {
                        $comma = $this->lang->comma;
                    } else {
                        $comma = '';
                    }
                    ++$loop;
                    $leaders .= $leader_profile_link . $comma;
                }
            }
            $this->view->offsetSet('leaders', $leaders);

            switch ($this->db->type) {
                case 'pgsql':
                case 'sqlite':
                    $query = $this->db->simple_select('users', '*', "','||additionalgroups||',' LIKE '%,{$gid},%' OR usergroup='{$gid}'", ['order_by' => 'username']);
                    break;
                default:
                    $query = $this->db->simple_select('users', '*', "CONCAT(',',additionalgroups,',') LIKE '%,{$gid},%' OR usergroup='{$gid}'", ['order_by' => 'username']);
            }

            $numusers = $this->db->num_rows($query);

            $perpage = (int)$this->bb->settings['membersperpage'];
            if ($perpage < 1) {
                $perpage = 20;
            }

            $page = $this->bb->getInput('page', 0);
            if ($page && $page > 0) {
                $start = ($page - 1) * $perpage;
            } else {
                $start = 0;
                $page = 1;
            }
            $multipage = $this->pagination->multipage($numusers, $perpage, $page, 'managegroup?gid=' . $gid);
            $this->view->offsetSet('multipage', $multipage);

            switch ($this->db->type) {
                case 'pgsql':
                case 'sqlite':
                    $query = $this->db->simple_select('users', '*', "','||additionalgroups||',' LIKE '%,{$gid},%' OR usergroup='{$gid}'", ['order_by' => 'username', 'limit' => $perpage, 'limit_start' => $start]);
                    break;
                default:
                    $query = $this->db->simple_select('users', '*', "CONCAT(',',additionalgroups,',') LIKE '%,{$gid},%' OR usergroup='{$gid}'", ['order_by' => 'username', 'limit' => $perpage, 'limit_start' => $start]);
            }

            $users = [];
            while ($user = $this->db->fetch_array($query)) {
                $altbg = alt_trow();
                //$regdate = $this->time->formatDate('relative', $user['regdate']);
                $post = $user;
                $sendpm = $email = '';
                if ($this->bb->settings['enablepms'] == 1 && $post['receivepms'] != 0 && $this->bb->usergroup['cansendpms'] == 1 && my_strpos(',' . $post['ignorelist'] . ',', ',' . $this->user->uid . ',') === false) {
                    $sendpm = "<a href=\"{$this->bb->settings['bburl']}/private/send?uid={$post['uid']}\" title=\"{$this->lang->postbit_pm}\" class=\"postbit_pm\"><span>{$this->lang->postbit_button_pm}</span></a>";
                }

                $email = '';
                if ($user['hideemail'] != 1) {
                    $email = "<a href=\"{$this->bb->settings['bburl']}/emailuser?uid={$post['uid']}\" title=\"{$this->lang->postbit_email}\" class=\"postbit_email\"><span>{$this->lang->postbit_button_email}</span></a>";
                }

                $user['username'] = $this->user->format_name($user['username'], $user['usergroup'], $user['displaygroup']);
                //$user['profilelink'] = $this->user->build_profile_link($user['username'], $user['uid']);
                $leader = '';
                if (in_array($user['uid'], $leaders_array)) {
                    $leader = $this->lang->leader;
                }

                // Checkbox for user management - only if current user is allowed
                $checkbox = '';
                if ($groupleader['canmanagemembers'] == 1) {
                    $checkbox = "<input type=\"checkbox\" class=\"checkbox\" name=\"removeuser[{$user['uid']}]\" value=\"{$user['uid']}\" />";
                }
                $users[] = [
                    'altbg' => $altbg,
                    'profilelink' => $this->user->build_profile_link($user['username'], $user['uid']),
                    'leader' => $leader,
                    'email' => $email,
                    'sendpm' => $sendpm,
                    'regdate' => $this->time->formatDate('relative', $user['regdate']),
                    'postnum' => $user['postnum'],
                    'checkbox' => $checkbox
                ];
            }
            $this->view->offsetSet('users', $users);

            $this->bb->showMangeUsers = false;
            if ($groupleader['canmanagemembers'] == 1) {
                $this->bb->showMangeUsers = true;
            }

            $this->bb->showInvite = false;
            if ($usergroup['type'] == 5 && $groupleader['caninvitemembers'] == 1) {
                $this->bb->showInvite = true;
            }

            $this->plugins->runHooks('managegroup_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/managegroup.html.twig');
        }
    }
}
