<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Activate extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'activate');

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $this->lang->load('member');
        $this->bb->add_breadcrumb($this->lang->nav_activate);

        $this->plugins->runHooks('member_activate_start');

        if (isset($this->bb->input['username'])) {
            $this->bb->input['username'] = $this->bb->getInput('username', '', true);
            $options = [
                'username_method' => $this->bb->settings['username_method'],
                'fields' => '*',
            ];
            $user = $this->user->get_user_by_username($this->bb->input['username'], $options);
            if (!$user) {
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $this->bb->error($this->lang->error_invalidpworusername);
                        break;
                    case 1:
                        $this->bb->error($this->lang->error_invalidpworusername1);
                        break;
                    case 2:
                        $this->bb->error($this->lang->error_invalidpworusername2);
                        break;
                    default:
                        $this->bb->error($this->lang->error_invalidpworusername);
                        break;
                }
            }
            $uid = $user['uid'];
        } else {
            $user = $this->user->get_user($this->bb->getInput('uid', 0));
        }
        if (isset($this->bb->input['code']) && $user) {
            $query = $this->db->simple_select('awaitingactivation', '*', "uid='" . $user['uid'] . "' AND (type='r' OR type='e' OR type='b')");
            $activation = $this->db->fetch_array($query);
            if (!$activation['uid']) {
                $this->bb->error($this->lang->error_alreadyactivated);
            }
            if ($activation['code'] != $this->bb->getInput('code', '')) {
                $this->bb->error($this->lang->error_badactivationcode);
            }

            if ($activation['type'] == 'b' && $activation['validated'] == 1) {
                $this->bb->error($this->lang->error_alreadyvalidated);
            }

            $this->db->delete_query('awaitingactivation', "uid='" . $user['uid'] . "' AND (type='r' OR type='e')");

            if ($user['usergroup'] == 5 && $activation['type'] != 'e' && $activation['type'] != 'b') {
                $this->db->update_query('users', ['usergroup' => 2], "uid='" . $user['uid'] . "'");

                $this->cache->update_awaitingactivation();
            }
            if ($activation['type'] == 'e') {
                $newemail = [
                    'email' => $this->db->escape_string($activation['misc']),
                ];
                $this->db->update_query('users', $newemail, "uid='" . $user['uid'] . "'");
                $this->plugins->runHooks('member_activate_emailupdated');

                $this->bb->redirect($this->bb->settings['bburl'] . '/usercp', $this->lang->redirect_emailupdated);
            } elseif ($activation['type'] == 'b') {
                $update = [
                    'validated' => 1,
                ];
                $this->db->update_query('awaitingactivation', $update, "uid='" . $user['uid'] . "' AND type='b'");
                $this->plugins->runHooks('member_activate_emailactivated');

                $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_accountactivated_admin, '', true);
            } else {
                $this->plugins->runHooks('member_activate_accountactivated');

                $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_accountactivated);
            }
        } else {
            $this->plugins->runHooks('member_activate_form');

            $code = htmlspecialchars_uni($this->bb->getInput('code', ''));

            if (!isset($user['username'])) {
                $user['username'] = '';
            }
            $this->view->offsetSet('code', $code);
            $this->view->offsetSet('username', $user['username']);

            //ev al('\$activate = \''.$templates->get('member_activate').'\';');
            $this->bb->output_page();
            $this->view->render($response, '@forum/Member/activate.html.twig');
        }
    }
}
