<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Emailuser extends AbstractController
{

    private function init()
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'emailuser');
    }

    public function index(Request $request, Response $response)
    {
        $this->init();

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $do_captcha = $correct = false;
        $inline_errors = '';

        $this->lang->load('member');
        $this->bb->add_breadcrumb($this->lang->nav_emailuser);

        $this->plugins->runHooks('member_emailuser_start');

        // Guests or those without permission can't email other users
        if ($this->bb->usergroup['cansendemail'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Check group limits
        if ($this->bb->usergroup['maxemails'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $query = $this->db->simple_select(
                'maillogs',
                'COUNT(*) AS sent_count',
                "{$user_check} AND dateline >= '" . (TIME_NOW - (60 * 60 * 24)) . "'"
            );
            $sent_count = $this->db->fetch_field($query, 'sent_count');
            if ($sent_count >= $this->bb->usergroup['maxemails']) {
                $this->lang->error_max_emails_day = $this->lang->sprintf($this->lang->error_max_emails_day, $this->bb->usergroup['maxemails']);
                $this->bb->error($this->lang->error_max_emails_day);
            }
        }

        // Check email flood control
        if ($this->bb->usergroup['emailfloodtime'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $timecut = TIME_NOW - $this->bb->usergroup['emailfloodtime'] * 60;

            $query = $this->db->simple_select(
                'maillogs',
                'mid, dateline',
                "{$user_check} AND dateline > '{$timecut}'",
                ['order_by' => 'dateline', 'order_dir' => 'DESC']
            );
            $last_email = $this->db->fetch_array($query);

            // Users last email was within the flood time, show the error
            if ($last_email['mid']) {
                $remaining_time = ($this->bb->usergroup['emailfloodtime'] * 60) - (TIME_NOW - $last_email['dateline']);

                if ($remaining_time == 1) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_second,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } elseif ($remaining_time < 60) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_seconds,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time
                    );
                } elseif ($remaining_time > 60 && $remaining_time < 120) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_minute,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } else {
                    $remaining_time_minutes = ceil($remaining_time / 60);
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_minutes,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time_minutes
                    );
                }

                $this->bb->error($this->lang->error_emailflooding);
            }
        }

        $query = $this->db->simple_select(
            'users',
            'uid, username, email, hideemail, ignorelist',
            "uid='" . $this->bb->getInput('uid', 0) . "'"
        );
        $to_user = $this->db->fetch_array($query);

        $this->lang->email_user = $this->lang->sprintf($this->lang->email_user, $to_user['username']);

        if (!$to_user['uid']) {
            $this->bb->error($this->lang->error_invaliduser);
        }

        if ($to_user['hideemail'] != 0) {
            $this->bb->error($this->lang->error_hideemail);
        }

        if ($to_user['ignorelist'] &&
            (my_strpos(',' . $to_user['ignorelist'] . ',', ',' . $this->user->uid . ',') !== false &&
                $this->bb->usergroup['cansendemailoverride'] != 1)
        ) {
            return $this->bb->error_no_permission();
        }

        if (isset($errors) && count($errors) > 0) {
            $errors = $this->bb->inline_error($errors);
            $fromname = htmlspecialchars_uni($this->bb->getInput('fromname', '', true));
            $fromemail = htmlspecialchars_uni($this->bb->getInput('fromemail' . '', true));
            $subject = htmlspecialchars_uni($this->bb->getInput('subject', '', true));
            $message = htmlspecialchars_uni($this->bb->getInput('message', '', true));
        } else {
            $errors = '';
            $fromname = '';
            $fromemail = '';
            $subject = '';
            $message = '';
        }
        $this->view->offsetSet('errors', $errors);
        $this->view->offsetSet('fromname', $fromname);
        $this->view->offsetSet('fromemail', $fromemail);
        $this->view->offsetSet('subject', $subject);
        $this->view->offsetSet('message', $message);

        $this->view->offsetSet('uid', $to_user['uid']);

        // Generate CAPTCHA?
        $captcha = '';
        if ($this->bb->settings['captchaimage'] && $this->user->uid == 0) {
            $post_captcha = new \RunBB\Core\Captcha($this, true, 'post_captcha');

            if ($post_captcha->html) {
                $captcha = $post_captcha->html;
            }
        }
        $this->view->offsetSet('captcha', $captcha);

//    $from_email = '';
//    if($this->user->uid == 0)
//    {
//      ev al('\$from_email = \''.$templates->get('member_emailuser_guest').'\';');
//    }

        $this->plugins->runHooks('member_emailuser_end');

        //ev al('\$emailuser = \''.$templates->get('member_emailuser').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Member/emailuser.html.twig');
    }

    public function doEmailuser(Request $request, Response $response)
    {
        $this->init();

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('member_do_emailuser_start');

        // Guests or those without permission can't email other users
        if ($this->bb->usergroup['cansendemail'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Check group limits
        if ($this->bb->usergroup['maxemails'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $query = $this->db->simple_select('maillogs', 'COUNT(*) AS sent_count', "{$user_check} AND dateline >= '" . (TIME_NOW - (60 * 60 * 24)) . "'");
            $sent_count = $this->db->fetch_field($query, 'sent_count');
            if ($sent_count >= $this->bb->usergroup['maxemails']) {
                $this->lang->error_max_emails_day = $this->lang->sprintf($this->lang->error_max_emails_day, $this->bb->usergroup['maxemails']);
                $this->bb->error($this->lang->error_max_emails_day);
            }
        }

        // Check email flood control
        if ($this->bb->usergroup['emailfloodtime'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $timecut = TIME_NOW - $this->bb->usergroup['emailfloodtime'] * 60;

            $query = $this->db->simple_select(
                'maillogs',
                'mid, dateline',
                "{$user_check} AND dateline > '{$timecut}'",
                ['order_by' => 'dateline', 'order_dir' => 'DESC']
            );
            $last_email = $this->db->fetch_array($query);

            // Users last email was within the flood time, show the error
            if ($last_email['mid']) {
                $remaining_time = ($this->bb->usergroup['emailfloodtime'] * 60) - (TIME_NOW - $last_email['dateline']);

                if ($remaining_time == 1) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_second,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } elseif ($remaining_time < 60) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_seconds,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time
                    );
                } elseif ($remaining_time > 60 && $remaining_time < 120) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_minute,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } else {
                    $remaining_time_minutes = ceil($remaining_time / 60);
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_minutes,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time_minutes
                    );
                }

                $this->bb->error($this->lang->error_emailflooding);
            }
        }

        $query = $this->db->simple_select('users', 'uid, username, email, hideemail', "uid='" . $this->bb->getInput('uid', 0) . "'");
        $to_user = $this->db->fetch_array($query);

        if (!$to_user['username']) {
            $this->bb->error($this->lang->error_invalidusername);
        }

        if ($to_user['hideemail'] != 0) {
            $this->bb->error($this->lang->error_hideemail);
        }

        $errors = [];

        if ($this->user->uid) {
            $this->bb->input['fromemail'] = $this->user->email;
            $this->bb->input['fromname'] = $this->user->username;
        }

        if (!validate_email_format($this->bb->input['fromemail'])) {
            $errors[] = $this->lang->error_invalidfromemail;
        }

        if (empty($this->bb->input['fromname'])) {
            $errors[] = $this->lang->error_noname;
        }

        if (empty($this->bb->input['subject'])) {
            $errors[] = $this->lang->error_no_email_subject;
        }

        if (empty($this->bb->input['message'])) {
            $errors[] = $this->lang->error_no_email_message;
        }

        if ($this->bb->settings['captchaimage'] && $this->user->uid == 0) {
            $captcha = new \RunBB\Core\Captcha($this);

            if ($captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($captcha->get_errors() as $error) {
                    $errors[] = $error;
                }
            }
        }

        if (count($errors) == 0) {
            if ($this->bb->settings['mail_handler'] == 'smtp') {
                $from = $this->bb->input['fromemail'];
            } else {
                $from = "{$this->bb->input['fromname']} <{$this->bb->input['fromemail']}>";
            }

            $message = $this->lang->sprintf(
                $this->lang->email_emailuser,
                $to_user['username'],
                $this->bb->input['fromname'],
                $this->bb->settings['bbname'],
                $this->bb->settings['bburl'],
                $this->bb->getInput('message', '', true)
            );
            $this->mail->send(
                $to_user['email'],
                $this->bb->getInput('subject', '', true),
                $message,
                $from,
                '',
                '',
                false,
                'text',
                '',
                $this->bb->input['fromemail']
            );

            if ($this->bb->settings['mail_logging'] > 0) {
                // Log the message
                $log_entry = [
                    'subject' => $this->db->escape_string($this->bb->getInput('subject', '', true)),
                    'message' => $this->db->escape_string($this->bb->getInput('message', '', true)),
                    'dateline' => TIME_NOW,
                    'fromuid' => $this->user->uid,
                    'fromemail' => $this->db->escape_string($this->bb->input['fromemail']),
                    'touid' => $to_user['uid'],
                    'toemail' => $this->db->escape_string($to_user['email']),
                    'tid' => 0,
                    'ipaddress' => $this->session->ipaddress,
                    'type' => 1
                ];
                $this->db->insert_query('maillogs', $log_entry);
            }

            $this->plugins->runHooks('member_do_emailuser_end');

            $this->bb->redirect(get_profile_link($to_user['uid']), $this->lang->redirect_emailsent);
        } else {
            $this->index($request, $response);
        }
    }
}
