<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Login extends AbstractController
{
    private function init()
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'login');
    }

    public function index(Request $request, Response $response, $vars, $noinit = false)
    {
        if ($noinit === false) {
            $this->init();


            $this->bb->nosession['avatar'] = 1;// FIXME what???
            // Load global language phrases
            $this->lang->load('member');

//    $this->bb->input['action'] = $this->bb->getInput('action');

            $this->bb->add_breadcrumb($this->lang->nav_login);

            $this->do_captcha = $this->correct = false;
            $inline_errors = '';

            $this->plugins->runHooks('member_login');
        }
        //$member_loggedin_notice = '';
        if ($this->user->uid != 0) {
            $this->lang->already_logged_in = $this->lang->sprintf(
                $this->lang->already_logged_in,
                $this->user->build_profile_link($this->user->username, $this->user->uid)
            );
            //ev al("\$member_loggedin_notice = \"".$this->templates->get('member_loggedin_notice')."\";");
        }

        // Checks to make sure the user can login; they haven't had too many tries at logging in.
        // Is a fatal call if user has had too many tries
        $this->bb->login_attempt_check();

//    // Redirect to the page where the user came from, but not if that was the login page.
//    if(isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'action=login') === false)
//    {
//      $redirect_url = htmlentities($_SERVER['HTTP_REFERER']);
//    }
//    else
//    {
//      $redirect_url = '';
//    }
        $this->view->offsetSet('redirect_url', $this->bb->settings['bburl']);//$redirect_url);

        $captcha = '';
        // Show captcha image for guests if enabled and only if we have to do

        $this->bb->settings['captchaimage'] = 1;
        $this->do_captcha = true;

        if ($this->bb->settings['captchaimage'] && $this->do_captcha == true) {
            $login_captcha = new \RunBB\Core\Captcha($this->bb, false, 'post_captcha');

            if ($login_captcha->type == 1) {
                if (!$this->correct) {
                    $login_captcha->build_captcha();
                } else {
                    $captcha = $login_captcha->build_hidden_captcha();
                }
            } elseif ($login_captcha->type == 2 || $login_captcha->type == 4) {
                $login_captcha->build_recaptcha();
            }

            if ($login_captcha->html) {
                $captcha = $login_captcha->html;
            }
        }
        $this->view->offsetSet('captcha', $captcha);

        $username = $password = '';
        if (isset($this->bb->input['username']) && $this->bb->request_method === 'post') {
            $username = htmlspecialchars_uni($this->bb->getInput('username', '', true));
        }

        if (isset($this->bb->input['password']) && $this->bb->request_method === 'post') {
            $password = htmlspecialchars_uni($this->bb->getInput('password', '', true));
        }
        $this->view->offsetSet('username', $username);
        $this->view->offsetSet('password', $password);

        if (!empty($this->errors)) {
//      $this->bb->input['action'] = 'login';
//      $this->bb->request_method = 'get';

            $inline_errors = $this->bb->inline_error($this->errors);
        }
        $this->view->offsetSet('inline_errors', $inline_errors);

        switch ($this->bb->settings['username_method']) {
            case 1:
                $this->lang->username = $this->lang->username1;
                break;
            case 2:
                $this->lang->username = $this->lang->username2;
                break;
            default:
                break;
        }

        $this->plugins->runHooks('member_login_end');

        //ev al("\$login = \"".$this->templates->get('member_login')."\";");
        $this->bb->output_page();
        $this->view->render($response, '@forum/Member/login.html.twig');
    }

    public function doLogin(Request $request, Response $response)
    {
        $this->init();

        $this->do_captcha = $this->correct = false;

        $this->plugins->runHooks('member_do_login_start');

        // Is a fatal call if user has had too many tries
        $this->errors = [];
        $logins = $this->bb->login_attempt_check();

        $loginhandler = new \RunBB\Handlers\DataHandlers\LoginDataHandler($this->bb, 'get');

        if ($this->bb->getInput('quick_password', '', true) && $this->bb->getInput('quick_username', '', true)) {
            $this->bb->input['password'] = $this->bb->getInput('quick_password', '', true);
            $this->bb->input['username'] = $this->bb->getInput('quick_username', '', true);
            $this->bb->input['remember'] = $this->bb->getInput('quick_remember', '', true);
        }

        $user = [
            'username' => $this->bb->getInput('username', '', true),
            'password' => $this->bb->getInput('password', '', true),
            'remember' => $this->bb->getInput('remember', '', true),
            'imagestring' => $this->bb->getInput('imagestring', '', true)
        ];

        $options = [
            'fields' => 'loginattempts',
            'username_method' => (int)$this->bb->settings['username_method'],
        ];

        $user_loginattempts = $this->user->get_user_by_username($user['username'], $options);
        $user['loginattempts'] = (int)$user_loginattempts['loginattempts'];

        $loginhandler->set_data($user);
        $validated = $loginhandler->validate_login();

        if (!$validated) {
//      $this->bb->input['action'] = 'login';
//      $this->bb->request_method = 'get';

            $this->bb->my_setcookie('loginattempts', $logins + 1);
            $this->db->update_query(
                'users',
                ['loginattempts' => 'loginattempts+1'],
                "uid='" . (int)$loginhandler->login_data['uid'] . "'",
                1,
                true
            );

            $this->errors = $loginhandler->get_friendly_errors();

            $user['loginattempts'] = (int)$loginhandler->login_data['loginattempts'];

            // If we need a captcha set it here
            if ($this->bb->settings['failedcaptchalogincount'] > 0 &&
                ($user['loginattempts'] > $this->bb->settings['failedcaptchalogincount'] ||
                    (int)$this->bb->cookies['loginattempts'] > $this->bb->settings['failedcaptchalogincount'])
            ) {
                $this->do_captcha = true;
                $this->correct = $loginhandler->captcha_verified;
            }
            return $this->index($request, $response, '', true);
        } elseif ($validated && $loginhandler->captcha_verified == true) {
            // Successful login
            if ($loginhandler->login_data['coppauser']) {
                $this->bb->error($this->lang->error_awaitingcoppa);
            }

            $loginhandler->complete_login();

            $this->plugins->runHooks('member_do_login_end');

            $this->bb->input['url'] = $this->bb->getInput('url', '');

            if (!empty($this->bb->input['url']) &&
                my_strpos(basename($this->bb->input['url']), 'member') === false &&
                !preg_match('#^javascript:#i', $this->bb->input['url'])
            ) {
                if ((my_strpos(basename($this->bb->input['url']), 'newthread') !== false ||
                        my_strpos(basename($this->bb->input['url']), 'newreply') !== false) &&
                    my_strpos($this->bb->input['url'], '&processed=1') !== false
                ) {
                    $this->bb->input['url'] = str_replace('&processed=1', '', $this->bb->input['url']);
                }

                $this->bb->input['url'] = str_replace('&amp;', '&', $this->bb->input['url']);
                // Redirect to the URL if it is not member.php
                $this->bb->redirect(htmlentities($this->bb->input['url']), $this->lang->redirect_loggedin);
            } else {
                $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_loggedin);
            }
        }

        $this->plugins->runHooks('member_do_login_end');
    }
}
