<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Logout extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'logout');

        $this->plugins->runHooks('member_logout_start');

        if (!$this->user->uid) {
            return $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_alreadyloggedout);
        }

        // Check session ID if we have one
        if (isset($this->bb->input['sid']) && $this->bb->getInput('sid', '') !== $this->session->sid) {
            $this->bb->error($this->lang->error_notloggedout);
        } // Otherwise, check logoutkey
        elseif (!isset($this->bb->input['sid']) && $this->bb->getInput('logoutkey', '') !== $this->user->logoutkey) {
            $this->bb->error($this->lang->error_notloggedout);
        }

        $this->bb->my_unsetcookie('runbbuser');
        $this->bb->my_unsetcookie('sid');

        if ($this->user->uid > 0) {
            $time = TIME_NOW;
            // Run this after the shutdown query from session system
            $this->db->shutdown_query('UPDATE ' . TABLE_PREFIX . "users SET lastvisit='{$time}', lastactive='{$time}' WHERE uid='{$this->user->uid}'");
            $this->db->delete_query('sessions', "sid = '{$this->session->sid}'");
        }

        $this->plugins->runHooks('member_logout_end');

        $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_loggedout);
    }
}
