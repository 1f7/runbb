<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Lostpw extends AbstractController
{

    private function init()
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'lostpw');
    }

    public function index(Request $request, Response $response)
    {
        $this->init();

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $this->lang->load('member');
        $this->bb->add_breadcrumb($this->lang->nav_lostpw);

        $this->plugins->runHooks('member_lostpw');

        //ev al('\$lostpw = \''.$templates->get('member_lostpw').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Member/lostpw.html.twig');
    }

    public function doLostpw(Request $request, Response $response)
    {
        $this->init();

        $this->plugins->runHooks('member_do_lostpw_start');

        $email = $this->db->escape_string($this->bb->input['email']);
        $query = $this->db->simple_select('users', '*', "email='" . $this->db->escape_string($this->bb->getInput('email', '')) . "'");
        $numusers = $this->db->num_rows($query);
        if ($numusers < 1) {
            $this->bb->error($this->lang->error_invalidemail);
        } else {
            while ($user = $this->db->fetch_array($query)) {
                $this->db->delete_query('awaitingactivation', "uid='{$user['uid']}' AND type='p'");
                $user['activationcode'] = random_str();
                $now = TIME_NOW;
                $uid = $user['uid'];
                $awaitingarray = [
                    'uid' => $user['uid'],
                    'dateline' => TIME_NOW,
                    'code' => $user['activationcode'],
                    'type' => 'p'
                ];
                $this->db->insert_query('awaitingactivation', $awaitingarray);
                $username = $user['username'];
                $email = $user['email'];
                $activationcode = $user['activationcode'];
                $emailsubject = $this->lang->sprintf($this->lang->emailsubject_lostpw, $this->bb->settings['bbname']);
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_lostpw,
                            $username,
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $uid,
                            $activationcode
                        );
                        break;
                    case 1:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_lostpw1,
                            $username,
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $uid,
                            $activationcode
                        );
                        break;
                    case 2:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_lostpw2,
                            $username,
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $uid,
                            $activationcode
                        );
                        break;
                    default:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_lostpw,
                            $username,
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $uid,
                            $activationcode
                        );
                        break;
                }
                $this->mail->send($email, $emailsubject, $emailmessage);
            }
        }
        $this->plugins->runHooks('member_do_lostpw_end');

        $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_lostpwsent, '', true);
    }
}
