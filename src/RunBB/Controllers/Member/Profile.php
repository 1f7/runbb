<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Profile extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        $this->bb->nosession['avatar'] = 1;// FIXME what???
        // Load global language phrases
        $this->lang->load('member');

        $this->plugins->runHooks('member_profile_start');

        if ($this->bb->usergroup['canviewprofiles'] == 0) {
            return $this->bb->error_no_permission();
        }

        $uid = $this->bb->getInput('uid', 0);
        if ($uid > 0) {
            $memprofile = $this->user->get_user($uid);
        } //    elseif($this->user->uid)
//    {
//      $memprofile = $this->bb->user;
//    }
        else {
            $memprofile = false;
        }

        if (!$memprofile) {
            $this->bb->error($this->lang->error_nomember);
        }

        $uid = $memprofile->uid;

        $this->lang->profile = $this->lang->sprintf($this->lang->profile, $memprofile->username);

        // Get member's permissions
        $memperms = $this->user->user_permissions($memprofile->uid);

        $this->lang->nav_profile = $this->lang->sprintf($this->lang->nav_profile, $memprofile->username);
        $this->bb->add_breadcrumb($this->lang->nav_profile);

        $this->lang->users_forum_info = $this->lang->sprintf($this->lang->users_forum_info, $memprofile->username);
        $this->lang->users_contact_details = $this->lang->sprintf($this->lang->users_contact_details, $memprofile->username);

        if ($this->bb->settings['enablepms'] != 0 &&
            (($memprofile->receivepms != 0 &&
                    $memperms['canusepms'] != 0 &&
                    my_strpos(',' . $memprofile->ignorelist . ',', ',' . $this->user->uid . ',') === false) ||
                $this->bb->usergroup['canoverridepm'] == 1)
        ) {
            $this->lang->send_pm = $this->lang->sprintf($this->lang->send_pm, $memprofile->username);
        } else {
            $this->lang->send_pm = '';
        }
        $this->lang->away_note = $this->lang->sprintf($this->lang->away_note, $memprofile->username);
        $this->lang->users_additional_info = $this->lang->sprintf($this->lang->users_additional_info, $memprofile->username);
        $this->lang->users_signature = $this->lang->sprintf($this->lang->users_signature, $memprofile->username);
        $this->lang->send_user_email = $this->lang->sprintf($this->lang->send_user_email, $memprofile->username);

        //$useravatar = $this->user->format_avatar($memprofile->avatar, $memprofile->avatardimensions);
        $this->view->offsetSet(
            'useravatar',
            $this->user->format_avatar($memprofile->avatar, $memprofile->avatardimensions)
        );
        //ev al('\$avatar = \''.$this->templates->get('member_profile_avatar').'\';');

        $website = $sendemail = $sendpm = $contact_details = '';

        if ($memprofile->website && !$this->user->is_member($this->bb->settings['hidewebsite']) && $memperms['canchangewebsite'] == 1) {
            $memprofile->website = htmlspecialchars_uni($memprofile->website);
            //$bgcolor = alt_trow();
            //ev al('\$website = \''.$this->templates->get('member_profile_website').'\';');
            $website = true;
        }

        if ($memprofile->hideemail != 1 &&
            (my_strpos(',' . $memprofile->ignorelist . ',', ',' . $this->user->uid . ',') === false ||
                $this->bb->usergroup['cansendemailoverride'] != 0)
        ) {
            $sendemail = true;
            //$bgcolor = alt_trow();
            //ev al('\$sendemail = \''.$this->templates->get('member_profile_email').'\';');
        }

        if ($this->bb->settings['enablepms'] == 1 &&
            $memprofile->receivepms != 0 &&
            $this->bb->usergroup['cansendpms'] == 1 &&
            my_strpos(',' . $memprofile->ignorelist . ',', ',' . $this->user->uid . ',') === false
        ) {
            $sendpm = true;
            //$bgcolor = alt_trow();
            //ev al('$sendpm = ''.$this->templates->get('member_profile_pm').'';');
        }

//    $contact_fields = [];
//    $any_contact_field = false;
//    foreach(array('icq', 'aim', 'yahoo', 'skype', 'google') as $field)
//    {
//      $contact_fields[$field] = '';
//      $settingkey = 'allow'.$field.'field';
//
//      if(!empty($memprofile[$field]) &&
//        $this->user->is_member($this->bb->settings[$settingkey],
//          array('usergroup' => $memprofile->usergroup, 'additionalgroups' => $memprofile->additionalgroups)))
//      {
//        $any_contact_field = true;
//
//        if($field == 'icq')
//        {
//          $memprofile[$field] = (int)$memprofile[$field];
//        }
//        else
//        {
//          $memprofile[$field] = htmlspecialchars_uni($memprofile[$field]);
//        }
//        $tmpl = 'member_profile_contact_fields_'.$field;
//
//        $bgcolors[$field] = alt_trow();
//        ev al('$contact_fields[\''.$field.'\'] = ''.$this->templates->get($tmpl).'';');
//      }
//    }

        $this->bb->showContactDetails = false;
//    if($any_contact_field || $sendemail || $sendpm || $website)
        if ($sendemail || $sendpm || $website) {
            $this->bb->showContactDetails = true;
            //ev al('$contact_details = ''.$this->templates->get('member_profile_contact_details').'';');
        }

        $this->bb->showSignature = false;
        if ($memprofile->signature &&
            ($memprofile->suspendsignature == 0 ||
                $memprofile->suspendsigtime < TIME_NOW) &&
            !$this->user->is_member($this->bb->settings['hidesignatures']) &&
            $memperms['canusesig'] &&
            $memperms['canusesigxposts'] <= $memprofile->postnum
        ) {
            $this->bb->showSignature = true;
            $sig_parser = [
                'allow_html' => $this->bb->settings['sightml'],
                'allow_mycode' => $this->bb->settings['sigmycode'],
                'allow_smilies' => $this->bb->settings['sigsmilies'],
                'allow_imgcode' => $this->bb->settings['sigimgcode'],
                'me_username' => $memprofile->username,
                'filter_badwords' => 1
            ];

            if ($memperms['signofollow']) {
                $sig_parser['nofollow_on'] = 1;
            }

            if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
            ) {
                $sig_parser['allow_imgcode'] = 0;
            }
            $memprofile->signature = $this->parser->parse_message($memprofile->signature, $sig_parser);
            //ev al('\$signature = \''.$this->templates->get('member_profile_signature').'\';');
        }

        $daysreg = (TIME_NOW - $memprofile->regdate) / (24 * 3600);

        if ($daysreg < 1) {
            $daysreg = 1;
        }

        $stats = $this->cache->read('stats');

        // Format post count, per day count and percent of total
        $ppd = $memprofile->postnum / $daysreg;
        $ppd = round($ppd, 2);
        if ($ppd > $memprofile->postnum) {
            $ppd = $memprofile->postnum;
        }

        $numposts = $stats['numposts'];
        if ($numposts == 0) {
            $post_percent = '0';
        } else {
            $post_percent = $memprofile->postnum * 100 / $numposts;
            $post_percent = round($post_percent, 2);
        }

        if ($post_percent > 100) {
            $post_percent = 100;
        }

        // Format thread count, per day count and percent of total
        $tpd = $memprofile->threadnum / $daysreg;
        $tpd = round($tpd, 2);
        if ($tpd > $memprofile->threadnum) {
            $tpd = $memprofile->threadnum;
        }

        $numthreads = $stats['numthreads'];
        if ($numthreads == 0) {
            $thread_percent = '0';
        } else {
            $thread_percent = $memprofile->threadnum * 100 / $numthreads;
            $thread_percent = round($thread_percent, 2);
        }

        if ($thread_percent > 100) {
            $thread_percent = 100;
        }

//    $findposts = $findthreads = '';
//    if($this->bb->usergroup['cansearch'] == 1)
//    {
//      //ev al('\$findposts = \''.$this->templates->get('member_profile_findposts').'\';');
//      //ev al('\$findthreads = \''.$this->templates->get('member_profile_findthreads').'\';');
//    }
        $this->view->offsetSet('uid', $uid);

        $awaybit = '';
        if ($memprofile->away == 1 && $this->bb->settings['allowaway'] != 0) {
            $this->lang->away_note = $this->lang->sprintf($this->lang->away_note, $memprofile->username);
            $awaydate = $this->time->formatDate($this->bb->settings['dateformat'], $memprofile->awaydate);
            if (!empty($memprofile->awayreason)) {
                $reason = $this->parser->parse_badwords($memprofile->awayreason);
                $awayreason = htmlspecialchars_uni($reason);
            } else {
                $awayreason = $this->lang->away_no_reason;
            }
            if ($memprofile->returndate == '') {
                $returndate = $this->lang->unknown;
            } else {
                $returnhome = explode('-', $memprofile->returndate);

                $returnmkdate = mktime(0, 0, 0, $returnhome[1], $returnhome[0], $returnhome[2]);
                $returndate = $this->time->formatDate($this->bb->settings['dateformat'], $returnmkdate);

                // If our away time has expired already, we should be back, right?
                if ($returnmkdate < TIME_NOW) {
                    $this->db->update_query(
                        'users',
                        ['away' => '0', 'awaydate' => '0',
                        'returndate' => '', 'awayreason' => ''],
                        'uid=\'' . (int)$memprofile->uid . '\''
                    );

                    // Update our status to 'not away'
                    $memprofile->away = 0;
                }
            }

            // Check if our away status is set to 1, it may have been updated already (see a few lines above)
            if ($memprofile->away == 1) {
                $this->view->offsetSet(
                    'awaybit',
                    [
                        'awayreason' => $awayreason,
                        'awaydate' => $awaydate,
                        'returndate' => $returndate
                    ]
                );
                //ev al('\$awaybit = \''.$this->templates->get('member_profile_away').'\';');
            }
        }

        if ($memprofile->dst == 1) {
            $memprofile->timezone++;
            if (my_substr($memprofile->timezone, 0, 1) != '-') {
                $memprofile->timezone = "+{$memprofile->timezone}";
            }
        }

        //$memregdate = $this->time->formatDate($this->bb->settings['dateformat'], $memprofile->regdate);
        $this->view->offsetSet('memregdate', $this->time->formatDate($this->bb->settings['dateformat'], $memprofile->regdate));
        $memlocaldate = gmdate($this->bb->settings['dateformat'], TIME_NOW + ($memprofile->timezone * 3600));
        $memlocaltime = gmdate($this->bb->settings['timeformat'], TIME_NOW + ($memprofile->timezone * 3600));

        //$localtime = $this->lang->sprintf($this->lang->local_time_format, $memlocaldate, $memlocaltime);
        $this->view->offsetSet('localtime', $this->lang->sprintf($this->lang->local_time_format, $memlocaldate, $memlocaltime));

        if ($memprofile->lastactive) {
            $memlastvisitdate = $this->time->formatDate($this->bb->settings['dateformat'], $memprofile->lastactive);
            $memlastvisitsep = $this->lang->comma;
            $memlastvisittime = $this->time->formatDate($this->bb->settings['timeformat'], $memprofile->lastactive);
        } else {
            $memlastvisitdate = $this->lang->lastvisit_never;
            $memlastvisitsep = '';
            $memlastvisittime = '';
        }
        $this->view->offsetSet('memlastvisitdate', $memlastvisitdate);


        if ($memprofile->birthday) {
            $membday = explode('-', $memprofile->birthday);

            if ($memprofile->birthdayprivacy != 'none') {
                if ($membday[0] && $membday[1] && $membday[2]) {
                    $this->lang->membdayage = $this->lang->sprintf($this->lang->membdayage, $this->user->get_age($memprofile->birthday));

                    $bdayformat = $this->time->fixMktime($this->bb->settings['dateformat'], $membday[2]);
                    $membday = mktime(0, 0, 0, $membday[1], $membday[0], $membday[2]);
                    $membday = date($bdayformat, $membday);

                    $membdayage = $this->lang->membdayage;
                } elseif ($membday[2]) {
                    $membday = mktime(0, 0, 0, 1, 1, $membday[2]);
                    $membday = date('Y', $membday);
                    $membdayage = '';
                } else {
                    $membday = mktime(0, 0, 0, $membday[1], $membday[0], 0);
                    $membday = date('F j', $membday);
                    $membdayage = '';
                }
            }

            if ($memprofile->birthdayprivacy == 'age') {
                $membday = $this->lang->birthdayhidden;
            } elseif ($memprofile->birthdayprivacy == 'none') {
                $membday = $this->lang->birthdayhidden;
                $membdayage = '';
            }
        } else {
            $membday = $this->lang->not_specified;
            $membdayage = '';
        }
        $this->view->offsetSet('membday', $membday);
        $this->view->offsetSet('membdayage', $membdayage);

        if (!$memprofile->displaygroup) {
            $memprofile->displaygroup = $memprofile->usergroup;
        }

        // Grab the following fields from the user's displaygroup
        $this->bb->displaygroupfields = [
            'title',
            'usertitle',
            'stars',
            'starimage',
            'image',
            'usereputationsystem'
        ];
        $displaygroup = $this->group->usergroup_displaygroup($memprofile->displaygroup);

        // Get the user title for this user
        unset($usertitle);
        unset($stars);
        $starimage = '';
        if (trim($memprofile->usertitle) != '') {
            // User has custom user title
            $usertitle = $memprofile->usertitle;
        } elseif (trim($displaygroup['usertitle']) != '') {
            // User has group title
            $usertitle = $displaygroup['usertitle'];
        } else {
            // No usergroup title so get a default one
            $usertitles = $this->cache->read('usertitles');

            if (is_array($usertitles)) {
                foreach ($usertitles as $title) {
                    if ($memprofile->postnum >= $title['posts']) {
                        $usertitle = $title['title'];
                        $stars = $title['stars'];
                        $starimage = $title['starimage'];

                        break;
                    }
                }
            }
        }

        $this->view->offsetSet('usertitle', htmlspecialchars_uni($usertitle));

        if ($displaygroup['stars'] || $displaygroup['usertitle']) {
            // Set the number of stars if display group has constant number of stars
            $stars = $displaygroup['stars'];
        } elseif (empty($stars)) {
            if (empty($usertitles)) {
                $usertitles = $this->cache->read('usertitles');
            }

            // This is for cases where the user has a title, but the group has no defined number
            // of stars (use number of stars as per default usergroups)
            if (is_array($usertitles)) {
                foreach ($usertitles as $title) {
                    if ($memprofile->postnum >= $title['posts']) {
                        $stars = $title['stars'];
                        $starimage = $title['starimage'];
                        break;
                    }
                }
            }
        }

        $groupimage = '';
        if (!empty($displaygroup['image'])) {
            if (!empty($this->user->language)) {
                $language = $this->user->language;
            } else {
                $language = $this->bb->settings['bblanguage'];
            }
            $displaygroup['image'] = str_replace('{lang}', $language, $displaygroup['image']);
            $displaygroup['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $displaygroup['image']);

            $groupimage = $displaygroup['image'];
            //ev al('\$groupimage = \''.$this->templates->get('member_profile_groupimage').'\';');
        }
        $this->view->offsetSet('groupimage', $groupimage);

        if (empty($starimage)) {
            $starimage = $displaygroup['starimage'];
        }

        if (!empty($starimage)) {
            // Only display stars if we have an image to use...
            $starimage = $this->bb->asset_url . '/' . str_replace('{theme}', $this->bb->theme['imgdir'], $starimage);
            $this->view->offsetSet('userstars', $stars);
            $this->view->offsetSet('starimage', $starimage);
//      for($i = 0; $i < $stars; ++$i)
//      {
//        ev al('\$userstars .= \''.$this->templates->get('member_profile_userstar', 1, 0).'\';');
//      }
        }
        // User is currently online and this user has permissions to view the user on the WOL
        $timesearch = TIME_NOW - $this->bb->settings['wolcutoffmins'] * 60;
//        $query = $this->db->simple_select('sessions', 'location,nopermission', "uid='$uid' AND time>'{$timesearch}'",
//            array('order_by' => 'time', 'order_dir' => 'DESC', 'limit' => 1));
//        $session = $this->db->fetch_array($query);
        $s = \RunBB\Models\Session::where([
            ['uid', '=', $uid], ['time', '>', $timesearch]
        ])
            ->take(1)
            ->orderBy('time', 'desc')
            ->get(['location', 'nopermission'])
            ->toArray();
        $session = isset($s[0]) ? $s[0] : [];//FIXME

        $online_status = '';
        if ($memprofile->invisible != 1 ||
            $this->bb->usergroup['canviewwolinvis'] == 1 ||
            $memprofile->uid == $this->user->uid
        ) {
            // Lastvisit
            if ($memprofile->lastactive) {
                $memlastvisitsep = $this->lang->comma;
                $memlastvisitdate = $this->time->formatDate('relative', $memprofile->lastactive);
            }

            // Time Online
            $timeonline = $this->lang->none_registered;
            if ($memprofile->timeonline > 0) {
                $timeonline = $this->time->niceTime($memprofile->timeonline);
            }
            $this->view->offsetSet('timeonline', $timeonline);

            $this->onliner = new \RunBB\Helpers\OnlineHelper($this->bb);

            // Online?
            if (!empty($session)) {
                // Fetch their current location
                $this->lang->load('online');
                $activity = $this->onliner->fetchWolActivity($session['location'], $session['nopermission']);
                $this->view->offsetSet('online_status', [
                    'location' => $this->onliner->buildFriendlyWolLocation($activity),
                    'location_time' => $this->time->formatDate($this->bb->settings['timeformat'], $memprofile->lastactive)
                ]);
            }
        }

        if ($memprofile->invisible == 1 &&
            $this->bb->usergroup['canviewwolinvis'] != 1 &&
            $memprofile->uid != $this->user->uid
        ) {
            $memlastvisitsep = '';
            $memlastvisittime = '';
            $memlastvisitdate = $this->lang->lastvisit_never;

            if ($memprofile->lastactive) {
                // We have had at least some active time, hide it instead
                $memlastvisitdate = $this->lang->lastvisit_hidden;
            }

            $timeonline = $this->lang->timeonline_hidden;
        }

        // Reset the background colours to keep it inline
        $alttrow = 'trow1';

//    // Build Referral
//    $referrals = '';
//    if($this->bb->settings['usereferrals'] == 1)
//    {
//      //$bg_color = alt_trow();
//      //ev al('\$referrals = \''.$this->templates->get('member_profile_referrals').'\';');
//    }
        // Fetch the reputation for this user
        $this->bb->showReputation = false;
        if ($memperms['usereputationsystem'] == 1 &&
            (isset($displaygroup['usereputationsystem']) && $displaygroup['usereputationsystem'] == 1) &&
            $this->bb->settings['enablereputation'] == 1
        ) {
            $this->bb->showReputation = true;
//      $bg_color = alt_trow();
            //$reputation = get_reputation($memprofile->reputation);
            $memprofile->reputation = $this->user->get_reputation($memprofile->reputation);

            // If this user has permission to give reputations show the vote link
            $this->bb->showVoteLink = false;
            if ($this->bb->usergroup['cangivereputations'] == 1 &&
                $memprofile->uid != $this->user->uid &&
                ($this->bb->settings['posrep'] || $this->bb->settings['neurep'] || $this->bb->settings['negrep'])
            ) {
                $this->bb->showVoteLink = true;
                //ev al('\$vote_link = \''.$this->templates->get('member_profile_reputation_vote').'\';');
            }
            //ev al('\$reputation = \''.$this->templates->get('member_profile_reputation').'\';');
        }

        $this->bb->showWarningLevel = false;
        if ($this->bb->settings['enablewarningsystem'] != 0 &&
            $memperms['canreceivewarnings'] != 0 &&
            ($this->bb->usergroup['canwarnusers'] != 0 ||
                ($this->user->uid == $memprofile->uid &&
                    $this->bb->settings['canviewownwarning'] != 0))
        ) {
            $this->bb->showWarningLevel = true;
            //$bg_color = alt_trow();
            if ($this->bb->settings['maxwarningpoints'] < 1) {
                $this->bb->settings['maxwarningpoints'] = 10;
            }

            $warning_level = round($memprofile->warningpoints / $this->bb->settings['maxwarningpoints'] * 100);
            if ($warning_level > 100) {
                $warning_level = 100;
            }

            $this->bb->showWarnUser = false;
            $warning_link = 'usercp';
            $warning_level = get_colored_warning_level($warning_level);
            if ($this->bb->usergroup['canwarnusers'] != 0 && $memprofile->uid != $this->user->uid) {
                $this->bb->showWarnUser = true;
                //ev al('\$warn_user = \''.$this->templates->get('member_profile_warn').'\';');
                $warning_link = "warnings?uid={$memprofile->uid}";
            }
            $this->view->offsetSet('warning_link', $warning_link);
            $this->view->offsetSet('warning_level', $warning_level);
            //ev al('\$warning_level = \''.$this->templates->get('member_profile_warninglevel').'\';');
        }

        $bgcolor = $alttrow = 'trow1';
        //$customfields = $profilefields = '';
        $customfields = [];

//        $query = $this->db->simple_select('userfields', '*', "ufid = '{$uid}'");
//        $userfields = $this->db->fetch_array($query);
        $uf = \RunBB\Models\Userfield::where('ufid', '=', $uid)
            ->get()
            ->toArray();
        $userfields = isset($uf[0]) ? $uf[0] : [];

        // If this user is an Administrator or a Moderator then we wish to show all profile fields
        $pfcache = $this->cache->read('profilefields');

        if (is_array($pfcache)) {
            foreach ($pfcache as $customfield) {
                if ($this->bb->usergroup['cancp'] != 1 &&
                    $this->bb->usergroup['issupermod'] != 1 &&
                    $this->bb->usergroup['canmodcp'] != 1 &&
                    !$this->user->is_member($customfield['viewableby'])
                ) {
                    continue;
                }

                $thing = explode("\n", $customfield['type'], '2');
                $type = trim($thing[0]);

                $customfieldval = $customfield_val = '';
                $field = "fid{$customfield['fid']}";

                if (isset($userfields[$field])) {
                    $useropts = explode("\n", $userfields[$field]);
                    $customfieldval = $comma = '';
                    if (is_array($useropts) && ($type == 'multiselect' || $type == 'checkbox')) {
                        foreach ($useropts as $val) {
                            if ($val != '') {
                                $customfield_val .= "<li style=\"margin-left: 0;\">{$val}</li>";
                                //ev al('\$customfield_val .= \''.$this->templates->get('member_profile_customfields_field_multi_item').'\';');
                            }
                        }
                        if ($customfield_val != '') {
                            $customfieldval = "
              <ul style=\"margin: 0; padding-left: 15px;\">
                {$customfield_val}
              </ul>";
                            //ev al('\$customfieldval = \''.$this->templates->get('member_profile_customfields_field_multi').'\';');
                        }
                    } else {
                        $parser_options = [
                            'allow_html' => $customfield['allowhtml'],
                            'allow_mycode' => $customfield['allowmycode'],
                            'allow_smilies' => $customfield['allowsmilies'],
                            'allow_imgcode' => $customfield['allowimgcode'],
                            'allow_videocode' => $customfield['allowvideocode'],
                            'nofollow_on' => 1,
                            'filter_badwords' => 1
                        ];

                        if ($customfield['type'] == 'textarea') {
                            $parser_options['me_username'] = $memprofile->username;
                        } else {
                            $parser_options['nl2br'] = 0;
                        }

                        if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                            ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
                        ) {
                            $parser_options['allow_imgcode'] = 0;
                        }

                        $customfieldval = $this->parser->parse_message($userfields[$field], $parser_options);
                    }
                }

                if ($customfieldval) {
                    $customfield['name'] = htmlspecialchars_uni($customfield['name']);
                    //ev al('\$customfields .= \''.$this->templates->get('member_profile_customfields_field').'\';');
                    //$bgcolor = alt_trow();
                    $customfields[] = [
                        'bgcolor' => alt_trow(),
                        'fieldname' => htmlspecialchars_uni($customfield['name']),
                        'fieldval' => $customfieldval
                    ];
                }
            }
        }
        $this->view->offsetSet('customfields', $customfields);

//    if($customfields)
//    {
//      //ev al('\$profilefields = \''.$this->templates->get('member_profile_customfields').'\';');
//    }

        $memprofile->postnum = $this->parser->formatNumber($memprofile->postnum);
        //$this->view->offsetSet('postnum', $memprofile->postnum);
        $this->lang->ppd_percent_total = $this->lang->sprintf($this->lang->ppd_percent_total, $this->parser->formatNumber($ppd), $post_percent);

        $memprofile->threadnum = $this->parser->formatNumber($memprofile->threadnum);
        //$this->view->offsetSet('threadnum', $this->parser->formatNumber($memprofile->threadnum));
        $this->lang->tpd_percent_total = $this->lang->sprintf($this->lang->tpd_percent_total, $this->parser->formatNumber($tpd), $thread_percent);

        //$formattedname = $this->user->format_name($memprofile->username, $memprofile->usergroup, $memprofile->displaygroup);
        $this->view->offsetSet(
            'formattedname',
            $this->user->format_name($memprofile->username, $memprofile->usergroup, $memprofile->displaygroup)
        );

        $bannedbit = '';
        if ($memperms['isbannedgroup'] == 1 && $this->bb->usergroup['canbanusers'] == 1) {
            // Fetch details on their ban
            $query = $this->db->simple_select(
                'banned b LEFT JOIN ' . TABLE_PREFIX . 'users a ON (b.admin=a.uid)',
                'b.*, a.username AS adminuser',
                "b.uid='{$uid}'",
                ['limit' => 1]
            );
            $memban = $this->db->fetch_array($query);

            if ($memban['reason']) {
                $memban['reason'] = htmlspecialchars_uni($this->parser->parse_badwords($memban['reason']));
            } else {
                $memban['reason'] = $this->lang->na;
            }

            if ($memban['lifted'] == 'perm' ||
                $memban['lifted'] == '' ||
                $memban['bantime'] == 'perm' ||
                $memban['bantime'] == '---'
            ) {
                $banlength = $this->lang->permanent;
                $timeremaining = $this->lang->na;
            } else {
                // Set up the array of ban times.
                $bantimes = $this->ban->fetch_ban_times();

                $banlength = $bantimes[$memban['bantime']];
                $remaining = $memban['lifted'] - TIME_NOW;

                $timeremaining = $this->time->niceTime($remaining, ['short' => 1, 'seconds' => false]) . '';

                if ($remaining < 3600) {
                    $timeremaining = "<span style=\"color: red;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } elseif ($remaining < 86400) {
                    $timeremaining = "<span style=\"color: maroon;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } elseif ($remaining < 604800) {
                    $timeremaining = "<span style=\"color: green;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } else {
                    $timeremaining = "({$timeremaining} {$this->lang->ban_remaining})";
                }
            }

            //$memban['adminuser'] = $this->user->build_profile_link($memban['adminuser'], $memban['admin']);

            $this->view->offsetSet(
                'bannedbit',
                [
                    'reason' => $memban['reason'],
                    'adminuser' => $this->user->build_profile_link($memban['adminuser'], $memban['admin']),
                    'banlength' => $banlength,
                    'timeremaining' => $timeremaining
                ]
            );
            // Display a nice warning to the user
            //ev al('$bannedbit = ''.$this->templates->get('member_profile_banned').'';');
        }

//    $adminoptions = '';
//    if($this->bb->usergroup['cancp'] == 1 && $this->config['hide_admin_links'] != 1)
//    {
//      ev al('\$adminoptions = \''.$this->templates->get('member_profile_adminoptions').'\';');
//    }

        $modoptions = $viewnotes = $editnotes = $editprofile = $banuser = $manageuser = '';
        $this->bb->can_purge_spammer = $this->user->purgespammer_show(
            $memprofile->postnum,
            $memprofile->usergroup,
            $memprofile->uid
        );
        $this->bb->showModOptions = false;
        if ($this->bb->usergroup['canmodcp'] == 1 || $this->bb->can_purge_spammer) {
            $this->bb->showModOptions = true;
            $memprofile->usernotes = nl2br(htmlspecialchars_uni($memprofile->usernotes));

            if (!empty($memprofile->usernotes)) {
                if (strlen($memprofile->usernotes) > 100) {
                    $viewnotes = "[<a href=\"javascript:MyBB.viewNotes({$memprofile->uid});\">{$this->lang->view_all_notes}</a>]";
                    //ev al('\$viewnotes = \''.$this->templates->get('member_profile_modoptions_viewnotes').'\';');
                    $memprofile->usernotes = my_substr($memprofile->usernotes, 0, 100) . "... {$viewnotes}";
                }
            } else {
                $memprofile->usernotes = $this->lang->no_usernotes;
            }

//      if($this->bb->usergroup['caneditprofiles'] == 1)
//      {
//        //ev al('\$editprofile = \''.$this->templates->get('member_profile_modoptions_editprofile').'\';');
//        //ev al('\$editnotes = \''.$this->templates->get('member_profile_modoptions_editnotes').'\';');
//      }

//        (!$memban['uid'] || isset($memban['uid']) &&
            $this->bb->showBanUser = false;
            if ($this->bb->usergroup['canbanusers'] == 1 &&
                (isset($memban['uid']) &&
                    ($this->user->uid == $memban['admin']) ||
                    $this->bb->usergroup['issupermod'] == 1 ||
                    $this->bb->usergroup['cancp'] == 1)
            ) {
                $this->bb->showBanUser = true;
                //ev al('\$banuser = \''.$this->templates->get('member_profile_modoptions_banuser').'\';');
            }

//      if($this->can_purge_spammer)
//      {
//        ev al('\$purgespammer = \''.$this->templates->get('member_profile_modoptions_purgespammer').'\';');
//      }

//      if(!empty($editprofile) || !empty($banuser) || !empty($purgespammer))
//      {
//        ev al('\$manageuser = \''.$this->templates->get('member_profile_modoptions_manageuser').'\';');
//      }

            //ev al('\$modoptions = \''.$this->templates->get('member_profile_modoptions').'\';');
        }

        $add_remove_options = [];
        $buddy_options = $ignore_options = $report_options = '';
        if ($this->user->uid != $memprofile->uid && $this->user->uid != 0) {
            $buddy_list = explode(',', $this->user->buddylist);
            $ignore_list = explode(',', $this->user->ignorelist);

            if (in_array($uid, $buddy_list)) {
                $add_remove_options = ['url' => "{$this->bb->settings['bburl']}/usercp/do_editlists?delete={$uid}&my_post_key={$this->bb->post_code}",
                    'class' => 'remove_buddy_button', 'lang' => $this->lang->remove_from_buddy_list];
            } else {
                $add_remove_options = ['url' => "{$this->bb->settings['bburl']}/usercp/do_editlists?add_username=" . urlencode($memprofile->username) . "&my_post_key={$this->bb->post_code}",
                    'class' => 'add_buddy_button', 'lang' => $this->lang->add_to_buddy_list];
            }

            if (!in_array($uid, $ignore_list)) {
                $this->view->offsetSet('buddy_options', $add_remove_options);
                //ev al('\$buddy_options = \''.$this->templates->get('member_profile_addremove').'\';'); // Add/Remove Buddy
            }

            if (in_array($uid, $ignore_list)) {
                $add_remove_options = ['url' => "{$this->bb->settings['bburl']}/usercp/do_editlists?manage=ignored&delete={$uid}&my_post_key={$this->bb->post_code}",
                    'class' => 'remove_ignore_button', 'lang' => $this->lang->remove_from_ignore_list];
            } else {
                $add_remove_options = ['url' => "{$this->bb->settings['bburl']}/usercp/do_editlists?manage=ignored&add_username=" . urlencode($memprofile->username) . "&my_post_key={$this->bb->post_code}",
                    'class' => 'add_ignore_button', 'lang' => $this->lang->add_to_ignore_list];
            }

            if (!in_array($uid, $buddy_list)) {
                $this->view->offsetSet('ignore_options', $add_remove_options);
                //ev al('\$ignore_options = \''.$this->templates->get('member_profile_addremove').'\';'); // Add/Remove Ignore
            }

            if (isset($memperms['canbereported']) && $memperms['canbereported'] == 1) {
                $add_remove_options = ['url' => "javascript:Report.reportUser({$memprofile->uid});",
                    'class' => 'report_user_button', 'lang' => $this->lang->report_user];
                $this->view->offsetSet('report_options', $add_remove_options);
                //ev al('\$report_options = \''.$this->templates->get('member_profile_addremove').'\';'); // Report User
            }
        }

        $this->view->offsetSet('memprofile', $memprofile);

        $this->plugins->runHooks('member_profile_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Member/profile.html.twig');
    }
}
