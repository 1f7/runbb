<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Register extends AbstractController
{

    private function common(& $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'register');

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $this->regerrors = '';

        $this->lang->load('member');
        $this->bb->add_breadcrumb($this->lang->nav_register);

        if ($this->bb->usergroup['cancp'] != 1) {
            if ($this->bb->settings['disableregs'] == 1) {
                $this->return = true;
                $this->bb->error($this->lang->registrations_disabled);
            }
            if ($this->user->uid != 0) {
                $this->return = true;
                $this->bb->error($this->lang->error_alreadyregistered);
            }
            if ($this->bb->settings['betweenregstime'] && $this->bb->settings['maxregsbetweentime']) {
                $time = TIME_NOW;
                $datecut = $time - (60 * 60 * $this->bb->settings['betweenregstime']);
                $query = $this->db->simple_select('users', '*', 'regip=' . $this->session->ipaddress . " AND regdate > '$datecut'");
                $regcount = $this->db->num_rows($query);
                if ($regcount >= $this->bb->settings['maxregsbetweentime']) {
                    $this->lang->error_alreadyregisteredtime = $this->lang->sprintf(
                        $this->lang->error_alreadyregisteredtime,
                        $regcount,
                        $this->bb->settings['betweenregstime']
                    );
                    $this->return = true;
                    $this->bb->error($this->lang->error_alreadyregisteredtime);
                }
            }
        }
        $bdaysel = '';
        if ($this->bb->settings['coppa'] == 'disabled') {
            $bdaysel = $bday2blank = '';
        }
        $this->bb->input['bday1'] = $this->bb->getInput('bday1', 0);
        for ($day = 1; $day <= 31; ++$day) {
            $selected = '';
            if ($this->bb->input['bday1'] == $day) {
                $selected = ' selected="selected"';
            }
            $bdaysel .= "<option value=\"{$day}\"{$selected}>{$day}</option>";
            //ev al("\$bdaysel .= \"".$this->templates->get('member_register_day')."\";");
        }
        $this->view->offsetSet('bdaysel', $bdaysel);

        $this->bb->input['bday2'] = $this->bb->getInput('bday2', 0);
        $bdaymonthsel = [];
        foreach (range(1, 12) as $number) {
            $bdaymonthsel[$number] = '';
        }
        $bdaymonthsel[$this->bb->input['bday2']] = 'selected="selected"';

        $this->view->offsetSet('bdaymonthsel', $bdaymonthsel);

        $this->bb->input['bday3'] = $this->bb->getInput('bday3', 0);

        if ($this->bb->input['bday3'] == 0) {
            $this->bb->input['bday3'] = '';
        }

        // Is COPPA checking enabled?
        if ($this->bb->settings['coppa'] != 'disabled' && !isset($this->bb->input['step'])) {
            // Just selected DOB, we check
            if ($this->bb->input['bday1'] && $this->bb->input['bday2'] && $this->bb->input['bday3']) {
                $this->bb->my_unsetcookie('coppauser');

                $months = get_bdays($this->bb->input['bday3']);
                if ($this->bb->input['bday2'] < 1 || $this->bb->input['bday2'] > 12 || $this->bb->input['bday3'] < (date('Y') - 100) ||
                    $this->bb->input['bday3'] > date('Y') || $this->bb->input['bday1'] > $months[$this->bb->input['bday2'] - 1]
                ) {
                    $this->return = true;
                    $this->bb->error($this->lang->error_invalid_birthday);
                }

                $bdaytime = @mktime(0, 0, 0, $this->bb->input['bday2'], $this->bb->input['bday1'], $this->bb->input['bday3']);

                // Store DOB in cookie so we can save it with the registration
                $this->bb->my_setcookie('coppadob', "{$this->bb->input['bday1']}-{$this->bb->input['bday2']}-{$this->bb->input['bday3']}", -1);

                // User is <= 13, we mark as a coppa user
                if ($bdaytime >= mktime(0, 0, 0, $this->time->formatDate('n'), $this->time->formatDate('d'), $this->time->formatDate('Y') - 13)) {
                    $this->bb->my_setcookie('coppauser', 1, -0);
                    $this->under_thirteen = true;
                }
                $this->bb->request_method = '';
            } // Show DOB select form
            else {
                $this->plugins->runHooks('member_register_coppa');

                $this->bb->my_unsetcookie('coppauser');

                //ev al("\$coppa = \"".$this->templates->get('member_register_coppa')."\";");
                $this->bb->output_page();
                return $this->view->render($response, '@forum/Member/coppa.html.twig');
                //exit;
            }
        }
    }

    public function index(Request $request, Response $response)
    {
        $this->common($response);
        if (isset($this->return)) {
            return;
        }

        $this->bb->showCoppaAgreement = false;
        // Is this user a COPPA user? We need to show the COPPA agreement too
        if ($this->bb->settings['coppa'] != 'disabled' && ($this->bb->cookies['coppauser'] == 1 || $this->under_thirteen)) {
            if ($this->bb->settings['coppa'] == 'deny') {
                $this->bb->error($this->lang->error_need_to_be_thirteen);
            }
            $this->bb->showCoppaAgreement = true;
            $this->lang->coppa_agreement_1 = $this->lang->sprintf($this->lang->coppa_agreement_1, $this->bb->settings['bbname']);
            //ev al("\$coppa_agreement = \"".$this->templates->get('member_register_agreement_coppa')."\";");
        }

        $this->plugins->runHooks('member_register_agreement');

        //ev al("\$agreement = \"".$this->templates->get('member_register_agreement')."\";");
        $this->bb->output_page();
        $this->view->render($response, '@forum/Member/agreement.html.twig');
    }

    public function doRegister(Request $request, Response $response)
    {
        $this->common($response);
        if (isset($this->return)) {
            return;
        }

        if (isset($this->bb->input['agree']) && !isset($this->bb->input['regsubmit'])) {// && $fromreg == 0)
            return $this->buildForm($response);
        }


        $this->plugins->runHooks('member_do_register_start');

        // Are checking how long it takes for users to register?
        if ($this->bb->settings['regtime'] > 0) {
            // Is the field actually set?
            if (isset($this->bb->input['regtime'])) {
                // Check how long it took for this person to register
                $timetook = TIME_NOW - $this->bb->getInput('regtime', 0);

                // See if they registered faster than normal
                if ($timetook < $this->bb->settings['regtime']) {
                    // This user registered pretty quickly, bot detected!
                    $this->lang->error_spam_deny_time = $this->lang->sprintf(
                        $this->lang->error_spam_deny_time,
                        $this->bb->settings['regtime'],
                        $timetook
                    );
                    $this->bb->error($this->lang->error_spam_deny_time);
                }
            } else {
                $this->bb->error($this->lang->error_spam_deny);
            }
        }

        // If we have hidden CATPCHA enabled and it's filled, deny registration
        if ($this->bb->settings['hiddencaptchaimage']) {
            $string = $this->bb->settings['hiddencaptchaimagefield'];

            if (!empty($this->bb->input[$string])) {
                $this->bb->error($this->lang->error_spam_deny);
            }
        }

        if ($this->bb->settings['regtype'] == 'randompass') {
            $password_length = (int)$this->bb->settings['minpasswordlength'];
            if ($password_length < 8) {
                $password_length = min(8, (int)$this->bb->settings['maxpasswordlength']);
            }

            $this->bb->input['password'] = random_str($password_length, $this->bb->settings['requirecomplexpasswords']);
            $this->bb->input['password2'] = $this->bb->input['password'];
        }

        if ($this->bb->settings['regtype'] == 'verify' || $this->bb->settings['regtype'] == 'admin' ||
            $this->bb->settings['regtype'] == 'both' || $this->bb->getInput('coppa', 0) == 1
        ) {
            $usergroup = 5;
        } else {
            $usergroup = 2;
        }

        // Set up user handler.
        $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'insert');

        $coppauser = 0;
        if (isset($this->bb->cookies['coppauser'])) {
            $coppauser = (int)$this->bb->cookies['coppauser'];
        }

        // Set the data for the new user.
        $user = [
            'username' => $this->bb->getInput('username', '', true),
            'password' => $this->bb->getInput('password', '', true),
            'password2' => $this->bb->getInput('password2', '', true),
            'email' => $this->bb->getInput('email', ''),
            'email2' => $this->bb->getInput('email2', ''),
            'usergroup' => $usergroup,
            'referrer' => $this->bb->getInput('referrername', '', true),
            'timezone' => $this->bb->getInput('timezoneoffset', ''),
            'language' => $this->bb->getInput('language', ''),
            'profile_fields' => $this->bb->getInput('profile_fields', []),
            'regip' => $this->session->ipaddress,
            'coppa_user' => $coppauser,
            'regcheck1' => $this->bb->getInput('regcheck1', ''),
            'regcheck2' => $this->bb->getInput('regcheck2', ''),
            'registration' => true
        ];

        // Do we have a saved COPPA DOB?
        if (isset($this->bb->cookies['coppadob'])) {
            list($dob_day, $dob_month, $dob_year) = explode('-', $this->bb->cookies['coppadob']);
            $user['birthday'] = [
                'day' => $dob_day,
                'month' => $dob_month,
                'year' => $dob_year
            ];
        }

        $user['options'] = [
            'allownotices' => $this->bb->getInput('allownotices', 0),
            'hideemail' => $this->bb->getInput('hideemail', 0),
            'subscriptionmethod' => $this->bb->getInput('subscriptionmethod', 0),
            'receivepms' => $this->bb->getInput('receivepms', 0),
            'pmnotice' => $this->bb->getInput('pmnotice', 0),
            'pmnotify' => $this->bb->getInput('pmnotify', 0),
            'invisible' => $this->bb->getInput('invisible', 0),
            'dstcorrection' => $this->bb->getInput('dstcorrection', '')
        ];

        $userhandler->set_data($user);

        $errors = '';

        if (!$userhandler->validate_user()) {
            $errors = $userhandler->get_friendly_errors();
        }

        if ($this->bb->settings['enablestopforumspam_on_register']) {
            $stop_forum_spam_checker = new \RunBB\Core\StopForumSpamChecker(
                $this->plugins,
                $this->bb->settings['stopforumspam_min_weighting_before_spam'],
                $this->bb->settings['stopforumspam_check_usernames'],
                $this->bb->settings['stopforumspam_check_emails'],
                $this->bb->settings['stopforumspam_check_ips'],
                $this->bb->settings['stopforumspam_log_blocks']
            );

            try {
                if ($stop_forum_spam_checker->is_user_a_spammer($user['username'], $user['email'], $this->session->ipaddress)) {
                    $this->bb->error($this->lang->sprintf(
                        $this->lang->error_stop_forum_spam_spammer,
                        $stop_forum_spam_checker->getErrorText([
                            'stopforumspam_check_usernames',
                            'stopforumspam_check_emails',
                            'stopforumspam_check_ips'
                        ])
                    ));
                }
            } catch (\Exception $e) {
                if ($this->bb->settings['stopforumspam_block_on_error']) {
                    $this->bb->error($this->lang->error_stop_forum_spam_fetching);
                }
            }
        }

        if ($this->bb->settings['captchaimage']) {
            $captcha = new \RunBB\Core\Captcha($this);

            if ($captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($captcha->get_errors() as $error) {
                    $errors[] = $error;
                }
            }
        }

        // If we have a security question, check to see if answer is correct
        if ($this->bb->settings['securityquestion']) {
            $question_id = $this->db->escape_string($this->bb->getInput('question_id', ''));
            $answer = $this->db->escape_string($this->bb->getInput('answer', '', true));

            $query = $this->db->query('
			SELECT q.*, s.sid
			FROM ' . TABLE_PREFIX . 'questionsessions s
			LEFT JOIN ' . TABLE_PREFIX . "questions q ON (q.qid=s.qid)
			WHERE q.active='1' AND s.sid='{$question_id}'
		");
            if ($this->db->num_rows($query) > 0) {
                $question = $this->db->fetch_array($query);
                $valid_answers = explode("\n", $question['answer']);
                $validated = 0;

                foreach ($valid_answers as $answers) {
                    if (my_strtolower($answers) == my_strtolower($answer)) {
                        $validated = 1;
                    }
                }

                if ($validated != 1) {
                    $update_question = [
                        'incorrect' => $question['incorrect'] + 1
                    ];
                    $this->db->update_query('questions', $update_question, "qid='{$question['qid']}'");

                    $errors[] = $this->lang->error_question_wrong;
                } else {
                    $update_question = [
                        'correct' => $question['correct'] + 1
                    ];
                    $this->db->update_query('questions', $update_question, "qid='{$question['qid']}'");
                }

                $this->db->delete_query('questionsessions', "sid='{$question_id}'");
            }
        }

        if (is_array($errors)) {
            //$referrername = htmlspecialchars_uni($this->bb->getInput('referrername'));
            $this->view->offsetSet('referrername', htmlspecialchars_uni($this->bb->getInput('referrername', '', true)));

            $regvars = [
                'allownoticescheck' => ($this->bb->getInput('allownotices', 0) == 1) ? 'checked="checked"' : '',
                'hideemailcheck' => ($this->bb->getInput('hideemail', 0) == 1) ? 'checked="checked"' : '',
//          'emailnotifycheck' => '',
                'receivepmscheck' => ($this->bb->getInput('receivepms', 0) == 1) ? 'checked="checked"' : '',
                'pmnoticecheck' => ($this->bb->getInput('pmnotice', 0) == 1) ? 'checked="checked"' : '',
                'pmnotifycheck' => ($this->bb->getInput('pmnotify', 0) == 1) ? 'checked="checked"' : '',
                'invisiblecheck' => ($this->bb->getInput('invisible', 0) == 1) ? 'checked="checked"' : '',
                'enabledstcheck' => ($this->bb->settings['dstcorrection'] == 1) ? 'checked="checked"' : '',
                'username' => htmlspecialchars_uni($this->bb->getInput('username', '', true)),
                'email' => htmlspecialchars_uni($this->bb->getInput('email', '')),
                'email2' => htmlspecialchars_uni($this->bb->getInput('email2', '')),
                'no_auto_subscribe_selected' => ($this->bb->getInput('subscriptionmethod', 0) == 0) ? 'selected="selected"' : '',
                'no_subscribe_selected' => ($this->bb->getInput('subscriptionmethod', 0) == 1) ? 'selected="selected"' : '',
                'instant_email_subscribe_selected' => ($this->bb->getInput('subscriptionmethod', 0) == 2) ? 'selected="selected"' : '',
                'instant_pm_subscribe_selected' => ($this->bb->getInput('subscriptionmethod', 0) == 3) ? 'selected="selected"' : '',
                'dst_auto_selected' => ($this->bb->getInput('dstcorrection', 0) == 2) ? 'selected="selected"' : '',
                'dst_enabled_selected' => ($this->bb->getInput('dstcorrection', 0) == 1) ? 'selected="selected"' : '',
                'dst_disabled_selected' => ($this->bb->getInput('dstcorrection', 0) == 0) ? 'selected="selected"' : ''
            ];
            $this->view->offsetSet('regvars', $regvars);

            $this->regerrors = $this->bb->inline_error($errors);
            $this->view->offsetSet('regerrors', $this->regerrors);
            $this->bb->input['action'] = 'register';
            $this->fromreg = 1;
            $this->buildForm($response);// return show error
        } else {
            $user_info = $userhandler->insert_user();

            // Invalidate solved captcha
            if ($this->bb->settings['captchaimage']) {
                $captcha->invalidate_captcha();
            }

            if ($this->bb->settings['regtype'] != 'randompass' && !isset($this->bb->cookies['coppauser'])) {
                // Log them in
                $this->bb->my_setcookie('runbbuser', $user_info['uid'] . '_' . $user_info['loginkey'], null, true);
            }

            if (isset($this->bb->cookies['coppauser'])) {
                $this->lang->redirect_registered_coppa_activate = $this->lang->sprintf(
                    $this->lang->redirect_registered_coppa_activate,
                    $this->bb->settings['bbname'],
                    $user_info['username']
                );
                $this->bb->my_unsetcookie('coppauser');
                $this->bb->my_unsetcookie('coppadob');
                $this->plugins->runHooks('member_do_register_end');
                $this->bb->error($this->lang->redirect_registered_coppa_activate);
            } elseif ($this->bb->settings['regtype'] == 'verify') {
                $activationcode = random_str();
                $now = TIME_NOW;
                $activationarray = [
                    'uid' => $user_info['uid'],
                    'dateline' => TIME_NOW,
                    'code' => $activationcode,
                    'type' => 'r'
                ];
                $this->db->insert_query('awaitingactivation', $activationarray);
                $emailsubject = $this->lang->sprintf($this->lang->emailsubject_activateaccount, $this->bb->settings['bbname']);
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                    case 1:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount1,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                    case 2:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount2,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                    default:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                }
                $this->mail->send($user_info['email'], $emailsubject, $emailmessage);

                $this->lang->redirect_registered_activation = $this->lang->sprintf(
                    $this->lang->redirect_registered_activation,
                    $this->bb->settings['bbname'],
                    $user_info['username']
                );

                $this->plugins->runHooks('member_do_register_end');

                $this->bb->error($this->lang->redirect_registered_activation);
            } elseif ($this->bb->settings['regtype'] == 'randompass') {
                $emailsubject = $this->lang->sprintf($this->lang->emailsubject_randompassword, $this->bb->settings['bbname']);
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_randompassword,
                            $user['username'],
                            $this->bb->settings['bbname'],
                            $user_info['username'],
                            $user_info['password']
                        );
                        break;
                    case 1:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_randompassword1,
                            $user['username'],
                            $this->bb->settings['bbname'],
                            $user_info['username'],
                            $user_info['password']
                        );
                        break;
                    case 2:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_randompassword2,
                            $user['username'],
                            $this->bb->settings['bbname'],
                            $user_info['username'],
                            $user_info['password']
                        );
                        break;
                    default:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_randompassword,
                            $user['username'],
                            $this->bb->settings['bbname'],
                            $user_info['username'],
                            $user_info['password']
                        );
                        break;
                }
                $this->mail->send($user_info['email'], $emailsubject, $emailmessage);

                $this->plugins->runHooks('member_do_register_end');

                $this->bb->error($this->lang->redirect_registered_passwordsent);
            } elseif ($this->bb->settings['regtype'] == 'admin') {
                $groups = $this->cache->read('usergroups');
                $admingroups = [];
                if (!empty($groups)) { // Shouldn't be...
                    foreach ($groups as $group) {
                        if ($group['cancp'] == 1) {
                            $admingroups[] = (int)$group['gid'];
                        }
                    }
                }

                if (!empty($admingroups)) {
                    $sqlwhere = 'usergroup IN (' . implode(',', $admingroups) . ')';
                    foreach ($admingroups as $admingroup) {
                        switch ($this->db->type) {
                            case 'pgsql':
                            case 'sqlite':
                                $sqlwhere .= " OR ','||additionalgroups||',' LIKE '%,{$admingroup},%'";
                                break;
                            default:
                                $sqlwhere .= " OR CONCAT(',',additionalgroups,',') LIKE '%,{$admingroup},%'";
                                break;
                        }
                    }
                    $q = $this->db->simple_select('users', 'uid,username,email,language', $sqlwhere);
                    while ($recipient = $this->db->fetch_array($q)) {
                        // First we check if the user's a super admin: if yes, we don't care about permissions
                        $is_super_admin = $this->user->is_super_admin($recipient['uid']);
                        if (!$is_super_admin) {
                            // Include admin functions
                            if (!file_exists(MYBB_ROOT . $this->config['admin_dir'] . '/inc/functions.php')) {
                                continue;
                            }

                            require_once MYBB_ROOT . $this->config['admin_dir'] . '/inc/functions.php';

                            // Verify if we have permissions to access user-users
                            require_once MYBB_ROOT . $this->config['admin_dir'] . '/modules/user/module_meta.php';
                            if (function_exists('user_admin_permissions')) {
                                // Get admin permissions
                                $adminperms = get_admin_permissions($recipient['uid']);

                                $permissions = user_admin_permissions();
                                if (array_key_exists('users', $permissions['permissions']) && $adminperms['user']['users'] != 1) {
                                    continue; // No permissions
                                }
                            }
                        }

                        // Load language
                        if ($recipient['language'] != $this->user->language && $this->lang->language_exists($recipient['language'])) {
                            $reset_lang = true;
                            $this->lang->set_language($recipient['language']);
                            $this->lang->load('member');
                        }

                        $subject = $this->lang->sprintf($this->lang->newregistration_subject, $this->bb->settings['bbname']);
                        $message = $this->lang->sprintf(
                            $this->lang->newregistration_message,
                            $recipient['username'],
                            $this->bb->settings['bbname'],
                            $user['username']
                        );
                        $this->mail->send($recipient['email'], $subject, $message);
                    }

                    // Reset language
                    if (isset($reset_lang)) {
                        $this->lang->set_language($this->user->language);
                        $this->lang->load('member');
                    }
                }

                $this->lang->redirect_registered_admin_activate = $this->lang->sprintf(
                    $this->lang->redirect_registered_admin_activate,
                    $this->bb->settings['bbname'],
                    $user_info['username']
                );

                $this->plugins->runHooks('member_do_register_end');

                $this->bb->error($this->lang->redirect_registered_admin_activate);
            } elseif ($this->bb->settings['regtype'] == 'both') {
                $groups = $this->cache->read('usergroups');
                $admingroups = [];
                if (!empty($groups)) { // Shouldn't be...
                    foreach ($groups as $group) {
                        if ($group['cancp'] == 1) {
                            $admingroups[] = (int)$group['gid'];
                        }
                    }
                }

                if (!empty($admingroups)) {
                    $sqlwhere = 'usergroup IN (' . implode(',', $admingroups) . ')';
                    foreach ($admingroups as $admingroup) {
                        switch ($this->db->type) {
                            case 'pgsql':
                            case 'sqlite':
                                $sqlwhere .= " OR ','||additionalgroups||',' LIKE '%,{$admingroup},%'";
                                break;
                            default:
                                $sqlwhere .= " OR CONCAT(',',additionalgroups,',') LIKE '%,{$admingroup},%'";
                                break;
                        }
                    }
                    $q = $this->db->simple_select('users', 'uid,username,email,language', $sqlwhere);
                    while ($recipient = $this->db->fetch_array($q)) {
                        // First we check if the user's a super admin: if yes, we don't care about permissions
                        $is_super_admin = $this->user->is_super_admin($recipient['uid']);
                        if (!$is_super_admin) {
                            // Include admin functions
                            if (!file_exists(MYBB_ROOT . $this->config['admin_dir'] . '/inc/functions.php')) {
                                continue;
                            }

                            require_once MYBB_ROOT . $this->config['admin_dir'] . '/inc/functions.php';

                            // Verify if we have permissions to access user-users
                            require_once MYBB_ROOT . $this->config['admin_dir'] . '/modules/user/module_meta.php';
                            if (function_exists('user_admin_permissions')) {
                                // Get admin permissions
                                $adminperms = get_admin_permissions($recipient['uid']);

                                $permissions = user_admin_permissions();
                                if (array_key_exists('users', $permissions['permissions']) && $adminperms['user']['users'] != 1) {
                                    continue; // No permissions
                                }
                            }
                        }

                        // Load language
                        if ($recipient['language'] != $this->user->language && $this->lang->language_exists($recipient['language'])) {
                            $reset_lang = true;
                            $this->lang->set_language($recipient['language']);
                            $this->lang->load('member');
                        }

                        $subject = $this->lang->sprintf($this->lang->newregistration_subject, $this->bb->settings['bbname']);
                        $message = $this->lang->sprintf(
                            $this->lang->newregistration_message,
                            $recipient['username'],
                            $this->bb->settings['bbname'],
                            $user['username']
                        );
                        $this->mail->send($recipient['email'], $subject, $message);
                    }

                    // Reset language
                    if (isset($reset_lang)) {
                        $this->lang->set_language($this->user->language);
                        $this->lang->load('member');
                    }
                }

                $activationcode = random_str();
                $activationarray = [
                    'uid' => $user_info['uid'],
                    'dateline' => TIME_NOW,
                    'code' => $activationcode,
                    'type' => 'b'
                ];
                $this->db->insert_query('awaitingactivation', $activationarray);
                $emailsubject = $this->lang->sprintf($this->lang->emailsubject_activateaccount, $this->bb->settings['bbname']);
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                    case 1:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount1,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                    case 2:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount2,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                    default:
                        $emailmessage = $this->lang->sprintf(
                            $this->lang->email_activateaccount,
                            $user_info['username'],
                            $this->bb->settings['bbname'],
                            $this->bb->settings['bburl'],
                            $user_info['uid'],
                            $activationcode
                        );
                        break;
                }
                $this->mail->send($user_info['email'], $emailsubject, $emailmessage);

                $this->lang->redirect_registered_activation = $this->lang->sprintf(
                    $this->lang->redirect_registered_activation,
                    $this->bb->settings['bbname'],
                    $user_info['username']
                );

                $this->plugins->runHooks('member_do_register_end');

                $this->bb->error($this->lang->redirect_registered_activation);
            } else {
                $this->lang->redirect_registered = $this->lang->sprintf(
                    $this->lang->redirect_registered,
                    $this->bb->settings['bbname'],
                    $user_info['username']
                );

                $this->plugins->runHooks('member_do_register_end');

                $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_registered);
            }
        }
    }

    private function buildForm($response)
    {
        $this->plugins->runHooks('member_register_start');

        $validator_extra = '';

        if (isset($this->bb->input['timezoneoffset'])) {
            $timezoneoffset = $this->bb->getInput('timezoneoffset', '');
        } else {
            $timezoneoffset = $this->bb->settings['timezoneoffset'];
        }
        $tzselect = $this->time->buildTimezoneSelect('timezoneoffset', $timezoneoffset, true);
        $this->view->offsetSet('tzselect', $tzselect);

        $stylelist = $this->themes->build_theme_select('style');

        if ($this->bb->settings['usertppoptions']) {
            $tppoptions = '';
            $explodedtpp = explode(',', $this->bb->settings['usertppoptions']);
            if (is_array($explodedtpp)) {
                foreach ($explodedtpp as $val) {
                    $val = trim($val);
                    $tpp_option = $this->lang->sprintf($this->lang->tpp_option, $val);
                    $selected = '';//FIXME ???
                    $tppoptions .= "<option value=\"{$val}\"{$selected}>{$tpp_option}</option>";
                    //ev al("\$tppoptions .= \"".$this->templates->get('usercp_options_tppselect_option')."\";");
                }
            }
            $tppselect = '
          <tr>
            <td colspan="2"><span class="smalltext">' . $this->lang->tpp . '</span></td>
          </tr>
          <tr>
            <td colspan="2">
              <select name="tpp">
              <option value="">' . $this->lang->use_default . '</option>
                ' . $tppoptions . '
              </select>
            </td>
          </tr>';
            //ev al("\$tppselect = \"".$this->templates->get('usercp_options_tppselect')."\";");
        }
        if ($this->bb->settings['userpppoptions']) {
            $pppoptions = '';
            $explodedppp = explode(',', $this->bb->settings['userpppoptions']);
            if (is_array($explodedppp)) {
                foreach ($explodedppp as $val) {
                    $val = trim($val);
                    $ppp_option = $this->lang->sprintf($this->lang->ppp_option, $val);
                    eval("\$pppoptions .= \"" . $this->templates->get('usercp_options_pppselect_option') . "\";");
                }
            }
            $pppselect = '
          <tr>
            <td colspan="2"><span class="smalltext">' . $this->lang->ppp . '</span></td>
          </tr>
          <tr>
            <td colspan="2">
              <select name="ppp">
              <option value="">' . $this->lang->use_default . '</option>
                ' . $pppoptions . '
              </select>
            </td>
          </tr>';
            //ev al("\$pppselect = \"".$this->templates->get('usercp_options_pppselect')."\";");
        }
        $referrername = '';
        if ($this->bb->settings['usereferrals'] == 1 && !$this->user->uid) {
            if (isset($this->bb->cookies['mybb']['referrer'])) {
                $query = $this->db->simple_select('users', 'uid,username', "uid='" . (int)$this->bb->cookies['mybb']['referrer'] . "'");
                $ref = $this->db->fetch_array($query);
                $referrername = $ref['username'];
            } elseif (isset($referrer)) {
                $query = $this->db->simple_select('users', 'username', "uid='" . (int)$referrer['uid'] . "'");
                $ref = $this->db->fetch_array($query);
                $referrername = $ref['username'];
            } elseif (!empty($referrername)) {
                $ref = $this->user->get_user_by_username($referrername);
                if (!$ref['uid']) {
                    $errors[] = $this->lang->error_badreferrer;
                }
            } else {
                $referrername = '';
            }
            if (isset($quickreg)) {
                $refbg = 'trow1';
            } else {
                $refbg = 'trow2';
            }
            //ev al("\$referrer = \"".$this->templates->get('member_register_referrer')."\";");
        }
//      else
//      {
//        $referrer = '';
//      }
        $this->view->offsetSet('referrername', $referrername);

        $this->bb->input['profile_fields'] = $this->bb->getInput('profile_fields', []);
        // Custom profile fields baby!
        $altbg = 'trow1';
        $requiredfields = $customfields = '';

        if ($this->bb->settings['regtype'] == 'verify' || $this->bb->settings['regtype'] == 'admin' ||
            $this->bb->settings['regtype'] == 'both' || $this->bb->getInput('coppa', 0) == 1
        ) {
            $usergroup = 5;
        } else {
            $usergroup = 2;
        }

        $pfcache = $this->cache->read('profilefields');

        if (is_array($pfcache)) {
            foreach ($pfcache as $profilefield) {
                if ($profilefield['required'] != 1 && $profilefield['registration'] != 1 ||
                    !$this->user->is_member(
                        $profilefield['editableby'],
                        ['usergroup' => $this->user->usergroup, 'additionalgroups' => $usergroup]
                    )
                ) {
                    continue;
                }

                $code = $select = $val = $options = $expoptions = $useropts = $seloptions = '';
                $profilefield['type'] = htmlspecialchars_uni($profilefield['type']);
                $thing = explode("\n", $profilefield['type'], '2');
                $type = trim($thing[0]);
                $options = $thing[1];
                $select = '';
                $field = "fid{$profilefield['fid']}";
                $profilefield['description'] = htmlspecialchars_uni($profilefield['description']);
                $profilefield['name'] = htmlspecialchars_uni($profilefield['name']);
                if ($errors && isset($this->bb->input['profile_fields'][$field])) {
                    $userfield = $this->bb->input['profile_fields'][$field];
                } else {
                    $userfield = '';
                }
                if ($type == 'multiselect') {
                    if ($errors) {
                        $useropts = $userfield;
                    } else {
                        $useropts = explode("\n", $userfield);
                    }
                    if (is_array($useropts)) {
                        foreach ($useropts as $key => $val) {
                            $seloptions[$val] = $val;
                        }
                    }
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $val = trim($val);
                            $val = str_replace("\n", "\\n", $val);

                            $sel = '';
                            if (isset($seloptions[$val]) && $val == $seloptions[$val]) {
                                $sel = ' selected="selected"';
                            }

                            eval("\$select .= \"" . $this->templates->get('usercp_profile_profilefields_select_option') . "\";");
                        }
                        if (!$profilefield['length']) {
                            $profilefield['length'] = 3;
                        }

                        eval("\$code = \"" . $this->templates->get('usercp_profile_profilefields_multiselect') . "\";");
                    }
                } elseif ($type == 'select') {
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $val = trim($val);
                            $val = str_replace("\n", "\\n", $val);
                            $sel = '';
                            if ($val == $userfield) {
                                $sel = ' selected="selected"';
                            }

                            eval("\$select .= \"" . $this->templates->get('usercp_profile_profilefields_select_option') . "\";");
                        }
                        if (!$profilefield['length']) {
                            $profilefield['length'] = 1;
                        }

                        eval("\$code = \"" . $this->templates->get('usercp_profile_profilefields_select') . "\";");
                    }
                } elseif ($type == 'radio') {
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $checked = '';
                            if ($val == $userfield) {
                                $checked = 'checked="checked"';
                            }

                            eval("\$code .= \"" . $this->templates->get('usercp_profile_profilefields_radio') . "\";");
                        }
                    }
                } elseif ($type == 'checkbox') {
                    if ($errors) {
                        $useropts = $userfield;
                    } else {
                        $useropts = explode("\n", $userfield);
                    }
                    if (is_array($useropts)) {
                        foreach ($useropts as $key => $val) {
                            $seloptions[$val] = $val;
                        }
                    }
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $checked = '';
                            if (isset($seloptions[$val]) && $val == $seloptions[$val]) {
                                $checked = 'checked="checked"';
                            }

                            eval("\$code .= \"" . $this->templates->get('usercp_profile_profilefields_checkbox') . "\";");
                        }
                    }
                } elseif ($type == 'textarea') {
                    $value = htmlspecialchars_uni($userfield);
                    eval("\$code = \"" . $this->templates->get('usercp_profile_profilefields_textarea') . "\";");
                } else {
                    $value = htmlspecialchars_uni($userfield);
                    $maxlength = '';
                    if ($profilefield['maxlength'] > 0) {
                        $maxlength = " maxlength=\"{$profilefield['maxlength']}\"";
                    }

                    eval("\$code = \"" . $this->templates->get('usercp_profile_profilefields_text') . "\";");
                }

                if ($profilefield['required'] == 1) {
                    // JS validator extra, choose correct selectors for everything except single select which always has value
                    if ($type != 'select') {
                        if ($type == 'textarea') {
                            $inp_selector = "$('textarea[name=\"profile_fields[{$field}]\"]')";
                        } elseif ($type == 'multiselect') {
                            $inp_selector = "$('select[name=\"profile_fields[{$field}][]\"]')";
                        } elseif ($type == 'checkbox') {
                            $inp_selector = "$('input[name=\"profile_fields[{$field}][]\"]')";
                        } else {
                            $inp_selector = "$('input[name=\"profile_fields[{$field}]\"]')";
                        }

                        $validator_extra .= "
						{$inp_selector}.rules('add', {
							required: true,
							messages: {
								required: '{$this->lang->js_validator_not_empty}'
							}
						});\n";
                    }

                    eval("\$requiredfields .= \"" . $this->templates->get('member_register_customfield') . "\";");
                } else {
                    eval("\$customfields .= \"" . $this->templates->get('member_register_customfield') . "\";");
                }
            }

            if ($requiredfields) {
                $requiredfields = '
            <fieldset class="trow2">
                <legend><strong>' . $this->lang->required_info . '</strong></legend>
                <table cellspacing="0" cellpadding="' . $this->bb->theme['tablespace'] . '">
                    ' . $requiredfields . '
                </table>
            </fieldset>';
                //ev al("\$requiredfields = \"".$this->templates->get('member_register_requiredfields')."\";");
            }

            if ($customfields) {
                $customfields = '
            <br />
            <fieldset class="trow2">
                <legend><strong>' . $this->lang->additional_info . '</strong></legend>
                <table cellspacing="0" cellpadding="' . $this->bb->theme['tablespace'] . '">
                    ' . $customfields . '
                </table>
            </fieldset>';
                //ev al("\$customfields = \"".$this->templates->get('member_register_additionalfields')."\";");
            }
        }
        $this->view->offsetSet('requiredfields', $requiredfields);
        $this->view->offsetSet('customfields', $customfields);

        $regvars = [];
        if (!isset($this->fromreg)) {
            $regvars = [
                'allownoticescheck' => 'checked="checked"',
                'hideemailcheck' => '',
//        'emailnotifycheck' => '',
                'receivepmscheck' => 'checked="checked"',
                'pmnoticecheck' => 'checked="checked"',
                'pmnotifycheck' => '',
                'invisiblecheck' => '',
                'enabledstcheck' => ($this->bb->settings['dstcorrection'] == 1) ? 'checked="checked"' : '',
                'username' => '',
                'email' => '',
                'email2' => '',
                'no_auto_subscribe_selected' => '',
                'no_subscribe_selected' => '',
                'instant_email_subscribe_selected' => '',
                'instant_pm_subscribe_selected' => '',
                'dst_auto_selected' => '',
                'dst_enabled_selected' => '',
                'dst_disabled_selected' => ''
            ];
        }
        $this->view->offsetSet('regvars', $regvars);

        // Spambot registration image thingy
        $regimage = '';
        if ($this->bb->settings['captchaimage']) {
            $captcha = new \RunBB\Core\Captcha($this, true, 'member_register_regimage');

            if (is_array($captcha->html)) {
                $regimage = $captcha->html;//must be array

                if ($this->bb->settings['captchaimage'] == 1) {
                    // JS validator extra for our default CAPTCHA
                    $validator_extra .= "
					$('#imagestring').rules('add', {
						required: true,
						remote:{
							url: '{$this->bb->settings['bburl']}/xmlhttp?action=validate_captcha',
							type: 'post',
							dataType: 'json',
							data:
							{
								imagehash: function () {
									return $('#imagehash').val();
								},
								my_post_key: my_post_key
							},
						},
						messages: {
							remote: '{$this->lang->js_validator_no_image_text}'
						}
					});\n";
                }
            }
        }
        $this->view->offsetSet('regimage', $regimage);

        // Security Question
        $this->bb->showQuestionBox = false;
        if ($this->bb->settings['securityquestion']) {
            $sid = $this->user->generate_question();
            $query = $this->db->query('
				SELECT q.question, s.sid
				FROM ' . TABLE_PREFIX . 'questionsessions s
				LEFT JOIN ' . TABLE_PREFIX . "questions q ON (q.qid=s.qid)
				WHERE q.active='1' AND s.sid='{$sid}'
			");
            if ($this->db->num_rows($query) > 0) {
                $question = $this->db->fetch_array($query);
                $refresh = false;
                // Total questions
                $q = $this->db->simple_select('questions', 'COUNT(qid) as num', 'active=1');
                $num = $this->db->fetch_field($q, 'num');
                if ($num > 1) {
                    $refresh = true;
                    //ev al("\$refresh = \"".$this->templates->get('member_register_question_refresh')."\";");
                }
                $this->bb->showQuestionBox = true;
                $this->view->offsetSet('question', $question['question']);
                $this->view->offsetSet('sid', $sid);
                $this->view->offsetSet('refresh', $refresh);

                //ev al("\$questionbox = \"".$this->templates->get('member_register_question')."\";");

                $validator_extra .= "
              $('#answer').rules('add', {
                required: true,
                remote:{
                  url: '{$this->bb->settings['bburl']}/xmlhttp?action=validate_question',
                  type: 'post',
                  dataType: 'json',
                  data:
                  {
                    question: function () {
                      return $('#question_id').val();
                    },
                    my_post_key: my_post_key
                  },
                },
                messages: {
                  remote: '{$this->lang->js_validator_no_security_question}'
                }
              });\n";
            }
        }

//      $hiddencaptcha = '';
//      // Hidden CAPTCHA for Spambots
//      if($this->bb->settings['hiddencaptchaimage'])
//      {
//        $captcha_field = $this->bb->settings['hiddencaptchaimagefield'];
//        //ev al("\$hiddencaptcha = \"".$this->templates->get('member_register_hiddencaptcha')."\";");
//      }

        if ($this->bb->settings['regtype'] != 'randompass') {
            // JS validator extra
            $this->lang->js_validator_password_length = $this->lang->sprintf(
                $this->lang->js_validator_password_length,
                $this->bb->settings['minpasswordlength']
            );

            // See if the board has 'require complex passwords' enabled.
            if ($this->bb->settings['requirecomplexpasswords'] == 1) {
                $this->lang->password = $this->lang->complex_password = $this->lang->sprintf(
                    $this->lang->complex_password,
                    $this->bb->settings['minpasswordlength']
                );

                $validator_extra .= "
				$('#password').rules('add', {
					required: true,
					minlength: {$this->bb->settings['minpasswordlength']},
					remote:{
						url: '{$this->bb->settings['bburl']}/xmlhttp?action=complex_password',
						type: 'post',
						dataType: 'json',
						data:
						{
							my_post_key: my_post_key
						},
					},
					messages: {
						minlength: '{$this->lang->js_validator_password_length}',
						required: '{$this->lang->js_validator_password_length}',
						remote: '{$this->lang->js_validator_no_image_text}'
					}
				});\n";
            } else {
                $validator_extra .= "
				$('#password').rules('add', {
					required: true,
					minlength: {$this->bb->settings['minpasswordlength']},
					messages: {
						minlength: '{$this->lang->js_validator_password_length}',
						required: '{$this->lang->js_validator_password_length}'
					}
				});\n";
            }

            $validator_extra .= "
				$('#password2').rules('add', {
					required: true,
					minlength: {$this->bb->settings['minpasswordlength']},
					equalTo: '#password',
					messages: {
						minlength: '{$this->lang->js_validator_password_length}',
						required: '{$this->lang->js_validator_password_length}',
						equalTo: '{$this->lang->js_validator_password_matches}'
					}
				});\n";

            //ev al("\$passboxes = \"".$this->templates->get('member_register_password')."\";");
        }
        $this->view->offsetSet('validator_extra', $validator_extra);

        // JS validator extra
        if ($this->bb->settings['maxnamelength'] > 0 && $this->bb->settings['minnamelength'] > 0) {
            $this->lang->js_validator_username_length = $this->lang->sprintf(
                $this->lang->js_validator_username_length,
                $this->bb->settings['minnamelength'],
                $this->bb->settings['maxnamelength']
            );
        }

        $languages = $this->lang->get_languages();
        $boardlanguage = [];
        if (count($languages) > 1) {
            foreach ($languages as $name => $language) {
                $language = htmlspecialchars_uni($language);
                $sel = '';
                if ($this->bb->getInput('language', '') == $name) {
                    $sel = ' selected="selected"';
                }
                $boardlanguage[] = [
                    'name' => $name,
                    'sel' => $sel,
                    'language' => $language
                ];

                //ev al('$langoptions .= "'.$this->templates->get('usercp_options_language_option').'";');
            }
            //ev al('$boardlanguage = "'.$this->templates->get('member_register_language').'";');
        }
        $this->view->offsetSet('boardlanguage', $boardlanguage);
        $this->view->offsetSet('regerrors', $this->regerrors);

        // Set the time so we can find automated signups
        //$time = TIME_NOW;
        $this->view->offsetSet('time', TIME_NOW);

        $this->plugins->runHooks('member_register_end');

        //ev al("\$registration = \"".$this->templates->get('member_register')."\";");
        $this->bb->output_page();
        return $this->view->render($response, '@forum/Member/register.html.twig');
    }
}
