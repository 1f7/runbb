<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class ResendActivation extends AbstractController
{

    private function init()
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'resendactivation');
    }

    public function index(Request $request, Response $response)
    {
        $this->init();

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $this->lang->load('member');
        $this->bb->add_breadcrumb($this->lang->nav_resendactivation);

        $this->plugins->runHooks('member_resendactivation');

        if ($this->bb->settings['regtype'] == 'admin') {
            $this->bb->error($this->lang->error_activated_by_admin);
        }
        if ($this->user->uid && $this->user->usergroup != 5) {
            $this->bb->error($this->lang->error_alreadyactivated);
        }

        $query = $this->db->simple_select('awaitingactivation', '*', "uid='" . (int)$this->user->uid . "' AND type='b'");
        $activation = $this->db->fetch_array($query);

        if ($activation['validated'] == 1) {
            $this->bb->error($this->lang->error_activated_by_admin);
        }

        $this->plugins->runHooks('member_resendactivation_end');

        //ev al('\$activate = \''.$templates->get('member_resendactivation').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Member/resendactivation.html.twig');
    }

    public function doResendActivation(Request $request, Response $response)
    {
        $this->init();

        $this->plugins->runHooks('member_do_resendactivation_start');

        if ($this->bb->settings['regtype'] == 'admin') {
            $this->bb->error($this->lang->error_activated_by_admin);
        }

        $query = $this->db->query('
		SELECT u.uid, u.username, u.usergroup, u.email, a.code, a.type, a.validated
		FROM ' . TABLE_PREFIX . 'users u
		LEFT JOIN ' . TABLE_PREFIX . "awaitingactivation a ON (a.uid=u.uid AND a.type='r' OR a.type='b')
		WHERE u.email='" . $this->db->escape_string($this->bb->getInput('email', '')) . "'
	");
        $numusers = $this->db->num_rows($query);
        if ($numusers < 1) {
            $this->bb->error($this->lang->error_invalidemail);
        } else {
            while ($user = $this->db->fetch_array($query)) {
                if ($user['type'] == 'b' && $user['validated'] == 1) {
                    $this->bb->error($this->lang->error_activated_by_admin);
                }

                if ($user['usergroup'] == 5) {
                    if (!$user['code']) {
                        $user['code'] = random_str();
                        $uid = $user['uid'];
                        $awaitingarray = [
                            'uid' => $uid,
                            'dateline' => TIME_NOW,
                            'code' => $user['code'],
                            'type' => $user['type']
                        ];
                        $this->db->insert_query('awaitingactivation', $awaitingarray);
                    }
                    $username = $user['username'];
                    $email = $user['email'];
                    $activationcode = $user['code'];
                    $emailsubject = $this->lang->sprintf($this->lang->emailsubject_activateaccount, $this->bb->settings['bbname']);
                    switch ($this->bb->settings['username_method']) {
                        case 0:
                            $emailmessage = $this->lang->sprintf(
                                $this->lang->email_activateaccount,
                                $user['username'],
                                $this->bb->settings['bbname'],
                                $this->bb->settings['bburl'],
                                $user['uid'],
                                $activationcode
                            );
                            break;
                        case 1:
                            $emailmessage = $this->lang->sprintf(
                                $this->lang->email_activateaccount1,
                                $user['username'],
                                $this->bb->settings['bbname'],
                                $this->bb->settings['bburl'],
                                $user['uid'],
                                $activationcode
                            );
                            break;
                        case 2:
                            $emailmessage = $this->lang->sprintf(
                                $this->lang->email_activateaccount2,
                                $user['username'],
                                $this->bb->settings['bbname'],
                                $this->bb->settings['bburl'],
                                $user['uid'],
                                $activationcode
                            );
                            break;
                        default:
                            $emailmessage = $this->lang->sprintf(
                                $this->lang->email_activateaccount,
                                $user['username'],
                                $this->bb->settings['bbname'],
                                $this->bb->settings['bburl'],
                                $user['uid'],
                                $activationcode
                            );
                            break;
                    }
                    $this->mail->send($email, $emailsubject, $emailmessage);
                }
            }
            $this->plugins->runHooks('member_do_resendactivation_end');

            $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_activationresent);
        }
    }
}
