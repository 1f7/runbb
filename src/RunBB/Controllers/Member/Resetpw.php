<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Resetpw extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'resetpassword');

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $this->lang->load('member');
        $this->bb->add_breadcrumb($this->lang->nav_resetpassword);

        $this->plugins->runHooks('member_resetpassword_start');

        if (isset($this->bb->input['username'])) {
            $this->bb->input['username'] = $this->bb->getInput('username', '', true);
            $options = [
                'username_method' => $this->bb->settings['username_method'],
                'fields' => '*',
            ];
            $user = $this->user->get_user_by_username($this->bb->input['username'], $options);
            if (!$user) {
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $this->bb->error($this->lang->error_invalidpworusername);
                        break;
                    case 1:
                        $this->bb->error($this->lang->error_invalidpworusername1);
                        break;
                    case 2:
                        $this->bb->error($this->lang->error_invalidpworusername2);
                        break;
                    default:
                        $this->bb->error($this->lang->error_invalidpworusername);
                        break;
                }
            }
        } else {
            $user = $this->user->get_user($this->bb->getInput('uid', 0));
        }
        if (isset($this->bb->input['code']) && $user) {
            $query = $this->db->simple_select('awaitingactivation', 'code', "uid='" . $user['uid'] . "' AND type='p'");
            $activationcode = $this->db->fetch_field($query, 'code');
            $now = TIME_NOW;
            if (!$activationcode || $activationcode != $this->bb->getInput('code', '')) {
                $this->bb->error($this->lang->error_badlostpwcode);
            }
            $this->db->delete_query('awaitingactivation', "uid='" . $user['uid'] . "' AND type='p'");
            $username = $user['username'];

            // Generate a new password, then update it
            $password_length = (int)$this->bb->settings['minpasswordlength'];

            if ($password_length < 8) {
                $password_length = 8;
            }

            $password = random_str($password_length);
            $logindetails = $this->user->update_password($user['uid'], md5($password), $user['salt']);

            $email = $user['email'];

            $this->plugins->runHooks('member_resetpassword_process');

            $emailsubject = $this->lang->sprintf($this->lang->emailsubject_passwordreset, $this->bb->settings['bbname']);
            $emailmessage = $this->lang->sprintf($this->lang->email_passwordreset, $username, $this->bb->settings['bbname'], $password);
            $this->mail->send($email, $emailsubject, $emailmessage);

            $this->plugins->runHooks('member_resetpassword_reset');

            $this->bb->error($this->lang->redirect_passwordreset);
        } else {
            $this->plugins->runHooks('member_resetpassword_form');

            switch ($this->bb->settings['username_method']) {
                case 0:
                    $lang_username = $this->lang->username;
                    break;
                case 1:
                    $lang_username = $this->lang->username1;
                    break;
                case 2:
                    $lang_username = $this->lang->username2;
                    break;
                default:
                    $lang_username = $this->lang->username;
                    break;
            }

            $code = $this->bb->getInput('code', '');

            if (!isset($user['username'])) {
                $user['username'] = '';
            }
            $this->view->offsetSet('lang_username', $lang_username);
            $this->view->offsetSet('code', $code);
            $this->view->offsetSet('username', $user['username']);

            //ev al('\$activate = \''.$templates->get('member_resetpassword').'\';');
            $this->bb->output_page();
            $this->view->render($response, '@forum/Member/resetpw.html.twig');
        }
    }
}
