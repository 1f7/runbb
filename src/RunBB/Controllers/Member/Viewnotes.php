<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Member;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Viewnotes extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        define('ALLOWABLE_PAGE', 'viewnotes');

        $this->bb->nosession['avatar'] = 1;//FIXME ???

        $this->lang->load('member');

        $uid = $this->bb->getInput('uid', 0);
        $user = $this->user->get_user($uid);

        // Make sure we are looking at a real user here.
        if (!$user) {
            $this->bb->error($this->lang->error_nomember);
        }

        if ($this->user->uid == 0 || $this->bb->usergroup['canmodcp'] != 1) {
            return $this->bb->error_no_permission();
        }

        $this->lang->view_notes_for = $this->lang->sprintf($this->lang->view_notes_for, $user->username);

        $user->usernotes = nl2br(htmlspecialchars_uni($user->usernotes));

        $this->plugins->runHooks('member_viewnotes');

        $this->bb->output_page();
        return $this->view->render($response, '@forum/Member/viewnotes.html.twig');
    }
}
