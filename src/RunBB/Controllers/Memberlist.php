<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Memberlist extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('memberlist');

        if ($this->bb->settings['enablememberlist'] == 0) {
            $this->bb->error($this->lang->memberlist_disabled);
        }

        $this->plugins->runHooks('memberlist_start');

        $this->bb->add_breadcrumb($this->lang->nav_memberlist, 'memberlist');

        if ($this->bb->usergroup['canviewmemberlist'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Showing advanced search page?
        if ($this->bb->getInput('action', '') === 'search') {
            $this->plugins->runHooks('memberlist_search');
            $this->bb->add_breadcrumb($this->lang->nav_memberlist_search);

            $this->bb->output_page();
            $this->view->render($response, '@forum/memberlist_search.html.twig');
        } else {
            $colspan = 6;
            $search_url = '';

            // Incoming sort field?
            if (isset($this->bb->input['sort'])) {
                $this->bb->input['sort'] = strtolower($this->bb->getInput('sort', ''));
            } else {
                $this->bb->input['sort'] = $this->bb->settings['default_memberlist_sortby'];
            }

            $sort_selected = [
                'regdate' => '',
                'lastvisit' => '',
                'reputation' => '',
                'postnum' => '',
                'referrals' => '',
                'username' => ''
            ];

            switch ($this->bb->input['sort']) {
                case 'regdate':
                    $sort_field = 'u.regdate';
                    break;
                case 'lastvisit':
                    $sort_field = 'u.lastactive';
                    break;
                case 'reputation':
                    $sort_field = 'u.reputation';
                    break;
                case 'postnum':
                    $sort_field = 'u.postnum';
                    break;
                case 'threadnum':
                    $sort_field = 'u.threadnum';
                    break;
                case 'referrals':
                    $sort_field = 'u.referrals';
                    break;
                default:
                    $sort_field = 'u.username';
                    $this->bb->input['sort'] = 'username';
                    break;
            }
            $sort_selected[$this->bb->input['sort']] = ' selected="selected"';
            $this->view->offsetSet('sort_selected', $sort_selected);

            // Incoming sort order?
            if (isset($this->bb->input['order'])) {
                $this->bb->input['order'] = strtolower($this->bb->input['order']);
            } else {
                $this->bb->input['order'] = strtolower($this->bb->settings['default_memberlist_order']);
            }

            $order_check = ['ascending' => '', 'descending' => ''];
            if ($this->bb->input['order'] == 'ascending' ||
                (!$this->bb->input['order'] && $this->bb->input['sort'] == 'username')) {
                $sort_order = 'ASC';
                $sortordernow = 'ascending';
                $oppsort = $this->lang->desc;
                $oppsortnext = 'descending';
                $this->bb->input['order'] = 'ascending';
            } else {
                $sort_order = 'DESC';
                $sortordernow = 'descending';
                $oppsort = $this->lang->asc;
                $oppsortnext = 'ascending';
                $this->bb->input['order'] = 'descending';
            }
            $order_check[$this->bb->input['order']] = ' checked="checked"';
            $this->view->offsetSet('order_check', $order_check);

            // Incoming results per page?
            $this->bb->input['perpage'] = $this->bb->getInput('perpage', 0);
            if ($this->bb->input['perpage'] > 0 && $this->bb->input['perpage'] <= 500) {
                $per_page = $this->bb->input['perpage'];
            } elseif ($this->bb->settings['membersperpage']) {
                $per_page = $this->bb->input['perpage'] = (int)$this->bb->settings['membersperpage'];
            } else {
                $per_page = $this->bb->input['perpage'] = 20;
            }

            $search_query = '1=1';
            $search_url = '';

            // Limiting results to a certain letter
            if (isset($this->bb->input['letter'])) {
                $letter = chr(ord($this->bb->getInput('letter', '')));
                if ($this->bb->input['letter'] == -1) {
                    $search_query .= " AND u.username NOT REGEXP('[a-zA-Z]')";
                } elseif (strlen($letter) == 1) {
                    $search_query .= " AND u.username LIKE '" . $this->db->escape_string_like($letter) . "%'";
                }
                $search_url .= "&letter={$letter}";
            }

            // Searching for a matching username
            $search_username = htmlspecialchars_uni(trim($this->bb->getInput('username', '')));
            $this->view->offsetSet('search_username', $search_username);
            if ($search_username != '') {
                $username_like_query = $this->db->escape_string_like($search_username);

                // Name begins with
                if ($this->bb->input['username_match'] == 'begins') {
                    $search_query .= " AND u.username LIKE '" . $username_like_query . "%'";
                    $search_url .= '&username_match=begins';
                } // Just contains
                else {
                    $search_query .= " AND u.username LIKE '%" . $username_like_query . "%'";
                }

                $search_url .= '&username=' . urlencode($search_username);
            }

            // Website contains
            $this->bb->input['website'] = trim($this->bb->getInput('website', ''));

            $this->view->offsetSet('search_website', htmlspecialchars_uni($this->bb->input['website']));

            if (trim($this->bb->input['website'])) {
                $search_query .= " AND u.website LIKE '%" .
                    $this->db->escape_string_like($this->bb->input['website']) . "%'";
                $search_url .= '&website=' . urlencode($this->bb->input['website']);
            }

            // Search by contact field input
            foreach (['aim', 'icq', 'google', 'skype', 'yahoo'] as $cfield) {
                $csetting = 'allow' . $cfield . 'field';
                $this->bb->input[$cfield] = trim($this->bb->getInput($cfield, ''));
                if ($this->bb->input[$cfield] && $this->bb->settings[$csetting] != '') {
                    if ($this->bb->settings[$csetting] != -1) {
                        $gids = explode(',', (string)$this->bb->settings[$csetting]);

                        $search_query .= ' AND (';
                        $or = '';
                        foreach ($gids as $gid) {
                            $gid = (int)$gid;
                            $search_query .= $or . 'u.usergroup=\'' . $gid . '\'';
                            switch ($this->db->type) {
                                case 'pgsql':
                                case 'sqlite':
                                    $search_query .= " OR ','||u.additionalgroups||',' LIKE '%,{$gid},%'";
                                    break;
                                default:
                                    $search_query .= " OR CONCAT(',',u.additionalgroups,',') LIKE '%,{$gid},%'";
                                    break;
                            }
                            $or = ' OR ';
                        }
                        $search_query .= ')';
                    }
                    if ($cfield == 'icq') {
                        $search_query .= " AND u.{$cfield} LIKE '%" . (int)$this->bb->input[$cfield] . "%'";
                    } else {
                        $search_query .= " AND u.{$cfield} LIKE '%" .
                            $this->db->escape_string_like($this->bb->input[$cfield]) . "%'";
                    }
                    $search_url .= "&{$cfield}=" . urlencode($this->bb->input[$cfield]);
                }
            }

            $usergroups_cache = $this->cache->read('usergroups');

            $group = [];
            foreach ($usergroups_cache as $gid => $groupcache) {
                if ($groupcache['showmemberlist'] == 0) {
                    $group[] = (int)$gid;
                }
            }

            if (is_array($group) && !empty($group)) {
                $hiddengroup = implode(',', $group);

                $search_query .= " AND u.usergroup NOT IN ({$hiddengroup})";

                foreach ($group as $hidegid) {
                    switch ($this->db->type) {
                        case 'pgsql':
                        case 'sqlite':
                            $search_query .= " AND ','||u.additionalgroups||',' NOT LIKE '%,{$hidegid},%'";
                            break;
                        default:
                            $search_query .= " AND CONCAT(',',u.additionalgroups,',') NOT LIKE '%,{$hidegid},%'";
                            break;
                    }
                }
            }
            $sorturl = htmlspecialchars_uni("memberlist?perpage={$this->bb->input['perpage']}{$search_url}");
            $this->view->offsetSet('sorturl', $sorturl);
            $search_url = htmlspecialchars_uni("memberlist?sort={$this->bb->input['sort']}
                &order={$this->bb->input['order']}&perpage={$this->bb->input['perpage']}{$search_url}");
            //$this->view->offsetSet('errors', $this->errors);

            $this->plugins->runHooks('memberlist_intermediate');

            $query = $this->db->simple_select('users u', 'COUNT(*) AS users', "{$search_query}");
            $num_users = $this->db->fetch_field($query, 'users');

            $page = $this->bb->getInput('page', 0);
            if ($page && $page > 0) {
                $start = ($page - 1) * $per_page;
            } else {
                $start = 0;
                $page = 1;
            }

            $sort = htmlspecialchars_uni($this->bb->input['sort']);
            $orderarrow[$sort] = "<span class=\"smalltext\">[<a href=\"{$sorturl}&sort={$sort}
                &order={$oppsortnext}\">{$oppsort}</a>]</span>";

            $this->view->offsetSet('orderarrow', $orderarrow);

            $this->view->offsetSet(
                'multipage',
                $this->pagination->multipage($num_users, $per_page, $page, $search_url)
            );

            // Cache a few things
            $usertitles = $this->cache->read('usertitles');
            $usertitles_cache = [];
            foreach ($usertitles as $usertitle) {
                $usertitles_cache[$usertitle['posts']] = $usertitle;
            }
            $users = [];
            $query = $this->db->query('
		SELECT u.*, f.*
		FROM ' . TABLE_PREFIX . 'users u
		LEFT JOIN ' . TABLE_PREFIX . "userfields f ON (f.ufid=u.uid)
		WHERE {$search_query}
		ORDER BY {$sort_field} {$sort_order}
		LIMIT {$start}, {$per_page}
	");
            while ($user = $this->db->fetch_array($query)) {
                $user = $this->plugins->runHooks('memberlist_user', $user);

                $alt_bg = alt_trow();

                $user['username'] = $this->user->format_name(
                    $user['username'],
                    $user['usergroup'],
                    $user['displaygroup']
                );

                $user['profilelink'] = $this->user->build_profile_link($user['username'], $user['uid']);

                // Get the display usergroup
                if (empty($user['displaygroup'])) {
                    $user['displaygroup'] = $user['usergroup'];
                }
                $usergroup = $usergroups_cache[$user['displaygroup']];

                $usergroup['groupimage'] = '';
                // Work out the usergroup/title stuff
                if (!empty($usergroup['image'])) {
                    if (!empty($this->user->language)) {
                        $language = $this->user->language;
                    } else {
                        $language = $this->bb->settings['bblanguage'];
                    }
                    $usergroup['image'] = str_replace('{lang}', $language, $usergroup['image']);
                    $usergroup['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $usergroup['image']);
                    $usergroup['groupimage'] = "<img src=\"{$usergroup['image']}\" alt=\"{$usergroup['title']}\" 
                        title=\"{$usergroup['title']}\" />";
                }

                $has_custom_title = 0;
                if (trim($user['usertitle']) != '') {
                    $has_custom_title = 1;
                }

                if ($usergroup['usertitle'] != '' && !$has_custom_title) {
                    $user['usertitle'] = $usergroup['usertitle'];
                } elseif (is_array($usertitles_cache) && !$usergroup['usertitle']) {
                    foreach ($usertitles_cache as $posts => $titleinfo) {
                        if ($user['postnum'] >= $posts) {
                            if (!$has_custom_title) {
                                $user['usertitle'] = $titleinfo['title'];
                            }
                            $user['stars'] = $titleinfo['stars'];
                            $user['starimage'] = $titleinfo['starimage'];
                            break;
                        }
                    }
                }

                $user['usertitle'] = htmlspecialchars_uni($user['usertitle']);

                if (!empty($usergroup['stars'])) {
                    $user['stars'] = $usergroup['stars'];
                }

                if (empty($user['starimage'])) {
                    $user['starimage'] = $usergroup['starimage'];
                }

                $user['userstars'] = '';
                if (!empty($user['starimage'])) {
                    // Only display stars if we have an image to use...
                    $starimage = str_replace('{theme}', $this->bb->theme['imgdir'], $user['starimage']);

                    for ($i = 0; $i < $user['stars']; ++$i) {
                        $user['userstars'] .= "<img src=\"{$this->bb->asset_url}/{$starimage}\" 
                            border=\"0\" alt=\"*\" />";
                    }
                }

                if ($user['userstars'] && $usergroup['groupimage']) {
                    $user['userstars'] = '<br />' . $user['userstars'];
                }

                // Show avatar
                $useravatar = $this->user->format_avatar(
                    $user['avatar'],
                    $user['avatardimensions'],
                    my_strtolower($this->bb->settings['memberlistmaxavatarsize'])
                );
                //$user['avatar'] = "<img src=\"{$useravatar['image']}\" alt=\"\" {$useravatar['width_height']} />";
//                $user['avatar'] = '<div class="img-circle hidden-xs" style="width:70px; height:70px;
//  background:url(' . $useravatar['image'] . ') no-repeat center center; background-size:cover;"></div>';
                $user['avatar'] = $useravatar['image'];

                if ($user['invisible'] == 1 &&
                    $this->bb->usergroup['canviewwolinvis'] != 1 &&
                    $user['uid'] != $this->user->uid
                ) {
                    $user['lastvisit'] = $this->lang->lastvisit_never;

                    if ($user['lastvisit']) {
                        // We have had at least some active time, hide it instead
                        $user['lastvisit'] = $this->lang->lastvisit_hidden;
                    }
                } else {
                    $user['lastvisit'] = $this->time->formatDate('relative', $user['lastactive']);
                }

                $user['regdate'] = $this->time->formatDate('relative', $user['regdate']);
                $user['postnum'] = $this->parser->formatNumber($user['postnum']);
                $user['threadnum'] = $this->parser->formatNumber($user['threadnum']);
                $users[] = [
                    'alt_bg' => $alt_bg,
                    'groupimage' => $usergroup['groupimage'],
                    'user' => $user
                ];
            }
            $this->view->offsetSet('users', $users);

            $this->plugins->runHooks('memberlist_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/memberlist.html.twig');
        }
    }
}
