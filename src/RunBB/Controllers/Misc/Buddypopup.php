<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Buddypopup extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        $this->plugins->runHooks('misc_buddypopup_start');

        if ($this->user->uid == 0) {
            return $this->bb->error_no_permission();
        }

        if (isset($this->bb->input['removebuddy']) && $this->bb->verify_post_check($this->bb->input['my_post_key'])) {
            $buddies = $this->user->buddylist;
            $namesarray = explode(',', $buddies);
            $this->bb->input['removebuddy'] = $this->bb->getInput('removebuddy', 0);
            if (is_array($namesarray)) {
                foreach ($namesarray as $key => $buddyid) {
                    if ($buddyid == $this->bb->input['removebuddy']) {
                        unset($namesarray[$key]);
                    }
                }
                $buddylist = implode(',', $namesarray);
                $this->db->update_query('users', ['buddylist' => $buddylist], "uid='" . $this->user->uid . "'");
                $this->user->buddylist = $buddylist;
            }
        }

        // Load Buddies
        $buddies = '';
        if ($this->user->buddylist != '') {
            $buddys = ['online' => [], 'offline' => []];
            $timecut = TIME_NOW - $this->bb->settings['wolcutoff'];

            $query = $this->db->simple_select('users', '*', "uid IN ({$this->user->buddylist})", ['order_by' => 'lastactive']);

            while ($buddy = $this->db->fetch_array($query)) {
                $buddy_name = $this->user->format_name($buddy['username'], $buddy['usergroup'], $buddy['displaygroup']);
                $profile_link = $this->user->build_profile_link(
                    $buddy_name,
                    $buddy['uid'],
                    '_blank',
                    'if(window.opener) { window.opener.location = this.href; return false; }'
                );

                $send_pm = '';
                if ($this->user->receivepms != 0 &&
                    $buddy['receivepms'] != 0 &&
                    $this->bb->groupscache[$buddy['usergroup']]['canusepms'] != 0
                ) {
                    $send_pm = "<a href=\"{$this->bb->settings['bburl']}/private/send?uid={$buddy['uid']}\" target=\"_blank\" onclick=\"window.opener.location.href=this.href; return false;\">{$this->lang->pm_buddy}</a>";
                }

                if ($buddy['lastactive']) {
                    $last_active = $this->lang->sprintf($this->lang->last_active, $this->time->formatDate('relative', $buddy['lastactive']));
                } else {
                    $last_active = $this->lang->sprintf($this->lang->last_active, $this->lang->never);
                }

                $buddy['avatar'] = $this->user->format_avatar($buddy['avatar'], $buddy['avatardimensions'], '44x44');

                if ($buddy['lastactive'] > $timecut &&
                    ($buddy['invisible'] == 0 || $this->user->usergroup == 4) &&
                    $buddy['lastvisit'] != $buddy['lastactive']
                ) {
                    $buddys['online'][] = [
                        'boffline_alt' => alt_trow(),
                        'buddy' => $buddy,
                        'profile_link' => $profile_link,
                        'last_active' => $last_active,
                        'send_pm' => $send_pm
                    ];
                } else {
                    $buddys['offline'][] = [
                        'boffline_alt' => alt_trow(),
                        'buddy' => $buddy,
                        'profile_link' => $profile_link,
                        'last_active' => $last_active,
                        'send_pm' => $send_pm
                    ];
                }
            }

            if (empty($buddys['online'])) {
                $buddys['online'] = $this->lang->online_none;
            }

            if (empty($buddys['offline'])) {
                $buddys['offline'] = $this->lang->offline_none;
            }
        } else {
            // No buddies? :(
            $buddys['error'] = $this->lang->no_buddies;
        }

        $this->view->offsetSet('buddys', $buddys);

        $this->plugins->runHooks('misc_buddypopup_end');

        return $this->view->render($response, '@forum/Misc/buddypopup.html.twig');
    }
}
