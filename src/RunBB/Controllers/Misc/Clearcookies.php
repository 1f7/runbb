<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Clearcookies extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('misc_clearcookies');

        $remove_cookies = [
            'runbbuser',
            'mybb[announcements]',
            'mybb[lastvisit]',
            'mybb[lastactive]',
            'collapsed',
            'mybb[forumread]',
            'mybb[threadsread]',
            'mybbadmin',
            'mybblang',
            'mybbtheme',
            'multiquote',
            'mybb[readallforums]',
            'coppauser',
            'coppadob',
            'mybb[referrer]'
        ];

        foreach ($remove_cookies as $name) {
            $this->bb->my_unsetcookie($name);
        }
        $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_cookiescleared);
    }
}
