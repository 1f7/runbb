<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Help extends AbstractController
{
    private function init()
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');
    }

    public function index(Request $request, Response $response)
    {
        $this->init();

        $this->lang->load('helpdocs');
        $this->lang->load('helpsections');
        $this->lang->load('customhelpdocs');
        $this->lang->load('customhelpsections');

        $hid = $this->bb->getInput('hid', 0);
        $this->bb->add_breadcrumb($this->lang->nav_helpdocs, $this->bb->settings['bburl'] . '/misc/help');

        if ($hid) {
            $query = $this->db->query('
			SELECT h.*, s.enabled AS section
			FROM ' . TABLE_PREFIX . 'helpdocs h
			LEFT JOIN ' . TABLE_PREFIX . "helpsections s ON (s.sid=h.sid)
			WHERE h.hid='{$hid}'
		");

            $helpdoc = $this->db->fetch_array($query);
            if ($helpdoc['section'] != 0 && $helpdoc['enabled'] != 0) {
                $this->plugins->runHooks('misc_help_helpdoc_start');

                // If we have incoming search terms to highlight - get it done (only if not using translation).
                if (!empty($this->bb->input['highlight']) && $helpdoc['usetranslation'] != 1) {
                    $highlight = $this->bb->input['highlight'];
                    $helpdoc['name'] = $this->parser->highlight_message($helpdoc['name'], $highlight);
                    $helpdoc['document'] = $this->parser->highlight_message($helpdoc['document'], $highlight);
                }

                if ($helpdoc['usetranslation'] == 1) {
                    $langnamevar = 'd' . $helpdoc['hid'] . '_name';
                    $langdescvar = 'd' . $helpdoc['hid'] . '_desc';
                    $langdocvar = 'd' . $helpdoc['hid'] . '_document';
                    if (isset($this->lang->$langnamevar)) {
                        $helpdoc['name'] = $this->lang->$langnamevar;
                    }
                    if (isset($this->lang->$langdescvar)) {
                        $helpdoc['description'] = $this->lang->$langdescvar;
                    }
                    if (isset($this->lang->$langdocvar)) {
                        $helpdoc['document'] = $this->lang->$langdocvar;
                    }
                }

                if ($helpdoc['hid'] == 3) {
                    $helpdoc['document'] = $this->lang->sprintf($helpdoc['document'], $this->bb->post_code);
                }

                $this->bb->add_breadcrumb($helpdoc['name']);
                $this->view->offsetSet('helpdoc', $helpdoc);

                $this->plugins->runHooks('misc_help_helpdoc_end');

                $this->bb->output_page();
                $this->view->render($response, '@forum/Misc/helpdoc.html.twig');
            } else {
                $this->bb->error($this->lang->error_invalidhelpdoc);
            }
        } else {
            $this->plugins->runHooks('misc_help_section_start');

            $query = $this->db->simple_select('helpdocs', '*', '', ['order_by' => 'sid, disporder']);
            while ($helpdoc = $this->db->fetch_array($query)) {
                $helpdocs[$helpdoc['sid']][$helpdoc['disporder']][$helpdoc['hid']] = $helpdoc;
            }
            unset($helpdoc);
            $sections = [];
            $query = $this->db->simple_select('helpsections', '*', 'enabled != 0', ['order_by' => 'disporder']);
            while ($section = $this->db->fetch_array($query)) {
                if ($section['usetranslation'] == 1) {
                    $langnamevar = 's' . $section['sid'] . '_name';
                    $langdescvar = 's' . $section['sid'] . '_desc';
                    if (isset($this->lang->$langnamevar)) {
                        $section['name'] = $this->lang->$langnamevar;
                    }
                    if (isset($this->lang->$langdescvar)) {
                        $section['description'] = $this->lang->$langdescvar;
                    }
                }
                if (is_array($helpdocs[$section['sid']])) {
                    $helpbits = [];
                    foreach ($helpdocs[$section['sid']] as $key => $bit) {
                        foreach ($bit as $key => $helpdoc) {
                            if ($helpdoc['enabled'] != 0) {
                                if ($helpdoc['usetranslation'] == 1) {
                                    $langnamevar = 'd' . $helpdoc['hid'] . '_name';
                                    $langdescvar = 'd' . $helpdoc['hid'] . '_desc';
                                    if (isset($this->lang->$langnamevar)) {
                                        $helpdoc['name'] = $this->lang->$langnamevar;
                                    }
                                    if (isset($this->lang->$langdescvar)) {
                                        $helpdoc['description'] = $this->lang->$langdescvar;
                                    }
                                }
                                $helpbits[] = [
                                    'altbg' => alt_trow(),
                                    'helpdoc' => $helpdoc
                                ];
                            }
                        }
                        $expdisplay = '';
                        $sname = 'sid_' . $section['sid'] . '_c';
                        if (isset($collapsed[$sname]) && $collapsed[$sname] == 'display: show;') {
                            $expcolimage = 'collapse_collapsed.png';
                            $expdisplay = 'display: none;';
                            $expthead = ' thead_collapsed';
                        } else {
                            $expcolimage = 'collapse.png';
                            $expthead = '';
                        }
                    }
                    $sections[] = [
                        'helpbits' => $helpbits,
                        'expthead' => $expthead,
                        'expcolimage' => $expcolimage,
                        'section' => $section,
                        'expdisplay' => $expdisplay
                    ];
                }
            }
            $this->view->offsetSet('sections', $sections);

            $this->plugins->runHooks('misc_help_section_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/Misc/help.html.twig');
        }
    }

    public function doHelpsearch(Request $request, Response $response)
    {
        $this->init();
        $this->lang->load('search');

        $this->plugins->runHooks('misc_do_helpsearch_start');

        if ($this->bb->settings['helpsearch'] != 1) {
            $this->bb->error($this->lang->error_helpsearchdisabled);
        }

        // Check if search flood checking is enabled and user is not admin
        if ($this->bb->settings['searchfloodtime'] > 0 && $this->bb->usergroup['cancp'] != 1) {
            // Fetch the time this user last searched
            $timecut = TIME_NOW - $this->bb->settings['searchfloodtime'];
            $query = $this->db->simple_select(
                'searchlog',
                '*',
                "uid='{$this->user->uid}' AND dateline > '$timecut'",
                ['order_by' => 'dateline', 'order_dir' => 'DESC']
            );
            $last_search = $this->db->fetch_array($query);
            // Users last search was within the flood time, show the error
            if ($last_search['sid']) {
                $remaining_time = $this->bb->settings['searchfloodtime'] - (TIME_NOW - $last_search['dateline']);
                if ($remaining_time == 1) {
                    $this->lang->error_searchflooding = $this->lang->sprintf(
                        $this->lang->error_searchflooding_1,
                        $this->bb->settings['searchfloodtime']
                    );
                } else {
                    $this->lang->error_searchflooding = $this->lang->sprintf(
                        $this->lang->error_searchflooding,
                        $this->bb->settings['searchfloodtime'],
                        $remaining_time
                    );
                }
                $this->bb->error($this->lang->error_searchflooding);
            }
        }

        if ($this->bb->getInput('name', 0) != 1 && $this->bb->getInput('document', 0) != 1) {
            $this->bb->error($this->lang->error_nosearchresults);
        }

        if ($this->bb->getInput('document', 0) == 1) {
            $resulttype = 'helpdoc';
        } else {
            $resulttype = 'helpname';
        }

        $search_data = [
            'keywords' => $this->bb->getInput('keywords', ''),
            'name' => $this->bb->getInput('name', 0),
            'document' => $this->bb->getInput('document', 0),
        ];

        if ($this->db->can_search == true) {
            $search_results = $this->search->helpdocument_perform_search_mysql($search_data);
            if ($search_results === 'exit') {
                return;
            }
        } else {
            $this->bb->error($this->lang->error_no_search_support);
        }
        $sid = md5(uniqid(microtime(), true));
        $searcharray = [
            'sid' => $this->db->escape_string($sid),
            'uid' => $this->user->uid,
            'dateline' => TIME_NOW,
            'ipaddress' => $this->session->ipaddress,
            'threads' => '',
            'posts' => '',
            'resulttype' => $resulttype,
            'querycache' => $search_results['querycache'],
            'keywords' => $this->db->escape_string($this->bb->getInput('keywords', '', true)),
        ];
        $this->plugins->runHooks('misc_do_helpsearch_process');

        $this->db->insert_query('searchlog', $searcharray);

        $this->plugins->runHooks('misc_do_helpsearch_end');
        $this->bb->redirect($this->bb->settings['bburl'] . '/misc/helpresults?sid=' . $sid, $this->lang->redirect_searchresults);
    }
}
