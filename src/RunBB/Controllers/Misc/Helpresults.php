<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Helpresults extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        if ($this->bb->settings['helpsearch'] != 1) {
            $this->bb->error($this->lang->error_helpsearchdisabled);
        }

        $sid = $this->bb->getInput('sid', '');
        $query = $this->db->simple_select(
            'searchlog',
            '*',
            "sid='" . $this->db->escape_string($sid) . "' AND uid='{$this->user->uid}'"
        );
        $search = $this->db->fetch_array($query);

        if (!$search) {
            $this->bb->error($this->lang->error_invalidsearch);
        }

        $this->plugins->runHooks('misc_helpresults_start');

        $this->bb->add_breadcrumb($this->lang->nav_helpdocs, $this->bb->settings['bburl'] . '/misc/help');
        $this->bb->add_breadcrumb($this->lang->search_results, $this->bb->settings['bburl'] . '/misc/helpresults?sid=' . $sid);

        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        // Work out pagination, which page we're at, as well as the limits.
        $perpage = $this->bb->settings['threadsperpage'];
        $page = $this->bb->getInput('page', 0);
        if ($page > 0) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }
//    $end = $start + $perpage;
//    $lower = $start+1;
//    $upper = $end;

        // Work out if we have terms to highlight
        $highlight = '';
        if ($search['keywords']) {
            $highlight = '&highlight=' . urlencode($search['keywords']);
        }

        // Do Multi Pages
        $query = $this->db->simple_select(
            'helpdocs',
            'COUNT(*) AS total',
            'hid IN(' . $this->db->escape_string($search['querycache']) . ')'
        );
        $helpcount = $this->db->fetch_array($query);

//    if($upper > $helpcount)
//    {
//      $upper = $helpcount;
//    }
        //$multipage = multipage($helpcount['total'], $perpage, $page, $this->bb->settings['bburl']."/misc/helpresults?sid='".htmlspecialchars_uni($this->bb->getInput('sid'))."'");
        $helpdoclist = [];

        $query = $this->db->query('
		SELECT h.*, s.enabled
		FROM ' . TABLE_PREFIX . 'helpdocs h
		LEFT JOIN ' . TABLE_PREFIX . 'helpsections s ON (s.sid=h.sid)
		WHERE h.hid IN(' . $this->db->escape_string($search['querycache']) . ") AND h.enabled='1' AND s.enabled='1'
		LIMIT {$start}, {$perpage}
	");
        while ($helpdoc = $this->db->fetch_array($query)) {
            $bgcolor = alt_trow();

            if (my_strlen($helpdoc['name']) > 50) {
                $helpdoc['name'] = htmlspecialchars_uni(my_substr($helpdoc['name'], 0, 50) . '...');
            } else {
                $helpdoc['name'] = htmlspecialchars_uni($helpdoc['name']);
            }

            $parser_options = [
                'allow_html' => 1,
                'allow_mycode' => 0,
                'allow_smilies' => 0,
                'allow_imgcode' => 0,
                'filter_badwords' => 1
            ];
            $helpdoc['helpdoc'] = strip_tags($this->parser->parse_message($helpdoc['document'], $parser_options));

            if (my_strlen($helpdoc['helpdoc']) > 350) {
                $prev = my_substr($helpdoc['helpdoc'], 0, 350) . '...';
            } else {
                $prev = $helpdoc['helpdoc'];
            }
            $helpdoclist[] = [
                'bgcolor' => $bgcolor,
                'helpdoc' => $helpdoc,
                'highlight' => $highlight,
                'prev' => $prev
            ];

            $this->plugins->runHooks('misc_helpresults_bit');
        }
        $this->view->offsetSet('helpdoclist', $helpdoclist);

        $this->plugins->runHooks('misc_helpresults_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Misc/helpresults.html.twig');
    }
}
