<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Imcenter extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        $this->bb->input['imtype'] = $this->bb->getInput('imtype', '');
        if ($this->bb->input['imtype'] !== 'aim' &&
            $this->bb->input['imtype'] !== 'skype' &&
            $this->bb->input['imtype'] !== 'yahoo') {
            $this->bb->error($this->lang->error_invalidimtype);
        }
        $uid = $this->bb->getInput('uid', 0);
        $user = $this->user->get_user($uid);

        if (!$user) {
            $this->bb->error($this->lang->error_invaliduser);
        }

        if (!$user->{$this->bb->input['imtype']}) {
            $this->bb->error($this->lang->error_invalidimtype);
        }

        $settingkey = 'allow'.$this->bb->input['imtype'].'field';
        if (!$this->user->is_member($this->bb->settings[$settingkey], $user)) {
            return $this->bb->error_no_permission();
        }

        $user->skype = htmlspecialchars_uni($user->skype);
        $user->yahoo = htmlspecialchars_uni($user->yahoo);
        $user->aim = htmlspecialchars_uni($user->aim);

        $this->lang->chat_on_skype = $this->lang->sprintf($this->lang->chat_on_skype, $user->username);
        $this->lang->call_on_skype = $this->lang->sprintf($this->lang->call_on_skype, $user->username);
        $this->lang->send_y_message = $this->lang->sprintf($this->lang->send_y_message, $user->username);
        $this->lang->view_y_profile = $this->lang->sprintf($this->lang->view_y_profile, $user->username);

        $this->view->offsetSet('user', $user);

        switch ($this->bb->input['imtype']) {
            case 'aim':
                return $this->view->render($response, '@forum/Misc/imcenter_aim.html.twig');
                break;
            case 'skype':
                return $this->view->render($response, '@forum/Misc/imcenter_skype.html.twig');
                break;
            case 'yahoo':
                return $this->view->render($response, '@forum/Misc/imcenter_yahoo.html.twig');
                break;
        }
    }
}
