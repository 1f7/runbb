<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Markread extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        if ($this->user->uid && $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''), true) !== true) {
            // Protect our user's unread forums from CSRF
            $this->bb->error($this->lang->invalid_post_code);
        }

        if (isset($this->bb->input['fid'])) {
            $validforum = $this->forum->get_forum($this->bb->input['fid']);
            if (!$validforum) {
                if (!isset($this->bb->input['ajax'])) {
                    $this->bb->error($this->lang->error_invalidforum);
                } else {
                    echo 0;
                    exit;
                }
            }

            $this->indicator->mark_forum_read($this->bb->input['fid']);

            $this->plugins->runHooks('misc_markread_forum');

            if (!isset($this->bb->input['ajax'])) {
                $this->bb->redirect($this->forum->get_forum_link($this->bb->input['fid']), $this->lang->redirect_markforumread);
            } else {
                echo 1;
                exit;
            }
        } else {
            $this->plugins->runHooks('misc_markread_end');
            $this->indicator->mark_all_forums_read();
            $this->bb->redirect($this->bb->settings['bburl'], $this->lang->redirect_markforumsread);
        }
    }
}
