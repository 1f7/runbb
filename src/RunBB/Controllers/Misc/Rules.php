<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Rules extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        $this->bb->current_page = 'misc';

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        if (isset($this->bb->input['fid'])) {
            $this->plugins->runHooks('misc_rules_start');

            $fid = $this->bb->input['fid'];

            $forum = $this->forum->get_forum($fid);
            if (!$forum || $forum['type'] != 'f' || $forum['rules'] == '') {
                $this->bb->error($this->lang->error_invalidforum);
            }

            $forumpermissions = $this->forum->forum_permissions($forum['fid']);
            if ($forumpermissions['canview'] != 1) {
                return $this->bb->error_no_permission();
            }

            if (!$forum['rulestitle']) {
                $forum['rulestitle'] = $this->lang->sprintf($this->lang->forum_rules, $forum['name']);
            }

            $parser_options = [
                'allow_html' => 1,
                'allow_mycode' => 1,
                'allow_smilies' => 1,
                'allow_imgcode' => 1,
                'filter_badwords' => 1
            ];

            $forum['rules'] = $this->parser->parse_message($forum['rules'], $parser_options);

            // Make navigation
            $this->bb->build_forum_breadcrumb($this->bb->input['fid']);
            $this->bb->add_breadcrumb($forum['rulestitle']);

            $this->plugins->runHooks('misc_rules_end');

            $this->view->offsetSet('forum', $forum);

            $this->bb->output_page();
            return $this->view->render($response, '@forum/Misc/rules.html.twig');
        }
    }
}
