<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Smilies extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        $smilies = [];
        if (!empty($this->bb->input['popup']) && !empty($this->bb->input['editor'])) { // make small popup list of smilies
//            $editor = preg_replace('#([^a-zA-Z0-9_-]+)#', '', $this->bb->getInput('editor'));
            $smilies_cache = $this->cache->read('smilies');
            if (is_array($smilies_cache)) {
                foreach ($smilies_cache as $smilie) {
                    $smilie['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $smilie['image']);
                    $smilie['image'] = htmlspecialchars_uni($this->themes->get_asset_url($smilie['image']));
                    $smilie['name'] = htmlspecialchars_uni($smilie['name']);

                    // Only show the first text to replace in the box
                    $temp = explode("\n", $smilie['find']); // use temporary variable for php 5.3 compatibility
                    $smilie['find'] = $temp[0];

                    $smilie['find'] = htmlspecialchars_uni($smilie['find']);
                    $smilie_insert = str_replace(['\\', "'"], ['\\\\', "\'"], $smilie['find']);

                    $smilies[] = [
                        'class' => alt_trow(),
                        'smilie' => $smilie,
                        'smilie_insert' => $smilie_insert
                    ];
                }
            }

            $this->view->offsetSet('smilies', $smilies);

            $this->bb->output_page();
            $this->view->render($response, '@forum/Misc/smilies_popup.html.twig');
        } else {
            $this->bb->add_breadcrumb($this->lang->nav_smilies);
            $smilies_cache = $this->cache->read('smilies');
            if (is_array($smilies_cache)) {
                foreach ($smilies_cache as $smilie) {
                    $smilie['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $smilie['image']);
                    $smilie['image'] = htmlspecialchars_uni($this->themes->get_asset_url($smilie['image']));
                    $smilie['name'] = htmlspecialchars_uni($smilie['name']);
                    $smilie['find'] = nl2br(htmlspecialchars_uni($smilie['find']));

                    $smilies[] = [
                        'class' => alt_trow(),
                        'smilie' => $smilie
                    ];
                }
            }
            $this->view->offsetSet('smilies', $smilies);

            $this->bb->output_page();
            $this->view->render($response, '@forum/Misc/smilies.html.twig');
        }
    }
}
