<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Syndication extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('misc');

        $this->plugins->runHooks('misc_start');

        $this->plugins->runHooks('misc_syndication_start');

        $fid = $this->bb->getInput('fid', 0);
        $version = $this->bb->getInput('version', '');
        $limit = $this->bb->getInput('limit', 0);
        $forums = $this->bb->getInput('forums', []);
        if (empty($limit) || $limit > 35) {
            $limit = 15;
        }

        $add = false;
        $this->bb->add_breadcrumb($this->lang->nav_syndication);
        $this->unexp = array_merge(
            $this->forum->get_unviewable_forums(),
            $this->forum->get_inactive_forums()
        );

        $url = '';
        if (is_array($forums)) {
            foreach ($this->unexp as $fid) {
                $unview[$fid] = true;
            }

            $syndicate = '';
            $comma = '';
            $all = false;
            foreach ($forums as $fid) {
                if ($fid == 'all') {
                    $all = true;
                    break;
                } elseif (is_numeric($fid)) {
                    if (!isset($unview[$fid])) {
                        $syndicate .= $comma . $fid;
                        $comma = ',';
                        $flist[$fid] = true;
                    }
                }
            }

            $url = $this->bb->settings['homeurl'] . $this->bb->settings['bburl'] . '/syndication';
            if (!$all) {
                $url .= "?fid=$syndicate";
                $add = true;
            }

            // If the version is not RSS2.0, set the type to Atom1.0.
            if ($version != 'rss2.0') {
                if (!$add) {
                    $url .= '?';
                } else {
                    $url .= '&';
                }
                $url .= 'type=atom1.0';
                $add = true;
            }
            if ((int)$limit > 0) {
                if ($limit > 100) {
                    $limit = 100;
                }
                if (!$add) {
                    $url .= '?';
                } else {
                    $url .= '&';
                }
                if (is_numeric($limit)) {
                    $url .= "limit=$limit";
                }
            }
        }

        unset($GLOBALS['forumcache']);
        $this->view->offsetSet('url', $url);
        $this->view->offsetSet('limit', $limit);

        // If there is no version in the input, check the default (RSS2.0).
        if ($version == 'atom1.0') {
            $atom1check = 'checked="checked"';
            $rss2check = '';
        } else {
            $atom1check = '';
            $rss2check = 'checked="checked"';
        }
        $this->view->offsetSet('atom1check', $atom1check);
        $this->view->offsetSet('rss2check', $rss2check);
        //$forumselect = $this->makesyndicateforums();
        $this->view->offsetSet('forumselect', $this->makesyndicateforums());

        $this->plugins->runHooks('misc_syndication_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Misc/syndication.html.twig');
    }

    /**
     * Build a list of forums for RSS multiselect.
     *
     * @param int $pid Parent forum ID.
     * @param string $selitem deprecated
     * @param boolean $addselect Whether to add selected attribute or not.
     * @param string $depth HTML for the depth of the forum.
     * @return string HTML of the list of forums for CSS.
     */
    private function makesyndicateforums($pid = 0, $selitem = '', $addselect = true, $depth = '')
    {
        static $forumcache;

        $pid = (int)$pid;
        $forumlist = '';

        if (!is_array($forumcache)) {
            // Get Forums
//            $query = $this->db->simple_select('forums',
//                '*',
//                "linkto = '' AND active!=0",
//                array('order_by' => 'pid, disporder'));
            $f = \RunBB\Models\Forum::where([
                ['linkto', '=', ''],
                ['active', '!=', 0]
            ])
                ->orderBy('pid')->orderBy('disporder')
                ->get()
                ->toArray();
//            while ($forum = $this->db->fetch_array($query)) {
            foreach ($f as $forum) {
                $forumcache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
            }
        }

        if (empty($this->bb->permissioncache)) {
            $this->bb->permissioncache = $this->forum->forum_permissions();
        }
        $forumlistbits = '';
        if (is_array($forumcache[$pid])) {
            foreach ($forumcache[$pid] as $key => $main) {
                foreach ($main as $key => $forum) {
                    $perms = $this->bb->permissioncache[$forum['fid']];
                    if ($perms['canview'] == 1 || $this->bb->settings['hideprivateforums'] == 0) {
                        $optionselected = '';
                        if (isset($flist[$forum['fid']])) {
                            $optionselected = 'selected="selected"';
                            $selecteddone = '1';
                        }

                        if ($forum['password'] == '' && !in_array($forum['fid'], $this->unexp) ||
                            $forum['password'] && isset($this->bb->cookies['forumpass'][$forum['fid']]) &&
                            $this->bb->cookies['forumpass'][$forum['fid']] === md5($this->user->uid . $forum['password'])
                        ) {
                            $forumlistbits .= "<option value=\"{$forum['fid']}\" $optionselected>$depth {$forum['name']}</option>\n";
                        }

                        if (!empty($forumcache[$forum['fid']])) {
                            $newdepth = $depth . '&nbsp;&nbsp;&nbsp;&nbsp;';
                            $forumlistbits .= $this->makesyndicateforums($forum['fid'], '', 0, $newdepth);
                        }
                    }
                }
            }
        }

        if ($addselect) {
            $addsel = '';
            if (empty($selecteddone)) {
                $addsel = ' selected="selected"';
            }
            $forumlist = "
              <select name=\"forums[]\" size=\"10\" multiple=\"multiple\">
                <option value=\"all\" {$addsel}>{$this->lang->syndicate_all_forums}</option>
                <option value=\"all\">----------------------</option>
                {$forumlistbits}
              </select>";
        }

        return $forumlist;
    }
}
