<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Controllers\Misc;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class WhoPosted extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');
        $this->bb->current_page = 'misc';
        // Load global language phrases
        $this->lang->load('misc');
        $this->plugins->runHooks('misc_start');


        $numposts = 0;

        $tid = $this->bb->getInput('tid', 0);
        $this->view->offsetSet('tid', $tid);

        $thread = $this->thread->get_thread($tid);

        // Make sure we are looking at a real thread here.
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        // Make sure we are looking at a real thread here.
        if (($thread['visible'] == -1 &&
                !$this->user->is_moderator($thread['fid'], 'canviewdeleted')) ||
            ($thread['visible'] == 0 &&
                !$this->user->is_moderator($thread['fid'], 'canviewunapprove')) ||
            $thread['visible'] > 1) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        if ($this->user->is_moderator($thread['fid'], 'canviewdeleted') ||
            $this->user->is_moderator($thread['fid'], 'canviewunapprove')) {
            if ($this->user->is_moderator($thread['fid'], 'canviewunapprove') &&
                !$this->user->is_moderator($thread['fid'], 'canviewdeleted')) {
                $show_posts = 'p.visible IN (0,1)';
            } elseif ($this->user->is_moderator($thread['fid'], 'canviewdeleted') &&
                !$this->user->is_moderator($thread['fid'], 'canviewunapprove')) {
                $show_posts = 'p.visible IN (-1,1)';
            } else {
                $show_posts = 'p.visible IN (-1,0,1)';
            }
        } else {
            $show_posts = 'p.visible = 1';
        }

        // Does the thread belong to a valid forum?
        $forum = $this->forum->get_forum($thread['fid']);
        if (!$forum || $forum['type'] != 'f') {
            $this->bb->error($this->lang->error_invalidforum);
        }

        // Does the user have permission to view this thread?
        $forumpermissions = $this->forum->forum_permissions($forum['fid']);

        if ($forumpermissions['canview'] == 0 ||
            $forumpermissions['canviewthreads'] == 0 ||
            (isset($forumpermissions['canonlyviewownthreads']) &&
                $forumpermissions['canonlyviewownthreads'] != 0 &&
                $thread['uid'] != $this->user->uid)) {
            return $this->bb->error_no_permission();
        }

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($forum['fid']);

        if ($this->bb->getInput('sort', '') != 'username') {
            $sortsql = ' ORDER BY posts DESC';
        } else {
            $sortsql = ' ORDER BY p.username ASC';
        }
        $whoposted = [];
        $query = $this->db->query('
		SELECT COUNT(p.pid) AS posts, p.username AS postusername, u.uid, u.username, u.usergroup, u.displaygroup
		FROM '.TABLE_PREFIX.'posts p
		LEFT JOIN '.TABLE_PREFIX."users u ON (u.uid=p.uid)
		WHERE tid='".$tid."' AND $show_posts
		GROUP BY u.uid, p.username, u.uid, u.username, u.usergroup, u.displaygroup
		".$sortsql);
        while ($poster = $this->db->fetch_array($query)) {
            if ($poster['username'] == '') {
                $poster['username'] = $poster['postusername'];
            }
            $poster_name = $this->user->format_name($poster['username'], $poster['usergroup'], $poster['displaygroup']);
            $onclick = '';
            if ($poster['uid']) {
                $onclick = "opener.location.href='".get_profile_link($poster['uid'])."'; return false;";
            }
            $numposts += $poster['posts'];
            $whoposted[] = [
                'altbg' => alt_trow(),
                'profile_link' => $this->user->build_profile_link($poster_name, $poster['uid'], '_blank', $onclick),
                'posts' => $poster['posts']
            ];
        }
        $this->view->offsetSet('whoposted', $whoposted);

        $this->view->offsetSet('numposts', $this->parser->formatNumber($numposts));
        $poster['posts'] = $this->parser->formatNumber($poster['posts']);

        return $this->view->render($response, '@forum/Misc/whoposted.html.twig');
    }
}
