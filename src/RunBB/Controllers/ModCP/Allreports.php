<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Allreports extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canmanagereportedcontent'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->lang->load('report');

        $this->bb->add_breadcrumb($this->lang->report_center, $this->bb->settings['bburl'] . '/modcp/reports');
        $this->bb->add_breadcrumb($this->lang->all_reports, $this->bb->settings['bburl'] . '/modcp/allreports');

        if (!$this->bb->settings['threadsperpage']) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        // Figure out if we need to display multiple pages.
        $perpage = $this->bb->settings['threadsperpage'];
        if ($this->bb->getInput('page', '') !== 'last') {
            $page = $this->bb->getInput('page', 0);
        }

        if ($this->bb->usergroup['cancp'] || $this->bb->usergroup['issupermod']) {
            $query = $this->db->simple_select('reportedcontent', 'COUNT(rid) AS count');
            $report_count = $this->db->fetch_field($query, 'count');
        } else {
            $query = $this->db->simple_select('reportedcontent', 'id3', "type = 'post' OR type = ''");

            $report_count = 0;
            while ($fid = $this->db->fetch_field($query, 'id3')) {
                if ($this->user->is_moderator($fid, 'canmanagereportedposts')) {
                    ++$report_count;
                }
            }
            unset($fid);
        }

        if (isset($this->bb->input['rid'])) {
            $this->bb->input['rid'] = $this->bb->getInput('rid', 0);
            $query = $this->db->simple_select('reportedcontent', 'COUNT(rid) AS count', "rid <= '" . $this->bb->input['rid'] . "'");
            $result = $this->db->fetch_field($query, 'count');
            if (($result % $perpage) == 0) {
                $page = $result / $perpage;
            } else {
                $page = (int)$result / $perpage + 1;
            }
        }
        $postcount = (int)$report_count;
        $pages = $postcount / $perpage;
        $pages = ceil($pages);

        if ($this->bb->getInput('page', '') === 'last') {
            $page = $pages;
        }

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }

        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }
        $upper = $start + $perpage;

        $multipage = $this->pagination->multipage($postcount, $perpage, $page, $this->bb->settings['bburl'] . '/modcp/allreports');
        $this->view->offsetSet('multipage', $multipage);

        $this->plugins->runHooks('modcp_allreports_start');

        $query = $this->db->query('
		SELECT r.*, u.username, p.username AS postusername, up.uid AS postuid, t.subject AS threadsubject, prrep.username AS repusername, pr.username AS profileusername
		FROM ' . TABLE_PREFIX . 'reportedcontent r
		LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (r.id=p.pid)
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (p.tid=t.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (r.uid=u.uid)
		LEFT JOIN ' . TABLE_PREFIX . 'users up ON (p.uid=up.uid)
		LEFT JOIN ' . TABLE_PREFIX . 'users pr ON (pr.uid=r.id)
		LEFT JOIN ' . TABLE_PREFIX . "users prrep ON (prrep.uid=r.id2)
		{$this->wflist_reports}
		ORDER BY r.dateline DESC
		LIMIT {$start}, {$perpage}
	");

        $allreports = [];
        while ($report = $this->db->fetch_array($query)) {
            //$trow = alt_trow();
            if ($report['type'] == 'post') {
                $post = get_post_link($report['id']) . "#pid{$report['id']}";
                $user = $this->user->build_profile_link($report['postusername'], $report['postuid']);
                $report_data['content'] = $this->lang->sprintf($this->lang->report_info_post, $post, $user);

                $thread_link = get_thread_link($report['id2']);
                $thread_subject = htmlspecialchars_uni($report['threadsubject']);
                $report_data['content'] .= $this->lang->sprintf($this->lang->report_info_post_thread, $thread_link, $thread_subject);
            } elseif ($report['type'] == 'profile') {
                $user = $this->user->build_profile_link($report['profileusername'], $report['id']);
                $report_data['content'] = $this->lang->sprintf($this->lang->report_info_profile, $user);
            } elseif ($report['type'] == 'reputation') {
                $user = $this->user->build_profile_link($report['repusername'], $report['id2']);
                $reputation_link = $this->bb->settings['bburl'] . "/reputation?uid={$report['id3']}#rid{$report['id']}";
                $report_data['content'] = $this->lang->sprintf($this->lang->report_info_reputation, $reputation_link, $user);
            }

            // Report reason and comment
            $report_data['comment'] = $this->lang->na;
            $report_string = "report_reason_{$report['reason']}";

            $report['reporterlink'] = get_profile_link($report['uid']);
            if (!$report['username']) {
                $report['username'] = $this->lang->na_deleted;
                $report['reporterlink'] = $post;
            }

            if (isset($this->lang->$report_string)) {
                $report_data['comment'] = $this->lang->$report_string;
            } elseif (!empty($report['reason'])) {
                $report_data['comment'] = htmlspecialchars_uni($report['reason']);
            }

            $report_data['reports'] = $this->parser->formatNumber($report['reports']);
            $report_data['time'] = $this->time->formatDate('relative', $report['dateline']);

            $allreports[] = [
                'trow' => alt_trow(),
                'report_data' => $report_data,
                'report' => $report
            ];

            $this->plugins->runHooks('modcp_allreports_report');
        }

        $this->view->offsetSet('allreports', $allreports);

        $this->plugins->runHooks('modcp_allreports_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/allreports.html.twig');
    }
}
