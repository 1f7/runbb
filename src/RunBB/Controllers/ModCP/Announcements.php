<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Announcements extends Common
{
    private $ann_arry = [];

    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        if ($this->numannouncements == 0 && $this->bb->usergroup['issupermod'] != 1) {
            $this->bb->error($this->lang->you_cannot_manage_announcements);
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_announcements, $this->bb->settings['bburl'] . '/modcp/announcements');

        // Fetch announcements into their proper arrays
//        $query = $this->db->simple_select('announcements', 'aid, fid, subject, enddate');
        $this->announcements = $global_announcements = [];
        $an = \RunBB\Models\Announcement::get(['aid', 'fid', 'subject', 'enddate'])->toArray();

//        while ($announcement = $this->db->fetch_array($query)) {
        foreach ($an as $announcement) {
            if ($announcement['fid'] == -1) {
                $global_announcements[$announcement['aid']] = $announcement;
                continue;
            }
            $this->announcements[$announcement['fid']][$announcement['aid']] = $announcement;
        }

        $announcements_global = [];
        if ($this->bb->usergroup['issupermod'] == 1) {
            if ($global_announcements && $this->bb->usergroup['issupermod'] == 1) {
                // Get the global announcements
                foreach ($global_announcements as $aid => $announcement) {
                    //$trow = alt_trow();
                    if ((isset($announcement['startdate']) && $announcement['startdate'] > TIME_NOW) ||
                        ($announcement['enddate'] < TIME_NOW && $announcement['enddate'] != 0)
                    ) {
                        $icon = "<div class=\"subforumicon subforum_minioff\" title=\"{$this->lang->expired_announcement}\"></div>";
                    } else {
                        $icon = "<div class=\"subforumicon subforum_minion\" title=\"{$this->lang->active_announcement}\"></div>";
                    }
                    $announcements_global[] = [
                        'trow' => alt_trow(),
                        'icon' => $icon,
                        'aid' => $aid,
                        'subject' => htmlspecialchars_uni($announcement['subject'])
                    ];
                }
            }
            $this->view->offsetSet('announcements_global', $announcements_global);
        }

        $this->fetch_forum_announcements();

        $this->view->offsetSet('announcements_forum', $this->ann_arry);//$this->announcements_forum);

        $this->plugins->runHooks('modcp_announcements');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/announcements.html.twig');
    }

    /**
     * Fetch forums the moderator can manage announcements to
     *
     * @param int $pid (Optional) The parent forum ID
     * @param int $depth (Optional) The depth from parent forum the moderator can manage to
     */
    public function fetch_forum_announcements($pid = 0, $depth = 1)
    {
        static $forums_by_parent, $parent_forums;

        if (empty($this->bb->forum_cache)) {
            $this->forum->cache_forums();
        }
        if (!is_array($parent_forums) && $this->bb->usergroup['issupermod'] != 1) {
            // Get a list of parentforums to show for normal moderators
            $parent_forums = [];
            foreach ($this->moderated_forums as $mfid) {
                $parent_forums = array_merge($parent_forums, explode(',', $this->bb->forum_cache[$mfid]['parentlist']));
            }
        }

        if (!is_array($forums_by_parent)) {
            foreach ($this->bb->forum_cache as $forum) {
                $forums_by_parent[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
            }
        }

        if (!is_array($forums_by_parent[$pid])) {
            return;
        }
        $padding = 0;
        foreach ($forums_by_parent[$pid] as $children) {
            foreach ($children as $forum) {
                if ($forum['linkto'] || (is_array($this->unviewableforums) && in_array($forum['fid'], $this->unviewableforums))) {
                    continue;
                }

                if ($forum['active'] == 0 || !$this->user->is_moderator($forum['fid'], 'canmanageannouncements')) {
                    // Check if this forum is a parent of a moderated forum
                    if (is_array($parent_forums) && in_array($forum['fid'], $parent_forums)) {
                        // A child is moderated, so print out this forum's title.  RECURSE!
// no announce, forum name only
                        $this->ann_arry[] = [
                            'trow' => alt_trow(),
                            'padding' => $padding,
                            'name' => $forum['name'],
                            'fid' => 0,
                            'icon' => 0,
                            'aid' => 0,
                            'subject' => 0
                        ];
                    } else {
                        // No subforum is moderated by this mod, so safely continue
                        continue;
                    }
                } else {
                    // This forum is moderated by the user, so print out the forum's title, and its announcements
// no announce, forum name, add announce
                    $padding = 30 * ($depth - 1);
                    $this->ann_arry[] = [
                        'trow' => alt_trow(),
                        'padding' => $padding,
                        'name' => $forum['name'],
                        'fid' => (int)$forum['fid'],
                        'icon' => 0,
                        'aid' => 0,
                        'subject' => 0,
                    ];
                    //ev al("\$this->announcements_forum .= \"".$this->templates->get("modcp_announcements_forum")."\";");

                    if (isset($this->announcements[$forum['fid']])) {
                        foreach ($this->announcements[$forum['fid']] as $aid => $announcement) {
                            if ($announcement['enddate'] < TIME_NOW && $announcement['enddate'] != 0) {
                                $icon = "<div class=\"subforumicon subforum_minioff\" title=\"{$this->lang->expired_announcement}\"></div>";
                            } else {
                                $icon = "<div class=\"subforumicon subforum_minion\" title=\"{$this->lang->active_announcement}\"></div>";
                            }
// announce exist, edit, delete
                            $this->ann_arry[] = [
                                'trow' => alt_trow(),
                                'padding' => $padding,
                                'name' => 0,
                                'fid' => 0,
                                'icon' => $icon,
                                'aid' => $aid,
                                'subject' => htmlspecialchars_uni($announcement['subject']),
                            ];
                        }
                    }
                }
                // Build the list for any sub forums of this forum
                if (isset($forums_by_parent[$forum['fid']])) {
                    $this->fetch_forum_announcements($forum['fid'], $depth + 1);
                }
            }
        }
    }
}
