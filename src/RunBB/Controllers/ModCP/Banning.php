<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Banning extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canbanusers'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_banning, $this->bb->settings['bburl'] . '/modcp/banning');

        if (!$this->bb->settings['threadsperpage']) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        // Figure out if we need to display multiple pages.
        $perpage = $this->bb->settings['threadsperpage'];
        if ($this->bb->getInput('page', '') !== 'last') {
            $page = $this->bb->getInput('page', 0);
        }

        $query = $this->db->simple_select('banned', 'COUNT(uid) AS count');
        $banned_count = $this->db->fetch_field($query, 'count');

        $postcount = (int)$banned_count;
        $pages = $postcount / $perpage;
        $pages = ceil($pages);

        if ($this->bb->getInput('page', '') === 'last') {
            $page = $pages;
        }

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }

        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }
        $upper = $start + $perpage;

        $multipage = $this->pagination->multipage($postcount, $perpage, $page, $this->bb->settings['bburl'] . '/modcp/banning');
        $this->view->offsetSet('multipage', $multipage);

        $this->plugins->runHooks('modcp_banning_start');

        $query = $this->db->query('
		SELECT b.*, a.username AS adminuser, u.username
		FROM ' . TABLE_PREFIX . 'banned b
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (b.uid=u.uid)
		LEFT JOIN ' . TABLE_PREFIX . "users a ON (b.admin=a.uid)
		ORDER BY dateline DESC
		LIMIT {$start}, {$perpage}
	");

        // Get the banned users
        $bannedusers = [];
        while ($banned = $this->db->fetch_array($query)) {
            // Only show the edit & lift links if current user created ban, or is super mod/admin
            $show_edit_link = false;
            if ($this->user->uid == $banned['admin'] ||
                !$banned['adminuser'] ||
                $this->bb->usergroup['issupermod'] == 1 ||
                $this->bb->usergroup['cancp'] == 1
            ) {
                $show_edit_link = true;
//                $edit_link = "
//<br /><span class=\"smalltext\">
//<a href=\"{$this->bb->settings['bburl']}/modcp/banuser?uid={$banned['uid']}\">{$this->lang->edit_ban}</a> |
//<a href=\"{$this->bb->settings['bburl']}/modcp/liftban?uid={$banned['uid']}&amp;my_post_key={$this->bb->post_code}\">
//{$this->lang->lift_ban}</a></span>";
                //ev al('\$edit_link = \''.$this->templates->get('modcp_banning_edit').'\';');
            }

            $admin_profile = $this->user->build_profile_link($banned['adminuser'], $banned['admin']);
            if ($banned['reason']) {
                $banned['reason'] = htmlspecialchars_uni($this->parser->parse_badwords($banned['reason']));
            } else {
                $banned['reason'] = $this->lang->na;
            }

            if ($banned['lifted'] == 'perm' ||
                $banned['lifted'] == '' ||
                $banned['bantime'] == 'perm' ||
                $banned['bantime'] == '---') {
                $banlength = $this->lang->permanent;
                $timeremaining = $this->lang->na;
            } else {
                $banlength = $this->bantimes[$banned['bantime']];
                $remaining = $banned['lifted'] - TIME_NOW;

                $timeremaining = $this->time->niceTime($remaining, ['short' => 1, 'seconds' => false]);

                if ($remaining < 3600) {
                    $timeremaining = "<span style=\"color: red;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } elseif ($remaining < 86400) {
                    $timeremaining = "<span style=\"color: maroon;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } elseif ($remaining < 604800) {
                    $timeremaining = "<span style=\"color: green;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } else {
                    $timeremaining = "({$timeremaining} {$this->lang->ban_remaining})";
                }
            }
            $bannedusers[] = [
                'trow' => alt_trow(),
                'profile_link' => $this->user->build_profile_link($banned['username'], $banned['uid']),
//                'edit_link' => $edit_link,
                'reason' => $banned['reason'],
                'banlength' => $banlength,
                'timeremaining' => $timeremaining,
                'admin_profile' => $admin_profile,
                'show_edit_link' => $show_edit_link,
                'uid' => $banned['uid']
            ];
            //ev al('\$bannedusers .= \''.$this->templates->get('modcp_banning_ban').'\';');
        }
        $this->view->offsetSet('bannedusers', $bannedusers);

        $this->plugins->runHooks('modcp_banning');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/banning.html.twig');
    }
}
