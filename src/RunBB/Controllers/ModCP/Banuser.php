<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Banuser extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() == 'exit') {
                return;
            }
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_banning, $this->bb->settings['bburl'] . '/modcp/banning');

        if ($this->bb->usergroup['canbanusers'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->input['uid'] = $this->bb->getInput('uid', 0);
        if ($this->bb->input['uid']) {
            $this->bb->add_breadcrumb($this->lang->mcp_nav_ban_user);
        } else {
            $this->bb->add_breadcrumb($this->lang->mcp_nav_editing_ban);
        }

        $this->plugins->runHooks('modcp_banuser_start');

        $banuser_username = [];
        $banreason = '';

        // If incoming user ID, we are editing a ban
        $banned = [];
        if ($this->bb->input['uid']) {
            $query = $this->db->query('
			SELECT b.*, u.username, u.uid
			FROM ' . TABLE_PREFIX . 'banned b
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (b.uid=u.uid)
			WHERE b.uid='{$this->bb->input['uid']}'
		");
            $banned = $this->db->fetch_array($query);
            if ($banned['username']) {
                $username = htmlspecialchars_uni($banned['username']);
                $banreason = htmlspecialchars_uni($banned['reason']);
//                $uid = $this->bb->input['uid'];
//                $user = $this->user->get_user($banned['uid']);
                $this->lang->ban_user = $this->lang->edit_ban; // Swap over lang variables
                $banuser_username = [
                    'nameonly' => true,
                    'username' => $username
                ];
            }
        }

        // Permission to edit this ban?
        if (!empty($banned['uid']) &&
            $this->user->uid != $banned['admin'] &&
            $this->bb->usergroup['issupermod'] != 1 &&
            $this->bb->usergroup['cancp'] != 1
        ) {
            return $this->bb->error_no_permission();
        }

        // New ban!
        if (!$banuser_username) {
            if ($this->bb->input['uid']) {
                $user = $this->user->get_user($this->bb->input['uid']);
                $username = $user->username;
            } else {
                $username = htmlspecialchars_uni($this->bb->getInput('username', '', true));
            }
            $banuser_username = [
                'nameonly' => false,
                'username' => $username
            ];
        }
        $this->view->offsetSet('banuser_username', $banuser_username);

        // Coming back to this page from an error?
        $errors = '';
        if ($this->errors) {
            $errors = $this->bb->inline_error($this->errors);
            $banned = [
                'bantime' => $this->bb->getInput('liftafter', ''),
                'reason' => $this->bb->getInput('reason', '', true),
                'gid' => $this->bb->getInput('gid', 0)
            ];
            $banreason = htmlspecialchars_uni($this->bb->getInput('banreason', '', true));
        }
        $this->view->offsetSet('errors', $errors);
        $this->view->offsetSet('banreason', $banreason);

        // Generate the banned times dropdown
        $liftlist = [];
        foreach ($this->bantimes as $time => $title) {
            $selected = '';
            if (isset($banned['bantime']) && $banned['bantime'] == $time) {
                $selected = ' selected="selected"';
            }

            $thattime = '';
            if ($time != '---') {
                $dateline = TIME_NOW;
                if (isset($banned['dateline'])) {
                    $dateline = $banned['dateline'];
                }
                $thatime = $this->time->formatDate('D, jS M Y @ g:ia', $this->ban->ban_date2timestamp($time, $dateline));
                $thattime = " ({$thatime})";
            }
            $liftlist[] = [
                'time' => $time,
                'selected' => $selected,
                'title' => $title,
                'thattime' => $thattime
            ];
        }
        $this->view->offsetSet('liftlist', $liftlist);

        $bangroup_option = [];
        //$bangroups = '';
        $numgroups = $banned_group = 0;
        $this->bb->groupscache = $this->cache->read('usergroups');

        foreach ($this->bb->groupscache as $key => $group) {
            if ($group['isbannedgroup']) {
                $selected = '';
                if (isset($banned['gid']) && $banned['gid'] == $group['gid']) {
                    $selected = ' selected="selected"';
                }

                $group['title'] = htmlspecialchars_uni($group['title']);
                $bangroup_option[] = [
                    'group' => $group,
                    'selected' => $selected
                ];
                $banned_group = $group['gid'];
                ++$numgroups;
            }
        }
        $this->view->offsetSet('bangroup_option', $bangroup_option);
        $this->view->offsetSet('banned_group', $banned_group);

        if ($numgroups == 0) {
            $this->bb->error($this->lang->no_banned_group);
        }

        $this->view->offsetSet('banned', $banned);

        $this->plugins->runHooks('modcp_banuser_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/banuser.html.twig');
    }

    public function doBanuser(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canbanusers'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Editing an existing ban
        $existing_ban = false;
        if ($this->bb->getInput('uid', 0)) {
            // Get the users info from their uid
            $query = $this->db->query('
			SELECT b.*, u.uid, u.username, u.usergroup, u.additionalgroups, u.displaygroup
			FROM ' . TABLE_PREFIX . 'banned b
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (b.uid=u.uid)
			WHERE b.uid='{$this->bb->input['uid']}'
		");
            $user = $this->db->fetch_array($query);

            if ($user->uid) {
                $existing_ban = true;
            }

            // Permission to edit this ban?
            if ($existing_ban && $this->user->uid != $user->admin &&
                $this->bb->usergroup['issupermod'] != 1 && $this->bb->usergroup['cancp'] != 1
            ) {
                return $this->bb->error_no_permission();
            }
        }

        // Creating a new ban
        if (!$existing_ban) {
            // Get the users info from their Username
            $options = [
                'fields' => ['username', 'usergroup', 'additionalgroups', 'displaygroup']
            ];

            $user = $this->user->get_user_by_username($this->bb->input['username'], $options);

            if (!$user->uid) {
                $this->errors[] = $this->lang->invalid_username;
            }
        }

        if ($user->uid == $this->user->uid) {
            $this->errors[] = $this->lang->error_cannotbanself;
        }

        // Have permissions to ban this user?
        if (!$this->modcp->modcp_can_manage_user($user->uid)) {
            $this->errors[] = $this->lang->error_cannotbanuser;
        }

        // Check for an incoming reason
        if (empty($this->bb->input['banreason'])) {
            $this->errors[] = $this->lang->error_nobanreason;
        }

        // Check banned group
        $usergroups_cache = $this->cache->read('usergroups');
        $usergroup = $usergroups_cache[$this->bb->getInput('usergroup', 0)];
        $query = $this->db->simple_select('usergroups', 'gid', "isbannedgroup=1 AND gid='" . $this->bb->getInput('usergroup', 0) . "'");

        if (empty($usergroup['gid']) || empty($usergroup['isbannedgroup'])) {
            $this->errors[] = $this->lang->error_nobangroup;
        }

        // If this is a new ban, we check the user isn't already part of a banned group
        if (!$existing_ban && $user->uid) {
            $query = $this->db->simple_select('banned', 'uid', "uid='{$user->uid}'");
            if ($this->db->fetch_field($query, 'uid')) {
                $this->errors[] = $this->lang->error_useralreadybanned;
            }
        }

        $this->plugins->runHooks('modcp_do_banuser_start');

        // Still no errors? Ban the user
        if (!$this->errors) {
            // Ban the user
            if ($this->bb->getInput('liftafter', '') === '---') {
                $lifted = 0;
            } else {
                if (!isset($user->dateline)) {
                    $user->dateline = 0;
                }
                $lifted = $this->ban->ban_date2timestamp($this->bb->getInput('liftafter', ''), $user->dateline);
            }

            $banreason = my_substr($this->bb->getInput('banreason', '', true), 0, 255);

            if ($existing_ban) {
                $update_array = [
                    'gid' => $this->bb->getInput('usergroup', 0),
                    'dateline' => TIME_NOW,
                    'bantime' => $this->db->escape_string($this->bb->getInput('liftafter', '')),
                    'lifted' => $this->db->escape_string($lifted),
                    'reason' => $this->db->escape_string($banreason)
                ];

                $this->db->update_query('banned', $update_array, "uid='{$user->uid}'");
            } else {
                $insert_array = [
                    'uid' => $user->uid,
                    'gid' => $this->bb->getInput('usergroup', 0),
                    'oldgroup' => (int)$user->usergroup,
                    'oldadditionalgroups' => (string)$user->additionalgroups,
                    'olddisplaygroup' => (int)$user->displaygroup,
                    'admin' => (int)$this->user->uid,
                    'dateline' => TIME_NOW,
                    'bantime' => $this->db->escape_string($this->bb->getInput('liftafter', '')),
                    'lifted' => $this->db->escape_string($lifted),
                    'reason' => $this->db->escape_string($banreason)
                ];

                $this->db->insert_query('banned', $insert_array);
            }

            // Move the user to the banned group
            $update_array = [
                'usergroup' => $this->bb->getInput('usergroup', 0),
                'displaygroup' => 0,
                'additionalgroups' => '',
            ];
            $this->db->update_query('users', $update_array, "uid = {$user->uid}");

            $this->cache->update_banned();

            // Log edit or add ban
            if ($existing_ban) {
                $this->bblogger->log_moderator_action(['uid' => $user->uid, 'username' => $user->username], $this->lang->edited_user_ban);
            } else {
                $this->bblogger->log_moderator_action(['uid' => $user->uid, 'username' => $user->username], $this->lang->banned_user);
            }

            $this->plugins->runHooks('modcp_do_banuser_end');

            if ($existing_ban) {
                return $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/banning', $this->lang->redirect_banuser_updated);
            } else {
                return $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/banning', $this->lang->redirect_banuser);
            }
        } // Otherwise has errors, throw back to ban page
        else {
            return $this->index($request, $response, true);
        }
    }
}
