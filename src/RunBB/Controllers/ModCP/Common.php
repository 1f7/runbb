<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{
    public $app;
    protected $errors;
    protected $numannouncements = 0;
    protected $nummodqueuethreads = 0;
    protected $nummodqueueposts = 0;
    protected $nummodqueueattach = 0;
    protected $numreportedposts = 0;
    protected $nummodlogs = 0;

    protected $tflist = '';
    protected $flist_queue_threads = '';
    protected $tflist_modlog = '';
    protected $tflist_reports = '';
    protected $flist_reports = '';
    protected $tflist_queue_threads = '';
    protected $tflist_queue_posts = '';
    protected $tflist_queue_attach = '';
    protected $wflist_reports = '';

    protected $moderated_forums = [];

    protected function init()
    {
        // в случае возврата не обнуляем
        if (!isset($this->errors)) {
            $this->errors = '';
        }

        // Set up the array of ban times.
        $this->bantimes = $this->ban->fetch_ban_times();

        // Load global language phrases
        $this->lang->load('modcp');
        $this->lang->load('announcements');

        if ($this->user->uid == 0 || $this->bb->usergroup['canmodcp'] != 1) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        $flist = $flist_queue_posts = $flist_queue_attach = $flist_modlog = '';

        // SQL for fetching items only related to forums this user moderates
        $this->moderated_forums = [];
        if ($this->bb->usergroup['issupermod'] != 1) {
            $query = $this->db->simple_select(
                'moderators',
                '*',
                "(id='{$this->user->uid}' AND isgroup = '0') OR (id='{$this->user->usergroup}' AND isgroup = '1')"
            );
            while ($forum = $this->db->fetch_array($query)) {
                // For Announcements
                if ($forum['canmanageannouncements'] == 1) {
                    ++$this->numannouncements;
                }

                // For the Mod Queues
                if ($forum['canapproveunapprovethreads'] == 1) {
                    $this->flist_queue_threads .= ",'{$forum['fid']}'";

                    $children = $this->forum->get_child_list($forum['fid']);
                    if (!empty($children)) {
                        $this->flist_queue_threads .= ",'" . implode("','", $children) . "'";
                    }
                    ++$this->nummodqueuethreads;
                }

                if ($forum['canapproveunapproveposts'] == 1) {
                    $flist_queue_posts .= ",'{$forum['fid']}'";

                    $children = $this->forum->get_child_list($forum['fid']);
                    if (!empty($children)) {
                        $flist_queue_posts .= ",'" . implode("','", $children) . "'";
                    }
                    ++$this->nummodqueueposts;
                }

                if ($forum['canapproveunapproveattachs'] == 1) {
                    $flist_queue_attach .= ",'{$forum['fid']}'";

                    $children = $this->forum->get_child_list($forum['fid']);
                    if (!empty($children)) {
                        $flist_queue_attach .= ",'" . implode("','", $children) . "'";
                    }
                    ++$this->nummodqueueattach;
                }

                // For Reported posts
                if ($forum['canmanagereportedposts'] == 1) {
                    $this->flist_reports .= ",'{$forum['fid']}'";

                    $children = $this->forum->get_child_list($forum['fid']);
                    if (!empty($children)) {
                        $this->flist_reports .= ",'" . implode("','", $children) . "'";
                    }
                    ++$this->numreportedposts;
                }

                // For the Mod Log
                if ($forum['canviewmodlog'] == 1) {
                    $flist_modlog .= ",'{$forum['fid']}'";

                    $children = $this->forum->get_child_list($forum['fid']);
                    if (!empty($children)) {
                        $flist_modlog .= ",'" . implode("','", $children) . "'";
                    }
                    ++$this->nummodlogs;
                }

                $flist .= ",'{$forum['fid']}'";

                $children = $this->forum->get_child_list($forum['fid']);
                if (!empty($children)) {
                    $flist .= ",'" . implode("','", $children) . "'";
                }
                $this->moderated_forums[] = $forum['fid'];
            }
            if ($this->flist_queue_threads) {
                $this->tflist_queue_threads = " AND t.fid IN (0{$this->flist_queue_threads})";
                $this->flist_queue_threads = " AND fid IN (0{$this->flist_queue_threads})";
            }
            if ($flist_queue_posts) {
                $this->tflist_queue_posts = " AND t.fid IN (0{$flist_queue_posts})";
                $flist_queue_posts = " AND fid IN (0{$flist_queue_posts})";
            }
            if ($flist_queue_attach) {
                $this->tflist_queue_attach = " AND t.fid IN (0{$flist_queue_attach})";
                $flist_queue_attach = " AND fid IN (0{$flist_queue_attach})";
            }
            if ($this->flist_reports) {
                $this->wflist_reports = "WHERE r.id3 IN (0{$this->flist_reports})";
                $this->tflist_reports = " AND r.id3 IN (0{$this->flist_reports})";
                $this->flist_reports = " AND id3 IN (0{$this->flist_reports})";
            }
            if ($flist_modlog) {
                $this->tflist_modlog = " AND t.fid IN (0{$flist_modlog})";
                $flist_modlog = " AND fid IN (0{$flist_modlog})";
            }
            if ($flist) {
                $this->tflist = " AND t.fid IN (0{$flist})";
                $flist = " AND fid IN (0{$flist})";
            }
        }

        // Retrieve a list of unviewable forums
        $this->unviewableforums = $this->forum->get_unviewable_forums();
        $this->inactiveforums = $this->forum->get_inactive_forums();
        $unviewablefids1 = $unviewablefids2 = [];

        if ($this->unviewableforums) {
            $flist .= ' AND fid NOT IN (' . implode(',', $this->unviewableforums) . ')';
            $this->tflist .= ' AND t.fid NOT IN (' . implode(',', $this->unviewableforums) . ')';

            $unviewablefids1 = explode(',', $this->unviewableforums);
        }

        if ($this->inactiveforums) {
            $flist .= ' AND fid NOT IN (' . implode(',', $this->inactiveforums) . ')';
            $this->tflist .= ' AND t.fid NOT IN (' . implode(',', $this->inactiveforums) . ')';

            $unviewablefids2 = explode(',', $this->inactiveforums);
        }

        $this->unviewableforums = array_merge($unviewablefids1, $unviewablefids2);

        if (!isset($this->bb->collapsedimg['modcpforums'])) {
            $this->bb->collapsedimg['modcpforums'] = '';
        }

        if (!isset($collapsed['modcpforums_e'])) {
            $collapsed['modcpforums_e'] = '';
        }

        if (!isset($this->bb->collapsedimg['modcpusers'])) {
            $this->bb->collapsedimg['modcpusers'] = '';
        }

        if (!isset($collapsed['modcpusers_e'])) {
            $collapsed['modcpusers_e'] = '';
        }

        // Fetch the Mod CP menu
        //$nav_announcements = $nav_modqueue = $nav_reportcenter = $nav_modlogs =
        //$nav_editprofile = $nav_banning = $nav_warninglogs = $nav_ipsearch =
        //$nav_forums_posts = $modcp_nav_users = '';
        $this->bb->showAnnouncements = false;
        if (($this->numannouncements > 0 ||
                $this->bb->usergroup['issupermod'] == 1) &&
            $this->bb->usergroup['canmanageannounce'] == 1
        ) {
            $this->bb->showAnnouncements = true;
            //ev al("\$nav_announcements = \"".$this->templates->get('modcp_nav_announcements')."\";");
        }

        $this->bb->showModQueue = false;
        if (($this->nummodqueuethreads > 0 ||
                $this->nummodqueueposts > 0 ||
                $this->nummodqueueattach > 0 ||
                $this->bb->usergroup['issupermod'] == 1) &&
            $this->bb->usergroup['canmanagemodqueue'] == 1
        ) {
            $this->bb->showModQueue = true;
            //ev al("\$nav_modqueue = \"".$this->templates->get('modcp_nav_modqueue')."\";");
        }

        $this->bb->showReportCenter = false;
        if (($this->numreportedposts > 0 || $this->bb->usergroup['issupermod'] == 1) && $this->bb->usergroup['canmanagereportedcontent'] == 1) {
            $this->bb->showReportCenter = true;
            //ev al("\$nav_reportcenter = \"".$this->templates->get('modcp_nav_reportcenter')."\";");
        }
        $this->bb->showModLogs = false;
        if (($this->nummodlogs > 0 || $this->bb->usergroup['issupermod'] == 1) && $this->bb->usergroup['canviewmodlogs'] == 1) {
            $this->bb->showModLogs = true;
            //ev al("\$nav_modlogs = \"".$this->templates->get('modcp_nav_modlogs')."\";");
        }

//    if($this->bb->usergroup['caneditprofiles'] == 1)
//    {
//      //ev al("\$nav_editprofile = \"".$this->templates->get('modcp_nav_editprofile')."\";");
//    }

//    if($this->bb->usergroup['canbanusers'] == 1)
//    {
//      //ev al("\$nav_banning = \"".$this->templates->get('modcp_nav_banning')."\";");
//    }

//    if($this->bb->usergroup['canviewwarnlogs'] == 1)
//    {
//      //ev al("\$nav_warninglogs = \"".$this->templates->get('modcp_nav_warninglogs')."\";");
//    }

//    if($this->bb->usergroup['canuseipsearch'] == 1)
//    {
//      //ev al("\$nav_ipsearch = \"".$this->templates->get('modcp_nav_ipsearch')."\";");
//    }

        $this->plugins->runHooks('modcp_nav');

//    $this->showNavForumPosts = false;
//    if(!empty($nav_announcements) || !empty($nav_modqueue) || !empty($nav_reportcenter) || !empty($nav_modlogs))
//    {
//      $this->showNavForumPosts = true;
//      //ev al("\$modcp_nav_forums_posts = \"".$this->templates->get('modcp_nav_forums_posts')."\";");
//    }
//    $this->showNavUsers = false;
//    if(
//      ($this->bb->usergroup['caneditprofiles'] == 1) ||
//      ($this->bb->usergroup['canbanusers'] == 1) ||
//      ($this->bb->usergroup['canviewwarnlogs'] == 1) ||
//      ($this->bb->usergroup['canuseipsearch'] == 1)
//    )
//    {
//      $this->showNavUsers = true;
//      //ev al("\$modcp_nav_users = \"".$this->templates->get('modcp_nav_users')."\";");
//    }

        //ev al("\$modcp_nav = \"".$this->templates->get('modcp_nav')."\";");

        $this->plugins->runHooks('modcp_start');

        // Make navigation
        $this->bb->add_breadcrumb($this->lang->nav_modcp, $this->bb->settings['bburl'] . '/modcp');
    }
}
