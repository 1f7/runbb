<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class DeleteAnnouncement extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() == 'exit') {
            return;
        }
        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        $aid = $this->bb->getInput('aid', '');
        $query = $this->db->simple_select('announcements', 'aid, subject, fid', "aid='{$aid}'");

        $announcement = $this->db->fetch_array($query);
        $announcement['subject'] = htmlspecialchars_uni($announcement['subject']);

        if (!$announcement) {
            $this->bb->error($this->lang->error_invalid_announcement);
        }

        if (($this->bb->usergroup['issupermod'] != 1 && $announcement['fid'] == -1) || ($announcement['fid'] != -1 &&
        !$this->user->is_moderator($announcement['fid'], 'canmanageannouncements')) ||
        ($this->unviewableforums && in_array($announcement['fid'], $this->unviewableforums))) {
            return $this->bb->error_no_permission();
        }

        $this->view->offsetSet('subject', $announcement['subject']);
        $this->view->offsetSet('aid', $aid);
        $this->plugins->runHooks('modcp_delete_announcement');

        //ev al('\$announcements = \''.$templates->get('modcp_announcements_delete').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/deleteAnnouncement.html.twig');
    }

    public function doDelete(Request $request, Response $response)
    {
        if ($this->init() == 'exit') {
            return;
        }

        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        $aid = $this->bb->getInput('aid', '');
        $query = $this->db->simple_select('announcements', 'aid, subject, fid', "aid='{$aid}'");
        $announcement = $this->db->fetch_array($query);

        if (!$announcement) {
            $this->bb->error($this->lang->error_invalid_announcement);
        }
        if (($this->bb->usergroup['issupermod'] != 1 && $announcement['fid'] == -1) || ($announcement['fid'] != -1 &&
        !$this->user->is_moderator($announcement['fid'], 'canmanageannouncements')) ||
        ($this->unviewableforums && in_array($announcement['fid'], $this->unviewableforums))) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('modcp_do_delete_announcement');

        $this->db->delete_query('announcements', "aid='{$aid}'");
        $this->bblogger->log_moderator_action(['aid' => $announcement['aid'], 'subject' => $announcement['subject']], $this->lang->announcement_deleted);
        $this->cache->update_forumsdisplay();

        $this->bb->redirect($this->bb->settings['bburl'].'/modcp/announcements', $this->lang->redirect_delete_announcement);
    }
}
