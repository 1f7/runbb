<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class EditAnnouncement extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() == 'exit') {
                return;
            }
        }

        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        $aid = $this->bb->getInput('aid', 0);

        $this->bb->add_breadcrumb($this->lang->mcp_nav_announcements, $this->bb->settings['bburl'] . '/modcp/announcements');
        $this->bb->add_breadcrumb($this->lang->edit_announcement, $this->bb->settings['bburl'] . '/modcp/edit_announcements?aid=' . $aid);

        // Get announcement
        if (!isset($announcement) || $this->bb->request_method != 'post') {
            $query = $this->db->simple_select('announcements', '*', "aid='{$aid}'");
            $announcement = $this->db->fetch_array($query);
        }

        if (!$announcement) {
            $this->bb->error($this->lang->error_invalid_announcement);
        }
        if (($this->bb->usergroup['issupermod'] != 1 && $announcement['fid'] == -1) ||
            ($announcement['fid'] != -1 &&
                !$this->user->is_moderator($announcement['fid'], 'canmanageannouncements')) ||
            ($this->unviewableforums && in_array($announcement['fid'], $this->unviewableforums))
        ) {
            return $this->bb->error_no_permission();
        }

        if (!$announcement['startdate']) {
            // No start date? Make it now.
            $announcement['startdate'] = TIME_NOW;
        }

        $makeshift_end = false;
        if (!$announcement['enddate']) {
            $makeshift_end = true;
            $makeshift_time = TIME_NOW;
            if ($announcement['startdate']) {
                $makeshift_time = $announcement['startdate'];
            }

            // No end date? Make it a year from now.
            $announcement['enddate'] = $makeshift_time + (60 * 60 * 24 * 366);
        }

        // Deal with inline errors
        if (!empty($this->errors) || isset($this->preview)) {
            if (!empty($this->errors)) {
                $this->errors = $this->bb->inline_error($this->errors);
            } else {
                $this->errors = '';
            }

            // Set $announcement to input stuff
            $announcement['subject'] = $this->bb->input['title'];
            $announcement['message'] = $this->bb->input['message'];
            $announcement['allowhtml'] = $this->bb->input['allowhtml'];//$allowhtml;
            $announcement['allowmycode'] = $this->bb->input['allowmycode'];//$allowmycode;
            $announcement['allowsmilies'] = $this->bb->input['allowsmilies'];//$allowsmilies;

            $startmonth = $this->bb->input['starttime_month'];
            $startdateyear = htmlspecialchars_uni($this->bb->input['starttime_year']);
            $startday = $this->bb->getInput('starttime_day', 0);
            $starttime_time = htmlspecialchars_uni($this->bb->input['starttime_time']);
            $endmonth = $this->bb->input['endtime_month'];
            $enddateyear = htmlspecialchars_uni($this->bb->input['endtime_year']);
            $endday = $this->bb->getInput('endtime_day', 0);
            $endtime_time = htmlspecialchars_uni($this->bb->input['endtime_time']);

            $errored = true;
        } else {
            // Note: dates are in GMT timezone
            $starttime_time = gmdate('g:i a', $announcement['startdate']);
            $endtime_time = gmdate('g:i a', $announcement['enddate']);

            $startday = gmdate('j', $announcement['startdate']);
            $endday = gmdate('j', $announcement['enddate']);

            $startmonth = gmdate('n', $announcement['startdate']);
            $endmonth = gmdate('n', $announcement['enddate']);
//      $startmonth = gmdate('m', $announcement['startdate']);
//      $endmonth = gmdate('m', $announcement['enddate']);

            $startdateyear = gmdate('Y', $announcement['startdate']);
            $enddateyear = gmdate('Y', $announcement['enddate']);

            $errored = false;
        }
        $this->view->offsetSet('startdateyear', $startdateyear);
        $this->view->offsetSet('starttime_time', $starttime_time);
        $this->view->offsetSet('enddateyear', $enddateyear);
        $this->view->offsetSet('endtime_time', $endtime_time);

        // Generate form elements
        $startdateday = $enddateday = '';
        for ($day = 1; $day <= 31; ++$day) {
            if ($startday == $day) {
                $startdateday .= "<option value=\"{$day}\" selected=\"selected\">{$day}</option>";
            } else {
                $startdateday .= "<option value=\"{$day}\">{$day}</option>";
            }

            if ($endday == $day) {
                $enddateday .= "<option value=\"{$day}\" selected=\"selected\">{$day}</option>";
            } else {
                $enddateday .= "<option value=\"{$day}\">{$day}</option>";
            }
        }
        $this->view->offsetSet('startdateday', $startdateday);
        $this->view->offsetSet('enddateday', $enddateday);

        $startmonthsel = $endmonthsel = [];
        //foreach(array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12') as $month)
        foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] as $month) {
            $startmonthsel[$month] = '';
            $endmonthsel[$month] = '';
        }

        $startmonthsel[$startmonth] = 'selected="selected"';
        $endmonthsel[$endmonth] = 'selected="selected"';
        $this->view->offsetSet('startmonthsel', $startmonthsel);
        $this->view->offsetSet('endmonthsel', $endmonthsel);

        $this->view->offsetSet('title', htmlspecialchars_uni($announcement['subject']));
        $this->view->offsetSet('message', htmlspecialchars_uni($announcement['message']));

        $html_sel = $mycode_sel = $smilies_sel = ['yes' => '', 'no' => ''];
        if ($announcement['allowhtml']) {
            $html_sel['yes'] = ' checked="checked"';
        } else {
            $html_sel['no'] = ' checked="checked"';
        }
        $this->view->offsetSet('html_sel', $html_sel);

        if ($announcement['allowmycode']) {
            $mycode_sel['yes'] = ' checked="checked"';
        } else {
            $mycode_sel['no'] = ' checked="checked"';
        }
        $this->view->offsetSet('mycode_sel', $mycode_sel);

        if ($announcement['allowsmilies']) {
            $smilies_sel['yes'] = ' checked="checked"';
        } else {
            $smilies_sel['no'] = ' checked="checked"';
        }
        $this->view->offsetSet('smilies_sel', $smilies_sel);

        $end_type_sel = ['infinite' => '', 'finite' => ''];
        if (($errored && $this->bb->getInput('endtime_type', 0) == 2) ||
            (!$errored && (int)$announcement['enddate'] == 0) || $makeshift_end == true
        ) {
            $end_type_sel['infinite'] = ' checked="checked"';
        } else {
            $end_type_sel['finite'] = ' checked="checked"';
        }
        $this->view->offsetSet('end_type_sel', $end_type_sel);

        // MyCode editor
        $this->bb->codebuttons = $this->editor->build_mycode_inserter();
        $this->bb->smilieinserter = $this->editor->build_clickable_smilies();

        $postbit = '';
        if (isset($this->preview)) {
            $this->bb->announcementarray = [
                'aid' => $announcement['aid'],
                'fid' => $announcement['fid'],
                'uid' => $this->user->uid,
                'subject' => $this->bb->input['title'],
                'message' => $this->bb->input['message'],
                'allowhtml' => $this->bb->getInput('allowhtml', 0),
                'allowmycode' => $this->bb->getInput('allowmycode', 0),
                'allowsmilies' => $this->bb->getInput('allowsmilies', 0),
                'dateline' => TIME_NOW,
                'userusername' => $this->user->username,
                'ipaddress' => $this->session->ipaddress
            ];

            $array = $this->bb->user;
            foreach ($array as $key => $element) {
                $this->bb->announcementarray[$key] = $element;
            }

            // Gather usergroup data from the cache
            $data_key = [
                'title' => 'grouptitle',
                'usertitle' => 'groupusertitle',
                'stars' => 'groupstars',
                'starimage' => 'groupstarimage',
                'image' => 'groupimage',
                'namestyle' => 'namestyle',
                'usereputationsystem' => 'usereputationsystem'
            ];

            foreach ($data_key as $field => $key) {
                $this->bb->announcementarray[$key] = $this->bb->groupscache[$this->bb->announcementarray['usergroup']][$field];
            }
            $this->fid = $announcement['fid'];//to postbit

            $postbit = $this->post->build_postbit($this->bb->announcementarray, 3);
        }

        $this->view->offsetSet('postbit', $postbit);
        $this->view->offsetSet('errors', $this->errors);
        $this->view->offsetSet('fid', $announcement['fid']);
        $this->view->offsetSet('aid', $aid);

        $this->plugins->runHooks('modcp_edit_announcement');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/editAnnouncements.html.twig');
    }

    public function doEdit(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Get the announcement
        $aid = $this->bb->getInput('aid', 0);
        $query = $this->db->simple_select('announcements', '*', "aid='{$aid}'");
        $announcement = $this->db->fetch_array($query);

        // Check that it exists
        if (!$announcement) {
            $this->bb->error($this->lang->error_invalid_announcement);
        }

        // Mod has permissions to edit this announcement
        if (($this->bb->usergroup['issupermod'] != 1 && $announcement['fid'] == -1) ||
            ($announcement['fid'] != -1 &&
                !$this->user->is_moderator($announcement['fid'], 'canmanageannouncements')) ||
            ($this->unviewableforums && in_array($announcement['fid'], $this->unviewableforums))
        ) {
            return $this->bb->error_no_permission();
        }

        $this->errors = [];

        // Basic error checking
        $this->bb->input['title'] = $this->bb->getInput('title', '', true);
        if (!trim($this->bb->input['title'])) {
            $this->errors[] = $this->lang->error_missing_title;
        }

        $this->bb->input['message'] = $this->bb->getInput('message', '', true);
        if (!trim($this->bb->input['message'])) {
            $this->errors[] = $this->lang->error_missing_message;
        }

        $this->bb->input['starttime_time'] = $this->bb->getInput('starttime_time', '');
        $this->bb->input['endtime_time'] = $this->bb->getInput('endtime_time', '');
        $startdate = @explode(' ', $this->bb->input['starttime_time']);
        $startdate = @explode(':', $startdate[0]);
        $enddate = @explode(' ', $this->bb->input['endtime_time']);
        $enddate = @explode(':', $enddate[0]);

        if (stristr($this->bb->input['starttime_time'], 'pm')) {
            $startdate[0] = 12 + $startdate[0];
            if ($startdate[0] >= 24) {
                $startdate[0] = '00';
            }
        }

        if (stristr($this->bb->input['endtime_time'], 'pm')) {
            $enddate[0] = 12 + $enddate[0];
            if ($enddate[0] >= 24) {
                $enddate[0] = '00';
            }
        }

        $this->bb->input['starttime_month'] = $this->bb->getInput('starttime_month', '');
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        if (!in_array($this->bb->input['starttime_month'], $months)) {
            $this->bb->input['starttime_month'] = '01';
        }

        $startdate = gmmktime((int)$startdate[0], (int)$startdate[1], 0, $this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0));
        if (!checkdate($this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0)) || $startdate < 0 || $startdate == false) {
            $this->errors[] = $this->lang->error_invalid_start_date;
        }

        if ($this->bb->getInput('endtime_type', 0) == '2') {
            $enddate = '0';
            $this->bb->input['endtime_month'] = '01';
        } else {
            $this->bb->input['endtime_month'] = $this->bb->getInput('endtime_month', '');
            if (!in_array($this->bb->input['endtime_month'], $months)) {
                $this->bb->input['endtime_month'] = '01';
            }
            $enddate = gmmktime((int)$enddate[0], (int)$enddate[1], 0, $this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0));
            if (!checkdate($this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0)) || $enddate < 0 || $enddate == false) {
                $this->errors[] = $this->lang->error_invalid_end_date;
            } elseif ($enddate <= $startdate) {
                $this->errors[] = $this->lang->error_end_before_start;
            }
        }

        if ($this->bb->getInput('allowhtml', 0) == 1) {
            $allowhtml = 1;
        } else {
            $allowhtml = 0;
        }
        if ($this->bb->getInput('allowmycode', 0) == 1) {
            $allowmycode = 1;
        } else {
            $allowmycode = 0;
        }
        if ($this->bb->getInput('allowsmilies', 0) == 1) {
            $allowsmilies = 1;
        } else {
            $allowsmilies = 0;
        }

        $this->plugins->runHooks('modcp_do_edit_announcement_start');

        // Proceed to update if no errors
        if (!$this->errors) {
            if (isset($this->bb->input['preview'])) {
                $this->preview = [];
                $this->index($request, $response, true);
            } else {
                $update_announcement = [
                    'uid' => $this->user->uid,
                    'subject' => $this->db->escape_string($this->bb->input['title']),
                    'message' => $this->db->escape_string($this->bb->input['message']),
                    'startdate' => $startdate,
                    'enddate' => $enddate,
                    'allowhtml' => $allowhtml,
                    'allowmycode' => $allowmycode,
                    'allowsmilies' => $allowsmilies
                ];
                $this->db->update_query('announcements', $update_announcement, "aid='{$aid}'");

                $this->bblogger->log_moderator_action(
                    ['aid' => $announcement['aid'], 'subject' => $this->db->escape_string($this->bb->input['title'])],
                    $this->lang->announcement_edited
                );

                $this->plugins->runHooks('modcp_do_edit_announcement_end');

                $this->cache->update_forumsdisplay();
                $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/announcements', $this->lang->redirect_edit_announcement);
            }
        } else {
            $this->index($request, $response, true);
        }
    }
}
