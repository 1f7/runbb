<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Helpers\WarnHelper;

class Editprofile extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() == 'exit') {
                return;
            }
        }

        if ($this->bb->usergroup['caneditprofiles'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_editprofile, $this->bb->settings['bburl'] . '/modcp/editprofile');

        $user = $this->user->get_user($this->bb->getInput('uid', 0));
        if (!$user) {
            $this->bb->error($this->lang->error_nomember);
        }

        // Check if the current user has permission to edit this user
        if (!$this->modcp->modcp_can_manage_user($user->uid)) {
            return $this->bb->error_no_permission();
        }

        if ($user->website == '' || $user->website == 'http://') {
            $user->website = 'http://';
        }

        if ($user->icq != '0') {
            $user->icq = (int)$user->icq;
        }

        if (!$this->errors) {
            $this->bb->input = array_merge($user, $this->bb->input);
            $birthday = explode('-', $user->birthday);
            if (!isset($birthday[1])) {
                $birthday[1] = '';
            }
            if (!isset($birthday[2])) {
                $birthday[2] = '';
            }
            list($this->bb->input['birthday_day'], $this->bb->input['birthday_month'], $this->bb->input['birthday_year']) = $birthday;
        } else {
            $this->errors = $this->bb->inline_error($this->errors);
        }

        // Sanitize all input
        foreach (['usertitle', 'website', 'icq', 'aim', 'yahoo', 'skype', 'google', 'signature', 'birthday_day', 'birthday_month', 'birthday_year'] as $field) {
            $this->bb->input[$field] = htmlspecialchars_uni($this->bb->getInput($field, ''));
        }

        // Custom user title, check to see if we have a default group title
        if (!$user->displaygroup) {
            $user->displaygroup = $user->usergroup;
        }

        $this->bb->displaygroupfields = ['usertitle'];
        $display_group = $this->group->usergroup_displaygroup($user->displaygroup);

        if (!empty($display_group['usertitle'])) {
            $defaulttitle = htmlspecialchars_uni($display_group['usertitle']);
        } else {
            // Go for post count title if a group default isn't set
            $usertitles = $this->cache->read('usertitles');

            foreach ($usertitles as $title) {
                if ($title['posts'] <= $user->postnum) {
                    $defaulttitle = $title['title'];
                    break;
                }
            }
        }
        $this->view->offsetSet('defaulttitle', $defaulttitle);

        $user->usertitle = htmlspecialchars_uni($user->usertitle);

        if (empty($user->usertitle)) {
            $this->lang->current_custom_usertitle = '';
        }
        $this->view->offsetSet('usertitle', $user->usertitle);

        $bdaydaysel = [];
        for ($day = 1; $day <= 31; ++$day) {
            if ($this->bb->input['birthday_day'] == $day) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $bdaydaysel[] = [
                'day' => $day,
                'selected' => $selected
            ];
        }
        $this->view->offsetSet('bdaydaysel', $bdaydaysel);

        $bdaymonthsel = [];
        foreach (range(1, 12) as $month) {
            $bdaymonthsel[$month] = '';
        }
        $bdaymonthsel[$this->bb->input['birthday_month']] = 'selected="selected"';
        $this->view->offsetSet('bdaymonthsel', $bdaymonthsel);

        if ($this->bb->settings['allowaway'] != 0) {
            $awaycheck = ['', ''];
            if ($this->errors) {
                if ($user->away == 1) {
                    $awaycheck[1] = 'checked="checked"';
                } else {
                    $awaycheck[0] = 'checked="checked"';
                }
                $returndate = [];
                $returndate[0] = $this->bb->getInput('awayday', '');
                $returndate[1] = $this->bb->getInput('awaymonth', '');
                $returndate[2] = $this->bb->getInput('awayyear', 0);
                $user->awayreason = htmlspecialchars_uni($this->bb->getInput('awayreason', ''));
            } else {
                $user->awayreason = htmlspecialchars_uni($user->awayreason);
                if ($user->away == 1) {
                    $awaydate = $this->time->formatDate($this->bb->settings['dateformat'], $user->awaydate);
                    $awaycheck[1] = 'checked="checked"';
//FIXME not used???          $awaynotice = $this->lang->sprintf($this->lang->away_notice_away, $awaydate);
                } else {
//FIXME not used???          $awaynotice = $this->lang->away_notice;
                    $awaycheck[0] = 'checked="checked"';
                }
                $returndate = explode('-', $user->returndate);
            }
            $this->view->offsetSet('awaycheck', $awaycheck);
            $this->view->offsetSet('awayreason', $user->awayreason);

            $returndatesel = [];
            for ($day = 1; $day <= 31; ++$day) {
                if ($returndate[0] == $day) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }
                $returndatesel[] = [
                    'day' => $day,
                    'selected' => $selected
                ];
            }
            $this->view->offsetSet('returndatesel', $returndatesel);

            $returndatemonthsel = [];
            foreach (range(1, 12) as $month) {
                $returndatemonthsel[$month] = '';
            }
            if (isset($returndate[1])) {
                $returndatemonthsel[$returndate[1]] = ' selected="selected"';
            }
            $this->view->offsetSet('returndatemonthsel', $returndatemonthsel);

            if (!isset($returndate[2])) {
                $returndate[2] = '';
            }
            $this->view->offsetSet('returndate', $returndate);
        }

        $this->plugins->runHooks('modcp_editprofile_start');

        // Fetch profile fields
        $query = $this->db->simple_select('userfields', '*', "ufid='{$user->uid}'");
        $user_fields = $this->db->fetch_array($query);

        $requiredfields = $customfields = [];
        $this->bb->input['profile_fields'] = $this->bb->getInput('profile_fields', []);

        $pfcache = $this->cache->read('profilefields');

        if (is_array($pfcache)) {
            foreach ($pfcache as $profilefield) {
                $userfield = $code = $select = $val = $options = $expoptions = $useropts = $seloptions = '';
                $profilefield['type'] = htmlspecialchars_uni($profilefield['type']);
                $profilefield['name'] = htmlspecialchars_uni($profilefield['name']);
                $profilefield['description'] = htmlspecialchars_uni($profilefield['description']);
                $thing = explode("\n", $profilefield['type'], '2');
                $type = $thing[0];
                if (isset($thing[1])) {
                    $options = $thing[1];
                }
                $field = "fid{$profilefield['fid']}";
                if ($this->errors) {
                    if (isset($this->bb->input['profile_fields'][$field])) {
                        $userfield = $this->bb->input['profile_fields'][$field];
                    }
                } else {
                    $userfield = $user_fields[$field];
                }
                if ($type == 'multiselect') {
                    if ($this->errors) {
                        $useropts = $userfield;
                    } else {
                        $useropts = explode("\n", $userfield);
                    }
                    if (is_array($useropts)) {
                        foreach ($useropts as $key => $val) {
                            $seloptions[$val] = $val;
                        }
                    }
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $val = trim($val);
                            $val = str_replace("\n", "\\n", $val);

                            $sel = '';
                            if (isset($seloptions[$val]) && $val == $seloptions[$val]) {
                                $sel = ' selected="selected"';
                            }
                            $select .= "<option value=\"{$val}\"{$sel}>{$val}</option>";
                        }
                        if (!$profilefield['length']) {
                            $profilefield['length'] = 3;
                        }
                        $code = "
            <select name=\"profile_fields[$field][]\" size=\"{$profilefield['length']}\" multiple=\"multiple\">
                {$select}
            </select>";
                    }
                } elseif ($type == 'select') {
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $val = trim($val);
                            $val = str_replace("\n", "\\n", $val);
                            $sel = '';
                            if ($val == $userfield) {
                                $sel = ' selected="selected"';
                            }
                            $select .= "<option value=\"{$val}\"{$sel}>{$val}</option>";
                        }
                        if (!$profilefield['length']) {
                            $profilefield['length'] = 1;
                        }
                        $code = "
            <select name=\"profile_fields[$field]\" size=\"{$profilefield['length']}\">
                {$select}
            </select>";
                    }
                } elseif ($type == 'radio') {
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $checked = '';
                            if ($val == $userfield) {
                                $checked = ' checked="checked"';
                            }
                            $code .= "
                <input type=\"radio\" class=\"radio\" name=\"profile_fields[$field]\" value=\"{$val}\"{$checked} />
                <small>{$val}</small><br />";
                        }
                    }
                } elseif ($type == 'checkbox') {
                    if ($this->errors) {
                        $useropts = $userfield;
                    } else {
                        $useropts = explode("\n", $userfield);
                    }
                    if (is_array($useropts)) {
                        foreach ($useropts as $key => $val) {
                            $seloptions[$val] = $val;
                        }
                    }
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $checked = '';
                            if (isset($seloptions[$val]) && $val == $seloptions[$val]) {
                                $checked = ' checked="checked"';
                            }
                            $code .= "
                <input type=\"checkbox\" class=\"checkbox\" name=\"profile_fields[$field][]\" value=\"{$val}\"{$checked} />
                <small>{$val}</small><br />";
                        }
                    }
                } elseif ($type == 'textarea') {
                    $value = htmlspecialchars_uni($userfield);
                    $code = "<textarea name=\"profile_fields[$field]\" rows=\"6\" cols=\"30\" style=\"width: 95%\">{$value}</textarea>";
                } else {
                    $value = htmlspecialchars_uni($userfield);
                    $maxlength = '';
                    if ($profilefield['maxlength'] > 0) {
                        $maxlength = " maxlength=\"{$profilefield['maxlength']}\"";
                    }
                    $code = "<input type=\"text\" name=\"profile_fields[$field]\" class=\"textbox\" size=\"{$profilefield['length']}\"{$maxlength} value=\"{$value}\" />";
                }

                if ($profilefield['required'] == 1) {
                    $requiredfields[] = [
                        'name' => $profilefield['name'],
                        'description' => $profilefield['description'],
                        'code' => $code
                    ];
                } else {
                    $customfields[] = [
                        'name' => $profilefield['name'],
                        'description' => $profilefield['description'],
                        'code' => $code
                    ];
                }
            }
        }
        $this->view->offsetSet('requiredfields', $requiredfields);
        $this->view->offsetSet('customfields', $customfields);

        $this->lang->edit_profile = $this->lang->sprintf($this->lang->edit_profile, $user->username);
        $profile_link = $this->user->build_profile_link($this->user->format_name($user->username, $user->usergroup, $user->displaygroup), $user->uid);
        $this->view->offsetSet('profile_link', $profile_link);

        $user->signature = htmlspecialchars_uni($user->signature);
        $this->view->offsetSet('signature', $user->signature);
        $this->bb->codebuttons = $this->editor->build_mycode_inserter('signature');

        // Do we mark the suspend signature box?
        if ($user->suspendsignature || ($this->bb->getInput('suspendsignature', 0) && !empty($this->errors))) {
            $checked = 1;
            $checked_item = 'checked="checked"';
        } else {
            $checked = 0;
            $checked_item = '';
        }
        $this->view->offsetSet('checked', $checked);
        $this->view->offsetSet('checked_item', $checked_item);

        // Do we mark the moderate posts box?
        $modpost_check = 0;
        $modpost_checked = '';
        if ($user->moderateposts || ($this->bb->getInput('moderateposting', 0) && !empty($this->errors))) {
            $modpost_check = 1;
            $modpost_checked = 'checked="checked"';
        }
        $this->view->offsetSet('modpost_check', $modpost_check);
        $this->view->offsetSet('modpost_checked', $modpost_checked);

        // Do we mark the suspend posts box?
        $suspost_check = 0;
        $suspost_checked = '';
        if ($user->suspendposting || ($this->bb->getInput('suspendposting', 0) && !empty($this->errors))) {
            $suspost_check = 1;
            $suspost_checked = 'checked="checked"';
        }

        $this->view->offsetSet('suspost_check', $suspost_check);
        $this->view->offsetSet('suspost_checked', $suspost_checked);

        $moderator_options = [
            1 => [
                'action' => 'suspendsignature', // The input action for this option
                'option' => 'suspendsignature', // The field in the database that this option relates to
                'time' => 'action_time', // The time we've entered
                'length' => 'suspendsigtime', // The length of suspension field in the database
                'select_option' => 'action' // The name of the select box of this option
            ],
            2 => [
                'action' => 'moderateposting',
                'option' => 'moderateposts',
                'time' => 'modpost_time',
                'length' => 'moderationtime',
                'select_option' => 'modpost'
            ],
            3 => [
                'action' => 'suspendposting',
                'option' => 'suspendposting',
                'time' => 'suspost_time',
                'length' => 'suspensiontime',
                'select_option' => 'suspost'
            ]
        ];

        $periods = [
            'hours' => $this->lang->expire_hours,
            'days' => $this->lang->expire_days,
            'weeks' => $this->lang->expire_weeks,
            'months' => $this->lang->expire_months,
            'never' => $this->lang->expire_permanent
        ];

        $suspendsignature_info = $moderateposts_info = $suspendposting_info = '';
        $action_options = $modpost_options = $suspost_options = '';
        foreach ($moderator_options as $option) {
            $this->bb->input[$option['time']] = $this->bb->getInput($option['time'], 0);
            // Display the suspension info, if this user has this option suspended
            if ($user->{$option['option']}) {
                if ($user->{$option['length']} == 0) {
                    // User has a permanent ban
                    $string = $option['option'] . '_perm';
                    $suspension_info = $this->lang->$string;
                } else {
                    // User has a temporary (or limited) ban
                    $string = $option['option'] . '_for';
                    $for_date = $this->time->formatDate('relative', $user->{$option['length']}, '', 2);
                    $suspension_info = $this->lang->sprintf($this->lang->$string, $for_date);
                }

                switch ($option['option']) {
                    case 'suspendsignature':
                        $suspendsignature_info = $suspension_info;
                        break;
                    case 'moderateposts':
                        $moderateposts_info = $suspension_info;
                        break;
                    case 'suspendposting':
                        $suspendposting_info = $suspension_info;
                        break;
                }
            }
            $this->view->offsetSet('moderateposts_info', $moderateposts_info);
            $this->view->offsetSet('suspendposting_info', $suspendposting_info);
            $this->view->offsetSet('suspendsignature_info', $suspendsignature_info);

            // Generate the boxes for this option
            $selection_options = '';
            foreach ($periods as $key => $value) {
                $string = $option['select_option'] . '_period';
                $selected = '';
                if ($this->bb->getInput($string, '') === $key) {
                    $selected = 'selected="selected"';
                }
                $selection_options .= "<option value=\"{$key}\" {$selected}>{$value}</option>";
            }

            $select_name = $option['select_option'] . '_period';
            switch ($option['option']) {
                case 'suspendsignature':
                    $action_options = "
                    <select class=\"form-control\" name=\"{$select_name}\">
                        {$selection_options}
                    </select>";
                    break;
                case 'moderateposts':
                    $modpost_options = "
                    <select class=\"form-control\" name=\"{$select_name}\">
                        {$selection_options}
                    </select>";
                    break;
                case 'suspendposting':
                    $suspost_options = "
                    <select class=\"form-control\" name=\"{$select_name}\">
                        {$selection_options}
                    </select>";
                    break;
            }
        }
        $this->view->offsetSet('modpost_options', $modpost_options);
        $this->view->offsetSet('suspost_options', $suspost_options);
        $this->view->offsetSet('action_options', $action_options);

        if (!isset($newtitle)) {
            $newtitle = '';
        }
        $this->view->offsetSet('newtitle', $newtitle);
        $this->view->offsetSet('errors', $this->errors);
        $this->view->offsetSet('usernotes', $user->usernotes);
        $this->view->offsetSet('uid', $user->uid);

        $this->plugins->runHooks('modcp_editprofile_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/editprofile.html.twig');
    }

    public function doEdit(Request $request, Response $response)
    {
        if ($this->init() == 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->input['my_post_key']);

        if ($this->bb->usergroup['caneditprofiles'] == 0) {
            return $this->bb->error_no_permission();
        }

        $user = $this->user->get_user($this->bb->input['uid']);
        if (!$user) {
            $this->bb->error($this->lang->error_nomember);
        }

        // Check if the current user has permission to edit this user
        if (!$this->modcp->modcp_can_manage_user($user->uid)) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('modcp_do_editprofile_start');

        if ($this->bb->getInput('away', 0) == 1 && $this->bb->settings['allowaway'] != 0) {
            $awaydate = TIME_NOW;
            if (!empty($this->bb->input['awayday'])) {
                // If the user has indicated that they will return on a specific day,
                // but not month or year, assume it is current month and year
                if (!$this->bb->getInput('awaymonth', 0)) {
                    $this->bb->input['awaymonth'] = $this->time->formatDate('n', $awaydate);
                }
                if (!$this->bb->getInput('awayyear', 0)) {
                    $this->bb->input['awayyear'] = $this->time->formatDate('Y', $awaydate);
                }

                $return_month = (int)substr($this->bb->getInput('awaymonth', ''), 0, 2);
                $return_day = (int)substr($this->bb->getInput('awayday', ''), 0, 2);
                $return_year = min((int)$this->bb->getInput('awayyear', ''), 9999);

                // Check if return date is after the away date.
                $returntimestamp = gmmktime(0, 0, 0, $return_month, $return_day, $return_year);
                $awaytimestamp = gmmktime(0, 0, 0, $this->time->formatDate('n', $awaydate), $this->time->formatDate('j', $awaydate), $this->time->formatDate('Y', $awaydate));
                if ($return_year < $this->time->formatDate('Y', $awaydate) || ($returntimestamp < $awaytimestamp && $return_year == $this->time->formatDate('Y', $awaydate))) {
                    $this->bb->error($this->lang->error_modcp_return_date_past);
                }

                $returndate = "{$return_day}-{$return_month}-{$return_year}";
            } else {
                $returndate = '';
            }
            $away = [
                'away' => 1,
                'date' => $awaydate,
                'returndate' => $returndate,
                'awayreason' => $this->bb->getInput('awayreason', '', true)
            ];
        } else {
            $away = [
                'away' => 0,
                'date' => '',
                'returndate' => '',
                'awayreason' => ''
            ];
        }

        // Set up user handler.
        $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

        // Set the data for the new user.
        $updated_user = [
            'uid' => $user->uid,
            'profile_fields' => $this->bb->getInput('profile_fields', []),
            'profile_fields_editable' => true,
            'website' => $this->bb->getInput('website', ''),
            'icq' => $this->bb->getInput('icq', ''),
            'aim' => $this->bb->getInput('aim', ''),
            'yahoo' => $this->bb->getInput('yahoo', ''),
            'skype' => $this->bb->getInput('skype', ''),
            'google' => $this->bb->getInput('google', ''),
            'signature' => $this->bb->getInput('signature', '', true),
            'usernotes' => $this->bb->getInput('usernotes', '', true),
            'away' => $away
        ];

        $updated_user['birthday'] = [
            'day' => $this->bb->getInput('birthday_day', 0),
            'month' => $this->bb->getInput('birthday_month', 0),
            'year' => $this->bb->getInput('birthday_year', 0)
        ];

        if (!empty($this->bb->input['usertitle'])) {
            $updated_user['usertitle'] = $this->bb->getInput('usertitle', '', true);
        } elseif (!empty($this->bb->input['reverttitle'])) {
            $updated_user['usertitle'] = '';
        }

        if (!empty($this->bb->input['remove_avatar'])) {
            $updated_user['avatarurl'] = '';
        }

        // Set the data of the user in the datahandler.
        $userhandler->set_data($updated_user);
        $this->errors = '';

        // Validate the user and get any errors that might have occurred.
        if (!$userhandler->validate_user()) {
            $this->errors = $userhandler->get_friendly_errors();
            return $this->index($request, $response, true);
        } else {
            // Are we removing an avatar from this user?
            if (!empty($this->bb->input['remove_avatar'])) {
                $extra_user_updates = [
                    'avatar' => '',
                    'avatardimensions' => '',
                    'avatartype' => ''
                ];
                $this->upload->remove_avatars($user->uid);
            }

            // Moderator 'Options' (suspend signature, suspend/moderate posting)
            $moderator_options = [
                1 => [
                    'action' => 'suspendsignature', // The moderator action we're performing
                    'period' => 'action_period', // The time period we've selected from the dropdown box
                    'time' => 'action_time', // The time we've entered
                    'update_field' => 'suspendsignature', // The field in the database to update if true
                    'update_length' => 'suspendsigtime' // The length of suspension field in the database
                ],
                2 => [
                    'action' => 'moderateposting',
                    'period' => 'modpost_period',
                    'time' => 'modpost_time',
                    'update_field' => 'moderateposts',
                    'update_length' => 'moderationtime'
                ],
                3 => [
                    'action' => 'suspendposting',
                    'period' => 'suspost_period',
                    'time' => 'suspost_time',
                    'update_field' => 'suspendposting',
                    'update_length' => 'suspensiontime'
                ]
            ];

            foreach ($moderator_options as $option) {
                $this->bb->input[$option['time']] = $this->bb->getInput($option['time'], 0);
                $this->bb->input[$option['period']] = $this->bb->getInput($option['period'], '');
                if (empty($this->bb->input[$option['action']])) {
                    if ($user->{$option['update_field']} == 1) {
                        // We're revoking the suspension
                        $extra_user_updates[$option['update_field']] = 0;
                        $extra_user_updates[$option['update_length']] = 0;
                    }

                    // Skip this option if we haven't selected it
                    continue;
                } else {
                    if ($this->bb->input[$option['time']] == 0 &&
                        $this->bb->input[$option['period']] != 'never' &&
                        $user->{$option['update_field']} != 1) {
                        // User has selected a type of ban, but not entered a valid time frame
                        $string = $option['action'] . '_error';
                        $this->errors[] = $this->lang->$string;
                    }

                    if (!is_array($this->errors)) {
                        $suspend_length = (new WarnHelper($this->db))->fetch_time_length((int)$this->bb->input[$option['time']], $this->bb->input[$option['period']]);

                        if ($user->{$option['update_field']} == 1 &&
                            ($this->bb->input[$option['time']] || $this->bb->input[$option['period']] == 'never')) {
                            // We already have a suspension, but entered a new time
                            if ($suspend_length == '-1') {
                                // Permanent ban on action
                                $extra_user_updates[$option['update_length']] = 0;
                            } elseif ($suspend_length && $suspend_length != '-1') {
                                // Temporary ban on action
                                $extra_user_updates[$option['update_length']] = TIME_NOW + $suspend_length;
                            }
                        } elseif (!$user->{$option['update_field']}) {
                            // New suspension for this user... bad user!
                            $extra_user_updates[$option['update_field']] = 1;
                            if ($suspend_length == '-1') {
                                $extra_user_updates[$option['update_length']] = 0;
                            } else {
                                $extra_user_updates[$option['update_length']] = TIME_NOW + $suspend_length;
                            }
                        }
                    }
                }
            }

            // Those with javascript turned off will be able to select both - cheeky!
            // Check to make sure we're not moderating AND suspending posting
            if (isset($extra_user_updates) && $extra_user_updates['moderateposts'] && $extra_user_updates['suspendposting']) {
                $this->errors[] = $this->lang->suspendmoderate_error;
            }

            if (is_array($this->errors)) {
                //$this->bb->input['action'] = 'editprofile';
                return $this->index($request, $response, true);
            } else {
                $this->plugins->runHooks('modcp_do_editprofile_update');

                // Continue with the update if there is no errors
                $user_info = $userhandler->update_user();
                if (!empty($extra_user_updates)) {
                    $this->db->update_query('users', $extra_user_updates, "uid='{$user->uid}'");
                }
                $this->bblogger->log_moderator_action(['uid' => $user->uid, 'username' => $user->username], $this->lang->edited_user);

                $this->plugins->runHooks('modcp_do_editprofile_end');

                $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/finduser', $this->lang->redirect_user_updated);
            }
        }
    }
}
