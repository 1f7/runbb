<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Finduser extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['caneditprofiles'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_users, $this->bb->settings['bburl'] . '/modcp/finduser');

        $perpage = $this->bb->getInput('perpage', 0);
        if (!$perpage || $perpage <= 0) {
            $perpage = $this->bb->settings['threadsperpage'];
        }
        $where = '';

        if (isset($this->bb->input['username'])) {
            switch ($this->db->type) {
                case 'mysql':
                case 'mysqli':
                    $field = 'username';
                    break;
                default:
                    $field = 'LOWER(username)';
                    break;
            }
            $where = " AND {$field} LIKE '%" . my_strtolower($this->db->escape_string_like($this->bb->getInput('username', '', true))) . "%'";
        }

        // Sort order & direction
        switch ($this->bb->getInput('sortby', '')) {
            case 'lastvisit':
                $sortby = 'lastvisit';
                break;
            case 'postnum':
                $sortby = 'postnum';
                break;
            case 'username':
                $sortby = 'username';
                break;
            default:
                $sortby = 'regdate';
        }
        $sortbysel = ['lastvisit' => '', 'postnum' => '', 'username' => '', 'regdate' => ''];
        $sortbysel[$this->bb->getInput('sortby', '')] = ' selected="selected"';
        $this->view->offsetSet('sortbysel', $sortbysel);

        $order = $this->bb->getInput('order', '');
        if ($order !== 'asc') {
            $order = 'desc';
        }
        $ordersel = ['asc' => '', 'desc' => ''];
        $ordersel[$order] = ' selected="selected"';
        $this->view->offsetSet('ordersel', $ordersel);

        $query = $this->db->simple_select('users', 'COUNT(uid) AS count', "1=1 {$where}");
        $user_count = $this->db->fetch_field($query, 'count');

        // Figure out if we need to display multiple pages.
        if ($this->bb->getInput('page', '') !== 'last') {
            $page = $this->bb->getInput('page', '');
        }

        $pages = $user_count / $perpage;
        $pages = ceil($pages);

        if ($this->bb->getInput('page', '') === 'last') {
            $page = $pages;
        }

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }
        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $page_url = $this->bb->settings['bburl'] . '/modcp/finduser';
        foreach (['username', 'sortby', 'order'] as $field) {
            $this->bb->input[$field] = urlencode($this->bb->getInput($field, ''));
            if (!empty($this->bb->input[$field])) {
                $page_url .= "&amp;{$field}=" . $this->bb->input[$field];
            }
        }

        $multipage = $this->pagination->multipage($user_count, $perpage, $page, $page_url);
        $this->view->offsetSet('multipage', $multipage);

        $usergroups_cache = $this->cache->read('usergroups');

        $this->plugins->runHooks('modcp_finduser_start');

        // Fetch out results
        $query = $this->db->simple_select(
            'users',
            '*',
            "1=1 {$where}",
            ['order_by' => $sortby, 'order_dir' => $order, 'limit' => $perpage, 'limit_start' => $start]
        );
        $users = [];
        while ($user = $this->db->fetch_array($query)) {
            //$alt_row = alt_trow();
            $user['username'] = $this->user->format_name($user['username'], $user['usergroup'], $user['displaygroup']);
            $user['postnum'] = $this->parser->formatNumber($user['postnum']);
            //$regdate = $this->time->formatDate('relative', $user->regdate);
            if ($user['invisible'] == 1 && $this->bb->usergroup['canviewwolinvis'] != 1 && $user['uid'] != $this->user->uid) {
                $lastdate = $this->lang->lastvisit_never;

                if ($user['lastvisit']) {
                    // We have had at least some active time, hide it instead
                    $lastdate = $this->lang->lastvisit_hidden;
                }
            } else {
                $lastdate = $this->time->formatDate('relative', $user['lastvisit']);
            }

            $users[] = [
                'alt_row' => alt_trow(),
                'uid' => $user['uid'],
                'username' => $user['username'],
                'usergroup' => htmlspecialchars_uni($usergroups_cache[$user['usergroup']]['title']),
                'regdate' => $this->time->formatDate('relative', $user['regdate']),
                'lastdate' => $lastdate,
                'postnum' => $user['postnum']
            ];
        }
        $this->view->offsetSet('users', $users);

        $this->plugins->runHooks('modcp_finduser_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/finduser.html.twig');
    }
}
