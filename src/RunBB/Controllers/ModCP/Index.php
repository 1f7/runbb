<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canmanagemodqueue'] == 1) {
            if ($this->bb->settings['enableattachments'] == 1 &&
                ($this->nummodqueueattach > 0 || $this->bb->usergroup['issupermod'] == 1)
            ) {
                if ($this->nummodqueueposts > 0 || $this->bb->usergroup['issupermod'] == 1) {
                    $bgcolor = 'trow1';
                } else {
                    $bgcolor = 'trow2';
                }
                $this->view->offsetSet('bgcolor', $bgcolor);

                $query = $this->db->query('
				SELECT COUNT(aid) AS unapprovedattachments
				FROM  ' . TABLE_PREFIX . 'attachments a
				LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
				LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
				WHERE a.visible='0' {$this->tflist}
			");
                $unapproved_attachments = $this->db->fetch_field($query, 'unapprovedattachments');
                $attachment = [];
                if ($unapproved_attachments > 0) {
                    $query = $this->db->query('
					SELECT t.tid, p.pid, p.uid, t.username, a.filename, a.dateuploaded
					FROM  ' . TABLE_PREFIX . 'attachments a
					LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
					LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
					WHERE a.visible='0' {$this->tflist}
					ORDER BY a.dateuploaded DESC
					LIMIT 1
				");
                    $attachment = $this->db->fetch_array($query);
                    $attachment['date'] = $this->time->formatDate('relative', $attachment['dateuploaded']);
                    $attachment['profilelink'] = $this->user->build_profile_link($attachment['username'], $attachment['uid']);
                    $attachment['link'] = get_post_link($attachment['pid'], $attachment['tid']);
                    $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                    $unapproved_attachments = $this->parser->formatNumber($unapproved_attachments);

                    eval("\$latest_attachment = \"" . $this->templates->get('modcp_lastattachment') . "\";");
                }
//        else
//        {
//          //ev al("\$latest_attachment = \"".$this->templates->get('modcp_awaitingmoderation_none')."\";");
//        }
                $this->view->offsetSet('unapproved_attachments', $unapproved_attachments);
                $this->view->offsetSet('attachment', $attachment);

                //ev al("\$awaitingattachments = \"".$this->templates->get('modcp_awaitingattachments')."\";");
            }

            if ($this->nummodqueueposts > 0 || $this->bb->usergroup['issupermod'] == 1) {
                $query = $this->db->query('
				SELECT COUNT(pid) AS unapprovedposts
				FROM  ' . TABLE_PREFIX . 'posts p
				LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
				WHERE p.visible='0' {$this->tflist} AND t.firstpost != p.pid
			");
                $unapproved_posts = $this->db->fetch_field($query, 'unapprovedposts');

                $post = [];
                if ($unapproved_posts > 0) {
                    $query = $this->db->query('
					SELECT p.pid, p.tid, p.subject, p.uid, p.username, p.dateline
					FROM  ' . TABLE_PREFIX . 'posts p
					LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
					WHERE p.visible='0' {$this->tflist} AND t.firstpost != p.pid
					ORDER BY p.dateline DESC
					LIMIT 1
				");
                    $post = $this->db->fetch_array($query);
                    $post['date'] = $this->time->formatDate('relative', $post['dateline']);
                    $post['profilelink'] = $this->user->build_profile_link($post['username'], $post['uid']);
                    $post['link'] = get_post_link($post['pid'], $post['tid']);
                    $post['subject'] = $post['fullsubject'] = $this->parser->parse_badwords($post['subject']);
                    if (my_strlen($post['subject']) > 25) {
                        $post['subject'] = my_substr($post['subject'], 0, 25) . '...';
                    }
                    $post['subject'] = htmlspecialchars_uni($post['subject']);
                    $post['fullsubject'] = htmlspecialchars_uni($post['fullsubject']);
                    $unapproved_posts = $this->parser->formatNumber($unapproved_posts);

                    //ev al("\$latest_post = \"".$this->templates->get('modcp_lastpost')."\";");
                }
//        else
//        {
//          //ev al("\$latest_post = \"".$this->templates->get('modcp_awaitingmoderation_none')."\";");
//        }
                $this->view->offsetSet('unapproved_posts', $unapproved_posts);
                $this->view->offsetSet('post', $post);

                //ev al("\$awaitingposts = \"".$this->templates->get('modcp_awaitingposts')."\";");
            }

            if ($this->nummodqueuethreads > 0 || $this->bb->usergroup['issupermod'] == 1) {
                $query = $this->db->simple_select('threads', 'COUNT(tid) AS unapprovedthreads', "visible='0' {$this->flist_queue_threads}");
                $unapproved_threads = $this->db->fetch_field($query, 'unapprovedthreads');
                $thread = [];
                if ($unapproved_threads > 0) {
                    $query = $this->db->simple_select('threads', 'tid, subject, uid, username, dateline', "visible='0' {$this->flist_queue_threads}", ['order_by' => 'dateline', 'order_dir' => 'DESC', 'limit' => 1]);
                    $thread = $this->db->fetch_array($query);
                    $thread['date'] = $this->time->formatDate('relative', $thread['dateline']);
                    $thread['profilelink'] = $this->user->build_profile_link($thread['username'], $thread['uid']);
                    $thread['link'] = get_thread_link($thread['tid']);
                    $thread['subject'] = $thread['fullsubject'] = $this->parser->parse_badwords($thread['subject']);
                    if (my_strlen($thread['subject']) > 25) {
                        $post['subject'] = my_substr($thread['subject'], 0, 25) . '...';
                    }
                    $thread['subject'] = htmlspecialchars_uni($thread['subject']);
                    $thread['fullsubject'] = htmlspecialchars_uni($thread['fullsubject']);
                    $unapproved_threads = $this->parser->formatNumber($unapproved_threads);
                    //ev al("\$latest_thread = \"".$this->templates->get('modcp_lastthread')."\";");
                }
//        else
//        {
//          //ev al("\$latest_thread = \"".$this->templates->get('modcp_awaitingmoderation_none')."\";");
//        }
                $this->view->offsetSet('unapproved_threads', $unapproved_threads);
                $this->view->offsetSet('thread', $thread);
                //ev al("\$awaitingthreads = \"".$this->templates->get('modcp_awaitingthreads')."\";");
            }

//      if(!empty($awaitingattachments) || !empty($awaitingposts) || !empty($awaitingthreads))
//      {
//        //ev al("\$awaitingmoderation = \"".$this->templates->get('modcp_awaitingmoderation')."\";");
//      }
        }

        $this->bb->showLatestModActions = false;
        $modlogresults = [];
        if (($this->nummodlogs > 0 || $this->bb->usergroup['issupermod'] == 1) && $this->bb->usergroup['canviewmodlogs'] == 1) {
            $this->bb->showLatestModActions = true;
            $where = '';
            if ($this->tflist_modlog) {
                $where = "WHERE (t.fid <> 0 {$this->tflist_modlog}) OR (!l.fid)";
            }

            $query = $this->db->query('
			SELECT l.*, u.username, u.usergroup, u.displaygroup, t.subject AS tsubject, f.name AS fname, p.subject AS psubject
			FROM ' . TABLE_PREFIX . 'moderatorlog l
			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=l.uid)
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=l.tid)
			LEFT JOIN ' . TABLE_PREFIX . 'forums f ON (f.fid=l.fid)
			LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=l.pid)
			{$where}
			ORDER BY l.dateline DESC
			LIMIT 5
		");

            $modlogresults = '';
            while ($logitem = $this->db->fetch_array($query)) {
                $information = '';
                $logitem['action'] = htmlspecialchars_uni($logitem['action']);
                //$log_date = $this->time->formatDate('relative', $logitem['dateline']);
                //$trow = alt_trow();
                $username = $this->user->format_name($logitem['username'], $logitem['usergroup'], $logitem['displaygroup']);
                $logitem['profilelink'] = $this->user->build_profile_link($username, $logitem['uid']);

                if ($logitem['tsubject']) {
                    $logitem['tsubject'] = htmlspecialchars_uni($logitem['tsubject']);
                    $logitem['thread'] = get_thread_link($logitem['tid']);
                    $information .= "<strong>{$this->lang->thread}:</strong> <a href=\"{$logitem['thread']}\" target=\"_blank\">{$logitem['tsubject']}</a><br />";
                    //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_thread')."\";");
                }
                if ($logitem['fname']) {
                    $logitem['forum'] = $this->forum->get_forum_link($logitem['fid']);
                    $information .= "<strong>{$this->lang->forum}:</strong> <a href=\"{$logitem['forum']}\" target=\"_blank\">{$logitem['fname']}</a><br />";
                    //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_forum')."\";");
                }
                if ($logitem['psubject']) {
                    $logitem['psubject'] = htmlspecialchars_uni($logitem['psubject']);
                    $logitem['post'] = get_post_link($logitem['pid']);
                    $information .= "<strong>{$this->lang->post}:</strong> <a href=\"{$logitem['post']}#pid{$logitem['pid']}\">{$logitem['psubject']}</a>";
                    //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_post')."\";");
                }

                // Edited a user or managed announcement?
                if (!$logitem['tsubject'] || !$logitem['fname'] || !$logitem['psubject']) {
                    $data = my_unserialize($logitem['data']);
                    if (!empty($data['uid'])) {
                        $information = $this->lang->sprintf($this->lang->edited_user_info, htmlspecialchars_uni($data['username']), get_profile_link($data['uid']));
                    }
                    if (!empty($data['aid'])) {
                        $data['subject'] = htmlspecialchars_uni($data['subject']);
                        $data['announcement'] = get_announcement_link($data['aid']);
                        $information .= "<strong>{$this->lang->announcement}:</strong> <a href=\"{$data['announcement']}\" target=\"_blank\">{$data['subject']}</a>";
                        //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_announcement')."\";");
                    }
                }
                $modlogresults[] = [
                    'trow' => alt_trow(),
                    'profilelink' => $logitem['profilelink'],
                    'log_date' => $this->time->formatDate('relative', $logitem['dateline']),
                    'action' => $logitem['action'],
                    'information' => $information,
                    'ipaddress' => $logitem['ipaddress']
                ];
                //ev al("\$modlogresults .= \"".$this->templates->get('modcp_modlogs_result')."\";");
            }

//      if(!$modlogresults)
//      {
//        //ev al("\$modlogresults = \"".$this->templates->get('modcp_modlogs_nologs')."\";");
//      }
            //ev al("\$latestfivemodactions = \"".$this->templates->get('modcp_latestfivemodactions')."\";");
        }
        $this->view->offsetSet('modlogresults', $modlogresults);

        $query = $this->db->query('
		SELECT b.*, a.username AS adminuser, u.username
		FROM ' . TABLE_PREFIX . 'banned b
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (b.uid=u.uid)
		LEFT JOIN ' . TABLE_PREFIX . "users a ON (b.admin=a.uid)
		WHERE b.bantime != '---' AND b.bantime != 'perm'
		ORDER BY lifted ASC
		LIMIT 5
	");

        $banned_cache = [];
        while ($banned = $this->db->fetch_array($query)) {
            $banned['remaining'] = $banned['lifted'] - TIME_NOW;
            $banned_cache[$banned['remaining'] . $banned['uid']] = $banned;
            unset($banned);
        }

        // Get the banned users
        $bannedusers = [];
        foreach ($banned_cache as $banned) {
            //$profile_link = $this->user->build_profile_link($banned['username'], $banned['uid']);

            // Only show the edit & lift links if current user created ban, or is super mod/admin
            $edit_link = '';
            if ($this->user->uid == $banned['admin'] || !$banned['adminuser'] ||
                $this->bb->usergroup['issupermod'] == 1 || $this->bb->usergroup['cancp'] == 1
            ) {
                $edit_link = "<br /><span class=\"smalltext\"><a href=\"{$this->bb->settings['bburl']}/modcp/banuser?uid={$banned['uid']}\">{$this->lang->edit_ban}</a> | <a href=\"{$this->bb->settings['bburl']}/modcp/liftban?uid={$banned['uid']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->lift_ban}</a></span>";
                //ev al("\$edit_link = \"".$this->templates->get('modcp_banning_edit')."\";");
            }
            //$admin_profile = $this->user->build_profile_link($banned['adminuser'], $banned['admin']);
            //$trow = alt_trow();
            if ($banned['reason']) {
                $banned['reason'] = htmlspecialchars_uni($this->parser->parse_badwords($banned['reason']));
            } else {
                $banned['reason'] = $this->lang->na;
            }

            if ($banned['lifted'] == 'perm' ||
                $banned['lifted'] == '' ||
                $banned['bantime'] == 'perm' ||
                $banned['bantime'] == '---'
            ) {
                $banlength = $this->lang->permanent;
                $timeremaining = $this->lang->na;
            } else {
                $banlength = $this->bantimes[$banned['bantime']];
                $remaining = $banned['remaining'];
                $timeremaining = $this->time->niceTime($remaining, ['short' => 1, 'seconds' => false]);

                if ($remaining <= 0) {
                    $timeremaining = "<span style=\"color: red;\">({$this->lang->ban_ending_imminently})</span>";
                } elseif ($remaining < 3600) {
                    $timeremaining = "<span style=\"color: red;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } elseif ($remaining < 86400) {
                    $timeremaining = "<span style=\"color: maroon;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } elseif ($remaining < 604800) {
                    $timeremaining = "<span style=\"color: green;\">({$timeremaining} {$this->lang->ban_remaining})</span>";
                } else {
                    $timeremaining = "({$timeremaining} {$this->lang->ban_remaining})";
                }
            }
            $bannedusers[] = [
                'trow' => alt_trow(),
                'profile_link' => $this->user->build_profile_link($banned['username'], $banned['uid']),
                'edit_link' => $edit_link,
                'reason' => $banned['reason'],
                'banlength' => $banlength,
                'timeremaining' => $timeremaining,
                'admin_profile' => $this->user->build_profile_link($banned['adminuser'], $banned['admin'])
            ];
            //ev al("\$bannedusers .= \"".$this->templates->get('modcp_banning_ban')."\";");
        }
        $this->view->offsetSet('bannedusers', $bannedusers);

//    if(!$bannedusers)
//    {
//      //ev al("\$bannedusers = \"".$this->templates->get('modcp_nobanned')."\";");
//    }

        $modnotes = $this->cache->read('modnotes');
        $modnotes = htmlspecialchars_uni($modnotes['modmessage']);
        $this->view->offsetSet('modnotes', $modnotes);

        $this->plugins->runHooks('modcp_end');

        //ev al('\$modcp = \''.$this->templates->get('modcp').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/index.html.twig');
    }
}
