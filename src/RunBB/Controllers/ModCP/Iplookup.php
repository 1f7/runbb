<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Iplookup extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canuseipsearch'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->input['ipaddress'] = $this->bb->getInput('ipaddress', '');
        $this->lang->ipaddress_misc_info = $this->lang->sprintf($this->lang->ipaddress_misc_info, htmlspecialchars_uni($this->bb->input['ipaddress']));
        $ipaddress_location = $this->lang->na;
        $ipaddress_host_name = $this->lang->na;
        //$modcp_ipsearch_misc_info = '';
        if (!strstr($this->bb->input['ipaddress'], '*')) {
            // Return GeoIP information if it is available to us
            if (function_exists('geoip_record_by_name')) {
                $ip_record = @geoip_record_by_name($this->bb->input['ipaddress']);
                if ($ip_record) {
                    $ipaddress_location = htmlspecialchars_uni(utf8_encode($ip_record['country_name']));
                    if ($ip_record['city']) {
                        $ipaddress_location .= $this->lang->comma.htmlspecialchars_uni(utf8_encode($ip_record['city']));
                    }
                }
            }

            $ipaddress_host_name = htmlspecialchars_uni(@gethostbyaddr($this->bb->input['ipaddress']));

            // gethostbyaddr returns the same ip on failure
            if ($ipaddress_host_name == $this->bb->input['ipaddress']) {
                $ipaddress_host_name = $this->lang->na;
            }
        }

        $this->view->offsetSet('ipaddress_host_name', $ipaddress_host_name);
        $this->view->offsetSet('ipaddress_location', $ipaddress_location);

        $this->plugins->runHooks('modcp_iplookup_end');

        //ev al('\$iplookup = \''.$templates->get('modcp_ipsearch_misc_info', 1, 0).'\';');
        //echo($iplookup);
        $this->view->render($response, '@forum/ModCP/ipsearch_modal.html.twig');
        //exit;
    }
}
