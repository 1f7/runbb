<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Ipsearch extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canuseipsearch'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_ipsearch, $this->bb->settings['bburl'] . '/modcp/ipsearch');

        $this->bb->input['ipaddress'] = $this->bb->getInput('ipaddress', '');
        if ($this->bb->input['ipaddress']) {
            if (!is_array($this->bb->groupscache)) {
                $this->bb->groupscache = $this->cache->read('usergroups');
            }

            $ipaddressvalue = htmlspecialchars_uni($this->bb->input['ipaddress']);
            $this->view->offsetSet('ipaddressvalue', $ipaddressvalue);

            $ip_range = fetch_ip_range($this->bb->input['ipaddress']);

            $post_results = $user_results = 0;

            // Searching post IP addresses
            if (isset($this->bb->input['search_posts'])) {
                $post_ip_sql = '';
                if ($ip_range) {
                    if (!is_array($ip_range)) {
                        $post_ip_sql = 'p.ipaddress=' . $ip_range;
                    } else {
                        $post_ip_sql = 'p.ipaddress BETWEEN ' . $ip_range[0] . ' AND ' . $ip_range[1];
                    }
                }

                $this->plugins->runHooks('modcp_ipsearch_posts_start');

                if ($post_ip_sql) {
                    $where_sql = '';

                    $unviewable_forums = $this->forum->get_unviewable_forums(true);

                    if ($unviewable_forums) {
                        $where_sql .= " AND p.fid NOT IN ({$unviewable_forums})";
                    }

                    if ($this->inactiveforums) {
                        $where_sql .= " AND p.fid NOT IN ({$this->inactiveforums})";
                    }

                    // Check group permissions if we can't view threads not started by us
                    $onlyusfids = [];
                    $group_permissions = $this->forum->forum_permissions();
                    foreach ($group_permissions as $fid => $forumpermissions) {
                        if (isset($forumpermissions['canonlyviewownthreads']) && $forumpermissions['canonlyviewownthreads'] == 1) {
                            $onlyusfids[] = $fid;
                        }
                    }

                    if (!empty($onlyusfids)) {
                        $where_sql .= " AND ((t.fid IN(" . implode(',', $onlyusfids) . ") AND t.uid='{$this->user->uid}') OR t.fid NOT IN(" . implode(',', $onlyusfids) . '))';
                    }

                    // Moderators can view unapproved/deleted posts
                    if ($this->bb->usergroup['issupermod'] != 1) {
                        $unapprove_forums = [];
                        $deleted_forums = [];
                        $visible_sql = ' AND (p.visible = 1 AND t.visible = 1)';
                        $query = $this->db->simple_select(
                            'moderators',
                            'fid, canviewunapprove, canviewdeleted',
                            "(id='{$this->user->uid}' AND isgroup='0') OR (id='{$this->user->usergroup}' AND isgroup='1')"
                        );
                        while ($moderator = $this->db->fetch_array($query)) {
                            if ($moderator['canviewunapprove'] == 1) {
                                $unapprove_forums[] = $moderator['fid'];
                            }

                            if ($moderator['canviewdeleted'] == 1) {
                                $deleted_forums[] = $moderator['fid'];
                            }
                        }

                        if (!empty($unapprove_forums)) {
                            $visible_sql .= ' OR (p.visible = 0 AND p.fid IN(' . implode(',', $unapprove_forums) . ')) OR (t.visible = 0 AND t.fid IN(' . implode(',', $unapprove_forums) . '))';
                        }
                        if (!empty($deleted_forums)) {
                            $visible_sql .= ' OR (p.visible = -1 AND p.fid IN(' . implode(',', $deleted_forums) . ')) OR (t.visible = -1 AND t.fid IN(' . implode(',', $deleted_forums) . '))';
                        }
                    } else {
                        // Super moderators (and admins)
                        $visible_sql = ' AND p.visible >= -1';
                    }

                    $query = $this->db->query('
					SELECT COUNT(p.pid) AS count
					FROM ' . TABLE_PREFIX . 'posts p
					LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid = p.tid)
					WHERE {$post_ip_sql}{$where_sql}{$visible_sql}
				");
                    $post_results = $this->db->fetch_field($query, 'count');
                }
            }

            // Searching user IP addresses
            if (isset($this->bb->input['search_users'])) {
                $user_ip_sql = '';
                if ($ip_range) {
                    if (!is_array($ip_range)) {
                        $user_ip_sql = 'regip=' . $ip_range . ' OR lastip=' . $ip_range;
                    } else {
                        $user_ip_sql = 'regip BETWEEN ' . $ip_range[0] . ' AND ' . $ip_range[1] . ' OR lastip BETWEEN ' . $ip_range[0] . ' AND ' . $ip_range[1];
                    }
                }

                $this->plugins->runHooks('modcp_ipsearch_users_start');

                if ($user_ip_sql) {
                    $query = $this->db->simple_select('users', 'COUNT(uid) AS count', $user_ip_sql);

                    $user_results = $this->db->fetch_field($query, 'count');
                }
            }

            $total_results = $post_results + $user_results;

            if (!$total_results) {
                $total_results = 1;
            }

            // Now we have the result counts, paginate
            $perpage = $this->bb->getInput('perpage', 0);
            if (!$perpage || $perpage <= 0) {
                $perpage = $this->bb->settings['threadsperpage'];
            }

            // Figure out if we need to display multiple pages.
            if ($this->bb->getInput('page', '') !== 'last') {
                $page = $this->bb->getInput('page', 0);
            }

            $pages = $total_results / $perpage;
            $pages = ceil($pages);

            if ($this->bb->getInput('page', '') === 'last') {
                $page = $pages;
            }

            if ($page > $pages || $page <= 0) {
                $page = 1;
            }

            if ($page) {
                $start = ($page - 1) * $perpage;
            } else {
                $start = 0;
                $page = 1;
            }

            $page_url = $this->bb->settings['bburl'] . '/modcp/ipsearch?perpage=' . $perpage;
            foreach (['ipaddress', 'search_users', 'search_posts'] as $input) {
                if (!empty($this->bb->input[$input])) {
                    $page_url .= "&amp;{$input}=" . urlencode($this->bb->input[$input]);
                }
            }
            $multipage = $this->pagination->multipage($total_results, $perpage, $page, $page_url);
            $this->view->offsetSet('multipage', $multipage);

            $post_limit = $perpage;
            $results = [];
            if (isset($this->bb->input['search_users']) && $user_results && $start <= $user_results) {
                $query = $this->db->simple_select(
                    'users',
                    'username, uid, regip, lastip',
                    $user_ip_sql,
                    ['order_by' => 'regdate', 'order_dir' => 'DESC', 'limit_start' => $start, 'limit' => $perpage]
                );

                while ($ipaddress = $this->db->fetch_array($query)) {
                    $result = false;
                    $profile_link = $this->user->build_profile_link($ipaddress['username'], $ipaddress['uid']);
                    //$trow = alt_trow();
                    $ip = false;
                    if (is_array($ip_range)) {
                        if (strcmp($ip_range[0], $ipaddress['regip']) <= 0 && strcmp($ip_range[1], $ipaddress['regip']) >= 0) {
                            $subject = "<strong>{$this->lang->ipresult_regip}</strong> {$profile_link}";
                            //ev al("\$subject = \"".$this->templates->get('modcp_ipsearch_result_regip')."\";");
                            $ip = $ipaddress['regip'];
                        } elseif (strcmp($ip_range[0], $ipaddress['lastip']) <= 0 && strcmp($ip_range[1], $ipaddress['lastip']) >= 0) {
                            $subject = "<strong>{$this->lang->ipresult_lastip}</strong> {$profile_link}";
                            //ev al("\$subject = \"".$this->templates->get("modcp_ipsearch_result_lastip")."\";");
                            $ip = $ipaddress['lastip'];
                        }
                    } elseif ($ipaddress['regip'] == $ip_range) {
                        $subject = "<strong>{$this->lang->ipresult_regip}</strong> {$profile_link}";
                        //ev al("\$subject = \"".$this->templates->get("modcp_ipsearch_result_regip")."\";");
                        $ip = $ipaddress['regip'];
                    } elseif ($ipaddress['lastip'] == $ip_range) {
                        $subject = "<strong>{$this->lang->ipresult_lastip}</strong> {$profile_link}";
                        //ev al("\$subject = \"".$this->templates->get("modcp_ipsearch_result_lastip")."\";");
                        $ip = $ipaddress['lastip'];
                    }
                    if ($ip) {
                        $results[] = [
                            'trow' => alt_trow(),
                            'ip' => $ip,
                            'subject' => $subject
                        ];
                        //ev al("\$results .= \"".$this->templates->get("modcp_ipsearch_result")."\";");
                        $result = true;
                    }
                    if ($result) {
                        --$post_limit;
                    }
                }
            }
            $post_start = 0;
            if ($total_results > $user_results && $post_limit) {
                $post_start = $start - $user_results;
                if ($post_start < 0) {
                    $post_start = 0;
                }
            }
            if (isset($this->bb->input['search_posts']) &&
                $post_results &&
                (!isset($this->bb->input['search_users']) ||
                    (isset($this->bb->input['search_users']) && $post_limit > 0))
            ) {
                $ipaddresses = $tids = $uids = [];

                $query = $this->db->query('
				SELECT p.username AS postusername, p.uid, p.subject, p.pid, p.tid, p.ipaddress
				FROM ' . TABLE_PREFIX . 'posts p
				LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid = p.tid)
				WHERE {$post_ip_sql}{$where_sql}{$visible_sql}
				ORDER BY p.dateline desc
				LIMIT {$post_start}, {$post_limit}
			");
                while ($ipaddress = $this->db->fetch_array($query)) {
                    $tids[$ipaddress['tid']] = $ipaddress['pid'];
                    $uids[$ipaddress['uid']] = $ipaddress['pid'];
                    $ipaddresses[$ipaddress['pid']] = $ipaddress;
                }

                if (!empty($ipaddresses)) {
                    $query = $this->db->simple_select('threads', 'subject, tid', 'tid IN(' . implode(',', array_keys($tids)) . ')');
                    while ($thread = $this->db->fetch_array($query)) {
                        $ipaddresses[$tids[$thread['tid']]]['threadsubject'] = $thread['subject'];
                    }
                    unset($tids);

                    $query = $this->db->simple_select('users', 'username, uid', 'uid IN(' . implode(',', array_keys($uids)) . ')');
                    while ($user = $this->db->fetch_array($query)) {
                        $ipaddresses[$uids[$user['uid']]]['username'] = $user['username'];
                    }
                    unset($uids);

                    foreach ($ipaddresses as $ipaddress) {
                        //$ip = $ipaddress['ipaddress'];
                        if (empty($ipaddress['username'])) {
                            $ipaddress['username'] = $ipaddress['postusername']; // Guest username support
                        }
                        //$trow = alt_trow();
                        if (!$ipaddress['subject']) {
                            $ipaddress['subject'] = "RE: {$ipaddress['threadsubject']}";
                        }

                        $ipaddress['postlink'] = get_post_link($ipaddress['pid'], $ipaddress['tid']);
                        $ipaddress['subject'] = htmlspecialchars_uni($ipaddress['subject']);
                        $ipaddress['profilelink'] = $this->user->build_profile_link($ipaddress['username'], $ipaddress['uid']);

                        $subject = "<strong>{$this->lang->ipresult_post}</strong> <a href=\"{$ipaddress['postlink']}\">{$ipaddress['subject']}</a> {$this->lang->by} {$ipaddress['profilelink']}";
                        //ev al("\$subject = \"".$this->templates->get("modcp_ipsearch_result_post")."\";");
                        $results[] = [
                            'trow' => alt_trow(),
                            'ip' => $ipaddress['ipaddress'],
                            'subject' => $subject
                        ];
                        //ev al("\$results .= \"".$this->templates->get("modcp_ipsearch_result")."\";");
                    }
                }
            }
            $this->view->offsetSet('results', $results);

//      if(!$results)
//      {
//        //ev al("\$results = \"".$this->templates->get("modcp_ipsearch_noresults")."\";");
//      }

            if ($ipaddressvalue) {
                $this->lang->ipsearch_results = $this->lang->sprintf($this->lang->ipsearch_results, $ipaddressvalue);
            } else {
                $this->lang->ipsearch_results = $this->lang->ipsearch;
            }

            $ipaddress = '';//$ipaddress_url = $misc_info_link = '';
            if (!strstr($this->bb->input['ipaddress'], '*') && !strstr($this->bb->input['ipaddress'], '/')) {
                $ipaddress = htmlspecialchars_uni($this->bb->input['ipaddress']);
                //$ipaddress_url = urlencode($this->bb->input['ipaddress']);
                //ev al("\$misc_info_link = \"".$this->templates->get("modcp_ipsearch_results_information")."\";");
            }
            $this->view->offsetSet('ipaddress', $ipaddress);

            //ev al("\$ipsearch_results = \"".$this->templates->get("modcp_ipsearch_results")."\";");
        }

        // Fetch filter options
        if (!$this->bb->input['ipaddress']) {
            $this->bb->input['search_posts'] = 1;
            $this->bb->input['search_users'] = 1;
        }
        $usersearchselect = $postsearchselect = '';
        if (isset($this->bb->input['search_posts'])) {
            $postsearchselect = ' checked="checked"';
        }
        if (isset($this->bb->input['search_users'])) {
            $usersearchselect = ' checked="checked"';
        }
        $this->view->offsetSet('usersearchselect', $usersearchselect);
        $this->view->offsetSet('postsearchselect', $postsearchselect);

        $this->plugins->runHooks('modcp_ipsearch_end');

        //ev al('\$ipsearch = \''.$this->templates->get('modcp_ipsearch').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/ipsearch.html.twig');
    }
}
