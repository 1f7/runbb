<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;
use RunBB\Core\Logger;

class Liftban extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() == 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canbanusers'] == 0) {
            return $this->bb->error_no_permission();
        }

        $query = $this->db->simple_select('banned', '*', "uid='".$this->bb->getInput('uid', 0)."'");
        $ban = $this->db->fetch_array($query);

        if (!$ban) {
            $this->bb->error($this->lang->error_invalidban);
        }

        // Permission to edit this ban?
        if ($this->user->uid != $ban['admin'] && $this->bb->usergroup['issupermod'] != 1 && $this->bb->usergroup['cancp'] != 1) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('modcp_liftban_start');

        $query = $this->db->simple_select('users', 'username', "uid = '{$ban['uid']}'");
        $username = $this->db->fetch_field($query, 'username');

        $updated_group = [
        'usergroup' => $ban['oldgroup'],
        'additionalgroups' => $ban['oldadditionalgroups'],
        'displaygroup' => $ban['olddisplaygroup']
        ];
        $this->db->update_query('users', $updated_group, "uid='{$ban['uid']}'");
        $this->db->delete_query('banned', "uid='{$ban['uid']}'");

        $this->cache->update_banned();
        $this->cache->update_moderators();
        $this->bblogger->log_moderator_action(['uid' => $ban['uid'], 'username' => $username], $this->lang->lifted_ban);

        $this->plugins->runHooks('modcp_liftban_end');

        $this->bb->redirect($this->bb->settings['bburl'].'/modcp/banning', $this->lang->redirect_banlifted);
    }
}
