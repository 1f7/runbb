<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Modlogs extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canviewmodlogs'] == 0) {
            return $this->bb->error_no_permission();
        }

        if ($this->nummodlogs == 0 && $this->bb->usergroup['issupermod'] != 1) {
            $this->bb->error($this->lang->you_cannot_view_mod_logs);
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_modlogs, 'modcp.php?action=modlogs');

        $perpage = $this->bb->getInput('perpage', 0);
        if (!$perpage || $perpage <= 0) {
            $perpage = $this->bb->settings['threadsperpage'];
        }
        $this->view->offsetSet('perpage', $perpage);

        $where = '';

        // Searching for entries by a particular user
        if ($this->bb->getInput('uid', 0)) {
            $where .= " AND l.uid='" . $this->bb->getInput('uid', 0) . "'";
        }

        // Searching for entries in a specific forum
        if ($this->bb->getInput('fid', 0)) {
            $where .= " AND t.fid='" . $this->bb->getInput('fid', 0) . "'";
        }

        $this->bb->input['sortby'] = $this->bb->getInput('sortby', '');

        // Order?
        switch ($this->bb->input['sortby']) {
            case 'username':
                $sortby = 'u.username';
                break;
            case 'forum':
                $sortby = 'f.name';
                break;
            case 'thread':
                $sortby = 't.subject';
                break;
            default:
                $sortby = 'l.dateline';
        }
        $order = $this->bb->getInput('order', '');
        if ($order !== 'asc') {
            $order = 'desc';
        }

        $this->plugins->runHooks('modcp_modlogs_start');

        $query = $this->db->query('
		SELECT COUNT(l.dateline) AS count
		FROM ' . TABLE_PREFIX . 'moderatorlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=l.uid)
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=l.tid)
		WHERE 1=1 {$where}{$this->tflist_modlog}
	");
        $rescount = $this->db->fetch_field($query, 'count');

        // Figure out if we need to display multiple pages.
        if ($this->bb->getInput('page') != 'last') {
            $page = $this->bb->getInput('page', 0);
        }

        $postcount = (int)$rescount;
        $pages = $postcount / $perpage;
        $pages = ceil($pages);

        if ($this->bb->getInput('page', '') === 'last') {
            $page = $pages;
        }

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }

        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $page_url = $this->bb->settings['bburl'].'/modcp/modlogs?perpage=' . $perpage;
        foreach (['uid', 'fid'] as $field) {
            $this->bb->input[$field] = $this->bb->getInput($field, 0);
            if (!empty($this->bb->input[$field])) {
                $page_url .= "&amp;{$field}=" . $this->bb->input[$field];
            }
        }
        foreach (['sortby', 'order'] as $field) {
            $this->bb->input[$field] = htmlspecialchars_uni($this->bb->getInput($field, ''));
            if (!empty($this->bb->input[$field])) {
                $page_url .= "&amp;{$field}=" . $this->bb->input[$field];
            }
        }

        $multipage = $this->pagination->multipage($postcount, $perpage, $page, $page_url);
        $this->view->offsetSet('multipage', $multipage);

        $query = $this->db->query('
		SELECT l.*, u.username, u.usergroup, u.displaygroup, t.subject AS tsubject, f.name AS fname, p.subject AS psubject
		FROM ' . TABLE_PREFIX . 'moderatorlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=l.uid)
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=l.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'forums f ON (f.fid=l.fid)
		LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=l.pid)
		WHERE 1=1 {$where}{$this->tflist_modlog}
		ORDER BY {$sortby} {$order}
		LIMIT {$start}, {$perpage}
	");
        $results = [];
        while ($logitem = $this->db->fetch_array($query)) {
            $information = '';
            $logitem['action'] = htmlspecialchars_uni($logitem['action']);
            //$log_date = $this->time->formatDate('relative', $logitem['dateline']);
            //$trow = alt_trow();
            $username = $this->user->format_name($logitem['username'], $logitem['usergroup'], $logitem['displaygroup']);
            $logitem['profilelink'] = $this->user->build_profile_link($username, $logitem['uid']);

            if ($logitem['tsubject']) {
                $logitem['tsubject'] = htmlspecialchars_uni($logitem['tsubject']);
                $logitem['thread'] = get_thread_link($logitem['tid']);
                $information .= "<strong>{$this->lang->thread}:</strong> <a href=\"{$logitem['thread']}\" target=\"_blank\">{$logitem['tsubject']}</a><br />";
                //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_thread')."\";");
            }
            if ($logitem['fname']) {
                $logitem['forum'] = $this->forum->get_forum_link($logitem['fid']);
                $information .= "<strong>{$this->lang->forum}:</strong> <a href=\"{$logitem['forum']}\" target=\"_blank\">{$logitem['fname']}</a><br />";
                //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_forum')."\";");
            }
            if ($logitem['psubject']) {
                $logitem['psubject'] = htmlspecialchars_uni($logitem['psubject']);
                $logitem['post'] = get_post_link($logitem['pid']);
                $information .= "<strong>{$this->lang->post}:</strong> <a href=\"{$logitem['post']}#pid{$logitem['pid']}\">{$logitem['psubject']}</a>";
                //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_post')."\";");
            }

            // Edited a user or managed announcement?
            if (!$logitem['tsubject'] || !$logitem['fname'] || !$logitem['psubject']) {
                $data = my_unserialize($logitem['data']);
                if (!empty($data['uid'])) {
                    $information = $this->lang->sprintf($this->lang->edited_user_info, htmlspecialchars_uni($data['username']), get_profile_link($data['uid']));
                }
                if (!empty($data['aid'])) {
                    $data['subject'] = htmlspecialchars_uni($data['subject']);
                    $data['announcement'] = get_announcement_link($data['aid']);
                    $information .= "<strong>{$this->lang->announcement}:</strong> <a href=\"{$data['announcement']}\" target=\"_blank\">{$data['subject']}</a>";
                    //ev al("\$information .= \"".$this->templates->get('modcp_modlogs_result_announcement')."\";");
                }
            }
            $results[] = [
                'trow' => alt_trow(),
                'logitem' => $logitem,
                'log_date' => $this->time->formatDate('relative', $logitem['dateline']),
                'information' => $information
            ];
        }
        $this->view->offsetSet('results', $results);

        $this->plugins->runHooks('modcp_modlogs_filter');

        // Fetch filter options
        $sortbysel = ['username' => '', 'forum' => '', 'thread' => '', 'dateline' => ''];
        $sortbysel[$this->bb->input['sortby']] = ' selected="selected"';
        $this->view->offsetSet('sortbysel', $sortbysel);

        $ordersel = ['asc' => '', 'desc' => ''];
        $ordersel[$order] = 'selected="selected"';
        $this->view->offsetSet('ordersel', $ordersel);

        $user_options = '';
        $query = $this->db->query('
		SELECT DISTINCT l.uid, u.username
		FROM ' . TABLE_PREFIX . 'moderatorlog l
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (l.uid=u.uid)
		ORDER BY u.username ASC
	');
        while ($user = $this->db->fetch_array($query)) {
            // Deleted Users
            if (!$user['username']) {
                $user['username'] = $this->lang->na_deleted;
            }

            $selected = '';
            if ($this->bb->getInput('uid', 0) == $user['uid']) {
                $selected = ' selected="selected"';
            }

            $user['username'] = htmlspecialchars_uni($user['username']);
            $user_options .= "<option value=\"{$user['uid']}\"{$selected}>{$user['username']}</option>";
        }
        $this->view->offsetSet('user_options', $user_options);

        $forum_select = $this->forum->build_forum_jump('', $this->bb->getInput('fid', 0), 1, '', 0, true, '', 'fid');
        $this->view->offsetSet('forum_select', $forum_select);

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/modlogs.html.twig');
    }
}
