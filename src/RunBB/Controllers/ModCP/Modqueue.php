<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Modqueue extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        $navsep = '';
        if ($this->bb->usergroup['canmanagemodqueue'] == 0) {
            return $this->bb->error_no_permission();
        }

        if ($this->nummodqueuethreads == 0 &&
            $this->nummodqueueposts == 0 &&
            $this->nummodqueueattach == 0 &&
            $this->bb->usergroup['issupermod'] != 1
        ) {
            $this->bb->error($this->lang->you_cannot_use_mod_queue);
        }

        $this->bb->input['type'] = $this->bb->getInput('type', '');
        $threadqueue = $postqueue = $attachmentqueue = '';
        if ($this->bb->input['type'] == 'threads' ||
            !$this->bb->input['type'] &&
            ($this->nummodqueuethreads > 0 ||
                $this->bb->usergroup['issupermod'] == 1)
        ) {
            if ($this->nummodqueuethreads == 0 && $this->bb->usergroup['issupermod'] != 1) {
                $this->bb->error($this->lang->you_cannot_moderate_threads);
            }

            $this->bb->forum_cache = $this->cache->read('forums');

            $query = $this->db->simple_select('threads', 'COUNT(tid) AS unapprovedthreads', "visible='0' {$this->flist_queue_threads}");
            $unapproved_threads = $this->db->fetch_field($query, 'unapprovedthreads');

            // Figure out if we need to display multiple pages.
            if ($this->bb->getInput('page', '') !== 'last') {
                $page = $this->bb->getInput('page', 0);
            }

            $perpage = $this->bb->settings['threadsperpage'];
            $pages = $unapproved_threads / $perpage;
            $pages = ceil($pages);

            if ($this->bb->getInput('page', '') === 'last') {
                $page = $pages;
            }

            if ($page > $pages || $page <= 0) {
                $page = 1;
            }

            if ($page) {
                $start = ($page - 1) * $perpage;
            } else {
                $start = 0;
                $page = 1;
            }

            $multipage = $this->pagination->multipage($unapproved_threads, $perpage, $page, $this->bb->settings['bburl'].'/modcp/modqueue?type=threads');
            $this->view->offsetSet('multipage', $multipage);

            $query = $this->db->query('
			SELECT t.tid, t.dateline, t.fid, t.subject, t.username AS threadusername, p.message AS postmessage, u.username AS username, t.uid
			FROM ' . TABLE_PREFIX . 'threads t
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=t.firstpost)
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=t.uid)
			WHERE t.visible='0' {$this->tflist_queue_threads}
			ORDER BY t.lastpost DESC
			LIMIT {$start}, {$perpage}
		");
            $threads = '';
            while ($thread = $this->db->fetch_array($query)) {
                $altbg = alt_trow();
                $thread['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($thread['subject']));
                $thread['threadlink'] = get_thread_link($thread['tid']);
                $forum_link = $this->get_forum_link($thread['fid']);
                $forum_name = $this->bb->forum_cache[$thread['fid']]['name'];
                $threaddate = $this->time->formatDate('relative', $thread['dateline']);

                if ($thread['username'] == '') {
                    if ($thread['threadusername'] != '') {
                        $profile_link = $thread['threadusername'];
                    } else {
                        $profile_link = $this->lang->guest;
                    }
                } else {
                    $profile_link = $this->user->build_profile_link($thread['username'], $thread['uid']);
                }

                $thread['postmessage'] = nl2br(htmlspecialchars_uni($thread['postmessage']));
                eval("\$forum = \"" . $this->templates->get('modcp_modqueue_link_forum') . "\";");
                eval("\$threads .= \"" . $this->templates->get('modcp_modqueue_threads_thread') . "\";");
            }

            if (!$threads && $this->bb->input['type'] == 'threads') {
                eval("\$threads = \"" . $this->templates->get('modcp_modqueue_threads_empty') . "\";");
            }

            if ($threads) {
                $this->bb->add_breadcrumb($this->lang->mcp_nav_modqueue_threads, 'modcp.php?action=modqueue&amp;type=threads');

                $this->plugins->runHooks('modcp_modqueue_threads_end');

                if ($this->nummodqueueposts > 0 || $this->bb->usergroup['issupermod'] == 1) {
                    $navsep = ' | ';
                    eval("\$post_link = \"" . $this->templates->get('modcp_modqueue_post_link') . "\";");
                }

                if ($this->bb->settings['enableattachments'] == 1 && ($this->nummodqueueattach > 0 || $this->bb->usergroup['issupermod'] == 1)) {
                    $navsep = ' | ';
                    eval("\$attachment_link = \"" . $this->templates->get('modcp_modqueue_attachment_link') . "\";");
                }

                eval("\$mass_controls = \"" . $this->templates->get('modcp_modqueue_masscontrols') . "\";");
                eval("\$threadqueue = \"" . $this->templates->get('modcp_modqueue_threads') . "\";");
                output_page($threadqueue);
            }
            $type = 'threads';
        }

        if ($this->bb->input['type'] == 'posts' || (!$this->bb->input['type'] && !$threadqueue && ($this->nummodqueueposts > 0 || $this->bb->usergroup['issupermod'] == 1))) {
            if ($this->nummodqueueposts == 0 && $this->bb->usergroup['issupermod'] != 1) {
                $this->bb->error($this->lang->you_cannot_moderate_posts);
            }

            $this->bb->forum_cache = $this->cache->read('forums');

            $query = $this->db->query('
			SELECT COUNT(pid) AS unapprovedposts
			FROM  ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
			WHERE p.visible='0' {$this->tflist_queue_posts} AND t.firstpost != p.pid
		");
            $unapproved_posts = $this->db->fetch_field($query, 'unapprovedposts');

            // Figure out if we need to display multiple pages.
            if ($this->bb->getInput('page', '') !== 'last') {
                $page = $this->bb->getInput('page', 0);
            }

            $perpage = $this->bb->settings['postsperpage'];
            $pages = $unapproved_posts / $perpage;
            $pages = ceil($pages);

            if ($this->bb->getInput('page', '') === 'last') {
                $page = $pages;
            }

            if ($page > $pages || $page <= 0) {
                $page = 1;
            }

            if ($page) {
                $start = ($page - 1) * $perpage;
            } else {
                $start = 0;
                $page = 1;
            }

            $multipage = $this->pagination->multipage($unapproved_posts, $perpage, $page, 'modcp.php?action=modqueue&amp;type=posts');
            $this->view->offsetSet('multipage', $multipage);

            $query = $this->db->query('
			SELECT p.pid, p.subject, p.message, p.username AS postusername, t.subject AS threadsubject, t.tid, u.username, p.uid, t.fid, p.dateline
			FROM  ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
			WHERE p.visible='0' {$this->tflist_queue_posts} AND t.firstpost != p.pid
			ORDER BY p.dateline DESC
			LIMIT {$start}, {$perpage}
		");
            $posts = '';
            while ($post = $this->db->fetch_array($query)) {
                $altbg = alt_trow();
                $post['threadsubject'] = htmlspecialchars_uni($this->parser->parse_badwords($post['threadsubject']));
                $post['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($post['subject']));
                $post['threadlink'] = get_thread_link($post['tid']);
                $post['postlink'] = get_post_link($post['pid'], $post['tid']);
                $forum_link = $this->get_forum_link($post['fid']);
                $forum_name = $this->bb->forum_cache[$post['fid']]['name'];
                $postdate = $this->time->formatDate('relative', $post['dateline']);

                if ($post['username'] == '') {
                    if ($post['postusername'] != '') {
                        $profile_link = $post['postusername'];
                    } else {
                        $profile_link = $this->lang->guest;
                    }
                } else {
                    $profile_link = $this->user->build_profile_link($post['username'], $post['uid']);
                }

                eval("\$thread = \"" . $this->templates->get('modcp_modqueue_link_thread') . "\";");
                eval("\$forum = \"" . $this->templates->get('modcp_modqueue_link_forum') . "\";");
                $post['message'] = nl2br(htmlspecialchars_uni($post['message']));
                eval("\$posts .= \"" . $this->templates->get('modcp_modqueue_posts_post') . "\";");
            }

            if (!$posts && $this->bb->input['type'] == 'posts') {
                eval("\$posts = \"" . $this->templates->get('modcp_modqueue_posts_empty') . "\";");
            }

            if ($posts) {
                $this->bb->add_breadcrumb($this->lang->mcp_nav_modqueue_posts, 'modcp.php?action=modqueue&amp;type=posts');

                $this->plugins->runHooks('modcp_modqueue_posts_end');

                if ($this->nummodqueuethreads > 0 || $this->bb->usergroup['issupermod'] == 1) {
                    $navsep = ' | ';
                    eval("\$thread_link = \"" . $this->templates->get('modcp_modqueue_thread_link') . "\";");
                }

                if ($this->bb->settings['enableattachments'] == 1 && ($this->nummodqueueattach > 0 || $this->bb->usergroup['issupermod'] == 1)) {
                    $navsep = ' | ';
                    eval("\$attachment_link = \"" . $this->templates->get('modcp_modqueue_attachment_link') . "\";");
                }

                eval("\$mass_controls = \"" . $this->templates->get('modcp_modqueue_masscontrols') . "\";");
                eval("\$postqueue = \"" . $this->templates->get('modcp_modqueue_posts') . "\";");
                output_page($postqueue);
            }
        }

        if ($this->bb->input['type'] == 'attachments' ||
            (!$this->bb->input['type'] &&
                !$postqueue &&
                !$threadqueue &&
                $this->bb->settings['enableattachments'] == 1 &&
                ($this->nummodqueueattach > 0 || $this->bb->usergroup['issupermod'] == 1))
        ) {
            if ($this->bb->settings['enableattachments'] == 0) {
                $this->bb->error($this->lang->attachments_disabled);
            }

            if ($this->nummodqueueattach == 0 && $this->bb->usergroup['issupermod'] != 1) {
                $this->bb->error($this->lang->you_cannot_moderate_attachments);
            }

            $query = $this->db->query('
			SELECT COUNT(aid) AS unapprovedattachments
			FROM  ' . TABLE_PREFIX . 'attachments a
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
			WHERE a.visible='0'{$this->tflist_queue_attach}
		");
            $unapproved_attachments = $this->db->fetch_field($query, 'unapprovedattachments');

            // Figure out if we need to display multiple pages.
            if ($this->bb->getInput('page', '') !== 'last') {
                $page = $this->bb->getInput('page', 0);
            }

            $perpage = $this->bb->settings['postsperpage'];
            $pages = $unapproved_attachments / $perpage;
            $pages = ceil($pages);

            if ($this->bb->getInput('page', '') === 'last') {
                $page = $pages;
            }

            if ($page > $pages || $page <= 0) {
                $page = 1;
            }

            if ($page) {
                $start = ($page - 1) * $perpage;
            } else {
                $start = 0;
                $page = 1;
            }

            $multipage = $this->pagination->multipage($unapproved_attachments, $perpage, $page, 'modcp.php?action=modqueue&amp;type=attachments');
            $this->view->offsetSet('multipage', $multipage);

            $query = $this->db->query('
			SELECT a.*, p.subject AS postsubject, p.dateline, p.uid, u.username, t.tid, t.subject AS threadsubject
			FROM  ' . TABLE_PREFIX . 'attachments a
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (p.pid=a.pid)
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
			WHERE a.visible='0'{$this->tflist_queue_attach}
			ORDER BY a.dateuploaded DESC
			LIMIT {$start}, {$perpage}
		");
            $attachments = '';
            while ($attachment = $this->db->fetch_array($query)) {
                $altbg = alt_trow();

                if (!$attachment['dateuploaded']) {
                    $attachment['dateuploaded'] = $attachment['dateline'];
                }

                $attachdate = $this->time->formatDate('relative', $attachment['dateuploaded']);

                $attachment['postsubject'] = htmlspecialchars_uni($attachment['postsubject']);
                $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                $attachment['threadsubject'] = htmlspecialchars_uni($attachment['threadsubject']);
                $attachment['filesize'] = $this->parser->friendlySize($attachment['filesize']);

                $link = get_post_link($attachment['pid'], $attachment['tid']) . "#pid{$attachment['pid']}";
                $thread_link = get_thread_link($attachment['tid']);
                $profile_link = $this->user->build_profile_link($attachment['username'], $attachment['uid']);

                eval("\$attachments .= \"" . $this->templates->get('modcp_modqueue_attachments_attachment') . "\";");
            }

            if (!$attachments && $this->bb->input['type'] == 'attachments') {
                eval("\$attachments = \"" . $this->templates->get('modcp_modqueue_attachments_empty') . "\";");
            }

            if ($attachments) {
                $this->bb->add_breadcrumb($this->lang->mcp_nav_modqueue_attachments, 'modcp.php?action=modqueue&amp;type=attachments');

                $this->plugins->runHooks('modcp_modqueue_attachments_end');

                if ($this->nummodqueuethreads > 0 || $this->bb->usergroup['issupermod'] == 1) {
                    eval("\$thread_link = \"" . $this->templates->get('modcp_modqueue_thread_link') . "\";");
                    $navsep = ' | ';
                }

                if ($this->nummodqueueposts > 0 || $this->bb->usergroup['issupermod'] == 1) {
                    eval("\$post_link = \"" . $this->templates->get('modcp_modqueue_post_link') . "\";");
                    $navsep = ' | ';
                }

                eval("\$mass_controls = \"" . $this->templates->get('modcp_modqueue_masscontrols') . "\";");
                eval("\$attachmentqueue = \"" . $this->templates->get('modcp_modqueue_attachments') . "\";");
                output_page($attachmentqueue);
            }
        }

        // Still nothing? All queues are empty! :-D
        if (!$threadqueue && !$postqueue && !$attachmentqueue) {
            $this->bb->add_breadcrumb($this->lang->mcp_nav_modqueue, $this->bb->settings['bburl'].'/modcp/modqueue');

            $this->plugins->runHooks('modcp_modqueue_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/ModCP/modqueue_empty.html.twig');
        }
    }

    public function doModqueue(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canmanagemodqueue'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('modcp_do_modqueue_start');

        $this->bb->input['threads'] = $this->bb->getInput('threads', []);
        $this->bb->input['posts'] = $this->bb->getInput('posts', []);
        $this->bb->input['attachments'] = $this->bb->getInput('attachments', []);
        if (!empty($this->bb->input['threads'])) {
            $threads = array_map('intval', array_keys($this->bb->input['threads']));
            $threads_to_approve = $threads_to_delete = [];
            // Fetch threads
            $query = $this->db->simple_select('threads', 'tid', 'tid IN (' . implode(',', $threads) . "){$this->flist_queue_threads}");
            while ($thread = $this->db->fetch_array($query)) {
                if (!isset($this->bb->input['threads'][$thread['tid']])) {
                    continue;
                }
                $action = $this->bb->input['threads'][$thread['tid']];
                if ($action == 'approve') {
                    $threads_to_approve[] = $thread['tid'];
                } elseif ($action == 'delete') {
                    $threads_to_delete[] = $thread['tid'];
                }
            }
            if (!empty($threads_to_approve)) {
                $this->moderation->approve_threads($threads_to_approve);
                $this->bblogger->log_moderator_action(['tids' => $threads_to_approve], $this->lang->multi_approve_threads);
            }
            if (!empty($threads_to_delete)) {
                if ($this->bb->settings['soft_delete'] == 1) {
                    $this->moderation->soft_delete_threads($threads_to_delete);
                    $this->bblogger->log_moderator_action(['tids' => $threads_to_delete], $this->lang->multi_soft_delete_threads);
                } else {
                    foreach ($threads_to_delete as $tid) {
                        $this->moderation->delete_thread($tid);
                    }
                    $this->bblogger->log_moderator_action(['tids' => $threads_to_delete], $this->lang->multi_delete_threads);
                }
            }

            $this->plugins->runHooks('modcp_do_modqueue_end');

            $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/modqueue', $this->lang->redirect_threadsmoderated);
        } elseif (!empty($this->bb->input['posts'])) {
            $posts = array_map('intval', array_keys($this->bb->input['posts']));
            // Fetch posts
            $posts_to_approve = $posts_to_delete = [];
            $query = $this->db->simple_select('posts', 'pid', 'pid IN (' . implode(',', $posts) . "){$flist_queue_posts}");
            while ($post = $this->db->fetch_array($query)) {
                if (!isset($this->bb->input['posts'][$post['pid']])) {
                    continue;
                }
                $action = $this->bb->input['posts'][$post['pid']];
                if ($action == 'approve') {
                    $posts_to_approve[] = $post['pid'];
                } elseif ($action == 'delete' && $this->bb->settings['soft_delete'] != 1) {
                    $this->moderation->delete_post($post['pid']);
                } elseif ($action == 'delete') {
                    $posts_to_delete[] = $post['pid'];
                }
            }
            if (!empty($posts_to_approve)) {
                $this->moderation->approve_posts($posts_to_approve);
                $this->bblogger->log_moderator_action(['pids' => $posts_to_approve], $this->lang->multi_approve_posts);
            }
            if (!empty($posts_to_delete)) {
                if ($this->bb->settings['soft_delete'] == 1) {
                    $this->moderation->soft_delete_posts($posts_to_delete);
                    $this->bblogger->log_moderator_action(['pids' => $posts_to_delete], $this->lang->multi_soft_delete_posts);
                } else {
                    $this->bblogger->log_moderator_action(['pids' => $posts_to_delete], $this->lang->multi_delete_posts);
                }
            }

            $this->plugins->runHooks('modcp_do_modqueue_end');

            $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/modqueue?type=posts', $this->lang->redirect_postsmoderated);
        } elseif (!empty($this->bb->input['attachments'])) {
            $attachments = array_map('intval', array_keys($this->bb->input['attachments']));
            $query = $this->db->query('
			SELECT a.pid, a.aid
			FROM  ' . TABLE_PREFIX . 'attachments a
			LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (a.pid=p.pid)
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
			WHERE aid IN (' . implode(',', $attachments) . "){$this->tflist_queue_attach}
		");
            while ($attachment = $this->db->fetch_array($query)) {
                if (!isset($this->bb->input['attachments'][$attachment['aid']])) {
                    continue;
                }
                $action = $this->bb->input['attachments'][$attachment['aid']];
                if ($action == 'approve') {
                    $this->db->update_query('attachments', ['visible' => 1], "aid='{$attachment['aid']}'");
                } elseif ($action == 'delete') {
                    remove_attachment($attachment['pid'], '', $attachment['aid']);
                }
            }

            $this->plugins->runHooks('modcp_do_modqueue_end');

            $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/modqueue?type=attachments', $this->lang->redirect_attachmentsmoderated);
        }
    }
}
