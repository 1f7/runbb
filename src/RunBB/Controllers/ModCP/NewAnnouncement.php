<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class NewAnnouncement extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() == 'exit') {
                return;
            }
        }

        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_announcements, $this->bb->settings['bburl'] . '/modcp//announcements');
        $this->bb->add_breadcrumb($this->lang->add_announcement, $this->bb->settings['bburl'] . '/modcp/new_announcements');

        $announcement_fid = $this->bb->getInput('fid', 0);

        if (($this->bb->usergroup['issupermod'] != 1 && $announcement_fid == -1) ||
            ($announcement_fid != -1 && !$this->user->is_moderator($announcement_fid, 'canmanageannouncements')) ||
            ($this->unviewableforums && in_array($announcement_fid, $this->unviewableforums))
        ) {
            return $this->bb->error_no_permission();
        }

        // Deal with inline errors
        if (!empty($this->errors) || isset($this->preview)) {
            if (!empty($this->errors)) {
                $this->errors = $this->bb->inline_error($this->errors);
            } else {
                $this->errors = '';
            }

            // Set $announcement to input stuff
            $announcement['subject'] = $this->bb->input['title'];
            $announcement['message'] = $this->bb->input['message'];
            $announcement['allowhtml'] = $this->bb->input['allowhtml'];//$allowhtml;
            $announcement['allowmycode'] = $this->bb->input['allowmycode'];//$allowmycode;
            $announcement['allowsmilies'] = $this->bb->input['allowsmilies'];//$allowsmilies;

            $startmonth = $this->bb->input['starttime_month'];
            $startdateyear = htmlspecialchars_uni($this->bb->input['starttime_year']);
            $startday = $this->bb->getInput('starttime_day', 0);
            $starttime_time = htmlspecialchars_uni($this->bb->input['starttime_time']);
            $endmonth = $this->bb->input['endtime_month'];
            $enddateyear = htmlspecialchars_uni($this->bb->input['endtime_year']);
            $endday = $this->bb->getInput('endtime_day', 0);
            $endtime_time = htmlspecialchars_uni($this->bb->input['endtime_time']);
        } else {
            // Note: dates are in GMT timezone
            $starttime_time = gmdate('g:i a', TIME_NOW);
            $endtime_time = gmdate('g:i a', TIME_NOW);
            $startday = $endday = gmdate('j', TIME_NOW);
            //$startmonth = $endmonth = gmdate('m', TIME_NOW);
            $startmonth = $endmonth = gmdate('n', TIME_NOW);
            $startdateyear = gmdate('Y', TIME_NOW);

            $announcement = [
                'subject' => '',
                'message' => '',
                'allowhtml' => 1,
                'allowmycode' => 1,
                'allowsmilies' => 1
            ];

            $enddateyear = $startdateyear + 1;
        }
        $this->view->offsetSet('startdateyear', $startdateyear);
        $this->view->offsetSet('starttime_time', $starttime_time);
        $this->view->offsetSet('enddateyear', $enddateyear);
        $this->view->offsetSet('endtime_time', $endtime_time);

        // Generate form elements
        $startdateday = $enddateday = '';
        for ($day = 1; $day <= 31; ++$day) {
            if ($startday == $day) {
                $startdateday .= "<option value=\"{$day}\" selected=\"selected\">{$day}</option>";
            } else {
                $startdateday .= "<option value=\"{$day}\">{$day}</option>";
            }

            if ($endday == $day) {
                $enddateday .= "<option value=\"{$day}\" selected=\"selected\">{$day}</option>";
            } else {
                $enddateday .= "<option value=\"{$day}\">{$day}</option>";
            }
        }
        $this->view->offsetSet('startdateday', $startdateday);
        $this->view->offsetSet('enddateday', $enddateday);

        $startmonthsel = $endmonthsel = [];
        //foreach(array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12') as $month)
        foreach ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] as $month) {
            $startmonthsel[$month] = '';
            $endmonthsel[$month] = '';
        }
        $startmonthsel[$startmonth] = 'selected="selected"';
        $endmonthsel[$endmonth] = 'selected="selected"';
        $this->view->offsetSet('startmonthsel', $startmonthsel);
        $this->view->offsetSet('endmonthsel', $endmonthsel);

        $this->view->offsetSet('title', htmlspecialchars_uni($announcement['subject']));
        $this->view->offsetSet('message', htmlspecialchars_uni($announcement['message']));

        $html_sel = $mycode_sel = $smilies_sel = ['yes' => '', 'no' => ''];
        if ($announcement['allowhtml']) {
            $html_sel['yes'] = ' checked="checked"';
        } else {
            $html_sel['no'] = ' checked="checked"';
        }
        $this->view->offsetSet('html_sel', $html_sel);

        if ($announcement['allowmycode']) {
            $mycode_sel['yes'] = ' checked="checked"';
        } else {
            $mycode_sel['no'] = ' checked="checked"';
        }
        $this->view->offsetSet('mycode_sel', $mycode_sel);

        if ($announcement['allowsmilies']) {
            $smilies_sel['yes'] = ' checked="checked"';
        } else {
            $smilies_sel['no'] = ' checked="checked"';
        }
        $this->view->offsetSet('smilies_sel', $smilies_sel);

        $end_type_sel = ['infinite' => '', 'finite' => ''];
        if (!isset($this->bb->input['endtime_type']) || $this->bb->input['endtime_type'] == 2) {
            $end_type_sel['infinite'] = ' checked="checked"';
        } else {
            $end_type_sel['finite'] = ' checked="checked"';
        }
        $this->view->offsetSet('end_type_sel', $end_type_sel);

        $this->bb->codebuttons = $this->editor->build_mycode_inserter();
        $this->bb->smilieinserter = $this->editor->build_clickable_smilies();

        $postbit = '';
        if (isset($this->preview)) {
            $this->bb->announcementarray = [
                'aid' => 0,
                'fid' => $announcement_fid,
                'uid' => $this->user->uid,
                'subject' => $this->bb->input['title'],
                'message' => $this->bb->input['message'],
                'allowhtml' => $this->bb->getInput('allowhtml', 0),
                'allowmycode' => $this->bb->getInput('allowmycode', 0),
                'allowsmilies' => $this->bb->getInput('allowsmilies', 0),
                'dateline' => TIME_NOW,
                'userusername' => $this->user->username,
                'ipaddress' => $this->session->ipaddress
            ];

            $array = $this->bb->user;
            foreach ($array as $key => $element) {
                $this->bb->announcementarray[$key] = $element;
            }

            // Gather usergroup data from the cache
            // Field => Array Key
            $data_key = [
                'title' => 'grouptitle',
                'usertitle' => 'groupusertitle',
                'stars' => 'groupstars',
                'starimage' => 'groupstarimage',
                'image' => 'groupimage',
                'namestyle' => 'namestyle',
                'usereputationsystem' => 'usereputationsystem'
            ];

            foreach ($data_key as $field => $key) {
                $this->bb->announcementarray[$key] = $this->bb->groupscache[$this->bb->announcementarray['usergroup']][$field];
            }

            $postbit = $this->post->build_postbit($this->bb->announcementarray, 3);
        }

        $this->view->offsetSet('postbit', $postbit);
        $this->view->offsetSet('errors', $this->errors);
        $this->view->offsetSet('announcement_fid', $announcement_fid);

        $this->plugins->runHooks('modcp_new_announcement');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/newAnnouncement.html.twig');
    }

    public function doNew(Request $request, Response $response)
    {
        if ($this->init() == 'exit') {
            return;
        }

        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canmanageannounce'] == 0) {
            return $this->bb->error_no_permission();
        }

        $announcement_fid = $this->bb->getInput('fid', 0);
        if (($this->bb->usergroup['issupermod'] != 1 && $announcement_fid == -1) || ($announcement_fid != -1 &&
                !$this->user->is_moderator($announcement_fid, 'canmanageannouncements')) ||
            ($this->unviewableforums && in_array($announcement_fid, $this->unviewableforums))
        ) {
            return $this->bb->error_no_permission();
        }

        $this->errors = [];

        $this->bb->input['title'] = $this->bb->getInput('title', '', true);
        if (!trim($this->bb->input['title'])) {
            $this->errors[] = $this->lang->error_missing_title;
        }

        $this->bb->input['message'] = $this->bb->getInput('message', '', true);
        if (!trim($this->bb->input['message'])) {
            $this->errors[] = $this->lang->error_missing_message;
        }

        if (!$announcement_fid) {
            $this->errors[] = $this->lang->error_missing_forum;
        }

        $this->bb->input['starttime_time'] = $this->bb->getInput('starttime_time', '');
        $this->bb->input['endtime_time'] = $this->bb->getInput('endtime_time', '');
        $startdate = @explode(' ', $this->bb->input['starttime_time']);
        $startdate = @explode(':', $startdate[0]);
        $enddate = @explode(' ', $this->bb->input['endtime_time']);
        $enddate = @explode(':', $enddate[0]);

        if (stristr($this->bb->input['starttime_time'], 'pm')) {
            $startdate[0] = 12 + $startdate[0];
            if ($startdate[0] >= 24) {
                $startdate[0] = '00';
            }
        }

        if (stristr($this->bb->input['endtime_time'], 'pm')) {
            $enddate[0] = 12 + $enddate[0];
            if ($enddate[0] >= 24) {
                $enddate[0] = '00';
            }
        }

        $this->bb->input['starttime_month'] = $this->bb->getInput('starttime_month', '');
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        if (!in_array($this->bb->input['starttime_month'], $months)) {
            $this->bb->input['starttime_month'] = '01';
        }

        $startdate = gmmktime((int)$startdate[0], (int)$startdate[1], 0, $this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0));
        if (!checkdate($this->bb->getInput('starttime_month', 0), $this->bb->getInput('starttime_day', 0), $this->bb->getInput('starttime_year', 0)) || $startdate < 0 || $startdate == false) {
            $this->errors[] = $this->lang->error_invalid_start_date;
        }

        if ($this->bb->getInput('endtime_type', 0) == 2) {
            $enddate = '0';
            $this->bb->input['endtime_month'] = '01';
        } else {
            $this->bb->input['endtime_month'] = $this->bb->getInput('endtime_month', '');
            if (!in_array($this->bb->input['endtime_month'], $months)) {
                $this->bb->input['endtime_month'] = '01';
            }
            $enddate = gmmktime((int)$enddate[0], (int)$enddate[1], 0, $this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0));
            if (!checkdate($this->bb->getInput('endtime_month', 0), $this->bb->getInput('endtime_day', 0), $this->bb->getInput('endtime_year', 0)) || $enddate < 0 || $enddate == false) {
                $this->errors[] = $this->lang->error_invalid_end_date;
            }

            if ($enddate <= $startdate) {
                $this->errors[] = $this->lang->error_end_before_start;
            }
        }

        if ($this->bb->getInput('allowhtml', 0) == 1) {
            $allowhtml = 1;
        } else {
            $allowhtml = 0;
        }
        if ($this->bb->getInput('allowmycode', 0) == 1) {
            $allowmycode = 1;
        } else {
            $allowmycode = 0;
        }
        if ($this->bb->getInput('allowsmilies', 0) == 1) {
            $allowsmilies = 1;
        } else {
            $allowsmilies = 0;
        }

        $this->plugins->runHooks('modcp_do_new_announcement_start');

        if (!$this->errors) {
            if (isset($this->bb->input['preview'])) {
                $this->preview = [];
                $this->bb->input['action'] = 'new_announcement';
                return $this->index($request, $response, true);
            } else {
                $insert_announcement = [
                    'fid' => $announcement_fid,
                    'uid' => $this->user->uid,
                    'subject' => $this->db->escape_string($this->bb->input['title']),
                    'message' => $this->db->escape_string($this->bb->input['message']),
                    'startdate' => $startdate,
                    'enddate' => $enddate,
                    'allowhtml' => $allowhtml,
                    'allowmycode' => $allowmycode,
                    'allowsmilies' => $allowsmilies
                ];
                $aid = $this->db->insert_query('announcements', $insert_announcement);

                $this->bblogger->log_moderator_action(
                    ['aid' => $aid, 'subject' => $this->db->escape_string($this->bb->input['title'])],
                    $this->lang->announcement_added
                );

                $this->plugins->runHooks('modcp_do_new_announcement_end');

                $this->cache->update_forumsdisplay();
                $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/announcements', $this->lang->redirect_add_announcement);
            }
        } else {
            return $this->index($request, $response, true);
        }
    }
}
