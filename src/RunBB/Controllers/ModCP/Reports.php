<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Reports extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canmanagereportedcontent'] == 0) {
            return $this->bb->error_no_permission();
        }

        if ($this->numreportedposts == 0 && $this->bb->usergroup['issupermod'] != 1) {
            $this->bb->error($this->lang->you_cannot_view_reported_posts);
        }

        $this->lang->load('report');
        $this->bb->add_breadcrumb($this->lang->mcp_nav_report_center, $this->bb->settings['bburl'] . '/modcp/reports');

        $perpage = $this->bb->settings['threadsperpage'];
        if (!$perpage) {
            $perpage = 20;
        }

        // Multipage
        if ($this->bb->usergroup['cancp'] || $this->bb->usergroup['issupermod']) {
            $query = $this->db->simple_select('reportedcontent', 'COUNT(rid) AS count', "reportstatus ='0'");
            $report_count = $this->db->fetch_field($query, 'count');
        } else {
            $query = $this->db->simple_select('reportedcontent', 'id3', "reportstatus='0' AND (type = 'post' OR type = '')");

            $report_count = 0;
            while ($fid = $this->db->fetch_field($query, 'id3')) {
                if ($this->user->is_moderator($fid, 'canmanagereportedposts')) {
                    ++$report_count;
                }
            }
            unset($fid);
        }

        $page = $this->bb->getInput('page', 0);

        $postcount = (int)$report_count;
        $pages = $postcount / $perpage;
        $pages = ceil($pages);

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }

        if ($page && $page > 0) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }
        $this->view->offsetSet('page', $page);

        $multipage = $reportspages = '';
        if ($postcount > $perpage) {
            $multipage = $this->pagination->multipage($postcount, $perpage, $page, $this->bb->settings['bburl'] . '/modcp/reports');
        }
        $this->view->offsetSet('multipage', $multipage);

        $this->plugins->runHooks('modcp_reports_start');

        // Reports
        $reports = [];
        $query = $this->db->query('
		SELECT r.*, u.username
		FROM ' . TABLE_PREFIX . 'reportedcontent r
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (r.uid = u.uid)
		WHERE r.reportstatus = '0'{$this->tflist_reports}
		ORDER BY r.reports DESC
		LIMIT {$start}, {$perpage}
	");

        if ($this->db->num_rows($query) > 0) {
            $reportedcontent = $this->cache->read('reportedcontent');
            $reportcache = $usercache = $postcache = [];

            while ($report = $this->db->fetch_array($query)) {
                if ($report['type'] == 'profile' || $report['type'] == 'reputation') {
                    // Profile UID is in ID
                    if (!isset($usercache[$report['id']])) {
                        $usercache[$report['id']] = $report['id'];
                    }

                    // Reputation comment? The offender is the ID2
                    if ($report['type'] == 'reputation') {
                        if (!isset($usercache[$report['id2']])) {
                            $usercache[$report['id2']] = $report['id2'];
                        }
                        if (!isset($usercache[$report['id3']])) {
                            // The user who was offended
                            $usercache[$report['id3']] = $report['id3'];
                        }
                    }
                } elseif (!$report['type'] || $report['type'] == 'post') {
                    // This (should) be a post
                    $postcache[$report['id']] = $report['id'];
                }

                // Lastpost info - is it missing (pre-1.8)?
                $lastposter = $report['uid'];
                if (!$report['lastreport']) {
                    // Last reporter is our first reporter
                    $report['lastreport'] = $report['dateline'];
                }

                if ($report['reporters']) {
                    $reporters = my_unserialize($report['reporters']);

                    if (is_array($reporters)) {
                        $lastposter = end($reporters);
                    }
                }

                if (!isset($usercache[$lastposter])) {
                    $usercache[$lastposter] = $lastposter;
                }

                $report['lastreporter'] = $lastposter;
                $reportcache[] = $report;
            }

            // Report Center gets messy
            // Find information about our users (because we don't log it when they file a report)
            if (!empty($usercache)) {
                $sql = implode(',', array_keys($usercache));
                $query = $this->db->simple_select('users', 'uid, username', "uid IN ({$sql})");

                while ($user = $this->db->fetch_array($query)) {
                    $usercache[$user['uid']] = $user;
                }
            }

            // Messy * 2
            // Find out post information for our reported posts
            if (!empty($postcache)) {
                $sql = implode(',', array_keys($postcache));
                $query = $this->db->query('
				SELECT p.pid, p.uid, p.username, p.tid, t.subject
				FROM ' . TABLE_PREFIX . 'posts p
				LEFT JOIN ' . TABLE_PREFIX . "threads t ON (p.tid = t.tid)
				WHERE p.pid IN ({$sql})
			");

                while ($post = $this->db->fetch_array($query)) {
                    $postcache[$post['pid']] = $post;
                }
            }

            $this->plugins->runHooks('modcp_reports_intermediate');

            // Now that we have all of the information needed, display the reports
            foreach ($reportcache as $report) {
                //$trow = alt_trow();
                if (!$report['type']) {
                    // Assume a post
                    $report['type'] = 'post';
                }

                // Report Information
                $report_data = [];

                switch ($report['type']) {
                    case 'post':
                        $post = get_post_link($report['id']) . "#pid{$report['id']}";
                        $user = $this->user->build_profile_link($postcache[$report['id']]['username'], $postcache[$report['id']]['uid']);
                        $report_data['content'] = $this->lang->sprintf($this->lang->report_info_post, $post, $user);

                        $thread_link = get_thread_link($postcache[$report['id']]['tid']);
                        $thread_subject = htmlspecialchars_uni($postcache[$report['id']]['subject']);
                        $report_data['content'] .= $this->lang->sprintf($this->lang->report_info_post_thread, $thread_link, $thread_subject);

                        break;
                    case 'profile':
                        $user = $this->user->build_profile_link($usercache[$report['id']]['username'], $usercache[$report['id']]['uid']);
                        $report_data['content'] = $this->lang->sprintf($this->lang->report_info_profile, $user);
                        break;
                    case 'reputation':
                        $reputation_link = $this->bb->settings['bburl'] . "/reputation?uid={$usercache[$report['id3']]['uid']}#rid{$report['id']}";
                        $bad_user = $this->user->build_profile_link($usercache[$report['id2']]['username'], $usercache[$report['id2']]['uid']);
                        $report_data['content'] = $this->lang->sprintf($this->lang->report_info_reputation, $reputation_link, $bad_user);

                        $good_user = $this->user->build_profile_link($usercache[$report['id3']]['username'], $usercache[$report['id3']]['uid']);
                        $report_data['content'] .= $this->lang->sprintf($this->lang->report_info_rep_profile, $good_user);
                        break;
                }

                // Report reason and comment
                $report_data['comment'] = $this->lang->na;
                $report_string = "report_reason_{$report['reason']}";

                if (isset($this->lang->$report_string)) {
                    $report_data['comment'] = $this->lang->$report_string;
                } elseif (!empty($report['reason'])) {
                    $report_data['comment'] = htmlspecialchars_uni($report['reason']);
                }

                $report_reports = 1;
                if ($report['reports']) {
                    $report_data['reports'] = $this->parser->formatNumber($report['reports']);
                }

                if ($report['lastreporter']) {
                    if (is_array($usercache[$report['lastreporter']])) {
                        $lastreport_user = $this->user->build_profile_link($usercache[$report['lastreporter']]['username'], $report['lastreporter']);
                    } elseif ($usercache[$report['lastreporter']] > 0) {
                        $lastreport_user = $this->lang->na_deleted;
                    }

                    $lastreport_date = $this->time->formatDate('relative', $report['lastreport']);
                    $report_data['lastreporter'] = $this->lang->sprintf($this->lang->report_info_lastreporter, $lastreport_date, $lastreport_user);
                }
                $reports[] = [
                    'trow' => alt_trow(),
                    'content' => $report_data['content'],
                    'comment' => $report_data['comment'],
                    'reports' => $report_data['reports'],
                    'lastreporter' => $report_data['lastreporter'],
                    'rid' => $report['rid']
                ];
                $this->plugins->runHooks('modcp_reports_report');
            }
        }
        $this->view->offsetSet('reports', $reports);

        $this->plugins->runHooks('modcp_reports_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/reports.html.twig');
    }

    public function doReports(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->bb->input['reports'] = $this->bb->getInput('reports', []);
        if (empty($this->bb->input['reports'])) {
            $this->bb->error($this->lang->error_noselected_reports);
        }

        $sql = '1=1';
        if (empty($this->bb->input['allbox'])) {
            $this->bb->input['reports'] = array_map('intval', $this->bb->input['reports']);
            $rids = implode("','", $this->bb->input['reports']);

            $sql = "rid IN ('0','{$rids}')";
        }

        $this->plugins->runHooks('modcp_do_reports');

        $this->db->update_query('reportedcontent', ['reportstatus' => 1], "{$sql}{$this->flist_reports}");
        $this->cache->update_reportedcontent();

        $page = $this->bb->getInput('page', 0);

        $this->bb->redirect($this->bb->settings['bburl'] . '/modcp/reports?page=' . $page, $this->lang->redirect_reportsmarked);
    }
}
