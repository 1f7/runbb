<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\ModCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Warninglogs extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canviewwarnlogs'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->bb->add_breadcrumb($this->lang->mcp_nav_warninglogs, $this->bb->settings['bburl'] . '/modcp/warninglogs');

        // Filter options
        $where_sql = '';
        $this->bb->input['filter'] = $this->bb->getInput('filter', []);
        $this->bb->input['search'] = $this->bb->getInput('search', []);
        if (!empty($this->bb->input['filter']['username'])) {
            $search_user = $this->user->get_user_by_username($this->bb->input['filter']['username']);

            $this->bb->input['filter']['uid'] = (int)$search_user['uid'];
            $this->bb->input['filter']['username'] = htmlspecialchars_uni($this->bb->input['filter']['username']);
        } else {
            $this->bb->input['filter']['username'] = '';
        }
        if (!empty($this->bb->input['filter']['uid'])) {
            $search['uid'] = (int)$this->bb->input['filter']['uid'];
            $where_sql .= " AND w.uid='{$search['uid']}'";
            if (!isset($this->bb->input['search']['username'])) {
                $user = $this->user->get_user($this->bb->input['search']['uid']);
                $this->bb->input['search']['username'] = htmlspecialchars_uni($user->username);
            }
        } else {
            $this->bb->input['filter']['uid'] = '';
        }
        if (!empty($this->bb->input['filter']['mod_username'])) {
            $mod_user = $this->user->get_user_by_username($this->bb->input['filter']['mod_username']);

            $this->bb->input['filter']['mod_uid'] = (int)$mod_user['uid'];
            $this->bb->input['filter']['mod_username'] = htmlspecialchars_uni($this->bb->input['filter']['mod_username']);
        } else {
            $this->bb->input['filter']['mod_username'] = '';
        }
        if (!empty($this->bb->input['filter']['mod_uid'])) {
            $search['mod_uid'] = (int)$this->bb->input['filter']['mod_uid'];
            $where_sql .= " AND w.issuedby='{$search['mod_uid']}'";
            if (!isset($this->bb->input['search']['mod_username'])) {
                $mod_user = $this->user->get_user($this->bb->input['search']['uid']);
                $this->bb->input['search']['mod_username'] = htmlspecialchars_uni($mod_user['username']);
            }
        } else {
            $this->bb->input['filter']['mod_uid'] = '';
        }
        if (!empty($this->bb->input['filter']['reason'])) {
            $search['reason'] = $this->db->escape_string_like($this->bb->input['filter']['reason']);
            $where_sql .= " AND (w.notes LIKE '%{$search['reason']}%' OR t.title LIKE '%{$search['reason']}%' OR w.title LIKE '%{$search['reason']}%')";
            $this->bb->input['filter']['reason'] = htmlspecialchars_uni($this->bb->input['filter']['reason']);
        } else {
            $this->bb->input['filter']['reason'] = '';
        }
        $sortbysel = ['username' => '', 'expires' => '', 'issuedby' => '', 'dateline' => ''];
        if (!isset($this->bb->input['filter']['sortby'])) {
            $this->bb->input['filter']['sortby'] = '';
        }
        switch ($this->bb->input['filter']['sortby']) {
            case 'username':
                $sortby = 'u.username';
                $sortbysel['username'] = ' selected="selected"';
                break;
            case 'expires':
                $sortby = 'w.expires';
                $sortbysel['expires'] = ' selected="selected"';
                break;
            case 'issuedby':
                $sortby = 'i.username';
                $sortbysel['issuedby'] = ' selected="selected"';
                break;
            default: // 'dateline'
                $sortby = 'w.dateline';
                $sortbysel['dateline'] = ' selected="selected"';
        }
        $this->view->offsetSet('sortbysel', $sortbysel);

        if (!isset($this->bb->input['filter']['order'])) {
            $this->bb->input['filter']['order'] = '';
        }
        $order = $this->bb->input['filter']['order'];
        $ordersel = ['asc' => '', 'desc' => ''];
        if ($order != 'asc') {
            $order = 'desc';
            $ordersel['desc'] = ' selected="selected"';
        } else {
            $ordersel['asc'] = ' selected="selected"';
        }
        $this->view->offsetSet('ordersel', $ordersel);

        $this->plugins->runHooks('modcp_warninglogs_start');

        // Pagination stuff
        $sql = '
		SELECT COUNT(wid) as count
		FROM
			' . TABLE_PREFIX . 'warnings w
			LEFT JOIN ' . TABLE_PREFIX . "warningtypes t ON (w.tid=t.tid)
		WHERE 1=1
			{$where_sql}
	";
        $query = $this->db->query($sql);
        $total_warnings = $this->db->fetch_field($query, 'count');
        $page = $this->bb->getInput('page', 0);
        if ($page <= 0) {
            $page = 1;
        }
        $per_page = 20;
        if (isset($this->bb->input['filter']['per_page']) && (int)$this->bb->input['filter']['per_page'] > 0) {
            $per_page = (int)$this->bb->input['filter']['per_page'];
        }
        $this->view->offsetSet('per_page', $per_page);

        $start = ($page - 1) * $per_page;
        // Build the base URL for pagination links
        $url = $this->bb->settings['bburl'] . '/modcp/warninglogs';
        if (is_array($this->bb->input['filter']) && count($this->bb->input['filter'])) {
            foreach ($this->bb->input['filter'] as $field => $value) {
                $value = urlencode($value);
                $url .= "&amp;filter[{$field}]={$value}";
            }
        }
        $multipage = $this->pagination->multipage($total_warnings, $per_page, $page, $url);
        $this->view->offsetSet('multipage', $multipage);

        // The actual query
        $sql = '
		SELECT
			w.wid, w.title as custom_title, w.points, w.dateline, w.issuedby, w.expires, w.expired, w.daterevoked, w.revokedby,
			t.title,
			u.uid, u.username, u.usergroup, u.displaygroup,
			i.uid as mod_uid, i.username as mod_username, i.usergroup as mod_usergroup, i.displaygroup as mod_displaygroup
		FROM ' . TABLE_PREFIX . 'warnings w
			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (w.uid=u.uid)
			LEFT JOIN ' . TABLE_PREFIX . 'warningtypes t ON (w.tid=t.tid)
			LEFT JOIN ' . TABLE_PREFIX . "users i ON (i.uid=w.issuedby)
		WHERE 1=1
			{$where_sql}
		ORDER BY {$sortby} {$order}
		LIMIT {$start}, {$per_page}
	";
        $query = $this->db->query($sql);


        $warning_list = '';
        while ($row = $this->db->fetch_array($query)) {
            $username = $this->user->format_name($row['username'], $row['usergroup'], $row['displaygroup']);
            $mod_username = $this->user->format_name($row['mod_username'], $row['mod_usergroup'], $row['mod_displaygroup']);
            $revoked_text = '';
            if ($row['daterevoked'] > 0) {
                $revoked_date = $this->time->formatDate('relative', $row['daterevoked']);
                $revoked_text = "<br /><small><strong>{$this->lang->revoked}</strong> {$revoked_date}</small>";
            }
            if ($row['expires'] > 0) {
                $expire_date = $this->time->formatDate('relative', $row['expires'], '', 2);
            } else {
                $expire_date = $this->lang->never;
            }
            $title = $row['title'];
            if (empty($row['title'])) {
                $title = $row['custom_title'];
            }
            if ($row['points'] >= 0) {
                $points = '+' . $row['points'];
            }
            $warning_list[] = [
                'trow' => alt_trow(),
                'username_link' => $this->user->build_profile_link($username, $row['uid']),
                'title' => htmlspecialchars_uni($title),
                'points' => $points,
                'issued_date' => $this->time->formatDate($this->bb->settings['dateformat'], $row['dateline']) . ' ' . $this->time->formatDate($this->bb->settings['timeformat'], $row['dateline']),
                'expire_date' => $expire_date,
                'revoked_text' => $revoked_text,
                'mod_username_link' => $this->user->build_profile_link($mod_username, $row['mod_uid']),
                'wid' => $row['wid']
            ];
        }
        $this->view->offsetSet('warning_list', $warning_list);
        $this->view->offsetSet('username', $this->bb->input['filter']['username']);
        $this->view->offsetSet('mod_username', $this->bb->input['filter']['mod_username']);
        $this->view->offsetSet('reason', $this->bb->input['filter']['reason']);

        $this->plugins->runHooks('modcp_warninglogs_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/ModCP/warninglogs.html.twig');
    }
}
