<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Moderation;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{
    public $modlogdata = [];
    protected $action = '';

    protected function init()
    {
        // Load global language phrases
        $this->bb->lang->load('moderation');

        $this->bb->plugins->runHooks('moderation_start');

        $this->tid = $this->bb->getInput('tid', 0);
        $this->pid = $this->bb->getInput('pid', 0);
        $this->fid = $this->bb->getInput('fid', 0);
        $this->bb->pmid = $this->bb->getInput('pmid', 0);
        $this->action = $this->bb->getInput('action', '');

        if ($this->pid) {
            $this->curpost = $this->post->get_post($this->pid);
            if (!$this->curpost) {
                $this->bb->error($this->bb->lang->error_invalidpost);
                return 'exit';
            }
            $this->tid = $this->curpost['tid'];
        }
        $this->view->offsetSet('tid', $this->tid);
        $this->view->offsetSet('fid', $this->fid);

        if ($this->tid) {
            $this->curthread = $this->thread->get_thread($this->tid);
            if (!$this->curthread) {
                $this->bb->error($this->bb->lang->error_invalidthread);
                return 'exit';
            }
            $this->fid = $this->curthread['fid'];
        }

        if ($this->fid) {
            $this->modlogdata['fid'] = $this->fid;
            $forum = $this->forum->get_forum($this->fid);

            // Make navigation
            $this->bb->build_forum_breadcrumb($this->fid);

            // Get our permissions all nice and setup
            $permissions = $this->forum->forum_permissions($this->fid);
        }

        if ($this->bb->pmid > 0) {
            $query = $this->db->simple_select('privatemessages', 'uid, subject, ipaddress, fromid', "pmid = {$this->bb->pmid}");

            $pm = $this->db->fetch_array($query);

            if (!$pm) {
                $this->bb->error($this->bb->lang->error_invalidpm);
                return 'exit';
            }
        }

        // Get some navigation if we need it
        //$this->bb->input['action'] = $this->bb->getInput('action');
        switch ($this->bb->getInput('action', '')) {
            case 'reports':
                $this->bb->add_breadcrumb($this->bb->lang->reported_posts);
                break;
            case 'allreports':
                $this->bb->add_breadcrumb($this->bb->lang->all_reported_posts);
                break;
        }

        if (!empty($this->curthread)) {
            $this->curthread['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($this->curthread['subject']));
            $this->bb->add_breadcrumb($this->curthread['subject'], get_thread_link($this->curthread['tid']));
            $this->modlogdata['tid'] = $this->curthread['tid'];
        }

        if (!empty($forum)) {
            // Check if this forum is password protected and we have a valid password
            $this->forum->check_forum_password($forum['fid']);
        }

        $allowable_moderation_actions = [
            'getip',
            'getpmip',
            'cancel_delayedmoderation',
            'delayedmoderation',
            'threadnotes',
            'do_threadnotes',
            'purgespammer',
            'viewthreadnotes',
            'multideletethreads',
            'do_multideletethreads',
            'multideleteposts',
            'do_multideleteposts',
            'multiclosethreads',
        ];
        //FIXME
        if (/*$this->bb->request_method != 'post' &&*/
            (!in_array($this->action, $allowable_moderation_actions) &&
                !in_array($this->bb->current_page, $allowable_moderation_actions))
        ) {
            $this->bb->error_no_permission();
            return 'exit';
        }
    }

    /**
     * Some little handy functions for our inline moderation
     *
     * @param int $id
     * @param string $type
     *
     * @return array
     */
    public function getids($id, $type)
    {
        $newids = [];
        $cookie = 'inlinemod_' . $type . $id;
        if (isset($this->bb->cookies[$cookie])) {
            $cookie_ids = explode('|', $this->bb->cookies[$cookie]);

            foreach ($cookie_ids as $cookie_id) {
                if (empty($cookie_id)) {
                    continue;
                }

                if ($cookie_id == 'ALL') {
                    $newids += $this->getallids($id, $type);
                } else {
                    $newids[] = (int)$cookie_id;
                }
            }
        }

        return $newids;
    }

    /**
     * @param int $id
     * @param string $type
     *
     * @return array
     */
    public function getallids($id, $type)
    {
        $ids = [];

        // Get any removed threads (after our user hit 'all')
        $removed_ids = [];
        $cookie = 'inlinemod_' . $type . $id . '_removed';
        if (isset($this->bb->cookies[$cookie])) {
            $removed_ids = explode('|', $this->bb->cookies[$cookie]);

            if (!is_array($removed_ids)) {
                $removed_ids = [];
            }
        }

        // 'Select all Threads in this forum' only supported by forumdisplay and search
        if ($type == 'forum') {
            $query = $this->db->simple_select('threads', 'tid', "fid='" . (int)$id . "'");
            while ($this->tid = $this->db->fetch_field($query, 'tid')) {
                if (in_array($this->tid, $removed_ids)) {
                    continue;
                }

                $ids[] = $this->tid;
            }
        } elseif ($type == 'search') {
            $query = $this->db->simple_select(
                'searchlog',
                'resulttype, posts, threads',
                "sid='" . $this->db->escape_string($id) . "' AND uid='{$this->user->uid}'",
                1
            );
            $searchlog = $this->db->fetch_array($query);
            if ($searchlog['resulttype'] == 'posts') {
                $ids = explode(',', $searchlog['posts']);
            } else {
                $ids = explode(',', $searchlog['threads']);
            }

            if (is_array($ids)) {
                foreach ($ids as $key => $this->tid) {
                    if (in_array($this->tid, $removed_ids)) {
                        unset($ids[$key]);
                    }
                }
            }
        }

        return $ids;
    }

    /**
     * @param int $id
     * @param string $type
     */
    public function clearinline($id, $type)
    {
        $this->bb->my_unsetcookie('inlinemod_' . $type . $id);
        $this->bb->my_unsetcookie("inlinemod_{$type}{$id}_removed");
    }

    /**
     * @param int $id
     * @param string $type
     */
    public function extendinline($id, $type)
    {
        $this->bb->my_setcookie("inlinemod_{$type}{$id}", '', TIME_NOW + 3600);
        $this->bb->my_setcookie("inlinemod_{$type}{$id}_removed", '', TIME_NOW + 3600);
    }

    /**
     * Checks if the current user is a moderator of all the posts specified
     *
     * Note: If no posts are specified, this function will return true.  It is the
     * responsibility of the calling script to error-check this case if necessary.
     *
     * @param array $posts Array of post IDs
     * @param string $permission Permission to check
     * @return bool True if moderator of all; false otherwise
     */
    public function is_moderator_by_pids($posts, $permission = '')
    {
        // Speedy determination for supermods/admins and guests
        if ($this->bb->usergroup['issupermod']) {
            return true;
        } elseif (!$this->user->uid) {
            return false;
        }
        // Make an array of threads if not an array
        if (!is_array($posts)) {
            $posts = [$posts];
        }
        // Validate input
        $posts = array_map('intval', $posts);
        $posts[] = 0;
        // Get forums
        $posts_string = implode(',', $posts);
        $query = $this->db->simple_select('posts', 'DISTINCT fid', "pid IN ($posts_string)");
        while ($forum = $this->db->fetch_array($query)) {
            if (!$this->user->is_moderator($forum['fid'], $permission)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the current user is a moderator of all the threads specified
     *
     * Note: If no threads are specified, this function will return true.  It is the
     * responsibility of the calling script to error-check this case if necessary.
     *
     * @param array $threads Array of thread IDs
     * @param string $permission Permission to check
     * @return bool True if moderator of all; false otherwise
     */
    public function is_moderator_by_tids($threads, $permission = '')
    {
        // Speedy determination for supermods/admins and guests
        if ($this->bb->usergroup['issupermod']) {
            return true;
        } elseif (!$this->user->uid) {
            return false;
        }
        // Make an array of threads if not an array
        if (!is_array($threads)) {
            $threads = [$threads];
        }
        // Validate input
        $threads = array_map('intval', $threads);
        $threads[] = 0;
        // Get forums
        $threads_string = implode(',', $threads);
        $query = $this->db->simple_select('threads', 'DISTINCT fid', "tid IN ($threads_string)");
        while ($forum = $this->db->fetch_array($query)) {
            if (!$this->user->is_moderator($forum['fid'], $permission)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Special redirect that takes a return URL into account
     * @param string $url URL
     * @param string $message Message
     * @param string $title Title
     */
    public function moderation_redirect($url, $message = '', $title = '')
    {
        if (!empty($this->bb->input['url'])) {
            return $this->bb->redirect(htmlentities($this->bb->input['url']), $message, $title);
        }
        return $this->bb->redirect($url, $message, $title);
    }
}
