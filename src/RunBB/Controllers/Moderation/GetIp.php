<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Moderation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class GetIp extends Common
{
    public function index(Request $request, Response $response)
    {
//    $this->bb->input['action'] = 'getip';
        if ($this->init() === 'exit') {
            return;
        }

        // Let's look up the ip address of a post
        $this->bb->add_breadcrumb($this->lang->nav_getip);
        if (!$this->user->is_moderator($this->fid, 'canviewips')) {
            return $this->bb->error_no_permission();
        }

        $hostname = @gethostbyaddr($this->curpost['ipaddress']);
        if (!$hostname || $hostname == $this->curpost['ipaddress']) {
            $hostname = $this->lang->resolve_fail;
        }
        $this->view->offsetSet('hostname', $hostname);
        $this->view->offsetSet('ipaddress', $this->curpost['ipaddress']);

        $username = $this->user->build_profile_link($this->curpost['username'], $this->curpost['uid']);
        $this->view->offsetSet('username', $username);

        // Moderator options
        $this->bb->showModoptions = false;
        if ($this->bb->usergroup['canmodcp'] == 1 && $this->bb->usergroup['canuseipsearch'] == 1) {
            $this->bb->showModoptions = true;
            //$ipaddress = $this->curpost['ipaddress'];
            $this->view->offsetSet('ipaddress', $this->curpost['ipaddress']);

            //eval('\$modoptions = \"".$templates->get('moderation_getip_modoptions').'\';');
        }

        $this->plugins->runHooks('moderation_getip');

        //eval('\$getip = \"".$templates->get('moderation_getip').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Moderation/getip.html.twig');
    }
}
