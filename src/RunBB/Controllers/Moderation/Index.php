<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Moderation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $tool = $this->moderation->tool_info($this->action);
        if ($tool !== false) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $options = my_unserialize($tool['threadoptions']);

            if (!$this->user->is_member($tool['groups'])) {
                return $this->bb->error_no_permission();
            }

            if (!empty($options['confirmation']) && empty($this->bb->input['confirm'])) {
                $this->bb->add_breadcrumb($this->lang->confirm_execute_tool);

                $this->lang->confirm_execute_tool_desc = $this->lang->sprintf($this->lang->confirm_execute_tool_desc, htmlspecialchars_uni($tool['name']));

                $modtype = $this->bb->getInput('modtype', '');
                $inlinetype = $this->bb->getInput('inlinetype', '');
                $searchid = $this->bb->getInput('searchid', '');
                $url = htmlspecialchars_uni($this->bb->getInput('url', ''));
                $this->plugins->runHooks('moderation_confirmation');

                //ev al('$page = "'.$templates->get('moderation_confirmation').'";');
                $this->bb->output_page();
                return $this->view->render($response, '@forum/Moderation/confirmation.html.twig');
            }

            if ($tool['type'] == 't' && $this->bb->getInput('modtype', '') === 'inlinethread') {
                if ($this->bb->getInput('inlinetype', '') === 'search') {
                    $tids = $this->getids($this->bb->getInput('searchid', ''), 'search');
                } else {
                    $tids = $this->getids($this->fid, 'forum');
                }
                if (count($tids) < 1) {
                    $this->bb->error($this->lang->error_inline_nopostsselected);
                }
                if (!$this->is_moderator_by_tids($tids, 'canusecustomtools')) {
                    return $this->bb->error_no_permission();
                }

                $thread_options = my_unserialize($tool['threadoptions']);
                if ($thread_options['movethread'] && $this->bb->forum_cache[$thread_options['movethread']]['type'] != "f") {
                    $this->bb->error($this->lang->error_movetocategory);
                }

                $this->moderation->execute($this->bb->getInput('action', 0), $tids);
                $this->lang->custom_tool = $this->lang->sprintf($this->lang->custom_tool, $tool['name']);
                $this->bblogger->log_moderator_action($this->modlogdata, $this->lang->custom_tool);
                if ($this->bb->getInput('inlinetype', '') === 'search') {
                    $this->clearinline($this->bb->getInput('searchid', 0), 'search');
                    $this->lang->redirect_customtool_search = $this->lang->sprintf($this->lang->redirect_customtool_search, $tool['name']);
                    $return_url = htmlspecialchars_uni($this->bb->getInput('url', ''));
                    return $this->bb->redirect($return_url, $this->lang->redirect_customtool_search);
                } else {
                    $this->clearinline($this->fid, 'forum');
                    $this->lang->redirect_customtool_forum = $this->lang->sprintf($this->lang->redirect_customtool_forum, $tool['name']);
                    return $this->bb->redirect($this->forum->get_forum_link($this->fid), $this->lang->redirect_customtool_forum);
                }
            } elseif ($tool['type'] == 't' && $this->bb->getInput('modtype', '') === 'thread') {
                if (!$this->is_moderator_by_tids($this->tid, "canusecustomtools")) {
                    return $this->bb->error_no_permission();
                }

                $thread_options = my_unserialize($tool['threadoptions']);
                if ($thread_options['movethread'] && $this->bb->forum_cache[$thread_options['movethread']]['type'] != "f") {
                    $this->bb->error($this->lang->error_movetocategory);
                }

                $ret = $this->moderation->execute($this->bb->getInput('action', 0), $this->tid);
                $this->lang->custom_tool = $this->lang->sprintf($this->lang->custom_tool, $tool['name']);
                $this->bblogger->log_moderator_action($this->modlogdata, $this->lang->custom_tool);
                if ($ret == 'forum') {
                    $this->lang->redirect_customtool_forum = $this->lang->sprintf($this->lang->redirect_customtool_forum, $tool['name']);
                    return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $this->lang->redirect_customtool_forum);
                } else {
                    $this->lang->redirect_customtool_thread = $this->lang->sprintf($this->lang->redirect_customtool_thread, $tool['name']);
                    return $this->moderation_redirect(get_thread_link($thread['tid']), $this->lang->redirect_customtool_thread);
                }
            } elseif ($tool['type'] == 'p' && $this->bb->getInput('modtype', '') === 'inlinepost') {
                if ($this->bb->getInput('inlinetype', '') === 'search') {
                    $pids = $this->getids($this->bb->getInput('searchid', ''), 'search');
                } else {
                    $pids = $this->getids($this->tid, 'thread');
                }

                if (count($pids) < 1) {
                    $this->bb->error($this->lang->error_inline_nopostsselected);
                }
                if (!$this->is_moderator_by_pids($pids, "canusecustomtools")) {
                    return $this->bb->error_no_permission();
                }

                // Get threads which are associated with the posts
                $tids = [];
                $options = [
                    'order_by' => 'dateline',
                    'order_dir' => 'asc'
                ];
                $query = $this->db->simple_select("posts", "DISTINCT tid", "pid IN (" . implode(',', $pids) . ")", $options);
                while ($row = $this->db->fetch_array($query)) {
                    $tids[] = $row['tid'];
                }

                $ret = $this->moderation->execute($this->bb->getInput('action', 0), $tids, $pids);
                $this->lang->custom_tool = $this->lang->sprintf($this->lang->custom_tool, $tool['name']);
                $this->bblogger->log_moderator_action($this->modlogdata, $this->lang->custom_tool);
                if ($this->bb->getInput('inlinetype', '') === 'search') {
                    $this->clearinline($this->bb->getInput('searchid', 0), 'search');
                    $this->lang->redirect_customtool_search = $this->lang->sprintf($this->lang->redirect_customtool_search, $tool['name']);
                    $return_url = htmlspecialchars_uni($this->bb->getInput('url', ''));
                    return $this->bb->redirect($return_url, $this->lang->redirect_customtool_search);
                } else {
                    $this->clearinline($this->tid, 'thread');
                    if ($ret == 'forum') {
                        $this->lang->redirect_customtool_forum = $this->lang->sprintf($this->lang->redirect_customtool_forum, $tool['name']);
                        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $this->lang->redirect_customtool_forum);
                    } else {
                        $this->lang->redirect_customtool_thread = $this->lang->sprintf($this->lang->redirect_customtool_thread, $tool['name']);
                        return $this->moderation_redirect(get_thread_link($this->tid), $this->lang->redirect_customtool_thread);
                    }
                }
            }
        }
        \Tracy\Debugger::dump($this->action);
        if ($this->action === 'multideleteposts') {
            // Delete posts - Inline moderation
            (new \RunBB\Controllers\Moderation\Multideleteposts($this->get_container()))->index();
        } elseif ($this->action === 'do_multideleteposts') {
            // Actually delete the posts in inline moderation
            (new \RunBB\Controllers\Moderation\Multideleteposts($this->get_container()))->doMultiDelete();
        } elseif ($this->action === 'multideletethreads') {
            // Delete Threads - Inline moderation
            (new \RunBB\Controllers\Moderation\Multideletethreads($this->get_container()))->index();
        } elseif ($this->action === 'do_multideletethreads') {
            // Actually delete the threads - Inline moderation
            (new \RunBB\Controllers\Moderation\Multideletethreads($this->get_container()))->doMultiDelete();
        } elseif ($this->action === 'multiclosethreads') {
            // Close threads - Inline moderation
            (new \RunBB\Controllers\Moderation\Multiclosethreads($this))->go();
        } elseif ($this->action === 'threadnotes') {
            // Thread notes editor
            (new \RunBB\Controllers\Moderation\Threadnotes($this))->go();
        } elseif ($this->action === 'do_threadnotes') {
            // Update the thread notes!
            (new \RunBB\Controllers\Moderation\Threadnotes($this))->doThreadNotes();
        } else {
            return $this->bb->error_no_permission();
        }
    }
}
