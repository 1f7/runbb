<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Moderation;

class Multiclosethreads
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function go()
    {
        // Verify incoming POST request
        $this->app->bb->verify_post_check($this->app->bb->getInput('my_post_key', ''));

        if (!empty($this->app->bb->input['searchid'])) {
            // From search page
            $threads = $this->app->getids($this->app->bb->getInput('searchid', ''), 'search');
            if (!$this->app->is_moderator_by_tids($threads, 'canopenclosethreads')) {
                return $this->app->bb->error_no_permission();
            }
        } else {
            $threads = $this->app->getids($this->app->fid, 'forum');
            if (!$this->app->user->is_moderator($this->app->fid, 'canopenclosethreads')) {
                return $this->app->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            return $this->app->bb->error($this->app->lang->error_inline_nothreadsselected);
        }

        $this->app->moderation->close_threads($threads);

        $this->app->bblogger->log_moderator_action($this->app->modlogdata, $this->app->lang->multi_closed_threads);
        if ($this->app->bb->getInput('inlinetype', '') === 'search') {
            $this->app->clearinline($this->app->bb->getInput('searchid', 0), 'search');
        } else {
            $this->app->clearinline($this->app->fid, 'forum');
        }
        return $this->app->moderation_redirect(
            $this->app->forum->get_forum_link($this->app->fid),
            $this->app->lang->redirect_inline_threadsclosed
        );
    }
}
