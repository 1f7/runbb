<?php
/**
 * Created by PhpStorm.
 * User: svl
 * Date: 25.11.16
 * Time: 20:09
 */

namespace RunBB\Controllers\Moderation;

class Multideleteposts extends Common
{

    public function __construct($c)
    {
        parent::__construct($c);
    }

    public function index()
    {
        if ($this->init() === 'exit') {
            return;
        }

        $this->bb->add_breadcrumb($this->lang->nav_multi_deleteposts);

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }

        if (count($posts) < 1) {
            $this->bb->error($this->lang->error_inline_nopostsselected);
        }
        if (!$this->is_moderator_by_pids($posts, "candeleteposts")) {
            return $this->bb->error_no_permission();
        }

//        $inlineids = implode("|", $posts);
        $this->view->offsetSet('inlineids', implode('|', $posts));

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }

//        $return_url = htmlspecialchars_uni($this->bb->getInput('url', ''));
        $this->view->offsetSet('return_url', htmlspecialchars_uni($this->bb->getInput('url', '')));

        //ev al("\$multidelete = \"".$templates->get("moderation_inline_deleteposts")."\";");
        $this->bb->output_page();
        $this->view->render($this->response, '@forum/Moderation/inline_deleteposts.html.twig');
    }

    public function doMultiDelete()
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $postlist = explode('|', $this->bb->getInput('posts', ''));
        if (!$this->is_moderator_by_pids($postlist, 'candeleteposts')) {
            return $this->bb->error_no_permission();
        }
        $postlist = array_map('intval', $postlist);
        $pids = implode(',', $postlist);

        $tids = [];
        if ($pids) {
            $query = $this->db->simple_select('threads', 'tid', "firstpost IN({$pids})");
            while ($threadid = $this->db->fetch_field($query, 'tid')) {
                $tids[] = $threadid;
            }
        }

        $deletecount = 0;
        foreach ($postlist as $this->pid) {
            $this->pid = (int)$this->pid;
            $this->moderation->delete_post($this->pid);
            $plist[] = $this->pid;
            $deletecount++;
        }

        // If we have multiple threads, we must be coming from the search
        if (!empty($tids)) {
            foreach ($tids as $this->tid) {
                $this->moderation->delete_thread($this->tid);
                $this->moderation->mark_reports($this->tid, 'thread');
                $url = $this->forum->get_forum_link($this->fid);
            }
        } // Otherwise we're just deleting from showthread.php
        else {
            $query = $this->db->simple_select('posts', 'pid', "tid = $this->tid");
            $numposts = $this->db->num_rows($query);
            if (!$numposts) {
                $this->moderation->delete_thread($this->tid);
                $this->moderation->mark_reports($this->tid, 'thread');
                $url = $this->forum->get_forum_link($this->fid);
            } else {
                $this->moderation->mark_reports($plist, 'posts');
                $url = get_thread_link($this->tid);
            }
        }

        $this->lang->deleted_selective_posts = $this->lang->sprintf($this->lang->deleted_selective_posts, $deletecount);
        $this->bblogger->log_moderator_action($this->modlogdata, $this->lang->deleted_selective_posts);
        return $this->moderation_redirect($url, $this->lang->redirect_postsdeleted);
    }
}
