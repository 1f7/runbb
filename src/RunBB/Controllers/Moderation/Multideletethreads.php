<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Controllers\Moderation;

class Multideletethreads extends Common
{
    public function __construct($c)
    {
        parent::__construct($c);
    }

    public function index()
    {
        if ($this->init() === 'exit') {
            return;
        }

        $this->bb->add_breadcrumb($this->lang->nav_multi_deletethreads);

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'candeletethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'candeletethreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($this->lang->error_inline_nothreadsselected);
        }

//        $inlineids = implode('|', $threads);
        $this->view->offsetSet('inlineids', implode('|', $threads));

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }

        $this->view->offsetSet('return_url', htmlspecialchars_uni($this->bb->getInput('url', '')));

//        eval('\$multidelete = \''.$templates->get('moderation_inline_deletethreads.html.twig').'\';');
        $this->bb->output_page();
        $this->view->render($this->response, '@forum/Moderation/inline_deletethreads.html.twig');
    }

    public function doMultiDelete()
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $threadlist = explode('|', $this->bb->getInput('threads', ''));
        if (!$this->is_moderator_by_tids($threadlist, 'candeletethreads')) {
            return $this->bb->error_no_permission();
        }
        $tlist=[];
        foreach ($threadlist as $this->tid) {
            $this->tid = (int)$this->tid;
            $this->moderation->delete_thread($this->tid);
            $tlist[] = $this->tid;
        }
        $this->bblogger->log_moderator_action($this->modlogdata, $this->lang->multi_deleted_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        $this->moderation->mark_reports($tlist, 'threads');
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $this->lang->redirect_inline_threadsdeleted);
    }
}
