<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Moderation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Purgespammer extends Common
{
    private function check()
    {
        $groups = explode(',', $this->bb->settings['purgespammergroups']);
        if (!in_array($this->user->usergroup, $groups)) {
            return $this->bb->error_no_permission();
        }

        $this->uid = $this->bb->input['uid'];
        $this->curuser = $this->user->get_user($this->uid);
        if (!$this->curuser['uid'] || !$this->user->purgespammer_show($this->curuser['postnum'], $this->curuser['usergroup'], $this->curuser['uid'])) {
            $this->bb->error($this->lang->purgespammer_invalid_user);
        }
    }

    public function index(Request $request, Response $response)
    {
        $this->bb->input['action'] = 'purgespammer';
        if ($this->init() === 'exit') {
            return;
        }
        $this->check();

        $this->plugins->runHooks('moderation_purgespammer_show');

        $this->bb->add_breadcrumb($this->lang->purgespammer);
        $this->lang->purgespammer_purge = $this->lang->sprintf($this->lang->purgespammer_purge, $this->curuser['username']);
        if ($this->bb->settings['purgespammerbandelete'] == 'ban') {
            $this->lang->purgespammer_purge_desc = $this->lang->sprintf($this->lang->purgespammer_purge_desc, $this->lang->purgespammer_ban);
        } else {
            $this->lang->purgespammer_purge_desc = $this->lang->sprintf($this->lang->purgespammer_purge_desc, $this->lang->purgespammer_delete);
        }
        //ev al('\$purgespammer = \"".$templates->get('moderation_purgespammer').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Moderation/purgespammer.html.twig');
    }

    public function doPurgespammer(Request $request, Response $response)
    {
        $this->bb->input['action'] = 'purgespammer';
        if ($this->init() === 'exit') {
            return;
        }
        $this->check();

        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $user_deleted = false;

        // Run the hooks first to avoid any issues when we delete the user
        $this->plugins->runHooks('moderation_purgespammer_purge');

        $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'delete');

        if ($this->bb->settings['purgespammerbandelete'] == 'ban') {
            // First delete everything
            $userhandler->delete_content($this->uid);
            $userhandler->delete_posts($this->uid);

            // Next ban him (or update the banned reason, shouldn't happen)
            $query = $this->db->simple_select('banned', 'uid', "uid = '{$this->uid}'");
            if ($this->db->num_rows($query) > 0) {
                $banupdate = [
                    'reason' => $this->db->escape_string($this->bb->settings['purgespammerbanreason'])
                ];
                $this->db->update_query('banned', $banupdate, "uid = '{$this->uid}'");
            } else {
                $insert = [
                    'uid' => $this->uid,
                    'gid' => (int)$this->bb->settings['purgespammerbangroup'],
                    'oldgroup' => 2,
                    'oldadditionalgroups' => '',
                    'olddisplaygroup' => 0,
                    'admin' => (int)$this->user->uid,
                    'dateline' => TIME_NOW,
                    'bantime' => '---',
                    'lifted' => 0,
                    'reason' => $this->db->escape_string($this->bb->settings['purgespammerbanreason'])
                ];
                $this->db->insert_query('banned', $insert);
            }

            // Add the IP's to the banfilters
            foreach ([$this->curuser['regip'], $this->curuser['lastip']] as $ip) {
                $query = $this->db->simple_select('banfilters', 'type', "type = 1 AND filter = '" . $this->db->escape_string($ip) . "'");
                if ($this->db->num_rows($query) == 0) {
                    $insert = [
                        'filter' => $this->db->escape_string($ip),
                        'type' => 1,
                        'dateline' => TIME_NOW
                    ];
                    $this->db->insert_query('banfilters', $insert);
                }
            }

            // Clear the profile
            $userhandler->clear_profile($this->uid, $this->bb->settings['purgespammerbangroup']);

            $this->cache->update_banned();
            $this->cache->update_bannedips();
            $this->cache->update_awaitingactivation();

            // Update reports cache
            $this->cache->update_reportedcontent();
        } elseif ($this->bb->settings['purgespammerbandelete'] == 'delete') {
            $user_deleted = $userhandler->delete_user($this->uid, 1);
        }

        // Submit the user to stop forum spam
        if (!empty($this->bb->settings['purgespammerapikey'])) {
            $sfs = @fetch_remote_file('http://stopforumspam.com/add.php?username=' . urlencode($this->curuser['username'])
                . '&ip_addr=' . urlencode($this->curuser['lastip'])
                . '&email=' . urlencode($this->curuser['email'])
                . '&api_key=' . urlencode($this->bb->settings['purgespammerapikey']));
        }

        $this->bblogger->log_moderator_action(
            [
                'uid' => $this->uid,
                'username' => $this->curuser['username']
            ],
            $this->lang->purgespammer_modlog
        );

        if ($user_deleted) {
            return $this->bb->redirect($this->bb->settings['bburl'], $this->lang->purgespammer_success);
        } else {
            return $this->bb->redirect(get_profile_link($this->uid), $this->lang->purgespammer_success);
        }
    }
}
