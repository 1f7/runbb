<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Moderation;

class Threadnotes
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    }

    public function go()
    {
        $this->app->bb->add_breadcrumb($this->app->lang->nav_threadnotes);
        if (!$this->app->user->is_moderator($this->app->fid, 'canmanagethreads')) {
            return $this->app->bb->error_no_permission();
        }
        $trow = alt_trow(1);
        $this->app->view->offsetSet('notes', htmlspecialchars_uni($this->app->parser->parse_badwords($this->app->curthread['notes'])));

        if ($this->app->user->is_moderator($this->app->fid, 'canviewmodlog')) {
            $query = $this->app->db->query('
				SELECT l.*, u.username, t.subject AS tsubject, f.name AS fname, p.subject AS psubject
				FROM '.TABLE_PREFIX.'moderatorlog l
				LEFT JOIN '.TABLE_PREFIX.'users u ON (u.uid=l.uid)
				LEFT JOIN '.TABLE_PREFIX.'threads t ON (t.tid=l.tid)
				LEFT JOIN '.TABLE_PREFIX.'forums f ON (f.fid=l.fid)
				LEFT JOIN '.TABLE_PREFIX."posts p ON (p.pid=l.pid)
				WHERE t.tid='{$this->app->tid}'
				ORDER BY l.dateline DESC
				LIMIT  0, 20
			");
            $modactions = [];
            while ($modaction = $this->app->db->fetch_array($query)) {
                $modaction['dateline'] = $this->app->time->formatDate('jS M Y, G:i', $modaction['dateline']);
                $modaction['profilelink'] = $this->app->user->build_profile_link($modaction['username'], $modaction['uid']);
                $modaction['action'] = htmlspecialchars_uni($modaction['action']);
                $info = '';
                if ($modaction['tsubject']) {
                    $info .= "<strong>{$this->app->lang->thread}</strong> <a href=\"".get_thread_link($modaction['tid'])."\">".htmlspecialchars_uni($modaction['tsubject'])."</a><br />";
                }
                if ($modaction['fname']) {
                    $info .= "<strong>{$this->app->lang->forum}</strong> <a href=\"".$this->app->forum->get_forum_link($modaction['fid'])."\">".htmlspecialchars_uni($modaction['fname'])."</a><br />";
                }
                if ($modaction['psubject']) {
                    $info .= "<strong>{$this->app->lang->post}</strong> <a href=\"".get_post_link($modaction['pid'])."#pid".$modaction['pid']."\">".htmlspecialchars_uni($modaction['psubject'])."</a>";
                }
                $modactions[]=[
                    'trow' => $trow,
                    'modaction' => $modaction,
                    'info' => $info
                ];
                $trow = alt_trow();
            }
            $this->app->view->offsetSet('modactions', $modactions);
        }

        $actions = [
            'openclosethread' => $this->app->lang->open_close_thread,
            'deletethread' => $this->app->lang->delete_thread,
            'move' => $this->app->lang->move_copy_thread,
            'stick' => $this->app->lang->stick_unstick_thread,
            'merge' => $this->app->lang->merge_threads,
            'removeredirects' => $this->app->lang->remove_redirects,
            'removesubscriptions' => $this->app->lang->remove_subscriptions,
            'approveunapprovethread' => $this->app->lang->approve_unapprove_thread
        ];

        switch ($this->app->db->type) {
            case 'pgsql':
            case 'sqlite':
                $query = $this->app->db->simple_select('modtools', 'tid, name', "(','||forums||',' LIKE '%,{$this->app->fid},%' OR ','||forums||',' LIKE '%,-1,%' OR forums='') AND type = 't'");
                break;
            default:
                $query = $this->app->db->simple_select('modtools', 'tid, name', "(CONCAT(',',forums,',') LIKE '%,{$this->app->fid},%' OR CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='') AND type = 't'");
        }
        while ($tool = $this->app->db->fetch_array($query)) {
            $actions['modtool_'.$tool['tid']] = htmlspecialchars_uni($tool['name']);
        }

        $this->app->bb->forum_cache = $this->app->cache->read('forums');

        $trow = alt_trow(1);
        switch ($this->app->db->type) {
            case 'pgsql':
            case 'sqlite':
                $query = $this->app->db->query('
					SELECT d.*, u.username, f.name AS fname
					FROM '.TABLE_PREFIX.'delayedmoderation d
					LEFT JOIN '.TABLE_PREFIX.'users u ON (u.uid=d.uid)
					LEFT JOIN '.TABLE_PREFIX."forums f ON (f.fid=d.fid)
					WHERE ','||d.tids||',' LIKE '%,{$this->app->tid},%'
					ORDER BY d.dateline DESC
					LIMIT  0, 20
				");
                break;
            default:
                $query = $this->app->db->query('
					SELECT d.*, u.username, f.name AS fname
					FROM '.TABLE_PREFIX.'delayedmoderation d
					LEFT JOIN '.TABLE_PREFIX.'users u ON (u.uid=d.uid)
					LEFT JOIN '.TABLE_PREFIX."forums f ON (f.fid=d.fid)
					WHERE CONCAT(',',d.tids,',') LIKE '%,{$this->app->tid},%'
					ORDER BY d.dateline DESC
					LIMIT  0, 20
				");
        }
        $delayedmods = [];
        while ($delayedmod = $this->app->db->fetch_array($query)) {
            $delayedmod['dateline'] = $this->app->time->formatDate('jS M Y, G:i', $delayedmod['delaydateline']);
            $delayedmod['profilelink'] = $this->app->user->build_profile_link($delayedmod['username'], $delayedmod['uid']);
            $delayedmod['action'] = $actions[$delayedmod['type']];
            $info = '';
            if (strpos($delayedmod['tids'], ',') === false) {
                $info .= "<strong>{$this->app->lang->thread}</strong> <a href=\"".get_thread_link($delayedmod['tids'])."\">{$this->app->curthread['subject']}</a><br />";
            } else {
                $info .= "<strong>{$this->app->lang->thread}</strong> {$this->app->lang->multiple_threads}<br />";
            }

            if ($delayedmod['fname']) {
                $info .= "<strong>{$this->app->lang->forum}</strong> <a href=\"".$this->app->forum->get_forum_link($delayedmod['fid'])."\">".htmlspecialchars_uni($delayedmod['fname'])."</a><br />";
            }
            $delayedmod['inputs'] = my_unserialize($delayedmod['inputs']);

            if ($delayedmod['type'] == 'move') {
                $info .= "<strong>{$this->app->lang->new_forum}</strong>  <a href=\"".$this->app->forum->get_forum_link($delayedmod['inputs']['new_forum'])."\">".htmlspecialchars_uni($this->app->bb->forum_cache[$delayedmod['inputs']['new_forum']]['name'])."</a><br />";
                if ($delayedmod['inputs']['method'] == "redirect") {
                    $info .= "<strong>{$this->app->lang->leave_redirect_for}</strong> ".(int)$delayedmod['inputs']['redirect_expire']." {$this->app->lang->days}<br />";
                }
            } elseif ($delayedmod['type'] == 'merge') {
                $info .= "<strong>{$this->app->lang->new_subject}</strong> ".htmlspecialchars_uni($delayedmod['inputs']['subject'])."<br />";
                $info .= "<strong>{$this->app->lang->thread_to_merge_with}</strong> <a href=\"".htmlspecialchars_uni($delayedmod['inputs']['threadurl'])."\">".htmlspecialchars_uni($delayedmod['inputs']['threadurl'])."</a><br />";
            }
            $delayedmods[]=[
                'trow' => $trow,
                'delayedmod' => $delayedmod,
                'info' => $info
            ];
            $trow = alt_trow();
        }
        $this->app->view->offsetSet('delayedmods', $delayedmods);

        $this->app->plugins->runHooks('moderation_threadnotes');

        $this->app->bb->output_page();
        $this->app->view->render($this->app->response, '@forum/Moderation/threadnotes.html.twig');
    }

    public function doThreadNotes()
    {
        // Verify incoming POST request
        $this->app->bb->verify_post_check($this->app->bb->getInput('my_post_key', ''));

        if (!$this->app->user->is_moderator($this->app->fid, 'canmanagethreads')) {
            return $this->app->bb->error_no_permission();
        }

        $this->app->plugins->runHooks('moderation_do_threadnotes');

        $this->app->bblogger->log_moderator_action($this->app->modlogdata, $this->app->lang->thread_notes_edited);
        $sqlarray = [
            'notes' => $this->app->db->escape_string($this->app->bb->getInput('threadnotes', '')),
        ];
        $this->app->db->update_query('threads', $sqlarray, "tid='{$this->app->tid}'");

        return $this->app->moderation_redirect(get_thread_link($this->app->curthread['tid']), $this->app->lang->redirect_threadnotesupdated);
    }
}
