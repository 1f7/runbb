<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class Newreply extends AbstractController
{

    private function init()
    {
        // Load global language phrases
        $this->lang->load('newreply');

        // Get the pid and tid and replyto from the input.
        $this->tid = $this->bb->getInput('tid', 0);
        $this->replyto = $this->bb->getInput('replyto', 0);

        // AJAX quick reply?
        if (!empty($this->bb->input['ajax'])) {
            unset($this->bb->input['previewpost']);
        }
        // Edit a draft post.
        $this->bb->pid = 0;
        $editdraftpid = '';
        $this->bb->input['action'] = $this->bb->getInput('action', '');
        if (($this->bb->input['action'] == 'editdraft') && $this->bb->getInput('pid', 0)) {
            $this->bb->pid = $this->bb->getInput('pid', 0);
            $this->curpost = $this->post->get_post($this->bb->pid);
            if (!$this->curpost) {
                $this->bb->error($this->lang->error_invalidpost);
                return 'exit';
            } elseif ($this->user->uid != $this->curpost['uid']) {
                $this->bb->error($this->lang->error_post_noperms);
                return 'exit';
            }
            $this->bb->pid = $this->curpost['pid'];
            $this->tid = $this->curpost['tid'];
            $editdraftpid = "<input type=\"hidden\" name=\"pid\" value=\"{$this->bb->pid}\" />";
        }

        // Set up $thread and $forum for later use.
        $this->bb->threads = $this->thread->get_thread($this->tid);
        if (!$this->bb->threads) {
            $this->bb->error($this->lang->error_invalidthread);
            return 'exit';
        }
        $this->bb->fid = $this->bb->threads['fid'];

        // Get forum info
        $this->bb->forums = $this->forum->get_forum($this->bb->fid);
        if (!$this->bb->forums) {
            $this->bb->error($this->lang->error_invalidforum);
            return 'exit';
        }

        // Make navigation
        $this->bb->build_forum_breadcrumb($this->bb->fid);
        $this->thread_subject = $this->bb->threads['subject'];
        $this->bb->threads['subject'] =
            htmlspecialchars_uni($this->parser->parse_badwords($this->bb->threads['subject']));
        $this->bb->add_breadcrumb($this->bb->threads['subject'], get_thread_link($this->bb->threads['tid']));
        $this->bb->add_breadcrumb($this->lang->nav_newreply);

        $this->forumpermissions = $this->forum->forum_permissions($this->bb->fid);

        // See if everything is valid up to here.
        if (isset($this->curpost) && (($this->curpost['visible'] == 0 &&
                    !$this->user->is_moderator($this->bb->fid, 'canviewunapprove')) ||
                ($this->curpost['visible'] < 0 && $this->curpost['uid'] != $this->user->uid))
        ) {
            $this->bb->error($this->lang->error_invalidpost);
            return 'exit';
        }
        if (($this->bb->threads['visible'] == 0 &&
                !$this->user->is_moderator($this->bb->fid, 'canviewunapprove')) ||
            $this->bb->threads['visible'] < 0
        ) {
            $this->bb->error($this->lang->error_invalidthread);
            return 'exit';
        }
        if ($this->bb->forums['open'] == 0 || $this->bb->forums['type'] != 'f') {
            $this->bb->error($this->lang->error_closedinvalidforum);
            return 'exit';
        }
        if ($this->forumpermissions['canview'] == 0 || $this->forumpermissions['canpostreplys'] == 0) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        if ($this->user->suspendposting == 1) {
            $suspendedpostingtype = $this->lang->error_suspendedposting_permanent;
            if ($this->user->suspensiontime) {
                $suspendedpostingtype = $this->lang->sprintf(
                    $this->lang->error_suspendedposting_temporal,
                    $this->time->formatDate($this->bb->settings['dateformat'], $this->user->suspensiontime)
                );
            }

            $this->lang->error_suspendedposting = $this->lang->sprintf(
                $this->lang->error_suspendedposting,
                $suspendedpostingtype,
                $this->time->formatDate($this->bb->settings['timeformat'], $this->user->suspensiontime)
            );

            $this->bb->error($this->lang->error_suspendedposting);
            return 'exit';
        }

        if (isset($this->forumpermissions['canonlyviewownthreads']) &&
            $this->forumpermissions['canonlyviewownthreads'] == 1 &&
            $this->bb->threads['uid'] != $this->user->uid
        ) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        if (isset($this->forumpermissions['canonlyreplyownthreads']) &&
            $this->forumpermissions['canonlyreplyownthreads'] == 1 &&
            $this->bb->threads['uid'] != $this->user->uid
        ) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        // Coming from quick reply? Set some defaults
        if ($this->bb->getInput('method', '') === 'quickreply') {
            if ($this->user->subscriptionmethod == 1) {
                $this->bb->input['postoptions']['subscriptionmethod'] = 'none';
            } elseif ($this->user->subscriptionmethod == 2) {
                $this->bb->input['postoptions']['subscriptionmethod'] = 'email';
            } elseif ($this->user->subscriptionmethod == 3) {
                $this->bb->input['postoptions']['subscriptionmethod'] = 'pm';
            }
        }

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($this->bb->forums['fid']);

        if ($this->bb->settings['bbcodeinserter'] != 0 &&
            $this->bb->forums['allowmycode'] != 0 &&
            (!$this->user->uid || $this->user->showcodebuttons != 0)
        ) {
            $this->bb->codebuttons = $this->editor->build_mycode_inserter('message', $this->bb->forums['allowsmilies']);
            if ($this->bb->forums['allowsmilies'] != 0) {
                $this->bb->smilieinserter = $this->editor->build_clickable_smilies();
            }
        }

        // Check to see if the thread is closed, and if the user is a mod.
        if (!$this->user->is_moderator($this->bb->fid, 'canpostclosedthreads')) {
            if ($this->bb->threads['closed'] == 1) {
                $this->bb->error($this->lang->redirect_threadclosed);
                return 'exit';
            }
        }

        // No weird actions allowed, show new reply form if no regular action.
        if ($this->bb->input['action'] != 'do_newreply' && $this->bb->input['action'] != 'editdraft') {
            $this->bb->input['action'] = 'newreply';
        }

        // Even if we are previewing, still show the new reply form.
        if (!empty($this->bb->input['previewpost'])) {
            $this->bb->input['action'] = 'newreply';
        }

        // Setup a unique posthash for attachment management
        if (!$this->bb->getInput('posthash', '') && !$this->bb->pid) {
            $this->bb->input['posthash'] = md5($this->bb->threads['tid'] . $this->user->uid . random_str());
        }

        if ((empty($_POST) && empty($_FILES)) && $this->bb->getInput('processed', 0) == 1) {
            $this->bb->error($this->lang->error_cannot_upload_php_post);
            return 'exit';
        }

        $this->errors = [];
        $maximageserror = $attacherror = '';
        if ($this->bb->settings['enableattachments'] == 1 &&
            !$this->bb->getInput('attachmentaid', 0) &&
            ($this->bb->getInput('newattachment', '') ||
                $this->bb->getInput('updateattachment', '') ||
                (
//          $this->bb->input['action'] == 'do_newreply' &&
                    $this->bb->getInput('submit', '') &&
                    $_FILES['attachment']))
        ) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            if ($this->bb->pid) {
                $attachwhere = "pid='{$this->bb->pid}'";
            } else {
                $attachwhere = "posthash='" . $this->bb->getInput('posthash', '') . "'";
            }

            // If there's an attachment, check it and upload it
            if ($this->forumpermissions['canpostattachments'] != 0) {
                // If attachment exists..
                if (!empty($_FILES['attachment']['name']) && !empty($_FILES['attachment']['type'])) {
                    if ($_FILES['attachment']['size'] > 0) {
                        $query = $this->db->simple_select(
                            'attachments',
                            'aid',
                            "filename='" . $_FILES['attachment']['name'] . "' AND {$attachwhere}"
                        );
                        $updateattach = $this->db->fetch_field($query, 'aid');

                        $update_attachment = false;
                        if ($updateattach > 0 && $this->bb->getInput('updateattachment', '')) {
                            $update_attachment = true;
                        }
                        $attachedfile = $this->upload->upload_attachment($_FILES['attachment'], $update_attachment);
                    } else {
                        $this->errors[] = $this->lang->error_uploadempty;
                        $this->bb->input['action'] = 'newreply';
                    }
                }
            }

            if (!empty($attachedfile['error'])) {
                $this->errors[] = $attachedfile['error'];
                $this->bb->input['action'] = 'newreply';
            }

            if (!$this->bb->getInput('submit', '')) {
                $editdraftpid = "<input type=\"hidden\" name=\"pid\" value=\"{$this->bb->pid}\" />";
                $this->bb->input['action'] = 'newreply';
            }
        }

        // Remove an attachment.
        if ($this->bb->settings['enableattachments'] == 1 &&
            $this->bb->getInput('attachmentaid', 0) &&
            $this->bb->getInput('attachmentact', '') === 'remove'
        ) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->upload->remove_attachment(
                $this->bb->pid,
                $this->bb->getInput('posthash', ''),
                $this->bb->getInput('attachmentaid', 0)
            );
            if (!$this->bb->getInput('submit', '')) {
                $editdraftpid = "<input type=\"hidden\" name=\"pid\" value=\"{$this->bb->pid}\" />";
                $this->bb->input['action'] = 'newreply';
            }
        }
        $this->view->offsetSet('editdraftpid', $editdraftpid);

        $this->reply_errors = $this->quoted_ids = '';
        $this->hide_captcha = false;

        // Check the maximum posts per day for this user
        if ($this->bb->usergroup['maxposts'] > 0 && $this->bb->usergroup['cancp'] != 1) {
            $daycut = TIME_NOW - 60 * 60 * 24;
            $query = $this->db->simple_select(
                'posts',
                'COUNT(*) AS posts_today',
                "uid='{$this->user->uid}' AND visible='1' AND dateline>{$daycut}"
            );
            $post_count = $this->db->fetch_field($query, 'posts_today');
            if ($post_count >= $this->bb->usergroup['maxposts']) {
                $this->lang->error_maxposts = $this->lang->sprintf(
                    $this->lang->error_maxposts,
                    $this->bb->usergroup['maxposts']
                );
                $this->bb->error($this->lang->error_maxposts);
                return 'exit';
            }
        } elseif ($this->bb->input['action'] == 'newreply') {
            return 'index';
        }
    }

    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() == 'exit') {
                return;
            }
        }
        $this->plugins->runHooks('newreply_start');

        $quote_ids = '';
        $multiquote = [];
        // If this isn't a preview and we're not editing a draft, then handle quoted posts
        if (empty($this->bb->input['previewpost']) &&
            !$this->reply_errors &&
            $this->bb->input['action'] != 'editdraft' &&
            !$this->bb->getInput('attachmentaid', 0) &&
            !$this->bb->getInput('newattachment', '') &&
            !$this->bb->getInput('updateattachment', '') &&
            !$this->bb->getInput('rem', '')
        ) {
            $message = '';
            $quoted_posts = [];
            // Handle multiquote
            if (isset($this->bb->cookies['multiquote']) && $this->bb->settings['multiquote'] != 0) {
                $multiquoted = explode('|', $this->bb->cookies['multiquote']);
                foreach ($multiquoted as $post) {
                    $quoted_posts[$post] = (int)$post;
                }
            }
            // Handle incoming 'quote' button
            if ($this->replyto) {
                $quoted_posts[$this->replyto] = $this->replyto;
            }

            // Quoting more than one post - fetch them
            if (count($quoted_posts) > 0) {
                $external_quotes = 0;
//                $quoted_posts = implode(',', $quoted_posts);
                $unviewable_forums = $this->forum->get_unviewable_forums();
                $inactiveforums = $this->forum->get_inactive_forums();
//                if ($unviewable_forums) {
//                    $unviewable_forums = ' AND t.fid NOT IN ('.implode(',', $unviewable_forums).')';
//                }
//                if ($inactiveforums) {
//                    $inactiveforums = ' AND t.fid NOT IN ('.implode(',', $inactiveforums).')';
//                }

                // Check group permissions if we can't view threads not started by us
                $group_permissions = $this->forum->forum_permissions();
                $onlyusfids = [];
                $onlyusforums = '';
                foreach ($group_permissions as $this->bb->fid => $forum_permissions) {
                    if (isset($forum_permissions['canonlyviewownthreads']) &&
                        $forum_permissions['canonlyviewownthreads'] == 1
                    ) {
                        $onlyusfids[] = $this->bb->fid;
                    }
                }
//                if (!empty($onlyusfids)) {
//                    $onlyusforums = 'AND ((t.fid IN(' . implode(',', $onlyusfids) . ")
//              AND t.uid='{$this->user->uid}') OR t.fid NOT IN(" . implode(',', $onlyusfids) . '))';
//                }

                if ($this->user->is_moderator($this->bb->fid, 'canviewunapprove') &&
                    $this->user->is_moderator($this->bb->fid, 'canviewdeleted')
                ) {
//                    $visible_where = 'AND p.visible IN (-1,0,1)';
                    $visible_where = [-1,0,1];
                } elseif ($this->user->is_moderator($this->bb->fid, 'canviewunapprove') &&
                    !$this->user->is_moderator($this->bb->fid, 'canviewdeleted')
                ) {
//                    $visible_where = 'AND p.visible IN (0,1)';
                    $visible_where = [0,1];
                } elseif (!$this->user->is_moderator($this->bb->fid, 'canviewunapprove') &&
                    $this->user->is_moderator($this->bb->fid, 'canviewdeleted')
                ) {
//                    $visible_where = 'AND p.visible IN (-1,1)';
                    $visible_where = [-1,1];
                } else {
//                    $visible_where = 'AND p.visible=1';
                    $visible_where = 1;
                }
//                $query = $this->db->query('
//				SELECT p.subject, p.message, p.pid, p.tid, p.username, p.dateline, u.username AS userusername
//				FROM ' . TABLE_PREFIX . 'posts p
//				LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
//				LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
//				WHERE p.pid IN ({$quoted_posts}) {$unviewable_forums} {$inactiveforums} {$onlyusforums} {$visible_where}
//			");
                $p = \RunBB\Models\Post::whereIn('posts.pid', $quoted_posts)
                    ->where(function ($q) use ($unviewable_forums, $inactiveforums) {
                        if ($unviewable_forums) {
                            $q->whereNotIn('threads.fid', $unviewable_forums);
                        }
                        if ($inactiveforums) {
                            $q->whereNotIn('threads.fid', $inactiveforums);
                        }
                    })
                    ->where(function ($q) use ($onlyusfids) {
                        if (!empty($onlyusfids)) {
                            $q->whereIn('threads.fid', $onlyusfids);
                            $q->where('threads.fid', '=', $this->user->uid);
                            $q->whereNotIn('threads.fid', $onlyusfids);
                        }
                    })
                    ->where(function ($q) use ($visible_where) {
                        if (is_array($visible_where)) {
                            $q->whereIn('posts.visible', $visible_where);
                        } else {
                            $q->where('posts.visible', '=', $visible_where);
                        }
                    })
                    ->leftJoin('threads', 'threads.tid', '=', 'posts.tid')
                    ->leftJoin('users', 'users.uid', '=', 'posts.uid')
                    ->get([
                        'posts.subject',
                        'posts.message',
                        'posts.pid',
                        'posts.tid',
                        'posts.username',
                        'posts.dateline',
                        'users.username AS userusername'
                    ])
                    ->toArray();
                $load_all = $this->bb->getInput('load_all_quotes', 0);
//                while ($quoted_post = $this->db->fetch_array($query)) {
                foreach ($p as $quoted_post) {
                    // Only show messages for the current thread
                    if ($quoted_post['tid'] == $this->tid || $load_all == 1) {
                        // If this post was the post for which a quote button was clicked, set the subject
                        if ($this->replyto == $quoted_post['pid']) {
                            $subject = preg_replace('#^RE:\s?#i', '', $quoted_post['subject']);
                            // Subject too long? Shorten it to avoid error message
                            if (my_strlen($subject) > 85) {
                                $subject = my_substr($subject, 0, 82) . '...';
                            }
                            $subject = 'RE: ' . $subject;
                        }
                        $message .= $this->post->parse_quoted_message($quoted_post);
                        $this->quoted_ids[] = $quoted_post['pid'];
                    } // Count the rest
                    else {
                        ++$external_quotes;
                    }
                }
                if ($this->bb->settings['maxquotedepth'] != '0') {
                    $message = $this->post->remove_message_quotes($message);
                }
                if ($external_quotes > 0) {
                    if ($external_quotes == 1) {
                        $multiquote_text = $this->lang->multiquote_external_one;
                        $multiquote_deselect = $this->lang->multiquote_external_one_deselect;
                        $multiquote_quote = $this->lang->multiquote_external_one_quote;
                    } else {
                        $multiquote_text = $this->lang->sprintf($this->lang->multiquote_external, $external_quotes);
                        $multiquote_deselect = $this->lang->multiquote_external_deselect;
                        $multiquote_quote = $this->lang->multiquote_external_quote;
                    }
                    $multiquote[] = [
                        'text' => $multiquote_text,
                        'deselect' => $multiquote_deselect,
                        'quote' => $multiquote_quote
                    ];
                }
                if (is_array($this->quoted_ids) && count($this->quoted_ids) > 0) {
                    $this->quoted_ids = implode('|', $this->quoted_ids);
                }
            }
        }
        $this->view->offsetSet('multiquote', $multiquote);

        if (isset($this->bb->input['quoted_ids'])) {
            $this->quoted_ids = htmlspecialchars_uni($this->bb->getInput('quoted_ids', 0));
        }
        $this->view->offsetSet('quoted_ids', $this->quoted_ids);

        if (isset($this->bb->input['previewpost'])) {
            $previewmessage = $this->bb->getInput('message', '', true);
        }
        if (empty($message)) {
            $message = $this->bb->getInput('message', '', true);
        }

        $this->view->offsetSet('message', htmlspecialchars_uni($message));

        $postoptionschecked = ['signature' => '', 'disablesmilies' => ''];
        $postoptions_subscriptionmethod_dont = $postoptions_subscriptionmethod_none =
        $postoptions_subscriptionmethod_email = $postoptions_subscriptionmethod_pm = '';

        // Set up the post options.
        if (!empty($this->bb->input['previewpost']) || $this->reply_errors != '') {
            $postoptions = $this->bb->getInput('postoptions', []);

            if (isset($postoptions['signature']) && $postoptions['signature'] == 1) {
                $postoptionschecked['signature'] = ' checked="checked"';
            }
            if (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'none') {
                $postoptions_subscriptionmethod_none = 'checked="checked"';
            } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'email') {
                $postoptions_subscriptionmethod_email = 'checked="checked"';
            } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'pm') {
                $postoptions_subscriptionmethod_pm = 'checked="checked"';
            } else {
                $postoptions_subscriptionmethod_dont = 'checked="checked"';
            }
            if (isset($postoptions['disablesmilies']) && $postoptions['disablesmilies'] == 1) {
                $postoptionschecked['disablesmilies'] = ' checked="checked"';
            }
            $subject = $this->bb->input['subject'];
        } elseif ($this->bb->input['action'] == 'editdraft' && $this->user->uid) {
            $message = htmlspecialchars_uni($this->curpost['message']);
            $subject = $this->curpost['subject'];
            if ($this->curpost['includesig'] != 0) {
                $postoptionschecked['signature'] = ' checked="checked"';
            }
            if ($this->curpost['smilieoff'] == 1) {
                $postoptionschecked['disablesmilies'] = ' checked="checked"';
            }
            if (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'none') {
                $postoptions_subscriptionmethod_none = 'checked="checked"';
            } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'email') {
                $postoptions_subscriptionmethod_email = 'checked="checked"';
            } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'pm') {
                $postoptions_subscriptionmethod_pm = 'checked="checked"';
            } else {
                $postoptions_subscriptionmethod_dont = 'checked="checked"';
            }
            $this->bb->input['icon'] = $this->curpost['icon'];
        } else {
            if ($this->user->signature != '') {
                $postoptionschecked['signature'] = ' checked="checked"';
            }
            if ($this->user->subscriptionmethod == 1) {
                $postoptions_subscriptionmethod_none = 'checked="checked"';
            } elseif ($this->user->subscriptionmethod == 2) {
                $postoptions_subscriptionmethod_email = 'checked="checked"';
            } elseif ($this->user->subscriptionmethod == 3) {
                $postoptions_subscriptionmethod_pm = 'checked="checked"';
            } else {
                $postoptions_subscriptionmethod_dont = 'checked="checked"';
            }
        }
        $this->view->offsetSet('postoptionschecked', $postoptionschecked);
        $this->view->offsetSet('postoptions', [
            'dont' => $postoptions_subscriptionmethod_dont,
            'none' => $postoptions_subscriptionmethod_none,
            'email' => $postoptions_subscriptionmethod_email,
            'pm' => $postoptions_subscriptionmethod_pm
        ]);


        $posticons = [];
        if ($this->bb->forums['allowpicons'] != 0) {
            $posticons = $this->post->get_post_icons();
        }
        $this->view->offsetSet('posticons', $posticons);

        // No subject?
        if (!isset($subject)) {
            if (!empty($this->bb->input['subject'])) {
                $subject = $this->bb->getInput('subject', '');
            } else {
                $subject = $this->thread_subject;
                // Subject too long? Shorten it to avoid error message
                if (my_strlen($subject) > 85) {
                    $subject = my_substr($subject, 0, 82) . '...';
                }
                $subject = 'RE: ' . $subject;
            }
        }

        // Preview a post that was written.
        $postbit = '';
        if (!empty($this->bb->input['previewpost'])) {
            // If this isn't a logged in user, then we need to do some special validation.
            if ($this->user->uid == 0) {
                // If they didn't specify a username then give them 'Guest'
                if (!$this->bb->getInput('username', '')) {
                    $username = $this->lang->guest;
                } // Otherwise use the name they specified.
                else {
                    $username = $this->bb->getInput('username', '');
                }
                $uid = 0;
            } // This user is logged in.
            else {
                $username = $this->user->username;
                $uid = $this->user->uid;
            }

            // Set up posthandler.
            $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'insert');
            $posthandler->action = 'post';

            // Set the post data that came from the input to the $post array.
            $post = [
                'tid' => $this->bb->getInput('tid', 0),
                'replyto' => $this->bb->getInput('replyto', 0),
                'fid' => $this->bb->threads['fid'],
                'subject' => $this->bb->getInput('subject', '', true),
                'icon' => $this->bb->getInput('icon', 0),
                'uid' => $uid,
                'username' => $username,
                'message' => $this->bb->getInput('message', '', true),
                'ipaddress' => $this->session->ipaddress,
                'posthash' => $this->bb->getInput('posthash', '')
            ];

            if (isset($this->bb->input['pid'])) {
                $post['pid'] = $this->bb->getInput('pid', 0);
            }

            $posthandler->set_data($post);

            // Now let the post handler do all the hard work.
            $valid_post = $posthandler->verifyMessage();
            $valid_subject = $posthandler->verifySubject();

            // guest post --> verify author
            if ($post['uid'] == 0) {
                $valid_username = $posthandler->verifyAuthor();
            } else {
                $valid_username = true;
            }

            $post_errors = [];
            // Fetch friendly error messages if this is an invalid post
            if (!$valid_post || !$valid_subject || !$valid_username) {
                $post_errors = $posthandler->get_friendly_errors();
            }

            // One or more errors returned, fetch error list and throw to newreply page
            if (count($post_errors) > 0) {
                $this->reply_errors = $this->bb->inline_error($post_errors);
            } else {
                $quote_ids = htmlspecialchars_uni($this->bb->getInput('quote_ids', ''));
                if (!isset($this->bb->input['username'])) {
                    $this->bb->input['username'] = $this->lang->guest;
                }
                $this->bb->input['icon'] = $this->bb->getInput('icon', 0);
                $query = $this->db->query('
				SELECT u.*, f.*
				FROM ' . TABLE_PREFIX . 'users u
				LEFT JOIN ' . TABLE_PREFIX . "userfields f ON (f.ufid=u.uid)
				WHERE u.uid='" . $this->user->uid . "'
			");
                $post = $this->db->fetch_array($query);
                if (!$this->user->uid || !$post['username']) {
                    $post['username'] = $this->bb->getInput('username', '');
                } else {
                    $post['userusername'] = $this->user->username;
                    $post['username'] = $this->user->username;
                }
                $post['message'] = $previewmessage;
                $post['subject'] = $subject;
                $post['icon'] = $this->bb->getInput('icon', 0);
                $post['ipaddress'] = $this->session->ipaddress;
                $this->bb->input['postoptions'] = $this->bb->getInput('postoptions', []);
                if (isset($this->bb->input['postoptions']['disablesmilies'])) {
                    $post['smilieoff'] = $this->bb->input['postoptions']['disablesmilies'];
                }
                $post['dateline'] = TIME_NOW;
                if (isset($this->bb->input['postoptions']['signature'])) {
                    $post['includesig'] = $this->bb->input['postoptions']['signature'];
                }
                if (!isset($post['includesig']) || $post['includesig'] != 1) {
                    $post['includesig'] = 0;
                }

                // Fetch attachments assigned to this post.
                if ($this->bb->getInput('pid', 0)) {
                    $attachwhere = "pid='" . $this->bb->getInput('pid', 0) . "'";
                } else {
                    $attachwhere = "posthash='" . $this->bb->getInput('posthash', '') . "'";
                }

                $query = $this->db->simple_select('attachments', '*', $attachwhere);
                while ($attachment = $this->db->fetch_array($query)) {
                    $this->bb->attachcache[0][$attachment['aid']] = $attachment;
                }
                $postbit = $this->post->build_postbit($post, 1);
            }
        }
        $this->view->offsetSet('postbit', $postbit);

        $subject = htmlspecialchars_uni($this->parser->parse_badwords($subject));
        $this->view->offsetSet('subject', $subject);
        $posthash = htmlspecialchars_uni($this->bb->getInput('posthash', ''));
        $this->view->offsetSet('posthash', $posthash);

        // Do we have attachment errors?
        if (count($this->errors) > 0) {
            $this->reply_errors = $this->bb->inline_error($this->errors);
        }

        // Get a listing of the current attachments.
        $this->bb->showAttachBox = false;
        if ($this->bb->settings['enableattachments'] != 0 && $this->forumpermissions['canpostattachments'] != 0) {
            $this->bb->showAttachBox = true;
            $attachcount = 0;
            if ($this->bb->pid) {
//                $attachwhere = "pid='{$this->bb->pid}'";
                $a = \RunBB\Models\Attachment::where('pid', '=', $this->bb->pid)->get()->toArray();
            } else {
//                $attachwhere = "'" . $this->db->escape_string($posthash) . "'";
                $a = \RunBB\Models\Attachment::where('posthash', '=', $posthash)->get()->toArray();
            }

            $attachments = [];
//            $query = $this->db->simple_select('attachments', '*', $attachwhere);
//            while ($attachment = $this->db->fetch_array($query)) {
            foreach ($a as $attachment) {
                $attachment['size'] = $this->parser->friendlySize($attachment['filesize']);
                $attachment['icon'] = $this->upload->get_attachment_icon(get_extension($attachment['filename']));
                $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);

                $postinsert = false;
                if ($this->bb->settings['bbcodeinserter'] != 0 &&
                    $this->bb->forums['allowmycode'] != 0 &&
                    (!$this->user->uid || $this->user->showcodebuttons != 0)
                ) {
                    $postinsert = true;
                }

                $attachments[] = [
                    'attachment' => $attachment,
                    'postinsert' => $postinsert,
                    'attach_mod_options' => false
                ];
                $attachcount++;
            }
            $this->view->offsetSet('attachments', $attachments);

            $noshowattach = '';
//            $query = $this->db->simple_select('attachments', 'SUM(filesize) AS ausage',
//  "uid='" . $this->user->uid . "'");
//            $usage = $this->db->fetch_array($query);
            $au = \RunBB\Models\Attachment::where('uid', '=', $this->user->uid)
                ->select(DB::raw('SUM(filesize) AS ausage'))
                ->get();
            $ausage = isset($au[0]) ? $au[0]->ausage : 0;

            if ($ausage > ($this->bb->usergroup['attachquota'] * 1024) && $this->bb->usergroup['attachquota'] != 0) {
                $noshowattach = 1;
            }

            if ($this->bb->usergroup['attachquota'] == 0) {
                $friendlyquota = $this->lang->unlimited;
            } else {
                $friendlyquota = $this->parser->friendlySize($this->bb->usergroup['attachquota'] * 1024);
            }

            $friendlyusage = $this->parser->friendlySize($ausage);
            $this->lang->attach_quota = $this->lang->sprintf($this->lang->attach_quota, $friendlyusage, $friendlyquota);

            $this->bb->attach_add = false;
            if ($this->bb->settings['maxattachments'] == 0 || ($this->bb->settings['maxattachments'] != 0 &&
                    $attachcount < $this->bb->settings['maxattachments']) && !$noshowattach
            ) {
                $this->bb->attach_add = true;
            }
            $this->bb->attach_update = false;
            if (($this->bb->usergroup['caneditattachments'] || $this->forumpermissions['caneditattachments']) &&
                $attachcount > 0) {
                $this->bb->attach_update = true;
            }
        }

        // Show captcha image for guests if enabled
        $captcha = '';
        if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
            $correct = false;
            $post_captcha = new \RunBB\Core\Captcha($this, false, 'post_captcha');

            if ((!empty($this->bb->input['previewpost']) || $this->hide_captcha == true) && $post_captcha->type == 1) {
                // If previewing a post - check their current captcha input - if correct, hide the captcha input area
                // ... but only if it's a default one, reCAPTCHA and Are You a Human must be filled in every
                //  time due to draconian limits
                if ($post_captcha->validate_captcha() == true) {
                    $correct = true;

                    // Generate a hidden list of items for our captcha
                    $captcha = $post_captcha->build_hidden_captcha();
                }
            }

            if (!$correct) {
                if ($post_captcha->type == 1) {
                    $post_captcha->build_captcha();
                } elseif ($post_captcha->type == 2 || $post_captcha->type == 4) {
                    $post_captcha->build_recaptcha();
                }

                if ($post_captcha->html) {
                    $captcha = $post_captcha->html;
                }
            } elseif ($correct && ($post_captcha->type == 2 || $post_captcha->type == 4)) {
                $post_captcha->build_recaptcha();

                if ($post_captcha->html) {
                    $captcha = $post_captcha->html;
                }
            }
        }
        $this->view->offsetSet('captcha', $captcha);

        $reviewmore = '';
        if ($this->bb->settings['threadreview'] != 0) {
            if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                $this->bb->settings['postsperpage'] = 20;
            }

            if ($this->user->is_moderator($this->bb->fid, 'canviewunapprove')) {
                $visibility = "(visible='1' OR visible='0')";
                $numposts = \RunBB\Models\Post::where('tid', '=', $this->tid)
                    ->where(function ($w) {
                        $w->where('visible', '=', '1');
                        $w->orWhere('visible', '=', '0');
                    })->count();

                $p = \RunBB\Models\Post::where('tid', '=', $this->tid)
                    ->where(function ($w) {
                        $w->where('visible', '=', '1');
                        $w->orWhere('visible', '=', '0');
                    })
                    ->take($this->bb->settings['postsperpage'])
                    ->orderBy('dateline', 'desc')
                    ->get(['pid']);
            } else {
                $visibility = "visible='1'";
                $numposts = \RunBB\Models\Post::where('tid', '=', $this->tid)
                    ->where('visible', '=', '1')
                    ->count();

                $p = \RunBB\Models\Post::where('tid', '=', $this->tid)
                    ->where('visible', '=', '1')
                    ->take($this->bb->settings['postsperpage'])
                    ->orderBy('dateline', 'desc')
                    ->get(['pid']);
            }
//            $query = $this->db->simple_select('posts', 'COUNT(pid) AS post_count', "tid='{$this->tid}'
//  AND {$visibility}");
//            $numposts = $this->db->fetch_field($query, 'post_count');

            if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                $this->bb->settings['postsperpage'] = 20;
            }

            $this->bb->showReviewMore = false;
            if ($numposts > $this->bb->settings['postsperpage']) {
                $this->bb->showReviewMore = true;
                $numposts = $this->bb->settings['postsperpage'];
                $this->lang->thread_review_more = $this->lang->sprintf(
                    $this->lang->thread_review_more,
                    $this->bb->settings['postsperpage'],
                    get_thread_link($this->tid)
                );
            }

//            $query = $this->db->simple_select(
//                'posts',
//                'pid',
//                "tid='{$this->tid}' AND {$visibility}",
//                ['order_by' => 'dateline', 'order_dir' => 'desc', 'limit' => $this->bb->settings['postsperpage']]
//            );

//            while ($post = $this->db->fetch_array($query)) {
            foreach ($p as $post) {
                $pidin[] = $post->pid;//$post['pid'];
            }
//            $pidin = implode(',', $pidin);

            // Fetch attachments
//            $query = $this->db->simple_select('attachments', '*', "pid IN ($pidin)");
            $at = \RunBB\Models\Attachment::whereIn('pid', $pidin)
                ->get()
                ->toArray();
//            while ($attachment = $this->db->fetch_array($query)) {
            foreach ($at as $attachment) {
                $this->bb->attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
            }
//            $query = $this->db->query('
//			SELECT p.*, u.username AS userusername
//			FROM ' . TABLE_PREFIX . 'posts p
//			LEFT JOIN ' . TABLE_PREFIX . "users u ON (p.uid=u.uid)
//			WHERE pid IN ($pidin)
//			ORDER BY dateline DESC
//		");
            $posts = \RunBB\Models\Post::whereIn('pid', $pidin)
                ->leftJoin('users', 'posts.uid', '=', 'users.uid')
                ->orderBy('dateline', 'desc')
                ->get(['posts.*', 'users.username AS userusername'])
                ->toArray();
            $postsdone = 0;
            $altbg = 'trow1';
            $reviewbits = [];
//            while ($post = $this->db->fetch_array($query)) {
            foreach ($posts as $post) {
                if ($post['userusername']) {
                    $post['username'] = $post['userusername'];
                }
                $reviewpostdate = $this->time->formatDate('relative', $post['dateline']);
                $parser_options = [
                    'allow_html' => $this->bb->forums['allowhtml'],
                    'allow_mycode' => $this->bb->forums['allowmycode'],
                    'allow_smilies' => $this->bb->forums['allowsmilies'],
                    'allow_imgcode' => $this->bb->forums['allowimgcode'],
                    'allow_videocode' => $this->bb->forums['allowvideocode'],
                    'me_username' => $post['username'],
                    'filter_badwords' => 1
                ];
                if ($post['smilieoff'] == 1) {
                    $parser_options['allow_smilies'] = 0;
                }

                if ($this->user->showimages != 1 && $this->user->uid != 0 ||
                    $this->bb->settings['guestimages'] != 1 && $this->user->uid == 0
                ) {
                    $parser_options['allow_imgcode'] = 0;
                }

                if ($this->user->showvideos != 1 && $this->user->uid != 0 ||
                    $this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0
                ) {
                    $parser_options['allow_videocode'] = 0;
                }

                if ($post['visible'] != 1) {
                    $altbg = 'trow_shaded';
                }

                $post['message'] = $this->parser->parse_message($post['message'], $parser_options);
                $this->post->get_post_attachments($post['pid'], $post);
                //$reviewmessage = $post['message'];
                $reviewbits[] = [
                    'username' => $post['username'],
                    'reviewpostdate' => $reviewpostdate,
                    'altbg' => $altbg,
                    'message' => $post['message']
                ];
                $altbg = ($altbg == 'trow1') ? 'trow2' : 'trow1';
            }
            $this->view->offsetSet('reviewbits', $reviewbits);
        }

        // Can we disable smilies or are they disabled already?
        $this->bb->showDisableSmilies = false;
        if ($this->bb->forums['allowsmilies'] != 0) {
            $this->bb->showDisableSmilies = true;
        }

        $this->bb->showModOptions = false;
        // Show the moderator options.
        if ($this->user->is_moderator($this->bb->fid)) {
            if ($this->bb->getInput('processed', 0)) {
                $this->bb->input['modoptions'] = $this->bb->getInput('modoptions', []);
                if (!isset($this->bb->input['modoptions']['closethread'])) {
                    $this->bb->input['modoptions']['closethread'] = 0;
                }
                $closed = (int)$this->bb->input['modoptions']['closethread'];
                if (!isset($this->bb->input['modoptions']['stickthread'])) {
                    $this->bb->input['modoptions']['stickthread'] = 0;
                }
                $stuck = (int)$this->bb->input['modoptions']['stickthread'];
            } else {
                $closed = $this->bb->threads['closed'];
                $stuck = $this->bb->threads['sticky'];
            }

            if ($closed) {
                $closecheck = ' checked="checked"';
            } else {
                $closecheck = '';
            }

            if ($stuck) {
                $stickycheck = ' checked="checked"';
            } else {
                $stickycheck = '';
            }
            $this->bb->showModOptions = true;
            $this->view->offsetSet('closecheck', $closecheck);
            $this->view->offsetSet('stickycheck', $stickycheck);
            $bgcolor = 'trow1';
        } else {
            $bgcolor = 'trow2';
        }
        $this->view->offsetSet('bgcolor', $bgcolor);

        $this->lang->post_reply_to = $this->lang->sprintf($this->lang->post_reply_to, $this->bb->threads['subject']);
        $this->lang->reply_to = $this->lang->sprintf($this->lang->reply_to, $this->bb->threads['subject']);

        // Do we have any forum rules to show for this forum?
        //$forumrules = '';
        $this->bb->rulesType = 0;
        if ($this->bb->forums['rulestype'] >= 2 && $this->bb->forums['rules']) {
            if (!$this->bb->forums['rulestitle']) {
                $this->bb->forums['rulestitle'] = $this->lang->sprintf(
                    $this->lang->forum_rules,
                    $this->bb->forums['name']
                );
            }

            $rules_parser = [
                'allow_html' => 1,
                'allow_mycode' => 1,
                'allow_smilies' => 1,
                'allow_imgcode' => 1
            ];

            $this->bb->forums['rules'] = $this->parser->parse_message($this->bb->forums['rules'], $rules_parser);
            //$foruminfo = $this->bb->forums;
            $this->view->offsetSet('foruminfo', $this->bb->forums);

            if ($this->bb->forums['rulestype'] == 3) {
                $this->bb->rulesType = 3;
            } elseif ($this->bb->forums['rulestype'] == 2) {
                $this->bb->rulesType = 2;
            }
        }

        $moderation_text = '';
        if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveattachs')) {
            if ($this->forumpermissions['modattachments'] == 1 && $this->forumpermissions['canpostattachments'] != 0) {
                $moderation_text = $this->lang->moderation_forum_attachments;
            }
        }
        if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveposts')) {
            if ($this->forumpermissions['modposts'] == 1) {
                $moderation_text = $this->lang->moderation_forum_posts;
            }

            if ($this->user->moderateposts == 1) {
                $moderation_text = $this->lang->moderation_user_posts;
            }
        }
        $this->view->offsetSet('moderation_text', $moderation_text);
        $this->view->offsetSet('reply_errors', $this->reply_errors);
        $this->view->offsetSet('tid', $this->tid);
        $this->view->offsetSet('fid', $this->bb->fid);
        $this->view->offsetSet('replyto', $this->replyto);

        $this->plugins->runHooks('newreply_end');

        $this->bb->forums['name'] = strip_tags($this->bb->forums['name']);

        $this->bb->output_page();
        $this->view->render($response, '@forum/newreply.html.twig');
    }


    public function doNewreply(Request $request, Response $response)
    {
        $retval = $this->init();
        if ($retval == 'exit') {
            return;
        } elseif ($retval == 'index') {
            return $this->index($request, $response, true);
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('newreply_do_newreply_start');

        // If this isn't a logged in user, then we need to do some special validation.
        if ($this->user->uid == 0) {
            // If they didn't specify a username then give them 'Guest'
            if (!$this->bb->getInput('username', '')) {
                $username = $this->lang->guest;
            } // Otherwise use the name they specified.
            else {
                $username = $this->bb->getInput('username', '');
            }
            $uid = 0;


            if ($this->bb->settings['stopforumspam_on_newreply']) {
                $stop_forum_spam_checker = new \RunBB\Core\StopForumSpamChecker(
                    $this->plugins,
                    $this->bb->settings['stopforumspam_min_weighting_before_spam'],
                    $this->bb->settings['stopforumspam_check_usernames'],
                    $this->bb->settings['stopforumspam_check_emails'],
                    $this->bb->settings['stopforumspam_check_ips'],
                    $this->bb->settings['stopforumspam_log_blocks']
                );

                try {
                    if ($stop_forum_spam_checker->is_user_a_spammer(
                        $this->bb->getInput('username', ''),
                        '',
                        $this->session->ipaddress
                    )) {
                        $this->bb->error($this->lang->sprintf(
                            $this->lang->error_stop_forum_spam_spammer,
                            $stop_forum_spam_checker->getErrorText([
                                'stopforumspam_check_usernames',
                                'stopforumspam_check_ips'
                            ])
                        ));
                    }
                } catch (\Exception $e) {
                    if ($this->bb->settings['stopforumspam_block_on_error']) {
                        $this->bb->error($this->lang->error_stop_forum_spam_fetching);
                    }
                }
            }
        } // This user is logged in.
        else {
            $username = $this->user->username;
            $uid = $this->user->uid;
        }

        // Attempt to see if this post is a duplicate or not
//        if ($uid > 0) {
//            $user_check = "p.uid='{$uid}'";
//        } else {
//            $user_check = 'p.ipaddress=' . $this->session->ipaddress;
//        }
        if (!$this->bb->getInput('savedraft', '')) {
//            $query = $this->db->simple_select('posts p', 'p.pid, p.visible',
//                "{$user_check}
//                AND p.tid='{$this->bb->threads['tid']}'
//                AND p.subject='" . $this->bb->getInput('subject') . "'
//                AND p.message='" . $this->bb->getInput('message') . "'
//                AND p.visible != '-2'
//                AND p.dateline>" . (TIME_NOW - 600));
//            $duplicate_check = $this->db->fetch_field($query, 'pid');
            $dc = \RunBB\Models\Post::where(function ($q) use ($uid) {
                if ($uid > 0) {
                    $q->where('uid', '=', $uid);
                } else {
                    $q->where('ipaddress', '=', $this->session->ipaddress);
                }
            })
                ->where('tid', '=', $this->bb->threads['tid'])
                ->where('subject', '=', $this->bb->getInput('subject', '', true))
                ->where('message', '=', $this->bb->getInput('message', '', true))
                ->where('visible', '!=', '-2')
                ->where('dateline', '>', (TIME_NOW - 600))
                ->count();

//            if ($duplicate_check) {
            if ($dc > 0) {
                $this->bb->error($this->lang->error_post_already_submitted);
            }
        }

        // Set up posthandler.
        $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'insert');

        // Set the post data that came from the input to the $post array.
        $post = [
            'tid' => $this->bb->getInput('tid', 0),
            'replyto' => $this->bb->getInput('replyto', 0),
            'fid' => $this->bb->threads['fid'],
            'subject' => $this->bb->getInput('subject', '', true),
            'icon' => $this->bb->getInput('icon', 0),
            'uid' => $uid,
            'username' => $username,
            'message' => $this->bb->getInput('message', '', true),
            'ipaddress' => $this->session->ipaddress,
            'posthash' => $this->bb->getInput('posthash', '')
        ];

        if (isset($this->bb->input['pid'])) {
            $post['pid'] = $this->bb->getInput('pid', 0);
        }

        // Are we saving a draft post?
        if ($this->bb->getInput('savedraft', '') && $this->user->uid) {
            $post['savedraft'] = 1;
        } else {
            $post['savedraft'] = 0;
        }

        $postoptions = $this->bb->getInput('postoptions', []);
        if (!isset($postoptions['signature'])) {
            $postoptions['signature'] = 0;
        }
        if (!isset($postoptions['subscriptionmethod'])) {
            $postoptions['subscriptionmethod'] = 0;
        }
        if (!isset($postoptions['disablesmilies'])) {
            $postoptions['disablesmilies'] = 0;
        }

        // Set up the post options from the input.
        $post['options'] = [
            'signature' => $postoptions['signature'],
            'subscriptionmethod' => $postoptions['subscriptionmethod'],
            'disablesmilies' => $postoptions['disablesmilies']
        ];

        // Apply moderation options if we have them
        $post['modoptions'] = $this->bb->getInput('modoptions', []);

        $posthandler->set_data($post);

        // Now let the post handler do all the hard work.
        $valid_post = $posthandler->validatePost();

        $post_errors = [];
        // Fetch friendly error messages if this is an invalid post
        if (!$valid_post) {
            $post_errors = $posthandler->get_friendly_errors();
        }

        // Mark thread as read
        $this->indicator->mark_thread_read($this->tid, $this->bb->fid);

        // Check captcha image
        $post_captcha = new \RunBB\Core\Captcha($this, false, 'post_captcha');
        if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
            if ($post_captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($post_captcha->get_errors() as $error) {
                    $post_errors[] = $error;
                }
            } else {
                $this->hide_captcha = true;
            }

            if ($this->bb->getInput('ajax', 0)) {
                if ($post_captcha->type == 1) {
                    $randomstr = random_str(5);
                    $imagehash = md5(random_str(12));

                    $imagearray = [
                        'imagehash' => $imagehash,
                        'imagestring' => $randomstr,
                        'dateline' => TIME_NOW
                    ];

                    $this->db->insert_query('captcha', $imagearray);

                    //header("Content-type: text/html; charset={$this->lang->settings['charset']}");
                    $data = '';
                    $data .= "<captcha>$imagehash";

                    if ($this->hide_captcha) {
                        $data .= "|$randomstr";
                    }

                    $data .= '</captcha>';

                    //header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                    $json_data = ['data' => $data];
                } elseif ($post_captcha->type == 2) {
                    //header("Content-type: text/html; charset={$this->lang->settings['charset']}");
                    $data = '<captcha>reload</captcha>';

                    //header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                    $json_data = ['data' => $data];
                }
            }
        }

        // One or more errors returned, fetch error list and throw to newreply page
        if (count($post_errors) > 0) {
            $this->reply_errors = $this->bb->inline_error($post_errors, '', $json_data);
            $this->bb->input['action'] = 'newreply';
            return $this->index($request, $response, true);
        } else {
            $postinfo = $posthandler->insertPost();
            $this->bb->pid = $postinfo['pid'];
            $visible = $postinfo['visible'];
            $closed = $postinfo['closed'];

            // Invalidate solved captcha
            if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
                $post_captcha->invalidate_captcha();
            }

            $force_redirect = false;

            // Deciding the fate
            if ($visible == -2) {
                // Draft post
                $this->lang->redirect_newreply = $this->lang->draft_saved;
                $url = $this->bb->settings['bburl'] . '/usercp/drafts';
            } elseif ($visible == 1) {
                // Visible post
                $this->lang->redirect_newreply .= $this->lang->redirect_newreply_post;
                $url = get_post_link($this->bb->pid, $this->tid) . "#pid{$this->bb->pid}";
            } else {
                // Moderated post
                $this->lang->redirect_newreply .= '<br />' . $this->lang->redirect_newreply_moderation;
                $url = get_thread_link($this->tid);

                // User must see moderation notice, regardless of redirect settings
                $force_redirect = true;
            }

            // Mark any quoted posts so they're no longer selected - attempts to maintain those which weren't selected
            if (isset($this->bb->input['quoted_ids']) &&
                isset($this->bb->cookies['multiquote']) &&
                $this->bb->settings['multiquote'] != 0
            ) {
                // We quoted all posts - remove the entire cookie
                if ($this->bb->getInput('quoted_ids', '') === 'all') {
                    $this->bb->my_unsetcookie('multiquote');
                } // Only quoted a few - attempt to remove them from the cookie
                else {
                    $this->quoted_ids = explode('|', $this->bb->getInput('quoted_ids', ''));
                    $multiquote = explode('|', $this->bb->cookies['multiquote']);
                    if (is_array($multiquote) && is_array($this->quoted_ids)) {
                        foreach ($multiquote as $key => $quoteid) {
                            // If this ID was quoted, remove it from the multiquote list
                            if (in_array($quoteid, $this->quoted_ids)) {
                                unset($multiquote[$key]);
                            }
                        }
                        // Still have an array - set the new cookie
                        if (is_array($multiquote)) {
                            $new_multiquote = implode(',', $multiquote);
                            $this->bb->my_setcookie('multiquote', $new_multiquote);
                        } // Otherwise, unset it
                        else {
                            $this->bb->my_unsetcookie('multiquote');
                        }
                    }
                }
            }

            $this->plugins->runHooks('newreply_do_newreply_end');

            // This was a post made via the ajax quick reply - we need to do some special things here
            if ($this->bb->getInput('ajax', 0)) {
                // Visible post
                if ($visible == 1) {
                    // Set post counter
                    $this->postcounter = $this->bb->threads['replies'] + 1;

                    if ($this->user->is_moderator($this->bb->fid, 'canviewunapprove')) {
                        $this->postcounter += $this->bb->threads['unapprovedposts'];
                    }
                    if ($this->user->is_moderator($this->bb->fid, 'canviewdeleted')) {
                        $this->postcounter += $this->bb->threads['deletedposts'];
                    }

                    // Was there a new post since we hit the quick reply button?
                    if ($this->bb->getInput('lastpid', 0)) {
                        $query = $this->db->simple_select(
                            'posts',
                            'pid',
                            "tid = '{$this->tid}' AND pid != '{$this->bb->pid}'",
                            ['order_by' => 'pid', 'order_dir' => 'desc']
                        );
                        $new_post = $this->db->fetch_array($query);
                        if ($new_post['pid'] != $this->bb->getInput('lastpid', 0)) {
                            return $this->bb->redirect(get_thread_link($this->tid, 0, 'lastpost'));
                        }
                    }

                    if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                        $this->bb->settings['postsperpage'] = 20;
                    }
                    $perpage = $this->bb->settings['postsperpage'];

                    // Lets see if this post is on the same page as the one we're viewing or not
                    // if it isn't, redirect us
                    if ($perpage > 0 && (($this->postcounter) % $perpage) == 0) {
                        $post_page = ($this->postcounter) / $this->bb->settings['postsperpage'];
                    } else {
                        $post_page = (int)($this->postcounter / $this->bb->settings['postsperpage']) + 1;
                    }

                    if ($post_page > $this->bb->getInput('from_page', 0)) {
                        return $this->bb->redirect(get_thread_link($this->tid, 0, 'lastpost'));
                        //exit;
                    }

                    // Return the post HTML and display it inline
                    $query = $this->db->query('
					SELECT u.*, u.username AS userusername, p.*, f.*, eu.username AS editusername
					FROM ' . TABLE_PREFIX . 'posts p
					LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=p.uid)
					LEFT JOIN ' . TABLE_PREFIX . 'userfields f ON (f.ufid=u.uid)
					LEFT JOIN ' . TABLE_PREFIX . "users eu ON (eu.uid=p.edituid)
					WHERE p.pid='{$this->bb->pid}'
				");
                    $post = $this->db->fetch_array($query);

                    // Now lets fetch all of the attachments for this post
                    $query = $this->db->simple_select('attachments', '*', "pid='{$this->bb->pid}'");
                    while ($attachment = $this->db->fetch_array($query)) {
                        $this->bb->attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
                    }

                    // Establish altbg - may seem like this is backwards, but build_postbit reverses it
                    if (($this->postcounter - $this->bb->settings['postsperpage']) % 2 != 0) {
                        $altbg = 'trow1';
                    } else {
                        $altbg = 'trow2';
                    }

                    $this->bb->pid = $post['pid'];
                    $post = $this->post->build_postbit($post);

                    $data = '';
                    $data .= $post;

                    // Build a new posthash incase the user wishes to quick reply again
                    $new_posthash = md5($this->user->uid . random_str());
                    $data .= "<script type=\"text/javascript\">\n";
                    $data .= "var hash = document.getElementById('posthash');\n";
                    $data .= "if(hash) { hash.value = '{$new_posthash}'; }\n";
                    $data .= "if(typeof(inlineModeration) != 'undefined') {
					$('#inlinemod_{$this->bb->pid}').bind(\"click\", function(e) {
						inlineModeration.checkItem();
					});
				}\n";

                    if ($closed == 1) {
                        $data .= "$('#quick_reply_form .trow1').removeClass('trow1 trow2').addClass('trow_shaded');\n";
                    } else {
                        $data .= "$('#quick_reply_form .trow_shaded').removeClass('trow_shaded').addClass('trow1');\n";
                    }

                    $data .= "</script>\n";

                    header("Content-type: application/json; charset={$this->lang->settings['charset']}");
                    echo json_encode(['data' => $data]);

                    exit;
                } // Post is in the moderation queue
                else {
                    $this->bb->redirect(
                        get_thread_link($this->tid, 0, 'lastpost'),
                        $this->lang->redirect_newreply_moderation,
                        '',
                        true
                    );
                    //exit;
                }
            } else {
                $this->lang->redirect_newreply .= $this->lang->sprintf(
                    $this->lang->redirect_return_forum,
                    $this->forum->get_forum_link($this->bb->fid)
                );
                $this->bb->redirect($url, $this->lang->redirect_newreply, '', $force_redirect);
                //exit;
            }
        }
    }
}
