<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Newthread;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{
    protected $errors;

    protected function init()
    {
        // Load global language phrases
        $this->lang->load('newthread');

        $tid = $this->bb->pid = 0;
        $this->bb->input['action'] = $this->bb->getInput('action', '');
        $this->bb->input['tid'] = $this->bb->getInput('tid', 0);
        $this->bb->input['pid'] = $this->bb->getInput('pid', 0);

        if (//$this->bb->input['action'] == 'editdraft'
            stristr($this->request->getUri()->getPath(), 'editdraft') ||
            ($this->bb->getInput('savedraft', '') && $this->bb->input['tid']) ||
            ($this->bb->input['tid'] && $this->bb->input['pid'])
        ) {
            $this->bb->threads = $this->thread->get_thread($this->bb->input['tid']);

            $query = $this->db->simple_select(
                'posts',
                '*',
                "tid='" . $this->bb->getInput('tid', 0) . "' AND visible='-2'",
                ['order_by' => 'dateline', 'limit' => 1]
            );
            $this->curpost = $this->db->fetch_array($query);

            if (!$this->bb->threads['tid'] ||
                !$this->curpost['pid'] ||
                $this->bb->threads['visible'] != -2 ||
                $this->bb->threads['uid'] != $this->user->uid
            ) {
                $this->bb->error($this->lang->invalidthread);
                return 'exit';
            }

            $this->bb->pid = $this->curpost['pid'];
            $this->fid = $this->bb->threads['fid'];
            $tid = $this->bb->threads['tid'];
            $editdraftpid = "<input type=\"hidden\" name=\"pid\" value=\"{$this->bb->pid}\" />";
        } else {
            $this->fid = $this->bb->getInput('fid', 0);
            $editdraftpid = '';
        }
        $this->view->offsetSet('editdraftpid', $editdraftpid);
        $this->view->offsetSet('fid', $this->fid);
        $this->view->offsetSet('tid', $tid);

        // Fetch forum information.
        $this->bb->forums = $this->forum->get_forum($this->fid);
        if (!$this->bb->forums) {
            $this->bb->error($this->lang->error_invalidforum);
            return 'exit';
        }

        // Draw the navigation
        $this->bb->build_forum_breadcrumb($this->fid);
        $this->bb->add_breadcrumb($this->lang->nav_newthread);

        $this->forumpermissions = $this->forum->forum_permissions($this->fid);

        if ($this->bb->forums['open'] == 0 || $this->bb->forums['type'] != 'f' || $this->bb->forums['linkto'] != '') {
            $this->bb->error($this->lang->error_closedinvalidforum);
            return 'exit';
        }

        if ($this->forumpermissions['canview'] == 0 || $this->forumpermissions['canpostthreads'] == 0) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        if ($this->user->suspendposting == 1) {
            $suspendedpostingtype = $this->lang->error_suspendedposting_permanent;
            if ($this->user->suspensiontime) {
                $suspendedpostingtype = $this->lang->sprintf(
                    $this->lang->error_suspendedposting_temporal,
                    $this->time->formatDate($this->bb->settings['dateformat'], $this->user->suspensiontime)
                );
            }

            $this->lang->error_suspendedposting = $this->lang->sprintf(
                $this->lang->error_suspendedposting,
                $suspendedpostingtype,
                $this->time->formatDate($this->bb->settings['timeformat'], $this->user->suspensiontime)
            );

            $this->bb->error($this->lang->error_suspendedposting);
            return 'exit';
        }
        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($this->bb->forums['fid']);

        // If MyCode is on for this forum and the MyCode editor is enabled in the Admin CP,
        // draw the code buttons and smilie inserter.
        if ($this->bb->settings['bbcodeinserter'] != 0 &&
            $this->bb->forums['allowmycode'] != 0 &&
            (!$this->user->uid ||
                $this->user->showcodebuttons != 0)
        ) {
            $this->bb->codebuttons = $this->editor->build_mycode_inserter('message', $this->bb->forums['allowsmilies']);
            if ($this->bb->forums['allowsmilies'] != 0) {
                $this->bb->smilieinserter = $this->editor->build_clickable_smilies();
            }
        }

        // Does this forum allow post icons? If so, fetch the post icons.
        $posticons = [];
        if ($this->bb->forums['allowpicons'] != 0) {
            $posticons = $this->post->get_post_icons();
        }
        $this->view->offsetSet('posticons', $posticons);

        // Previewing a post, overwrite the action to the new thread action.
        if (!empty($this->bb->input['previewpost'])) {
            $this->bb->input['action'] = 'newthread';
        }

        // Setup a unique posthash for attachment management
        if (!$this->bb->getInput('posthash', '') && !$this->bb->pid) {
            $this->bb->input['posthash'] = md5($this->user->uid . random_str());
        }

        if ((empty($_POST) && empty($_FILES)) && $this->bb->getInput('processed', 0) == 1) {
            $this->bb->error($this->lang->error_cannot_upload_php_post);
            return 'exit';
        }

        $this->errors = [];

        // Handle attachments if we've got any.
        if ($this->bb->settings['enableattachments'] == 1 &&
            !$this->bb->getInput('attachmentaid', 0) &&
            ($this->bb->getInput('newattachment', '') ||
                $this->bb->getInput('updateattachment', '') ||
                ($this->bb->input['action'] === 'do_newthread' &&
                    $this->bb->getInput('submit', '') && $_FILES['attachment']))
        ) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            //if($this->bb->input['action'] == 'editdraft' || ($this->bb->input['tid'] && $this->bb->input['pid']))
            if ($this->bb->input['tid'] && $this->bb->input['pid']) {
                $attachwhere = "pid='{$this->bb->pid}'";
            } else {
                $attachwhere = "posthash='" . $this->db->escape_string($this->bb->getInput('posthash', '')) . "'";
            }

            // If there's an attachment, check it and upload it
            if ($this->forumpermissions['canpostattachments'] != 0) {
                if (!empty($_FILES['attachment']['name']) && !empty($_FILES['attachment']['type'])) {
                    if ($_FILES['attachment']['size'] > 0) {
                        $query = $this->db->simple_select(
                            'attachments',
                            'aid',
                            "filename='" . $this->db->escape_string($_FILES['attachment']['name']) . "' AND {$attachwhere}"
                        );
                        $updateattach = $this->db->fetch_field($query, 'aid');

                        $update_attachment = false;
                        if ($updateattach > 0 && $this->bb->getInput('updateattachment', '')) {
                            $update_attachment = true;
                        }
                        $attachedfile = $this->upload->upload_attachment($_FILES['attachment'], $update_attachment);
                    } else {
                        $this->errors[] = $this->lang->error_uploadempty;
                        $this->bb->input['action'] = 'newthread';
                    }
                }
            }

            // Error with attachments - should use new inline errors?
            if (!empty($attachedfile['error'])) {
                $this->errors[] = $attachedfile['error'];
                $this->bb->input['action'] = 'newthread';
            }

            // If we were dealing with an attachment but didn't click 'Post Thread', force the new thread page again.
            if (!$this->bb->getInput('submit', '')) {
                //$editdraftpid = '<input type=\'hidden\' name=\'pid\' value=\'$this->bb->pid\' />';
                $this->bb->input['action'] = 'newthread';
            }
        }

        // Are we removing an attachment from the thread?
        if ($this->bb->settings['enableattachments'] == 1 &&
            $this->bb->getInput('attachmentaid', 0) &&
            $this->bb->getInput('attachmentact', '') === 'remove'
        ) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->upload->remove_attachment($this->bb->pid, $this->bb->getInput('posthash', ''), $this->bb->getInput('attachmentaid', 0));
            if (!$this->bb->getInput('submit', '')) {
                $this->bb->input['action'] = 'newthread';
            }
        }

        $this->thread_errors = '';
        $this->hide_captcha = false;

        // Check the maximum posts per day for this user
        if ($this->bb->usergroup['maxposts'] > 0 && $this->bb->usergroup['cancp'] != 1) {
            $daycut = TIME_NOW - 60 * 60 * 24;
            $query = $this->db->simple_select(
                'posts',
                'COUNT(*) AS posts_today',
                "uid='{$this->user->uid}' AND visible='1' AND dateline>{$daycut}"
            );
            $post_count = $this->db->fetch_field($query, 'posts_today');
            if ($post_count >= $this->bb->usergroup['maxposts']) {
                $this->lang->error_maxposts = $this->lang->sprintf($this->lang->error_maxposts, $this->bb->usergroup['maxposts']);
                $this->bb->error($this->lang->error_maxposts);
                return 'exit';
            }
        }
    }
}
