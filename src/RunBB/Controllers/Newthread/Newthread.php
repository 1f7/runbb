<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Newthread;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Newthread extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        $this->plugins->runHooks('newthread_start');

        // Do we have attachment errors?
        if (count($this->errors) > 0) {
            $this->thread_errors = $this->bb->inline_error($this->errors);
        }

        $multiquote_external = $quoted_ids = '';

        $subject = $message = '';
        $multiquote = [];
        // If this isn't a preview and we're not editing a draft, then handle quoted posts
        //if(empty($this->bb->input['previewpost']) && !$this->thread_errors && $this->bb->input['action'] != 'editdraft')
        if (empty($this->bb->input['previewpost']) && !$this->thread_errors) {
            $quoted_posts = [];
            // Handle multiquote
            if (isset($this->bb->cookies['multiquote']) && $this->bb->settings['multiquote'] != 0) {
                $multiquoted = explode('|', $this->bb->cookies['multiquote']);
                foreach ($multiquoted as $post) {
                    $quoted_posts[$post] = (int)$post;
                }
            }

            // Quoting more than one post - fetch them
            if (count($quoted_posts) > 0) {
                $external_quotes = 0;
                $quoted_posts = implode(',', $quoted_posts);
                $unviewable_forums = $this->forum->get_unviewable_forums();
                $inactiveforums = $this->forum->get_inactive_forums();
                if ($unviewable_forums) {
                    $unviewable_forums = ' AND t.fid NOT IN (' . implode(',', $unviewable_forums) . ')';
                }
                if ($inactiveforums) {
                    $inactiveforums = ' AND t.fid NOT IN (' . implode(',', $inactiveforums) . ')';
                }

                if ($this->user->is_moderator($this->fid)) {
                    $visible_where = 'AND p.visible != 2';
                } else {
                    $visible_where = 'AND p.visible > 0';
                }

                if ($this->bb->getInput('load_all_quotes', 0) == 1) {
                    $query = $this->db->query('
					SELECT p.subject, p.message, p.pid, p.tid, p.username, p.dateline, u.username AS userusername
					FROM ' . TABLE_PREFIX . 'posts p
					LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
					LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
					WHERE p.pid IN ({$quoted_posts}) {$unviewable_forums} {$inactiveforums} {$visible_where}
					ORDER BY p.dateline
				");
                    while ($quoted_post = $this->db->fetch_array($query)) {
                        if ($quoted_post['userusername']) {
                            $quoted_post['username'] = $quoted_post['userusername'];
                        }
                        $quoted_post['message'] = preg_replace('#(^|\r|\n)/me ([^\r\n<]*)#i', "\\1* {$quoted_post['username']} \\2", $quoted_post['message']);
                        $quoted_post['message'] = preg_replace('#(^|\r|\n)/slap ([^\r\n<]*)#i', "\\1* {$quoted_post['username']} {$this->lang->slaps} \\2 {$this->lang->with_trout}", $quoted_post['message']);
                        $quoted_post['message'] = preg_replace("#\[attachment=([0-9]+?)\]#i", '', $quoted_post['message']);
                        $message .= "[quote='{$quoted_post['username']}' pid='{$quoted_post['pid']}' dateline='{$quoted_post['dateline']}']\n{$quoted_post['message']}\n[/quote]\n\n";
                    }

                    $quoted_ids = 'all';
                } else {
                    $query = $this->db->query('
					SELECT COUNT(*) AS quotes
					FROM ' . TABLE_PREFIX . 'posts p
					LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
					WHERE p.pid IN ({$quoted_posts}) {$unviewable_forums} {$inactiveforums} {$visible_where}
				");
                    $external_quotes = $this->db->fetch_field($query, 'quotes');

                    if ($external_quotes > 0) {
                        if ($external_quotes == 1) {
                            $multiquote_text = $this->lang->multiquote_external_one;
                            $multiquote_deselect = $this->lang->multiquote_external_one_deselect;
                            $multiquote_quote = $this->lang->multiquote_external_one_quote;
                        } else {
                            $multiquote_text = $this->lang->sprintf($this->lang->multiquote_external, $external_quotes);
                            $multiquote_deselect = $this->lang->multiquote_external_deselect;
                            $multiquote_quote = $this->lang->multiquote_external_quote;
                        }
                        $multiquote[] = [
                            'text' => $multiquote_text,
                            'deselect' => $multiquote_deselect,
                            'quote' => $multiquote_quote
                        ];
                    }
                }
            }
        }
        $this->view->offsetSet('multiquote', $multiquote);

        if (isset($this->bb->input['quoted_ids'])) {
            $quoted_ids = htmlspecialchars_uni($this->bb->getInput('quoted_ids', ''));
        }
        $this->view->offsetSet('quoted_ids', $quoted_ids);

        $postoptionschecked = ['signature' => '', 'disablesmilies' => ''];
        $postoptions_subscriptionmethod_dont = $postoptions_subscriptionmethod_none =
        $postoptions_subscriptionmethod_email = $postoptions_subscriptionmethod_pm = '';
        $postpollchecked = '';

        // Check the various post options if we're
        // a -> previewing a post
        // b -> removing an attachment
        // c -> adding a new attachment
        // d -> have errors from posting

        if (!empty($this->bb->input['previewpost']) ||
            $this->bb->getInput('attachmentaid', 0) ||
            $this->bb->getInput('newattachment', '') ||
            $this->bb->getInput('updateattachment', '') || $this->thread_errors
        ) {
            $postoptions = $this->bb->getInput('postoptions', []);
            if (isset($postoptions['signature']) && $postoptions['signature'] == 1) {
                $postoptionschecked['signature'] = ' checked="checked"';
            }
            if (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'none') {
                $postoptions_subscriptionmethod_none = 'checked="checked"';
            } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'email') {
                $postoptions_subscriptionmethod_email = 'checked="checked"';
            } elseif (isset($postoptions['subscriptionmethod']) && $postoptions['subscriptionmethod'] == 'pm') {
                $postoptions_subscriptionmethod_pm = 'checked="checked"';
            } else {
                $postoptions_subscriptionmethod_dont = 'checked="checked"';
            }
            if (isset($postoptions['disablesmilies']) && $postoptions['disablesmilies'] == 1) {
                $postoptionschecked['disablesmilies'] = ' checked="checked"';
            }
            if ($this->bb->getInput('postpoll', 0) == 1) {
                $postpollchecked = 'checked="checked"';
            }
            $numpolloptions = $this->bb->getInput('numpolloptions', 0);
        } // Editing a draft thread
        elseif (//$this->bb->input['action'] == 'editdraft' &&
            stristr($this->request->getUri()->getPath(), 'editdraft') &&
            $this->user->uid
        ) {
            $this->bb->input['threadprefix'] = $this->bb->threads['prefix'];
            $message = htmlspecialchars_uni($this->curpost['message']);
            $subject = htmlspecialchars_uni($this->curpost['subject']);
            if ($this->curpost['includesig'] != 0) {
                $postoptionschecked['signature'] = ' checked="checked"';
            }
            if ($this->curpost['smilieoff'] == 1) {
                $postoptionschecked['disablesmilies'] = ' checked="checked"';
            }
            $icon = $this->curpost['icon'];
// in Common      if($this->bb->forums['allowpicons'] != 0)
//      {
//        $posticons = $this->post->get_post_icons();
//      }
//      if($postoptions['subscriptionmethod'] == 'none')
//      {
//        $postoptions_subscriptionmethod_none = 'checked="checked"';
//      }
//      else if($postoptions['subscriptionmethod'] == 'email')
//      {
//        $postoptions_subscriptionmethod_email = 'checked="checked"';
//      }
//      else if($postoptions['subscriptionmethod'] == 'pm')
//      {
//        $postoptions_subscriptionmethod_pm = 'checked="checked"';
//      }
//      else
//      {
            $postoptions_subscriptionmethod_dont = 'checked="checked"';
//      }
            $numpolloptions = '2';
        } // Otherwise, this is our initial visit to this page.
        else {
            if ($this->user->signature != '') {
                $postoptionschecked['signature'] = ' checked="checked"';
            }
            if ($this->user->subscriptionmethod == 1) {
                $postoptions_subscriptionmethod_none = 'checked="checked"';
            } elseif ($this->user->subscriptionmethod == 2) {
                $postoptions_subscriptionmethod_email = 'checked="checked"';
            } elseif ($this->user->subscriptionmethod == 3) {
                $postoptions_subscriptionmethod_pm = 'checked="checked"';
            } else {
                $postoptions_subscriptionmethod_dont = 'checked="checked"';
            }
            $numpolloptions = '2';
        }
        $this->view->offsetSet('postoptionschecked', $postoptionschecked);
        $this->view->offsetSet('postoptions', [
            'dont' => $postoptions_subscriptionmethod_dont,
            'none' => $postoptions_subscriptionmethod_none,
            'email' => $postoptions_subscriptionmethod_email,
            'pm' => $postoptions_subscriptionmethod_pm
        ]);
        $this->view->offsetSet('postpollchecked', $postpollchecked);
        $this->view->offsetSet('numpolloptions', $numpolloptions);

        //$preview = '';
        // If we're preving a post then generate the preview.
        $postbit = '';
        if (!empty($this->bb->input['previewpost'])) {
            // If this isn't a logged in user, then we need to do some special validation.
            if ($this->user->uid == 0) {
                // If they didn't specify a username then give them 'Guest'
                if (!$this->bb->getInput('username', '', true)) {
                    $username = $this->lang->guest;
                } // Otherwise use the name they specified.
                else {
                    $username = $this->bb->getInput('username', '', true);
                }
                $uid = 0;
            } // This user is logged in.
            else {
                $username = $this->user->username;
                $uid = $this->user->uid;
            }

            // Set up posthandler.
            $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'insert');
            $posthandler->action = 'thread';

            // Set the thread data that came from the input to the $thread array.
            $new_thread = [
                'fid' => $this->bb->forums['fid'],
                'prefix' => $this->bb->getInput('threadprefix', 0),
                'subject' => $this->bb->getInput('subject', '', true),
                'icon' => $this->bb->getInput('icon', ''),
                'uid' => $uid,
                'username' => $username,
                'message' => $this->bb->getInput('message', '', true),
                'ipaddress' => $this->session->ipaddress,
                'posthash' => $this->bb->getInput('posthash', '')
            ];

            if ($this->bb->pid != '') {
                $new_thread['pid'] = $this->bb->pid;
            }

            $posthandler->set_data($new_thread);

            // Now let the post handler do all the hard work.
            $valid_thread = $posthandler->verifyMessage();
            $valid_subject = $posthandler->verifySubject();

            // guest post --> verify author
            if ($new_thread['uid'] == 0) {
                $valid_username = $posthandler->verifyAuthor();
            } else {
                $valid_username = true;
            }

            $post_errors = [];
            // Fetch friendly error messages if this is an invalid post
            if (!$valid_thread || !$valid_subject || !$valid_username) {
                $post_errors = $posthandler->get_friendly_errors();
            }

            // One or more errors returned, fetch error list and throw to newreply page
            if (count($post_errors) > 0) {
                $this->thread_errors = $this->bb->inline_error($post_errors);
            } else {
                if (empty($this->bb->input['username'])) {
                    $this->bb->input['username'] = $this->lang->guest;
                }
                $query = $this->db->query('
				SELECT u.*, f.*
				FROM ' . TABLE_PREFIX . 'users u
				LEFT JOIN ' . TABLE_PREFIX . "userfields f ON (f.ufid=u.uid)
				WHERE u.uid='" . $this->user->uid . "'
			");
                $post = $this->db->fetch_array($query);
                if (!$this->user->uid || !$post['username']) {
                    $post['username'] = htmlspecialchars_uni($this->bb->getInput('username', '', true));
                } else {
                    $post['userusername'] = $this->user->username;
                    $post['username'] = $this->user->username;
                }
                $previewmessage = $this->bb->getInput('message', '', true);
                $post['message'] = $previewmessage;
                $post['subject'] = $this->bb->getInput('subject', '', true);
                $post['icon'] = $this->bb->getInput('icon', 0);
                $this->bb->input['postoptions'] = $this->bb->getInput('postoptions', []);
                if (isset($this->bb->input['postoptions']['disablesmilies'])) {
                    $post['smilieoff'] = $this->bb->input['postoptions']['disablesmilies'];
                }
                $post['dateline'] = TIME_NOW;
                if (isset($this->bb->input['postoptions']['signature'])) {
                    $post['includesig'] = $this->bb->input['postoptions']['signature'];
                }
                if (!isset($post['includesig']) || $post['includesig'] != 1) {
                    $post['includesig'] = 0;
                }

                // Fetch attachments assigned to this post
                if ($this->bb->getInput('pid', 0)) {
                    $attachwhere = "pid='" . $this->bb->getInput('pid', 0) . "'";
                } else {
                    $attachwhere = "posthash='" . $this->db->escape_string($this->bb->getInput('posthash', '')) . "'";
                }

                $query = $this->db->simple_select('attachments', '*', $attachwhere);
                while ($attachment = $this->db->fetch_array($query)) {
                    $attachcache[0][$attachment['aid']] = $attachment;
                }

                $postbit = $this->post->build_postbit($post, 1);
            }
            $message = htmlspecialchars_uni($this->bb->getInput('message', '', true));
            $subject = htmlspecialchars_uni($this->bb->getInput('subject', '', true));
        } // Removing an attachment or adding a new one, or showing thread errors.
        elseif ($this->bb->getInput('attachmentaid', 0) ||
            $this->bb->getInput('newattachment', '') ||
            $this->bb->getInput('updateattachment', '') || $this->thread_errors
        ) {
            $message = htmlspecialchars_uni($this->bb->getInput('message', '', true));
            $subject = htmlspecialchars_uni($this->bb->getInput('subject', '', true));
        }
        $this->view->offsetSet('postbit', $postbit);
        $this->view->offsetSet('thread_errors', $this->thread_errors);
        $this->view->offsetSet('message', $message);
        $this->view->offsetSet('subject', $subject);

        // Generate thread prefix selector
        if (!$this->bb->getInput('threadprefix', 0)) {
            $this->bb->input['threadprefix'] = 0;
        }

        $prefixselect = $this->thread->build_prefix_select($this->bb->forums['fid'], $this->bb->getInput('threadprefix', 0));
        $this->view->offsetSet('prefixselect', $prefixselect);

        $posthash = htmlspecialchars_uni($this->bb->getInput('posthash', ''));
        $this->view->offsetSet('posthash', $posthash);

        // Can we disable smilies or are they disabled already?
        $this->bb->showDisableSmilies = false;
        if ($this->bb->forums['allowsmilies'] != 0) {
            $this->bb->showDisableSmilies = true;
        }

        $this->bb->showModOptions = false;
        // Show the moderator options
        if ($this->user->is_moderator($this->fid)) {
            $modoptions = $this->bb->getInput('modoptions', []);
            if (isset($modoptions['closethread']) && $modoptions['closethread'] == 1) {
                $closecheck = 'checked="checked"';
            } else {
                $closecheck = '';
            }
            if (isset($modoptions['stickthread']) && $modoptions['stickthread'] == 1) {
                $stickycheck = 'checked="checked"';
            } else {
                $stickycheck = '';
            }
            $this->bb->showModOptions = true;
            $this->view->offsetSet('closecheck', $closecheck);
            $this->view->offsetSet('stickycheck', $stickycheck);
            $bgcolor = 'trow1';
            $bgcolor2 = 'trow2';
        } else {
            $bgcolor = 'trow2';
            $bgcolor2 = 'trow1';
        }
        $this->view->offsetSet('bgcolor', $bgcolor);
        $this->view->offsetSet('bgcolor2', $bgcolor2);

        $this->bb->showAttachBox = false;
        if ($this->bb->settings['enableattachments'] != 0 && $this->forumpermissions['canpostattachments'] != 0) {
            $this->bb->showAttachBox = true;
            // Get a listing of the current attachments, if there are any
            $attachcount = 0;
            if (//$this->bb->input['action'] == 'editdraft' ||
            ($this->bb->input['tid'] && $this->bb->input['pid'])
            ) {
                $attachwhere = "pid='$this->bb->pid'";
            } else {
                $attachwhere = "posthash='" . $this->db->escape_string($posthash) . "'";
            }
            $query = $this->db->simple_select('attachments', '*', $attachwhere);
            $attachments = [];
            while ($attachment = $this->db->fetch_array($query)) {
                $attachment['size'] = $this->parser->friendlySize($attachment['filesize']);
                $attachment['icon'] = $this->upload->get_attachment_icon(get_extension($attachment['filename']));
                $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);

                $postinsert = false;
                if ($this->bb->settings['bbcodeinserter'] != 0 &&
                    $this->bb->forums['allowmycode'] != 0 &&
                    (!$this->user->uid || $this->user->showcodebuttons != 0)
                ) {
                    $postinsert = true;
                }

                $attachments[] = [
                    'attachment' => $attachment,
                    'postinsert' => $postinsert,
                    'attach_mod_options' => false
                ];
                $attachcount++;
            }
            $this->view->offsetSet('attachments', $attachments);

            $query = $this->db->simple_select('attachments', 'SUM(filesize) AS ausage', "uid='" . $this->user->uid . "'");
            $usage = $this->db->fetch_array($query);
            if ($usage['ausage'] > ($this->bb->usergroup['attachquota'] * 1024) && $this->bb->usergroup['attachquota'] != 0) {
                $noshowattach = 1;
            }
            if ($this->bb->usergroup['attachquota'] == 0) {
                $friendlyquota = $this->lang->unlimited;
            } else {
                $friendlyquota = $this->parser->friendlySize($this->bb->usergroup['attachquota'] * 1024);
            }
            $friendlyusage = $this->parser->friendlySize($usage['ausage']);
            $this->lang->attach_quota = $this->lang->sprintf($this->lang->attach_quota, $friendlyusage, $friendlyquota);

            $this->bb->attach_add = false;
            if ($this->bb->settings['maxattachments'] == 0 ||
                ($this->bb->settings['maxattachments'] != 0 &&
                    $attachcount < $this->bb->settings['maxattachments']) &&
                !isset($noshowattach)
            ) {
                $this->bb->attach_add = true;
            }
            $this->bb->attach_update = false;
            if (($this->bb->usergroup['caneditattachments'] || $this->forumpermissions['caneditattachments']) && $attachcount > 0) {
                $this->bb->attach_update = true;
            }
            $bgcolor = alt_trow();
        }

        $captcha = '';
        // Show captcha image for guests if enabled
        if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
            $correct = false;
            $post_captcha = new \RunBB\Core\Captcha($this, false, 'post_captcha');

            if ((!empty($this->bb->input['previewpost']) || $this->hide_captcha == true) && $post_captcha->type == 1) {
                // If previewing a post - check their current captcha input - if correct, hide the captcha input area
                // ... but only if it's a default one, reCAPTCHA and Are You a Human must be filled in every time due to draconian limits
                if ($post_captcha->validate_captcha() == true) {
                    $correct = true;

                    // Generate a hidden list of items for our captcha
                    $captcha = $post_captcha->build_hidden_captcha();
                }
            }
            if (!$correct) {
                if ($post_captcha->type == 1) {
                    $post_captcha->build_captcha();
                } elseif ($post_captcha->type == 2 || $post_captcha->type == 4) {
                    $post_captcha->build_recaptcha();
                }

                if ($post_captcha->html) {
                    $captcha = $post_captcha->html;
                }
            } elseif ($correct && ($post_captcha->type == 2 || $post_captcha->type == 4)) {
                $post_captcha->build_recaptcha();

                if ($post_captcha->html) {
                    $captcha = $post_captcha->html;
                }
            }
        }
        $this->view->offsetSet('captcha', $captcha);

        $this->bb->showPollBox = false;
        if ($this->forumpermissions['canpostpolls'] != 0) {
            $this->bb->showPollBox = true;
            $this->lang->max_options = $this->lang->sprintf($this->lang->max_options, $this->bb->settings['maxpolloptions']);
        }

        // Do we have any forum rules to show for this forum?
        //$forumrules = '';
        $this->bb->rulesType = 0;
        if ($this->bb->forums['rulestype'] >= 2 && $this->bb->forums['rules']) {
            if (!$this->bb->forums['rulestitle']) {
                $this->bb->forums['rulestitle'] = $this->lang->sprintf($this->lang->forum_rules, $this->bb->forums['name']);
            }

            $rules_parser = [
                'allow_html' => 1,
                'allow_mycode' => 1,
                'allow_smilies' => 1,
                'allow_imgcode' => 1
            ];

            $this->bb->forums['rules'] = $this->parser->parse_message($this->bb->forums['rules'], $rules_parser);
            //$foruminfo = $forum;
            $this->view->offsetSet('foruminfo', $this->bb->forums);

            if ($this->bb->forums['rulestype'] == 3) {
                $this->bb->rulesType = 3;
            } elseif ($this->bb->forums['rulestype'] == 2) {
                $this->bb->rulesType = 2;
            }
        }

        $moderation_text = '';
        if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveattachs')) {
            if ($this->forumpermissions['modattachments'] == 1 && $this->forumpermissions['canpostattachments'] != 0) {
                $moderation_text = $this->lang->moderation_forum_attachments;
            }
        }

        if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapprovethreads')) {
            if ($this->forumpermissions['modthreads'] == 1) {
                $moderation_text = $this->lang->moderation_forum_thread;
            }
        }

        if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveposts')) {
            if ($this->user->moderateposts == 1) {
                $moderation_text = $this->lang->moderation_user_posts;
            }
        }
        $this->view->offsetSet('moderation_text', $moderation_text);

        $this->plugins->runHooks('newthread_end');

        $this->bb->forums['name'] = strip_tags($this->bb->forums['name']);
        $this->lang->newthread_in = $this->lang->sprintf($this->lang->newthread_in, $this->bb->forums['name']);

        $this->bb->output_page();
        $this->view->render($response, '@forum/newthread.html.twig');
    }

    public function doNewthread(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        if ($this->bb->input['action'] === 'newthread') {// возвращаемся на previewpost
            return $this->index($request, $response, true);
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('newthread_do_newthread_start');

        // If this isn't a logged in user, then we need to do some special validation.
        if ($this->user->uid == 0) {
            // If they didn't specify a username then give them 'Guest'
            if (!$this->bb->getInput('username', '', true)) {
                $username = $this->lang->guest;
            } // Otherwise use the name they specified.
            else {
                $username = $this->bb->getInput('username', '', true);
            }
            $uid = 0;

            if (!$this->user->uid && $this->bb->settings['stopforumspam_on_newthread']) {
                $stop_forum_spam_checker = new \RunBB\Core\StopForumSpamChecker(
                    $this->plugins,
                    $this->bb->settings['stopforumspam_min_weighting_before_spam'],
                    $this->bb->settings['stopforumspam_check_usernames'],
                    $this->bb->settings['stopforumspam_check_emails'],
                    $this->bb->settings['stopforumspam_check_ips'],
                    $this->bb->settings['stopforumspam_log_blocks']
                );

                try {
                    if ($stop_forum_spam_checker->is_user_a_spammer($this->bb->getInput('username', '', true), '', $this->session->ipaddress)) {
                        $errors[] = $this->lang->sprintf(
                            $this->lang->error_stop_forum_spam_spammer,
                            $stop_forum_spam_checker->getErrorText([
                                'stopforumspam_check_usernames',
                                'stopforumspam_check_ips'
                            ])
                        );
                    }
                } catch (\Exception $e) {
                    if ($this->bb->settings['stopforumspam_block_on_error']) {
                        $errors[] = $this->lang->error_stop_forum_spam_fetching;
                    }
                }
            }
        } // This user is logged in.
        else {
            $username = $this->user->username;
            $uid = $this->user->uid;
        }

        // Attempt to see if this post is a duplicate or not
        if ($uid > 0) {
            $user_check = "p.uid='{$uid}'";
        } else {
            $user_check = 'p.ipaddress=' . $this->session->ipaddress;
        }
        if (!$this->bb->getInput('savedraft', '') && !$this->bb->pid) {
            $query = $this->db->simple_select(
                'posts p',
                'p.pid',
                "$user_check AND p.fid='{$this->bb->forums['fid']}' 
        AND p.subject='" . $this->db->escape_string($this->bb->getInput('subject', '', true)) . "' 
        AND p.message='" . $this->db->escape_string($this->bb->getInput('message', '', true)) . "' 
        AND p.dateline>" . (TIME_NOW - 600)
            );
            $duplicate_check = $this->db->fetch_field($query, 'pid');
            if ($duplicate_check) {
                $this->bb->error($this->lang->error_post_already_submitted);
            }
        }

        // Set up posthandler.
        $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'insert');
        $posthandler->action = 'thread';

        // Set the thread data that came from the input to the $thread array.
        $new_thread = [
            'fid' => $this->bb->forums['fid'],
            'subject' => $this->bb->getInput('subject', '', true),
            'prefix' => $this->bb->getInput('threadprefix', 0),
            'icon' => $this->bb->getInput('icon', 0),
            'uid' => $uid,
            'username' => $username,
            'message' => $this->bb->getInput('message', '', true),
            'ipaddress' => $this->session->ipaddress,
            'posthash' => $this->bb->getInput('posthash', '')
        ];

        if ($this->bb->pid != '') {
            $new_thread['pid'] = $this->bb->pid;
        }

        // Are we saving a draft thread?
        if ($this->bb->getInput('savedraft', '') && $this->user->uid) {
            $new_thread['savedraft'] = 1;
        } else {
            $new_thread['savedraft'] = 0;
        }

        // Is this thread already a draft and we're updating it?
        if (isset($this->bb->threads['tid']) && $this->bb->threads['visible'] == -2) {
            $new_thread['tid'] = $this->bb->threads['tid'];
        }

        $postoptions = $this->bb->getInput('postoptions', []);
        if (!isset($postoptions['signature'])) {
            $postoptions['signature'] = 0;
        }
        if (!isset($postoptions['subscriptionmethod'])) {
            $postoptions['subscriptionmethod'] = 0;
        }
        if (!isset($postoptions['disablesmilies'])) {
            $postoptions['disablesmilies'] = 0;
        }

        // Set up the thread options from the input.
        $new_thread['options'] = [
            'signature' => $postoptions['signature'],
            'subscriptionmethod' => $postoptions['subscriptionmethod'],
            'disablesmilies' => $postoptions['disablesmilies']
        ];

        // Apply moderation options if we have them
        $new_thread['modoptions'] = $this->bb->getInput('modoptions', []);

        $posthandler->set_data($new_thread);

        // Now let the post handler do all the hard work.
        $valid_thread = $posthandler->validateThread();

        $post_errors = [];
        // Fetch friendly error messages if this is an invalid thread
        if (!$valid_thread) {
            $post_errors = $posthandler->get_friendly_errors();
        }

        // Check captcha image
        if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
            $post_captcha = new \RunBB\Core\Captcha($this);

            if ($post_captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($post_captcha->get_errors() as $error) {
                    $post_errors[] = $error;
                }
            } else {
                $this->hide_captcha = true;
            }
        }

        // One or more errors returned, fetch error list and throw to newthread page
        if (count($post_errors) > 0) {
            $this->thread_errors = $this->bb->inline_error($post_errors);
            //$this->bb->input['action'] = 'newthread';
            $this->index($request, $response, true);
        } // No errors were found, it is safe to insert the thread.
        else {
            $thread_info = $posthandler->insertThread();
            $tid = $thread_info['tid'];
            $visible = $thread_info['visible'];

            // Invalidate solved captcha
            if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
                $post_captcha->invalidate_captcha();
            }

            $force_redirect = false;

            // Mark thread as read
            $this->indicator->mark_thread_read($tid, $this->fid);

            // We were updating a draft thread, send them back to the draft listing.
            if ($new_thread['savedraft'] == 1) {
                $this->lang->redirect_newthread = $this->lang->draft_saved;
                $url = 'usercp/drafts';
            } // A poll was being posted with this thread, throw them to poll posting page.
            elseif ($this->bb->getInput('postpoll', 0) && $this->forumpermissions['canpostpolls']) {
                $url = "polls/newpoll&tid=$tid&polloptions=" . $this->bb->getInput('numpolloptions', 0);
                $this->lang->redirect_newthread .= $this->lang->redirect_newthread_poll;
            } // This thread is stuck in the moderation queue, send them back to the forum.
            elseif (!$visible) {
                // Moderated thread
                $this->lang->redirect_newthread .= $this->lang->redirect_newthread_moderation;
                $url = $this->forum->get_forum_link($this->fid);

                // User must see moderation notice, regardless of redirect settings
                $force_redirect = true;
            } // The thread is being made in a forum the user cannot see threads in, send them back to the forum.
            elseif ($visible == 1 && $this->forumpermissions['canviewthreads'] != 1) {
                $this->lang->redirect_newthread .= $this->lang->redirect_newthread_unviewable;
                $url = $this->forum->get_forum_link($this->fid);

                // User must see permission notice, regardless of redirect settings
                $force_redirect = true;
            } // This is just a normal thread - send them to it.
            else {
                // Visible thread
                $this->lang->redirect_newthread .= $this->lang->redirect_newthread_thread;
                $url = get_thread_link($tid);
            }

            // Mark any quoted posts so they're no longer selected - attempts to maintain those which weren't selected
            if (isset($this->bb->input['quoted_ids']) &&
                isset($this->bb->cookies['multiquote']) &&
                $this->bb->settings['multiquote'] != 0
            ) {
                // We quoted all posts - remove the entire cookie
                if ($this->bb->getInput('quoted_ids', '') == 'all') {
                    $this->bb->my_unsetcookie('multiquote');
                }
            }

            $this->plugins->runHooks('newthread_do_newthread_end');

            // Hop to it! Send them to the next page.
            if (!$this->bb->getInput('postpoll', 0)) {
                $this->lang->redirect_newthread .= $this->lang->sprintf(
                    $this->lang->redirect_return_forum,
                    $this->forum->get_forum_link($this->fid)
                );
            }
            $this->bb->redirect($url, $this->lang->redirect_newthread, '', $force_redirect);
        }
    }
}
