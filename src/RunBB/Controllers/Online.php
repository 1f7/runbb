<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Online extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('online');

        if ($this->bb->usergroup['canviewonline'] == 0) {
            return $this->bb->error_no_permission();
        }
        // Make navigation
        $this->bb->add_breadcrumb($this->lang->nav_online, 'online');

        if ($this->bb->getInput('action', '') === 'today') {
            $this->bb->add_breadcrumb($this->lang->nav_onlinetoday);

            $this->plugins->runHooks('online_today_start');

            $threshold = TIME_NOW - (60 * 60 * 24);
//            $query = $this->db->simple_select('users', 'COUNT(uid) AS users', "lastactive > '{$threshold}'");
//            $todaycount = $this->db->fetch_field($query, 'users');

            $todaycount = \RunBB\Models\User::where('lastactive', '>', $threshold)->count();

//            $query = $this->db->simple_select('users',
//  'COUNT(uid) AS users', "lastactive > '{$threshold}' AND invisible = '1'");
//            $invis_count = $this->db->fetch_field($query, 'users');

            $invis_count = \RunBB\Models\User::where([
                ['lastactive', '>', $threshold],
                ['invisible', '=', '1']
            ])->count();

            if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                $this->bb->settings['threadsperpage'] = 20;
            }

            // Add pagination
            $perpage = $this->bb->settings['threadsperpage'];

            if ($this->bb->getInput('page', 0) > 0) {
                $page = $this->bb->getInput('page', 0);
                $start = ($page - 1) * $perpage;
                $pages = ceil($todaycount / $perpage);
                if ($page > $pages) {
                    $start = 0;
                    $page = 1;
                }
            } else {
                $start = 0;
                $page = 1;
            }

//            $query = $this->db->simple_select('users', '*', "lastactive > '{$threshold}'",
//                array('order_by' => 'lastactive', 'order_dir' => 'desc',
//'limit' => $perpage, 'limit_start' => $start));

            $on = \RunBB\Models\User::where('lastactive', '>', $threshold)
                ->orderBy('lastactive', 'desc')
                ->skip($start)->take($perpage)->get();

            $todayrows = [];
//            while ($online = $this->db->fetch_array($query)) {
            foreach ($on as $online) {
                $invisiblemark = '';
                if ($online['invisible'] == 1) {
                    $invisiblemark = '*';
                }

                if ($online['invisible'] != 1 ||
                    $this->bb->usergroup['canviewwolinvis'] == 1 ||
                    $online['uid'] == $this->user->uid) {
                    $username = $online['username'];
                    $username = $this->user->format_name($username, $online['usergroup'], $online['displaygroup']);
                    $online['profilelink'] = $this->user->build_profile_link($username, $online['uid']);
                    $onlinetime = $this->time->formatDate($this->bb->settings['timeformat'], $online['lastactive']);
                    $todayrows[] = [
                        'profilelink' => $online['profilelink'],
                        'invisiblemark' => $invisiblemark,
                        'onlinetime' => $onlinetime
                    ];
                }
            }
            $this->view->offsetSet('todayrows', $todayrows);

            $todaycount = $this->parser->formatNumber($todaycount);
            $invis_count = $this->parser->formatNumber($invis_count);

            if ($todaycount == 1) {
                $onlinetoday = $this->lang->member_online_today;
            } else {
                $onlinetoday = $this->lang->sprintf($this->lang->members_were_online_today, $todaycount);
            }

            if ($invis_count) {
                $string = $this->lang->members_online_hidden;

                if ($invis_count == 1) {
                    $string = $this->lang->member_online_hidden;
                }

                $onlinetoday .= $this->lang->sprintf($string, $invis_count);
            }
            $this->view->offsetSet('onlinetoday', $onlinetoday);

            $multipage = $this->pagination->multipage($todaycount, $perpage, $page, 'online?action=today');
            $this->view->offsetSet('multipage', $multipage);

            $this->plugins->runHooks('online_today_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/onlinetoday.html.twig');
        } else {
            $this->plugins->runHooks('online_start');

            $this->onliner = new \RunBB\Helpers\OnlineHelper($this->bb);

            // Custom sorting options
            $sortby = $this->bb->getInput('sortby', '');
            if ($sortby == 'username') {
//                $sql = 'users.username ASC, sessions.time DESC';
                $condition = 'users.username';
                $direction = 'asc';
                $refresh_string = '?sortby=username';
            } elseif ($sortby == 'location') {
//                $sql = 'sessions.location ASC, sessions.time DESC';
                $condition = 'sessions.location';
                $direction = 'asc';
                $refresh_string = '?sortby=location';
            } // Otherwise sort by last refresh
            else {
//                switch ($this->db->type) {
//                    case 'sqlite':
//                    case 'pgsql':
////                        $sql = 'sessions.time DESC';
//                        break;
//                    default:
//                        $sql = 'IF( sessions.uid >0, 1, 0 ) DESC';//, sessions.time DESC';
//                        break;
//                }
                $condition = 'sessions.time';
                $direction = 'desc';
                $refresh_string = '';
            }

            $timesearch = TIME_NOW - $this->bb->settings['wolcutoffmins'] * 60;

            // Exactly how many users are currently online?
//            switch ($this->db->type) {
//                case 'sqlite':
//                    $sessions = array();
//                    $query = $this->db->simple_select('sessions', 'sid', "time > {$timesearch}");
//                    while ($sid = $this->db->fetch_field($query, 'sid')) {
//                        $sessions[$sid] = 1;
//                    }
//                    $online_count = count($sessions);
//                    unset($sessions);
//                    break;
//                case 'pgsql':
//                default:
//                    $query = $this->db->simple_select('sessions', 'COUNT(sid) as online', "time > {$timesearch}");
//                    $online_count = $this->db->fetch_field($query, 'online');
//                    break;
//            }
            $online_count = \RunBB\Models\Session::where('time', '>', $timesearch)->count();

            if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                $this->bb->settings['threadsperpage'] = 20;
            }

            // How many pages are there?
            $perpage = $this->bb->settings['threadsperpage'];

            if ($this->bb->getInput('page', 0) > 0) {
                $page = $this->bb->getInput('page', 0);
                $start = ($page - 1) * $perpage;
                $pages = ceil($online_count / $perpage);
                if ($page > $pages) {
                    $start = 0;
                    $page = 1;
                }
            } else {
                $start = 0;
                $page = 1;
            }

            // Assemble page URL
            $multipage = $this->pagination->multipage($online_count, $perpage, $page, 'online' . $refresh_string);
            $this->view->offsetSet('multipage', $multipage);

            // Query for active sessions
//            $query = $this->db->query('
//		SELECT DISTINCT s.sid, s.ip, s.uid, s.time, s.location, u.username, s.nopermission, u.invisible,
//u.usergroup, u.displaygroup
//		FROM ' . TABLE_PREFIX . 'sessions s
//		LEFT JOIN ' . TABLE_PREFIX . "users u ON (s.uid=u.uid)
//		WHERE s.time>'$timesearch'
//		ORDER BY $sql
//		LIMIT {$start}, {$perpage}
//	");
            $u = \RunBB\Models\Session::where('sessions.time', '>', $timesearch)
                ->leftJoin('users', 'sessions.uid', '=', 'users.uid')
                ->orderBy($condition, $direction)
//                ->orderBy('sessions.time', 'desc')
                ->skip($start)->take($perpage)
                ->get([
                    'sessions.sid',
                    'sessions.ip',
                    'sessions.uid',
                    'sessions.time',
                    'sessions.location',
                    'sessions.nopermission',
                    'users.username',
                    'users.invisible',
                    'users.usergroup',
                    'users.displaygroup'
                ]);

            // Fetch spiders
            $spiders = $this->cache->read('spiders');

//            while ($user = $this->db->fetch_array($query)) {
            foreach ($u as $user) {
                $this->plugins->runHooks('online_user');

                // Fetch the WOL activity
                $user['activity'] = $this->onliner->fetchWolActivity($user['location'], $user['nopermission']);

                $botkey = my_strtolower(str_replace('bot=', '', $user['sid']));

                // Have a registered user
                if ($user['uid'] > 0) {
                    if (empty($users[$user['uid']]) || $users[$user['uid']]['time'] < $user['time']) {
                        $users[$user['uid']] = $user;
                    }
                } // Otherwise this session is a bot
                elseif (my_strpos($user['sid'], 'bot=') !== false && $spiders[$botkey]) {
                    $user['bot'] = $spiders[$botkey]['name'];
                    $user['usergroup'] = $spiders[$botkey]['usergroup'];
                    $guests[] = $user;
                } // Or a guest
                else {
                    $guests[] = $user;
                }
            }

            // Now we build the actual online rows - we do this separately because we need to query
            // all of the specific activity and location information
            $online_rows = [];
            if (isset($users) && is_array($users)) {
                reset($users);
                foreach ($users as $user) {
                    $online_rows[] = $this->onliner->buildWolRow($user);
                }
            }
            if (isset($guests) && is_array($guests)) {
                reset($guests);
                foreach ($guests as $user) {
                    $online_rows[] = $this->onliner->buildWolRow($user);
                }
            }
            $this->view->offsetSet('online_rows', $online_rows);

//            // Fetch the most online information
//            $most_online = $this->cache->read('mostonline');
//            $record_count = $most_online['numusers'];
//            $record_date = $this->time->formatDate('relative', $most_online['time']);

            // Set automatic refreshing if enabled
            if ($this->bb->settings['refreshwol'] > 0) {
                $refresh_time = $this->bb->settings['refreshwol'] * 60;
                $refresh = "<meta http-equiv=\"refresh\" 
                content=\"{$refresh_time};URL={$this->bb->settings['bburl']}/online{$refresh_string}\" />";
            }
            $this->view->offsetSet('refresh', $refresh);

            $this->plugins->runHooks('online_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/online.html.twig');
        }
    }
}
