<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AdvancedSearch extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->nav_search);

        $this->plugins->runHooks('private_advanced_search');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/advancedsearch.html.twig');
    }

    public function doSearch(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->plugins->runHooks('private_do_search_start');

        // Simulate coming from our advanced search form with some preset options
        if ($this->bb->getInput('quick_search', '')) {
            $this->bb->input['action'] = 'do_search';
            $this->bb->input['subject'] = 1;
            $this->bb->input['message'] = 1;
            $this->bb->input['folder'] = $this->bb->input['fid'];
            unset($this->bb->input['jumpto']);
            unset($this->bb->input['fromfid']);
        }

        // Check if search flood checking is enabled and user is not admin
        if ($this->bb->settings['searchfloodtime'] > 0 && $this->bb->usergroup['cancp'] != 1) {
            // Fetch the time this user last searched
            $timecut = TIME_NOW-$this->bb->settings['searchfloodtime'];
            $query = $this->db->simple_select(
                'searchlog',
                '*',
                "uid='{$this->user->uid}' AND dateline > '$timecut'",
                ['order_by' => 'dateline', 'order_dir' => 'DESC']
            );
            $last_search = $this->db->fetch_array($query);
            // Users last search was within the flood time, show the error
            if ($last_search['sid']) {
                $remaining_time = $this->bb->settings['searchfloodtime']-(TIME_NOW-$last_search['dateline']);
                if ($remaining_time == 1) {
                    $this->lang->error_searchflooding = $this->lang->sprintf(
                        $this->lang->error_searchflooding_1,
                        $this->bb->settings['searchfloodtime']
                    );
                } else {
                    $this->lang->error_searchflooding = $this->lang->sprintf(
                        $this->lang->error_searchflooding,
                        $this->bb->settings['searchfloodtime'],
                        $remaining_time
                    );
                }
                $this->bb->error($this->lang->error_searchflooding);
            }
        }

        if ($this->bb->getInput('subject', 0) != 1 && $this->bb->getInput('message', 0) != 1) {
            $this->bb->error($this->lang->error_nosearchresults);
        }

        if ($this->bb->getInput('message', 0) == 1) {
            $resulttype = 'pmmessages';
        } else {
            $resulttype = 'pmsubjects';
        }

        $search_data = [
            'keywords' => $this->bb->getInput('keywords', '', true),
            'subject' => $this->bb->getInput('subject', 0),
            'message' => $this->bb->getInput('message', 0),
            'sender' => $this->bb->getInput('sender', '', true),
            'status' => $this->bb->getInput('status', []),
            'folder' => $this->bb->getInput('folder', [])
        ];

        if ($this->db->can_search == true) {
            $search_results = $this->search->privatemessage_perform_search_mysql($search_data);
            if ($search_results === 'exit') {
                return;
            }
        } else {
            $this->bb->error($this->lang->error_no_search_support);
        }
        $sid = md5(uniqid(microtime(), true));
        $searcharray = [
            'sid' => $this->db->escape_string($sid),
            'uid' => $this->user->uid,
            'dateline' => TIME_NOW,
            'ipaddress' => $this->session->ipaddress,
            'threads' => '',
            'posts' => '',
            'resulttype' => $resulttype,
            'querycache' => $search_results['querycache'],
            'keywords' => $this->db->escape_string($this->bb->getInput('keywords', '', true)),
        ];
        $this->plugins->runHooks('private_do_search_process');

        $this->db->insert_query('searchlog', $searcharray);

        // Sender sort won't work yet
        $sortby = ['subject', 'sender', 'dateline'];

        if (in_array($this->bb->getInput('sort', ''), $sortby)) {
            $sortby = $this->bb->getInput('sort', '');
        } else {
            $sortby = 'dateline';
        }

        if (my_strtolower($this->bb->getInput('sortordr', '')) === 'asc') {
            $sortorder = 'asc';
        } else {
            $sortorder = 'desc';
        }

        $this->plugins->runHooks('private_do_search_end');
        $this->bb->redirect($this->bb->settings['bburl'] . '/private/results?sid='.$sid.'&sortby='.$sortby.'
            &order='.$sortorder, $this->lang->redirect_searchresults);
    }
}
