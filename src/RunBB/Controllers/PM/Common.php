<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{
    protected $errors;

    protected function init()
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('private');

        $this->send_errors = '';

        if ($this->bb->settings['enablepms'] == 0) {
            $this->bb->error($this->lang->pms_disabled);
            return 'exit';
        }

        if ($this->user->uid == '/' || $this->user->uid == 0 || $this->bb->usergroup['canusepms'] == 0) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        if (!$this->user->pmfolders) {
            $this->user->pmfolders = '1**$%%$2**$%%$3**$%%$4**';

            $sql_array = [
                'pmfolders' => $this->user->pmfolders
            ];
            $this->db->update_query('users', $sql_array, 'uid = ' . $this->user->uid);
        }

        $this->bb->input['fid'] = $this->bb->getInput('fid', 0);

        $folder_id = $folder_name = '';

        $this->foldernames = [];
        $foldersexploded = explode('$%%$', $this->user->pmfolders);
        $folderjump_folder = $folderoplist_folder = $foldersearch_folder = '';
        $folderoplist = '';
        foreach ($foldersexploded as $key => $folders) {
            $folderinfo = explode('**', $folders, 2);
            if ($this->bb->input['fid'] == $folderinfo[0]) {
                $sel = ' selected="selected"';
            } else {
                $sel = '';
            }
            $folderinfo[1] = $this->user->get_pm_folder_name($folderinfo[0], $folderinfo[1]);
            $this->foldernames[$folderinfo[0]] = $folderinfo[1];

            $folder_id = $folderinfo[0];
            $folder_name = $folderinfo[1];

            $folderoplist .= "<option value=\"{$folder_id}\"{$sel}>{$folder_name}</option>";
//      $folderjump_folder    .= "<option value=\"{$folder_id}\"{$sel}>{$folder_name}</option>";
//      //ev al("\$folderjump_folder    .= \"".$this->templates->get("private_jump_folders_folder")."\";");
//      $folderoplist_folder  .= "<option value=\"{$folder_id}\"{$sel}>{$folder_name}</option>";
//      //ev al("\$folderoplist_folder  .= \"".$this->templates->get("private_jump_folders_folder")."\";");
//      $foldersearch_folder  .= "<option value=\"{$folder_id}\"{$sel}>{$folder_name}</option>";
//      //ev al("\$foldersearch_folder  .= \"".$this->templates->get("private_jump_folders_folder")."\";");
        }
        $this->view->offsetSet('folderoplist', $folderoplist);


        /* post to template
            $folderjump = "
              <select name=\"jumpto\">
                  {$folderjump_folder}
              </select>";
            //ev al("\$folderjump = \"".$this->templates->get("private_jump_folders")."\";");

            $folderoplist = "
              <input type=\"hidden\" value=\"{$this->bb->input['fid']}\" name=\"fromfid\" />
              <select name=\"fid\">
              {$folderoplist_folder}
              </select>";
            //ev al("\$folderoplist = \"".$this->templates->get("private_move")."\";");


            $foldersearch = "
              <select multiple=\"multiple\" name=\"folder[]\" id=\"folder\">
              <option selected=\"selected\">{$this->lang->all_folders}</option>
              {{ folderoplist|raw }}
              </select>";
            //ev al("\$foldersearch = \"".$this->templates->get("private_advanced_search_folders")."\";");
        */

        $this->user->usercp_menu();

        $this->plugins->runHooks('private_start');

        // Make navigation
        $this->bb->add_breadcrumb($this->lang->nav_pms, $this->bb->settings['bburl'] . '/private');
    }
}
