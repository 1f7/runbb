<?php
/**
 * Created by PhpStorm.
 * User: svl
 * Date: 26.11.16
 * Time: 11:26
 */

namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Delete extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_delete_start');

        $query = $this->db->simple_select(
            "privatemessages",
            "*",
            "pmid='".$this->bb->getInput('pmid', 0)."' AND uid='".$this->user->uid."' AND folder='4'",
            ['order_by' => 'pmid']
        );
        if ($this->db->num_rows($query) == 1) {
            $this->db->delete_query("privatemessages", "pmid='".$this->bb->getInput('pmid', 0)."'");
        } else {
            $sql_array = [
                "folder" => 4,
                "deletetime" => TIME_NOW
            ];
            $this->db->update_query(
                'privatemessages',
                $sql_array,
                "pmid='".$this->bb->getInput('pmid', 0)."' AND uid='".$this->user->uid."'"
            );
        }

        // Update PM count
        $this->user->update_pm_count();

        $this->plugins->runHooks('private_delete_end');
        $this->bb->redirect($this->bb->settings['bburl'].'/private', $this->lang->redirect_pmsdeleted);
    }
}
