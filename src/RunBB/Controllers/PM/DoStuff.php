<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class DoStuff extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_do_stuff');

        if (!empty($this->bb->input['hop'])) {
            return $response->withRedirect(
                $this->bb->settings['bburl'].'/private?fid='.$this->bb->getInput('jumpto', '')
            );
        } elseif (!empty($this->bb->input['moveto'])) {
            $this->bb->input['check'] = $this->bb->getInput('check', []);
            if (!empty($this->bb->input['check'])) {
                foreach ($this->bb->input['check'] as $key => $val) {
                    $sql_array = [
                        'folder' => $this->bb->input['fid']
                    ];
                    $this->db->update_query(
                        'privatemessages',
                        $sql_array,
                        "pmid='".(int)$key."' AND uid='".$this->user->uid."'"
                    );
                }
            }
            // Update PM count
            $this->user->update_pm_count();

            if (!empty($this->bb->input['fromfid'])) {
                $this->bb->redirect(
                    $this->bb->settings['bburl'] . '/private?fid='.$this->bb->getInput('fromfid', 0),
                    $this->lang->redirect_pmsmoved
                );
            } else {
                $this->bb->redirect($this->bb->settings['bburl'] . '/private', $this->lang->redirect_pmsmoved);
            }
        } elseif (!empty($this->bb->input['delete'])) {
            $this->bb->input['check'] = $this->bb->getInput('check', [0]);
            if (!empty($this->bb->input['check'])) {
                $pmssql = '';
                foreach ($this->bb->input['check'] as $key => $val) {
                    if ($pmssql) {
                        $pmssql .= ',';
                    }
                    $pmssql .= "'".(int)$key."'";
                }

                $deletepms = [];
                $query = $this->db->simple_select(
                    'privatemessages',
                    'pmid, folder',
                    "pmid IN ($pmssql) AND uid='".$this->user->uid."' AND folder='4'",
                    ['order_by' => 'pmid']
                );
                while ($delpm = $this->db->fetch_array($query)) {
                    $deletepms[$delpm['pmid']] = 1;
                }

                foreach ($this->bb->input['check'] as $key => $val) {
                    $key = (int)$key;
                    if (!empty($deletepms[$key])) {
                        $this->db->delete_query('privatemessages', "pmid='$key' AND uid='".$this->user->uid."'");
                    } else {
                        $sql_array = [
                            'folder' => 4,
                            'deletetime' => TIME_NOW
                        ];
                        $this->db->update_query(
                            'privatemessages',
                            $sql_array,
                            "pmid='".$key."' AND uid='".$this->user->uid."'"
                        );
                    }
                }
            }
            // Update PM count
            $this->user->update_pm_count();

            if (!empty($this->bb->input['fromfid'])) {
                $this->bb->redirect(
                    $this->bb->settings['bburl'] . '/private?fid='.$this->bb->getInput('fromfid', 0),
                    $this->lang->redirect_pmsdeleted
                );
            } else {
                $this->bb->redirect($this->bb->settings['bburl'] . '/private', $this->lang->redirect_pmsdeleted);
            }
        }
    }
}
