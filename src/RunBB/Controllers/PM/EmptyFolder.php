<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class EmptyFolder extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->nav_empty);

        if ($this->user->totalpms == 0) {
            $this->bb->error($this->lang->error_nopms);
        }

        $this->plugins->runHooks('private_empty_start');

        $foldersexploded = explode('$%%$', $this->user->pmfolders);
        $folderlist = [];
        foreach ($foldersexploded as $key => $folders) {
            $folderinfo = explode('**', $folders, 2);

//            $query = $this->db->simple_select('privatemessages', 'COUNT(*) AS pmsinfolder',
//                " folder='" . $folderinfo[0] . "' AND uid='" . $this->user->uid . "'");
//            $thing = $this->db->fetch_array($query);
            $cnt = \RunBB\Models\Privatemessage::where([
                ['folder', '=', $folderinfo[0]],
                ['uid', '=', $this->user->uid]
            ])->count();

            $folderlist[] = [
                'foldername' => $this->user->get_pm_folder_name($folderinfo[0], $folderinfo[1]),
                'foldercount' => $this->parser->formatNumber($cnt),//$thing['pmsinfolder']),
                'fid' => $folderinfo[0]
            ];
        }
        $this->view->offsetSet('folderlist', $folderlist);

        $this->plugins->runHooks('private_empty_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/emptyfolder.html.twig');
    }

    public function doEmpty(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_do_empty_start');

        $emptyq = '';
        $this->bb->input['empty'] = $this->bb->getInput('empty', []);
        $keepunreadq = '';
        if ($this->bb->getInput('keepunread', 0) == 1) {
            $keepunreadq = " AND status!='0'";
        }
        if (!empty($this->bb->input['empty'])) {
            foreach ($this->bb->input['empty'] as $key => $val) {
                if ($val == 1) {
                    $key = (int)$key;
                    if ($emptyq) {
                        $emptyq .= ' OR ';
                    }
                    $emptyq .= "folder='$key'";
                }
            }

            if ($emptyq != '') {
                $this->db->delete_query('privatemessages', "($emptyq) AND uid='". $this->user->uid ."'{$keepunreadq}");
            }
        }

        // Update PM count
        $this->user->update_pm_count();

        $this->plugins->runHooks('private_do_empty_end');
        $this->bb->redirect($this->bb->settings['bburl'] . '/private', $this->lang->redirect_pmfoldersemptied);
    }
}
