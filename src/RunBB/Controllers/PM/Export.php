<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Export extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->user->totalpms == 0) {
            $this->bb->error($this->lang->error_nopms);
        }
        $this->plugins->runHooks('private_export_start');

        $this->bb->add_breadcrumb($this->lang->nav_export);

        $foldersexploded = explode('$%%$', $this->user->pmfolders);
        $folderlist = [];
        foreach ($foldersexploded as $key => $folders) {
            $folderinfo = explode('**', $folders, 2);
            $folderinfo[1] = $this->user->get_pm_folder_name($folderinfo[0], $folderinfo[1]);

            $folderlist[] = [
                'folder_id' => $folderinfo[0],
                'folder_name' => $folderinfo[1]
            ];
        }
        $this->view->offsetSet('folderlist', $folderlist);

        $this->plugins->runHooks('private_export_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/export.html.twig');
    }

    public function doExport(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_do_export_start');

        $this->lang->private_messages_for = $this->lang->sprintf(
            $this->lang->private_messages_for,
            $this->user->username
        );
        $exdate = $this->time->formatDate($this->bb->settings['dateformat'], TIME_NOW, 0, 0);
        $extime = $this->time->formatDate($this->bb->settings['timeformat'], TIME_NOW, 0, 0);
        $this->lang->exported_date = $this->lang->sprintf($this->lang->exported_date, $exdate, $extime);
        $foldersexploded = explode('$%%$', $this->user->pmfolders);
        foreach ($foldersexploded as $key => $folders) {
            $folderinfo = explode('**', $folders, 2);
            $folderinfo[1] = $this->user->get_pm_folder_name($folderinfo[0], $folderinfo[1]);
            $foldersexploded[$key] = implode('**', $folderinfo);
        }

        if ($this->bb->getInput('pmid', 0)) {
            $wsql = "pmid='" . $this->bb->getInput('pmid', 0) . "' AND uid='" . $this->user->uid . "'";
        } else {
            if ($this->bb->getInput('daycut', 0) && ($this->bb->getInput('dayway', '') !== 'disregard')) {
                $datecut = TIME_NOW - ($this->bb->getInput('daycut', 0) * 86400);
                $wsql = 'pm.dateline';
                if ($this->bb->getInput('dayway', '') === 'older') {
                    $wsql .= '<=';
                } else {
                    $wsql .= '>=';
                }
                $wsql .= "'$datecut'";
            } else {
                $wsql = '1=1';
            }

            $this->bb->input['exportfolders'] = $this->bb->getInput('exportfolders', []);
            if (!empty($this->bb->input['exportfolders'])) {
                $folderlst = '';
                foreach ($this->bb->input['exportfolders'] as $key => $val) {
                    $val = $this->db->escape_string($val);
                    if ($val == 'all') {
                        $folderlst = '';
                        break;
                    } else {
                        if (!$folderlst) {
                            $folderlst = " AND pm.folder IN ('$val'";
                        } else {
                            $folderlst .= ",'$val'";
                        }
                    }
                }
                if ($folderlst) {
                    $folderlst .= ')';
                }
                $wsql .= $folderlst;
            } else {
                $this->bb->error($this->lang->error_pmnoarchivefolders);
            }

            if ($this->bb->getInput('exportunread', 0) != 1) {
                $wsql .= " AND pm.status!='0'";
            }
        }
        $query = $this->db->query('
		SELECT pm.*, fu.username AS fromusername, tu.username AS tousername
		FROM ' . TABLE_PREFIX . 'privatemessages pm
		LEFT JOIN ' . TABLE_PREFIX . 'users fu ON (fu.uid=pm.fromid)
		LEFT JOIN ' . TABLE_PREFIX . "users tu ON (tu.uid=pm.toid)
		WHERE $wsql AND pm.uid='" . $this->user->uid . "'
		ORDER BY pm.folder ASC, pm.dateline DESC
	");
        $numpms = $this->db->num_rows($query);
        if (!$numpms) {
            $this->bb->error($this->lang->error_nopmsarchive);
        }

        $this->bb->input['exporttype'] = $this->bb->getInput('exporttype', '');

        $pmsdownload = $ids = '';
        while ($message = $this->db->fetch_array($query)) {
            if ($message['folder'] == 2 || $message['folder'] == 3) { // Sent Items or Drafts Folder Check
                if ($message['toid']) {
                    $tofromuid = $message['toid'];
                    if ($this->bb->input['exporttype'] == 'txt') {
                        $tofromusername = $message['tousername'];
                    } else {
                        $tofromusername = $this->user->build_profile_link($message['tousername'], $tofromuid);
                    }
                } else {
                    $tofromusername = $this->lang->not_sent;
                }
                $tofrom = $this->lang->to;
            } else {
                $tofromuid = $message['fromid'];
                if ($this->bb->input['exporttype'] == 'txt') {
                    $tofromusername = $message['fromusername'];
                } else {
                    $tofromusername = $this->user->build_profile_link($message['fromusername'], $tofromuid);
                }

                if ($tofromuid == 0) {
                    $tofromusername = $this->lang->mybb_engine;
                }
                $tofrom = $this->lang->from;
            }

            if ($tofromuid == 0) {
                $message['fromusername'] = $this->lang->mybb_engine;
            }

            if (!$message['toid'] && $message['folder'] == 3) {
                $message['tousername'] = $this->lang->not_sent;
            }

            $message['subject'] = $this->parser->parse_badwords($message['subject']);
            if ($message['folder'] != '3') {
                $senddate = $this->time->formatDate($this->bb->settings['dateformat'], $message['dateline']);
                $sendtime = $this->time->formatDate($this->bb->settings['timeformat'], $message['dateline']);
                $senddate .= " {$this->lang->at} $sendtime";
            } else {
                $senddate = $this->lang->not_sent;
            }

            if ($this->bb->input['exporttype'] == 'html') {
                $parser_options = [
                    'allow_html' => $this->bb->settings['pmsallowhtml'],
                    'allow_mycode' => $this->bb->settings['pmsallowmycode'],
                    'allow_smilies' => 0,
                    'allow_imgcode' => $this->bb->settings['pmsallowimgcode'],
                    'allow_videocode' => $this->bb->settings['pmsallowvideocode'],
                    'me_username' => $this->user->username,
                    'filter_badwords' => 1
                ];

                $message['message'] = $this->parser->parse_message($message['message'], $parser_options);
                $message['subject'] = htmlspecialchars_uni($message['subject']);
            }

            if ($this->bb->input['exporttype'] == 'txt' || $this->bb->input['exporttype'] == 'csv') {
                $message['message'] = str_replace("\r\n", "\n", $message['message']);
                $message['message'] = str_replace("\n", "\r\n", $message['message']);
            }

            if ($this->bb->input['exporttype'] == 'csv') {
                $message['message'] = addslashes($message['message']);
                $message['subject'] = addslashes($message['subject']);
                $message['tousername'] = addslashes($message['tousername']);
                $message['fromusername'] = addslashes($message['fromusername']);
            }

            if (empty($donefolder[$message['folder']])) {
                reset($foldersexploded);
                foreach ($foldersexploded as $key => $val) {
                    $folderinfo = explode('**', $val, 2);
                    if ($folderinfo[0] == $message['folder']) {
                        $foldername = $folderinfo[1];
                        if ($this->bb->input['exporttype'] != 'csv') {
                            if ($this->bb->input['exporttype'] != 'html') {
                                $this->bb->input['exporttype'] == 'txt';
                            }
                            eval("\$pmsdownload .= \"" . $this->templates->get('private_archive_' .
                                    $this->bb->input['exporttype'] . '_folderhead', 1, 0) . "\";");
                        } else {
                            $foldername = addslashes($folderinfo[1]);
                        }
                        $donefolder[$message['folder']] = 1;
                    }
                }
            }

            eval("\$pmsdownload .= \"" . $this->templates->get('private_archive_' .
                    $this->bb->input['exporttype'] . '_message', 1, 0) . "\";");
            $ids .= ",'{$message['pmid']}'";
        }

        if ($this->bb->input['exporttype'] == 'html') {
            // Gather global stylesheet for HTML
            $query = $this->db->simple_select('themestylesheets', 'stylesheet', "sid = '1'", ['limit' => 1]);
            $css = $this->db->fetch_field($query, 'stylesheet');
        }

        $this->plugins->runHooks('private_do_export_end');
        $archived = '';
        eval("\$archived = \"" . $this->templates->get('private_archive_' .
                $this->bb->input['exporttype'], 1, 0) . "\";");
        if ($this->bb->getInput('deletepms', 0) == 1) { // delete the archived pms
            $this->db->delete_query('privatemessages', "pmid IN ('0'$ids)");
            // Update PM count
            $this->user->update_pm_count();
        }

        if ($this->bb->input['exporttype'] == 'html') {
            $filename = 'pm-archive.html';
            $contenttype = 'text/html';
        } elseif ($this->bb->input['exporttype'] == 'csv') {
            $filename = 'pm-archive.csv';
            $contenttype = 'application/octet-stream';
        } else {
            $filename = 'pm-archive.txt';
            $contenttype = 'text/plain';
        }

        $archived = str_replace("\\\'", "'", $archived);
        header('Content-disposition: filename=' . $filename);
        header('Content-type: ' . $contenttype);

        if ($this->bb->input['exporttype'] == 'html') {
            $this->bb->output_page();
            echo $archived;
            return;
        } else {
            echo "\xEF\xBB\xBF"; // UTF-8 BOM
            echo $archived;
            exit;
        }
    }
}
