<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Folders extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->nav_folders);

        $this->plugins->runHooks('private_folders_start');

        $folderlist = [];
        $foldersexploded = explode('$%%$', $this->user->pmfolders);
        foreach ($foldersexploded as $key => $folders) {
            $folderinfo = explode('**', $folders, 2);
            //$foldername = $folderinfo[1];
            //$fid = $folderinfo[0];
            //$foldername = $this->user->get_pm_folder_name($folderinfo[0], $folderinfo[1]);

            $foldername2 = '';
            if ($folderinfo[0] == '1' || $folderinfo[0] == '2' || $folderinfo[0] == '3' || $folderinfo[0] == '4') {
                $foldername2 = $this->user->get_pm_folder_name($folderinfo[0]);
                //ev al("\$folderlist .= \"".$this->templates->get("private_folders_folder_unremovable")."\";");
                unset($name);
            } else {
                //ev al("\$folderlist .= \"".$this->templates->get("private_folders_folder")."\";");
            }
            $folderlist[] = [
                'fid' => $folderinfo[0],
                'foldername' => $this->user->get_pm_folder_name($folderinfo[0], $folderinfo[1]),
                'foldername2' => $foldername2
            ];
        }
        $this->view->offsetSet('folderlist', $folderlist);

        $newfolders = [];
        for ($i = 1; $i <= 5; ++$i) {
//      $fid = "new$i";
//      $foldername = '';
            $newfolders[] = [
                'fid' => 'new' . $i,
                'foldername' => ''
            ];
            //ev al('\$newfolders .= \''.$this->templates->get('private_folders_folder').'\';');
        }
        $this->view->offsetSet('newfolders', $newfolders);

        $this->plugins->runHooks('private_folders_end');

        //ev al('\$folders = \''.$this->templates->get('private_folders').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/folders.html.twig');
    }

    public function doFolders(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_do_folders_start');

        $highestid = 2;
        $folders = '';
        $donefolders = [];
        $this->bb->input['folder'] = $this->bb->getInput('folder', []);
        foreach ($this->bb->input['folder'] as $key => $val) {
            if (empty($donefolders[$val])) {// Probably was a check for duplicate folder names,
                //but doesn't seem to be used now
                if (my_substr($key, 0, 3) == 'new') { // Create a new folder
                    ++$highestid;
                    $fid = (int)$highestid;
                } else // Editing an existing folder
                {
                    if ($key > $highestid) {
                        $highestid = $key;
                    }

                    $fid = (int)$key;
                    // Use default language strings if empty or value is language string
                    switch ($fid) {
                        case 1:
                            if ($val == $this->lang->folder_inbox || trim($val) == '') {
                                $val = '';
                            }
                            break;
                        case 2:
                            if ($val == $this->lang->folder_sent_items || trim($val) == '') {
                                $val = '';
                            }
                            break;
                        case 3:
                            if ($val == $this->lang->folder_drafts || trim($val) == '') {
                                $val = '';
                            }
                            break;
                        case 4:
                            if ($val == $this->lang->folder_trash || trim($val) == '') {
                                $val = '';
                            }
                            break;
                    }
                }

                if ($val != '' && trim($val) == '' && !($key >= 1 && $key <= 4)) {
                    // If the name only contains whitespace and it's not a default folder, print an error
                    $this->bb->error($this->lang->error_emptypmfoldername);
                }

                if ($val != '' || ($key >= 1 && $key <= 4)) {
                    // If there is a name or if this is a default folder, save it
                    $foldername = $this->db->escape_string(htmlspecialchars_uni($val));

                    if (my_strpos($foldername, '$%%$') === false) {
                        if ($folders != '') {
                            $folders .= '$%%$';
                        }
                        $folders .= "$fid**$foldername";
                    } else {
                        $this->bb->error($this->lang->error_invalidpmfoldername);
                    }
                } else {
                    // Delete PMs from the folder
                    $this->db->delete_query('privatemessages', "folder='$fid' AND uid='" . $this->user->uid . "'");
                }
            }
        }

        $sql_array = [
            'pmfolders' => $folders
        ];
        $this->db->update_query('users', $sql_array, "uid='" . $this->user->uid . "'");

        // Update PM count
        $this->user->update_pm_count();

        $this->plugins->runHooks('private_do_folders_end');

        $this->bb->redirect($this->bb->settings['bburl'] . '/private/folders', $this->lang->redirect_pmfoldersupdated);
    }
}
