<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        $this->plugins->runHooks('private_inbox');

//        $folder = $this->bb->input['fid'];
        $folder = $this->bb->getInput('fid', 0);
//        if (!$this->bb->input['fid'] || !array_key_exists($this->bb->input['fid'], $this->foldernames)) {
//            $this->bb->input['fid'] = 1;
//        }
        if (!array_key_exists($folder, $this->foldernames)) {
            $folder = 1;
        }


        $foldername = $this->foldernames[$folder];
        $this->view->offsetSet('folder', $folder);
        $this->view->offsetSet('foldername', $foldername);

        if ($folder == 2 || $folder == 3) { // Sent Items Folder
            $sender = $this->lang->sentto;
        } else {
            $sender = $this->lang->sender;
        }
        $this->view->offsetSet('sender', $sender);

        $this->bb->input['order'] = htmlspecialchars_uni($this->bb->getInput('order', ''));
        $ordersel = ['asc' => '', 'desc'];
        switch (my_strtolower($this->bb->input['order'])) {
            case 'asc':
                $sortordernow = 'asc';
                $ordersel['asc'] = 'selected="selected"';
                $oppsort = $this->lang->desc;
                $oppsortnext = 'desc';
                break;
            default:
                $sortordernow = 'desc';
                $ordersel['desc'] = 'selected="selected"';
                $oppsort = $this->lang->asc;
                $oppsortnext = 'asc';
                break;
        }

        // Sort by which field?
        $sortby = htmlspecialchars_uni($this->bb->getInput('sortby', ''));
        switch ($this->bb->getInput('sortby', '')) {
            case 'subject':
                $sortfield = 'subject';
                break;
            case 'username':
                $sortfield = 'username';
                break;
            default:
                $sortby = 'dateline';
                $sortfield = 'dateline';
                $this->bb->input['sortby'] = 'dateline';
                break;
        }
        $orderarrow = $sortsel = ['subject' => '', 'username' => '', 'dateline' => ''];
        $sortsel[$sortby] = 'selected="selected"';

        $orderarrow['$sortby'] = "<span class=\"smalltext\">[<a href=\"{$this->bb->settings['bburl']}/private?
            fid={$folder}&sortby={$sortby}&order={$oppsortnext}\">{$oppsort}</a>]</span>";
        //ev al("\$orderarrow['$sortby'] = \"".$this->templates->get("private_orderarrow")."\";");
        $this->view->offsetSet('orderarrow', $orderarrow);

        // Do Multi Pages
//        $query = $this->db->simple_select('privatemessages', 'COUNT(*) AS total',
//            "uid='" . $this->user->uid . "' AND folder='$folder'");
//        $pmscount = $this->db->fetch_array($query);
        $pmscount = \RunBB\Models\Privatemessage::where([
            ['uid', $this->user->uid],
            ['folder', $folder]
        ])->count();

        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        $perpage = $this->bb->settings['threadsperpage'];
        $page = $this->bb->getInput('page', 0);

        if ($page > 0) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $end = $start + $perpage;
        $lower = $start + 1;
        $upper = $end;

        if ($upper > $pmscount) {
            $upper = $pmscount;
        }

        if ($this->bb->input['order'] || ($sortby && $sortby != 'dateline')) {
            $page_url = "private.php?fid={$folder}&sortby={$sortby}&order={$this->bb->input['order']}";
        } else {
            $page_url = "private.php?fid={$folder}";
        }

        $multipage = $this->pagination->multipage($pmscount, $perpage, $page, $page_url);
        $this->view->offsetSet('multipage', $multipage);

        $messagelist = [];
        $icon_cache = $this->cache->read('posticons');

        // Cache users in multiple recipients for sent & drafts folder
        if ($folder == 2 || $folder == 3) {
            if ($sortfield == 'username') {
                $u = 'u.';
            } else {
                $u = 'pm.';
            }

            // Get all recipients into an array
            $cached_users = $get_users = [];
            $users_query = $this->db->query('
			SELECT pm.recipients
			FROM ' . TABLE_PREFIX . 'privatemessages pm
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=pm.toid)
			WHERE pm.folder='{$folder}' AND pm.uid='{$this->user->uid}'
			ORDER BY {$u}{$sortfield} {$this->bb->input['order']}
			LIMIT {$start}, {$perpage}
		");
            while ($row = $this->db->fetch_array($users_query)) {
                $recipients = my_unserialize($row['recipients']);
                if (!empty($recipients['to']) && count($recipients['to'])) {
                    $get_users = array_merge($get_users, $recipients['to']);
                }

                if (isset($recipients['bcc']) && is_array($recipients['bcc']) && count($recipients['bcc'])) {
                    $get_users = array_merge($get_users, $recipients['bcc']);
                }
            }

            $get_users = implode(',', array_unique($get_users));

            // Grab info
            if ($get_users) {
                $users_query = $this->db->simple_select(
                    'users',
                    'uid, username, usergroup, displaygroup',
                    "uid IN ({$get_users})"
                );
                while ($user = $this->db->fetch_array($users_query)) {
                    $cached_users[$user['uid']] = $user;
                }
            }
        }

        if ($folder == 2 || $folder == 3) {
            if ($sortfield == 'username') {
                $pm = 'tu.';
            } else {
                $pm = 'privatemessages.';
            }
        } else {
            if ($sortfield == 'username') {
                $pm = 'fu.';
            } else {
                $pm = 'privatemessages.';
            }
        }

//        $query = $this->db->query('
//		SELECT pm.*, fu.username AS fromusername, tu.username as tousername
//		FROM ' . TABLE_PREFIX . 'privatemessages pm
//		LEFT JOIN ' . TABLE_PREFIX . 'users fu ON (fu.uid=pm.fromid)
//		LEFT JOIN ' . TABLE_PREFIX . "users tu ON (tu.uid=pm.toid)
//		WHERE pm.folder='$folder' AND pm.uid='" . $this->user->uid . "'
//		ORDER BY {$pm}{$sortfield} {$sortordernow}
//		LIMIT $start, $perpage
//	");
        $pm = \RunBB\Models\Privatemessage::where([
            ['privatemessages.folder', '=', $folder],
            ['privatemessages.uid', '=', $this->user->uid]
        ])
            ->leftJoin('users as fu', 'fu.uid', '=', 'privatemessages.fromid')
            ->leftJoin('users as tu', 'tu.uid', '=', 'privatemessages.toid')
            ->orderBy($pm . $sortfield, $sortordernow)
            ->skip($start)->take($perpage)
            ->get(['privatemessages.*', 'fu.username AS fromusername', 'tu.username as tousername'])
            ->toArray();

//        if ($this->db->num_rows($query) > 0) {
        if (count($pm) > 0) {
//            while ($message = $this->db->fetch_array($query)) {
            foreach ($pm as $message) {
                $msgalt = $msgsuffix = $msgprefix = '';
                // Determine Folder Icon
                if ($message['status'] == 0) {
                    $msgfolder = 'new_pm.png';
                    $msgalt = $this->lang->new_pm;
                    $msgprefix = '<strong>';
                    $msgsuffix = '</strong>';
                } elseif ($message['status'] == 1) {
                    $msgfolder = 'old_pm.png';
                    $msgalt = $this->lang->old_pm;
                } elseif ($message['status'] == 3) {
                    $msgfolder = 're_pm.png';
                    $msgalt = $this->lang->reply_pm;
                } elseif ($message['status'] == 4) {
                    $msgfolder = 'fw_pm.png';
                    $msgalt = $this->lang->fwd_pm;
                }

                $tofromuid = 0;
                if ($folder == 2 || $folder == 3) { // Sent Items or Drafts Folder Check
                    $recipients = my_unserialize($message['recipients']);
                    $to_users = $bcc_users = '';
                    if ((isset($recipients['to']) && count($recipients['to']) > 1)
                        || (isset($recipients['to']) && (count($recipients['to']) == 1) &&
                            isset($recipients['bcc']) && count($recipients['bcc']) > 0)
                    ) {
                        foreach ($recipients['to'] as $uid) {
                            $profilelink = get_profile_link($uid);
                            $user = $cached_users[$uid];
                            $username = $this->user->format_name(
                                $user['username'],
                                $user['usergroup'],
                                $user['displaygroup']
                            );
                            if (!$user['username']) {
                                $username = $this->lang->na;
                            }
                            $to_users .= "<div class=\"popup_item_container\"><a href=\"{$profilelink}\" 
                            class=\"popup_item\">{$username}</a></div>";
                        }
                        if (isset($recipients['bcc']) && is_array($recipients['bcc']) && count($recipients['bcc'])) {
                            $bcc_users = "<div class=\"tcat\"><strong>{$this->lang->bcc}</strong></div>";
                            foreach ($recipients['bcc'] as $uid) {
                                $profilelink = get_profile_link($uid);
                                $user = $cached_users[$uid];
                                $username = $this->user->format_name(
                                    $user['username'],
                                    $user['usergroup'],
                                    $user['displaygroup']
                                );
                                if (!$user['username']) {
                                    $username = $this->lang->na;
                                }
                                $to_users .= "<div class=\"popup_item_container\"><a href=\"{$profilelink}\" 
                                class=\"popup_item\">{$username}</a></div>";
                            }
                        }
                        $tofromusername = "<a href=\"{$this->bb->settings['bburl']}/private/read?
                            pmid={$message['pmid']}\" id=\"private_message_{$message['pmid']}\">
                        {$this->lang->multiple_recipients}</a>
                        <div id=\"private_message_{$message['pmid']}_popup\" class=\"popup_menu\" 
                            style=\"display: none;\"><div class=\"tcat\"><strong>{$this->lang->to}</strong></div>
                            {$to_users}{$bcc_users}</div>
                        <script type=\"text/javascript\">
                        <!--
                        $(\"#private_message_{$message['pmid']}\").popupMenu();
                        // -->
                        </script>";
                    } elseif ($message['toid']) {
                        $tofromusername = $message['tousername'];
                        $tofromuid = $message['toid'];
                    } else {
                        $tofromusername = $this->lang->not_sent;
                    }
                } else {
                    $tofromusername = $message['fromusername'];
                    $tofromuid = $message['fromid'];
                    if ($tofromuid == 0) {
                        $tofromusername = $this->lang->mybb_engine;
                    }

                    if (!$tofromusername) {
                        $tofromuid = 0;
                        $tofromusername = $this->lang->na;
                    }
                }

                $tofromusername = $this->user->build_profile_link($tofromusername, $tofromuid);

                if ($this->bb->usergroup['candenypmreceipts'] == 1 &&
                    $message['receipt'] == '1' &&
                    $message['folder'] != '3' &&
                    $message['folder'] != '2'
                ) {
                    $denyreceipt = " <span class=\"smalltext\"><a href=\"{$this->bb->settings['bburl']}/private/read?
                    pmid={$message['pmid']}&denyreceipt=1\">{$this->lang->deny_receipt}</a></span>";
                } else {
                    $denyreceipt = '';
                }

                if ($message['icon'] > 0 && $icon_cache[$message['icon']]) {
                    $icon = $icon_cache[$message['icon']];
                    $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
                    $icon['path'] = htmlspecialchars_uni($icon['path']);
                    $icon['name'] = htmlspecialchars_uni($icon['name']);
                    $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" 
                        title=\"{$icon['name']}\" align=\"center\" valign=\"middle\" />";
                } else {
                    $icon = '&#009;';
                }

                if (!trim($message['subject'])) {
                    $message['subject'] = $this->lang->pm_no_subject;
                }

                $message['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($message['subject']));
                if ($message['folder'] != '3') {
                    $senddate = $this->time->formatDate('relative', $message['dateline']);
                } else {
                    $senddate = $this->lang->not_sent;
                }
                $this->plugins->runHooks('private_message');
                $messagelist[] = [
                    'msgfolder' => $msgfolder,
                    'msgalt' => $msgalt,
                    'icon' => $icon,
                    'msgprefix' => $msgprefix,
                    'pmid' => $message['pmid'],
                    'subject' => $message['subject'],
                    'msgsuffix' => $msgsuffix,
                    'denyreceipt' => $denyreceipt,
                    'tofromusername' => $tofromusername,
                    'senddate' => $senddate,
                    'alt_trow' => alt_trow()
                ];
            }
        }
        $this->view->offsetSet('messagelist', $messagelist);

        $pmspacebar = [];
        if ($this->bb->usergroup['pmquota'] != '0' && $this->bb->usergroup['cancp'] != 1) {
//            $query = $this->db->simple_select('privatemessages', 'COUNT(*) AS total', "uid='" .$this->user->uid."'");
//            $pmscount = $this->db->fetch_array($query);
            $pmscount = \RunBB\Models\Privatemessage::where('uid', '=', $this->user->uid)->count();
            if ($pmscount == 0) {
                $spaceused = 0;
            } else {
                $spaceused = $pmscount / $this->bb->usergroup['pmquota'] * 100;
            }
            $spaceused2 = 100 - $spaceused;
            $belowhalf = $overhalf = 0;//'';
            if ($spaceused <= '50') {
                $spaceused_severity = 'low';
                $belowhalf = round($spaceused, 0);// . '%';
                if ((int)$belowhalf > 100) {
                    $belowhalf = 100;//'100%';
                }
            } else {
                if ($spaceused <= '75') {
                    $spaceused_severity = 'medium';
                } else {
                    $spaceused_severity = 'high';
                }

                $overhalf = round($spaceused, 0) . '%';
                if ((int)$overhalf > 100) {
                    $overhalf = '100%';
                }
            }

            if ($spaceused > 100) {
                $spaceused = 100;
                $spaceused2 = 0;
            }
            $pmspacebar = [
//                'spaceused_severity' => $spaceused_severity,
//                'spaceused' => $spaceused,
//                'overhalf' => $overhalf,
//                'spaceused2' => $spaceused2,
                'belowhalf' => $belowhalf,
            ];
        }
        $this->view->offsetSet('pmspacebar', $pmspacebar);

        $this->bb->showLimitWarning = false;
        if ($this->bb->usergroup['pmquota'] != '0' && $pmscount >= $this->bb->usergroup['pmquota'] &&
            $this->bb->usergroup['cancp'] != 1
        ) {
            $this->bb->showLimitWarning = true;
        }

        $this->plugins->runHooks('private_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/index.html.twig');
    }
}
