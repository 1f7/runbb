<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Read extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        $this->plugins->runHooks('private_read');

        $this->bb->pmid = $this->bb->getInput('pmid', 0);

//        $query = $this->db->query('
//		SELECT pm.*, u.*, f.*
//		FROM ' . TABLE_PREFIX . 'privatemessages pm
//		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=pm.fromid)
//		LEFT JOIN ' . TABLE_PREFIX . "userfields f ON (f.ufid=u.uid)
//		WHERE pm.pmid='{$this->bb->pmid}' AND pm.uid='" . $this->user->uid . "'
//	");
//        $pm = $this->db->fetch_array($query);
        $p = \RunBB\Models\Privatemessage::where([
            ['privatemessages.pmid', '=', $this->bb->pmid],
            ['privatemessages.uid', '=', $this->user->uid]
        ])
            ->leftJoin('users', 'users.uid', '=', 'privatemessages.fromid')
            ->leftJoin('userfields', 'userfields.ufid', '=', 'users.uid')
            ->get([
                'privatemessages.*',
                'users.*',
                'userfields.*'
            ])
            ->toArray();
        $pm = $p[0];

        if (!$pm) {
            $this->bb->error($this->lang->error_invalidpm);
        }

        if ($pm['folder'] == 3) {
            return $response->withRedirect($this->bb->settings['bburl'].'/private/send?pmid='.$pm['pmid']);
        }
        // If we've gotten a PM, attach the group info
        $data_key = [
            'title' => 'grouptitle',
            'usertitle' => 'groupusertitle',
            'stars' => 'groupstars',
            'starimage' => 'groupstarimage',
            'image' => 'groupimage',
            'namestyle' => 'namestyle'
        ];

        foreach ($data_key as $field => $key) {
            $pm[$key] = $this->bb->groupscache[$pm['usergroup']][$field];
        }

        if ($pm['receipt'] == 1) {
            if ($this->bb->usergroup['candenypmreceipts'] == 1 && $this->bb->getInput('denyreceipt', 0) == 1) {
                $receiptadd = 0;
            } else {
                $receiptadd = 2;
            }
        }

        $this->bb->showActionTime = false;
        if ($pm['status'] == 0) {
            $time = TIME_NOW;
            $updatearray = [
                'status' => 1,
                'readtime' => $time
            ];

            if (isset($receiptadd)) {
                $updatearray['receipt'] = $receiptadd;
            }

//            $this->db->update_query('privatemessages', $updatearray, "pmid='{$this->bb->pmid}'");
            \RunBB\Models\Privatemessage::where('pmid', '=', $this->bb->pmid)
                ->update($updatearray);

            // Update the unread count - it has now changed.
            $this->user->update_pm_count($this->user->uid, 6);

            // Update PM notice value if this is our last unread PM
            if ($this->user->unreadpms - 1 <= 0 && $this->user->pmnotice == 2) {
                $updated_user = [
                    'pmnotice' => 1
                ];
//                $this->db->update_query('users', $updated_user, "uid='{$this->user->uid}'");
                \RunBB\Models\User::where('uid', '=', $this->user->uid)
                    ->update($updated_user);
            }
        } // Replied PM?
        elseif ($pm['status'] == 3 && $pm['statustime']) {
            $this->bb->showActionTime = false;
            $reply_string = $this->lang->you_replied_on;
            $reply_date = $this->time->formatDate('relative', $pm['statustime']);

            if ((TIME_NOW - $pm['statustime']) < 3600) {
                // Relative string for the first hour
                $reply_string = $this->lang->you_replied;
            }

            $this->view->offsetSet('actioned_on', $this->lang->sprintf($reply_string, $reply_date));
        } elseif ($pm['status'] == 4 && $pm['statustime']) {
            $this->bb->showActionTime = false;
            $forward_string = $this->lang->you_forwarded_on;
            $forward_date = $this->time->formatDate('relative', $pm['statustime']);

            if ((TIME_NOW - $pm['statustime']) < 3600) {
                $forward_string = $this->lang->you_forwarded;
            }

            $this->view->offsetSet('actioned_on', $this->lang->sprintf($forward_string, $forward_date));
        }

        $pm['userusername'] = $pm['username'];
        $pm['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($pm['subject']));

        if ($pm['fromid'] == 0) {
            $pm['username'] = $this->lang->mybb_engine;
        }

        if (!$pm['username']) {
            $pm['username'] = $this->lang->na;
        }

        // Fetch the recipients for this message
        $pm['recipients'] = my_unserialize($pm['recipients']);

        if (isset($pm['recipients']['to'])) {
            $uid_sql = $pm['recipients']['to'];//implode(',', $pm['recipients']['to']);
        } else {
            $uid_sql = [$pm['toid']];
            $pm['recipients']['to'] = [$pm['toid']];
        }

        $show_bcc = 0;

        // If we have any BCC recipients and this user is an Administrator, add them on to the query
        if (isset($pm['recipients']['bcc']) && count($pm['recipients']['bcc']) > 0 &&
            $this->bb->usergroup['cancp'] == 1) {
            $show_bcc = 1;
//            $uid_sql .= ',' . implode(',', $pm['recipients']['bcc']);
            $uid_sql = array_merge($uid_sql, $pm['recipients']['bcc']);
        }

        // Fetch recipient names from the database
        $bcc_recipients = $to_recipients = $bcc_form_val = [];
//        $query = $this->db->simple_select('users', 'uid, username', "uid IN ({$uid_sql})");
        $n = \RunBB\Models\User::whereIn('uid', $uid_sql)// FIXME check multiple BCC !!!
            ->get(['uid', 'username'])
            ->toArray();

//        while ($recipient = $this->db->fetch_array($query)) {
        foreach ($n as $recipient) {
            // User is a BCC recipient
            if ($show_bcc && in_array($recipient['uid'], $pm['recipients']['bcc'])) {
                $bcc_recipients[] = $this->user->build_profile_link($recipient['username'], $recipient['uid']);
                $bcc_form_val[] = $recipient['username'];
            } // User is a normal recipient
            elseif (in_array($recipient['uid'], $pm['recipients']['to'])) {
                $to_recipients[] = $this->user->build_profile_link($recipient['username'], $recipient['uid']);
            }
        }

        $bcc = '';
        if (count($bcc_recipients) > 0) {
            $bcc_recipients = implode(', ', $bcc_recipients);
            $bcc_form_val = implode(',', $bcc_form_val);
            $bcc = "<br />{$this->lang->bcc} {$bcc_recipients}";
        } else {
            $bcc_form_val = '';
        }

        $this->bb->replyall = false;
        if (count($to_recipients) > 1) {
            $this->bb->replyall = true;
        }

        if (count($to_recipients) > 0) {
            $to_recipients = implode($this->lang->comma, $to_recipients);
        } else {
            $to_recipients = $this->lang->nobody;
        }

        $pm['subject_extra'] = "<br /> {$this->lang->to} {$to_recipients} {$bcc}";

        $this->bb->add_breadcrumb($pm['subject']);

        $this->view->offsetSet('message', $this->post->build_postbit($pm, 2));

        // Decide whether or not to show quick reply.
        $this->bb->showQuickReply = false;
        if ($this->bb->settings['pmquickreply'] != 0 &&
            $this->user->showquickreply != 0 &&
            $this->bb->usergroup['cansendpms'] != 0 &&
            $pm['fromid'] != 0 &&
            $pm['folder'] != 3
        ) {
            $this->bb->showQuickReply = true;
            //$trow = alt_trow();

            $optionschecked = ['savecopy' => 'checked="checked"'];
            if (!empty($this->user->signature)) {
                $optionschecked['signature'] = 'checked="checked"';
            }
            if ($this->bb->usergroup['cantrackpms'] == 1) {
                $optionschecked['readreceipt'] = 'checked="checked"';
            }

            $quoted_message = [
                'message' => htmlspecialchars_uni($this->parser->parse_badwords($pm['message'])),
                'username' => $pm['username'],
                'quote_is_pm' => true
            ];
            $quoted_message = $this->post->parse_quoted_message($quoted_message);

            if ($this->bb->settings['maxpmquotedepth'] != '0') {
                $quoted_message = $this->post->remove_message_quotes(
                    $quoted_message,
                    $this->bb->settings['maxpmquotedepth']
                );
            }
            $this->view->offsetSet('quoted_message', $quoted_message);

            $subject = preg_replace("#(FW|RE):( *)#is", '', $pm['subject']);

            if ($this->user->uid == $pm['fromid']) {
                $to = htmlspecialchars_uni($this->user->username);
            } else {
//                $query = $this->db->simple_select('users', 'username', "uid='{$pm['fromid']}'");
//                $to = htmlspecialchars_uni($this->db->fetch_field($query, 'username'));
                $t = \RunBB\Models\User::where('uid', '=', $pm['fromid'])
                    ->first(['username']);
                $to = $t->username;
            }

            if ($this->bb->usergroup['cantrackpms']) {
                $this->lang->options_read_receipt = $this->lang->quickreply_read_receipt;
            }
        }
        $data = [
            'subject' => $subject,
            'pm' => $pm,
            'to' => $to,
            'bcc_form_val' => $bcc_form_val,
            'pmid' => $this->bb->pmid,
            'optionschecked' => $optionschecked,
            'trow' => alt_trow()
        ];
        $this->view->offsetSet('data', $data);

        $this->plugins->runHooks('private_read_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/read.html.twig');
    }
}
