<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Results extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->nav_results);

        $sid = $this->bb->getInput('sid', '');
//        $query = $this->db->simple_select('searchlog', '*',
//            "sid='".$this->db->escape_string($sid)."' AND uid='{$this->user->uid}'");
//        $search = $this->db->fetch_array($query);

        $s = \RunBB\Models\Searchlog::where([
            ['sid', '=', $sid],
            ['uid', '=', $this->user->uid]
        ])
            ->take(1)
            ->get()
            ->toArray();
        $search = $s[0];

        if (!$search) {
            $this->bb->error($this->lang->error_invalidsearch);
        }

        $this->plugins->runHooks('private_results_start');

        // Decide on our sorting fields and sorting order.
        $order = my_strtolower($this->bb->getInput('order', ''));
        $sortby = my_strtolower($this->bb->getInput('sortby', ''));

        $sortby_accepted = ['subject', 'username', 'dateline'];

        if (in_array($sortby, $sortby_accepted)) {
            $query_sortby = $sortby;

            if ($query_sortby == 'username') {
                $query_sortby = 'fromusername';
            }
        } else {
            $sortby = $query_sortby = 'dateline';
        }

        if ($order != 'asc') {
            $order = 'desc';
        }

        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        // Work out pagination, which page we're at, as well as the limits.
        $perpage = $this->bb->settings['threadsperpage'];
        $page = $this->bb->getInput('page', 0);
        if ($page > 0) {
            $start = ($page-1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }
        $end = $start + $perpage;
        $lower = $start+1;
        $upper = $end;

        // Work out if we have terms to highlight
        $highlight = '';
        if ($search['keywords']) {
            $highlight = '&amp;highlight='.urlencode($search['keywords']);
        }

        // Do Multi Pages
//        $query = $this->db->simple_select('privatemessages',
//            'COUNT(*) AS total',
//            'pmid IN('.$this->db->escape_string($search['querycache']).')');
//        $pmscount = $this->db->fetch_array($query);

        $pmscount = \RunBB\Models\Privatemessage::whereIn('pmid', [$search['querycache']])
            ->count();

        if ($upper > $pmscount) {
            $upper = $pmscount;
        }
//        $multipage = (new \RunBB\Helpers\Pagination($this))->multipage($pmscount['total'], $perpage, $page,
        $multipage = $this->pagination->multipage(
            $pmscount,
            $perpage,
            $page,
            $this->bb->settings['bburl'].'/private/results?sid='.
            htmlspecialchars_uni($this->bb->getInput('sid', ''))."&amp;sortby={$sortby}&amp;order={$order}"
        );
        $this->view->offsetSet('multipage', $multipage);


        $messagelist = [];
        $icon_cache = $this->cache->read('posticons');

        // Cache users in multiple recipients for sent & drafts folder
        // Get all recipients into an array
        $cached_users = $get_users = [];
//        $users_query = $this->db->simple_select('privatemessages', 'recipients',
//            'pmid IN('.$this->db->escape_string($search['querycache']).')',
//            array('limit_start' => $start, 'limit' => $perpage, 'order_by' => $query_sortby, 'order_dir' => $order));
        $uq = \RunBB\Models\Privatemessage::whereIn('pmid', [$search['querycache']])
            ->skip($start)->take($perpage)
            ->orderBy($query_sortby, $order)
            ->get(['recipients'])
            ->toArray();

//        while($row = $this->db->fetch_array($users_query))
        foreach ($uq as $row) {
            $recipients = my_unserialize($row['recipients']);
            if (is_array($recipients['to']) && count($recipients['to'])) {
                $get_users = array_merge($get_users, $recipients['to']);
            }

            if (isset($recipients['bcc']) && is_array($recipients['bcc']) && count($recipients['bcc'])) {
                $get_users = array_merge($get_users, $recipients['bcc']);
            }
        }

        $get_users = implode(',', array_unique($get_users));

        // Grab info
        if ($get_users) {
//            $users_query = $this->db->simple_select('users', 'uid, username, usergroup, displaygroup',
//              "uid IN ({$get_users})");
            $u = \RunBB\Models\User::whereIn('uid', [$get_users])
                ->get([
                    'uid',
                    'username',
                    'usergroup',
                    'displaygroup'
                ])
                ->toArray();//FIXME check with multiple uid's

//            while($user = $this->db->fetch_array($users_query))
            foreach ($u as $user) {
                $cached_users[$user['uid']] = $user;
            }
        }

//        $query = $this->db->query('
//		SELECT pm.*, fu.username AS fromusername, tu.username as tousername
//		FROM '.TABLE_PREFIX.'privatemessages pm
//		LEFT JOIN '.TABLE_PREFIX.'users fu ON (fu.uid=pm.fromid)
//		LEFT JOIN '.TABLE_PREFIX.'users tu ON (tu.uid=pm.toid)
//		WHERE pm.pmid IN('.$this->db->escape_string($search['querycache']).") AND pm.uid='{$this->user->uid}'
//		ORDER BY pm.{$query_sortby} {$order}
//		LIMIT {$start}, {$perpage}
//	");
        $m = \RunBB\Models\Privatemessage::whereIn('privatemessages.pmid', [$search['querycache']])
            ->where('privatemessages.uid', '=', $this->user->uid)
            ->leftJoin('users as fu', 'fu.uid', '=', 'privatemessages.fromid')
            ->leftJoin('users as tu', 'tu.uid', '=', 'privatemessages.toid')
            ->orderBy('privatemessages.'.$query_sortby, $order)
            ->skip($start)->take($perpage)
            ->get([
                'privatemessages.*',
                'fu.username AS fromusername',
                'tu.username as tousername'
            ])
            ->toArray();

//        while($message = $this->db->fetch_array($query))
        foreach ($m as $message) {
            $msgalt = $msgsuffix = $msgprefix = '';

            // Determine Folder Icon
            if ($message['status'] == 0) {
                $msgfolder = 'new_pm.png';
                $msgalt = $this->lang->new_pm;
                $msgprefix = '<strong>';
                $msgsuffix = '</strong>';
            } elseif ($message['status'] == 1) {
                $msgfolder = 'old_pm.png';
                $msgalt = $this->lang->old_pm;
            } elseif ($message['status'] == 3) {
                $msgfolder = 're_pm.png';
                $msgalt = $this->lang->reply_pm;
            } elseif ($message['status'] == 4) {
                $msgfolder = 'fw_pm.png';
                $msgalt = $this->lang->fwd_pm;
            }

            $folder = $message['folder'];

            $tofromuid = 0;
            if ($folder == 2 || $folder == 3) {
                // Sent Items or Drafts Folder Check
                $recipients = my_unserialize($message['recipients']);
                $to_users = $bcc_users = '';
                if (count($recipients['to']) > 1 ||
                    (count($recipients['to']) == 1 && isset($recipients['bcc']) && count($recipients['bcc']) > 0)
                ) {
                    foreach ($recipients['to'] as $uid) {
                        $profilelink = get_profile_link($uid);
                        $user = $cached_users[$uid];
                        $username = $this->user->format_name(
                            $user['username'],
                            $user['usergroup'],
                            $user['displaygroup']
                        );
                        $to_users .= "<div class=\"popup_item_container\"><a href=\"{$profilelink}\" 
                        class=\"popup_item\">{$username}</a></div>";
                    }
                    if (isset($recipients['bcc']) && is_array($recipients['bcc']) && count($recipients['bcc'])) {
                        $bcc_users = "<div class=\"tcat\"><strong>{$this->lang->bcc}</strong></div>";
                        foreach ($recipients['bcc'] as $uid) {
                            $profilelink = get_profile_link($uid);
                            $user = $cached_users[$uid];
                            $username = $this->user->format_name(
                                $user['username'],
                                $user['usergroup'],
                                $user['displaygroup']
                            );
                            $to_users .= "<div class=\"popup_item_container\"><a href=\"{$profilelink}\" 
                            class=\"popup_item\">{$username}</a></div>";
                        }
                    }
                    $tofromusername = "<a href=\"{$this->bb->settings['bburl']}/private/read?pmid={$message['pmid']}\" 
                        id=\"private_message_{$message['pmid']}\">{$this->lang->multiple_recipients}</a>
                        <div id=\"private_message_{$message['pmid']}_popup\" class=\"popup_menu\" 
                            style=\"display: none;\">
                            <div class=\"tcat\"><strong>{$this->lang->to}</strong></div>
                            {$to_users}{$bcc_users}
                        </div>
                        <script type=\"text/javascript\">
                        <!--
                        $(\"#private_message_{$message['pmid']}\").popupMenu();
                        // -->
                        </script>";
                } elseif ($message['toid']) {
                    $tofromusername = $message['tousername'];
                    $tofromuid = $message['toid'];
                } else {
                    $tofromusername = $this->lang->not_sent;
                }
            } else {
                $tofromusername = $message['fromusername'];
                $tofromuid = $message['fromid'];
                if ($tofromuid == 0) {
                    $tofromusername = $this->lang->mybb_engine;
                }
            }

            $tofromusername = $this->user->build_profile_link($tofromusername, $tofromuid);

            $denyreceipt = '';

            if ($message['icon'] > 0 && $icon_cache[$message['icon']]) {
                $icon = $icon_cache[$message['icon']];
                $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
                $icon['path'] = htmlspecialchars_uni($icon['path']);
                $icon['name'] = htmlspecialchars_uni($icon['name']);
                $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" 
                    title=\"{$icon['name']}\" align=\"center\" valign=\"middle\" />";
            } else {
                $icon = '&#009;';
            }

            if (!trim($message['subject'])) {
                $message['subject'] = $this->lang->pm_no_subject;
            }

            $message['subject'] = $this->parser->parse_badwords($message['subject']);

            if (my_strlen($message['subject']) > 50) {
                $message['subject'] = htmlspecialchars_uni(my_substr($message['subject'], 0, 50).'...');
            } else {
                $message['subject'] = htmlspecialchars_uni($message['subject']);
            }

            if ($message['folder'] != '3') {
                $senddate = $this->time->formatDate('relative', $message['dateline']);
            } else {
                $senddate = $this->lang->not_sent;
            }

            $foldername = $this->foldernames[$message['folder']];

            // What we do here is parse the post using our post parser, then strip the tags from it
            $parser_options = [
                'allow_html' => 0,
                'allow_mycode' => 1,
                'allow_smilies' => 0,
                'allow_imgcode' => 0,
                'filter_badwords' => 1
            ];
            $message['message'] = strip_tags($this->parser->parse_message($message['message'], $parser_options));
            if (my_strlen($message['message']) > 200) {
                $message['message'] = my_substr($message['message'], 0, 200).'...';
            }
            $messagelist[]=[
                'msgfolder' => $msgfolder,
                'msgalt' => $msgalt,
                'icon' => $icon,
                'msgprefix' => $msgprefix,
                'message' => $message,
                'msgsuffix' => $msgsuffix,
                'denyreceipt' => $denyreceipt,
                'tofromusername' => $tofromusername,
                'foldername' => $foldername,
                'senddate' => $senddate,
            ];
        }
        $this->view->offsetSet('messagelist', $messagelist);

        $this->plugins->runHooks('private_results_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/results.html.twig');
    }
}
