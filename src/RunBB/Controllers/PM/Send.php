<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Send extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        $this->bb->add_breadcrumb($this->lang->nav_send);

        if ($this->bb->usergroup['cansendpms'] == 0) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('private_send_start');

        $smilieinserter = $codebuttons = '';

        if ($this->bb->settings['bbcodeinserter'] != 0 &&
            $this->bb->settings['pmsallowmycode'] != 0 &&
            $this->user->showcodebuttons != 0
        ) {
            $this->bb->codebuttons = $this->editor->build_mycode_inserter(
                'message',
                $this->bb->settings['pmsallowsmilies']
            );
            if ($this->bb->settings['pmsallowsmilies'] != 0) {
                $this->bb->smilieinserter = $this->editor->build_clickable_smilies();
            }
        }
        $this->lang->post_icon = $this->lang->message_icon;

        $message = htmlspecialchars_uni($this->parser->parse_badwords($this->bb->getInput('message', '', true)));
        $subject = htmlspecialchars_uni($this->parser->parse_badwords($this->bb->getInput('subject', '', true)));
//        $this->view->offsetSet('message', $message);
//        $this->view->offsetSet('subject', $subject);

        $optionschecked = ['signature' => '', 'disablesmilies' => '', 'savecopy' => '', 'readreceipt' => ''];
        $to = $bcc = '';

        if (!empty($this->bb->input['preview']) || $this->send_errors) {
            $options = $this->bb->getInput('options', [''=>'']);

            if (isset($options['signature']) && $options['signature'] == 1) {
                $optionschecked['signature'] = 'checked="checked"';
            }
            if (isset($options['disablesmilies']) && $options['disablesmilies'] == 1) {
                $optionschecked['disablesmilies'] = 'checked="checked"';
            }
            if (isset($options['savecopy']) && $options['savecopy'] != 0) {
                $optionschecked['savecopy'] = 'checked="checked"';
            }
            if (isset($options['readreceipt']) && $options['readreceipt'] != 0) {
                $optionschecked['readreceipt'] = 'checked="checked"';
            }
            $to = htmlspecialchars_uni($this->bb->getInput('to', '', true));
            $bcc = htmlspecialchars_uni($this->bb->getInput('bcc', '', true));
        }

        $do = $this->bb->getInput('do', '');
        $this->bb->pmid = $this->bb->getInput('pmid', 0);
        $postbit = '';
        $posticon = $this->bb->getInput('subject', 0);
        // Preview
        if (!empty($this->bb->input['preview'])) {
            $options = $this->bb->getInput('options', [0]);
            $query = $this->db->query('
			SELECT u.username AS userusername, u.*, f.*
			FROM ' . TABLE_PREFIX . 'users u
			LEFT JOIN ' . TABLE_PREFIX . "userfields f ON (f.ufid=u.uid)
			WHERE u.uid='" . $this->user->uid . "'
		    ");

            $post = $this->db->fetch_array($query);

            $post['userusername'] = $this->user->username;
            $post['postusername'] = $this->user->username;
            $post['message'] = $message;//$this->bb->getInput('message', '', true);
            $post['subject'] = $subject;//htmlspecialchars_uni($this->bb->getInput('subject', '', true));
            $posticon = $post['icon'] = $this->bb->getInput('icon', 0);
            $post['ipaddress'] = $this->session->ipaddress;
            if (!isset($options['disablesmilies'])) {
                $options['disablesmilies'] = 0;
            }
            $post['smilieoff'] = $options['disablesmilies'];
            $post['dateline'] = TIME_NOW;

            if (!isset($options['signature'])) {
                $post['includesig'] = 0;
            } else {
                $post['includesig'] = 1;
            }

            // Merge usergroup data from the cache
            $data_key = [
                'title' => 'grouptitle',
                'usertitle' => 'groupusertitle',
                'stars' => 'groupstars',
                'starimage' => 'groupstarimage',
                'image' => 'groupimage',
                'namestyle' => 'namestyle',
                'usereputationsystem' => 'usereputationsystem'
            ];

            foreach ($data_key as $field => $key) {
                $post[$key] = $this->bb->groupscache[$post['usergroup']][$field];
            }
            $postbit = $this->post->build_postbit($post, 2);
        } elseif (!$this->send_errors) {
            // New PM, so load default settings
            if ($this->user->signature != '') {
                $optionschecked['signature'] = 'checked="checked"';
            }
            if ($this->bb->usergroup['cantrackpms'] == 1) {
                $optionschecked['readreceipt'] = 'checked="checked"';
            }
            $optionschecked['savecopy'] = 'checked="checked"';
        }
        $this->view->offsetSet('postbit', $postbit);

        // Draft, reply, forward
//        if ($this->bb->getInput('pmid', 0) && empty($this->bb->input['preview']) && !$this->send_errors) {
        if (empty($this->bb->input['preview']) && !$this->send_errors) {
            $query = $this->db->query('
			SELECT pm.*, u.username AS quotename
			FROM ' . TABLE_PREFIX . 'privatemessages pm
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=pm.fromid)
			WHERE pm.pmid='" . $this->bb->getInput('pmid', 0) . "' AND pm.uid='{$this->user->uid}'
		    ");

            $pm = $this->db->fetch_array($query);

            $message = htmlspecialchars_uni($this->parser->parse_badwords($pm['message']));
            $subject = htmlspecialchars_uni($this->parser->parse_badwords($pm['subject']));

            if ($pm['folder'] == '3') {
                // message saved in drafts
                $this->bb->input['uid'] = $pm['toid'];

                if ($pm['includesig'] == 1) {
                    $optionschecked['signature'] = 'checked="checked"';
                }
                if ($pm['smilieoff'] == 1) {
                    $optionschecked['disablesmilies'] = 'checked="checked"';
                }
                if ($pm['receipt']) {
                    $optionschecked['readreceipt'] = 'checked="checked"';
                }

                // Get list of recipients
                $recipients = my_unserialize($pm['recipients']);
                $comma = $recipientids = '';
                if (isset($recipients['to']) && is_array($recipients['to'])) {
                    foreach ($recipients['to'] as $recipient) {
                        $recipient_list['to'][] = $recipient;
                        $recipientids .= $comma . $recipient;
                        $comma = ',';
                    }
                }

                if (isset($recipients['bcc']) && is_array($recipients['bcc'])) {
                    foreach ($recipients['bcc'] as $recipient) {
                        $recipient_list['bcc'][] = $recipient;
                        $recipientids .= $comma . $recipient;
                        $comma = ',';
                    }
                }

                if (!empty($recipientids)) {
                    $query = $this->db->simple_select('users', 'uid, username', "uid IN ({$recipientids})");
                    while ($user = $this->db->fetch_array($query)) {
                        if (isset($recipients['bcc']) && is_array($recipients['bcc']) &&
                            in_array($user['uid'], $recipient_list['bcc'])) {
                            $bcc .= htmlspecialchars_uni($user['username']) . ', ';
                        } else {
                            $to .= htmlspecialchars_uni($user['username']) . ', ';
                        }
                    }
                }
            } else {
                // forward/reply
                $subject = preg_replace('#(FW|RE):( *)#is', '', $subject);
                $message = "[quote='{$pm['quotename']}']\n$message\n[/quote]";
                $message = preg_replace('#^/me (.*)$#im', "* " . $pm['quotename'] . " \\1", $message);

                if ($this->bb->settings['maxpmquotedepth'] != '0') {
                    $message = $this->post->remove_message_quotes($message, $this->bb->settings['maxpmquotedepth']);
                }

                if ($do == 'forward') {
                    $subject = "Fw: $subject";
                } elseif ($do == 'reply') {
                    $subject = "Re: $subject";
                    $uid = $pm['fromid'];
                    if ($this->user->uid == $uid) {
                        $to = $this->user->username;
                    } else {
                        $query = $this->db->simple_select('users', 'username', "uid='{$uid}'");
                        $to = $this->db->fetch_field($query, 'username');
                    }
                    $to = htmlspecialchars_uni($to);
                } elseif ($do == 'replyall') {
                    $subject = "Re: $subject";

                    // Get list of recipients
                    $recipients = my_unserialize($pm['recipients']);
                    $recipientids = $pm['fromid'];
                    if (isset($recipients['to']) && is_array($recipients['to'])) {
                        foreach ($recipients['to'] as $recipient) {
                            if ($recipient == $this->user->uid) {
                                continue;
                            }
                            $recipientids .= ',' . $recipient;
                        }
                    }
                    $comma = '';
                    $query = $this->db->simple_select('users', 'uid, username', "uid IN ({$recipientids})");
                    while ($user = $this->db->fetch_array($query)) {
                        $to .= $comma . htmlspecialchars_uni($user['username']);
                        $comma = $this->lang->comma;
                    }
                }
            }

            $posticon = $pm['icon'];
        }

        // New PM with recipient preset
        if ($this->bb->getInput('uid', 0) && empty($this->bb->input['preview'])) {
            $query = $this->db->simple_select('users', 'username', "uid='" . $this->bb->getInput('uid', 0) . "'");
            $to = htmlspecialchars_uni($this->db->fetch_field($query, 'username')) . ', ';
        }

        $max_recipients = '';
        if ($this->bb->usergroup['maxpmrecipients'] > 0) {
            $max_recipients = $this->lang->sprintf(
                $this->lang->max_recipients,
                $this->bb->usergroup['maxpmrecipients']
            );
        }
        $this->view->offsetSet('max_recipients', $max_recipients);

        if ($this->send_errors) {
            $to = htmlspecialchars_uni($this->bb->getInput('to', '', true));
            $bcc = htmlspecialchars_uni($this->bb->getInput('bcc', '', true));
        }


        $pmid = $this->bb->getInput('pmid', 0);
//        $do = $this->bb->getInput('do', '');
        if ($do !== 'forward' && $do !== 'reply' && $do !== 'replyall') {
            $do = '';
        }
        $this->view->offsetSet('optionschecked', $optionschecked);

        $this->view->offsetSet('to', $to);
        $this->view->offsetSet('bcc', $bcc);
        $this->view->offsetSet('do', $do);
        $this->view->offsetSet('pmid', $pmid);
        $this->view->offsetSet('send_errors', $this->send_errors);

        $this->bb->input['icon'] = $posticon;
        $this->view->offsetSet('posticons', $this->post->get_post_icons());

        $this->view->offsetSet('message', $message);
        $this->view->offsetSet('subject', $subject);

        $this->plugins->runHooks('private_send_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/send.html.twig');
    }


    public function doSend(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['cansendpms'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_send_do_send');

        // Attempt to see if this PM is a duplicate or not
        $to = array_map('trim', explode(',', $this->bb->getInput('to', '', true)));
        $to_escaped = implode("','", array_map([$this->db, 'escape_string'], array_map('my_strtolower', $to)));
        $time_cutoff = TIME_NOW - (5 * 60 * 60);
        $query = $this->db->query('
		SELECT pm.pmid
		FROM ' . TABLE_PREFIX . 'privatemessages pm
		LEFT JOIN ' . TABLE_PREFIX . "users u ON(u.uid=pm.toid)
		WHERE LOWER(u.username) IN ('{$to_escaped}') 
		  AND pm.dateline > {$time_cutoff} 
		  AND pm.fromid='{$this->user->uid}' 
		  AND pm.subject='" . $this->db->escape_string($this->bb->getInput('subject', '', true)) . "' 
		  AND pm.message='" . $this->db->escape_string($this->bb->getInput('message', '', true)) . "' 
		  AND pm.folder!='3'
		LIMIT 0, 1
	");
        $duplicate_check = $this->db->fetch_field($query, 'pmid');
        if ($duplicate_check) {
            $this->bb->error($this->lang->error_pm_already_submitted);
        }

        $pmhandler = new \RunBB\Handlers\DataHandlers\PMDataHandler($this->bb);

        $pm = [
            'subject' => $this->bb->getInput('subject', '', true),
            'message' => $this->bb->getInput('message', '', true),
            'icon' => $this->bb->getInput('icon', 0),
            'fromid' => $this->user->uid,
            'do' => $this->bb->getInput('do', ''),
            'pmid' => $this->bb->getInput('pmid', 0),
            'ipaddress' => $this->session->ipaddress
        ];

        // Split up any recipients we have
        $pm['to'] = $to;
        if (!empty($this->bb->input['bcc'])) {
            $pm['bcc'] = explode(',', $this->bb->getInput('bcc', '', true));
            $pm['bcc'] = array_map('trim', $pm['bcc']);
        }

        if (!$this->bb->usergroup['cantrackpms']) {
            $this->bb->input['options']['readreceipt'] = false;
        }
        $options = $this->bb->getInput('options', [''=>'']);

        $pm['options'] = [];
        if (isset($options['signature']) && $options['signature'] == 1) {
            $pm['options']['signature'] = 1;
        } else {
            $pm['options']['signature'] = 0;
        }
        if (isset($options['disablesmilies'])) {
            $pm['options']['disablesmilies'] = $options['disablesmilies'];
        }
        if (isset($options['savecopy']) && $options['savecopy'] == 1) {
            $pm['options']['savecopy'] = 1;
        } else {
            $pm['options']['savecopy'] = 0;
        }

        if (isset($options['readreceipt'])) {
            $pm['options']['readreceipt'] = $options['readreceipt'];
        }

        if (!empty($this->bb->input['saveasdraft'])) {
            $pm['saveasdraft'] = 1;
        }
        $pmhandler->set_data($pm);

        // Now let the pm handler do all the hard work.
        if (!$pmhandler->validate_pm()) {
            $pm_errors = $pmhandler->get_friendly_errors();
            $this->send_errors = $this->bb->inline_error($pm_errors);
            //$this->bb->input['action'] = 'send';
            $this->index($request, $response, true);
        } elseif (!empty($this->bb->input['preview'])) {
            $this->index($request, $response, true);
        } else {
            $pminfo = $pmhandler->insert_pm();
            $this->plugins->runHooks('private_do_send_end');

            if (isset($pminfo['draftsaved'])) {
                $this->bb->redirect($this->bb->settings['bburl'] . '/private', $this->lang->redirect_pmsaved);
            } else {
                $this->bb->redirect($this->bb->settings['bburl'] . '/private', $this->lang->redirect_pmsent);
            }
        }
    }
}
