<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\PM;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Tracking extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        if (!$this->bb->usergroup['cantrackpms']) {
            return $this->bb->error_no_permission();
        }
        $this->bb->add_breadcrumb($this->lang->nav_tracking);

        $this->plugins->runHooks('private_tracking_start');

        if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
            $this->bb->settings['postsperpage'] = 20;
        }

        // Figure out if we need to display multiple pages.
        $perpage = $this->bb->settings['postsperpage'];

//        $query = $this->db->simple_select('privatemessages', 'COUNT(pmid) as readpms',
//            "receipt='2' AND folder!='3' AND status!='0' AND fromid='" . $this->user->uid . "'");
//        $postcount = $this->db->fetch_field($query, 'readpms');

        $postcount = \RunBB\Models\Privatemessage::where([
            ['receipt', '=', '2'],
            ['folder', '!=', '3'],
            ['status', '!=', '0'],
            ['fromid', '=', $this->user->uid]
        ])->count();

        $page = $this->bb->getInput('read_page', 0);
        $pages = $postcount / $perpage;
        $pages = ceil($pages);

        if ($this->bb->getInput('read_page', '') === 'last') {
            $page = $pages;
        }

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }

        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $read_multipage = $this->pagination->multipage(
            $postcount,
            $perpage,
            $page,
            $this->bb->settings['bburl'].'/private/tracking?read_page={page}'
        );
        $this->view->offsetSet('read_multipage', $read_multipage);

//        $query = $this->db->query('
//		SELECT pm.pmid, pm.subject, pm.toid, pm.readtime, u.username as tousername
//		FROM ' . TABLE_PREFIX . 'privatemessages pm
//		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=pm.toid)
//		WHERE pm.receipt='2' AND pm.folder!='3'  AND pm.status!='0' AND pm.fromid='" . $this->user->uid . "'
//		ORDER BY pm.readtime DESC
//		LIMIT {$start}, {$perpage}
//	");
        $rm = \RunBB\Models\Privatemessage::where([
            ['privatemessages.receipt', '=', '2'],
            ['privatemessages.folder', '!=', '3'],
            ['privatemessages.status', '!=', '0'],
            ['privatemessages.fromid', '=', $this->user->uid]
        ])
            ->leftJoin('users', 'users.uid', '=', 'privatemessages.toid')
            ->orderBy('privatemessages.readtime', 'desc')
            ->skip($start)->take($perpage)->get([
                'privatemessages.pmid',
                'privatemessages.subject',
                'privatemessages.toid',
                'privatemessages.readtime',
                'users.username as tousername'
            ])
            ->toArray();

        $readmessages = [];
//        while ($readmessage = $this->db->fetch_array($query)) {
        foreach ($rm as $readmessage) {
            $readmessages[] = [
                'subject' => htmlspecialchars_uni($this->parser->parse_badwords($readmessage['subject'])),
                'profilelink' => $this->user->build_profile_link($readmessage['tousername'], $readmessage['toid']),
                'readdate' => $this->time->formatDate('relative', $readmessage['readtime']),
                'pmid' => $readmessage['pmid'],
                'bgcolor' => alt_trow()
            ];
        }
        $this->view->offsetSet('readmessages', $readmessages);

//        $query = $this->db->simple_select('privatemessages', 'COUNT(pmid) as unreadpms',
//            "receipt='1' AND folder!='3' AND status='0' AND fromid='" . $this->user->uid . "'");
//        $postcount = $this->db->fetch_field($query, 'unreadpms');

        $postcount = \RunBB\Models\Privatemessage::where([
            ['receipt', '=', '1'],
            ['folder', '!=', '3'],
            ['status', '=', '0'],
            ['fromid', '=', $this->user->uid]
        ])->count();

        $page = $this->bb->getInput('unread_page', 0);
        $pages = $postcount / $perpage;
        $pages = ceil($pages);

        if ($this->bb->getInput('unread_page', '') === 'last') {
            $page = $pages;
        }

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }

        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $unread_multipage = $this->pagination->multipage(
            $postcount,
            $perpage,
            $page,
            'private/tracking?unread_page={page}'
        );
        $this->view->offsetSet('unread_multipage', $unread_multipage);

//        $query = $this->db->query('
//		SELECT pm.pmid, pm.subject, pm.toid, pm.dateline, u.username as tousername
//		FROM ' . TABLE_PREFIX . 'privatemessages pm
//		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=pm.toid)
//		WHERE pm.receipt='1' AND pm.folder!='3' AND pm.status='0' AND pm.fromid='" . $this->user->uid . "'
//		ORDER BY pm.dateline DESC
//		LIMIT {$start}, {$perpage}
//	");

        $um = \RunBB\Models\Privatemessage::where([
            ['privatemessages.receipt', '=', '1'],
            ['privatemessages.folder', '!=', '3'],
            ['privatemessages.status', '=', '0'],
            ['privatemessages.fromid', '=', $this->user->uid]
        ])
            ->leftJoin('users', 'users.uid', '=', 'privatemessages.toid')
            ->orderBy('privatemessages.dateline', 'desc')
            ->skip($start)->take($perpage)->get([
                'privatemessages.pmid',
                'privatemessages.subject',
                'privatemessages.toid',
                'privatemessages.dateline',
                'users.username as tousername'
            ])
            ->toArray();

        $unreadmessages = [];
//        while ($unreadmessage = $this->db->fetch_array($query)) {
        foreach ($um as $unreadmessage) {
            $unreadmessages[] = [
                'subject' => htmlspecialchars_uni($this->parser->parse_badwords($unreadmessage['subject'])),
                'profilelink' => $this->user->build_profile_link($unreadmessage['tousername'], $unreadmessage['toid']),
                'senddate' => $this->time->formatDate('relative', $unreadmessage['dateline']),
                'pmid' => $unreadmessage['pmid'],
                'bgcolor' => alt_trow()
            ];
        }
        $this->view->offsetSet('unreadmessages', $unreadmessages);

        if (!$unreadmessages) {
            $this->lang->no_readmessages = $this->lang->no_unreadmessages;
        }

        $this->plugins->runHooks('private_tracking_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/PM/tracking.html.twig');
    }

    public function doTracking(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('private_do_tracking_start');

        if (!empty($this->bb->input['stoptracking'])) {
            $this->bb->input['readcheck'] = $this->bb->getInput('readcheck', []);
            if (!empty($this->bb->input['readcheck'])) {
                foreach ($this->bb->input['readcheck'] as $key => $val) {
                    $sql_array = [
                        'receipt' => 0
                    ];
                    $this->db->update_query(
                        'privatemessages',
                        $sql_array,
                        'pmid=' . (int)$key . ' AND fromid=' . $this->user->uid
                    );
                }
            }
            $this->plugins->runHooks('private_do_tracking_end');
            $this->bb->redirect(
                $this->bb->settings['bburl'] . '/private/tracking',
                $this->lang->redirect_pmstrackingstopped
            );
        } elseif (!empty($this->bb->input['stoptrackingunread'])) {
            $this->bb->input['unreadcheck'] = $this->bb->getInput('unreadcheck', []);
            if (!empty($this->bb->input['unreadcheck'])) {
                foreach ($this->bb->input['unreadcheck'] as $key => $val) {
                    $sql_array = [
                        'receipt' => 0
                    ];
                    $this->db->update_query(
                        'privatemessages',
                        $sql_array,
                        'pmid=' . (int)$key . ' AND fromid=' . $this->user->uid
                    );
                }
            }
            $this->plugins->runHooks('private_do_tracking_end');
            $this->bb->redirect(
                $this->bb->settings['bburl'] . '/private/tracking',
                $this->lang->redirect_pmstrackingstopped
            );
        } elseif (!empty($this->bb->input['cancel'])) {
            $this->bb->input['unreadcheck'] = $this->bb->getInput('unreadcheck', []);
            if (!empty($this->bb->input['unreadcheck'])) {
                foreach ($this->bb->input['unreadcheck'] as $pmid => $val) {
                    $pmids[$pmid] = (int)$pmid;
                }

                $pmids = implode(',', $pmids);
                $query = $this->db->simple_select(
                    'privatemessages',
                    'uid',
                    "pmid IN ($pmids) AND fromid='" . $this->user->uid . "'"
                );
                while ($pm = $this->db->fetch_array($query)) {
                    $pmuids[$pm['uid']] = $pm['uid'];
                }

                $this->db->delete_query(
                    'privatemessages',
                    "pmid IN ($pmids) AND receipt='1' AND status='0' AND fromid='" . $this->user->uid . "'"
                );
                foreach ($pmuids as $uid) {
                    // Message is canceled, update PM count for this user
                    $this->user->update_pm_count($uid);
                }
            }
            $this->plugins->runHooks('private_do_tracking_end');
            $this->bb->redirect(
                $this->bb->settings['bburl'] . '/private/tracking',
                $this->lang->redirect_pmstrackingcanceled
            );
        }
    }
}
