<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Poll;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Editpoll extends AbstractController
{

    private function init()
    {
        // Load global language phrases
        $this->lang->load('polls');
        $this->plugins->runHooks('polls_start');
    }

    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            $this->init();
        }

        $pid = $this->bb->getInput('pid', 0);
        $this->view->offsetSet('pid', $pid);

        $this->plugins->runHooks('polls_editpoll_start');

        $query = $this->db->simple_select('polls', '*', "pid='$pid'");
        $poll = $this->db->fetch_array($query);

        if (!$poll) {
            $this->bb->error($this->lang->error_invalidpoll);
        }

        $query = $this->db->simple_select('threads', '*', "poll='$pid'");
        $thread = $this->db->fetch_array($query);
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $tid = $thread['tid'];
        $this->view->offsetSet('tid', $tid);
        $fid = $thread['fid'];

        // Make navigation
        $this->bb->build_forum_breadcrumb($fid);
        $this->bb->add_breadcrumb(htmlspecialchars_uni($thread['subject']), get_thread_link($thread['tid']));
        $this->bb->add_breadcrumb($this->lang->nav_editpoll);

        //$forumpermissions = $this->forum->forum_permissions($fid);

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0 && !$this->user->is_moderator($fid, 'canmanagepolls')) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }

        if (!$this->user->is_moderator($fid, 'canmanagepolls')) {
            return $this->bb->error_no_permission();
        }

        $postoptionschecked = ['closed' => '', 'multiple' => '', 'public' => ''];

        $polldate = $this->time->formatDate($this->bb->settings['dateformat'], $poll['dateline']);
        $this->view->offsetSet('polldate', $polldate);

        if (empty($this->bb->input['updateoptions'])) {
            if ($poll['closed'] == 1) {
                $postoptionschecked['closed'] = 'checked="checked"';
            }

            if ($poll['multiple'] == 1) {
                $postoptionschecked['multiple'] = 'checked="checked"';
            }

            if ($poll['public'] == 1) {
                $postoptionschecked['public'] = 'checked="checked"';
            }

            $optionsarray = explode('||~|~||', $poll['options']);
            $votesarray = explode('||~|~||', $poll['votes']);

            $poll['totvotes'] = 0;
            for ($i = 1; $i <= $poll['numoptions']; ++$i) {
                $poll['totvotes'] = $poll['totvotes'] + $votesarray[$i - 1];
            }

            $question = htmlspecialchars_uni($poll['question']);
            $numoptions = $poll['numoptions'];
            $optionbits = [];
            for ($i = 0; $i < $numoptions; ++$i) {
                $optionvotes = (int)$votesarray[$i];

                if (!$optionvotes) {
                    $optionvotes = 0;
                }
                $optionbits[] = [
                    'counter' => $i + 1,
                    'option' => htmlspecialchars_uni($optionsarray[$i]),
                    'optionvotes' => $optionvotes
                ];
                $option = '';
                $optionvotes = '';
            }

            if (!$poll['timeout']) {
                $timeout = 0;
            } else {
                $timeout = $poll['timeout'];
            }

            if (!$poll['maxoptions']) {
                $maxoptions = 0;
            } else {
                $maxoptions = $poll['maxoptions'];
            }
        } else {
            if ($this->bb->settings['maxpolloptions'] && $this->bb->getInput('numoptions', 0) > $this->bb->settings['maxpolloptions']) {
                $numoptions = $this->bb->settings['maxpolloptions'];
            } elseif ($this->bb->getInput('numoptions', 0) < 2) {
                $numoptions = 2;
            } else {
                $numoptions = $this->bb->getInput('numoptions', 0);
            }
            $question = htmlspecialchars_uni($this->bb->input['question']);

            $postoptions = $this->bb->getInput('postoptions', []);
            if (isset($postoptions['multiple']) && $postoptions['multiple'] == 1) {
                $postoptionschecked['multiple'] = 'checked="checked"';
            }

            if (isset($postoptions['public']) && $postoptions['public'] == 1) {
                $postoptionschecked['public'] = 'checked="checked"';
            }

            if (isset($postoptions['closed']) && $postoptions['closed'] == 1) {
                $postoptionschecked['closed'] = 'checked="checked"';
            }

            $options = $this->bb->getInput('options', []);
            $votes = $this->bb->getInput('votes', []);
            $optionbits = [];
            for ($i = 1; $i <= $numoptions; ++$i) {
                if (!isset($options[$i])) {
                    $options[$i] = '';
                }
                if (!isset($votes[$i])) {
                    $votes[$i] = 0;
                }
                $optionvotes = (int)$votes[$i];

                if (!$optionvotes) {
                    $optionvotes = 0;
                }
                $optionbits[] = [
                    'counter' => $i,
                    'option' => htmlspecialchars_uni($options[$i]),
                    'optionvotes' => $optionvotes
                ];
                $option = '';
            }

            if ($this->bb->getInput('timeout', 0) > 0) {
                $timeout = $this->bb->getInput('timeout', 0);
            } else {
                $timeout = 0;
            }

            if (!$poll['maxoptions']) {
                $maxoptions = 0;
            } else {
                $maxoptions = $poll['maxoptions'];
            }
        }
        $this->view->offsetSet('optionbits', $optionbits);
        $this->view->offsetSet('numoptions', $numoptions);
        $this->view->offsetSet('question', $question);
        $this->view->offsetSet('postoptionschecked', $postoptionschecked);
        $this->view->offsetSet('maxoptions', $maxoptions);
        $this->view->offsetSet('timeout', $timeout);

        $this->plugins->runHooks('polls_editpoll_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Poll/editpoll.html.twig');
    }

    public function doEditpoll(Request $request, Response $response)
    {
        $this->init();

        if (!empty($this->bb->input['updateoptions'])) {
            return $this->index($request, $response, true);
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('polls_do_editpoll_start');

        $query = $this->db->simple_select('polls', '*', "pid='" . $this->bb->getInput('pid', 0) . "'");
        $poll = $this->db->fetch_array($query);

        if (!$poll) {
            $this->bb->error($this->lang->error_invalidpoll);
        }

        $query = $this->db->simple_select('threads', '*', "poll='" . $this->bb->getInput('pid', 0) . "'");
        $thread = $this->db->fetch_array($query);
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        //$forumpermissions = $this->forum->forum_permissions($thread['fid']);

        // Get forum info
        $forum = $this->forum->get_forum($thread['fid']);
        $fid = $thread['fid'];
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0 && !$this->user->is_moderator($fid, 'canmanagepolls')) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }

        if (!$this->user->is_moderator($thread['fid'], 'canmanagepolls')) {
            return $this->bb->error_no_permission();
        }

        if ($this->bb->settings['maxpolloptions'] && $this->bb->getInput('numoptions', 0) > $this->bb->settings['maxpolloptions']) {
            $numoptions = $this->bb->settings['maxpolloptions'];
        } elseif ($this->bb->getInput('numoptions', 0) < 2) {
            $numoptions = 2;
        } else {
            $numoptions = $this->bb->getInput('numoptions', 0);
        }

        $postoptions = $this->bb->getInput('postoptions', []);
        if (!isset($postoptions['multiple']) || $postoptions['multiple'] != '1') {
            $postoptions['multiple'] = 0;
        }

        if (!isset($postoptions['public']) || $postoptions['public'] != '1') {
            $postoptions['public'] = 0;
        }

        if (!isset($postoptions['closed']) || $postoptions['closed'] != '1') {
            $postoptions['closed'] = 0;
        }
        $optioncount = '0';
        $options = $this->bb->input['options'];

        for ($i = 1; $i <= $numoptions; ++$i) {
            if (!isset($options[$i])) {
                $options[$i] = '';
            }

            if ($this->bb->settings['polloptionlimit'] != 0 && my_strlen($options[$i]) > $this->bb->settings['polloptionlimit']) {
                $lengtherror = 1;
                break;
            }

            if (strpos($options[$i], '||~|~||') !== false) {
                $sequenceerror = 1;
                break;
            }

            if (trim($options[$i]) != '') {
                $optioncount++;
            }
        }

        if (isset($lengtherror)) {
            $this->bb->error($this->lang->error_polloptiontoolong);
        }

        if (isset($sequenceerror)) {
            $this->bb->error($this->lang->error_polloptionsequence);
        }

        $this->bb->input['question'] = $this->bb->getInput('question', '', true);
        if (trim($this->bb->input['question']) == '' || $optioncount < 2) {
            $this->bb->error($this->lang->error_noquestionoptions);
        }

        $optionslist = '';
        $voteslist = '';
        $numvotes = '';
        $votes = $this->bb->input['votes'];
        for ($i = 1; $i <= $numoptions; ++$i) {
            if (trim($options[$i]) != '') {
                if ($optionslist != '') {
                    $optionslist .= '||~|~||';
                    $voteslist .= '||~|~||';
                }

                $optionslist .= trim($options[$i]);
                if (!isset($votes[$i]) || (int)$votes[$i] <= 0) {
                    $votes[$i] = '0';
                }
                $voteslist .= $votes[$i];
                $numvotes = $numvotes + $votes[$i];
            }
        }

        if ($this->bb->getInput('timeout', 0) > 0) {
            $timeout = $this->bb->getInput('timeout', 0);
        } else {
            $timeout = 0;
        }

        if ($this->bb->getInput('maxoptions', 0) > 0 && $this->bb->getInput('maxoptions', 0) < $numoptions) {
            $maxoptions = $this->bb->getInput('maxoptions', 0);
        } else {
            $maxoptions = 0;
        }

        $updatedpoll = [
            'question' => $this->db->escape_string($this->bb->input['question']),
            'options' => $this->db->escape_string($optionslist),
            'votes' => $this->db->escape_string($voteslist),
            'numoptions' => (int)$optioncount,
            'numvotes' => $numvotes,
            'timeout' => $timeout,
            'closed' => $postoptions['closed'],
            'multiple' => $postoptions['multiple'],
            'public' => $postoptions['public'],
            'maxoptions' => $maxoptions
        ];

        $this->plugins->runHooks('polls_do_editpoll_process');

        $this->db->update_query('polls', $updatedpoll, "pid='" . $this->bb->getInput('pid', 0) . "'");

        $this->plugins->runHooks('polls_do_editpoll_end');

        $modlogdata['fid'] = $thread['fid'];
        $modlogdata['tid'] = $thread['tid'];
        $this->bblogger->log_moderator_action($modlogdata, $this->lang->poll_edited);

        $this->bb->redirect(get_thread_link($thread['tid']), $this->lang->redirect_pollupdated);
    }
}
