<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Poll;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Newpoll extends AbstractController
{

    private function init()
    {
        // Load global language phrases
        $this->lang->load('polls');
        $this->plugins->runHooks('polls_start');
    }

    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            $this->init();
        }

        // Form for new poll
        $tid = $this->bb->getInput('tid', 0);
        $this->view->offsetSet('tid', $tid);

        $this->plugins->runHooks('polls_newpoll_start');

        $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        // Is the currently logged in user a moderator of this forum?
        $ismod = $this->user->is_moderator($thread['fid']);

        // Make sure we are looking at a real thread here.
        if (($thread['visible'] != 1 && $ismod == false) || ($thread['visible'] > 1 && $ismod == true)) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $fid = $thread['fid'];
        $forumpermissions = $this->forum->forum_permissions($fid);

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0 && !$this->user->is_moderator($fid, 'canmanagepolls')) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }
        // Make navigation
        $this->bb->build_forum_breadcrumb($fid);
        $this->bb->add_breadcrumb(htmlspecialchars_uni($thread['subject']), get_thread_link($thread['tid']));
        $this->bb->add_breadcrumb($this->lang->nav_postpoll);

        // No permission if: Not thread author; not moderator; no forum perms to view, post threads, post polls
        if (($thread['uid'] != $this->user->uid &&
                !$this->user->is_moderator($fid, 'canmanagepolls')) ||
            ($forumpermissions['canview'] == 0 ||
                $forumpermissions['canpostthreads'] == 0 ||
                $forumpermissions['canpostpolls'] == 0)
        ) {
            return $this->bb->error_no_permission();
        }

        if ($thread['poll']) {
            $this->bb->error($this->lang->error_pollalready);
        }

        $time = TIME_NOW;
        if ($thread['dateline'] < ($time - ($this->bb->settings['polltimelimit'] * 60 * 60)) &&
            $this->bb->settings['polltimelimit'] != 0 && $ismod == false
        ) {
            $this->lang->poll_time_limit = $this->lang->sprintf($this->lang->poll_time_limit, $this->bb->settings['polltimelimit']);
            $this->bb->error($this->lang->poll_time_limit);
        }

        // Sanitize number of poll options
        if ($this->bb->getInput('numpolloptions', 0) > 0) {
            $this->bb->input['polloptions'] = $this->bb->getInput('numpolloptions', 0);
        }
        if ($this->bb->settings['maxpolloptions'] && $this->bb->getInput('polloptions', 0) > $this->bb->settings['maxpolloptions']) {    // Too big
            $polloptions = $this->bb->settings['maxpolloptions'];
        } elseif ($this->bb->getInput('polloptions', 0) < 2) {    // Too small
            $polloptions = 2;
        } else {    // Just right
            $polloptions = $this->bb->getInput('polloptions', 0);
        }
        $this->view->offsetSet('polloptions', $polloptions);

        $question = htmlspecialchars_uni($this->bb->getInput('question', '', true));
        $this->view->offsetSet('question', $question);

        $postoptionschecked = ['public' => '', 'multiple' => ''];
        $postoptions = $this->bb->getInput('postoptions', 0);
        if (isset($postoptions['multiple']) && $postoptions['multiple'] == 1) {
            $postoptionschecked['multiple'] = 'checked="checked"';
        }
        if (isset($postoptions['public']) && $postoptions['public'] == 1) {
            $postoptionschecked['public'] = 'checked="checked"';
        }
        $this->view->offsetSet('postoptionschecked', $postoptionschecked);

        $options = $this->bb->getInput('options', []);
        $optionbits = [];
        for ($i = 1; $i <= $polloptions; ++$i) {
            if (!isset($options[$i])) {
                $options[$i] = '';
            }
            $option = $options[$i];
            $optionbits[] = [
                'i' => $i,
                'option' => htmlspecialchars_uni($option)
            ];
            $option = '';
        }
        $this->view->offsetSet('optionbits', $optionbits);

        if ($this->bb->getInput('timeout', 0) > 0) {
            $timeout = $this->bb->getInput('timeout', 0);
        } else {
            $timeout = 0;
        }
        $this->view->offsetSet('timeout', $timeout);

        if ($this->bb->getInput('maxoptions', 0) > 0 &&
            $this->bb->getInput('maxoptions', 0) < $polloptions
        ) {
            $maxoptions = $this->bb->getInput('maxoptions', 0);
        } else {
            $maxoptions = 0;
        }

        $this->plugins->runHooks('polls_newpoll_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Poll/newpoll.html.twig');
    }

    public function doNewpoll(Request $request, Response $response)
    {
        $this->init();

        if (!empty($this->bb->input['updateoptions'])) {
            return $this->index($request, $response, true);
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('polls_do_newpoll_start');

        $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $fid = $thread['fid'];
        $forumpermissions = $this->forum->forum_permissions($fid);

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0 && !$this->user->is_moderator($fid, 'canmanagepolls')) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }

        // No permission if: Not thread author; not moderator; no forum perms to view, post threads, post polls
        if (($thread['uid'] != $this->user->uid &&
                !$this->user->is_moderator($fid, 'canmanagepolls')) ||
            ($forumpermissions['canview'] == 0 ||
                $forumpermissions['canpostthreads'] == 0 ||
                $forumpermissions['canpostpolls'] == 0)
        ) {
            return $this->bb->error_no_permission();
        }

        if ($thread['poll']) {
            $this->bb->error($this->lang->error_pollalready);
        }

        $polloptions = $this->bb->getInput('polloptions', 0);
        if ($this->bb->settings['maxpolloptions'] && $polloptions > $this->bb->settings['maxpolloptions']) {
            $polloptions = $this->bb->settings['maxpolloptions'];
        }

        $postoptions = $this->bb->getInput('postoptions', []);
        if (!isset($postoptions['multiple']) || $postoptions['multiple'] != '1') {
            $postoptions['multiple'] = 0;
        }

        if (!isset($postoptions['public']) || $postoptions['public'] != '1') {
            $postoptions['public'] = 0;
        }

        if ($polloptions < 2) {
            $polloptions = '2';
        }
        $optioncount = '0';
        $options = $this->bb->getInput('options', []);

        for ($i = 1; $i <= $polloptions; ++$i) {
            if (!isset($options[$i])) {
                $options[$i] = '';
            }

            if ($this->bb->settings['polloptionlimit'] != 0 &&
                my_strlen($options[$i]) >
                $this->bb->settings['polloptionlimit']
            ) {
                $lengtherror = 1;
                break;
            }

            if (strpos($options[$i], '||~|~||') !== false) {
                $sequenceerror = 1;
                break;
            }

            if (trim($options[$i]) != '') {
                $optioncount++;
            }
        }

        if (isset($lengtherror)) {
            $this->bb->error($this->lang->error_polloptiontoolong);
        }

        if (isset($sequenceerror)) {
            $this->bb->error($this->lang->error_polloptionsequence);
        }

        $this->bb->input['question'] = $this->bb->getInput('question', '');

        if (trim($this->bb->input['question']) == '' || $optioncount < 2) {
            $this->bb->error($this->lang->error_noquestionoptions);
        }

        $optionslist = '';
        $voteslist = '';
        for ($i = 1; $i <= $polloptions; ++$i) {
            if (trim($options[$i]) != '') {
                if ($optionslist != '') {
                    $optionslist .= '||~|~||';
                    $voteslist .= '||~|~||';
                }
                $optionslist .= trim($options[$i]);
                $voteslist .= '0';
            }
        }

        if ($this->bb->getInput('timeout', 0) > 0) {
            $timeout = $this->bb->getInput('timeout', 0);
        } else {
            $timeout = 0;
        }

        if ($this->bb->getInput('maxoptions', 0) > 0 &&
            $this->bb->getInput('maxoptions', 0) < $polloptions
        ) {
            $maxoptions = $this->bb->getInput('maxoptions', 0);
        } else {
            $maxoptions = 0;
        }

        $newpoll = [
            'tid' => $thread['tid'],
            'question' => $this->db->escape_string($this->bb->input['question']),
            'dateline' => TIME_NOW,
            'options' => $this->db->escape_string($optionslist),
            'votes' => $this->db->escape_string($voteslist),
            'numoptions' => (int)$optioncount,
            'numvotes' => 0,
            'timeout' => $timeout,
            'closed' => 0,
            'multiple' => $postoptions['multiple'],
            'public' => $postoptions['public'],
            'maxoptions' => $maxoptions
        ];

        $this->plugins->runHooks('polls_do_newpoll_process');

        $pid = $this->db->insert_query('polls', $newpoll);

        $this->db->update_query('threads', ['poll' => $pid], "tid='" . $thread['tid'] . "'");

        $this->plugins->runHooks('polls_do_newpoll_end');

        if ($thread['visible'] == 1) {
            $this->bb->redirect(get_thread_link($thread['tid']), $this->lang->redirect_pollposted);
        } else {
            $this->bb->redirect($this->forum->get_forum_link($thread['fid']), $this->lang->redirect_pollpostedmoderated);
        }
    }
}
