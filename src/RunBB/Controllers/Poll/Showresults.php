<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Poll;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Showresults extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('polls');
        $this->plugins->runHooks('polls_start');


        $query = $this->db->simple_select('polls', '*', "pid='" . $this->bb->getInput('pid', 0) . "'");
        $poll = $this->db->fetch_array($query);

        if (!$poll) {
            $this->bb->error($this->lang->error_invalidpoll);
        }

        $tid = $poll['tid'];
        $thread = $this->thread->get_thread($tid);
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $fid = $thread['fid'];

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        }

        $forumpermissions = $this->forum->forum_permissions($forum['fid']);

        $this->plugins->runHooks('polls_showresults_start');

        if ($forumpermissions['canviewthreads'] == 0 ||
            $forumpermissions['canview'] == 0 ||
            (isset($forumpermissions['canonlyviewownthreads']) &&
                $forumpermissions['canonlyviewownthreads'] != 0 &&
                $thread['uid'] != $this->user->uid)
        ) {
            return $this->bb->error_no_permission();
        }

        // Make navigation
        $this->bb->build_forum_breadcrumb($fid);
        $this->bb->add_breadcrumb(htmlspecialchars_uni($thread['subject']), get_thread_link($thread['tid']));
        $this->bb->add_breadcrumb($this->lang->nav_pollresults);

        $voters = $votedfor = [];

        // Calculate votes
        $query = $this->db->query('
		SELECT v.*, u.username
		FROM ' . TABLE_PREFIX . 'pollvotes v
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=v.uid)
		WHERE v.pid='{$poll['pid']}'
		ORDER BY u.username
	");
        while ($voter = $this->db->fetch_array($query)) {
            // Mark for current user's vote
            if ($this->user->uid == $voter['uid'] && $this->user->uid) {
                $votedfor[$voter['voteoption']] = 1;
            }

            // Count number of guests and users without a username (assumes they've been deleted)
            if ($voter['uid'] == 0 || $voter['username'] == '') {
                // Add one to the number of voters for guests
                ++$guest_voters[$voter['voteoption']];
            } else {
                $voters[$voter['voteoption']][$voter['uid']] = $voter['username'];
            }
        }

        $optionsarray = explode('||~|~||', $poll['options']);
        $votesarray = explode('||~|~||', $poll['votes']);
        $poll['totvotes'] = 0;
        for ($i = 1; $i <= $poll['numoptions']; ++$i) {
            $poll['totvotes'] = $poll['totvotes'] + $votesarray[$i - 1];
        }

        $polloptions = [];
        for ($i = 1; $i <= $poll['numoptions']; ++$i) {
            $parser_options = [
                'allow_html' => $forum['allowhtml'],
                'allow_mycode' => $forum['allowmycode'],
                'allow_smilies' => $forum['allowsmilies'],
                'allow_imgcode' => $forum['allowimgcode'],
                'allow_videocode' => $forum['allowvideocode'],
                'filter_badwords' => 1
            ];
            $option = $this->parser->parse_message($optionsarray[$i - 1], $parser_options);

            $votes = $votesarray[$i - 1];
            $number = $i;
            // Make the mark for current user's voted option
            if (!empty($votedfor[$number])) {
                $optionbg = 'trow2';
                $votestar = '*';
            } else {
                $optionbg = 'trow1';
                $votestar = '';
            }

            if ($votes == 0) {
                $percent = 0;
            } else {
                $percent = number_format($votes / $poll['totvotes'] * 100, 2);
            }

            $imagewidth = round($percent);
            $comma = '';
            $guest_comma = '';
            $userlist = '';
            $guest_count = 0;
            if ($poll['public'] == 1 || $this->user->is_moderator($fid, 'canmanagepolls')) {
                if (isset($voters[$number]) && is_array($voters[$number])) {
                    foreach ($voters[$number] as $uid => $username) {
                        $userlist .= $comma . $this->user->build_profile_link($username, $uid);
                        $comma = $guest_comma = $this->lang->comma;
                    }
                }

                if (isset($guest_voters[$number]) && $guest_voters[$number] > 0) {
                    if ($guest_voters[$number] == 1) {
                        $userlist .= $guest_comma . $this->lang->guest_count;
                    } else {
                        $userlist .= $guest_comma . $this->lang->sprintf($this->lang->guest_count_multiple, $guest_voters[$number]);
                    }
                }
            }
            $polloptions[] = [
                'optionbg' => $optionbg,
                'option' => $option,
                'votestar' => $votestar,
                'imagewidth' => $imagewidth,
                'percent' => $percent,
                'userlist' => $userlist,
                'votes' => $votes
            ];
        }
        $this->view->offsetSet('polloptions', $polloptions);

        if ($poll['totvotes']) {
            $totpercent = '100%';
        } else {
            $totpercent = '0%';
        }
        $this->view->offsetSet('totpercent', $totpercent);

        $this->plugins->runHooks('polls_showresults_end');

        $this->view->offsetSet('question', htmlspecialchars_uni($poll['question']));
        $this->view->offsetSet('numvotes', $poll['numvotes']);

        $this->bb->output_page();
        $this->view->render($response, '@forum/Poll/showresults.html.twig');
    }
}
