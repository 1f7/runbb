<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Poll;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Undovote extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('polls');
        $this->plugins->runHooks('polls_start');


        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canundovotes'] != 1) {
            return $this->bb->error_no_permission();
        }

        $query = $this->db->simple_select('polls', '*', "pid='" . $this->bb->getInput('pid', 0) . "'");
        $poll = $this->db->fetch_array($query);

        if (!$poll['pid']) {
            $this->bb->error($this->lang->error_invalidpoll);
        }

        $this->plugins->runHooks('polls_do_undovote_start');

        $poll['numvotes'] = (int)$poll['numvotes'];

        // We do not have $forum_cache available here since no forums permissions are checked in undo vote
        // Get thread ID and then get forum info
        $thread = $this->thread->get_thread($poll['tid']);
        if (!$thread || $thread['visible'] == 0) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $fid = $thread['fid'];

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }

        $poll['timeout'] = $poll['timeout'] * 60 * 60 * 24;


        $expiretime = $poll['dateline'] + $poll['timeout'];
        if ($poll['closed'] == 1 || $thread['closed'] == 1 || ($expiretime < TIME_NOW && $poll['timeout'])) {
            $this->bb->error($this->lang->error_pollclosed);
        }

        // Check if the user has voted before...
        $vote_options = [];
        if ($this->user->uid) {
            $query = $this->db->simple_select('pollvotes', 'vid,voteoption', "uid='" . $this->user->uid . "' AND pid='" . $poll['pid'] . "'");
            while ($voteoption = $this->db->fetch_array($query)) {
                $vote_options[$voteoption['vid']] = $voteoption['voteoption'];
            }
        } elseif (isset($this->bb->cookies['pollvotes'][$poll['pid']])) {
            // for Guests, we simply see if they've got the cookie
            $vote_options = explode(',', $this->bb->cookies['pollvotes'][$poll['pid']]);
        }

        if (empty($vote_options)) {
            $this->bb->error($this->lang->error_notvoted);
        } elseif (!$this->user->uid) {
            // clear cookie for Guests
            $this->bb->my_setcookie("pollvotes[{$poll['pid']}]", '');
        }

        // Note, this is not thread safe!
        $votesarray = explode('||~|~||', $poll['votes']);
        if (count($votesarray) > $poll['numoptions']) {
            $votesarray = array_slice(0, $poll['numoptions']);
        }

        if ($poll['multiple'] == 1) {
            foreach ($vote_options as $vote) {
                if (isset($votesarray[$vote - 1])) {
                    --$votesarray[$vote - 1];
                    --$poll['numvotes'];
                }
            }
        } else {
            $voteoption = reset($vote_options);
            if (isset($votesarray[$voteoption - 1])) {
                --$votesarray[$voteoption - 1];
                --$poll['numvotes'];
            }
        }

        // check if anything < 0 - possible if Guest vote undoing is allowed (generally Guest unvoting should be disabled >_>)
        if ($poll['numvotes'] < 0) {
            $poll['numvotes'] = 0;
        }

        foreach ($votesarray as $i => $votes) {
            if ($votes < 0) {
                $votesarray[$i] = 0;
            }
        }

        $voteslist = implode('||~|~||', $votesarray);
        $updatedpoll = [
            'votes' => $this->db->escape_string($voteslist),
            'numvotes' => (int)$poll['numvotes'],
        ];

        $this->plugins->runHooks('polls_do_undovote_process');

        $this->db->delete_query('pollvotes', "uid='" . $this->user->uid . "' AND pid='" . $poll['pid'] . "'");
        $this->db->update_query('polls', $updatedpoll, "pid='" . $poll['pid'] . "'");

        $this->plugins->runHooks('polls_do_undovote_end');

        $this->bb->redirect(get_thread_link($poll['tid']), $this->lang->redirect_unvoted);
    }
}
