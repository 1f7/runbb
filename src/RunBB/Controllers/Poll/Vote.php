<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Poll;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Vote extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('polls');
        $this->plugins->runHooks('polls_start');


        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $query = $this->db->simple_select('polls', '*', "pid='" . $this->bb->getInput('pid', '') . "'");
        $poll = $this->db->fetch_array($query);

        if (!$poll) {
            $this->bb->error($this->lang->error_invalidpoll);
        }

        $this->plugins->runHooks('polls_vote_start');

        $poll['timeout'] = $poll['timeout'] * 60 * 60 * 24;

        $query = $this->db->simple_select('threads', '*', "poll='" . (int)$poll['pid'] . "'");
        $thread = $this->db->fetch_array($query);

        if (!$thread || $thread['visible'] == 0) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $fid = $thread['fid'];
        $forumpermissions = $this->forum->forum_permissions($fid);
        if ($forumpermissions['canvotepolls'] == 0) {
            return $this->bb->error_no_permission();
        }

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }

        $expiretime = $poll['dateline'] + $poll['timeout'];
        $now = TIME_NOW;
        if ($poll['closed'] == 1 || $thread['closed'] == 1 || ($expiretime < $now && $poll['timeout'])) {
            $this->bb->error($this->lang->error_pollclosed);
        }

        if (!isset($this->bb->input['option'])) {
            $this->bb->error($this->lang->error_nopolloptions);
        }

        // Check if the user has voted before...
        if ($this->user->uid) {
            $query = $this->db->simple_select('pollvotes', '*', "uid='" . $this->user->uid . "' AND pid='" . $poll['pid'] . "'");
            $votecheck = $this->db->fetch_array($query);
        }

        if ($votecheck['vid'] || (isset($this->bb->cookies['pollvotes'][$poll['pid']]) &&
                $this->bb->cookies['pollvotes'][$poll['pid']] !== '')
        ) {
            $this->bb->error($this->lang->error_alreadyvoted);
        } elseif (!$this->user->uid) {
            // Give a cookie to guests to inhibit revotes
            if (is_array($this->bb->input['option'])) {
                // We have multiple options here...
                $votes_cookie = implode(',', array_keys($this->bb->input['option']));
            } else {
                $votes_cookie = $this->bb->input['option'];
            }

            $this->bb->my_setcookie("pollvotes[{$poll['pid']}]", $votes_cookie);
        }

        $votesql = '';
        $now = TIME_NOW;
        $votesarray = explode('||~|~||', $poll['votes']);
        $option = $this->bb->input['option'];
        $numvotes = (int)$poll['numvotes'];
        if ($poll['multiple'] == 1) {
            if (is_array($option)) {
                $total_options = 0;

                foreach ($option as $voteoption => $vote) {
                    if ($vote == 1 && isset($votesarray[$voteoption - 1])) {
                        if ($votesql) {
                            $votesql .= ',';
                        }
                        $votesql .= "('" . $poll['pid'] . "','" . $this->user->uid . "','" . $this->db->escape_string($voteoption) . "','$now')";
                        $votesarray[$voteoption - 1]++;
                        $numvotes = $numvotes + 1;
                        $total_options++;
                    }
                }

                if ($total_options > $poll['maxoptions'] && $poll['maxoptions'] != 0) {
                    $this->bb->error($this->lang->sprintf($this->lang->error_maxpolloptions, $poll['maxoptions']));
                }
            }
        } else {
            if (is_array($option) || !isset($votesarray[$option - 1])) {
                $this->bb->error($this->lang->error_nopolloptions);
            }
            $votesql = "('" . $poll['pid'] . "','" . $this->user->uid . "','" . $this->db->escape_string($option) . "','$now')";
            $votesarray[$option - 1]++;
            $numvotes = $numvotes + 1;
        }

        if (!$votesql) {
            $this->bb->error($this->lang->error_nopolloptions);
        }

        $this->db->write_query('
		INSERT INTO
		' . TABLE_PREFIX . "pollvotes (pid,uid,voteoption,dateline)
		VALUES $votesql
	");
        $voteslist = '';
        for ($i = 1; $i <= $poll['numoptions']; ++$i) {
            if ($i > 1) {
                $voteslist .= '||~|~||';
            }
            $voteslist .= $votesarray[$i - 1];
        }
        $updatedpoll = [
            'votes' => $this->db->escape_string($voteslist),
            'numvotes' => (int)$numvotes,
        ];

        $this->plugins->runHooks('polls_vote_process');

        $this->db->update_query('polls', $updatedpoll, "pid='" . $poll['pid'] . "'");

        $this->plugins->runHooks('polls_vote_end');

        $this->bb->redirect(get_thread_link($poll['tid']), $this->lang->redirect_votethanks);
    }
}
