<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Portal extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IN_PORTAL', 1);

        // Load global language phrases
        $this->lang->load('portal');

        if ($this->bb->settings['portal'] == 0) {
            $this->bb->error($this->lang->portal_disabled);
        }

        // Fetch the current URL
        $portal_url = $this->session->get_current_location();
        $this->view->offsetSet('portal_url', $portal_url);

        $file_name = strtok(my_strtolower(basename($portal_url)), '?');
        $this->bb->add_breadcrumb($this->lang->nav_portal, $file_name);

        $this->plugins->runHooks('portal_start');

        $tunviewwhere = $unviewwhere = '';
        // get forums user cannot view
        $unviewable = $this->forum->get_unviewable_forums(true);
        if ($unviewable) {
            $unviewwhere = ' AND fid NOT IN (' . implode(',', $unviewable) . ')';
            $tunviewwhere = ' AND t.fid NOT IN (' . implode(',', $unviewable) . ')';
        }

        // get inactive forums
        $inactive = $this->forum->get_inactive_forums();
        if ($inactive) {
            $unviewwhere .= ' AND fid NOT IN (' . implode(',', $inactive) . ')';
            $tunviewwhere .= ' AND t.fid NOT IN (' . implode(',', $inactive) . ')';
        }

        $welcome = '';
        // If user is known, welcome them
        if ($this->bb->settings['portal_showwelcome'] != 0) {
            if ($this->user->uid != 0) {
                // Get number of new posts, threads, announcements
                $query = $this->db->simple_select(
                    'posts',
                    'COUNT(pid) AS newposts',
                    "visible=1 AND dateline>'" . $this->user->lastvisit . "'{$unviewwhere}"
                );
                $newposts = $this->db->fetch_field($query, 'newposts');
                if ($newposts) {
                    // If there aren't any new posts, there is no point in wasting two more queries
                    $query = $this->db->simple_select(
                        'threads',
                        'COUNT(tid) AS newthreads',
                        "visible=1 AND dateline>'" . $this->user->lastvisit . "'{$unviewwhere}"
                    );
                    $newthreads = $this->db->fetch_field($query, 'newthreads');

                    $newann = 0;
                    if (!empty($this->bb->settings['portal_announcementsfid'])) {
                        $annfidswhere = '';
                        if ($this->bb->settings['portal_announcementsfid'] != -1) {
                            $announcementsfids = explode(',', (string)$this->bb->settings['portal_announcementsfid']);
                            if (is_array($announcementsfids)) {
                                foreach ($announcementsfids as &$fid) {
                                    $fid = (int)$fid;
                                }
                                unset($fid);

                                $announcementsfids = implode(',', $announcementsfids);

                                $annfidswhere = ' AND fid IN (' . $announcementsfids . ')';
                            }
                        }

                        $query = $this->db->simple_select(
                            'threads',
                            'COUNT(tid) AS newann',
                            "visible=1 AND dateline>'" . $this->user->lastvisit . "'{$annfidswhere}{$unviewwhere}"
                        );
                        $newann = $this->db->fetch_field($query, 'newann');
                    }
                } else {
                    $newposts = 0;
                    $newthreads = 0;
                    $newann = 0;
                }

                // Make the text
                if ($newann == 1) {
                    $this->lang->new_announcements = $this->lang->new_announcement;
                } else {
                    $this->lang->new_announcements = $this->lang->sprintf($this->lang->new_announcements, $newann);
                }
                if ($newthreads == 1) {
                    $this->lang->new_threads = $this->lang->new_thread;
                } else {
                    $this->lang->new_threads = $this->lang->sprintf($this->lang->new_threads, $newthreads);
                }
                if ($newposts == 1) {
                    $this->lang->new_posts = $this->lang->new_post;
                } else {
                    $this->lang->new_posts = $this->lang->sprintf($this->lang->new_posts, $newposts);
                }
            } else {
                $this->lang->guest_welcome_registration = $this->lang->sprintf(
                    $this->lang->guest_welcome_registration,
                    $this->bb->settings['bburl'] . '/member/register'
                );
                $this->user->username = $this->lang->guest;
                switch ($this->bb->settings['username_method']) {
                    case 0:
                        $username = $this->lang->username;
                        break;
                    case 1:
                        $username = $this->lang->username1;
                        break;
                    case 2:
                        $username = $this->lang->username2;
                        break;
                    default:
                        $username = $this->lang->username;
                        break;
                }
                $this->view->offsetSet('username', $username);
            }
            $this->lang->welcome = $this->lang->sprintf($this->lang->welcome, $this->user->username);
        }

        $messages = ['pms_total' => 0, 'pms_unread' => 0];
        // Private messages box
        if ($this->bb->settings['portal_showpms'] != 0) {
            if ($this->user->uid != 0 &&
                $this->user->receivepms != 0 &&
                $this->bb->usergroup['canusepms'] != 0 &&
                $this->bb->settings['enablepms'] != 0
            ) {
                $messages['pms_total'] = $this->user->pms_total;
                $messages['pms_unread'] = $this->user->pms_unread;

                $this->lang->pms_received_new = $this->lang->sprintf(
                    $this->lang->pms_received_new,
                    $this->user->username,
                    $messages['pms_unread']
                );
            }
        }
        $this->view->offsetSet('messages', $messages);

        $stats = ['numthreads' => 0, 'numposts' => 0, 'numusers' => 0];
        $newestmember = '';
        // Get Forum Statistics
        if ($this->bb->settings['portal_showstats'] != 0) {
            $stats = $this->cache->read('stats');
            $stats['numthreads'] = $this->parser->formatNumber($stats['numthreads']);
            $stats['numposts'] = $this->parser->formatNumber($stats['numposts']);
            $stats['numusers'] = $this->parser->formatNumber($stats['numusers']);
            if (!$stats['lastusername']) {
                $newestmember = '<strong>' . $this->lang->nobody . '</strong>';
            } else {
                $newestmember = $this->user->build_profile_link($stats['lastusername'], $stats['lastuid']);
            }
        }
        $this->view->offsetSet('newestmember', $newestmember);
        $this->view->offsetSet('stats', $stats);

        $whosonline = '';
        // Get the online users
        if ($this->bb->settings['portal_showwol'] != 0 &&
            $this->bb->usergroup['canviewonline'] != 0
        ) {
            if ($this->bb->settings['wolorder'] == 'username') {
                $order_by = 'u.username ASC';
                $order_by2 = 's.time DESC';
            } else {
                $order_by = 's.time DESC';
                $order_by2 = 'u.username ASC';
            }

            $timesearch = TIME_NOW - $this->bb->settings['wolcutoff'];
            $comma = '';
            $guestcount = $membercount = $botcount = $anoncount = 0;
            $onlinemembers = '';
            $doneusers = [];
            $query = $this->db->query('
		SELECT s.sid, s.ip, s.uid, s.time, s.location, u.username, u.invisible, u.usergroup, u.displaygroup
		FROM ' . TABLE_PREFIX . 'sessions s
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (s.uid=u.uid)
		WHERE s.time>'$timesearch'
		ORDER BY {$order_by}, {$order_by2}
	");
            while ($user = $this->db->fetch_array($query)) {
                // Create a key to test if this user is a search bot.
                $botkey = my_strtolower(str_replace('bot=', '', $user['sid']));

                if ($user['uid'] == '0') {
                    ++$guestcount;
                } elseif (my_strpos($user['sid'], 'bot=') !== false && $this->session->bots[$botkey]) {
                    // The user is a search bot.
                    $onlinemembers .= $comma . $this->user->format_name($this->session->bots[$botkey], $this->session->botgroup);
                    $comma = $this->lang->comma;
                    ++$botcount;
                } else {
                    if (empty($doneusers[$user['uid']]) || $doneusers[$user['uid']] < $user['time']) {
                        ++$membercount;

                        $doneusers[$user['uid']] = $user['time'];

                        // If the user is logged in anonymously, update the count for that.
                        if ($user['invisible'] == 1) {
                            ++$anoncount;
                        }

                        if ($user['invisible'] == 1) {
                            $invisiblemark = '*';
                        } else {
                            $invisiblemark = '';
                        }

                        if (($user['invisible'] == 1 &&
                                ($this->bb->usergroup['canviewwolinvis'] == 1 ||
                                    $user['uid'] == $this->user->uid)) ||
                            $user['invisible'] != 1
                        ) {
                            $user['username'] = $this->user->format_name($user['username'], $user['usergroup'], $user['displaygroup']);
                            $user['profilelink'] = get_profile_link($user['uid']);
                            $onlinemembers .= "{$comma}<a href=\"{$user['profilelink']}\">{$user['username']}</a>{$invisiblemark}";
                            $comma = $this->lang->comma;
                        }
                    }
                }
            }
            $this->view->offsetSet('onlinemembers', $onlinemembers);

            $onlinecount = $membercount + $guestcount + $botcount;

            // If we can see invisible users add them to the count
            if ($this->bb->usergroup['canviewwolinvis'] == 1) {
                $onlinecount += $anoncount;
            }

            // If we can't see invisible users but the user is an invisible user incriment the count by one
            if ($this->bb->usergroup['canviewwolinvis'] != 1 &&
                isset($this->user->invisible) &&
                $this->user->invisible == 1
            ) {
                ++$onlinecount;
            }

            // Most users online
            $mostonline = $this->cache->read('mostonline');
            if ($onlinecount > $mostonline['numusers']) {
                $time = TIME_NOW;
                $mostonline['numusers'] = $onlinecount;
                $mostonline['time'] = $time;
                $this->cache->update('mostonline', $mostonline);
            }
            $recordcount = $mostonline['numusers'];
            $recorddate = $this->time->formatDate('relative', $mostonline['time']);

            if ($onlinecount == 1) {
                $this->lang->online_users = $this->lang->online_user;
            } else {
                $this->lang->online_users = $this->lang->sprintf($this->lang->online_users, $onlinecount);
            }
            $this->lang->online_counts = $this->lang->sprintf($this->lang->online_counts, $membercount, $guestcount);
        }

        $threadlist = [];
        // Latest forum discussions
        if ($this->bb->settings['portal_showdiscussions'] != 0 &&
            $this->bb->settings['portal_showdiscussionsnum'] &&
            $this->bb->settings['portal_excludediscussion'] != -1
        ) {
            $excludeforums = '';
            if (!empty($this->bb->settings['portal_excludediscussion'])) {
                $excludeforums = "AND t.fid NOT IN ({$this->bb->settings['portal_excludediscussion']})";
            }

            $query = $this->db->query('
		SELECT t.tid, t.fid, t.uid, t.lastpost, t.lastposteruid, t.lastposter, t.subject, t.replies, t.views, u.username
		FROM ' . TABLE_PREFIX . 'threads t
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=t.uid)
		WHERE 1=1 {$excludeforums}{$tunviewwhere} AND t.visible='1' AND t.closed NOT LIKE 'moved|%'
		ORDER BY t.lastpost DESC
		LIMIT 0, " . $this->bb->settings['portal_showdiscussionsnum']);
            while ($thread = $this->db->fetch_array($query)) {
                $forumpermissions[$thread['fid']] = $this->forum->forum_permissions($thread['fid']);

                if ($thread['fid'] == 0) {
                    continue;
                }
                // Make sure we can view this thread
                if (isset($forumpermissions[$thread['fid']]['canonlyviewownthreads']) &&
                    $forumpermissions[$thread['fid']]['canonlyviewownthreads'] == 1 &&
                    $thread['uid'] != $this->user->uid
                ) {
                    continue;
                }

                // Don't link to guest's profiles (they have no profile).
                if ($thread['lastposteruid'] == 0) {
                    $lastposterlink = $thread['lastposter'];
                } else {
                    $lastposterlink = $this->user->build_profile_link($thread['lastposter'], $thread['lastposteruid']);
                }
                if (my_strlen($thread['subject']) > 25) {
                    $thread['subject'] = my_substr($thread['subject'], 0, 25) . '...';
                }
                $threadlist[] = [
                    'altbg' => alt_trow(),
                    'threadlink' => get_thread_link($thread['tid']),
                    'subject' => htmlspecialchars_uni($this->parser->parse_badwords($thread['subject'])),
                    'forumlink' => $this->forum->get_forum_link($thread['fid']),
                    'forumname' => $this->bb->forum_cache[$thread['fid']]['name'],
                    'lastpostlink' => get_thread_link($thread['tid'], 0, 'lastpost'),
                    'lastposterlink' => $lastposterlink,
                    'lastpostdate' => $this->time->formatDate('relative', $thread['lastpost']),
                    'replies' => $this->parser->formatNumber($thread['replies']),
                    'views' => $this->parser->formatNumber($thread['views'])
                ];
            }
        }
        $this->view->offsetSet('threadlist', $threadlist);

        $announcements = [];
        if (!empty($this->bb->settings['portal_announcementsfid'])) {
            // Get latest news announcements
            // Build where clause
            $annfidswhere = '';
            $announcementcount = 0;
            if ($this->bb->settings['portal_announcementsfid'] != -1) {
                // First validate announcement fids:
                $announcementsfids = explode(',', (string)$this->bb->settings['portal_announcementsfid']);
                if (is_array($announcementsfids)) {
                    foreach ($announcementsfids as $fid) {
                        $fid_array[] = (int)$fid;
                    }
                    unset($fid);

                    $announcementsfids = implode(',', $fid_array);

                    $annfidswhere = " AND t.fid IN ($announcementsfids)";
                }
            }

            // And get them!
            foreach ($this->bb->forum_cache as $fid => $f) {
                if (empty($fid_array) || (is_array($fid_array) && in_array($fid, $fid_array))) {
                    $forum[$fid] = $f;
                }
            }

            $query = $this->db->simple_select(
                'threads t',
                'COUNT(t.tid) AS threads',
                "t.visible='1'{$annfidswhere}{$tunviewwhere} AND t.closed NOT LIKE 'moved|%'",
                ['limit' => 1]
            );
            $announcementcount = $this->db->fetch_field($query, 'threads');

            $numannouncements = (int)$this->bb->settings['portal_numannouncements'];
            if (!$numannouncements) {
                $numannouncements = 10; // Default back to 10
            }

            $page = $this->bb->getInput('page', 0);
            $pages = $announcementcount / $numannouncements;
            $pages = ceil($pages);

            if ($page > $pages || $page <= 0) {
                $page = 1;
            }

            if ($page) {
                $start = ($page - 1) * $numannouncements;
            } else {
                $start = 0;
                $page = 1;
            }

            $this->view->offsetSet('multipage', $this->pagination->multipage($announcementcount, $numannouncements, $page, $file_name));

            $pids = '';
            $tids = '';
            $comma = '';
            $posts = [];
            $attachmentcount = [];
            $query = $this->db->query('
		SELECT p.pid, p.message, p.tid, p.smilieoff, t.attachmentcount
		FROM ' . TABLE_PREFIX . 'posts p
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
		WHERE t.visible='1'{$annfidswhere}{$tunviewwhere} AND t.closed NOT LIKE 'moved|%' AND t.firstpost=p.pid
		ORDER BY t.dateline DESC
		LIMIT {$start}, {$numannouncements}");
            while ($getid = $this->db->fetch_array($query)) {
                $attachmentcount[$getid['tid']] = $getid['attachmentcount'];
                foreach ($attachmentcount as $tid => $attach_count) {
                    if ($attach_count > 0) {
                        $pids .= ",'{$getid['pid']}'";
                    }

                    $posts[$getid['tid']] = $getid;
                }

                $tids .= ",'{$getid['tid']}'";
            }
            if (!empty($posts)) {
                if ($pids != '' && $this->bb->settings['enableattachments'] == 1) {
                    $pids = "pid IN(0{$pids})";
                    // Now lets fetch all of the attachments for these posts
                    $query = $this->db->simple_select('attachments', '*', $pids);
                    while ($attachment = $this->db->fetch_array($query)) {
                        $this->bb->attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
                    }
                }

                if (is_array($forum)) {
                    foreach ($forum as $fid => $forumrow) {
                        $forumpermissions[$fid] = $this->forum->forum_permissions($fid);
                    }
                }

                $icon_cache = $this->cache->read('posticons');

                $query = $this->db->query('
			SELECT t.*, t.username AS threadusername, u.username, u.avatar, u.avatardimensions
			FROM ' . TABLE_PREFIX . 'threads t
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid = t.uid)
			WHERE t.tid IN (0{$tids}){$annfidswhere}{$tunviewwhere} AND t.visible='1' AND t.closed NOT LIKE 'moved|%'
			ORDER BY t.dateline DESC
			LIMIT 0, {$numannouncements}");
                while ($announcement = $this->db->fetch_array($query)) {
                    if ($announcement['fid'] == 0) {
                        continue;
                    }
                    // Make sure we can view this announcement
                    if (isset($forumpermissions[$announcement['fid']]['canonlyviewownthreads']) &&
                        $forumpermissions[$announcement['fid']]['canonlyviewownthreads'] == 1 &&
                        $announcement['uid'] != $this->user->uid
                    ) {
                        continue;
                    }

                    $announcement['message'] = $posts[$announcement['tid']]['message'];
                    $announcement['pid'] = $posts[$announcement['tid']]['pid'];
                    $announcement['smilieoff'] = $posts[$announcement['tid']]['smilieoff'];
                    $announcement['threadlink'] = get_thread_link($announcement['tid']);
                    $announcement['forumlink'] = $this->forum->get_forum_link($announcement['fid']);
                    $announcement['forumname'] = $this->bb->forum_cache[$announcement['fid']]['name'];

                    $announcement['username'] = htmlspecialchars_uni($announcement['username']);
                    $announcement['threadusername'] = htmlspecialchars_uni($announcement['threadusername']);

                    if ($announcement['uid'] == 0) {
                        $profilelink = $announcement['threadusername'];
                    } else {
                        $profilelink = $this->user->build_profile_link($announcement['username'], $announcement['uid']);
                    }

                    if (!$announcement['username']) {
                        $announcement['username'] = $announcement['threadusername'];
                    }
                    $announcement['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($announcement['subject']));
                    if ($announcement['icon'] > 0 && $icon_cache[$announcement['icon']]) {
                        $icon = $icon_cache[$announcement['icon']];
                        $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
                        $icon['path'] = htmlspecialchars_uni($icon['path']);
                        $icon['name'] = htmlspecialchars_uni($icon['name']);
                        $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" title=\"{$icon['name']}\" />";
                    } else {
                        $icon = '&nbsp;';
                    }

                    $useravatar = $this->user->format_avatar($announcement['avatar'], $announcement['avatardimensions']);
                    $anndate = $this->time->formatDate('relative', $announcement['dateline']);

                    if ($announcement['replies']) {
                        $numcomments = "- <a href=\"{$announcement['threadlink']}\">{$this->lang->replies}</a> ({$announcement['replies']})";
                    } else {
                        $numcomments = '- ' . $this->lang->no_replies;
                        $lastcomment = '';
                    }

                    $senditem = '';
                    if ($this->user->uid > 0 && $this->bb->usergroup['cansendemail'] == 1) {
                        $senditem = "&nbsp;<a href=\"{$this->bb->settings['bburl']}/sendthread?tid={$announcement['tid']}\"><img src=\"{$this->bb->theme['imgdir']}/send.png\" alt=\"{$this->lang->send_to_friend}\" title=\"{$this->lang->send_to_friend}\" /></a>";
                    }

                    $this->plugins->runHooks('portal_announcement');

                    $parser_options = [
                        'allow_html' => $forum[$announcement['fid']]['allowhtml'],
                        'allow_mycode' => $forum[$announcement['fid']]['allowmycode'],
                        'allow_smilies' => $forum[$announcement['fid']]['allowsmilies'],
                        'allow_imgcode' => $forum[$announcement['fid']]['allowimgcode'],
                        'allow_videocode' => $forum[$announcement['fid']]['allowvideocode'],
                        'filter_badwords' => 1
                    ];
                    if ($announcement['smilieoff'] == 1) {
                        $parser_options['allow_smilies'] = 0;
                    }

                    if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                        ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
                    ) {
                        $parser_options['allow_imgcode'] = 0;
                    }

                    if (($this->user->showvideos != 1 && $this->user->uid != 0) ||
                        ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
                    ) {
                        $parser_options['allow_videocode'] = 0;
                    }

                    $message = $this->parser->parse_message($announcement['message'], $parser_options);

                    $post['attachments'] = '';
                    if ($this->bb->settings['enableattachments'] == 1 &&
                        isset($this->bb->attachcache[$announcement['pid']]) &&
                        is_array($this->bb->attachcache[$announcement['pid']])
                    ) { // This post has 1 or more attachments
                        $validationcount = 0;
                        $id = $announcement['pid'];
                        $post['attachmentlist'] = $post['thumblist'] =
                        $post['imagelist'] = $post['attachedthumbs'] =
                        $post['attachedimages'] = '';
                        foreach ($this->bb->attachcache[$id] as $aid => $attachment) {
                            if ($attachment['visible']) { // There is an attachment thats visible!
                                $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                                $attachment['filesize'] = $this->parser->friendlySize($attachment['filesize']);
                                $ext = get_extension($attachment['filename']);
                                if ($ext == 'jpeg' || $ext == 'gif' || $ext == 'bmp' || $ext == 'png' || $ext == 'jpg') {
                                    $isimage = true;
                                } else {
                                    $isimage = false;
                                }
                                $attachment['icon'] = $this->upload->get_attachment_icon($ext);
                                $this->attachdate = $this->time->formatDate('relative', $attachment['dateuploaded']);
                                // Support for [attachment=id] code
                                if (stripos($message, '[attachment=' . $attachment['aid'] . ']') !== false) {
                                    if ($attachment['thumbnail'] != 'SMALL' && $attachment['thumbnail'] != '') { // We have a thumbnail to show (and its not the 'SMALL' enough image
                                        $attbit = "
                                        <a href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" target=\"_blank\"><img src=\"{$this->bb->settings['bburl']}/attachment?thumbnail={$attachment['aid']}\" class=\"attachment\" alt=\"\" title=\"{$this->lang->postbit_attachment_filename} {$attachment['filename']}
                                        {$this->lang->postbit_attachment_size} {$attachment['filesize']}
                                        {$this->attachdate}\" /></a>&nbsp;&nbsp;&nbsp;";
                                    } elseif ($attachment['thumbnail'] == 'SMALL' &&
                                        $forumpermissions[$announcement['fid']]['candlattachments'] == 1
                                    ) {
                                        // Image is small enough to show - no thumbnail
                                        eval("\$attbit = \"" . $this->templates->get('postbit_attachments_images_image') . "\";");
                                    } else {
                                        // Show standard link to attachment
                                        eval("\$attbit = \"" . $this->templates->get('postbit_attachments_attachment') . "\";");
                                    }
                                    $message = preg_replace('#\[attachment=' . $attachment['aid'] . ']#si', $attbit, $message);
                                } else {
                                    if ($attachment['thumbnail'] != 'SMALL' && $attachment['thumbnail'] != '') { // We have a thumbnail to show
                                        $post['thumblist'] .= "
                                        <a href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" target=\"_blank\"><img src=\"{$this->bb->settings['bburl']}/attachment?thumbnail={$attachment['aid']}\" class=\"attachment\" alt=\"\" title=\"{$this->lang->postbit_attachment_filename} {$attachment['filename']}
                                        {$this->lang->postbit_attachment_size} {$attachment['filesize']}
                                        {$this->attachdate}\" /></a>&nbsp;&nbsp;&nbsp;";
                                        if ($tcount == 5) {
                                            $thumblist .= '<br />';
                                            $tcount = 0;
                                        }
                                        ++$tcount;
                                    } elseif ($attachment['thumbnail'] == 'SMALL' &&
                                        $forumpermissions[$announcement['fid']]['candlattachments'] == 1
                                    ) {
                                        // Image is small enough to show - no thumbnail
                                        eval("\$post['imagelist'] .= \"" . $this->templates->get('postbit_attachments_images_image') . "\";");
                                    } else {
                                        eval("\$post['attachmentlist'] .= \"" . $this->templates->get('postbit_attachments_attachment') . "\";");
                                    }
                                }
                            } else {
                                $validationcount++;
                            }
                        }
                        if ($post['thumblist']) {
                            eval("\$post['attachedthumbs'] = \"" . $this->templates->get('postbit_attachments_thumbnails') . "\";");
                        }
                        if ($post['imagelist']) {
                            eval("\$post['attachedimages'] = \"" . $this->templates->get('postbit_attachments_images') . "\";");
                        }
                        if ($post['attachmentlist'] || $post['thumblist'] || $post['imagelist']) {
                            eval("\$post['attachments'] = \"" . $this->templates->get('postbit_attachments') . "\";");
                        }
                    }
                    $announcements[] = [
                        'icon' => $icon,
                        'threadlink' => $announcement['threadlink'],
                        'subject' => $announcement['subject'],
                        'profilelink' => $profilelink,
                        'anndate' => $anndate,
                        'forumlink' => $announcement['forumlink'],
                        'forumname' => $announcement['forumname'],
                        'numcomments' => $numcomments,
                        'useravatar' => $useravatar,
                        'message' => $message,
                        'attachments' => $post['attachments'],
                        'tid' => $announcement['tid'],
                        'senditem' => $senditem
                    ];
                    unset($post);
                }
            }
        }
        $this->view->offsetSet('announcements', $announcements);

        $this->plugins->runHooks('portal_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/portal.html.twig');
    }
}
