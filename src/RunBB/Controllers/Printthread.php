<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Printthread extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('printthread');
        $this->lang->load('archive');

        $this->plugins->runHooks('printthread_start');

        $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));

        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $this->plugins->runHooks('printthread_start');

        $thread['threadprefix'] = $thread['displaystyle'] = '';
        if ($thread['prefix']) {
            $threadprefix = $this->thread->build_prefixes($thread['prefix']);
            if (!empty($threadprefix)) {
                $thread['threadprefix'] = $threadprefix['prefix'];
                $thread['displaystyle'] = $threadprefix['displaystyle'];
            }
        }

        $thread['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($thread['subject']));

        $this->fid = $thread['fid'];
        $tid = $thread['tid'];

        // Is the currently logged in user a moderator of this forum?
        $ismod = $this->user->is_moderator($this->fid);

        // Make sure we are looking at a real thread here.
        if (($thread['visible'] != 1 && $ismod == false) || ($thread['visible'] > 1 && $ismod == true)) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        // Get forum info
        $this->forums = $this->forum->get_forum($this->fid);
        if (!$this->forums) {
            $this->bb->error($this->lang->error_invalidforum);
        }

        $this->view->offsetSet('breadcrumb', $this->makeprintablenav());

        $parentsexp = explode(',', $this->forums['parentlist']);
        $numparents = count($parentsexp);
        $tdepth = '-';
        for ($i = 0; $i < $numparents; ++$i) {
            $tdepth .= '-';
        }
        $this->view->offsetSet('tdepth', $tdepth);
        $forumpermissions = $this->forum->forum_permissions($this->forums['fid']);

        if ($this->forums['type'] != 'f') {
            $this->bb->error($this->lang->error_invalidforum);
        }
        if ($forumpermissions['canview'] == 0 ||
            $forumpermissions['canviewthreads'] == 0 ||
            (isset($forumpermissions['canonlyviewownthreads']) &&
                $forumpermissions['canonlyviewownthreads'] != 0 &&
                $thread['uid'] != $this->user->uid)
        ) {
            return $this->bb->error_no_permission();
        }

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($this->forums['fid']);

        $page = $this->bb->getInput('page', 0);

        // Paginate this thread
        if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
            $this->bb->settings['postsperpage'] = 20;
        }
        $perpage = $this->bb->settings['postsperpage'];
        $postcount = (int)$thread['replies'] + 1;
        $pages = ceil($postcount / $perpage);

        if ($page > $pages) {
            $page = 1;
        }
        if ($page > 0) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        if ($postcount > $perpage) {
            $multipage = $this->printthread_multipage($postcount, $perpage, $page, $this->bb->settings['bburl'] . '/printthread?tid=' . $tid);
        } else {
            $multipage = '';
        }
        $this->view->offsetSet('multipage', $multipage);

        $thread['threadlink'] = get_thread_link($tid);

        $postrows = [];
//        if ($this->user->is_moderator($this->forums['fid'], 'canviewunapprove')) {
//            $visible = "AND (p.visible='0' OR p.visible='1')";
//        } else {
//            $visible = "AND p.visible='1'";
//        }
//        $query = $this->db->query('
//	SELECT u.*, u.username AS userusername, p.*
//	FROM ' . TABLE_PREFIX . 'posts p
//	LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
//	WHERE p.tid='$tid' {$visible}
//	ORDER BY p.dateline
//	LIMIT {$start}, {$perpage}
//");
        $pr = \RunBB\Models\Post::where(function ($q) use ($tid) {
            $q->where('posts.tid', '=', $tid);
            if ($this->user->is_moderator($this->forums['fid'], 'canviewunapprove')) {
                $q->where('posts.visible', '=', 0);
                $q->where('posts.visible', '=', 1);
            } else {
                $q->where('posts.visible', '=', 1);
            }
        })
            ->leftJoin('users', 'users.uid', '=', 'posts.uid')
            ->orderBy('posts.dateline')
            ->skip($start)->take($perpage)
            ->get(['users.*', 'users.username AS userusername', 'posts.*'])
            ->toArray();

//        while ($postrow = $this->db->fetch_array($query)) {
        foreach ($pr as $postrow) {
            if ($postrow['userusername']) {
                $postrow['username'] = $postrow['userusername'];
            }
            $postrow['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($postrow['subject']));
            $postrow['date'] = $this->time->formatDate($this->bb->settings['dateformat'], $postrow['dateline'], null, 0);
            $postrow['profilelink'] = $this->user->build_profile_link($postrow['username'], $postrow['uid']);
            $parser_options = [
                'allow_html' => $this->forums['allowhtml'],
                'allow_mycode' => $this->forums['allowmycode'],
                'allow_smilies' => $this->forums['allowsmilies'],
                'allow_imgcode' => $this->forums['allowimgcode'],
                'allow_videocode' => $this->forums['allowvideocode'],
                'me_username' => $postrow['username'],
                'shorten_urls' => 0,
                'filter_badwords' => 1
            ];
            if ($postrow['smilieoff'] == 1) {
                $parser_options['allow_smilies'] = 0;
            }

            if ((isset($this->user->showimages) && $this->user->showimages != 1 && $this->user->uid != 0) ||
                ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
            ) {
                $parser_options['allow_imgcode'] = 0;
            }

            if ((isset($this->user->showvideos) && $this->user->showvideos != 1 && $this->user->uid != 0) ||
                ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
            ) {
                $parser_options['allow_videocode'] = 0;
            }

            $postrow['message'] = $this->parser->parse_message($postrow['message'], $parser_options);
            $postrows[] = $postrow;
            $this->plugins->runHooks('printthread_post');
        }
        $this->view->offsetSet('postrows', $postrows);
        $this->view->offsetSet('thread', $thread);

        $this->plugins->runHooks('printthread_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/printthread.html.twig');
    }

    /**
     * @param int $pid
     * @param string $depth
     *
     * @return string
     */
    private function makeprintablenav($pid = 0, $depth = '--')
    {
        if (empty($this->bb->pforumcache)) {
            $parlist = $this->forum->build_parent_list($this->fid, 'fid', 'OR', $this->forums['parentlist']);
//            $query = $this->db->simple_select('forums', 'name, fid, pid', "$parlist", array('order_by' => 'pid, disporder'));
            $fn = \RunBB\Models\Forum::whereRaw($parlist)//FIXME rebuild without raw
                ->orderBy('pid')->orderBy('disporder')
                ->get(['name', 'fid', 'pid'])
                ->toArray();

//            while ($forumnav = $this->db->fetch_array($query)) {
            foreach ($fn as $forumnav) {
                $this->bb->pforumcache[$forumnav['pid']][$forumnav['fid']] = $forumnav;
            }
            unset($forumnav);
        }
        $forums = '';
        if (is_array($this->bb->pforumcache[$pid])) {
            foreach ($this->bb->pforumcache[$pid] as $key => $forumnav) {
                $forums .= '+' . $depth . " {$this->lang->forum} {$forumnav['name']} (<i>" . $this->bb->settings['bburl'] . '/' . $this->forum->get_forum_link($forumnav['fid']) . "</i>)<br />\n";
                if (!empty($this->bb->pforumcache[$forumnav['fid']])) {
                    $newdepth = $depth . '-';
                    $forums .= $this->makeprintablenav($forumnav['fid'], $newdepth);
                }
            }
        }
        return $forums;
    }

    /**
     * Output multipage navigation.
     *
     * @param int $count The total number of items.
     * @param int $perpage The items per page.
     * @param int $current_page The current page.
     * @param string $url The URL base.
     *
     * @return string
     */
    private function printthread_multipage($count, $perpage, $current_page, $url)
    {
        $multipage = '';
        if ($count > $perpage) {
            $pages = $count / $perpage;
            $pages = ceil($pages);

            $mppage = null;
            for ($page = 1; $page <= $pages; ++$page) {
                if ($page == $current_page) {
                    $mppage .= "<strong>{$page}</strong>";
                } else {
                    $mppage .= "<a href=\"{$url}&page={$page}\">{$page}</a>";
                }
            }
            $multipage = "<div class=\"multipage\">{$this->lang->pages} <strong>{$this->lang->archive_pages}</strong> {$mppage}</div>";
        }
        return $multipage;
    }
}
