<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Ratethread extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->lang->load('ratethread');

        $tid = $this->bb->getInput('tid', 0);
        $thread = $this->thread->get_thread($tid);
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        // Is the currently logged in user a moderator of this forum?
        $ismod = $this->user->is_moderator($thread['fid']);

        // Make sure we are looking at a real thread here.
        if (($thread['visible'] != 1 && $ismod == false) || ($thread['visible'] > 1 && $ismod == true)) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        if ($thread['uid'] == $this->user->uid) {
            $this->bb->error($this->lang->error_cannotrateownthread);
        }

        $forumpermissions = $this->forum->forum_permissions($thread['fid']);
        if ($forumpermissions['canview'] == 0 ||
        $forumpermissions['canratethreads'] == 0 ||
        $this->bb->usergroup['canratethreads'] == 0 ||
        $this->bb->settings['allowthreadratings'] == 0 ||
        (isset($forumpermissions['canonlyviewownthreads']) &&
        $forumpermissions['canonlyviewownthreads'] != 0)) {
            return $this->bb->error_no_permission();
        }

        // Get forum info
        $fid = $thread['fid'];
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        }

        // Get forum info
        $forum = $this->forum->get_forum($fid);
        if (!$forum) {
            $this->bb->error($this->lang->error_invalidforum);
        } else {
            // Is our forum closed?
            if ($forum['open'] == 0) {
                // Doesn't look like it is
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
        }

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($forum['fid']);

        if ($forum['allowtratings'] == 0) {
            return $this->bb->error_no_permission();
        }
        $this->bb->input['rating'] = $this->bb->getInput('rating', 0);
        if ($this->bb->input['rating'] < 1 || $this->bb->input['rating'] > 5) {
            $this->bb->error($this->lang->error_invalidrating);
        }
        $this->plugins->runHooks('ratethread_start');

        if ($this->user->uid != 0) {
            $whereclause = "uid='{$this->user->uid}'";
        } else {
            $whereclause = 'ipaddress='.$this->session->ipaddress;
        }
        $query = $this->db->simple_select('threadratings', '*', "{$whereclause} AND tid='{$tid}'");
        $ratecheck = $this->db->fetch_array($query);

        if ($ratecheck['rid'] || isset($this->bb->cookies['mybbratethread'][$tid])) {
            $this->bb->error($this->lang->error_alreadyratedthread);
        } else {
            $this->plugins->runHooks('ratethread_process');

            $this->db->write_query('
		UPDATE '.TABLE_PREFIX."threads
		SET numratings=numratings+1, totalratings=totalratings+'{$this->bb->input['rating']}'
		WHERE tid='{$tid}'
	");
            if ($this->user->uid != 0) {
                  $insertarray = [
                    'tid' => $tid,
                    'uid' => $this->user->uid,
                    'rating' => $this->bb->input['rating'],
                    'ipaddress' => $this->session->ipaddress
                  ];
                  $this->db->insert_query('threadratings', $insertarray);
            } else {
                  $insertarray = [
                    'tid' => $tid,
                    'rating' => $this->bb->input['rating'],
                    'ipaddress' => $this->session->ipaddress
                  ];
                  $this->db->insert_query('threadratings', $insertarray);
                  $time = TIME_NOW;
                  $this->bb->my_setcookie("mybbratethread[{$tid}]", $this->bb->input['rating']);
            }
        }
        $this->plugins->runHooks('ratethread_end');

        if (!empty($this->bb->input['ajax'])) {
            $json = ['success' => $this->lang->rating_added];
            $query = $this->db->simple_select('threads', 'totalratings, numratings', "tid='$tid'", ['limit' => 1]);
            $fetch = $this->db->fetch_array($query);
            $width = 0;
            if ($fetch['numratings'] >= 0) {
                $averagerating = (float)round($fetch['totalratings']/$fetch['numratings'], 2);
                $width = (int)round($averagerating)*20;
                $fetch['numratings'] = (int)$fetch['numratings'];
                $ratingvotesav = $this->lang->sprintf($this->lang->rating_votes_average, $fetch['numratings'], $averagerating);
                $json = $json + ['average' => $ratingvotesav];
            }
            $json = $json + ['width' => $width];

            @header("Content-type: application/json; charset={$this->lang->settings['charset']}");
            echo json_encode($json);
            exit;
        }

        $this->bb->redirect(get_thread_link($thread['tid']), $this->lang->redirect_threadrated);
    }
}
