<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Report extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        $this->lang->load('report');

        if (!$this->user->uid) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('report_start');

        $report = [];
        $verified = false;
        $report_type = 'post';
        $error = $report_type_db = '';

        if (!empty($this->bb->input['type'])) {
            $report_type = htmlspecialchars_uni($this->bb->getInput('type', ''));
        }

        $report_title = $this->lang->report_content;
        $report_string = "report_reason_{$report_type}";

        if (isset($this->lang->$report_string)) {
            $report_title = $this->lang->$report_string;
        }

        $id = 0;
        if ($report_type == 'post') {
            if ($this->bb->usergroup['canview'] == 0) {
                return $this->bb->error_no_permission();
            }

            // Do we have a valid post?
            $post = $this->post->get_post($this->bb->getInput('pid', 0));

            if (!$post) {
                $error = $this->lang->error_invalid_report;
            } else {
                $id = $post['pid'];
                $id2 = $post['tid'];
                $report_type_db = "(type = 'post' OR type = '')";

                // Check for a valid forum
                $forum = $this->forum->get_forum($post['fid']);

                if (!isset($forum['fid'])) {
                    $error = $this->lang->error_invalid_report;
                } else {
                    $verified = true;
                }

                // Password protected forums ......... yhummmmy!
                $id3 = $forum['fid'];
                $this->forum->check_forum_password($forum['parentlist']);
            }
        } elseif ($report_type == 'profile') {
            $user = $this->user->get_user($this->bb->getInput('pid', 0));

            if (!isset($user['uid'])) {
                $error = $this->lang->error_invalid_report;
            } else {
                $id2 = $id3 = 0; // We don't use these on the profile
                $id = $user['uid']; // id is the profile user
                $permissions = $this->user->user_permissions($user['uid']);

                if (empty($permissions['canbereported'])) {
                    $error = $this->lang->error_invalid_report;
                } else {
                    $verified = true;
                    $report_type_db = "type = 'profile'";
                }
            }
        } elseif ($report_type == 'reputation') {
            // Any member can report a reputation comment but let's make sure it exists first
            $query = $this->db->simple_select('reputation', '*', "rid = '" . $this->bb->getInput('pid', 0) . "'");

            if (!$this->db->num_rows($query)) {
                $error = $this->lang->error_invalid_report;
            } else {
                $verified = true;
                $reputation = $this->db->fetch_array($query);

                $id = $reputation['rid']; // id is the reputation id
                $id2 = $reputation['adduid']; // id2 is the user who gave the comment
                $id3 = $reputation['uid']; // id3 is the user who received the comment

                $report_type_db = "type = 'reputation'";
            }
        }

        $this->plugins->runHooks('report_type');

        // Check for an existing report
        if (!empty($report_type_db)) {
            $query = $this->db->simple_select('reportedcontent', '*', "reportstatus != '1' AND id = '{$id}' AND {$report_type_db}");

            if ($this->db->num_rows($query)) {
                // Existing report
                $report = $this->db->fetch_array($query);
                $report['reporters'] = my_unserialize($report['reporters']);

                if ($this->user->uid == $report['uid'] ||
                    is_array($report['reporters']) &&
                    in_array($this->user->uid, $report['reporters'])
                ) {
                    $error = $this->lang->success_report_voted;
                }
            }
        }

        $this->bb->input['action'] = $this->bb->getInput('action', '');

        if (empty($error) &&
            $verified == true &&
            $this->bb->input['action'] == 'do_report' &&
            $this->bb->request_method == 'post'
        ) {
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $this->plugins->runHooks('report_do_report_start');

            // Is this an existing report or a new offender?
            if (!empty($report)) {
                // Existing report, add vote
                $report['reporters'][] = $this->user->uid;
                $this->modcp->update_report($report);

                $this->plugins->runHooks('report_do_report_end');

                return $this->view->render($response, '@forum/report_thanks.html.twig', ['title' => $report_title]);
            } else {
                // Bad user!
                $new_report = [
                    'id' => $id,
                    'id2' => $id2,
                    'id3' => $id3,
                    'uid' => $this->user->uid
                ];

                // Figure out the reason
                $reason = trim($this->bb->getInput('reason', ''));

                if ($reason == 'other') {
                    // Replace the reason with the user comment
                    $reason = trim($this->bb->getInput('comment', ''));
                } else {
                    $report_reason_string = "report_reason_{$reason}";
                    $reason = "\n" . $this->lang->$report_reason_string;
                }

                if (my_strlen($reason) < 3) {
                    $error = $this->lang->error_report_length;
                }

                if (empty($error)) {
                    $new_report['reason'] = $reason;
                    $this->modcp->add_report($new_report, $report_type);

                    $this->plugins->runHooks('report_do_report_end');

                    return $this->view->render($response, '@forum/report_thanks.html.twig', ['title' => $report_title]);
                }
            }
        }

        if (!empty($error) || $verified == false) {
            $this->bb->input['action'] = '';

            if ($verified == false && empty($error)) {
                $error = $this->lang->error_invalid_report;
            }
        }

        if (!$this->bb->input['action']) {
            $report_reasons = '';
            if (!empty($error)) {
                if (isset($this->bb->input['no_modal'])) {
                    $report_reasons = "
                    <table border=\"0\" cellspacing=\"{$this->bb->theme['borderwidth']}\" cellpadding=\"{$this->bb->theme['tablespace']}\" class=\"tborder\">
                        <tr>
                            <td class=\"thead\" colspan=\"2\"><strong>{$report_title}</strong></td>
                        </tr>
                        <tr>
                            <td class=\"tcat\" colspan=\"2\">{$this->lang->report_to_mod}</td>
                        </tr>
                        <tr>
                            <td colspan=\"2\" class=\"trow1\">{$error}</td>
                        </tr>
                    </table>";
                } else {
                    $report_reasons = "
                    <tr>
                        <td colspan=\"2\" class=\"trow1\">{$error}</td>
                    </tr>";
                }
            } else {
                if (!empty($report)) {
                    $report_reasons = "
                    <tr>
                        <td class=\"trow1\">{$this->lang->error_report_duplicate}</td>
                    </tr>
                    <tr>
                        <td class=\"tfoot\"><input type=\"submit\" class=\"button\" value=\"{$report_title}\" /></td>
                    </tr>";
                } else {
                    $report_reasons = "
                    <tr>
                        <td class=\"trow1\" align=\"left\" style=\"width: 25%\"><span class=\"smalltext\"><strong>{$this->lang->report_reason}</strong></span></td>
                        <td class=\"trow1\" align=\"left\">
                            <select name=\"reason\" id=\"report_reason\">
                            <option value=\"rules\">{$this->lang->report_reason_rules}</option>
                            <option value=\"bad\">{$this->lang->report_reason_bad}</option>
                            <option value=\"spam\">{$this->lang->report_reason_spam}</option>
                            <option value=\"wrong\">{$this->lang->report_reason_wrong}</option>
                            <option value=\"other\">{$this->lang->report_reason_other}</option>
                            </select>
                        </td>
                    </tr>
                    <tr id=\"reason\">
                        <td class=\"trow2\">&nbsp;</td>
                        <td class=\"trow2\" align=\"left\">
                            <div>{$this->lang->report_reason_other_description}</div>
                            <input type=\"text\" class=\"textbox\" name=\"comment\" size=\"40\" maxlength=\"250\" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" class=\"tfoot\"><input type=\"submit\" class=\"button\" value=\"{$report_title}\" /></td>
                    </tr>";
                }
            }

            if (isset($this->bb->input['no_modal'])) {
                echo $report_reasons;
                exit;
            }
            $data = [
                'id' => $id,
                'report_type' => $report_type,
                'report_title' => $report_title,
                'report_reasons' => $report_reasons
            ];

            $this->plugins->runHooks('report_end');

            $this->view->render($response, '@forum/report.html.twig', ['data' => $data]);
        }
    }
}
