<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Reputation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Add extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        if ($this->common($response) === 'exit') {
            return;
        }

        $this->plugins->runHooks('reputation_add_start');

        $this->view->offsetSet('uid', $this->repuser->uid);

        // If we have an existing reputation for this user, the user can modify or delete it.
        $this->bb->showDeleteBaton = false;
        if (!empty($this->existing_reputation['uid'])) {
            $vote_title = $this->lang->sprintf($this->lang->update_reputation_vote, $this->repuser->username);
            $vote_button = $this->lang->update_vote;
            $comments = htmlspecialchars_uni($this->existing_reputation['comments']);

            if ($this->bb->usergroup['issupermod'] == 1 ||
                ($this->bb->usergroup['candeletereputations'] == 1 &&
                    $this->existing_reputation['adduid'] == $this->user->uid &&
                    $this->user->uid != 0)
            ) {
                $this->bb->showDeleteBaton = true;
            }
        } // Otherwise we're adding an entirely new reputation for this user.
        else {
            $vote_title = $this->lang->sprintf($this->lang->add_reputation_vote, $this->repuser->username);
            $vote_button = $this->lang->add_vote;
            $comments = '';
        }
        $this->view->offsetSet('vote_title', $vote_title);
        $this->view->offsetSet('vote_button', $vote_button);
        $this->view->offsetSet('comments', $comments);

        $this->lang->user_comments = $this->lang->sprintf($this->lang->user_comments, $this->repuser->username);
        $post_rep_info = '';
        if ($this->bb->getInput('pid', 0)) {
            $post_rep_info = $this->lang->sprintf($this->lang->add_reputation_to_post, $this->repuser->username);
            $this->lang->user_comments = $this->lang->no_comment_needed;
        }
        $this->view->offsetSet('post_rep_info', $post_rep_info);

        // Draw the 'power' options
        if ($this->bb->settings['negrep'] || $this->bb->settings['neurep'] || $this->bb->settings['posrep']) {
            $vote_check = [];
            $positive_power = $negative_power = $neutral_power = '';
            $reputationpower = (int)$this->bb->usergroup['reputationpower'];

            foreach (range(-$this->bb->usergroup['reputationpower'], $this->bb->usergroup['reputationpower']) as $value) {
                $vote_check[$value] = '';
            }

            if (!empty($this->existing_reputation['uid']) && !$this->was_post) {
                $vote_check[$this->existing_reputation['reputation']] = ' selected="selected"';
            }

            if ($this->bb->settings['neurep']) {
                $neutral_power = "<option value=\"0\" class=\"reputation_neutral\" onclick=\"$('#reputation').attr('class', 'reputation_neutral')\"{$vote_check[0]}>{$this->lang->power_neutral}</option>";
            }

            for ($value = 1; $value <= $reputationpower; ++$value) {
                if ($this->bb->settings['posrep']) {
                    $positive_title = $this->lang->sprintf($this->lang->power_positive, '+' . $value);
                    $positive_power = "<option value=\"{$value}\" class=\"reputation_positive\" onclick=\"$('#reputation').attr('class', 'reputation_positive')\"{$vote_check[$value]}>{$positive_title}</option>{$positive_power}";
                }

                if ($this->bb->settings['negrep']) {
                    $negative_title = $this->lang->sprintf($this->lang->power_negative, '-' . $value);
                    $neg_value = "-{$value}";
                    $negative_power .= "<option value=\"{$neg_value}\" class=\"reputation_negative\" onclick=\"$('#reputation').attr('class', 'reputation_negative')\"{$vote_check[$neg_value]}>{$negative_title}</option>";
                }
            }
            $this->view->offsetSet('positive_power', $positive_power);
            $this->view->offsetSet('negative_power', $negative_power);
            $this->view->offsetSet('neutral_power', $neutral_power);

            $this->bb->input['pid'] = $this->bb->getInput('pid', 0);

            $this->plugins->runHooks('reputation_add_end');

            $this->view->render($response, '@forum/Reputation/reputation_add.html.twig');
        } else {
            $this->plugins->runHooks('reputation_add_end_error');

            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_all_rep_disabled];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }
    }
}
