<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Reputation;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{

    protected function init()
    {
        // Load global language phrases
        $this->lang->load('reputation');

        $this->plugins->runHooks('reputation_start');

        // Check if the reputation system is globally disabled or not.
        if ($this->bb->settings['enablereputation'] != 1) {
            $this->bb->error($this->lang->reputation_disabled);
            return 'exit';
        }

        // Does this user have permission to view the board?
        if ($this->bb->usergroup['canview'] != 1) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        // If we have a specified incoming username, validate it and fetch permissions for it
        $this->uid = $this->bb->getInput('uid', 0);
        $this->repuser = $this->user->get_user($this->uid);

        if (!$this->repuser) {
            $this->bb->error($this->lang->add_no_uid);
            return 'exit';
        }
        $this->user_permissions = $this->user->user_permissions($this->uid);
    }

    protected function common($response)
    {
        // This user doesn't have permission to give reputations.
        if ($this->bb->usergroup['cangivereputations'] != 1) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_no_permission];
            if (isset($this->bb->input['nomodal'])) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return 'exit';
        }

        // The user we're trying to give a reputation to doesn't have permission to receive reps.
        if ($this->user_permissions['usereputationsystem'] != 1) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_disabled];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return 'exit';
        }

        // Is this user trying to give themself a reputation?
        if ($this->uid == $this->user->uid) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_yours];
            if (isset($this->bb->input['nomodal'])) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return 'exit';
        }

        // If a post has been given but post ratings have been disabled, set the post to 0.
        // This will mean all subsequent code will think no post was given.
        if ($this->bb->settings['postrep'] != 1) {
            $this->bb->input['pid'] = 0;
        }

        // Check if this user has reached their 'maximum reputations per day' quota
        if ($this->bb->usergroup['maxreputationsday'] != 0 && isset($this->bb->input['action']) &&
            ($this->bb->input['action'] != 'do_add' ||
                ($this->bb->input['action'] == 'do_add' &&
                    empty($this->bb->input['delete'])))
        ) {
            $timesearch = TIME_NOW - (60 * 60 * 24);
            $query = $this->db->simple_select('reputation', '*', "adduid='" . $this->user->uid . "' AND dateline>'$timesearch'");
            $numtoday = $this->db->num_rows($query);

            // Reached the quota - error.
            if ($numtoday >= $this->bb->usergroup['maxreputationsday']) {
                $data = ['header' => $this->lang->error, 'message' => $this->lang->add_maxperday];
                if ($this->bb->input['nomodal']) {
                    $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
                } else {
                    $this->view->render($response, '@forum/message_modal.html.twig', $data);
                }
                return 'exit';
            }
        }

        // Is the user giving too much reputation to another?
        if ($this->bb->usergroup['maxreputationsperuser'] != 0 &&
            ($this->bb->input['action'] != 'do_add' ||
                ($this->bb->input['action'] == 'do_add' &&
                    empty($this->bb->input['delete'])))
        ) {
            $timesearch = TIME_NOW - (60 * 60 * 24);
            $query = $this->db->simple_select('reputation', '*', "uid='" . $this->uid . "' AND dateline>'$timesearch'");
            $numtoday = $this->db->num_rows($query);

            if ($numtoday >= $this->bb->usergroup['maxreputationsperuser']) {
                $data = ['header' => $this->lang->error, 'message' => $this->lang->add_maxperuser];
                if ($this->bb->input['nomodal']) {
                    $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
                } else {
                    $this->view->render($response, '@forum/message_modal.html.twig', $data);
                }
                return 'exit';
            }
        }

        if ($this->bb->getInput('pid', 0)) {
            // Make sure that this post exists, and that the author of the post we're
            // giving this reputation for corresponds with the user the rep is being given to.
            $post = $this->post->get_post($this->bb->getInput('pid', 0));
            if ($post) {
                $thread = $this->thread->get_thread($post['tid']);
                $forum = $this->forum->get_forum($thread['fid']);
                $forumpermissions = $this->forum->forum_permissions($forum['fid']);
                // Post doesn't belong to that user or isn't visible
                if ($this->uid != $post['uid'] || $post['visible'] != 1) {
                    $this->bb->input['pid'] = 0;
                } // Thread isn't visible
                elseif ($thread['visible'] != 1) {
                    $this->bb->input['pid'] = 0;
                } // Current user can't see the forum
                elseif ($forumpermissions['canview'] == 0 ||
                    $forumpermissions['canpostreplys'] == 0 ||
                    $this->user->suspendposting == 1
                ) {
                    $this->bb->input['pid'] = 0;
                } // Current user can't see that thread
                elseif (isset($forumpermissions['canonlyviewownthreads']) &&
                    $forumpermissions['canonlyviewownthreads'] == 1 &&
                    $thread['uid'] != $this->user->uid
                ) {
                    $this->bb->input['pid'] = 0;
                } else {                     // We have the correct post, but has the user given too much reputation to another in the same thread?
                    if ($this->bb->usergroup['maxreputationsperthread'] != 0 &&
                        ($this->bb->input['action'] != 'do_add' ||
                            ($this->bb->input['action'] == 'do_add' &&
                                empty($this->bb->input['delete'])))
                    ) {
                        $timesearch = TIME_NOW - (60 * 60 * 24);
                        $query = $this->db->query('
					SELECT COUNT(p.pid) AS posts
					FROM ' . TABLE_PREFIX . 'reputation r
					LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid = r.pid)
					WHERE r.uid = '{$this->uid}' AND r.adduid = '{$this->user->uid}' AND p.tid = '{$post['tid']}' AND r.dateline > '{$timesearch}'
				");

                        $numtoday = $this->db->fetch_field($query, 'posts');

                        if ($numtoday >= $this->bb->usergroup['maxreputationsperthread']) {
                            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_maxperthread];
                            if ($this->bb->input['nomodal']) {
                                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
                            } else {
                                $this->view->render($response, '@forum/message_modal.html.twig', $data);
                            }
                            return 'exit';
                        }
                    }
                }
            } else {
                $this->bb->input['pid'] = 0;
            }
        }

        $this->bb->rid = 0;

        // Fetch the existing reputation for this user given by our current user if there is one.
        // If multiple reputations is allowed, then this isn't needed
//        if ($this->bb->settings['multirep'] != 1 && $this->bb->getInput('pid', 0) == 0) {
        if ($this->bb->getInput('pid', 0) == 0) {
            $query = $this->db->simple_select('reputation', '*', "adduid='" . $this->user->uid . "' AND uid='{$this->uid}' AND pid='0'");
            $this->existing_reputation = $this->db->fetch_array($query);
            $this->bb->rid = $this->existing_reputation['rid'];
            $this->was_post = false;
        }
        if ($this->bb->getInput('pid', 0) != 0) {
            $query = $this->db->simple_select(
                'reputation',
                '*',
                "adduid='" . $this->user->uid . "' AND uid='{$this->uid}' AND pid = '" . $this->bb->getInput('pid', 0) . "'"
            );
            $this->existing_reputation = $this->db->fetch_array($query);
            $this->bb->rid = $this->existing_reputation['rid'];
            $this->was_post = true;
        }
    }
}
