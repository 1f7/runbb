<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Reputation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Delete extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        // Fetch the existing reputation for this user given by our current user if there is one.
        $query = $this->db->query('
		SELECT r.*, u.username
		FROM ' . TABLE_PREFIX . 'reputation r
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=r.adduid)
		WHERE rid = '" . $this->bb->getInput('rid', 0) . "'
	");
        $existing_reputation = $this->db->fetch_array($query);

        // Only administrators, super moderators, as well as users who gave a specifc vote can delete one.
        if ($this->bb->usergroup['issupermod'] != 1 &&
            ($this->bb->usergroup['candeletereputations'] != 1 ||
                $existing_reputation['adduid'] != $this->user->uid ||
                $this->user->uid == 0)
        ) {
            return $this->bb->error_no_permission();
        }

        // Delete the specified reputation
        $this->db->delete_query('reputation', "uid='{$this->uid}' AND rid='" . $this->bb->getInput('rid', 0) . "'");

        // Recount the reputation of this user - keep it in sync.
        $query = $this->db->simple_select('reputation', 'SUM(reputation) AS reputation_count', "uid='{$this->uid}'");
        $reputation_value = $this->db->fetch_field($query, 'reputation_count');

        // Create moderator log
        $this->bblogger->log_moderator_action(
            ['uid' => $this->repuser->uid, 'username' => $this->repuser->username],
            $this->lang->sprintf(
                $this->lang->delete_reputation_log,
                $existing_reputation['username'],
                $existing_reputation['adduid']
            )
        );

        $this->db->update_query('users', ['reputation' => (int)$reputation_value], "uid='{$this->uid}'");

        $this->bb->redirect($this->bb->settings['bburl'] . '/reputation?uid=' . $this->uid, $this->lang->vote_deleted_message);
    }
}
