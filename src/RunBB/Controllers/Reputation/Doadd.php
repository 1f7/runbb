<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Reputation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Doadd extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        if ($this->common($response) === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('reputation_do_add_start');

        // Check if the reputation power they're trying to give is within their 'power limit'
        $reputation = abs($this->bb->getInput('reputation', 0));

        // Deleting our current reputation of this user.
        if (!empty($this->bb->input['delete'])) {
            // Only administrators, super moderators, as well as users who gave a specifc vote can delete one.
            if ($this->bb->usergroup['issupermod'] != 1 &&
                ($this->bb->usergroup['candeletereputations'] != 1 ||
                    $this->existing_reputation['adduid'] != $this->user->uid || $this->user->uid == 0)
            ) {
                return $this->bb->error_no_permission();
            }

            if ($this->bb->getInput('pid', 0) != 0) {
                $this->db->delete_query('reputation', "uid='{$this->uid}' AND adduid='" . $this->user->uid . "' AND pid = '" . $this->bb->getInput('pid', 0) . "'");
            } else {
                $this->db->delete_query('reputation', "rid='{$this->bb->rid}' AND uid='{$this->uid}' AND adduid='" . $this->user->uid . "'");
            }

            // Recount the reputation of this user - keep it in sync.
            $query = $this->db->simple_select('reputation', 'SUM(reputation) AS reputation_count', "uid='{$this->uid}'");
            $reputation_value = $this->db->fetch_field($query, 'reputation_count');

            $this->db->update_query('users', ['reputation' => (int)$reputation_value], "uid='{$this->uid}'");

            $data = [
                'header' => $this->lang->vote_deleted,
                'message' => $this->lang->vote_deleted_message
            ];
            return $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
        }

        $this->bb->input['comments'] = trim($this->bb->getInput('comments', '', true)); // Trim whitespace to check for length
        if (my_strlen($this->bb->input['comments']) < $this->bb->settings['minreplength'] && $this->bb->getInput('pid', 0) == 0) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_no_comment];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }

        // The power for the reputation they specified was invalid.
        if ($reputation > $this->bb->usergroup['reputationpower']) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_invalidpower];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }

        // The user is trying to give a negative reputation, but negative reps have been disabled.
        if ($this->bb->getInput('reputation', 0) < 0 && $this->bb->settings['negrep'] != 1) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_negative_disabled];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }

        // This user is trying to give a neutral reputation, but neutral reps have been disabled.
        if ($this->bb->getInput('reputation', 0) == 0 && $this->bb->settings['neurep'] != 1) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_neutral_disabled];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }

        // This user is trying to give a positive reputation, but positive reps have been disabled.
        if ($this->bb->getInput('reputation', 0) > 0 && $this->bb->settings['posrep'] != 1) {
            $data = ['header' => $this->lang->error, 'message' => $this->lang->add_positive_disabled];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }

        // The length of the comment is too long
        if (my_strlen($this->bb->input['comments']) > $this->bb->settings['maxreplength']) {
            $message = $this->lang->sprintf($this->lang->add_toolong, $this->bb->settings['maxreplength']);
            $data = ['header' => $this->lang->error, 'message' => $message];
            if ($this->bb->input['nomodal']) {
                $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
            } else {
                $this->view->render($response, '@forum/message_modal.html.twig', $data);
            }
            return;
        }

        // Build array of reputation data.
        $reputation = [
            'uid' => $this->uid,
            'adduid' => $this->user->uid,
            'pid' => $this->bb->getInput('pid', 0),
            'reputation' => $this->bb->getInput('reputation', 0),
            'dateline' => TIME_NOW,
            'comments' => $this->db->escape_string($this->bb->input['comments'])
        ];

        $this->plugins->runHooks('reputation_do_add_process');

        // Updating an existing reputation
        if (!empty($this->existing_reputation['uid'])) {
            $this->db->update_query('reputation', $reputation, "rid='" . $this->existing_reputation['rid'] . "'");

            // Recount the reputation of this user - keep it in sync.
            $query = $this->db->simple_select('reputation', 'SUM(reputation) AS reputation_count', "uid='{$this->uid}'");
            $reputation_value = $this->db->fetch_field($query, 'reputation_count');

            $this->db->update_query('users', ['reputation' => (int)$reputation_value], "uid='{$this->uid}'");

            $this->lang->vote_added = $this->lang->vote_updated;
            $this->lang->vote_added_message = $this->lang->vote_updated_message;
        } // Insert a new reputation
        else {
            $this->db->insert_query('reputation', $reputation);

            // Recount the reputation of this user - keep it in sync.
            $query = $this->db->simple_select('reputation', 'SUM(reputation) AS reputation_count', "uid='{$this->uid}'");
            $reputation_value = $this->db->fetch_field($query, 'reputation_count');

            $this->db->update_query('users', ['reputation' => (int)$reputation_value], "uid='{$this->uid}'");
        }

        $this->plugins->runHooks('reputation_do_add_end');

        $data = ['header' => $this->lang->vote_added, 'message' => $this->lang->vote_added_message];
        if ($this->bb->input['nomodal']) {
            $this->view->render($response, '@forum/message_nomodal.html.twig', $data);
        } else {
            $this->view->render($response, '@forum/message_modal.html.twig', $data);
        }
    }
}
