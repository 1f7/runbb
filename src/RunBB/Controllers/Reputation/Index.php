<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Reputation;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canviewprofiles'] == 0) {
            // Reputation page is a part of a profile
            return $this->bb->error_no_permission();
        }

        // Set display group to their user group if they don't have a display group.
        if (!$this->repuser->displaygroup) {
            $this->repuser->displaygroup = $this->repuser->usergroup;
        }

        // Fetch display group properties.
        $this->bb->displaygroupfields = ['title', 'description', 'namestyle', 'usertitle', 'stars', 'starimage', 'image', 'usereputationsystem'];
        $display_group = $this->group->usergroup_displaygroup($this->repuser->displaygroup);
        if ($this->user_permissions['usereputationsystem'] != 1 || ($display_group['title'] && $display_group['usereputationsystem'] == 0)) {
            // Group has reputation disabled or user has a display group that has reputation disabled
            $this->bb->error($this->lang->reputations_disabled_group);
        }

        $this->lang->nav_profile = $this->lang->sprintf($this->lang->nav_profile, $this->repuser->username);
        $this->lang->reputation_report = $this->lang->sprintf($this->lang->reputation_report, $this->repuser->username);

        // Format the user name using the group username style
        $username = $this->user->format_name($this->repuser->username, $this->repuser->usergroup, $this->repuser->displaygroup);
        $this->view->offsetSet('username', $username);

        // Set display group to their user group if they don't have a display group.
        if (!$this->repuser->displaygroup) {
            $this->repuser->displaygroup = $this->repuser->usergroup;
        }

        $usertitle = '';
        // This user has a custom user title
        if (trim($this->repuser->usertitle) != '') {
            $usertitle = $this->repuser->usertitle;
        } // Using our display group's user title
        elseif (trim($display_group['usertitle']) != '') {
            $usertitle = $display_group['usertitle'];
        } // Otherwise, fetch it from our titles table for the number of posts this user has
        else {
            $usertitles = $this->cache->read('usertitles');
            foreach ($usertitles as $title) {
                if ($title['posts'] <= $this->repuser->postnum) {
                    $usertitle = $title['title'];
                    break;
                }
            }
            unset($usertitles, $title);
        }

        $this->view->offsetSet('usertitle', htmlspecialchars_uni($usertitle));

        // If the user has permission to add reputations - show the image
        $add_reputation = '';
        if ($this->bb->usergroup['cangivereputations'] == 1 &&
            $this->repuser->uid != $this->user->uid &&
            ($this->bb->settings['posrep'] || $this->bb->settings['neurep'] || $this->bb->settings['negrep'])
        ) {
            $add_reputation = "<div class=\"float_right\" style=\"padding-bottom: 4px;\"><a href=\"javascript:MyBB.reputation({$this->repuser->uid});\" class=\"button rate_user_button\"><span>{$this->lang->rate_user}</span></a></div>";
        }
        $this->view->offsetSet('add_reputation', $add_reputation);

        // Build navigation menu
        $this->bb->add_breadcrumb($this->lang->nav_profile, get_profile_link($this->repuser->uid));
        $this->bb->add_breadcrumb($this->lang->nav_reputation);

        // Check our specified conditionals for what type of reputations to show
        $show_selected = ['all' => '', 'positive' => '', 'neutral' => '', 'negative' => ''];
        switch ($this->bb->getInput('show', '')) {
            case 'positive':
                $s_url = '&show=positive';
                $conditions = 'AND r.reputation>0';
                $show_selected['positive'] = 'selected="selected"';
                break;
            case 'neutral':
                $s_url = '&show=neutral';
                $conditions = 'AND r.reputation=0';
                $show_selected['neutral'] = 'selected="selected"';
                break;
            case 'negative':
                $s_url = '&show=negative';
                $conditions = 'AND r.reputation<0';
                $show_selected['negative'] = 'selected="selected"';
                break;
            default:
                $s_url = '&show=all';
                $conditions = '';
                $show_select['all'] = 'selected="selected"';
                break;
        }
        $this->view->offsetSet('show_selected', $show_selected);

        // Check the sorting options for the reputation list
        $sort_selected = ['username' => '', 'last_ipdated' => ''];
        switch ($this->bb->getInput('sort', '')) {
            case 'username':
                $s_url .= '&sort=username';
                $order = 'u.username ASC';
                $sort_selected['username'] = 'selected="selected"';
                break;
            default:
                $s_url .= '&sort=dateline';
                $order = 'r.dateline DESC';
                $sort_selected['last_updated'] = 'selected="selected"';
                break;
        }
        $this->view->offsetSet('sort_selected', $sort_selected);

        if (empty($this->bb->input['show']) && empty($this->bb->input['sort'])) {
            $s_url = '';
        }

        // Fetch the total number of reputations for this user
        $query = $this->db->simple_select(
            'reputation r',
            'COUNT(r.rid) AS reputation_count',
            "r.uid='{$this->repuser->uid}' $conditions"
        );
        $reputation_count = $this->db->fetch_field($query, 'reputation_count');

        // If the user has no reputation, suspect 0...
        if (!$this->repuser->reputation) {
            $this->repuser->reputation = 0;
        }

        // Quickly check to see if we're in sync...
        $query = $this->db->simple_select(
            'reputation',
            'SUM(reputation) AS reputation, COUNT(rid) AS total_reputation',
            "uid = '" . $this->repuser->uid . "'"
        );
        $reputation = $this->db->fetch_array($query);

        $sync_reputation = (int)$reputation['reputation'];
        $total_reputation = $reputation['total_reputation'];

        if ($sync_reputation != $this->repuser->reputation) {
            // We're out of sync! Oh noes!
            $this->db->update_query('users', ['reputation' => $sync_reputation], "uid = '" . $this->repuser->uid . "'");
            $this->repuser->reputation = $sync_reputation;
        }

        // Set default count variables to 0
        $positive_count = $negative_count = $neutral_count = 0;
        $positive_week = $negative_week = $neutral_week = 0;
        $positive_month = $negative_month = $neutral_month = 0;
        $positive_6months = $negative_6months = $neutral_6months = 0;

        // Unix timestamps for when this week, month and last 6 months started
        $last_week = TIME_NOW - 604800;
        $last_month = TIME_NOW - 2678400;
        $last_6months = TIME_NOW - 16070400;

        // Query reputations for the 'reputation card'
        $query = $this->db->simple_select('reputation', 'reputation, dateline', "uid='{$this->repuser->uid}'");
        while ($reputation_vote = $this->db->fetch_array($query)) {
            // This is a positive reputation
            if ($reputation_vote['reputation'] > 0) {
                $positive_count++;
                if ($reputation_vote['dateline'] >= $last_week) {
                    $positive_week++;
                }
                if ($reputation_vote['dateline'] >= $last_month) {
                    $positive_month++;
                }
                if ($reputation_vote['dateline'] >= $last_6months) {
                    $positive_6months++;
                }
            } // Negative reputation given
            elseif ($reputation_vote['reputation'] < 0) {
                $negative_count++;
                if ($reputation_vote['dateline'] >= $last_week) {
                    $negative_week++;
                }
                if ($reputation_vote['dateline'] >= $last_month) {
                    $negative_month++;
                }
                if ($reputation_vote['dateline'] >= $last_6months) {
                    $negative_6months++;
                }
            } // Neutral reputation given
            else {
                $neutral_count++;
                if ($reputation_vote['dateline'] >= $last_week) {
                    $neutral_week++;
                }
                if ($reputation_vote['dateline'] >= $last_month) {
                    $neutral_month++;
                }
                if ($reputation_vote['dateline'] >= $last_6months) {
                    $neutral_6months++;
                }
            }
        }

        // Format all reputation numbers
        $this->view->offsetSet('rep_total', $this->parser->formatNumber($this->repuser->reputation));
        $this->view->offsetSet('f_positive_count', $this->parser->formatNumber($positive_count));
        $this->view->offsetSet('f_negative_count', $this->parser->formatNumber($negative_count));
        $this->view->offsetSet('f_neutral_count', $this->parser->formatNumber($neutral_count));
        $this->view->offsetSet('f_positive_week', $this->parser->formatNumber($positive_week));
        $this->view->offsetSet('f_negative_week', $this->parser->formatNumber($negative_week));
        $this->view->offsetSet('f_neutral_week', $this->parser->formatNumber($neutral_week));
        $this->view->offsetSet('f_positive_month', $this->parser->formatNumber($positive_month));
        $this->view->offsetSet('f_negative_month', $this->parser->formatNumber($negative_month));
        $this->view->offsetSet('f_neutral_month', $this->parser->formatNumber($neutral_month));
        $this->view->offsetSet('f_positive_6months', $this->parser->formatNumber($positive_6months));
        $this->view->offsetSet('f_negative_6months', $this->parser->formatNumber($negative_6months));
        $this->view->offsetSet('f_neutral_6months', $this->parser->formatNumber($neutral_6months));

        $this->view->offsetSet('uid', $this->repuser->uid);

        // Format the user's 'total' reputation
        $total_class = '_neutral';
        if ($this->repuser->reputation < 0) {
            $total_class = '_minus';
        } elseif ($this->repuser->reputation > 0) {
            $total_class = '_plus';
        }
        $this->view->offsetSet('total_class', $total_class);

        // Figure out how many reps have come from posts / 'general'
        // Posts
        $query = $this->db->simple_select('reputation', 'COUNT(rid) AS rep_posts', "uid = '" . $this->repuser->uid . "' AND pid > 0");
        $rep_post_count = $this->db->fetch_field($query, 'rep_posts');
        $this->view->offsetSet('rep_posts', $this->parser->formatNumber($rep_post_count));

        // General
        // We count how many reps in total, then subtract the reps from posts
        $this->view->offsetSet('rep_members', $this->parser->formatNumber($total_reputation - $rep_post_count));

        // Is negative reputation disabled? If so, tell the user
        if ($this->bb->settings['negrep'] == 0) {
            $neg_rep_info = $this->lang->neg_rep_disabled;
        }

        if ($this->bb->settings['posrep'] == 0) {
            $pos_rep_info = $this->lang->pos_rep_disabled;
        }

        if ($this->bb->settings['neurep'] == 0) {
            $neu_rep_info = $this->lang->neu_rep_disabled;
        }

        $perpage = (int)$this->bb->settings['repsperpage'];
        if ($perpage < 1) {
            $perpage = 15;
        }

        // Check if we're browsing a specific page of results
        if ($this->bb->getInput('page', 0) > 0) {
            $page = $this->bb->getInput('page', 0);
            $start = ($page - 1) * $perpage;
            $pages = $reputation_count / $perpage;
            $pages = ceil($pages);
            if ($page > $pages) {
                $start = 0;
                $page = 1;
            }
        } else {
            $start = 0;
            $page = 1;
        }

        $multipage = '';
        // Build out multipage navigation
        if ($reputation_count > 0) {
            $multipage = $this->pagination->multipage($reputation_count, $perpage, $page, $this->bb->settings['bburl'] . "/reputation?uid={$this->repuser->uid}" . $s_url);
        }
        $this->view->offsetSet('multipage', $multipage);

        // Fetch the reputations which will be displayed on this page
        $query = $this->db->query('
		SELECT r.*, r.uid AS rated_uid, u.uid, u.username, u.reputation AS user_reputation, u.usergroup AS user_usergroup, u.displaygroup AS user_displaygroup
		FROM ' . TABLE_PREFIX . 'reputation r
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=r.adduid)
		WHERE r.uid='{$this->repuser->uid}' $conditions
		ORDER BY $order
		LIMIT $start, {$perpage}
	");

        // Gather a list of items that have post reputation
        $reputation_cache = $post_cache = $post_reputation = [];

        while ($reputation_vote = $this->db->fetch_array($query)) {
            $reputation_cache[] = $reputation_vote;

            // If this is a post, hold it and gather some information about it
            if ($reputation_vote['pid'] && !isset($post_cache[$reputation_vote['pid']])) {
                $post_cache[$reputation_vote['pid']] = $reputation_vote['pid'];
            }
        }

        if (!empty($post_cache)) {
            $pids = implode(',', $post_cache);

            $sql = ["p.pid IN ({$pids})"];

            // get forums user cannot view
            $unviewable = $this->forum->get_unviewable_forums(true);
            if ($unviewable) {
                $sql[] = ' p.fid NOT IN (' . implode(',', $unviewable) . ')';
            }

            // get inactive forums
            $inactive = $this->forum->get_inactive_forums();
            if ($inactive) {
                $sql[] = ' p.fid NOT IN (' . implode(',', $inactive) . ')';
            }

            if (!$this->user->ismoderator) {
                $sql[] = "p.visible='1'";
                $sql[] = "t.visible='1'";
            }

            $sql = implode(' AND ', $sql);

            $query = $this->db->query('
			SELECT p.pid, p.uid, p.fid, p.visible, p.message, t.tid, t.subject, t.visible AS thread_visible
			FROM ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
			WHERE {$sql}
		");

            $forumpermissions = [];

            while ($post = $this->db->fetch_array($query)) {
                if (($post['visible'] == 0 ||
                        $post['thread_visible'] == 0) &&
                    !$this->user->is_moderator($post['fid'], 'canviewunapprove')
                ) {
                    continue;
                }

                if (($post['visible'] == -1 ||
                        $post['thread_visible'] == -1) &&
                    !$this->user->is_moderator($post['fid'], 'canviewdeleted')
                ) {
                    continue;
                }

                if (!isset($forumpermissions[$post['fid']])) {
                    $forumpermissions[$post['fid']] = $this->forum->forum_permissions($post['fid']);
                }

                // Make sure we can view this post
                if (isset($forumpermissions[$post['fid']]['canonlyviewownthreads']) &&
                    $forumpermissions[$post['fid']]['canonlyviewownthreads'] == 1 &&
                    $post['uid'] != $this->user->uid
                ) {
                    continue;
                }

                $post_reputation[$post['pid']] = $post;
            }
        }

        $reputation_votes = [];
        foreach ($reputation_cache as $reputation_vote) {
            // Get the reputation for the user who posted this comment
            if ($reputation_vote['adduid'] == 0) {
                $reputation_vote['user_reputation'] = 0;
            }

            $reputation_vote['user_reputation'] = $this->user->get_reputation($reputation_vote['user_reputation'], $reputation_vote['adduid']);

            // Format the username of this poster
            if (!$reputation_vote['username']) {
                $reputation_vote['username'] = $this->lang->na;
                $reputation_vote['user_reputation'] = '';
            } else {
                $reputation_vote['username'] = $this->user->format_name($reputation_vote['username'], $reputation_vote['user_usergroup'], $reputation_vote['user_displaygroup']);
                $reputation_vote['username'] = $this->user->build_profile_link($reputation_vote['username'], $reputation_vote['uid']);
                $reputation_vote['user_reputation'] = "({$reputation_vote['user_reputation']})";
            }

            $vote_reputation = (int)$reputation_vote['reputation'];

            // This is a negative reputation
            if ($vote_reputation < 0) {
                $status_class = 'trow_reputation_negative';
                $vote_type_class = 'reputation_negative';
                $vote_type = $this->lang->negative;
            } // This is a neutral reputation
            elseif ($vote_reputation == 0) {
                $status_class = 'trow_reputation_neutral';
                $vote_type_class = 'reputation_neutral';
                $vote_type = $this->lang->neutral;
            } // Otherwise, this is a positive reputation
            else {
                $vote_reputation = "+{$vote_reputation}";
                $status_class = 'trow_reputation_positive';
                $vote_type_class = 'reputation_positive';
                $vote_type = $this->lang->positive;
            }

            $vote_reputation = "({$vote_reputation})";

            // Format the date this reputation was last modified
            $last_updated_date = $this->time->formatDate('relative', $reputation_vote['dateline']);
            $last_updated = $this->lang->sprintf($this->lang->last_updated, $last_updated_date);

            // Is this rating specific to a post?
            $postrep_given = '';
            if ($reputation_vote['pid']) {
                $postrep_given = $this->lang->sprintf($this->lang->postrep_given_nolink, $this->repuser->username);
                if (isset($post_reputation[$reputation_vote['pid']])) {
                    $thread_link = get_thread_link($post_reputation[$reputation_vote['pid']]['tid']);
                    $subject = htmlspecialchars_uni($this->parser->parse_badwords($post_reputation[$reputation_vote['pid']]['subject']));

                    $thread_link = $this->lang->sprintf($this->lang->postrep_given_thread, $thread_link, $subject);
                    $link = get_post_link($reputation_vote['pid']) . "#pid{$reputation_vote['pid']}";

                    $postrep_given = $this->lang->sprintf($this->lang->postrep_given, $link, $this->repuser->username, $thread_link);
                }
            }

            // Does the current user have permission to delete this reputation? Show delete link
            $delete_link = false;
            if ($this->bb->usergroup['issupermod'] == 1 ||
                ($this->bb->usergroup['candeletereputations'] == 1 &&
                    $reputation_vote['adduid'] == $this->user->uid &&
                    $this->user->uid != 0)
            ) {
                $delete_link = true;
            }

            // Parse smilies in the reputation vote
            $reputation_parser = [
                'allow_html' => 0,
                'allow_mycode' => 0,
                'allow_smilies' => 1,
                'allow_imgcode' => 0,
                'filter_badwords' => 1
            ];

            $reputation_vote['comments'] = $this->parser->parse_message($reputation_vote['comments'], $reputation_parser);
            if ($reputation_vote['comments'] == '') {
                $reputation_vote['comments'] = $this->lang->no_comment;
            }
            $this->plugins->runHooks('reputation_vote');
            $reputation_votes[] = [
                'status_class' => $status_class,
                'reputation_vote' => $reputation_vote,
                'delete_link' => $delete_link,
                'report_link' => (($this->user->uid != 0) ? true : false),
                'last_updated' => $last_updated,
                'postrep_given' => $postrep_given,
                'vote_type_class' => $vote_type_class,
                'vote_type' => $vote_type,
                'vote_reputation' => $vote_reputation
            ];
        }
        $this->view->offsetSet('reputation_votes', $reputation_votes);

        $this->plugins->runHooks('reputation_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Reputation/reputation.html.twig');
    }
}
