<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class Search extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'sid');

        // Load global language phrases
        $this->lang->load('search');

        $this->bb->add_breadcrumb($this->lang->nav_search, $this->bb->settings['bburl'] . '/search');

        $this->bb->input['action'] = $this->bb->getInput('action', '');
        switch ($this->bb->input['action']) {
            case 'results':
                $this->bb->add_breadcrumb($this->lang->nav_results);
                break;
        }

        if ($this->bb->usergroup['cansearch'] == 0) {
            return $this->bb->error_no_permission();
        }

        $now = TIME_NOW;
        $this->bb->input['keywords'] = trim($this->bb->getInput('keywords', ''));

        $limitsql = '';
        if ((int)$this->bb->settings['searchhardlimit'] > 0) {
            $limitsql = 'LIMIT ' . (int)$this->bb->settings['searchhardlimit'];
        }

        if ($this->bb->input['action'] == 'results') {
//            $sid = $this->db->escape_string($this->bb->getInput('sid'));
            $sid = $this->bb->getInput('sid', 0);
//            $query = $this->db->simple_select('searchlog', '*', "sid='$sid'");
//            $search = $this->db->fetch_array($query);
            $s = \RunBB\Models\Searchlog::where('sid', '=', $sid)
                ->get()->toArray();
            $search = $s[0];

            $this->view->offsetSet('sid', $sid);

            if (!$search['sid']) {
                $this->bb->error($this->lang->error_invalidsearch);
            }

            $this->plugins->runHooks('search_results_start');

            // Decide on our sorting fields and sorting order.
            $order = my_strtolower(htmlspecialchars_uni($this->bb->getInput('order', '')));
            $sortby = my_strtolower(htmlspecialchars_uni($this->bb->getInput('sortby', '')));

            switch ($sortby) {
                case 'replies':
                    $sortfield = 't.replies';
                    break;
                case 'views':
                    $sortfield = 't.views';
                    break;
                case 'subject':
                    if ($search['resulttype'] == 'threads') {
                        $sortfield = 't.subject';
                    } else {
                        $sortfield = 'p.subject';
                    }
                    break;
                case 'forum':
                    $sortfield = 't.fid';
                    break;
                case 'starter':
                    if ($search['resulttype'] == 'threads') {
                        $sortfield = 't.username';
                    } else {
                        $sortfield = 'p.username';
                    }
                    break;
                case 'lastpost':
                default:
                    if ($search['resulttype'] == 'threads') {
                        $sortfield = 't.lastpost';
                        $sortby = 'lastpost';
                    } else {
                        $sortfield = 'p.dateline';
                        $sortby = 'dateline';
                    }
                    break;
            }

            if ($order != 'asc') {
                $order = 'desc';
                $oppsortnext = 'asc';
                $oppsort = $this->lang->asc;
            } else {
                $oppsortnext = 'desc';
                $oppsort = $this->lang->desc;
            }

            if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                $this->bb->settings['threadsperpage'] = 20;
            }

            // Work out pagination, which page we're at, as well as the limits.
            $perpage = $this->bb->settings['threadsperpage'];
            $page = $this->bb->getInput('page', '');
            if ($page > 0) {
                $start = ($page - 1) * $perpage;
            } else {
                $start = 0;
                $page = 1;
            }
            $end = $start + $perpage;
            $lower = $start + 1;
            $upper = $end;

            // Work out if we have terms to highlight
            $highlight = '';
            if ($search['keywords']) {
                if ($this->bb->seo_support == true) {
                    $highlight = '?highlight=' . urlencode($search['keywords']);
                } else {
                    $highlight = '&amp;highlight=' . urlencode($search['keywords']);
                }
            }

            $sorturl = $this->bb->settings['bburl'] . '/search?action=results&sid=' . $sid;
            $this->view->offsetSet('sorturl', $sorturl);
            $thread_url = '';
            $post_url = '';

            $orderarrow = [
                'replies' => '',
                'views' => '',
                'subject' => '',
                'forum' => '',
                'starter' => '',
                'lastpost' => '',
                'dateline' => ''
            ];
            $orderarrow[$sortby] = "<span class=\"smalltext\">[<a href=\"{$sorturl}&sortby={$sortby}
                &order={$oppsortnext}\">{$oppsort}</a>]</span>";
            $this->view->offsetSet('orderarrow', $orderarrow);

            // Read some caches we will be using
            $forumcache = $this->cache->read('forums');
            $icon_cache = $this->cache->read('posticons');

            $threads = [];

            if ($this->user->uid == 0) {
                // Build a forum cache.
//                $query = $this->db->query('
//			SELECT fid
//			FROM ' . TABLE_PREFIX . 'forums
//			WHERE active != 0
//			ORDER BY pid, disporder
//		');
                $f = \RunBB\Models\Forum::where('active', '!=', 0)
                    ->orderBy('pid')
                    ->orderBy('disporder')
                    ->get(['fid'])
                    ->toArray();
                $forumsread = '';
                if (isset($this->bb->cookies['mybb']['forumread'])) {
                    $forumsread = my_unserialize($this->bb->cookies['mybb']['forumread']);
                }
            } else {
                // Build a forum cache.
//                $query = $this->db->query('
//			SELECT f.fid, fr.dateline AS lastread
//			FROM ' . TABLE_PREFIX . 'forums f
//			LEFT JOIN ' . TABLE_PREFIX . "forumsread fr ON (fr.fid=f.fid AND fr.uid='{$this->user->uid}')
//			WHERE f.active != 0
//			ORDER BY pid, disporder
//		");
                $f = \RunBB\Models\Forum::where('forums.active', '!=', 0)
                    ->leftJoin('forumsread as fr', function ($q) {
                        $q->on(function ($q) {
                            $q->on('fr.fid', '=', 'forums.fid');
                            $q->on('fr.uid', '=', DB::raw($this->user->uid));
                        });
                    })
                    ->orderBy('pid')
                    ->orderBy('disporder')
                    ->get([
                        'forums.fid',
                        'fr.dateline AS lastread'
                    ])
                    ->toArray();
            }
//            while ($forum = $this->db->fetch_array($query)) {
            foreach ($f as $forum) {
                if ($this->user->uid == 0) {
                    if (isset($forumsread[$forum['fid']])) {
                        $forum['lastread'] = $forumsread[$forum['fid']];
                    }
                }
                $readforums[$forum['fid']] = isset($forum['lastread']) ? $forum['lastread'] : '';
            }
            $fpermissions = $this->forum->forum_permissions();

            // Inline Mod Column for moderators
            $inlinecookie = '';
            $is_mod = $is_supermod = false;
            $this->bb->showInlineModCol = false;
            if ($this->bb->usergroup['issupermod']) {
                $is_supermod = true;
            }
            $return_url = '';
            $this->inlinecount = 0;
            if ($is_supermod || $this->user->is_moderator()) {
                $this->bb->showInlineModCol = true;
                $this->inlinecookie = 'inlinemod_search' . $sid;
                $is_mod = true;
                $return_url = $this->bb->settings['bburl'].'/search?' . htmlspecialchars_uni($_SERVER['QUERY_STRING']);
            }
            $this->view->offsetSet('return_url', $return_url);
//            $this->view->offsetSet('inlinemodcol', $inlinemodcol);

            // Show search results as 'threads'
            if ($search['resulttype'] == 'threads') {
                $threadcount = 0;
                // Moderators can view unapproved threads
                $query = $this->db->simple_select(
                    'moderators',
                    'fid, canviewunapprove, canviewdeleted',
                    "(id='{$this->user->uid}' AND isgroup='0') OR (id='{$this->user->usergroup}' AND isgroup='1')"
                );
                if ($this->bb->usergroup['issupermod'] == 1) {
                    // Super moderators (and admins)
                    $unapproved_where = 't.visible>=-1';
                } elseif ($this->db->num_rows($query)) {
                    // Normal moderators
                    $unapprove_forums = [];
                    $deleted_forums = [];
                    $unapproved_where = '(t.visible = 1';
                    while ($moderator = $this->db->fetch_array($query)) {
                        if ($moderator['canviewunapprove'] == 1) {
                            $unapprove_forums[] = $moderator['fid'];
                        }

                        if ($moderator['canviewdeleted'] == 1) {
                            $deleted_forums[] = $moderator['fid'];
                        }
                    }

                    if (!empty($unapprove_forums)) {
                        $unapproved_where .= ' OR (t.visible = 0 AND t.fid IN('. implode(',', $unapprove_forums) .'))';
                    }
                    if (!empty($deleted_forums)) {
                        $unapproved_where .= ' OR (t.visible = -1 AND t.fid IN('. implode(',', $deleted_forums) .'))';
                    }
                    $unapproved_where .= ')';
                } else {
                    // Normal users
                    $unapproved_where = 't.visible>0';
                }

                // If we have saved WHERE conditions, execute them
                if ($search['querycache'] != '') {
                    $where_conditions = $search['querycache'];
                    $query = $this->db->simple_select(
                        'threads t',
                        't.tid',
                        $where_conditions . " AND {$unapproved_where} AND t.closed NOT LIKE 'moved|%' 
                        ORDER BY t.lastpost DESC {$limitsql}"
                    );
                    while ($thread = $this->db->fetch_array($query)) {
                        $threads[$thread['tid']] = $thread['tid'];
                        $threadcount++;
                    }
                    // Build our list of threads.
                    if ($threadcount > 0) {
                        $search['threads'] = implode(',', $threads);
                    } // No results.
                    else {
                        $this->bb->error($this->lang->error_nosearchresults);
                    }
                    $where_conditions = 't.tid IN (' . $search['threads'] . ')';
                } // This search doesn't use a query cache, results stored in search table.
                else {
                    $where_conditions = 't.tid IN (' . $search['threads'] . ')';
                    $query = $this->db->simple_select(
                        'threads t',
                        'COUNT(t.tid) AS resultcount',
                        $where_conditions . " AND {$unapproved_where} AND t.closed NOT LIKE 'moved|%' {$limitsql}"
                    );
                    $count = $this->db->fetch_array($query);

                    if (!$count['resultcount']) {
                        $this->bb->error($this->lang->error_nosearchresults);
                    }
                    $threadcount = $count['resultcount'];
                }

                $permsql = '';
                $onlyusfids = [];

                // Check group permissions if we can't view threads not started by us
                $group_permissions = $this->forum->forum_permissions();

                foreach ($group_permissions as $fid => $forum_permissions) {
                    if (isset($forum_permissions['canonlyviewownthreads']) &&
                        $forum_permissions['canonlyviewownthreads'] == 1) {
                        $onlyusfids[] = $fid;
                    }
                }
                if (!empty($onlyusfids)) {
                    $permsql .= 'AND ((t.fid IN(' . implode(',', $onlyusfids) . ") 
                    AND t.uid='{$this->user->uid}') OR t.fid NOT IN(" . implode(',', $onlyusfids) . '))';
                }

                $unsearchforums = $this->search->get_unsearchable_forums();
                if ($unsearchforums) {
                    $permsql .= ' AND t.fid NOT IN (' . implode(',', $unsearchforums) . ')';
                }
                $inactiveforums = $this->forum->get_inactive_forums();
                if ($inactiveforums) {
                    $permsql .= ' AND t.fid NOT IN (' . implode(',', $inactiveforums) . ')';
                }

                // Begin selecting matching threads, cache them.
                $sqlarray = [
                    'order_by' => $sortfield,
                    'order_dir' => $order,
                    'limit_start' => $start,
                    'limit' => $perpage
                ];
                $query = $this->db->query('
			SELECT t.*, u.username AS userusername
			FROM ' . TABLE_PREFIX . 'threads t
			LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=t.uid)
			WHERE $where_conditions AND {$unapproved_where} {$permsql} AND t.closed NOT LIKE 'moved|%'
			ORDER BY $sortfield $order
			LIMIT $start, $perpage
		");

                $threadprefixes = $this->thread->build_prefixes();
                $thread_cache = [];
                while ($thread = $this->db->fetch_array($query)) {
                    $thread['threadprefix'] = '';
                    if ($thread['prefix'] && !empty($threadprefixes[$thread['prefix']])) {
                        $thread['threadprefix'] = $threadprefixes[$thread['prefix']]['displaystyle'];
                    }
                    $thread_cache[$thread['tid']] = $thread;
                }
                $thread_ids = implode(',', array_keys($thread_cache));

                if (empty($thread_ids)) {
                    $this->bb->error($this->lang->error_nosearchresults);
                }

                // Fetch dot icons if enabled
                if ($this->bb->settings['dotfolders'] != 0 && $this->user->uid && $thread_cache) {
                    $p_unapproved_where = str_replace('t.', '', $unapproved_where);
                    $query = $this->db->simple_select(
                        'posts',
                        'DISTINCT tid,uid',
                        "uid='{$this->user->uid}' AND tid IN({$thread_ids}) AND {$p_unapproved_where}"
                    );
                    while ($thread = $this->db->fetch_array($query)) {
                        $thread_cache[$thread['tid']]['dot_icon'] = 1;
                    }
                }

                // Fetch the read threads.
                if ($this->user->uid && $this->bb->settings['threadreadcut'] > 0) {
                    $query = $this->db->simple_select(
                        'threadsread',
                        'tid,dateline',
                        "uid='" . $this->user->uid . "' AND tid IN(" . $thread_ids . ')'
                    );
                    while ($readthread = $this->db->fetch_array($query)) {
                        $thread_cache[$readthread['tid']]['lastread'] = $readthread['dateline'];
                    }
                }

                if (!$this->bb->settings['maxmultipagelinks']) {
                    $this->bb->settings['maxmultipagelinks'] = 5;
                }

                $results = [];
                foreach ($thread_cache as $thread) {
                    $bgcolor = alt_trow();
                    $folder = '';
                    $prefix = '';

                    // Unapproved colour
                    if ($thread['visible'] == 0) {
                        $bgcolor = 'trow_shaded';
                    } elseif ($thread['visible'] == -1) {
                        $bgcolor = 'trow_shaded trow_deleted';
                    }

                    if ($thread['userusername']) {
                        $thread['username'] = $thread['userusername'];
                    }
                    $thread['profilelink'] = $this->user->build_profile_link($thread['username'], $thread['uid']);

                    // If this thread has a prefix, insert a space between prefix and subject
                    if ($thread['prefix'] != 0) {
                        $thread['threadprefix'] .= '&nbsp;';
                    }

                    $thread['subject'] = $this->parser->parse_badwords($thread['subject']);
                    $thread['subject'] = htmlspecialchars_uni($thread['subject']);

                    if (isset($icon_cache[$thread['icon']])) {
                        $posticon = $icon_cache[$thread['icon']];
                        $posticon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $posticon['path']);
                        $posticon['path'] = htmlspecialchars_uni($posticon['path']);
                        $posticon['name'] = htmlspecialchars_uni($posticon['name']);
                        $icon = "<img src=\"{$this->bb->asset_url}/{$posticon['path']}\" alt=\"{$posticon['name']}\" 
                            title=\"{$posticon['name']}\" />";
                    } else {
                        $icon = '&nbsp;';
                    }
                    if ($thread['poll']) {
                        $prefix = $this->lang->poll_prefix;
                    }

                    // Determine the folder
                    $folder = '';
                    $folder_label = '';
                    if (isset($thread['dot_icon'])) {
                        $folder = 'dot_';
                        $folder_label .= $this->lang->icon_dot;
                    }
                    $gotounread = '';
                    $isnew = 0;
                    $donenew = 0;
                    $last_read = 0;

                    if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
                        $forum_read = $readforums[$thread['fid']];

                        $read_cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                        if ($forum_read == 0 || $forum_read < $read_cutoff) {
                            $forum_read = $read_cutoff;
                        }
                    } else {
                        $forum_read = $forumsread[$thread['fid']];
                    }

                    if ($this->bb->settings['threadreadcut'] > 0 &&
                        $this->user->uid && $thread['lastpost'] > $forum_read) {
                        if (isset($thread['lastread'])) {
                            $last_read = $thread['lastread'];
                        } else {
                            $last_read = $read_cutoff;
                        }
                    } else {
                        $last_read = $this->bb->my_get_array_cookie('threadread', $thread['tid']);
                    }

                    if ($forum_read > $last_read) {
                        $last_read = $forum_read;
                    }

                    if ($thread['lastpost'] > $last_read && $last_read) {
                        $folder .= 'new';
                        $new_class = 'subject_new';
                        $folder_label .= $this->lang->icon_new;
                        $thread['newpostlink'] = get_thread_link($thread['tid'], 0, 'newpost') . $highlight;
                        $gotounread = "<a href=\"{$thread['newpostlink']}\">
                            <img src=\"{$this->bb->theme['imgdir']}/jump.png\" alt=\"{$this->lang->goto_first_unread}\"
                            title=\"{$this->lang->goto_first_unread}\" /></a> ";
                        $unreadpost = 1;
                    } else {
                        $new_class = 'subject_old';
                        $folder_label .= $this->lang->icon_no_new;
                    }

                    if ($thread['replies'] >= $this->bb->settings['hottopic'] ||
                        $thread['views'] >= $this->bb->settings['hottopicviews']) {
                        $folder .= 'hot';
                        $folder_label .= $this->lang->icon_hot;
                    }
                    if ($thread['closed'] == 1) {
                        $folder .= 'lock';
                        $folder_label .= $this->lang->icon_lock;
                    }
                    $folder .= 'folder';

                    if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                        $this->bb->settings['postsperpage'] = 20;
                    }

                    $thread['pages'] = 0;
                    $thread['multipage'] = '';
                    $threadpages = '';
                    $morelink = '';
                    $thread['posts'] = $thread['replies'] + 1;
                    if ($this->user->is_moderator($thread['fid'], 'canviewunapprove')) {
                        $thread['posts'] += $thread['unapprovedposts'];
                    }
                    if ($this->user->is_moderator($thread['fid'], 'canviewdeleted')) {
                        $thread['posts'] += $thread['deletedposts'];
                    }

                    if ($thread['posts'] > $this->bb->settings['postsperpage']) {
                        $thread['pages'] = $thread['posts'] / $this->bb->settings['postsperpage'];
                        $thread['pages'] = ceil($thread['pages']);
                        if ($thread['pages'] > $this->bb->settings['maxmultipagelinks']) {
                            $pagesstop = $this->bb->settings['maxmultipagelinks'] - 1;
                            $page_link = get_thread_link($thread['tid'], $thread['pages']) . $highlight;
                            $morelink = "... <a href=\"{$page_link}\">{$thread['pages']}</a>";
                        } else {
                            $pagesstop = $thread['pages'];
                        }
                        for ($i = 1; $i <= $pagesstop; ++$i) {
                            $page_link = get_thread_link($thread['tid'], $i) . $highlight;
                            $threadpages .= "<a href=\"{$page_link}\">{$i}</a> ";
                        }
//FIXME no lang->pages in search            $thread['multipage'] = " <span class=\"smalltext\">({$this->lang->pages}
// {$threadpages}{$morelink})</span>";
                        $thread['multipage'] = " <span class=\"smalltext\">( {$threadpages}{$morelink})</span>";
                    } else {
                        $threadpages = '';
                        $morelink = '';
                        $thread['multipage'] = '';
                    }
                    $lastpostdate = $this->time->formatDate('relative', $thread['lastpost']);
                    $lastposter = $thread['lastposter'];
                    $thread['lastpostlink'] = get_thread_link($thread['tid'], 0, 'lastpost');
                    $lastposteruid = $thread['lastposteruid'];
                    $thread_link = get_thread_link($thread['tid']);

                    // Don't link to guest's profiles (they have no profile).
                    if ($lastposteruid == 0) {
                        $lastposterlink = $lastposter;
                    } else {
                        $lastposterlink = $this->user->build_profile_link($lastposter, $lastposteruid);
                    }

                    $thread['replies'] = $this->parser->formatNumber($thread['replies']);
                    $thread['views'] = $this->parser->formatNumber($thread['views']);
                    $thread['forumlink'] = '';
                    if ($forumcache[$thread['fid']]) {
                        $thread['forumlink'] = "<a href=\"" . $this->forum->get_forum_link($thread['fid']) . "\">" .
                            $forumcache[$thread['fid']]['name'] . '</a>';
                    }

                    // If this user is the author of the thread and it is not closed or they are a moderator,
                    //  they can edit
                    $inline_edit_class = '';
                    if (($thread['uid'] == $this->user->uid && $thread['closed'] != 1 &&
                            $this->user->uid != 0 &&
                            $fpermissions[$thread['fid']]['caneditposts'] == 1) ||
                        $this->user->is_moderator($thread['fid'], 'caneditposts')
                    ) {
                        $inline_edit_class = 'subject_editable';
                    }

                    // If this thread has 1 or more attachments show the papperclip
                    $attachment_count = '';
                    if ($this->bb->settings['enableattachments'] == 1 && $thread['attachmentcount'] > 0) {
                        if ($thread['attachmentcount'] > 1) {
                            $attachment_count = $this->lang->sprintf(
                                $this->lang->attachment_count_multiple,
                                $thread['attachmentcount']
                            );
                        } else {
                            $attachment_count = $this->lang->attachment_count;
                        }
                        // $attachment_count = "<div class=\"float_right\"><img
                        // src=\"{$this->bb->theme['imgdir']}/paperclip.png\" alt=\"\"
                        // title=\"{$attachment_count}\" /></div>";
                    }

                    // Inline thread moderation
                    $inline_mod_checkbox = false;
                    if ($is_supermod || $this->user->is_moderator($thread['fid'])) {
                        if (isset($this->bb->cookies[$this->inlinecookie]) &&
                            my_strpos($this->bb->cookies[$this->inlinecookie], "|{$thread['tid']}|")) {
                            $inlinecheck = 'checked="checked"';
                            ++$this->inlinecount;
                        } else {
                            $inlinecheck = '';
                        }
                        $inline_mod_checkbox = true;
                        //"<td class=\"{$bgcolor}\" align=\"center\" style=\"white-space: nowrap\">
                        //<input type=\"checkbox\" class=\"checkbox\" name=\"inlinemod_{$thread['tid']}\"
                        //id=\"inlinemod_{$thread['tid']}\" value=\"1\" style=\"vertical-align: middle;\"
                        // {$inlinecheck}  /></td>";
                    }
                    $this->view->offsetSet('inlinecount', $this->inlinecount);
                    $this->plugins->runHooks('search_results_thread');
                    $results[] = [
                        'bgcolor' => $bgcolor,
                        'folder' => $folder,
                        'folder_label' => $folder_label,
                        'icon' => $icon,
                        'attachment_count' => $attachment_count,
                        'prefix' => $prefix,
                        'gotounread' => $gotounread,
                        'threadprefix' => $thread['threadprefix'],
                        'thread_link' => $thread_link,
                        'highlight' => $highlight,
                        'inline_edit_class' => $inline_edit_class,
                        'new_class' => $new_class,
                        'inline_edit_tid' => $thread['tid'],
                        'subject' => $thread['subject'],
                        'multipage' => $thread['multipage'],
                        'profilelink' => $thread['profilelink'],
                        'forumlink' => $thread['forumlink'],
                        'tid' => $thread['tid'],
                        'replies' => $thread['replies'],
                        'views' => $thread['views'],
                        'lastpostdate' => $lastpostdate,
                        'lastpostlink' => $thread['lastpostlink'],
                        'lastposterlink' => $lastposterlink,
                        'inline_mod_checkbox' => $inline_mod_checkbox,
                        'inlinecheck' => $inlinecheck
                    ];
                }
                if (empty($results)) {
                    $this->bb->error($this->lang->error_nosearchresults);
                }
                $this->view->offsetSet('results', $results);

                $multipage = $this->pagination->multipage(
                    $threadcount,
                    $perpage,
                    $page,
                    "search?action=results&amp;sid=$sid&amp;sortby=$sortby&amp;order=$order&amp;uid=" .
                    $this->bb->getInput('uid', 0)
                );
                $this->view->offsetSet('multipage', $multipage);
                if ($upper > $threadcount) {
                    $upper = $threadcount;
                }

                // Inline Thread Moderation Options
                $this->bb->showInlineMod = false;
                if ($is_mod) {
                    $this->bb->showInlineMod = true;
                    // If user has moderation tools available, prepare the Select All feature
                    $this->lang->page_selected = $this->lang->sprintf($this->lang->page_selected, count($thread_cache));
                    $this->lang->all_selected = $this->lang->sprintf($this->lang->all_selected, (int)$threadcount);
                    $this->lang->select_all = $this->lang->sprintf($this->lang->select_all, (int)$threadcount);

                    $this->view->offsetSet('threadcount', $threadcount);

                    $customthreadtools = '';
//                    switch ($this->db->type) {
//                        case 'pgsql':
//                        case 'sqlite':
//                            $query = $this->db->simple_select('modtools', 'tid, name', "type='t' AND
//                                   (','||forums||',' LIKE '%,-1,%' OR forums='')");
//                            break;
//                        default:
//                            $query = $this->db->simple_select('modtools', 'tid, name', "type='t' AND
//                                  (CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='')");
//                    }
                    $t = \RunBB\Models\Modtool::where('type', '=', "'t'")
                        ->where(function ($q) {
                            if ($this->settings['db']['default'] === 'mysql') {
                                $q->raw("(CONCAT(',',forums,',') LIKE '%,-1,%'");
                            } else {
                                $q->raw("','||forums||',' LIKE '%,-1,%'");
                            }
                            $q->orWhere('forums', '=', "''");
                        })
                        ->get([
                            'tid', 'name'
                        ])
                        ->toArray();

//                    while ($tool = $this->db->fetch_array($query)) {
                    foreach ($t as $tool) {
                        $customthreadtools .= "<option value=\"{$tool['tid']}\">{$tool['name']}</option>";
                    }
                    // Build inline moderation dropdown
                    if (!empty($customthreadtools)) {
                        $customthreadtools = "<optgroup label=\"{$this->lang->custom_mod_tools}\">
                            {$customthreadtools}</optgroup>";
                    }
                    $this->view->offsetSet('customthreadtools', $customthreadtools);
                }

                $this->plugins->runHooks('search_results_end');

                \Tracy\Debugger::barDump('search_results_threads / '.__METHOD__.' / '.__LINE__);
                $this->bb->output_page();
                $this->view->render($response, '@forum/search_results_threads.html.twig');
            } else // Displaying results as posts
            {
                if (!$search['posts']) {
                    $this->bb->error($this->lang->error_nosearchresults);
                }

                $postcount = 0;

                // Moderators can view unapproved threads
                $query = $this->db->simple_select(
                    'moderators',
                    'fid, canviewunapprove, canviewdeleted',
                    "(id='{$this->user->uid}' AND isgroup='0') OR (id='{$this->user->usergroup}' AND isgroup='1')"
                );
                if ($this->bb->usergroup['issupermod'] == 1) {
                    // Super moderators (and admins)
                    $unapproved_where = 'visible >= -1';
                } elseif ($this->db->num_rows($query)) {
                    // Normal moderators
                    $unapprove_forums = [];
                    $deleted_forums = [];
                    $unapproved_where = '(visible = 1';

                    while ($moderator = $this->db->fetch_array($query)) {
                        if ($moderator['canviewunapprove'] == 1) {
                            $unapprove_forums[] = $moderator['fid'];
                        }

                        if ($moderator['canviewdeleted'] == 1) {
                            $deleted_forums[] = $moderator['fid'];
                        }
                    }

                    if (!empty($unapprove_forums)) {
                        $unapproved_where .= ' OR (visible = 0 AND fid IN(' . implode(',', $unapprove_forums) . '))';
                    }
                    if (!empty($deleted_forums)) {
                        $unapproved_where .= ' OR (visible = -1 AND fid IN(' . implode(',', $deleted_forums) . '))';
                    }
                    $unapproved_where .= ')';
                } else {
                    // Normal users
                    $unapproved_where = 'visible = 1';
                }

                $post_cache_options = [];
                if ((int)$this->bb->settings['searchhardlimit'] > 0) {
                    $post_cache_options['limit'] = (int)$this->bb->settings['searchhardlimit'];
                }

                if (strpos($sortfield, 'p.') !== false) {
                    $post_cache_options['order_by'] = str_replace('p.', '', $sortfield);
                    $post_cache_options['order_dir'] = $order;
                }

                $tids = [];
                $pids = [];
                // Make sure the posts we're viewing we have permission to view.
                $query = $this->db->simple_select(
                    'posts',
                    'pid, tid',
                    'pid IN(' . $this->db->escape_string($search['posts']) . ") AND {$unapproved_where}",
                    $post_cache_options
                );
                while ($post = $this->db->fetch_array($query)) {
                    $pids[$post['pid']] = $post['tid'];
                    $tids[$post['tid']][$post['pid']] = $post['pid'];
                }

                if (!empty($pids)) {
                    $temp_pids = [];

                    $group_permissions = $this->forum->forum_permissions();
                    $permsql = '';
                    $onlyusfids = [];

                    foreach ($group_permissions as $fid => $forum_permissions) {
                        if (!empty($forum_permissions['canonlyviewownthreads'])) {
                            $onlyusfids[] = $fid;
                        }
                    }

                    if ($onlyusfids) {
                        $permsql .= ' OR (fid IN(' . implode(',', $onlyusfids) . ") AND uid!={$this->user->uid})";
                    }
                    $unsearchforums = $this->search->get_unsearchable_forums();
                    if ($unsearchforums) {
                        $permsql .= ' OR fid IN (' . implode(',', $unsearchforums) . ')';
                    }
                    $inactiveforums = $this->forum->get_inactive_forums();
                    if ($inactiveforums) {
                        $permsql .= ' OR fid IN (' . implode(',', $inactiveforums) . ')';
                    }

                    // Check the thread records as well. If we don't have permissions, remove them from the listing.
                    $query = $this->db->simple_select(
                        'threads',
                        'tid',
                        'tid IN(' . $this->db->escape_string(implode(',', $pids)) . ") 
                        AND ({$unapproved_where}{$permsql} OR closed LIKE 'moved|%')"
                    );
                    while ($thread = $this->db->fetch_array($query)) {
                        if (array_key_exists($thread['tid'], $tids) != true) {
                            $temp_pids = $tids[$thread['tid']];
                            foreach ($temp_pids as $pid) {
                                unset($pids[$pid]);
                                unset($tids[$thread['tid']]);
                            }
                        }
                    }
                    unset($temp_pids);
                }

                // Declare our post count
                $postcount = count($pids);

                if (!$postcount) {
                    $this->bb->error($this->lang->error_nosearchresults);
                }

                // And now we have our sanatized post list
                $search['posts'] = implode(',', array_keys($pids));

                $tids = implode(',', array_keys($tids));

                // Read threads
                if ($this->user->uid && $this->bb->settings['threadreadcut'] > 0) {
                    $query = $this->db->simple_select(
                        'threadsread',
                        'tid, dateline',
                        "uid='" . $this->user->uid . "' AND tid IN(" . $this->db->escape_string($tids) . ')'
                    );
                    while ($readthread = $this->db->fetch_array($query)) {
                        $readthreads[$readthread['tid']] = $readthread['dateline'];
                    }
                }

                $dot_icon = [];
                if ($this->bb->settings['dotfolders'] != 0 && $this->user->uid != 0) {
                    $query = $this->db->simple_select(
                        'posts',
                        'DISTINCT tid,uid',
                        "uid='{$this->user->uid}' AND tid IN({$this->db->escape_string($tids)}) AND {$unapproved_where}"
                    );
                    while ($post = $this->db->fetch_array($query)) {
                        $dot_icon[$post['tid']] = true;
                    }
                }

                $results = [];
                $query = $this->db->query('
			SELECT p.*, u.username AS userusername, t.subject AS thread_subject, t.replies AS thread_replies,
			    t.views AS thread_views, t.lastpost AS thread_lastpost, t.closed AS thread_closed, t.uid as thread_uid
			FROM ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=p.uid)
			WHERE p.pid IN (' . $this->db->escape_string($search['posts']) . ")
			ORDER BY $sortfield $order
			LIMIT $start, $perpage
		");
                while ($post = $this->db->fetch_array($query)) {
                    $bgcolor = alt_trow();
                    if ($post['visible'] == 0) {
                        $bgcolor = 'trow_shaded';
                    } elseif ($post['visible'] == -1) {
                        $bgcolor = 'trow_shaded trow_deleted';
                    }
                    if ($post['userusername']) {
                        $post['username'] = $post['userusername'];
                    }
                    $post['profilelink'] = $this->user->build_profile_link($post['username'], $post['uid']);
                    $post['subject'] = $this->parser->parse_badwords($post['subject']);
                    $post['thread_subject'] = $this->parser->parse_badwords($post['thread_subject']);
                    $post['thread_subject'] = htmlspecialchars_uni($post['thread_subject']);

                    $icon = '&nbsp;';
                    if (isset($icon_cache[$post['icon']])) {
                        $posticon = $icon_cache[$post['icon']];
                        $posticon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $posticon['path']);
                        $posticon['path'] = htmlspecialchars_uni($posticon['path']);
                        $posticon['name'] = htmlspecialchars_uni($posticon['name']);
                        $icon = "<img src=\"{$this->bb->asset_url}/{$posticon['path']}\" alt=\"{$posticon['name']}\"
                            title=\"{$posticon['name']}\" />";
                    }

                    $post['forumlink'] = '';
                    if (!empty($forumcache[$thread['fid']])) {
                        $post['forumlink'] = "<a href=\"" . $this->forum->get_forum_link($post['fid']) . "\">" .
                            $forumcache[$post['fid']]['name'] . '</a>';
                    }
                    // Determine the folder
                    $folder = '';
                    $folder_label = '';
                    $gotounread = '';
                    $isnew = 0;
                    $donenew = 0;
                    $last_read = 0;
                    $post['thread_lastread'] = isset($readthreads[$post['tid']]) ? $readthreads[$post['tid']] : '';

                    if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
                        $forum_read = $readforums[$post['fid']];

                        $read_cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                        if ($forum_read == 0 || $forum_read < $read_cutoff) {
                            $forum_read = $read_cutoff;
                        }
                    } else {
                        $forum_read = $forumsread[$post['fid']];
                    }

                    if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid &&
                        $post['thread_lastpost'] > $forum_read) {
                        $cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                        if ($post['thread_lastpost'] > $cutoff) {
                            if ($post['thread_lastread']) {
                                $last_read = $post['thread_lastread'];
                            } else {
                                $last_read = 1;
                            }
                        }
                    }

                    if (isset($dot_icon[$post['tid']])) {
                        $folder = 'dot_';
                        $folder_label .= $this->lang->icon_dot;
                    }

                    if (!$last_read) {
                        $readcookie = $threadread = $this->bb->my_get_array_cookie('threadread', $post['tid']);
                        if ($readcookie > $forum_read) {
                            $last_read = $readcookie;
                        } elseif ($forum_read > $this->user->lastvisit) {
                            $last_read = $forum_read;
                        } else {
                            $last_read = $this->user->lastvisit;
                        }
                    }

                    if ($post['thread_lastpost'] > $last_read && $last_read) {
                        $folder .= 'new';
                        $folder_label .= $this->lang->icon_new;
                        $gotounread = "<a href=\"{$thread['newpostlink']}\">
                            <img src=\"{$this->bb->theme['imgdir']}/jump.png\" alt=\"{$this->lang->goto_first_unread}\"
                            title=\"{$this->lang->goto_first_unread}\" /></a> ";
                        $unreadpost = 1;
                    } else {
                        $folder_label .= $this->lang->icon_no_new;
                    }

                    if ($post['thread_replies'] >= $this->bb->settings['hottopic'] ||
                        $post['thread_views'] >= $this->bb->settings['hottopicviews']) {
                        $folder .= 'hot';
                        $folder_label .= $this->lang->icon_hot;
                    }
                    if ($post['thread_closed'] == 1) {
                        $folder .= 'lock';
                        $folder_label .= $this->lang->icon_lock;
                    }
                    $folder .= 'folder';

                    $post['thread_replies'] = $this->parser->formatNumber($post['thread_replies']);
                    $post['thread_views'] = $this->parser->formatNumber($post['thread_views']);

                    if ($forumcache[$post['fid']]) {
                        $post['forumlink'] = "<a href=\"" . $this->forum->get_forum_link($post['fid']) . "\">" .
                            $forumcache[$post['fid']]['name'] . '</a>';
                    } else {
                        $post['forumlink'] = '';
                    }

                    if (!$post['subject']) {
                        $post['subject'] = $post['message'];
                    }
                    if (my_strlen($post['subject']) > 50) {
                        $post['subject'] = htmlspecialchars_uni(my_substr($post['subject'], 0, 50) . '...');
                    } else {
                        $post['subject'] = htmlspecialchars_uni($post['subject']);
                    }
                    // What we do here is parse the post using our post parser, then strip the tags from it
                    $parser_options = [
                        'allow_html' => 0,
                        'allow_mycode' => 1,
                        'allow_smilies' => 0,
                        'allow_imgcode' => 0,
                        'filter_badwords' => 1
                    ];
                    $post['message'] = strip_tags($this->parser->parse_message($post['message'], $parser_options));
                    if (my_strlen($post['message']) > 200) {
                        $prev = my_substr($post['message'], 0, 200) . '...';
                    } else {
                        $prev = $post['message'];
                    }
                    $posted = $this->time->formatDate('relative', $post['dateline']);

                    $thread_url = get_thread_link($post['tid']);
                    $post_url = get_post_link($post['pid'], $post['tid']);

                    // Inline post moderation
                    $inline_mod_checkbox = false;
                    if ($is_supermod || $this->user->is_moderator($post['fid'])) {
                        if (isset($this->bb->cookies[$this->inlinecookie]) &&
                            my_strpos($this->bb->cookies[$this->inlinecookie], "|{$post['pid']}|")) {
                            $inlinecheck = 'checked="checked"';
                            ++$this->inlinecount;
                        } else {
                            $inlinecheck = '';
                        }
                        $inline_mod_checkbox = true;
                    }
                    $this->view->offsetSet('inlinecount', $this->inlinecount);
                    $this->plugins->runHooks('search_results_post');
                    $results[] = [
                        'bgcolor' => $bgcolor,
                        'folder' => $folder,
                        'folder_label' => $folder_label,
                        'icon' => $icon,
                        'thread_url' => $thread_url,
                        'highlight' => $highlight,
                        'thread_subject' => $post['thread_subject'],
                        'post_url' => $post_url,
                        'pid' => $post['pid'],
                        'post_subject' => $post['subject'],
                        'prev' => $prev,
                        'profilelink' => $post['profilelink'],
                        'forumlink' => $post['forumlink'],
                        'tid' => $post['tid'],
                        'thread_replies' => isset($post['thread_replies']) ? $post['thread_replies'] : '',
                        'thread_views' => $post['thread_views'],
                        'posted' => $posted,
                        'inlinecheck' => $inlinecheck,
                        'inline_mod_checkbox' => $inline_mod_checkbox
                    ];
                }
                if (empty($results)) {
                    $this->bb->error($this->lang->error_nosearchresults);
                }
                $this->view->offsetSet('results', $results);

                $multipage = $this->pagination->multipage(
                    $postcount,
                    $perpage,
                    $page,
                    'search?action=results&amp;sid=' . htmlspecialchars_uni($this->bb->getInput('sid', 0)) .
                    "&amp;sortby=$sortby&amp;order=$order&amp;uid=" . $this->bb->getInput('uid', 0)
                );
                $this->view->offsetSet('multipage', $multipage);
                $this->view->offsetSet('postcount', $postcount);
                if ($upper > $postcount) {
                    $upper = $postcount;
                }

                // Inline Post Moderation Options
                $this->bb->showInlineMod = false;
                if ($is_mod) {
                    $this->bb->showInlineMod = true;
                    // If user has moderation tools available, prepare the Select All feature
                    $num_results = $this->db->num_rows($query);
                    $this->lang->page_selected = $this->lang->sprintf($this->lang->page_selected, (int)$num_results);
                    $this->lang->select_all = $this->lang->sprintf($this->lang->select_all, (int)$postcount);
                    $this->lang->all_selected = $this->lang->sprintf($this->lang->all_selected, (int)$postcount);

                    $customthreadtools = $customposttools = '';
                    switch ($this->db->type) {
                        case 'pgsql':
                        case 'sqlite':
                            $query = $this->db->simple_select(
                                'modtools',
                                'tid, name, type',
                                "type='p' AND (','||forums||',' LIKE '%,-1,%' OR forums='')"
                            );
                            break;
                        default:
                            $query = $this->db->simple_select(
                                'modtools',
                                'tid, name, type',
                                "type='p' AND (CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='')"
                            );
                    }

                    while ($tool = $this->db->fetch_array($query)) {
                        $customposttools .= "<option value=\"{$tool['tid']}\">{$tool['name']}</option>";
                    }
                    // Build inline moderation dropdown
                    if (!empty($customposttools)) {
                        $customposttools = "<optgroup label=\"{$this->lang->custom_mod_tools}\">
                            {$customposttools}</optgroup>";
                    }
                    $this->view->offsetSet('customposttools', $customposttools);
                }

                $this->plugins->runHooks('search_results_end');
                \Tracy\Debugger::barDump('search_results_posts / '.__METHOD__.' / '.__LINE__);
                $this->bb->output_page();
                $this->view->render($response, '@forum/search_results_posts.html.twig');
            }
        } elseif ($this->bb->input['action'] == 'findguest') {
            $where_sql = "uid='0'";

            $unsearchforums = $this->search->get_unsearchable_forums();
            if ($unsearchforums) {
                $where_sql .= ' AND fid NOT IN (' . implode(',', $unsearchforums) . ')';
            }
            $inactiveforums = $this->forum->get_inactive_forums();
            if ($inactiveforums) {
                $where_sql .= ' AND fid NOT IN (' . implode(',', $inactiveforums) . ')';
            }

            $permsql = '';
            $onlyusfids = [];

            // Check group permissions if we can't view threads not started by us
            $group_permissions = $this->forum->forum_permissions();
            foreach ($group_permissions as $fid => $forum_permissions) {
                if (isset($forum_permissions['canonlyviewownthreads']) &&
                    $forum_permissions['canonlyviewownthreads'] == 1) {
                    $onlyusfids[] = $fid;
                }
            }
            if (!empty($onlyusfids)) {
                $where_sql .= ' AND fid NOT IN(' . implode(',', $onlyusfids) . ')';
            }

            $options = [
                'order_by' => 'dateline',
                'order_dir' => 'desc'
            ];

            // Do we have a hard search limit?
            if ($this->bb->settings['searchhardlimit'] > 0) {
                $options['limit'] = (int)$this->bb->settings['searchhardlimit'];
            }

            $pids = '';
            $comma = '';
            $query = $this->db->simple_select('posts', 'pid', "{$where_sql}", $options);
            while ($pid = $this->db->fetch_field($query, 'pid')) {
                $pids .= $comma . $pid;
                $comma = ',';
            }

            $tids = '';
            $comma = '';
            $query = $this->db->simple_select('threads', 'tid', $where_sql);
            while ($tid = $this->db->fetch_field($query, 'tid')) {
                $tids .= $comma . $tid;
                $comma = ',';
            }

            $sid = md5(uniqid(microtime(), true));
            $searcharray = [
                'sid' => $this->db->escape_string($sid),
                'uid' => $this->user->uid,
                'dateline' => TIME_NOW,
                'ipaddress' => $this->session->ipaddress,
                'threads' => $this->db->escape_string($tids),
                'posts' => $this->db->escape_string($pids),
                'resulttype' => 'posts',
                'querycache' => '',
                'keywords' => ''
            ];
            $this->plugins->runHooks('search_do_search_process');
            $this->db->insert_query('searchlog', $searcharray);
            return $this->bb->redirect('search?action=results&sid=' . $sid, $this->lang->redirect_searchresults);
        } elseif ($this->bb->input['action'] == 'finduser') {
            $where_sql = "uid='" . $this->bb->getInput('uid', 0) . "'";

            $unsearchforums = $this->search->get_unsearchable_forums();
            if ($unsearchforums) {
                $where_sql .= ' AND fid NOT IN (' . implode(',', $unsearchforums) . ')';
            }
            $inactiveforums = $this->forum->get_inactive_forums();
            if ($inactiveforums) {
                $where_sql .= ' AND fid NOT IN (' . implode(',', $inactiveforums) . ')';
            }

            $permsql = '';
            $onlyusfids = [];

            // Check group permissions if we can't view threads not started by us
            $group_permissions = $this->forum->forum_permissions();
            foreach ($group_permissions as $fid => $forum_permissions) {
                if (isset($forum_permissions['canonlyviewownthreads']) &&
                    $forum_permissions['canonlyviewownthreads'] == 1) {
                    $onlyusfids[] = $fid;
                }
            }
            if (!empty($onlyusfids)) {
                $where_sql .= 'AND ((fid IN(' . implode(',', $onlyusfids) . ")
                    AND uid='{$this->user->uid}') OR fid NOT IN(" . implode(',', $onlyusfids) . '))';
            }

            $options = [
                'order_by' => 'dateline',
                'order_dir' => 'desc'
            ];

            // Do we have a hard search limit?
            if ($this->bb->settings['searchhardlimit'] > 0) {
                $options['limit'] = (int)$this->bb->settings['searchhardlimit'];
            }

            $pids = '';
            $comma = '';
            $query = $this->db->simple_select('posts', 'pid', "{$where_sql}", $options);
            while ($pid = $this->db->fetch_field($query, 'pid')) {
                $pids .= $comma . $pid;
                $comma = ',';
            }

            $tids = '';
            $comma = '';
            $query = $this->db->simple_select('threads', 'tid', $where_sql);
            while ($tid = $this->db->fetch_field($query, 'tid')) {
                $tids .= $comma . $tid;
                $comma = ',';
            }

            $sid = md5(uniqid(microtime(), true));
            $searcharray = [
                'sid' => $this->db->escape_string($sid),
                'uid' => $this->user->uid,
                'dateline' => TIME_NOW,
                'ipaddress' => $this->session->ipaddress,
                'threads' => $this->db->escape_string($tids),
                'posts' => $this->db->escape_string($pids),
                'resulttype' => 'posts',
                'querycache' => '',
                'keywords' => ''
            ];
            $this->plugins->runHooks('search_do_search_process');
            $this->db->insert_query('searchlog', $searcharray);
            return $this->bb->redirect('search?action=results&sid=' . $sid, $this->lang->redirect_searchresults);
        } elseif ($this->bb->input['action'] == 'finduserthreads') {
            $where_sql = "t.uid='" . $this->bb->getInput('uid', 0) . "'";

            $unsearchforums = $this->search->get_unsearchable_forums();
            if ($unsearchforums) {
                $where_sql .= ' AND t.fid NOT IN (' . implode(',', $unsearchforums) . ')';
            }
            $inactiveforums = $this->forum->get_inactive_forums();
            if ($inactiveforums) {
                $where_sql .= ' AND t.fid NOT IN (' . implode(',', $inactiveforums) . ')';
            }

            $permsql = '';
            $onlyusfids = [];

            // Check group permissions if we can't view threads not started by us
            $group_permissions = $this->forum->forum_permissions();
            foreach ($group_permissions as $fid => $forum_permissions) {
                if (isset($forum_permissions['canonlyviewownthreads']) &&
                    $forum_permissions['canonlyviewownthreads'] == 1) {
                    $onlyusfids[] = $fid;
                }
            }
            if (!empty($onlyusfids)) {
                $where_sql .= 'AND ((t.fid IN(' . implode(',', $onlyusfids) . ")
                AND t.uid='{$this->user->uid}') OR t.fid NOT IN(" . implode(',', $onlyusfids) . '))';
            }

            $sid = md5(uniqid(microtime(), true));
            $searcharray = [
                'sid' => $this->db->escape_string($sid),
                'uid' => $this->user->uid,
                'dateline' => TIME_NOW,
                'ipaddress' => $this->session->ipaddress,
                'threads' => '',
                'posts' => '',
                'resulttype' => 'threads',
                'querycache' => $this->db->escape_string($where_sql),
                'keywords' => ''
            ];
            $this->plugins->runHooks('search_do_search_process');
            $this->db->insert_query('searchlog', $searcharray);
            return $this->bb->redirect('search?action=results&sid=' . $sid, $this->lang->redirect_searchresults);
        } elseif ($this->bb->input['action'] == 'getnew') {
            $where_sql = "t.lastpost >= '" . (int)$this->user->lastvisit . "'";

            if ($this->bb->getInput('fid', 0)) {
                $where_sql .= " AND t.fid='" . $this->bb->getInput('fid', 0) . "'";
            } elseif ($this->bb->getInput('fids', '', false)) {
                $fids = explode(',', $this->bb->getInput('fids', '', false));
                foreach ($fids as $key => $fid) {
                    $fids[$key] = (int)$fid;
                }

                if (!empty($fids)) {
                    $where_sql .= ' AND t.fid IN (' . implode(',', $fids) . ')';
                }
            }

            $unsearchforums = $this->search->get_unsearchable_forums();
            if ($unsearchforums) {
                $where_sql .= ' AND t.fid NOT IN (' . implode(',', $unsearchforums) . ')';
            }
            $inactiveforums = $this->forum->get_inactive_forums();
            if ($inactiveforums) {
                $where_sql .= ' AND t.fid NOT IN (' . implode(',', $inactiveforums) . ')';
            }

            $permsql = '';
            $onlyusfids = [];

            // Check group permissions if we can't view threads not started by us
            $group_permissions = $this->forum->forum_permissions();
            foreach ($group_permissions as $fid => $forum_permissions) {
                if (isset($forum_permissions['canonlyviewownthreads']) &&
                    $forum_permissions['canonlyviewownthreads'] == 1) {
                    $onlyusfids[] = $fid;
                }
            }
            if (!empty($onlyusfids)) {
                $where_sql .= 'AND ((t.fid IN(' . implode(',', $onlyusfids) . ")
                AND t.uid='{$this->user->uid}') OR t.fid NOT IN(" . implode(',', $onlyusfids) . '))';
            }

            $sid = md5(uniqid(microtime(), true));
            $searcharray = [
                'sid' => $this->db->escape_string($sid),
                'uid' => $this->user->uid,
                'dateline' => TIME_NOW,
                'ipaddress' => $this->session->ipaddress,
                'threads' => '',
                'posts' => '',
                'resulttype' => 'threads',
                'querycache' => $this->db->escape_string($where_sql),
                'keywords' => ''
            ];

            $this->plugins->runHooks('search_do_search_process');
            $this->db->insert_query('searchlog', $searcharray);
            return $this->bb->redirect('search?action=results&sid=' . $sid, $this->lang->redirect_searchresults);
        } elseif ($this->bb->input['action'] == 'getdaily') {
            if ($this->bb->getInput('days', 0) < 1) {
                $days = 1;
            } else {
                $days = $this->bb->getInput('days', 0);
            }
            $datecut = TIME_NOW - (86400 * $days);

            $where_sql = "t.lastpost >='" . $datecut . "'";

            if ($this->bb->getInput('fid', 0)) {
                $where_sql .= " AND t.fid='" . $this->bb->getInput('fid', 0) . "'";
            } elseif ($this->bb->getInput('fids', '', false)) {
                $fids = explode(',', $this->bb->getInput('fids', '', false));
                foreach ($fids as $key => $fid) {
                    $fids[$key] = (int)$fid;
                }

                if (!empty($fids)) {
                    $where_sql .= ' AND t.fid IN (' . implode(',', $fids) . ')';
                }
            }

            $unsearchforums = $this->search->get_unsearchable_forums();
            if ($unsearchforums) {
                $where_sql .= ' AND t.fid NOT IN (' . implode(',', $unsearchforums) . ')';
            }
            $inactiveforums = $this->forum->get_inactive_forums();
            if ($inactiveforums) {
                $where_sql .= ' AND t.fid NOT IN (' . implode(',', $inactiveforums) . ')';
            }

            $permsql = '';
            $onlyusfids = [];

            // Check group permissions if we can't view threads not started by us
            $group_permissions = $this->forum->forum_permissions();
            foreach ($group_permissions as $fid => $forum_permissions) {
                if (isset($forum_permissions['canonlyviewownthreads']) &&
                    $forum_permissions['canonlyviewownthreads'] == 1) {
                    $onlyusfids[] = $fid;
                }
            }
            if (!empty($onlyusfids)) {
                $where_sql .= 'AND ((t.fid IN(' . implode(',', $onlyusfids) . ")
                AND t.uid='{$this->user->uid}') OR t.fid NOT IN(" . implode(',', $onlyusfids) . '))';
            }

            $sid = md5(uniqid(microtime(), true));
//            $searcharray = [
//                'sid' => $this->db->escape_string($sid),
//                'uid' => $this->user->uid,
//                'dateline' => TIME_NOW,
//                'ipaddress' => $this->session->ipaddress,
//                'threads' => '',
//                'posts' => '',
//                'resulttype' => 'threads',
//                'querycache' => $this->db->escape_string($where_sql),
//                'keywords' => ''
//            ];
            $s = \RunBB\Models\Searchlog::firstOrCreate(['sid' => $sid]);
            $s->uid = $this->user->uid;
            $s->dateline = TIME_NOW;
            $s->ipaddress = $this->session->ipaddress;
            $s->threads = '';
            $s->posts = '';
            $s->resulttype = 'threads';
            $s->querycache = $where_sql;
            $s->keywords = '';
            $s->save();

            $this->plugins->runHooks('search_do_search_process');
//            $this->db->insert_query('searchlog', $searcharray);
            return $this->bb->redirect('search?action=results&sid=' . $sid, $this->lang->redirect_searchresults);
        } elseif ($this->bb->input['action'] == 'do_search' && $this->bb->request_method == 'post') {
            $this->plugins->runHooks('search_do_search_start');

            // Check if search flood checking is enabled and user is not admin
            if ($this->bb->settings['searchfloodtime'] > 0 && $this->bb->usergroup['cancp'] != 1) {
                // Fetch the time this user last searched
                if ($this->user->uid) {
                    $conditions = "uid='{$this->user->uid}'";
                } else {
                    $conditions = "uid='0' AND ipaddress='" . $this->session->ipaddress ."'";
                }
                $timecut = TIME_NOW - $this->bb->settings['searchfloodtime'];
                $query = $this->db->simple_select(
                    'searchlog',
                    '*',
                    "$conditions AND dateline > '$timecut'",
                    ['order_by' => 'dateline', 'order_dir' => 'DESC']
                );
                $last_search = $this->db->fetch_array($query);
                // Users last search was within the flood time, show the error
                if ($last_search['sid']) {
                    $remaining_time = $this->bb->settings['searchfloodtime'] - (TIME_NOW - $last_search['dateline']);
                    if ($remaining_time == 1) {
                        $this->lang->error_searchflooding = $this->lang->sprintf(
                            $this->lang->error_searchflooding_1,
                            $this->bb->settings['searchfloodtime']
                        );
                    } else {
                        $this->lang->error_searchflooding = $this->lang->sprintf(
                            $this->lang->error_searchflooding,
                            $this->bb->settings['searchfloodtime'],
                            $remaining_time
                        );
                    }
                    $this->bb->error($this->lang->error_searchflooding);
                }
            }
            if ($this->bb->getInput('showresults', '') === 'threads') {
                $resulttype = 'threads';
            } else {
                $resulttype = 'posts';
            }

            $search_data = [
                'keywords' => $this->bb->input['keywords'],
                'author' => $this->bb->getInput('author', ''),
                'postthread' => $this->bb->getInput('postthread', 0),
                'matchusername' => $this->bb->getInput('matchusername', 0),
                'postdate' => $this->bb->getInput('postdate', 0),
                'pddir' => $this->bb->getInput('pddir', 0),
                'forums' => $this->bb->getInput('forums', ''),
                'findthreadst' => $this->bb->getInput('findthreadst', 0),
                'numreplies' => $this->bb->getInput('numreplies', 0),
                'threadprefix' => $this->bb->getInput('threadprefix', [])
            ];

            if ($this->user->is_moderator() && !empty($this->bb->input['visible'])) {
                $search_data['visible'] = $this->bb->getInput('visible', 0);
            }

            if ($this->db->can_search == true) {
                if ($this->bb->settings['searchtype'] == 'fulltext' &&
                    $this->db->supports_fulltext_boolean('posts') &&
                    $this->db->is_fulltext('posts')
                ) {
                    $search_results = $this->search->perform_search_mysql_ft($search_data);
                } else {
                    $search_results = $this->search->perform_search_mysql($search_data);
                }
                if ($search_results === 'exit') {
                    return;
                }
            } else {
                $this->bb->error($this->lang->error_no_search_support);
            }
            $sid = md5(uniqid(microtime(), true));
            $searcharray = [
                'sid' => $this->db->escape_string($sid),
                'uid' => $this->user->uid,
                'dateline' => $now,
                'ipaddress' => $this->session->ipaddress,
                'threads' => $search_results['threads'],
                'posts' => $search_results['posts'],
                'resulttype' => $resulttype,
                'querycache' => $search_results['querycache'],
                'keywords' => $this->db->escape_string($this->bb->input['keywords']),
            ];
            $this->plugins->runHooks('search_do_search_process');

            $this->db->insert_query('searchlog', $searcharray);

            if (my_strtolower($this->bb->getInput('sortordr', '')) === 'asc' ||
                my_strtolower($this->bb->getInput('sortordr', '') === 'desc')) {
                $sortorder = $this->bb->getInput('sortordr', '');
            } else {
                $sortorder = 'desc';
            }
            $sortby = htmlspecialchars_uni($this->bb->getInput('sortby', ''));
            $this->plugins->runHooks('search_do_search_end');
            return $this->bb->redirect($this->bb->settings['bburl'] . '/search?action=results&sid=' . $sid .
                '&sortby=' . $sortby . '&order=' . $sortorder, $this->lang->redirect_searchresults);
        } elseif ($this->bb->input['action'] == 'thread') {
            // Fetch thread info
            $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));
            $ismod = $this->user->is_moderator($thread['fid']);

            if (!$thread || ($thread['visible'] != 1 &&
                    $ismod == false &&
                    ($thread['visible'] != -1 ||
                        $this->bb->settings['soft_delete'] != 1 ||
                        !$this->user->uid ||
                        $this->user->uid != $thread['uid'])) ||
                ($thread['visible'] > 1 && $ismod == true)
            ) {
                $this->bb->error($this->lang->error_invalidthread);
            }

            // Get forum info
            $forum = $this->forum->get_forum($thread['fid']);
            if (!$forum) {
                $this->bb->error($this->lang->error_invalidforum);
            }

            $forum_permissions = $this->forum->forum_permissions($forum['fid']);

            if ($forum['open'] == 0 || $forum['type'] != 'f') {
                $this->bb->error($this->lang->error_closedinvalidforum);
            }
            if ($forum_permissions['canview'] == 0 ||
                $forum_permissions['canviewthreads'] != 1 ||
                (isset($forum_permissions['canonlyviewownthreads']) &&
                    $forum_permissions['canonlyviewownthreads'] != 0 &&
                    $thread['uid'] != $this->user->uid)
            ) {
                return $this->bb->error_no_permission();
            }

            $this->plugins->runHooks('search_thread_start');

            // Check if search flood checking is enabled and user is not admin
            if ($this->bb->settings['searchfloodtime'] > 0 && $this->bb->usergroup['cancp'] != 1) {
                // Fetch the time this user last searched
                if ($this->user->uid) {
                    $conditions = "uid='{$this->user->uid}'";
                } else {
                    $conditions = "uid='0' AND ipaddress=" . $this->session->ipaddress;
                }
                $timecut = TIME_NOW - $this->bb->settings['searchfloodtime'];
                $query = $this->db->simple_select(
                    'searchlog',
                    '*',
                    "$conditions AND dateline > '$timecut'",
                    ['order_by' => 'dateline', 'order_dir' => 'DESC']
                );
                $last_search = $this->db->fetch_array($query);

                // We shouldn't show remaining time if time is 0 or under.
                $remaining_time = $this->bb->settings['searchfloodtime'] - (TIME_NOW - $last_search['dateline']);
                // Users last search was within the flood time, show the error.
                if ($last_search['sid'] && $remaining_time > 0) {
                    if ($remaining_time == 1) {
                        $this->lang->error_searchflooding = $this->lang->sprintf(
                            $this->lang->error_searchflooding_1,
                            $this->bb->settings['searchfloodtime']
                        );
                    } else {
                        $this->lang->error_searchflooding = $this->lang->sprintf(
                            $this->lang->error_searchflooding,
                            $this->bb->settings['searchfloodtime'],
                            $remaining_time
                        );
                    }
                    $this->bb->error($this->lang->error_searchflooding);
                }
            }

            $search_data = [
                'keywords' => $this->bb->input['keywords'],
                'postthread' => 1,
                'tid' => $this->bb->getInput('tid', 0)
            ];

            if ($this->db->can_search == true) {
                if ($this->bb->settings['searchtype'] == 'fulltext' &&
                    $this->db->supports_fulltext_boolean('posts') &&
                    $this->db->is_fulltext('posts')
                ) {
                    $search_results = $this->search->perform_search_mysql_ft($search_data);
                } else {
                    $search_results = $this->search->perform_search_mysql($search_data);
                }
            } else {
                $this->bb->error($this->lang->error_no_search_support);
            }
            $sid = md5(uniqid(microtime(), true));
            $searcharray = [
                'sid' => $this->db->escape_string($sid),
                'uid' => $this->user->uid,
                'dateline' => $now,
                'ipaddress' => $this->session->ipaddress,
                'threads' => $search_results['threads'],
                'posts' => $search_results['posts'],
                'resulttype' => 'posts',
                'querycache' => $search_results['querycache'],
                'keywords' => $this->db->escape_string($this->bb->input['keywords'])
            ];
            $this->plugins->runHooks('search_thread_process');

            $this->db->insert_query('searchlog', $searcharray);

            $this->plugins->runHooks('search_do_search_end');
            $this->bb->redirect(
                $this->bb->settings['bburl'] . '/search?action=results&sid=' . $sid,
                $this->lang->redirect_searchresults
            );
        } else {
            $this->plugins->runHooks('search_start');

            $this->view->offsetSet('srchlist', $this->search->make_searchable_forums());
            $this->view->offsetSet('prefixselect', $this->thread->build_prefix_select('all', 'any', 1));

            $rowspan = 5;

            $this->bb->showModeratorOptions = false;
            if ($this->user->is_moderator()) {
                $this->bb->showModeratorOptions = true;
                $rowspan += 2;
            }
            $this->view->offsetSet('rowspan', $rowspan);


            $this->plugins->runHooks('search_end');
            \Tracy\Debugger::barDump('search / '.__METHOD__.' / '.__LINE__);
            $this->bb->output_page();
            $this->view->render($response, '@forum/search.html.twig');
        }
    }
}
