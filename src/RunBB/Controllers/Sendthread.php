<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Sendthread extends AbstractController
{

    private function common()
    {
        // Load global language phrases
        $this->lang->load('sendthread');

        // Get thread info
        $tid = $this->bb->getInput('tid', 0);
        $this->view->offsetSet('tid', $tid);
        $this->thread = $this->thread->get_thread($tid);

        // Invalid thread
        if (!$this->thread || $this->thread['visible'] != 1) {
            $this->bb->error($this->lang->error_invalidthread);
            return 'exit';
        }

        // Get thread prefix
        $breadcrumbprefix = '';
        $threadprefix = ['prefix' => ''];
        if ($this->thread['prefix']) {
            $threadprefix = $this->thread->build_prefixes($this->thread['prefix']);
            if (!empty($threadprefix['displaystyle'])) {
                $breadcrumbprefix = $threadprefix['displaystyle'] . '&nbsp;';
            }
        }
        $this->view->offsetSet('threadprefixprefix', $threadprefix['prefix']);

        $this->thread['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($this->thread['subject']));
        $this->view->offsetSet('threadsubject', $this->thread['subject']);

        // Make navigation
        $this->bb->build_forum_breadcrumb($this->thread['fid']);
        $this->bb->add_breadcrumb($breadcrumbprefix . $this->thread['subject'], get_thread_link($this->thread['tid']));
        $this->bb->add_breadcrumb($this->lang->nav_sendthread);

        // Get forum info
        $forum = $this->forum->get_forum($this->thread['fid']);
        $forumpermissions = $this->forum->forum_permissions($forum['fid']);

        // Invalid forum?
        if (!$forum['fid'] || $forum['type'] != 'f') {
            $this->bb->error($this->lang->error_invalidforum);
            return 'exit';
        }

        // This user can't view this forum or this thread
        if ($forumpermissions['canview'] == 0 ||
            $forumpermissions['canviewthreads'] == 0 ||
            (isset($forumpermissions['canonlyviewownthreads']) &&
                $forumpermissions['canonlyviewownthreads'] != 0 &&
                $this->thread['uid'] != $this->user->uid)
        ) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($forum['fid']);

        if ($this->bb->usergroup['cansendemail'] == 0) {
            $this->bb->error_no_permission();
            return 'exit';
        }

        // Check group limits
        if ($this->bb->usergroup['maxemails'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $query = $this->db->simple_select(
                'maillogs',
                'COUNT(*) AS sent_count',
                "{$user_check} AND dateline >= '" . (TIME_NOW - (60 * 60 * 24)) . "'"
            );
            $sent_count = $this->db->fetch_field($query, 'sent_count');
            if ($sent_count >= $this->bb->usergroup['maxemails']) {
                $this->lang->error_max_emails_day = $this->lang->sprintf(
                    $this->lang->error_max_emails_day,
                    $this->bb->usergroup['maxemails']
                );
                $this->bb->error($this->lang->error_max_emails_day);
                return 'exit';
            }
        }

        // Check email flood control
        if ($this->bb->usergroup['emailfloodtime'] > 0) {
            if ($this->user->uid > 0) {
                $user_check = "fromuid='{$this->user->uid}'";
            } else {
                $user_check = 'ipaddress=' . $this->session->ipaddress;
            }

            $timecut = TIME_NOW - $this->bb->usergroup['emailfloodtime'] * 60;

            $query = $this->db->simple_select(
                'maillogs',
                'mid, dateline',
                "{$user_check} AND dateline > '{$timecut}'",
                ['order_by' => 'dateline', 'order_dir' => 'DESC']
            );
            $last_email = $this->db->fetch_array($query);

            // Users last email was within the flood time, show the error
            if ($last_email['mid']) {
                $remaining_time = ($this->bb->usergroup['emailfloodtime'] * 60) - (TIME_NOW - $last_email['dateline']);

                if ($remaining_time == 1) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_second,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } elseif ($remaining_time < 60) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_seconds,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time
                    );
                } elseif ($remaining_time > 60 && $remaining_time < 120) {
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_1_minute,
                        $this->bb->usergroup['emailfloodtime']
                    );
                } else {
                    $remaining_time_minutes = ceil($remaining_time / 60);
                    $this->lang->error_emailflooding = $this->lang->sprintf(
                        $this->lang->error_emailflooding_minutes,
                        $this->bb->usergroup['emailfloodtime'],
                        $remaining_time_minutes
                    );
                }

                $this->bb->error($this->lang->error_emailflooding);
                return 'exit';
            }
        }

        $this->errors = [];
    }

    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->common() == 'exit') {
                return;
            }
        }

        $this->plugins->runHooks('sendthread_start');

        // Do we have some errors?
        if (count($this->errors) >= 1) {
            $this->errors = $this->bb->inline_error($this->errors);
            $email = htmlspecialchars_uni($this->bb->input['email']);
            $fromname = htmlspecialchars_uni($this->bb->input['fromname']);
            $fromemail = htmlspecialchars_uni($this->bb->input['fromemail']);
            $subject = htmlspecialchars_uni($this->bb->input['subject']);
            $message = htmlspecialchars_uni($this->bb->input['message']);
        } else {
            $this->errors = '';
            $email = '';
            $fromname = '';
            $fromemail = '';
            $subject = $this->lang->sprintf($this->lang->emailsubject_sendtofriend, $this->bb->settings['bbname']);
            $message = '';
        }
        $this->view->offsetSet('fromname', $fromname);//FIXME get directly form input???
        $this->view->offsetSet('fromemail', $fromemail);
        $this->view->offsetSet('email', $email);
        $this->view->offsetSet('subject', $subject);
        $this->view->offsetSet('message', $message);

        // Generate CAPTCHA?
        if ($this->bb->settings['captchaimage'] && $this->user->uid == 0) {
            $post_captcha = new \RunBB\Core\Captcha($this, true, 'post_captcha');

            if ($post_captcha->html) {
                $captcha = $post_captcha->html;
            }
        } else {
            $captcha = '';
        }
        $this->view->offsetSet('errors', $this->errors);
        $this->view->offsetSet('captcha', $captcha);

        $this->plugins->runHooks('sendthread_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/sendthread.html.twig');
    }

    public function doSend(Request $request, Response $response)
    {
        if ($this->common() == 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->input['my_post_key']);

        $this->plugins->runHooks('sendthread_do_sendtofriend_start');

        if (!validate_email_format($this->bb->input['email'])) {
            $this->errors[] = $this->lang->error_invalidemail;
        }

        if ($this->user->uid) {
            $this->bb->input['fromemail'] = $this->user->email;
            $this->bb->input['fromname'] = $this->user->username;
        }

        if (!validate_email_format($this->bb->input['fromemail'])) {
            $this->errors[] = $this->lang->error_invalidfromemail;
        }

        if (empty($this->bb->input['fromname'])) {
            $this->errors[] = $this->lang->error_noname;
        }

        if (empty($this->bb->input['subject'])) {
            $this->errors[] = $this->lang->error_nosubject;
        }

        if (empty($this->bb->input['message'])) {
            $this->errors[] = $this->lang->error_nomessage;
        }

        if ($this->bb->settings['captchaimage'] && $this->user->uid == 0) {
            $captcha = new \RunBB\Core\Captcha($this);

            if ($captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($captcha->get_errors() as $error) {
                    $this->errors[] = $error;
                }
            }
        }

        // No errors detected
        if (count($this->errors) == 0) {
            if ($this->bb->settings['mail_handler'] == 'smtp') {
                $from = $this->bb->input['fromemail'];
            } else {
                $from = "{$this->bb->input['fromname']} <{$this->bb->input['fromemail']}>";
            }

            $threadlink = get_thread_link($this->thread['tid']);

            $message = $this->lang->sprintf(
                $this->lang->email_sendtofriend,
                $this->bb->input['fromname'],
                $this->bb->settings['bbname'],
                $this->bb->settings['bburl'] . '/' . $threadlink,
                $this->bb->input['message']
            );

            // Send the actual message
            $this->mail->send(
                $this->bb->input['email'],
                $this->bb->input['subject'],
                $message,
                $from,
                '',
                '',
                false,
                'text',
                '',
                $this->bb->input['fromemail']
            );

            if ($this->bb->settings['mail_logging'] > 0) {
                // Log the message
                $log_entry = [
                    'subject' => $this->db->escape_string($this->bb->input['subject']),
                    'message' => $this->db->escape_string($message),
                    'dateline' => TIME_NOW,
                    'fromuid' => $this->user->uid,
                    'fromemail' => $this->db->escape_string($this->bb->input['fromemail']),
                    'touid' => 0,
                    'toemail' => $this->db->escape_string($this->bb->input['email']),
                    'tid' => $this->thread['tid'],
                    'ipaddress' => $this->session->ipaddress,
                    'type' => 2
                ];
                $this->db->insert_query('maillogs', $log_entry);
            }

            $this->plugins->runHooks('sendthread_do_sendtofriend_end');
            $this->bb->redirect(get_thread_link($this->thread['tid']), $this->lang->redirect_emailsent);
        } else {
            //$this->bb->input['action'] = '';
            $this->index($request, $response, true);
        }
    }
}
