<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Showteam
{
    private $c = null;

    public function __construct($c)
    {
        $this->c = $c;
    }

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->c['lang']->load('showteam');

        $this->c['bb']->add_breadcrumb($this->c['lang']->nav_showteam);

        $this->c['plugins']->runHooks('showteam_start');

        $timecut = TIME_NOW - $this->c['bb']->settings['wolcutoff'];

        $usergroups = $moderators = $users = [];

        // Fetch the list of groups which are to be shown on the page
//        $query = $this->db->simple_select('usergroups', 'gid, title, usertitle', 'showforumteam=1',
//            array('order_by' => 'disporder'));
        $ug = \RunBB\Models\Usergroup::where('showforumteam', '=', 1)
            ->orderBy('disporder')
            ->get(['gid', 'title', 'usertitle'])
            ->toArray();

//        while ($usergroup = $this->db->fetch_array($query)) {
        foreach ($ug as $usergroup) {
            $usergroups[$usergroup['gid']] = $usergroup;
        }

        if (empty($usergroups)) {
            $this->c['bb']->error($this->c['lang']->error_noteamstoshow);
        }

        // Fetch specific forum moderator details
        if ($usergroups[6]['gid']) {
//            $query = $this->db->query('
//		SELECT m.*, f.name
//		FROM ' . TABLE_PREFIX . 'moderators m
//		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=m.id)
//		LEFT JOIN ' . TABLE_PREFIX . 'forums f ON (f.fid=m.fid)
//		WHERE f.active = 1 AND m.isgroup = 0
//		ORDER BY u.username
//	');
            $mod = \RunBB\Models\Moderator::where('forums.active', '=', 1)
                ->where('moderators.isgroup', '=', 0)
                ->orderBy('users.username')
                ->leftJoin('users', 'users.uid', '=', 'moderators.id')
                ->leftJoin('forums', 'forums.fid', '=', 'moderators.fid')
                ->get(['moderators.*', 'forums.name'])
                ->toArray();

//            while ($moderator = $this->db->fetch_array($query)) {
            foreach ($mod as $moderator) {
                $moderators[$moderator['id']][] = $moderator;
            }
        }

        // Now query the users of those specific groups
//        $groups_in = implode(',', array_keys($usergroups));
//        $users_in = implode(',', array_keys($moderators));
        if (!$usergroups) {//$groups_in) {
//            $groups_in = 0;
            $usergroups = [0];
        }
        if (!$moderators) {//$users_in) {
//            $users_in = 0;
            $moderators = [0];
        }
        $forum_permissions = $this->c['forum']->forum_permissions();
        $forumlist = '';
//        $query = $this->db->simple_select('users',
//            'uid, username, displaygroup, usergroup, ignorelist, hideemail, receivepms, lastactive, lastvisit, invisible, away',
//            "displaygroup IN ($groups_in) OR (displaygroup='0' AND usergroup IN ($groups_in)) OR uid IN ($users_in)",
//            array('order_by' => 'username'));
        $u = \RunBB\Models\User::whereIn('displaygroup', array_keys($usergroups))
            ->orWhere(function ($q) use ($usergroups) {
                $q->where('displaygroup', '=', '0');
                $q->whereIn('usergroup', array_keys($usergroups));
            })
            ->orWhere(function ($q) use ($moderators) {
                $q->whereIn('uid', array_keys($moderators));
            })
            ->orderBy('username')
            ->get(['uid', 'username', 'displaygroup', 'usergroup', 'ignorelist', 'hideemail',
                'receivepms', 'lastactive', 'lastvisit', 'invisible', 'away'])
            ->toArray();

//        while ($user = $this->db->fetch_array($query)) {
        foreach ($u as $user) {
            // If this user is a moderator
            if (isset($moderators[$user['uid']])) {
                foreach ($moderators[$user['uid']] as $forum) {
                    if ($forum_permissions[$forum['fid']]['canview'] == 1) {
                        $forum_url = $this->c['forum']->get_forum_link($forum['fid']);
                        $forumlist .= "<a href=\"{$forum_url}\">{$forum['name']}</a><br/>";
                    }
                }
                $user['forumlist'] = $forumlist;
                $forumlist = '';
                $usergroups[6]['user_list'][$user['uid']] = $user;
            }

            if ($user['displaygroup'] == '6' || $user['usergroup'] == '6') {
                $usergroups[6]['user_list'][$user['uid']] = $user;
            }

            // Are they also in another group which is being shown on the list?
            if ($user['displaygroup'] != 0) {
                $group = $user['displaygroup'];
            } else {
                $group = $user['usergroup'];
            }

            if ($usergroups[$group] && $group != 6) {
                $usergroups[$group]['user_list'][$user['uid']] = $user;
            }
        }

        // Now we have all of our user details we can display them.
        $grouplist = [];
        foreach ($usergroups as $usergroup) {
            $usergrouprows = $modrows = [];

            // If we have no users - don't show this group
            if (!isset($usergroup['user_list'])) {
                continue;
            }

            $bgcolor = '';
            foreach ($usergroup['user_list'] as $user) {
                $user['username'] = $this->c['user']->format_name(
                    $user['username'],
                    $user['usergroup'],
                    $user['displaygroup']
                );
                $user['profilelink'] = get_profile_link($user['uid']);

                // For the postbit templates
                $post['uid'] = $user['uid'];
                $emailcode = $pmcode = '';
                if ($user['hideemail'] != 1) {
                    $emailcode = "<a href=\"{$this->c['bb']->settings['bburl']}/emailuser?uid={$post['uid']}\" title=\"{$this->c['lang']->postbit_email}\" class=\"postbit_email\"><span>{$this->c['lang']->postbit_button_email}</span></a>";
                }

                if ($user['receivepms'] != 0 &&
                    $this->c['bb']->settings['enablepms'] != 0 &&
                    my_strpos(',' . $user['ignorelist'] . ',', ',' . $this->c['user']->uid . ',') === false
                ) {
                    $pmcode = "<a href=\"{$this->c['bb']->settings['bburl']}/private/send?uid={$post['uid']}\" title=\"{$this->c['lang']->postbit_pm}\" class=\"postbit_pm\"><span>{$this->c['lang']->postbit_button_pm}</span></a>";
                }

                // For the online image
                if ($user['lastactive'] > $timecut &&
                    ($user['invisible'] == 0 || $this->c['bb']->usergroup['canviewwolinvis'] == 1) &&
                    $user['lastvisit'] != $user['lastactive']
                ) {
                    $status = 'online';
                } elseif ($user['away'] == 1 && $this->c['bb']->settings['allowaway'] != 0) {
                    $status = 'away';
                } else {
                    $status = 'offline';
                }

                if ($user['invisible'] == 1 &&
                    $this->c['bb']->usergroup['canviewwolinvis'] != 1 &&
                    $user['uid'] != $this->c['user']->uid) {
                    if ($user['lastactive']) {
                        $user['lastvisit'] = $this->c['lang']->lastvisit_hidden;
                    } else {
                        $user['lastvisit'] = $this->c['lang']->lastvisit_never;
                    }
                } else {
                    $user['lastvisit'] = $this->c['time']->formatDate('relative', $user['lastactive']);
                }

                $bgcolor = alt_trow();

                $this->c['plugins']->runHooks('showteam_user');

                // If the current group is a moderator group
                if ($usergroup['gid'] == 6 && !empty($user['forumlist'])) {
                    $modrows[] = [
                        'bgcolor' => $bgcolor,
                        'status' => $status,
                        'lang_status' => $this->c['lang']->$status,
                        'user' => $user,
                        'forumslist' => $user['forumlist'],
                        'emailcode' => $emailcode,
                        'pmcode' => $pmcode
                    ];
                } else {
                    $usergrouprows[] = [
                        'bgcolor' => $bgcolor,
                        'status' => $status,
                        'lang_status' => $this->c['lang']->$status,
                        'user' => $user,
                        'emailcode' => $emailcode,
                        'pmcode' => $pmcode
                    ];
                }
            }

            if ($modrows && $usergroup['gid'] == 6) {
                $grouplist[] = [
                    'modrows' => $modrows
                ];
            }

            if ($usergrouprows) {
                $grouplist[] = [
                    'title' => $usergroup['title'],
                    'usergrouprows' => $usergrouprows
                ];
            }
        }

        if (empty($grouplist)) {
            $this->c['bb']->error($this->c['lang']->error_noteamstoshow);
        }

        $this->c['view']->offsetSet('grouplist', $grouplist);

        $this->c['plugins']->runHooks('showteam_end');

        $this->c['bb']->output_page();
        $this->c['view']->render($response, '@forum/showteam.html.twig');
    }
}
