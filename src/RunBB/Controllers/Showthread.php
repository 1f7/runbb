<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Showthread extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->lang->load('showthread');

        // If there is no tid but a pid, trick the system into thinking there was a tid anyway.
        if (!empty($this->bb->input['pid']) && !isset($this->bb->input['tid'])) {
            // see if we already have the post information
            if (isset($style) && $style['pid'] == $this->bb->getInput('pid', 0) && $style['tid']) {
                $this->bb->input['tid'] = $style['tid'];
                unset($style['tid']); // stop the thread caching code from being tricked
            } else {
                $options = [
                    'limit' => 1
                ];
                $query = $this->db->simple_select('posts', 'fid,tid,visible', 'pid=' . $this->bb->getInput('pid', 0), $options);
                $post = $this->db->fetch_array($query);

                if (empty($post) || ($post['visible'] == 0 &&
                        !$this->user->is_moderator($post['fid'], 'canviewunapprove')) ||
                    ($post['visible'] == -1 && !$this->user->is_moderator($post['fid'], 'canviewdeleted'))
                ) {
                    // post does not exist --> show corresponding error
                    $this->bb->error($this->lang->error_invalidpost);
                }

                $this->bb->input['tid'] = $post['tid'];
            }
        }
        // Get the thread details from the database.
        $this->bb->threads = $this->thread->get_thread($this->bb->getInput('tid', 0));
        if (!$this->bb->threads || substr($this->bb->threads['closed'], 0, 6) == 'moved|') {
            $this->bb->error($this->lang->error_invalidthread);
        }

        // Get thread prefix if there is one.
        $this->bb->threads['threadprefix'] = '';
        $this->bb->threads['displayprefix'] = '';
        if ($this->bb->threads['prefix'] != 0) {
            $threadprefix = $this->thread->build_prefixes($this->bb->threads['prefix']);

            if (!empty($threadprefix['prefix'])) {
                $this->bb->threads['threadprefix'] = htmlspecialchars_uni($threadprefix['prefix']) . '&nbsp;';
                $this->bb->threads['displayprefix'] = $threadprefix['displaystyle'] . '&nbsp;';
            }
        }

        $reply_subject = $this->parser->parse_badwords($this->bb->threads['subject']);
        $this->bb->threads['subject'] = htmlspecialchars_uni($reply_subject);

        // Subject too long? Shorten it to avoid error message
        if (my_strlen($reply_subject) > 85) {
            $reply_subject = my_substr($reply_subject, 0, 82) . '...';
        }

        $this->view->offsetSet('reply_subject', htmlspecialchars_uni($reply_subject));
        $tid = $this->bb->threads['tid'];
        $fid = $this->bb->threads['fid'];
        $this->fid = $this->bb->threads['fid'];


        if (!$this->bb->threads['username']) {
            $this->bb->threads['username'] = $this->lang->guest;
        }

        // Is the currently logged in user a moderator of this forum?
        if ($this->user->is_moderator($fid)) {
            $ismod = true;
            if ($this->user->is_moderator($fid, 'canviewdeleted') == true ||
                $this->user->is_moderator($fid, 'canviewunapprove') == true
            ) {
                if ($this->user->is_moderator($fid, 'canviewunapprove') == true &&
                    $this->user->is_moderator($fid, 'canviewdeleted') == false
                ) {
                    $visibleonly = ' visible IN (0,1)';
                    $visibleonly2 = 'AND p.visible IN (0,1) AND t.visible IN (0,1)';
                } elseif ($this->user->is_moderator($fid, 'canviewdeleted') == true &&
                    $this->user->is_moderator($fid, 'canviewunapprove') == false
                ) {
                    $visibleonly = ' visible IN (-1,1)';
                    $visibleonly2 = 'AND p.visible IN (-1,1) AND t.visible IN (-1,1)';
                } else {
                    $visibleonly = ' visible IN (-1,0,1)';
                    $visibleonly2 = 'AND p.visible IN (-1,0,1) AND t.visible IN (-1,0,1)';
                }
            }
        } else {
            $ismod = false;
            $visibleonly = ' visible=1';
            $visibleonly2 = 'AND p.visible=1 AND t.visible=1';
        }

        // Make sure we are looking at a real thread here.
        if (($this->bb->threads['visible'] != 1 && $ismod == false) ||
            ($this->bb->threads['visible'] == 0 && !$this->user->is_moderator($fid, 'canviewunapprove')) ||
            ($this->bb->threads['visible'] == -1 && !$this->user->is_moderator($fid, 'canviewdeleted'))
        ) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $forumpermissions = $this->forum->forum_permissions($this->bb->threads['fid']);

        // Does the user have permission to view this thread?
        if ($forumpermissions['canview'] != 1 || $forumpermissions['canviewthreads'] != 1) {
            return $this->bb->error_no_permission();
        }

        if (isset($forumpermissions['canonlyviewownthreads']) &&
            $forumpermissions['canonlyviewownthreads'] == 1 &&
            $this->bb->threads['uid'] != $this->user->uid
        ) {
            return $this->bb->error_no_permission();
        }

        // Does the thread belong to a valid forum?
        $this->bb->forums = $this->forum->get_forum($fid);
        if (!$this->bb->forums || $this->bb->forums['type'] != 'f') {
            $this->bb->error($this->lang->error_invalidforum);
        }

        $threadnoteslink = '';
        if ($this->user->is_moderator($fid, 'canmanagethreads') && !empty($this->bb->threads['notes'])) {
            $threadnoteslink = " <a class=\"btn btn-default btn-xs\" href=\"{$this->bb->settings['bburl']}/moderation?action=threadnotes&modtype=thread&tid={$this->bb->threads['tid']}\">{$this->lang->view_thread_notes}</a>";
        }
        $this->view->offsetSet('threadnoteslink', $threadnoteslink);

        // Check if this forum is password protected and we have a valid password
        $this->forum->check_forum_password($this->bb->forums['fid']);

        // If there is no specific action, we must be looking at the thread.
        if (!$this->bb->getInput('action', '', false)) {
            $this->bb->input['action'] = 'thread';
        }

        // Jump to the unread posts.
        if ($this->bb->input['action'] == 'newpost') {
            // First, figure out what time the thread or forum were last read
            $query = $this->db->simple_select('threadsread', 'dateline', "uid='{$this->user->uid}' AND tid='{$this->bb->threads['tid']}'");
            $thread_read = $this->db->fetch_field($query, 'dateline');

            if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
                $query = $this->db->simple_select('forumsread', 'dateline', "fid='{$fid}' AND uid='{$this->user->uid}'");
                $forum_read = $this->db->fetch_field($query, 'dateline');

                $read_cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                if ($forum_read == 0 || $forum_read < $read_cutoff) {
                    $forum_read = $read_cutoff;
                }
            } else {
                $forum_read = (int)$this->bb->my_get_array_cookie('forumread', $fid);
            }

            if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid && $this->bb->threads['lastpost'] > $forum_read) {
                $cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                if ($this->bb->threads['lastpost'] > $cutoff) {
                    if ($thread_read) {
                        $lastread = $thread_read;
                    } else {
                        // Set $lastread to zero to make sure 'lastpost' is invoked in the last IF
                        $lastread = 0;
                    }
                }
            }

            if (!$lastread) {
                $readcookie = $threadread = (int)$this->bb->my_get_array_cookie('threadread', $this->bb->threads['tid']);
                if ($readcookie > $forum_read) {
                    $lastread = $readcookie;
                } else {
                    $lastread = $forum_read;
                }
            }

            if ($cutoff && $lastread < $cutoff) {
                $lastread = $cutoff;
            }

            // Next, find the proper pid to link to.
            $options = [
                'limit_start' => 0,
                'limit' => 1,
                'order_by' => 'dateline',
                'order_dir' => 'asc'
            ];

            $lastread = (int)$lastread;
            $query = $this->db->simple_select('posts', 'pid', "tid='{$tid}' AND dateline > '{$lastread}' AND {$visibleonly}", $options);
            $newpost = $this->db->fetch_array($query);

            if ($newpost['pid'] && $lastread) {
                $highlight = '';
                if ($this->bb->getInput('highlight', '')) {
                    $string = '&';
                    if ($this->bb->seo_support == true) {
                        $string = '?';
                    }

                    $highlight = $string . 'highlight=' . $this->bb->getInput('highlight', '');
                }

                return $response->withRedirect(htmlspecialchars_decode(get_post_link($newpost['pid'], $tid)) . $highlight . "#pid{$newpost['pid']}");
            } else {
                // show them to the last post
                $this->bb->input['action'] = 'lastpost';
            }
        }

        // Jump to the last post.
        if ($this->bb->input['action'] == 'lastpost') {
            if (my_strpos($this->bb->threads['closed'], 'moved|')) {
                $query = $this->db->query('
			SELECT p.pid
			FROM ' . TABLE_PREFIX . 'posts p
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON(p.tid=t.tid)
			WHERE t.fid='" . $this->bb->threads['fid'] . "' AND t.closed NOT LIKE 'moved|%' AND {$visibleonly2}
			ORDER BY p.dateline DESC
			LIMIT 1
		");
                $pid = $this->db->fetch_field($query, 'pid');
            } else {
                $options = [
                    'order_by' => 'dateline',
                    'order_dir' => 'desc',
                    'limit_start' => 0,
                    'limit' => 1
                ];
                $query = $this->db->simple_select('posts', 'pid', "tid={$tid} AND {$visibleonly}", $options);
                $pid = $this->db->fetch_field($query, 'pid');
            }
            return $response->withRedirect(htmlspecialchars_decode(get_post_link($pid, $tid)) . "#pid{$pid}");
        }

        // Jump to the next newest posts.
        if ($this->bb->input['action'] == 'nextnewest') {
            $options = [
                'limit_start' => 0,
                'limit' => 1,
                'order_by' => 'lastpost'
            ];
            $query = $this->db->simple_select(
                'threads',
                '*',
                "fid={$this->bb->threads['fid']}
                AND lastpost > {$this->bb->threads['lastpost']}
                AND {$visibleonly} AND closed NOT LIKE 'moved|%'",
                $options
            );
            $nextthread = $this->db->fetch_array($query);

            // Are there actually next newest posts?
            if (!$nextthread['tid']) {
                $this->bb->error($this->lang->error_nonextnewest);
            }
            $options = [
                'limit_start' => 0,
                'limit' => 1,
                'order_by' => 'dateline',
                'order_dir' => 'desc'
            ];
            $query = $this->db->simple_select('posts', 'pid', "tid='{$nextthread['tid']}'", $options);

            // Redirect to the proper page.
            $pid = $this->db->fetch_field($query, 'pid');
            return $response->withRedirect(htmlspecialchars_decode(get_post_link($pid, $nextthread['tid'])) . "#pid{$pid}");
        }

        // Jump to the next oldest posts.
        if ($this->bb->input['action'] == 'nextoldest') {
            $options = [
                'limit' => 1,
                'limit_start' => 0,
                'order_by' => 'lastpost',
                'order_dir' => 'desc'
            ];
            $query = $this->db->simple_select(
                'threads',
                '*',
                'fid=' . $this->bb->threads['fid'] . ' AND lastpost < ' . $this->bb->threads['lastpost'] . "
                AND {$visibleonly} AND closed NOT LIKE 'moved|%'",
                $options
            );
            $nextthread = $this->db->fetch_array($query);

            // Are there actually next oldest posts?
            if (!$nextthread['tid']) {
                $this->bb->error($this->lang->error_nonextoldest);
            }
            $options = [
                'limit_start' => 0,
                'limit' => 1,
                'order_by' => 'dateline',
                'order_dir' => 'desc'
            ];
            $query = $this->db->simple_select('posts', 'pid', "tid='" . $nextthread['tid'] . "'", $options);

            // Redirect to the proper page.
            $pid = $this->db->fetch_field($query, 'pid');
            return $response->withRedirect(htmlspecialchars_decode(get_post_link($pid, $nextthread['tid'])) . "#pid{$pid}");
        }

        $pid = $this->bb->input['pid'] = $this->bb->getInput('pid', 0);

        // Forumdisplay cache
        $forum_stats = $this->cache->read('forumsdisplay');

        $breadcrumb_multipage = [];
        if ($this->bb->settings['showforumpagesbreadcrumb']) {
            // How many pages are there?
            if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
                $this->bb->settings['threadsperpage'] = 20;
            }

//            $query = $this->db->simple_select('forums', 'threads, unapprovedthreads', "fid = '{$fid}'", array('limit' => 1));
//            $forum_threads = $this->db->fetch_array($query);
//            $threadcount = $forum_threads['threads'];

            $tc = \RunBB\Models\Forum::where('fid', '=', $fid)
                ->take(1)
                ->get(['threads', 'unapprovedthreads']);

            $threadcount = $tc[0]->threads;
            if ($this->user->is_moderator($fid, 'canviewunapprove') == true) {
//                $threadcount += $forum_threads['unapprovedthreads'];
                $threadcount += $tc[0]->unapprovedthreads;
            }

            // Limit to only our own threads
            $uid_only = '';
            if (isset($forumpermissions['canonlyviewownthreads']) && $forumpermissions['canonlyviewownthreads'] == 1) {
                $uid_only = " AND uid = '" . $this->user->uid . "'";

                $query = $this->db->simple_select(
                    'threads',
                    'COUNT(tid) AS threads',
                    "fid = '$fid' AND $visibleonly $uid_only",
                    ['limit' => 1]
                );
                $threadcount = $this->db->fetch_field($query, 'threads');
            }

            // If we have 0 threads double check there aren't any 'moved' threads
            if ($threadcount == 0) {
                $query = $this->db->simple_select(
                    'threads',
                    'COUNT(tid) AS threads',
                    "fid = '$fid' AND $visibleonly $uid_only",
                    ['limit' => 1]
                );
                $threadcount = $this->db->fetch_field($query, 'threads');
            }
            $this->view->offsetSet('threadcount', $threadcount);

            $stickybit = ' OR sticky=1';
            if ($this->bb->threads['sticky'] == 1) {
                $stickybit = ' AND sticky=1';
            }

            // Figure out what page the thread is actually on
//            switch ($this->db->type) {
//                case 'pgsql':
//                    $query = $this->db->query('
//				SELECT COUNT(tid) as threads
//				FROM ' . TABLE_PREFIX . "threads
//				WHERE fid = '$fid' AND (lastpost >= '" . (int)$this->bb->threads['lastpost'] . "'{$stickybit}) {$visibleonly} {$uid_only}
//				GROUP BY lastpost
//				ORDER BY lastpost DESC
//			");
//                    break;
//                default:
//                    $query = $this->db->simple_select('threads', 'COUNT(tid) as threads',
//                        "fid = '$fid' AND (lastpost >= '" . (int)$this->bb->threads['lastpost'] . "'{$stickybit})
//                         AND {$visibleonly} {$uid_only}",
//                        array('order_by' => 'lastpost', 'order_dir' => 'desc'));
//            }
//            $thread_position = $this->db->fetch_field($query, 'threads');

            $th = \RunBB\Models\Thread::where('fid', '=', $fid)
                ->where(function ($q) {
                    if ($this->bb->threads['sticky'] == 1) {
                        $q->where('lastpost', '>=', (int)$this->bb->threads['lastpost'])
                            ->where('sticky', '=', 1);
                    } else {
                        $q->where('lastpost', '>=', (int)$this->bb->threads['lastpost'])
                            ->orWhere('sticky', '=', 1);
                    }
                    if (!empty($uid_only)) {
                        $q->where('uid', '=', "'.$this->user->uid.'");
                    }
                })
                ->whereRaw($visibleonly)
                ->orderBy('lastpost', 'desc')
                ->count();
            $thread_position = $th;

            $thread_page = ceil(($thread_position / $this->bb->settings['threadsperpage']));

            $breadcrumb_multipage = [
                'num_threads' => $threadcount,
                'current_page' => $thread_page
            ];
        }

        // Build the navigation.
        $this->bb->build_forum_breadcrumb($fid, $breadcrumb_multipage);
        $this->bb->add_breadcrumb($this->bb->threads['displayprefix'] . $this->bb->threads['subject'], get_thread_link($this->bb->threads['tid']));

        $this->plugins->runHooks('showthread_start');

        // Show the entire thread (taking into account pagination).
        if ($this->bb->input['action'] == 'thread') {
            if ($this->bb->threads['firstpost'] == 0) {
                $this->thread->update_first_post($tid);
            }

            // Does this thread have a poll?
            $this->bb->showPollBox = false;
            if ($this->bb->threads['poll']) {
                $options = [
                    'limit' => 1
                ];
                $query = $this->db->simple_select('polls', '*', "pid='" . $this->bb->threads['poll'] . "'", $options);
                $poll = $this->db->fetch_array($query);
                $poll['timeout'] = $poll['timeout'] * 60 * 60 * 24;
                $expiretime = $poll['dateline'] + $poll['timeout'];
                $now = TIME_NOW;

                // If the poll or the thread is closed or if the poll is expired, show the results.
                $showresults = 0;
                if ($poll['closed'] == 1 || $this->bb->threads['closed'] == 1 || ($expiretime < $now && $poll['timeout'] > 0)) {
                    $showresults = 1;
                }

                // If the user is not a guest, check if he already voted.
                $alreadyvoted = 0;
                if ($this->user->uid != 0) {
                    $query = $this->db->simple_select('pollvotes', '*', "uid='" . $this->user->uid . "' AND pid='" . $poll['pid'] . "'");
                    while ($votecheck = $this->db->fetch_array($query)) {
                        $alreadyvoted = 1;
                        $votedfor[$votecheck['voteoption']] = 1;
                    }
                } else {
                    if (isset($this->bb->cookies['pollvotes'][$poll['pid']]) && $this->bb->cookies['pollvotes'][$poll['pid']] !== '') {
                        $alreadyvoted = 1;
                    }
                }
                $optionsarray = explode('||~|~||', $poll['options']);
                $votesarray = explode('||~|~||', $poll['votes']);
                $poll['question'] = htmlspecialchars_uni($poll['question']);
                $polloptions = [];
                $totalvotes = 0;
                $poll['totvotes'] = 0;

                for ($i = 1; $i <= $poll['numoptions']; ++$i) {
                    $poll['totvotes'] = $poll['totvotes'] + $votesarray[$i - 1];
                }

                // Loop through the poll options.
                for ($i = 1; $i <= $poll['numoptions']; ++$i) {
                    // Set up the parser options.
                    $parser_options = [
                        'allow_html' => $this->bb->forums['allowhtml'],
                        'allow_mycode' => $this->bb->forums['allowmycode'],
                        'allow_smilies' => $this->bb->forums['allowsmilies'],
                        'allow_imgcode' => $this->bb->forums['allowimgcode'],
                        'allow_videocode' => $this->bb->forums['allowvideocode'],
                        'filter_badwords' => 1
                    ];

                    if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                        ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
                    ) {
                        $parser_options['allow_imgcode'] = 0;
                    }

                    if (($this->user->showvideos != 1 && $this->user->uid != 0) ||
                        ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
                    ) {
                        $parser_options['allow_videocode'] = 0;
                    }

                    $option = $this->parser->parse_message($optionsarray[$i - 1], $parser_options);
                    $votes = $votesarray[$i - 1];
                    $totalvotes += $votes;
                    $number = $i;

                    // Mark the option the user voted for.
                    if (!empty($votedfor[$number])) {
                        $optionbg = 'trow2';
                        $votestar = '*';
                    } else {
                        $optionbg = 'trow1';
                        $votestar = '';
                    }

                    // If the user already voted or if the results need to be shown, do so; else show voting screen.
                    if ($alreadyvoted === 1 || $showresults === 1) {
                        if ((int)$votes == '0') {
                            $percent = '0';
                        } else {
                            $percent = number_format($votes / $poll['totvotes'] * 100, 2);
                        }
                        $polloptions[]=[
                            'optionbg' => $optionbg,
                            'option' => $option,
                            'votestar' => $votestar,
                            'imagewidth' => round($percent),
                            'percent' => $percent,
                            'votes' => $votes
                        ];
                    } else {
                        $polloptions[]=[
                            'multiple' => $poll['multiple'],
                            'optionbg' => $optionbg,
                            'option' => $option,
                            'number' => $number,
                        ];
                    }
                }
                $this->view->offsetSet('polloptions', $polloptions);

                // If there are any votes at all, all votes together will be 100%;
                // if there are no votes, all votes together will be 0%.
                if ($poll['totvotes']) {
                    $totpercent = '100%';
                } else {
                    $totpercent = '0%';
                }
                $this->view->offsetSet('totpercent', $totpercent);

                // Check if user is allowed to edit posts; if so, show 'edit poll' link.
                $edit_poll = '';
                if ($this->user->is_moderator($fid, 'canmanagepolls')) {
                    $edit_poll = "  <a class=\"btn btn-primary\" href=\"{$this->bb->settings['bburl']}/polls/editpoll?pid={$poll['pid']}\">{$this->lang->edit_poll}</a>";
                }
                $this->view->offsetSet('edit_poll', $edit_poll);

                $this->bb->showPollBox = true;
                // Decide what poll status to show depending on the status of the poll and whether or not the user voted already.
                $this->bb->showPollBoxResults = false;
                if ($alreadyvoted === 1 || $showresults === 1) {
                    $this->bb->showPollBoxResults = true;
                    if ($alreadyvoted) {
                        $pollstatus = $this->lang->already_voted;

                        if ($this->bb->usergroup['canundovotes'] == 1) {
                            $pollstatus .= " <a class=\"btn btn-default btn-xs\" href=\"{$this->bb->settings['bburl']}/polls/undovote?pid={$poll['pid']}&my_post_key={$this->bb->post_code}\">{$this->lang->undo_vote}</a>";
                        }
                    } else {
                        $pollstatus = $this->lang->poll_closed;
                    }
                    $this->view->offsetSet('pollstatus', $pollstatus);
                    $this->lang->total_votes = $this->lang->sprintf($this->lang->total_votes, $totalvotes);
                    $this->plugins->runHooks('showthread_poll_results');
                    $pollbox = true;
                } else {
                    $closeon = '';
                    if ($poll['timeout'] != 0) {
                        $closeon = $this->lang->sprintf($this->lang->poll_closes, $this->time->formatDate($this->bb->settings['dateformat'], $expiretime));
                    }

                    $publicnote = '&nbsp;';
                    if ($poll['public'] == 1) {
                        $publicnote = $this->lang->public_note;
                    }
                    $this->view->offsetSet('closeon', $closeon);
                    $this->view->offsetSet('publicnote', $publicnote);
                    $pollbox = true;
                    $this->plugins->runHooks('showthread_poll');
                }
                $this->view->offsetSet('poll', $poll);
            } else {
                $pollbox = false;
            }

            // Create the forum jump dropdown box.
            $forumjump = '';
            if ($this->bb->settings['enableforumjump'] != 0) {
                $forumjump = $this->forum->build_forum_jump('', $fid, 1);
            }
            $this->view->offsetSet('forumjump', $forumjump);

            // Fetch some links
            $this->view->offsetSet('next_oldest_link', get_thread_link($tid, 0, 'nextoldest'));
            $this->view->offsetSet('next_newest_link', get_thread_link($tid, 0, 'nextnewest'));

            // Mark this thread as read
            $this->indicator->mark_thread_read($tid, $fid);

            // If the forum is not open, show closed newreply button unless the user is a moderator of this forum.
            $newthread = $newreply = '';
            if ($this->bb->forums['open'] != 0 && $this->bb->forums['type'] == 'f') {
                if ($forumpermissions['canpostthreads'] != 0 && $this->user->suspendposting != 1) {
                    //$newthread = "<a href=\"{$this->bb->settings['bburl']}/newthread?fid={$fid}\" class=\"button new_thread_button\"><span>{$this->lang->post_thread}</span></a>&nbsp;";
                    $newthread = "<a href=\"{$this->bb->settings['bburl']}/newthread?fid={$fid}\" class=\"btn btn-primary active\"><span>{$this->lang->post_thread}</span></a>&nbsp;";
                }

                // Show the appropriate reply button if this thread is open or closed
                if ($forumpermissions['canpostreplys'] != 0 &&
                    $this->user->suspendposting != 1 &&
                    ($this->bb->threads['closed'] != 1 ||
                        $this->user->is_moderator($fid, 'canpostclosedthreads')) &&
                    ($this->bb->threads['uid'] == $this->user->uid ||
                        $forumpermissions['canonlyreplyownthreads'] != 1)
                ) {
                    //$newreply = "<a href=\"{$this->bb->settings['bburl']}/newreply?tid={$tid}\" class=\"button new_reply_button\"><span>{$this->lang->new_reply}</span></a>&nbsp;";
                    $newreply = "<a href=\"{$this->bb->settings['bburl']}/newreply?tid={$tid}\" class=\"btn btn-primary active\"><span>{$this->lang->new_reply}</span></a>&nbsp;";
                } elseif ($this->bb->threads['closed'] == 1) {
                    //$newreply = "<a href=\"{$this->bb->settings['bburl']}/newreply?tid={$tid}\" class=\"button closed_button\"><span>{$this->lang->thread_closed}</span></a>&nbsp;";
                    $newreply = "<a href=\"{$this->bb->settings['bburl']}/newreply?tid={$tid}\" class=\"btn btn-primary disabled\"><span>{$this->lang->thread_closed}</span></a>&nbsp;";
                }
            }
            $this->view->offsetSet('newreply', $newreply);
            //$this->view->offsetSet('newthread', $newthread);

            // Create the admin tools dropdown box.
            $closeoption = '';
            if ($ismod == true) {
                $closelinkch = $stickch = '';

                if ($this->bb->threads['closed'] == 1) {
                    $closelinkch = ' checked="checked"';
                }

                if ($this->bb->threads['sticky']) {
                    $stickch = ' checked="checked"';
                }

                if ($this->user->is_moderator($this->bb->threads['fid'], 'canopenclosethreads')) {
                    $closeoption .= "<br /><label><input type=\"checkbox\" class=\"checkbox-inline\" name=\"modoptions[closethread]\" value=\"1\"{$closelinkch} />&nbsp;<strong>{$this->lang->close_thread}</strong></label>";
                }

                if ($this->user->is_moderator($this->bb->threads['fid'], 'canstickunstickthreads')) {
                    $closeoption .= "<br /><label><input type=\"checkbox\" class=\"checkbox-inline\" name=\"modoptions[stickthread]\" value=\"1\"{$stickch} />&nbsp;<strong>{$this->lang->stick_thread}</strong></label>";
                }

                $this->inlinecount = '0';
                $this->inlinecookie = 'inlinemod_thread' . $tid;

                $this->plugins->runHooks('showthread_ismod');
            } else {
                $modoptions = '';
                $inlinemod = '';
            }
            $this->view->offsetSet('closeoption', $closeoption);

            // Increment the thread view.
            if ($this->bb->settings['delayedthreadviews'] == 1) {
                $this->db->shutdown_query('INSERT INTO ' . TABLE_PREFIX . "threadviews (tid) VALUES('{$tid}')");
            } else {
                $this->db->shutdown_query('UPDATE ' . TABLE_PREFIX . "threads SET views=views+1 WHERE tid='{$tid}'");
            }
            ++$this->bb->threads['views'];

            // Work out the thread rating for this thread.
            $rating = '';
            if ($this->bb->settings['allowthreadratings'] != 0 && $this->bb->forums['allowtratings'] != 0) {
                $rated = 0;
                $this->lang->load('ratethread');
                if ($this->bb->threads['numratings'] <= 0) {
                    $this->bb->threads['width'] = 0;
                    $this->bb->threads['averagerating'] = 0;
                    $this->bb->threads['numratings'] = 0;
                } else {
                    $this->bb->threads['averagerating'] = (float)round($this->bb->threads['totalratings'] / $this->bb->threads['numratings'], 2);
                    $this->bb->threads['width'] = (int)round($this->bb->threads['averagerating']) * 20;
                    $this->bb->threads['numratings'] = (int)$this->bb->threads['numratings'];
                }

                if ($this->bb->threads['numratings']) {
                    // At least >someone< has rated this thread, was it me?
                    // Check if we have already voted on this thread - it won't show hover effect then.
//                    $query = $this->db->simple_select('threadratings', 'uid', "tid='{$tid}' AND uid='{$this->user->uid}'");
//                    $rated = $this->db->fetch_field($query, 'uid');
                    $rated = \RunBB\Models\Threadrating::where([
                        'tid' => $tid,
                        'uid' => $this->user->uid
                    ])
                        ->value('uid');
                }

                $not_rated = '';
                if (!$rated) {
                    $not_rated = ' star_rating_notrated';
                }
                $this->view->offsetSet('not_rated', $not_rated);

                $ratingvotesav = $this->lang->sprintf($this->lang->rating_average, $this->bb->threads['numratings'], $this->bb->threads['averagerating']);
                $this->view->offsetSet('ratingvotesav', $ratingvotesav);
            }
            // Work out if we are showing unapproved posts as well (if the user is a moderator etc.)
            if ($ismod && $this->user->is_moderator($fid, 'canviewdeleted') == true &&
                $this->user->is_moderator($fid, 'canviewunapprove') == false
            ) {
                $visible = 'posts.visible IN (-1,1)';
            } elseif ($ismod && $this->user->is_moderator($fid, 'canviewdeleted') == false &&
                $this->user->is_moderator($fid, 'canviewunapprove') == true
            ) {
                $visible = 'posts.visible IN (0,1)';
            } elseif ($ismod && $this->user->is_moderator($fid, 'canviewdeleted') == true &&
                $this->user->is_moderator($fid, 'canviewunapprove') == true
            ) {
                $visible = 'posts.visible IN (-1,0,1)';
            } else {
                $visible = "posts.visible='1'";
            }

            // Can this user perform searches? If so, we can show them the 'Search thread' form
            $this->bb->showSearchThread = false;
            if ($forumpermissions['cansearch'] != 0) {
                $this->bb->showSearchThread = true;
            }

            // Fetch the ignore list for the current user if they have one
            $ignored_users = [];
            if ($this->user->uid > 0 && $this->user->ignorelist != '') {
                $ignore_list = explode(',', $this->user->ignorelist);
                foreach ($ignore_list as $uid) {
                    $ignored_users[$uid] = 1;
                }
            }

            // Fetch profile fields to display on postbit
            $pfcache = $this->cache->read('profilefields');

            if (is_array($pfcache)) {
                foreach ($pfcache as $profilefield) {
                    if ($profilefield['postbit'] != 1) {
                        continue;
                    }

                    $profile_fields[$profilefield['fid']] = $profilefield;
                }
            }

            // Which thread mode is our user using by default?
            if (!empty($this->user->threadmode)) {
                $defaultmode = $this->user->threadmode;
            } elseif ($this->bb->settings['threadusenetstyle'] == 1) {
                $defaultmode = 'threaded';
            } else {
                $defaultmode = 'linear';
            }

            // If mode is unset, set the default mode
            if (!isset($this->bb->input['mode'])) {
                $this->bb->input['mode'] = $defaultmode;
            }

            // Threaded or linear display?
            $threadedbits = '';
            if ($this->bb->getInput('mode', '', false) === 'threaded') {
                $isfirst = 1;

                // Are we linked to a specific pid?
                if ($this->bb->input['pid']) {
                    $where = "AND p.pid='" . $this->bb->input['pid'] . "'";
                } else {
                    $where = ' ORDER BY dateline LIMIT 0, 1';
                }
//                $query = $this->db->query('
//			SELECT u.*, u.username AS userusername, p.*, f.*, eu.username AS editusername
//			FROM ' . TABLE_PREFIX . 'posts p
//			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=p.uid)
//			LEFT JOIN ' . TABLE_PREFIX . 'userfields f ON (f.ufid=u.uid)
//			LEFT JOIN ' . TABLE_PREFIX . "users eu ON (eu.uid=p.edituid)
//			WHERE p.tid='$tid' AND " . TABLE_PREFIX . "$visible $where
//		");
                $sp = \RunBB\Models\Post::where('posts.tid', '=', $tid)
                    ->whereRaw(TABLE_PREFIX . $visible)
                    ->where(function ($q) {
                        if ($this->bb->input['pid']) {
//                            $where = "AND p.pid='" . $this->bb->input['pid'] . "'";
                            $q->where('posts.pid', '=', $this->bb->input['pid']);
                        } else {
//                            $where = ' ORDER BY dateline LIMIT 0, 1';
                            $q->orderBy('dateline', 'asc');
                            $q->take(1);
                        }
                    })
                    ->leftJoin('users as u', 'u.uid', '=', 'posts.uid')
                    ->leftJoin('userfields', 'userfields.ufid', '=', 'u.uid')
                    ->leftJoin('users as eu', 'eu.uid', '=', 'posts.edituid')
                    ->get([
                        'u.*',
                        'u.username AS userusername',
                        'posts.*',
                        'userfields.*',
                        'eu.username AS editusername'
                    ])
                    ->toArray();
//                $showpost = $this->db->fetch_array($query);
                $showpost = $sp[0];

                // Choose what pid to display.
                if (!$this->bb->input['pid']) {
                    $this->bb->input['pid'] = $showpost['pid'];
                }

                // Is there actually a pid to display?
                if (!$showpost['pid']) {
                    $this->bb->error($this->lang->error_invalidpost);
                }

                $this->bb->attachcache = [];
                if ($this->bb->settings['enableattachments'] == 1 && $this->bb->threads['attachmentcount'] > 0 ||
                    $this->user->is_moderator($fid, 'caneditposts')
                ) {
                    // Get the attachments for this post.
                    $query = $this->db->simple_select('attachments', '*', 'pid=' . $this->bb->input['pid']);
                    while ($attachment = $this->db->fetch_array($query)) {
                        $this->bb->attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
                    }
                }

                // Build the threaded post display tree.
//                $query = $this->db->query('
//            SELECT p.username, p.uid, p.pid, p.replyto, p.subject, p.dateline
//            FROM ' . TABLE_PREFIX . "posts p
//            WHERE p.tid='$tid'
//            AND " . TABLE_PREFIX . "$visible
//            ORDER BY p.dateline
//        ");
                $p = \RunBB\Models\Post::where('tid', '=', $tid)
                    ->whereRaw(TABLE_PREFIX . $visible)
                    ->orderBy('dateline')
                    ->get([
                        'username',
                        'uid',
                        'pid',
                        'replyto',
                        'subject',
                        'dateline'
                    ])
                    ->toArray();
//                while ($post = $this->db->fetch_array($query)) {
                $postsdone = $this->tree = [];
                foreach ($p as $post) {
                    if (!isset($postsdone[$post['pid']])) {
                        if ($post['pid'] == $this->bb->input['pid'] || ($isfirst && !$this->bb->input['pid'])) {
                            $this->postcounter = count($postsdone);
                            $isfirst = 0;
                        }
                        $this->tree[$post['replyto']][$post['pid']] = $post;
                        $postsdone[$post['pid']] = 1;
                    }
                }

                $threadedbits = $this->buildtree($tid);//FIXME not tested
                $posts = $this->post->build_postbit($showpost);
                $this->plugins->runHooks('showthread_threaded');
            } else // Linear display
            {
                $threadexbox = '';
                if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                    $this->bb->settings['postsperpage'] = 20;
                }

                // Figure out if we need to display multiple pages.
                $page = 1;
                $perpage = $this->bb->settings['postsperpage'];
                if ($this->bb->getInput('page', 0) && $this->bb->getInput('page') != 'last') {
                    $page = $this->bb->getInput('page', 0);
                }

                if (!empty($this->bb->input['pid'])) {
                    $post = $this->post->get_post($this->bb->input['pid']);
                    if (empty($post) || ($post['visible'] == 0 &&
                            !$this->user->is_moderator($post['fid'], 'canviewunapprove')) ||
                        ($post['visible'] == -1 &&
                            !$this->user->is_moderator($post['fid'], 'canviewdeleted'))
                    ) {
                        $footer .= '<script type="text/javascript">$(document).ready(function() { $.jGrowl(\'' . $this->lang->error_invalidpost . '\', {theme: \'jgrowl_error\'}); });</script>';
                    } else {
//                        $query = $this->db->query('
//					SELECT COUNT(p.dateline) AS count
//					FROM ' . TABLE_PREFIX . "posts p
//					WHERE p.tid = '{$tid}'
//					AND p.dateline <= '{$post['dateline']}'
//					AND " . TABLE_PREFIX . "$visible");
                        $result = \RunBB\Models\Post::where([
                            ['tid', '=', $tid],
                            ['dateline', '<=', $post['dateline']]
                        ])
                            ->whereRaw(TABLE_PREFIX . $visible)
                            ->count();

//                        $result = $this->db->fetch_field($query, 'count');
                        if (($result % $perpage) == 0) {
                            $page = $result / $perpage;
                        } else {
                            $page = (int)($result / $perpage) + 1;
                        }
                    }
                }

                // Recount replies if user is a moderator to take into account unapproved posts.
                if ($ismod) {
//                    $query = $this->db->simple_select('posts p', 'COUNT(*) AS replies', "p.tid='$tid' AND " . TABLE_PREFIX . "$visible");
                    $p = \RunBB\Models\Post::where('tid', '=', $tid)
                        ->whereRaw(TABLE_PREFIX . $visible)
                        ->count();

                    $cached_replies = $this->bb->threads['replies'] + $this->bb->threads['unapprovedposts'] + $this->bb->threads['deletedposts'];
//                    $this->bb->threads['replies'] = $this->db->fetch_field($query, 'replies') - 1;
                    $this->bb->threads['replies'] = $p - 1;

                    // The counters are wrong? Rebuild them
                    // This doesn't cover all cases however it is a good addition to the manual rebuild function
                    if ($this->bb->threads['replies'] != $cached_replies) {
                        (new \RunBB\Helpers\Restorer($this))->rebuild_thread_counters($this->bb->threads['tid']);
                    }
                }

                $postcount = (int)$this->bb->threads['replies'] + 1;
                $pages = $postcount / $perpage;
                $pages = ceil($pages);

                if ($this->bb->getInput('page', '', false) === 'last') {
                    $page = $pages;
                }

                if ($page > $pages || $page <= 0) {
                    $page = 1;
                }

                if ($page) {
                    $start = ($page - 1) * $perpage;
                } else {
                    $start = 0;
                    $page = 1;
                }
                $upper = $start + $perpage;

                // Work out if we have terms to highlight
                $highlight = '';
                $threadmode = '';
                if ($this->bb->seo_support == true) {
                    if ($this->bb->getInput('highlight', '')) {
                        $highlight = '?highlight=' . urlencode($this->bb->getInput('highlight', ''));
                    }

                    if ($defaultmode != 'linear') {
                        if ($this->bb->getInput('highlight', '')) {
                            $threadmode = '&amp;mode=linear';
                        } else {
                            $threadmode = '?mode=linear';
                        }
                    }
                } else {
                    if (!empty($this->bb->input['highlight'])) {
                        if (is_array($this->bb->input['highlight'])) {
                            foreach ($this->bb->input['highlight'] as $highlight_word) {
                                $highlight .= '&amp;highlight[]=' . urlencode($highlight_word);
                            }
                        } else {
                            $highlight = '&amp;highlight=' . urlencode($this->bb->getInput('highlight', ''));
                        }
                    }

                    if ($defaultmode != 'linear') {
                        $threadmode = '&amp;mode=linear';
                    }
                }

                $multipage = $this->pagination->multipage($postcount, $perpage, $page, str_replace(
                    '{tid}',
                    $tid,
                    THREAD_URL_PAGED . $highlight . $threadmode
                ));
                $this->view->offsetSet('page', $page);
                $this->view->offsetSet('multipage', $multipage);

                // Lets get the pids of the posts on this page.
//                $pids = $comma = '';
                $pids = [];
//                $query = $this->db->simple_select('posts p', 'p.pid', "p.tid='$tid' AND $visible",
//                    array('order_by' => 'p.dateline', 'limit_start' => $start, 'limit' => $perpage));
                $p = \RunBB\Models\Post::where('tid', '=', $tid)
                    ->whereRaw(TABLE_PREFIX . $visible)
                    ->orderBy('dateline')
                    ->skip($start)
                    ->take($perpage)
                    ->get()
                    ->toArray();

//                while ($getid = $this->db->fetch_array($query)) {
                foreach ($p as $getid) {
                    // Set the ID of the first post on page to $pid if it doesn't hold any value
                    // to allow this value to be used for Thread Mode/Linear Mode links
                    // and ensure the user lands on the correct page after changing view mode
                    if (empty($pid)) {
                        $pid = $getid['pid'];
                    }
                    // Gather a comma separated list of post IDs
//                    $pids .= "$comma'{$getid['pid']}'";
                    $pids[] = $getid['pid'];
//                    $comma = ',';
                }

                if ($pids) {
//                    $pids = "pid IN($pids)";

                    $this->bb->attachcache = [];
                    if ($this->bb->settings['enableattachments'] == 1 && $this->bb->threads['attachmentcount'] > 0 ||
                        $this->user->is_moderator($fid, 'caneditposts')
                    ) {
                        // Now lets fetch all of the attachments for these posts.
//                        $query = $this->db->simple_select('attachments', '*', "pid IN($pids)");
                        $a = \RunBB\Models\Attachment::whereIn('pid', $pids)
                            ->get()->toArray();
//                        while ($attachment = $this->db->fetch_array($query)) {
                        foreach ($a as $attachment) {
                            $this->bb->attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
                        }
                    }
                } else {
                    // If there are no pid's the thread is probably awaiting approval.
                    $this->bb->error($this->lang->error_invalidthread);
                }

                // Get the actual posts from the database here.
                $posts = '';
//                $query = $this->db->query('
//			SELECT u.*, u.username AS userusername, p.*, f.*, eu.username AS editusername
//			FROM ' . TABLE_PREFIX . 'posts p
//			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=p.uid)
//			LEFT JOIN ' . TABLE_PREFIX . 'userfields f ON (f.ufid=u.uid)
//			LEFT JOIN ' . TABLE_PREFIX . "users eu ON (eu.uid=p.edituid)
//			WHERE pid IN($pids)
//			ORDER BY p.dateline
//		");
                $p = \RunBB\Models\Post::whereIn('pid', $pids)
                    ->orderBy('dateline')
                    ->leftJoin('users as u', 'u.uid', '=', 'posts.uid')
                    ->leftJoin('userfields', 'userfields.ufid', '=', 'u.uid')
                    ->leftJoin('users as eu', 'eu.uid', '=', 'posts.edituid')
                    ->get([
                        'u.*',
                        'u.username AS userusername',
                        'posts.*',
                        'userfields.*',
                        'eu.username AS editusername'
                    ])
                    ->toArray();

//                while ($post = $this->db->fetch_array($query)) {
                foreach ($p as $post) {
                    if ($this->bb->threads['firstpost'] == $post['pid'] && $this->bb->threads['visible'] == 0) {
                        $post['visible'] = 0;
                    }
                    $posts .= $this->post->build_postbit($post);
                    $post = '';
                }
                $this->plugins->runHooks('showthread_linear');
            }
            $this->view->offsetSet('posts', $posts);//FIXME !!!
            $this->view->offsetSet('threadedbits', $threadedbits);

            // Show the similar threads table if wanted.
            $similarthreadbits = [];// FIXME fulltext search
            if ($this->bb->settings['showsimilarthreads'] != 0) {
                $own_perm = '';
                if ($forumpermissions['canonlyviewownthreads'] == 1) {
                    $own_perm = " AND t.uid={$this->user->uid}";
                }

                switch ($this->db->type) {
//                    case 'pgsql':
//                        $query = $this->db->query('
//					SELECT t.*, t.username AS threadusername, u.username
//					FROM ' . TABLE_PREFIX . 'threads t
//					LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid = t.uid), plainto_tsquery ('" . $this->db->escape_string($this->bb->threads['subject']) . "') AS query
//					WHERE t.fid='{$this->bb->threads['fid']}' AND t.tid!='{$this->bb->threads['tid']}' AND t.visible='1' AND t.closed NOT LIKE 'moved|%' AND t.subject @@ query{$own_perm}
//					ORDER BY t.lastpost DESC
//					OFFSET 0 LIMIT {$this->bb->settings['similarlimit']}
//				");
//                        break;
                    default:
                        $query = $this->db->query("
					SELECT t.*, t.username AS threadusername, u.username, MATCH (t.subject) AGAINST ('" . $this->db->escape_string($this->bb->threads['subject']) . "') AS relevance
					FROM " . TABLE_PREFIX . "threads t
					LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid = t.uid)
					WHERE t.fid='{$this->bb->threads['fid']}' AND t.tid!='{$this->bb->threads['tid']}' AND t.visible='1' AND t.closed NOT LIKE 'moved|%'{$own_perm} AND MATCH (t.subject) AGAINST ('" . $this->db->escape_string($this->bb->threads['subject']) . "') >= '{$this->bb->settings['similarityrating']}'
					ORDER BY t.lastpost DESC
					LIMIT 0, {$this->bb->settings['similarlimit']}
				");
                }

                $count = 0;
                $similarthreadbits = '';
                $icon_cache = $this->cache->read('posticons');
                while ($similar_thread = $this->db->fetch_array($query)) {
                    ++$count;
                    //$trow = alt_trow();
                    if ($similar_thread['icon'] > 0 && $icon_cache[$similar_thread['icon']]) {
                        $icon = $icon_cache[$similar_thread['icon']];
                        $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
                        $icon['path'] = htmlspecialchars_uni($icon['path']);
                        $icon['name'] = htmlspecialchars_uni($icon['name']);
                        $icon = "<img src=\"{$icon['path']}\" alt=\"{$icon['name']}\" title=\"{$icon['name']}\" />";
                        //ev al("\$icon = \"" . $this->templates->get("forumdisplay_thread_icon") . "\";");
                    } else {
                        $icon = '&nbsp;';
                    }
                    if (!$similar_thread['username']) {
                        $similar_thread['username'] = $similar_thread['threadusername'];
                        $similar_thread['profilelink'] = $similar_thread['threadusername'];
                    } else {
                        $similar_thread['profilelink'] = $this->user->build_profile_link(
                            $similar_thread['username'],
                            $similar_thread['uid']
                        );
                    }

                    // If this thread has a prefix, insert a space between prefix and subject
                    if ($similar_thread['prefix'] != 0) {
                        $prefix = $this->thread->build_prefixes($similar_thread['prefix']);
                        if (!empty($prefix)) {
                            $similar_thread['threadprefix'] = $prefix['displaystyle'] . '&nbsp;';
                        }
                    }

                    $similar_thread['subject'] = $this->parser->parse_badwords($similar_thread['subject']);
                    $similar_thread['subject'] = htmlspecialchars_uni($similar_thread['subject']);
                    $similar_thread['threadlink'] = get_thread_link($similar_thread['tid']);
                    $similar_thread['lastpostlink'] = get_thread_link($similar_thread['tid'], 0, 'lastpost');

                    $lastpostdate = $this->time->formatDate('relative', $similar_thread['lastpost']);
                    $lastposter = $similar_thread['lastposter'];
                    $lastposteruid = $similar_thread['lastposteruid'];

                    // Don't link to guest's profiles (they have no profile).
                    if ($lastposteruid == 0) {
                        $lastposterlink = $lastposter;
                    } else {
                        $lastposterlink = $this->user->build_profile_link($lastposter, $lastposteruid);
                    }
                    $similar_thread['replies'] = $this->parser->formatNumber($similar_thread['replies']);
                    $similar_thread['views'] = $this->parser->formatNumber($similar_thread['views']);
                    $similarthreadbits[] = [
                        'trow' => alt_trow(),
                        'icon' => $icon,
                        'similar_thread' => $similar_thread,
                        'lastpostdate' => $lastpostdate,
                        'lastposterlink' => $lastposterlink
                    ];
                }
            }
            $this->view->offsetSet('similarthreadbits', $similarthreadbits);

            // Decide whether or not to show quick reply.
            $this->bb->showQuickReply = false;
            if ($forumpermissions['canpostreplys'] != 0 &&
                $this->user->suspendposting != 1 &&
                ($this->bb->threads['closed'] != 1 || $this->user->is_moderator($fid, 'canpostclosedthreads')) &&
                $this->bb->settings['quickreply'] != 0 &&
                $this->user->showquickreply != '0' &&
                $this->bb->forums['open'] != 0 &&
                ($this->bb->threads['uid'] == $this->user->uid ||
                    $forumpermissions['canonlyreplyownthreads'] != 1)
            ) {
                $this->bb->showQuickReply = true;
//                $query = $this->db->simple_select('posts', 'pid', "tid='{$tid}'",
//                    array('order_by' => 'pid', 'order_dir' => 'desc', 'limit' => 1));
//                $last_pid = $this->db->fetch_field($query, 'pid');
//                $this->view->offsetSet('last_pid', $last_pid);

                $lp = \RunBB\Models\Post::where('tid', '=', $tid)
                    ->orderBy('pid', 'desc')
                    ->take(1)
                    ->get(['pid']);

                $this->view->offsetSet('last_pid', $lp[0]->pid);

                // Show captcha image for guests if enabled
                $captcha = '';
                if ($this->bb->settings['captchaimage'] && !$this->user->uid) {
                    $post_captcha = new \RunBB\Core\Captcha($this, true, 'post_captcha');

                    if ($post_captcha->html) {
                        $captcha = $post_captcha->html;
                    }
                }
                $this->view->offsetSet('captcha', $captcha);

                $postoptionschecked = ['signature' => '', 'emailnotify' => ''];
                if ($this->user->signature) {
                    $postoptionschecked['signature'] = 'checked="checked"';
                }

                // Hide signature option if no permission
//        $option_signature = '';
//        if($this->bb->usergroup['canusesig'] && !$this->user->suspendsignature)
//        {
//          $option_signature = "<label><input type=\"checkbox\" class=\"checkbox\" name=\"postoptions[signature]\" value=\"1\" {{ postoptionschecked.signature }} />&nbsp;<strong>{$this->lang->signature}</strong></label><br />";
//        }

                if (isset($this->user->emailnotify) && $this->user->emailnotify == 1) {
                    $postoptionschecked['emailnotify'] = 'checked="checked"';
                }
                $this->view->offsetSet('postoptionschecked', $postoptionschecked);

                $trow = alt_trow();
                if ($this->bb->threads['closed'] == 1) {
                    $trow = 'trow_shaded';
                }
                $this->view->offsetSet('trow', $trow);

                $moderation_notice = '';
                if (!$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveposts')) {
                    if ($forumpermissions['modposts'] == 1) {
                        $moderation_notice = "<div class=\"pm_alert\">{$this->lang->moderation_forum_posts}</div>";
                    }

                    if ($this->user->moderateposts == 1) {
                        $moderation_notice = "<div class=\"pm_alert\">{$this->lang->moderation_user_posts}</div>";
                    }
                }
                $this->view->offsetSet('moderation_notice', $moderation_notice);
                $this->view->offsetSet('posthash', md5($this->user->uid . random_str()));
            }

            $this->bb->showModMenu = false;
            $this->bb->showThreadNotesBox = false;
            // If the user is a moderator, show the moderation tools.
            if ($ismod) {
                $customthreadtools = $customposttools = $standardthreadtools = $standardposttools = '';

                $viewnotes = '';
                if (!empty($this->bb->threads['notes'])) {
                    $this->bb->threads['notes'] = nl2br(htmlspecialchars_uni($this->bb->threads['notes']));

                    if (strlen($this->bb->threads['notes']) > 200) {
                        eval("\$viewnotes = \"" . $this->templates->get("showthread_threadnotes_viewnotes") . "\";");
                        $this->bb->threads['notes'] = my_substr($this->bb->threads['notes'], 0, 200) . "... {$viewnotes}";
                    }
                    $this->bb->showThreadNotesBox = true;
                }

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canusecustomtools') &&
                    (!empty($forum_stats[-1]['modtools']) ||
                        !empty($forum_stats[$this->bb->forums['fid']]['modtools']))
                ) {
                    $gids = explode(',', $this->user->additionalgroups);
                    $gids[] = $this->user->usergroup;
                    $gids = array_filter(array_unique($gids));
                    $gidswhere = '';
                    switch ($this->db->type) {
                        case 'pgsql':
                        case 'sqlite':
                            foreach ($gids as $gid) {
                                $gid = (int)$gid;
                                $gidswhere .= " OR ','||groups||',' LIKE '%,{$gid},%'";
                            }
                            $query = $this->db->simple_select(
                                'modtools',
                                'tid, name, type',
                                "(','||forums||',' LIKE '%,$fid,%' OR ','||forums||',' LIKE '%,-1,%' OR forums='') AND (groups='' OR ','||groups||',' LIKE '%,-1,%'{$gidswhere})"
                            );
                            break;
                        default:
                            foreach ($gids as $gid) {
                                $gid = (int)$gid;
                                $gidswhere .= " OR CONCAT(',',groups,',') LIKE '%,{$gid},%'";
                            }
                            $query = $this->db->simple_select(
                                'modtools',
                                'tid, name, type',
                                "(CONCAT(',',forums,',') LIKE '%,$fid,%' OR CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='') AND (groups='' OR CONCAT(',',groups,',') LIKE '%,-1,%'{$gidswhere})"
                            );
                            break;
                    }

                    while ($tool = $this->db->fetch_array($query)) {
                        if ($tool['type'] == 'p') {
                            $customposttools .= "<option value=\"{$tool['tid']}\">{$tool['name']}</option>";
                        } else {
                            $customthreadtools .= "<option value=\"{$tool['tid']}\">{$tool['name']}</option>";
                        }
                    }

                    // Build inline moderation dropdown
                    if (!empty($customposttools)) {
                        $customposttools = "<optgroup label=\"{$this->lang->custom_mod_tools}\">{$customposttools}</optgroup>";
                    }
                }
                $this->view->offsetSet('customposttools', $customposttools);

                $inlinemodsoftdelete = $inlinemodrestore = $inlinemoddelete = $inlinemodmanage = $inlinemodapprove = '';

                if ($this->user->is_moderator($this->bb->forums['fid'], 'cansoftdeleteposts')) {
                    $inlinemodsoftdelete = "<option value=\"multisoftdeleteposts\">{$this->lang->inline_soft_delete_posts}</option>";
                }
                $this->view->offsetSet('inlinemodsoftdelete', $inlinemodsoftdelete);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canrestoreposts')) {
                    $inlinemodrestore = "<option value=\"multirestoreposts\">{$this->lang->inline_restore_posts}</option>";
                }
                $this->view->offsetSet('inlinemodrestore', $inlinemodrestore);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'candeleteposts')) {
                    $inlinemoddelete = "<option value=\"multideleteposts\">{$this->lang->inline_delete_posts}</option>";
                }
                $this->view->offsetSet('inlinemoddelete', $inlinemoddelete);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canmanagethreads')) {
                    $inlinemodmanage = "
            <option value=\"multimergeposts\">{$this->lang->inline_merge_posts}</option>
            <option value=\"multisplitposts\">{$this->lang->inline_split_posts}</option>
            <option value=\"multimoveposts\">{$this->lang->inline_move_posts}</option>";
                }
                $this->view->offsetSet('inlinemodmanage', $inlinemodmanage);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveposts')) {
                    $inlinemodapprove = "
            <option value=\"multiapproveposts\">{$this->lang->inline_approve_posts}</option>
            <option value=\"multiunapproveposts\">{$this->lang->inline_unapprove_posts}</option>";
                }
                $this->view->offsetSet('inlinemodapprove', $inlinemodapprove);

                $this->bb->showStandardPostTools = false;
                if (!empty($inlinemodsoftdelete) ||
                    !empty($inlinemodrestore) ||
                    !empty($inlinemoddelete) ||
                    !empty($inlinemodmanage) ||
                    !empty($inlinemodapprove)
                ) {
                    $this->bb->showStandardPostTools = true;
                }

                // Only show inline mod menu if there's options to show
                $this->bb->showInlineMod = false;
                if ($this->bb->showStandardPostTools || !empty($customposttools)) {
                    $this->bb->showInlineMod = true;
                }

                // Build thread moderation dropdown
                if (!empty($customthreadtools)) {
                    $customthreadtools = "<optgroup label=\"{$this->lang->custom_mod_tools}\">{$customthreadtools}</optgroup>";
                }
                $this->view->offsetSet('customthreadtools', $customthreadtools);

                $openclosethread = $stickunstickthread = $deletethread = $threadnotes =
                $managethread = $adminpolloptions = $approveunapprovethread = $softdeletethread = '';

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canopenclosethreads')) {
                    $openclosethread = "<option value=\"openclosethread\">{$this->lang->open_close_thread}</option>";
                }
                $this->view->offsetSet('openclosethread', $openclosethread);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canstickunstickthreads')) {
                    $stickunstickthread = "<option value=\"stick\">{$this->lang->stick_unstick_thread}</option>";
                }
                $this->view->offsetSet('stickunstickthread', $stickunstickthread);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'candeletethreads')) {
                    $deletethread = "<option value=\"deletethread\">{$this->lang->delete_thread}</option>";
                }
                $this->view->offsetSet('deletethread', $deletethread);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canmanagethreads')) {
                    $threadnotes = "<option value=\"threadnotes\" selected=\"selected\">{$this->lang->thread_notes}</option>";
                    $managethread = "
            <option value=\"move\">{$this->lang->move_thread}</option>
            <option value=\"split\">{$this->lang->split_thread}</option>
            <option value=\"merge\">{$this->lang->merge_threads}</option>
            <option value=\"removeredirects\">{$this->lang->remove_redirects}</option>
            <option value=\"removesubscriptions\">{$this->lang->remove_subscriptions}</option>";
                }
                $this->view->offsetSet('threadnotes', $threadnotes);
                $this->view->offsetSet('managethread', $managethread);

                if ($pollbox && $this->user->is_moderator($this->bb->forums['fid'], 'canmanagepolls')) {
                    $adminpolloptions = "<option value=\"deletepoll\">{$this->lang->delete_poll}</option>";
                }
                $this->view->offsetSet('adminpolloptions', $adminpolloptions);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapprovethreads')) {
                    if ($this->bb->threads['visible'] == 0) {
                        $approveunapprovethread = "<option value=\"approvethread\">" . $this->lang->approve_thread . "</option>";
                    } else {
                        $approveunapprovethread = "<option value=\"unapprovethread\">" . $this->lang->unapprove_thread . "</option>";
                    }
                }
                $this->view->offsetSet('approveunapprovethread', $approveunapprovethread);

                if ($this->user->is_moderator($this->bb->forums['fid'], 'cansoftdeletethreads') && $this->bb->threads['visible'] != -1) {
                    $softdeletethread = "<option value=\"softdeletethread\">{$this->lang->soft_delete_thread}</option>";
                } elseif ($this->user->is_moderator($this->bb->forums['fid'], 'canrestorethreads') && $this->bb->threads['visible'] == -1) {
                    $softdeletethread = "<option value=\"restorethread\">{$this->lang->restore_thread}</option>";
                }
                $this->view->offsetSet('softdeletethread', $softdeletethread);

                $this->bb->showStandardThreadTools = false;
                if (!empty($openclosethread) ||
                    !empty($stickunstickthread) ||
                    !empty($deletethread) ||
                    !empty($managethread) ||
                    !empty($adminpolloptions) ||
                    !empty($approveunapprovethread) ||
                    !empty($softdeletethread)
                ) {
                    $this->bb->showStandardThreadTools = true;
                }
                // Only show mod menu if there's any options to show
                if ($this->bb->showStandardThreadTools || !empty($customthreadtools)) {
                    $this->bb->showModMenu = true;
                }
            }

//      // Display 'send thread' link if permissions allow
//      $sendthread = '';
//      if($this->bb->usergroup['cansendemail'] == 1)
//      {
//        ev al("\$sendthread = \"".$this->templates->get("showthread_send_thread")."\";");
//      }

            // Display 'add poll' link to thread creator (or mods) if thread doesn't have a poll already
            $this->bb->showAddPoll = false;
            $time = TIME_NOW;
            if (!$this->bb->threads['poll'] &&
                ($this->bb->threads['uid'] == $this->user->uid || $ismod == true) &&
                $forumpermissions['canpostpolls'] == 1 &&
                $this->bb->forums['open'] != 0 &&
                $this->bb->threads['closed'] != 1 &&
                ($ismod == true || $this->bb->threads['dateline'] > ($time - ($this->bb->settings['polltimelimit'] * 60 * 60)) ||
                    $this->bb->settings['polltimelimit'] == 0)
            ) {
                $this->bb->showAddPoll = true;
            }
            $this->view->offsetSet('thread', $this->bb->threads);

            // Subscription status
            $add_remove_subscription = 'add';
            $add_remove_subscription_text = $this->lang->subscribe_thread;
            if ($this->user->uid) {
//                $query = $this->db->simple_select('threadsubscriptions', 'tid',
//                    "tid='" . (int)$tid . "' AND uid='" . (int)$this->user->uid . "'", ['limit' => 1]);
                $ts = \RunBB\Models\Threadsubscription::where([
                    ['tid', '=', (int)$tid],
                    ['uid', '=', (int)$this->user->uid]
                ])
                    ->take(1)
                    ->get(['tid'])
                    ->toArray();
//                if ($this->db->fetch_field($query, 'tid')) {
                if (isset($ts[0]['tid'])) {
                    $add_remove_subscription = 'remove';
                    $add_remove_subscription_text = $this->lang->unsubscribe_thread;
                }
            }
            $this->view->offsetSet('add_remove_subscription', $add_remove_subscription);
            $this->view->offsetSet('add_remove_subscription_text', $add_remove_subscription_text);

//      $classic_header = '';
//      if($this->bb->settings['postlayout'] == 'classic')
//      {
//        ev al("\$classic_header = \"".$this->templates->get("showthread_classic_header")."\";");
//      }
            // Get users viewing this thread
            $usersbrowsing = [];
            if ($this->bb->settings['browsingthisthread'] != 0) {
                $timecut = TIME_NOW - $this->bb->settings['wolcutoff'];

                $comma = '';
                $guestcount = 0;
                $membercount = 0;
                $inviscount = 0;
                $onlinemembers = '';
                $doneusers = [];

//                $query = $this->db->query('
//			SELECT s.ip, s.uid, s.time, u.username, u.invisible, u.usergroup, u.displaygroup
//			FROM ' . TABLE_PREFIX . 'sessions s
//			LEFT JOIN ' . TABLE_PREFIX . "users u ON (s.uid=u.uid)
//			WHERE s.time > '$timecut' AND location2='$tid' AND nopermission != 1
//			ORDER BY u.username ASC, s.time DESC
//		");
                $u = \RunBB\Models\Session::where('sessions.time', '>', "'$timecut'")
                    ->where('sessions.location2', '=', $tid)
                    ->where('sessions.nopermission', '!=', 1)
                    ->leftJoin('users', function ($join) {
                        $join->on('sessions.uid', '=', 'users.uid');
                    })
                    ->orderBy('users.username', 'asc')
                    ->orderBy('sessions.time', 'desc')
                    ->get(['sessions.ip', 'sessions.uid', 'sessions.time',
                        'users.username', 'users.invisible',
                        'users.usergroup', 'users.displaygroup'
                    ])
                    ->toArray();

//                while ($user = $this->db->fetch_array($query)) {
                foreach ($u as $user) {
                    if ($user['uid'] == 0) {
                        ++$guestcount;
                    } elseif (empty($doneusers[$user['uid']]) || $doneusers[$user['uid']] < $user['time']) {
                        ++$membercount;
                        $doneusers[$user['uid']] = $user['time'];

                        $invisiblemark = '';
                        if ($user['invisible'] == 1) {
                            $invisiblemark = '*';
                            ++$inviscount;
                        }

                        if ($user['invisible'] != 1 || $this->bb->usergroup['canviewwolinvis'] == 1 || $user['uid'] == $this->user->uid) {
                            $user['profilelink'] = get_profile_link($user['uid']);
                            $user['username'] = $this->user->format_name($user['username'], $user['usergroup'], $user['displaygroup']);
                            $user['reading'] = $this->time->formatDate($this->bb->settings['timeformat'], $user['time']);
                            $onlinemembers .= "{$comma}<a href=\"{$user['profilelink']}\" title=\"{$this->lang->users_browsing_thread_reading} ({$user['reading']})\">{$user['username']}</a>{$invisiblemark}";
                            $comma = $this->lang->comma;
                        }
                    }
                }

                $guestsonline = '';
                if ($guestcount) {
                    $guestsonline = $this->lang->sprintf($this->lang->users_browsing_thread_guests, $guestcount);
                }

                $invisonline = '';
                if (isset($this->user->invisible) && $this->user->invisible == 1) {
                    // the user was counted as invisible user --> correct the inviscount
                    $inviscount -= 1;
                }
                if ($inviscount && $this->bb->usergroup['canviewwolinvis'] != 1) {
                    $invisonline = $this->lang->sprintf($this->lang->users_browsing_forum_invis, $inviscount);
                }

                $onlinesep = '';
                if ($invisonline != '' && $onlinemembers) {
                    $onlinesep = $this->lang->comma;
                }

                $onlinesep2 = '';
                if (($invisonline != '' && $guestcount) || ($onlinemembers && $guestcount)) {
                    $onlinesep2 = $this->lang->comma;
                }
                $usersbrowsing = [
                    'onlinemembers' => $onlinemembers,
                    'onlinesep' => $onlinesep,
                    'invisonline' => $invisonline,
                    'onlinesep2' => $onlinesep2,
                    'guestsonline' => $guestsonline
                ];
            }

            $this->view->offsetSet('usersbrowsing', $usersbrowsing);
            $this->view->offsetSet('tid', $tid);
            $this->view->offsetSet('pid', $pid);

            $this->plugins->runHooks('showthread_end');

            $this->bb->output_page();
            $this->view->render($response, '@forum/showthread.html.twig');
        }
    }

    /**
     * Build a navigation tree for threaded display.
     *
     * @param int $replyto
     * @param int $indent
     * @return string
     */
    private function buildtree($tid = 0, $replyto = 0, $indent = 0)
    {
        $indentsize = 13 * $indent;

        ++$indent;
        $posts = '';
        if (is_array($this->tree[$replyto])) {
            foreach ($this->tree[$replyto] as $key => $post) {
                $postdate = $this->time->formatDate('relative', $post['dateline']);
                $post['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($post['subject']));

                if (!$post['subject']) {
                    $post['subject'] = '[' . $this->lang->no_subject . ']';
                }

                $post['profilelink'] = $this->user->build_profile_link($post['username'], $post['uid']);

                if ($this->bb->input['pid'] == $post['pid']) {
                    $posts .= "<div style=\"margin-left: {$indentsize}px;\"><strong>{$post['subject']}</strong> <span class=\"smalltext\">- {$this->lang->by} {$post['profilelink']} - {$postdate}</span></div>";
                } else {
                    $posts .= "<div style=\"margin-left: {$indentsize}px;\"><a href=\"{$this->bb->settings['bburl']}/showthread?tid={$tid}&pid={$post['pid']}&mode=threaded\">{$post['subject']}</a> <span class=\"smalltext\">- {$this->lang->by} {$post['profilelink']} - {$postdate}</span></div>";
                }

                if (isset($this->tree[$post['pid']])) {
                    $posts .= $this->buildtree($tid, $post['pid'], $indent);
                }
            }
            --$indent;
        }
        return $posts;
    }
}
