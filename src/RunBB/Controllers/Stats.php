<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Stats
{
    private $c = null;

    public function __construct($c)
    {
        $this->c = $c;
    }

    public function index(Request $request, Response $response)
    {
        // Load global language phrases
        $this->c['lang']->load('stats');

        $this->c['bb']->add_breadcrumb($this->c['lang']->nav_stats);

        $stats = $this->c['cache']->read('stats');

        if ($stats['numthreads'] < 1 || $stats['numusers'] < 1) {
            $this->c['bb']->error($this->c['lang']->not_enough_info_stats);
        }

        if ($this->c['bb']->settings['statsenabled'] != 1) {
            $this->c['bb']->error($this->c['lang']->stats_disabled);
        }

        $this->c['plugins']->runHooks('stats_start');

        $repliesperthread = $this->c['parser']->formatNumber(
            round((($stats['numposts'] - $stats['numthreads']) / $stats['numthreads']), 2)
        );
        $postspermember = $this->c['parser']->formatNumber(round(($stats['numposts'] / $stats['numusers']), 2));
        $threadspermember = $this->c['parser']->formatNumber(round(($stats['numthreads'] / $stats['numusers']), 2));

        // Get number of days since board start (might need improvement)
//        $query = $this->db->simple_select('users', 'regdate', '', array('order_by' => 'regdate', 'limit' => 1));
//        $result = $this->db->fetch_array($query);
//        $days = (TIME_NOW - $result['regdate']) / 86400;
        $d = \RunBB\Models\User::take(1)
            ->orderBy('regdate')
            ->get(['regdate']);

        $days = (TIME_NOW - $d[0]->regdate) / 86400;
        if ($days < 1) {
            $days = 1;
        }
        // Get 'per day' things
        $postsperday = $this->c['parser']->formatNumber(round(($stats['numposts'] / $days), 2));
        $threadsperday = $this->c['parser']->formatNumber(round(($stats['numthreads'] / $days), 2));
        $membersperday = $this->c['parser']->formatNumber(round(($stats['numusers'] / $days), 2));

        // Get forum permissions
        $unviewableforums = $this->c['forum']->get_unviewable_forums(true);
        $inactiveforums = $this->c['forum']->get_inactive_forums();

        $fidnot = '';
//        if ($unviewableforums) {
//            $fidnot .= ' AND fid NOT IN ('.implode(',', $unviewableforums).')';
//            $unviewablefids = explode(',', $unviewableforums);
//        }
//        if ($inactiveforums) {
//            $fidnot .= ' AND fid NOT IN ('.implode(',', $inactiveforums).')';
//            $inactivefids = explode(',', $inactiveforums);
//        }

//        $unviewableforumsarray = array_merge($unviewablefids, $inactivefids);
        $unviewableforumsarray = array_merge($unviewableforums, $inactiveforums);

        // Most replied-to threads
        $most_replied = $this->c['cache']->read('most_replied_threads');

        if (!$most_replied) {
            $this->c['cache']->update_most_replied_threads();
            $most_replied = $this->c['cache']->read('most_replied_threads', true);
        }

        $mostreplies = [];
        if (!empty($most_replied)) {
            foreach ($most_replied as $key => $thread) {
                if (!in_array($thread['fid'], $unviewableforumsarray)) {
                    $thread['subject'] = htmlspecialchars_uni($this->c['parser']->parse_badwords($thread['subject']));
                    $numberbit = $this->c['parser']->formatNumber($thread['replies']);
                    $thread['threadlink'] = get_thread_link($thread['tid']);
                    $mostreplies[] = [
                        'threadlink' => $thread['threadlink'],
                        'subject' => $thread['subject'],
                        'numberbit' => $this->c['parser']->formatNumber($thread['replies'])
                    ];
                }
            }
        }
        $this->c['view']->offsetSet('mostreplies', $mostreplies);

        // Most viewed threads
        $most_viewed = $this->c['cache']->read('most_viewed_threads');

        if (!$most_viewed) {
            $this->c['cache']->update_most_viewed_threads();
            $most_viewed = $this->c['cache']->read('most_viewed_threads', true);
        }

        $mostviews = [];
        if (!empty($most_viewed)) {
            foreach ($most_viewed as $key => $thread) {
                if (!in_array($thread['fid'], $unviewableforumsarray)) {
                    $thread['subject'] = htmlspecialchars_uni($this->c['parser']->parse_badwords($thread['subject']));
                    $thread['threadlink'] = get_thread_link($thread['tid']);
                    $mostviews[] = [
                        'threadlink' => $thread['threadlink'],
                        'subject' => $thread['subject'],
                        'numberbit' => $this->c['parser']->formatNumber($thread['views'])
                    ];
                }
            }
        }
        $this->c['view']->offsetSet('mostviews', $mostviews);

        $statistics = $this->c['cache']->read('statistics');
        $this->c['bb']->settings['statscachetime'] = (int)$this->c['bb']->settings['statscachetime'];

        if ($this->c['bb']->settings['statscachetime'] < 1) {
            $this->c['bb']->settings['statscachetime'] = 0;
        }

        $interval = $this->c['bb']->settings['statscachetime'] * 3600;

        if (!$statistics || $interval == 0 || TIME_NOW - $interval > $statistics['time']) {
            $this->c['cache']->update_statistics();
            $statistics = $this->c['cache']->read('statistics');
        }

        // Top forum
//        $query = $this->db->simple_select('forums', 'fid, name, threads, posts', "type='f'$fidnot",
//            ['order_by' => 'posts', 'order_dir' => 'DESC', 'limit' => 1]);
//        $forum = $this->db->fetch_array($query);

        $forum = \RunBB\Models\Forum::where('type', '=', 'f')
            ->where(function ($q) use ($unviewableforums, $inactiveforums) {
                if ($unviewableforums) {
                    $q->whereNotIn('fid', $unviewableforums);
                }
                if ($inactiveforums) {
                    $q->whereNotIn('fid', $inactiveforums);
                }
            })
            ->take(1)
            ->orderBy('posts', 'desc')
            ->get(['fid', 'name', 'threads', 'posts'])
            ->toArray();

        if (empty($forum[0]['fid'])) {
            $topforum = $this->c['lang']->none;
            $topforumposts = $this->c['lang']->no;
            $topforumthreads = $this->c['lang']->no;
        } else {
            $forum[0]['name'] = htmlspecialchars_uni(strip_tags($forum[0]['name']));
            $topforum = '<a href="' .$this->c['forum']->get_forum_link($forum[0]['fid']). "\">{$forum[0]['name']}</a>";
            $topforumposts = $forum[0]['posts'];
            $topforumthreads = $forum[0]['threads'];
        }

        // Top referrer defined for the templates even if we don't use it
        $top_referrer = '';
        if ($this->c['bb']->settings['statstopreferrer'] == 1 && isset($statistics['top_referrer']['uid'])) {
            // Only show this if we have anything more the 0 referrals
            if ($statistics['top_referrer']['referrals'] > 0) {
                $toprefuser = $this->c['user']->build_profile_link(
                    $statistics['top_referrer']['username'],
                    $statistics['top_referrer']['uid']
                );
                $top_referrer = $this->c['lang']->sprintf(
                    $this->c['lang']->top_referrer,
                    $toprefuser,
                    $this->c['parser']->formatNumber($statistics['top_referrer']['referrals'])
                );
            }
        }
        $this->c['view']->offsetSet('top_referrer', $top_referrer);

        // Today's top poster
        if (!isset($statistics['top_poster']['uid'])) {
            $topposter = $this->c['lang']->nobody;
            $topposterposts = $this->c['lang']->no_posts;
        } else {
            if (!$statistics['top_poster']['uid']) {
                $topposter = $this->c['lang']->guest;
            } else {
                $topposter = $this->c['user']->build_profile_link(
                    $statistics['top_poster']['username'],
                    $statistics['top_poster']['uid']
                );
            }

            $topposterposts = $statistics['top_poster']['poststoday'];
        }

        // What percent of members have posted?
        $posters = $statistics['posters'];
        //$havepostedpercent = $this->parser->formatNumber(round((($posters / $stats['numusers']) * 100), 2)) . '%';

        $this->c['lang']->todays_top_poster = $this->c['lang']->sprintf(
            $this->c['lang']->todays_top_poster,
            $topposter,
            $this->c['parser']->formatNumber($topposterposts)
        );
        $this->c['lang']->popular_forum = $this->c['lang']->sprintf(
            $this->c['lang']->popular_forum,
            $topforum,
            $this->c['parser']->formatNumber($topforumposts),
            $this->c['parser']->formatNumber($topforumthreads)
        );

        $stats['havepostedpercent'] = $this->c['parser']->formatNumber(
            round((($posters / $stats['numusers']) * 100), 2)
        ) . '%';

        $stats['numposts'] = $this->c['parser']->formatNumber($stats['numposts']);
        $stats['numthreads'] = $this->c['parser']->formatNumber($stats['numthreads']);
        $stats['numusers'] = $this->c['parser']->formatNumber($stats['numusers']);
        $stats['newest_user'] = $this->c['user']->build_profile_link($stats['lastusername'], $stats['lastuid']);

        $stats['repliesperthread'] = $repliesperthread;
        $stats['postspermember'] = $postspermember;
        $stats['threadspermember'] = $threadspermember;

        $stats['postsperday'] = $postsperday;
        $stats['threadsperday'] = $threadsperday;
        $stats['membersperday'] = $membersperday;

        $this->c['view']->offsetSet('stats', $stats);

        $this->c['plugins']->runHooks('stats_end');

        $this->c['bb']->output_page();
        $this->c['view']->render($response, '@forum/stats.html.twig');
    }
}
