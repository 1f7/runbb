<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Syndication extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('IGNORE_CLEAN_VARS', 'fid');
        define('NO_ONLINE', 1);

        // Load global language phrases
        $this->lang->load('syndication');

        // Load syndication class.
        $feedgenerator = new \RunBB\Core\FeedGenerator($this->lang);

        // Find out the thread limit.
        if ($this->bb->getInput('portal', '') && $this->bb->settings['portal'] != 0) {
            $thread_limit = $this->bb->settings['portal_numannouncements'];
        } else {
            $thread_limit = $this->bb->getInput('limit', 0);
        }

        if ($thread_limit > 50) {
            $thread_limit = 50;
        } elseif (!$thread_limit || $thread_limit < 0) {
            $thread_limit = 20;
        }

        // Syndicate a specific forum or all viewable?
        if ($this->bb->getInput('portal', '') && $this->bb->settings['portal'] != 0) {
            if ($this->bb->settings['portal_announcementsfid'] != '-1') {
                $forumlist = explode(',', $this->bb->settings['portal_announcementsfid']);
            }
        } elseif ($this->bb->getInput('fid', '')) {
            $forumlist = explode(',', $this->bb->getInput('fid', ''));
        }

        // Get the forums the user is not allowed to see.
        $unviewableforums = $this->forum->get_unviewable_forums(true);
        $inactiveforums = $this->forum->get_inactive_forums();

        $unviewable = '';

        $this->plugins->runHooks('syndication_start');

        // If there are any, add SQL to exclude them.
        if ($unviewableforums) {
            $unviewable .= ' AND fid NOT IN(' . implode(',', $unviewableforums) . ')';
        }

        if ($inactiveforums) {
            $unviewable .= ' AND fid NOT IN(' . implode(',', $inactiveforums) . ')';
        }

        // If there are no forums to syndicate, syndicate all viewable.
        if (!empty($forumlist)) {
            $forum_ids = "'-1'";
            foreach ($forumlist as $fid) {
                $forum_ids .= ",'" . (int)$fid . "'";
            }
            $forumlist = "AND fid IN ($forum_ids) $unviewable";
        } else {
            $forumlist = $unviewable;
            $all_forums = 1;
        }

        // Find out which title to add to the feed.
        $title = $this->bb->settings['bbname'];
        $query = $this->db->simple_select('forums', 'name, fid, allowhtml, allowmycode, allowsmilies, allowimgcode, allowvideocode', '1=1 ' . $forumlist);
        $comma = ' - ';
        while ($forum = $this->db->fetch_array($query)) {
            if (!$this->bb->getInput('portal', '') || $this->bb->settings['portal'] == 0) {
                $title .= $comma . $forum['name'];
                $comma = $this->lang->comma;
            }
            $forumcache[$forum['fid']] = $forum;
        }

        if ($this->bb->getInput('portal', '') && $this->bb->settings['portal'] != 0) {
            $title .= $comma . $this->lang->portal;
        }

        // If syndicating all forums then cut the title back to 'All Forums'
        if (isset($all_forums)) {
            if ($this->bb->getInput('portal', '') && $this->bb->settings['portal'] != 0) {
                $title = $this->bb->settings['bbname'] . ' - ' . $this->lang->portal;
            } else {
                $title = $this->bb->settings['bbname'] . ' - ' . $this->lang->all_forums;
            }
        }

        // Set the feed type.
        $feedgenerator->set_feed_format($this->bb->getInput('type', ''));

        // Set the channel header.
        $channel = [
            'title' => $title,
            'link' => $this->bb->settings['bburl'] . '/',
            'date' => TIME_NOW,
            'description' => $this->bb->settings['bbname'] . ' - ' . $this->bb->settings['bburl']
        ];
        $feedgenerator->set_channel($channel);

        $permsql = '';
        $onlyusfids = [];

        // Check group permissions if we can't view threads not started by us
        $group_permissions = $this->forum->forum_permissions();
        foreach ($group_permissions as $fid => $forum_permissions) {
            if (isset($forum_permissions['canonlyviewownthreads']) && $forum_permissions['canonlyviewownthreads'] == 1) {
                $onlyusfids[] = $fid;
            }
        }
        if (!empty($onlyusfids)) {
            $permsql .= 'AND ((fid IN(' . implode(',', $onlyusfids) . ") AND uid='{$this->user->uid}') OR fid NOT IN(" . implode(',', $onlyusfids) . '))';
        }

        // Get the threads to syndicate.
        $query = $this->db->simple_select('threads', 'subject, tid, dateline, firstpost', "visible='1' AND closed NOT LIKE 'moved|%' {$permsql} {$forumlist}", ['order_by' => 'dateline', 'order_dir' => 'DESC', 'limit' => $thread_limit]);
        // Loop through all the threads.
        while ($thread = $this->db->fetch_array($query)) {
            $items[$thread['tid']] = [
                'title' => $this->parser->parse_badwords($thread['subject']),
//        'link' => $channel['link'].get_thread_link($thread['tid']),
                'link' => get_thread_link($thread['tid']),
                'date' => $thread['dateline'],
            ];

            $firstposts[] = $thread['firstpost'];
        }

        $this->plugins->runHooks('syndication_get_posts');

        if (!empty($firstposts)) {
            $firstpostlist = 'pid IN(' . $this->db->escape_string(implode(',', $firstposts)) . ')';

            if ($this->bb->settings['enableattachments'] == 1) {
                $attachments = [];
                $query = $this->db->simple_select('attachments', '*', $firstpostlist);
                while ($attachment = $this->db->fetch_array($query)) {
                    if (!isset($attachments[$attachment['pid']])) {
                        $attachments[$attachment['pid']] = [];
                    }
                    $attachments[$attachment['pid']][] = $attachment;
                }
            }

            $query = $this->db->simple_select(
                'posts',
                'message, edittime, tid, fid, pid',
                $firstpostlist,
                ['order_by' => 'dateline', 'order_dir' => 'desc']
            );

            while ($post = $this->db->fetch_array($query)) {
                $parser_options = [
                    'allow_html' => $forumcache[$post['fid']]['allowhtml'],
                    'allow_mycode' => $forumcache[$post['fid']]['allowmycode'],
                    'allow_smilies' => $forumcache[$post['fid']]['allowsmilies'],
                    'allow_imgcode' => $forumcache[$post['fid']]['allowimgcode'],
                    'allow_videocode' => $forumcache[$post['fid']]['allowvideocode'],
                    'filter_badwords' => 1,
                    'filter_cdata' => 1
                ];

                $parsed_message = $this->parser->parse_message($post['message'], $parser_options);

                if ($this->bb->settings['enableattachments'] == 1 && isset($attachments[$post['pid']]) && is_array($attachments[$post['pid']])) {
                    foreach ($attachments[$post['pid']] as $attachment) {
                        $ext = get_extension($attachment['filename']);
                        $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                        $attachment['filesize'] = $this->parser->friendlySize($attachment['filesize']);
                        $attachment['icon'] = $this->upload->get_attachment_icon($ext);
                        $attachdate = $this->time->formatDate('relative', $attachment['dateuploaded']);
                        $attbit = "<br />{$attachment['icon']}&nbsp;&nbsp;<a href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" target=\"_blank\" title=\"{$attachdate}\">{$attachment['filename']}</a> ({$this->lang->postbit_attachment_size} {$attachment['filesize']} / {$this->lang->postbit_attachment_downloads} {$attachment['downloads']})";
                        //ev al("\$attbit = \"".$this->templates->get('postbit_attachments_attachment')."\";");
                        if (stripos($parsed_message, '[attachment=' . $attachment['aid'] . ']') !== false) {
                            $parsed_message = preg_replace('#\[attachment=' . $attachment['aid'] . ']#si', $attbit, $parsed_message);
                        } else {
                            $parsed_message .= '<br />' . $attbit;
                        }
                    }
                }

                $items[$post['tid']]['description'] = $parsed_message;
                $items[$post['tid']]['updated'] = $post['edittime'];
                $feedgenerator->add_item($items[$post['tid']]);
            }
        }

        // Then output the feed XML.
        $feedgenerator->output_feed();
    }
}
