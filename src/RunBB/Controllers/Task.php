<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Task extends AbstractController
{
    public function index(Request $request, Response $response)
    {
        ignore_user_abort(true);
        @set_time_limit(0);

        define('NO_ONLINE', 1);
        define('IN_TASK', 1);

        // Load language
        $this->lang->set_language($this->bb->settings['bblanguage']);
        $this->lang->load('global');
        $this->lang->load('messages');

        $this->task = new \RunBB\Core\Task($this);

        if (function_exists('mb_internal_encoding') && !empty($this->lang->settings['charset'])) {
            @mb_internal_encoding($this->lang->settings['charset']);
        }

        // Are tasks set to run via cron instead & are we accessing this file via the CLI?
        // php task.php [tid]
        if (PHP_SAPI == 'cli') {
            // Passing a specific task ID
            if ($_SERVER['argc'] == 2) {
                $query = $this->db->simple_select('tasks', 'tid', "tid='" . (int)$_SERVER['argv'][1] . "'");
                $tid = $this->db->fetch_field($query, 'tid');
            }

            if ($tid) {
                $this->task->run_task($tid);
            } else {
                $this->task->run_task();
            }
        } // Otherwise false GIF image, only supports running next available task
        else {
            // Send our fake gif image (clear 1x1 transparent image)
            header('Content-type: image/gif');
            header('Expires: Sat, 1 Jan 2000 01:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            echo base64_decode('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');

            // If the use shutdown functionality is turned off, run any shutdown related items now.
            if ($this->bb->use_shutdown == true) {
                $this->bb->add_shutdown('run_task');
            } else {
                $this->task->run_task();
            }
        }
    }
}
