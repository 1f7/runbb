<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Acceptrequest extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        // Validate request
        $query = $this->db->simple_select(
            'buddyrequests',
            '*',
            'id='.$this->bb->getInput('id', 0).' AND touid='.(int)$this->user->uid
        );
        $request = $this->db->fetch_array($query);
        if (empty($request)) {
            $this->bb->error($this->lang->invalid_request);
        }

        $this->plugins->runHooks('usercp_acceptrequest_start');

        $user = $this->user->get_user($request['uid']);
        if (!empty($user)) {
            // We want to add us to this user's buddy list
            if ($user['buddylist'] != '') {
                $user['buddylist'] = explode(',', $user['buddylist']);
            } else {
                $user['buddylist'] = [];
            }

            $user['buddylist'][] = (int)$this->user->uid;

            // Now we have the new list, so throw it all back together
            $new_list = implode(',', $user['buddylist']);

            // And clean it up a little to ensure there is no possibility of bad values
            $new_list = preg_replace('#,{2,}#', ',', $new_list);
            $new_list = preg_replace('#[^0-9,]#', '', $new_list);

            if (my_substr($new_list, 0, 1) == ',') {
                $new_list = my_substr($new_list, 1);
            }
            if (my_substr($new_list, -1) == ',') {
                $new_list = my_substr($new_list, 0, my_strlen($new_list)-2);
            }

            $user['buddylist'] = $this->db->escape_string($new_list);

            $this->db->update_query('users', ['buddylist' => $user['buddylist']], "uid='".(int)$user['uid']."'");


            // We want to add the user to our buddy list
            if ($this->user->buddylist != '') {
                $this->user->buddylist = explode(',', $this->user->buddylist);
            } else {
                $this->user->buddylist = [];
            }

            $this->user->buddylist[] = (int)$request['uid'];

            // Now we have the new list, so throw it all back together
            $new_list = implode(',', $this->user->buddylist);

            // And clean it up a little to ensure there is no possibility of bad values
            $new_list = preg_replace('#,{2,}#', ',', $new_list);
            $new_list = preg_replace('#[^0-9,]#', '', $new_list);

            if (my_substr($new_list, 0, 1) == ',') {
                $new_list = my_substr($new_list, 1);
            }
            if (my_substr($new_list, -1) == ',') {
                $new_list = my_substr($new_list, 0, my_strlen($new_list)-2);
            }

            $this->user->buddylist = $this->db->escape_string($new_list);

            $this->db->update_query(
                'users',
                ['buddylist' => $this->user->buddylist],
                "uid='".(int)$this->user->uid."'"
            );

            $pm = [
            'subject' => 'buddyrequest_accepted_request',
            'message' => 'buddyrequest_accepted_request_message',
            'touid' => $user['uid'],
            'language' => $user['language'],
            'language_file' => 'usercp'
            ];

            $this->pm->send_pm($pm, $this->user->uid, true);

            $this->db->delete_query('buddyrequests', 'id='.(int)$request['id']);
        } else {
            $this->bb->error($this->lang->user_doesnt_exist);
        }

        $this->plugins->runHooks('usercp_acceptrequest_end');

        $this->bb->redirect($this->bb->settings['bburl'].'/usercp/editlists', $this->lang->buddyrequest_accepted);
    }
}
