<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class AddSubscription extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $server_http_referer = htmlentities($_SERVER['HTTP_REFERER']);

        if ($this->bb->getInput('type', '') === 'forum') {
            $forum = $this->forum->get_forum($this->bb->getInput('fid', 0));
            if (!$forum) {
                $this->bb->error($this->lang->error_invalidforum);
            }
            $forumpermissions = $this->forum->forum_permissions($forum['fid']);
            if ($forumpermissions['canview'] == 0 || $forumpermissions['canviewthreads'] == 0) {
                return $this->bb->error_no_permission();
            }

            $this->plugins->runHooks('usercp2_addsubscription_forum');

            $this->user->add_subscribed_forum($forum['fid']);
            if ($server_http_referer) {
                $url = $server_http_referer;
            } else {
                $url = $this->bb->settings['bburl'];
            }
            return $this->bb->redirect($url, $this->lang->redirect_forumsubscriptionadded);
        } else {
            $thread  = $this->thread->get_thread($this->bb->getInput('tid', 0));
            if (!$thread) {
                $this->bb->error($this->lang->error_invalidthread);
            }

            // Is the currently logged in user a moderator of this forum?
            $ismod = $this->user->is_moderator($thread['fid']);

            // Make sure we are looking at a real thread here.
            if (($thread['visible'] != 1 && $ismod == false) || ($thread['visible'] > 1 && $ismod == true)) {
                $this->bb->error($this->lang->error_invalidthread);
            }

            $this->bb->add_breadcrumb(
                $this->lang->nav_subthreads,
                $this->bb->settings['bburl'].'/usercp/subscriptions'
            );
            $this->bb->add_breadcrumb($this->lang->nav_addsubscription);

            $forumpermissions = $this->forum->forum_permissions($thread['fid']);
            if ($forumpermissions['canview'] == 0 ||
            $forumpermissions['canviewthreads'] == 0 ||
            (isset($forumpermissions['canonlyviewownthreads']) &&
              $forumpermissions['canonlyviewownthreads'] != 0 &&
              $thread['uid'] != $this->user->uid)) {
                return $this->bb->error_no_permission();
            }
            $referrer = '';
            if ($server_http_referer) {
                $referrer = $server_http_referer;
            }

            $thread['subject'] = $this->parser->parse_badwords($thread['subject']);
            $thread['subject'] = htmlspecialchars_uni($thread['subject']);
            $this->lang->subscribe_to_thread = $this->lang->sprintf(
                $this->lang->subscribe_to_thread,
                $thread['subject']
            );

            $notification_none_checked = $notification_email_checked = $notification_pm_checked = '';
            if ($this->user->subscriptionmethod == 1 || $this->user->subscriptionmethod == 0) {
                $notification_none_checked = 'checked="checked"';
            } elseif ($this->user->subscriptionmethod == 2) {
                $notification_email_checked = 'checked="checked"';
            } elseif ($this->user->subscriptionmethod == 3) {
                $notification_pm_checked = 'checked="checked"';
            }
            $this->view->offsetSet('notification', [
            'none' => $notification_none_checked,
            'email' => $notification_email_checked,
            'pm' => $notification_pm_checked
            ]);
            $this->view->offsetSet('tid', $thread['tid']);

            $this->plugins->runHooks('usercp2_addsubscription_thread');

            //ev al('\$add_subscription = \''.$templates->get('usercp_addsubscription_thread').'\';');
            $this->bb->output_page();
            $this->view->render($response, '@forum/UserCP/addsubscription.html.twig');
        }
    }

    public function doAddsubscription(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));
        if (!$thread) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        // Is the currently logged in user a moderator of this forum?
        $ismod = $this->user->is_moderator($thread['fid']);

        // Make sure we are looking at a real thread here.
        if (($thread['visible'] != 1 && $ismod == false) || ($thread['visible'] > 1 && $ismod == true)) {
            $this->bb->error($this->lang->error_invalidthread);
        }

        $forumpermissions = $this->forum->forum_permissions($thread['fid']);
        if ($forumpermissions['canview'] == 0 ||
        $forumpermissions['canviewthreads'] == 0 ||
        (isset($forumpermissions['canonlyviewownthreads']) &&
        $forumpermissions['canonlyviewownthreads'] != 0 &&
        $thread['uid'] != $this->user->uid)) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('usercp2_do_addsubscription');

        $this->user->add_subscribed_thread($thread['tid'], $this->bb->getInput('notification', 0));

        if ($this->bb->getInput('referrer', '')) {
            $url = htmlspecialchars_uni($this->bb->getInput('referrer', ''));
        } else {
            $url = get_thread_link($thread['tid']);
        }
        $this->bb->redirect($url, $this->lang->redirect_subscriptionadded);
    }
}
