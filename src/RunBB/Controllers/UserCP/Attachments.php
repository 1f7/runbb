<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Attachments extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->ucp_nav_attachments);
        $this->plugins->runHooks('usercp_attachments_start');

        if ($this->bb->settings['enableattachments'] == 0) {
            $this->bb->error($this->lang->attachments_disabled);
        }

        $attachments = [];
        // Pagination
        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        $perpage = $this->bb->settings['threadsperpage'];
        $page = $this->bb->getInput('page', 0);

        if ($page > 0) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $end = $start + $perpage;
        $lower = $start + 1;

        $query = $this->db->query('
		SELECT a.*, p.subject, p.dateline, t.tid, t.subject AS threadsubject
		FROM ' . TABLE_PREFIX . 'attachments a
		LEFT JOIN ' . TABLE_PREFIX . 'posts p ON (a.pid=p.pid)
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=p.tid)
		WHERE a.uid='" . $this->user->uid . "'
		ORDER BY p.dateline DESC LIMIT {$start}, {$perpage}
	");

        $bandwidth = $totaldownloads = 0;
        while ($attachment = $this->db->fetch_array($query)) {
            if ($attachment['dateline'] && $attachment['tid']) {
                $attachment['subject'] = htmlspecialchars_uni($this->parser->parse_badwords($attachment['subject']));
                $attachment['postlink'] = get_post_link($attachment['pid'], $attachment['tid']);
                $attachment['threadlink'] = get_thread_link($attachment['tid']);
                $attachment['threadsubject'] =
                    htmlspecialchars_uni($this->parser->parse_badwords($attachment['threadsubject']));

                $attachments[] = [
                    'altbg' => alt_trow(),
                    'icon' => $this->upload->get_attachment_icon(get_extension($attachment['filename'])),
                    'aid' => $attachment['aid'],
                    'filename' => htmlspecialchars_uni($attachment['filename']),
                    'sizedownloads' => $this->lang->sprintf(
                        $this->lang->attachment_size_downloads,
                        $this->parser->friendlySize($attachment['filesize']),
                        $attachment['downloads']
                    ),
                    'attachdate' => $this->time->formatDate('relative', $attachment['dateline']),
                    'postlink' => $attachment['postlink'],
                    'pid' => $attachment['pid'],
                    'subject' => $attachment['subject'],
                    'threadlink' => $attachment['threadlink'],
                    'threadsubject' => $attachment['threadsubject']
                ];

                // Add to bandwidth total
                $bandwidth += ($attachment['filesize'] * $attachment['downloads']);
                $totaldownloads += $attachment['downloads'];
            } else {
                // This little thing delets attachments without a thread/post
                $this->upload->remove_attachment($attachment['pid'], $attachment['posthash'], $attachment['aid']);
            }
        }

        $query = $this->db->simple_select(
            'attachments',
            "SUM(filesize) AS ausage, COUNT(aid) AS acount",
            "uid='" . $this->user->uid . "'"
        );
        $usage = $this->db->fetch_array($query);
        $totalusage = $usage['ausage'];
        $totalattachments = $usage['acount'];
        $friendlyusage = $this->parser->friendlySize($totalusage);
        if ($this->bb->usergroup['attachquota']) {
            $percent = round(($totalusage / ($this->bb->usergroup['attachquota'] * 1024)) * 100) . '%';
            $attachquota = $this->parser->friendlySize($this->bb->usergroup['attachquota'] * 1024);
            $usagenote = $this->lang->sprintf(
                $this->lang->attachments_usage_quota,
                $friendlyusage,
                $attachquota,
                $percent,
                $totalattachments
            );
        } else {
            $percent = $this->lang->unlimited;
            $attachquota = $this->lang->unlimited;
            $usagenote = $this->lang->sprintf($this->lang->attachments_usage, $friendlyusage, $totalattachments);
        }

        $multipage = $this->pagination->multipage(
            $totalattachments,
            $perpage,
            $page,
            $this->bb->settings['bburl'] . '/usercp/attachments'
        );

        if (!$attachments) {
            $usagenote = '';
        }
        $data = [
            'bandwidth' => $this->parser->friendlySize($bandwidth),
            'totaldownloads' => $totaldownloads,
            'attachquota' => $attachquota,
            'percent' => $percent,
            'friendlyusage' => $friendlyusage,
            'totalattachments' => $totalattachments,
            'multipage' => $multipage,
            'usagenote' => $usagenote,
            'attachments' => $attachments
        ];

        $this->plugins->runHooks('usercp_attachments_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/attachments.html.twig', $data);
    }

    public function doAttachments(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_attachments_start');

        if (!isset($this->bb->input['attachments']) || !is_array($this->bb->input['attachments'])) {
            $this->bb->error($this->lang->no_attachments_selected);
        }
        $aids = implode(',', array_map('intval', $this->bb->input['attachments']));
        $query = $this->db->simple_select('attachments', '*', "aid IN ($aids) AND uid='" . $this->user->uid . "'");
        while ($attachment = $this->db->fetch_array($query)) {
            $this->upload->remove_attachment($attachment['pid'], '', $attachment['aid']);
        }
        $this->plugins->runHooks('usercp_do_attachments_end');
        $this->bb->redirect('usercp.php?action=attachments', $this->lang->attachments_deleted);
    }
}
