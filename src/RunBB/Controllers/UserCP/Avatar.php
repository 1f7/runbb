<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Avatar extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        $this->bb->add_breadcrumb($this->lang->nav_avatar);
        $this->plugins->runHooks('usercp_avatar_start');

        $avatarurl = '';

        if ($this->user->avatartype == 'upload' ||
            stristr($this->user->avatar, $this->bb->settings['avataruploadpath'])) {
            $this->view->offsetSet('avatarmsg', $this->lang->already_uploaded_avatar);
        } elseif ($this->user->avatartype == 'remote' ||
            my_strpos(my_strtolower($this->user->avatar), 'http://') !== false) {
            $this->view->offsetSet('avatarmsg', $this->lang->using_remote_avatar);
            $avatarurl = htmlspecialchars_uni($this->user->avatar);
        }
        $this->view->offsetSet('avatarurl', $avatarurl);

        $useravatar = $this->user->format_avatar($this->user->avatar, $this->user->avatardimensions, '100x100');

        $this->view->offsetSet('useravatar', $useravatar);

        if ($this->bb->settings['maxavatardims'] != '') {
            list($maxwidth, $maxheight) = explode('x', my_strtolower($this->bb->settings['maxavatardims']));
            $this->lang->avatar_note .= '<br />' . $this->lang->sprintf(
                $this->lang->avatar_note_dimensions,
                $maxwidth,
                $maxheight
            );
        }

        if ($this->bb->settings['avatarsize']) {
            $maxsize = $this->parser->friendlySize($this->bb->settings['avatarsize'] * 1024);
            $this->lang->avatar_note .= '<br />' . $this->lang->sprintf($this->lang->avatar_note_size, $maxsize);
        }

        $this->plugins->runHooks('usercp_avatar_end');

        $this->view->offsetSet('errors', $this->errors);

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/avatar.html.twig');
    }

    public function doAvatar(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_avatar_start');

        $this->errors = '';

        if (!empty($this->bb->input['remove'])) { // remove avatar
            $updated_avatar = [
                'avatar' => '',
                'avatardimensions' => '',
                'avatartype' => ''
            ];
            $this->db->update_query('users', $updated_avatar, "uid='" . $this->user->uid . "'");
            $this->upload->remove_avatars($this->user->uid);
        } elseif ($_FILES['avatarupload']['name']) { // upload avatar
            if ($this->bb->usergroup['canuploadavatars'] == 0) {
                return $this->bb->error_no_permission();
            }
            $avatar = $this->upload->upload_avatar();
            if ($avatar['error']) {
                $this->errors = $avatar['error'];
            } else {
                if ($avatar['width'] > 0 && $avatar['height'] > 0) {
                    $avatar_dimensions = $avatar['width'] . '|' . $avatar['height'];
                }
                $updated_avatar = [
                    'avatar' => $avatar['avatar'] . '?dateline=' . TIME_NOW,
                    'avatardimensions' => $avatar_dimensions,
                    'avatartype' => 'upload'
                ];
                $this->db->update_query('users', $updated_avatar, "uid='" . $this->user->uid . "'");
            }
        } else // remote avatar
        {
            $this->bb->input['avatarurl'] = trim($this->bb->getInput('avatarurl', ''));
            if (validate_email_format($this->bb->input['avatarurl']) != false) {
                // Gravatar
                $this->bb->input['avatarurl'] = my_strtolower($this->bb->input['avatarurl']);

                // If user image does not exist, or is a higher rating, use the mystery man
                $email = md5($this->bb->input['avatarurl']);

                $s = '';
                if (!$this->bb->settings['maxavatardims']) {
                    $this->bb->settings['maxavatardims'] = '100x100'; // Hard limit of 100 if there are no limits
                }

                // Because Gravatars are square, hijack the width
                list($maxwidth, $maxheight) = explode('x', my_strtolower($this->bb->settings['maxavatardims']));
                $maxheight = (int)$maxwidth;

                // Rating?
                $types = ['g', 'pg', 'r', 'x'];
                $rating = $this->bb->settings['useravatarrating'];

                if (!in_array($rating, $types)) {
                    $rating = 'g';
                }

                $s = "?s={$maxheight}&r={$rating}&d=mm";

                $updated_avatar = [
                    'avatar' => "https://www.gravatar.com/avatar/{$email}{$s}",
                    'avatardimensions' => "{$maxheight}|{$maxheight}",
                    'avatartype' => 'gravatar'
                ];

                $this->db->update_query('users', $updated_avatar, "uid = '{$this->user->uid}'");
            } else {
                $this->bb->input['avatarurl'] = preg_replace('#script:#i', '', $this->bb->getInput('avatarurl', ''));
                $ext = get_extension($this->bb->input['avatarurl']);

                // Copy the avatar to the local server (work around remote URL access disabled for getimagesize)
                $file = fetch_remote_file($this->bb->input['avatarurl']);
                if (!$file) {
                    $this->errors = $this->lang->error_invalidavatarurl;
                } else {
                    $tmp_name = $this->bb->settings['avataruploadpath'] . '/remote_' . md5(random_str());
                    $fp = @fopen($tmp_name, 'wb');
                    if (!$fp) {
                        $this->errors = $this->lang->error_invalidavatarurl;
                    } else {
                        fwrite($fp, $file);
                        fclose($fp);
                        list($width, $height, $type) = @getimagesize($tmp_name);
                        @unlink($tmp_name);
                        if (!$type) {
                            $this->errors = $this->lang->error_invalidavatarurl;
                        }
                    }
                }

                if (empty($this->errors)) {
                    if ($width && $height && $this->bb->settings['maxavatardims'] != '') {
                        list($maxwidth, $maxheight) = explode('x', my_strtolower($this->bb->settings['maxavatardims']));
                        if (($maxwidth && $width > $maxwidth) || ($maxheight && $height > $maxheight)) {
                            $this->lang->error_avatartoobig = $this->lang->sprintf(
                                $this->lang->error_avatartoobig,
                                $maxwidth,
                                $maxheight
                            );
                            $this->errors = $this->lang->error_avatartoobig;
                        }
                    }
                }

                if (empty($this->errors)) {
                    if ($width > 0 && $height > 0) {
                        $avatar_dimensions = (int)$width . '|' . (int)$height;
                    }
                    $updated_avatar = [
                        'avatar' => $this->db->escape_string($this->bb->input['avatarurl'] . '?dateline=' . TIME_NOW),
                        'avatardimensions' => $avatar_dimensions,
                        'avatartype' => 'remote'
                    ];
                    $this->db->update_query('users', $updated_avatar, "uid='" . $this->user->uid . "'");
                    $this->upload->remove_avatars($this->user->uid);
                }
            }
        }

        if (empty($this->errors)) {
            $this->plugins->runHooks('usercp_do_avatar_end');
            $this->bb->redirect($this->bb->settings['bburl'] . '/usercp/avatar', $this->lang->redirect_avatarupdated);
        } else {
            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        }
    }
}
