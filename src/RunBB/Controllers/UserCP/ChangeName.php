<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ChangeName extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }
        $this->bb->add_breadcrumb($this->lang->nav_changename);

        $this->plugins->runHooks('usercp_changename_start');
        if ($this->bb->usergroup['canchangename'] != 1) {
            return $this->bb->error_no_permission();
        }

        $this->view->offsetSet('errors', $this->errors);
        $this->plugins->runHooks('usercp_changename_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/changename.html.twig');
    }

    public function doChangename(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_changename_start');
        if ($this->bb->usergroup['canchangename'] != 1) {
            return $this->bb->error_no_permission();
        }

        if ($this->user->validate_password_from_uid(
            $this->user->uid,
            $this->bb->getInput('password', '', true)
        ) == false) {
            $this->errors[] = $this->lang->error_invalidpassword;
        } else {
            // Set up user handler.
            $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

            $user = [
                'uid' => $this->user->uid,
                'username' => $this->bb->getInput('username', '', true)
            ];

            $userhandler->set_data($user);

            if (!$userhandler->validate_user()) {
                $this->errors = $userhandler->get_friendly_errors();
            } else {
                $userhandler->update_user();
                $this->plugins->runHooks('usercp_do_changename_end');
                $this->bb->redirect(
                    $this->bb->settings['bburl'] . '/usercp/changename',
                    $this->lang->redirect_namechanged
                );
            }
        }
        if (count($this->errors) > 0) {
            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        }
    }
}
