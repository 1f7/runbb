<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Declinerequest extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        // Validate request
        $query = $this->db->simple_select(
            'buddyrequests',
            '*',
            'id='.$this->bb->getInput('id', 0).' AND touid='.(int)$this->user->uid
        );
        $request = $this->db->fetch_array($query);
        if (empty($request)) {
            $this->bb->error($this->lang->invalid_request);
        }

        $this->plugins->runHooks('usercp_declinerequest_start');

        $user = $this->user->get_user($request['uid']);
        if (!empty($user)) {
            $this->db->delete_query('buddyrequests', 'id='.(int)$request['id']);
        } else {
            $this->bb->error($this->lang->user_doesnt_exist);
        }

        $this->plugins->runHooks('usercp_declinerequest_end');

        $this->bb->redirect($this->bb->settings['bburl'].'/usercp/editlists', $this->lang->buddyrequest_declined);
    }
}
