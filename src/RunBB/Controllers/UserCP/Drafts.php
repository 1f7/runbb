<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;
use RunBB\Models\Post;

class Drafts extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->ucp_nav_drafts);
        $this->plugins->runHooks('usercp_drafts_start');

//        $query = $this->db->simple_select('posts', 'COUNT(pid) AS draftcount',
//"visible='-2' AND uid='{$this->user->uid}'");
//        $draftcount = $this->db->fetch_field($query, 'draftcount');
        $draftcount = \RunBB\Models\Post::draftCount($this->user->uid);

        $drafts = [];
        $disable_delete_drafts = '';
        $this->lang->drafts_count = $this->lang->sprintf(
            $this->lang->drafts_count,
            $this->parser->formatNumber($draftcount)
        );

        // Show a listing of all of the current 'draft' posts or threads the user has.
        if ($draftcount) {
//            $query = $this->db->query('
//			SELECT p.subject, p.pid, t.tid,
//			    t.subject AS threadsubject, t.fid,
//			    f.name AS forumname, p.dateline,
//			    t.visible AS threadvisible,
//			    p.visible AS postvisible
//			FROM ' . TABLE_PREFIX . 'posts p
//			LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
//			LEFT JOIN ' . TABLE_PREFIX . "forums f ON (f.fid=t.fid)
//			WHERE p.uid = '{$this->user->uid}' AND p.visible = '-2'
//			ORDER BY p.dateline DESC
//		");

            $d = \RunBB\Models\Post::getAllUserDarfts($this->user->uid);

//            while ($draft = $this->db->fetch_array($query)) {
            foreach ($d as $draft) {
                $detail = '';
                if ($draft['threadvisible'] == 1) { // We're looking at a draft post
                    $draft['threadlink'] = get_thread_link($draft['tid']);
                    $draft['threadsubject'] = htmlspecialchars_uni($draft['threadsubject']);
                    $detail = "{$this->lang->thread}: <a href=\"{$draft['threadlink']}\">{$draft['threadsubject']}</a>";
                    $editurl = $this->bb->settings['bburl'] . "/newreply?action=editdraft&pid={$draft['pid']}";
                    $id = $draft['pid'];
                    $type = 'post';
                } elseif ($draft['threadvisible'] == -2) { // We're looking at a draft thread
                    $draft['forumlink'] = $this->forum->get_forum_link($draft['fid']);
                    $draft['forumname'] = htmlspecialchars_uni($draft['forumname']);
                    $detail = "{$this->lang->forum}: <a href=\"{$draft['forumlink']}\">{$draft['forumname']}</a>";
                    //$editurl = $this->bb->settings['bburl']."/newthread?action=editdraft&tid={$draft['tid']}";
                    $editurl = $this->bb->settings['bburl'] . "/editdraft?tid={$draft['tid']}";
                    $id = $draft['tid'];
                    $type = 'thread';
                }

                $drafts[] = [
                    'trow' => alt_trow(),
                    'subject' => htmlspecialchars_uni($draft['subject']),
                    'detail' => $detail,
                    'savedate' => $this->time->formatDate('relative', $draft['dateline']),
                    'editurl' => $editurl,
                    'id' => $id,
                    'type' => $type
                ];
            }
        } else {
            $disable_delete_drafts = 'disabled="disabled"';
        }
        $this->view->offsetSet('drafts', $drafts);
        $this->view->offsetSet('disable_delete_drafts', $disable_delete_drafts);

        $this->plugins->runHooks('usercp_drafts_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/drafts.html.twig');
    }

    public function doDrafts(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_drafts_start');
        $this->bb->input['deletedraft'] = $this->bb->getInput('deletedraft', []);
        if (empty($this->bb->input['deletedraft'])) {
            $this->bb->error($this->lang->no_drafts_selected);
        }
        $pidin = [];
        $tidin = [];
        foreach ($this->bb->input['deletedraft'] as $id => $val) {
            if ($val == 'post') {
                $pidin[] = "'" . (int)$id . "'";
            } elseif ($val == 'thread') {
                $tidin[] = "'" . (int)$id . "'";
            }
        }
        if ($tidin) {
            $tidin = implode(',', $tidin);
            $this->db->delete_query('threads', "tid IN ($tidin) AND visible='-2' AND uid='" . $this->user->uid . "'");
            $tidinp = "OR tid IN ($tidin)";
        }
        if ($pidin || $tidinp) {
            $pidinq = $tidin = '';
            if ($pidin) {
                $pidin = implode(',', $pidin);
                $pidinq = "pid IN ($pidin)";
            } else {
                $pidinq = '1=0';
            }
            $this->db->delete_query('posts', "($pidinq $tidinp) AND visible='-2' AND uid='" . $this->user->uid . "'");
        }
        $this->plugins->runHooks('usercp_do_drafts_end');
        $this->bb->redirect($this->bb->settings['bburl'] . '/usercp/drafts', $this->lang->selected_drafts_deleted);
    }
}
