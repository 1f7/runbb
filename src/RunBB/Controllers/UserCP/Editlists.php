<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Editlists extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }
        $this->bb->add_breadcrumb($this->lang->ucp_nav_editlists);
        $this->plugins->runHooks('usercp_editlists_start');

        $timecut = TIME_NOW - $this->bb->settings['wolcutoff'];

        // Fetch out buddies
        $buddy_count = 0;
        $buddy_list = [];
        if ($this->user->buddylist) {
            $type = 'buddy';
            $query = $this->db->simple_select(
                'users',
                '*',
                "uid IN ({$this->user->buddylist})",
                ['order_by' => 'username']
            );
            while ($user = $this->db->fetch_array($query)) {
                $profile_link = $this->user->build_profile_link(
                    $this->user->format_name(
                        $user['username'],
                        $user['usergroup'],
                        $user['displaygroup']
                    ),
                    $user['uid']
                );
                if ($user['lastactive'] > $timecut && ($user['invisible'] == 0 ||
                        $this->bb->usergroup['canviewwolinvis'] == 1) && $user['lastvisit'] != $user['lastactive']
                ) {
                    $status = 'online';
                } else {
                    $status = 'offline';
                }
                $buddy_list[] = [
                    'uid' => $user['uid'],
                    'status' => $status,
                    'profile_link' => $profile_link
                ];
                ++$buddy_count;
            }
        }
        $this->view->offsetSet('buddy_list', $buddy_list);

        $this->lang->current_buddies = $this->lang->sprintf($this->lang->current_buddies, $buddy_count);

        // Fetch out ignore list users
        $ignore_count = 0;
        $ignore_list = [];
        if ($this->user->ignorelist) {
            $type = 'ignored';
            $query = $this->db->simple_select(
                'users',
                '*',
                "uid IN ({$this->user->ignorelist})",
                ['order_by' => 'username']
            );
            while ($user = $this->db->fetch_array($query)) {
                $profile_link = $this->user->build_profile_link($this->user->format_name(
                    $user['username'],
                    $user['usergroup'],
                    $user['displaygroup']
                ), $user['uid']);
                if ($user['lastactive'] > $timecut && ($user['invisible'] == 0 ||
                        $this->bb->usergroup['canviewwolinvis'] == 1) && $user['lastvisit'] != $user['lastactive']
                ) {
                    $status = 'online';
                } else {
                    $status = 'offline';
                }
                $ignore_list[] = [
                    'uid' => $user['uid'],
                    'status' => $status,
                    'profile_link' => $profile_link
                ];
                ++$ignore_count;
            }
        }
        $this->view->offsetSet('ignore_list', $ignore_list);

        $this->lang->current_ignored_users = $this->lang->sprintf($this->lang->current_ignored_users, $ignore_count);

        // If an AJAX request from buddy management, echo out whatever the new list is.
        if ($this->bb->request_method == 'post' && $this->bb->input['ajax'] == 1) {
            if ($this->bb->input['manage'] == 'ignored') {
                echo $ignore_list;
                echo "<script type=\"text/javascript\"> 
                $(\"#ignored_count\").html(\"{$ignore_count}\"); {$this->message_js}</script>";
            } else {
                if (isset($this->sent) && $this->sent === true) {
                    $sent_rows = '';
                    $query = $this->db->query('
					SELECT r.*, u.username
					FROM ' . TABLE_PREFIX . 'buddyrequests r
					LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=r.touid)
					WHERE r.uid=' . (int)$this->user->uid);

                    while ($request = $this->db->fetch_array($query)) {
                        $request['username'] = $this->user->build_profile_link(
                            htmlspecialchars_uni($request['username']),
                            (int)$request['touid']
                        );
                        $request['date'] = $this->time->formatDate(
                            $this->bb->settings['dateformat'],
                            $request['date']
                        ) . ' ' .
                            $this->time->formatDate($this->bb->settings['timeformat'], $request['date']);
                        $data = [
                            'bgcolor' => alt_trow(),
                            'username' => $request['username'],
                            'date' => $request['date'],
                            'url' => $this->bb->settings['bburl'],
                            'id' => $request['id'],
                            'post_code' => $this->bb->post_code,
                            'lang_cancel' => $this->lang->cancel
                        ];
                        $sent_rows .= $this->view->fetch('@forum/UserCP/ajax/editlists_sent_request.html.twig', $data);
                    }

                    if ($sent_rows == '') {
                        $sent_rows = '
                        <tr>
                          <td class="' . alt_trow() . '" colspan="3">
                            ' . $this->lang->no_requests . '
                          </td>
                        </tr>';
                    }
                    echo $this->view->fetch(
                        '@forum/UserCP/ajax/editlists_sent_requests.html.twig',
                        ['sent_rows' => $sent_rows]
                    );
                    echo "<script type=\"text/javascript\">{$this->message_js}</script>";
                } else {
                    echo $buddy_list;
                    echo "<script type=\"text/javascript\">
                     $(\"#buddy_count\").html(\"{$buddy_count}\"); {$this->message_js}</script>";
                }
            }
            exit;
        }

        $received_rows = [];
        $query = $this->db->query('
		SELECT r.*, u.username
		FROM ' . TABLE_PREFIX . 'buddyrequests r
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=r.uid)
		WHERE r.touid=' . (int)$this->user->uid);

        while ($request = $this->db->fetch_array($query)) {
            $request['username'] = $this->user->build_profile_link(
                htmlspecialchars_uni($request['username']),
                (int)$request['uid']
            );
            $request['date'] = $this->time->formatDate($this->bb->settings['dateformat'], $request['date']) . ' ' .
                $this->time->formatDate($this->bb->settings['timeformat'], $request['date']);
            $received_rows[] = [
                'bgcolor' => alt_trow(),
                'username' => $request['username'],
                'date' => $request['date'],
                'id' => $request['id']
            ];
        }
        $this->view->offsetSet('received_rows', $received_rows);

        $sent_rows = [];
        $query = $this->db->query('
		SELECT r.*, u.username
		FROM ' . TABLE_PREFIX . 'buddyrequests r
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=r.touid)
		WHERE r.uid=' . (int)$this->user->uid);

        while ($request = $this->db->fetch_array($query)) {
            $request['username'] = $this->user->build_profile_link(
                htmlspecialchars_uni($request['username']),
                (int)$request['touid']
            );
            $request['date'] = $this->time->formatDate($this->bb->settings['dateformat'], $request['date']) . ' ' .
                $this->time->formatDate($this->bb->settings['timeformat'], $request['date']);
            $sent_rows[] = [
                'bgcolor' => alt_trow(),
                'username' => $request['username'],
                'date' => $request['date'],
                'id' => $request['id']
            ];
        }
        $this->view->offsetSet('sent_rows', $sent_rows);

        $this->plugins->runHooks('usercp_editlists_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/editlists.html.twig');
    }

    public function doEditlists(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_editlists_start');

        $existing_users = $selected_list = [];
        if ($this->bb->getInput('manage', '') === 'ignored') {
            if ($this->user->ignorelist) {
                $existing_users = explode(',', $this->user->ignorelist);
            }

            if ($this->user->buddylist) {
                // Create a list of buddies...
                $selected_list = explode(',', $this->user->buddylist);
            }
        } else {
            if ($this->user->buddylist) {
                $existing_users = explode(',', $this->user->buddylist);
            }

            if ($this->user->ignorelist) {
                // Create a list of ignored users
                $selected_list = explode(',', $this->user->ignorelist);
            }
        }

        $error_message = '';
        $message = '';

        // Adding one or more users to this list
        if ($this->bb->getInput('add_username', '', true)) {
            // Split up any usernames we have
            $found_users = 0;
            $adding_self = false;
            $users = explode(',', $this->bb->getInput('add_username', '', true));
            $users = array_map('trim', $users);
            $users = array_unique($users);
            foreach ($users as $key => $username) {
                if (empty($username)) {
                    unset($users[$key]);
                    continue;
                }

                if (my_strtoupper($this->user->username) == my_strtoupper($username)) {
                    $adding_self = true;
                    unset($users[$key]);
                    continue;
                }
                $users[$key] = $this->db->escape_string($username);
            }

            // Get the requests we have sent that are still pending
            $query = $this->db->simple_select('buddyrequests', 'touid', 'uid=' . (int)$this->user->uid);
            $requests = [];
            while ($req = $this->db->fetch_array($query)) {
                $requests[$req['touid']] = true;
            }

            // Get the requests we have received that are still pending
            $query = $this->db->simple_select('buddyrequests', 'uid', 'touid=' . (int)$this->user->uid);
            $requests_rec = [];
            while ($req = $this->db->fetch_array($query)) {
                $requests_rec[$req['uid']] = true;
            }

            $this->sent = false;

            // Fetch out new users
            if (count($users) > 0) {
                switch ($this->db->type) {
                    case 'mysql':
                    case 'mysqli':
                        $field = 'username';
                        break;
                    default:
                        $field = 'LOWER(username)';
                        break;
                }
                $query = $this->db->simple_select(
                    'users',
                    'uid,buddyrequestsauto,buddyrequestspm,language',
                    "{$field} IN ('" . my_strtolower(implode("','", $users)) . "')"
                );
                while ($user = $this->db->fetch_array($query)) {
                    ++$found_users;

                    // Make sure we're not adding a duplicate
                    if (in_array($user['uid'], $existing_users) || in_array($user['uid'], $selected_list)) {
                        if ($this->bb->getInput('manage', '') === 'ignored') {
                            $error_message = 'ignore';
                        } else {
                            $error_message = 'buddy';
                        }

                        // On another list?
                        $string = 'users_already_on_' . $error_message . '_list';
                        if (in_array($user['uid'], $selected_list)) {
                            $string .= '_alt';
                        }

                        $error_message = $this->lang->$string;
                        array_pop($users); // To maintain a proper count when we call count($users)
                        continue;
                    }

                    if (isset($requests[$user['uid']])) {
                        if ($this->bb->getInput('manage', '') !== 'ignored') {
                            $error_message = $this->lang->users_already_sent_request;
                        } elseif ($this->bb->getInput('manage', '') === 'ignored') {
                            $error_message = $this->lang->users_already_sent_request_alt;
                        }

                        array_pop($users); // To maintain a proper count when we call count($users)
                        continue;
                    }

                    if (isset($requests_rec[$user['uid']])) {
                        if ($this->bb->getInput('manage', '') !== 'ignored') {
                            $error_message = $this->lang->users_already_rec_request;
                        } elseif ($this->bb->getInput('manage', '') === 'ignored') {
                            $error_message = $this->lang->users_already_rec_request_alt;
                        }

                        array_pop($users); // To maintain a proper count when we call count($users)
                        continue;
                    }
                    // Do we have auto approval set to On?
                    if ($user['buddyrequestsauto'] == 1 && $this->bb->getInput('manage', '') !== 'ignored') {
                        $existing_users[] = $user['uid'];

                        $pm = [
                            'subject' => 'buddyrequest_new_buddy',
                            'message' => 'buddyrequest_new_buddy_message',
                            'touid' => $user['uid'],
                            'receivepms' => (int)$user['buddyrequestspm'],
                            'language' => $user['language'],
                            'language_file' => 'usercp'
                        ];

                        $this->pm->send_pm($pm);
                    } elseif ($user['buddyrequestsauto'] != 1 && $this->bb->getInput('manage', '') !== 'ignored') {
                        // Send request
                        $id = $this->db->insert_query('buddyrequests', ['uid' => (int)$this->user->uid,
                            'touid' => (int)$user['uid'], 'date' => TIME_NOW]);

                        $pm = [
                            'subject' => 'buddyrequest_received',
                            'message' => 'buddyrequest_received_message',
                            'touid' => $user['uid'],
                            'receivepms' => (int)$user['buddyrequestspm'],
                            'language' => $user['language'],
                            'language_file' => 'usercp'
                        ];

                        $this->pm->send_pm($pm);

                        $this->sent = true;
                    } elseif ($this->bb->getInput('manage', '') === 'ignored') {
                        $existing_users[] = $user['uid'];
                    }
                }
            }

            if ($found_users < count($users)) {
                if ($error_message) {
                    $error_message .= '<br />';
                }

                $error_message .= $this->lang->invalid_user_selected;
            }

            if (($adding_self != true || ($adding_self == true && count($users) > 0)) &&
                ($error_message == '' || count($users) > 1)
            ) {
                if ($this->bb->getInput('manage', '') === 'ignored') {
                    $message = $this->lang->users_added_to_ignore_list;
                } else {
                    $message = $this->lang->users_added_to_buddy_list;
                }
            }

            if ($adding_self == true) {
                if ($this->bb->getInput('manage', '') === 'ignored') {
                    $error_message = $this->lang->cant_add_self_to_ignore_list;
                } else {
                    $error_message = $this->lang->cant_add_self_to_buddy_list;
                }
            }

            if (count($existing_users) == 0) {
                $message = '';

                if ($this->sent === true) {
                    $message = $this->lang->buddyrequests_sent_success;
                }
            }
        } // Removing a user from this list
        elseif ($this->bb->getInput('delete', 0)) {
            // Check if user exists on the list
            $key = array_search($this->bb->getInput('delete', 0), $existing_users);
            if ($key !== false) {
                unset($existing_users[$key]);
                $user = $this->user->get_user($this->bb->getInput('delete', 0));
                if (!empty($user)) {
                    // We want to remove us from this user's buddy list
                    if ($user['buddylist'] != '') {
                        $user['buddylist'] = explode(',', $user['buddylist']);
                    } else {
                        $user['buddylist'] = [];
                    }

                    $key = array_search($this->bb->getInput('delete', 0), $user['buddylist']);
                    unset($user['buddylist'][$key]);

                    // Now we have the new list, so throw it all back together
                    $new_list = implode(',', $user['buddylist']);

                    // And clean it up a little to ensure there is no possibility of bad values
                    $new_list = preg_replace('#,{2,}#', ',', $new_list);
                    $new_list = preg_replace('#[^0-9,]#', '', $new_list);

                    if (my_substr($new_list, 0, 1) == ',') {
                        $new_list = my_substr($new_list, 1);
                    }
                    if (my_substr($new_list, -1) == ',') {
                        $new_list = my_substr($new_list, 0, my_strlen($new_list) - 2);
                    }

                    $user['buddylist'] = $this->db->escape_string($new_list);

                    $this->db->update_query(
                        'users',
                        ['buddylist' => $user['buddylist']],
                        "uid='" . (int)$user['uid'] . "'"
                    );
                }

                if ($this->bb->getInput('manage', '') === 'ignored') {
                    $message = $this->lang->removed_from_ignore_list;
                } else {
                    $message = $this->lang->removed_from_buddy_list;
                }
                $message = $this->lang->sprintf($message, $user['username']);
            }
        }

        // Now we have the new list, so throw it all back together
        $new_list = implode(',', $existing_users);

        // And clean it up a little to ensure there is no possibility of bad values
        $new_list = preg_replace('#,{2,}#', ',', $new_list);
        $new_list = preg_replace('#[^0-9,]#', '', $new_list);

        if (my_substr($new_list, 0, 1) == ',') {
            $new_list = my_substr($new_list, 1);
        }
        if (my_substr($new_list, -1) == ',') {
            $new_list = my_substr($new_list, 0, my_strlen($new_list) - 2);
        }

        // And update
        $user = [];
        if ($this->bb->getInput('manage', '') === 'ignored') {
            $user['ignorelist'] = $this->db->escape_string($new_list);
            $this->user->ignorelist = $user['ignorelist'];
        } else {
            $user['buddylist'] = $this->db->escape_string($new_list);
            $this->user->buddylist = $user['buddylist'];
        }

        $this->db->update_query('users', $user, "uid='" . $this->user->uid . "'");

        $this->plugins->runHooks('usercp_do_editlists_end');

        // Ajax based request, throw new list to browser
        if (!empty($this->bb->input['ajax'])) {
            if ($this->bb->getInput('manage', '') === 'ignored') {
                $list = 'ignore';
            } else {
                $list = 'buddy';
            }

            $this->message_js = '';
            if ($message) {
                $this->message_js = "$.jGrowl('{$message}');";
            }

            if ($error_message) {
                $this->message_js .= " $.jGrowl('{$error_message}');";
            }

            if ($this->bb->getInput('delete', 0)) {
                header('Content-type: text/javascript');
                echo "$(\"#" . $this->bb->getInput('manage', '') . "_" . $this->bb->getInput('delete', 0) .
                    "\").remove();\n";
                if ($new_list == '') {
                    echo "\$(\"#" . $this->bb->getInput('manage', '') . "_count\").html(\"0\");\n";
                    if ($this->bb->getInput('manage', '') === 'ignored') {
                        echo "\$(\"#ignore_list\").html(\"<li>{$this->lang->ignore_list_empty}</li>\");\n";
                    } else {
                        echo "\$(\"#buddy_list\").html(\"<li>{$this->lang->buddy_list_empty}</li>\");\n";
                    }
                } else {
                    echo "\$(\"#" . $this->bb->getInput('manage', '') . "_count\").html(\"" .
                        count(explode(",", $new_list)) . "\");\n";
                }
                echo $this->message_js;
                exit;
            }
            $this->index($request, $response, true);
        } else {
            if ($error_message) {
                $message .= '<br />' . $error_message;
            }
            $this->bb->redirect(
                $this->bb->settings['bburl'] . '/usercp/editlists#' .
                $this->bb->getInput('manage', ''),
                $message
            );
        }
    }
}
