<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Editsig extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }
        $this->bb->add_breadcrumb($this->lang->nav_editsig);

        $this->plugins->runHooks('usercp_editsig_start');
        if (!empty($this->bb->input['preview']) && empty($this->errors)) {
            $sig = $this->bb->getInput('signature', '', true);
            $template = 'preview';//'usercp_editsig_preview';
        } elseif (empty($this->errors)) {
            $sig = $this->user->signature;
            $template = 'current';//'usercp_editsig_current';
        } else {
            $sig = $this->bb->getInput('signature', '', true);
            $template = false;
        }

        if (!isset($this->errors)) {
            $this->errors = '';
        }
        $this->view->offsetSet('error', $this->errors);

        if ($this->user->suspendsignature &&
            ($this->user->suspendsigtime == 0 ||
                $this->user->suspendsigtime > 0 &&
                $this->user->suspendsigtime > TIME_NOW)
        ) {
            // User currently has no signature and they're suspended
            $this->bb->error($this->lang->sig_suspended);
        }

        if ($this->bb->usergroup['canusesig'] != 1) {
            // Usergroup has no permission to use this facility
            return $this->bb->error_no_permission();
        } elseif ($this->bb->usergroup['canusesig'] == 1 &&
            $this->bb->usergroup['canusesigxposts'] > 0 &&
            $this->user->postnum < $this->bb->usergroup['canusesigxposts']
        ) {
            // Usergroup can use this facility, but only after x posts
            $this->bb->error($this->lang->sprintf(
                $this->lang->sig_suspended_posts,
                $this->bb->usergroup['canusesigxposts']
            ));
        }

        $this->bb->showSignature = false;
        $sigpreview = '';
        if ($sig && $template) {
            $this->bb->showSignature = true;
            $this->view->offsetSet('template', $template);
            $sig_parser = [
                'allow_html' => $this->bb->settings['sightml'],
                'allow_mycode' => $this->bb->settings['sigmycode'],
                'allow_smilies' => $this->bb->settings['sigsmilies'],
                'allow_imgcode' => $this->bb->settings['sigimgcode'],
                'me_username' => $this->user->username,
                'filter_badwords' => 1
            ];

            if ($this->user->showimages != 1 && $this->user->uid != 0) {
                $sig_parser['allow_imgcode'] = 0;
            }
            $sigpreview = $this->parser->parse_message($sig, $sig_parser);
            //ev al("\$signature = \"".$this->templates->get($template)."\";");
        }
        $this->view->offsetSet('sigpreview', $sigpreview);

        $this->smilieinserter = $this->codebuttons = false;

        // User has a current signature, so let's display it (but show an error message)
        if ($this->user->suspendsignature && $this->user->suspendsigtime > TIME_NOW) {
            $this->plugins->runHooks('usercp_editsig_end');

            // User either doesn't have permission, or has their signature suspended
            //ev al("\$editsig = \"".$this->templates->get("usercp_editsig_suspended")."\";");
            return $this->view->render($response, '@forum/UserCP/editsigSuspended.html.twig');
        } else {
            // User is allowed to edit their signature
            if ($this->bb->settings['sigsmilies'] == 1) {
                $sigsmilies = $this->lang->on;
                $this->bb->smilieinserter = $this->editor->build_clickable_smilies();
            } else {
                $sigsmilies = $this->lang->off;
            }
            if ($this->bb->settings['sigmycode'] == 1) {
                $sigmycode = $this->lang->on;
            } else {
                $sigmycode = $this->lang->off;
            }
            if ($this->bb->settings['sightml'] == 1) {
                $sightml = $this->lang->on;
            } else {
                $sightml = $this->lang->off;
            }
            if ($this->bb->settings['sigimgcode'] == 1) {
                $sigimgcode = $this->lang->on;
            } else {
                $sigimgcode = $this->lang->off;
            }
            //$sig = htmlspecialchars_uni($sig);
            $this->view->offsetSet('sig', htmlspecialchars_uni($sig));
            $this->lang->edit_sig_note2 = $this->lang->sprintf(
                $this->lang->edit_sig_note2,
                $sigsmilies,
                $sigmycode,
                $sigimgcode,
                $sightml,
                $this->bb->settings['siglength']
            );

            if ($this->bb->settings['bbcodeinserter'] != 0 || $this->user->showcodebuttons != 0) {
                $this->bb->codebuttons = $this->editor->build_mycode_inserter('signature');
            }

            $this->plugins->runHooks('usercp_editsig_end');

            //ev al('\$editsig = \''.$this->templates->get('usercp_editsig').'\';');
        }
        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/editsig.html.twig');
    }

    public function doEditsig(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_editsig_start');

        // User currently has a suspended signature
        if ($this->user->suspendsignature == 1 && $this->user->suspendsigtime > TIME_NOW) {
            return $this->bb->error_no_permission();
        }
//////////////////////////
        $parser_options = [
            'allow_html' => $this->bb->settings['sightml'],
            'filter_badwords' => 1,
            'allow_mycode' => $this->bb->settings['sigmycode'],
            'allow_smilies' => $this->bb->settings['sigsmilies'],
            'allow_imgcode' => $this->bb->settings['sigimgcode'],
            "filter_badwords" => 1
        ];

        if ($this->user->showimages != 1 && $this->user->uid != 0) {
            $parser_options['allow_imgcode'] = 0;
        }

        $parsed_sig = $this->parser->parse_message($this->bb->getInput('signature', '', true), $parser_options);
        if ((($this->bb->settings['sigimgcode'] == 0 && $this->bb->settings['sigsmilies'] != 1) &&
                substr_count($parsed_sig, "<img") > 0) ||
            (($this->bb->settings['sigimgcode'] == 1 || $this->bb->settings['sigsmilies'] == 1) &&
                substr_count($parsed_sig, "<img") > $this->bb->settings['maxsigimages'])
        ) {
            if ($this->bb->settings['sigimgcode'] == 1) {
                $imgsallowed = $this->bb->settings['maxsigimages'];
            } else {
                $imgsallowed = 0;
            }
            $this->lang->too_many_sig_images2 = $this->lang->sprintf($this->lang->too_many_sig_images2, $imgsallowed);
            $this->errors = $this->bb->inline_error($this->lang->too_many_sig_images . ' ' .
                $this->lang->too_many_sig_images2);
            $this->bb->input['preview'] = 1;
        } elseif ($this->bb->settings['siglength'] > 0) {
            if ($this->bb->settings['sigcountmycode'] == 0) {
                $parsed_sig = $this->parser->text_parse_message($this->bb->getInput('signature', '', true));
            } else {
                $parsed_sig = $this->bb->getInput('signature', '', true);
            }
            $parsed_sig = preg_replace('#\s#', '', $parsed_sig);
            $sig_length = my_strlen($parsed_sig);
            if ($sig_length > $this->bb->settings['siglength']) {
                $this->lang->sig_too_long = $this->lang->sprintf(
                    $this->lang->sig_too_long,
                    $this->bb->settings['siglength']
                );
                if ($sig_length - $this->bb->settings['siglength'] > 1) {
                    $this->lang->sig_too_long .= $this->lang->sprintf(
                        $this->lang->sig_remove_chars_plural,
                        $sig_length - $this->bb->settings['siglength']
                    );
                } else {
                    $this->lang->sig_too_long .= $this->lang->sig_remove_chars_singular;
                }
                $this->errors = $this->bb->inline_error($this->lang->sig_too_long);
            }
        }
        if (!empty($this->errors) || !empty($this->bb->input['preview'])) {
            return $this->index($request, $response, true);
        }
//////////////////////////
        if ($this->bb->getInput('updateposts', '') === 'enable') {
            $update_signature = [
                'includesig' => 1
            ];
            $this->db->update_query('posts', $update_signature, "uid='" . $this->user->uid . "'");
        } elseif ($this->bb->getInput('updateposts', '') === 'disable') {
            $update_signature = [
                'includesig' => 0
            ];
            $this->db->update_query('posts', $update_signature, "uid='" . $this->user->uid . "'");
        }
        $new_signature = [
            'signature' => $this->db->escape_string($this->bb->getInput('signature', '', true))
        ];
        $this->plugins->runHooks('usercp_do_editsig_process');
        $this->db->update_query('users', $new_signature, "uid='" . $this->user->uid . "'");
        $this->plugins->runHooks('usercp_do_editsig_end');
        $this->bb->redirect($this->bb->settings['bburl'] . '/usercp/editsig', $this->lang->redirect_sigupdated);
    }
}
