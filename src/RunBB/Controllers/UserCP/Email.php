<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Email extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        $this->bb->add_breadcrumb($this->lang->nav_email);

        // Coming back to this page after one or more errors were experienced,
        // show fields the user previously entered (with the exception of the password)
        if ($this->errors) {
            $email = htmlspecialchars_uni($this->bb->getInput('email', ''));
            $email2 = htmlspecialchars_uni($this->bb->getInput('email2', ''));
        } else {
            $email = $email2 = '';
        }
        $data = [
            'email' => $email,
            'email2' => $email2,
        ];

        $this->view->offsetSet('errors', $data);
        $this->view->offsetSet('errors', $this->errors);
        $this->plugins->runHooks('usercp_email');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/email.html.twig');
    }

    public function doEmail(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->errors = [];

        $this->plugins->runHooks('usercp_do_email_start');
        if ($this->user->validate_password_from_uid(
            $this->user->uid,
            $this->bb->getInput('password', '', true)
        ) == false) {
            $this->errors[] = $this->lang->error_invalidpassword;
        } else {
            // Set up user handler.
            $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

            $user = [
                'uid' => $this->user->uid,
                'email' => $this->bb->getInput('email', ''),
                'email2' => $this->bb->getInput('email2', '')
            ];

            $userhandler->set_data($user);

            if (!$userhandler->validate_user()) {
                $this->errors = $userhandler->get_friendly_errors();
            } else {
                if ($this->user->usergroup != '5' && $this->bb->usergroup['cancp'] != 1 &&
                    $this->bb->settings['regtype'] != 'verify') {
                    $uid = $this->user->uid;
                    $username = $this->user->username;

                    // Emails require verification
                    $activationcode = random_str();
                    $this->db->delete_query('awaitingactivation', "uid='" . $this->user->uid . "'");

                    $newactivation = [
                        'uid' => $this->user->uid,
                        'dateline' => TIME_NOW,
                        'code' => $activationcode,
                        'type' => 'e',
                        'misc' => $this->db->escape_string($this->bb->getInput('email', ''))
                    ];

                    $this->db->insert_query('awaitingactivation', $newactivation);

                    $mail_message = $this->lang->sprintf(
                        $this->lang->email_changeemail,
                        $this->user->username,
                        $this->bb->settings['bbname'],
                        $this->user->email,
                        $this->bb->getInput('email', ''),
                        $this->bb->settings['bburl'],
                        $activationcode,
                        $this->user->username,
                        $this->user->uid
                    );

                    $this->lang->emailsubject_changeemail = $this->lang->sprintf(
                        $this->lang->emailsubject_changeemail,
                        $this->bb->settings['bbname']
                    );
                    $this->mail->send(
                        $this->bb->getInput('email', ''),
                        $this->lang->emailsubject_changeemail,
                        $mail_message
                    );

                    $this->plugins->runHooks('usercp_do_email_verify');
                    $this->bb->error($this->lang->redirect_changeemail_activation);
                } else {
                    $userhandler->update_user();
                    // Email requires no activation
                    $mail_message = $this->lang->sprintf(
                        $this->lang->email_changeemail_noactivation,
                        $this->user->username,
                        $this->bb->settings['bbname'],
                        $this->user->email,
                        $this->bb->getInput('email', ''),
                        $this->bb->settings['bburl']
                    );
                    $this->mail->send($this->bb->getInput('email', ''), $this->lang->sprintf(
                        $this->lang->emailsubject_changeemail,
                        $this->bb->settings['bbname']
                    ), $mail_message);
                    $this->plugins->runHooks('usercp_do_email_changed');
                    $this->bb->redirect(
                        $this->bb->settings['bburl'] . '/usercp/email',
                        $this->lang->redirect_emailupdated
                    );
                }
            }
        }
        if (count($this->errors) > 0) {
            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        }
    }
}
