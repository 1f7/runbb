<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Forumsubscriptions extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->ucp_nav_forum_subscriptions);

        $this->plugins->runHooks('usercp_forumsubscriptions_start');

        if ($this->user->uid == 0) {
            // Build a forum cache.
            $query = $this->db->query('
			SELECT fid
			FROM ' . TABLE_PREFIX . 'forums
			WHERE active != 0
			ORDER BY pid, disporder
		');

            if (isset($this->bb->cookies['mybb']['forumread'])) {
                $forumsread = my_unserialize($this->bb->cookies['mybb']['forumread']);
            }
        } else {
            // Build a forum cache.
            $query = $this->db->query('
			SELECT f.fid, fr.dateline AS lastread
			FROM ' . TABLE_PREFIX . 'forums f
			LEFT JOIN ' . TABLE_PREFIX . "forumsread fr ON (fr.fid=f.fid AND fr.uid='{$this->user->uid}')
			WHERE f.active != 0
			ORDER BY pid, disporder
		");
        }
        $readforums = [];
        while ($forum = $this->db->fetch_array($query)) {
            if ($this->user->uid == 0) {
                if ($forumsread[$forum['fid']]) {
                    $forum['lastread'] = $forumsread[$forum['fid']];
                }
            }
            $readforums[$forum['fid']] = $forum['lastread'];
        }

        $fpermissions = $this->forum->forum_permissions();

        $query = $this->db->query('
		SELECT fs.*, f.*, t.subject AS lastpostsubject, fr.dateline AS lastread
		FROM ' . TABLE_PREFIX . 'forumsubscriptions fs
		LEFT JOIN ' . TABLE_PREFIX . 'forums f ON (f.fid = fs.fid)
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid = f.lastposttid)
		LEFT JOIN ' . TABLE_PREFIX . "forumsread fr ON (fr.fid=f.fid AND fr.uid='{$this->user->uid}')
		WHERE f.type='f' AND fs.uid='" . $this->user->uid . "'
		ORDER BY f.name ASC
	");

        $forums = [];
        while ($forum = $this->db->fetch_array($query)) {
            $forum_url = $this->forum->get_forum_link($forum['fid']);
            $forumpermissions = $fpermissions[$forum['fid']];

            if ($forumpermissions['canview'] == 0 || $forumpermissions['canviewthreads'] == 0) {
                continue;
            }

            $lightbulb = $this->forum->get_forum_lightbulb(
                ['open' => $forum['open'], 'lastread' => $forum['lastread']],
                ['lastpost' => $forum['lastpost']]
            );
            $folder = $lightbulb['folder'];

            if (isset($forumpermissions['canonlyviewownthreads']) && $forumpermissions['canonlyviewownthreads'] != 0) {
                $posts = '-';
                $threads = '-';
            } else {
                $posts = $this->parser->formatNumber($forum['posts']);
                $threads = $this->parser->formatNumber($forum['threads']);
            }

            if ($forum['lastpost'] == 0 || $forum['lastposter'] == '') {
                $lastpost = '<div style="text-align: center;">' . $this->lang->lastpost_never . '</div>';
            } // Hide last post
            elseif (isset($forumpermissions['canonlyviewownthreads']) &&
                $forumpermissions['canonlyviewownthreads'] != 0 &&
                $forum['lastposteruid'] != $this->user->uid
            ) {
                $lastpost = '<div style="text-align: center;">-</div>';
            } else {
                $forum['lastpostsubject'] = $this->parser->parse_badwords($forum['lastpostsubject']);
                $lastpost_date = $this->time->formatDate('relative', $forum['lastpost']);
                $lastposttid = $forum['lastposttid'];
                $lastposter = $forum['lastposter'];
                $lastpost_profilelink = $this->user->build_profile_link($lastposter, $forum['lastposteruid']);
                $full_lastpost_subject = $lastpost_subject = htmlspecialchars_uni($forum['lastpostsubject']);
                if (my_strlen($lastpost_subject) > 25) {
                    $lastpost_subject = my_substr($lastpost_subject, 0, 25) . '...';
                }
                $lastpost_link = get_thread_link($forum['lastposttid'], 0, 'lastpost');
                $lastpost = "
        <span class=\"smalltext\">
        <a href=\"{$lastpost_link}\" title=\"{$full_lastpost_subject}\"><strong>{$lastpost_subject}</strong></a>
        <br />{$lastpost_date}<br />{$this->lang->by} {$lastpost_profilelink}</span>";
            }

            if ($this->bb->settings['showdescriptions'] == 0) {
                $forum['description'] = '';
            }
            $forums[] = [
                'lightbulb' => $lightbulb,
                'forum_url' => $forum_url,
                'forumname' => $forum['name'],
                'fid' => $forum['fid'],
                'posts' => $posts,
                'threads' => $threads,
                'lastpost' => $lastpost
            ];
        }
        $this->view->offsetSet('forums', $forums);

        $this->plugins->runHooks('usercp_forumsubscriptions_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/forumsubscriptions.html.twig');
    }
}
