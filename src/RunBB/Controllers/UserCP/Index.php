<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager as DB;

class Index extends Common
{
    //
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Make navigation
        $this->bb->add_breadcrumb($this->lang->nav_usercp, $this->bb->settings['bburl'] . '/usercp');

        // Get posts per day
        $daysreg = (TIME_NOW - $this->user->regdate) / (24 * 3600);

        if ($daysreg < 1) {
            $daysreg = 1;
        }

        $perday = $this->user->postnum / $daysreg;
        $perday = round($perday, 2);
        if ($perday > $this->user->postnum) {
            $perday = $this->user->postnum;
        }

        $stats = $this->cache->read('stats');
        $posts = $stats['numposts'];
        if ($posts == 0) {
            $percent = '0';
        } else {
            $percent = $this->user->postnum * 100 / $posts;
            $percent = round($percent, 2);
        }

        //$colspan = 2;
        $this->lang->posts_day = $this->lang->sprintf(
            $this->lang->posts_day,
            $this->parser->formatNumber($perday),
            $percent
        );
        //$regdate = $this->time->formatDate('relative', $this->user->regdate);
        $this->view->offsetSet('regdate', $this->time->formatDate('relative', $this->user->regdate));

        $useravatar = $this->user->format_avatar($this->user->avatar, $this->user->avatardimensions, '100x100');
        //$avatar_username = htmlspecialchars_uni($this->user->username);
        $useravatar['username'] = htmlspecialchars_uni($this->user->username);
        //ev al("\$avatar = \"".$this->templates->get("usercp_currentavatar")."\";");
        $this->view->offsetSet('useravatar', $useravatar);

        $usergroup = htmlspecialchars_uni($this->bb->groupscache[$this->user->usergroup]['title']);
        if ($this->user->usergroup == 5 && $this->bb->settings['regtype'] != 'admin') {
            $usergroup .= "<br />(<a href=\"{$this->bb->settings['bburl']}/resendactivation\">
            {$this->lang->resend_activation}</a>)";
            //ev al("\$usergroup .= \"".$this->templates->get('usercp_resendactivation')."\";");
        }
        $this->view->offsetSet('usergroup', $usergroup);
        // Make reputations row
        //$reputations = '';
        if ($this->bb->usergroup['usereputationsystem'] == 1 && $this->bb->settings['enablereputation'] == 1) {
            $this->view->offsetSet('reputation_link', $this->user->get_reputation($this->user->reputation));
            //ev al("\$reputation = \"".$this->templates->get("usercp_reputation")."\";");
        }

        $latest_warnings = [];
        if ($this->bb->settings['enablewarningsystem'] != 0 && $this->bb->settings['canviewownwarning'] != 0) {
            if ($this->bb->settings['maxwarningpoints'] < 1) {
                $this->bb->settings['maxwarningpoints'] = 10;
            }
            $warning_level = round($this->user->warningpoints / $this->bb->settings['maxwarningpoints'] * 100);
            if ($warning_level > 100) {
                $warning_level = 100;
            }

            if ($this->user->warningpoints > $this->bb->settings['maxwarningpoints']) {
                $this->user->warningpoints = $this->bb->settings['maxwarningpoints'];
            }

            if ($warning_level > 0) {
                $warningshandler = new \RunBB\Handlers\DataHandlers\WarningsHandler($this->bb, 'update');

                $warningshandler->expire_warnings();

                $this->lang->current_warning_level = $this->lang->sprintf(
                    $this->lang->current_warning_level,
                    $warning_level,
                    $this->user->warningpoints,
                    $this->bb->settings['maxwarningpoints']
                );
                $warnings = '';
                // Fetch latest warnings
                $query = $this->db->query(
                    "SELECT w.*, t.title AS type_title, u.username, p.subject AS post_subject
				FROM " . TABLE_PREFIX . "warnings w
				LEFT JOIN " . TABLE_PREFIX . "warningtypes t ON (t.tid=w.tid)
				LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=w.issuedby)
				LEFT JOIN " . TABLE_PREFIX . "posts p ON (p.pid=w.pid)
				WHERE w.uid='{$this->user->uid}'
				ORDER BY w.expired ASC, w.dateline DESC
				LIMIT 5
			"
                );
                while ($warning = $this->db->fetch_array($query)) {
                    $post_link = '';
                    if ($warning['post_subject']) {
                        $warning['post_subject'] = $this->parser->parse_badwords($warning['post_subject']);
                        $warning['post_subject'] = htmlspecialchars_uni($warning['post_subject']);
                        $warning['postlink'] = get_post_link($warning['pid']);
                        $post_link .= "<br /><small>{$this->lang->warning_for_post} <a href=\"{$warning['postlink']}\">
                        {$warning['post_subject']}</a></small>";
                    }
                    $issuedby = $this->user->build_profile_link($warning['username'], $warning['issuedby']);
                    $date_issued = $this->time->formatDate('relative', $warning['dateline']);
                    if ($warning['type_title']) {
                        $warning_type = $warning['type_title'];
                    } else {
                        $warning_type = $warning['title'];
                    }
                    $warning_type = htmlspecialchars_uni($warning_type);
                    if ($warning['points'] > 0) {
                        $warning['points'] = "+{$warning['points']}";
                    }
                    $points = $this->lang->sprintf($this->lang->warning_points, $warning['points']);

                    // Figure out expiration time
                    if ($warning['daterevoked']) {
                        $expires = $this->lang->warning_revoked;
                    } elseif ($warning['expired']) {
                        $expires = $this->lang->already_expired;
                    } elseif ($warning['expires'] == 0) {
                        $expires = $this->lang->never;
                    } else {
                        $expires = $this->time->formatDate('relative', $warning['expires']);
                    }
                    //$alt_bg = alt_trow();
                    $latest_warnings[] = [
                        'alt_bg' => alt_trow(),
                        'warning_type' => $warning_type,
                        'points' => $points,
                        'post_link' => $post_link,
                        'date_issued' => $date_issued,
                        'expires' => $expires,
                        'issuedby' => $issuedby
                    ];
                }
            }
        }
        $this->view->offsetSet('latest_warnings', $latest_warnings);

        // Format username
        $username = $this->user->format_name($this->user->username, $this->user->usergroup, $this->user->displaygroup);
        $this->view->offsetSet(
            'username',
            $this->user->build_profile_link($username, $this->user->uid)
        );

        // Format post numbers
        $this->user->posts = $this->parser->formatNumber($this->user->postnum);

        // Build referral link
        if ($this->bb->settings['usereferrals'] == 1) {
            $this->view->offsetSet(
                'referral_link',
                $this->lang->sprintf($this->lang->referral_link, $this->bb->settings['bburl'], $this->user->uid)
            );
            //ev al("\$referral_info = \"".$this->templates->get("usercp_referrals")."\";");
        }

        // User Notepad
        $this->plugins->runHooks("usercp_notepad_start");
        $this->user->notepad = htmlspecialchars_uni($this->user->notepad);
        //ev al("\$user_notepad = \"".$this->templates->get("usercp_notepad")."\";");


        $this->plugins->runHooks("usercp_notepad_end");

        // Thread Subscriptions with New Posts
        $subscriptionsArray = [];
//        $query = $this->db->simple_select("threadsubscriptions",
//            "sid", "uid = '" . $this->user->uid . "'", array("limit" => 1));
        $fs = \RunBB\Models\Threadsubscription::where('uid', '=', $this->user->uid)
            ->take(1)
            ->get(['sid']);

//        if ($this->db->num_rows($query)) {
        if (count($fs) > 0) {
//            $visible = "AND t.visible != 0";
//            if ($this->user->is_moderator() == true) {
//                $visible = '';
//            }

//            $query = $this->db->query("
//			SELECT s.*, t.*, t.username AS threadusername, u.username
//			FROM " . TABLE_PREFIX . "threadsubscriptions s
//			LEFT JOIN " . TABLE_PREFIX . "threads t ON (s.tid=t.tid)
//			LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid = t.uid)
//			WHERE s.uid='" . $this->user->uid . "' {$visible}
//			ORDER BY t.lastpost DESC
//			LIMIT 0, 10");

            $s = \RunBB\Models\Threadsubscription::where('threadsubscriptions.uid', '=', $this->user->uid)
                ->where(function ($q) {
                    if ($this->user->is_moderator() === false) {
                        $q->where('threads.visible', '!=', 0);
                    }
                })
                ->leftJoin('threads', 'threadsubscriptions.tid', '=', 'threads.tid')
                ->leftJoin('users', 'users.uid', '=', 'threads.uid')
                ->orderBy('threads.lastpost', 'desc')
                ->skip(0)->take(10)
                ->get(['threadsubscriptions.*', 'threads.*', 'threads.username AS threadusername', 'users.username'])
                ->toArray();

            $fpermissions = $this->forum->forum_permissions();
//            while ($subscription = $this->db->fetch_array($query)) {
            $subscriptions =[];
            foreach ($s as $subscription) {
                $forumpermissions =
                    isset($fpermissions[$subscription['fid']]) ? $fpermissions[$subscription['fid']] : null;
                if ($forumpermissions['canview'] != 0 &&
                    $forumpermissions['canviewthreads'] != 0 &&
                    ($forumpermissions['canonlyviewownthreads'] == 0 ||
                        $subscription['uid'] == $this->user->uid)) {
                    $subscriptions[$subscription['tid']] = $subscription;
                }
            }

            if (!empty($subscriptions)) {
//                $tids = implode(',', array_keys($subscriptions));
                $where = array_keys($subscriptions);

                // Checking read
                if ($this->bb->settings['threadreadcut'] > 0) {
//                    $query = $this->db->simple_select('threadsread', '*',
//                        "uid='{$this->user->uid}' AND tid IN ({$tids})");
                    $t = \RunBB\Models\Threadsread::where('uid', '=', $this->user->uid)
                        ->whereIn('tid', $where)
                        ->get()
                        ->toArray();

//                    while ($readthread = $this->db->fetch_array($query)) {
                    foreach ($t as $readthread) {
                        if ($readthread['dateline'] >= $subscriptions[$readthread['tid']]['lastpost']) {
                            unset($subscriptions[$readthread['tid']]); // If it's already been read,
                            //then don't display the thread
                        } else {
                            $subscriptions[$readthread['tid']]['lastread'] = $readthread['dateline'];
                        }
                    }
                }
                if ($subscriptions) {
                    if ($this->bb->settings['dotfolders'] != 0) {
//                        $query = $this->db->simple_select("posts", "tid,uid", "uid='{$this->user->uid}'
// AND tid IN ({$tids})");
                        $p = \RunBB\Models\Post::where('uid', '=', $this->user->uid)
                            ->whereIn('tid', $where)
                            ->get(['tid', 'uid'])
                            ->toArray();

//                        while ($post = $this->db->fetch_array($query)) {
                        foreach ($p as $post) {
                            $subscriptions[$post['tid']]['doticon'] = 1;
                        }
                    }

                    $icon_cache = $this->cache->read("posticons");
                    $threadprefixes = $this->thread->build_prefixes();

                    foreach ($subscriptions as $thread) {
                        $folder = '';
                        $folder_label = '';
                        $gotounread = '';
                        if (isset($thread['tid'])) {
                            $bgcolor = alt_trow();
                            $thread['subject'] = $this->parser->parse_badwords($thread['subject']);
                            $thread['subject'] = htmlspecialchars_uni($thread['subject']);
                            $thread['threadlink'] = get_thread_link($thread['tid']);
                            $thread['lastpostlink'] = get_thread_link($thread['tid'], 0, "lastpost");

                            // If this thread has a prefix...
                            if ($thread['prefix'] != 0 && !empty($threadprefixes[$thread['prefix']])) {
                                $thread['displayprefix'] = $threadprefixes[$thread['prefix']]['displaystyle'] .'&nbsp;';
                            } else {
                                $thread['displayprefix'] = '';
                            }

                            // Icons
                            if ($thread['icon'] > 0 && isset($icon_cache[$thread['icon']])) {
                                $icon = $icon_cache[$thread['icon']];
                                $icon['path'] = str_replace("{theme}", $this->bb->theme['imgdir'], $icon['path']);
                                $icon['path'] = htmlspecialchars_uni($icon['path']);
                                $icon['name'] = htmlspecialchars_uni($icon['name']);
                                $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" 
                                title=\"{$icon['name']}\" />";
                            } else {
                                $icon = "&nbsp;";
                            }

                            if ($thread['doticon']) {
                                $folder = 'dot_';
                                $folder_label .= $this->lang->icon_dot;
                            }

                            // Check to see which icon we display
                            if (isset($thread['lastread']) && $thread['lastread'] < $thread['lastpost']) {
                                $folder .= 'new';
                                $folder_label .= $this->lang->icon_new;
                                $new_class = "subject_new";
                                $thread['newpostlink'] = get_thread_link($thread['tid'], 0, "newpost");
                                $gotounread = "<a href=\"{$thread['newpostlink']}\">
                                <img src=\"{$this->bb->theme['imgdir']}/jump.png\" 
                                alt=\"{$this->lang->goto_first_unread}\" title=\"{$this->lang->goto_first_unread}\" />
                                </a> ";
                            } else {
                                $folder_label .= $this->lang->icon_no_new;
                                $new_class = 'subject_old';
                            }

                            $folder .= 'folder';

                            if ($thread['visible'] == 0) {
                                $bgcolor = "trow_shaded";
                            }

                            $lastpostdate = $this->time->formatDate('relative', $thread['lastpost']);
                            $lastposter = $thread['lastposter'];
                            $lastposteruid = $thread['lastposteruid'];

                            if ($lastposteruid == 0) {
                                $lastposterlink = $lastposter;
                            } else {
                                $lastposterlink = $this->user->build_profile_link($lastposter, $lastposteruid);
                            }

                            $thread['replies'] = $this->parser->formatNumber($thread['replies']);
                            $thread['views'] = $this->parser->formatNumber($thread['views']);
                            $thread['author'] = $this->user->build_profile_link($thread['username'], $thread['uid']);
                            $thread['bgcolor'] = $bgcolor;
                            $thread['folder'] = $folder;
                            $thread['folder_label'] = $folder_label;
                            $thread['new_class'] = $new_class;
                            $thread['icon'] = $icon;
                            $thread['gotounread'] = $gotounread;
                            $thread['lastpostdate'] = $lastpostdate;
                            $thread['lastposterlink'] = $lastposterlink;
                            $subscriptionsArray[] = $thread;
                            //ev al("\$latest_subscribed_threads .= \"".
                            //$this->templates->get("usercp_latest_subscribed_threads")."\";");
                        }
                    }
                    //ev al("\$latest_subscribed = \"".$this->templates->get("usercp_latest_subscribed")."\";");
                }
            }
        }
        $this->view->offsetSet('subscriptionsArray', $subscriptionsArray);

        // User's Latest Threads

        // Get unviewable forums
        $f_perm_sql = '';
        $unviewable_forums = $this->forum->get_unviewable_forums();
        $inactiveforums = $this->forum->get_inactive_forums();
        if ($unviewable_forums) {
            $f_perm_sql = ' AND t.fid NOT IN ('.implode(',', $unviewable_forums).')';
        }
        if ($inactiveforums) {
            $f_perm_sql .= ' AND t.fid NOT IN ('.implode(',', $inactiveforums).')';
        }

        $visible = " AND t.visible != 0";
        if ($this->user->is_moderator() == true) {
            $visible = '';
        }

//        $query = $this->db->query(
//        "SELECT t.*, t.username AS threadusername, u.username
//		FROM " . TABLE_PREFIX . "threads t
//		LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid = t.uid)
//		WHERE t.uid='" . $this->user->uid . "' AND t.firstpost != 0 AND t.visible >= 0 {$visible}{$f_perm_sql}
//		ORDER BY t.lastpost DESC
//		LIMIT 0, 5");
        $th = \RunBB\Models\Thread::where([
            ['threads.uid', '=', $this->user->uid],
            ['threads.firstpost', '!=', 0],
            ['threads.visible', '>=', 0]
        ])
            ->where(function ($q) use ($unviewable_forums, $inactiveforums) {
                if ($this->user->is_moderator() === false) {
                    $q->where('threads.visible', '!=', 0);
                }
                if ($unviewable_forums) {
                    $q->whereNotIn('threads.fid', $unviewable_forums);
                }
                if ($inactiveforums) {
                    $q->whereNotIn('threads.fid', $inactiveforums);
                }
            })
            ->leftJoin('users', 'users.uid', '=', 'threads.uid')
            ->orderBy('threads.lastpost', 'desc')
            ->skip(0)->take(5)
            ->get(['threads.*', 'threads.username AS threadusername', 'users.username'])
            ->toArray();

        // Figure out whether we can view these threads...
        $threadcache = [];
        $fpermissions = $this->forum->forum_permissions();
//        while ($thread = $this->db->fetch_array($query)) {
        foreach ($th as $thread) {
            // Moderated, and not moderator?
            if ($thread['visible'] == 0 && $this->user->is_moderator($thread['fid'], "canviewunapprove") === false) {
                continue;
            }

            $forumpermissions = $fpermissions[$thread['fid']];
            if ($forumpermissions['canview'] != 0 || $forumpermissions['canviewthreads'] != 0) {
                $threadcache[$thread['tid']] = $thread;
            }
        }

        $latest_threads = [];
        if (!empty($threadcache)) {
            $tids = implode(",", array_keys($threadcache));
            $where = array_keys($threadcache);

            // Read Forums
//            $query = $this->db->query("
//			SELECT f.fid, fr.dateline AS lastread
//			FROM " . TABLE_PREFIX . "forums f
//			LEFT JOIN " . TABLE_PREFIX . "forumsread fr ON (fr.fid=f.fid AND fr.uid='{$this->user->uid}')
//			WHERE f.active != 0
//			ORDER BY pid, disporder
//		");
            $f = \RunBB\Models\Forum::where('forums.active', '!=', 0)
                ->leftJoin('forumsread', function ($j) {
                    $j->on('forumsread.fid', '=', 'forums.fid');
                    $j->on('forumsread.uid', '=', DB::raw($this->user->uid));
                })
                ->orderBy('pid')
                ->orderBy('disporder')
                ->get(['forums.fid', 'forumsread.dateline AS lastread'])
                ->toArray();

//            while ($forum = $this->db->fetch_array($query)) {
            foreach ($f as $forum) {
                $readforums[$forum['fid']] = $forum['lastread'];
            }

            // Threads being read?
            if ($this->bb->settings['threadreadcut'] > 0) {
//                $query = $this->db->simple_select("threadsread", "*",
//"uid='{$this->user->uid}' AND tid IN ({$tids})");
                $rt = \RunBB\Models\Threadsread::where('uid', '=', $this->user->uid)
                    ->whereIn('tid', $where)
                    ->get()
                    ->toArray();

//                while ($readthread = $this->db->fetch_array($query)) {
                foreach ($rt as $readthread) {
                    $threadcache[$readthread['tid']]['lastread'] = $readthread['dateline'];
                }
            }

            // Icon Stuff
            if ($this->bb->settings['dotfolders'] != 0) {
//                $query = $this->db->simple_select("posts", "tid,uid",
//"uid='{$this->user->uid}' AND tid IN ({$tids})");
                $p = \RunBB\Models\Post::where('uid', '=', $this->user->uid)
                    ->whereIn('tid', $where)
                    ->get(['tid', 'uid'])
                    ->toArray();

//                while ($post = $this->db->fetch_array($query)) {
                foreach ($p as $post) {
                    $threadcache[$post['tid']]['doticon'] = 1;
                }
            }

            $icon_cache = $this->cache->read("posticons");
            $threadprefixes = $this->thread->build_prefixes();

            // Run the threads...
            $latest_threads_threads = '';
            foreach ($threadcache as $thread) {
                if ($thread['tid']) {
                    $bgcolor = alt_trow();
                    $folder = '';
                    $folder_label = '';
                    $prefix = '';
                    $gotounread = '';
                    $isnew = 0;
                    $donenew = 0;
                    $lastread = 0;

                    // If this thread has a prefix...
                    if ($thread['prefix'] != 0) {
                        if (!empty($threadprefixes[$thread['prefix']])) {
                            $thread['displayprefix'] = $threadprefixes[$thread['prefix']]['displaystyle'] . '&nbsp;';
                        }
                    } else {
                        $thread['displayprefix'] = '';
                    }

                    $thread['subject'] = $this->parser->parse_badwords($thread['subject']);
                    $thread['subject'] = htmlspecialchars_uni($thread['subject']);
                    $thread['threadlink'] = get_thread_link($thread['tid']);
                    $thread['lastpostlink'] = get_thread_link($thread['tid'], 0, "lastpost");

                    if ($thread['icon'] > 0 && $icon_cache[$thread['icon']]) {
                        $icon = $icon_cache[$thread['icon']];
                        //$icon['path'] = str_replace("{theme}", $this->bb->theme['imgdir'], $icon['path']);
                        $icon['path'] = htmlspecialchars_uni($icon['path']);
                        $icon['name'] = htmlspecialchars_uni($icon['name']);
                        $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" 
                        title=\"{$icon['name']}\" />";
                    } else {
                        $icon = "&nbsp;";
                    }

                    if ($this->bb->settings['threadreadcut'] > 0) {
                        $forum_read = $readforums[$thread['fid']];

                        $read_cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                        if ($forum_read == 0 || $forum_read < $read_cutoff) {
                            $forum_read = $read_cutoff;
                        }
                    }

                    if ($this->bb->settings['threadreadcut'] > 0 && $thread['lastpost'] > $forum_read) {
                        $cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                    }

                    $cutoff = 0;
                    if ($thread['lastpost'] > $cutoff) {
                        if (isset($thread['lastread'])) {
                            $lastread = $thread['lastread'];
                        }
                    }

                    if (!$lastread) {
                        $readcookie = $threadread = $this->bb->my_get_array_cookie("threadread", $thread['tid']);
                        if ($readcookie > $forum_read) {
                            $lastread = $readcookie;
                        } else {
                            $lastread = $forum_read;
                        }
                    }

                    // Folder Icons
                    if ($thread['doticon']) {
                        $folder = "dot_";
                        $folder_label .= $this->lang->icon_dot;
                    }

                    if ($thread['lastpost'] > $lastread && $lastread) {
                        $folder .= "new";
                        $folder_label .= $this->lang->icon_new;
                        $new_class = "subject_new";
                        $thread['newpostlink'] = get_thread_link($thread['tid'], 0, "newpost");
                        $gotounread = "<a href=\"{$thread['newpostlink']}\">
                        <img src=\"{$this->bb->theme['imgdir']}/jump.png\" alt=\"{$this->lang->goto_first_unread}\" 
                        title=\"{$this->lang->goto_first_unread}\" /></a> ";
                        $unreadpost = 1;
                    } else {
                        $folder_label .= $this->lang->icon_no_new;
                        $new_class = "subject_old";
                    }

                    if ($thread['replies'] >= $this->bb->settings['hottopic'] ||
                        $thread['views'] >= $this->bb->settings['hottopicviews']) {
                        $folder .= "hot";
                        $folder_label .= $this->lang->icon_hot;
                    }

                    // Is our thread visible?
                    if ($thread['visible'] == 0) {
                        $bgcolor = 'trow_shaded';
                    }

                    if ($thread['closed'] == 1) {
                        $folder .= "lock";
                        $folder_label .= $this->lang->icon_lock;
                    }

                    $folder .= "folder";

                    $lastpostdate = $this->time->formatDate('relative', $thread['lastpost']);
                    $lastposter = $thread['lastposter'];
                    $lastposteruid = $thread['lastposteruid'];

                    if ($lastposteruid == 0) {
                        $lastposterlink = $lastposter;
                    } else {
                        $lastposterlink = $this->user->build_profile_link($lastposter, $lastposteruid);
                    }

                    $thread['replies'] = $this->parser->formatNumber($thread['replies']);
                    $thread['views'] = $this->parser->formatNumber($thread['views']);
                    $thread['author'] = $this->user->build_profile_link($thread['username'], $thread['uid']);
                    $thread['bgcolor'] = $bgcolor;
                    $thread['folder'] = $folder;
                    $thread['folder_label'] = $folder_label;
                    $thread['icon'] = $icon;
                    $thread['gotounread'] = $gotounread;
                    $thread['new_class'] = $new_class;
                    $thread['lastpostdate'] = $lastpostdate;
                    $thread['lastposterlink'] = $lastposterlink;
                    $latest_threads[] = $thread;
                    /*

                     */
                    //ev al("\$latest_threads_threads .= \"".$this->templates->get
                    //("usercp_latest_threads_threads")."\";");
                }
            }

            //ev al("\$latest_threads = \"".$this->templates->get("usercp_latest_threads")."\";");
        }
        $this->view->offsetSet('latest_threads', $latest_threads);

        $this->plugins->runHooks("usercp_end");

        //ev al("\$usercp = \"".$this->templates->get("usercp")."\";");
        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/index.html.twig');
    }
}
