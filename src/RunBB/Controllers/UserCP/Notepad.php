<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Notepad extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        //add_breadcrumb($lang->ucp_nav_notepad);
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        // Cap at 60,000 chars; text will allow up to 65535?
        if (my_strlen($this->bb->getInput('notepad', '', true)) > 60000) {
            $this->bb->input['notepad'] = my_substr($this->bb->getInput('notepad', '', true), 0, 60000);
        }

        $this->plugins->runHooks('usercp_do_notepad_start');
        $this->db->update_query(
            'users',
            ['notepad' => $this->db->escape_string($this->bb->getInput('notepad', '', true))],
            "uid='".$this->user->uid."'"
        );
        $this->plugins->runHooks('usercp_do_notepad_end');
        $this->bb->redirect($this->bb->settings['bburl'].'/usercp', $this->lang->redirect_notepadupdated);
    }
}
