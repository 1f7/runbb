<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Options extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if ($noinit == false) {
            if ($this->init() === 'exit') {
                return;
            }
        }
        $this->bb->add_breadcrumb($this->lang->nav_options);

        $this->plugins->runHooks('usercp_options_start');

        if ($this->errors != '') {
            $user = $this->bb->input;
        } else {
            $user = $this->user;
        }

        $languages = $this->lang->get_languages();
        $board_language = [];
        if (count($languages) > 1) {
            foreach ($languages as $name => $language) {
                $language = htmlspecialchars_uni($language);
                $sel = '';
                if (isset($user->language) && $user->language == $name) {
                    $sel = ' selected="selected"';
                }
                $board_language[] = [
                    'name' => $name,
                    'sel' => $sel,
                    'language' => $language
                ];
            }
        }
        $this->view->offsetSet('board_language', $board_language);

        // Lets work out which options the user has selected and check the boxes
        $data['allownoticescheck'] =
            (isset($user->allownotices) && $user->allownotices == 1) ? 'checked="checked"' : '';
        $data['invisiblecheck'] = (isset($user->invisible) && $user->invisible == 1) ? 'checked="checked"' : '';
        $data['hideemailcheck'] = (isset($user->hideemail) && $user->hideemail == 1) ? 'checked="checked"' : '';


        $no_auto_subscribe_selected = $instant_email_subscribe_selected =
        $instant_pm_subscribe_selected = $no_subscribe_selected = '';
        if (isset($user->subscriptionmethod) && $user->subscriptionmethod == 1) {
            $no_subscribe_selected = 'selected="selected"';
        } elseif (isset($user->subscriptionmethod) && $user->subscriptionmethod == 2) {
            $instant_email_subscribe_selected = 'selected="selected"';
        } elseif (isset($user->subscriptionmethod) && $user->subscriptionmethod == 3) {
            $instant_pm_subscribe_selected = 'selected="selected"';
        } else {
            $no_auto_subscribe_selected = 'selected="selected"';
        }
        $data['no_auto_subscribe_selected'] = $no_auto_subscribe_selected;
        $data['instant_email_subscribe_selected'] = $instant_email_subscribe_selected;
        $data['instant_pm_subscribe_selected'] = $instant_pm_subscribe_selected;
        $data['no_subscribe_selected'] = $no_subscribe_selected;

        $data['showimagescheck'] = (isset($user->showimages) && $user->showimages == 1) ? 'checked="checked"' : '';
        $data['showvideoscheck'] = (isset($user->showvideos) && $user->showvideos == 1) ? 'checked="checked"' : '';
        $data['showsigscheck'] = (isset($user->showsigs) && $user->showsigs == 1) ? 'checked="checked"' : '';
        $data['showavatarscheck'] = (isset($user->showavatars) && $user->showavatars == 1) ? 'checked="checked"' : '';
        $data['showquickreplycheck'] =
            (isset($user->showquickreply) && $user->showquickreply == 1) ? 'checked="checked"' : '';
        $data['receivepmscheck'] = (isset($user->receivepms) && $user->receivepms == 1) ? 'checked="checked"' : '';
        $data['receivefrombuddycheck'] =
            (isset($user->receivefrombuddy) && $user->receivefrombuddy == 1) ? 'checked="checked"' : '';
        $data['pmnoticecheck'] = (isset($user->pmnotice) && $user->pmnotice >= 1) ? 'checked="checked"' : '';

        $dst_auto_selected = $dst_enabled_selected = $dst_disabled_selected = '';
        if (isset($user->dstcorrection) && $user->dstcorrection == 2) {
            $dst_auto_selected = 'selected="selected"';
        } elseif (isset($user->dstcorrection) && $user->dstcorrection == 1) {
            $dst_enabled_selected = 'selected="selected"';
        } else {
            $dst_disabled_selected = 'selected="selected"';
        }
        $data['dst_auto_selected'] = $dst_auto_selected;
        $data['dst_enabled_selected'] = $dst_enabled_selected;
        $data['dst_disabled_selected'] = $dst_disabled_selected;

        $data['showcodebuttonscheck'] =
            (isset($user->showcodebuttons) && $user->showcodebuttons == 1) ? 'checked="checked"' : '';
        $data['sourcemodecheck'] = (isset($user->sourceeditor) && $user->sourceeditor == 1) ? 'checked="checked"' : '';
        $data['showredirectcheck'] =
            (isset($user->showredirect) && $user->showredirect != 0) ? 'checked="checked"' : '';
        $data['pmnotifycheck'] = (isset($user->pmnotify) && $user->pmnotify != 0) ? 'checked="checked"' : '';
        $data['buddyrequestspmcheck'] =
            (isset($user->buddyrequestspm) && $user->buddyrequestspm != 0) ? 'checked="checked"' : '';
        $data['buddyrequestsautocheck'] =
            (isset($user->buddyrequestsauto) && $user->buddyrequestsauto != 0) ? 'checked="checked"' : '';

        if (!isset($user->threadmode) || ($user->threadmode != 'threaded' && $user->threadmode != 'linear')) {
            $user->threadmode = ''; // Leave blank to show default
        }

        $data['classicpostbitcheck'] =
            (isset($user->classicpostbit) && $user->classicpostbit != 0) ? 'checked="checked"' : '';

        $date_format_options = [];
        foreach ($this->bb->date_formats as $key => $format) {
            $selected = '';
            if (isset($user->dateformat) && $user->dateformat == $key) {
                $selected = ' selected="selected"';
            }
            $date_format_options[] = [
                'key' => $key,
                'selected' => $selected,
                'dateformat' => $this->time->formatDate($format, TIME_NOW, '', 0)
            ];
        }
        $this->view->offsetSet('date_format_options', $date_format_options);

        $time_format_options = [];
        foreach ($this->bb->time_formats as $key => $format) {
            $selected = '';
            if (isset($user->timeformat) && $user->timeformat == $key) {
                $selected = ' selected="selected"';
            }
            $time_format_options[] = [
                'key' => $key,
                'selected' => $selected,
                'timeformat' => $this->time->formatDate($format, TIME_NOW, '', 0)
            ];
        }
        $this->view->offsetSet('time_format_options', $time_format_options);
        $this->view->offsetSet(
            'tzselect',
            $this->time->buildTimezoneSelect('timezoneoffset', $this->user->timezone, true)
        );

        $threadview = ['linear' => '', 'threaded' => ''];
        if (isset($user->threadmode) && is_scalar($user->threadmode)) {
            $threadview[$user->threadmode] = 'selected="selected"';
        }
        $this->view->offsetSet('threadview', $threadview);

        $daysprunesel = [1 => '', 5 => '', 10 => '', 20 => '', 50 => '', 75 => '', 100 => '', 365 => '', 9999 => ''];
        if (isset($user->daysprune) && is_numeric($user->daysprune)) {
            $daysprunesel[$user->daysprune] = 'selected="selected"';
        }
        $this->view->offsetSet('daysprunesel', $daysprunesel);

        if (!isset($user->style)) {
            $user->style = '';
        }
        $this->view->offsetSet('stylelist', $this->themes->build_theme_select('style', $user->style));

        $tppselect = [];
        if ($this->bb->settings['usertppoptions']) {
            $explodedtpp = explode(',', $this->bb->settings['usertppoptions']);
            if (is_array($explodedtpp)) {
                foreach ($explodedtpp as $key => $val) {
                    $selected = '';
                    if (isset($user->tpp) && $user->tpp == $val) {
                        $selected = ' selected="selected"';
                    }
                    $tppselect[] = [
                        'val' => trim($val),
                        'selected' => $selected,
                        'tpp_option' => $this->lang->sprintf($this->lang->tpp_option, $val)
                    ];
                }
            }
        }
        $this->view->offsetSet('tppselect', $tppselect);

        $pppselect = [];
        if ($this->bb->settings['userpppoptions']) {
            $explodedppp = explode(',', $this->bb->settings['userpppoptions']);
            if (is_array($explodedppp)) {
                foreach ($explodedppp as $key => $val) {
                    $selected = '';
                    if (isset($user->ppp) && $user->ppp == $val) {
                        $selected = ' selected="selected"';
                    }
                    $pppselect[] = [
                        'val' => trim($val),
                        'selected' => $selected,
                        'ppp_option' => $this->lang->sprintf($this->lang->ppp_option, $val)
                    ];
                }
            }
        }
        $this->view->offsetSet('pppselect', $pppselect);
        $this->view->offsetSet('errors', $this->errors);
        $this->view->offsetSet('opt', $data);

        $this->plugins->runHooks('usercp_options_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/options.html.twig');
    }

    public function doOptions(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_options_start');

        // Set up user handler.
        $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

        $user = [
            'uid' => $this->user->uid,
            'style' => $this->bb->getInput('style', 0),
            'dateformat' => $this->bb->getInput('dateformat', 0),
            'timeformat' => $this->bb->getInput('timeformat', 0),
            'timezone' => $this->db->escape_string($this->bb->getInput('timezoneoffset', '')),
            'language' => $this->bb->getInput('language', ''),
            'usergroup' => $this->user->usergroup,
            'additionalgroups' => $this->user->additionalgroups
        ];

        $user['options'] = [
            'allownotices' => $this->bb->getInput('allownotices', 0),
            'hideemail' => $this->bb->getInput('hideemail', 0),
            'subscriptionmethod' => $this->bb->getInput('subscriptionmethod', 0),
            'invisible' => $this->bb->getInput('invisible', 0),
            'dstcorrection' => $this->bb->getInput('dstcorrection', 0),
            'threadmode' => $this->bb->getInput('threadmode', ''),
            'showimages' => $this->bb->getInput('showimages', 0),
            'showvideos' => $this->bb->getInput('showvideos', 0),
            'showsigs' => $this->bb->getInput('showsigs', 0),
            'showavatars' => $this->bb->getInput('showavatars', 0),
            'showquickreply' => $this->bb->getInput('showquickreply', 0),
            'receivepms' => $this->bb->getInput('receivepms', 0),
            'pmnotice' => $this->bb->getInput('pmnotice', 0),
            'receivefrombuddy' => $this->bb->getInput('receivefrombuddy', 0),
            'daysprune' => $this->bb->getInput('daysprune', 0),
            'showcodebuttons' => $this->bb->getInput('showcodebuttons', 0),
            'sourceeditor' => $this->bb->getInput('sourceeditor', 0),
            'pmnotify' => $this->bb->getInput('pmnotify', 0),
            'buddyrequestspm' => $this->bb->getInput('buddyrequestspm', 0),
            'buddyrequestsauto' => $this->bb->getInput('buddyrequestsauto', 0),
            'showredirect' => $this->bb->getInput('showredirect', 0),
            'classicpostbit' => $this->bb->getInput('classicpostbit', 0)
        ];

        if ($this->bb->settings['usertppoptions']) {
            $user['options']['tpp'] = $this->bb->getInput('tpp', 0);
        }

        if ($this->bb->settings['userpppoptions']) {
            $user['options']['ppp'] = $this->bb->getInput('ppp', 0);
        }

        $userhandler->set_data($user);


        if (!$userhandler->validate_user()) {
            $this->errors = $userhandler->get_friendly_errors();
            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        } else {
            $userhandler->update_user();

            $this->plugins->runHooks('usercp_do_options_end');

            $this->bb->redirect($this->bb->settings['bburl'] . '/usercp/options', $this->lang->redirect_optionsupdated);
        }
    }
}
