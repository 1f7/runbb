<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Password extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        $this->bb->add_breadcrumb($this->lang->nav_password);

        $this->view->offsetSet('errors', $this->errors);
        $this->plugins->runHooks('usercp_password');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/password.html.twig');
    }

    public function doPassword(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->errors = [];

        $this->plugins->runHooks('usercp_do_password_start');
        if ($this->user->validate_password_from_uid(
            $this->user->uid,
            $this->bb->getInput('oldpassword', '', true)
        ) == false) {
            $this->errors[] = $this->lang->error_invalidpassword;
        } else {
            // Set up user handler.
            $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

            $user = [
                'uid' => $this->user->uid,
                'password' => $this->bb->getInput('password', '', true),
                'password2' => $this->bb->getInput('password2', '', true),
                'username' => $this->user->username,//add for validator
                'email' => $this->user->email//add for validator
            ];

            $userhandler->set_data($user);

            if (!$userhandler->validate_user()) {
                $this->errors = $userhandler->get_friendly_errors();
            } else {
                $userhandler->update_user();
                $this->bb->my_setcookie('runbbuser', $this->user->uid . '_' . $userhandler->data['loginkey']);

                // Notify the user by email that their password has been changed
                $mail_message = $this->lang->sprintf(
                    $this->lang->email_changepassword,
                    $this->user->username,
                    $this->user->email,
                    $this->bb->settings['bbname'],
                    $this->bb->settings['bburl']
                );
                $this->lang->emailsubject_changepassword = $this->lang->sprintf(
                    $this->lang->emailsubject_changepassword,
                    $this->bb->settings['bbname']
                );
                $this->mail->send($this->user->email, $this->lang->emailsubject_changepassword, $mail_message);

                $this->plugins->runHooks('usercp_do_password_end');
                $this->bb->redirect(
                    $this->bb->settings['bburl'] . '/usercp/password',
                    $this->lang->redirect_passwordupdated
                );
            }
        }
        if (count($this->errors) > 0) {
            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        }
    }
}
