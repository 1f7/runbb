<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Profile extends Common
{
    //
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        // Make navigation
        $this->bb->add_breadcrumb($this->lang->nav_usercp, 'usercp');
        $this->bb->add_breadcrumb($this->lang->ucp_nav_profile);

        if ($this->errors) {
            $user = $this->bb->input;
            $bday = [];
            $bday[0] = $this->bb->getInput('bday1', 0);
            $bday[1] = $this->bb->getInput('bday2', 0);
            $bday[2] = $this->bb->getInput('bday3', 0);
        } else {
            $user = $this->user;
            $bday = explode('-', $user->birthday);
            if (!isset($bday[1])) {
                $bday[1] = 0;
            }
            if (!isset($bday[2])) {
                $bday[2] = '';
            }
        }
        $this->view->offsetSet('bday', $bday);

        $this->plugins->runHooks('usercp_profile_start');

        $bdaydaysel = [];
        for ($day = 1; $day <= 31; ++$day) {
            if ($bday[0] == $day) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            $bdaydaysel[] = [
                'day' => $day,
                'selected' => $selected
            ];
        }
        $this->view->offsetSet('bdaydaysel', $bdaydaysel);

        $bdaymonthsel = [];
        foreach (range(1, 12) as $month) {
            $bdaymonthsel[$month] = '';
        }
        $bdaymonthsel[$bday[1]] = 'selected="selected"';
        $this->view->offsetSet('bdaymonthsel', $bdaymonthsel);

        $allselected = $noneselected = $ageselected = '';
        if ($user->birthdayprivacy == 'all' || !$user->birthdayprivacy) {
            $allselected = ' selected="selected"';
        } elseif ($user->birthdayprivacy == 'none') {
            $noneselected = ' selected="selected"';
        } elseif ($user->birthdayprivacy == 'age') {
            $ageselected = ' selected="selected"';
        }
        $this->view->offsetSet('allselected', $allselected);
        $this->view->offsetSet('noneselected', $noneselected);
        $this->view->offsetSet('ageselected', $ageselected);

        if ($user->website == '' || $user->website == 'http://') {
            $user->website = 'http://';
        } else {
            $user->website = htmlspecialchars_uni($user->website);
        }

        if ($user->icq != '0') {
            $user->icq = (int)$user->icq;
        }

        if ($user->icq == 0) {
            $user->icq = '';
        }

        if ($this->errors) {
            $user->skype = htmlspecialchars_uni($user->skype);
            $user->google = htmlspecialchars_uni($user->google);
            $user->aim = htmlspecialchars_uni($user->aim);
            $user->yahoo = htmlspecialchars_uni($user->yahoo);
        }

        $contact_fields = [];
        $this->bb->cfieldsshow = false;

        foreach (['icq', 'aim', 'yahoo', 'skype', 'google'] as $cfield) {
            //$contact_fields[$cfield] = '';
            $csetting = 'allow' . $cfield . 'field';
            if ($this->bb->settings[$csetting] == '') {
                continue;
            }

            if (!$this->user->is_member($this->bb->settings[$csetting])) {
                continue;
            }

            $this->bb->cfieldsshow = true;
            $contact_fields[] = [
                'lang_string' => $this->lang->{'contact_field_' . $cfield},
                'cfield' => $cfield,
                'cfvalue' => htmlspecialchars_uni($user->$cfield)
            ];
        }
        $this->view->offsetSet('contact_fields', $contact_fields);

        $returndatesel = $returndate = $returndatemonthsel = [];
        $awaycheck = ['', ''];
        $awayreason = '';
        if ($this->bb->settings['allowaway'] != 0) {
            if ($this->errors) {
                if ($user->away == 1) {
                    $awaycheck[1] = 'checked="checked"';
                } else {
                    $awaycheck[0] = 'checked="checked"';
                }
//        $returndate = [];
                $returndate[0] = $this->bb->getInput('awayday', 0);
                $returndate[1] = $this->bb->getInput('awaymonth', 0);
                $returndate[2] = $this->bb->getInput('awayyear', 0);
                $user->awayreason = htmlspecialchars_uni($this->bb->getInput('awayreason', '', true));
            } else {
                $user->awayreason = htmlspecialchars_uni($user->awayreason);
                if ($this->user->away == 1) {
                    $awaydate = $this->time->formatDate($this->bb->settings['dateformat'], $this->user->awaydate);
                    $awaycheck[1] = 'checked="checked"';
//FIXME not used???          $awaynotice = $this->lang->sprintf($this->lang->away_notice_away, $awaydate);
                } else {
//FIXME not used???          $awaynotice = $this->lang->away_notice;
                    $awaycheck[0] = 'checked="checked"';
                }
                $returndate = explode('-', $this->user->returndate);
                if (!isset($returndate[1])) {
                    $returndate[1] = 0;
                }
                if (!isset($returndate[2])) {
                    $returndate[2] = '';
                }
            }

            for ($day = 1; $day <= 31; ++$day) {
                if ($returndate[0] == $day) {
                    $selected = 'selected="selected"';
                } else {
                    $selected = '';
                }
                $returndatesel[] = [
                    'day' => $day,
                    'selected' => $selected
                ];
            }

            foreach (range(1, 12) as $month) {
                $returndatemonthsel[$month] = '';
            }
            $returndatemonthsel[$returndate[1]] = 'selected';
        }
        $this->view->offsetSet('awayreason', $awayreason);
        $this->view->offsetSet('awaycheck', $awaycheck);
        $this->view->offsetSet('returndate', $returndate);
        $this->view->offsetSet('returndatemonthsel', $returndatemonthsel);
        $this->view->offsetSet('returndatesel', $returndatesel);

        // Custom profile fields baby!
        $altbg = 'trow1';
        $requiredfields = $customfields = [];
        $this->bb->input['profile_fields'] = $this->bb->getInput('profile_fields', []);

        $pfcache = $this->cache->read('profilefields');

        if (is_array($pfcache)) {
            $code = '';
            foreach ($pfcache as $profilefield) {
                if (!$this->user->is_member($profilefield['editableby']) ||
                    ($profilefield['postnum'] && $profilefield['postnum'] > $this->user->postnum)
                ) {
                    continue;
                }

                $profilefield['type'] = htmlspecialchars_uni($profilefield['type']);
                $profilefield['name'] = htmlspecialchars_uni($profilefield['name']);
                $profilefield['description'] = htmlspecialchars_uni($profilefield['description']);
                $thing = explode("\n", $profilefield['type'], '2');
                $type = $thing[0];
                if (isset($thing[1])) {
                    $options = $thing[1];
                } else {
                    $options = [];
                }
                $field = "fid{$profilefield['fid']}";
                $select = '';
                if ($this->errors) {
                    if (!isset($this->bb->input['profile_fields'][$field])) {
                        $this->bb->input['profile_fields'][$field] = '';
                    }
                    $userfield = $this->bb->input['profile_fields'][$field];
                } else {
                    $userfield = '';//$user->$field;//FIXME
                }
                if ($type == 'multiselect') {
                    if ($this->errors) {
                        $useropts = $userfield;
                    } else {
                        $useropts = explode("\n", $userfield);
                    }
                    if (is_array($useropts)) {
                        foreach ($useropts as $key => $val) {
                            $val = htmlspecialchars_uni($val);
                            $seloptions[$val] = $val;
                        }
                    }
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $val = trim($val);
                            $val = str_replace("\n", "\\n", $val);

                            $sel = '';
                            if (isset($seloptions[$val]) && $val == $seloptions[$val]) {
                                $sel = ' selected="selected"';
                            }
                            $select .= "<option value=\"{$val}\"{$sel}>{$val}</option>";
                        }
                        if (!$profilefield['length']) {
                            $profilefield['length'] = 3;
                        }
                        $code = "
            <select name=\"profile_fields[$field][]\" size=\"{$profilefield['length']}\" multiple=\"multiple\">
              {$select}
            </select>";
                    }
                } elseif ($type == 'select') {
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $val = trim($val);
                            $val = str_replace("\n", "\\n", $val);
                            $sel = '';
                            if ($val == htmlspecialchars_uni($userfield)) {
                                $sel = ' selected="selected"';
                            }
                            $select .= "<option value=\"{$val}\"{$sel}>{$val}</option>";
                        }
                        if (!$profilefield['length']) {
                            $profilefield['length'] = 1;
                        }
                        $code = "
            <select name=\"profile_fields[$field]\" size=\"{$profilefield['length']}\">
                {$select}
            </select>";
                    }
                } elseif ($type == 'radio') {
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $checked = '';
                            if ($val == $userfield) {
                                $checked = ' checked="checked"';
                            }
                            $code .= "
                <input type=\"radio\" class=\"radio\" name=\"profile_fields[$field]\" value=\"{$val}\"{$checked} />
                <span class=\"smalltext\">{$val}</span><br />";
                        }
                    }
                } elseif ($type == 'checkbox') {
                    if ($this->errors) {
                        $useropts = $userfield;
                    } else {
                        $useropts = explode("\n", $userfield);
                    }
                    if (is_array($useropts)) {
                        foreach ($useropts as $key => $val) {
                            $seloptions[$val] = $val;
                        }
                    }
                    $expoptions = explode("\n", $options);
                    if (is_array($expoptions)) {
                        foreach ($expoptions as $key => $val) {
                            $checked = '';
                            if (isset($seloptions[$val]) && $val == $seloptions[$val]) {
                                $checked = ' checked="checked"';
                            }
                            $code .= "
                <input type=\"checkbox\" class=\"checkbox\" name=\"profile_fields[$field][]\" 
                value=\"{$val}\"{$checked} />
                <span class=\"smalltext\">{$val}</span><br />";
                        }
                    }
                } elseif ($type == 'textarea') {
                    $value = htmlspecialchars_uni($userfield);
                    $code = "<textarea name=\"profile_fields[$field]\" rows=\"6\" cols=\"30\" 
                    style=\"width: 95%\">{$value}</textarea>";
                } else {
                    $value = htmlspecialchars_uni($userfield);
                    $maxlength = '';
                    if ($profilefield['maxlength'] > 0) {
                        $maxlength = " maxlength=\"{$profilefield['maxlength']}\"";
                    }
                    $code = "<input type=\"text\" name=\"profile_fields[$field]\" class=\"textbox\" 
                    size=\"{$profilefield['length']}\"{$maxlength} value=\"{$value}\" />";
                }

                if ($profilefield['required'] == 1) {
                    $requiredfields[] = [
                        'name' => $profilefield['name'],
                        'description' => $profilefield['description'],
                        'code' => $code
                    ];
                } else {
                    $customfields[] = [
                        'name' => $profilefield['name'],
                        'description' => $profilefield['description'],
                        'code' => $code
                    ];
                }
                $code = '';
                $select = '';
                $val = '';
                $options = '';
                $expoptions = '';
                $useropts = '';
                $seloptions = '';
            }
        }
        $this->view->offsetSet('requiredfields', $requiredfields);
        $this->view->offsetSet('customfields', $customfields);

        if ($this->bb->usergroup['cancustomtitle'] == 1) {
            if ($this->bb->usergroup['usertitle'] == '') {
                $defaulttitle = '';
                $usertitles = $this->cache->read('usertitles');

                foreach ($usertitles as $title) {
                    if ($title['posts'] <= $this->user->postnum) {
                        $defaulttitle = htmlspecialchars_uni($title['title']);
                        break;
                    }
                }
            } else {
                $defaulttitle = htmlspecialchars_uni($this->bb->usergroup['usertitle']);
            }
            $this->view->offsetSet('defaulttitle', $defaulttitle);

            $newtitle = '';
            if (trim($user->usertitle) == '') {
                $this->lang->current_custom_usertitle = '';
            } else {
                if ($this->errors) {
                    $newtitle = htmlspecialchars_uni($user->usertitle);
//                    $user->usertitle = $this->user->usertitle;
                }
            }
            $this->view->offsetSet('newtitle', $newtitle);

//            $user->usertitle = htmlspecialchars_uni($user->usertitle);
        }

        $this->view->offsetSet('errors', $this->errors);

        $this->plugins->runHooks('usercp_profile_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/profile.html.twig');
    }


    public function doProfile(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_profile_start');

        if ($this->bb->getInput('away', 0) == 1 && $this->bb->settings['allowaway'] != 0) {
            $awaydate = TIME_NOW;
            if (!empty($this->bb->input['awayday'])) {
                // If the user has indicated that they will return on a specific day, but not month or year,
                //assume it is current month and year
                if (!$this->bb->getInput('awaymonth', 0)) {
                    $this->bb->input['awaymonth'] = $this->time->formatDate('n', $awaydate);
                }
                if (!$this->bb->getInput('awayyear', 0)) {
                    $this->bb->input['awayyear'] = $this->time->formatDate('Y', $awaydate);
                }

                $return_month = (int)substr($this->bb->getInput('awaymonth', ''), 0, 2);
                $return_day = (int)substr($this->bb->getInput('awayday', ''), 0, 2);
                $return_year = min((int)$this->bb->getInput('awayyear', ''), 9999);

                // Check if return date is after the away date.
                $returntimestamp = gmmktime(0, 0, 0, $return_month, $return_day, $return_year);
                $awaytimestamp = gmmktime(
                    0,
                    0,
                    0,
                    $this->time->formatDate('n', $awaydate),
                    $this->time->formatDate('j', $awaydate),
                    $this->time->formatDate('Y', $awaydate)
                );
                if ($return_year < $this->time->formatDate('Y', $awaydate) ||
                    ($returntimestamp < $awaytimestamp && $return_year == $this->time->formatDate('Y', $awaydate))
                ) {
                    $this->bb->error($this->lang->error_usercp_return_date_past);
                }

                $returndate = "{$return_day}-{$return_month}-{$return_year}";
            } else {
                $returndate = '';
            }
            $away = [
                'away' => 1,
                'date' => $awaydate,
                'returndate' => $returndate,
                'awayreason' => $this->bb->getInput('awayreason', '', true)
            ];
        } else {
            $away = [
                'away' => 0,
                'date' => '',
                'returndate' => '',
                'awayreason' => ''
            ];
        }

        $bday = [
            'day' => $this->bb->getInput('bday1', 0),
            'month' => $this->bb->getInput('bday2', 0),
            'year' => $this->bb->getInput('bday3', 0)
        ];

        // Set up user handler.
        $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb, 'update');

        $user = [
            'uid' => $this->user->uid,
            'postnum' => $this->user->postnum,
            'usergroup' => $this->user->usergroup,
            'additionalgroups' => $this->user->additionalgroups,
            'birthday' => $bday,
            'birthdayprivacy' => $this->bb->getInput('birthdayprivacy', ''),
            'away' => $away,
            'profile_fields' => $this->bb->getInput('profile_fields', [])
        ];
        foreach (['icq', 'aim', 'yahoo', 'skype', 'google'] as $cfield) {
            $csetting = 'allow' . $cfield . 'field';
            if ($this->bb->settings[$csetting] == '') {
                continue;
            }

            if (!$this->user->is_member($this->bb->settings[$csetting])) {
                continue;
            }

            if ($cfield == 'icq') {
                $user->$cfield = $this->bb->getInput($cfield, 1);
            } else {
                $user->$cfield = $this->bb->getInput($cfield, 0);
            }
        }

        if ($this->bb->usergroup['canchangewebsite'] == 1) {
            $user->website = $this->bb->getInput('website', '');
        }

        if ($this->bb->usergroup['cancustomtitle'] == 1) {
            if ($this->bb->getInput('usertitle', '', true) !== '') {
                $user->usertitle = $this->bb->getInput('usertitle', '', true);
            } elseif (!empty($this->bb->input['reverttitle'])) {
                $user->usertitle = '';
            }
        }
        $userhandler->set_data($user);

        if (!$userhandler->validate_user()) {
            $this->errors = $userhandler->get_friendly_errors();

            // Set allowed value otherwise select options disappear
            if (in_array($this->lang->userdata_invalid_birthday_privacy, $this->errors)) {
                $this->bb->input['birthdayprivacy'] = 'none';
            }

            $this->errors = $this->bb->inline_error($this->errors);
            $this->index($request, $response, true);
        } else {
            $userhandler->update_user();

            $this->plugins->runHooks('usercp_do_profile_end');
            $this->bb->redirect($this->bb->settings['bburl'] .'/usercp/profile', $this->lang->redirect_profileupdated);
        }
    }
}
