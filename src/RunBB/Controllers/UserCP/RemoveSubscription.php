<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class RemoveSubscription extends Common
{
    public function removesubscription(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $server_http_referer = htmlentities($_SERVER['HTTP_REFERER']);

        if ($this->bb->getInput('type', '') === 'forum') {
            $forum = $this->forum->get_forum($this->bb->getInput('fid', 0));
            if (!$forum) {
                $this->bb->error($this->lang->error_invalidforum);
            }

            $this->plugins->runHooks('usercp2_removesubscription_forum');

            $this->user->remove_subscribed_forum($forum['fid']);
            if ($server_http_referer) {
                $url = $server_http_referer;
            } else {
                $url = $this->bb->settings['bburl'] . '/usercp/forumsubscriptions';
            }
            $this->bb->redirect($url, $this->lang->redirect_forumsubscriptionremoved);
        } else {
            $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));
            if (!$thread) {
                $this->bb->error($this->lang->error_invalidthread);
            }

            // Is the currently logged in user a moderator of this forum?
            $ismod = $this->user->is_moderator($thread['fid']);

            // Make sure we are looking at a real thread here.
            if (($thread['visible'] != 1 && $ismod == false) || ($thread['visible'] > 1 && $ismod == true)) {
                $this->bb->error($this->lang->error_invalidthread);
            }

            $this->plugins->runHooks('usercp2_removesubscription_thread');

            $this->user->remove_subscribed_thread($thread['tid']);
            if ($server_http_referer) {
                $url = $server_http_referer;
            } else {
                $url = $this->bb->settings['bburl'] . '/usercp/subscriptions';
            }
            $this->bb->redirect($url, $this->lang->redirect_subscriptionremoved);
        }
    }

    public function removesubscriptions(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));
        $server_http_referer = htmlentities($_SERVER['HTTP_REFERER']);

        if ($this->bb->getInput('type', '') === 'forum') {
            $this->plugins->runHooks('usercp2_removesubscriptions_forum');

            $this->db->delete_query('forumsubscriptions', "uid='" . $this->user->uid . "'");
            if ($server_http_referer) {
                $url = $server_http_referer;
            } else {
                $url = $this->bb->settings['bburl'] . '/usercp/forumsubscriptions';
            }
            $this->bb->redirect($url, $this->lang->redirect_forumsubscriptionsremoved);
        } else {
            $this->plugins->runHooks('usercp2_removesubscriptions_thread');

            $this->db->delete_query('threadsubscriptions', "uid='" . $this->user->uid . "'");
            if ($server_http_referer) {
                $url = $server_http_referer;
            } else {
                $url = $this->bb->settings['bburl'] . '/usercp/subscriptions';
            }
            $this->bb->redirect($url, $this->lang->redirect_subscriptionsremoved);
        }
    }
}
