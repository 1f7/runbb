<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Subscriptions extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->ucp_nav_subscribed_threads);

        $this->plugins->runHooks('usercp_subscriptions_start');

        // Thread visiblity
        $visible = 'AND t.visible != 0';
        if ($this->user->is_moderator() == true) {
            $visible = '';
        }

        // Do Multi Pages
        $query = $this->db->query('
		SELECT COUNT(ts.tid) as threads
		FROM ' . TABLE_PREFIX . 'threadsubscriptions ts
		LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid = ts.tid)
		WHERE ts.uid = '" . $this->user->uid . "' AND t.visible >= 0 {$visible}
	");
        $threadcount = $this->db->fetch_field($query, 'threads');

        if (!$this->bb->settings['threadsperpage'] || (int)$this->bb->settings['threadsperpage'] < 1) {
            $this->bb->settings['threadsperpage'] = 20;
        }

        $perpage = $this->bb->settings['threadsperpage'];
        $page = $this->bb->getInput('page', 0);
        if ($page > 0) {
            $start = ($page - 1) * $perpage;
            $pages = $threadcount / $perpage;
            $pages = ceil($pages);
            if ($page > $pages || $page <= 0) {
                $start = 0;
                $page = 1;
            }
        } else {
            $start = 0;
            $page = 1;
        }
        $end = $start + $perpage;
        $lower = $start + 1;
        $upper = $end;
        if ($upper > $threadcount) {
            $upper = $threadcount;
        }
        //$multipage = multipage($threadcount, $perpage, $page, $this->bb->settings['bburl'].'/usercp/subscriptions');
        $this->view->offsetSet('multipage', $this->pagination->multipage(
            $threadcount,
            $perpage,
            $page,
            $this->bb->settings['bburl'] . '/usercp/subscriptions'
        ));
        $fpermissions = $this->forum->forum_permissions();
        $del_subscriptions = $subscriptions = [];

        // Fetch subscriptions
        $query = $this->db->query('
		SELECT s.*, t.*, t.username AS threadusername, u.username
		FROM ' . TABLE_PREFIX . 'threadsubscriptions s
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (s.tid=t.tid)
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid = t.uid)
		WHERE s.uid='" . $this->user->uid . "' and t.visible >= 0 {$visible}
		ORDER BY t.lastpost DESC
		LIMIT $start, $perpage
	");
        while ($subscription = $this->db->fetch_array($query)) {
            $forumpermissions = $fpermissions[$subscription['fid']];

            if ($forumpermissions['canview'] == 0 || $forumpermissions['canviewthreads'] == 0 ||
                (isset($forumpermissions['canonlyviewownthreads']) &&
                    $forumpermissions['canonlyviewownthreads'] != 0 &&
                    $subscription['uid'] != $this->user->uid)
            ) {
                // Hmm, you don't have permission to view this thread - unsubscribe!
                $del_subscriptions[] = $subscription['sid'];
            } elseif ($subscription['tid']) {
                $subscriptions[$subscription['tid']] = $subscription;
            }
        }

        if (!empty($del_subscriptions)) {
            $sids = implode(',', $del_subscriptions);

            if ($sids) {
                $this->db->delete_query('threadsubscriptions', "sid IN ({$sids}) AND uid='{$this->user->uid}'");
            }

            $threadcount = $threadcount - count($del_subscriptions);

            if ($threadcount < 0) {
                $threadcount = 0;
            }
        }
        $this->view->offsetSet('threadcount', $threadcount);

        $threads = [];
        if (!empty($subscriptions)) {
            $tids = implode(',', array_keys($subscriptions));

            if ($this->user->uid == 0) {
                // Build a forum cache.
                $query = $this->db->simple_select('forums', 'fid', 'active != 0', ['order_by' => 'pid, disporder']);

                $forumsread = my_unserialize($this->bb->cookies['mybb']['forumread']);
            } else {
                // Build a forum cache.
                $query = $this->db->query('
				SELECT f.fid, fr.dateline AS lastread
				FROM ' . TABLE_PREFIX . 'forums f
				LEFT JOIN ' . TABLE_PREFIX . "forumsread fr ON (fr.fid=f.fid AND fr.uid='{$this->user->uid}')
				WHERE f.active != 0
				ORDER BY pid, disporder
			");
            }
            while ($forum = $this->db->fetch_array($query)) {
                if ($this->user->uid == 0) {
                    if ($forumsread[$forum['fid']]) {
                        $forum['lastread'] = $forumsread[$forum['fid']];
                    }
                }
                $readforums[$forum['fid']] = $forum['lastread'];
            }

            // Check participation by the current user in any of these threads - for 'dot' folder icons
            if ($this->bb->settings['dotfolders'] != 0) {
                $query = $this->db->simple_select('posts', 'tid,uid', "uid='{$this->user->uid}' AND tid IN ({$tids})");
                while ($post = $this->db->fetch_array($query)) {
                    $subscriptions[$post['tid']]['doticon'] = 1;
                }
            }

            // Read threads
            if ($this->bb->settings['threadreadcut'] > 0) {
                $query = $this->db->simple_select('threadsread', '*', "uid='{$this->user->uid}' AND tid IN ({$tids})");
                while ($readthread = $this->db->fetch_array($query)) {
                    $subscriptions[$readthread['tid']]['lastread'] = $readthread['dateline'];
                }
            }

            $icon_cache = $this->cache->read('posticons');
            $threadprefixes = $this->thread->build_prefixes();

            // Now we can build our subscription list
            foreach ($subscriptions as $thread) {
                $bgcolor = alt_trow();

                $folder = '';
                $prefix = '';
                $thread['threadprefix'] = '';

                // If this thread has a prefix, insert a space between prefix and subject
                if ($thread['prefix'] != 0 && !empty($threadprefixes[$thread['prefix']])) {
                    $thread['threadprefix'] = $threadprefixes[$thread['prefix']]['displaystyle'] . '&nbsp;';
                }

                // Sanitize
                $thread['subject'] = $this->parser->parse_badwords($thread['subject']);
                $thread['subject'] = htmlspecialchars_uni($thread['subject']);

                // Build our links
                $thread['threadlink'] = get_thread_link($thread['tid']);
                $thread['lastpostlink'] = get_thread_link($thread['tid'], 0, 'lastpost');

                // Fetch the thread icon if we have one
                if ($thread['icon'] > 0 && $icon_cache[$thread['icon']]) {
                    $icon = $icon_cache[$thread['icon']];
                    $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
                    $icon['path'] = htmlspecialchars_uni($icon['path']);
                    $icon['name'] = htmlspecialchars_uni($icon['name']);
                    $icon = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" 
                    title=\"{$icon['name']}\" />";
                } else {
                    $icon = '&nbsp;';
                }

                // Determine the folder
                $folder = '';
                $folder_label = '';

                if (isset($thread['doticon'])) {
                    $folder = 'dot_';
                    $folder_label .= $this->lang->icon_dot;
                }

                $gotounread = '';
                $isnew = 0;
                $donenew = 0;
                $lastread = 0;

                if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
                    $forum_read = $readforums[$thread['fid']];

                    $read_cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                    if ($forum_read == 0 || $forum_read < $read_cutoff) {
                        $forum_read = $read_cutoff;
                    }
                } else {
                    $forum_read = $forumsread[$thread['fid']];
                }

                $cutoff = 0;
                if ($this->bb->settings['threadreadcut'] > 0 && $thread['lastpost'] > $forum_read) {
                    $cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;
                }

                if ($thread['lastpost'] > $cutoff) {
                    if (isset($thread['lastread'])) {
                        $lastread = $thread['lastread'];
                    } else {
                        $lastread = 1;
                    }
                }

                if (!$lastread) {
                    $readcookie = $threadread = $this->bb->my_get_array_cookie('threadread', $thread['tid']);
                    if ($readcookie > $forum_read) {
                        $lastread = $readcookie;
                    } else {
                        $lastread = $forum_read;
                    }
                }

                if ($lastread && $lastread < $thread['lastpost']) {
                    $folder .= 'new';
                    $folder_label .= $this->lang->icon_new;
                    $new_class = 'subject_new';
                    $thread['newpostlink'] = get_thread_link($thread['tid'], 0, 'newpost');
                    $gotounread = "<a href=\"{$thread['newpostlink']}\">
                    <img src=\"{$this->bb->theme['imgdir']}/jump.png\" alt=\"{$this->lang->goto_first_unread}\" 
                    title=\"{$this->lang->goto_first_unread}\" /></a> ";
                    $unreadpost = 1;
                } else {
                    $folder_label .= $this->lang->icon_no_new;
                    $new_class = 'subject_old';
                }

                if ($thread['replies'] >= $this->bb->settings['hottopic'] ||
                    $thread['views'] >= $this->bb->settings['hottopicviews']) {
                    $folder .= 'hot';
                    $folder_label .= $this->lang->icon_hot;
                }

                if ($thread['closed'] == 1) {
                    $folder .= 'lock';
                    $folder_label .= $this->lang->icon_lock;
                }

                $folder .= 'folder';

                if ($thread['visible'] == 0) {
                    $bgcolor = 'trow_shaded';
                }

                // Build last post info
                $lastpostdate = $this->time->formatDate('relative', $thread['lastpost']);
                $lastposter = $thread['lastposter'];
                $lastposteruid = $thread['lastposteruid'];

                // Don't link to guest's profiles (they have no profile).
                if ($lastposteruid == 0) {
                    $lastposterlink = $lastposter;
                } else {
                    $lastposterlink = $this->user->build_profile_link($lastposter, $lastposteruid);
                }

                $thread['replies'] = $this->parser->formatNumber($thread['replies']);
                $thread['views'] = $this->parser->formatNumber($thread['views']);

                // What kind of notification type do we have here?
                switch ($thread['notification']) {
                    case '2': // PM
                        $notification_type = $this->lang->pm_notification;
                        break;
                    case '1': // Email
                        $notification_type = $this->lang->email_notification;
                        break;
                    default: // No notification
                        $notification_type = $this->lang->no_notification;
                }
                $threads[] = [
                    'bgcolor' => $bgcolor,
                    'folder' => $folder,
                    'folder_label' => $folder_label,
                    'icon' => $icon,
                    'thread' => $thread,
                    'gotounread' => $gotounread,
                    'new_class' => $new_class,
                    'notification_type' => $notification_type,
                    'lastpostdate' => $lastpostdate,
                    'lastposterlink' => $lastposterlink
                ];
            }
        }
        $this->view->offsetSet('threads', $threads);

        $this->plugins->runHooks('usercp_subscriptions_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/subscriptions.html.twig');
    }

    public function doSubscriptions(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->plugins->runHooks('usercp_do_subscriptions_start');

        if (!isset($this->bb->input['check']) || !is_array($this->bb->input['check'])) {
            $this->bb->error($this->lang->no_subscriptions_selected);
        }

        // Clean input - only accept integers thanks!
        $tids = implode(',', array_map('intval', $this->bb->getInput('check', [0])));

        // Deleting these subscriptions?
        if ($this->bb->getInput('do', '') === 'delete') {
            $this->db->delete_query('threadsubscriptions', "tid IN ($tids) AND uid='{$this->user->uid}'");
        } // Changing subscription type
        else {
            if ($this->bb->getInput('do', '') === 'no_notification') {
                $new_notification = 0;
            } elseif ($this->bb->getInput('do', '') === 'email_notification') {
                $new_notification = 1;
            } elseif ($this->bb->getInput('do', '') === 'pm_notification') {
                $new_notification = 2;
            }

            // Update
            $update_array = ['notification' => $new_notification];
            $this->db->update_query(
                'threadsubscriptions',
                $update_array,
                "tid IN ($tids) AND uid='{$this->user->uid}'"
            );
        }

        // Done, redirect
        $this->bb->redirect(
            $this->bb->settings['bburl'] . '/usercp/subscriptions',
            $this->lang->redirect_subscriptions_updated
        );
    }
}
