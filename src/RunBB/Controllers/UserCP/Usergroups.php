<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\UserCP;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Usergroups extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        $this->bb->add_breadcrumb($this->lang->ucp_nav_usergroups);
        $this->plugins->runHooks('usercp_usergroups_start');
        $ingroups = ',' . $this->user->usergroup . ',' . $this->user->additionalgroups . ',' .
            $this->user->displaygroup . ',';

        $usergroups = $this->cache->read('usergroups');

        // Changing our display group
        if ($this->bb->getInput('displaygroup', 0)) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            if (my_strpos($ingroups, ',' . $this->bb->input['displaygroup'] . ',') === false) {
                $this->bb->error($this->lang->not_member_of_group);
            }

            $dispgroup = $usergroups[$this->bb->getInput('displaygroup', 0)];
            if ($dispgroup['candisplaygroup'] != 1) {
                $this->bb->error($this->lang->cannot_set_displaygroup);
            }
            $this->db->update_query(
                'users',
                ['displaygroup' => $this->bb->getInput('displaygroup', 0)],
                "uid='" . $this->user->uid . "'"
            );
            $this->cache->update_moderators();
            $this->plugins->runHooks('usercp_usergroups_change_displaygroup');
            $this->bb->redirect('usercp.php?action=usergroups', $this->lang->display_group_changed);
        }

        // Leaving a group
        if ($this->bb->getInput('leavegroup', 0)) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->input['my_post_key']);

            if (my_strpos($ingroups, ',' . $this->bb->getInput('leavegroup', 0) . ',') === false) {
                $this->bb->error($this->lang->not_member_of_group);
            }
            if ($this->user->usergroup == $this->bb->getInput('leavegroup', 0)) {
                $this->bb->error($this->lang->cannot_leave_primary_group);
            }

            $usergroup = $usergroups[$this->bb->getInput('leavegroup', 0)];
            if ($usergroup['type'] != 4 && $usergroup['type'] != 3 && $usergroup['type'] != 5) {
                $this->bb->error($this->lang->cannot_leave_group);
            }
            $this->group->leave_usergroup($this->user->uid, $this->bb->getInput('leavegroup', 0));
            $this->plugins->runHooks('usercp_usergroups_leave_group');
            $this->bb->redirect('usercp.php?action=usergroups', $this->lang->left_group);
        }

        $groupleaders = [];

        // List of usergroup leaders
        $query = $this->db->query('
		SELECT g.*, u.username, u.displaygroup, u.usergroup, u.email, u.language
		FROM ' . TABLE_PREFIX . 'groupleaders g
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=g.uid)
		ORDER BY u.username ASC
	');
        while ($leader = $this->db->fetch_array($query)) {
            $groupleaders[$leader['gid']][$leader['uid']] = $leader;
        }

        // Joining a group
        if ($this->bb->getInput('joingroup', 0)) {
            // Verify incoming POST request
            $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

            $usergroup = $usergroups[$this->bb->getInput('joingroup', 0)];

            if ($usergroup['type'] == 5) {
                error($this->lang->cannot_join_invite_group);
            }

            if (($usergroup['type'] != 4 && $usergroup['type'] != 3) || !$usergroup['gid']) {
                error($this->lang->cannot_join_group);
            }

            if (my_strpos($ingroups, ',' . $this->bb->getInput('joingroup', 0) . ',') !== false) {
                error($this->lang->already_member_of_group);
            }

            $query = $this->db->simple_select(
                'joinrequests',
                '*',
                "uid='" . $this->user->uid . "' AND gid='" . $this->bb->getInput('joingroup', 0) . "'"
            );
            $joinrequest = $this->db->fetch_array($query);
            if ($joinrequest['rid']) {
                error($this->lang->already_sent_join_request);
            }
            if ($this->bb->getInput('do') == 'joingroup' && $usergroup['type'] == 4) {
                $now = TIME_NOW;
                $joinrequest = [
                    'uid' => $this->user->uid,
                    'gid' => $this->bb->getInput('joingroup', 0),
                    'reason' => $this->db->escape_string($this->bb->getInput('reason', '', true)),
                    'dateline' => TIME_NOW
                ];

                $this->db->insert_query('joinrequests', $joinrequest);

                if (array_key_exists($usergroup['gid'], $groupleaders)) {
                    foreach ($groupleaders[$usergroup['gid']] as $leader) {
                        // Load language
                        $this->lang->set_language($leader['language']);
                        $this->lang->load('messages');

                        $subject = $this->lang->sprintf(
                            $this->lang->emailsubject_newjoinrequest,
                            $this->bb->settings['bbname']
                        );
                        $message = $this->lang->sprintf(
                            $this->lang->email_groupleader_joinrequest,
                            $leader['username'],
                            $this->user->username,
                            $usergroup['title'],
                            $this->bb->settings['bbname'],
                            $this->bb->getInput('reason', '', true),
                            $this->bb->settings['bburl'],
                            $leader['gid']
                        );
                        $this->mail->send($leader['email'], $subject, $message);
                    }
                }

                // Load language
                $this->lang->set_language($this->user->language);
                $this->lang->load('messages');

                $this->plugins->runHooks('usercp_usergroups_join_group_request');
                redirect('usercp.php?action=usergroups', $this->lang->group_join_requestsent);
                exit;
            } elseif ($usergroup['type'] == 4) {
                $joingroup = $this->bb->getInput('joingroup', 0);
                eval("\$joinpage = \"" . $this->templates->get('usercp_usergroups_joingroup') . "\";");
                output_page($joinpage);
                exit;
            } else {
                $this->group->join_usergroup($this->user->uid, $this->bb->getInput('joingroup', 0));
                $this->plugins->runHooks('usercp_usergroups_join_group');
                redirect('usercp.php?action=usergroups', $this->lang->joined_group);
            }
        }

        // Accepting invitation
        if ($this->bb->getInput('acceptinvite', 0)) {
            // Verify incoming POST request
            verify_post_check($this->bb->getInput('my_post_key', ''));

            $usergroup = $usergroups[$this->bb->getInput('acceptinvite', 0)];

            if (my_strpos($ingroups, ',' . $this->bb->getInput('acceptinvite', 0) . ',') !== false) {
                error($this->lang->already_accepted_invite);
            }

            $query = $this->db->simple_select(
                'joinrequests',
                '*',
                "uid='" . $this->user->uid . "' AND gid='" . $this->bb->getInput('acceptinvite', 0) . "' AND invite='1'"
            );
            $joinrequest = $this->db->fetch_array($query);
            if ($joinrequest['rid']) {
                $this->group->join_usergroup($this->user->uid, $this->bb->getInput('acceptinvite', 0));
                $this->db->delete_query(
                    'joinrequests',
                    "uid='{$this->user->uid}' AND gid='" . $this->bb->getInput('acceptinvite', 0) . "'"
                );
                $this->plugins->runHooks('usercp_usergroups_accept_invite');
                redirect('usercp.php?action=usergroups', $this->lang->joined_group);
            } else {
                error($this->lang->no_pending_invitation);
            }
        }
        // Show listing of various group related things

        // List of groups this user is a leader of
        $groupsledlist = [];
        switch ($this->db->type) {
            case 'pgsql':
            case 'sqlite':
                $query = $this->db->query('
				SELECT g.title, g.gid, g.type, COUNT(DISTINCT u.uid) AS users,
				COUNT(DISTINCT j.rid) AS joinrequests, l.canmanagerequests, l.canmanagemembers, l.caninvitemembers
				FROM ' . TABLE_PREFIX . 'groupleaders l
				LEFT JOIN ' . TABLE_PREFIX . 'usergroups g ON(g.gid=l.gid)
				LEFT JOIN ' . TABLE_PREFIX . "users u ON(((','|| u.additionalgroups|| ',' LIKE '%,'|| g.gid|| ',%')
				OR u.usergroup = g.gid))
				LEFT JOIN " . TABLE_PREFIX . "joinrequests j ON(j.gid=g.gid AND j.uid != 0)
				WHERE l.uid='" . $this->user->uid . "'
				GROUP BY g.gid, g.title, g.type, l.canmanagerequests, l.canmanagemembers, l.caninvitemembers
			");
                break;
            default:
                $query = $this->db->query('
				SELECT g.title, g.gid, g.type, COUNT(DISTINCT u.uid) AS users, COUNT(DISTINCT j.rid) AS joinrequests,
				l.canmanagerequests, l.canmanagemembers, l.caninvitemembers
				FROM ' . TABLE_PREFIX . 'groupleaders l
				LEFT JOIN ' . TABLE_PREFIX . 'usergroups g ON(g.gid=l.gid)
				LEFT JOIN ' . TABLE_PREFIX . "users u ON(((CONCAT(',', u.additionalgroups, ',')
				LIKE CONCAT('%,', g.gid, ',%')) OR u.usergroup = g.gid))
				LEFT JOIN " . TABLE_PREFIX . "joinrequests j ON(j.gid=g.gid AND j.uid != 0)
				WHERE l.uid='" . $this->user->uid . "'
				GROUP BY g.gid, g.title, g.type, l.canmanagerequests, l.canmanagemembers, l.caninvitemembers
			");
        }

        while ($usergroup = $this->db->fetch_array($query)) {
            $moderaterequestslink = false;
            $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);
            if ($usergroup['type'] != 4) {
                $usergroup['joinrequests'] = '--';
            }
            if ($usergroup['joinrequests'] > 0 && $usergroup['canmanagerequests'] == 1) {
                $moderaterequestslink = true;
            }
            $groupleader[$usergroup['gid']] = 1;
            //$trow = alt_trow();
            $groupsledlist[] = [
                'trow' => alt_trow(),
                'title' => $usergroup['title'],
                'users' => $usergroup['users'],
                'gid' => $usergroup['gid'],
                'joinrequests' => $usergroup['joinrequests'],
                'moderaterequestslink' => $moderaterequestslink
            ];
        }
        $this->view->offsetSet('groupsledlist', $groupsledlist);

        // Fetch the list of groups the member is in
        // Do the primary group first
        $usergroup = $usergroups[$this->user->usergroup];
        $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);
        $usergroup['usertitle'] = htmlspecialchars_uni($usergroup['usertitle']);
        $usergroup['description'] = htmlspecialchars_uni($usergroup['description']);

        if ($usergroup['candisplaygroup'] == 1 && $usergroup['gid'] == $this->user->displaygroup) {
            $displaycode = '(' . $this->lang->display_group . ')';
        } elseif ($usergroup['candisplaygroup'] == 1) {
            $displaycode = " (<a href=\"{$this->bb->settings['bburl']}/usercp/usergroups?
            displaygroup={$usergroup['gid']}&my_post_key={$this->bb->post_code}\">
            {$this->lang->set_as_display_group}</a>)";
        } else {
            $displaycode = '';
        }
        $this->view->offsetSet('usergroup', $usergroup);
        $this->view->offsetSet('trow', alt_trow());
        $this->view->offsetSet('displaycode', $displaycode);

        $memberoflist = [];
        if ($this->user->additionalgroups) {
            $query = $this->db->simple_select(
                'usergroups',
                '*',
                'gid IN (' . $this->user->additionalgroups . ") AND gid !='" . $this->user->usergroup . "'",
                ['order_by' => 'title']
            );
            while ($usergroup = $this->db->fetch_array($query)) {
                if (isset($groupleader[$usergroup['gid']])) {
                    $leavelink = $this->lang->usergroup_leave_leader;
                } elseif ($usergroup['type'] != 4 && $usergroup['type'] != 3 && $usergroup['type'] != 5) {
                    $leavelink = $this->lang->usergroup_cannot_leave;
                } else {
                    $leavelink = "<a href=\"{$this->bb->settings['bburl']}/usercp/usergroups?
                    leavegroup={$usergroup['gid']}&amp;my_post_key={$this->bb->post_code}\">
                    {$this->lang->usergroup_leave}</a>";
                }

                $description = '';
                $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);
                $usergroup['usertitle'] = htmlspecialchars_uni($usergroup['usertitle']);
                if ($usergroup['description']) {
                    $usergroup['description'] = htmlspecialchars_uni($usergroup['description']);
                    $description = "<br /><small>{$usergroup['description']}</small>";
                }
                if ($usergroup['candisplaygroup'] == 1 && $usergroup['gid'] == $this->user->displaygroup) {
                    $displaycode = "({$this->lang->display_group})";
                } elseif ($usergroup['candisplaygroup'] == 1) {
                    $displaycode = "(<a href=\"{$this->bb->settings['bburl']}usercp/usergroups?
                    displaygroup={$usergroup['gid']}&my_post_key={$this->bb->post_code}\">
                    {$this->lang->set_as_display_group}</a>)";
                } else {
                    $displaycode = '';
                }
                $memberoflist[] = [
                    'trow' => alt_trow(),
                    'title' => $usergroup['title'],
                    'displaycode' => $displaycode,
                    'description' => $description,
                    'usertitle' => $usergroup['usertitle'],
                    'leavelink' => $leavelink
                ];
            }
        }
        $this->view->offsetSet('memberoflist', $memberoflist);

        // List of groups this user has applied for but has not been accepted in to
        $query = $this->db->simple_select('joinrequests', '*', "uid='" . $this->user->uid . "'");
        while ($request = $this->db->fetch_array($query)) {
            $appliedjoin[$request['gid']] = $request['dateline'];
        }

        // Fetch list of groups the member can join
        $existinggroups = $this->user->usergroup;
        if ($this->user->additionalgroups) {
            $existinggroups .= ',' . $this->user->additionalgroups;
        }

        $joinablegrouplist = [];
        $query = $this->db->simple_select(
            'usergroups',
            '*',
            "(type='3' OR type='4' OR type='5') AND gid NOT IN ($existinggroups)",
            ['order_by' => 'title']
        );
        while ($usergroup = $this->db->fetch_array($query)) {
            $description = '';
            $usergroup['title'] = htmlspecialchars_uni($usergroup['title']);
            if ($usergroup['description']) {
                $usergroup['description'] = htmlspecialchars_uni($usergroup['description']);
                $description = "<br /><span class=\"smalltext\">{$usergroup['description']}</span>";
            }

            // Moderating join requests?
            if ($usergroup['type'] == 4) {
                $conditions = $this->lang->usergroup_joins_moderated;
            } elseif ($usergroup['type'] == 5) {
                $conditions = $this->lang->usergroup_joins_invite;
            } else {
                $conditions = $this->lang->usergroup_joins_anyone;
            }

            if (isset($appliedjoin[$usergroup['gid']]) && $usergroup['type'] != 5) {
                $applydate = $this->time->formatDate('relative', $appliedjoin[$usergroup['gid']]);
                $joinlink = $this->lang->sprintf($this->lang->join_group_applied, $applydate);
            } elseif (isset($appliedjoin[$usergroup['gid']]) && $usergroup['type'] == 5) {
                $joinlink = $this->lang->sprintf(
                    $this->lang->pending_invitation,
                    $usergroup['gid'],
                    $this->bb->post_code
                );
            } elseif ($usergroup['type'] == 5) {
                $joinlink = '--';
            } else {
                $joinlink = "<a href=\"{$this->bb->settings['bburl']}/usercp/usergroups?
                joingroup={$usergroup['gid']}&amp;my_post_key={$this->bb->post_code}\">{$this->lang->join_group}</a>";
            }

            $usergroupleaders = '';
            if (!empty($groupleaders[$usergroup['gid']])) {
                $comma = '';
                $usergroupleaders = '';
                foreach ($groupleaders[$usergroup['gid']] as $leader) {
                    $leader['username'] = $this->user->format_name(
                        $leader['username'],
                        $leader['usergroup'],
                        $leader['displaygroup']
                    );
                    $usergroupleaders .= $comma . $this->user->build_profile_link($leader['username'], $leader['uid']);
                    $comma = $this->lang->comma;
                }
                $usergroupleaders = $this->lang->usergroup_leaders . ' ' . $usergroupleaders;
            }

            if (my_strpos($usergroupleaders, $this->user->username) === false) {
                $joinablegrouplist[] = [
                    'trow' => alt_trow(),
                    'title' => $usergroup['title'],
                    'description' => $description,
                    'conditions' => $conditions,
                    'usergroupleaders' => $usergroupleaders,
                    'joinlink' => $joinlink
                ];
            }
        }
        $this->view->offsetSet('joinablegrouplist', $joinablegrouplist);

        $this->plugins->runHooks('usercp_usergroups_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/UserCP/usergroups.html.twig');
    }
}
