<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Warnings;

use RunCMF\Core\AbstractController;

class Common extends AbstractController
{

    protected function init()
    {
        $this->lang->load('warnings');
        $this->lang->load('datahandler_warnings');

        if ($this->bb->settings['enablewarningsystem'] == 0) {
            $this->bb->error($this->lang->error_warning_system_disabled);
            return 'exit';
        }
        // Expire old warnings
        $this->warningshandler = new \RunBB\Handlers\DataHandlers\WarningsHandler($this->bb, 'update');
        $this->warningshandler->expire_warnings();
        $this->plugins->runHooks('warnings_start');
    }
}
