<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Warnings;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Index extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canwarnusers'] != 1) {
            return $this->bb->error_no_permission();
        }

        $user = $this->user->get_user($this->bb->getInput('uid', 0));
        if (!$user->uid) {
            $this->bb->error($this->lang->error_invalid_user);
        }

        $group_permissions = $this->user->user_permissions($user->uid);
        if ($group_permissions['canreceivewarnings'] != 1) {
            $this->bb->error($this->lang->error_cant_warn_group);
        }

        $this->lang->nav_profile = $this->lang->sprintf($this->lang->nav_profile, $user->username);
        $this->bb->add_breadcrumb($this->lang->nav_profile, get_profile_link($user->uid));
        $this->bb->add_breadcrumb($this->lang->nav_warning_log);

        if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
            $this->bb->settings['postsperpage'] = 20;
        }

        // Figure out if we need to display multiple pages.
        $perpage = $this->bb->settings['postsperpage'];
        $page = $this->bb->getInput('page', 0);

        $query = $this->db->simple_select('warnings', 'COUNT(wid) AS warning_count', "uid='{$user->uid}'");
        $warning_count = $this->db->fetch_field($query, 'warning_count');

        $pages = ceil($warning_count / $perpage);

        if ($page > $pages || $page <= 0) {
            $page = 1;
        }
        if ($page) {
            $start = ($page - 1) * $perpage;
        } else {
            $start = 0;
            $page = 1;
        }

        $multipage = $this->pagination->multipage($warning_count, $perpage, $page, $this->bb->settings['bburl'] . '/warnings?uid=' . $user->uid);
        $this->view->offsetSet('multipage', $multipage);

        if ($this->bb->settings['maxwarningpoints'] < 1) {
            $this->bb->settings['maxwarningpoints'] = 10;
        }

        $warning_level = round($user->warningpoints / $this->bb->settings['maxwarningpoints'] * 100);
        if ($warning_level > 100) {
            $warning_level = 100;
        }

        if ($user->warningpoints > $this->bb->settings['maxwarningpoints']) {
            $user->warningpoints = $this->bb->settings['maxwarningpoints'];
        }

        if ($warning_level > 0) {
            $this->lang->current_warning_level = $this->lang->sprintf(
                $this->lang->current_warning_level,
                $warning_level,
                $user->warningpoints,
                $this->bb->settings['maxwarningpoints']
            );
        } else {
            $this->lang->current_warning_level = '';
        }

        // Fetch the actual warnings
        $query = $this->db->query('
		SELECT w.*, t.title AS type_title, u.username, p.subject AS post_subject
		FROM ' . TABLE_PREFIX . 'warnings w
		LEFT JOIN ' . TABLE_PREFIX . 'warningtypes t ON (t.tid=w.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=w.issuedby)
		LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=w.pid)
		WHERE w.uid='{$user->uid}'
		ORDER BY w.expired ASC, w.dateline DESC
		LIMIT {$start}, {$perpage}
	");
        $warnings = [];
        while ($warning = $this->db->fetch_array($query)) {
            $expired = false;
            if (!isset($last_expired) || $warning['expired'] != $last_expired) {
                if ($warning['expired'] == 0) {
                    //ev al('\$warnings .= \''.$this->templates->get('warnings_active_header').'\';');
                } else {
                    $expired = true;
                    //ev al('\$warnings .= \''.$this->templates->get('warnings_expired_header').'\';');
                }
            }
            $last_expired = $warning['expired'];

            $post_link = '';
            if ($warning['post_subject']) {
                $warning['post_subject'] = $this->parser->parse_badwords($warning['post_subject']);
                $warning['post_subject'] = htmlspecialchars_uni($warning['post_subject']);
                $post_link = "<br /><small>{$this->lang->warning_for_post} <a href=\"" . get_post_link($warning['pid']) . "#pid{$warning['pid']}\">{$warning['post_subject']}</a></small>";
            }
            $issuedby = $this->user->build_profile_link($warning['username'], $warning['issuedby']);
            $date_issued = $this->time->formatDate('relative', $warning['dateline']);
            if ($warning['type_title']) {
                $warning_type = $warning['type_title'];
            } else {
                $warning_type = $warning['title'];
            }
            $warning_type = htmlspecialchars_uni($warning_type);
            if ($warning['points'] > 0) {
                $warning['points'] = "+{$warning['points']}";
            }
            $points = $this->lang->sprintf($this->lang->warning_points, $warning['points']);
            if ($warning['expired'] != 1) {
                if ($warning['expires'] == 0) {
                    $expires = $this->lang->never;
                } else {
                    $expires = $this->time->formatDate($this->bb->settings['dateformat'], $warning['expires']) . ', ' .
                        $this->time->formatDate($this->bb->settings['timeformat'], $warning['expires']);
                }
            } else {
                if ($warning['daterevoked']) {
                    $expires = $this->lang->warning_revoked;
                } elseif ($warning['expires']) {
                    $expires = $this->lang->already_expired;
                }
            }
            $this->plugins->runHooks('warnings_warning');
            $warnings[] = [
                'expired' => $expired,
                'alt_bg' => alt_trow(),
                'warning_type' => $warning_type,
                'points' => $points,
                'post_link' => $post_link,
                'date_issued' => $date_issued,
                'expires' => $expires,
                'issuedby' => $issuedby,
                'wid' => $warning['wid']
            ];
            //ev al('\$warnings .= \''.$this->templates->get('warnings_warning').'\';');
        }
        $this->view->offsetSet('warnings', $warnings);

        $this->plugins->runHooks('warnings_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Warnings/index.html.twig');
    }
}
