<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Warnings;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Revoke extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canwarnusers'] != 1) {
            return $this->bb->error_no_permission();
        }

        $warning = $this->warningshandler->get($this->bb->input['wid']);

        if (!$warning) {
            $this->bb->error($this->lang->error_invalid_warning);
        } elseif ($warning['daterevoked']) {
            $this->bb->error($this->lang->warning_already_revoked);
        }

        $user = $this->user->get_user($warning['uid']);

        $group_permissions = $this->user->user_permissions($user->uid);
        if ($group_permissions['canreceivewarnings'] != 1) {
            $this->bb->error($this->lang->error_cant_warn_group);
        }

        $this->plugins->runHooks('warnings_do_revoke_start');

        if (!trim($this->bb->getInput('reason', '', true))) {
            $this->warn_errors[] = $this->lang->no_revoke_reason;
            $this->warn_errors = $this->bb->inline_error($this->warn_errors);
            //$this->bb->input['action'] = 'view';
      //FIXME do return to index show error
        } else {
            $warning_data = [
            'wid' => $warning['wid'],
            'reason' => $this->bb->getInput('reason', '', true),
            'expired' => $warning['expired'],
            'uid' => $warning['uid'],
            'points' => $warning['points']
            ];
            $this->warningshandler->set_data($warning_data);
            $this->warningshandler->update_warning();
            $this->bb->redirect($this->bb->settings['bburl'].'/warnings/view?wid='.$warning['wid'], $this->lang->redirect_warning_revoked);
        }
    }
}
