<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Warnings;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class View extends Common
{
    public function index(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        if ($this->bb->usergroup['canwarnusers'] != 1) {
            return $this->bb->error_no_permission();
        }

        $query = $this->db->query('
		SELECT w.*, t.title AS type_title, u.username, p.subject AS post_subject
		FROM ' . TABLE_PREFIX . 'warnings w
		LEFT JOIN ' . TABLE_PREFIX . 'warningtypes t ON (t.tid=w.tid)
		LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=w.issuedby)
		LEFT JOIN ' . TABLE_PREFIX . "posts p ON (p.pid=w.pid)
		WHERE w.wid='" . $this->bb->getInput('wid', 0) . "'
	");
        $warning = $this->db->fetch_array($query);

        if (!$warning) {
            $this->bb->error($this->lang->error_invalid_warning);
        }

        $user = $this->user->get_user((int)$warning['uid']);
        if (!$user) {
            $user->username = $this->lang->guest;
        }

        $group_permissions = $this->user->user_permissions($user->uid);
        if ($group_permissions['canreceivewarnings'] != 1) {
            $this->bb->error($this->lang->error_cant_warn_group);
        }

        $this->plugins->runHooks('warnings_view_start');

        $this->lang->nav_profile = $this->lang->sprintf($this->lang->nav_profile, $user->username);
        if ($user->uid) {
            $this->bb->add_breadcrumb($this->lang->nav_profile, get_profile_link($user->uid));
            $this->bb->add_breadcrumb($this->lang->nav_warning_log, $this->bb->settings['bburl'] . '/warnings?uid=' . $user->uid);
        } else {
            $this->bb->add_breadcrumb($this->lang->nav_profile);
            $this->bb->add_breadcrumb($this->lang->nav_warning_log);
        }
        $this->bb->add_breadcrumb($this->lang->nav_view_warning);

        $user_link = $this->user->build_profile_link($user->username, $user->uid);

        $post_link = '';
        $this->bb->warningPost = false;
        if ($warning['post_subject']) {
            $this->bb->warningPost = true;
            $warning['post_subject'] = $this->parser->parse_badwords($warning['post_subject']);
            $warning['post_subject'] = htmlspecialchars_uni($warning['post_subject']);
            $post_link = get_post_link($warning['pid']) . "#pid{$warning['pid']}";
            $this->view->offsetSet('warn', [
                'user_link' => $user_link,
                'post_link' => $post_link,
                'post_subject' => $warning['post_subject']
            ]);
        } else {
            $this->view->offsetSet('warn', [
                'user_link' => $user_link
            ]);
        }

        $issuedby = $this->user->build_profile_link($warning['username'], $warning['issuedby']);
        $this->view->offsetSet('issuedby', $issuedby);

        $notes = nl2br(htmlspecialchars_uni($warning['notes']));
        $this->view->offsetSet('notes', $notes);

        $date_issued = $this->time->formatDate('relative', $warning['dateline']);
        $this->view->offsetSet('date_issued', $date_issued);
        if ($warning['type_title']) {
            $warning_type = $warning['type_title'];
        } else {
            $warning_type = $warning['title'];
        }
        $this->view->offsetSet('warning_type', htmlspecialchars_uni($warning_type));

        if ($warning['points'] > 0) {
            $warning['points'] = "+{$warning['points']}";
        }

        $revoked_date = '';
        $points = $this->lang->sprintf($this->lang->warning_points, $warning['points']);
        $this->view->offsetSet('points', $points);
        if ($warning['expired'] != 1) {
            if ($warning['expires'] == 0) {
                $expires = $this->lang->never;
            } else {
                $expires = $this->time->formatDate($this->bb->settings['dateformat'], $warning['expires']) . ', ' .
                    $this->time->formatDate($this->bb->settings['timeformat'], $warning['expires']);
            }
            $status = $this->lang->warning_active;
        } else {
            if ($warning['daterevoked']) {
                $expires = $status = $this->lang->warning_revoked;
            } elseif ($warning['expires']) {
                $revoked_date = '(' . $this->time->formatDate($this->bb->settings['dateformat'], $warning['expires']) . ', ' .
                    $this->time->formatDate($this->bb->settings['timeformat'], $warning['expires']) . ')';
                $expires = $status = $this->lang->already_expired;
            }
        }
        $this->view->offsetSet('status', $status);
        $this->view->offsetSet('expires', $expires);
        $this->view->offsetSet('revoked_date', $revoked_date);

        $this->bb->isRevoked = false;
        if (!$warning['daterevoked']) {
            if (!isset($this->warn_errors)) {//FIXME check warn_error from revoke / warn
                $this->warn_errors = '';
            }
            $this->view->offsetSet('warn_errors', $this->warn_errors);
            $this->view->offsetSet('wid', $warning['wid']);
        } else {
            $this->bb->isRevoked = true;
            $revoked_user = $this->user->get_user($warning['revokedby']);
            if (!$revoked_user->username) {
                $revoked_user->username = $this->lang->guest;
            }
            $this->view->offsetSet('warning', [
                'revoked_by' => $this->user->build_profile_link($revoked_user->username, $revoked_user->uid),
                'date_revoked' => $this->time->formatDate('relative', $warning['daterevoked']),
                'revoke_reason' => nl2br(htmlspecialchars_uni($warning['revokereason']))
            ]);
        }
        $this->plugins->runHooks('warnings_view_end');

        $this->bb->output_page();
        $this->view->render($response, '@forum/Warnings/view.html.twig');
    }
}
