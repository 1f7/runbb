<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers\Warnings;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunBB\Core\BB;

class Warn extends Common
{
    public function index(Request $request, Response $response, $noinit = false)
    {
        if (!$noinit) {
            if ($this->init() === 'exit') {
                return;
            }
        }

        if ($this->bb->usergroup['canwarnusers'] != 1) {
            return $this->bb->error_no_permission();
        }

        // Check we haven't exceeded the maximum number of warnings per day
        if ($this->bb->usergroup['maxwarningsday'] != 0) {
            $timecut = TIME_NOW-60*60*24;
            $query = $this->db->simple_select('warnings', 'COUNT(wid) AS given_today', "issuedby='{$this->user->uid}' AND dateline>'$timecut'");
            $given_today = $this->db->fetch_field($query, 'given_today');
            if ($given_today >= $this->bb->usergroup['maxwarningsday']) {
                $this->bb->error($this->lang->sprintf(
                    $this->lang->warnings_reached_max_warnings_day,
                    $this->bb->usergroup['maxwarningsday']
                ));
            }
        }

        $user = $this->user->get_user($this->bb->getInput('uid', 0));
        if (!$user) {
            $this->bb->error($this->lang->error_invalid_user);
        }

        if ($user->uid == $this->user->uid) {
            $this->bb->error($this->lang->warnings_error_cannot_warn_self);
        }

        if ($user->warningpoints >= $this->bb->settings['maxwarningpoints']) {
            $this->bb->error($this->lang->warnings_error_user_reached_max_warning);
        }

        $group_permissions = $this->user->user_permissions($user->uid);

        if ($group_permissions['canreceivewarnings'] != 1) {
            $this->bb->error($this->lang->error_cant_warn_group);
        }

        if (!$this->modcp->modcp_can_manage_user($user->uid)) {
            $this->bb->error($this->lang->error_cant_warn_user);
        }

        $post = $existing_warnings = '';
        // Giving a warning for a specific post
        $this->bb->showWarnings = false;
        if ($this->bb->getInput('pid', 0)) {
            $this->bb->showWarnings = true;
            $post = $this->post->get_post($this->bb->getInput('pid', 0));

            if ($post) {
                $thread = $this->thread->get_thread($post['tid']);
            }

            if (!$post || !$thread) {
                $this->bb->error($this->lang->warnings_error_invalid_post);
            }

            $forum_permissions = $this->forum->forum_permissions($thread['fid']);
            if ($forum_permissions['canview'] != 1) {
                return $this->bb->error_no_permission();
            }

            $post['subject'] = $this->parser->parse_badwords($post['subject']);
            $post['subject'] = htmlspecialchars_uni($post['subject']);
            //$post_link = get_post_link($post['pid']);
            $this->view->offsetSet('post', [
            'pid' => $post['pid'],
            'subject' => $post['subject'],
            'link' => get_post_link($post['pid'])
            ]);
            //ev al("\$post = \"".$this->templates->get('warnings_warn_post')."\";");

            // Fetch any existing warnings issued for this post
            $query = $this->db->query('
			SELECT w.*, t.title AS type_title, u.username
			FROM '.TABLE_PREFIX.'warnings w
			LEFT JOIN '.TABLE_PREFIX.'warningtypes t ON (t.tid=w.tid)
			LEFT JOIN '.TABLE_PREFIX."users u ON (u.uid=w.issuedby)
			WHERE w.pid='".$this->bb->getInput('pid', 0)."'
			ORDER BY w.expired ASC, w.dateline DESC
		");
            $first = true;
            $warnings = [];
            while ($warning = $this->db->fetch_array($query)) {
                  $warn_active = false;
                if ($warning['expired'] != $last_expired || $first) {
                    if ($warning['expired'] == 0) {
                        $warn_active = true;
                        //ev al("\$warnings .= \"".$this->templates->get("warnings_active_header")."\";");
                    }
            //          else
            //          {
            //            //ev al("\$warnings .= \"".$this->templates->get("warnings_expired_header")."\";");
            //          }
                }
                    $last_expired = $warning['expired'];
                    $first = false;

                    $post_link = '';
                    $issuedby = $this->user->build_profile_link($warning['username'], $warning['issuedby']);
                    $date_issued = $this->time->formatDate('relative', $warning['dateline']);
                if ($warning['type_title']) {
                    $warning_type = $warning['type_title'];
                } else {
                    $warning_type = $warning['title'];
                }
                    $warning_type = htmlspecialchars_uni($warning_type);
                if ($warning['points'] > 0) {
                    $warning['points'] = "+{$warning['points']}";
                }
                    $points = $this->lang->sprintf($this->lang->warning_points, $warning['points']);
                if ($warning['expired'] != 1) {
                    if ($warning['expires'] == 0) {
                        $expires = $this->lang->never;
                    } else {
                        $expires = $this->time->formatDate($this->bb->settings['dateformat'], $warning['expires']) . ', ' .
                        $this->time->formatDate($this->bb->settings['timeformat'], $warning['expires']);
                    }
                } else {
                    if ($warning['daterevoked']) {
                        $expires = $this->lang->warning_revoked;
                    } elseif ($warning['expires']) {
                        $expires = $this->lang->already_expired;
                    }
                }
                    //$alt_bg = alt_trow();
                    $this->plugins->runHooks('warnings_warning');
                    $warnings[]=[
                    'warn_active' => $warn_active,
                    'alt_bg' => alt_trow(),
                    'warning_type' => $warning_type,
                    'points' => $points,
                    'post_link' => $post_link,
                    'date_issued' => $date_issued,
                    'expires' => $expires,
                    'issuedby' => $issuedby,
                    'wid' => $warning['wid']
                    ];
            /*
            <tr>
              <td class="trow_sep" colspan="5">
                {% if row.warn_active == true %}
                {$lang->active_warnings}
                {% else %}
                {$lang->expired_warnings}
                {% endif %}
              </td>
            </tr>
            <tr>
              <td class="{{ row.alt_bg }}">{{ row.warning_type }} {{ row.points }}{{ row.post_link|raw }}</td>
              <td class="{{ row.alt_bg }}" style="text-align: center;">{{ row.date_issued }}</td>
              <td class="{{ row.alt_bg }}" style="text-align: center;">{{ row.expires }}</td>
              <td class="{{ row.alt_bg }}" style="text-align: center;">{{ row.issuedby }}</td>
              <td class="{{ row.alt_bg }}" style="text-align: center;"><a href="{{ bburl }}/warnings/view?wid={{ row.wid }}">{{ getVar('view', 'lang') }}</a></td>
            </tr>
             */
                    //ev al("\$warnings .= \"".$this->templates->get("warnings_warning")."\";");
            }
            $this->view->offsetSet('warnings', $warnings);
      //      if($warnings)
      //      {
      //        //ev al("\$existing_warnings = \"".$this->templates->get("warnings_warn_existing")."\";");
      //      }
        }

        $this->plugins->runHooks('warnings_warn_start');

        $type_checked = ['custom' => ''];
        $expires_period = ['hours' => '', 'days' => '', 'weeks' => '', 'months' => '', 'never' => ''];
        $send_pm_checked = '';

        // Coming here from failed do_warn?
        if (!empty($this->warn_errors)) {
            $notes = htmlspecialchars_uni($this->bb->getInput('notes', '', true));
            if ($this->bb->getInput('type', 0)) {
                $type_checked[$this->bb->getInput('type', 0)] = 'checked="checked"';
            }
            $pm_subject = htmlspecialchars_uni($this->bb->getInput('pm_subject', '', true));
            $message = htmlspecialchars_uni($this->bb->getInput('pm_message', '', true));
            if (!empty($this->bb->input['send_pm'])) {
                $send_pm_checked = 'checked="checked"';
            }
            $custom_reason = htmlspecialchars_uni($this->bb->getInput('custom_reason', '', true));
            $custom_points = $this->bb->getInput('custom_points', 0);
            $expires = $this->bb->getInput('expires', 0);
            if ($this->bb->getInput('expires_period', 0)) {
                $expires_period[$this->bb->getInput('expires_period', 0)] = 'selected="selected"';
            }
        } else {
            $notes = $custom_reason = $custom_points = $expires = '';
            $expires = 1;
            $custom_points = 2;
            $pm_subject = $this->lang->warning_pm_subject;
            $message = $this->lang->sprintf($this->lang->warning_pm_message, $user->username, $this->bb->settings['bbname']);
            $this->warn_errors = '';
        }
        $this->view->offsetSet('expires_period', $expires_period);
        $this->view->offsetSet('warn_errors', $this->warn_errors);
        $this->view->offsetSet('notes', $notes);
        $this->view->offsetSet('custom_reason', $custom_reason);
        $this->view->offsetSet('custom_points', $custom_points);
        $this->view->offsetSet('expires', $expires);
        $this->view->offsetSet('send_pm_checked', $send_pm_checked);
        $this->view->offsetSet('pm_subject', $pm_subject);
        $this->view->offsetSet('message', $message);

        $this->lang->nav_profile = $this->lang->sprintf($this->lang->nav_profile, $user->username);
        $this->bb->add_breadcrumb($this->lang->nav_profile, get_profile_link($user->uid));
        $this->bb->add_breadcrumb($this->lang->nav_add_warning);

        $user_link = $this->user->build_profile_link($user->username, $user->uid);
        $this->view->offsetSet('user_link', $user_link);

        if ($this->bb->settings['maxwarningpoints'] < 1) {
            $this->bb->settings['maxwarningpoints'] = 10;
        }

        if (!is_array($this->bb->groupscache)) {
            $this->bb->groupscache = $this->cache->read('usergroups');
        }

        $current_level = round($user->warningpoints/$this->bb->settings['maxwarningpoints']*100);

        // Fetch warning levels
        $levels = [];
        $wanrHelper = new \RunBB\Helpers\WarnHelper($this->db);
        $query = $this->db->simple_select('warninglevels', '*');
        while ($level = $this->db->fetch_array($query)) {
            $level['action'] = my_unserialize($level['action']);
            switch ($level['action']['type']) {
                case 1:
                    if ($level['action']['length'] > 0) {
                        $ban_length = $wanrHelper->fetch_friendly_expiration($level['action']['length']);
                        $lang_str = 'expiration_'.$ban_length['period'];
                        $period = $this->lang->sprintf($this->lang->result_period, $ban_length['time'], $this->lang->$lang_str);
                    } else {
                        $period = $this->lang->result_period_perm;
                    }
                    $group_name = $this->bb->groupscache[$level['action']['usergroup']]['title'];
                    $level['friendly_action'] = $this->lang->sprintf($this->lang->result_banned, $group_name, $period);
                    break;
                case 2:
                    if ($level['action']['length'] > 0) {
                        $period = $wanrHelper->fetch_friendly_expiration($level['action']['length']);
                        $lang_str = 'expiration_'.$period['period'];
                        $period = $this->lang->sprintf($this->lang->result_period, $period['time'], $this->lang->$lang_str);
                    } else {
                        $period = $this->lang->result_period_perm;
                    }
                    $level['friendly_action'] = $this->lang->sprintf($this->lang->result_suspended, $period);
                    break;
                case 3:
                    if ($level['action']['length'] > 0) {
                        $period = $wanrHelper->fetch_friendly_expiration($level['action']['length']);
                        $lang_str = 'expiration_'.$period['period'];
                        $period = $this->lang->sprintf($this->lang->result_period, $period['time'], $this->lang->$lang_str);
                    } else {
                        $period = $this->lang->result_period_perm;
                    }
                    $level['friendly_action'] = $this->lang->sprintf($this->lang->result_moderated, $period);
                    break;
            }
            $levels[$level['percentage']] = $level;
        }
        krsort($levels);

        $types = [];
        // Fetch all current warning types
        $query = $this->db->simple_select('warningtypes', '*', '', ['order_by' => 'title']);
        while ($type = $this->db->fetch_array($query)) {
            if (!isset($type_checked[$type['tid']])) {
                $type_checked[$type['tid']] = '';
            }
            $checked = $type_checked[$type['tid']];
            $type['title'] = htmlspecialchars_uni($type['title']);
            $new_warning_level = round(($user->warningpoints+$type['points'])/$this->bb->settings['maxwarningpoints']*100);
            if ($new_warning_level > 100) {
                $new_warning_level = 100;
            }
            if ($type['points'] > 0) {
                $type['points'] = "+{$type['points']}";
            }
            $points = $this->lang->sprintf($this->lang->warning_points, $type['points']);

            if (is_array($levels)) {
                foreach ($levels as $level) {
                    if ($new_warning_level >= $level['percentage']) {
                        $new_level = $level;
                        break;
                    }
                }
            }
      //      //$level_diff = $new_warning_level-$current_level;
      //      if(!empty($new_level['friendly_action']))
      //      {
      //        ev al("\$result = \"".$this->templates->get("warnings_warn_type_result")."\";");
      //      }
            $types[]=[
            'type' => $type,
            'checked' => $checked,
            'points' => $points,
            'current_level' => $current_level,
            'level_diff' => $new_warning_level-$current_level,
            'new_warning_level' => $new_warning_level,
            'friendly_action' => $new_level['friendly_action'],
            ];
            //ev al("\$types .= \"".$this->templates->get("warnings_warn_type")."\";");
            unset($new_level);
            //unset($result);
        }
        $this->view->offsetSet('types', $types);

        $this->view->offsetSet('uid', $user->uid);

    //    $custom_warning = '';
        if ($this->bb->settings['allowcustomwarnings'] != 0) {
            if ((empty($types) && empty($this->warn_errors)) || $this->bb->getInput('type', '') === 'custom') {
                $type_checked['custom'] = 'checked="checked"';
            }
            $this->view->offsetSet('type_checked_custom', $type_checked['custom']);
            //ev al("\$custom_warning = \"".$this->templates->get("warnings_warn_custom")."\";");
        }

        $this->bb->showPMNotify = false;
        if ($group_permissions['canusepms']  != 0 && $this->user->receivepms != 0 && $this->bb->settings['enablepms'] != 0) {
            $this->bb->showPMNotify = true;
            $smilieinserter = $codebuttons = '';

            if ($this->bb->settings['bbcodeinserter'] != 0 && $this->bb->settings['pmsallowmycode'] != 0 && $this->user->showcodebuttons != 0) {
                $this->bb->codebuttons = $this->editor->build_mycode_inserter('message', $this->bb->settings['pmsallowsmilies']);
        //        if($this->bb->settings['pmsallowsmilies'] != 0)
        //        {
        //          $this->smilieinserter = $editor->build_clickable_smilies();
        //        }
            }

            $anonymous_pm = '';
            if ($this->bb->settings['allowanonwarningpms'] == 1) {
                $checked = '';
                if ($this->bb->getInput('pm_anonymous', 0)) {
                    $checked = ' checked="checked"';
                }
                $this->view->offsetSet('anon_checked', $checked);
                //ev al('$anonymous_pm = "'.$this->templates->get('warnings_warn_pm_anonymous').'";');
            }

            //ev al("\$pm_notify = \"".$this->templates->get("warnings_warn_pm")."\";");
        }

        $this->plugins->runHooks('warnings_warn_end');

        //ev al('\$warn = \''.$this->templates->get('warnings_warn').'\';');
        $this->bb->output_page();
        $this->view->render($response, '@forum/Warnings/warn.html.twig');
    }

    public function doWarn(Request $request, Response $response)
    {
        if ($this->init() === 'exit') {
            return;
        }

        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->usergroup['canwarnusers'] != 1) {
            return $this->bb->error_no_permission();
        }

        $user = $this->user->get_user($this->bb->getInput('uid', 0));

        if (!$user->uid) {
            $this->bb->error($this->lang->error_invalid_user);
        }

        $group_permissions = $this->user->user_permissions($user->uid);

        if ($group_permissions['canreceivewarnings'] != 1) {
            $this->bb->error($this->lang->error_cant_warn_group);
        }

        if (!$this->modcp->modcp_can_manage_user($user->uid)) {
            $this->bb->error($this->lang->error_cant_warn_user);
        }

        $this->plugins->runHooks('warnings_do_warn_start');

        $warning = [
        'uid' => $this->bb->getInput('uid', 0),
        'notes' => $this->bb->getInput('notes', '', true),
        'type' => $this->bb->getInput('type', ''),
        'custom_reason' => $this->bb->getInput('custom_reason', '', true),
        'custom_points' => $this->bb->getInput('custom_points', 0),
        'expires' => $this->bb->getInput('expires', 0),
        'expires_period' => $this->bb->getInput('expires_period', '')
        ];

        // Is this warning being given for a post?
        if ($this->bb->getInput('pid', 0)) {
            $warning['pid'] = $this->bb->getInput('pid', 0);

            $post = $this->post->get_post($warning['pid']);

            $forum_permissions = $this->forum->forum_permissions($post['fid']);

            if ($forum_permissions['canview'] != 1) {
                return $this->bb->error_no_permission();
            }
        }

        $this->warningshandler->set_data($warning);

        if ($this->warningshandler->validate_warning()) {
            $warninginfo = $this->warningshandler->insert_warning();

            // Are we notifying the user?
            if ($this->bb->getInput('send_pm', 0) == 1 &&
            $group_permissions['canusepms'] != 0 &&
            $this->bb->settings['enablepms'] != 0) {
                $pm = [
                  'subject' => $this->bb->getInput('pm_subject', '', true),
                  'message' => $this->bb->getInput('pm_message', '', true),
                  'touid' => $user->uid
                ];

                $sender_uid = $this->user->uid;
                if ($this->bb->settings['allowanonwarningpms'] == 1 && $this->bb->getInput('pm_anonymous', 0)) {
                      $sender_uid = -1;
                }

                // Some kind of friendly error notification
                if (!$this->pm->send_pm($pm, $sender_uid, true)) {
                      $this->warningshandler->friendly_action .= $this->lang->redirect_warned_pmerror;
                }
            }

            $this->plugins->runHooks('warnings_do_warn_end');

            $this->lang->redirect_warned = $this->lang->sprintf(
                $this->lang->redirect_warned,
                htmlspecialchars_uni($user->username),
                $this->warningshandler->new_warning_level,
                $this->warningshandler->friendly_action
            );

            if (!empty($post['pid'])) {
                  $this->bb->redirect(get_post_link($post['pid']), $this->lang->redirect_warned);
            } else {
                  $this->bb->redirect(get_profile_link($user->uid), $this->lang->redirect_warned);
            }
        } else {
            $this->warn_errors = $this->warningshandler->get_friendly_errors();
            $this->warn_errors = $this->bb->inline_error($this->warn_errors);
            //$this->bb->input['action'] = 'warn';
            return $this->index($request, $response, true);
        }
    }
}
