<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Controllers;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;

class Xmlhttp extends AbstractController
{

    public function index(Request $request, Response $response)
    {
        define('NO_ONLINE', 1);

        $shutdown_queries = $shutdown_functions = [];

        // Load some of the stock caches we'll be using.
        $groupscache = $this->cache->read('usergroups');

        if (!is_array($groupscache)) {
            $this->cache->update_usergroups();
            $groupscache = $this->cache->read('usergroups');
        }

        // Send no cache headers
        header('Expires: Sat, 1 Jan 2000 01:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');

        // Create the session
//    $session = new session;
//    $session->init();

        // Load the language we'll be using
        if (!isset($this->bb->settings['bblanguage'])) {
            $this->bb->settings['bblanguage'] = 'english';
        }
        if (isset($this->user->language) && $this->lang->language_exists($this->user->language)) {
            $this->bb->settings['bblanguage'] = $this->user->language;
        }
        $this->lang->set_language($this->bb->settings['bblanguage']);

        if (function_exists('mb_internal_encoding') && !empty($this->lang->settings['charset'])) {
            @mb_internal_encoding($this->lang->settings['charset']);
        }

        // Load the theme
        // 1. Check cookies
        if (!$this->user->uid && !empty($this->bb->cookies['mybbtheme'])) {
            $this->user->style = (int)$this->bb->cookies['mybbtheme'];
        }

        // 2. Load style
        if (isset($this->user->style) && (int)$this->user->style != 0) {
            $loadstyle = "tid='" . (int)$this->user->style . "'";
        } else {
            $loadstyle = "def='1'";
        }

        // Load basic theme information that we could be needing.
        if ($loadstyle != "def='1'") {
            $query = $this->db->simple_select('themes', 'name, tid, properties, allowedgroups', $loadstyle, ['limit' => 1]);
            $theme = $this->db->fetch_array($query);

            if (isset($theme['tid']) &&
                !$this->user->is_member($theme['allowedgroups']) &&
                $theme['allowedgroups'] != 'all'
            ) {
                if (isset($this->bb->cookies['mybbtheme'])) {
                    $this->bb->my_unsetcookie('mybbtheme');
                }

                $loadstyle = "def='1'";
            }
        }

        if ($loadstyle == "def='1'") {
            if (!$this->cache->read('default_theme')) {
                $this->cache->update_default_theme();
            }

            $theme = $this->cache->read('default_theme');
        }

        // No theme was found - we attempt to load the master or any other theme
        if (!isset($theme['tid']) || (isset($theme['tid']) && !$theme['tid'])) {
            // Missing theme was from a user, run a query to set any users using the theme to the default
            $this->db->update_query('users', ['style' => 0], "style = '{$this->user->style}'");

            // Attempt to load the master or any other theme if the master is not available
            $query = $this->db->simple_select('themes', 'name, tid, properties, stylesheets', '', ['order_by' => 'tid', 'limit' => 1]);
            $theme = $this->db->fetch_array($query);
        }
        $theme = @array_merge($theme, my_unserialize($theme['properties']));

        // Set the appropriate image language directory for this theme.
        // Are we linking to a remote theme server?
        if (my_substr($theme['imgdir'], 0, 7) == 'http://' || my_substr($theme['imgdir'], 0, 8) == 'https://') {
            // If a language directory for the current language exists within the theme - we use it
            if (!empty($this->user->language)) {
                $theme['imglangdir'] = $theme['imgdir'] . '/' . $this->user->language;
            } else {
                // Check if a custom language directory exists for this theme
                if (!empty($this->bb->settings['bblanguage'])) {
                    $theme['imglangdir'] = $theme['imgdir'] . '/' . $this->bb->settings['bblanguage'];
                } // Otherwise, the image language directory is the same as the language directory for the theme
                else {
                    $theme['imglangdir'] = $theme['imgdir'];
                }
            }
        } else {
            $img_directory = $theme['imgdir'];

            if ($this->bb->settings['usecdn'] && !empty($this->bb->settings['cdnpath'])) {
                $img_directory = rtrim($this->bb->settings['cdnpath'], '/') . '/' . ltrim($theme['imgdir'], '/');
            }

            if (!@is_dir($img_directory)) {
                $theme['imgdir'] = 'images';
            }

            // If a language directory for the current language exists within the theme - we use it
            if (!empty($this->user->language) && is_dir($img_directory . '/' . $this->user->language)) {
                $theme['imglangdir'] = $theme['imgdir'] . '/' . $this->user->language;
            } else {
                // Check if a custom language directory exists for this theme
                if (is_dir($img_directory . '/' . $this->bb->settings['bblanguage'])) {
                    $theme['imglangdir'] = $theme['imgdir'] . '/' . $this->bb->settings['bblanguage'];
                } // Otherwise, the image language directory is the same as the language directory for the theme
                else {
                    $theme['imglangdir'] = $theme['imgdir'];
                }
            }

            $theme['imgdir'] = $this->themes->get_asset_url($theme['imgdir']);
            $theme['imglangdir'] = $this->themes->get_asset_url($theme['imglangdir']);
        }

        $templatelist = 'postbit_editedby,xmlhttp_buddyselect_online,xmlhttp_buddyselect_offline,xmlhttp_buddyselect';
        $this->templates->cache($this->db->escape_string($templatelist));

        if ($this->lang->settings['charset']) {
            $charset = $this->lang->settings['charset'];
        } // If not, revert to UTF-8
        else {
            $charset = 'UTF-8';
        }

        $this->lang->load('global');
        $this->lang->load('xmlhttp');

        $closed_bypass = ['refresh_captcha', 'validate_captcha'];

        $this->bb->input['action'] = $this->bb->getInput('action', '', false);

        $this->plugins->runHooks('xmlhttp');

        // If the board is closed, the user is not an administrator and they're not trying to login, show the board closed message
        if ($this->bb->settings['boardclosed'] == 1 &&
            $this->bb->usergroup['canviewboardclosed'] != 1 &&
            !in_array($this->bb->input['action'], $closed_bypass)
        ) {
            // Show error
            if (!$this->bb->settings['boardclosed_reason']) {
                $this->bb->settings['boardclosed_reason'] = $this->lang->boardclosed_reason;
            }

            $this->lang->error_boardclosed .= "<br /><em>{$this->bb->settings['boardclosed_reason']}</em>";

            $this->xmlhttp_error($this->lang->error_boardclosed);
        }

        // Fetch a list of usernames beginning with a certain string (used for auto completion)
        if ($this->bb->input['action'] === 'get_users') {
            $this->bb->input['query'] = ltrim($this->bb->getInput('query', ''));

            // If the string is less than 2 characters, quit.
            if (my_strlen($this->bb->input['query']) < 2) {
                exit;
            }

            if ($this->bb->getInput('getone', 0) == 1) {
                $limit = 1;
            } else {
                $limit = 15;
            }

            // Send our headers.
            header("Content-type: application/json; charset={$charset}");

            // Query for any matching users.
            $query_options = [
                'order_by' => 'username',
                'order_dir' => 'asc',
                'limit_start' => 0,
                'limit' => $limit
            ];

            $this->plugins->runHooks('xmlhttp_get_users_start');

            $query = $this->db->simple_select('users', 'uid, username', "username LIKE '" .
                $this->db->escape_string_like($this->bb->input['query']) . "%'", $query_options);
            if ($limit == 1) {
                $user = $this->db->fetch_array($query);
                $user['username'] = htmlspecialchars_uni($user['username']);
                $data = ['id' => $user['username'], 'text' => $user['username']];
            } else {
                $data = [];
                while ($user = $this->db->fetch_array($query)) {
                    $user['username'] = htmlspecialchars_uni($user['username']);
                    $data[] = ['id' => $user['username'], 'text' => $user['username']];
                }
            }

            $this->plugins->runHooks('xmlhttp_get_users_end');

            echo json_encode($data);
            exit;
        } // This action provides editing of thread/post subjects from within their respective list pages.
        elseif ($this->bb->input['action'] == 'edit_subject' && $this->bb->request_method == 'post') {
            // Verify POST request
            if (!$this->bb->verify_post_check($this->bb->getInput('my_post_key', '', false), true)) {
                $this->xmlhttp_error($this->lang->invalid_post_code);
            }

            // We're editing a thread subject.
            if ($this->bb->getInput('tid', 0)) {
                // Fetch the thread.
                $thread = $this->thread->get_thread($this->bb->getInput('tid', 0));
                if (!$thread) {
                    $this->xmlhttp_error($this->lang->thread_doesnt_exist);
                }

                // Fetch some of the information from the first post of this thread.
                $query_options = [
                    'order_by' => 'dateline',
                    'order_dir' => 'asc',
                ];
                $query = $this->db->simple_select('posts', 'pid,uid,dateline', "tid='" . $thread['tid'] . "'", $query_options);
                $post = $this->db->fetch_array($query);
            } else {
                exit;
            }

            // Fetch the specific forum this thread/post is in.
            $forum = $this->forum->get_forum($thread['fid']);

            // Missing thread, invalid forum? Error.
            if (!$forum || $forum['type'] != 'f') {
                $this->xmlhttp_error($this->lang->thread_doesnt_exist);
            }

            // Fetch forum permissions.
            $forumpermissions = $this->forum->forum_permissions($forum['fid']);

            $this->plugins->runHooks('xmlhttp_edit_subject_start');

            // If this user is not a moderator with 'caneditposts' permissions.
            if (!$this->user->is_moderator($forum['fid'], 'caneditposts')) {
                // Thread is closed - no editing allowed.
                if ($thread['closed'] == 1) {
                    $this->xmlhttp_error($this->lang->thread_closed_edit_subjects);
                } // Forum is not open, user doesn't have permission to edit, or author doesn't match this user - don't allow editing.
                elseif ($forum['open'] == 0 ||
                    $forumpermissions['caneditposts'] == 0 ||
                    $this->user->uid != $post['uid'] ||
                    $this->user->uid == 0
                ) {
                    $this->xmlhttp_error($this->lang->no_permission_edit_subject);
                } // If we're past the edit time limit - don't allow editing.
                elseif ($this->bb->usergroup['edittimelimit'] != 0 && $post['dateline'] < (TIME_NOW - ($this->bb->usergroup['edittimelimit'] * 60))) {
                    $this->lang->edit_time_limit = $this->lang->sprintf($this->lang->edit_time_limit, $this->bb->usergroup['edittimelimit']);
                    $this->xmlhttp_error($this->lang->edit_time_limit);
                }
                $ismod = false;
            } else {
                $ismod = true;
            }
            $subject = $this->bb->getInput('value', '');
            if (my_strtolower($charset) != 'utf-8') {
                if (function_exists('iconv')) {
                    $subject = iconv($charset, 'UTF-8//IGNORE', $subject);
                } elseif (function_exists('mb_convert_encoding')) {
                    $subject = @mb_convert_encoding($subject, $charset, 'UTF-8');
                } elseif (my_strtolower($charset) == 'iso-8859-1') {
                    $subject = utf8_decode($subject);
                }
            }

            // Only edit subject if subject has actually been changed
            if ($thread['subject'] != $subject) {
                // Set up posthandler.
                $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'update');
                $posthandler->action = 'post';

                // Set the post data that came from the input to the $post array.
                $updatepost = [
                    'pid' => $post['pid'],
                    'tid' => $thread['tid'],
                    'prefix' => $thread['prefix'],
                    'subject' => $subject,
                    'edit_uid' => $this->user->uid
                ];
                $posthandler->set_data($updatepost);

                // Now let the post handler do all the hard work.
                if (!$posthandler->validatePost()) {
                    $post_errors = $posthandler->get_friendly_errors();
                    $this->xmlhttp_error($post_errors);
                } // No errors were found, we can call the update method.
                else {
                    $posthandler->updatePost();
                    if ($ismod == true) {
                        $modlogdata = [
                            'tid' => $thread['tid'],
                            'fid' => $forum['fid']
                        ];
                        $this->bblogger->log_moderator_action($modlogdata, $this->lang->edited_post);
                    }
                }
            }

            // Send our headers.
            header("Content-type: application/json; charset={$charset}");

            $this->plugins->runHooks('xmlhttp_edit_subject_end');

            $this->bb->input['value'] = $this->parser->parse_badwords($this->bb->getInput('value', ''));

            // Spit the subject back to the browser.
            $subject = substr($this->bb->input['value'], 0, 120); // 120 is the varchar length for the subject column
            echo json_encode(['subject' => '<a href="' . get_thread_link($thread['tid']) . '">' . htmlspecialchars_uni($subject) . '</a>']);

            // Close the connection.
            exit;
        } elseif ($this->bb->input['action'] == 'edit_post') {
            // Fetch the post from the database.
            $post = $this->post->get_post($this->bb->getInput('pid', 0));

            // No result, die.
            if (!$post) {
                $this->xmlhttp_error($this->lang->post_doesnt_exist);
            }

            // Fetch the thread associated with this post.
            $thread = $this->thread->get_thread($post['tid']);

            // Fetch the specific forum this thread/post is in.
            $forum = $this->forum->get_forum($thread['fid']);

            // Missing thread, invalid forum? Error.
            if (!$thread || !$forum || $forum['type'] != 'f') {
                $this->xmlhttp_error($this->lang->thread_doesnt_exist);
            }

            // Check if this forum is password protected and we have a valid password
            if ($this->forum->check_forum_password($forum['fid'], 0, true)) {
                $this->xmlhttp_error($this->lang->wrong_forum_password);
            }

            // Fetch forum permissions.
            $forumpermissions = $this->forum->forum_permissions($forum['fid']);

            $this->plugins->runHooks('xmlhttp_edit_post_start');

            // If this user is not a moderator with 'caneditposts' permissions.
            if (!$this->user->is_moderator($forum['fid'], 'caneditposts')) {
                // Thread is closed - no editing allowed.
                if ($thread['closed'] == 1) {
                    $this->xmlhttp_error($this->lang->thread_closed_edit_message);
                } // Forum is not open, user doesn't have permission to edit, or author doesn't match this user - don't allow editing.
                elseif ($forum['open'] == 0 ||
                    $forumpermissions['caneditposts'] == 0 ||
                    $this->user->uid != $post['uid'] ||
                    $this->user->uid == 0 ||
                    $this->user->suspendposting == 1
                ) {
                    $this->xmlhttp_error($this->lang->no_permission_edit_post);
                } // If we're past the edit time limit - don't allow editing.
                elseif ($this->bb->usergroup['edittimelimit'] != 0 && $post['dateline'] < (TIME_NOW - ($this->bb->usergroup['edittimelimit'] * 60))) {
                    $this->lang->edit_time_limit = $this->lang->sprintf($this->lang->edit_time_limit, $this->bb->usergroup['edittimelimit']);
                    $this->xmlhttp_error($this->lang->edit_time_limit);
                }
                // User can't edit unapproved post
                if ($post['visible'] == 0) {
                    $this->xmlhttp_error($this->lang->post_moderation);
                }
            }

            $this->plugins->runHooks('xmlhttp_edit_post_end');

            if ($this->bb->getInput('do', '', false) === 'get_post') {
                // Send our headers.
                header("Content-type: application/json; charset={$charset}");

                // Send the contents of the post.
                echo json_encode($post['message']);
                exit;
            } elseif ($this->bb->getInput('do', '', false) === 'update_post') {
                // Verify POST request
                if (!$this->bb->verify_post_check($this->bb->getInput('my_post_key', '', false), true)) {
                    $this->xmlhttp_error($this->lang->invalid_post_code);
                }

                $message = $this->bb->getInput('value', '');
                $editreason = $this->bb->getInput('editreason', '');
                if (my_strtolower($charset) != 'utf-8') {
                    if (function_exists('iconv')) {
                        $message = iconv($charset, 'UTF-8//IGNORE', $message);
                        $editreason = iconv($charset, 'UTF-8//IGNORE', $editreason);
                    } elseif (function_exists('mb_convert_encoding')) {
                        $message = @mb_convert_encoding($message, $charset, 'UTF-8');
                        $editreason = @mb_convert_encoding($editreason, $charset, 'UTF-8');
                    } elseif (my_strtolower($charset) == 'iso-8859-1') {
                        $message = utf8_decode($message);
                        $editreason = utf8_decode($editreason);
                    }
                }

                // Set up posthandler.
                $posthandler = new \RunBB\Handlers\DataHandlers\PostDataHandler($this->bb, 'update');
                $posthandler->action = 'post';

                // Set the post data that came from the input to the $post array.
                $updatepost = [
                    'pid' => $post['pid'],
                    'message' => $message,
                    'editreason' => $editreason,
                    'edit_uid' => $this->user->uid
                ];

                // If this is the first post set the prefix. If a forum requires a prefix the quick edit would throw an error otherwise
                if ($post['pid'] == $thread['firstpost']) {
                    $updatepost['prefix'] = $thread['prefix'];
                }

                $posthandler->set_data($updatepost);

                // Now let the post handler do all the hard work.
                if (!$posthandler->validatePost()) {
                    $post_errors = $posthandler->get_friendly_errors();
                    $this->xmlhttp_error($post_errors);
                } // No errors were found, we can call the update method.
                else {
                    $postinfo = $posthandler->updatePost();
                    $visible = $postinfo['visible'];
                    if ($visible == 0 && !$this->user->is_moderator($post['fid'], 'canviewunapprove')) {
                        // Is it the first post?
                        if ($thread['firstpost'] == $post['pid']) {
                            echo json_encode([
                                'moderation_thread' => $this->lang->thread_moderation,
                                'url' => $this->bb->settings['bburl'] . '/' . $this->forum->get_forum_link($thread['fid']),
                                'message' => $post['message']]);
                            exit;
                        } else {
                            echo json_encode([
                                'moderation_post' => $this->lang->post_moderation,
                                'url' => $this->bb->settings['bburl'] . '/' . get_thread_link($thread['tid']),
                                'message' => $post['message']]);
                            exit;
                        }
                    }
                }

                $parser_options = [
                    'allow_html' => $forum['allowhtml'],
                    'allow_mycode' => $forum['allowmycode'],
                    'allow_smilies' => $forum['allowsmilies'],
                    'allow_imgcode' => $forum['allowimgcode'],
                    'allow_videocode' => $forum['allowvideocode'],
                    'me_username' => $post['username'],
                    'filter_badwords' => 1
                ];

                if ($post['smilieoff'] == 1) {
                    $parser_options['allow_smilies'] = 0;
                }

                if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                    ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
                ) {
                    $parser_options['allow_imgcode'] = 0;
                }

                if (($this->user->showvideos != 1 && $this->user->uid != 0) ||
                    ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
                ) {
                    $parser_options['allow_videocode'] = 0;
                }

                $post['message'] = $this->parser->parse_message($message, $parser_options);

                // Now lets fetch all of the attachments for these posts.
                if ($this->bb->settings['enableattachments'] != 0) {
                    $query = $this->db->simple_select('attachments', '*', "pid='{$post['pid']}'");
                    while ($attachment = $this->db->fetch_array($query)) {
                        $attachcache[$attachment['pid']][$attachment['aid']] = $attachment;
                    }

                    $this->post->get_post_attachments($post['pid'], $post);
                }

                // Figure out if we need to show an 'edited by' message
                // Only show if at least one of 'showeditedby' or 'showeditedbyadmin' is enabled
                if ($this->bb->settings['showeditedby'] != 0 && $this->bb->settings['showeditedbyadmin'] != 0) {
                    $post['editdate'] = $this->time->formatDate('relative', TIME_NOW);
                    $post['editnote'] = $this->lang->sprintf($this->lang->postbit_edited, $post['editdate']);
                    $post['editedprofilelink'] = $this->user->build_profile_link($this->user->username, $this->user->uid);
                    $post['editreason'] = trim($editreason);
                    $editreason = '';
                    if ($post['editreason'] != '') {
                        $post['editreason'] = $this->parser->parse_badwords($post['editreason']);
                        $post['editreason'] = htmlspecialchars_uni($post['editreason']);
                        $editreason = " <em>{$this->lang->postbit_editreason}: {$post['editreason']}</em>";
                    }
                    $editedmsg = "<span class=\"edited_post\">({$post['editnote']} {$post['editedprofilelink']}.{$editreason})</span>";
                }

                // Send our headers.
                header("Content-type: application/json; charset={$charset}");

                $editedmsg_response = null;
                if ($editedmsg) {
                    $editedmsg_response = str_replace(["\r", "\n"], '', $editedmsg);
                }

                $this->plugins->runHooks('xmlhttp_update_post');

                echo json_encode(['message' => $post['message'] . "\n", 'editedmsg' => $editedmsg_response]);
                exit;
            }
        } // Fetch the list of multiquoted posts which are not in a specific thread
        elseif ($this->bb->input['action'] == 'get_multiquoted') {
            // If the cookie does not exist, exit
            if (!array_key_exists('multiquote', $this->cookies)) {
                exit;
            }
            // Divide up the cookie using our delimeter
            $multiquoted = explode('|', $this->bb->cookies['multiquote']);

            $this->plugins->runHooks('xmlhttp_get_multiquoted_start');

            // No values - exit
            if (!is_array($multiquoted)) {
                exit;
            }

            // Loop through each post ID and sanitize it before querying
            foreach ($multiquoted as $post) {
                $quoted_posts[$post] = (int)$post;
            }

            // Join the post IDs back together
            $quoted_posts = implode(',', $quoted_posts);

            // Fetch unviewable forums
            $unviewable_forums = $this->forum->get_unviewable_forums();
            $inactiveforums = $this->forum->get_inactive_forums();
            if ($unviewable_forums) {
                $unviewable_forums = ' AND t.fid NOT IN (' . implode(',', $unviewable_forums) . ')';
            }
            if ($inactiveforums) {
                $inactiveforums = ' AND t.fid NOT IN (' . implode(',', $inactiveforums) . ')';
            }
            $message = '';

            // Are we loading all quoted posts or only those not in the current thread?
            if (empty($this->bb->input['load_all'])) {
                $from_tid = "p.tid != '" . $this->bb->getInput('tid', 0) . "' AND ";
            } else {
                $from_tid = '';
            }

            $this->plugins->runHooks('xmlhttp_get_multiquoted_intermediate');

            // Query for any posts in the list which are not within the specified thread
            $query = $this->db->query('
		SELECT p.subject, p.message, p.pid, p.tid, p.username, p.dateline, t.fid, p.visible, u.username AS userusername
		FROM ' . TABLE_PREFIX . 'posts p
		LEFT JOIN ' . TABLE_PREFIX . 'threads t ON (t.tid=p.tid)
		LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=p.uid)
		WHERE {$from_tid}p.pid IN ({$quoted_posts}) {$unviewable_forums} {$inactiveforums}
		ORDER BY p.dateline
	");
            while ($quoted_post = $this->db->fetch_array($query)) {
                if (!$this->user->is_moderator($quoted_post['fid'], 'canviewunapprove') && $quoted_post['visible'] == 0) {
                    continue;
                }

                $message .= $this->post->parse_quoted_message($quoted_post, false);
            }
            if ($this->bb->settings['maxquotedepth'] != '0') {
                $message = $this->post->remove_message_quotes($message);
            }

            // Send our headers.
            header("Content-type: application/json; charset={$charset}");

            $this->plugins->runHooks('xmlhttp_get_multiquoted_end');

            echo json_encode(['message' => $message]);
            exit;
        } elseif ($this->bb->input['action'] == 'refresh_captcha') {
            $imagehash = $this->db->escape_string($this->bb->getInput('imagehash', ''));
            $query = $this->db->simple_select('captcha', 'dateline', "imagehash='$imagehash'");
            if ($this->db->num_rows($query) == 0) {
                $this->xmlhttp_error($this->lang->captcha_not_exists);
            }
            $this->db->delete_query('captcha', "imagehash='$imagehash'");
            $randomstr = random_str(5);
            $imagehash = md5(random_str(12));
            $regimagearray = [
                'imagehash' => $imagehash,
                'imagestring' => $randomstr,
                'dateline' => TIME_NOW
            ];

            $this->plugins->runHooks('xmlhttp_refresh_captcha');

            $this->db->insert_query('captcha', $regimagearray);
            header("Content-type: application/json; charset={$charset}");
            echo json_encode(['imagehash' => $imagehash]);
            exit;
        } elseif ($this->bb->input['action'] == 'validate_captcha') {
            header("Content-type: application/json; charset={$charset}");
            $imagehash = $this->db->escape_string($this->bb->getInput('imagehash', ''));
            $query = $this->db->simple_select('captcha', 'imagestring', "imagehash='$imagehash'");
            if ($this->db->num_rows($query) == 0) {
                echo json_encode($this->lang->captcha_valid_not_exists);
                exit;
            }
            $imagestring = $this->db->fetch_field($query, 'imagestring');

            $this->plugins->runHooks('xmlhttp_validate_captcha');

            if (my_strtolower($imagestring) == my_strtolower($this->bb->getInput('imagestring', ''))) {
                //echo json_encode(array('success' => $this->lang->captcha_matches));
                echo json_encode('true');
                exit;
            } else {
                echo json_encode($this->lang->captcha_does_not_match);
                exit;
            }
        } elseif ($this->bb->input['action'] == 'refresh_question' && $this->bb->settings['securityquestion']) {
            header("Content-type: application/json; charset={$charset}");

            $sid = $this->db->escape_string($this->bb->getInput('question_id', ''));
            $query = $this->db->query('
		SELECT q.qid, s.sid
		FROM ' . TABLE_PREFIX . 'questionsessions s
		LEFT JOIN ' . TABLE_PREFIX . "questions q ON (q.qid=s.qid)
		WHERE q.active='1' AND s.sid='{$sid}'
	");

            if ($this->db->num_rows($query) == 0) {
                $this->xmlhttp_error($this->lang->answer_valid_not_exists);
            }

            $qsession = $this->db->fetch_array($query);

            // Delete previous question session
            $this->db->delete_query('questionsessions', "sid='$sid'");

            $sid = $this->user->generate_question($qsession['qid']);
            $query = $this->db->query('
		SELECT q.question, s.sid
		FROM ' . TABLE_PREFIX . 'questionsessions s
		LEFT JOIN ' . TABLE_PREFIX . "questions q ON (q.qid=s.qid)
		WHERE q.active='1' AND s.sid='{$sid}' AND q.qid!='{$qsession['qid']}'
	");

            $this->plugins->runHooks('xmlhttp_refresh_question');

            if ($this->db->num_rows($query) > 0) {
                $question = $this->db->fetch_array($query);

                echo json_encode([
                    'question' => htmlspecialchars_uni($question['question']),
                    'sid' => htmlspecialchars_uni($question['sid'])]);
                exit;
            } else {
                $this->xmlhttp_error($this->lang->answer_valid_not_exists);
            }
        } elseif ($this->bb->input['action'] == 'validate_question' && $this->bb->settings['securityquestion']) {
            header("Content-type: application/json; charset={$charset}");
            $sid = $this->db->escape_string($this->bb->getInput('question', ''));
            $answer = $this->db->escape_string($this->bb->getInput('answer', ''));

            $query = $this->db->query('
		SELECT q.*, s.sid
		FROM ' . TABLE_PREFIX . 'questionsessions s
		LEFT JOIN ' . TABLE_PREFIX . "questions q ON (q.qid=s.qid)
		WHERE q.active='1' AND s.sid='{$sid}'
	");

            if ($this->db->num_rows($query) == 0) {
                echo json_encode($this->lang->answer_valid_not_exists);
                exit;
            } else {
                $question = $this->db->fetch_array($query);
                $valid_answers = preg_split("/\r\n|\n|\r/", $question['answer']);
                $validated = 0;

                foreach ($valid_answers as $answers) {
                    if (my_strtolower($answers) == my_strtolower($answer)) {
                        $validated = 1;
                    }
                }

                $this->plugins->runHooks('xmlhttp_validate_question');

                if ($validated != 1) {
                    echo json_encode($this->lang->answer_does_not_match);
                    exit;
                } else {
                    echo json_encode('true');
                    exit;
                }
            }

            exit;
        } elseif ($this->bb->input['action'] == 'complex_password') {
            $password = trim($this->bb->getInput('password', ''));
            $password = str_replace(
                [unichr(160), unichr(173), unichr(0xCA), dec_to_utf8(8238), dec_to_utf8(8237), dec_to_utf8(8203)],
                [' ', '-', '', '', '', ''],
                $password
            );

            header("Content-type: application/json; charset={$charset}");

            $this->plugins->runHooks('xmlhttp_complex_password');

            if (!preg_match("/^.*(?=.{" . $this->bb->settings['minpasswordlength'] . ",})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $password)) {
                echo json_encode($this->lang->complex_password_fails);
            } else {
                // Return nothing but an OK password if passes regex
                echo json_encode('true');
            }

            exit;
        } elseif ($this->bb->input['action'] == 'username_availability') {
            if (!$this->bb->verify_post_check($this->bb->getInput('my_post_key', '', false), true)) {
                $this->xmlhttp_error($this->lang->invalid_post_code);
            }

            $username = $this->bb->getInput('username', '');

            // Fix bad characters
            $username = trim_blank_chrs($username);
            $username = str_replace(
                [unichr(160), unichr(173), unichr(0xCA), dec_to_utf8(8238), dec_to_utf8(8237), dec_to_utf8(8203)],
                [' ', '-', '', '', '', ''],
                $username
            );

            // Remove multiple spaces from the username
            $username = preg_replace("#\s{2,}#", ' ', $username);

            header("Content-type: application/json; charset={$charset}");

            if (empty($username)) {
                echo json_encode($this->lang->banned_characters_username);
                exit;
            }

            // Check if the username belongs to the list of banned usernames.
            $banned_username = $this->ban->is_banned_username($username, true);
            if ($banned_username) {
                echo json_encode($this->lang->banned_username);
                exit;
            }

            // Check for certain characters in username (<, >, &, and slashes)
            if (strpos($username, '<') !== false ||
                strpos($username, '>') !== false ||
                strpos($username, '&') !== false ||
                my_strpos($username, '\\') !== false ||
                strpos($username, ';') !== false ||
                strpos($username, ',') !== false ||
                !validate_utf8_string($username, false, false)
            ) {
                echo json_encode($this->lang->banned_characters_username);
                exit;
            }

            // Check if the username is actually already in use
            $user = $this->user->get_user_by_username($username);

            $this->plugins->runHooks('xmlhttp_username_availability');

            if ($user['uid']) {
                $this->lang->username_taken = $this->lang->sprintf($this->lang->username_taken, htmlspecialchars_uni($username));
                echo json_encode($this->lang->username_taken);
                exit;
            } else {
                //$this->lang->username_available = $this->lang->sprintf($this->lang->username_available, htmlspecialchars_uni($username));
                echo json_encode('true');
                exit;
            }
        } elseif ($this->bb->input['action'] == 'username_exists') {
            if (!$this->bb->verify_post_check($this->bb->getInput('my_post_key', '', false), true)) {
                $this->xmlhttp_error($this->lang->invalid_post_code);
            }

            $username = $this->bb->getInput('value', '');

            header("Content-type: application/json; charset={$charset}");

            if (!trim($username)) {
                echo json_encode(['success' => 1]);
                exit;
            }

            // Check if the username actually exists
            $user = $this->user->get_user_by_username($username);

            $this->plugins->runHooks('xmlhttp_username_exists');

            if ($user['uid']) {
                $this->lang->valid_username = $this->lang->sprintf($this->lang->valid_username, htmlspecialchars_uni($username));
                echo json_encode(['success' => $this->lang->valid_username]);
                exit;
            } else {
                $this->lang->invalid_username = $this->lang->sprintf($this->lang->invalid_username, htmlspecialchars_uni($username));
                echo json_encode($this->lang->invalid_username);
                exit;
            }
        } elseif ($this->bb->input['action'] == 'get_buddyselect') {
            // Send our headers.
            header("Content-type: text/plain; charset={$charset}");

            if ($this->user->buddylist != '') {
                $query_options = [
                    'order_by' => 'username',
                    'order_dir' => 'asc'
                ];

                $this->plugins->runHooks('xmlhttp_get_buddyselect_start');

                $timecut = TIME_NOW - $this->bb->settings['wolcutoff'];
//                $query = $this->db->simple_select('users', 'uid, username, usergroup, displaygroup, lastactive, lastvisit, invisible',
//                    "uid IN ({$this->user->buddylist})", $query_options);
                $b = \RunBB\Models\User::whereIn('uid', explode(',', $this->user->buddylist))
                    ->orderBy('username', 'asc')
                    ->get(['uid', 'username', 'usergroup', 'displaygroup', 'lastactive', 'lastvisit', 'invisible'])
                    ->toArray();

                $online = $offline = [];
//                while ($buddy = $this->db->fetch_array($query)) {
                foreach ($b as $buddy) {
                    $buddy_name = $this->user->format_name($buddy['username'], $buddy['usergroup'], $buddy['displaygroup']);
                    $profile_link = $this->user->build_profile_link($buddy_name, $buddy['uid'], '_blank');
                    if ($buddy['lastactive'] > $timecut &&
                        ($buddy['invisible'] == 0 || $this->user->usergroup == 4) &&
                        $buddy['lastvisit'] != $buddy['lastactive']
                    ) {
                        $online[] = [
                            'uid' => $buddy['uid'],
                            'username' => $buddy['username'],
                            'buddy_name' => $buddy_name
                        ];
                    } else {
                        $offline[] = [
                            'uid' => $buddy['uid'],
                            'username' => $buddy['username'],
                            'buddy_name' => $buddy_name
                        ];
                    }
                }
                $this->view->offsetSet('online', $online);
                $this->view->offsetSet('offline', $offline);

                $this->plugins->runHooks('xmlhttp_get_buddyselect_end');

                echo $this->view->fetch('@forum/Misc/buddySelect.html.twig');
            } else {
                $this->xmlhttp_error($this->lang->buddylist_error);
            }
        }
    }

    /**
     * Spits an XML Http based error message back to the browser
     *
     * @param string $message The message to send back.
     */
    private function xmlhttp_error($message)
    {
        // Send our headers.
        header("Content-type: application/json; charset={$this->lang->settings['charset']}");

        // Do we have an array of messages?
        if (is_array($message)) {
            $response = [];
            foreach ($message as $error) {
                $response[] = $error;
            }
            // Send the error messages.
            echo json_encode(['errors' => [$response]]);
            exit;
        }
        // Just a single error? Send it along.
        echo json_encode(['errors' => [$message]]);
        exit;
    }
}
