<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Archive extends AbstractController
{
    /**
     * Output the archive page header.
     *
     * @param string $title The page title.
     * @param string $fulltitle The full page title.
     * @param string $fullurl The full page URL.
     */
    public function archive_header($title = '', $fulltitle = '', $fullurl = '')
    {
        // Build the archive navigation.
        $this->bb->nav = $this->archive_navigation();

        // If there is a title, append it to the bbname.
        if (!$title) {
            $title = $this->bb->settings['bbname'];
        } else {
            $title = $this->bb->settings['bbname'] . ' - ' . $title;
        }

        // If the language doesn't have a charset, make it UTF-8.
        if ($this->bb->lang->settings['charset']) {
            $charset = $this->bb->lang->settings['charset'];
        } else {
            $charset = 'utf-8';
        }

        $dir = '';
        if ($this->bb->lang->settings['rtl'] == 1) {
            $dir = ' dir="rtl"';
        }

        if ($this->bb->lang->settings['htmllang']) {
            $htmllang = " xml:lang=\"" . $this->bb->lang->settings['htmllang'] . "\" lang=\"" . $this->bb->lang->settings['htmllang'] . "\"";
        } else {
            $htmllang = " xml:lang=\"en\" lang=\"en\"";
        }
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml"<?php echo $dir;
        echo $htmllang; ?>>
        <head>
            <title><?php echo $title; ?></title>
            <meta http-equiv="content-type" content="text/html; charset=<?php echo $charset; ?>"/>
            <meta name="robots" content="index,follow"/>
            <link type="text/css" rel="stylesheet" rev="stylesheet"
                  href="<?php echo $this->bb->asset_url; ?>/archive/screen.css" media="screen"/>
            <link type="text/css" rel="stylesheet" rev="stylesheet"
                  href="<?php echo $this->bb->asset_url; ?>/archive/print.css" media="print"/>
        </head>
        <body>
        <div id="container">
        <h1>
            <a href="<?php echo $this->bb->settings['bburl']; ?>/index.php"><?php echo $this->bb->settings['bbname_orig']; ?></a>
        </h1>
        <div class="navigation"><?php echo $this->bb->nav; ?></div>
        <div id="fullversion"><strong><?php echo $this->bb->lang->archive_fullversion; ?></strong> <a
                href="<?php echo $fullurl; ?>"><?php echo $fulltitle; ?></a></div>
        <div id="infobox"><?php echo $this->bb->lang->sprintf($this->bb->lang->archive_note, $fullurl); ?></div>
        <div id="content">
        <?php
        $this->sent_header = 1;
    }

    /**
     * Build the archive navigation.
     *
     * @return string The build navigation
     */
    private function archive_navigation()
    {
        $navsep = " &gt; ";
        $this->bb->nav = $activesep = '';
        if (is_array($this->bb->navbits)) {
            reset($this->bb->navbits);
            foreach ($this->bb->navbits as $key => $navbit) {
                if (!empty($this->bb->navbits[$key + 1])) {
                    if (!empty($this->bb->navbits[$key + 2])) {
                        $sep = $navsep;
                    } else {
                        $sep = "";
                    }
                    $this->bb->nav .= "<a href=\"" . $navbit['url'] . "\">" . $navbit['name'] . "</a>$sep";
                }
            }
        }
        $navsize = count($this->bb->navbits);
        $navbit = $this->bb->navbits[$navsize - 1];
        if (!empty($this->bb->nav)) {
            $activesep = $navsep;
        }
        $this->bb->nav .= $activesep . $navbit['name'];

        return $this->bb->nav;
    }

    /**
     * Output multipage navigation.
     *
     * @param int $count The total number of items.
     * @param int $perpage The items per page.
     * @param int $page The current page.
     * @param string $url The URL base.
     */
    function archive_multipage($count, $perpage, $page, $url)
    {
        if ($count > $perpage) {
            $pages = $count / $perpage;
            $pages = ceil($pages);

            $mppage = null;
            for ($i = 1; $i <= $pages; ++$i) {
                if ($i == $page) {
                    $mppage .= "<strong>$i</strong> ";
                } else {
                    $mppage .= "<a href=\"$url-$i.html\">$i</a> ";
                }
            }
            $multipage = "<div class=\"multipage\"><strong>" . $this->bb->lang->archive_pages . "</strong> $mppage</div>";
            echo $multipage;
        }
    }

    /**
     * Output the archive footer.
     *
     */
    function archive_footer()
    {
        $totaltime = $this->bb->maintimer->stop();
        if ($this->bb->settings['showvernum'] == 1) {
            $bbversion = ' ' . $this->bb->version;
        } else {
            $bbversion = '';
        }
        ?>
        </div>
        <div class="navigation"><?php echo $this->bb->nav; ?></div>
        </div>
        <div id="footer">
            <?php echo $this->bb->lang->powered_by; ?> <a href="http://www.runcmf.ru">RunBB</a><?php echo $bbversion; ?>
            , &copy; 2016-<?php echo date("Y"); ?> Based on <a href="http://www.mybb.com">MyBB 1.8.7</a>
        </div>
        </body>
        </html>
        <?php
    }

    /**
     * Output an archive error.
     *
     * @param string $error The error language string identifier.
     */
    function archive_error($error)
    {
        if (!isset($this->sent_header)) {
            $this->archive_header('', $this->bb->settings['bbname'], $this->bb->settings['bburl']);//."/index.php");
        }
        ?>
        <div class="error">
            <div class="header"><?php echo $this->bb->lang->error; ?></div>
            <div class="message"><?php echo $error; ?></div>
        </div>
        <?php
        $this->archive_footer();
        //exit;
    }

    /**
     * Ouput a "no permission"page.
     */
    function archive_error_no_permission()
    {
        $noperm_array = [
            "nopermission" => '1',
            "location1" => 0,
            "location2" => 0
        ];

        \RunBB\Models\Session::where('sid', $this->bb->session->sid)
            ->update($noperm_array);

        $this->archive_error($this->bb->lang->archive_nopermission);
    }

    /**
     * Check the password given on a certain forum for validity
     *
     * @param int $fid The forum ID
     * @param int $pid The Parent ID
     * @return bool Returns false on failure
     */
    public function check_forum_password_archive($fid, $pid = 0)
    {
        if (!isset($this->bb->forum_cache)) {
            $this->bb->forum->cache_forums();
            if (!$this->bb->forum_cache) {
                return false;
            }
        }

        // Loop through each of parent forums to ensure we have a password for them too
        $parents = explode(',', $this->bb->forum_cache[$fid]['parentlist']);
        rsort($parents);
        if (!empty($parents)) {
            foreach ($parents as $parent_id) {
                if ($parent_id == $fid || $parent_id == $pid) {
                    continue;
                }

                if ($this->bb->forum_cache[$parent_id]['password'] != "") {
                    $this->check_forum_password_archive($parent_id, $fid);
                }
            }
        }

        $password = $this->bb->forum_cache[$fid]['password'];
        if ($password) {
            if (!$this->bb->cookies['forumpass'][$fid] ||
                ($this->bb->cookies['forumpass'][$fid] &&
                    md5($this->user->uid . $password) !== $this->bb->cookies['forumpass'][$fid])
            ) {
                $this->archive_error_no_permission();
                return 'exit';
            }
        }
    }
}