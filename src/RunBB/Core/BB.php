<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

// Define Custom MyBB error handler constants with a value not used by php's error handler.
define('MYBB_SQL', 20);
define('MYBB_TEMPLATE', 30);
define('MYBB_GENERAL', 40);
define('MYBB_NOT_INSTALLED', 41);
define('MYBB_NOT_UPGRADED', 42);
define('MYBB_INSTALL_DIR_EXISTS', 43);
define('MYBB_SQL_LOAD_ERROR', 44);
define('MYBB_CACHE_NO_WRITE', 45);
define('MYBB_CACHEHANDLER_LOAD_ERROR', 46);

use RunCMF\Core\AbstractController;
use RunBB\Helpers\TwigGetVarHelper;

class BB extends AbstractController
// extends Core
{
    /**
     * The friendly version number of BB we're running.
     *
     * @var string
     */
    public $version = '1.8.7';

    /**
     * The version code of BB we're running.
     *
     * @var integer
     */
    public $version_code = 1807;

    /**
     * The current working directory.
     *
     * @var string
     */
    public $cwd = '.';

    /**
     * Input variables received from the outer world.
     *
     * @var array
     */
    public $input = [];

    /**
     * Cookie variables received from the outer world.
     *
     * @var array
     */
    public $cookies = [];

    /**
     * Information about the current usergroup.
     *
     * @var array
     */
    public $usergroup = [];

    /**
     * BB settings.
     *
     * @var array
     */
    public $settings = [];

    /**
     * Whether or not magic quotes are enabled.
     *
     * @var int
     */
    public $magicquotes = 0;

    /**
     * Whether or not BB supports SEO URLs
     *
     * @var boolean
     */
    public $seo_support = false;

    /**
     * BB configuration.
     *
     * @var array
     */
    public $config = [];

    /**
     * The request method that called this page.
     *
     * @var string
     */
    public $request_method = '';

    /**
     * Whether or not PHP's safe_mode is enabled
     *
     * @var boolean
     */
    public $safemode = false;

    /**
     * Loads templates directly from the master theme and disables the installer locked error
     *
     * @var boolean
     */
    public $dev_mode = false;

    /**
     * Variables that need to be clean.
     *
     * @var array
     */
    public $clean_variables = [
        'int' => [
            'tid', 'pid', 'uid',
            'eid', 'pmid', 'fid',
            'aid', 'rid', 'sid',
            'vid', 'cid', 'bid',
            'hid', 'gid', 'mid',
            'wid', 'lid', 'iid',
            'did', 'qid', 'id'
        ],
        'pos' => [
            'page', 'perpage'
        ],
        'a-z' => [
            'sortby', 'order'
        ]
    ];

    /**
     * Variables that are to be ignored from cleansing process
     *
     * @var array
     */
    public $ignore_clean_variables = [];

    /**
     * Using built in shutdown functionality provided by register_shutdown_function for < PHP 5?
     *
     * @var bool
     */
    public $use_shutdown = true;

    /**
     * Debug mode?
     *
     * @var bool
     */
    public $debug_mode = false;

    /**
     * Binary database fields need to be handled differently
     *
     * @var array
     */
    public $binary_fields = [
        'adminlog' => ['ipaddress' => true],
        'adminsessions' => ['ip' => true],
        'maillogs' => ['ipaddress' => true],
        'moderatorlog' => ['ipaddress' => true],
        'posts' => ['ipaddress' => true],
        'privatemessages' => ['ipaddress' => true],
        'searchlog' => ['ipaddress' => true],
        'sessions' => ['ip' => true],
        'threadratings' => ['ipaddress' => true],
        'users' => ['regip' => true, 'lastip' => true],
        'spamlog' => ['ipaddress' => true],
    ];

    /**
     * The base URL to assets.
     *
     * @var string
     */
    public $asset_url = null;
    /**
     * Base URL for smilies
     *
     * @access public
     * @var string
     */
    public $base_url;

    public $admin_url;

    /**
     * Do this here because the core is used on every BB page
     */
    public $grouppermignore = ['gid', 'type', 'title', 'description', 'namestyle', 'usertitle', 'stars', 'starimage', 'image'];
    public $groupzerogreater = ['pmquota', 'maxpmrecipients', 'maxreputationsday', 'attachquota', 'maxemails', 'maxwarningsday', 'maxposts', 'edittimelimit', 'canusesigxposts', 'maxreputationsperthread', 'emailfloodtime'];
    public $displaygroupfields = ['title', 'description', 'namestyle', 'usertitle', 'stars', 'starimage', 'image'];

    // These are fields in the usergroups table that are also forum permission specific.
    public $fpermfields = [
        'canview',
        'canviewthreads',
        'candlattachments',
        'canpostthreads',
        'canpostreplys',
        'canpostattachments',
        'canratethreads',
        'caneditposts',
        'candeleteposts',
        'candeletethreads',
        'caneditattachments',
        'modposts',
        'modthreads',
        'modattachments',
        'mod_edit_posts',
        'canpostpolls',
        'canvotepolls',
        'cansearch'
    ];
    // An array of valid date formats (Used for user selections etc)
    public $date_formats = [
        1 => 'm-d-Y',
        2 => 'm-d-y',
        3 => 'm.d.Y',
        4 => 'm.d.y',
        5 => 'd-m-Y',
        6 => 'd-m-y',
        7 => 'd.m.Y',
        8 => 'd.m.y',
        9 => 'F jS, Y',
        10 => 'l, F jS, Y',
        11 => 'jS F, Y',
        12 => 'l, jS F, Y',
        // ISO 8601
        13 => 'Y-m-d'
    ];

    // An array of valid time formats (Used for user selections etc)
    public $time_formats = [
        1 => 'h:i a',
        2 => 'h:i A',
        3 => 'H:i'
    ];

    public $currentRoute = '';
    public $shutdown_queries = [];
    public $shutdown_functions = [];
    public $theme = [];

    public $theme_stylesheets = [];
    public $done_shutdown = false;

    public $tcache = [];
    public $fcache = [];
    public $permissioncache = [];
    public $forumpermissions = [
        'canonlyreplyownthreads' => 0,
        'canpostreplys' => 0,
        'candeletethreads' => 0,
        'candeleteposts' => 0
    ];
    public $user_cache = [];
    public $pforumcache = [];
    public $forum_cache = [];
    public $forum_viewers = [];

    public $fid = 0;// forum id
    public $attachcache = [];
    public $smiliecache = [];
    public $groupscache = [];

    public $inlinecookie = '';
    public $nosession = [];
    public $threads = [];

    public $navbits = [];

    public $current_page = '';
    public $collapsedimg = '';
    public $collapsed = '';

    public function initBB()
    {
//xt_start();
        $this->maintimer = new Timer;
        $this->currentRoute = $this->request->getUri()->getPath();
        $this->admin_url = \RunBB\Init::getAdminUrl();

        define('IN_MYBB', 1);
        define('MYBB_ROOT', dirname(__DIR__) . '/');
        define('TIME_NOW', time());

        // Set up BB
        $protected = ['_GET', '_POST', '_SERVER', '_COOKIE', '_FILES', '_ENV', 'GLOBALS'];
        foreach ($protected as $var) {
            if (isset($_POST[$var]) || isset($_GET[$var]) || isset($_COOKIE[$var]) || isset($_FILES[$var])) {
                die('Hacking attempt');
            }
        }

        if (defined('IGNORE_CLEAN_VARS')) {
            if (!is_array(IGNORE_CLEAN_VARS)) {
                $this->ignore_clean_variables = [IGNORE_CLEAN_VARS];
            } else {
                $this->ignore_clean_variables = IGNORE_CLEAN_VARS;
            }
        }

        // Determine input
        $this->parse_incoming($_GET);
        $this->parse_incoming($_POST);

        $this->request_method = strtolower($this->request->getMethod());

        $this->clean_input();

        define('SAFEMODE', false);

        // Are we running on a development server?
        if (isset($_SERVER['MYBB_DEV_MODE']) && $_SERVER['MYBB_DEV_MODE'] == 1) {
            $this->dev_mode = 1;
        }

        // Are we running in debug mode?
        if (isset($this->input['debug']) && $this->input['debug'] == 1) {
            $this->debug_mode = true;
        }

        // load twig helper
        $this->view->addExtension(new TwigGetVarHelper($this));

        require_once MYBB_ROOT . 'inc/functions.php';

        $this->config = [
            'admin_dir' => 'Admin',
            'hide_admin_links' => 0,
            'cache_store' => 'files',//'db';
            'memcache' => [
                'host' => 'localhost',
                'port' => 11211,
            ],
            'super_admins' => '1',
            'database' => [
                'encoding' => 'utf8'
            ],
            'log_pruning' => [
                'admin_logs' => 365, // Administrator logs
                'mod_logs' => 365, // Moderator logs
                'task_logs' => 30, // Scheduled task logs
                'mail_logs' => 180, // Mail error logs
                'user_mail_logs' => 180, // User mail logs
                'promotion_logs' => 180 // Promotion logs
            ],
            'secret_pin' => ''
        ];

        if (!is_array($this->config['database'])) {
            $this->trigger_generic_error('board_not_upgraded');
        }

        // Trigger an error if the installation directory exists
        if (is_dir(MYBB_ROOT . 'install') && !file_exists(MYBB_ROOT . 'install/lock')) {
            $this->trigger_generic_error('install_directory');
        }

        // Check if our DB engine is loaded
        if (!extension_loaded($this->db->engine)) {
            // Throw our super awesome db loading error
            $this->trigger_generic_error('sql_load_error');
//            $this->error('
//            BB was unable to load the SQL extension. Please contact the RunCMF Group for support. <a href="http://www.runcmf.ru">RunBB Website</a>');
        }

        $cfg = $this->get_container()->get('settings')['db']['connections']['mysql'];
        $this->config['database']['type'] = $cfg['driver'];
        $this->config['database']['database'] = $cfg['database'];
        $this->config['database']['table_prefix'] = $cfg['prefix'];
        $this->config['database']['hostname'] = $cfg['host'];
        $this->config['database']['username'] = $cfg['username'];
        $this->config['database']['password'] = $cfg['password'];

        // Connect to Database
        define('TABLE_PREFIX', $this->config['database']['table_prefix']);
        $this->db->connect($this->config['database']);
        $this->db->set_table_prefix(TABLE_PREFIX);
        $this->db->type = $this->config['database']['type'];

        // Load cache
        $this->cache->cache();

        // Load Settings
        if (file_exists(DIR . 'var/cache/forum/settings.php')) {
            $this->settings = require_once DIR . 'var/cache/forum/settings.php';
        }
        if (!file_exists(DIR . 'var/cache/forum/settings.php') || empty($this->settings)) {
            (new \RunBB\Helpers\Restorer($this))->rebuild_settings();
        }

        $settings = [];
        $settings['wolcutoff'] = $this->settings['wolcutoffmins'] * 60;
        $settings['bbname_orig'] = $this->settings['bbname'];
        $settings['bbname'] = strip_tags($this->settings['bbname']);
        $settings['orig_bblanguage'] = $this->settings['bblanguage'];

        // Fix for people who for some specify a trailing slash on the board URL
        if (substr($this->settings['bburl'], -1) == '/') {
            $this->settings['bburl'] = rtrim($this->settings['bburl'], '/');
        }

        // Setup our internal settings and load our encryption key
        $settings['internal'] = $this->cache->read('internal_settings');
        if (!$settings['internal']['encryption_key']) {
            $this->cache->update('internal_settings', ['encryption_key' => random_str(32)]);
            $settings['internal'] = $this->cache->read('internal_settings');
        }
        $this->settings = array_merge($this->settings, $settings);

        $this->parse_cookies();

        $this->asset_url = $this->themes->get_asset_url();

        if ($this->use_shutdown == true) {
            register_shutdown_function([$this, 'run_shutdown']);
        }

        // Did we just upgrade to a new version and haven't run the upgrade scripts yet?
        $version = $this->cache->read('version');
        if (!defined('IN_INSTALL') && !defined('IN_UPGRADE') && $version['version_code'] < $this->version_code) {
            $version_history = $this->cache->read('version_history');
            if (empty($version_history) || file_exists(MYBB_ROOT . 'install/resources/upgrade' . (int)(end($version_history) + 1) . '.php')) {
                $this->trigger_generic_error('board_not_upgraded');
            }
        }

        // Load plugins
        if (!defined('NO_PLUGINS') && !($this->settings['no_plugins'] == 1)) {
            $this->plugins->load();
        }

        // Set up any shutdown functions we need to run globally
        $this->add_shutdown([$this->mail, 'sendMailQueue']);

        /* URL Definitions */
        if ($this->settings['seourls'] == 'yes' || ($this->settings['seourls'] == 'auto' &&
                isset($_SERVER['SEO_SUPPORT']) && $_SERVER['SEO_SUPPORT'] == 1)
        ) {
            $this->seo_support = true;

            define('FORUM_URL', 'forum-{fid}.html');
            define('FORUM_URL_PAGED', 'forum-{fid}-page-{page}.html');
            define('THREAD_URL', 'thread-{tid}.html');
            define('THREAD_URL_PAGED', 'thread-{tid}-page-{page}.html');
            define('THREAD_URL_ACTION', 'thread-{tid}-{action}.html');
            define('THREAD_URL_POST', 'thread-{tid}-post-{pid}.html');
            define('POST_URL', 'post-{pid}.html');
            define('PROFILE_URL', 'user-{uid}.html');
            define('ANNOUNCEMENT_URL', 'announcement-{aid}.html');
            define('CALENDAR_URL', 'calendar-{calendar}.html');
            define('CALENDAR_URL_MONTH', 'calendar-{calendar}-year-{year}-month-{month}.html');
            define('CALENDAR_URL_DAY', 'calendar-{calendar}-year-{year}-month-{month}-day-{day}.html');
            define('CALENDAR_URL_WEEK', 'calendar-{calendar}-week-{week}.html');
            define('EVENT_URL', 'event-{eid}.html');
        } else {
            define('FORUM_URL', $this->settings['bburl'] . '/forumdisplay?fid={fid}');
            define('FORUM_URL_PAGED', $this->settings['bburl'] . '/forumdisplay?fid={fid}&page={page}');
            define('THREAD_URL', $this->settings['bburl'] . '/showthread?tid={tid}');
            define('THREAD_URL_PAGED', $this->settings['bburl'] . '/showthread?tid={tid}&page={page}');
            define('THREAD_URL_ACTION', $this->settings['bburl'] . '/showthread?tid={tid}&action={action}');
            define('THREAD_URL_POST', $this->settings['bburl'] . '/showthread?tid={tid}&pid={pid}');
            define('POST_URL', $this->settings['bburl'] . '/showthread?pid={pid}');
            define('PROFILE_URL', $this->settings['bburl'] . '/profile?uid={uid}');
            define('ANNOUNCEMENT_URL', $this->settings['bburl'] . '/announcements?aid={aid}');
            define('CALENDAR_URL', $this->settings['bburl'] . '/calendar?calendar={calendar}');
            define('CALENDAR_URL_MONTH', $this->settings['bburl'] . '/calendar?calendar={calendar}&year={year}&month={month}');
            define('CALENDAR_URL_DAY', $this->settings['bburl'] . '/calendar/dayview?calendar={calendar}&year={year}&month={month}&day={day}');
            define('CALENDAR_URL_WEEK', $this->settings['bburl'] . '/calendar/weekview?calendar={calendar}&week={week}');
            define('EVENT_URL', $this->settings['bburl'] . '/calendar/event?eid={eid}');
        }
///////////////////////////////////////////////////////
//xt_stop();

        // Read the usergroups cache as well as the moderators cache
        $this->groupscache = $this->cache->read('usergroups');

        // If the groups cache doesn't exist, update it and re-read it
        if (!is_array($this->groupscache)) {
            $this->cache->update_usergroups();
            $this->groupscache = $this->cache->read('usergroups');
        }
        \Tracy\Debugger::barDump($this->request->getUri()->getPath());
        $this->current_page = my_strtolower(basename($this->request->getUri()->getPath()));
        \Tracy\Debugger::barDump($this->current_page);

        // Do not use session system for defined pages
        if ((isset($this->input['action']) && isset($this->nosession[$this->input['action']])) ||
            (isset($this->input['thumbnail']) && $this->current_page == 'attachment')
        ) {
            define('NO_ONLINE', 1);
        }
        $this->session->init();

        $this->user->ismoderator = $this->user->is_moderator('', '', $this->user->uid);

        // Set our POST validation code here
        $this->post_code = $this->generate_post_check();

        // Set and load the language
        if (isset($this->input['language']) &&
            $this->lang->language_exists($this->getInput('language', '', false)) &&
            $this->verify_post_check($this->getInput('my_post_key', '', false), true)
        ) {
            $this->settings['bblanguage'] = $this->getInput('language', '', false);
            // If user is logged in, update their language selection with the new one
            if ($this->user->uid) {
                if (isset($this->cookies['mybblang'])) {
                    $this->my_unsetcookie('mybblang');
                }

//                $this->db->update_query('users',
//                    array('language' => $this->db->escape_string($this->settings['bblanguage'])),
//                    "uid = '{$this->user->uid}'");
                \RunBB\Models\User::where('uid', $this->user->uid)
                    ->update(['language' => $this->settings['bblanguage']]);
            } // Guest = cookie
            else {
                $this->my_setcookie('mybblang', $this->settings['bblanguage']);
            }
            $this->user->language = $this->settings['bblanguage'];
        } // Cookied language!
        elseif (!$this->user->uid && !empty($this->cookies['mybblang']) &&
            $this->lang->language_exists($this->cookies['mybblang'])
        ) {
            $this->settings['bblanguage'] = $this->cookies['mybblang'];
        } elseif (!isset($this->settings['bblanguage'])) {
            $this->settings['bblanguage'] = 'english';
        }

        if (!defined('IN_ADMINCP')) {
            // Load language
            $this->lang->set_language($this->settings['bblanguage']);
            $this->lang->load('global');
            $this->lang->load('messages');
        }

        // Run global_start plugin hook now that the basics are set up
        $this->plugins->runHooks('global_start');

        if (function_exists('mb_internal_encoding') && !empty($this->lang->settings['charset'])) {
            @mb_internal_encoding($this->lang->settings['charset']);
        }
        $this->settings['homeurl'] = $this->request->getUri()->getScheme() . '://' . $this->request->getUri()->getHost();

        // Add some global twig wars
        $this->view->offsetSet('bburl', $this->settings['bburl']);
        $this->view->offsetSet('asset_url', $this->asset_url);


        // Select the board theme to use.
        $loadstyle = '';
        $load_from_forum = $load_from_user = 0;
        $style = [];
        // The user used our new quick theme changer
        if (isset($this->input['theme']) && $this->verify_post_check($this->getInput('my_post_key', ''), true)) {
            // Set up user handler.
            $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this, 'update');

            $user = [
                'uid' => $this->user->uid,
                'style' => $this->getInput('theme', 0),
                'usergroup' => $this->user->usergroup,
                'additionalgroups' => $this->user->additionalgroups
            ];

            $userhandler->set_data($user);

            // validate_user verifies the style if it is set in the data array.
            if ($userhandler->validate_user()) {
                $this->user->style = $user['style'];

                // If user is logged in, update their theme selection with the new one
                if ($this->user->uid) {
                    if (isset($this->cookies['mybbtheme'])) {
                        $this->my_unsetcookie('mybbtheme');
                    }

                    $userhandler->update_user();
                } // Guest = cookie
                else {
                    $this->my_setcookie('mybbtheme', $user['style']);
                }
            }
        } // Cookied theme!
        elseif (!$this->user->uid && !empty($this->cookies['mybbtheme'])) {
            $this->user->style = (int)$this->cookies['mybbtheme'];
        }

        // This user has a custom theme set in their profile
        if (isset($this->user->style) && (int)$this->user->style != 0) {
            //$loadstyle = "tid = '{$this->user->style}'";
            $loadstyle = ['tid' => $this->user->style];
            $load_from_user = 1;
        }

        $valid = [
            'showthread',
            'forumdisplay',
            'newthread',
            'newreply',
            'ratethread',
            'editpost',
            'polls',
            'sendthread',
            'printthread',
            'moderation'
        ];

        if (in_array($this->current_page, $valid)) {
            $this->forum->cache_forums();
            // If we're accessing a post, fetch the forum theme for it and if we're overriding it
            if (isset($this->input['pid']) && $this->current_page !== 'polls') {
//                $query = $this->db->simple_select('posts', 'fid', "pid = '{$this->input['pid']}'", array('limit' => 1));
//                $fid = $this->db->fetch_field($query, 'fid');
                $f = \RunBB\Models\Post::where('pid', $this->getInput('pid', 0))->first(['fid']);
                if (!empty($f->fid)) {
                    $style = $this->forum_cache[$f->fid];
                    $load_from_forum = 1;
                }
            } // We have a thread id and a forum id, we can easily fetch the theme for this forum
            elseif (isset($this->input['tid'])) {
//                $query = $this->db->simple_select('threads', 'fid', "tid = '{$this->input['tid']}'", array('limit' => 1));
//                $fid = $this->db->fetch_field($query, 'fid');
                $fid = \RunBB\Models\Thread::where('tid', '=', $this->input['tid'])->first(['fid']);

                if (!empty($fid->fid)) {
                    $style = $this->forum_cache[$fid->fid];
                    $load_from_forum = 1;
                }
            } // If we're accessing poll results, fetch the forum theme for it and if we're overriding it
            elseif (isset($this->input['pid']) && $this->current_page === 'polls') {
//                $query = $this->db->simple_select('threads', 'fid', "poll = '{$this->input['pid']}'", array('limit' => 1));
//                $fid = $this->db->fetch_field($query, 'fid');
                $fid = \RunBB\Models\Thread::where('poll', '=', $this->input['pid'])->first(['fid']);
                if ($fid->fid) {
                    $style = $this->forum_cache[$fid->fid];
                    $load_from_forum = 1;
                }
            } // We have a forum id - simply load the theme from it
            elseif (isset($this->input['fid']) && isset($this->forum_cache[$this->input['fid']])) {
                $style = $this->forum_cache[$this->input['fid']];
                $load_from_forum = 1;
            }
        }
        unset($valid);

        // From all of the above, a theme was found
        if (isset($style['style']) && $style['style'] > 0) {
            $style['style'] = (int)$style['style'];

            // This theme is forced upon the user, overriding their selection
            if ($style['overridestyle'] == 1 || !isset($this->user->style)) {
                //$loadstyle = "tid = '{$style['style']}'";
                $loadstyle = "tid => '{$style['style']}'";
            }
        }

        // After all of that no theme? Load the board default
        if (empty($loadstyle)) {
            $loadstyle = "def='1'";
        }
        // Fetch the theme to load from the cache
        if ($loadstyle != "def='1'") {
//            $query = $this->db->simple_select('themes', 'name, tid, properties, stylesheets, allowedgroups',
//                $loadstyle, ['limit' => 1]);
//            $this->theme = $this->db->fetch_array($query);
            $this->theme = \RunBB\Models\Theme::where($loadstyle)
                ->select('name', 'tid', 'properties', 'stylesheets', 'allowedgroups')
                ->first()
                ->toArray();

            if (isset($this->theme['tid']) && !$load_from_forum &&
                !$this->user->is_member($this->theme['allowedgroups']) && $this->theme['allowedgroups'] != 'all'
            ) {
                if ($load_from_user == 1) {
//                    $this->db->update_query('users', ['style' => 0],
//                        "style='{$this->user->style}' AND uid='{$this->user->uid}'");
                    \RunBB\Models\User::where([
                        'style' => $this->user->style,
                        'uid' => $this->user->uid
                    ])
                        ->update(['style' => 0]);
                }

                if (isset($this->cookies['mybbtheme'])) {
                    $this->my_unsetcookie('mybbtheme');
                }

                $loadstyle = "def='1'";
            }
        }
        if ($loadstyle == "def='1'") {
            if (!$this->cache->read('default_theme')) {
                $this->cache->update_default_theme();
            }
            $this->theme = $this->cache->read('default_theme');
            $load_from_forum = $load_from_user = 0;
        }

        // No theme was found - we attempt to load the master or any other theme
        if (!isset($this->theme['tid'])) {
            // Missing theme was from a forum, run a query to set any forums using the theme to the default
            if ($load_from_forum == 1) {
//                $this->db->update_query('forums', array('style' => 0), "style = '{$style['style']}'");
                \RunBB\Models\Forum::where('style', $style['style'])
                    ->update(['style' => 0]);
            } // Missing theme was from a user, run a query to set any users using the theme to the default
            elseif ($load_from_user == 1) {
//                $this->db->update_query('users', array('style' => 0), "style = '{$this->user->style}'");
                \RunBB\Models\Forum::where('style', $this->user->style)
                    ->update(['style' => 0]);
            }

            // Attempt to load the master or any other theme if the master is not available
            $query = $this->db->simple_select(
                'themes',
                'name, tid, properties, stylesheets',
                '',
                ['order_by' => 'tid', 'limit' => 1]
            );
            $this->theme = $this->db->fetch_array($query);
        }
        $this->theme = array_merge($this->theme, my_unserialize($this->theme['properties']));
        // Fetch all necessary stylesheets
        $stylesheets = '';
        $this->theme['stylesheets'] = my_unserialize($this->theme['stylesheets']);
        $stylesheet_scripts = ['global', $this->current_page];

        if (!empty($this->theme['color'])) {
            $stylesheet_scripts[] = $this->theme['color'];
        }
        $stylesheet_actions = ['global'];
        if (!empty($this->input['action'])) {
            $stylesheet_actions[] = $this->getInput('action', '', false);
        }

        foreach ($stylesheet_scripts as $stylesheet_script) {
            // Load stylesheets for global actions and the current action
            foreach ($stylesheet_actions as $stylesheet_action) {
                if (!$stylesheet_action) {
                    continue;
                }

                if (!empty($this->theme['stylesheets'][$stylesheet_script][$stylesheet_action])) {
                    // Actually add the stylesheets to the list
                    foreach ($this->theme['stylesheets'][$stylesheet_script][$stylesheet_action] as $page_stylesheet) {
                        if (!empty($already_loaded[$page_stylesheet])) {
                            continue;
                        }

                        if (strpos($page_stylesheet, 'css?') !== false) {
                            $stylesheet_url = $this->settings['bburl'] . '/' . $page_stylesheet;
                        } else {
                            $stylesheet_url = '/' . $page_stylesheet;//$this->themes->get_asset_url($page_stylesheet);FIXME
                        }

                        if ($this->settings['minifycss']) {
                            $stylesheet_url = str_replace('.css', '.min.css', $stylesheet_url);
                        }

                        if (strpos($page_stylesheet, 'css?') !== false) {
                            // We need some modification to get it working with the displayorder
                            $query_string = parse_url($stylesheet_url, PHP_URL_QUERY);
                            $id = (int)my_substr($query_string, 11);
                            $query = $this->db->simple_select('themestylesheets', 'name', "sid={$id}");
                            $real_name = $this->db->fetch_field($query, 'name');
                            $stylesheet_url = '/themes/theme' . $this->theme['tid'] . '/' . $real_name;
                            $this->theme_stylesheets[$real_name] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$stylesheet_url}\" />\n";
                        } else {
                            $this->theme_stylesheets[basename($page_stylesheet)] = "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$stylesheet_url}\" />\n";
                        }
                        $already_loaded[$page_stylesheet] = 1;
                    }
                }
            }
        }
        unset($actions);

        if (!empty($this->theme_stylesheets) && is_array($this->theme['disporder'])) {
            foreach ($this->theme['disporder'] as $style_name => $order) {
                if (!empty($this->theme_stylesheets[$style_name])) {
                    $stylesheets .= $this->theme_stylesheets[$style_name];
                }
            }
        }

        $this->view->offsetSet('stylesheets', $stylesheets);

        // Are we linking to a remote theme server?
        if (my_substr($this->theme['imgdir'], 0, 7) == 'http://' || my_substr($this->theme['imgdir'], 0, 8) == 'https://') {
            // If a language directory for the current language exists within the theme - we use it
            if (!empty($this->user->language)) {
                $this->theme['imglangdir'] = $this->theme['imgdir'] . '/' . $this->user->language;
            } else {
                // Check if a custom language directory exists for this theme
                if (!empty($this->settings['bblanguage'])) {
                    $this->theme['imglangdir'] = $this->theme['imgdir'] . '/' . $this->settings['bblanguage'];
                } // Otherwise, the image language directory is the same as the language directory for the theme
                else {
                    $this->theme['imglangdir'] = $this->theme['imgdir'];
                }
            }
        } else {
            $img_directory = $this->theme['imgdir'];

            if ($this->settings['usecdn'] && !empty($this->settings['cdnpath'])) {
                $img_directory = rtrim($this->settings['cdnpath'], '/') . '/' . ltrim($this->theme['imgdir'], '/');
            }

            if (!@is_dir($img_directory)) {
                $this->theme['imgdir'] = 'images';
            }

            // If a language directory for the current language exists within the theme - we use it
            if (!empty($this->user->language) && is_dir($img_directory . '/' . $this->user->language)) {
                $this->theme['imglangdir'] = $this->theme['imgdir'] . '/' . $this->user->language;
            } else {
                // Check if a custom language directory exists for this theme
                if (is_dir($img_directory . '/' . $this->settings['bblanguage'])) {
                    $this->theme['imglangdir'] = $this->theme['imgdir'] . '/' . $this->settings['bblanguage'];
                } // Otherwise, the image language directory is the same as the language directory for the theme
                else {
                    $this->theme['imglangdir'] = $this->theme['imgdir'];
                }
            }
            $this->theme['imgdir'] = $this->themes->get_asset_url($this->theme['imgdir']);
            $this->theme['imglangdir'] = $this->themes->get_asset_url($this->theme['imglangdir']);
        }

        // Theme logo - is it a relative URL to the forum root? Append bburl
        if (!preg_match("#^(\.\.?(/|$)|([a-z0-9]+)://)#i", $this->theme['logo']) &&
            substr($this->theme['logo'], 0, 1) != '/'
        ) {
            $this->theme['logo'] = $this->themes->get_asset_url($this->theme['logo']);
        }

        // Set the current date and time now
        $this->lang->welcome_current_time = $this->lang->sprintf(
            $this->lang->welcome_current_time,
            $this->time->formatDate($this->settings['dateformat'], TIME_NOW, '', false) .
            $this->lang->comma . $this->time->formatDate($this->settings['timeformat'], TIME_NOW)
        );

        // Format the last visit date of this user appropriately
        if (isset($this->user->lastvisit)) {
            $lastvisit = $this->time->formatDate('relative', $this->user->lastvisit, '', 2);
        } // Otherwise, they've never visited before
        else {
            $lastvisit = $this->lang->lastvisit_never;
        }
        $this->view->offsetSet('lastvisit', $lastvisit);

        $this->plugins->runHooks('global_intermediate');


        // Prepare the main templates for use
        $admincplink = $modcplink = '';
        // Load appropriate welcome block for the current logged in user
        if ($this->user->uid != 0) {
            // Format the welcome back message
            $this->lang->welcome_back = $this->lang->sprintf(
                $this->lang->welcome_back,
                $this->user->build_profile_link($this->user->username, $this->user->uid),
                $lastvisit
            );

            // Tell the user their PM usage
            $this->lang->welcome_pms_usage = $this->lang->sprintf(
                $this->lang->welcome_pms_usage,
                $this->parser->formatNumber($this->user->pms_unread),
                $this->parser->formatNumber($this->user->pms_total)
            );
        } else // Otherwise, we have a guest
        {
            switch ($this->settings['username_method']) {
                case 0:
                    $this->login_username = $this->lang->login_username;
                    break;
                case 1:
                    $this->login_username = $this->lang->login_username1;
                    break;
                case 2:
                    $this->login_username = $this->lang->login_username2;
                    break;
                default:
                    $this->login_username = $this->lang->login_username;
                    break;
            }
        }

        // Display menu links and quick search if user has permission
        $menu_search = $menu_memberlist = $menu_portal = $menu_calendar = $quicksearch = '';

        // See if there are any pending join requests for group leaders
        $this->showPendingJoinRequests = false;
        $groupleaders = $this->cache->read('groupleaders');
        if ($this->user->uid != 0 && is_array($groupleaders) && array_key_exists($this->user->uid, $groupleaders)) {
            $groupleader = $groupleaders[$this->user->uid];
//            $gids = "'0'";
            $gids[]=0;
            foreach ($groupleader as $user) {
                if ($user['canmanagerequests'] != 1) {
                    continue;
                }
//                $user['gid'] = (int)$user['gid'];
//                $gids .= ",'{$user['gid']}'";
                $gids[]=$user['gid'];
            }

//            $query = $this->db->simple_select('joinrequests', 'COUNT(uid) as total', "gid IN ({$gids}) AND invite='0'");
            //SELECT COUNT(uid) as total FROM mybb_joinrequests WHERE gid IN ('0','4') AND invite='0'
//            $total_joinrequests = $this->db->fetch_field($query, 'total');
            $total_joinrequests = \RunBB\Models\Joinrequest::whereIn('gid', $gids)
                ->where('invite', '0')
//                ->get(['uid'])
                ->count();

            if ($total_joinrequests > 0) {
                $this->showPendingJoinRequests = true;
                if ($total_joinrequests == 1) {
                    $this->lang->pending_joinrequests = $this->lang->pending_joinrequest;
                } else {
                    $total_joinrequests = $this->formatNumber($total_joinrequests);
                    $this->lang->pending_joinrequests = $this->lang->sprintf($this->lang->pending_joinrequests, $total_joinrequests);
                }
            }
        }

        $this->showUnreadReports = false;
        // This user is a moderator, super moderator or administrator
        if ($this->usergroup['cancp'] == 1 ||
            ($this->user->ismoderator &&
                $this->usergroup['canmodcp'] == 1 &&
                $this->usergroup['canmanagereportedcontent'] == 1)
        ) {
            // Only worth checking if we are here because we have ACP permissions and the other condition fails
            if ($this->usergroup['cancp'] == 1 && !($this->user->ismoderator &&
                    $this->usergroup['canmodcp'] == 1 && $this->usergroup['canmanagereportedcontent'] == 1)
            ) {
                // First we check if the user's a super admin: if yes, we don't care about permissions
                $can_access_moderationqueue = true;
//                $is_super_admin = $this->user->is_super_admin($recipient['uid']);
//                if (!$is_super_admin) {
                if (!$this->user->is_super_admin($this->user->uid)) {
                    // Include admin functions
                    if (!file_exists(MYBB_ROOT . $this->config['admin_dir'] . "/inc/functions.php")) {
                        $can_access_moderationqueue = false;
                    }

                    require_once MYBB_ROOT . $this->config['admin_dir'] . "/inc/functions.php";

                    // Verify if we have permissions to access forum-moderation_queue
//                    require_once MYBB_ROOT . $this->config['admin_dir'] . "/modules/forum/module_meta.php";
                    if (function_exists("forum_admin_permissions")) {
                        // Get admin permissions
                        $adminperms = $this->get_admin_permissions($this->user->uid);

                        $permissions = forum_admin_permissions();
                        if (array_key_exists('moderation_queue', $permissions['permissions']) &&
                            $adminperms['forum']['moderation_queue'] != 1
                        ) {
                            $can_access_moderationqueue = false;
                        }
                    }
                }
            } else {
                $can_access_moderationqueue = false;
            }

            if ($can_access_moderationqueue ||
                ($this->user->ismoderator &&
                    $this->usergroup['canmodcp'] == 1 &&
                    $this->usergroup['canmanagereportedcontent'] == 1)
            ) {
                // Read the reported content cache
                $reported = $this->cache->read('reportedcontent');

                // 0 or more reported items currently exist
                if ($reported['unread'] > 0) {
                    $this->showUnreadReports = true;
                    // We want to avoid one extra query for users that can moderate any forum
                    if ($this->usergroup['cancp'] || $this->usergroup['issupermod']) {
                        $unread = (int)$reported['unread'];
                    } else {
                        $unread = 0;
                        $query = $this->db->simple_select('reportedcontent', 'id3', "reportstatus='0' AND (type = 'post' OR type = '')");

                        while ($fid = $this->db->fetch_field($query, 'id3')) {
                            if ($this->user->is_moderator($fid, "canmanagereportedposts")) {
                                ++$unread;
                            }
                        }
                    }

                    if ($unread > 0) {
                        if ($unread == 1) {
                            $this->lang->unread_reports = $this->lang->unread_report;
                        } else {
                            $this->lang->unread_reports = $this->lang->sprintf($this->lang->unread_reports, $this->formatNumber($unread));
                        }
                    }
                }
            }
        }

        // Got a character set?
        $charset = 'UTF-8';
        if (isset($this->lang->settings['charset']) && $this->lang->settings['charset']) {
            $charset = $this->lang->settings['charset'];
        }
        $this->view->offsetSet('charset', $charset);

        // Is this user apart of a banned group?
        if ($this->usergroup['isbannedgroup'] == 1) {
            // Fetch details on their ban
            $query = $this->db->simple_select('banned', '*', "uid = '{$this->user->uid}'", ['limit' => 1]);
            $ban = $this->db->fetch_array($query);

            if ($ban['uid']) {
                // Format their ban lift date and reason appropriately
                $banlift = $this->lang->banned_lifted_never;
                $reason = htmlspecialchars_uni($ban['reason']);

                if ($ban['lifted'] > 0) {
                    $banlift = $this->time->formatDate(
                        $this->settings['dateformat'],
                        $ban['lifted']
                    ) . $this->lang->comma . $this->time->formatDate($this->settings['timeformat'], $ban['lifted']);
                }
            }

            if (empty($reason)) {
                $reason = $this->lang->unknown;
            }

            if (empty($banlift)) {
                $banlift = $this->lang->unknown;
            }
            $this->view->offsetSet('banlift', $banlift);
            $this->view->offsetSet('reason', $reason);
        }

        $this->lang->ajax_loading = str_replace("'", "\\'", $this->lang->ajax_loading);

        // Check if this user has a new private message.
        $this->showPMnotice = false;
//        if (isset($this->user->pmnotice) && $this->user->pmnotice == 2 &&
//FIXME why pmnotice == 2 ???
        if (isset($this->user->pmnotice) && $this->user->pmnotice > 0 &&
            $this->user->pms_unread > 0 && $this->settings['enablepms'] != 0 &&
            $this->usergroup['canusepms'] != 0 && $this->usergroup['canview'] != 0 &&
            ($this->current_page !== 'private' || $this->getInput('action', '') !== 'read')
        ) {
            $this->showPMnotice = true;
//            $query = $this->db->query("
//		SELECT pm.subject, pm.pmid, fu.username AS fromusername, fu.uid AS fromuid
//		FROM " . TABLE_PREFIX . "privatemessages pm
//		LEFT JOIN " . TABLE_PREFIX . "users fu on (fu.uid=pm.fromid)
//		WHERE pm.folder = '1'
//		    AND pm.uid = '{$this->user->uid}'
//		    AND pm.status = '0'
//		ORDER BY pm.dateline DESC
//		LIMIT 1
//	");
//            $pm = $this->db->fetch_array($query);
            $pm = \RunBB\Models\Privatemessage::where([
                ['privatemessages.folder', '=', '1'],
                ['privatemessages.uid', '=', $this->user->uid],
                ['privatemessages.status', '=', '0']
            ])
                ->leftJoin('users', 'users.uid', '=', 'privatemessages.fromid')
                ->orderBy('privatemessages.dateline', 'desc')
                ->get([
                    'privatemessages.subject',
                    'privatemessages.pmid',
                    'users.username AS fromusername',
                    'users.uid AS fromuid'
                ])
                ->first()
                ->toArray();

            $pm['subject'] = $this->parser->parse_badwords($pm['subject']);

            if ($pm['fromuid'] == 0) {
                $pm['fromusername'] = $this->lang->mybb_engine;
                $user_text = $pm['fromusername'];
            } else {
                $user_text = $this->user->build_profile_link($pm['fromusername'], $pm['fromuid']);
            }

            if ($this->user->pms_unread == 1) {
                $this->privatemessage_text = $this->lang->sprintf(
                    $this->lang->newpm_notice_one,
                    $user_text,
                    $this->settings['bburl'],
                    $pm['pmid'],
                    htmlspecialchars_uni($pm['subject'])
                );
            } else {
                $this->privatemessage_text = $this->lang->sprintf(
                    $this->lang->newpm_notice_multiple,
                    $this->user->pms_unread,
                    $user_text,
                    $this->settings['bburl'],
                    $pm['pmid'],
                    htmlspecialchars_uni($pm['subject'])
                );
            }
        }

        $this->showAwaitingUsers = false;
        if ($this->settings['awactialert'] == 1 && $this->usergroup['cancp'] == 1) {
            $awaitingusers = $this->cache->read('awaitingactivation');

            if (isset($awaitingusers['time']) && $awaitingusers['time'] + 86400 < TIME_NOW) {
                $this->cache->update_awaitingactivation();
                $awaitingusers = $this->cache->read('awaitingactivation');
            }

            if (!empty($awaitingusers['users'])) {
                $awaitingusers = (int)$awaitingusers['users'];
            } else {
                $awaitingusers = 0;
            }

            if ($awaitingusers < 1) {
                $awaitingusers = 0;
            } else {
                $awaitingusers = $this->formatNumber($awaitingusers);
            }

            if ($awaitingusers > 0) {
                $this->showAwaitingUsers = true;
                if ($awaitingusers == 1) {
                    $this->awaiting_message = $this->lang->awaiting_message_single;
                } else {
                    $this->awaiting_message = $this->lang->sprintf($this->lang->awaiting_message_plural, $awaitingusers);
                }

                if ($admincplink) {
                    $this->awaiting_message .= $this->lang->sprintf($this->lang->awaiting_message_link, $this->settings['bburl'], $this->config['admin_dir']);
                }
            }
        }

        $this->view->offsetSet('copy_year', $this->time->formatDate('Y', TIME_NOW));

        // Are we showing version numbers in the footer?
        $bbversion = ($this->settings['showvernum'] == 1) ? ' ' . $this->version : '';
        $this->view->offsetSet('bbversion', $bbversion);

        // Check to see if we have any tasks to run
        $task_image = '';
        $task_cache = $this->cache->read('tasks');
        if (!$task_cache['nextrun']) {
            $task_cache['nextrun'] = TIME_NOW;
        }

        if ($task_cache['nextrun'] <= TIME_NOW) {
            $task_image = '<img src="' . $this->settings['bburl'] . '/task" width="1" height="1" alt="" />';
        }
        $this->view->offsetSet('task_image', $task_image);

        // Are we showing the quick language selection box?
        //$lang_select = $lang_options = '';
        $this->showLangSelect = false;
        if ($this->settings['showlanguageselect'] != 0) {
            $languages = $this->lang->get_languages();

            if (count($languages) > 1) {
                $this->showLangSelect = true;
                foreach ($languages as $key => $language) {
                    $language = htmlspecialchars_uni($language);

                    // Current language matches
                    if ($this->lang->language == $key) {
                        $selected = ' selected="selected"';
                    } else {
                        $selected = '';
                    }
                    $lang_options[] = [
                        'key' => $key,
                        'selected' => $selected,
                        'language' => $language
                    ];
                }
                $this->view->offsetSet('lang_options', $lang_options);
                $this->view->offsetSet('lang_redirect_url', $this->session->get_current_location(true, ['language']));
            }
        }

        // Are we showing the quick theme selection box?
        //$theme_select = $theme_options = '';
        $this->showThemeSelect = false;
        if ($this->settings['showthemeselect'] != 0) {
            if (!isset($this->user->style)) {
                $this->user->style = 0;
            }
            $theme_options = $this->themes->build_theme_select('theme', $this->user->style, 0, '', false, true);
            $this->view->offsetSet('theme_options', $theme_options);
            if (!empty($theme_options)) {
                $this->showThemeSelect = true;
                $this->view->offsetSet('theme_redirect_url', $this->session->get_current_location(true, ['theme']));
            }
        }

        // If we use the contact form, show 'Contact Us' link when appropriate
        $contact_us = '';
        if (($this->settings['contactlink'] == "contact" &&
                $this->settings['contact'] == 1 &&
                ($this->settings['contact_guests'] != 1 &&
                    $this->user->uid == 0 || $this->user->uid > 0)) ||
            $this->settings['contactlink'] != "contact"
        ) {
            if (my_substr($this->settings['contactlink'], 0, 1) != '/' &&
                my_substr($this->settings['contactlink'], 0, 7) != 'http://' &&
                my_substr($this->settings['contactlink'], 0, 8) != 'https://' &&
                my_substr($this->settings['contactlink'], 0, 7) != 'mailto:'
            ) {
                $this->settings['contactlink'] = $this->settings['bburl'] . '/' . $this->settings['contactlink'];
            }
            $contact_us = '<li><a href="' . $this->settings['contactlink'] . '">' . $this->lang->bottomlinks_contactus . '</a></li>';
        }
        $this->view->offsetSet('contact_us', $contact_us);

        // DST Auto detection enabled?
        $auto_dst_detection = '';
        if ($this->user->uid > 0 && $this->user->dstcorrection == 2) {
            $auto_dst_detection = "<script type=\"text/javascript\">if(MyBB) { $([document, window]).bind(\"load\", function() { MyBB.detectDSTChange('" . ($this->user->timezone + $this->user->dst) . "'); }); }</script>\n";
        }
        $this->view->offsetSet('auto_dst_detection', $auto_dst_detection);

        // Add our main parts to the navigation
        $this->navbits[0]['name'] = $this->settings['bbname_orig'];
        $this->navbits[0]['url'] = $this->settings['bburl'];

        // Set the link to the archive.
        $this->view->offsetSet('archive_url', $this->build_archive_link());

        // Check banned ip addresses
        if ($this->ban->is_banned_ip($this->session->ipaddress, true)) {
            if ($this->user->uid) {
                \RunBB\Models\Session::where('ip', $this->session->ipaddress)
                    ->orWhere('uid', $this->user->uid)
                    ->delete();
            } else {
                \RunBB\Models\Session::where('ip', $this->session->ipaddress)->delete();
            }
            $this->error($this->lang->error_banned);
            exit;
        }

        $closed_bypass = [
            'member' => [
                'login',
                'logout',
            ],
            'captcha',
        ];

        // If the board is closed, the user is not an administrator and they're not trying to login, show the board closed message
        if ($this->settings['boardclosed'] == 1 &&
            $this->usergroup['canviewboardclosed'] != 1 &&
            !in_array($this->current_page, $closed_bypass) &&
            (!is_array($closed_bypass[$this->current_page]) ||
                !in_array($this->getInput('action', '', false), $closed_bypass[$this->current_page]))
        ) {
            // Show error
            if (!$this->settings['boardclosed_reason']) {
                $this->settings['boardclosed_reason'] = $this->lang->boardclosed_reason;
            }

            $this->lang->error_boardclosed .= "<blockquote>{$this->settings['boardclosed_reason']}</blockquote>";

            if (!$this->getInput('modal', '', false)) {
                $this->error($this->lang->error_boardclosed);
            } else {
                $output = '';
                eval('$output = "' . $this->templates->get('global_board_offline_modal', 1, 0) . '";');
                echo($output);
            }
            exit;
        }

        $force_bypass = [
            'member' => [
                'login',
                'do_login',
                'logout',
                'register',
                'do_register',
                'lostpw',
                'do_lostpw',
                'activate',
                'resendactivation',
                'do_resendactivation',
                'resetpassword',
            ],
            'captcha',
        ];

        // If the board forces user to login/register, and the user is a guest, show the force login message
        if ($this->settings['forcelogin'] == 1 &&
            $this->user->uid == 0 &&
            !in_array($this->current_page, $force_bypass) &&
            (!is_array($force_bypass[$this->current_page]) ||
                !in_array(
                    $this->getInput('action', '', false),
                    $force_bypass[$this->current_page]
                ))
        ) {
            // Show error
            $this->error_no_permission();
            exit;
        }

        // Load Limiting
        if ($this->usergroup['cancp'] != 1 && $this->settings['load'] > 0 &&
            ($load = get_server_load($this->lang->unknown)) && $load != $this->lang->unknown && $load > $this->settings['load']
        ) {
            // User is not an administrator and the load limit is higher than the limit, show an error
            $this->error($this->lang->error_loadlimit);
            exit;
        }

        // If there is a valid referrer in the URL, cookie it
        if (!$this->user->uid && $this->settings['usereferrals'] == 1 &&
            (isset($this->input['referrer']) || isset($this->input['referrername']))
        ) {
            if (isset($this->input['referrername'])) {
                $condition = "username = '" . $this->db->escape_string($this->getInput('referrername', '', false)) . "'";
            } else {
                $condition = "uid = '" . $this->getInput('referrer', 0) . "'";
            }

            $query = $this->db->simple_select('users', 'uid', $condition, ['limit' => 1]);
            $referrer = $this->db->fetch_array($query);

            if ($referrer['uid']) {
                $this->my_setcookie('mybb[referrer]', $referrer['uid']);
            }
        }

        if ($this->usergroup['canview'] != 1) {
            // Check pages allowable even when not allowed to view board
            if (defined('ALLOWABLE_PAGE')) {
                if (is_string(ALLOWABLE_PAGE)) {
                    $allowable_actions = explode(',', ALLOWABLE_PAGE);
                    if (!in_array($this->getInput('action', ''), $allowable_actions)) {
                        $this->error_no_permission();
                    }

                    unset($allowable_actions);
                } elseif (ALLOWABLE_PAGE !== 1) {
                    $this->error_no_permission();
                }
            } else {
                $this->error_no_permission();
            }
            exit;
        }

        // Find out if this user of ours is using a banned email address.
        // If they are, redirect them to change it
        if ($this->user->uid && $this->ban->is_banned_email($this->user->email) && $this->settings['emailkeep'] != 1) {
            if ($this->current_page !== 'usercp' ||
                ($this->current_page === 'usercp' &&
                    $this->getInput('action', '', false) !== 'email')
            ) {
                $this->redirect($this->settings['bburl'] . '/usercp/email');
                exit;
            } elseif ($this->request_method != 'post') {
//                $banned_email_error = $this->inline_error(array($this->lang->banned_email_warning));
                $this->bbErrorHandler->setWarning(
                    $this->inline_error([$this->lang->banned_email_warning])
                );
            }
        }

        // work out which items the user has collapsed
        $colcookie = '';
        if (!empty($this->cookies['collapsed'])) {
            $colcookie = $this->cookies['collapsed'];
        }

        // set up collapsable items (to automatically show them us expanded)
        $this->collapsedimg = $this->collapsed =
            ['boardstats' => '', 'boardstats_e' => '', 'quickreply' => '', 'quickreply_e' => ''];

        if ($colcookie) {
            $col = explode('|', $colcookie);
            if (!is_array($col)) {
                $col[0] = $colcookie; // only one item
            }
            $this->collapsed = [];
            foreach ($col as $key => $val) {
                $ex = $val . '_e';
                $co = $val . '_c';
                $this->collapsed[$co] = 'display: show;';
                $this->collapsed[$ex] = 'display: none;';
                $this->collapsedimg[$val] = '_collapsed';
                $collapsedthead[$val] = ' thead_collapsed';
            }
            $this->view->offsetSet('collapsedthead', $collapsedthead);
        }
        $this->view->offsetSet('collapsedimg', $this->collapsedimg);
        $this->view->offsetSet('collapsed', $this->collapsed);

        // Run hooks for end of global.php
        $this->plugins->runHooks('global_end');

        // init core & user menu
        $this->view->offsetSet('user_menu', $this->module->getUserMenu());

        $this->globaltime = $this->maintimer->getTime();
    }

    /**
     * Adds a function or class to the list of code to run on shutdown.
     *
     * @param string|array $name The name of the function.
     * @param mixed $arguments Either an array of arguments for the function or one argument
     * @return boolean True if function exists, otherwise false.
     */
    public function add_shutdown($name, array $arguments = [])
    {
        if (!is_array($this->shutdown_functions)) {
            $this->shutdown_functions = [];
        }

        if (!is_array($arguments)) {
            $arguments = [$arguments];
        }

        if (is_array($name) && method_exists($name[0], $name[1])) {
            $this->shutdown_functions[] = ['function' => $name, 'arguments' => $arguments];
            return true;
        } elseif (!is_array($name) && function_exists($name)) {
            $this->shutdown_functions[] = ['function' => $name, 'arguments' => $arguments];
            return true;
        }

        return false;
    }

    /**
     * Runs the shutdown items after the page has been sent to the browser.
     *
     */
    public function run_shutdown()
    {
        if ($this->done_shutdown == true ||
            !$this->config ||
            !empty($this->bbErrorHandler->has_errors)
        ) {
            return;
        }

        if (empty($this->shutdown_queries)) {// && empty($this->shutdown_functions))
        // Nothing to do
            return;
        }

        // Cache object deconstructed? reconstruct
        $this->cache->cache();

        // And finally.. plugins
        if (!defined('NO_PLUGINS') && !($this->settings['no_plugins'] == 1)) {
            $this->plugins->load();
        }

        // We have some shutdown queries needing to be run
        if (is_array($this->shutdown_queries)) {
            // Loop through and run them all
            foreach ($this->shutdown_queries as $query) {
                $this->db->query($query);
            }
        }

        // Run any shutdown functions if we have them
        if (!empty($this->shutdown_functions)) {
            foreach ($this->shutdown_functions as $function) {
                call_user_func_array($function['function'], $function['arguments']);
            }
        }

        $this->done_shutdown = true;
    }

    public function __destruct()
    {
        // Run shutdown function
        $this->run_shutdown();
    }

    /**
     * BB setcookie() wrapper.
     *
     * @param string $name The cookie identifier.
     * @param string $value The cookie value.
     * @param int|string $expires The timestamp of the expiry date.
     * @param boolean $httponly True if setting a HttpOnly cookie (supported by the majority of web browsers)
     */
    public function my_setcookie($name, $value = '', $expires = '', $httponly = false)
    {
//        if (!$this->settings['cookiepath']) {
            $this->settings['cookiepath'] = '/';
//        }

        if ($expires == -1) {
            $expires = 0;
        } elseif ($expires == '' || $expires == null) {
            $expires = TIME_NOW + (60 * 60 * 24 * 365); // Make the cookie expire in a years time
        } else {
            $expires = TIME_NOW + (int)$expires;
        }

        $this->settings['cookiepath'] = str_replace(["\n", "\r"], '', $this->settings['cookiepath']);
        $this->settings['cookiedomain'] = str_replace(["\n", "\r"], '', '.' . $this->request->getUri()->getHost());//$this->settings['cookiedomain']);
        $this->settings['cookieprefix'] = str_replace(["\n", "\r", ' '], '', $this->settings['cookieprefix']);

        // Versions of PHP prior to 5.2 do not support HttpOnly cookies and IE is buggy when specifying a blank domain so set the cookie manually
        $cookie = "Set-Cookie: {$this->settings['cookieprefix']}{$name}=" . urlencode($value);

        if ($expires > 0) {
            $cookie .= '; expires=' . @gmdate('D, d-M-Y H:i:s \\G\\M\\T', $expires);
        }

        if (!empty($this->settings['cookiepath'])) {
            $cookie .= "; path={$this->settings['cookiepath']}";
        }

        if (!empty($this->settings['cookiedomain'])) {
            $cookie .= "; domain={$this->settings['cookiedomain']}";
        }

        if ($httponly == true) {
            $cookie .= '; HttpOnly';
        }

        $this->cookies[$name] = $value;

        header($cookie, false);
    }

    /**
     * Unset a cookie set by BB.
     *
     * @param string $name The cookie identifier.
     */
    public function my_unsetcookie($name)
    {
        $expires = -3600;
        $this->my_setcookie($name, '', $expires);

        unset($this->cookies[$name]);
    }

    /**
     * Get the contents from a serialised cookie array.
     *
     * @param string $name The cookie identifier.
     * @param int $id The cookie content id.
     * @return array|boolean The cookie id's content array or false when non-existent.
     */
    public function my_get_array_cookie($name, $id)
    {
        if (!isset($this->cookies['mybb'][$name])) {
            return false;
        }

        $cookie = my_unserialize($this->cookies['mybb'][$name]);

        if (is_array($cookie) && isset($cookie[$id])) {
            return $cookie[$id];
        } else {
            return 0;
        }
    }

    /**
     * Set a serialised cookie array.
     *
     * @param string $name The cookie identifier.
     * @param int $id The cookie content id.
     * @param string $value The value to set the cookie to.
     * @param int|string $expires The timestamp of the expiry date.
     */
    public function my_set_array_cookie($name, $id, $value, $expires = '')
    {
        $cookie = $this->cookies['mybb'] ?: '';
        if (isset($cookie[$name])) {
            $newcookie = my_unserialize($cookie[$name]);
        } else {
            $newcookie = [];
        }

        $newcookie[$id] = $value;
        $newcookie = my_serialize($newcookie);
        $this->my_setcookie("mybb[$name]", addslashes($newcookie), $expires);

        // Make sure our current viarables are up-to-date as well
        $this->cookies['mybb'][$name] = $newcookie;
    }

    /**
     * Generates a unique code for POST requests to prevent XSS/CSRF attacks
     *
     * @return string The generated code
     */
    public function generate_post_check()
    {
        if ($this->user->uid) {
            return md5($this->user->loginkey . $this->user->salt . $this->user->regdate);
        } // Guests get a special string
        else {
            return md5($this->session->useragent .
                $this->config['database']['username'] .
                $this->settings['internal']['encryption_key']);
        }
    }

    /**
     * Verifies a POST check code is valid, if not shows an error (silently returns false on silent parameter)
     *
     * @param string $code The incoming POST check code
     * @param boolean $silent Silent mode or not (silent mode will not show the error to the user but returns false)
     * @return bool
     */
    public function verify_post_check($code, $silent = false)
    {
        if ($this->generate_post_check() != $code) {
            if ($silent == true) {
                return false;
            } else {
                if (defined('IN_ADMINCP')) {
                    return false;
                } else {
                    return $this->error($this->lang->invalid_post_code);
                }
            }
        } else {
            return true;
        }
    }

    /**
     * Builds a URL to an archive mode page
     *
     * @param string $type The type of page (thread|announcement|forum)
     * @param int $id The ID of the item
     * @return string The URL
     */
    public function build_archive_link($type = '', $id = 0)
    {
        if ($this->settings['seourls_archive'] == 1) {
            $base_url = $this->settings['bburl'] . '/archive';
        } else {
            $base_url = $this->settings['bburl'] . '/archive?';
        }

        switch ($type) {
            case 'thread':
                $url = "{$base_url}thread-{$id}.html";
                break;
            case 'announcement':
                $url = "{$base_url}announcement-{$id}.html";
                break;
            case 'forum':
                $url = "{$base_url}forum-{$id}.html";
                break;
            default:
                $url = $base_url;
        }

        return $url;
    }

    /**
     * Checks whether the administrator is on a mobile device
     *
     * @param string $useragent The useragent to be checked
     * @return boolean A true/false depending on if the administrator is on a mobile
     */
    public function is_mobile($useragent)
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $useragent);
    }

    /**
     * Build the breadcrumb navigation trail from the specified items
     *
     * @return string The formatted breadcrumb navigation trail
     */
    protected function build_breadcrumb()
    {
        static $nav;
        $navsep = ' ';
        $i = 0;
        $activesep = '';

        if (is_array($this->navbits)) {
            reset($this->navbits);
            foreach ($this->navbits as $key => $navbit) {
                if (isset($this->navbits[$key + 1])) {
                    if (isset($this->navbits[$key + 2])) {
                        $sep = $navsep;
                    } else {
                        $sep = ' ';
                    }

                    $multipage = null;
                    $multipage_dropdown = null;
                    if (!empty($navbit['multipage'])) {
                        if (!$this->settings['threadsperpage'] || (int)$this->settings['threadsperpage'] < 1) {
                            $this->settings['threadsperpage'] = 20;
                        }

                        $multipage = $this->pagination->multipage(
                            $navbit['multipage']['num_threads'],
                            $this->settings['threadsperpage'],
                            $navbit['multipage']['current_page'],
                            $navbit['multipage']['url'],
                            true
                        );
                        if ($multipage) {
                            ++$i;
                            $multipage_dropdown = "<img src=\"{$this->theme['imgdir']}/arrow_down.png\" alt=\"v\" title=\"\" class=\"pagination_breadcrumb_link\" id=\"breadcrumb_multipage\" />{$multipage}";
                            //ev al("\$multipage_dropdown = \"" . $this->templates->get("nav_dropdown") . "\";");
                            $sep = $multipage_dropdown . $sep;
                        }
                    }

                    // Replace page 1 URLs
                    $navbit['url'] = str_replace("-page-1.html", ".html", $navbit['url']);
                    $navbit['url'] = preg_replace("/&amp;page=1$/", "", $navbit['url']);

                    $nav .= ' <li><a href="' . $navbit['url'] . '">' . $navbit['name'] . '</a></li> ' . $sep;
                    //ev al("\$nav .= \"" . $this->templates->get("nav_bit") . "\";");
                }
            }
        }

        $activesep = ' ';
        $navsize = count($this->navbits);
        $navbit = $this->navbits[$navsize - 1];

        $donenav = '
        <div id="container">
            <ol class="breadcrumb">
                <li>
                    <a href="' . $this->settings['bburl'] . '" title="' . $this->settings['bbname'] . '"><i class="fa fa-home"></i></a>
                </li>&nbsp;
                ' . $navsep . ' ' . $nav . ' 
                ' . $navsep . ' <li><span class="active">' . $navbit['name'] . '</span></li>
            </ol>
        </div>';
        //ev al("\$donenav = \"" . $this->templates->get("nav") . "\";");

        return $donenav;
    }

    /**
     * Add a breadcrumb menu item to the list.
     *
     * @param string $name The name of the item to add
     * @param string $url The URL of the item to add
     */
    public function add_breadcrumb($name, $url = '')
    {
        $navsize = count($this->navbits);
        $this->navbits[$navsize]['name'] = $name;
        $this->navbits[$navsize]['url'] = $url;
    }

    /**
     * Build the forum breadcrumb nagiation (the navigation to a specific forum including all parent forums)
     *
     * @param int $fid The forum ID to build the navigation for
     * @param array $multipage The multipage drop down array of information
     * @return int Returns 1 in every case. Kept for compatibility
     */
    public function build_forum_breadcrumb($fid, array $multipage = [])
    {
        if (empty($this->pforumcache)) {
            if (empty($this->forum_cache)) {
                $this->forum->cache_forums();
            }

            foreach ($this->forum_cache as $key => $val) {
                $this->pforumcache[$val['fid']][$val['pid']] = $val;
            }
        }

        if (isset($this->pforumcache[$fid])) {
            foreach ($this->pforumcache[$fid] as $key => $forumnav) {
                if ($fid == $forumnav['fid']) {
                    if (!empty($this->pforumcache[$forumnav['pid']])) {
                        $this->build_forum_breadcrumb($forumnav['pid']);
                    }

                    $navsize = count($this->navbits);
                    // Convert & to &amp;
                    $this->navbits[$navsize]['name'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forumnav['name']);

                    if (defined("IN_ARCHIVE")) {
                        // Set up link to forum in breadcrumb.
                        if ($this->pforumcache[$fid][$forumnav['pid']]['type'] == 'f' ||
                            $this->pforumcache[$fid][$forumnav['pid']]['type'] == 'c'
                        ) {
                            $this->navbits[$navsize]['url'] = "{$this->settings['bburl']}/archive?forum-" . $forumnav['fid'] . ".html";
                        } else {
                            $this->navbits[$navsize]['url'] = $this->settings['bburl'] . '/archive';
                        }
                    } elseif (!empty($multipage)) {
                        $this->navbits[$navsize]['url'] = $this->forum->get_forum_link($forumnav['fid'], $multipage['current_page']);

                        $this->navbits[$navsize]['multipage'] = $multipage;
                        $this->navbits[$navsize]['multipage']['url'] = str_replace('{fid}', $forumnav['fid'], FORUM_URL_PAGED);
                    } else {
                        $this->navbits[$navsize]['url'] = $this->forum->get_forum_link($forumnav['fid']);
                    }
                }
            }
        }

        return 1;
    }

    /**
     * Resets the breadcrumb navigation to the first item, and clears the rest
     */
    private function reset_breadcrumb()
    {
        $newnav[0]['name'] = $this->navbits[0]['name'];
        $newnav[0]['url'] = $this->navbits[0]['url'];
        if (!empty($this->navbits[0]['options'])) {
            $newnav[0]['options'] = $this->navbits[0]['options'];
        }

        unset($GLOBALS['navbits']);
        $GLOBALS['navbits'] = $newnav;
    }

    /**
     * Cleans predefined input variables.
     *
     */
    private function clean_input()
    {
        foreach ($this->clean_variables as $type => $variables) {
            foreach ($variables as $var) {
                // If this variable is in the ignored array, skip and move to next.
                if (in_array($var, $this->ignore_clean_variables)) {
                    continue;
                }

                if (isset($this->input[$var])) {
                    switch ($type) {
                        case 'int':
                            $this->input[$var] = $this->getInput($var, 0);
                            break;
                        case 'a-z':
                            $this->input[$var] = preg_replace("#[^a-z\.\-_]#i", '', $this->getInput($var, ''));
                            break;
                        case 'pos':
                            if (($this->input[$var] < 0 && $var != 'page') ||
                                ($var == 'page' && $this->input[$var] != 'last' && $this->input[$var] < 0)
                            ) {
                                $this->input[$var] = 0;
                            }
                            break;
                    }
                }
            }
        }
    }

    /**
     * setVar
     * Set variable, used by {@link request_var the request_var function}
     *
     * @private
     */
    private function setVar(&$result, $var, $type, $multibyte = false)
    {
        settype($var, $type);
        $result = $var;

        if ($type == 'string') {
            $result = trim(htmlspecialchars(str_replace(["\r\n", "\r"], ["\n", "\n"], $result), ENT_QUOTES, 'UTF-8'));

            if (!empty($result)) {
                // Make sure multibyte characters are wellformed
                if ($multibyte) {
                    if (!preg_match('/^./u', $result)) {
                        $result = '';
                    }
                } else {
                    // no multibyte, allow only ASCII (0-127)
                    $result = preg_replace('/[\x80-\xFF]/', '?', $result);
                }
            }

//            $result = (RC_QUOTES_GPC) ? stripslashes($result) : $result;
        }
    }
    /**
     * Checks the input data type before usage.
     *
     * @param string $name Variable name ($this->input)
     * @param int $default The type of the variable to get.
     *  Should be one of '' - string, 0 - int, 0.0 - float, [] - array, [''] - strings array, [0] - int array
     *
     * @param $multibyte boolean
     *
     * @return int|float|array|string Checked data. Type depending on $type
     */
    public function getInput($name, $default, $multibyte = false)
    {
        if (!isset($this->input[$name]) || (is_array($this->input[$name]) && !is_array($default)) || (is_array($default) && !is_array($this->input[$name]))) {
            return (is_array($default)) ? [] : $default;
        }

        if (!is_array($default)) {
            $type = gettype($default);
        } else {
            list($key_type, $type) = each($default);
            $type = gettype($type);
            $key_type = gettype($key_type);
        }

        $var = $this->input[$name];

        if (is_array($var)) {
            $_var = $var;
            $var = [];

            foreach ($_var as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $_k => $_v) {
                        $this->setVar($k, $k, $key_type);
                        $this->setVar($_k, $_k, $key_type);
                        $this->setVar($var[$k][$_k], $_v, $type, $multibyte);
                    }
                } else {
                    $this->setVar($k, $k, $key_type);
                    $this->setVar($var[$k], $v, $type, $multibyte);
                }
            }
        } else {
            $this->setVar($var, $var, $type, $multibyte);
        }

        return $var;
        /*
                switch ($type) {
                    case []:
                        if (!isset($this->input[$name]) || !is_array($this->input[$name])) {
                            return [];
                        }
                        return $this->input[$name];
                    case 0:
                        if (!isset($this->input[$name]) || !is_numeric($this->input[$name])) {
                            return 0;
                        }
                        return (int)$this->input[$name];
                    case INPUT_FLOAT:
                        if (!isset($this->input[$name]) || !is_numeric($this->input[$name])) {
                            return 0.0;
                        }
                        return (float)$this->input[$name];
                    case INPUT_BOOL:
                        if (!isset($this->input[$name]) || !is_scalar($this->input[$name])) {
                            return false;
                        }
                        return (bool)$this->input[$name];
                    default:
                        if (!isset($this->input[$name]) || !is_scalar($this->input[$name])) {
                            return '';
                        }
                        return $this->input[$name];
                }
        */
    }

    /**
     * Parses the incoming variables.
     *
     * @param array $array The array of incoming variables.
     */
    private function parse_incoming($array)
    {
        if (!is_array($array)) {
            return;
        }
        foreach ($array as $key => $val) {
            $this->input[$key] = $val;
        }
    }

    /**
     * Parses the incoming cookies
     *
     */
    private function parse_cookies()
    {
        if (!is_array($_COOKIE)) {
            return;
        }

        $prefix_length = strlen($this->settings['cookieprefix']);

        foreach ($_COOKIE as $key => $val) {
            if ($prefix_length && substr($key, 0, $prefix_length) == $this->settings['cookieprefix']) {
                $key = substr($key, $prefix_length);

                // Fixes conflicts with one board having a prefix and another that doesn't on the same domain
                // Gives priority to our cookies over others (overwrites them)
                if ($this->cookies[$key]) {
                    unset($this->cookies[$key]);
                }
            }

            if (empty($this->cookies[$key])) {
                $this->cookies[$key] = $val;
            }
        }
    }

    /**
     * Checks to make sure a user has not tried to login more times than permitted
     *
     * @param bool $fatal (Optional) Stop execution if it finds an error with the login. Default is True
     * @return bool|int Number of logins when success, false if failed.
     */
    public function login_attempt_check($fatal = true)
    {
        if ($this->settings['failedlogincount'] == 0) {
            return 1;
        }
        // Note: Number of logins is defaulted to 1, because using 0 seems to clear cookie data.
        //Not really a problem as long as we account for 1 being default.

        // Use cookie if possible, otherwise use session
        // Find better solution to prevent clearing cookies
        $loginattempts = 0;
        $failedlogin = 0;

        if (!empty($this->cookies['loginattempts'])) {
            $loginattempts = $this->cookies['loginattempts'];
        }

        if (!empty($this->cookies['failedlogin'])) {
            $failedlogin = $this->cookies['failedlogin'];
        }

        // Work out if the user has had more than the allowed number of login attempts
        if ($loginattempts > $this->settings['failedlogincount']) {
            // If so, then we need to work out if they can try to login again
            // Some maths to work out how long they have left and display it to them
            $now = TIME_NOW;

            if (empty($this->cookies['failedlogin'])) {
                $failedtime = $now;
            } else {
                $failedtime = $this->cookies['failedlogin'];
            }

            $secondsleft = $this->settings['failedlogintime'] * 60 + $failedtime - $now;
            $hoursleft = floor($secondsleft / 3600);
            $minsleft = floor(($secondsleft / 60) % 60);
            $secsleft = floor($secondsleft % 60);

            // This value will be empty the first time the user doesn't login in, set it
            if (empty($failedlogin)) {
                $this->my_setcookie('failedlogin', $now);
                if ($fatal) {
                    $this->error($this->lang->sprintf($this->lang->failed_login_wait, $hoursleft, $minsleft, $secsleft));
                }

                return false;
            }

            // Work out if the user has waited long enough before letting them login again
            if ($this->cookies['failedlogin'] < ($now - $this->settings['failedlogintime'] * 60)) {
                $this->my_setcookie('loginattempts', 1);
                $this->my_unsetcookie('failedlogin');
                if ($this->user->uid != 0) {
                    $update_array = [
                        'loginattempts' => 1
                    ];
                    $this->db->update_query("users", $update_array, "uid = '{$this->user->uid}'");
                }
                return 1;
            } // Not waited long enough
            elseif ($this->cookies['failedlogin'] > ($now - $this->settings['failedlogintime'] * 60)) {
                if ($fatal) {
                    $this->error($this->lang->sprintf($this->lang->failed_login_wait, $hoursleft, $minsleft, $secsleft));
                }

                return false;
            }
        }

        // User can attempt another login
        return $loginattempts;
    }

    /**
     * Triggers a generic error.
     *
     * @param string $code The error code.
     */
    public function trigger_generic_error($code)
    {
        switch ($code) {
            case 'cache_no_write':
                $message = 'The data cache directory (cache/) needs to exist and be writable by the web server. Change its permissions so that it is writable (777 on Unix based servers).';
                $error_code = MYBB_CACHE_NO_WRITE;
                break;
            case 'install_directory':
                $message = 'The install directory (install/) still exists on your server and is not locked. To access BB please either remove this directory or create an empty file in it called \'lock\'.';
                $error_code = MYBB_INSTALL_DIR_EXISTS;
                break;
            case 'board_not_installed':
                $message = 'Your board has not yet been installed and configured. Please do so before attempting to browse it.';
                $error_code = MYBB_NOT_INSTALLED;
                break;
            case 'board_not_upgraded':
                $message = 'Your board has not yet been upgraded. Please do so before attempting to browse it.';
                $error_code = MYBB_NOT_UPGRADED;
                break;
//            case 'sql_load_error':
//                $message = "BB was unable to load the SQL extension. Please contact the BB Group for support. <a href=\"http://www.mybb.com\">BB Website</a>";
//                $error_code = MYBB_SQL_LOAD_ERROR;
//                break;
            case 'apc_load_error':
                $message = 'APC needs to be configured with PHP to use the APC cache support.';
                $error_code = MYBB_CACHEHANDLER_LOAD_ERROR;
                break;
            case 'eaccelerator_load_error':
                $message = 'eAccelerator needs to be configured with PHP to use the eAccelerator cache support.';
                $error_code = MYBB_CACHEHANDLER_LOAD_ERROR;
                break;
            case 'memcache_load_error':
                $message = 'Your server does not have memcache support enabled.';
                $error_code = MYBB_CACHEHANDLER_LOAD_ERROR;
                break;
            case 'memcached_load_error':
                $message = 'Your server does not have memcached support enabled.';
                $error_code = MYBB_CACHEHANDLER_LOAD_ERROR;
                break;
            case 'xcache_load_error':
                $message = 'Xcache needs to be configured with PHP to use the Xcache cache support.';
                $error_code = MYBB_CACHEHANDLER_LOAD_ERROR;
                break;
            default:
                $message = "BB has experienced an internal error. Please contact the BB Group for support. <a href=\"http://www.mybb.com\">BB Website</a>";
                $error_code = MYBB_GENERAL;
        }
        $this->bbErrorHandler->trigger($message, $error_code);
    }

    /**
     * Produce a friendly error message page
     *
     * @param string $error The error message to be shown
     * @param string $title The title of the message shown in the title of the page and the error table
     */
    public function error($error = '', $title = '')
    {
        $error = $this->plugins->runHooks('error', $error);
        if (!$error) {
            $error = $this->lang->unknown_error;
        }

        // AJAX error message?
        if ($this->getInput('ajax', 0)) {
            // Send our headers.
            @header("Content-type: application/json; charset={$this->lang->settings['charset']}");
            echo json_encode(['errors' => [$error]]);
            exit;
        }

        if (!$title) {
            $title = $this->settings['bbname'];
        }

        $this->reset_breadcrumb();
        $this->add_breadcrumb('Error');

        $this->view->offsetSet('title', $title);
        $this->view->offsetSet('error', $error);

        $this->output_page();
        die($this->view->fetch('@forum/Errors/common.html.twig'));
    }

    /**
     * Produce an error message for displaying inline on a page
     *
     * @param array $errors Array of errors to be shown
     * @param string $title The title of the error message
     * @param array $json_data JSON data to be encoded (we may want to send more data; e.g. newreply.php uses this for CAPTCHA)
     * @return string The inline error HTML
     */
    public function inline_error($errors, $title = '', $json_data = [])
    {
        if (!$title) {
            $title = $this->lang->please_correct_errors;
        }

        if (!is_array($errors)) {
            $errors = [$errors];
        }

        // AJAX error message?
        if ($this->getInput('ajax', 0)) {
            // Send our headers.
            @header("Content-type: application/json; charset={$this->lang->settings['charset']}");

            if (empty($json_data)) {
                echo json_encode(['errors' => $errors]);
            } else {
                echo json_encode(array_merge(['errors' => $errors], $json_data));
            }
            exit;
        }

        $errorlist = '';

        foreach ($errors as $error) {
            $errorlist .= '<li>' . $error . '</li>'."\n";
        }
        $this->view->offsetSet('errorlist', $errorlist);
        $this->view->offsetSet('title', $title);

        return $this->view->fetch('@forum/Errors/inline.html.twig');
    }

    /**
     * Presents the user with a "no permission" page
     */
    public function error_no_permission()
    {
        $time = TIME_NOW;
        $this->plugins->runHooks('no_permission');

        $noperm_array = [
            'nopermission' => '1',
            'location1' => 0,
            'location2' => 0
        ];

        $this->db->update_query('sessions', $noperm_array, "sid='{$this->session->sid}'");

        if ($this->getInput('ajax', 0)) {
            // Send our headers.
            header("Content-type: application/json; charset={$this->lang->settings['charset']}");
            echo json_encode(['errors' => [$this->lang->error_nopermission_user_ajax]]);
            exit;
        }

        if ($this->user->uid) {
            $this->lang->error_nopermission_user_username = $this->lang->sprintf(
                $this->lang->error_nopermission_user_username,
                $this->user->username
            );
            $errorpage = $this->view->fetch('@forum/Errors/nopermission_loggedin.html.twig');
        } else {
//      // Redirect to where the user came from
//      $redirect_url = $_SERVER['PHP_SELF'];
//      if ($_SERVER['QUERY_STRING']) {
//        $redirect_url .= '?' . $_SERVER['QUERY_STRING'];
//      }
            $uri = $this->request->getUri();
            //$redirect_url = $uri->getScheme().'://'.$uri->getHost().$uri->getPath();
            $redirect_url = $uri->getPath();

            //FIXME redirect
            $this->view->offsetSet('redirect_url', $redirect_url);

            switch ($this->settings['username_method']) {
                case 0:
                    $lang_username = $this->lang->username;
                    break;
                case 1:
                    $lang_username = $this->lang->username1;
                    break;
                case 2:
                    $lang_username = $this->lang->username2;
                    break;
                default:
                    $lang_username = $this->lang->username;
                    break;
            }
            $this->view->offsetSet('lang_username', $lang_username);
            $errorpage = $this->view->fetch('@forum/Errors/nopermission.html.twig');
        }

        $this->error($errorpage);
    }

    /**
     * Outputs a page directly to the browser, parsing anything which needs to be parsed.
     *
     * @param string $contents The contents of the page.
     */
    public function output_page()//$contents)
    {
        $this->view->offsetSet('navigation', $this->build_breadcrumb());

        if (isset($this->bbErrorHandler->warnings)) {
            $this->view->offsetSet('error_handler_warnings', $this->bbErrorHandler->show_warnings());
        }

        $totaltime = format_time_duration($this->maintimer->stop());
        //$contents = $this->plugins->runHooks('pre_output_page', $contents);

        if ($this->usergroup['cancp'] == 1 || $this->dev_mode == 1) {
            if ($this->settings['extraadmininfo'] != 0) {
                $phptime = $this->maintimer->totaltime - $this->db->query_time;
                $query_time = $this->db->query_time;

                if ($this->maintimer->totaltime > 0) {
                    $percentphp = number_format((($phptime / $this->maintimer->totaltime) * 100), 2);
                    $percentsql = number_format((($query_time / $this->maintimer->totaltime) * 100), 2);
                } else {
                    // if we've got a super fast script...  all we can do is assume something
                    $percentphp = 0;
                    $percentsql = 0;
                }

                $serverload = get_server_load($this->lang->unknown);

                if (my_strpos(getenv('REQUEST_URI'), '?')) {
                    $debuglink = htmlspecialchars_uni(getenv('REQUEST_URI')) . '&amp;debug=1';
                } else {
                    $debuglink = htmlspecialchars_uni(getenv('REQUEST_URI')) . '?debug=1';
                }

                $memory_usage = get_memory_usage();

                if ($memory_usage) {
                    $memory_usage = $this->lang->sprintf($this->lang->debug_memory_usage, $this->parser->friendlySize($memory_usage));
                } else {
                    $memory_usage = '';
                }
                // MySQLi is still MySQL, so present it that way to the user
                $database_server = $this->db->short_title;

                if ($database_server == 'MySQLi') {
                    $database_server = 'MySQL';
                }
                $generated_in = $this->lang->sprintf($this->lang->debug_generated_in, $totaltime);
                $debug_weight = $this->lang->sprintf($this->lang->debug_weight, $percentphp, $percentsql, $database_server);
                $sql_queries = $this->lang->sprintf($this->lang->debug_sql_queries, $this->db->query_count);
                $server_load = $this->lang->sprintf($this->lang->debug_server_load, $serverload);

                $debugstuff = '<div id="debug">' . $generated_in . '  ' . $debug_weight . ' <br />' . $sql_queries . ' / ' . $server_load . ' / ' . $memory_usage . '<br />[<a href="' . $debuglink . '" target="_blank">' . $this->lang->debug_advanced_details . '</a>]<br /></div>';

                $this->view->offsetSet('debugstuff', $debugstuff);
            }

            if ($this->debug_mode == true) {
                (new \RunBB\Helpers\DebugPage($this))->show();
            }
        }

        $this->plugins->runHooks('post_output_page');
    }

    /**
     * Redirect the user to a given URL with a given message
     *
     * @param string $url The URL to redirect the user to
     * @param string $message The redirection message to be shown
     * @param string $title The title of the redirection page
     * @param boolean $force_redirect Force the redirect page regardless of settings
     */
    public function redirect($url, $message = '', $title = '', $force_redirect = false)
    {
        $redirect_args = ['url' => &$url, 'message' => &$message, 'title' => &$title];

        $this->plugins->runHooks('redirect', $redirect_args);

        if ($this->getInput('ajax', 0)) {
            // Send our headers.
            //@header("Content-type: text/html; charset={$this->lang->settings['charset']}");
            $data = "<script type=\"text/javascript\">\n";
            if ($message != '') {
                $data .= 'alert("' . addslashes($message) . '");';
            }
            $url = str_replace("#", "&#", $url);
            $url = htmlspecialchars_decode($url);
            $url = str_replace(["\n", "\r", ";"], "", $url);
            $data .= 'window.location = "' . addslashes($url) . '";' . "\n";
            $data .= "</script>\n";
            //exit;

            @header("Content-type: application/json; charset={$this->lang->settings['charset']}");
            echo json_encode(["data" => $data]);
            exit;
        }

        if (!$message) {
            $message = $this->lang->redirect;
        }

        if (!$title) {
            $title = $this->settings['bbname'];
        }

        // Show redirects only if both ACP and UCP settings are enabled, or ACP is enabled, and user is a guest, or they are forced.
        if ($force_redirect == true ||
            ($this->settings['redirects'] == 1 &&
                ((isset($this->user->showredirect) && $this->user->showredirect == 1) ||
                !$this->user->uid)
            )
        ) {
            $url = str_replace("&amp;", "&", $url);
            $url = htmlspecialchars_uni($url);

            $this->view->offsetSet('url', $url);
            $this->view->offsetSet('title', $title);
            $this->view->offsetSet('message', $message);

            $this->output_page();
            $this->view->render($this->response, '@forum/Misc/redirect.html.twig');
        } else {
            $url = htmlspecialchars_decode($url);
            $url = str_replace(["\n", "\r", ';'], '', $url);

            $this->run_shutdown();

            if (my_substr($url, 0, 7) !== 'http://' && my_substr($url, 0, 8) !== 'https://' && my_substr($url, 0, 1) !== '/') {
                return $this->response->withRedirect($this->settings['bburl'] . '/' . $url);
            } else {
                return $this->response->withRedirect($url);
            }
        }
    }
}
