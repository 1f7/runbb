<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Ban extends AbstractController
{
    /**
     * Checks if a username has been disallowed for registration/use.
     *
     * @param string $username The username
     * @param boolean $update_lastuse True if the 'last used' dateline should be updated if a match is found.
     * @return boolean True if banned, false if not banned
     */
    public function is_banned_username($username, $update_lastuse = false)
    {
        $query = $this->db->simple_select('banfilters', 'filter, fid', "type='2'");
        while ($banned_username = $this->db->fetch_array($query)) {
            // Make regular expression * match
            $banned_username['filter'] = str_replace('\*', '(.*)', preg_quote($banned_username['filter'], '#'));
            if (preg_match("#(^|\b){$banned_username['filter']}($|\b)#i", $username)) {
                // Updating last use
                if ($update_lastuse == true) {
                    $this->db->update_query("banfilters", ["lastuse" => TIME_NOW], "fid='{$banned_username['fid']}'");
                }
                return true;
            }
        }
        // Still here - good username
        return false;
    }

    /**
     * Check if a specific email address has been banned.
     *
     * @param string $email The email address.
     * @param boolean $update_lastuse True if the 'last used' dateline should be updated if a match is found.
     * @return boolean True if banned, false if not banned
     */
    public function is_banned_email($email, $update_lastuse = false)
    {
        $banned_cache = $this->bb->cache->read("bannedemails");

        if ($banned_cache === false) {
            // Failed to read cache, see if we can rebuild it
            $this->bb->cache->update_bannedemails();
            $banned_cache = $this->bb->cache->read("bannedemails");
        }

        if (is_array($banned_cache) && !empty($banned_cache)) {
            foreach ($banned_cache as $banned_email) {
                // Make regular expression * match
                $banned_email['filter'] = str_replace('\*', '(.*)', preg_quote($banned_email['filter'], '#'));

                if (preg_match("#{$banned_email['filter']}#i", $email)) {
                    // Updating last use
                    if ($update_lastuse == true) {
                        $this->db->update_query("banfilters", ["lastuse" => TIME_NOW], "fid='{$banned_email['fid']}'");
                    }
                    return true;
                }
            }
        }

        // Still here - good email
        return false;
    }

    /**
     * Checks if a specific IP address has been banned.
     *
     * @param string $ip_address The IP address.
     * @param boolean $update_lastuse True if the 'last used' dateline should be updated if a match is found.
     * @return boolean True if banned, false if not banned.
     */
    public function is_banned_ip($ip_address, $update_lastuse = false)
    {
        $banned_ips = $this->bb->cache->read("bannedips");
        if (!is_array($banned_ips)) {
            return false;
        }

//        $ip_address = my_inet_pton($ip_address);
        foreach ($banned_ips as $banned_ip) {
            if (!$banned_ip['filter']) {
                continue;
            }

            $banned = false;

            $ip_range = fetch_ip_range($banned_ip['filter']);
            if (is_array($ip_range)) {
                if (strcmp($ip_range[0], $ip_address) <= 0 && strcmp($ip_range[1], $ip_address) >= 0) {
                    $banned = true;
                }
            } elseif ($ip_address == $ip_range) {
                $banned = true;
            }
            if ($banned) {
                // Updating last use
                if ($update_lastuse == true) {
                    $this->db->update_query("banfilters", ["lastuse" => TIME_NOW], "fid='{$banned_ip['fid']}'");
                }
                return true;
            }
        }

        // Still here - good ip
        return false;
    }

    /**
     * Checks to see if the email is already in use by another
     *
     * @param string $email The email to check.
     * @param int $uid User ID of the user (updating only)
     * @return boolean True when in use, false when not.
     */
    public function email_already_in_use($email, $uid = 0)
    {
        $uid_string = '';
        if ($uid) {
            $uid_string = " AND uid != '" . (int)$uid . "'";
        }
        $query = $this->db->simple_select("users", "COUNT(email) as emails", "email = '" . $this->db->escape_string($email) . "'{$uid_string}");

        if ($this->db->fetch_field($query, "emails") > 0) {
            return true;
        }

        return false;
    }

    /**
     * Fetch a list of ban times for a user account.
     *
     * @return array Array of ban times
     */
    public function fetch_ban_times()
    {
        // Days-Months-Years
        $ban_times = [
            '1-0-0' => '1 ' . $this->bb->lang->day,
            '2-0-0' => '2 ' . $this->bb->lang->days,
            '3-0-0' => '3 ' . $this->bb->lang->days,
            '4-0-0' => '4 ' . $this->bb->lang->days,
            '5-0-0' => '5 ' . $this->bb->lang->days,
            '6-0-0' => '6 ' . $this->bb->lang->days,
            '7-0-0' => '1 ' . $this->bb->lang->week,
            '14-0-0' => '2 ' . $this->bb->lang->weeks,
            '21-0-0' => '3 ' . $this->bb->lang->weeks,
            '0-1-0' => '1 ' . $this->bb->lang->month,
            '0-2-0' => '2 ' . $this->bb->lang->months,
            '0-3-0' => '3 ' . $this->bb->lang->months,
            '0-4-0' => '4 ' . $this->bb->lang->months,
            '0-5-0' => '5 ' . $this->bb->lang->months,
            '0-6-0' => '6 ' . $this->bb->lang->months,
            '0-0-1' => '1 ' . $this->bb->lang->year,
            '0-0-2' => '2 ' . $this->bb->lang->years
        ];

        $ban_times = $this->bb->plugins->runHooks('functions_fetch_ban_times', $ban_times);

        $ban_times['---'] = $this->bb->lang->permanent;
        return $ban_times;
    }

    /**
     * Format a ban length in to a UNIX timestamp.
     *
     * @param string $date The ban length string
     * @param int $stamp The optional UNIX timestamp, if 0, current time is used.
     * @return int The UNIX timestamp when the ban will be lifted
     */
    public function ban_date2timestamp($date, $stamp = 0)
    {
        if ($stamp == 0) {
            $stamp = TIME_NOW;
        }
        $d = explode('-', $date);
        $nowdate = date('H-j-n-Y', $stamp);
        $n = explode('-', $nowdate);
        $n[1] += $d[0];
        $n[2] += $d[1];
        $n[3] += $d[2];
        return mktime(date('G', $stamp), date('i', $stamp), 0, $n[2], $n[1], $n[3]);
    }
}
