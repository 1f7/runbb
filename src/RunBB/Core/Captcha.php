<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

class Captcha
{
    /**
     * Type of CAPTCHA.
     *
     * 1 = Default CAPTCHA
     * 2 = reCAPTCHA
     * 4 = NoCATPCHA reCAPTCHA
     *
     * @var int
     */
    public $type = 0;

    /**
     * The template to display the CAPTCHA in
     *
     * @var string
     */
    public $captcha_template = '';

    /**
     * CAPTCHA Server URL
     *
     * @var string
     */
    public $server = '';

    /**
     * CAPTCHA Verify Server
     *
     * @var string
     */
    public $verify_server = '';

    /**
     * HTML of the built CAPTCHA
     *
     * @var string
     */
    public $html = '';

    /**
     * The errors that occurred when handling data.
     *
     * @var array
     */
    public $errors = [];

    protected $bb;

    /**
     * @param bool $build
     * @param string $template
     */
    public function __construct(& $bb, $build = false, $template = '')
    {
        $this->bb = $bb;
        $this->type = $bb->settings['captchaimage'];

        $args = [
            'this' => &$this,
            'build' => &$build,
            'template' => &$template,
        ];

        $bb->plugins->runHooks('captcha_build_start', $args);

        // Prepare the build template
        if ($template) {
            $this->captcha_template = $template;

            if ($this->type == 2) {
                $this->captcha_template .= '_recaptcha';
            } elseif ($this->type == 4) {
                $this->captcha_template .= '_nocaptcha';
            }
        }

        // Work on which CAPTCHA we've got installed
        if ($this->type == 2 && $bb->settings['captchapublickey'] && $bb->settings['captchaprivatekey']) {
            // We want to use reCAPTCHA, set the server options
            $this->server = '//www.google.com/recaptcha/api';
            $this->verify_server = 'www.google.com';

            if ($build == true) {
                $this->build_recaptcha();
            }
        } elseif ($this->type == 4 && $bb->settings['captchapublickey'] && $bb->settings['captchaprivatekey']) {
            // We want to use reCAPTCHA, set the server options
            $this->server = '//www.google.com/recaptcha/api.js';
            $this->verify_server = 'https://www.google.com/recaptcha/api/siteverify';

            if ($build == true) {
                $this->build_recaptcha();
            }
        } elseif ($this->type == 1) {
            if (!function_exists('imagecreatefrompng')) {
                // We want to use the default CAPTCHA, but it's not installed
                return;
            } elseif ($build == true) {
                $this->build_captcha();
            }
        }

        $bb->plugins->runHooks('captcha_build_end', $args);
    }

    /**
     * @param bool $return Not used
     */
    public function build_captcha($return = false)
    {
        // This will build a MyBB CAPTCHA
        $randomstr = random_str(5);
        $imagehash = md5(random_str(12));

        $insert_array = [
            'imagehash' => $imagehash,
            'imagestring' => $randomstr,
            'dateline' => TIME_NOW
        ];
        $this->bb->db->insert_query('captcha', $insert_array);

        if ($this->captcha_template == 'member_register_regimage') {
            $this->html = $insert_array;
            return;
        }
        $this->html = $this->bb->view->fetch('@forum/Misc/postCaptcha.html.twig', ['imagehash' => $imagehash]);
        //ev al("\$this->html = \"" . $this->bb->templates->get($this->captcha_template) . "\";");
        //ev al("\$this->html = \"".$this->bb->templates->get('member_register_regimage')."\";");
    }

    function build_recaptcha()
    {
        // This will build a reCAPTCHA
        $server = $this->server;
        $public_key = $this->bb->settings['captchapublickey'];

        eval("\$this->html = \"" . $this->bb->templates->get($this->captcha_template, 1, 0) . "\";");
        //ev al("\$this->html = \"".$this->bb->templates->get('member_register_regimage_recaptcha')."\";");
    }

    /**
     * @return string
     */
    function build_hidden_captcha()
    {
        $field = [];

        if ($this->type == 1) {
            // Names
            $hash = 'imagehash';
            $string = 'imagestring';

            // Values
            $field['hash'] = $this->bb->db->escape_string($this->bb->input['imagehash']);
            $field['string'] = $this->bb->db->escape_string($this->bb->input['imagestring']);
        } elseif ($this->type == 2) {
            // Names
            $hash = 'recaptcha_challenge_field';
            $string = 'recaptcha_response_field';

            // Values
            $field['hash'] = $this->bb->input['recaptcha_challenge_field'];
            $field['string'] = $this->bb->input['recaptcha_response_field'];
        } elseif ($this->type == 3) {
            // Are You a Human can't be built as a hidden captcha
            return '';
        }

        eval("\$this->html = \"" . $this->bb->templates->get('post_captcha_hidden') . "\";");
        return $this->html;
    }

    /**
     * @return bool
     */
    function validate_captcha()
    {
        $this->bb->plugins->runHooks('captcha_validate_start', $this);

        if ($this->type == 1) {
            // We have a normal CAPTCHA to handle
            $imagehash = $this->bb->db->escape_string($this->bb->input['imagehash']);
            $imagestring = $this->bb->db->escape_string(my_strtolower($this->bb->input['imagestring']));

            switch ($this->bb->db->type) {
                case 'mysql':
                case 'mysqli':
                    $field = 'imagestring';
                    break;
                default:
                    $field = 'LOWER(imagestring)';
                    break;
            }

            $query = $this->bb->db->simple_select('captcha', '*', "imagehash = '{$imagehash}' AND {$field} = '{$imagestring}'");
            $imgcheck = $this->bb->db->fetch_array($query);

            if (!$imgcheck) {
//                $this->set_error($this->bb->lang->invalid_captcha_verify);
                $this->setError('invalid_captcha_verify');
                $this->bb->db->delete_query('captcha', "imagehash = '{$imagehash}'");
            }
        } elseif ($this->type == 2) {
            $challenge = $this->bb->input['recaptcha_challenge_field'];
            $response = $this->bb->input['recaptcha_response_field'];

            if (!$challenge || strlen($challenge) == 0 || !$response || strlen($response) == 0) {
//                $this->set_error($this->bb->lang->invalid_captcha);
                $this->setError('invalid_captcha');
            } else {
                // We have a reCAPTCHA to handle
                $data = $this->_qsencode([
                    'privatekey' => $this->bb->settings['captchaprivatekey'],
                    'remoteip' => $this->bb->session->ipaddress,
                    'challenge' => $challenge,
                    'response' => $response
                ]);

                // Contact Google and see if our reCAPTCHA was successful
                $http_request = "POST /recaptcha/api/verify HTTP/1.0\r\n";
                $http_request .= "Host: $this->verify_server\r\n";
                $http_request .= "Content-Type: application/x-www-form-urlencoded;\r\n";
                $http_request .= 'Content-Length: ' . strlen($data) . "\r\n";
                $http_request .= "User-Agent: reCAPTCHA/PHP\r\n";
                $http_request .= "\r\n";
                $http_request .= $data;

                $fs = @fsockopen($this->verify_server, 80, $errno, $errstr, 10);

                if ($fs == false) {
//                    $this->set_error($this->bb->lang->invalid_captcha_transmit);
                    $this->setError('invalid_captcha_transmit');
                } else {
                    // We connected, but is it correct?
                    fwrite($fs, $http_request);

                    while (!feof($fs)) {
                        $response .= fgets($fs, 1160);
                    }

                    fclose($fs);

                    $response = explode("\r\n\r\n", $response, 2);
                    $answer = explode("\n", $response[1]);

                    if (trim($answer[0]) != 'true') {
                        // We got it wrong! Oh no...
//                        $this->set_error($this->bb->lang->invalid_captcha_verify);
                        $this->setError('invalid_captcha_verify');
                    }
                }
            }
        } elseif ($this->type == 4) {
            $response = $this->bb->input['g-recaptcha-response'];
            if (!$response || strlen($response) == 0) {
//                $this->set_error($this->bb->lang->invalid_nocaptcha);
                $this->setError('invalid_nocaptcha');
            } else {
                // We have a noCAPTCHA to handle
                // Contact Google and see if our reCAPTCHA was successful
                $response = fetch_remote_file($this->verify_server, [
                    'secret' => $this->bb->settings['captchaprivatekey'],
                    'remoteip' => $this->bb->session->ipaddress,
                    'response' => $response
                ]);

                if ($response == false) {
//                    $this->set_error($this->bb->lang->invalid_nocaptcha_transmit);
                    $this->setError('invalid_nocaptcha_transmit');
                } else {
                    $answer = json_decode($response, true);

                    if ($answer['success'] != 'true') {
                        // We got it wrong! Oh no...
//                        $this->set_error($this->bb->lang->invalid_nocaptcha);
                        $this->setError('invalid_nocaptcha');
                    }
                }
            }
        }

        $this->bb->plugins->runHooks('captcha_validate_end', $this);

        if (count($this->errors) > 0) {
            return false;
        } else {
            return true;
        }
    }

    function invalidate_captcha()
    {
        if ($this->type == 1) {
            // We have a normal CAPTCHA to handle
            $imagehash = $this->bb->db->escape_string($this->bb->input['imagehash']);
            if ($imagehash) {
                $this->bb->db->delete_query('captcha', "imagehash = '{$imagehash}'");
            }
        }
        // Not necessary for reCAPTCHA or Are You a Human

        $this->bb->plugins->runHooks('captcha_invalidate_end', $this);
    }

    /**
     * Add an error to the error array.
     *
     * @param string $error
     * @param string $data
     */
    private function setError($error, $data = '')
    {
        $this->errors[$error] = [
            'error_code' => $error,
            'data' => $data
        ];
    }

    /**
     * Returns the error(s) that occurred when handling data
     * in a format that MyBB can handle.
     *
     * @return array An array of errors in a MyBB format.
     */
    function get_errors()
    {
        $errors = [];
        foreach ($this->errors as $error) {
            $lang_string = $error['error_code'];

            if (!$lang_string) {
                if ($this->bb->lang->invalid_captcha_verify) {
                    $lang_string = 'invalid_captcha_verify';
                } else {
                    $lang_string = 'unknown_error';
                }
            }

            if (!isset($this->bb->lang->$lang_string)) {
                $errors[] = $error['error_code'];
                continue;
            }

            if (!empty($error['data']) && !is_array($error['data'])) {
                $error['data'] = [$error['data']];
            }

            if (is_array($error['data'])) {
                array_unshift($error['data'], $this->bb->lang->$lang_string);
                $errors[] = call_user_func_array([$this->bb->lang, 'sprintf'], $error['data']);
            } else {
                $errors[] = $this->bb->lang->$lang_string;
            }
        }

        return $errors;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function _qsencode($data)
    {
        $req = '';
        foreach ($data as $key => $value) {
            $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
        }

        $req = substr($req, 0, (strlen($req) - 1));

        return $req;
    }
}
