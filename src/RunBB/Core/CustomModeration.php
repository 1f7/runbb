<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunBB\Handlers\DataHandlers\PostDataHandler;

class CustomModeration extends Moderation
{
    /**
     * Get info on a tool
     *
     * @param int $tool_id Tool ID
     * @return array|bool Returns tool data (tid, type, name, description) in an array, otherwise boolean false.
     */
    public function tool_info($tool_id)
    {
        // Get tool info
        $query = $this->db->simple_select('modtools', '*', 'tid="' . (int)$tool_id . '"');
        $tool = $this->db->fetch_array($query);
        if (!$tool['tid']) {
            return false;
        } else {
            return $tool;
        }
    }

    /**
     * Execute Custom Moderation Tool
     *
     * @param int $tool_id Tool ID
     * @param int|array Thread ID(s)
     * @param int|array Post ID(s)
     * @return string 'forum' or 'default' indicating where to redirect
     */
    public function execute($tool_id, $tids = 0, $pids = 0)
    {
        // Get tool info
        $query = $this->db->simple_select('modtools', '*', 'tid="' . (int)$tool_id . '"');
        $tool = $this->db->fetch_array($query);
        if (!$tool['tid']) {
            return false;
        }

        // Format single tid and pid
        if (!is_array($tids)) {
            $tids = [$tids];
        }
        if (!is_array($pids)) {
            $pids = [$pids];
        }

        // Unserialize custom moderation
        $post_options = my_unserialize($tool['postoptions']);
        $thread_options = my_unserialize($tool['threadoptions']);

        // If the tool type is a post tool, then execute the post moderation
        $deleted_thread = 0;
        if ($tool['type'] == 'p') {
            $deleted_thread = $this->execute_post_moderation($post_options, $pids, $tids);
        }
        // Always execute thead moderation
        $this->execute_thread_moderation($thread_options, $tids);

        // If the thread is deleted, indicate to the calling script to redirect to the forum, and not the nonexistant thread
        if ($thread_options['deletethread'] == 1 || $deleted_thread === 1) {
            return 'forum';
        }
        return 'default';
    }

    /**
     * Execute Inline Post Moderation
     *
     * @param array $post_options Moderation information
     * @param array $pids Post IDs
     * @param array|int $tid Thread IDs (in order of dateline ascending). Only the first one will be used
     * @return boolean true
     */
    private function execute_post_moderation($post_options, $pids, $tid)
    {
        if (is_array($tid)) {
            $tid = (int)$tid[0]; // There's only 1 thread when doing inline post moderation
            // The thread chosen is the first thread in the array of tids.
            // It is recommended that this be the tid of the oldest post
        }

        // Get the information about thread
        $thread = $this->thread->get_thread($tid);

        // If deleting posts, only do that
        if ($post_options['deleteposts'] == 1) {
            foreach ($pids as $pid) {
                $this->delete_post($pid);
            }

            $delete_tids = [];
            $imploded_pids = implode(',', array_map('intval', $pids));
            $query = $this->db->simple_select('threads', 'tid', "firstpost IN ({$imploded_pids})");
            while ($threadid = $this->db->fetch_field($query, 'tid')) {
                $delete_tids[] = $threadid;
            }
            if (!empty($delete_tids)) {
                foreach ($delete_tids as $delete_tid) {
                    $this->delete_thread($delete_tid);
                    $this->mark_reports($delete_tid, 'thread');
                }
                // return true here so the code in execute() above knows to redirect to the forum
                return true;
            }
        } else {
            if ($post_options['mergeposts'] == 1) { // Merge posts
                $this->merge_posts($pids);
            }

            if ($post_options['approveposts'] == 'approve') { // Approve posts
                $this->approve_posts($pids);
            } elseif ($post_options['approveposts'] == 'unapprove') { // Unapprove posts
                $this->unapprove_posts($pids);
            } elseif ($post_options['approveposts'] == 'toggle') { // Toggle post visibility
                $this->toggle_post_visibility($pids);
            }

            if ($post_options['softdeleteposts'] == 'softdelete') { // Soft delete posts
                $this->soft_delete_posts($pids);
            } elseif ($post_options['softdeleteposts'] == 'restore') { // Restore posts
                $this->restore_posts($pids);
            } elseif ($post_options['softdeleteposts'] == 'toggle') { // Toggle post visibility
                $this->toggle_post_softdelete($pids);
            }

            if ($post_options['splitposts'] > 0 || $post_options['splitposts'] == -2) { // Split posts
                $query = $this->db->simple_select('posts', 'COUNT(*) AS totalposts', "tid='{$tid}'");
                $count = $this->db->fetch_array($query);

                if ($count['totalposts'] == 1) {
                    error($this->lang->error_cantsplitonepost);
                }

                if ($count['totalposts'] == count($pids)) {
                    error($this->lang->error_cantsplitall);
                }

                if ($post_options['splitposts'] == -2) {
                    $post_options['splitposts'] = $thread['fid'];
                }
                if (empty($post_options['splitpostsnewsubject'])) {
                    // Enter in a subject if a predefined one does not exist.
                    $post_options['splitpostsnewsubject'] = "{$this->lang->split_thread_subject} {$thread['subject']}";
                }
                $new_subject = str_ireplace('{subject}', $thread['subject'], $post_options['splitpostsnewsubject']);
                $new_tid = $this->split_posts($pids, $tid, $post_options['splitposts'], $new_subject);
                if ($post_options['splitpostsclose'] == 'close') { // Close new thread
                    $this->close_threads($new_tid);
                }
                if ($post_options['splitpostsstick'] == 'stick') { // Stick new thread
                    $this->stick_threads($new_tid);
                }
                if ($post_options['splitpostsunapprove'] == 'unapprove') { // Unapprove new thread
                    $this->unapprove_threads($new_tid, $thread['fid']);
                }
                if ($post_options['splitthreadprefix'] != '0') {
                    $this->apply_thread_prefix($new_tid, $post_options['splitthreadprefix']); // Add thread prefix to new thread
                }
                if (!empty($post_options['splitpostsaddreply'])) { // Add reply to new thread
                    $posthandler = new PostDataHandler($this->bb, 'insert');

                    if (empty($post_options['splitpostsreplysubject'])) {
                        $post_options['splitpostsreplysubject'] = 'RE: ' . $new_subject;
                    } else {
                        $post_options['splitpostsreplysubject'] = str_ireplace('{username}', $this->user->username, $post_options['splitpostsreplysubject']);
                        $post_options['splitpostsreplysubject'] = str_ireplace('{subject}', $new_subject, $post_options['splitpostsreplysubject']);
                    }

                    // Set the post data that came from the input to the $post array.
                    $post = [
                        'tid' => $new_tid,
                        'fid' => $post_options['splitposts'],
                        'subject' => $post_options['splitpostsreplysubject'],
                        'uid' => $this->user->uid,
                        'username' => $this->user->username,
                        'message' => $post_options['splitpostsaddreply'],
                        'ipaddress' => $this->session->ipaddress,
                    ];
                    // Set up the post options from the input.
                    $post['options'] = [
                        'signature' => 1,
                        'emailnotify' => 0,
                        'disablesmilies' => 0
                    ];

                    $posthandler->set_data($post);

                    if ($posthandler->validatePost($post)) {
                        $posthandler->insertPost($post);
                    }
                }
            }
        }
        return true;
    }

    /**
     * Execute Normal and Inline Thread Moderation
     *
     * @param array $thread_options Moderation information
     * @param array Thread IDs. Only the first one will be used, but it needs to be an array
     * @return boolean true
     */
    private function execute_thread_moderation($thread_options, $tids)
    {
        $tid = (int)$tids[0]; // Take the first thread to get thread data from
        $query = $this->db->simple_select('threads', 'fid', "tid='$tid'");
        $thread = $this->db->fetch_array($query);

        // If deleting threads, only do that
        if ($thread_options['deletethread'] == 1) {
            foreach ($tids as $tid) {
                $this->delete_thread($tid);
            }
        } else {
            if ($thread_options['mergethreads'] == 1 && count($tids) > 1) { // Merge Threads (ugly temp code until find better fix)
                $tid_list = implode(',', $tids);
                $options = ['order_by' => 'dateline', 'order_dir' => 'DESC'];
                $query = $this->db->simple_select('threads', 'tid, subject', "tid IN ($tid_list)", $options); // Select threads from newest to oldest
                $last_tid = 0;
                while ($tid = $this->db->fetch_array($query)) {
                    if ($last_tid != 0) {
                        $this->merge_threads($last_tid, $tid['tid'], $tid['subject']); // And keep merging them until we get down to one thread.
                    }
                    $last_tid = $tid['tid'];
                }
            }
            if ($thread_options['deletepoll'] == 1) { // Delete poll
                foreach ($tids as $tid) {
                    $this->delete_poll($tid);
                }
            }
            if ($thread_options['removeredirects'] == 1) { // Remove redirects
                foreach ($tids as $tid) {
                    $this->remove_redirects($tid);
                }
            }

            if ($thread_options['removesubscriptions'] == 1) { // Remove thread subscriptions
                $this->remove_thread_subscriptions($tids, true);
            }

            if ($thread_options['approvethread'] == 'approve') { // Approve thread
                $this->approve_threads($tids, $thread['fid']);
            } elseif ($thread_options['approvethread'] == 'unapprove') { // Unapprove thread
                $this->unapprove_threads($tids, $thread['fid']);
            } elseif ($thread_options['approvethread'] == 'toggle') { // Toggle thread visibility
                $this->toggle_thread_visibility($tids, $thread['fid']);
            }

            if ($thread_options['softdeletethread'] == 'softdelete') { // Soft delete thread
                $this->soft_delete_threads($tids);
            } elseif ($thread_options['softdeletethread'] == 'restore') { // Restore thread
                $this->restore_threads($tids);
            } elseif ($thread_options['softdeletethread'] == 'toggle') { // Toggle thread visibility
                $this->toggle_thread_softdelete($tids);
            }

            if ($thread_options['openthread'] == 'open') { // Open thread
                $this->open_threads($tids);
            } elseif ($thread_options['openthread'] == 'close') { // Close thread
                $this->close_threads($tids);
            } elseif ($thread_options['openthread'] == 'toggle') { // Toggle thread visibility
                $this->toggle_thread_status($tids);
            }

            if ($thread_options['stickthread'] == 'stick') { // Stick thread
                $this->stick_threads($tids);
            } elseif ($thread_options['stickthread'] == 'unstick') { // Unstick thread
                $this->unstick_threads($tids);
            } elseif ($thread_options['stickthread'] == 'toggle') { // Toggle thread importance
                $this->toggle_thread_importance($tids);
            }

            if ($thread_options['threadprefix'] != '-1') {
                $this->apply_thread_prefix($tids, $thread_options['threadprefix']); // Update thread prefix
            }

            if (my_strtolower(trim($thread_options['newsubject'])) != '{subject}') { // Update thread subjects
                $this->change_thread_subject($tids, $thread_options['newsubject']);
            }
            if (!empty($thread_options['addreply'])) { // Add reply to thread
                $tid_list = implode(',', $tids);
                $query = $this->db->simple_select(
                    'threads',
                    'uid, fid, subject, tid, firstpost, closed',
                    "tid IN ($tid_list) AND closed NOT LIKE 'moved|%'"
                );

                // Loop threads adding a reply to each one
                while ($thread = $this->db->fetch_array($query)) {
                    $posthandler = new PostDataHandler($this->bb, 'insert');

                    if (empty($thread_options['replysubject'])) {
                        $new_subject = 'RE: ' . $thread['subject'];
                    } else {
                        $new_subject = str_ireplace('{username}', $this->user->username, $thread_options['replysubject']);
                        $new_subject = str_ireplace('{subject}', $thread['subject'], $new_subject);
                    }

                    // Set the post data that came from the input to the $post array.
                    $post = [
                        'tid' => $thread['tid'],
                        'replyto' => $thread['firstpost'],
                        'fid' => $thread['fid'],
                        'subject' => $new_subject,
                        'uid' => $this->user->uid,
                        'username' => $this->user->username,
                        'message' => $thread_options['addreply'],
                        'ipaddress' => $this->session->ipaddress,
                    ];

                    // Set up the post options from the input.
                    $post['options'] = [
                        'signature' => 1,
                        'emailnotify' => 0,
                        'disablesmilies' => 0
                    ];

                    if ($thread['closed'] == 1) {
                        // Keep this thread closed
                        $post['modoptions']['closethread'] = 1;
                    }

                    $posthandler->set_data($post);
                    if ($posthandler->validatePost($post)) {
                        $posthandler->insertPost($post);
                    }
                }
            }
            if ($thread_options['movethread'] > 0 &&
                $thread_options['movethread'] != $thread['fid']
            ) { // Move thread
                if ($thread_options['movethreadredirect'] == 1) { // Move Thread with redirect
                    $time = TIME_NOW + ($thread_options['movethreadredirectexpire'] * 86400);
                    foreach ($tids as $tid) {
                        $this->move_thread($tid, $thread_options['movethread'], 'redirect', $time);
                    }
                } else // Normal move
                {
                    $this->move_threads($tids, $thread_options['movethread']);
                }
            }
            if ($thread_options['copythread'] > 0 || $thread_options['copythread'] == -2) { // Copy thread
                if ($thread_options['copythread'] == -2) {
                    $thread_options['copythread'] = $thread['fid'];
                }
                foreach ($tids as $tid) {
                    $new_tid = $this->move_thread($tid, $thread_options['copythread'], 'copy');
                }
            }
            if (!empty($thread_options['recountrebuild'])) {
                foreach ($tids as $tid) {
                    rebuild_thread_counters($tid);
                }
            }
        }

        // Do we have a PM subject and PM message?
        if (isset($thread_options['pm_subject']) &&
            $thread_options['pm_subject'] != '' &&
            isset($thread_options['pm_message']) &&
            $thread_options['pm_message'] != ''
        ) {
            $tid_list = implode(',', $tids);

            // For each thread, we send a PM to the author
            $query = $this->db->simple_select('threads', 'uid', "tid IN ($tid_list)");
            while ($uid = $this->db->fetch_field($query, 'uid')) {
                // Let's send our PM
                $pm = [
                    'subject' => $thread_options['pm_subject'],
                    'message' => $thread_options['pm_message'],
                    'touid' => $uid
                ];
                $this->pm->send_pm($pm, $this->user->uid, 1);
            }
        }

        return true;
    }

    /**
     * Mark specific reported posts of a certain type as dealt with
     *
     * @param array|int $id An array or int of the ID numbers you're marking as dealt with
     * @param string $type The type of item the above IDs are for - post, posts, thread, threads, forum, all
     */
    function mark_reports($id, $type = 'post')
    {
        switch ($type) {
            case 'posts':
                if (is_array($id)) {
                    $rids = implode($id, "','");
                    $rids = "'0','$rids'";
                    $this->db->update_query('reportedcontent', ['reportstatus' => 1], "id IN($rids) AND reportstatus='0' AND (type = 'post' OR type = '')");
                }
                break;
            case "post":
                $this->db->update_query("reportedcontent", ['reportstatus' => 1], "id='$id' AND reportstatus='0' AND (type = 'post' OR type = '')");
                break;
            case "threads":
                if (is_array($id)) {
                    $rids = implode($id, "','");
                    $rids = "'0','$rids'";
                    $this->db->update_query("reportedcontent", ['reportstatus' => 1], "id2 IN($rids) AND reportstatus='0' AND (type = 'post' OR type = '')");
                }
                break;
            case "thread":
                $this->db->update_query("reportedcontent", ['reportstatus' => 1], "id2='$id' AND reportstatus='0' AND (type = 'post' OR type = '')");
                break;
            case "forum":
                $this->db->update_query("reportedcontent", ['reportstatus' => 1], "id3='$id' AND reportstatus='0' AND (type = 'post' OR type = '')");
                break;
            case "all":
                $this->db->update_query("reportedcontent", ['reportstatus' => 1], "reportstatus='0' AND (type = 'post' OR type = '')");
                break;
        }

        $arguments = ['id' => $id, 'type' => $type];
        $this->plugins->runHooks("mark_reports", $arguments);
        $this->cache->update_reportedcontent();
    }
}
