<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Core;

use RunBB\Handlers\CacheHandlers\CacheHandlerInterface;
use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class DataCache extends AbstractController
{
    /**
     * Cache contents.
     *
     * @var array
     */
    public $cache = [];

    /**
     * The current cache handler we're using
     *
     * @var CacheHandlerInterface
     */
    public $handler = null;

    /**
     * A count of the number of calls.
     *
     * @var int
     */
    public $call_count = 0;

    /**
     * A list of the performed calls.
     *
     * @var array
     */
    public $calllist = [];

    /**
     * The time spent on cache operations
     *
     * @var float
     */
    public $call_time = 0;

    /**
     * Explanation of a cache call.
     *
     * @var string
     */
    public $cache_debug;

    protected $moderators_forum_cache = [];

    /**
     * Build cache data.
     *
     */
    public function cache()
    {
        switch ($this->bb->config['cache_store']) {
            // Disk cache
            case 'files':
                $this->handler = new \RunBB\Handlers\CacheHandlers\DiskCacheHandler($this->bb);
                break;
            // Memcache cache
            case 'memcache':
                $this->handler = new \RunBB\Handlers\CacheHandlers\MemcacheCacheHandler();
                break;
            // Memcached cache
            case 'memcached':
                $this->handler = new \RunBB\Handlers\CacheHandlers\MemcachedCacheHandler();
                break;
            // eAccelerator cache
            case 'eaccelerator':
                $this->handler = new \RunBB\Handlers\CacheHandlers\EacceleratorCacheHandler();
                break;
            // Xcache cache
            case 'xcache':
                $this->handler = new \RunBB\Handlers\CacheHandlers\XcacheCacheHandler();
                break;
            // APC cache
            case 'apc':
                $this->handler = new \RunBB\Handlers\CacheHandlers\ApcCacheHandler();
                break;
        }

        if ($this->handler instanceof CacheHandlerInterface) {
            if (!$this->handler->connect()) {
                $this->handler = null;
            }
        } else {
            // Database cache
            $query = $this->db->simple_select('datacache', 'title,cache');
            while ($data = $this->db->fetch_array($query)) {
                $this->cache[$data['title']] = unserialize($data['cache']);
            }
        }
    }

    /**
     * Read cache from files or db.
     *
     * @param string $name The cache component to read.
     * @param boolean $hard If true, cannot be overwritten during script execution.
     * @return mixed
     */
    public function read($name, $hard = false)
    {
        // Already have this cache and we're not doing a hard refresh? Return cached copy
        if (isset($this->cache[$name]) && $hard == false) {
            return $this->cache[$name];
        } // If we're not hard refreshing, and this cache doesn't exist, return false
        // It would have been loaded pre-global if it did exist anyway...
        elseif ($hard == false && !($this->handler instanceof CacheHandlerInterface)) {
            return false;
        }

        if ($this->handler instanceof CacheHandlerInterface) {
            get_execution_time();

            $data = $this->handler->fetch($name);

            $call_time = get_execution_time();
            $this->call_time += $call_time;
            $this->call_count++;

            if ($this->bb->debug_mode) {
                $hit = true;
                if ($data === false) {
                    $hit = false;
                }
                $this->debug_call('read: ' . $name, $call_time, $hit);
            }

            // No data returned - cache gone bad?
            if ($data === false) {
                // Fetch from database
                $query = $this->db->simple_select('datacache', 'title,cache', "title='" . $this->db->escape_string($name) . "'");
                $cache_data = $this->db->fetch_array($query);
                $data = unserialize($cache_data['cache']);

                // Update cache for handler
                get_execution_time();

                $hit = $this->handler->put($name, $data);

                $call_time = get_execution_time();
                $this->call_time += $call_time;
                $this->call_count++;

                if ($this->bb->debug_mode) {
                    $this->debug_call('set: ' . $name, $call_time, $hit);
                }
            }
        } // Else, using internal database cache
        else {
            $query = $this->db->simple_select('datacache', 'title,cache', "title='$name'");
            $cache_data = $this->db->fetch_array($query);

            if (!$cache_data['title']) {
                $data = false;
            } else {
                $data = unserialize($cache_data['cache']);
            }
        }

        // Cache locally
        $this->cache[$name] = $data;

        if ($data !== false) {
            return $data;
        } else {
            return false;
        }
    }

    /**
     * Update cache contents.
     *
     * @param string $name The cache content identifier.
     * @param string $contents The cache content.
     */
    function update($name, $contents)
    {
        $this->cache[$name] = $contents;

        // We ALWAYS keep a running copy in the db just incase we need it
//        $dbcontents = $this->db->escape_string(serialize($contents));

//        $replace_array = [
//            'title' => $this->db->escape_string($name),
//            'cache' => $dbcontents
//        ];
//        $this->db->replace_query('datacache', $replace_array, '', false);

        $c = \RunBB\Models\Datacache::firstOrCreate(['title' => $name]);
        $c->cache = serialize($contents);
        $c->save();

        // Do we have a cache handler we're using?
        if ($this->handler instanceof CacheHandlerInterface) {
            get_execution_time();

            $hit = $this->handler->put($name, $contents);

            $call_time = get_execution_time();
            $this->call_time += $call_time;
            $this->call_count++;

            if ($this->bb->debug_mode) {
                $this->debug_call('update:' . $name, $call_time, $hit);
            }
        }
    }

    /**
     * Delete cache contents.
     * Originally from frostschutz's PluginLibrary
     * github.com/frostschutz
     *
     * @param string $name Cache name or title
     * @param boolean $greedy To delete a cache starting with name_
     */
    function delete($name, $greedy = false)
    {
        // Prepare for database query.
        $dbname = $this->db->escape_string($name);
        $where = "title = '{$dbname}'";

        // Delete on-demand or handler cache
        if ($this->handler instanceof CacheHandlerInterface) {
            get_execution_time();

            $hit = $this->handler->delete($name);

            $call_time = get_execution_time();
            $this->call_time += $call_time;
            $this->call_count++;

            if ($this->bb->debug_mode) {
                $this->debug_call('delete:' . $name, $call_time, $hit);
            }
        }

        // Greedy?
        if ($greedy) {
            $name .= '_';
            $names = [];
            $keys = array_keys($this->bb->cache->cache);

            foreach ($keys as $key) {
                if (strpos($key, $name) === 0) {
                    $names[$key] = 0;
                }
            }

            $ldbname = strtr(
                $dbname,
                [
                    '%' => '=%',
                    '=' => '==',
                    '_' => '=_'
                ]
            );

            $where .= " OR title LIKE '{$ldbname}=_%' ESCAPE '='";

            if ($this->handler instanceof CacheHandlerInterface) {
                $query = $this->db->simple_select('datacache', 'title', $where);

                while ($row = $this->db->fetch_array($query)) {
                    $names[$row['title']] = 0;
                }

                // ...from the filesystem...
                $start = strlen(MYBB_ROOT . 'cache/');
                foreach ((array)@glob(MYBB_ROOT . "cache/{$name}*.php") as $filename) {
                    if ($filename) {
                        $filename = substr($filename, $start, strlen($filename) - 4 - $start);
                        $names[$filename] = 0;
                    }
                }

                foreach ($names as $key => $val) {
                    get_execution_time();

                    $hit = $this->handler->delete($key);

                    $call_time = get_execution_time();
                    $this->call_time += $call_time;
                    $this->call_count++;

                    if ($this->bb->debug_mode) {
                        $this->debug_call('delete:' . $name, $call_time, $hit);
                    }
                }
            }
        }

        // Delete database cache
        $this->db->delete_query('datacache', $where);
    }

    /**
     * Debug a cache call to a non-database cache handler
     *
     * @param string $string The cache key
     * @param string $qtime The time it took to perform the call.
     * @param boolean $hit Hit or miss status
     */
    function debug_call($string, $qtime, $hit)
    {
        $debug_extra = '';
        if ($this->bb->plugins->current_hook) {
            $debug_extra = "<div style=\"float_right\">(Plugin Hook: {$this->bb->plugins->current_hook})</div>";
        }

        if ($hit) {
            $hit_status = 'HIT';
        } else {
            $hit_status = 'MISS';
        }

        $cache_data = explode(':', $string);
        $cache_method = $cache_data[0];
        $cache_key = $cache_data[1];

        $this->cache_debug = "<table style=\"background-color: #666;\" width=\"95%\" cellpadding=\"4\" cellspacing=\"1\" align=\"center\">
<tr>
	<td style=\"background-color: #ccc;\">{$debug_extra}<div><strong>#{$this->call_count} - " . ucfirst($cache_method) . " Call</strong></div></td>
</tr>
<tr style=\"background-color: #fefefe;\">
	<td><span style=\"font-family: Courier; font-size: 14px;\">({$this->bb->config['cache_store']}) [{$hit_status}] " . htmlspecialchars_uni($cache_key) . "</span></td>
</tr>
<tr>
	<td bgcolor=\"#ffffff\">Call Time: " . format_time_duration($qtime) . "</td>
</tr>
</table>
<br />\n";

        $this->calllist[$this->call_count]['key'] = $string;
        $this->calllist[$this->call_count]['time'] = $qtime;
    }

    /**
     * Select the size of the cache
     *
     * @param string $name The name of the cache
     * @return integer the size of the cache
     */
    function sizeOf($name = '')
    {
        if ($this->handler instanceof CacheHandlerInterface) {
            $size = $this->handler->sizeOf($name);
            if (!$size) {
                if ($name) {
                    $query = $this->db->simple_select('datacache', 'cache', "title='{$name}'");
                    return strlen($this->db->fetch_field($query, 'cache'));
                } else {
                    return $this->db->fetch_size('datacache');
                }
            } else {
                return $size;
            }
        } // Using MySQL as cache
        else {
            if ($name) {
                $query = $this->db->simple_select('datacache', 'cache', "title='{$name}'");
                return strlen($this->db->fetch_field($query, 'cache'));
            } else {
                return $this->db->fetch_size('datacache');
            }
        }
    }

    /**
     * Update the MyBB version in the cache.
     *
     */
    public function update_version()
    {
        $version = [
            'version' => $this->bb->version,
            'version_code' => $this->bb->version_code
        ];

        $this->update('version', $version);
    }

    /**
     * Update the attachment type cache.
     *
     */
    public function update_attachtypes()
    {
        $types = [];

        $query = $this->db->simple_select('attachtypes', '*');
        while ($type = $this->db->fetch_array($query)) {
            $type['extension'] = my_strtolower($type['extension']);
            $types[$type['extension']] = $type;
        }

        $this->update('attachtypes', $types);
    }

    /**
     * Update the smilies cache.
     *
     */
    public function update_smilies()
    {
        $smilies = [];

        $query = $this->db->simple_select('smilies', '*', '', ['order_by' => 'disporder', 'order_dir' => 'ASC']);
        while ($smilie = $this->db->fetch_array($query)) {
            $smilies[$smilie['sid']] = $smilie;
        }

        $this->update('smilies', $smilies);
    }

    /**
     * Update the posticon cache.
     *
     */
    public function update_posticons()
    {
        $icons = [];

        $query = $this->db->simple_select('icons', 'iid, name, path');
        while ($icon = $this->db->fetch_array($query)) {
            $icons[$icon['iid']] = $icon;
        }

        $this->update('posticons', $icons);
    }

    /**
     * Update the badwords cache.
     *
     */
    public function update_badwords()
    {
        $badwords = [];

        $query = $this->db->simple_select('badwords', '*');
        while ($badword = $this->db->fetch_array($query)) {
            $badwords[$badword['bid']] = $badword;
        }

        $this->update('badwords', $badwords);
    }

    /**
     * Update the usergroups cache.
     *
     */
    public function update_usergroups()
    {
        $query = $this->db->simple_select('usergroups');

        $gs = [];
        while ($g = $this->db->fetch_array($query)) {
            $gs[$g['gid']] = $g;
        }

        $this->update('usergroups', $gs);
    }

    /**
     * Update the forum permissions cache.
     *
     * @return bool When failed, returns false.
     */
    public function update_forumpermissions()
    {
        $this->built_forum_permissions = [0];
        $this->forum_permissions_forum_cache = $this->forum_permissions = [];

        // Get our forum list
        $this->bb->forum->cache_forums(true);
        if (!is_array($this->bb->forum_cache)) {
            return false;
        }

        reset($this->bb->forum_cache);
        $this->bb->fcache = [];

        // Resort in to the structure we require
        foreach ($this->bb->forum_cache as $fid => $forum) {
            $this->forum_permissions_forum_cache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
        }

        // Sort children
        foreach ($this->bb->fcache as $pid => $value) {
            ksort($this->bb->fcache[$pid]);
        }
        ksort($this->bb->fcache);

        // Fetch forum permissions from the database
        $query = $this->db->simple_select('forumpermissions');
        while ($forum_permission = $this->db->fetch_array($query)) {
            $this->forum_permissions[$forum_permission['fid']][$forum_permission['gid']] = $forum_permission;
        }

        $this->build_forum_permissions();
        $this->update('forumpermissions', $this->built_forum_permissions);

        return true;
    }

    /**
     * Build the forum permissions array
     *
     * @access private
     * @param array $permissions An optional permissions array.
     * @param int $pid An optional permission id.
     */
    private function build_forum_permissions($permissions = [], $pid = 0)
    {
        $usergroups = array_keys($this->read('usergroups', true));
        if (isset($this->forum_permissions_forum_cache[$pid])) {
            foreach ($this->forum_permissions_forum_cache[$pid] as $main) {
                foreach ($main as $forum) {
                    $perms = $permissions;
                    foreach ($usergroups as $gid) {
                        if (isset($this->forum_permissions[$forum['fid']][$gid])) {
                            $perms[$gid] = $this->forum_permissions[$forum['fid']][$gid];
                        }
                        if (isset($perms[$gid])) {
                            $perms[$gid]['fid'] = $forum['fid'];
                            $this->built_forum_permissions[$forum['fid']][$gid] = $perms[$gid];
                        }
                    }
                    $this->build_forum_permissions($perms, $forum['fid']);
                }
            }
        }
    }

    /**
     * Update the stats cache (kept for the sake of being able to rebuild this cache via the cache interface)
     *
     */
    public function update_stats()
    {
        (new \RunBB\Helpers\Restorer($this->bb))->rebuild_stats();
    }

    /**
     * Update the statistics cache
     *
     */
    public function update_statistics()
    {
//        $query = $this->db->simple_select('users',
//            'uid, username, referrals',
//            'referrals>0',
//            array('order_by' => 'referrals', 'order_dir' => 'DESC', 'limit' => 1));
//        $topreferrer = $this->db->fetch_array($query);

        $tr = \RunBB\Models\User::where('referrals', '>', 0)
            ->take(1)
            ->orderBy('referrals', 'desc')
            ->get(['uid', 'username', 'referrals'])
            ->toArray();

//        $timesearch = TIME_NOW - 86400;
//        switch ($this->db->type) {
//            case 'pgsql':
//                $group_by = $this->db->build_fields_string('users', 'u.');
//                break;
//            default:
//                $group_by = 'p.uid';
//                break;
//        }

//        $query = $this->db->query('
//			SELECT u.uid, u.username, COUNT(pid) AS poststoday
//			FROM ' . TABLE_PREFIX . 'posts p
//			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (p.uid=u.uid)
//			WHERE p.dateline>' . $timesearch . '
//			GROUP BY ' . $group_by . ' ORDER BY poststoday DESC
//			LIMIT 1
//		');
//        $topposter = $this->db->fetch_array($query);

        $tp = \RunBB\Models\Post::where('posts.dateline', '>', (TIME_NOW - 86400))//$timesearch)
            ->leftJoin('users', 'posts.uid', '=', 'users.uid')
            ->take(1)
            ->groupBy('posts.uid')
            ->orderBy('poststoday', 'desc')
            ->get(['users.uid', 'users.username', DB::raw('COUNT(pid) AS poststoday')])
            ->toArray();

//        $query = $this->db->simple_select('users', 'COUNT(uid) AS posters', 'postnum>0');
//        $posters = $this->db->fetch_field($query, 'posters');

        $p = \RunBB\Models\User::where('postnum', '>', 0)
            ->count();

        $statistics = [
            'time' => TIME_NOW,
            'top_referrer' => $tr[0],//(array)$topreferrer,
            'top_poster' => (isset($tp[0]) ? $tp[0] : 0),//(array)$topposter,
            'posters' => $p//$posters
        ];

        $this->update('statistics', $statistics);
    }

    /**
     * Update the moderators cache.
     *
     * @return bool Returns false on failure
     */
    public function update_moderators()
    {
        $this->built_moderators = [0];

        // Get our forum list
        $this->bb->forum->cache_forums(true);
        if (!isset($this->bb->forum_cache)) {
            return false;
        }

        reset($this->bb->forum_cache);
        $this->bb->fcache = [];

        // Resort in to the structure we require
        foreach ($this->bb->forum_cache as $fid => $forum) {
            $this->moderators_forum_cache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
        }

        // Sort children
        foreach ($this->bb->fcache as $pid => $value) {
            ksort($this->bb->fcache[$pid]);
        }
        ksort($this->bb->fcache);

        $this->moderators = [];

        // Fetch moderators from the database
        $query = $this->db->query('
			SELECT m.*, u.username, u.usergroup, u.displaygroup
			FROM ' . TABLE_PREFIX . 'moderators m
			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (m.id=u.uid)
			WHERE m.isgroup = "0"
			ORDER BY u.username
		');
        while ($moderator = $this->db->fetch_array($query)) {
            $this->moderators[$moderator['fid']]['users'][$moderator['id']] = $moderator;
        }

//    if(!function_exists('sort_moderators_by_usernames'))
//    {
//      function sort_moderators_by_usernames($a, $b)
//      {
//        return strcasecmp($a['username'], $b['username']);
//      }
//    }

        //Fetch moderating usergroups from the database
        $query = $this->db->query('
			SELECT m.*, u.title
			FROM ' . TABLE_PREFIX . 'moderators m
			LEFT JOIN ' . TABLE_PREFIX . 'usergroups u ON (m.id=u.gid)
			WHERE m.isgroup = "1"
			ORDER BY u.title
		');
        while ($moderator = $this->db->fetch_array($query)) {
            $this->moderators[$moderator['fid']]['usergroups'][$moderator['id']] = $moderator;
        }

        if (is_array($this->moderators)) {
            foreach (array_keys($this->moderators) as $fid) {
                uasort($this->moderators[$fid], [$this, 'sort_moderators_by_usernames']);
            }
        }

        $this->build_moderators();

        $this->update('moderators', $this->built_moderators);

        return true;
    }

    private function sort_moderators_by_usernames($a, $b)
    {
        return strcasecmp($a['username'], $b['username']);
    }

    /**
     * Update the users awaiting activation cache.
     *
     */
    public function update_awaitingactivation()
    {
        $query = $this->db->simple_select('users', 'COUNT(uid) AS awaitingusers', 'usergroup=\'5\'');
        $awaitingusers = (int)$this->db->fetch_field($query, 'awaitingusers');

        $data = [
            'users' => $awaitingusers,
            'time' => TIME_NOW
        ];

        $this->update('awaitingactivation', $data);
    }

    /**
     * Build the moderators array
     *
     * @access private
     * @param array $moderators An optional moderators array (moderators of the parent forum for example).
     * @param int $pid An optional parent ID.
     */
    private function build_moderators($moderators = [], $pid = 0)
    {
        if (isset($this->moderators_forum_cache[$pid])) {
            foreach ($this->moderators_forum_cache[$pid] as $main) {
                foreach ($main as $forum) {
                    $forum_mods = '';
                    if (count($moderators)) {
                        $forum_mods = $moderators;
                    }
                    // Append - local settings override that of a parent - array_merge works here
                    if (isset($this->moderators[$forum['fid']])) {
                        if (is_array($forum_mods) && count($forum_mods)) {
                            $forum_mods = array_merge($forum_mods, $this->moderators[$forum['fid']]);
                        } else {
                            $forum_mods = $this->moderators[$forum['fid']];
                        }
                    }
                    $this->built_moderators[$forum['fid']] = $forum_mods;
                    $this->build_moderators($forum_mods, $forum['fid']);
                }
            }
        }
    }

    /**
     * Update the forums cache.
     *
     */
    public function update_forums()
    {
        $forums = [];
        // Things we don't want to cache
        $exclude = ['unapprovedthreads', 'unapprovedposts', 'threads', 'posts', 'lastpost',
            'lastposter', 'lastposttid', 'lastposteruid', 'lastpostsubject', 'deletedthreads', 'deletedposts'];

//        $query = $this->db->simple_select('forums', '*', '', array('order_by' => 'pid,disporder'));
        $f = \RunBB\Models\Forum::orderBy('pid')
            ->orderBy('disporder')
            ->get()
            ->toArray();

//        while ($forum = $this->db->fetch_array($query)) {
        foreach ($f as $forum) {
            foreach ($forum as $key => $val) {
                if (in_array($key, $exclude)) {
                    unset($forum[$key]);
                }
            }
            $forums[$forum['fid']] = $forum;
        }

        $this->update('forums', $forums);
    }

    /**
     * Update usertitles cache.
     *
     */
    public function update_usertitles()
    {
        $usertitles = [];
        $query = $this->db->simple_select(
            'usertitles',
            'utid, posts, title, stars, starimage',
            '',
            ['order_by' => 'posts', 'order_dir' => 'DESC']
        );
        while ($usertitle = $this->db->fetch_array($query)) {
            $usertitles[] = $usertitle;
        }

        $this->update('usertitles', $usertitles);
    }

    /**
     * Update reported content cache.
     *
     */
    public function update_reportedcontent()
    {
        $query = $this->db->simple_select('reportedcontent', 'COUNT(rid) AS unreadcount', "reportstatus='0'");
        $num = $this->db->fetch_array($query);

        $query = $this->db->simple_select('reportedcontent', 'COUNT(rid) AS reportcount');
        $total = $this->db->fetch_array($query);

        $query = $this->db->simple_select(
            'reportedcontent',
            'dateline',
            "reportstatus='0'",
            ['order_by' => 'dateline', 'order_dir' => 'DESC']
        );
        $latest = $this->db->fetch_array($query);

        $reasons = [];

        if (!empty($this->bb->settings['reportreasons'])) {
            $options = $this->bb->settings['reportreasons'];
            $options = explode("\n", $options);

            foreach ($options as $option) {
                $option = explode('=', $option);
                $reasons[$option[0]] = $option[1];
            }
        }

        $reports = [
            'unread' => $num['unreadcount'],
            'total' => $total['reportcount'],
            'lastdateline' => $latest['dateline'],
            'reasons' => $reasons
        ];

        $this->update('reportedcontent', $reports);
    }

    /**
     * Update mycode cache.
     *
     */
    public function update_mycode()
    {
        $mycodes = [];
        $query = $this->db->simple_select('mycode', 'regex, replacement', 'active=1', ['order_by' => 'parseorder']);
        while ($mycode = $this->db->fetch_array($query)) {
            $mycodes[] = $mycode;
        }

        $this->update('mycode', $mycodes);
    }

    /**
     * Update the mailqueue cache
     *
     * @param int $last_run
     * @param int $lock_time
     */
    public function update_mailqueue($last_run = 0, $lock_time = 0)
    {
        $query = $this->db->simple_select('mailqueue', 'COUNT(*) AS queue_size');
        $queue_size = $this->db->fetch_field($query, 'queue_size');

        $mailqueue = $this->read('mailqueue');
        if (!is_array($mailqueue)) {
            $mailqueue = [];
        }
        $mailqueue['queue_size'] = $queue_size;
        if ($last_run > 0) {
            $mailqueue['last_run'] = $last_run;
        }
        $mailqueue['locked'] = $lock_time;

        $this->update('mailqueue', $mailqueue);
    }

    /**
     * Update update_check cache (dummy function used by upgrade/install scripts)
     */
    public function update_update_check()
    {
        $update_cache = [
            'dateline' => TIME_NOW
        ];

        $this->update('update_check', $update_cache);
    }

    /**
     * Update default_theme cache
     */
    public function update_default_theme()
    {
        $query = $this->db->simple_select('themes', 'name, tid, properties, stylesheets', "def='1'", ['limit' => 1]);
        $theme = $this->db->fetch_array($query);
        $this->update('default_theme', $theme);
    }

    /**
     * Updates the tasks cache saving the next run time
     */
    public function update_tasks()
    {
        $query = $this->db->simple_select(
            'tasks',
            'nextrun',
            'enabled=1',
            ['order_by' => 'nextrun', 'order_dir' => 'asc', 'limit' => 1]
        );
        $next_task = $this->db->fetch_array($query);

        $task_cache = $this->read('tasks');
        if (!is_array($task_cache)) {
            $task_cache = [];
        }
        $task_cache['nextrun'] = $next_task['nextrun'];

        if (!$task_cache['nextrun']) {
            $task_cache['nextrun'] = TIME_NOW + 3600;
        }

        $this->update('tasks', $task_cache);
    }

    /**
     * Updates the banned IPs cache
     */
    public function update_bannedips()
    {
        $banned_ips = [];
        $query = $this->db->simple_select('banfilters', 'fid,filter', 'type=1');
        while ($banned_ip = $this->db->fetch_array($query)) {
            $banned_ips[$banned_ip['fid']] = $banned_ip;
        }
        $this->update('bannedips', $banned_ips);
    }

    /**
     * Updates the banned emails cache
     */
    public function update_bannedemails()
    {
        $banned_emails = [];
        $query = $this->db->simple_select('banfilters', 'fid, filter', "type = '3'");

        while ($banned_email = $this->db->fetch_array($query)) {
            $banned_emails[$banned_email['fid']] = $banned_email;
        }

        $this->update('bannedemails', $banned_emails);
    }

    /**
     * Updates the search engine spiders cache
     */
    public function update_spiders()
    {
        $spiders = [];
        $query = $this->db->simple_select(
            'spiders',
            'sid, name, useragent, usergroup',
            '',
            ['order_by' => 'LENGTH(useragent)', 'order_dir' => 'DESC']
        );
        while ($spider = $this->db->fetch_array($query)) {
            $spiders[$spider['sid']] = $spider;
        }
        $this->update('spiders', $spiders);
    }

    public function update_most_replied_threads()
    {
        $threads = [];
//        $query = $this->db->simple_select('threads', 'tid, subject, replies, fid', "visible='1'",
//            ['order_by' => 'replies', 'order_dir' => 'DESC', 'limit_start' => 0, 'limit' => $this->bb->settings['statslimit']]);
        $t = \RunBB\Models\Thread::where('visible', '=', 1)
            ->skip(0)
            ->take($this->bb->settings['statslimit'])
            ->orderBy('replies', 'desc')
            ->get(['tid', 'subject', 'replies', 'fid'])
            ->toArray();

//        while ($thread = $this->db->fetch_array($query)) {
//        foreach ($t as $thread) {
//            $threads[] = $thread;
//        }

        $this->update('most_replied_threads', $t);//$threads);
    }

    public function update_most_viewed_threads()
    {
//        $threads = [];
//        $query = $this->db->simple_select('threads', 'tid, subject, views, fid', "visible='1'",
//            ['order_by' => 'views', 'order_dir' => 'DESC', 'limit_start' => 0, 'limit' => $this->bb->settings['statslimit']]);
        $t = \RunBB\Models\Thread::where('visible', '=', 1)
            ->skip(0)
            ->take($this->bb->settings['statslimit'])
            ->orderBy('views', 'desc')
            ->get(['tid', 'subject', 'views', 'fid'])
            ->toArray();

//        while ($thread = $this->db->fetch_array($query)) {
//        foreach ($t as $thread) {
//            $threads[] = $thread;
//        }

        $this->update('most_viewed_threads', $t);//$threads);
    }

    public function update_banned()
    {
        $bans = [];
        $query = $this->db->simple_select('banned');
        while ($ban = $this->db->fetch_array($query)) {
            $bans[$ban['uid']] = $ban;
        }

        $this->update('banned', $bans);
    }

    public function update_birthdays()
    {
        $birthdays = [];
        // Get today, yesterday, and tomorrow's time (for different timezones)
        $bdaytime = TIME_NOW;
        $bdaydate = $this->time->formatDate('j-n', $bdaytime, '', 0);
        $bdaydatetomorrow = $this->time->formatDate('j-n', ($bdaytime + 86400), '', 0);
        $bdaydateyesterday = $this->time->formatDate('j-n', ($bdaytime - 86400), '', 0);

        $query = $this->db->simple_select('users', 'uid, username, usergroup, displaygroup, birthday, birthdayprivacy', "birthday LIKE '$bdaydate-%' OR birthday LIKE '$bdaydateyesterday-%' OR birthday LIKE '$bdaydatetomorrow-%'");
        while ($bday = $this->db->fetch_array($query)) {
            // Pop off the year from the birthday because we don't need it.
            $bday['bday'] = explode('-', $bday['birthday']);
            array_pop($bday['bday']);
            $bday['bday'] = implode('-', $bday['bday']);

            if ($bday['birthdayprivacy'] != 'all') {
                ++$birthdays[$bday['bday']]['hiddencount'];
                continue;
            }

            // We don't need any excess caleries in the cache
            unset($bday['birthdayprivacy']);

            $birthdays[$bday['bday']]['users'][] = $bday;
        }

        $this->update('birthdays', $birthdays);
    }

    public function update_groupleaders()
    {
        $groupleaders = [];
        $query = $this->db->simple_select('groupleaders');
        while ($groupleader = $this->db->fetch_array($query)) {
            $groupleaders[$groupleader['uid']][] = $groupleader;
        }

        $this->update('groupleaders', $groupleaders);
    }

    public function update_threadprefixes()
    {
        $prefixes = [];
        $query = $this->db->simple_select('threadprefixes', '*', '', ['order_by' => 'prefix', 'order_dir' => 'ASC']);

        while ($prefix = $this->db->fetch_array($query)) {
            $prefixes[$prefix['pid']] = $prefix;
        }

        $this->update('threadprefixes', $prefixes);
    }

    public function update_forumsdisplay()
    {
        $fd_statistics = [];

        $time = TIME_NOW; // Look for announcements that don't end, or that are ending some time in the future
        $query = $this->db->simple_select('announcements', 'fid', "enddate = '0' OR enddate > '{$time}'", ['order_by' => 'aid']);

        if ($this->db->num_rows($query)) {
            while ($forum = $this->db->fetch_array($query)) {
                if (!isset($fd_statistics[$forum['fid']]['announcements'])) {
                    $fd_statistics[$forum['fid']]['announcements'] = 1;
                }
            }
        }

        // Do we have any mod tools to use in our forums?
        $query = $this->db->simple_select('modtools', 'forums, tid', '', ['order_by' => 'tid']);

        if ($this->db->num_rows($query)) {
            unset($forum);
            while ($tool = $this->db->fetch_array($query)) {
                $forums = explode(',', $tool['forums']);

                foreach ($forums as $forum) {
                    if (!$forum) {
                        $forum = -1;
                    }

                    if (!isset($fd_statistics[$forum]['modtools'])) {
                        $fd_statistics[$forum]['modtools'] = 1;
                    }
                }
            }
        }

        $this->update('forumsdisplay', $fd_statistics);
    }

    /**
     * Update profile fields cache.
     *
     */
    public function update_profilefields()
    {
        $fields = [];
        $query = $this->db->simple_select('profilefields', '*', '', ['order_by' => 'disporder']);
        while ($field = $this->db->fetch_array($query)) {
            $fields[] = $field;
        }

        $this->update('profilefields', $fields);
    }

    /* Other, extra functions for reloading caches if we just changed to another cache extension (i.e. from db -> xcache) */
    public function reload_mostonline()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='mostonline'");
        $this->update('mostonline', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_plugins()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='plugins'");
        $this->update('plugins', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_last_backup()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='last_backup'");
        $this->update('last_backup', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_internal_settings()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='internal_settings'");
        $this->update('internal_settings', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_version_history()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='version_history'");
        $this->update('version_history', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_modnotes()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='modnotes'");
        $this->update('modnotes', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_adminnotes()
    {
        $query = $this->db->simple_select('datacache', 'title,cache', "title='adminnotes'");
        $this->update('adminnotes', unserialize($this->db->fetch_field($query, 'cache')));
    }

    public function reload_mybb_credits()
    {
        return $this->response->withRedirect($this->bb->admin_url . '/credits?fetch_new=-2');
    }
}
