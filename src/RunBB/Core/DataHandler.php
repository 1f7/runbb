<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

class DataHandler
{
    /**
     * The data being managed by the data handler
     *
     * @var array Data being handled by the data handler.
     */
    public $data = [];

    /**
     * Whether or not the data has been validated. Note: "validated" != "valid".
     *
     * @var boolean True when validated, false when not validated.
     */
    public $is_validated = false;

    /**
     * The errors that occurred when handling data.
     *
     * @var array
     */
    public $errors = [];

    /**
     * The status of administrator override powers.
     *
     * @var boolean
     */
    public $admin_override = false;

    /**
     * Defines if we're performing an update or an insert.
     *
     * @var string
     */
    public $method;

    /**
     * The prefix for the language variables used in the data handler.
     *
     * @var string
     */
    public $language_prefix = '';


    protected $bb;

    /**
     * Constructor for the data handler.
     *
     * @param string $method The method we're performing with this object.
     */
    public function __construct(& $bb, $method = 'insert')
    {
        $this->bb = $bb;
        if ($method != 'update' && $method != 'insert' && $method != 'get' && $method != 'delete') {
            die('A valid method was not supplied to the data handler.');
        }
        $this->method = $method;
    }

    /**
     * Sets the data to be used for the data handler
     *
     * @param array $data The data.
     * @return bool
     */
    function set_data($data)
    {
        if (!is_array($data)) {
            return false;
        }
        $this->data = $data;
        return true;
    }

    /**
     * Add an error to the error array.
     *
     * @param string $error The error name.
     * @param string $data
     */
    protected function setError($error, $data = '')
    {
        $this->errors[$error] = [
            'error_code' => $error,
            'data' => $data
        ];
    }

    /**
     * Returns the error(s) that occurred when handling data.
     *
     * @return array An array of errors.
     */
    function get_errors()
    {
        return $this->errors;
    }

    /**
     * Returns the error(s) that occurred when handling data
     * in a format that MyBB can handle.
     *
     * @return array An array of errors in a MyBB format.
     */
    function get_friendly_errors()
    {
        // Load the language pack we need
        if ($this->language_file) {
            $this->bb->lang->load($this->language_file, true);
        }
        // Prefix all the error codes with the language prefix.
        $errors = [];
        foreach ($this->errors as $error) {
            $lang_string = $this->language_prefix . '_' . $error['error_code'];
            if (!isset($this->bb->lang->$lang_string)) {
                $errors[] = $error['error_code'];
                continue;
            }

            if (!empty($error['data']) && !is_array($error['data'])) {
                $error['data'] = [$error['data']];
            }

            if (is_array($error['data'])) {
                array_unshift($error['data'], $this->bb->lang->$lang_string);
                $errors[] = call_user_func_array([$this->bb->lang, 'sprintf'], $error['data']);
            } else {
                $errors[] = $this->bb->lang->$lang_string;
            }
        }
        return $errors;
    }

    /**
     * Sets whether or not we are done validating.
     *
     * @param boolean True when done, false when not done.
     */
    function set_validated($validated = true)
    {
        $this->is_validated = $validated;
    }

    /**
     * Returns whether or not we are done validating.
     *
     * @return boolean True when done, false when not done.
     */
    function get_validated()
    {
        if ($this->is_validated == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifies if yes/no options haven't been modified.
     *
     * @param array $options The user options array.
     * @param string $option The specific option to check.
     * @param int|bool $default Optionally specify if the default should be used.
     */
    protected function verify_yesno_option(&$options, $option, $default = 1)
    {
        if ($this->method == 'insert' || array_key_exists($option, $options)) {
            if (isset($options[$option]) && $options[$option] != $default && $options[$option] != '') {
                if ($default == 1) {
                    $options[$option] = 0;
                } else {
                    $options[$option] = 1;
                }
            } elseif (@array_key_exists($option, $options) && $options[$option] == '') {
                $options[$option] = 0;
            } else {
                $options[$option] = $default;
            }
        }
    }
}
