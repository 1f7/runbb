<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Editor extends AbstractController
{
    /**
     * Build the javascript based MyCode inserter.
     *
     * @param string $bind The ID of the textarea to bind to. Defaults to 'message'.
     * @param bool $smilies Whether to include smilies. Defaults to true.
     *
     * @return string The MyCode inserter
     */
    public function build_mycode_inserter($bind = 'message', $smilies = true)
    {
        $codeinsert = false;
        if ($this->bb->settings['bbcodeinserter'] != 0) {
            $editor_lang_strings = [
                'editor_bold' => 'Bold',
                'editor_italic' => 'Italic',
                'editor_underline' => 'Underline',
                'editor_strikethrough' => 'Strikethrough',
                'editor_subscript' => 'Subscript',
                'editor_superscript' => 'Superscript',
                'editor_alignleft' => 'Align left',
                'editor_center' => 'Center',
                'editor_alignright' => 'Align right',
                'editor_justify' => 'Justify',
                'editor_fontname' => 'Font Name',
                'editor_fontsize' => 'Font Size',
                'editor_fontcolor' => 'Font Color',
                'editor_removeformatting' => 'Remove Formatting',
                'editor_cut' => 'Cut',
                'editor_cutnosupport' => 'Your browser does not allow the cut command. Please use the keyboard shortcut Ctrl/Cmd-X',
                'editor_copy' => 'Copy',
                'editor_copynosupport' => 'Your browser does not allow the copy command. Please use the keyboard shortcut Ctrl/Cmd-C',
                'editor_paste' => 'Paste',
                'editor_pastenosupport' => 'Your browser does not allow the paste command. Please use the keyboard shortcut Ctrl/Cmd-V',
                'editor_pasteentertext' => 'Paste your text inside the following box:',
                'editor_pastetext' => 'PasteText',
                'editor_numlist' => 'Numbered list',
                'editor_bullist' => 'Bullet list',
                'editor_undo' => 'Undo',
                'editor_redo' => 'Redo',
                'editor_rows' => 'Rows:',
                'editor_cols' => 'Cols:',
                'editor_inserttable' => 'Insert a table',
                'editor_inserthr' => 'Insert a horizontal rule',
                'editor_code' => 'Code',
                'editor_width' => 'Width (optional):',
                'editor_height' => 'Height (optional):',
                'editor_insertimg' => 'Insert an image',
                'editor_email' => 'E-mail:',
                'editor_insertemail' => 'Insert an email',
                'editor_url' => 'URL:',
                'editor_insertlink' => 'Insert a link',
                'editor_unlink' => 'Unlink',
                'editor_more' => 'More',
                'editor_insertemoticon' => 'Insert an emoticon',
                'editor_videourl' => 'Video URL:',
                'editor_videotype' => 'Video Type:',
                'editor_insert' => 'Insert',
                'editor_insertyoutubevideo' => 'Insert a YouTube video',
                'editor_currentdate' => 'Insert current date',
                'editor_currenttime' => 'Insert current time',
                'editor_print' => 'Print',
                'editor_viewsource' => 'View source',
                'editor_description' => 'Description (optional):',
                'editor_enterimgurl' => 'Enter the image URL:',
                'editor_enteremail' => 'Enter the e-mail address:',
                'editor_enterdisplayedtext' => 'Enter the displayed text:',
                'editor_enterurl' => 'Enter URL:',
                'editor_enteryoutubeurl' => 'Enter the YouTube video URL or ID:',
                'editor_insertquote' => 'Insert a Quote',
                'editor_invalidyoutube' => 'Invalid YouTube video',
                'editor_dailymotion' => 'Dailymotion',
                'editor_metacafe' => 'MetaCafe',
                'editor_veoh' => 'Veoh',
                'editor_vimeo' => 'Vimeo',
                'editor_youtube' => 'Youtube',
                'editor_facebook' => 'Facebook',
                'editor_liveleak' => 'LiveLeak',
                'editor_insertvideo' => 'Insert a video',
                'editor_php' => 'PHP',
                'editor_maximize' => 'Maximize'
            ];
            $editor_language = "(function ($) {\n$.sceditor.locale[\"mybblang\"] = {\n";

            $editor_lang_strings = $this->bb->plugins->runHooks('mycode_add_codebuttons', $editor_lang_strings);

            $editor_languages_count = count($editor_lang_strings);
            $i = 0;
            foreach ($editor_lang_strings as $lang_string => $key) {
                $i++;
                $js_lang_string = str_replace("\"", "\\\"", $key);
                $string = str_replace("\"", "\\\"", $this->bb->lang->$lang_string);
                $editor_language .= "\t\"{$js_lang_string}\": \"{$string}\"";

                if ($i < $editor_languages_count) {
                    $editor_language .= ',';
                }

                $editor_language .= "\n";
            }

            $editor_language .= '}})(jQuery);';

            if (defined('IN_ADMINCP')) {
                $codeinsert = $this->build_codebuttons_editor($bind, $editor_language, $smilies);
            } else {
                // Smilies
                $emoticon = '';
                $emoticons_enabled = 'false';
                if ($smilies) {
                    if ($this->bb->settings['smilieinserter'] &&
                        $this->bb->settings['smilieinsertercols'] &&
                        $this->bb->settings['smilieinsertertot']
                    ) {
                        $emoticon = ',emoticon';
                    }
                    $emoticons_enabled = 'true';

                    if (empty($this->bb->smiliecache)) {
                        if (!isset($smilie_cache) || !is_array($smilie_cache)) {
                            $smilie_cache = $this->bb->cache->read('smilies');
                        }
                        foreach ($smilie_cache as $smilie) {
                            $smilie['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $smilie['image']);
                            $this->bb->smiliecache[$smilie['sid']] = $smilie;
                        }
                    }

                    unset($smilie);

                    if (is_array($this->bb->smiliecache)) {
                        reset($this->bb->smiliecache);

                        $dropdownsmilies = $moresmilies = $hiddensmilies = '';
                        $i = 0;

                        foreach ($this->bb->smiliecache as $smilie) {
                            $finds = explode("\n", $smilie['find']);
                            $finds_count = count($finds);

                            // Only show the first text to replace in the box
                            $smilie['find'] = $finds[0];

                            $find = str_replace(['\\', '"'], ['\\\\', '\"'], htmlspecialchars_uni($smilie['find']));
                            $image = htmlspecialchars_uni($this->themes->get_asset_url($smilie['image']));
                            $image = str_replace(['\\', '"'], ['\\\\', '\"'], $image);

                            if (!$this->bb->settings['smilieinserter'] ||
                                !$this->bb->settings['smilieinsertercols'] ||
                                !$this->bb->settings['smilieinsertertot'] ||
                                !$smilie['showclickable']
                            ) {
                                $hiddensmilies .= '"' . $find . '": "' . $image . '",';
                            } elseif ($i < $this->bb->settings['smilieinsertertot']) {
                                $dropdownsmilies .= '"' . $find . '": "' . $image . '",';
                                ++$i;
                            } else {
                                $moresmilies .= '"' . $find . '": "' . $image . '",';
                            }

                            for ($j = 1; $j < $finds_count; ++$j) {
                                $find = str_replace(['\\', '"'], ['\\\\', '\"'], htmlspecialchars_uni($finds[$j]));
                                $hiddensmilies .= '"' . $find . '": "' . $image . '",';
                            }
                        }
                        $data['dropdownsmilies'] = $dropdownsmilies;
                        $data['moresmilies'] = $moresmilies;
                        $data['hiddensmilies'] = $hiddensmilies;
                    }
                }
                $data['bind'] = $bind;
                $data['emoticon'] = $emoticon;
                $data['emoticons_enabled'] = $emoticons_enabled;

//        $basic1 = $basic2 = $align = $font = $size = $color = $removeformat =
//        $email = $link = $list = $code = $sourcemode = '';

                if ($this->bb->settings['allowbasicmycode'] == 1) {
                    //$basic1 = 'bold,italic,underline,strike|';
                    $data['basic1'] = 'bold,italic,underline,strike|';
                    //$basic2 = 'horizontalrule,';
                    $data['basic2'] = 'horizontalrule,';
                }

                if ($this->bb->settings['allowalignmycode'] == 1) {
                    //$align = 'left,center,right,justify|';
                    $data['align'] = 'left,center,right,justify|';
                }

                if ($this->bb->settings['allowfontmycode'] == 1) {
                    //$font = 'font,';
                    $data['font'] = 'font,';
                }

                if ($this->bb->settings['allowsizemycode'] == 1) {
                    //$size = 'size,';
                    $data['size'] = 'size,';
                }

                if ($this->bb->settings['allowcolormycode'] == 1) {
                    //$color = 'color,';
                    $data['color'] = 'color,';
                }

                if ($this->bb->settings['allowfontmycode'] == 1 ||
                    $this->bb->settings['allowsizemycode'] == 1 ||
                    $this->bb->settings['allowcolormycode'] == 1
                ) {
                    //$removeformat = 'removeformat|';
                    $data['removeformat'] = 'removeformat|';
                }

                if ($this->bb->settings['allowemailmycode'] == 1) {
                    //$email = 'email,';
                    $data['email'] = 'email,';
                }

                if ($this->bb->settings['allowlinkmycode'] == 1) {
                    //$link = 'link,unlink';
                    $data['link'] = 'link,unlink';
                }

                if ($this->bb->settings['allowlistmycode'] == 1) {
                    //$list = 'bulletlist,orderedlist|';
                    $data['list'] = 'bulletlist,orderedlist|';
                }

                if ($this->bb->settings['allowcodemycode'] == 1) {
                    //$code = 'code,php,';
                    $data['code'] = 'code,php,';
                }
                $data['rtl'] = $this->bb->lang->settings['rtl'];


                $this->view->offsetSet('edvar', $data);
                $this->view->offsetSet('editor_language', $editor_language);

                $codeinsert = true;
            }
        }

        return $codeinsert;
    }

    /**
     * Build the javascript clickable smilie inserter
     *
     * @return string The clickable smilies list
     */
    public function build_clickable_smilies()
    {
        $getmore = false;
        $smiliesArray = [];
        if ($this->bb->settings['smilieinserter'] != 0 &&
            $this->bb->settings['smilieinsertercols'] &&
            $this->bb->settings['smilieinsertertot']
        ) {
            if (!isset($this->bb->smiliecount)) {
                $smilie_cache = $this->bb->cache->read('smilies');
                $this->bb->smiliecount = count($smilie_cache);
            }

            if (empty($this->bb->smiliecache)) {
                if (!is_array($smilie_cache)) {
                    $smilie_cache = $this->bb->cache->read('smilies');
                }
                foreach ($smilie_cache as $smilie) {
                    $smilie['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $smilie['image']);
                    $this->bb->smiliecache[$smilie['sid']] = $smilie;
                }
            }

            unset($smilie);

            if (is_array($this->bb->smiliecache)) {
                reset($this->bb->smiliecache);

                if ($this->bb->settings['smilieinsertertot'] >= $this->bb->smiliecount) {
                    $this->bb->settings['smilieinsertertot'] = $this->bb->smiliecount;
                } elseif ($this->bb->settings['smilieinsertertot'] < $this->bb->smiliecount) {
                    $this->bb->smiliecount = $this->bb->settings['smilieinsertertot'];
                    $getmore = true;
                }

                $i = 0;

                foreach ($this->bb->smiliecache as $smilie) {
                    if ($i < $this->bb->settings['smilieinsertertot'] && $smilie['showclickable'] != 0) {
                        $smilie['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $smilie['image']);
                        $smilie['image'] = htmlspecialchars_uni($this->themes->get_asset_url($smilie['image']));
                        $smilie['name'] = htmlspecialchars_uni($smilie['name']);

                        // Only show the first text to replace in the box
                        $temp = explode("\n", $smilie['find']); // assign to temporary variable for php 5.3 compatibility
                        $smilie['find'] = $temp[0];

                        //$find = str_replace(array('\\', "'"), array('\\\\', "\'"), htmlspecialchars_uni($smilie['find']));
                        $smilie['find'] = str_replace(['\\', "'"], ['\\\\', "\'"], htmlspecialchars_uni($smilie['find']));

                        $smiliesArray[] = $smilie;
                        ++$i;
                    }
                }
                $clickablesmilies = true;
            } else {
                $clickablesmilies = false;
            }
        } else {
            $clickablesmilies = false;
        }
        $this->view->offsetSet('getmore', $getmore);
        $this->view->offsetSet('smiliesArray', $smiliesArray);

        return $clickablesmilies;
    }

    /**
     * Build a clickable MyCode editor for the Admin CP.
     *
     * @param string $bind The ID of the textarea to bind the editor to.
     * @param string $editor_language The language string for the editor.
     * @param bool $smilies Whether or not smilies should be included
     * @return string The build MyCode editor Javascript.
     */
    private function build_codebuttons_editor($bind, $editor_language, $smilies)
    {
        // Smilies
        $emoticon = '';
        $emoticons_enabled = 'false';
        if ($smilies) {
            if ($this->bb->settings['smilieinserter'] &&
                $this->bb->settings['smilieinsertercols'] &&
                $this->bb->settings['smilieinsertertot']
            ) {
                $emoticon = ',emoticon';
            }
            $emoticons_enabled = 'true';

            if (!isset($this->bb->smiliecount)) {//from editor
                $this->bb->smilie_cache = $this->bb->cache->read('smilies');
                $this->bb->smiliecount = count($this->bb->smilie_cache);
            }

            if (!isset($this->bb->smiliecache)) {
                if (!is_array($this->bb->smilie_cache)) {
                    $this->bb->smilie_cache = $this->bb->cache->read('smilies');
                }
                foreach ($this->bb->smilie_cache as $smilie) {
                    $smilie['image'] = str_replace('{theme}', 'images', $smilie['image']);
                    $this->bb->smiliecache[$smilie['sid']] = $smilie;
                }
            }

            unset($smilie);

            if (is_array($this->bb->smiliecache)) {
                reset($this->bb->smiliecache);
                $dropdownsmilies = $moresmilies = $hiddensmilies = '';
                $i = 0;
                foreach ($this->bb->smiliecache as $smilie) {
                    $finds = explode("\n", $smilie['find']);
                    $finds_count = count($finds);

                    // Only show the first text to replace in the box
                    $find = str_replace(['\\', '"'], ['\\\\', '\"'], htmlspecialchars_uni($finds[0]));
                    $image = str_replace(['\\', '"'], ['\\\\', '\"'], htmlspecialchars_uni($smilie['image']));
                    if (substr($image, 0, 4) != "http") {
                        $image = $this->bb->asset_url . '/' . $image;
                    }

                    if (!$this->bb->settings['smilieinserter'] ||
                        !$this->bb->settings['smilieinsertercols'] ||
                        !$this->bb->settings['smilieinsertertot'] ||
                        !$smilie['showclickable']
                    ) {
                        $hiddensmilies .= '"' . $find . '": "' . $image . '",';
                    } elseif ($i < $this->bb->settings['smilieinsertertot']) {
                        $dropdownsmilies .= '"' . $find . '": "' . $image . '",';
                        ++$i;
                    } else {
                        $moresmilies .= '"' . $find . '": "' . $image . '",';
                    }

                    for ($j = 1; $j < $finds_count; ++$j) {
                        $find = str_replace(['\\', '"'], ['\\\\', '\"'], htmlspecialchars_uni($finds[$j]));
                        $hiddensmilies .= '"' . $find . '": "' . $image . '",';
                    }
                }
            }
        }

        $basic1 = $basic2 = $align = $font = $size = $color = $removeformat = $email = $link = $list = $code = $sourcemode = '';

        if ($this->bb->settings['allowbasicmycode'] == 1) {
            $basic1 = 'bold,italic,underline,strike|';
            $basic2 = 'horizontalrule,';
        }

        if ($this->bb->settings['allowalignmycode'] == 1) {
            $align = 'left,center,right,justify|';
        }

        if ($this->bb->settings['allowfontmycode'] == 1) {
            $font = 'font,';
        }

        if ($this->bb->settings['allowsizemycode'] == 1) {
            $size = 'size,';
        }

        if ($this->bb->settings['allowcolormycode'] == 1) {
            $color = 'color,';
        }

        if ($this->bb->settings['allowfontmycode'] == 1 ||
            $this->bb->settings['allowsizemycode'] == 1 ||
            $this->bb->settings['allowcolormycode'] == 1
        ) {
            $removeformat = 'removeformat|';
        }

        if ($this->bb->settings['allowemailmycode'] == 1) {
            $email = 'email,';
        }

        if ($this->bb->settings['allowlinkmycode'] == 1) {
            $link = 'link,unlink';
        }

        if ($this->bb->settings['allowlistmycode'] == 1) {
            $list = 'bulletlist,orderedlist|';
        }

        if ($this->bb->settings['allowcodemycode'] == 1) {
            $code = 'code,php,';
        }

        if ($this->user->sourceeditor == 1) {
            $sourcemode = 'BBEditor.sourceMode(true);';
        }

        return <<<EOF
<link rel="stylesheet" href="{$this->bb->asset_url}/jscripts/sceditor/editor_themes/mybb.css" type="text/css" media="all" />
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/sceditor/jquery.sceditor.bbcode.min.js?ver=1805"></script>
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/bbcodes_sceditor.js?ver=1804"></script>
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/sceditor/editor_plugins/undo.js?ver=1805"></script>

<script type="text/javascript">
var partialmode = {$this->bb->settings['partialmode']},
opt_editor = {
	plugins: "bbcode,undo",
	style: "{$this->bb->asset_url}/jscripts/sceditor/textarea_styles/jquery.sceditor.mybb.css",
	rtl: {$this->bb->lang->settings['rtl']},
	locale: "mybblang",
	enablePasteFiltering: true,
	autoUpdate: true,
	emoticonsEnabled: {$emoticons_enabled},
	emoticons: {
		// Emoticons to be included in the dropdown
		dropdown: {
			{$dropdownsmilies}
		},
		// Emoticons to be included in the more section
		more: {
			{$moresmilies}
		},
		// Emoticons that are not shown in the dropdown but will still be converted. Can be used for things like aliases
		hidden: {
			{$hiddensmilies}
		}
	},
	emoticonsCompat: true,
	toolbar: "{$basic1}{$align}{$font}{$size}{$color}{$removeformat}{$basic2}image,{$email}{$link}|video{$emoticon}|{$list}{$code}quote|maximize,source",
};
{$editor_language}
$(function() {
	$("#{$bind}").sceditor(opt_editor);

	BBEditor = $("#{$bind}").sceditor("instance");
	{$sourcemode}
});
</script>
EOF;
    }
}
