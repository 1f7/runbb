<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Forum extends AbstractController
{
    public $unread_forums = 0;

    /**
     * Get inactivate forums.
     *
     * @return string The comma separated values of the inactivate forum.
     */
    public function get_inactive_forums()
    {
        if (!$this->bb->forum_cache) {
            $this->cache_forums();
        }

        $inactive = [];

        foreach ($this->bb->forum_cache as $fid => $forum) {
            if ($forum['active'] == 0) {
                $inactive[] = $fid;
                foreach ($this->bb->forum_cache as $fid1 => $forum1) {
                    if (my_strpos(',' . $forum1['parentlist'] . ',', ',' . $fid . ',') !== false && !in_array($fid1, $inactive)) {
                        $inactive[] = $fid1;
                    }
                }
            }
        }
//    $inactiveforums = implode(',', $inactive);
//    return $inactiveforums;
        return $inactive;
    }

    /**
     * Get the forum of a specific forum id.
     *
     * @param int $fid The forum id of the forum.
     * @param int $active_override (Optional) If set to 1, will override the active forum status
     * @return array|bool The database row of a forum. False on failure
     */
    public function get_forum($fid, $active_override = 0)
    {
        static $forum_cache;

        if (!isset($forum_cache) || is_array($forum_cache)) {
            $forum_cache = $this->cache->read('forums');
        }

        if (empty($forum_cache[$fid])) {
            return false;
        }

        if ($active_override != 1) {
            $parents = explode(',', $forum_cache[$fid]['parentlist']);
            if (is_array($parents)) {
                foreach ($parents as $parent) {
                    if ($forum_cache[$parent]['active'] == 0) {
                        return false;
                    }
                }
            }
        }

        return $forum_cache[$fid];
    }

    /**
     * Build the forum link.
     *
     * @param int $fid The forum id of the forum.
     * @param int $page (Optional) The page number of the forum.
     * @return string The url to the forum.
     */
    public function get_forum_link($fid, $page = 0)
    {
        if ($page > 0) {
            $link = str_replace('{fid}', $fid, FORUM_URL_PAGED);
            $link = str_replace('{page}', $page, $link);
            return htmlspecialchars_uni($link);
        } else {
            $link = str_replace('{fid}', $fid, FORUM_URL);
            return htmlspecialchars_uni($link);
        }
    }

    /**
     * Get a list of the unviewable forums for the current user
     *
     * @param boolean $only_readable_threads Set to true to only fetch those forums for which users can actually read a thread in.
     * @return string Comma separated values list of the forum IDs which the user cannot view
     */
    public function get_unviewable_forums($only_readable_threads = false)
    {
        if (empty($this->bb->forum_cache)) {
            $this->cache_forums();
        }

        if (empty($this->bb->permissioncache)) {
            $this->bb->permissioncache = $this->forum_permissions();
        }

        $password_forums = $unviewable = [];
        foreach ($this->bb->forum_cache as $fid => $forum) {
            if ($this->bb->permissioncache[$forum['fid']]) {
                $perms = $this->bb->permissioncache[$forum['fid']];
            } else {
                $perms = $this->bb->usergroup;
            }

            $pwverified = 1;

            if ($forum['password'] != '') {
                if ($this->bb->cookies['forumpass'][$forum['fid']] !== md5($this->user->uid . $forum['password'])) {
                    $pwverified = 0;
                }

                $password_forums[$forum['fid']] = $forum['password'];
            } else {
                // Check parents for passwords
                $parents = explode(',', $forum['parentlist']);
                foreach ($parents as $parent) {
                    if (isset($password_forums[$parent]) &&
                        $this->bb->cookies['forumpass'][$parent] !== md5($this->user->uid . $password_forums[$parent])
                    ) {
                        $pwverified = 0;
                    }
                }
            }

            if ($perms['canview'] == 0 || $pwverified == 0 || ($only_readable_threads == true && $perms['canviewthreads'] == 0)) {
                $unviewable[] = $forum['fid'];
            }
        }

//        $unviewableforums = implode(',', $unviewable);
//        return $unviewableforums;
        return $unviewable;
    }

    /**
     * Update the last post information for a specific forum
     *
     * @param int $fid The forum ID
     */
    public function update_forum_lastpost($fid)
    {
        // Fetch the last post for this forum
        $query = $this->db->query('
		SELECT tid, lastpost, lastposter, lastposteruid, subject
		FROM ' . TABLE_PREFIX . "threads
		WHERE fid='{$fid}' AND visible='1' AND closed NOT LIKE 'moved|%'
		ORDER BY lastpost DESC
		LIMIT 0, 1
	");
        $lastpost = $this->db->fetch_array($query);

        $updated_forum = [
            'lastpost' => (int)$lastpost['lastpost'],
            'lastposter' => $this->db->escape_string($lastpost['lastposter']),
            'lastposteruid' => (int)$lastpost['lastposteruid'],
            'lastposttid' => (int)$lastpost['tid'],
            'lastpostsubject' => $this->db->escape_string($lastpost['subject'])
        ];

        $this->db->update_query('forums', $updated_forum, "fid='{$fid}'");
    }

    /**
     * Updates the forum counters with a specific value (or addition/subtraction of the previous value)
     *
     * @param int $fid The forum ID
     * @param array $changes Array of items being updated (threads, posts, unapprovedthreads, unapprovedposts, deletedposts, deletedthreads) and their value (ex, 1, +1, -1)
     */
    public function update_forum_counters($fid, $changes = [])
    {
        $update_query = [];

        $counters = ['threads', 'unapprovedthreads', 'posts', 'unapprovedposts', 'deletedposts', 'deletedthreads'];

        // Fetch above counters for this forum
        $query = $this->db->simple_select('forums', implode(',', $counters), "fid='{$fid}'");
        $forum = $this->db->fetch_array($query);

        foreach ($counters as $counter) {
            if (array_key_exists($counter, $changes)) {
                if (substr($changes[$counter], 0, 2) == '+-') {
                    $changes[$counter] = substr($changes[$counter], 1);
                }
                // Adding or subtracting from previous value?
                if (substr($changes[$counter], 0, 1) == '+' || substr($changes[$counter], 0, 1) == '-') {
                    if ((int)$changes[$counter] != 0) {
                        $update_query[$counter] = $forum[$counter] + $changes[$counter];
                    }
                } else {
                    $update_query[$counter] = $changes[$counter];
                }

                // Less than 0? That's bad
                if (isset($update_query[$counter]) && $update_query[$counter] < 0) {
                    $update_query[$counter] = 0;
                }
            }
        }

        // Only update if we're actually doing something
        if (count($update_query) > 0) {
            $this->db->update_query('forums', $update_query, "fid='" . (int)$fid . "'");
        }

        // Guess we should update the statistics too?
        $new_stats = [];
        if (array_key_exists('threads', $update_query)) {
            $threads_diff = $update_query['threads'] - $forum['threads'];
            if ($threads_diff > -1) {
                $new_stats['numthreads'] = "+{$threads_diff}";
            } else {
                $new_stats['numthreads'] = "{$threads_diff}";
            }
        }

        if (array_key_exists('unapprovedthreads', $update_query)) {
            $unapprovedthreads_diff = $update_query['unapprovedthreads'] - $forum['unapprovedthreads'];
            if ($unapprovedthreads_diff > -1) {
                $new_stats['numunapprovedthreads'] = "+{$unapprovedthreads_diff}";
            } else {
                $new_stats['numunapprovedthreads'] = "{$unapprovedthreads_diff}";
            }
        }

        if (array_key_exists('posts', $update_query)) {
            $posts_diff = $update_query['posts'] - $forum['posts'];
            if ($posts_diff > -1) {
                $new_stats['numposts'] = "+{$posts_diff}";
            } else {
                $new_stats['numposts'] = "{$posts_diff}";
            }
        }

        if (array_key_exists('unapprovedposts', $update_query)) {
            $unapprovedposts_diff = $update_query['unapprovedposts'] - $forum['unapprovedposts'];
            if ($unapprovedposts_diff > -1) {
                $new_stats['numunapprovedposts'] = "+{$unapprovedposts_diff}";
            } else {
                $new_stats['numunapprovedposts'] = "{$unapprovedposts_diff}";
            }
        }

        if (array_key_exists('deletedposts', $update_query)) {
            $deletedposts_diff = $update_query['deletedposts'] - $forum['deletedposts'];
            if ($deletedposts_diff > -1) {
                $new_stats['numdeletedposts'] = "+{$deletedposts_diff}";
            } else {
                $new_stats['numdeletedposts'] = "{$deletedposts_diff}";
            }
        }

        if (array_key_exists('deletedthreads', $update_query)) {
            $deletedthreads_diff = $update_query['deletedthreads'] - $forum['deletedthreads'];
            if ($deletedthreads_diff > -1) {
                $new_stats['numdeletedthreads'] = "+{$deletedthreads_diff}";
            } else {
                $new_stats['numdeletedthreads'] = "{$deletedthreads_diff}";
            }
        }

        if (!empty($new_stats)) {
            $this->cache->update_stats($new_stats);
        }
    }

    /**
     * Check the password given on a certain forum for validity
     *
     * @param int $fid The forum ID
     * @param int $pid The Parent ID
     * @param bool $return
     * @return bool
     */
    public function check_forum_password($fid, $pid = 0, $return = false)
    {
        $showform = true;

        if (!isset($this->bb->forum_cache)) {
            $this->cache_forums();
            if (!$this->bb->forum_cache) {
                return false;
            }
        }

        // Loop through each of parent forums to ensure we have a password for them too
        if (isset($this->bb->forum_cache[$fid]['parentlist'])) {
            $parents = explode(',', $this->bb->forum_cache[$fid]['parentlist']);
            rsort($parents);
        }
        if (!empty($parents)) {
            foreach ($parents as $parent_id) {
                if ($parent_id == $fid || $parent_id == $pid) {
                    continue;
                }

                if ($this->bb->forum_cache[$parent_id]['password'] != '') {
                    $this->check_forum_password($parent_id, $fid);
                }
            }
        }

        if (!empty($this->bb->forum_cache[$fid]['password'])) {
            $password = $this->bb->forum_cache[$fid]['password'];
            if (isset($this->bb->input['pwverify']) && $pid == 0) {
                if ($password === $this->bb->getInput('pwverify', '')) {
                    $this->bb->my_setcookie("forumpass[$fid]", md5($this->user->uid .
                        $this->bb->getInput('pwverify', '')), null, true);
                    $showform = false;
                } else {
                    eval("\$pwnote = \"" . $this->bb->templates->get('forumdisplay_password_wrongpass') . "\";");
                    $showform = true;
                }
            } else {
                if (!$this->bb->cookies['forumpass'][$fid] ||
                    ($this->bb->cookies['forumpass'][$fid] &&
                        md5($this->user->uid . $password) !== $this->bb->cookies['forumpass'][$fid])
                ) {
                    $showform = true;
                } else {
                    $showform = false;
                }
            }
        } else {
            $showform = false;
        }

        if ($return) {
            return $showform;
        }

        if ($showform) {
            if ($pid) {
                return $response->withRedirect($this->bb->settings['bburl'] . '/' . $this->get_forum_link($fid));
            } else {
                $_SERVER['REQUEST_URI'] = htmlspecialchars_uni($_SERVER['REQUEST_URI']);
                eval("\$pwform = \"" . $this->bb->templates->get('forumdisplay_password') . "\";");
                output_page($pwform);
            }
            exit;
        }
    }

    /**
     * Fetches the permissions for a specific forum/group applying the inheritance scheme.
     * Called by forum_permissions()
     *
     * @param int $fid The forum ID
     * @param string $gid A comma separated list of usergroups
     * @param array $groupperms Group permissions
     * @return array Permissions for this forum
     */
    private function fetch_forum_permissions($fid, $gid, $groupperms)
    {
        $groups = explode(',', $gid);

        if (empty($this->bb->fpermcache[$fid])) { // This forum has no custom or inherited permissions so lets just return the group permissions
            if (!isset($groupperms['canonlyviewownthreads'])) {
                $groupperms['canonlyviewownthreads'] = 0;//FIXME
            }
            if (!isset($groupperms['canonlyreplyownthreads'])) {
                $groupperms['canonlyreplyownthreads'] = 0;
            }

            return $groupperms;
        }

        $current_permissions = [];
        $only_view_own_threads = 1;
        $only_reply_own_threads = 1;

        foreach ($groups as $gid) {
            if (!empty($this->bb->groupscache[$gid])) {
                $level_permissions = isset($this->bb->fpermcache[$fid][$gid]) ? $this->bb->fpermcache[$fid][$gid] : null;

                // If our permissions arn't inherited we need to figure them out
                if (empty($this->bb->fpermcache[$fid][$gid])) {
                    $parents = explode(',', $this->bb->forum_cache[$fid]['parentlist']);
                    rsort($parents);
                    if (!empty($parents)) {
                        foreach ($parents as $parent_id) {
                            if (!empty($this->bb->fpermcache[$parent_id][$gid])) {
                                $level_permissions = $this->bb->fpermcache[$parent_id][$gid];
                                break;
                            }
                        }
                    }
                }

                // If we STILL don't have forum permissions we use the usergroup itself
                if (empty($level_permissions)) {
                    $level_permissions = $this->bb->groupscache[$gid];
                }

                foreach ($level_permissions as $permission => $access) {
                    if (empty($current_permissions[$permission]) ||
                        $access >= $current_permissions[$permission] ||
                        ($access == 'yes' && $current_permissions[$permission] == 'no')
                    ) {
                        $current_permissions[$permission] = $access;
                    }
                }

                if ($level_permissions['canview'] && empty($level_permissions['canonlyviewownthreads'])) {
                    $only_view_own_threads = 0;
                }

                if ($level_permissions['canpostreplys'] && empty($level_permissions['canonlyreplyownthreads'])) {
                    $only_reply_own_threads = 0;
                }
            }
        }

        // Figure out if we can view more than our own threads
        if ($only_view_own_threads == 0) {
            $current_permissions['canonlyviewownthreads'] = 0;
        } else {
            $current_permissions['canonlyviewownthreads'] = $only_view_own_threads;
        }
        // Figure out if we can reply more than our own threads
        if ($only_reply_own_threads == 0) {
            $current_permissions['canonlyreplyownthreads'] = 0;
        } else {
            $current_permissions['canonlyreplyownthreads'] = $only_reply_own_threads;
        }

        if (count($current_permissions) == 0) {
            $current_permissions = $groupperms;
        }
        return $current_permissions;
    }

    /**
     * Build the forum permissions for a specific forum, user or group
     *
     * @param int $fid The forum ID to build permissions for (0 builds for all forums)
     * @param int $uid The user to build the permissions for (0 will select the uid automatically)
     * @param int $gid The group of the user to build permissions for (0 will fetch it)
     * @return array Forum permissions for the specific forum or forums
     */
    public function forum_permissions($fid = 0, $uid = 0, $gid = 0)
    {
        static $cached_forum_permissions_permissions;//, $cached_forum_permissions;

        if ($uid == 0) {
            $uid = $this->user->uid;
        }

        if (!$gid || $gid == 0) { // If no group, we need to fetch it
            if ($uid != 0 && $uid != $this->user->uid) {
                $user = $this->user->get_user($uid);

                $gid = $user->usergroup . ',' . $user->additionalgroups;
                $groupperms = $this->bb->group->usergroup_permissions($gid);
            } else {
                $gid = $this->user->usergroup;

                if (isset($this->user->additionalgroups)) {
                    $gid .= ',' . $this->user->additionalgroups;
                }

                $groupperms = $this->bb->usergroup;
            }
        }

        if (empty($this->bb->forum_cache)) {
            $this->cache_forums();
            if (!$this->bb->forum_cache) {
                return false;
            }
        }

        if (empty($this->bb->fpermcache)) {
            $this->bb->fpermcache = $this->cache->read('forumpermissions');
        }

        if ($fid) { // Fetch the permissions for a single forum
            if (empty($cached_forum_permissions_permissions[$gid][$fid])) {
                $cached_forum_permissions_permissions[$gid][$fid] = $this->fetch_forum_permissions($fid, $gid, $groupperms);
            }
            return $cached_forum_permissions_permissions[$gid][$fid];
        } else {
            if (empty($cached_forum_permissions[$gid])) {
                foreach ($this->bb->forum_cache as $forum) {
                    $cached_forum_permissions[$gid][$forum['fid']] = $this->fetch_forum_permissions($forum['fid'], $gid, $groupperms);
                }
            }
            return $cached_forum_permissions[$gid];
        }
    }

    /**
     * Build a parent list of a specific forum, suitable for querying
     *
     * @param int $fid The forum ID
     * @param string $column The column name to add to the query
     * @param string $joiner The joiner for each forum for querying (OR | AND | etc)
     * @param string $parentlist The parent list of the forum - if you have it
     * @return string The query string generated
     */
    public function build_parent_list($fid, $column = 'fid', $joiner = 'OR', $parentlist = '')
    {
        if (!$parentlist) {
            $parentlist = $this->get_parent_list($fid);
        }

        $parentsexploded = explode(',', $parentlist);
        $builtlist = '(';
        $sep = '';

        foreach ($parentsexploded as $key => $val) {
            $builtlist .= "$sep$column='$val'";
            $sep = " $joiner ";
        }

        $builtlist .= ')';

        return $builtlist;
    }

    /**
     * Return a parent list for the specified forum.
     *
     * @param int $fid The forum id to get the parent list for.
     * @return string The comma-separated parent list.
     */
    public function get_parent_list($fid)
    {
        static $forumarraycache;

        if ($forumarraycache[$fid]) {
            return $forumarraycache[$fid]['parentlist'];
        } elseif ($this->bb->forum_cache[$fid]) {
            return $this->bb->forum_cache[$fid]['parentlist'];
        } else {
            $this->cache_forums();
            return $this->bb->forum_cache[$fid]['parentlist'];
        }
    }

    /**
     * Generate an array of all child and descendant forums for a specific forum.
     *
     * @param int $fid The forum ID
     * @return Array of descendants
     */
    public function get_child_list($fid)
    {
        static $forums_by_parent;

        $forums = [];
        if (!is_array($forums_by_parent)) {
            $this->cache_forums();
            foreach ($this->bb->forum_cache as $forum) {
                if ($forum['active'] != 0) {
                    $forums_by_parent[$forum['pid']][$forum['fid']] = $forum;
                }
            }
        }
        if (!empty($forums_by_parent[$fid])) {
            return $forums;
        }

        foreach ($forums_by_parent[$fid] as $forum) {
            $forums[] = $forum['fid'];
            $children = $this->get_child_list($forum['fid']);
            if (is_array($children)) {
                $forums = array_merge($forums, $children);
            }
        }
        return $forums;
    }

    /**
     * Load the forum cache in to memory
     *
     * @param boolean $force True to force a reload of the cache
     * @return array The forum cache
     */
    public function cache_forums($force = false)
    {
        if ($force == true) {
            $this->bb->forum_cache = $this->cache->read('forums', 1);
        }

        if (empty($this->bb->forum_cache)) {
            $this->bb->forum_cache = $this->cache->read('forums');

            if (!$this->bb->forum_cache) {
                $this->cache->update_forums();
                $this->bb->forum_cache = $this->cache->read('forums', 1);
            }
        }
    }

    /**
     * Build a list of forum bits.
     *
     * @param int $pid The parent forum to fetch the child forums for (0 assumes all)
     * @param int $depth The depth to return forums with.
     * @return array Array of information regarding the child forums of this parent forum
     */
    public function build_forumbits($pid = 0, $depth = 1)
    {
        static $private_forums, $forum_cats, $forum_forums;

        // If no forums exist with this parent, do nothing
        if (empty($this->bb->fcache[$pid])) {// || !is_array($this->bb->fcache[$pid]))
            return;
        }

        $parent_counters = [
            'threads' => 0,
            'posts' => 0,
            'unapprovedposts' => 0,
            'unapprovedthreads' => 0,
            'viewers' => 0
        ];
        $forum_list = $comma = '';
        $donecount = 0;

        // Foreach of the forums in this parent
        foreach ($this->bb->fcache[$pid] as $parent) {
            foreach ($parent as $forum) {
                $subforums = $sub_forums = '';
                $lastpost_data = [
                    'lastpost' => 0
                ];
                $forum_viewers_text = $forum_viewers_text_plain = '';
                // Get the permissions for this forum
                $permissions = isset($this->bb->forumpermissions[$forum['fid']]) ? $this->bb->forumpermissions[$forum['fid']] : null;
                // If this user doesnt have permission to view this forum and we're hiding private forums, skip this forum
                if ($permissions['canview'] != 1 && $this->bb->settings['hideprivateforums'] == 1) {
                    continue;
                }
                $forum = $this->bb->plugins->runHooks('build_forumbits_forum', $forum);
                // Build the link to this forum
                $forum_url = $this->get_forum_link($forum['fid']);

                // This forum has a password, and the user isn't authenticated with it - hide post information
                $hideinfo = $hidecounters = $hidelastpostinfo = false;
                $showlockicon = 0;
                if (isset($permissions['canviewthreads']) && $permissions['canviewthreads'] != 1) {
                    $hideinfo = true;
                }

                if (isset($permissions['canonlyviewownthreads']) && $permissions['canonlyviewownthreads'] == 1) {
                    $hidecounters = true;

                    // If we only see our own threads, find out if there's a new post in one of them so the lightbulb shows
                    if (!is_array($private_forums)) {
                        $private_forums = $fids = [];
                        foreach ($this->bb->fcache as $fcache_p) {
                            foreach ($fcache_p as $parent_p) {
                                foreach ($parent_p as $forum_p) {
                                    if ($this->bb->forumpermissions[$forum_p['fid']]['canonlyviewownthreads']) {
                                        $fids[] = $forum_p['fid'];
                                    }
                                }
                            }
                        }

                        if (!empty($fids)) {
                            $fids = implode(',', $fids);
                            $query = $this->db->simple_select(
                                'threads',
                                'tid, fid, subject, lastpost, lastposter, lastposteruid',
                                "uid = '{$this->user->uid}' AND fid IN ({$fids}) AND visible != '-2'",
                                ['order_by' => 'lastpost', 'order_dir' => 'desc']
                            );

                            while ($thread = $this->db->fetch_array($query)) {
                                if (!$private_forums[$thread['fid']]) {
                                    $private_forums[$thread['fid']] = $thread;
                                }
                            }
                        }
                    }

                    if ($private_forums[$forum['fid']]['lastpost']) {
                        $forum['lastpost'] = $private_forums[$forum['fid']]['lastpost'];

                        $lastpost_data = [
                            'lastpost' => $private_forums[$forum['fid']]['lastpost'],
                            'lastpostsubject' => $private_forums[$forum['fid']]['subject'],
                            'lastposter' => $private_forums[$forum['fid']]['lastposter'],
                            'lastposttid' => $private_forums[$forum['fid']]['tid'],
                            'lastposteruid' => $private_forums[$forum['fid']]['lastposteruid']
                        ];
                    }
                } else {
                    $lastpost_data = [
                        'lastpost' => $forum['lastpost'],
                        'lastpostsubject' => $forum['lastpostsubject'],
                        'lastposter' => $forum['lastposter'],
                        'lastposttid' => $forum['lastposttid'],
                        'lastposteruid' => $forum['lastposteruid']
                    ];
                }

                if ($forum['password'] != '' && $this->bb->cookies['forumpass'][$forum['fid']] !== md5($this->user->uid . $forum['password'])) {
                    $hideinfo = true;
                    $showlockicon = 1;
                }

                // Fetch subforums of this forum
                if (isset($this->bb->fcache[$forum['fid']])) {
                    $forum_info = $this->build_forumbits($forum['fid'], $depth + 1);

                    // Increment forum counters with counters from child forums
                    $forum['threads'] += $forum_info['counters']['threads'];
                    $forum['posts'] += $forum_info['counters']['posts'];
                    $forum['unapprovedthreads'] += $forum_info['counters']['unapprovedthreads'];
                    $forum['unapprovedposts'] += $forum_info['counters']['unapprovedposts'];

//                    if (!empty($forum_info['counters']['viewing'])) {
//                        $forum['viewers'] += $forum_info['counters']['viewing'];
//                    }
                    if (!empty($forum_info['counters']['viewers'])) {
                        $forum['viewers'] += $forum_info['counters']['viewers'];
                    }

                    // If the child forums' lastpost is greater than the one for this forum, set it as the child forums greatest.
                    if ($forum_info['lastpost']['lastpost'] > $lastpost_data['lastpost']) {
                        $lastpost_data = $forum_info['lastpost'];

                        /*
                        // If our subforum is unread, then so must be our parents. Force our parents to unread as well
                        if(strstr($forum_info['lightbulb']['folder'], 'on') !== false)
                        {
                          $forum['lastread'] = 0;
                        }
                        // Otherwise, if we  have an explicit record in the db, we must make sure that it is explicitly set
                        else
                        {
                          $lastpost_data['lastpost'] = $forum['lastpost'];
                        }*/
                    }
                    $forum_forums = $forum_info['forum_forums'];
                    $sub_forums = $forum_info['forum_list'];
                }

                // If we are hiding information (lastpost) because we aren't authenticated
                // against the password for this forum,remove them
                if ($hidelastpostinfo == true) {
                    $lastpost_data = [
                        'lastpost' => 0,
                        'lastposter' => ''
                    ];
                }

                // If the current forums lastpost is greater than other child forums of the current parent
                // and forum info isn't hidden, overwrite it
                if ((!isset($parent_lastpost) || $lastpost_data['lastpost'] > $parent_lastpost['lastpost']) && $hideinfo != true) {
                    $parent_lastpost = $lastpost_data;
                }

                if (isset($this->bb->forum_viewers[$forum['fid']]) && $this->bb->forum_viewers[$forum['fid']] > 0) {
                    $forum['viewers'] = $this->bb->forum_viewers[$forum['fid']];
                }

                // Increment the counters for the parent forum (returned later)
                if ($hideinfo != true && $hidecounters != true) {
                    $parent_counters['threads'] += $forum['threads'];
                    $parent_counters['posts'] += $forum['posts'];
                    $parent_counters['unapprovedposts'] += $forum['unapprovedposts'];
                    $parent_counters['unapprovedthreads'] += $forum['unapprovedthreads'];

                    if (!empty($forum['viewers'])) {
                        $parent_counters['viewers'] += $forum['viewers'];
                    }
                }

                // Done with our math, lets talk about displaying - only display forums which are under a certain depth
                if ($depth > $this->bb->showdepth) {
                    continue;
                }

                // Get the lightbulb status indicator for this forum based on the lastpost
                $lightbulb = $this->get_forum_lightbulb($forum, $lastpost_data, $showlockicon);

                // Fetch the number of unapproved threads and posts for this forum
                $unapproved = $this->get_forum_unapproved($forum);

                if ($hideinfo == true) {
                    unset($unapproved);
                }

                // Sanitize name and description of forum.
                $forum['name'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['name']); // Fix & but allow unicode
                $forum['description'] = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $forum['description']); // Fix & but allow unicode
                $forum['name'] = preg_replace("#&([^\#])(?![a-z1-4]{1,10};)#i", "&#038;$1", $forum['name']);
                $forum['description'] = preg_replace("#&([^\#])(?![a-z1-4]{1,10};)#i", "&#038;$1", $forum['description']);

                // If this is a forum and we've got subforums of it, load the subforums list template
                if ($depth == 2 && $sub_forums) {
                    $subforums = '<br />' . $this->bb->lang->subforums . ' ' . $sub_forums;
                    //ev al("\$subforums = \"".$this->bb->templates->get("forumbit_subforums")."\";");
                } // A depth of three indicates a comma separated list of forums within a forum
                elseif ($depth == 3) {
                    if ($donecount < $this->bb->settings['subforumsindex']) {
                        $statusicon = '';

                        // Showing mini status icons for this forum
                        if ($this->bb->settings['subforumsstatusicons'] == 1) {
                            $lightbulb['folder'] = 'mini' . $lightbulb['folder'];
                            $statusicon = '<div title="' . $lightbulb['altonoff'] . '" class="subforumicon subforum_' . $lightbulb['folder'] . ' ajax_mark_read" id="mark_read_' . $forum['fid'] . '"></div>';
                            //ev al("\$statusicon = \"".$this->bb->templates->get('forumbit_depth3_statusicon', 1, 0)."\";");
                        }
                        // Fetch the template and append it to the list
                        $forum_list .= $comma . $statusicon . '<a href="' . $forum_url . '" title="' . $forum_viewers_text_plain . '">' . $forum['name'] . '</a>';
                        //ev al("\$forum_list .= \"".$this->bb->templates->get('forumbit_depth3', 1, 0)."\";");
                        $comma = $this->bb->lang->comma;
                    }

                    // Have we reached our max visible subforums? put a nice message and break out of the loop
                    ++$donecount;
                    if ($donecount == $this->bb->settings['subforumsindex']) {
                        if (subforums_count($this->bb->fcache[$pid]) > $donecount) {
                            $forum_list .= $comma . $this->bb->lang->sprintf(
                                $this->bb->lang->more_subforums,
                                (subforums_count($this->bb->fcache[$pid]) - $donecount)
                            );
                        }
                    }
                    continue;
                }

                // Forum is a category, set template type
                if ($forum['type'] == 'c') {
                    $forumcat = '_cat';
                } // Forum is a standard forum, set template type
                else {
                    $forumcat = '_forum';
                }

                if ($forum['linkto'] == '') {
                    // No posts have been made in this forum - show never text
                    if (($lastpost_data['lastpost'] == 0 || $lastpost_data['lastposter'] == '') && $hideinfo != true) {
                        $lastpost = '<div style="text-align: center;">' . $this->bb->lang->lastpost_never . '</div>';
                    } elseif ($hideinfo != true) {
                        // Format lastpost date and time
                        $lastpost_date = $this->time->formatDate('relative', $lastpost_data['lastpost']);

                        // Set up the last poster, last post thread id, last post subject and format appropriately
                        $lastpost_profilelink = $this->user->build_profile_link($lastpost_data['lastposter'], $lastpost_data['lastposteruid']);
                        $lastpost_link = get_thread_link($lastpost_data['lastposttid'], 0, 'lastpost');
                        $lastpost_subject = $full_lastpost_subject = $this->parser->parse_badwords($lastpost_data['lastpostsubject']);
                        if (my_strlen($lastpost_subject) > 25) {
                            $lastpost_subject = my_substr($lastpost_subject, 0, 25) . '...';
                        }
                        $lastpost_subject = htmlspecialchars_uni($lastpost_subject);
                        $full_lastpost_subject = htmlspecialchars_uni($full_lastpost_subject);

                        // Call lastpost template
                        if ($depth != 1) {
                            $lastpost = "
                        <small>
                        <a href=\"{$lastpost_link}\" title=\"{$full_lastpost_subject}\"><strong>{$lastpost_subject}</strong></a>
                        <br />{$lastpost_date}<br />{$this->lang->by} {$lastpost_profilelink}</small>";
                            //ev al("\$lastpost = \"".$this->bb->templates->get("forumbit_depth{$depth}_forum_lastpost")."\";");
                        }
                    }

                    if ($this->bb->settings['showforumviewing'] != 0 && (isset($forum['viewers']) && $forum['viewers'] > 0)) {
                        if ($forum['viewers'] == 1) {
                            $forum_viewers_text = $this->bb->lang->viewing_one;
                        } else {
                            $forum_viewers_text = $this->bb->lang->sprintf($this->bb->lang->viewing_multiple, $forum['viewers']);
                        }
                        $forum_viewers_text_plain = $forum_viewers_text;
                        $forum_viewers_text = '<span class="smalltext">' . $forum_viewers_text . '</span>';
                        //ev al("\$forum_viewers_text = \"".$this->bb->templates->get('forumbit_depth2_forum_viewers')."\";");
                    }
                }
                // If this forum is a link or is password protected and the user isn't authenticated, set counters to '-'
                if ($forum['linkto'] != '' || $hideinfo == true || $hidecounters == true) {
                    $posts = '-';
                    $threads = '-';
                } // Otherwise, format thread and post counts
                else {
                    $posts = $this->parser->formatNumber($forum['posts']);
                    $threads = $this->parser->formatNumber($forum['threads']);
                }

                // If this forum is a link or is password protected and the user isn't authenticated, set lastpost to '-'
                if ($forum['linkto'] != '' || $hideinfo == true || $hidelastpostinfo == true) {
                    $lastpost = '<div style="text-align: center;">-</div>';
                }

                // Moderator column is not off
                if ($this->bb->settings['modlist'] != 0) {
                    $done_moderators = [
                        'users' => [],
                        'groups' => []
                    ];
                    $moderators = '';
                    // Fetch list of moderators from this forum and its parents
                    $parentlistexploded = explode(',', $forum['parentlist']);
                    foreach ($parentlistexploded as $mfid) {
                        // This forum has moderators
                        if (isset($this->bb->moderatorcache[$mfid]) && is_array($this->bb->moderatorcache[$mfid])) {
                            // Fetch each moderator from the cache and format it, appending it to the list
                            foreach ($this->bb->moderatorcache[$mfid] as $modtype) {
                                foreach ($modtype as $moderator) {
                                    if ($moderator['isgroup']) {
                                        if (in_array($moderator['id'], $done_moderators['groups'])) {
                                            continue;
                                        }

                                        $moderator['title'] = htmlspecialchars_uni($moderator['title']);
                                        $moderators .= $comma . $moderator['title'];
                                        $done_moderators['groups'][] = $moderator['id'];
                                    } else {
                                        if (in_array($moderator['id'], $done_moderators['users'])) {
                                            continue;
                                        }

                                        $moderator['profilelink'] = get_profile_link($moderator['id']);
                                        $moderator['username'] = htmlspecialchars_uni($moderator['username']);
                                        $moderators .= "{$comma}<a href=\"{$moderator['profilelink']}\">{$moderator['username']}</a>";
                                        $done_moderators['users'][] = $moderator['id'];
                                    }
                                    $comma = $this->bb->lang->comma;
                                }
                            }
                        }
                    }
                    $comma = '';

                    // If we have a moderators list, load the template
                    $modlist = '';
                    if ($moderators) {
                        $modlist = '<br />' . $this->bb->lang->forumbit_moderated_by . ' ' . $moderators;
                    }
                }

                // Descriptions aren't being shown - blank them
                if ($this->bb->settings['showdescriptions'] == 0) {
                    $forum['description'] = '';
                }

                // Check if this category is either expanded or collapsed and hide it as necessary.
                $collapsed_name = "cat_{$forum['fid']}_c";

                if (isset($this->bb->collapsed[$collapsed_name]) && $this->bb->collapsed[$collapsed_name] == 'display: show;') {
                    $expcolimage = '_collapsed';//'collapse_collapsed.png';
                    $expdisplay = 'display: none;';
                    $expthead = ' thead_collapsed';
                    $expaltext = '[+]';
                } else {
                    $expdisplay = 'display: block;';
                    $expcolimage = '';//'collapse.png';
                    $expthead = '';
                    $expaltext = '[-]';
                }
//\Tracy\Debugger::barDump($sub_forums);
                // Swap over the alternate backgrounds
                $bgcolor = alt_trow();

//}
//        }
//        else{
                if ($forumcat == '_forum') {
                    $forum_forums[$forum['fid']] = [
                        'bgcolor' => alt_trow(),
                        'lightbulb' => $lightbulb,
                        'forum' => $forum,
                        'forum_url' => $forum_url,
                        'forum_viewers_text' => $forum_viewers_text,
                        'modlist' => $modlist,
                        'subforums' => $subforums,
                        'threads' => $threads,
                        'unapproved' => $unapproved,
                        'posts' => $posts,
                        'lastpost' => $lastpost
                    ];
                } else {
//        if($forumcat == '_cat'){
                    $forum_cats[$forum['fid']] = [
//            'expthead' => $expthead,
                        'expcolimage' => $expcolimage,
                        'forum' => $forum,
                        'expaltext' => $expaltext,
                        'forum_url' => $forum_url,
                        'expdisplay' => $expdisplay,
                        'forum_forums' => $forum_forums,
                    ];
                    $forum_forums = [];
                }

                \Tracy\Debugger::barDump("forumbit_depth$depth$forumcat");

                // Add the forum to the list
                //ev al("\$forum_list .= \"".$this->bb->templates->get("forumbit_depth$depth$forumcat")."\";");
            }
        }

        $forum_vars = [];
        if ($forum_cats == false) {
            $forum_vars = [
                'expaltext' => isset($expaltext) ? $expaltext : '',
                'fid' => $forum['fid'],
                'description' => $forum['description'],
                'expcolimage' => isset($expcolimage) ? $expcolimage : ''
            ];
        }
        if (!isset($parent_lastpost)) {
            $parent_lastpost = 0;
        }

        if (!isset($lightbulb)) {
            $lightbulb = '';
        }

        // Return an array of information to the parent forum including child forums list, counters and lastpost information
        return [
            'forum_cats' => $forum_cats,
            'forum_forums' => $forum_forums,
            'forum_list' => $forum_list,
            'forum_vars' => $forum_vars,
            'counters' => $parent_counters,
            'lastpost' => $parent_lastpost,
            'lightbulb' => $lightbulb,
        ];
    }

    /**
     * Fetch the status indicator for a forum based on its last post and the read date
     *
     * @param array $forum Array of information about the forum
     * @param array $lastpost Array of information about the lastpost date
     * @param int $locked Whether or not this forum is locked or not
     * @return array Array of the folder image to be shown and the alt text
     */
    public function get_forum_lightbulb($forum, $lastpost, $locked = 0)
    {
        // This forum is a redirect, so override the folder icon with the 'offlink' icon.
        if (!empty($forum['linkto'])) {
            $folder = 'offlink';
            $altonoff = $this->bb->lang->forum_redirect;
        } // This forum is closed, so override the folder icon with the 'offlock' icon.
        elseif ($forum['open'] == 0 || $locked) {
            $folder = 'offlock';
            $altonoff = $this->bb->lang->forum_locked;
        } else {
            // Fetch the last read date for this forum
            if (!empty($forum['lastread'])) {
                $forum_read = $forum['lastread'];
            } elseif (!empty($this->bb->cookies['mybb']['readallforums'])) {
                // We've hit the read all forums as a guest, so use the lastvisit of the user
                $forum_read = $this->bb->cookies['mybb']['lastvisit'];
            } else {
                $forum_read = 0;
                $threadcut = TIME_NOW - 60 * 60 * 24 * $this->bb->settings['threadreadcut'];

                // If the user is a guest, do they have a forumsread cookie?
                if (!$this->user->uid && isset($this->bb->cookies['mybb']['forumread'])) {
                    // If they've visited us before, then they'll have this cookie - otherwise everything is unread...
                    $forum_read = $this->bb->my_get_array_cookie('forumread', $forum['fid']);
                } elseif ($this->user->uid && $this->bb->settings['threadreadcut'] > 0 && $threadcut > $lastpost['lastpost']) {
                    // We have a user, the forum's unread and we're over our threadreadcut
                    // limit for the lastpost - we mark these as read
                    $forum_read = $lastpost['lastpost'] + 1;
                }
            }

            //if(!$forum_read)
            //{
            //$forum_read = $this->user->lastvisit;
            //}

            // If the lastpost is greater than the last visit and is greater than the forum read date, we have a new post
            if ($lastpost['lastpost'] > $forum_read && $lastpost['lastpost'] != 0) {
                $this->unread_forums++;
                $folder = 'on';
                $altonoff = $this->bb->lang->new_posts;
            } // Otherwise, no new posts
            else {
                $folder = 'off';
                $altonoff = $this->bb->lang->no_new_posts;
            }
        }

        return [
            'folder' => $folder,
            'altonoff' => $altonoff
        ];
    }

    /**
     * Fetch the number of unapproved posts, formatted, from a forum
     *
     * @param array $forum Array of information about the forum
     * @return array Array containing formatted string for posts and string for threads
     */
    private function get_forum_unapproved($forum)
    {
        $unapproved_threads = $unapproved_posts = '';

        // If the user is a moderator we need to fetch the count
        if ($this->user->is_moderator($forum['fid'], 'canviewunapprove')) {
            // Forum has one or more unaproved posts, format language string accordingly
            if ($forum['unapprovedposts']) {
                if ($forum['unapprovedposts'] > 1) {
                    $unapproved_posts_count = $this->bb->lang->sprintf($this->bb->lang->forum_unapproved_posts_count, $forum['unapprovedposts']);
                } else {
                    $unapproved_posts_count = $this->bb->lang->sprintf($this->bb->lang->forum_unapproved_post_count, 1);
                }

                $forum['unapprovedposts'] = $this->parser->formatNumber($forum['unapprovedposts']);
                $unapproved_posts = '<span title="' . $unapproved_posts_count . '">(' . $forum['unapprovedposts'] . ')</span>';
                //ev al("\$unapproved_posts = \"".$this->bb->templates->get('forumbit_depth2_forum_unapproved_posts')."\";");
            }
            // Forum has one or more unapproved threads, format language string accordingly
            if ($forum['unapprovedthreads']) {
                if ($forum['unapprovedthreads'] > 1) {
                    $unapproved_threads_count = $this->bb->lang->sprintf($this->bb->lang->forum_unapproved_threads_count, $forum['unapprovedthreads']);
                } else {
                    $unapproved_threads_count = $this->bb->lang->sprintf($this->bb->lang->forum_unapproved_thread_count, 1);
                }

                $forum['unapprovedthreads'] = $this->parser->formatNumber($forum['unapprovedthreads']);
                $unapproved_threads = '<span title="' . $unapproved_threads_count . '">(' . $forum['unapprovedthreads'] . ')</span>';
                //ev al("\$unapproved_threads = \"".$this->bb->templates->get('forumbit_depth2_forum_unapproved_threads')."\";");
            }
        }
        return [
            'unapproved_posts' => $unapproved_posts,
            'unapproved_threads' => $unapproved_threads
        ];
    }

    /**
     * Builds a forum jump menu
     *
     * @param int $pid The parent forum to start with
     * @param int $selitem The selected item ID
     * @param int $addselect If we need to add select boxes to this cal or not
     * @param string $depth The current depth of forums we're at
     * @param int $showextras Whether or not to show extra items such as User CP, Forum home
     * @param boolean $showall Ignore the showinjump setting and show all forums (for moderation pages)
     * @param mixed $permissions deprecated
     * @param string $name The name of the forum jump
     * @return string Forum jump items
     */
    public function build_forum_jump(
        $pid = 0,
        $selitem = 0,
        $addselect = 1,
        $depth = '',
        $showextras = 1,
        $showall = false,
        $permissions = '',
        $name = 'fid'
    ) {
    
        static $jumpfcache;

        $pid = (int)$pid;

        if (!is_array($jumpfcache)) {
            if (!is_array($this->bb->forum_cache)) {
                $this->cache_forums();
            }

            foreach ($this->bb->forum_cache as $fid => $forum) {
                if ($forum['active'] != 0) {
                    $jumpfcache[$forum['pid']][$forum['disporder']][$forum['fid']] = $forum;
                }
            }
        }

        //if (!is_array($this->bb->permissioncache)) {
        if (empty($this->bb->permissioncache)) {
            $this->bb->permissioncache = $this->forum_permissions();
        }

        if (isset($jumpfcache[$pid]) && is_array($jumpfcache[$pid])) {
            $forumjumpbits = '';
            foreach ($jumpfcache[$pid] as $main) {
                foreach ($main as $forum) {
                    $perms = $this->bb->permissioncache[$forum['fid']];

                    if ($forum['fid'] != "0" &&
                        ($perms['canview'] != 0 || $this->bb->settings['hideprivateforums'] == 0) &&
                        $forum['linkto'] == '' && ($forum['showinjump'] != 0 || $showall == true)
                    ) {
                        $optionselected = '';

                        if ($selitem == $forum['fid']) {
                            $optionselected = 'selected="selected"';
                        }

                        $forum['name'] = htmlspecialchars_uni(strip_tags($forum['name']));
                        $forumjumpbits .= "<option value=\"{$forum['fid']}\" {$optionselected}>{$depth} {$forum['name']}</option>";

                        if ($this->bb->forum_cache[$forum['fid']]) {
                            $newdepth = $depth . '--';
                            $forumjumpbits .= $this->build_forum_jump($forum['fid'], $selitem, 0, $newdepth, $showextras, $showall);
                        }
                    }
                }
            }
        }
        $forumjump = '';
        if ($addselect) {
            if ($showextras == 0) {
                //$template = "special";
                $forumjump = "
                <select class=\"form-control\" name=\"{$name}\">
                    {$forumjumpbits}
                </select>";
            } else {
                $template = "advanced";

//        if (strpos(FORUM_URL, '.html') !== false) {
//          $forum_link = "'" . str_replace('{fid}', "'+option+'", FORUM_URL) . "'";
//        } else {
                $forum_link = str_replace('{fid}', "'+option", FORUM_URL);
//        }
                $forumjump = "
      <form action=\"{$this->bb->settings['bburl']}/forumdisplay\" method=\"get\">
        <span class=\"smalltext\"><strong>{$this->bb->lang->forumjump}</strong></span>
        <select name=\"{$name}\" class=\"forumjump\">
        <option value=\"-4\">{$this->bb->lang->forumjump_pms}</option>
        <option value=\"-3\">{$this->bb->lang->forumjump_usercp}</option>
        <option value=\"-5\">{$this->bb->lang->forumjump_wol}</option>
        <option value=\"-2\">{$this->bb->lang->forumjump_search}</option>
        <option value=\"-1\">{$this->bb->lang->forumjump_home}</option>
        {$forumjumpbits}
        </select>
        <input type=\"submit\" class=\"button\" value=\"{$this->bb->lang->go}\" />
      </form>
      <script type=\"text/javascript\">
      $(\".forumjump\").change(function() {
        var option = $(this).val();
      
        if(option < 0)
        {
          window.location = '{$this->bb->settings['bburl']}/forumdisplay?fid='+option;
        }
        else
        {
          window.location = '{$forum_link};
        }
      });
      </script>";
            }
            //ev al("\$forumjump = \"" . $this->bb->templates->get("forumjump_" . $template) . "\";");
        }

        return $forumjump;
    }
}
