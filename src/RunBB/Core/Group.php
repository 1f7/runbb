<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Group extends AbstractController
{
    /**
     * Fetch the usergroup permissions for a specific group or series of groups combined
     *
     * @param int|string $gid A list of groups (Can be a single integer, or a list of groups separated by a comma)
     * @return array Array of permissions generated for the groups
     */
    public function usergroup_permissions($gid = 0)
    {
        if (!is_array($this->bb->groupscache)) {
            $this->bb->groupscache = $this->bb->cache->read('usergroups');
        }

        $groups = explode(',', $gid);

        if (count($groups) == 1) {
            return $this->bb->groupscache[$gid];
        }

        $usergroup = [];

        foreach ($groups as $gid) {
            if (trim($gid) == '' || !$this->bb->groupscache[$gid]) {
                continue;
            }

            foreach ($this->bb->groupscache[$gid] as $perm => $access) {
                if (!in_array($perm, $this->bb->grouppermignore)) {
                    if (isset($usergroup[$perm])) {
                        $permbit = $usergroup[$perm];
                    } else {
                        $permbit = '';
                    }

                    // 0 represents unlimited for numerical group permissions (i.e. private message limit) so take that into account.
                    if (in_array($perm, $this->bb->groupzerogreater) && ($access == 0 || $permbit === 0)) {
                        $usergroup[$perm] = 0;
                        continue;
                    }

                    if ($access > $permbit || ($access == 'yes' && $permbit == 'no') || !$permbit) { // Keep yes/no for compatibility?
                        $usergroup[$perm] = $access;
                    }
                }
            }
        }

        return $usergroup;
    }

    /**
     * Fetch the display group properties for a specific display group
     *
     * @param int $gid The group ID to fetch the display properties for
     * @return array Array of display properties for the group
     */
    public function usergroup_displaygroup($gid)
    {
        if (empty($this->bb->groupscache)) {
            $this->bb->groupscache = $this->bb->cache->read('usergroups');
        }

        $displaygroup = [];
        $group = $this->bb->groupscache[$gid];

        foreach ($this->bb->displaygroupfields as $field) {
            $displaygroup[$field] = $group[$field];
        }

        return $displaygroup;
    }

    /**
     * Add a user to a specific additional user group.
     *
     * @param int $uid The user ID
     * @param int $joingroup The user group ID to join
     * @return bool
     */
    public function join_usergroup($uid, $joingroup)
    {
        if ($uid == $this->user->uid) {
            $user = $this->user;
        } else {
            $query = $this->db->simple_select('users', 'additionalgroups, usergroup', "uid='" . (int)$uid . "'");
            $user = $this->db->fetch_array($query);
        }

        // Build the new list of additional groups for this user and make sure they're in the right format
        $usergroups = '';
        $usergroups = $user['additionalgroups'] . ',' . $joingroup;
        $groupslist = '';
        $groups = explode(',', $usergroups);

        if (is_array($groups)) {
            $comma = '';
            foreach ($groups as $gid) {
                if (trim($gid) != '' && $gid != $user['usergroup'] && !isset($donegroup[$gid])) {
                    $groupslist .= $comma . $gid;
                    $comma = ',';
                    $donegroup[$gid] = 1;
                }
            }
        }

        // What's the point in updating if they're the same?
        if ($groupslist != $user['additionalgroups']) {
            $this->db->update_query('users', ['additionalgroups' => $groupslist], "uid='" . (int)$uid . "'");
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove a user from a specific additional user group
     *
     * @param int $uid The user ID
     * @param int $leavegroup The user group ID
     */
    public function leave_usergroup($uid, $leavegroup)
    {
        $user = $this->user->get_user($uid);

        $groupslist = $comma = '';
        $usergroups = $user['additionalgroups'] . ',';
        $donegroup = [];

        $groups = explode(',', $user['additionalgroups']);

        if (is_array($groups)) {
            foreach ($groups as $gid) {
                if (trim($gid) != '' && $leavegroup != $gid && empty($donegroup[$gid])) {
                    $groupslist .= $comma . $gid;
                    $comma = ',';
                    $donegroup[$gid] = 1;
                }
            }
        }

        $dispupdate = '';
        if ($leavegroup == $user['displaygroup']) {
            $dispupdate = ', displaygroup=usergroup';
        }

        $this->db->write_query('
		UPDATE ' . TABLE_PREFIX . "users
		SET additionalgroups='$groupslist' $dispupdate
		WHERE uid='" . (int)$uid . "'
	");

        $this->bb->cache->update_moderators();
    }
}
