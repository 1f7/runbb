<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class Indicators extends AbstractController
{
    /**
     * Mark a particular thread as read for the current user.
     *
     * @param int $tid The thread ID
     * @param int $fid The forum ID of the thread
     */
    public function mark_thread_read($tid, $fid)
    {
        // Can only do "true" tracking for registered users
        if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
            // For registered users, store the information in the database.
//            switch ($this->db->type) {
//                case "pgsql":
//                case "sqlite":
//                    $this->db->replace_query("threadsread",
//                        array('tid' => $tid, 'uid' => $this->user->uid, 'dateline' => TIME_NOW),
//                        array("tid", "uid"));
//                    break;
//                default:
//                    $this->db->write_query("
//					REPLACE INTO " . TABLE_PREFIX . "threadsread (tid, uid, dateline)
//					VALUES('$tid', '{$this->user->uid}', '" . TIME_NOW . "')
//				");
//            }
            $tr = \RunBB\Models\Threadsread::firstOrCreate(['tid' => $tid, 'uid' => $this->user->uid]);
            $tr->dateline = TIME_NOW;
            $tr->save();
        } // Default back to cookie marking
        else {
            $this->bb->my_set_array_cookie("threadread", $tid, TIME_NOW, -1);
        }

        $unread_count = $this->fetch_unread_count($fid);
        if ($unread_count == 0) {
            $this->mark_forum_read($fid);
        }
    }

    /**
     * Fetches the number of unread threads for the current user in a particular forum.
     *
     * @param string $fid The forums (CSV list)
     * @return int The number of unread threads
     */
    public function fetch_unread_count($fid)
    {
        $onlyview = $onlyview2 = '';
        $permissions = $this->bb->forum->forum_permissions($fid);
        $cutoff = TIME_NOW - $this->bb->settings['threadreadcut'] * 60 * 60 * 24;

        if (!empty($permissions['canonlyviewownthreads'])) {
            $onlyview = " AND uid = '{$this->user->uid}'";
            $onlyview2 = " AND t.uid = '{$this->user->uid}'";
        }

        if ($this->user->uid == 0) {
            $comma = '';
            $tids = '';
            $threadsread = $forumsread = [];

            if (isset($this->bb->cookies['mybb']['threadread'])) {
                $threadsread = my_unserialize($this->bb->cookies['mybb']['threadread']);
            }
            if (isset($this->bb->cookies['mybb']['forumread'])) {
                $forumsread = my_unserialize($this->bb->cookies['mybb']['forumread']);
            }

            if (!empty($threadsread)) {
                foreach ($threadsread as $key => $value) {
                    $tids .= $comma . (int)$key;
                    $comma = ',';
                }
            }

            if (!empty($tids)) {
                $count = 0;

                // We've read at least some threads, are they here?
                $query = $this->db->simple_select(
                    "threads",
                    "lastpost, tid, fid",
                    "visible=1 AND closed NOT LIKE 'moved|%' AND fid IN ({$fid}) AND lastpost > '{$cutoff}'{$onlyview}",
                    ["limit" => 100]
                );

                while ($thread = $this->db->fetch_array($query)) {
                    if (isset($threadsread[$thread['tid']]) &&
                        $thread['lastpost'] > (int)$threadsread[$thread['tid']] &&
                        isset($forumsread[$thread['fid']]) &&
                        $thread['lastpost'] > (int)$forumsread[$thread['fid']]
                    ) {
                        ++$count;
                    }
                }

                return $count;
            }

            // Not read any threads?
            return false;
        } else {
            switch ($this->db->type) {
                case "pgsql":
                    $query = $this->db->query("
					SELECT COUNT(t.tid) AS unread_count
					FROM " . TABLE_PREFIX . "threads t
					LEFT JOIN " . TABLE_PREFIX . "threadsread tr ON (tr.tid=t.tid AND tr.uid='{$this->user->uid}')
					LEFT JOIN " . TABLE_PREFIX . "forumsread fr ON (fr.fid=t.fid AND fr.uid='{$this->user->uid}')
					WHERE t.visible=1 AND t.closed NOT LIKE 'moved|%' AND t.fid IN ($fid) AND t.lastpost > COALESCE(tr.dateline,$cutoff) AND t.lastpost > COALESCE(fr.dateline,$cutoff) AND t.lastpost>$cutoff{$onlyview2}
				");
                    break;
                default:
//                    $query = $this->db->query("
//					SELECT COUNT(t.tid) AS unread_count
//					FROM " . TABLE_PREFIX . "threads t
//					LEFT JOIN " . TABLE_PREFIX . "threadsread tr ON (tr.tid=t.tid AND tr.uid='{$this->user->uid}')
//					LEFT JOIN " . TABLE_PREFIX . "forumsread fr ON (fr.fid=t.fid AND fr.uid='{$this->user->uid}')
//					WHERE t.visible=1 AND t.closed NOT LIKE 'moved|%' AND t.fid IN ($fid)
//					    AND t.lastpost > IFNULL(tr.dateline,$cutoff)
//					    AND t.lastpost > IFNULL(fr.dateline,$cutoff)
//					    AND t.lastpost>$cutoff{$onlyview2}");
                    $c = \RunBB\Models\Thread::where([
                        ['visible', '=', 1],
                        ['closed', 'NOT LIKE', "'moved|%'"],
                        ['lastpost', '>', "IFNULL(`".DB::connection()->getTablePrefix()."threadsread.dateline`,$cutoff)"],
                        ['lastpost', '>', $cutoff]
                    ])
                        ->whereIn('threads.fid', [$fid])
                        ->leftJoin('threadsread', function ($j) {
                            $j->on('threadsread.tid', '=', 'threads.tid');
                            $j->on('threadsread.uid', '=', DB::raw($this->user->uid));
                        })
                        ->leftJoin('forumsread', function ($join) {
                            $join->on('forumsread.fid', '=', 'threads.fid');
                            $join->on('forumsread.uid', '=', DB::raw($this->user->uid));
                        })
                        ->count('threads.tid');
            }
//            return $this->db->fetch_field($query, "unread_count");
            return $c;
        }
    }

    /**
     * Mark a particular forum as read.
     *
     * @param int $fid The forum ID
     */
    public function mark_forum_read($fid)
    {
        // Can only do "true" tracking for registered users
        if ($this->bb->settings['threadreadcut'] > 0 && $this->user->uid) {
            // Experimental setting to mark parent forums as read
            $forums_to_read = [];

            if ($this->bb->settings['readparentforums']) {
                $ignored_forums = [];
                $forums = array_reverse(explode(",", $this->bb->forum->get_parent_list($fid)));

                unset($forums[0]);
                if (!empty($forums)) {
                    $ignored_forums[] = $fid;

                    foreach ($forums as $forum) {
                        $fids = [$forum];
                        $ignored_forums[] = $forum;

                        $children = explode(",", $this->bb->forum->get_parent_list($forum));
                        foreach ($children as $child) {
                            if (in_array($child, $ignored_forums)) {
                                continue;
                            }

                            $fids[] = $child;
                            $ignored_forums[] = $child;
                        }

                        if ($this->fetch_unread_count(implode(",", $fids)) == 0) {
                            $forums_to_read[] = $forum;
                        }
                    }
                }
            }

            switch ($this->db->type) {
                case "pgsql":
                case "sqlite":
                    $this->bb->add_shutdown(
                        [$this->bb->db, "replace_query"],
                        ["forumsread",
                            ['fid' => $fid, 'uid' => $this->user->uid, 'dateline' => TIME_NOW],
                        ["fid", "uid"]]
                    );

                    if (!empty($forums_to_read)) {
                        foreach ($forums_to_read as $forum) {
                            $this->bb->add_shutdown(
                                [$this->bb->db, "replace_query"],
                                ["forumsread",
                                    ['fid' => $forum, 'uid' => $this->user->uid, 'dateline' => TIME_NOW],
                                ['fid', 'uid']]
                            );
                        }
                    }
                    break;
                default:
                    $child_sql = '';
                    if (!empty($forums_to_read)) {
                        foreach ($forums_to_read as $forum) {
                            $child_sql .= ", ('{$forum}', '{$this->user->uid}', '" . TIME_NOW . "')";
                        }
                    }

                    $this->db->shutdown_query("
					REPLACE INTO " . TABLE_PREFIX . "forumsread (fid, uid, dateline)
					VALUES('{$fid}', '{$this->user->uid}', '" . TIME_NOW . "'){$child_sql}
				");
            }
        } // Mark in a cookie
        else {
            $this->bb->my_set_array_cookie("forumread", $fid, TIME_NOW, -1);
        }
    }

    /**
     * Marks all forums as read.
     *
     */
    public function mark_all_forums_read()
    {
        // Can only do "true" tracking for registered users
        if ($this->user->uid > 0) {
            $this->db->update_query("users", ['lastvisit' => TIME_NOW], "uid='" . $this->user->uid . "'");

            $this->user->update_pm_count('', 2);

            if ($this->bb->settings['threadreadcut'] > 0) {
                // Need to loop through all forums and mark them as read
                $forums = $this->bb->cache->read('forums');

                $update_count = ceil(count($forums) / 20);

                if ($update_count < 15) {
                    $update_count = 15;
                }

                $mark_query = '';
                $done = 0;
                foreach (array_keys($forums) as $fid) {
                    switch ($this->db->type) {
                        case "pgsql":
                        case "sqlite":
                            $mark_query[] = ['fid' => $fid, 'uid' => $this->user->uid, 'dateline' => TIME_NOW];
                            break;
                        default:
                            if ($mark_query != '') {
                                $mark_query .= ',';
                            }
                            $mark_query .= "('{$fid}', '{$this->user->uid}', '" . TIME_NOW . "')";
                    }
                    ++$done;

                    // Only do this in loops of $update_count, save query time
                    if ($done % $update_count) {
                        switch ($this->db->type) {
                            case "pgsql":
                            case "sqlite":
                                foreach ($mark_query as $replace_query) {
                                    $this->bb->add_shutdown(
                                        [$this->bb->db, "replace_query"],
                                        ["forumsread", $replace_query, ["fid", "uid"]]
                                    );
                                }
                                $mark_query = [];
                                break;
                            default:
                                $this->db->shutdown_query("
								REPLACE INTO " . TABLE_PREFIX . "forumsread (fid, uid, dateline)
								VALUES {$mark_query}
							");
                                $mark_query = '';
                        }
                    }
                }

                if ($mark_query != '') {
                    switch ($this->db->type) {
                        case "pgsql":
                        case "sqlite":
                            foreach ($mark_query as $replace_query) {
                                $this->bb->add_shutdown(
                                    [$this->bb->db, "replace_query"],
                                    ["forumsread", $replace_query, ["fid", "uid"]]
                                );
                            }
                            break;
                        default:
                            $this->db->shutdown_query("
							REPLACE INTO " . TABLE_PREFIX . "forumsread (fid, uid, dateline)
							VALUES {$mark_query}
						");
                    }
                }
            }
        } else {
            $this->bb->my_setcookie("mybb[readallforums]", 1);
            $this->bb->my_setcookie("mybb[lastvisit]", TIME_NOW);

            $this->bb->my_unsetcookie("mybb[threadread]");
            $this->bb->my_unsetcookie("mybb[forumread]");
        }
    }
}
