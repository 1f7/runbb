<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

//use RunCMF\Core\AbstractController;

/**
 * Class Language
 * @package RunBB\Core
 */
class Language// extends AbstractController
{
    protected $c;
    /**
     * The path to cache languages folder.
     *
     * @var string
     */
    public $path;

    /**
     * The language we are using.
     *
     * @var string
     */
    public $language;

    /**
     * The fallback language we are using.
     *
     * @var string
     */
    public $fallback = 'english';

    /**
     * Information about the current language.
     *
     * @var array
     */
    public $settings;

    /**
     * Language area: user or admin
     *
     * @var string
     */
    public $area = 'user';

    /**
     * Current language file
     *
     * @var string
     */
    private $lfile = '';

    /**
     * Installed languages
     *
     * @var array
     */
    public $installed = [];


    public function __construct(\Slim\Container $c)
    {
//        parent::__construct($c);
        $this->c = $c;

        $this->path = DIR . 'var/cache/languages';
    }

    /**
     * Set the path for the language folder.
     *
     * @param string $path The path to the language folder.
     */
//    public function set_path($path)
//    {
//        $this->path = $path;
//    }

    /**
     * Check if a specific language exists.
     *
     * @param string $language The language to check for.
     * @return boolean True when exists, false when does not exist.
     */
    public function language_exists($language)
    {
        $language = preg_replace("#[^a-z0-9\-_]#i", '', $language);

        if (array_key_exists($language, $this->get_languages())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set the language for an area.
     *
     * @param string $language The language to use.
     * @param string $area The area to set the language for.
     */
    public function set_language($language = 'english', $area = 'user')
    {
        $language = preg_replace("#[^a-z0-9\-_]#i", '', $language);
        $this->area = $area;

        // Default language is English.
        if ($language == '') {
            $language = 'english';
        }
        $this->language = $language;
        $this->lfile = $this->path . '/' . $language.'_user_langinfo.php';

        // Check if the language exists.
//        if (!$this->language_exists($language)) {
        if (!file_exists($this->lfile)) {
//            die('Language '.$language.' is not installed');

//        $langinfo = [];
//        require $this->path . '/' . $language . '.php';
//        $this->settings = $langinfo;

            // пытаемся загрузить и бд
            $class = '\\RunBB\\Models\\Lang' . ucfirst($language);
            $l = $class::where('section', 'langinfo')
                ->get(['var', 'trans'])
                ->keyBy('var')
                ->map(function ($vars) {
                    return $vars->var = $vars->trans;
                });
            if (empty($l)) {
                die('Language '.$language.' is not installed');
            } else {
                $this->put('langinfo', $l);
            }
        } else {
            $l = require_once $this->lfile;
        }

        if (!is_array($l) || empty($l)) {
//\Tracy\Debugger::dump('$l is not array or empty twice set_language()');
            return false;// try double set_language() ???
        }
        foreach ($l as $key => $val) {
            $this->settings[$key] = $val;
        }
//        // Load the admin language files as well, if needed.
//        if ($area === 'admin') {
//            if (!is_dir($this->path . '/' . $language . "/{$area}")) {
//                if (!is_dir($this->path . '/' . $this->bb->settings['cplanguage'] . "/{$area}")) {
//                    if (!is_dir($this->path . "/english/{$area}")) {
//                        die('Your forum does not contain an Administration set. Please reupload the english language administration pack.');
//                    } else {
//                        $language = 'english';
//                    }
//                } else {
//                    $language = $this->bb->settings['cplanguage'];
//                }
//            }
//            $this->language = $language . "/{$area}";
//            $this->fallback = $this->fallback . "/{$area}";
//        }
    }

    /**
     * Load the language variables for a section.
     *
     * @param string $section The section name.
     * @param boolean $isdatahandler Is this a datahandler?
     * @param boolean $supress_error supress the error if the file doesn't exist?
     */
    public function load($section, $isdatahandler = false, $supress_error = false)
    {
        // Assign language variables.
        // Datahandlers are never in admin lang directory.
        if ($this->area === 'admin' && $isdatahandler === false) {
            $isadmin = 1;
            $this->lfile = $this->path . '/' . $this->language.'_admin_'.$section.'.php';
        } else {
            $isadmin = 0;
            $this->lfile = $this->path . '/' . $this->language.'_user_'.$section.'.php';
        }
/*
        if (file_exists($lfile)) {
            require_once $lfile;
        } elseif (file_exists($this->path . '/' . $this->fallback . '/' . $section . '.lang.php')) {
            require_once $this->path . '/' . $this->fallback . '/' . $section . '.lang.php';
        } else {
            if ($supress_error != true) {
                die($lfile.' does not exist');
            }
        }
*/
        if (file_exists($this->lfile)) {
            $l = require_once $this->lfile;
            if (!is_array($l) || empty($l)) {
//\Tracy\Debugger::dump('cache $l is not array or empty. lfile: '.$this->lfile);
                return false;// try double load lang ???
            }
        } else {
            $class = '\\RunBB\\Models\\Lang' . ucfirst($this->language);
            $l = $class::where('section', $section)
                ->where('isadmin', $isadmin)
                ->get(['var', 'trans'])
                ->keyBy('var')
                ->map(function ($vars) {
                    return $vars->var = $vars->trans;
                });
            if (!$this->put($section, $l)) {
                return false;
            }
            if (!is_object($l) || empty($l)) {
                dump('db $l is not object or empty');
                return false;// try double load lang ???
            }
        }
        // We must unite and protect our language variables!
        $lang_keys_ignore = ['language', 'path', 'settings'];
        foreach ($l as $key => $val) {
            if ((empty($this->$key) || $this->$key != $val) && !in_array($key, $lang_keys_ignore)) {
                $this->$key = $val;
            }
        }
        unset($l);
    }

    private function put($section, $contents)
    {
        if (!is_writable($this->path)) {
            $this->c['bb']->trigger_generic_error('cache_no_write');
            return false;
        }
        $vars=[];
        foreach ($contents as $k => $v) {
            $vars[$k] = $v;
        }

        $cache_file = fopen($this->lfile, 'w') or $this->c['bb']->trigger_generic_error('cache_no_write');
        flock($cache_file, LOCK_EX);
        $cache_contents = "<?php\n/**\n * RunBB Generated Cache - Do Not Alter\n * Language Cache Section: $section\n * Generated: " . gmdate('r') . "\n*/\n\n";
        $cache_contents .= 'return ' . var_export($vars, true) . ';';//";\n\n? >";
        fwrite($cache_file, $cache_contents);
        flock($cache_file, LOCK_UN);
        fclose($cache_file);

        return true;
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public function sprintf($string)
    {
        $arg_list = func_get_args();
        $num_args = count($arg_list);

        for ($i = 1; $i < $num_args; $i++) {
            $string = str_replace('{' . $i . '}', $arg_list[$i], $string);
        }

        return $string;
    }

    /**
     * Get the language variables for a section.
     *
     * @param boolean $admin Admin variables when true, user when false.
     * @return array The language variables.
     */
    public function get_languages($admin = 0)//false)
    {
        if (empty($this->installed)) {
            $this->installed = $this->c['settings']['languages'];
/*
            $l = \RunBB\Models\Language::where('installed', 1)
                ->get(['name']);
            foreach ($l as $lang) {
                $this->installed[my_strtolower($lang->name)] = $lang->name;
            }
*/
            @ksort($this->installed);
        }
        return $this->installed;
//        $langinfo = [];
//        $dir = @opendir($this->path);
//        while ($lang = readdir($dir)) {
//            $ext = my_strtolower(get_extension($lang));
//            if ($lang != '.' && $lang != '..' && $ext == 'php') {
//                $lname = str_replace('.' . $ext, '', $lang);
//                require $this->path . '/' . $lang;
//                if (!$admin || ($admin && $langinfo['admin'])) {
//                    $languages[$lname] = $langinfo['name'];
//                }
//            }
//        }
//        @ksort($languages);
//        return $languages;
    }

    public function getSections($lang = '', $admin = 0)
    {
        $l=[];
        if (!empty($lang)) {
            $class = '\\RunBB\\Models\\Lang' . ucfirst($lang);
            $l = $class::where('isadmin', $admin)
                ->orderBy('section', 'asc')
                ->groupBy('section')
                ->get(['isadmin', 'section']);
        }

        return $l;
    }

    public function getSection($lang = '', $section = '', $admin = 0)
    {
        $ret=[];
        if (!empty($lang)) {
            $class = '\\RunBB\\Models\\Lang' . ucfirst($lang);
            $l = $class::where('isadmin', $admin)
                ->where('section', $section)
                ->get()
                ->keyBy('var')
                ->map(function ($vars) {
                    return $vars->var = $vars->trans;
                });
            foreach ($l as $k => $v) {
                $ret[$k] = $v;
            }
        }

        return $ret;
    }

    public function sectionCount($lang = '', $section = '', $admin = 0)
    {
        $class = '\\RunBB\\Models\\Lang' . ucfirst($lang);
        return $class::where('section', $section)
            ->where('isadmin', $admin)
            ->count();
    }

    public function getLangInfo($lang = '')
    {
        $class = '\\RunBB\\Models\\Lang' . ucfirst($lang);
        return $class::where('section', 'langinfo')
            ->get()
            ->keyBy('var')
            ->map(function ($vars) {
                return $vars->var = $vars->trans;
            });
    }

    public function saveSection($lang = '', $section = '', $vars = [], $admin = 0)
    {
        $class = '\\RunBB\\Models\\Lang' . ucfirst($lang);
        foreach ($vars as $k => $v) {
            $m = $class::where('section', $section)
                ->where('isadmin', $admin)
                ->where('var', $k)
                ->first();
            $m->trans = $v;
            $m->save();
            $m->touch();//update timestamp
        }
        return true;
    }

    public function clearCache()
    {
        $list = $this->c['files']->glob($this->path.'/*');
        foreach ($list as $file) {
            unlink($file);
        }
    }

    /**
     * Parse contents for language variables.
     *
     * @param string $contents The contents to parse.
     * @return string The parsed contents.
     */
    public function parse($contents)
    {
        $contents = preg_replace_callback("#<lang:([a-zA-Z0-9_]+)>#", [$this, 'parse_replace'], $contents);
        return $contents;
    }

    /**
     * Replace content with language variable.
     *
     * @param array $matches Matches.
     * @return string Language variable.
     */
    private function parse_replace($matches)
    {
        return $this->{$matches[1]};
    }
    ///////////////////////////////////////////////////////



/*
    ///// временное, заливка лэнгов в бд /////
    public function getLangsInfo()
    {
        $cat = DIR .'vendor/runcmf/runbb/src/RunBB/Languages';
        $v = array_filter(array_values(array_diff(scandir($cat), ['.', '..'])), function ($file) use ($cat) {
            return is_file($cat . DIRECTORY_SEPARATOR . $file) && is_readable($cat . DIRECTORY_SEPARATOR . $file);
        });
        foreach ($v as $item) {
            $langinfo = [];
            $pathinfo = pathinfo($cat . '/' . $item);

            require $cat . '/' . $item;
            foreach($langinfo as $k=>$v) {
                $data[$pathinfo['filename']][] = [
                    'section' => 'langinfo',
                    'var' => $k,
                    'trans' => $v
                ];
            }
        }
        return $data;
    }

    public function getLangData($l = '')
    {
        $cat = DIR .'vendor/runcmf/runbb/src/RunBB/Languages/'.$l;
        $v = array_filter(array_values(array_diff(scandir($cat), ['.', '..'])), function ($file) use ($cat) {
            return is_file($cat . DIRECTORY_SEPARATOR . $file) && is_readable($cat . DIRECTORY_SEPARATOR . $file);
        });
        foreach ($v as $file) {
            $l = [];
            $section = str_replace('.lang.php', '', $file);
            require $cat . '/' . $file;
            foreach($l as $k=>$v) {
                $data[$section][] = [
                    'var' => $k,
                    'trans' => $v
                ];
            }
        }
        return $data;
    }
*/
}
