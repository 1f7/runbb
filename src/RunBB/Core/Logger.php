<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Logger extends AbstractController
{
    /**
     * Log the actions of a moderator.
     *
     * @param array $data The data of the moderator's action.
     * @param string $action The message to enter for the action the moderator performed.
     */
    public function log_moderator_action($data, $action = '')
    {
        $fid = 0;
        if (isset($data['fid'])) {
            $fid = (int)$data['fid'];
            unset($data['fid']);
        }

        $tid = 0;
        if (isset($data['tid'])) {
            $tid = (int)$data['tid'];
            unset($data['tid']);
        }

        $pid = 0;
        if (isset($data['pid'])) {
            $pid = (int)$data['pid'];
            unset($data['pid']);
        }

        // Any remaining extra data - we my_serialize and insert in to its own column
        if (is_array($data)) {
            $data = my_serialize($data);
        }

        $sql_array = [
            'uid' => (int)$this->user->uid,
            'dateline' => TIME_NOW,
            'fid' => (int)$fid,
            'tid' => $tid,
            'pid' => $pid,
            'action' => $this->db->escape_string($action),
            'data' => $this->db->escape_string($data),
            'ipaddress' => $this->bb->session->ipaddress
        ];
        $this->db->insert_query('moderatorlog', $sql_array);
    }

    /**
     * Logs an administrator action taking any arguments as log data.
     */
    public function log_admin_action()
    {
        $data = func_get_args();

        if (count($data) == 1 && is_array($data[0])) {
            $data = $data[0];
        }

        if (!is_array($data)) {
            $data = [$data];
        }

        $log_entry = [
            'uid' => (int)$this->user->uid,
            'ipaddress' => $this->session->ipaddress,
            'dateline' => TIME_NOW,
            'module' => $this->db->escape_string($this->bb->getInput('module', '')),
            'action' => $this->db->escape_string($this->bb->getInput('action', '')),
            'data' => $this->db->escape_string(@my_serialize($data))
        ];

        $this->db->insert_query('adminlog', $log_entry);
    }

    /**
     * Log a user spam block from StopForumSpam (or other spam service providers...)
     *
     * @param string $username The username that the user was using.
     * @param string $email The email address the user was using.
     * @param string $ip_address The IP addres of the user.
     * @param array $data An array of extra data to go with the block (eg: confidence rating).
     * @return bool Whether the action was logged successfully.
     */
    public function log_spam_block($username = '', $email = '', $ip_address = '', $data = [])
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        if (!$ip_address) {
            $ip_address = $this->session->ipaddress;
        }

//        $ip_address = my_inet_pton($ip_address);

        $insert_array = [
            'username' => $this->db->escape_string($username),
            'email' => $this->db->escape_string($email),
            'ipaddress' => $ip_address,
            'dateline' => (int)TIME_NOW,
            'data' => $this->db->escape_string(@my_serialize($data)),
        ];

        return (bool)$this->db->insert_query('spamlog', $insert_array);
    }
}
