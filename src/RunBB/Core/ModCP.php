<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class ModCP extends AbstractController
{
    /**
     * Check if the current user has permission to perform a ModCP action on another user
     *
     * @param int $uid The user ID to perform the action on.
     * @return boolean True if the user has necessary permissions
     */
    public function modcp_can_manage_user($uid)
    {
        $user_permissions = $this->user->user_permissions($uid);

        // Current user is only a local moderator or use with ModCP permissions, cannot manage super mods or admins
        if ($this->bb->usergroup['issupermod'] == 0 && ($user_permissions['issupermod'] == 1 || $user_permissions['cancp'] == 1)) {
            return false;
        } // Current user is a super mod or is an administrator
        elseif ($user_permissions['cancp'] == 1 &&
            ($this->bb->usergroup['cancp'] != 1 ||
                ($this->user->is_super_admin($uid) && !$this->user->is_super_admin($this->user->uid)))
        ) {
            return false;
        }
        return true;
    }

    /**
     * Send reported content to moderators
     *
     * @param array $report Array of reported content
     * @param string $report_type Type of content being reported
     * @return bool|array PM Information or false
     */
    public function send_report($report, $report_type = 'post')
    {
        $nummods = false;
        if (!empty($forum['parentlist'])) {
            $query = $this->db->query("
			SELECT DISTINCT u.username, u.email, u.receivepms, u.uid
			FROM " . TABLE_PREFIX . "moderators m
			LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=m.id)
			WHERE m.fid IN (" . $forum['parentlist'] . ") AND m.isgroup = '0'
		");

            $nummods = $this->db->num_rows($query);
        }

        if (!$nummods) {
            unset($query);
            switch ($this->db->type) {
                case "pgsql":
                case "sqlite":
                    $query = $this->db->query("
					SELECT u.username, u.email, u.receivepms, u.uid
					FROM " . TABLE_PREFIX . "users u
					LEFT JOIN " . TABLE_PREFIX . "usergroups g ON (((','|| u.additionalgroups|| ',' LIKE '%,'|| g.gid|| ',%') OR u.usergroup = g.gid))
					WHERE (g.cancp=1 OR g.issupermod=1)
				");
                    break;
                default:
                    $query = $this->db->query("
					SELECT u.username, u.email, u.receivepms, u.uid
					FROM " . TABLE_PREFIX . "users u
					LEFT JOIN " . TABLE_PREFIX . "usergroups g ON (((CONCAT(',', u.additionalgroups, ',') LIKE CONCAT('%,', g.gid, ',%')) OR u.usergroup = g.gid))
					WHERE (g.cancp=1 OR g.issupermod=1)
				");
            }
        }

        $lang_string_subject = "emailsubject_report{$report_type}";
        $lang_string_message = "email_report{$report_type}";

        if (empty($this->bb->lang->$lang_string_subject) || empty($this->bb->lang->$lang_string_message)) {
            return false;
        }

        switch ($report_type) {
            case 'post':
                $send_report_subject = $post['subject'];
                $send_report_url = str_replace('&amp;', '&', get_post_link($post['pid'], $thread['tid']) . "#pid" . $post['pid']);
                break;
            case 'profile':
                $send_report_subject = $user['username'];
                $send_report_url = str_replace('&amp;', '&', get_profile_link($user['uid']));
                break;
            case 'reputation':
                $from_user = $this->user->get_user($reputation['adduid']);
                $send_report_subject = $from_user['username'];
                $send_report_url = $this->bb->settings['bburl'] . "/reputation?uid={$reputation['uid']}#rid{$reputation['rid']}";
                break;
        }

        $emailsubject = $this->bb->lang->sprintf($this->bb->lang->$lang_string_subject, $this->bb->settings['bbname']);
        $emailmessage = $this->bb->lang->sprintf($this->bb->lang->$lang_string_message, $this->user->username, $this->bb->settings['bbname'], $send_report_subject, $this->bb->settings['bburl'], $send_report_url, $report['reason']);

        while ($mod = $this->db->fetch_array($query)) {
            if ($this->bb->settings['reportmethod'] == "pms" && $mod['receivepms'] != 0 && $this->bb->settings['enablepms'] != 0) {
                $pm_recipients[] = $mod['uid'];
            } else {
                $this->mail->send($mod['email'], $emailsubject, $emailmessage);
            }
        }

        if (count($pm_recipients) > 0) {
            $pmhandler = new \RunBB\Handlers\DataHandlers\PMDataHandler($this->bb);

            $pm = [
                "subject" => $emailsubject,
                "message" => $emailmessage,
                "icon" => 0,
                "fromid" => $this->user->uid,
                "toid" => $pm_recipients,
                "ipaddress" => $this->session->ipaddress
            ];

            $pmhandler->admin_override = true;
            $pmhandler->set_data($pm);

            // Now let the pm handler do all the hard work.
            if (!$pmhandler->validate_pm()) {
                // Force it to valid to just get it out of here
                $pmhandler->is_validated = true;
                $pmhandler->errors = [];
            }

            $pminfo = $pmhandler->insert_pm();
            return $pminfo;
        }

        return false;
    }

    /**
     * Add a report
     *
     * @param array $report Array of reported content
     * @param string $type Type of content being reported
     * @return int Report ID
     */
    public function add_report($report, $type = 'post')
    {
        $insert_array = [
            'id' => (int)$report['id'],
            'id2' => (int)$report['id2'],
            'id3' => (int)$report['id3'],
            'uid' => (int)$report['uid'],
            'reportstatus' => 0,
            'reason' => $this->db->escape_string($report['reason']),
            'type' => $this->db->escape_string($type),
            'reports' => 1,
            'dateline' => TIME_NOW,
            'lastreport' => TIME_NOW,
            'reporters' => $this->db->escape_string(my_serialize([$report['uid']]))
        ];

        if ($this->bb->settings['reportmethod'] == "email" || $this->bb->settings['reportmethod'] == "pms") {
            return $this->send_report($report, $type);
        }

        $rid = $this->db->insert_query("reportedcontent", $insert_array);
        $this->bb->cache->update_reportedcontent();

        return $rid;
    }

    /**
     * Update an existing report
     *
     * @param array $report Array of reported content
     * @return bool true
     */
    public function update_report($report)
    {
        $update_array = [
            'reports' => ++$report['reports'],
            'lastreport' => TIME_NOW,
            'reporters' => $this->db->escape_string(my_serialize($report['reporters']))
        ];

        $this->db->update_query("reportedcontent", $update_array, "rid = '{$report['rid']}'");
        return true;
    }
}
