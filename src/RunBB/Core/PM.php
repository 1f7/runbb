<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class PM extends AbstractController
{
    /**
     * Send a Private Message to a user.
     *
     * @param array $pm Array containing: 'subject', 'message', 'touid' and 'receivepms' (the latter should reflect the value found in the users table: receivepms and receivefrombuddy)
     * @param int $fromid Sender UID (0 if you want to use $this->user->uid or -1 to use BB Engine)
     * @param bool $admin_override Whether or not do override user defined options for receiving PMs
     * @return bool True if PM sent
     */
    public function send_pm($pm, $fromid = 0, $admin_override = false)
    {
        if ($this->bb->settings['enablepms'] == 0) {
            return false;
        }

        if (!is_array($pm)) {
            return false;
        }

        if (isset($pm['language'])) {
            if ($pm['language'] != $this->user->language && $this->bb->lang->language_exists($pm['language'])) {
                // Load user language
                $this->bb->lang->set_language($pm['language']);
                $this->bb->lang->load($pm['language_file']);

                $revert = true;
            }

            foreach (['subject', 'message'] as $key) {
                if (is_array($pm[$key])) {
                    $lang_string = $this->bb->lang->{$pm[$key][0]};
                    $num_args = count($pm[$key]);

                    for ($i = 1; $i < $num_args; $i++) {
                        $lang_string = str_replace('{' . $i . '}', $pm[$key][$i], $lang_string);
                    }
                } else {
                    $lang_string = $this->bb->lang->{$pm[$key]};
                }

                $pm[$key] = $lang_string;
            }

            if (isset($revert)) {
                // Revert language
                $this->bb->lang->set_language($this->user->language);
                $this->bb->lang->load($pm['language_file']);
            }
        }

        if (!$pm['subject'] || !$pm['message'] || !$pm['touid'] || (!$pm['receivepms'] && !$admin_override)) {
            return false;
        }

        $pmhandler = new \RunBB\Handlers\DataHandlers\PMDataHandler($this->bb);

        $subject = $pm['subject'];
        $message = $pm['message'];
        $toid = $pm['touid'];

        // Our recipients
        if (is_array($toid)) {
            $recipients_to = $toid;
        } else {
            $recipients_to = [$toid];
        }

        $recipients_bcc = [];

        // Determine user ID
        if ((int)$fromid == 0) {
            $fromid = (int)$this->user->uid;
        } elseif ((int)$fromid < 0) {
            $fromid = 0;
        }

        // Build our final PM array
        $pm = [
            'subject' => $subject,
            'message' => $message,
            'icon' => -1,
            'fromid' => $fromid,
            'toid' => $recipients_to,
            'bccid' => $recipients_bcc,
            'do' => '',
            'pmid' => ''
        ];

        if (isset($this->bb->session)) {
            $pm['ipaddress'] = $this->bb->session->ipaddress;
        }

        $pm['options'] = [
            'signature' => 0,
            'disablesmilies' => 0,
            'savecopy' => 0,
            'readreceipt' => 0
        ];

        $pm['saveasdraft'] = 0;

        // Admin override
        $pmhandler->admin_override = (int)$admin_override;

        $pmhandler->set_data($pm);

        if ($pmhandler->validate_pm()) {
            $pmhandler->insert_pm();
            return true;
        }

        return false;
    }
}
