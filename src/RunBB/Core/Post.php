<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Post extends AbstractController
{
    /**
     * Get the post of a post id.
     *
     * @param int $pid The post id of the post.
     * @return array|bool The database row of the post. False on failure
     */
    public function get_post($pid)
    {
        static $post_cache;

        $pid = (int)$pid;

        if (isset($post_cache[$pid])) {
            return $post_cache[$pid];
        } else {
//            $query = $this->db->simple_select("posts", "*", "pid = '{$pid}'");
//            $post = $this->db->fetch_array($query);
            if ($pid > 0) {
                $post = \RunBB\Models\Post::where('pid', $pid)->first()->toArray();

//            if ($post) {
                $post_cache[$pid] = $post;
                return $post;
            } else {
                $post_cache[$pid] = false;
                return false;
            }
        }
    }

    /**
     * Generate a list of the posticons.
     *
     * @return string The template of posticons.
     */
    public function get_post_icons()
    {
        $icon = '';
        if (isset($this->bb->input['icon'])) {
            $icon = $this->bb->input['icon'];
        }

        $iconlist = $posticons = [];
        $no_icons_checked = ' checked="checked"';
        // read post icons from cache, and sort them accordingly
        $posticons_cache = $this->bb->cache->read('posticons');

        foreach ($posticons_cache as $posticon) {
            $posticons[$posticon['name']] = $posticon;
        }
        krsort($posticons);

        foreach ($posticons as $dbicon) {
            $dbicon['path'] = str_replace("{theme}", $this->bb->theme['imgdir'], $dbicon['path']);
            $dbicon['path'] = htmlspecialchars_uni($this->themes->get_asset_url($dbicon['path']));
            $dbicon['name'] = htmlspecialchars_uni($dbicon['name']);

            if ($icon == $dbicon['iid']) {
                $checked = 'checked="checked"';
                $no_icons_checked = '';
            } else {
                $checked = '';
            }
            $iconlist[] = [
                'dbicon' => $dbicon,
                'checked' => $checked,
            ];
        }
        $posticons = [
            'iconlist' => $iconlist,
            'no_icons_checked' => $no_icons_checked
        ];

        return $posticons;
    }

    /**
     * Build a post bit
     *
     * @param array $post The post data
     * @param int $post_type The type of post bit we're building (1 = preview, 2 = pm, 3 = announcement, else = post)
     * @return string The built post bit
     */
    public function build_postbit($post, $post_type = 0)
    {
        static $altbg;
        $hascustomtitle = 0;

        // Set default values for any fields not provided here
        foreach (['pid', 'aid', 'pmid', 'posturl', 'button_multiquote', 'subject_extra', 'attachments', 'button_rep', 'button_warn', 'button_purgespammer', 'button_pm', 'button_reply_pm', 'button_replyall_pm', 'button_forward_pm', 'button_delete_pm', 'replink', 'warninglevel'] as $post_field) {
            if (empty($post[$post_field])) {
                $post[$post_field] = '';
            }
        }
        if (!$post['pid']) {
            $post['pid'] = 0;
        }
        $unapproved_shade = '';
        if (isset($post['visible']) && $post['visible'] == 0 && $post_type == 0) {
            $altbg = $unapproved_shade = 'unapproved_post';
        } elseif (isset($post['visible']) && $post['visible'] == -1 && $post_type == 0) {
            $altbg = $unapproved_shade = 'unapproved_post deleted_post';
        } elseif (isset($altbg) && $altbg == 'trow1') {
            $altbg = 'trow2';
        } else {
            $altbg = 'trow1';
        }
        $post['unapproved_shade'] = $unapproved_shade;

        $post['fid'] = $this->bb->fid;

        switch ($post_type) {
            case 1: // Message preview
                $parser_options['allow_html'] = $this->bb->forums['allowhtml'];
                $parser_options['allow_mycode'] = $this->bb->forums['allowmycode'];
                $parser_options['allow_smilies'] = $this->bb->forums['allowsmilies'];
                $parser_options['allow_imgcode'] = $this->bb->forums['allowimgcode'];
                $parser_options['allow_videocode'] = $this->bb->forums['allowvideocode'];
                $parser_options['me_username'] = $post['username'];
                $parser_options['filter_badwords'] = 1;
                $id = 0;
                break;
            case 2: // Private message
                $idtype = 'pmid';
                $parser_options['allow_html'] = $this->bb->settings['pmsallowhtml'];
                $parser_options['allow_mycode'] = $this->bb->settings['pmsallowmycode'];
                $parser_options['allow_smilies'] = $this->bb->settings['pmsallowsmilies'];
                $parser_options['allow_imgcode'] = $this->bb->settings['pmsallowimgcode'];
                $parser_options['allow_videocode'] = $this->bb->settings['pmsallowvideocode'];
                $parser_options['me_username'] = $post['username'];
                $parser_options['filter_badwords'] = 1;
                $id = $this->bb->pmid;
                break;
            case 3: // Announcement
                $parser_options['allow_html'] = $this->bb->announcementarray['allowhtml'];
                $parser_options['allow_mycode'] = $this->bb->announcementarray['allowmycode'];
                $parser_options['allow_smilies'] = $this->bb->announcementarray['allowsmilies'];
                $parser_options['allow_imgcode'] = 1;
                $parser_options['allow_videocode'] = 1;
                $parser_options['me_username'] = $post['username'];
                $parser_options['filter_badwords'] = 1;
                $id = $this->bb->announcementarray['aid'];
                break;
            default: // Regular post
                $oldforum = $this->bb->forums;
                $id = (int)$post['pid'];
                $idtype = 'pid';
                $parser_options['allow_html'] = $this->bb->forums['allowhtml'];
                $parser_options['allow_mycode'] = $this->bb->forums['allowmycode'];
                $parser_options['allow_smilies'] = $this->bb->forums['allowsmilies'];
                $parser_options['allow_imgcode'] = $this->bb->forums['allowimgcode'];
                $parser_options['allow_videocode'] = $this->bb->forums['allowvideocode'];
                $parser_options['filter_badwords'] = 1;

                if (!$post['username']) {
                    $post['username'] = $this->lang->guest;
                }

                if ($post['userusername']) {
                    $parser_options['me_username'] = $post['userusername'];
                } else {
                    $parser_options['me_username'] = $post['username'];
                }
                break;
        }

        if (!isset($this->bb->postcounter)) { // Used to show the # of the post
            if (isset($page) && $page > 1) {
                if (!$this->bb->settings['postsperpage'] || (int)$this->bb->settings['postsperpage'] < 1) {
                    $this->bb->settings['postsperpage'] = 20;
                }

                $this->bb->postcounter = $this->bb->settings['postsperpage'] * ($page - 1);
            } else {
                $this->bb->postcounter = 0;
            }
            $post_extra_style = 'border-top-width: 0;';
        } elseif (isset($this->bb->input['mode']) && $this->bb->input['mode'] == 'threaded') {
            $post_extra_style = 'border-top-width: 0;';
        } else {
            $post_extra_style = 'margin-top: 5px;';
        }

        if (!$altbg) { // Define the alternate background colour if this is the first post
            $altbg = 'trow1';
        }
        $this->bb->postcounter++;

        // Format the post date and time using time->formatDate
        $post['postdate'] = $this->time->formatDate('relative', $post['dateline']);

        // Dont want any little 'nasties' in the subject
        $post['subject'] = $this->parser->parse_badwords($post['subject']);

        // Pm's have been htmlspecialchars_uni()'ed already.
        if ($post_type != 2) {
            $post['subject'] = htmlspecialchars_uni($post['subject']);
        }

        if (empty($post['subject'])) {
            $post['subject'] = '&nbsp;';
        }

        $post['author'] = $post['uid'];
        $post['subject_title'] = $post['subject'];

        // Get the usergroup
        if ($post['userusername']) {
            if (!$post['displaygroup']) {
                $post['displaygroup'] = $post['usergroup'];
            }
            $usergroup = $this->bb->groupscache[$post['displaygroup']];
        } else {
            $usergroup = $this->bb->groupscache[1];
        }

        if (!isset($titlescache)) {
            $cached_titles = $this->bb->cache->read('usertitles');
            if (!empty($cached_titles)) {
                foreach ($cached_titles as $usertitle) {
                    $titlescache[$usertitle['posts']] = $usertitle;
                }
            }

            if (is_array($titlescache)) {
                krsort($titlescache);
            }
            unset($usertitle, $cached_titles);
        }

        // Work out the usergroup/title stuff
        $post['groupimage'] = '';
        if (!empty($usergroup['image'])) {
            $language = $this->bb->settings['bblanguage'];
            if (!empty($this->user->language)) {
                $language = $this->user->language;
            }

            $usergroup['image'] = str_replace('{lang}', $language, $usergroup['image']);
            $usergroup['image'] = str_replace('{theme}', $this->bb->theme['imgdir'], $usergroup['image']);
            $post['groupimage'] = "<img src=\"{$usergroup['image']}\" alt=\"{$usergroup['title']}\" title=\"{$usergroup['title']}\" />";

            if ($this->bb->settings['postlayout'] == 'classic') {
                $post['groupimage'] .= '<br />';
            }
        }

        if ($post['userusername']) {
            // This post was made by a registered user
            $post['username'] = $post['userusername'];
            $post['profilelink_plain'] = get_profile_link($post['uid']);
            $post['username_formatted'] = $this->user->format_name($post['username'], $post['usergroup'], $post['displaygroup']);
            $post['profilelink'] = $this->user->build_profile_link($post['username_formatted'], $post['uid']);

            if (trim($post['usertitle']) != '') {
                $hascustomtitle = 1;
            }

            if ($usergroup['usertitle'] != '' && !$hascustomtitle) {
                $post['usertitle'] = $usergroup['usertitle'];
            } elseif (is_array($titlescache) && !$usergroup['usertitle']) {
                reset($titlescache);
                foreach ($titlescache as $key => $titleinfo) {
                    if ($post['postnum'] >= $key) {
                        if (!$hascustomtitle) {
                            $post['usertitle'] = $titleinfo['title'];
                        }
                        $post['stars'] = $titleinfo['stars'];
                        $post['starimage'] = $titleinfo['starimage'];
                        break;
                    }
                }
            }

            $post['usertitle'] = htmlspecialchars_uni($post['usertitle']);

            if ($usergroup['stars']) {
                $post['stars'] = $usergroup['stars'];
            }

            if (empty($post['starimage'])) {
                $post['starimage'] = $usergroup['starimage'];
            }

            if ($post['starimage'] && $post['stars']) {
                // Only display stars if we have an image to use...
                $post['starimage'] = str_replace('{theme}', $this->bb->theme['imgdir'], $post['starimage']);

                $post['userstars'] = '';
                for ($i = 0; $i < $post['stars']; ++$i) {
                    $post['userstars'] .= "<img src=\"{$this->bb->asset_url}/{$post['starimage']}\" border=\"0\" alt=\"*\" />";
                }

                $post['userstars'] .= '<br />';
            }

            $postnum = $post['postnum'];
            $post['postnum'] = $this->parser->formatNumber($post['postnum']);
            $post['threadnum'] = $this->parser->formatNumber($post['threadnum']);

            // Determine the status to show for the user (Online/Offline/Away)
            $timecut = TIME_NOW - $this->bb->settings['wolcutoff'];
            if ($post['lastactive'] > $timecut && ($post['invisible'] != 1 || $this->bb->usergroup['canviewwolinvis'] == 1) && $post['lastvisit'] != $post['lastactive']) {
                $post['onlinestatus'] = "<a href=\"{$this->bb->settings['bburl']}/online\" title=\"{$this->lang->postbit_status_online}\"><img src=\"{$this->bb->theme['imgdir']}/buddy_online.png\" border=\"0\" alt=\"{$this->lang->postbit_status_online}\" class=\"buddy_status\" /></a>";
            } else {
                if ($post['away'] == 1 && $this->bb->settings['allowaway'] != 0) {
                    $post['onlinestatus'] = "<a href=\"{$post['profilelink_plain']}\" title=\"{$this->lang->postbit_status_away}\"><img src=\"{$this->bb->theme['imgdir']}/buddy_away.png\" border=\"0\" alt=\"{$this->lang->postbit_status_away}\" class=\"buddy_status\" /></a>";
                } else {
                    $post['onlinestatus'] = "<img src=\"{$this->bb->theme['imgdir']}/buddy_offline.png\" title=\"{$this->lang->postbit_status_offline}\" alt=\"{$this->lang->postbit_status_offline}\" class=\"buddy_status\" />";
                }
            }

            $post['useravatar'] = '';
            if ($this->user->showavatars != 0 || $this->user->uid > 0) {
                $useravatar = $this->user->format_avatar($post['avatar'], $post['avatardimensions'], $this->bb->settings['postmaxavatarsize']);
                $post['useravatar'] = "<div class=\"author_avatar\"><a href=\"{$post['profilelink_plain']}\"><img src=\"{$useravatar['image']}\" alt=\"\" {$useravatar['width_height']} /></a></div>";
            }

            $post['button_find'] = '';
            if ($this->bb->usergroup['cansearch'] == 1) {
                $post['button_find'] = "<a href=\"{$this->bb->settings['bburl']}/search?action=finduser&uid={$post['uid']}\" title=\"{$this->lang->postbit_find}\" class=\"postbit_find\"><span>{$this->lang->postbit_button_find}</span></a>";
            }

            if ($this->bb->settings['enablepms'] == 1 && $post['receivepms'] != 0 &&
                $this->bb->usergroup['cansendpms'] == 1 &&
                my_strpos("," . $post['ignorelist'] . ",", "," . $this->user->uid . ",") === false
            ) {
                $post['button_pm'] = "<a href=\"{$this->bb->settings['bburl']}/private/send?uid={$post['uid']}\" title=\"{$this->lang->postbit_pm}\" class=\"postbit_pm\"><span>{$this->lang->postbit_button_pm}</span></a>";
            }

            $post['button_rep'] = '';
            if ($post_type != 3 && $this->bb->settings['enablereputation'] == 1 &&
                $this->bb->settings['postrep'] == 1 && $usergroup['cangivereputations'] == 1 &&
                $usergroup['usereputationsystem'] == 1 && ($this->bb->settings['posrep'] ||
                    $this->bb->settings['neurep'] || $this->bb->settings['negrep']) &&
                $post['uid'] != $this->user->uid && (!isset($post['visible']) ||
                    $post['visible'] == 1) &&
                (!isset($this->bb->threads['visible']) ||
                    $this->bb->threads['visible'] == 1)
            ) {
//        if(!$post['pid'])
//        {
//          $post['pid'] = 0;
//        }
                $post['button_rep'] = "<a href=\"javascript:MyBB.reputation({$post['uid']},{$post['pid']});\" title=\"{$this->lang->postbit_reputation_add}\" class=\"postbit_reputation_add\"><span>{$this->lang->postbit_button_reputation_add}</span></a>";
            }

            if ($post['website'] != '' &&
                !$this->user->is_member($this->bb->settings['hidewebsite']) &&
                $usergroup['canchangewebsite'] == 1
            ) {
                $post['website'] = htmlspecialchars_uni($post['website']);
                $post['button_www'] = "<a href=\"{$post['website']}\" target=\"_blank\" title=\"{$this->lang->postbit_website}\" class=\"postbit_website\"><span>{$this->lang->postbit_button_website}</span></a>";
            } else {
                $post['button_www'] = '';
            }

            if ($post['hideemail'] != 1 && $this->bb->usergroup['cansendemail'] == 1) {
                $post['button_email'] = "<a href=\"{$this->bb->settings['bburl']}/emailuser?uid={$post['uid']}\" title=\"{$this->lang->postbit_email}\" class=\"postbit_email\"><span>{$this->lang->postbit_button_email}</span></a>";
            } else {
                $post['button_email'] = '';
            }

            $post['userregdate'] = $this->time->formatDate($this->bb->settings['regdateformat'], $post['regdate']);

            // Work out the reputation this user has (only show if not announcement)
            if ($post_type != 3 && $usergroup['usereputationsystem'] != 0 && $this->bb->settings['enablereputation'] == 1) {
                $post['userreputation'] = $this->user->get_reputation($post['reputation'], $post['uid']);
                $post['replink'] = "<br />{$this->lang->postbit_reputation} {$post['userreputation']}";
            }

            // Showing the warning level? (only show if not announcement)
            if ($post_type != 3 && $this->bb->settings['enablewarningsystem'] != 0 &&
                $usergroup['canreceivewarnings'] != 0 && ($this->bb->usergroup['canwarnusers'] != 0 ||
                    ($this->user->uid == $post['uid'] && $this->bb->settings['canviewownwarning'] != 0))
            ) {
                if ($this->bb->settings['maxwarningpoints'] < 1) {
                    $this->bb->settings['maxwarningpoints'] = 10;
                }

                $warning_level = round($post['warningpoints'] / $this->bb->settings['maxwarningpoints'] * 100);
                if ($warning_level > 100) {
                    $warning_level = 100;
                }
                $warning_level = get_colored_warning_level($warning_level);

                // If we can warn them, it's not the same person, and we're in a PM or a post.
                if ($this->bb->usergroup['canwarnusers'] != 0 && $post['uid'] != $this->user->uid && ($post_type == 0 || $post_type == 2)) {
                    $post['button_warn'] = "<a href=\"{$this->bb->settings['bburl']}/warnings/warn?uid={$post['uid']}&pid={$post['pid']}\" title=\"{$this->lang->postbit_warn}\" class=\"postbit_warn\"><span>{$this->lang->postbit_button_warn}</span></a>";
                    $warning_link = $this->bb->settings['bburl'] . "/warnings?uid={$post['uid']}";
                } else {
                    $post['button_warn'] = '';
                    $warning_link = $this->bb->settings['bburl'] . '/usercp';
                }
                $post['warninglevel'] = "<br />{$this->lang->postbit_warning_level} <a href=\"{$warning_link}\">{$warning_level}</a>";
            }

            if ($post_type != 3 && $post_type != 1 &&
                $this->user->purgespammer_show($post['postnum'], $post['usergroup'], $post['uid'])
            ) {
                $post['button_purgespammer'] = "<a href=\"{$this->bb->settings['bburl']}/moderation/purgespammer?uid={$post['uid']}\" title=\"{$this->lang->postbit_purgespammer}\" class=\"postbit_purgespammer\"><span>{$this->lang->postbit_button_purgespammer}</span></a>";
            }

            // Display profile fields on posts - only if field is filled in
            //if(is_array($profile_fields))
            if (isset($profile_fields)) {
                foreach ($profile_fields as $field) {
                    $fieldfid = "fid{$field['fid']}";
                    if (!empty($post[$fieldfid])) {
                        $post['fieldvalue'] = '';
                        $post['fieldname'] = htmlspecialchars_uni($field['name']);

                        $thing = explode("\n", $field['type'], "2");
                        $type = trim($thing[0]);
                        $useropts = explode("\n", $post[$fieldfid]);

                        if (is_array($useropts) && ($type == "multiselect" || $type == "checkbox")) {
                            foreach ($useropts as $val) {
                                if ($val != '') {
                                    eval("\$post['fieldvalue_option'] .= \"" . $this->templates->get("postbit_profilefield_multiselect_value") . "\";");
                                }
                            }
                            if ($post['fieldvalue_option'] != '') {
                                eval("\$post['fieldvalue'] .= \"" . $this->templates->get("postbit_profilefield_multiselect") . "\";");
                            }
                        } else {
                            $field_parser_options = [
                                'allow_html' => $field['allowhtml'],
                                'allow_mycode' => $field['allowmycode'],
                                'allow_smilies' => $field['allowsmilies'],
                                'allow_imgcode' => $field['allowimgcode'],
                                'allow_videocode' => $field['allowvideocode'],
                                'nofollow_on' => 1,
                                'filter_badwords' => 1
                            ];

                            if ($customfield['type'] == 'textarea') {
                                $field_parser_options['me_username'] = $post['username'];
                            } else {
                                $field_parser_options['nl2br'] = 0;
                            }

                            if (($this->user->showimages != 1 && $this->user->uid != 0) ||
                                ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
                            ) {
                                $field_parser_options['allow_imgcode'] = 0;
                            }

                            $post['fieldvalue'] = $this->parser->parse_message($post[$fieldfid], $field_parser_options);
                        }
                        $post['profilefield'] .= "<br />{$post['fieldname']}: {$post['fieldvalue']}";
                    }
                }
            }
            $pf = isset($post['profilefield']) ? $post['profilefield'] : '';
            $post['user_details'] = "
            {$this->lang->postbit_posts} {$post['postnum']}<br />
            {$this->lang->postbit_threads} {$post['threadnum']}<br />
            {$this->lang->postbit_joined} {$post['userregdate']}
            {$post['replink']}{$pf}{$post['warninglevel']}";
        } else { // Message was posted by a guest or an unknown user
            $post['profilelink'] = $this->user->format_name($post['username'], 1);

            if ($usergroup['usertitle']) {
                $post['usertitle'] = $usergroup['usertitle'];
            } else {
                $post['usertitle'] = $this->lang->guest;
            }

            $post['usertitle'] = htmlspecialchars_uni($post['usertitle']);

            $usergroup['title'] = $this->lang->na;

            $post['userregdate'] = $this->lang->na;
            $post['postnum'] = $this->lang->na;
            $post['button_profile'] = '';
            $post['button_email'] = '';
            $post['button_www'] = '';
            $post['signature'] = '';
            $post['button_pm'] = '';
            $post['button_find'] = '';
            $post['onlinestatus'] = '';
            $post['replink'] = '';
            $post['user_details'] = '&nbsp;';
        }

        $post['button_edit'] = '';
        $post['button_quickdelete'] = '';
        $post['button_quickrestore'] = '';
        $post['button_quote'] = '';
        $post['button_quickquote'] = '';
        $post['button_report'] = '';
        $post['button_reply_pm'] = '';
        $post['button_replyall_pm'] = '';
        $post['button_forward_pm'] = '';
        $post['button_delete_pm'] = '';

        // For private messages, fetch the reply/forward/delete icons
        if ($post_type == 2 && $post['pmid']) {
            $post['button_reply_pm'] = "<a href=\"{$this->bb->settings['bburl']}/private/send?pmid={$id}&do=reply\" title=\"{$this->lang->reply_title}\" class=\"postbit_reply_pm\"><span>{$this->lang->postbit_button_reply_pm}</span></a>";
            $post['button_forward_pm'] = "<a href=\"{$this->bb->settings['bburl']}/private/send?pmid={$id}&do=forward\" title=\"{$this->lang->forward_title}\" class=\"postbit_forward_pm\"><span>{$this->lang->postbit_button_forward}</span></a>";
            $post['button_delete_pm'] = "<a href=\"{$this->bb->settings['bburl']}/private/delete?pmid={$id}&my_post_key={$this->bb->post_code}\" title=\"{$this->lang->delete_title}\" class=\"postbit_delete_pm\"><span>{$this->lang->postbit_button_delete_pm}</span></a>";

            if ($this->bb->replyall == true) {
                $post['button_replyall_pm'] = "<a href=\"{$this->bb->settings['bburl']}/private/send?pmid={$id}&do=replyall\" title=\"{$this->lang->reply_to_all}\" class=\"postbit_reply_all\"><span>{$this->lang->postbit_button_reply_all}</span></a>";
            }
        }

        $post['editedmsg'] = '';
        if (!$post_type) {
            if (empty($this->bb->forumpermissions)) {
                $this->bb->forumpermissions = $this->forum->forum_permissions($this->bb->fid);
            }

            // Figure out if we need to show an "edited by" message
            if ($post['edituid'] != 0 && $post['edittime'] != 0 && $post['editusername'] != '' &&
                (($this->bb->settings['showeditedby'] != 0 && $usergroup['cancp'] == 0) ||
                    ($this->bb->settings['showeditedbyadmin'] != 0 && $usergroup['cancp'] == 1))
            ) {
                $post['editdate'] = $this->time->formatDate('relative', $post['edittime']);
                $post['editnote'] = $this->lang->sprintf($this->lang->postbit_edited, $post['editdate']);
                $post['editedprofilelink'] = $this->user->build_profile_link($post['editusername'], $post['edituid']);
                $editreason = '';
                if ($post['editreason'] != '') {
                    $post['editreason'] = $this->parser->parse_badwords($post['editreason']);
                    $post['editreason'] = htmlspecialchars_uni($post['editreason']);
                    $editreason = "<em>{$this->lang->postbit_editreason}: {$post['editreason']}</em>";
                }
                $post['editedmsg'] = "<span class=\"edited_post\">({$post['editnote']} {$post['editedprofilelink']}.{$editreason})</span>";
            }

            $time = TIME_NOW;
            if (($this->user->is_moderator($this->bb->fid, "caneditposts") ||
                    ((isset($this->bb->forumpermissions['caneditposts']) && $this->bb->forumpermissions['caneditposts'] == 1) &&
                        $this->user->uid == $post['uid'] &&
                        $this->bb->threads['closed'] != 1 &&
                        ($this->bb->usergroup['edittimelimit'] == 0 ||
                            $this->bb->usergroup['edittimelimit'] != 0 &&
                            $post['dateline'] > ($time - ($this->bb->usergroup['edittimelimit'] * 60))))) &&
                $this->user->uid != 0
            ) {
                $post['button_edit'] = "
                <a href=\"{$this->bb->settings['bburl']}/editpost?pid={$post['pid']}\" id=\"edit_post_{$post['pid']}\" title=\"{$this->lang->postbit_edit}\" class=\"postbit_edit\"><span>{$this->lang->postbit_button_edit}</span></a>
                <div id=\"edit_post_{$post['pid']}_popup\" class=\"popup_menu\" style=\"display: none;\"><div class=\"popup_item_container\"><a href=\"javascript:;\" class=\"popup_item quick_edit_button\" id=\"quick_edit_post_{$post['pid']}\">{$this->lang->postbit_quick_edit}</a></div><div class=\"popup_item_container\"><a href=\"{$this->bb->settings['bburl']}/editpost?pid={$post['pid']}\" class=\"popup_item\">{$this->lang->postbit_full_edit}</a></div></div>
                <script type=\"text/javascript\">
                // <!--
                  if(use_xmlhttprequest == \"1\")
                  {
                    $(\"#edit_post_{$post['pid']}\").popupMenu();
                  }
                // -->
                </script>";
            }

            // Quick Delete button
            $can_delete_thread = $can_delete_post = 0;
            if ($this->user->uid == $post['uid'] && $this->bb->threads['closed'] == 0) {
                if ($this->bb->forumpermissions['candeletethreads'] == 1 && $this->bb->postcounter == 1) {
                    $can_delete_thread = 1;
                } elseif ($this->bb->forumpermissions['candeleteposts'] == 1 && $this->bb->postcounter != 1) {
                    $can_delete_post = 1;
                }
            }

            $postbit_qdelete = $postbit_qrestore = '';
            if ($this->user->uid != 0) {
                if (($this->user->is_moderator($this->bb->fid, 'candeleteposts') ||
                        $this->user->is_moderator($this->bb->fid, 'cansoftdeleteposts') ||
                        $can_delete_post == 1) && $this->bb->postcounter != 1
                ) {
                    $postbit_qdelete = $this->lang->postbit_qdelete_post;
                    $display = '';
                    if ($post['visible'] == -1) {
                        $display = 'none';
                    }
                    $post['button_quickdelete'] = "
                  <a href=\"{$this->bb->settings['bburl']}/editpost?pid={$post['pid']}\" onclick=\"Thread.deletePost({$post['pid']}); return false;\" style=\"display: none;\" id=\"quick_delete_{$post['pid']}\" title=\"{$postbit_qdelete}\" class=\"postbit_qdelete\"><span>{$this->lang->postbit_button_qdelete}</span></a>
                  <script type=\"text/javascript\">
                  // <!--
                    $('#quick_delete_{$post['pid']}').css('display', '{$display}');
                  // -->
                  </script>";
                } elseif (($this->user->is_moderator($this->bb->fid, 'candeletethreads') ||
                        $this->user->is_moderator($this->bb->fid, 'cansoftdeletethreads') ||
                        $can_delete_thread == 1) && $this->bb->postcounter == 1
                ) {
                    $postbit_qdelete = $this->lang->postbit_qdelete_thread;
                    $display = '';
                    if ($post['visible'] == -1) {
                        $display = 'none';
                    }
                    $post['button_quickdelete'] = "
                  <a href=\"{$this->bb->settings['bburl']}/editpost?pid={$post['pid']}\" onclick=\"Thread.deletePost({$post['pid']}); return false;\" style=\"display: none;\" id=\"quick_delete_{$post['pid']}\" title=\"{$postbit_qdelete}\" class=\"postbit_qdelete\"><span>{$this->lang->postbit_button_qdelete}</span></a>
                  <script type=\"text/javascript\">
                  // <!--
                    $('#quick_delete_{$post['pid']}').css('display', '{$display}');
                  // -->
                  </script>";
                }

                // Restore Post
                if ($this->user->is_moderator($this->bb->fid, 'canrestoreposts') && $this->bb->postcounter != 1) {
                    $display = 'none';
                    if ($post['visible'] == -1) {
                        $display = '';
                    }
                    $postbit_qrestore = $this->lang->postbit_qrestore_post;
                    $post['button_quickrestore'] = "
                  <a href=\"{$this->bb->settings['bburl']}/editpost?pid={$post['pid']}\" onclick=\"Thread.restorePost({$post['pid']}); return false;\" style=\"display: none;\" id=\"quick_restore_{$post['pid']}\" title=\"{$postbit_qrestore}\" class=\"postbit_qrestore\"><span>{$this->lang->postbit_button_qrestore}</span></a>
                  <script type=\"text/javascript\">
                  // <!--
                    $('#quick_restore_{$post['pid']}').css('display', '{$display}');
                  // -->
                  </script>";
                } // Restore Thread
                elseif ($this->user->is_moderator($this->bb->fid, 'canrestorethreads') && $this->bb->postcounter == 1) {
                    $display = 'none';
                    if ($post['visible'] == -1) {
                        $display = '';
                    }
                    $postbit_qrestore = $this->lang->postbit_qrestore_thread;
                    $post['button_quickrestore'] = "
                  <a href=\"{$this->bb->settings['bburl']}/editpost?pid={$post['pid']}\" onclick=\"Thread.restorePost({$post['pid']}); return false;\" style=\"display: none;\" id=\"quick_restore_{$post['pid']}\" title=\"{$postbit_qrestore}\" class=\"postbit_qrestore\"><span>{$this->lang->postbit_button_qrestore}</span></a>
                  <script type=\"text/javascript\">
                  // <!--
                    $('#quick_restore_{$post['pid']}').css('display', '{$display}');
                  // -->
                  </script>";
                }
            }

            if (!isset($ismod)) {
                $ismod = $this->user->is_moderator($this->bb->fid);
            }

            // Inline moderation stuff
            if ($ismod) {
                if (isset($this->bb->cookies[$this->bb->inlinecookie]) &&
                    my_strpos($this->bb->cookies[$this->bb->inlinecookie], '|' . $post['pid'] . '|')
                ) {
                    $inlinecheck = 'checked="checked"';
                    $this->bb->inlinecount++;
                } else {
                    $inlinecheck = '';
                }
                $post['inlinecheck'] = "<input type=\"checkbox\" class=\"checkbox\" name=\"inlinemod_{$post['pid']}\" id=\"inlinemod_{$post['pid']}\" value=\"1\" style=\"vertical-align: middle; margin: -18px 5px 0 15px;\" {$inlinecheck}  />";

                if ($post['visible'] == 0) {
                    $invisiblepost = 1;
                }
            } else {
                $post['inlinecheck'] = '';
            }
            $post['postlink'] = get_post_link($post['pid'], $post['tid']);
            $post['post_number'] = $this->parser->formatNumber($this->bb->postcounter);

            if ($this->bb->forums['open'] != 0 &&
                ($this->bb->threads['closed'] != 1 ||
                $this->user->is_moderator($this->bb->forums['fid'], "canpostclosedthreads")) &&
                ($this->bb->threads['uid'] == $this->user->uid) &&
                    $this->bb->forumpermissions['canonlyreplyownthreads'] != 1
            ) {
                $post['button_quote'] = "<a href=\"{$this->bb->settings['bburl']}/newreply?tid={$post['tid']}&replyto={$post['pid']}\" title=\"{$this->lang->postbit_quote}\" class=\"postbit_quote\"><span>{$this->lang->postbit_button_quote}</span></a>";
            }

            if ($this->bb->forumpermissions['canpostreplys'] != 0 &&
                ($this->bb->threads['uid'] == $this->user->uid ||
                    $this->bb->forumpermissions['canonlyreplyownthreads'] != 1) &&
                ($this->bb->threads['closed'] != 1 ||
                    $this->user->is_moderator($this->bb->fid, "canpostclosedthreads")) &&
                $this->bb->settings['multiquote'] != 0 && $this->bb->forums['open'] != 0 && !$post_type
            ) {
                $post['button_multiquote'] = "
                <a href=\"javascript:Thread.multiQuote({$post['pid']});\" style=\"display: none;\" id=\"multiquote_link_{$post['pid']}\" title=\"{$this->lang->postbit_multiquote}\" class=\"postbit_multiquote\"><span id=\"multiquote_{$post['pid']}\">{$this->lang->postbit_button_multiquote}</span></a>
                <script type=\"text/javascript\">
                //<!--
                  $('#multiquote_link_{$post['pid']}').css(\"display\", \"\");
                // -->
                </script>";
            }

            if ($this->user->uid > 0) {
                $post['button_report'] = "<a href=\"javascript:Report.reportPost({$post['pid']});\" title=\"{$this->lang->postbit_report}\" class=\"postbit_report\"><span>{$this->lang->postbit_button_report}</span></a>";
            }
        } elseif ($post_type == 3) { // announcement
            if ($this->bb->usergroup['canmodcp'] == 1 &&
                $this->bb->usergroup['canmanageannounce'] == 1 &&
                $this->user->is_moderator($this->bb->fid, "canmanageannouncements")
            ) {
                $post['button_edit'] = "<a href=\"{$this->bb->settings['bburl']}/modcp?action=edit_announcement&amp;aid={$post['aid']}\" title=\"{$this->lang->announcement_edit}\" class=\"postbit_edit\"><span>{$this->lang->postbit_button_edit}</span></a>";
                $post['button_quickdelete'] = "<a href=\"{$this->bb->settings['bburl']}/modcp?action=delete_announcement&amp;aid={$post['aid']}&amp;my_post_key={$this->bb->post_code}\" onclick=\"MyBB.deleteAnnouncement(this); return false;\" class=\"postbit_qdelete\"><span>{$this->lang->postbit_button_qdelete}</span></a>";
            }
        }

        $post['iplogged'] = '';
        $show_ips = $this->bb->settings['logip'];
        $ipaddress = isset($post['ipaddress']) ? $post['ipaddress'] : $this->session->ipaddress;

        // Show post IP addresses... PMs now can have IP addresses too as of 1.8!
        if ($post_type == 2) {
            $show_ips = $this->bb->settings['showpmip'];
        }

        if (!$post_type || $post_type == 2) {
            if ($show_ips != "no" && !empty($post['ipaddress'])) {
                if ($show_ips == "show") {
                    $post['iplogged'] = $this->lang->postbit_ipaddress . ' ' . $ipaddress;
                } elseif ($show_ips == "hide" &&
                    ($this->user->is_moderator($this->bb->fid, "canviewips") ||
                        $this->bb->usergroup['issupermod'])
                ) {
                    $action = 'getip';
                    if ($post_type == 2) {
                        $action = 'getpmip';
                    }
                    $post['iplogged'] = "{$this->lang->postbit_ipaddress} <a href=\"{$this->bb->settings['bburl']}/moderation/{$action}?{$idtype}={$post[$idtype]}\">{$this->lang->postbit_ipaddress_logged}</a>";
                }
            }
        }

        if (isset($post['smilieoff']) && $post['smilieoff'] == 1) {
            $parser_options['allow_smilies'] = 0;
        }

        if (($this->user->showimages != 1 && $this->user->uid != 0) ||
            ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
        ) {
            $parser_options['allow_imgcode'] = 0;
        }

        if (((isset($this->user->showvideos) && $this->user->showvideos != 1) &&
                $this->user->uid != 0) ||
            ($this->bb->settings['guestvideos'] != 1 && $this->user->uid == 0)
        ) {
            $parser_options['allow_videocode'] = 0;
        }

        // If we have incoming search terms to highlight - get it done.
        if (!empty($this->bb->input['highlight'])) {
            $parser_options['highlight'] = $this->bb->input['highlight'];
            $post['subject'] = $this->parser->highlight_message($post['subject'], $parser_options['highlight']);
        }
        $post['message'] = $this->parser->parse_message($post['message'], $parser_options);
        $post['attachments'] = '';
        if ($this->bb->settings['enableattachments'] != 0) {
            $this->get_post_attachments($id, $post);
        }

        if (isset($post['includesig']) && $post['includesig'] != 0 && $post['username'] &&
            $post['signature'] != "" && ($this->user->uid == 0 || $this->user->showsigs != 0)
            && ($post['suspendsignature'] == 0 || $post['suspendsignature'] == 1 &&
                $post['suspendsigtime'] != 0 && $post['suspendsigtime'] < TIME_NOW) && $usergroup['canusesig'] == 1
            && ($usergroup['canusesigxposts'] == 0 || $usergroup['canusesigxposts'] > 0 &&
                $postnum > $usergroup['canusesigxposts']) && !$this->user->is_member($this->bb->settings['hidesignatures'])
        ) {
            $sig_parser = [
                'allow_html' => $this->bb->settings['sightml'],
                'allow_mycode' => $this->bb->settings['sigmycode'],
                'allow_smilies' => $this->bb->settings['sigsmilies'],
                'allow_imgcode' => $this->bb->settings['sigimgcode'],
                'me_username' => $post['username'],
                'filter_badwords' => 1
            ];

            if ($usergroup['signofollow']) {
                $sig_parser['nofollow_on'] = 1;
            }

            if (((isset($this->user->showimages) && $this->user->showimages != 1) &&
                    $this->user->uid != 0) ||
                ($this->bb->settings['guestimages'] != 1 && $this->user->uid == 0)
            ) {
                $sig_parser['allow_imgcode'] = 0;
            }

            $post['signature'] = $this->parser->parse_message($post['signature'], $sig_parser);
            $post['signature'] = "<div class=\"signature scaleimages\">{$post['signature']}</div>";
        } else {
            $post['signature'] = '';
        }

        $icon_cache = $this->bb->cache->read('posticons');

        if (isset($post['icon']) && $post['icon'] > 0 && $icon_cache[$post['icon']]) {
            $icon = $icon_cache[$post['icon']];

            $icon['path'] = htmlspecialchars_uni($icon['path']);
            $icon['path'] = str_replace('{theme}', $this->bb->theme['imgdir'], $icon['path']);
            $icon['name'] = htmlspecialchars_uni($icon['name']);
            $post['icon'] = "<img src=\"{$this->bb->asset_url}/{$icon['path']}\" alt=\"{$icon['name']}\" title=\"{$icon['name']}\" style=\"vertical-align: middle;\" />&nbsp;";
        } else {
            $post['icon'] = '';
        }

        $post_visibility = $ignored_message = '';
        $this->bb->showIgnoreBit = false;
        switch ($post_type) {
            case 1: // Message preview
                $post = $this->bb->plugins->runHooks('postbit_prev', $post);
                break;
            case 2: // Private message
                $post = $this->bb->plugins->runHooks('postbit_pm', $post);
                break;
            case 3: // Announcement
                $post = $this->bb->plugins->runHooks('postbit_announcement', $post);
                break;
            default: // Regular post
                $post = $this->bb->plugins->runHooks('postbit', $post);

                if (!isset($ignored_users)) {
                    $ignored_users = [];
                    if ($this->user->uid > 0 && $this->user->ignorelist != '') {
                        $ignore_list = explode(',', $this->user->ignorelist);
                        foreach ($ignore_list as $uid) {
                            $ignored_users[$uid] = 1;
                        }
                    }
                }

                // Is this author on the ignore list of the current user? Hide this post
                if (is_array($ignored_users) && $post['uid'] != 0 &&
                    isset($ignored_users[$post['uid']]) && $ignored_users[$post['uid']] == 1
                ) {
                    $this->bb->showIgnoreBit = true;
                    $ignored_message = $this->lang->sprintf($this->lang->postbit_currently_ignoring_user, $post['username']);
                    $post_visibility = 'display: none;';
                }
                break;
        }
        $post['ignored_message'] = $ignored_message;
        $post['post_visibility'] = $post_visibility;

        $post['is_classic_tpl'] = '';
        if ($this->bb->settings['postlayout'] == 'classic') {
            $post['is_classic_tpl'] = 'classic';
        }

        $GLOBALS['post'] = '';
        return $this->view->fetch('@forum/Misc/postbit.html.twig', ['post' => $post]);
    }

    /**
     * Fetch the attachments for a specific post and parse inline [attachment=id] code.
     * Note: assumes you have $attachcache, an array of attachments set up.
     *
     * @param int $id The ID of the item.
     * @param array $post The post or item passed by reference.
     */
    public function get_post_attachments($id, &$post)
    {
        $validationcount = 0;
        $tcount = 0;
        $post['attachmentlist'] = $post['thumblist'] = $post['imagelist'] = '';

        if (empty($this->bb->forumpermissions)) {
            $this->bb->forumpermissions = $this->forum->forum_permissions($post['fid']);
        }

        if (isset($this->bb->attachcache[$id]) && is_array($this->bb->attachcache[$id])) { // This post has 1 or more attachments
            foreach ($this->bb->attachcache[$id] as $aid => $attachment) {
                if ($attachment['visible']) { // There is an attachment thats visible!
                    $attachment['filename'] = htmlspecialchars_uni($attachment['filename']);
                    $attachment['filesize'] = $this->parser->friendlySize($attachment['filesize']);
                    $ext = get_extension($attachment['filename']);
                    if ($ext == 'jpeg' || $ext == 'gif' || $ext == 'bmp' || $ext == 'png' || $ext == 'jpg') {
                        $isimage = true;
                    } else {
                        $isimage = false;
                    }
                    $attachment['icon'] = $this->upload->get_attachment_icon($ext);
                    $attachment['downloads'] = $this->parser->formatNumber($attachment['downloads']);

                    if (!$attachment['dateuploaded']) {
                        $attachment['dateuploaded'] = $attachment['dateline'];
                    }
                    $this->bb->attachdate = $this->time->formatDate('relative', $attachment['dateuploaded']);

                    // Support for [attachment=id] code
                    if (stripos($post['message'], '[attachment=' . $attachment['aid'] . ']') != false) {
                        // Show as thumbnail IF image is big && thumbnail exists && setting=='thumb'
                        // Show as full size image IF setting=='fullsize' || (image is small && permissions allow)
                        // Show as download for all other cases
                        if ($attachment['thumbnail'] != 'SMALL' &&
                            $attachment['thumbnail'] != '' && $this->bb->settings['attachthumbnails'] == 'yes'
                        ) {
                            $title = "{$this->lang->postbit_attachment_filename} {$attachment['filename']} {$this->lang->postbit_attachment_size} {$attachment['filesize']} {$this->bb->attachdate}";
                            $attbit = "<a href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" rel=\"lightbox\" title=\"$title\"><img src=\"{$this->bb->settings['bburl']}/attachment?thumbnail={$attachment['aid']}\" class=\"attachment\" alt=\"\" title=\"$title\" /></a>&nbsp;&nbsp;&nbsp;";
                        } elseif ((($attachment['thumbnail'] == "SMALL" &&
                                    (isset($this->bb->forumpermissions['candlattachments']) &&
                                        $this->bb->forumpermissions['candlattachments'] == 1)) ||
                                $this->bb->settings['attachthumbnails'] == "no") && $isimage
                        ) {
                            $attbit = "<img src=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" class=\"attachment\" alt=\"\" title=\"{$this->lang->postbit_attachment_filename} {$attachment['filename']} {$this->lang->postbit_attachment_size} {$attachment['filesize']} {$this->bb->attachdate}\" />&nbsp;&nbsp;&nbsp;";
                            //ev al("\$attbit = \"".$this->templates->get("postbit_attachments_images_image")."\";");
                        } else {
                            $attbit = "<br />{$attachment['icon']}&nbsp;&nbsp;<a href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" target=\"_blank\" title=\"{$this->bb->attachdate}\">{$attachment['filename']}</a> ({$this->lang->postbit_attachment_size} {$attachment['filesize']} / {$this->lang->postbit_attachment_downloads} {$attachment['downloads']})";
                            //ev al("\$attbit = \"".$this->templates->get("postbit_attachments_attachment")."\";");
                        }
                        $post['message'] = preg_replace("#\[attachment=" . $attachment['aid'] . "]#si", $attbit, $post['message']);
                    } else {
                        // Show as thumbnail IF image is big && thumbnail exists && setting=='thumb'
                        // Show as full size image IF setting=='fullsize' || (image is small && permissions allow)
                        // Show as download for all other cases
                        if ($attachment['thumbnail'] != 'SMALL' &&
                            $attachment['thumbnail'] != '' &&
                            $this->bb->settings['attachthumbnails'] == 'yes'
                        ) {
                            $title = "{$this->lang->postbit_attachment_filename} {$attachment['filename']} {$this->lang->postbit_attachment_size} {$attachment['filesize']} {$this->time->formatDate('relative', $attachment['dateuploaded'])}";
                            $post['thumblist'] .= "<a rel=\"lightbox\" href=\"{$this->bb->settings['bburl']}/attachment?aid={$attachment['aid']}\" ttitle=\"$title\"><img src=\"{$this->bb->settings['bburl']}/attachment?thumbnail={$attachment['aid']}\" class=\"attachment\" alt=\"\" title=\"$title\" /></a>&nbsp;&nbsp;&nbsp;";
                            if ($tcount == 5) {
                                $thumblist .= '<br />';
                                $tcount = 0;
                            }
                            ++$tcount;
                        } elseif ((($attachment['thumbnail'] == 'SMALL' && $this->bb->forumpermissions['candlattachments'] == 1) ||
                                $this->bb->settings['attachthumbnails'] == 'no') && $isimage
                        ) {
                            eval("\$post['imagelist'] .= \"" . $this->templates->get("postbit_attachments_images_image") . "\";");
                        } else {
                            eval("\$post['attachmentlist'] .= \"" . $this->templates->get("postbit_attachments_attachment") . "\";");
                        }
                    }
                } else {
                    $validationcount++;
                }
            }
            if ($validationcount > 0 && $this->user->is_moderator($post['fid'], 'canviewunapprove')) {
                if ($validationcount == 1) {
                    $postbit_unapproved_attachments = $this->lang->postbit_unapproved_attachment;
                } else {
                    $postbit_unapproved_attachments = $this->lang->sprintf($this->lang->postbit_unapproved_attachments, $validationcount);
                }
                eval("\$post['attachmentlist'] .= \"" . $this->templates->get("postbit_attachments_attachment_unapproved") . "\";");
            }
            if ($post['thumblist']) {
                $post['attachedthumbs'] = "<span class=\"smalltext\"><strong>{$this->lang->postbit_attachments_thumbnails}</strong></span><br />{$post['thumblist']}<br />";
                //ev al("\$post['attachedthumbs'] = \"".$this->templates->get("postbit_attachments_thumbnails")."\";");
            } else {
                $post['attachedthumbs'] = '';
            }
            if ($post['imagelist']) {
                eval("\$post['attachedimages'] = \"" . $this->templates->get("postbit_attachments_images") . "\";");
            } else {
                $post['attachedimages'] = '';
            }
            if ($post['attachmentlist'] || $post['thumblist'] || $post['imagelist']) {
                $post['attachments'] = "
        <br />
        <br />
        <fieldset>
        <legend><strong>{$this->lang->postbit_attachments}</strong></legend>
        {$post['attachedthumbs']}
        {$post['attachedimages']}
        {$post['attachmentlist']}
        </fieldset>";
                //ev al("\$post['attachments'] = \"".$this->templates->get("postbit_attachments")."\";");
            }
        }
    }

    /**
     * Selectively removes quote tags from a message, depending on its nested depth.  This is to be used with reply with quote functions.
     * For malformed quote tag structures, will try to simulate how MyBB's parser handles the issue, but is slightly inaccurate.
     * Examples, with a cutoff depth of 2:
     *  #1. INPUT:  [quote]a[quote=me]b[quote]c[/quote][/quote][/quote]
     *     OUTPUT:  [quote]a[quote=me]b[/quote][/quote]
     *  #2. INPUT:  [quote=a][quote=b][quote=c][quote=d][/quote][quote=e][/quote][/quote][quote=f][/quote][/quote]
     *     OUTPUT:  [quote=a][quote=b][/quote][quote=f][/quote][/quote]
     *
     * @param string $text the message from which quotes are to be removed
     * @param integer $rmdepth nested depth at which quotes should be removed; if none supplied, will use MyBB's default; must be at least 0
     * @return string the original message passed in $text, but with quote tags selectively removed
     */
    public function remove_message_quotes(&$text, $rmdepth = null)
    {
        if (!$text) {
            return $text;
        }
        if (!isset($rmdepth)) {
            $rmdepth = $this->bb->settings['maxquotedepth'];
        }
        $rmdepth = (int)$rmdepth;

        // find all tokens
        // note, at various places, we use the prefix "s" to denote "start" (ie [quote]) and "e" to denote "end" (ie [/quote])
        preg_match_all("#\[quote(=(?:&quot;|\"|')?.*?(?:&quot;|\"|')?)?\]#si", $text, $smatches, PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);
        preg_match_all("#\[/quote\]#i", $text, $ematches, PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);

        if (empty($smatches) || empty($ematches)) {
            return $text;
        }

        // make things easier by only keeping offsets
        $soffsets = $eoffsets = [];
        foreach ($smatches[0] as $id => $match) {
            $soffsets[] = $match[1];
        }
        // whilst we loop, also remove unnecessary end tokens at the start of string
        if (isset($soffsets[0])) {
            $first_token = $soffsets[0];
            foreach ($ematches[0] as $id => $match) {
                if ($match[1] > $first_token) {
                    $eoffsets[] = $match[1];
                }
            }
        }
        unset($smatches, $ematches);


        // elmininate malformed quotes by parsing like the parser does (preg_replace in a while loop)
        // NOTE: this is slightly inaccurate because the parser considers [quote] and [quote=...] to be different things
        $good_offsets = [];
        while (!empty($soffsets) && !empty($eoffsets)) { // don't rely on this condition - an end offset before the start offset will cause this to loop indefinitely
            $last_offset = 0;
            foreach ($soffsets as $sk => &$soffset) {
                if ($soffset >= $last_offset) {
                    // search for corresponding eoffset
                    foreach ($eoffsets as $ek => &$eoffset) { // use foreach instead of for to get around indexing issues with unset
                        if ($eoffset > $soffset) {
                            // we've found a pair
                            $good_offsets[$soffset] = 1;
                            $good_offsets[$eoffset] = -1;
                            $last_offset = $eoffset;

                            unset($soffsets[$sk], $eoffsets[$ek]);
                            break;
                        }
                    }
                }
            }

            // remove any end offsets occurring before start offsets
            $first_start = reset($soffsets);
            foreach ($eoffsets as $ek => &$eoffset) {
                if ($eoffset < $first_start) {
                    unset($eoffsets[$ek]);
                } else {
                    break;
                }
            }
            // we don't need to remove start offsets after the last end offset, because the loop will deplete something before that
        }

        if (empty($good_offsets)) {
            return $text;
        }
        ksort($good_offsets);


        // we now have a list of all the ordered tokens, ready to go through
        $depth = 0;
        $remove_regions = [];
        $tmp_start = 0;
        foreach ($good_offsets as $offset => $dincr) {
            if ($depth == $rmdepth && $dincr == 1) {
                $tmp_start = $offset;
            }
            $depth += $dincr;
            if ($depth == $rmdepth && $dincr == -1) {
                $remove_regions[] = [$tmp_start, $offset];
            }
        }

        if (empty($remove_regions)) {
            return $text;
        }

        // finally, remove the quotes from the string
        $newtext = '';
        $cpy_start = 0;
        foreach ($remove_regions as &$region) {
            $newtext .= substr($text, $cpy_start, $region[0] - $cpy_start);
            $cpy_start = $region[1] + 8; // 8 = strlen('[/quote]')
            // clean up newlines
            $next_char = $text{$region[1] + 8};
            if ($next_char == "\r" || $next_char == "\n") {
                ++$cpy_start;
                if ($next_char == "\r" && $text{$region[1] + 9} == "\n") {
                    ++$cpy_start;
                }
            }
        }
        // append remaining end text
        if (strlen($text) != $cpy_start) {
            $newtext .= substr($text, $cpy_start);
        }

        // we're done
        return $newtext;
    }

    /**
     * Performs cleanup of a quoted message, such as replacing /me commands, before presenting quoted post to the user.
     *
     * @param array $quoted_post quoted post info, taken from the DB (requires the 'message', 'username', 'pid' and 'dateline' entries to be set; will use 'userusername' if present. requires 'quote_is_pm' if quote message is from a private message)
     * @param boolean $remove_message_quotes whether to call remove_message_quotes() on the quoted message
     * @return string the cleaned up message, wrapped in a quote tag
     */

    public function parse_quoted_message(&$quoted_post, $remove_message_quotes = true)
    {
        // Swap username over if we have a registered user
        if (isset($quoted_post['userusername'])) {
            $quoted_post['username'] = $quoted_post['userusername'];
        }
        // Clean up the message
        $quoted_post['message'] = preg_replace([
            '#(^|\r|\n)/me ([^\r\n<]*)#i',
            '#(^|\r|\n)/slap ([^\r\n<]*)#i',
            '#\[attachment=([0-9]+?)\]#i'
        ], [
            "\\1* {$quoted_post['username']} \\2",
            "\\1* {$quoted_post['username']} {$this->lang->slaps} \\2 {$this->lang->with_trout}",
            "",
        ], $quoted_post['message']);
        $quoted_post['message'] = $this->parser->parse_badwords($quoted_post['message']);

        if ($remove_message_quotes) {
            $max_quote_depth = (int)$this->bb->settings['maxquotedepth'];
            if ($max_quote_depth) {
                // we're wrapping the message in a [quote] tag, so take away one quote depth level
                $quoted_post['message'] = $this->remove_message_quotes($quoted_post['message'], $max_quote_depth - 1);
            }
        }

        $quoted_post = $this->bb->plugins->runHooks("parse_quoted_message", $quoted_post);

        $extra = '';
        if (empty($quoted_post['quote_is_pm'])) {
            $extra = " pid='{$quoted_post['pid']}' dateline='{$quoted_post['dateline']}'";
        }

        return "[quote='{$quoted_post['username']}'{$extra}]\n{$quoted_post['message']}\n[/quote]\n\n";
    }
}
