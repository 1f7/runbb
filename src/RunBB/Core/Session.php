<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

//use Illuminate\Database\Capsule\Manager as DB;

class Session extends AbstractController
{
    /**
     * @var int
     */
    public $sid = 0;
    /**
     * @var int
     */
    public $uid = 0;
    /**
     * @var string
     */
    public $ipaddress = '';
    /**
     * @var string
     */
    public $useragent = '';
    /**
     * @var bool
     */
    public $is_spider = false;

    public $admin_session = [];

    public function init()
    {
        // Get our visitor's IP.
        $this->ipaddress = $this->getIp();
        // Find out the user agent.

        $this->useragent = $this->bb->request->getServerParams()['HTTP_USER_AGENT'];

        // Attempt to find a session id in the cookies.
        if (isset($this->bb->cookies['sid']) && !defined('IN_UPGRADE')) {
            $sid = isset($this->bb->cookies['sid']) ? $this->bb->cookies['sid'] : 0;
            // Load the session
            $sess = \RunBB\Models\Session::where([
                ['sid', $sid],
                ['ip', $this->ipaddress]
            ])->first();

            if (isset($sess->sid)) {
                $this->sid = $sess->sid;
            }
        }

        // If we have a valid session id and user id, load that users session.
        if (!empty($this->bb->cookies['runbbuser'])) {
            $logon = explode('_', $this->bb->cookies['runbbuser'], 2);
            $this->load_user($logon[0], $logon[1]);
        }
        // If no user still, then we have a guest.
        if ($this->user->uid === 0) {
            // Detect if this guest is a search engine spider. (bots don't get a cookied session ID so we first see if that's set)
            if (!$this->sid) {
                $spiders = $this->bb->cache->read('spiders');
                if (is_array($spiders)) {
                    foreach ($spiders as $spider) {
                        if (my_strpos(my_strtolower($this->useragent), my_strtolower($spider['useragent'])) !== false) {
                            $this->load_spider($spider['sid']);
                        }
                    }
                }
            }

            // Still nothing? JUST A GUEST!
            if (!$this->is_spider) {
                $this->load_guest();
            }
        }

        // As a token of our appreciation for getting this far (and they aren't a spider), give the user a cookie
        if ($this->sid && (!isset($this->bb->cookies['sid']) ||
                $this->bb->cookies['sid'] != $this->sid) && $this->is_spider != true
        ) {
            $this->bb->my_setcookie('sid', $this->sid, -1, true);
        }
    }

    /**
     * Updates an administration session data array.
     *
     * @param string $name The name of the item in the data session to update
     * @param mixed $value The value
     */
    public function update_admin_session($name, $value)
    {
        $this->admin_session['data'][$name] = $value;
        \RunBB\Models\Adminsession::where('sid', $this->admin_session['sid'])
            ->update(['data' => @my_serialize($this->admin_session['data'])]);
    }

    /**
     * Saves a "flash message" for the current user to be shown on their next page visit.
     *
     * @param string $message The message to show
     * @param string $type The type of message to be shown (success|error)
     */
    public function flash_message($message, $type = '')
    {
        $this->update_admin_session('flash_message', ['message' => $message, 'type' => $type]);
    }

    /**
     * Load a user via the user credentials.
     *
     * @param int $uid The user id.
     * @param string $loginkey The user's loginkey.
     * @return bool
     */
    private function load_user($uid, $loginkey = '')
    {
        // Read the banned cache
        $bannedcache = $this->bb->cache->read('banned');

        // If the banned cache doesn't exist, update it and re-read it
        if (!is_array($bannedcache)) {
            $this->bb->cache->update_banned();
            $bannedcache = $this->bb->cache->read('banned');
        }

        $uid = (int)$uid;

        $this->user->load($uid);


        if (!empty($bannedcache[$uid])) {
            $banned_user = $bannedcache[$uid];
            $this->user->bandate = $banned_user['dateline'];
            $this->user->banlifted = $banned_user['lifted'];
            $this->user->banoldgroup = $banned_user['oldgroup'];
            $this->user->banolddisplaygroup = $banned_user['olddisplaygroup'];
            $this->user->banoldadditionalgroups = $banned_user['oldadditionalgroups'];
        }

        // Check the password if we're not using a session
        if (empty($loginkey) || $loginkey != $this->user->loginkey || !$this->user->uid) {
            unset($this->user);
            $this->uid = 0;
            return false;
        }
        $this->uid = $this->user->uid;

        // Set the logout key for this user
        $this->user->logoutkey = md5($this->user->loginkey);

        // Sort out the private message count for this user.
        if (($this->user->totalpms == -1 ||
                $this->user->unreadpms == -1) &&
            $this->bb->settings['enablepms'] != 0
        ) { // Forced recount
            $update = 0;
            if ($this->user->totalpms == -1) {
                $update += 1;
            }
            if ($this->user->unreadpms == -1) {
                $update += 2;
            }

            $pmcount = $this->user->update_pm_count('', $update);
            if (is_array($pmcount)) {
                $this->user = array_merge($this->user, $pmcount);
            }
        }
        $this->user->pms_total = $this->user->totalpms;
        $this->user->pms_unread = $this->user->unreadpms;

        if ($this->user->lastip != $this->ipaddress &&
            array_key_exists('lastip', $this->user) &&
            !defined('IN_UPGRADE')) {
            $lastip_add = ", lastip='" . $this->ipaddress."'";
        } else {
            $lastip_add = '';
        }

        // If the last visit was over 900 seconds (session time out) ago then update lastvisit.
        $time = TIME_NOW;
        if ($time - $this->user->lastactive > 900) {
            $this->db->shutdown_query('UPDATE ' . TABLE_PREFIX . "users SET lastvisit='{$this->user->lastactive}', lastactive='$time'{$lastip_add} WHERE uid='{$this->user->uid}'");
            $this->user->lastvisit = $this->user->lastactive;
            $this->user->update_pm_count('', 2);
        } else {
            $timespent = TIME_NOW - $this->user->lastactive;
            $this->db->shutdown_query('UPDATE ' . TABLE_PREFIX . "users SET lastactive='$time', timeonline=timeonline+$timespent{$lastip_add} WHERE uid='{$this->user->uid}'");
        }

        // Sort out the language and forum preferences.
        if ($this->user->language && $this->bb->lang->language_exists($this->user->language)) {
            $this->bb->settings['bblanguage'] = $this->user->language;
        }
        if ($this->user->dateformat != 0 && $this->user->dateformat != '') {
            if ($this->bb->date_formats[$this->user->dateformat]) {
                $this->bb->settings['dateformat'] = $this->bb->date_formats[$this->user->dateformat];
            }
        }

        // Choose time format.
        if ($this->user->timeformat != 0 && $this->user->timeformat != '') {
            if ($this->bb->time_formats[$this->user->timeformat]) {
                $this->bb->settings['timeformat'] = $this->bb->time_formats[$this->user->timeformat];
            }
        }

        // Find out the threads per page preference.
        if ($this->user->tpp) {
            $this->bb->settings['threadsperpage'] = $this->user->tpp;
        }

        // Find out the posts per page preference.
        if ($this->user->ppp) {
            $this->bb->settings['postsperpage'] = $this->user->ppp;
        }

        // Does this user prefer posts in classic mode?
        if ($this->user->classicpostbit) {
            $this->bb->settings['postlayout'] = 'classic';
        } else {
            $this->bb->settings['postlayout'] = 'horizontal';
        }

        // Check if this user is currently banned and if we have to lift it.
        if (!empty($this->user->bandate) &&
            (isset($this->user->banlifted) &&
                !empty($this->user->banlifted)) &&
            $this->user->banlifted < $time
        ) {  // hmmm...bad user... how did you get banned =/
        // must have been good.. bans up :D
            $this->db->shutdown_query('UPDATE ' . TABLE_PREFIX . "users SET usergroup='" . (int)$this->user->banoldgroup . "', additionalgroups='" . $this->user->banoldadditionalgroups . "', displaygroup='" . (int)$this->user->banolddisplaygroup . "' WHERE uid='" . $this->user->uid . "'");
            $this->db->shutdown_query('DELETE FROM ' . TABLE_PREFIX . "banned WHERE uid='" . $this->user->uid . "'");
            // we better do this..otherwise they have dodgy permissions
            $this->user->usergroup = $this->user->banoldgroup;
            $this->user->displaygroup = $this->user->banolddisplaygroup;
            $this->user->additionalgroups = $this->user->banoldadditionalgroups;
            $this->bb->cache->update_banned();

            $mybbgroups = $this->user->usergroup;
            if ($this->user->additionalgroups) {
                $mybbgroups .= ',' . $this->user->additionalgroups;
            }
        } elseif (!empty($this->user->bandate) &&
            (empty($this->user->banlifted) ||
                !empty($this->user->banlifted) && $this->user->banlifted > $time)
        ) {
            $mybbgroups = $this->user->usergroup;
        } else {
            // Gather a full permission set for this user and the groups they are in.
            $mybbgroups = $this->user->usergroup;
            if ($this->user->additionalgroups) {
                $mybbgroups .= ',' . $this->user->additionalgroups;
            }
        }

        $this->bb->usergroup = $this->bb->group->usergroup_permissions($mybbgroups);
        if (!$this->user->displaygroup) {
            $this->user->displaygroup = $this->user->usergroup;
        }

        $mydisplaygroup = $this->bb->group->usergroup_displaygroup($this->user->displaygroup);
        if (is_array($mydisplaygroup)) {
            $this->bb->usergroup = array_merge($this->bb->usergroup, $mydisplaygroup);
        }

        if (!$this->user->usertitle) {
            $this->user->usertitle = $this->bb->usergroup['usertitle'];
        }

        // Update or create the session.
        if (!defined('NO_ONLINE') && !defined('IN_UPGRADE')) {
            if (!empty($this->sid)) {
                $this->update_session($this->sid, $this->user->uid);
            } else {
                $this->create_session($this->user->uid);
            }
        }
        return true;
    }

    /**
     * Load a guest user.
     *
     */
    private function load_guest()
    {
        // Set up some defaults
        $time = TIME_NOW;
        $this->user->usergroup = 1;
        $this->user->username = '';
        $this->user->uid = 0;
        $mybbgroups = 1;
        $this->user->displaygroup = 1;
        $this->user->showimages = 0;
        $this->user->showavatars = 1;
        $this->user->invisible = 0;
        $this->user->showvideos = 0;

        // Has this user visited before? Lastvisit need updating?
        if (isset($this->bb->cookies['mybb']['lastvisit'])) {
            if (!isset($this->bb->cookies['mybb']['lastactive'])) {
                $this->user->lastactive = $time;
                $this->bb->cookies['mybb']['lastactive'] = $this->user->lastactive;
            } else {
                $this->user->lastactive = (int)$this->bb->cookies['mybb']['lastactive'];
            }
            if ($time - $this->bb->cookies['mybb']['lastactive'] > 900) {
                $this->bb->my_setcookie('mybb[lastvisit]', $this->user->lastactive);
                $this->user->lastvisit = $this->user->lastactive;
            } else {
                $this->user->lastvisit = (int)$this->bb->cookies['mybb']['lastactive'];
            }
        } // No last visit cookie, create one.
        else {
            $this->bb->my_setcookie('mybb[lastvisit]', $time);
            $this->user->lastvisit = $time;
        }

        // Update last active cookie.
        $this->bb->my_setcookie('mybb[lastactive]', $time);

        // Gather a full permission set for this guest
        $this->bb->usergroup = $this->bb->group->usergroup_permissions($mybbgroups);
        $mydisplaygroup = $this->bb->group->usergroup_displaygroup($this->user->displaygroup);

        $this->bb->usergroup = array_merge($this->bb->usergroup, $mydisplaygroup);

        // Update the online data.
        if (!defined('NO_ONLINE') && !defined('IN_UPGRADE')) {
            if (!empty($this->sid)) {
                $this->update_session($this->sid);
            } else {
                $this->create_session();
            }
        }
    }

    /**
     * Load a search engine spider.
     *
     * @param int $spider_id The ID of the search engine spider
     */
    private function load_spider($spider_id)
    {
        // Fetch the spider preferences from the database
        $query = $this->db->simple_select('spiders', '*', "sid='{$spider_id}'");
        $spider = $this->db->fetch_array($query);

        // Set up some defaults
        $this->is_spider = true;
        if ($spider['usergroup']) {
            $this->user->usergroup = $spider['usergroup'];
        } else {
            $this->user->usergroup = 1;
        }
        $this->user->username = '';
        $this->user->uid = 0;
        $this->user->displaygroup = $this->user->usergroup;

        // Set spider language
        if ($spider['language'] && $this->bb->lang->language_exists($spider['language'])) {
            $this->bb->settings['bblanguage'] = $spider['language'];
        }

        // Set spider theme
        if ($spider['theme']) {
            $this->user->style = $spider['theme'];
        }

        // Gather a full permission set for this spider.
        $this->bb->usergroup = $this->bb->group->usergroup_permissions($this->user->usergroup);
        $mydisplaygroup = $this->bb->group->usergroup_displaygroup($this->user->displaygroup);
        $this->bb->usergroup = array_merge($this->bb->usergroup, $mydisplaygroup);

        // Update spider last minute (only do so on two minute intervals - decrease load for quick spiders)
        if ($spider['lastvisit'] < TIME_NOW - 120) {
            $updated_spider = [
                'lastvisit' => TIME_NOW
            ];
            $this->db->update_query('spiders', $updated_spider, "sid='{$spider_id}'");
        }

        // Update the online data.
        if (!defined('NO_ONLINE') && !defined('IN_UPGRADE')) {
            $this->sid = 'bot=' . $spider_id;
            $this->create_session();
        }
    }

    /**
     * Update a user session.
     *
     * @param int $sid The session id.
     * @param int $uid The user id.
     */
    private function update_session($sid, $uid = 0)
    {
        // Find out what the special locations are.
        $speciallocs = $this->get_special_locations();
        if ($uid) {
            $onlinedata['uid'] = $uid;
        } else {
            $onlinedata['uid'] = 0;
        }
        $onlinedata['time'] = TIME_NOW;

        $onlinedata['location'] = $this->db->escape_string(substr($this->get_current_location(), 0, 150));
        $onlinedata['useragent'] = $this->db->escape_string(my_substr($this->useragent, 0, 200));

        $onlinedata['location1'] = (int)$speciallocs['1'];
        $onlinedata['location2'] = (int)$speciallocs['2'];
        $onlinedata['nopermission'] = 0;
//        $sid = $this->db->escape_string($sid);
//        $this->db->update_query('sessions', $onlinedata, "sid='{$sid}'");
//        \RunBB\Models\Session::where(['sid' => $sid])->update($onlinedata);
        \RunBB\Models\Session::where('sid', $sid)->update($onlinedata);
    }

    /**
     * Create a new session.
     *
     * @param int $uid The user id to bind the session to.
     */
    private function create_session($uid = 0)
    {
        $speciallocs = $this->get_special_locations();

        // If there is a proper uid, delete by uid.
        if ($uid > 0) {
//            $this->db->delete_query('sessions', "uid='{$uid}'");
            \RunBB\Models\Session::where('uid', $uid)->delete();
            $onlinedata['uid'] = $uid;
        } // Is a spider - delete all other spider references
        elseif ($this->is_spider == true) {
//            $this->db->delete_query('sessions', "sid='{$this->sid}'");
            \RunBB\Models\Session::where('sid', $this->sid)->delete();
        } // Else delete by ip.
        else {
//            $this->db->delete_query('sessions', "ip='" . $this->ipaddress."'");
            \RunBB\Models\Session::where('ip', $this->ipaddress)->delete();
            $onlinedata['uid'] = 0;
        }

        // If the user is a search enginge spider, ...
        if ($this->is_spider == true) {
            $onlinedata['sid'] = $this->sid;
        } else {
            $onlinedata['sid'] = md5(random_str(50));
        }
        $onlinedata['time'] = TIME_NOW;
        $onlinedata['ip'] = $this->ipaddress;

        $onlinedata['location'] = $this->db->escape_string(substr($this->get_current_location(), 0, 150));
        $onlinedata['useragent'] = $this->db->escape_string(my_substr($this->useragent, 0, 200));

        $onlinedata['location1'] = (int)$speciallocs['1'];
        $onlinedata['location2'] = (int)$speciallocs['2'];
        $onlinedata['nopermission'] = 0;
        $this->db->replace_query('sessions', $onlinedata, 'sid', false);
        $this->sid = $onlinedata['sid'];
        $this->uid = $onlinedata['uid'];
    }

    /**
     * Find out the special locations.
     *
     * @return array Special locations array.
     */
    private function get_special_locations()
    {
        $array = ['1' => '', '2' => ''];
        if (preg_match('#forumdisplay#', $this->bb->currentRoute) && $this->bb->getInput('fid', 0) > 0) {
            $array[1] = $this->bb->getInput('fid', 0);
            $array[2] = '';
        } elseif (preg_match('#showthread#', $this->bb->currentRoute)) {
            if ($this->bb->getInput('tid', 0) > 0) {
                $array[2] = $this->bb->getInput('tid', 0);
            } // If there is no tid but a pid, trick the system into thinking there was a tid anyway.
            elseif (isset($this->bb->input['pid']) && !empty($this->bb->input['pid'])) {
                $options = [
                    'limit' => 1
                ];
                $query = $this->db->simple_select('posts', 'tid', 'pid=' . $this->bb->getInput('pid', 0), $options);
                $post = $this->db->fetch_array($query);
                $array[2] = $post['tid'];
            }

            $thread = $this->thread->get_thread($array[2]);
            $array[1] = $thread['fid'];
        }
        return $array;
    }

    /**
     * Get the current location taking in to account different web serves and systems
     *
     * @param boolean $fields True to return as "hidden" fields
     * @param array $ignore Array of fields to ignore if first argument is true
     * @param boolean $quick True to skip all inputs and return only the file path part of the URL
     * @return string The current URL being accessed
     */
    public function get_current_location($fields = false, $ignore = [], $quick = false)
    {
        if (defined('MYBB_LOCATION')) {
            return MYBB_LOCATION;
        }

        $location = $this->request->getUri()->getPath();

        if ($quick) {
            return $location;
        }

        if ($fields == true) {
            if (!is_array($ignore)) {
                $ignore = [$ignore];
            }

            $form_html = '';
            if (!empty($this->input)) {
                foreach ($this->input as $name => $value) {
                    if (in_array($name, $ignore) || is_array($name) || is_array($value)) {
                        continue;
                    }

                    $form_html .= "<input type=\"hidden\" name=\"" . htmlspecialchars_uni($name) . "\" value=\"" . htmlspecialchars_uni($value) . "\" />\n";
                }
            }

            return ['location' => $location, 'form_html' => $form_html, 'form_method' => $this->bb->request_method];
        } else {
            if (isset($_SERVER['QUERY_STRING'])) {
                $location .= '?' . htmlspecialchars_uni($_SERVER['QUERY_STRING']);
            } elseif (isset($_ENV['QUERY_STRING'])) {
                $location .= '?' . htmlspecialchars_uni($_ENV['QUERY_STRING']);
            }

            if ((isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') ||
                (isset($_ENV['REQUEST_METHOD']) && $_ENV['REQUEST_METHOD'] == 'POST')
            ) {
                $post_array = ['action', 'fid', 'pid', 'tid', 'uid', 'eid'];

                foreach ($post_array as $var) {
                    if (isset($_POST[$var])) {
                        $addloc[] = urlencode($var) . '=' . urlencode($_POST[$var]);
                    }
                }

                if (isset($addloc) && is_array($addloc)) {
                    if (strpos($location, '?') === false) {
                        $location .= '?';
                    } else {
                        $location .= '&amp;';
                    }
                    $location .= implode('&amp;', $addloc);
                }
            }

            return $location;
        }
    }

    /**
     * Fetch the IP address of the current user.
     *
     * @return string The IP address.
     */
    public function getIp()
    {
        static $ip = '';

        if ($ip != '') {
            return $ip;
        }

        $ip = strtolower(!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');

        if ($this->bb->settings['ip_forwarded_check']) {
            $addresses = [];

            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $addresses = explode(',', strtolower($_SERVER['HTTP_X_FORWARDED_FOR']));
            } elseif (isset($_SERVER['HTTP_X_REAL_IP'])) {
                $addresses = explode(',', strtolower($_SERVER['HTTP_X_REAL_IP']));
            }

            if (is_array($addresses)) {
                foreach ($addresses as $val) {
                    $val = trim($val);
                    // Validate IP address and exclude private addresses
//                    if (my_inet_ntop(my_inet_pton($val)) == $val && !preg_match("#^(10\.|172\.(1[6-9]|2[0-9]|3[0-1])\.|192\.168\.|fe80:|fe[c-f][0-f]:|f[c-d][0-f]{2}:)#", $val)) {
                    if (!preg_match("#^(10\.|172\.(1[6-9]|2[0-9]|3[0-1])\.|192\.168\.|fe80:|fe[c-f][0-f]:|f[c-d][0-f]{2}:)#", $val)) {
                        $ip = $val;
                        break;
                    }
                }
            }
        }

        if (!$ip) {
            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = strtolower($_SERVER['HTTP_CLIENT_IP']);
            }
        }

        if ($this->plugins) {
            // Used for backwards compatibility on this hook with the updated run_hooks() function.
            $ip_array = ['ip' => &$ip];
            $this->plugins->runHooks('get_ip', $ip_array);
        }

        return $ip;
    }
}
