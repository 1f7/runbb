<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Themes extends AbstractController
{

    /**
     * Get the theme data of a theme id.
     *
     * @param int $tid The theme id of the theme.
     * @return boolean|array False if no valid theme, Array with the theme data otherwise
     */
    public function get_theme($tid)
    {
        if (empty($this->bb->tcache)) {
//            $query = $this->db->simple_select('themes', 'tid, name, pid, allowedgroups', "pid!='0'");
            $t = \RunBB\Models\Theme::where('pid', '!=', '0')
                ->select('tid', 'name', 'pid', 'allowedgroups')
                ->get()
                ->toArray();

//            while ($theme = $this->db->fetch_array($query)) {
            foreach ($t as $theme) {
                $this->bb->tcache[$theme['pid']][$theme['tid']] = $theme;
            }
        }

        $s_theme = false;

        foreach ($this->bb->tcache as $themes) {
            foreach ($themes as $theme) {
                if ($tid == $theme['tid']) {
                    $s_theme = $theme;
                    break 2;
                }
            }
        }

        return $s_theme;
    }

    /**
     * Build a theme selection menu
     *
     * @param string $name The name of the menu
     * @param int $selected The ID of the selected theme
     * @param int $tid The ID of the parent theme to select from
     * @param string $depth The current selection depth
     * @param boolean $usergroup_override Whether or not to override usergroup permissions (true to override)
     * @param boolean $footer Whether or not theme select is in the footer (true if it is)
     * @param boolean $count_override Whether or not to override output based on theme count (true to override)
     * @return string The theme selection list
     */
    public function build_theme_select(
        $name,
        $selected = -1,
        $tid = 0,
        $depth = '',
        $usergroup_override = false,
        $footer = false,
        $count_override = false
    ) {
    
        if ($tid == 0) {
            $tid = 1;
            $num_themes = 0;
            $themeselect_option = '';

            if (!isset($this->lang->use_default)) {
                if (!isset($this->lang->lang_select_default)) {
                    $this->lang->load('member');
                }
                $this->lang->use_default = $this->lang->lang_select_default;
            }
        }

        if (empty($this->bb->tcache)) {
//            $query = $this->db->simple_select('themes', 'tid, name, pid, allowedgroups', "pid!='0'");
            $t = \RunBB\Models\Theme::where('pid', '!=', '0')
                ->select('tid', 'name', 'pid', 'allowedgroups')
                ->get()
                ->toArray();

//            while ($theme = $this->db->fetch_array($query)) {
            foreach ($t as $theme) {
                $this->bb->tcache[$theme['pid']][$theme['tid']] = $theme;
            }
        }

        $themeselect_option = '';
        $num_themes = 0;
        if (is_array($this->bb->tcache[$tid])) {
            // Figure out what groups this user is in
            if (isset($this->user->additionalgroups)) {
                $in_groups = explode(',', $this->user->additionalgroups);
            }
            $in_groups[] = $this->user->usergroup;

            foreach ($this->bb->tcache[$tid] as $theme) {
                $sel = '';
                // Show theme if allowed, or if override is on
                if (!$this->user->is_member($theme['allowedgroups']) ||
                    $theme['allowedgroups'] == "all" ||
                    $usergroup_override == true
                ) {
                    if ($theme['tid'] == $selected) {
                        $sel = ' selected="selected"';
                    }

                    if ($theme['pid'] != 0) {
                        $theme['name'] = htmlspecialchars_uni($theme['name']);
                        $themeselect_option .= "<option value=\"{$theme['tid']}\"{$sel}>{$depth}{$theme['name']}</option>";
                        //ev al("\$themeselect_option .= \"" . $this->templates->get("usercp_themeselector_option") . "\";");
                        ++$num_themes;
                        $depthit = $depth . "--";
                    }

                    if (array_key_exists($theme['tid'], $this->bb->tcache)) {
                        $this->build_theme_select(
                            $name,
                            $selected,
                            $theme['tid'],
                            $depthit,
                            $usergroup_override,
                            $footer,
                            $count_override
                        );
                    }
                }
            }
        }

        if ($tid == 1 && ($num_themes > 1 || $count_override == true)) {
            if ($footer == true) {
                $themeselect = "
                <select name=\"{$name}\" onchange=\"MyBB.changeTheme();\">
                  <optgroup label=\"{$this->lang->select_theme}\">
                  {$themeselect_option}
                  </optgroup>
                </select>";
            } else {
                $themeselect = "
                <select name=\"{$name}\">
                  <option value=\"0\">{$this->lang->use_default}</option>
                  <option value=\"0\">-----------</option>
                  {$themeselect_option}
                </select>";
                //ev al("\$themeselect = \"" . $this->templates->get("usercp_themeselector") . "\";");
            }

            return $themeselect;
        } else {
            return false;
        }
    }

    /**
     * Get the path to an asset using the CDN URL if configured.
     *
     * @param string $path The path to the file.
     * @param bool $use_cdn Whether to use the configured CDN options.
     *
     * @return string The complete URL to the asset.
     */
    public function get_asset_url($path = '', $use_cdn = true)
    {
        $path = (string)$path;
        $path = ltrim($path, '/');

        if (substr($path, 0, 4) != 'http') {
            if (substr($path, 0, 2) == './') {
                $path = substr($path, 2);
            }

            if ($use_cdn && $this->bb->settings['usecdn'] && !empty($this->bb->settings['cdnurl'])) {
                $base_path = rtrim($this->bb->settings['cdnurl'], '/');
            } else {
//                $base_path = rtrim($this->bb->settings['homeurl'], '/') . '/assets/runbb';
                $base_path = '/assets/runbb';
            }
            $url = $base_path;

            if (!empty($path)) {
                $url = $base_path . '/' . $path;
            }
        } else {
            $url = $path;
        }

        return $url;
    }
}
