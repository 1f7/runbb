<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Thread extends AbstractController
{
    public $unread_forums = 0;

    /**
     * Get the thread of a thread id.
     *
     * @param int $tid The thread id of the thread.
     * @param boolean $recache Whether or not to recache the thread.
     * @return array|bool The database row of the thread. False on failure
     */
    public function get_thread($tid, $recache = false)
    {
        static $thread_cache;

        $tid = (int)$tid;
        if (isset($thread_cache[$tid]) && !$recache) {
            return $thread_cache[$tid];
        } else {
//            $query = $this->db->simple_select("threads", "*", "tid = '{$tid}'");
//            $thread = $this->db->fetch_array($query);
            $thread = \RunBB\Models\Thread::where('tid', '=', $tid)
                ->get()->toArray();

            if (isset($thread[0])) {
                $thread_cache[$tid] = $thread[0];
                return $thread[0];
            } else {
                $thread_cache[$tid] = false;
                return false;
            }
        }
    }

    /**
     * Updates the thread counters with a specific value (or addition/subtraction of the previous value)
     *
     * @param int $tid The thread ID
     * @param array $changes Array of items being updated
     *  (replies, unapprovedposts, deletedposts, attachmentcount) and their value (ex, 1, +1, -1)
     */
    function update_thread_counters($tid, $changes = [])
    {
        $update_query = [];
        $tid = (int)$tid;

        $counters = ['replies', 'unapprovedposts', 'attachmentcount', 'deletedposts', 'attachmentcount'];

        // Fetch above counters for this thread
        $query = $this->db->simple_select("threads", implode(",", $counters), "tid='{$tid}'");
        $thread = $this->db->fetch_array($query);

        foreach ($counters as $counter) {
            if (array_key_exists($counter, $changes)) {
                if (substr($changes[$counter], 0, 2) == "+-") {
                    $changes[$counter] = substr($changes[$counter], 1);
                }
                // Adding or subtracting from previous value?
                if (substr($changes[$counter], 0, 1) == "+" || substr($changes[$counter], 0, 1) == "-") {
                    if ((int)$changes[$counter] != 0) {
                        $update_query[$counter] = $thread[$counter] + $changes[$counter];
                    }
                } else {
                    $update_query[$counter] = $changes[$counter];
                }

                // Less than 0? That's bad
                if (isset($update_query[$counter]) && $update_query[$counter] < 0) {
                    $update_query[$counter] = 0;
                }
            }
        }

        $this->db->free_result($query);

        // Only update if we're actually doing something
        if (count($update_query) > 0) {
            $this->db->update_query("threads", $update_query, "tid='{$tid}'");
        }
    }

    /**
     * Update the first post and lastpost data for a specific thread
     *
     * @param int $tid The thread ID
     */
    function update_thread_data($tid)
    {
        $thread = $this->get_thread($tid);

        // If this is a moved thread marker, don't update it - we need it to stay as it is
        if (strpos($thread['closed'], 'moved|') !== false) {
            return;
        }

        $query = $this->db->query("
		SELECT u.uid, u.username, p.username AS postusername, p.dateline
		FROM " . TABLE_PREFIX . "posts p
		LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=p.uid)
		WHERE p.tid='$tid' AND p.visible='1'
		ORDER BY p.dateline DESC
		LIMIT 1");
        $lastpost = $this->db->fetch_array($query);

        $this->db->free_result($query);

        $query = $this->db->query("
		SELECT u.uid, u.username, p.pid, p.username AS postusername, p.dateline
		FROM " . TABLE_PREFIX . "posts p
		LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=p.uid)
		WHERE p.tid='$tid'
		ORDER BY p.dateline ASC
		LIMIT 1
	");
        $firstpost = $this->db->fetch_array($query);

        $this->db->free_result($query);

        if (empty($firstpost['username'])) {
            $firstpost['username'] = $firstpost['postusername'];
        }

        if (empty($lastpost['username'])) {
            $lastpost['username'] = $lastpost['postusername'];
        }

        if (empty($lastpost['dateline'])) {
            $lastpost['username'] = $firstpost['username'];
            $lastpost['uid'] = $firstpost['uid'];
            $lastpost['dateline'] = $firstpost['dateline'];
        }

        $lastpost['username'] = $this->db->escape_string($lastpost['username']);
        $firstpost['username'] = $this->db->escape_string($firstpost['username']);

        $update_array = [
            'firstpost' => (int)$firstpost['pid'],
            'username' => $firstpost['username'],
            'uid' => (int)$firstpost['uid'],
            'dateline' => (int)$firstpost['dateline'],
            'lastpost' => (int)$lastpost['dateline'],
            'lastposter' => $lastpost['username'],
            'lastposteruid' => (int)$lastpost['uid'],
        ];
        $this->db->update_query("threads", $update_array, "tid='{$tid}'");
    }

    /**
     * Updates the first posts in a thread.
     *
     * @param int $tid The thread id for which to update the first post id.
     */
    function update_first_post($tid)
    {
        $query = $this->db->query("
		SELECT u.uid, u.username, p.pid, p.username AS postusername, p.dateline
		FROM " . TABLE_PREFIX . "posts p
		LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=p.uid)
		WHERE p.tid='$tid'
		ORDER BY p.dateline ASC
		LIMIT 1
	");
        $firstpost = $this->db->fetch_array($query);

        if (empty($firstpost['username'])) {
            $firstpost['username'] = $firstpost['postusername'];
        }
        $firstpost['username'] = $this->db->escape_string($firstpost['username']);

        $update_array = [
            'firstpost' => (int)$firstpost['pid'],
            'username' => $firstpost['username'],
            'uid' => (int)$firstpost['uid'],
            'dateline' => (int)$firstpost['dateline']
        ];
        $this->db->update_query("threads", $update_array, "tid='{$tid}'");
    }

    /**
     * Updates the last posts in a thread.
     *
     * @param int $tid The thread id for which to update the last post id.
     */
    function update_last_post($tid)
    {
        $query = $this->db->query("
		SELECT u.uid, u.username, p.username AS postusername, p.dateline
		FROM " . TABLE_PREFIX . "posts p
		LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=p.uid)
		WHERE p.tid='$tid' AND p.visible='1'
		ORDER BY p.dateline DESC
		LIMIT 1");
        $lastpost = $this->db->fetch_array($query);

        if (empty($lastpost['username'])) {
            $lastpost['username'] = $lastpost['postusername'];
        }

        if (empty($lastpost['dateline'])) {
            $query = $this->db->query("
			SELECT u.uid, u.username, p.pid, p.username AS postusername, p.dateline
			FROM " . TABLE_PREFIX . "posts p
			LEFT JOIN " . TABLE_PREFIX . "users u ON (u.uid=p.uid)
			WHERE p.tid='$tid'
			ORDER BY p.dateline ASC
			LIMIT 1
		");
            $firstpost = $this->db->fetch_array($query);

            $lastpost['username'] = $firstpost['username'];
            $lastpost['uid'] = $firstpost['uid'];
            $lastpost['dateline'] = $firstpost['dateline'];
        }

        $lastpost['username'] = $this->db->escape_string($lastpost['username']);

        $update_array = [
            'lastpost' => (int)$lastpost['dateline'],
            'lastposter' => $lastpost['username'],
            'lastposteruid' => (int)$lastpost['uid']
        ];
        $this->db->update_query("threads", $update_array, "tid='{$tid}'");
    }

    /**
     * Deletes a thread from the database
     *
     * @param int $tid The thread ID
     * @return bool
     */
    function delete_thread($tid)
    {
        return $this->moderation->delete_thread($tid);
    }

    /**
     * Builds thread prefixes and returns a selected prefix (or all)
     *
     * @param int $pid The prefix ID (0 to return all)
     * @return array The thread prefix's values (or all thread prefixes)
     */
    function build_prefixes($pid = 0)
    {
        static $prefixes_cache;

        if (is_array($prefixes_cache)) {
            if ($pid > 0 && is_array($prefixes_cache[$pid])) {
                return $prefixes_cache[$pid];
            }

            return $prefixes_cache;
        }

        $prefix_cache = $this->bb->cache->read("threadprefixes");

        if (!is_array($prefix_cache)) {
            // No cache
            $prefix_cache = $this->bb->cache->read("threadprefixes", true);

            if (!is_array($prefix_cache)) {
                return [];
            }
        }

        $prefixes_cache = [];
        foreach ($prefix_cache as $prefix) {
            $prefixes_cache[$prefix['pid']] = $prefix;
        }

        if ($pid != 0 && is_array($prefixes_cache[$pid])) {
            return $prefixes_cache[$pid];
        } elseif (!empty($prefixes_cache)) {
            return $prefixes_cache;
        }

        return false;
    }

    /**
     * Build the thread prefix selection menu for the current user
     *
     * @param int|string $fid The forum ID (integer ID or string all)
     * @param int|string $selected_pid The selected prefix ID (integer ID or string any)
     * @param int $multiple Allow multiple prefix selection
     * @param int $previous_pid The previously selected prefix ID
     * @return string The thread prefix selection menu
     */
    function build_prefix_select($fid, $selected_pid = 0, $multiple = 0, $previous_pid = 0)
    {
        if ($fid != 'all') {
            $fid = (int)$fid;
        }

        $prefix_cache = $this->build_prefixes(0);
        if (empty($prefix_cache)) {
            // We've got no prefixes to show
            return '';
        }

        // Go through each of our prefixes and decide which ones we can use
        $prefixes = [];
        foreach ($prefix_cache as $prefix) {
            if ($fid != "all" && $prefix['forums'] != "-1") {
                // Decide whether this prefix can be used in our forum
                $forums = explode(",", $prefix['forums']);

                if (!in_array($fid, $forums) && $prefix['pid'] != $previous_pid) {
                    // This prefix is not in our forum list
                    continue;
                }
            }

            if ($this->user->is_member($prefix['groups']) || $prefix['pid'] == $previous_pid) {
                // The current user can use this prefix
                $prefixes[$prefix['pid']] = $prefix;
            }
        }

        if (empty($prefixes)) {
            return '';
        }

        $prefixselect = $prefixselect_prefix = '';

        if ($multiple == 1) {
            $any_selected = "";
            if ($selected_pid == 'any') {
                $any_selected = " selected=\"selected\"";
            }
        }

        $default_selected = "";
        if (((int)$selected_pid == 0) && $selected_pid != 'any') {
            $default_selected = " selected=\"selected\"";
        }

        foreach ($prefixes as $prefix) {
            $selected = "";
            if ($prefix['pid'] == $selected_pid) {
                $selected = " selected=\"selected\"";
            }

            $prefix['prefix'] = htmlspecialchars_uni($prefix['prefix']);
            eval("\$prefixselect_prefix .= \"" . $this->bb->templates->get("post_prefixselect_prefix") . "\";");
        }

        if ($multiple != 0) {
            $prefixselect = "
      <select class=\"form-control\" name=\"threadprefix[]\" multiple=\"multiple\" size=\"5\">
        <option value=\"any\"{$any_selected}>{$this->lang->any_prefix}</option>
        <option value=\"0\"{$default_selected}>{$this->lang->no_prefix}</option>
        {$prefixselect_prefix}
      </select>";
            //ev al("\$prefixselect = \"" . $this->bb->templates->get("post_prefixselect_multiple") . "\";");
        } else {
            $prefixselect = "
        <select class=\"form-control\" name=\"threadprefix\">
          <option value=\"0\"{$default_selected}>{$this->lang->no_prefix}</option>
          {$prefixselect_prefix}
        </select>";
            //ev al("\$prefixselect = \"" . $this->bb->templates->get("post_prefixselect_single") . "\";");
        }

        return $prefixselect;
    }

    /**
     * Build the thread prefix selection menu for a forum without group permission checks
     *
     * @param int $fid The forum ID (integer ID)
     * @param int $selected_pid The selected prefix ID (integer ID)
     * @return string The thread prefix selection menu
     */
    function build_forum_prefix_select($fid, $selected_pid = 0)
    {
        $fid = (int)$fid;

        $prefix_cache = $this->build_prefixes(0);
        if (empty($prefix_cache)) {
            // We've got no prefixes to show
            return '';
        }

        // Go through each of our prefixes and decide which ones we can use
        $prefixes = [];
        foreach ($prefix_cache as $prefix) {
            if ($prefix['forums'] != "-1") {
                // Decide whether this prefix can be used in our forum
                $forums = explode(",", $prefix['forums']);

                if (in_array($fid, $forums)) {
                    // This forum can use this prefix!
                    $prefixes[$prefix['pid']] = $prefix;
                }
            } else {
                // This prefix is for anybody to use...
                $prefixes[$prefix['pid']] = $prefix;
            }
        }

        if (empty($prefixes)) {
            return '';
        }

        $default_selected = [
            'all' => '',
            'none' => '',
            'any' => ''
        ];
        $prefixselect_prefix = '';
        $selected_pid = (int)$selected_pid;

        if ($selected_pid == 0) {
            $default_selected['all'] = ' selected="selected"';
        } elseif ($selected_pid == -1) {
            $default_selected['none'] = ' selected="selected"';
        } elseif ($selected_pid == -2) {
            $default_selected['any'] = ' selected="selected"';
        }

        foreach ($prefixes as $prefix) {
            $selected = '';
            if ($prefix['pid'] == $selected_pid) {
                $selected = ' selected="selected"';
            }

            $prefix['prefix'] = htmlspecialchars_uni($prefix['prefix']);
            $prefixselect_prefix .= "<option value=\"{$prefix['pid']}\"{$selected}>{$this->lang->prefix} {$prefix['prefix']}</option>";
            //ev al('$prefixselect_prefix .= "' . $this->bb->templates->get("forumdisplay_threadlist_prefixes_prefix") . '";');
        }
        $prefixselect = "
      <select name=\"prefix\">
        <option value=\"-2\"{$default_selected['any']}>{$this->lang->prefix_any}</option>
        <option value=\"-1\"{$default_selected['none']}>{$this->lang->prefix_none}</option>
        <option value=\"0\"{$default_selected['all']}>{$this->lang->prefix_all}</option>
        {$prefixselect_prefix}
      </select>";
        //ev al('$prefixselect = "' . $this->bb->templates->get("forumdisplay_threadlist_prefixes") . '";');
        return $prefixselect;
    }
}
