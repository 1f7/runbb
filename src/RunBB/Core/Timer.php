<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

class Timer
{
    /**
     * The timer name.
     *
     * @var string
     */
    public $name;

    /**
     * The start time of this timer.
     *
     * @var int
     */
    public $start;

    /**
     * The end time of this timer.
     *
     * @var int
     */
    public $end;

    /**
     * The total time this timer has run.
     *
     * @var int
     */
    public $totaltime;

    /**
     * The formatted total time this timer has run.
     *
     * @var string
     */
    public $formatted;

    /**
     * Constructor of class.
     *
     */
    public function __construct()
    {
        $this->add();
    }

    /**
     * Starts the timer.
     *
     */
    function add()
    {
        if (!$this->start) {
            $this->start = microtime(true);
        }
    }

    /**
     * Gets the time for which the timer has run up until this point.
     *
     * @return string|boolean The formatted time up until now or false when timer is no longer running.
     */
    function getTime()
    {
        if ($this->end) { // timer has been stopped
            return $this->totaltime;
        } elseif ($this->start && !$this->end) { // timer is still going
            $currenttime = microtime(true);
            $totaltime = $currenttime - $this->start;
            return $this->format($totaltime);
        } else {
            return false;
        }
    }

    /**
     * Stops the timer.
     *
     * @return string The formatted total time.
     */
    function stop()
    {
        if ($this->start) {
            $this->end = microtime(true);
            $totaltime = $this->end - $this->start;
            $this->totaltime = $totaltime;
            $this->formatted = $this->format($totaltime);
            return $this->formatted;
        }
        return '';
    }

    /**
     * Removes the timer.
     *
     */
    function remove()
    {
        $this->name = '';
        $this->start = '';
        $this->end = '';
        $this->totaltime = '';
        $this->formatted = '';
    }

    /**
     * Formats the timer time in a pretty way.
     *
     * @param string $string The time string.
     * @return string The formatted time string.
     */
    function format($string)
    {
        return number_format($string, 7);
    }
}
