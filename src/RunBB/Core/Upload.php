<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class Upload extends AbstractController
{
    private $prefix = DIR . 'web/assets/runbb';

    /**
     * Get the attachment icon for a specific file extension
     *
     * @param string $ext The file extension
     * @return string The attachment icon
     */
    public function get_attachment_icon($ext)
    {
//    if (!$attachtypes) {
        $attachtypes = $this->cache->read('attachtypes');
//    }

        $ext = my_strtolower($ext);

        if ($attachtypes[$ext]['icon']) {
            static $attach_icons_schemes = [];
            if (!isset($attach_icons_schemes[$ext])) {
                $attach_icons_schemes[$ext] = parse_url($attachtypes[$ext]['icon']);
                if (!empty($attach_icons_schemes[$ext]['scheme'])) {
                    $attach_icons_schemes[$ext] = $attachtypes[$ext]['icon'];
//        } elseif (defined("IN_ADMINCP")) {
//          $attach_icons_schemes[$ext] = str_replace("{theme}", "", $attachtypes[$ext]['icon']);
//          if (my_substr($attach_icons_schemes[$ext], 0, 1) != "/") {
//            $attach_icons_schemes[$ext] = "../" . $attach_icons_schemes[$ext];
//          }
//        } elseif (defined("IN_PORTAL")) {
//          $attach_icons_schemes[$ext] = $change_dir . '/' . str_replace("{theme}", $this->bb->theme['imgdir'], $attachtypes[$ext]['icon']);
//          $attach_icons_schemes[$ext] = $this->themes->get_asset_url($attach_icons_schemes[$ext]);
//        } else {
                    $attach_icons_schemes[$ext] = str_replace("{theme}", $this->bb->theme['imgdir'], $attachtypes[$ext]['icon']);
                    $attach_icons_schemes[$ext] = $this->themes->get_asset_url($attach_icons_schemes[$ext]);
                }
            }

            $icon = $this->themes->get_asset_url($attach_icons_schemes[$ext]['path']);
            $name = htmlspecialchars_uni($attachtypes[$ext]['name']);
        } else {
//      if (defined("IN_ADMINCP")) {
//        $theme['imgdir'] = "../images";
//      } else if (defined("IN_PORTAL")) {
//        $theme['imgdir'] = "{$change_dir}/images";
            $this->bb->theme['imgdir'] = '/assets/runbb/images';
//      }

            $icon = $this->bb->theme['imgdir'] . '/attachtypes/unknown.png';

            $name = $this->lang->unknown;
        }

        $icon = htmlspecialchars_uni($icon);
        $attachment_icon = "<img src=\"{$icon}\" title=\"{$name}\" border=\"0\" alt=\".{$ext}\" />";

        return $attachment_icon;
    }

    /**
     * Remove an attachment from a specific post
     *
     * @param int $pid The post ID
     * @param string $posthash The posthash if available
     * @param int $aid The attachment ID
     */
    public function remove_attachment($pid, $posthash, $aid)
    {
        $aid = (int)$aid;
        $posthash = $this->db->escape_string($posthash);
        if ($posthash != '') {
            $query = $this->db->simple_select(
                'attachments',
                'aid, attachname, thumbnail, visible',
                "aid='{$aid}' AND posthash='{$posthash}'"
            );
            $attachment = $this->db->fetch_array($query);
        } else {
            $query = $this->db->simple_select(
                'attachments',
                'aid, attachname, thumbnail, visible',
                "aid='{$aid}' AND pid='{$pid}'"
            );
            $attachment = $this->db->fetch_array($query);
        }

        $this->bb->plugins->runHooks('remove_attachment_do_delete', $attachment);

        $this->db->delete_query('attachments', "aid='{$attachment['aid']}'");

//    if(defined('IN_ADMINCP'))
//    {
//      $uploadpath = '../'.$this->bb->settings['uploadspath'];
//    }
//    else
//    {
        $uploadpath = $this->prefix . '/' . $this->bb->settings['uploadspath'];
//    }

        // Check if this attachment is referenced in any other posts. If it isn't, then we are safe to delete the actual file.
        $query = $this->db->simple_select(
            'attachments',
            'COUNT(aid) as numreferences',
            "attachname='" . $this->db->escape_string($attachment['attachname']) . "'"
        );
        if ($this->db->fetch_field($query, 'numreferences') == 0) {
            $this->delete_uploaded_file($uploadpath . '/' . $attachment['attachname']);
            if ($attachment['thumbnail']) {
                $this->delete_uploaded_file($uploadpath . '/' . $attachment['thumbnail']);
            }

            $date_directory = explode('/', $attachment['attachname']);
            if (@is_dir($uploadpath . '/' . $date_directory[0])) {
                $this->delete_upload_directory($uploadpath . '/' . $date_directory[0]);
            }
        }

        if ($attachment['visible'] == 1 && $pid) {
            $post = $this->bb->post->get_post($pid);
            $this->bb->thread->update_thread_counters($post['tid'], ['attachmentcount' => '-1']);
        }
    }

    /**
     * Remove all of the attachments from a specific post
     *
     * @param int $pid The post ID
     * @param string $posthash The posthash if available
     */
    public function remove_attachments($pid, $posthash = '')
    {
        if ($pid) {
            $post = $this->bb->post->get_post($pid);
        }
        $posthash = $this->db->escape_string($posthash);
        if ($posthash != '' && !$pid) {
            $query = $this->db->simple_select('attachments', '*', "posthash='$posthash'");
        } else {
            $query = $this->db->simple_select('attachments', '*', "pid='$pid'");
        }

//    if(defined('IN_ADMINCP'))
//    {
//      $uploadpath = '../'.$this->bb->settings['uploadspath'];
//    }
//    else
//    {
        $uploadpath = $this->prefix . '/' . $this->bb->settings['uploadspath'];
//    }

        $num_attachments = 0;
        while ($attachment = $this->db->fetch_array($query)) {
            if ($attachment['visible'] == 1) {
                $num_attachments++;
            }

            $this->bb->plugins->runHooks('remove_attachments_do_delete', $attachment);

            $this->db->delete_query('attachments', "aid='" . $attachment['aid'] . "'");

            // Check if this attachment is referenced in any other posts. If it isn't, then we are safe to delete the actual file.
            $query2 = $this->db->simple_select(
                'attachments',
                'COUNT(aid) as numreferences',
                "attachname='" . $this->db->escape_string($attachment['attachname']) . "'"
            );
            if ($this->db->fetch_field($query2, 'numreferences') == 0) {
                $this->delete_uploaded_file($uploadpath . '/' . $attachment['attachname']);
                if ($attachment['thumbnail']) {
                    $this->delete_uploaded_file($uploadpath . '/' . $attachment['thumbnail']);
                }

                $date_directory = explode('/', $attachment['attachname']);
                if (@is_dir($uploadpath . '/' . $date_directory[0])) {
                    $this->delete_upload_directory($uploadpath . '/' . $date_directory[0]);
                }
            }
        }

        if ($post['tid']) {
            $this->bb->thread->update_thread_counters($post['tid'], ['attachmentcount' => "-{$num_attachments}"]);
        }
    }

    /**
     * Remove any matching avatars for a specific user ID
     *
     * @param int $uid The user ID
     * @param string $exclude A file name to be excluded from the removal
     */
    public function remove_avatars($uid, $exclude = '')
    {
//    if(defined('IN_ADMINCP'))
//    {
//      $avatarpath = '../'.$this->bb->settings['avataruploadpath'];
//    }
//    else
//    {
        $avatarpath = $this->prefix . '/' . $this->bb->settings['avataruploadpath'];
//    }

        $dir = opendir($avatarpath);
        if ($dir) {
            while ($file = @readdir($dir)) {
                $this->bb->plugins->runHooks('remove_avatars_do_delete', $file);

                if (preg_match('#avatar_' . $uid . '\.#', $file) && is_file($avatarpath . '/' . $file) && $file != $exclude) {
                    $this->delete_uploaded_file($avatarpath . '/' . $file);
                }
            }

            @closedir($dir);
        }
    }

    /**
     * Upload a new avatar in to the file system
     *
     * @param array $avatar Incoming FILE array, if we have one - otherwise takes $_FILES['avatarupload']
     * @param int $uid User ID this avatar is being uploaded for, if not the current user
     * @return array Array of errors if any, otherwise filename of successful.
     */
    public function upload_avatar($avatar = [], $uid = 0)
    {
        $ret = [];

        if (!$uid) {
            $uid = $this->user->uid;
        }

        if (!isset($avatar['name']) || !isset($avatar['tmp_name'])) {
            $avatar = $_FILES['avatarupload'];
        }

        if (!is_uploaded_file($avatar['tmp_name'])) {
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__;
            return $ret;
        }

        // Check we have a valid extension
        $ext = get_extension(my_strtolower($avatar['name']));
        if (!preg_match('#^(gif|jpg|jpeg|jpe|bmp|png)$#i', $ext)) {
            $ret['error'] = $this->bb->lang->error_avatartype;
            return $ret;
        }

//    if(defined('IN_ADMINCP'))
//    {
//      $avatarpath = '../'.$this->bb->settings['avataruploadpath'];
//      $this->bb->lang->load('messages', true);
//    }
//    else
//    {
        $avatarpath = $this->prefix . '/'. $this->bb->settings['avataruploadpath'];
//    }


        $filename = 'avatar_' . $uid . '.' . $ext;
        $file = $this->upload_file($avatar, $avatarpath, $filename);
        if ($file['error']) {
            $this->delete_uploaded_file($avatarpath . '/' . $filename);
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__;
            return $ret;
        }

        // Lets just double check that it exists
        if (!file_exists($avatarpath . '/' . $filename)) {
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__;
            $this->delete_uploaded_file($avatarpath . '/' . $filename);
            return $ret;
        }

        // Check if this is a valid image or not
        $img_dimensions = @getimagesize($avatarpath . '/' . $filename);
        if (!is_array($img_dimensions)) {
            $this->delete_uploaded_file($avatarpath . '/' . $filename);
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__;
            return $ret;
        }

        // Check avatar dimensions
        if ($this->bb->settings['maxavatardims'] != '') {
            list($maxwidth, $maxheight) = @explode('x', $this->bb->settings['maxavatardims']);
            if (($maxwidth && $img_dimensions[0] > $maxwidth) || ($maxheight && $img_dimensions[1] > $maxheight)) {
                // Automatic resizing enabled?
                if ($this->bb->settings['avatarresizing'] == 'auto' ||
                    ($this->bb->settings['avatarresizing'] == 'user' && $this->bb->input['auto_resize'] == 1)
                ) {
                    $img = new \RunBB\Core\Image();
                    $thumbnail = $img->generate_thumbnail($avatarpath . '/' . $filename, $avatarpath, $filename, $maxheight, $maxwidth);
                    if (!$thumbnail['filename']) {
                        $ret['error'] = $this->bb->lang->sprintf($this->bb->lang->error_avatartoobig, $maxwidth, $maxheight);
                        $ret['error'] .= '<br /><br />' . $this->bb->lang->error_avatarresizefailed;
                        $this->delete_uploaded_file($avatarpath . '/' . $filename);
                        return $ret;
                    } else {
                        // Copy scaled image to CDN
                        $this->copy_file_to_cdn($avatarpath . '/' . $thumbnail['filename']);
                        // Reset filesize
                        $avatar['size'] = filesize($avatarpath . '/' . $filename);
                        // Reset dimensions
                        $img_dimensions = @getimagesize($avatarpath . '/' . $filename);
                    }
                } else {
                    $ret['error'] = $this->bb->lang->sprintf($this->bb->lang->error_avatartoobig, $maxwidth, $maxheight);
                    if ($this->bb->settings['avatarresizing'] == 'user') {
                        $ret['error'] .= '<br /><br />' . $this->bb->lang->error_avataruserresize;
                    }
                    $this->delete_uploaded_file($avatarpath . '/' . $filename);
                    return $ret;
                }
            }
        }

        // Next check the file size
        if ($avatar['size'] > ($this->bb->settings['avatarsize'] * 1024) && $this->bb->settings['avatarsize'] > 0) {
            $this->delete_uploaded_file($avatarpath . '/' . $filename);
            $ret['error'] = $this->bb->lang->error_uploadsize;
            return $ret;
        }

        // Check a list of known MIME types to establish what kind of avatar we're uploading
        switch (my_strtolower($avatar['type'])) {
            case 'image/gif':
                $img_type = 1;
                break;
            case 'image/jpeg':
            case 'image/x-jpg':
            case 'image/x-jpeg':
            case 'image/pjpeg':
            case 'image/jpg':
                $img_type = 2;
                break;
            case 'image/png':
            case 'image/x-png':
                $img_type = 3;
                break;
            default:
                $img_type = 0;
        }

        // Check if the uploaded file type matches the correct image type (returned by getimagesize)
        if ($img_dimensions[2] != $img_type || $img_type == 0) {
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__;
            $this->delete_uploaded_file($avatarpath . '/' . $filename);
            return $ret;
        }
        // Everything is okay so lets delete old avatars for this user
        $this->remove_avatars($uid, $filename);

        $ret = [
            'avatar' => $this->bb->settings['avataruploadpath'] . '/' . $filename,
            'width' => (int)$img_dimensions[0],
            'height' => (int)$img_dimensions[1]
        ];
        $ret = $this->bb->plugins->runHooks('upload_avatar_end', $ret);
        return $ret;
    }

    /**
     * Upload an attachment in to the file system
     *
     * @param array $attachment Attachment data (as fed by PHPs $_FILE)
     * @param boolean $update_attachment Whether or not we are updating a current attachment or inserting a new one
     * @return array Array of attachment data if successful, otherwise array of error data
     */
    public function upload_attachment($attachment, $update_attachment = false)
    {
        $posthash = $this->db->escape_string($this->bb->getInput('posthash', ''));
        $pid = (int)$this->bb->pid;

        if (isset($attachment['error']) && $attachment['error'] != 0) {
            $ret['error'] = $this->bb->lang->error_uploadfailed  . __LINE__ . $this->bb->lang->error_uploadfailed_detail;
            switch ($attachment['error']) {
                case 1: // UPLOAD_ERR_INI_SIZE
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_php1;
                    break;
                case 2: // UPLOAD_ERR_FORM_SIZE
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_php2;
                    break;
                case 3: // UPLOAD_ERR_PARTIAL
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_php3;
                    break;
                case 4: // UPLOAD_ERR_NO_FILE
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_php4;
                    break;
                case 6: // UPLOAD_ERR_NO_TMP_DIR
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_php6;
                    break;
                case 7: // UPLOAD_ERR_CANT_WRITE
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_php7;
                    break;
                default:
                    $ret['error'] .= $this->bb->lang->sprintf($this->bb->lang->error_uploadfailed_phpx, $attachment['error']);
                    break;
            }
            return $ret;
        }

        if (!is_uploaded_file($attachment['tmp_name']) || empty($attachment['tmp_name'])) {
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__ . $this->bb->lang->error_uploadfailed_php4;
            return $ret;
        }

        $attachtypes = $this->bb->cache->read('attachtypes');
        $attachment = $this->bb->plugins->runHooks('upload_attachment_start', $attachment);

        $ext = get_extension($attachment['name']);
        // Check if we have a valid extension
        if (!isset($attachtypes[$ext])) {
            $ret['error'] = $this->bb->lang->error_attachtype;
            return $ret;
        } else {
            $attachtype = $attachtypes[$ext];
        }

        // Check the size
        if ($attachment['size'] > $attachtype['maxsize'] * 1024 && $attachtype['maxsize'] != '') {
            $ret['error'] = $this->bb->lang->sprintf($this->bb->lang->error_attachsize, $attachtype['maxsize']);
            return $ret;
        }

        // Double check attachment space usage
        if ($this->bb->usergroup['attachquota'] > 0) {
            $query = $this->db->simple_select('attachments', 'SUM(filesize) AS ausage', "uid='" . $this->user->uid . "'");
            $usage = $this->db->fetch_array($query);
            $usage = $usage['ausage'] + $attachment['size'];
            if ($usage > ($this->bb->usergroup['attachquota'] * 1024)) {
                $friendlyquota = $this->parser->friendlySize($this->bb->usergroup['attachquota'] * 1024);
                $ret['error'] = $this->lang->sprintf($this->bb->lang->error_reachedattachquota, $friendlyquota);
                return $ret;
            }
        }

        // Gather forum permissions
        $forumpermissions = $this->bb->forum->forum_permissions($this->bb->forums['fid']);

        // Check if an attachment with this name is already in the post
        if ($pid != 0) {
            $uploaded_query = "pid='{$pid}'";
        } else {
            $uploaded_query = "posthash='{$posthash}'";
        }
        $query = $this->db->simple_select('attachments', '*', "filename='" . $this->db->escape_string($attachment['name']) . "' AND " . $uploaded_query);
        $prevattach = $this->db->fetch_array($query);
        if ($prevattach['aid'] && $update_attachment == false) {
            if (!$this->bb->usergroup['caneditattachments'] && !$forumpermissions['caneditattachments']) {
                $ret['error'] = $this->bb->lang->error_alreadyuploaded_perm;
                return $ret;
            }

            $ret['error'] = $this->bb->lang->error_alreadyuploaded;
            return $ret;
        }

        // Check to see how many attachments exist for this post already
        if ($this->bb->settings['maxattachments'] > 0 && $update_attachment == false) {
            $query = $this->db->simple_select('attachments', 'COUNT(aid) AS numattachs', $uploaded_query);
            $attachcount = $this->db->fetch_field($query, 'numattachs');
            if ($attachcount >= $this->bb->settings['maxattachments']) {
                $ret['error'] = $this->bb->lang->sprintf($this->bb->lang->error_maxattachpost, $this->bb->settings['maxattachments']);
                return $ret;
            }
        }

        $month_dir = '';
        if (SAFEMODE == false) {
            // Check if the attachment directory (YYYYMM) exists, if not, create it
            $month_dir = gmdate('Ym');
            if (!@is_dir(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $month_dir)) {
                @mkdir(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $month_dir);
                // Still doesn't exist - oh well, throw it in the main directory
                if (!@is_dir(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $month_dir)) {
                    $month_dir = '';
                } else {
                    $index = @fopen(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $month_dir . '/index.html', 'w');
                    @fwrite($index, '<script>history.go(-1);</script>');
                    @fclose($index);
                }
            }
        }
        // All seems to be good, lets move the attachment!
        $filename = 'post_' . $this->user->uid . '_' . TIME_NOW . '_' . md5(random_str()) . '.attach';

        $file = $this->upload_file($attachment, DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $month_dir, $filename);

        // Failed to create the attachment in the monthly directory, just throw it in the main directory
        if (!empty($file['error']) && $month_dir) {
            $file = $this->upload_file($attachment, DIR . 'web' . $this->bb->settings['uploadspath'] . '/', $filename);
        } elseif ($month_dir) {
            $filename = $month_dir . '/' . $filename;
        }

        if (!empty($file['error'])) {
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__ . $this->bb->lang->error_uploadfailed_detail;
            switch ($file['error']) {
                case 1:
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_nothingtomove;
                    break;
                case 2:
                    $ret['error'] .= $this->bb->lang->error_uploadfailed_movefailed;
                    break;
            }
            return $ret;
        }

        // Lets just double check that it exists
        if (!file_exists(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $filename)) {
            $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__ . $this->bb->lang->error_uploadfailed_detail . $this->bb->lang->error_uploadfailed_lost;
            return $ret;
        }

        // Generate the array for the insert_query
        $attacharray = [
            'pid' => $pid,
            'posthash' => $posthash,
            'uid' => $this->user->uid,
            'filename' => $this->db->escape_string($file['original_filename']),
            'filetype' => $this->db->escape_string($file['type']),
            'filesize' => (int)$file['size'],
            'attachname' => $filename,
            'downloads' => 0,
            'dateuploaded' => TIME_NOW
        ];

        // If we're uploading an image, check the MIME type compared to the image type and attempt to generate a thumbnail
        if ($ext == 'gif' || $ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'jpe') {
            // Check a list of known MIME types to establish what kind of image we're uploading
            switch (my_strtolower($file['type'])) {
                case 'image/gif':
                    $img_type = 1;
                    break;
                case 'image/jpeg':
                case 'image/x-jpg':
                case 'image/x-jpeg':
                case 'image/pjpeg':
                case 'image/jpg':
                    $img_type = 2;
                    break;
                case 'image/png':
                case 'image/x-png':
                    $img_type = 3;
                    break;
                default:
                    $img_type = 0;
            }

            $supported_mimes = [];
            foreach ($attachtypes as $attachtype) {
                if (!empty($attachtype['mimetype'])) {
                    $supported_mimes[] = $attachtype['mimetype'];
                }
            }

            // Check if the uploaded file type matches the correct image type (returned by getimagesize)
            $img_dimensions = @getimagesize(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $filename);

            $mime = '';
            $file_path = DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $filename;
            if (function_exists('finfo_open')) {
                $file_info = finfo_open(FILEINFO_MIME);
                list($mime,) = explode(';', finfo_file($file_info, $file_path), 1);
                finfo_close($file_info);
            } elseif (function_exists('mime_content_type')) {
                $mime = mime_content_type(MYBB_ROOT . $file_path);
            }

            if (!is_array($img_dimensions) || ($img_dimensions[2] != $img_type && !in_array($mime, $supported_mimes))) {
                $this->delete_uploaded_file(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $filename);
                $ret['error'] = $this->bb->lang->error_uploadfailed . __LINE__;
                return $ret;
            }

            $thumbname = str_replace('.attach', "_thumb.$ext", $filename);

            $attacharray = $this->bb->plugins->runHooks('upload_attachment_thumb_start', $attacharray);

            $img = new \RunBB\Core\Image();
            $thumbnail = $img->generate_thumbnail(
                DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $filename,
                DIR . 'web' . $this->bb->settings['uploadspath'],
                $thumbname,
                $this->bb->settings['attachthumbh'],
                $this->bb->settings['attachthumbw']
            );

            if (isset($thumbnail['filename'])) {
                $attacharray['thumbnail'] = $thumbnail['filename'];
            } elseif ($thumbnail['code'] == 4) {
                $attacharray['thumbnail'] = 'SMALL';
            }
        }
        if ($forumpermissions['modattachments'] == 1 &&
            !$this->user->is_moderator($this->bb->forums['fid'], 'canapproveunapproveattachs')
        ) {
            $attacharray['visible'] = 0;
        } else {
            $attacharray['visible'] = 1;
        }

        $attacharray = $this->bb->plugins->runHooks('upload_attachment_do_insert', $attacharray);

        if ($prevattach['aid'] && $update_attachment == true) {
            unset($attacharray['downloads']); // Keep our download count if we're updating an attachment
            $this->db->update_query('attachments', $attacharray, "aid='" . $this->db->escape_string($prevattach['aid']) . "'");

            // Remove old attachment file
            // Check if this attachment is referenced in any other posts. If it isn't, then we are safe to delete the actual file.
            $query = $this->db->simple_select(
                'attachments',
                'COUNT(aid) as numreferences',
                "attachname='" . $this->db->escape_string($prevattach['attachname']) . "'"
            );
            if ($this->db->fetch_field($query, 'numreferences') == 0) {
                $this->delete_uploaded_file(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $prevattach['attachname']);
                if ($prevattach['thumbnail']) {
                    $this->delete_uploaded_file(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $prevattach['thumbnail']);
                }

                $date_directory = explode('/', $prevattach['attachname']);
                if (@is_dir($this->bb->settings['uploadspath'] . '/' . $date_directory[0])) {
                    $this->delete_upload_directory(DIR . 'web' . $this->bb->settings['uploadspath'] . '/' . $date_directory[0]);
                }
            }

            $aid = $prevattach['aid'];
        } else {
            $aid = $this->db->insert_query('attachments', $attacharray);
            if ($pid) {
                $this->bb->thread->update_thread_counters($this->bb->tid, ['attachmentcount' => '+1']);
            }
        }
        $ret['aid'] = $aid;
        return $ret;
    }

    /**
     * Delete an uploaded file both from the relative path and the CDN path if a CDN is in use.
     *
     * @param string $path The relative path to the uploaded file.
     *
     * @return bool Whether the file was deleted successfully.
     */
    private function delete_uploaded_file($path = '')
    {
        $deleted = false;

        $deleted = @unlink($path);

        $cdn_base_path = rtrim($this->bb->settings['cdnpath'], '/');
        $path = ltrim($path, '/');
        $cdn_path = realpath($cdn_base_path . '/' . $path);

        if ($this->bb->settings['usecdn'] && !empty($cdn_base_path)) {
            $deleted = $deleted && @unlink($cdn_path);
        }

        $hook_params = [
            'path' => &$path,
            'deleted' => &$deleted,
        ];

        $this->bb->plugins->runHooks('delete_uploaded_file', $hook_params);

        return $deleted;
    }

    /**
     * Delete an upload directory on both the local filesystem and the CDN filesystem.
     *
     * @param string $path The directory to delete.
     *
     * @return bool Whether the directory was deleted.
     */
    private function delete_upload_directory($path = '')
    {
        $deleted = false;

        $deleted = @rmdir($path);

        $cdn_base_path = rtrim($this->bb->settings['cdnpath'], '/');
        $path = ltrim($path, '/');
        $cdn_path = rtrim(realpath($cdn_base_path . '/' . $path), '/');

        if ($this->bb->settings['usecdn'] && !empty($cdn_base_path)) {
            $deleted = $deleted && @rmdir($cdn_path);
        }

        $hook_params = [
            'path' => &$path,
            'deleted' => &$deleted,
        ];

        $this->bb->plugins->runHooks('delete_upload_directory', $hook_params);

        return $deleted;
    }

    /**
     * Actually move a file to the uploads directory
     *
     * @param array $file The PHP $_FILE array for the file
     * @param string $path The path to save the file in
     * @param string $filename The filename for the file (if blank, current is used)
     * @return array The uploaded file
     */
    private function upload_file($file, $path, $filename = '')
    {
        $upload = [];

        if (empty($file['name']) || $file['name'] == 'none' || $file['size'] < 1) {
            $upload['error'] = 1;
            return $upload;
        }

        if (!$filename) {
            $filename = $file['name'];
        }

        $upload['original_filename'] = preg_replace('#/$#', '', $file['name']); // Make the filename safe
        $filename = preg_replace('#/$#', '', $filename); // Make the filename safe

        $moved = move_uploaded_file($file['tmp_name'], $path . '/' . $filename);

        $cdn_path = '';

        $moved_cdn = $this->copy_file_to_cdn($path . '/' . $filename, $cdn_path);

        if (!$moved) {
            $upload['error'] = 2;
            return $upload;
        }
        @my_chmod($path . '/' . $filename, '0644');
        $upload['filename'] = $filename;
        $upload['path'] = $path;
        $upload['type'] = $file['type'];
        $upload['size'] = $file['size'];
        $upload = $this->bb->plugins->runHooks('upload_file_end', $upload);

        if ($moved_cdn) {
            $upload['cdn_path'] = $cdn_path;
        }

        return $upload;
    }

    /**
     * Copy a file to the CDN.
     *
     * @param string $file_path The path to the file to upload to the CDN.
     *
     * @param string $uploaded_path The path the file was uploaded to, reference parameter for when this may be needed.
     *
     * @return bool Whether the file was copied successfully.
     */
    public function copy_file_to_cdn($file_path = '', &$uploaded_path = null)
    {
        $success = false;

        $file_path = (string)$file_path;

        $real_file_path = realpath($file_path);

        $file_dir_path = dirname($real_file_path);
        $file_dir_path = str_replace(MYBB_ROOT, '', $file_dir_path);
        $file_dir_path = ltrim($file_dir_path, './\\');

        $file_name = basename($real_file_path);

        if (file_exists($file_path)) {
            if ($this->bb->settings['usecdn'] && !empty($this->bb->settings['cdnpath'])) {
                $cdn_path = rtrim($this->bb->settings['cdnpath'], '/\\');

                if (substr($file_dir_path, 0, my_strlen(MYBB_ROOT)) == MYBB_ROOT) {
                    $file_dir_path = str_replace(MYBB_ROOT, '', $file_dir_path);
                }

                $cdn_upload_path = $cdn_path . DIRECTORY_SEPARATOR . $file_dir_path;

                if (!($dir_exists = is_dir($cdn_upload_path))) {
                    $dir_exists = @mkdir($cdn_upload_path, 0777, true);
                }

                if ($dir_exists) {
                    if (($cdn_upload_path = realpath($cdn_upload_path)) !== false) {
                        $success = @copy($file_path, $cdn_upload_path . DIRECTORY_SEPARATOR . $file_name);

                        if ($success) {
                            $uploaded_path = $cdn_upload_path;
                        }
                    }
                }
            }

            if (is_object($this->bb->plugins)) {
                $hook_args = [
                    'file_path' => &$file_path,
                    'real_file_path' => &$real_file_path,
                    'file_name' => &$file_name,
                    'uploaded_path' => &$uploaded_path,
                    'success' => &$success,
                ];

                $this->bb->plugins->runHooks('copy_file_to_cdn_end', $hook_args);
            }
        }

        return $success;
    }
}
