<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Core;

use RunCMF\Core\AbstractController;

class User extends AbstractController
{
    public $uid = 0;
    public $additionalgroups = '';

    public function load($uid = 0, $user_array = [])
    {
        $u = [];
        if ($uid > 0) {
            $u = \RunBB\Models\User::where('uid', '=', $uid)
                ->leftJoin('userfields', 'userfields.ufid', '=', 'users.uid')
                ->first()
                ->toArray();
        } elseif (!empty($user_array)) {
            $u = & $user_array;
        }
        if (!empty($u)) {
            foreach ($u as $key => $val) {
                $this->user->$key = $val;
            }
        }
    }
    /**
     * Checks if a user with uid $uid exists in the database.
     *
     * @param int $uid The uid to check for.
     * @return boolean True when exists, false when not.
     */
    public function user_exists($uid)
    {
        $query = $this->db->simple_select('users', 'COUNT(*) as user', "uid='" . (int)$uid . "'", ['limit' => 1]);
        if ($this->db->fetch_field($query, 'user') == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the age of a user with specified birthday.
     *
     * @param string $birthday The birthday of a user.
     * @return int The age of a user with that birthday.
     */
    public function get_age($birthday)
    {
        $bday = explode('-', $birthday);
        if (!$bday[2]) {
            return;
        }

        list($day, $month, $year) = explode('-', $this->time->formatDate('j-n-Y', TIME_NOW, 0, 0));

        $age = $year - $bday[2];

        if (($month == $bday[1] && $day < $bday[0]) || $month < $bday[1]) {
            --$age;
        }
        return $age;
    }

    /**
     * Checks if $username already exists in the database.
     *
     * @param string $username The username for check for.
     * @return boolean True when exists, false when not.
     */
    public function username_exists($username)
    {
        $options = ['username_method' => 2];

        return (bool)$this->get_user_by_username($username, $options);
    }

    /**
     * Checks a password with a supplied username.
     *
     * @param string $username The username of the user.
     * @param string $password The plain-text password.
     * @return boolean|array False when no match, array with user info when match.
     */
    public function validate_password_from_username($username, $password)
    {
        $options = [
            'fields' => ['username', 'password', 'salt', 'loginkey', 'coppauser', 'usergroup'],
            'username_method' => $this->bb->settings['username_method'],
        ];

        $user = $this->get_user_by_username($username, $options);

        if (!$user['uid']) {
            return false;
        }

        return $this->validate_password_from_uid($user['uid'], $password, $user);
    }

    /**
     * Checks a password with a supplied uid.
     *
     * @param int $uid The user id.
     * @param string $password The plain-text password.
     * @param array $user An optional user data array.
     * @return boolean|array False when not valid, user data array when valid.
     */
    public function validate_password_from_uid($uid, $password, $user = false)
    {
        if (isset($this->user->uid) && $this->user->uid == $uid) {
            $user = $this->user;
        }
        if (!$user->password) {
            $query = $this->db->simple_select('users', 'uid,username,password,salt,loginkey,usergroup', "uid='" . (int)$uid . "'");
            $user = $this->db->fetch_array($query);
        }
        if (!$user->salt) {
            // Generate a salt for this user and assume the password stored in db is a plain md5 password
            $user->salt = $this->generate_salt();
            $user->password = $this->salt_password($user->password, $user->salt);
            $sql_array = [
                'salt' => $user->salt,
                'password' => $user->password
            ];
            $this->db->update_query('users', $sql_array, "uid='" . $user->uid . "'");
        }

        if (!$user->loginkey) {
            $user->loginkey = $this->generate_loginkey();
            $sql_array = [
                'loginkey' => $user->loginkey
            ];
            $this->db->update_query('users', $sql_array, 'uid = ' . $user->uid);
        }
        if ($this->salt_password(md5($password), $user->salt) === $user->password) {
            return $user;
        } else {
            return false;
        }
    }

    /**
     * Updates a user's password.
     *
     * @param int $uid The user's id.
     * @param string $password The md5()'ed password.
     * @param string $salt (Optional) The salt of the user.
     * @return array The new password.
     */
    public function update_password($uid, $password, $salt = '')
    {
        $newpassword = [];

        // If no salt was specified, check in database first, if still doesn't exist, create one
        if (!$salt) {
            $query = $this->db->simple_select('users', 'salt', "uid='$uid'");
            $user = $this->db->fetch_array($query);
            if ($user['salt']) {
                $salt = $user['salt'];
            } else {
                $salt = $this->generate_salt();
            }
            $newpassword['salt'] = $salt;
        }

        // Create new password based on salt
        $saltedpw = $this->salt_password($password, $salt);

        // Generate new login key
        $loginkey = $this->generate_loginkey();

        // Update password and login key in database
        $newpassword['password'] = $saltedpw;
        $newpassword['loginkey'] = $loginkey;
        $this->db->update_query('users', $newpassword, "uid='$uid'");

        $this->bb->plugins->runHooks('$this->user->password_changed');

        return $newpassword;
    }

    /**
     * Salts a password based on a supplied salt.
     *
     * @param string $password The md5()'ed password.
     * @param string $salt The salt.
     * @return string The password hash.
     */
    public function salt_password($password, $salt)
    {
        return md5(md5($salt) . $password);
    }

    /**
     * Generates a random salt
     *
     * @return string The salt.
     */
    public function generate_salt()
    {
        return random_str(8);
    }

    /**
     * Generates a 50 character random login key.
     *
     * @return string The login key.
     */
    public function generate_loginkey()
    {
        return random_str(50);
    }

    /**
     * Updates a user's salt in the database (does not update a password).
     *
     * @param int $uid The uid of the user to update.
     * @return string The new salt.
     */
    public function update_salt($uid)
    {
        $salt = $this->generate_salt();
        $sql_array = [
            'salt' => $salt
        ];
        $this->db->update_query('users', $sql_array, "uid='{$uid}'");

        return $salt;
    }

    /**
     * Generates a new login key for a user.
     *
     * @param int $uid The uid of the user to update.
     * @return string The new login key.
     */
    public function update_loginkey($uid)
    {
        $loginkey = $this->generate_loginkey();
        $sql_array = [
            'loginkey' => $loginkey
        ];
        $this->db->update_query('users', $sql_array, "uid='{$uid}'");

        return $loginkey;
    }

    /**
     * Adds a thread to a user's thread subscription list.
     * If no uid is supplied, the currently logged in user's id will be used.
     *
     * @param int $tid The tid of the thread to add to the list.
     * @param int $notification (Optional) The type of notification to receive for replies (0=none, 1=email, 2=pm)
     * @param int $uid (Optional) The uid of the user who's list to update.
     * @return boolean True when success, false when otherwise.
     */
    public function add_subscribed_thread($tid, $notification = 1, $uid = 0)
    {
        if (!$uid) {
            $uid = $this->user->uid;
        }

        if (!$uid) {
            return false;
        }

        $query = $this->db->simple_select('threadsubscriptions', '*', "tid='" . (int)$tid . "' AND uid='" . (int)$uid . "'");
        $subscription = $this->db->fetch_array($query);
        if (!$subscription['tid']) {
            $insert_array = [
                'uid' => (int)$uid,
                'tid' => (int)$tid,
                'notification' => (int)$notification,
                'dateline' => TIME_NOW
            ];
            $this->db->insert_query('threadsubscriptions', $insert_array);
        } else {
            // Subscription exists - simply update notification
            $update_array = [
                'notification' => (int)$notification
            ];
            $this->db->update_query('threadsubscriptions', $update_array, "uid='{$uid}' AND tid='{$tid}'");
        }
        return true;
    }

    /**
     * Remove a thread from a user's thread subscription list.
     * If no uid is supplied, the currently logged in user's id will be used.
     *
     * @param int $tid The tid of the thread to remove from the list.
     * @param int $uid (Optional) The uid of the user who's list to update.
     * @return boolean True when success, false when otherwise.
     */
    public function remove_subscribed_thread($tid, $uid = 0)
    {
        if (!$uid) {
            $uid = $this->user->uid;
        }

        if (!$uid) {
            return false;
        }
        $this->db->delete_query('threadsubscriptions', "tid='" . $tid . "' AND uid='{$uid}'");

        return true;
    }

    /**
     * Adds a forum to a user's forum subscription list.
     * If no uid is supplied, the currently logged in user's id will be used.
     *
     * @param int $fid The fid of the forum to add to the list.
     * @param int $uid (Optional) The uid of the user who's list to update.
     * @return boolean True when success, false when otherwise.
     */
    public function add_subscribed_forum($fid, $uid = 0)
    {
        if (!$uid) {
            $uid = $this->user->uid;
        }

        if (!$uid) {
            return false;
        }

        $fid = (int)$fid;
        $uid = (int)$uid;

        $query = $this->db->simple_select('forumsubscriptions', '*', "fid='" . $fid . "' AND uid='{$uid}'", ['limit' => 1]);
        $fsubscription = $this->db->fetch_array($query);
        if (!$fsubscription['fid']) {
            $insert_array = [
                'fid' => $fid,
                'uid' => $uid
            ];
            $this->db->insert_query('forumsubscriptions', $insert_array);
        }

        return true;
    }

    /**
     * Removes a forum from a user's forum subscription list.
     * If no uid is supplied, the currently logged in user's id will be used.
     *
     * @param int $fid The fid of the forum to remove from the list.
     * @param int $uid (Optional) The uid of the user who's list to update.
     * @return boolean True when success, false when otherwise.
     */
    public function remove_subscribed_forum($fid, $uid = 0)
    {
        if (!$uid) {
            $uid = $this->user->uid;
        }

        if (!$uid) {
            return false;
        }
        $this->db->delete_query('forumsubscriptions', "fid='" . $fid . "' AND uid='{$uid}'");

        return true;
    }

    /**
     * Constructs the usercp navigation menu.
     *
     */
    public function usercp_menu()
    {
        $this->bb->lang->load('usercpnav');

        if ($this->bb->settings['enablepms'] != 0) {
            $this->usercp_menu_messenger();
        }

        $this->usercp_menu_profile();
        $this->usercp_menu_misc();
    }

    /**
     * Constructs the usercp messenger menu.
     *
     */
    public function usercp_menu_messenger()
    {
        $folderlinks = [];//$folder_id = $folder_name = '';
        $foldersexploded = explode('$%%$', $this->user->pmfolders);
        foreach ($foldersexploded as $key => $folders) {
            $folderinfo = explode('**', $folders, 2);
            $folderinfo[1] = $this->get_pm_folder_name($folderinfo[0], $folderinfo[1]);
            if ($folderinfo[0] == 4) {
                $class = 'usercp_nav_trash_pmfolder';
            } elseif ($folderlinks) {
                $class = 'usercp_nav_sub_pmfolder';
            } else {
                $class = 'usercp_nav_pmfolder';
            }
            $folderlinks[] = [
                'folder_id' => $folderinfo[0],
                'folder_name' => $folderinfo[1],
                'class' => $class
            ];
        }
        $this->view->offsetSet('folderlinks', $folderlinks);

        $this->check_collapsed();
    }

    private function check_collapsed()
    {
        if (!isset($this->bb->collapsedimg['usercppms'])) {
            $this->bb->collapsedimg['usercppms'] = '';
            $this->view->offsetSet('collapsedimg', $this->bb->collapsedimg);
        }

        if (!isset($collapsed['usercppms_e'])) {
            $collapsed['usercppms_e'] = '';
            $this->view->offsetSet('collapsed', $collapsed);
        }
    }

    /**
     * Constructs the usercp profile menu.
     *
     */
    public function usercp_menu_profile()
    {
        $this->bb->showChangeSigOp = false;
        if ($this->bb->usergroup['canusesig'] == 1 &&
            ($this->bb->usergroup['canusesigxposts'] == 0 ||
                $this->bb->usergroup['canusesigxposts'] > 0 &&
                $this->user->postnum > $this->bb->usergroup['canusesigxposts'])
        ) {
            if ($this->user->suspendsignature == 0 ||
                $this->user->suspendsignature == 1 &&
                $this->user->suspendsigtime > 0 &&
                $this->user->suspendsigtime < TIME_NOW
            ) {
                $this->bb->showChangeSigOp = true;
            }
        }

        $this->check_collapsed();
    }

    /**
     * Constructs the usercp misc menu.
     *
     */
    public function usercp_menu_misc()
    {
        $draftcount = $this->bb->lang->ucp_nav_drafts;

        $count = \RunBB\Models\Post::draftCount($this->user->uid);

        if ($count > 0) {
            $draftcount = $this->bb->lang->sprintf($this->bb->lang->ucp_nav_drafts_active, $this->parser->formatNumber($count));
        }
        $this->view->offsetSet('draftcount', $draftcount);

        $this->check_collapsed();

        $this->view->offsetSet('profile_link', get_profile_link($this->user->uid));
    }

    /**
     * Gets the usertitle for a specific uid.
     *
     * @param int $uid The uid of the user to get the usertitle of.
     * @return string The usertitle of the user.
     */
    public function get_usertitle($uid = 0)
    {
        if ($this->user->uid == $uid) {
            $user = $this->user;
        } else {
            $query = $this->db->simple_select('users', 'usertitle,postnum', "uid='$uid'", ['limit' => 1]);
            $user = $this->db->fetch_array($query);
        }

        if ($user->usertitle) {
            return $user->usertitle;
        } else {
            $usertitle = '';
            $usertitles = $this->bb->cache->read('usertitles');
            foreach ($usertitles as $title) {
                if ($title['posts'] <= $user['postnum']) {
                    $usertitle = $title;
                    break;
                }
            }

            return $usertitle['title'];
        }
    }

    /**
     * Updates a users private message count in the users table with the number of pms they have.
     *
     * @param int $uid The user id to update the count for. If none, assumes currently logged in user.
     * @param int $count_to_update Bitwise value for what to update. 1 = total, 2 = new, 4 = unread. Combinations accepted.
     * @return array The updated counters
     */
    public function update_pm_count($uid = 0, $count_to_update = 7)
    {
        // If no user id, assume that we mean the current logged in user.
        if ((int)$uid == 0) {
            $uid = $this->user->uid;
        }

        $uid = (int)$uid;
        $pmcount = [];
        if ($uid == 0) {
            return $pmcount;
        }

        // Update total number of messages.
        if ($count_to_update & 1) {
            $query = $this->db->simple_select('privatemessages', 'COUNT(pmid) AS pms_total', "uid='" . $uid . "'");
            $total = $this->db->fetch_array($query);
            $pmcount['totalpms'] = $total['pms_total'];
        }

        // Update number of unread messages.
        if ($count_to_update & 2) {// && $this->db->field_exists('unreadpms', 'users') == true)
        //            $query = $this->db->simple_select('privatemessages',
//                'COUNT(pmid) AS pms_unread',
//                "uid='" . $uid . "' AND status='0' AND folder='1'");
//            $unread = $this->db->fetch_array($query);
//            $pmcount['unreadpms'] = $unread['pms_unread'];

            $pmcount['unreadpms'] = \RunBB\Models\Privatemessage::where([
                'uid' => $uid,
                'status' => '0',
                'folder' => '1'
            ])->count('pmid');
        }

        if (!empty($pmcount)) {
            \RunBB\Models\User::where('uid', $uid)->update($pmcount);
//            $this->db->update_query('users', $pmcount, "uid='" . $uid . "'");
        }
        return $pmcount;
    }

    /**
     * Return the language specific name for a PM folder.
     *
     * @param int $fid The ID of the folder.
     * @param string $name The folder name - can be blank, will use language default.
     * @return string The name of the folder.
     */
    public function get_pm_folder_name($fid, $name = '')
    {
        if ($name != '') {
            return $name;
        }

        switch ($fid) {
            case 1:
                return $this->bb->lang->folder_inbox;
                break;
            case 2:
                return $this->bb->lang->folder_sent_items;
                break;
            case 3:
                return $this->bb->lang->folder_drafts;
                break;
            case 4:
                return $this->bb->lang->folder_trash;
                break;
            default:
                return $this->bb->lang->folder_untitled;
        }
    }

    /**
     * Generates a security question for registration.
     *
     * @param int $old_qid Optional ID of the old question.
     * @return string The question session id.
     */
    public function generate_question($old_qid = 0)
    {
        if ($this->db->type == 'pgsql' || $this->db->type == 'sqlite') {
            $order_by = 'RANDOM()';
        } else {
            $order_by = 'RAND()';
        }

        $excl_old = ($old_qid) ? ' AND qid != ' . (int)$old_qid : '';

        $query = $this->db->simple_select('questions', 'qid, shown', "active=1{$excl_old}", ['limit' => 1, 'order_by' => $order_by]);
        $question = $this->db->fetch_array($query);

        if (!$this->db->num_rows($query)) {
            // No active questions exist
            return false;
        } else {
            $sessionid = random_str(32);

            $sql_array = [
                'sid' => $sessionid,
                'qid' => $question['qid'],
                'dateline' => TIME_NOW
            ];
            $this->db->insert_query('questionsessions', $sql_array);

            $update_question = [
                'shown' => $question['shown'] + 1
            ];
            $this->db->update_query('questions', $update_question, "qid = '{$question['qid']}'");

            return $sessionid;
        }
    }

    /**
     * Check whether we can show the Purge Spammer Feature
     *
     * @param int $post_count The users post count
     * @param int $usergroup The usergroup of our user
     * @param int $uid The uid of our user
     * @return boolean Whether or not to show the feature
     */
    public function purgespammer_show($post_count, $usergroup, $uid)
    {
        // only show this if the current user has permission to use it and the user has less than the post limit for using this tool
        $bangroup = $this->bb->settings['purgespammerbangroup'];
        $usergroups = $this->bb->cache->read('usergroups');

        return ($this->user->uid != $uid && $this->is_member($this->bb->settings['purgespammergroups']) &&
            !$this->is_super_admin($uid)
            && !$usergroups[$usergroup]['cancp'] && !$usergroups[$usergroup]['canmodcp'] && !$usergroups[$usergroup]['issupermod']
            && (str_replace($this->bb->settings['thousandssep'], '', $post_count) <= $this->bb->settings['purgespammerpostlimit'] || $this->bb->settings['purgespammerpostlimit'] == 0)
            && !$this->is_member($bangroup, $uid) && !$usergroups[$usergroup]['isbannedgroup']);
    }

    /**
     * Fetch the permissions for a specific user
     *
     * @param int $uid The user ID
     * @return array Array of user permissions for the specified user
     */
    public function user_permissions($uid = 0)
    {
        // If no user id is specified, assume it is the current user
        if ($uid == 0) {
            $uid = $this->user->uid;
        }

        // User id does not match current user, fetch permissions
        if ($uid != $this->user->uid) {
            // We've already cached permissions for this user, return them.
            if (isset($this->bb->user_cache[$uid]->permissions)) {
                return $this->bb->user_cache[$uid]->permissions;
            }

            // This user was not already cached, fetch their user information.
            if (!isset($this->bb->user_cache[$uid])) {
                $this->bb->user_cache[$uid] = $this->get_user($uid);
            }

            // Collect group permissions.
            $gid = $this->bb->user_cache[$uid]->usergroup . ',' . $this->bb->user_cache[$uid]->additionalgroups;
            $groupperms = $this->bb->group->usergroup_permissions($gid);

            // Store group permissions in user cache.
            $this->bb->user_cache[$uid]->permissions = $groupperms;
            return $groupperms;
        } // This user is the current user, return their permissions
        else {
            return $this->bb->usergroup;
        }
    }


    /**
     * Updates the user counters with a specific value (or addition/subtraction of the previous value)
     *
     * @param int $uid The user ID
     * @param array $changes Array of items being updated (postnum, threadnum) and their value (ex, 1, +1, -1)
     */
    public function update_user_counters($uid, $changes = [])
    {
        $update_query = [];

        $counters = ['postnum', 'threadnum'];
        $uid = (int)$uid;

        // Fetch above counters for this user
        $query = $this->db->simple_select('users', implode(',', $counters), "uid='{$uid}'");
        $user = $this->db->fetch_array($query);

        foreach ($counters as $counter) {
            if (array_key_exists($counter, $changes)) {
                if (substr($changes[$counter], 0, 2) == "+-") {
                    $changes[$counter] = substr($changes[$counter], 1);
                }
                // Adding or subtracting from previous value?
                if (substr($changes[$counter], 0, 1) == "+" || substr($changes[$counter], 0, 1) == "-") {
                    if ((int)$changes[$counter] != 0) {
                        $update_query[$counter] = $user[$counter] + $changes[$counter];
                    }
                } else {
                    $update_query[$counter] = $changes[$counter];
                }

                // Less than 0? That's bad
                if (isset($update_query[$counter]) && $update_query[$counter] < 0) {
                    $update_query[$counter] = 0;
                }
            }
        }

        $this->db->free_result($query);

        // Only update if we're actually doing something
        if (count($update_query) > 0) {
            $this->db->update_query("users", $update_query, "uid='{$uid}'");
        }
    }

    /**
     * Get the user data of an user username.
     *
     * @param string $username The user username of the user.
     * @param array $options
     * @return array The users data
     */
    public function get_user_by_username($username, $options = [])
    {
        $username = $this->db->escape_string(my_strtolower($username));

        if (!isset($options['username_method'])) {
            $options['username_method'] = 0;
        }

        switch ($this->db->type) {
            case 'mysql':
            case 'mysqli':
                $field = 'username';
                $efield = 'email';
                break;
            default:
                $field = 'LOWER(username)';
                $efield = 'LOWER(email)';
                break;
        }

        switch ($options['username_method']) {
            case 1:
                $sqlwhere = "{$efield}='{$username}'";
                break;
            case 2:
                $sqlwhere = "{$field}='{$username}' OR {$efield}='{$username}'";
                break;
            default:
                $sqlwhere = "{$field}='{$username}'";
                break;
        }

        $fields = ['uid'];
        if (isset($options['fields'])) {
            $fields = array_merge((array)$options['fields'], $fields);
        }

        $query = $this->db->simple_select('users', implode(',', array_unique($fields)), $sqlwhere, ['limit' => 1]);

        if (isset($options['exists'])) {
            return (bool)$this->db->num_rows($query);
        }

        return $this->db->fetch_array($query);
    }

    /**
     * Get the user data of an user id.
     *
     * @param int $uid The user id of the user.
     * @return array The users data
     */
    public function get_user($uid = 0)
    {
        $uid = (int)$uid;
        if ($uid === $this->user->uid) {
            return $this->user;
//        } elseif (isset($this->user_cache[$uid])) {
//            return $this->user_cache[$uid];
        } elseif ($uid > 0) {
//            $query = $this->db->simple_select("users", "*", "uid = '{$uid}'");
//            $this->bb->user_cache[$uid] = $this->db->fetch_array($query);
            $u = [];

                $u = \RunBB\Models\User::where('uid', '=', $uid)
                    ->leftJoin('userfields', 'userfields.ufid', '=', 'users.uid')
                    ->first()
                    ->toArray();
            if (!empty($u)) {
                $user = new \stdClass();
                foreach ($u as $key => $val) {
                    $user->$key = $val;
                }
                return $this->bb->user_cache[$uid] = $user;
            }
        }
        return [];
    }

    /**
     * Build the profile link.
     *
     * @param string $username The Username of the profile.
     * @param int $uid The user id of the profile.
     * @param string $target The target frame
     * @param string $onclick Any onclick javascript.
     * @return string The complete profile link.
     */
    public function build_profile_link($username = '', $uid = 0, $target = '', $onclick = '')
    {
        if (!$username && $uid == 0) {
            // Return Guest phrase for no UID, no guest nickname
            return $this->bb->lang->guest;
        } elseif ($uid == 0) {
            // Return the guest's nickname if user is a guest but has a nickname
            return $username;
        } else {
            // Build the profile link for the registered user
            if (!empty($target)) {
                $target = " target=\"{$target}\"";
            }

            if (!empty($onclick)) {
                $onclick = " onclick=\"{$onclick}\"";
            }

            return "<a href=\"" . get_profile_link($uid) . "\"{$target}{$onclick}>{$username}</a>";
        }
    }

    /**
     * Checks if a moderator has permissions to perform an action in a specific forum
     *
     * @param int $fid The forum ID (0 assumes global)
     * @param string $action The action tyring to be performed. (blank assumes any action at all)
     * @param int $uid The user ID (0 assumes current user)
     * @return bool Returns true if the user has permission, false if they do not
     */
    public function is_moderator($fid = 0, $action = '', $uid = 0)
    {
        if ($uid == 0) {
            $uid = $this->user->uid;
        }

        if ($uid == 0) {
            return false;
        }

        $user_perms = $this->user_permissions($uid);
        if ($user_perms['issupermod'] == 1) {
            if ($fid) {
                $forumpermissions = $this->bb->forum->forum_permissions($fid);
                if ($forumpermissions['canview'] &&
                    $forumpermissions['canviewthreads'] &&
                    (isset($forumpermissions['canonlyviewownthreads']) && !$forumpermissions['canonlyviewownthreads'])
                ) {
                    return true;
                }
                return false;
            }
            return true;
        } else {
            if (!$fid) {
                $modcache = $this->bb->cache->read('moderators');
                if (!empty($modcache)) {
                    foreach ($modcache as $modusers) {
                        if (isset($modusers['users'][$uid]) && $modusers['users'][$uid]['mid']) {
                            return true;
                        } elseif (isset($user_perms['gid']) && isset($modusers['usergroups'][$user_perms['gid']])) {
                            // Moderating usergroup
                            return true;
                        }
                    }
                }
                return false;
            } else {
                $modperms = $this->get_moderator_permissions($fid, $uid);

                if (!$action && $modperms) {
                    return true;
                } else {
                    if (isset($modperms[$action]) && $modperms[$action] == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    /**
     * Checks if a particular user is a super administrator.
     *
     * @param int $uid The user ID to check against the list of super admins
     * @return boolean True if a super admin, false if not
     */
    public function is_super_admin($uid)
    {
        static $super_admins;

        if (!isset($super_admins)) {
            $super_admins = str_replace(' ', '', $this->bb->config['super_admins']);
        }
        // 0 is true
        if (my_strpos(",{$super_admins},", ",{$uid},") === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if a user is a member of a particular group
     * Originates from frostschutz's PluginLibrary
     * github.com/frostschutz
     *
     * @param array|int|string A selection of groups (as array or comma seperated) to check or -1 for any group
     * @param bool|array|int False assumes the current user. Otherwise an user array or an id can be passed
     * @return array Array of groups specified in the first param to which the user belongs
     */
    public function is_member($groups, $user = false)
    {
        if (empty($groups)) {
            return [];
        }

        if ($user == false) {
            $user = $this->user;
        } elseif (!is_array($user)) {
            // Assume it's a UID
            $user = $this->get_user($user);
        }

        if (isset($user->additionalgroups)) {
            $memberships = array_map('intval', explode(',', $user->additionalgroups));
        }
        $memberships[] = $user->usergroup;
        if (!is_array($groups)) {
            if ((int)$groups == -1) {
                return $memberships;
            } else {
                if (is_string($groups)) {
                    $groups = explode(',', $groups);
                } else {
                    $groups = (array)$groups;
                }
            }
        }

        $groups = array_filter(array_map('intval', $groups));

        return array_intersect($groups, $memberships);
    }

    /**
     * Return the permissions for a moderator in a specific forum
     *
     * @param int $fid The forum ID
     * @param int $uid The user ID to fetch permissions for (0 assumes current logged in user)
     * @param string $parentslist The parent list for the forum (if blank, will be fetched)
     * @return array Array of moderator permissions for the specific forum
     */
    private function get_moderator_permissions($fid, $uid = 0, $parentslist = '')
    {
        static $modpermscache;

        if ($uid < 1) {
            $uid = $this->user->uid;
        }

        if ($uid == 0) {
            return false;
        }

        if (isset($modpermscache[$fid][$uid])) {
            return $modpermscache[$fid][$uid];
        }

        if (!$parentslist) {
            $parentslist = explode(',', $this->bb->forum->get_parent_list($fid));
        }

        // Get user groups
        $perms = [];
        $user = $this->get_user($uid);

        $groups = [$user->usergroup];

        if (!empty($user->additionalgroups)) {
            $extra_groups = explode(',', $user->additionalgroups);

            foreach ($extra_groups as $extra_group) {
                $groups[] = $extra_group;
            }
        }

        $mod_cache = $this->bb->cache->read('moderators');

        foreach ($mod_cache as $forumid => $forum) {
            if (!is_array($forum) || !in_array($forumid, $parentslist)) {
                // No perms or we're not after this forum
                continue;
            }

            // User settings override usergroup settings
            if (isset($forum['users'][$uid])) {
                $perm = $forum['users'][$uid];
                foreach ($perm as $action => $value) {
                    if (strpos($action, 'can') === false) {
                        continue;
                    }

                    // Figure out the user permissions
                    if ($value == 0) {
                        // The user doesn't have permission to set this action
                        $perms[$action] = 0;
                    } else {
                        $perms[$action] = $value;
                        $perms[$action] = max($perm[$action], $perms[$action]);
                    }
                }
            }

            foreach ($groups as $group) {
                if (!empty($forum['users'][$uid]) ||
//                    is_array($forum['usergroups'][$group])
                    empty($forum['usergroups'][$group])
                ) {
                    // There are no permissions set for this group
                    continue;
                }

                $perm = $forum['usergroups'][$group];
                foreach ($perm as $action => $value) {
                    if (strpos($action, 'can') === false) {
                        continue;
                    }
                    $perms[$action] = $value;
                    $perms[$action] = max($perm[$action], $perms[$action]);
                }
            }
        }

        $modpermscache[$fid][$uid] = $perms;

        return $perms;
    }

    /**
     * Formats an avatar to a certain dimension
     *
     * @param string $avatar The avatar file name
     * @param string $dimensions Dimensions of the avatar, width x height (e.g. 44|44)
     * @param string $max_dimensions The maximum dimensions of the formatted avatar
     * @return array Information for the formatted avatar
     */
    public function format_avatar($avatar, $dimensions = '', $max_dimensions = '')
    {
        static $avatars;

        if (!isset($avatars)) {
            $avatars = [];
        }

        if (!$avatar) {
            // Default avatar
            $avatar = $this->bb->settings['useravatar'];
            $dimensions = $this->bb->settings['useravatardims'];
        }

        if (!$max_dimensions) {
            $max_dimensions = $this->bb->settings['maxavatardims'];
        }

        // An empty key wouldn't work so we need to add a fall back
        $key = $dimensions;
        if (empty($key)) {
            $key = 'default';
        }
        $key2 = $max_dimensions;
        if (empty($key2)) {
            $key2 = 'default';
        }

        if (isset($avatars[$avatar][$key][$key2])) {
            return $avatars[$avatar][$key][$key2];
        }

        $avatar_width_height = '';

        if ($dimensions) {
            $dimensions = explode("|", $dimensions);

            if ($dimensions[0] && $dimensions[1]) {
                list($max_width, $max_height) = explode('x', $max_dimensions);

                if (!empty($max_dimensions) && ($dimensions[0] > $max_width || $dimensions[1] > $max_height)) {
                    $img = new \RunBB\Core\Image();
                    $scaled_dimensions = $img->scale_image($dimensions[0], $dimensions[1], $max_width, $max_height);
                    $avatar_width_height = "width=\"{$scaled_dimensions['width']}\" height=\"{$scaled_dimensions['height']}\"";
                } else {
                    $avatar_width_height = "width=\"{$dimensions[0]}\" height=\"{$dimensions[1]}\"";
                }
            }
        }

        $avatars[$avatar][$key][$key2] = [
            'image' => htmlspecialchars_uni($this->themes->get_asset_url($avatar)),
            'width_height' => $avatar_width_height
        ];

        return $avatars[$avatar][$key][$key2];
    }

    /**
     * Formats a username based on their display group
     *
     * @param string $username The username
     * @param int $usergroup The usergroup for the user
     * @param int $displaygroup The display group for the user
     * @return string The formatted username
     */
    public function format_name($username, $usergroup, $displaygroup = 0)
    {
        if (!is_array($this->bb->groupscache)) {
            $this->bb->groupscache = $this->cache->read("usergroups");
        }

        if ($displaygroup != 0) {
            $usergroup = $displaygroup;
        }

        $ugroup = $this->bb->groupscache[$usergroup];
        $format = $ugroup['namestyle'];
        $userin = substr_count($format, "{username}");

        if ($userin == 0) {
            $format = "{username}";
        }

        $format = stripslashes($format);

        return str_replace("{username}", $username, $format);
    }

    /**
     * Get the formatted reputation for a user.
     *
     * @param int $reputation The reputation value
     * @param int $uid The user ID (if not specified, the generated reputation will not be a link)
     * @return string The formatted repuation
     */
    public function get_reputation($reputation, $uid = 0)
    {
        if ($reputation < 0) {
            $reputation_class = 'reputation_negative';
        } elseif ($reputation > 0) {
            $reputation_class = 'reputation_positive';
        } else {
            $reputation_class = 'reputation_neutral';
        }

        $reputation = $this->parser->formatNumber($reputation);

        if ($uid != 0) {
            $display_reputation = "<a href=\"{$this->bb->settings['bburl']}/reputation?uid={$uid}\"><strong class=\"{$reputation_class}\">{$reputation}</strong></a>";
        } else {
            $display_reputation = "<strong class=\"{$reputation_class}\">{$reputation}</strong>";
        }

        return $display_reputation;
    }
}
