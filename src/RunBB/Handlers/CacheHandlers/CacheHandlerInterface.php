<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\CacheHandlers;

/**
 * Cache Handler Interface
 */
interface CacheHandlerInterface
{
    /**
     * Connect and initialize this handler.
     *
     * @return boolean True if successful, false on failure
     */
    public function connect();

    /**
     * Connect and initialize this handler.
     *
     * @param string $name
     * @return boolean True if successful, false on failure
     */
    public function fetch($name);

    /**
     * Write an item to the cache.
     *
     * @param string $name The name of the cache
     * @param mixed $contents The data to write to the cache item
     * @return boolean True on success, false on failure
     */
    public function put($name, $contents);

    /**
     * Delete a cache
     *
     * @param string $name The name of the cache
     * @return boolean True on success, false on failure
     */
    public function delete($name);

    /**
     * Disconnect from the cache
     *
     * @return bool
     */
    public function disconnect();

    /**
     * @param string $name
     *
     * @return string
     */
    public function sizeOf($name = '');
}
