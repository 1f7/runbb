<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\CacheHandlers;

/**
 * Disk Cache Handler
 */
class DiskCacheHandler implements CacheHandlerInterface
{
    private $bb;
    private $cachePath;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
//        $this->cachePath = DIR . 'web/assets/runbb/cache';
        $this->cachePath = DIR . 'var/cache/forum';
    }

    /**
     * Connect and initialize this handler.
     *
     * @return boolean True if successful, false on failure
     */
    public function connect()
    {
        if (!@is_writable($this->cachePath)) {
            //return false;
            $this->bb->trigger_generic_error('cache_no_write');
        }

        return true;
    }

    /**
     * Retrieve an item from the cache.
     *
     * @param string $name The name of the cache
     * @return mixed Cache data if successful, false if failure
     */
    public function fetch($name)
    {
        if (!@file_exists($this->cachePath . "/{$name}.php")) {
            return false;
        }

        @include($this->cachePath . "/{$name}.php");

        // Return data
        return $$name;
    }

    /**
     * Write an item to the cache.
     *
     * @param string $name The name of the cache
     * @param mixed $contents The data to write to the cache item
     * @return boolean True on success, false on failure
     */
    public function put($name, $contents)
    {
        if (!is_writable($this->cachePath)) {
            $this->bb->trigger_generic_error('cache_no_write');
            return false;
        }

        $cache_file = fopen($this->cachePath . "/{$name}.php", 'w')
        or $this->bb->trigger_generic_error('cache_no_write');
        flock($cache_file, LOCK_EX);
        //$cache_contents = "<?php\ndeclare(encoding='UTF-8');\n\n/** MyBB Generated Cache -
        // Do Not Alter\n * Cache Name: $name\n * Generated: " . gmdate('r') . "\n*/\n\n";
        $cache_contents = "<?php\n/** MyBB Generated Cache - Do Not Alter\n * Cache Name: $name\n * Generated: " .
            gmdate('r') . "\n*/\n\n";
        $cache_contents .= "\$$name = " . var_export($contents, true) . ';';
        fwrite($cache_file, $cache_contents);
        flock($cache_file, LOCK_UN);
        fclose($cache_file);

        return true;
    }

    /**
     * Delete a cache
     *
     * @param string $name The name of the cache
     * @return boolean True on success, false on failure
     */
    public function delete($name)
    {
        return @unlink($this->cachePath . "/{$name}.php");
    }

    /**
     * Disconnect from the cache
     *
     * @return bool
     */
    public function disconnect()
    {
        return true;
    }

    /**
     * Select the size of the disk cache
     *
     * @param string $name The name of the cache
     * @return integer the size of the disk cache
     */
    public function sizeOf($name = '')
    {
        if ($name != '') {
            return @filesize($this->cachePath . "/{$name}.php");
        } else {
            $total = 0;
            $dir = opendir($this->cachePath);
            while (($file = readdir($dir)) !== false) {
                if ($file == '.' || $file == '..' || $file == '.svn' || !is_file($this->cachePath . "/{$file}")) {
                    continue;
                }

                $total += filesize($this->cachePath . "/{$file}");
            }
            return $total;
        }
    }
}
