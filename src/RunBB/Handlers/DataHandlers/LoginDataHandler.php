<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\DataHandlers;

use RunBB\Core\DataHandler;

/**
 * Login handling class, provides common structure to handle login events.
 *
 */
class LoginDataHandler extends DataHandler
{
    /**
     * The language file used in the data handler.
     *
     * @var string
     */
    public $language_file = 'datahandler_login';

    /**
     * The prefix for the language variables used in the data handler.
     *
     * @var string
     */
    public $language_prefix = 'logindata';

    /**
     * Array of data used via login events.
     *
     * @var array
     */
    public $login_data = [];

    /**
     * @var bool
     */
    public $captcha_verified = true;

    /**
     * @var bool|captcha
     */
    private $captcha = false;

    /**
     * @var int
     */
    public $username_method = null;

    /**
     * @param int $check_captcha
     */
    private function verify_attempts($check_captcha = 0)
    {
        $user = &$this->data;

        if ($check_captcha) {
            if (!isset($this->bb->cookies['loginattempts'])) {
                $this->bb->cookies['loginattempts'] = 0;
            }
            if ($this->bb->settings['failedcaptchalogincount'] > 0 &&
                ($user['loginattempts'] > $this->bb->settings['failedcaptchalogincount'] ||
                    (int)$this->bb->cookies['loginattempts'] > $this->bb->settings['failedcaptchalogincount'])
            ) {
                $this->captcha_verified = false;
                $this->verify_captcha();
            }
        }
    }

    /**
     * @return bool
     */
    private function verify_captcha()
    {
        $user = &$this->data;

        if ($user['imagestring'] || $this->bb->settings['captchaimage'] != 1) {
            // Check their current captcha input - if correct, hide the captcha input area
            $this->captcha = new \RunBB\Core\Captcha($this->bb);

            if ($this->captcha->validate_captcha() == false) {
                // CAPTCHA validation failed
                foreach ($this->captcha->get_errors() as $error) {
                    $this->setError($error);
                }
                return false;
            } else {
                $this->captcha_verified = true;
                return true;
            }
        } elseif ($this->bb->getInput('quick_login', 0) == 1 &&
            $this->bb->input['quick_password'] &&
            $this->bb->input['quick_username']
        ) {
            $this->setError('regimagerequired');
            return false;
        } else {
            $this->setError('regimageinvalid');
            return false;
        }
    }

    /**
     * @return bool
     */
    public function verify_username()
    {
        $this->get_login_data();

        if (!$this->login_data['uid']) {
            $this->invalid_combination();
            return false;
        }

        return true;
    }

    /**
     * @param bool $strict
     *
     * @return bool
     */
    private function verify_password($strict = true)
    {
        $this->get_login_data();

        if (empty($this->login_data['username'])) {
            // Username must be validated to apply a password to
            $this->invalid_combination();
            return false;
        }

        $args = [
            'this' => &$this,
            'strict' => &$strict,
        ];

        $this->bb->plugins->runHooks('datahandler_login_verify_password_start', $args);

        $user = &$this->data;

        $password = md5($user['password']);

        if (!$this->login_data['uid'] || $this->login_data['uid'] && !$this->login_data['salt'] && $strict == false) {
            $this->invalid_combination();
        }

        if ($strict == true) {
            if (!$this->login_data['salt']) {
                // Generate a salt for this user and assume the password stored in db is a plain md5 password
                $this->login_data['salt'] = $this->bb->user->generate_salt();
                $this->login_data['password'] = $this->bb->user->salt_password($this->login_data['password'], $this->login_data['salt']);

                $sql_array = [
                    'salt' => $this->login_data['salt'],
                    'password' => $this->login_data['password']
                ];

                $this->bb->db->update_query('users', $sql_array, "uid = '{$this->login_data['uid']}'");
            }

            if (!$this->login_data['loginkey']) {
                $this->login_data['loginkey'] = $this->bb->user->generate_loginkey();
                $sql_array = [
                    'loginkey' => $this->login_data['loginkey']
                ];
                $this->bb->db->update_query('users', $sql_array, "uid = '{$this->login_data['uid']}'");
            }
        }

        $salted_password = md5(md5($this->login_data['salt']) . $password);

        $this->bb->plugins->runHooks('datahandler_login_verify_password_end', $args);

        if ($salted_password !== $this->login_data['password']) {
            $this->invalid_combination(true);
            return false;
        }

        return true;
    }

    /**
     * @param bool $show_login_attempts
     */
    private function invalid_combination($show_login_attempts = false)
    {
        // Don't show an error when the captcha was wrong!
        if (!$this->captcha_verified) {
            return;
        }

        $login_text = '';
        if ($show_login_attempts) {
            if ($this->bb->settings['failedlogincount'] != 0 && $this->bb->settings['failedlogintext'] == 1) {
                $logins = $this->bb->login_attempt_check(false) + 1;
                $login_text = $this->bb->lang->sprintf($this->bb->lang->failed_login_again, $this->bb->settings['failedlogincount'] - $logins);
            }
        }

        switch ($this->bb->settings['username_method']) {
            case 1:
                $this->setError('invalidpwordusernameemail', $login_text);
                break;
            case 2:
                $this->setError('invalidpwordusernamecombo', $login_text);
                break;
            default:
                $this->setError('invalidpwordusername', $login_text);
                break;
        }
    }

    private function get_login_data()
    {
        $user = &$this->data;

        $options = [
            'fields' => ['uid', 'username', 'password', 'salt', 'loginkey', 'coppauser', 'usergroup', 'loginattempts'],
            'username_method' => (int)$this->bb->settings['username_method']
        ];

        if ($this->username_method !== null) {
            $options['username_method'] = (int)$this->username_method;
        }

        $this->login_data = $this->bb->user->get_user_by_username($user['username'], $options);
    }

    /**
     * @return bool
     */
    public function validate_login()
    {
        $user = &$this->data;

        $this->bb->plugins->runHooks('datahandler_login_validate_start', $this);

        if (!defined('IN_ADMINCP')) {
            $this->verify_attempts($this->bb->settings['captchaimage']);
        }

        if (array_key_exists('username', $user)) {
            $this->verify_username();
        }

        if (array_key_exists('password', $user)) {
            $this->verify_password();
        }

        $this->bb->plugins->runHooks('datahandler_login_validate_end', $this);

        $this->set_validated(true);

        if (count($this->get_errors()) > 0) {
            return false;
        }

        return true;
    }

    /**
     * @return bool true
     */
    public function complete_login()
    {
        $user = &$this->login_data;

        $this->bb->plugins->runHooks('datahandler_login_complete_start', $this);

        // Login to MyBB
        $this->bb->my_setcookie('loginattempts', 1);
        $this->bb->my_setcookie('sid', $this->bb->session->sid, -1, true);

//        $ip_address = $this->bb->session->ipaddress;
//        $this->bb->db->delete_query('sessions', "ip = {$ip_address} AND sid != '{$this->bb->session->sid}'");
        \RunBB\Models\Session::where('ip', $this->bb->session->ipaddress)
            ->where('sid', '!=', $this->bb->session->sid)
            ->delete();

        $newsession = [
            'uid' => $user['uid'],
        ];

        $this->bb->db->update_query('sessions', $newsession, "sid = '{$this->bb->session->sid}'");
        $this->bb->db->update_query('users', ['loginattempts' => 1], "uid = '{$user['uid']}'");

        $remember = null;
        if (!isset($this->bb->input['remember']) || $this->bb->input['remember'] != 'yes') {
            $remember = -1;
        }

        $this->bb->my_setcookie('runbbuser', $user['uid'] . '_' . $user['loginkey'], $remember, true);

        if ($this->captcha !== false) {
            $this->captcha->invalidate_captcha();
        }

        $this->bb->plugins->runHooks('datahandler_login_complete_end', $this);

        return true;
    }
}
