<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\DataHandlers;

use RunBB\Core\DataHandler;

/*
EXAMPLE USE:

$post = get from POST data
$thread = get from DB using POST data id

$postHandler = new postDataHandler();
if($postHandler->validatePost($post))
{
	$postHandler->insertPost($post);
}

*/

/**
 * Post handling class, provides common structure to handle post data.
 *
 */
class PostDataHandler extends DataHandler
{
    /**
     * The language file used in the data handler.
     *
     * @var string
     */
    public $language_file = 'datahandler_post';

    /**
     * The prefix for the language variables used in the data handler.
     *
     * @var string
     */
    public $language_prefix = 'postdata';

    /**
     * What are we performing?
     * post = New post
     * thread = New thread
     * edit = Editing a thread or post
     *
     * @var string
     */
    public $action;

    /**
     * Array of data inserted in to a post.
     *
     * @var array
     */
    public $post_insert_data = [];

    /**
     * Array of data used to update a post.
     *
     * @var array
     */
    public $post_update_data = [];

    /**
     * Post ID currently being manipulated by the datahandlers.
     *
     * @var int
     */
    public $pid = 0;

    /**
     * Array of data inserted in to a thread.
     *
     * @var array
     */
    public $thread_insert_data = [];

    /**
     * Array of data used to update a thread.
     *
     * @var array
     */
    public $thread_update_data = [];

    /**
     * Thread ID currently being manipulated by the datahandlers.
     *
     * @var int
     */
    public $tid = 0;

    /**
     * Values to be returned after inserting/updating a post/thread.
     *
     * @var array
     */
    public $return_values = [];

    /**
     * Is this the first post of a thread when editing
     *
     * @var boolean
     */
    public $first_post = false;

    /**
     * Verifies the author of a post and fetches the username if necessary.
     *
     * @return boolean True if the author information is valid, false if invalid.
     */
    public function verifyAuthor()
    {
        $post = &$this->data;

        // Don't have a user ID at all - not good (note, a user id of 0 will still work).
        if (!isset($post['uid'])) {
            $this->setError('invalid_user_id');
            return false;
        } // If we have a user id but no username then fetch the username.
        elseif ($post['uid'] > 0 && empty($post['username'])) {
            $user = $this->bb->user->get_user($post['uid']);
            $post['username'] = $user->username;
        } // if the uid is 0 verify the username
        elseif ($post['uid'] == 0 && $post['username'] != $this->bb->lang->guest) {
            // Set up user handler
            $userhandler = new \RunBB\Handlers\DataHandlers\UserDataHandler($this->bb);

            $data_array = ['username' => $post['username']];
            $userhandler->set_data($data_array);

            if (!$userhandler->verify_username()) {
                // invalid username
                $this->errors = array_merge($this->errors, $userhandler->get_errors());
                return false;
            }

            if ($userhandler->verify_username_exists()) {
                // username is in use
                $this->errors = array_merge($this->errors, $userhandler->get_errors());
                return false;
            }
        }

        // After all of this, if we still don't have a username, force the username as 'Guest'
        // (Note, this is not translatable as it is always a fallback)
        if (!$post['username']) {
            $post['username'] = 'Guest';
        }

        return true;
    }

    /**
     * Verifies a post subject.
     *
     * @return boolean True when valid, false when not valid.
     */
    public function verifySubject()
    {
        $post = &$this->data;
        $subject = &$post['subject'];
        $subject = trim_blank_chrs($subject);

        if ($this->method == 'update' && $post['pid']) {
            // If this is the first post there needs to be a subject, else make it the default one.
            if (my_strlen($subject) == 0 && $this->first_post) {
                $this->setError('firstpost_no_subject');
                return false;
            } elseif (my_strlen($subject) == 0) {
                $thread = $this->bb->thread->get_thread($post['tid']);
                $subject = 'RE: ' . $thread['subject'];
            }
        } // This is a new post
        elseif ($this->action == 'post') {
            if (my_strlen($subject) == 0) {
                $thread = $this->bb->thread->get_thread($post['tid']);
                $subject = 'RE: ' . $thread['subject'];
            }
        } // This is a new thread and we require that a subject is present.
        else {
            if (my_strlen($subject) == 0) {
                $this->setError('missing_subject');
                return false;
            }
        }

        // If post is reply and begins with 'RE: ', remove 4 from subject length.
        $subject_length = my_strlen($subject);
        if ($this->action == 'post') {
            $position_re = my_strpos($subject, 'RE: ');
            if ($position_re !== false && $position_re == 0) {
                $subject_length = $subject_length - 4;
            }
        }

        if ($subject_length > 85) {
            // Subject is too long
            $this->setError('subject_too_long', my_strlen($subject));
            return false;
        }

        // Subject is valid - return true.
        return true;
    }

    /**
     * Verifies a post message.
     *
     * @return bool
     */
    public function verifyMessage()
    {
        $post = &$this->data;
        $post['message'] = trim_blank_chrs($post['message']);

        // Do we even have a message at all?
        if (my_strlen($post['message']) == 0) {
            $this->setError('missing_message');
            return false;
        } // If this board has a maximum message length check if we're over it.
        //Use strlen because SQL limits are in bytes
        elseif (strlen($post['message']) > $this->bb->settings['maxmessagelength'] &&
            $this->bb->settings['maxmessagelength'] > 0 &&
            !$this->bb->user->is_moderator($post['fid'], '', $post['uid'])
        ) {
            $this->setError('message_too_long', [$this->bb->settings['maxmessagelength'], strlen($post['message'])]);
            return false;
        } // And if we've got a minimum message length do we meet that requirement too?
        else {
            if (!isset($post['fid'])) {
                $post['fid'] = 0;
            }
            if (!$this->bb->settings['mycodemessagelength']) {
                $message = $this->bb->parser->text_parse_message($post['message']);

                if (my_strlen($message) < $this->bb->settings['minmessagelength'] &&
                    $this->bb->settings['minmessagelength'] > 0 &&
                    !$this->bb->user->is_moderator($post['fid'], '', $post['uid'])
                ) {
                    $this->setError('message_too_short', [$this->bb->settings['minmessagelength']]);
                    return false;
                }
            } elseif (my_strlen($post['message']) < $this->bb->settings['minmessagelength'] &&
                $this->bb->settings['minmessagelength'] > 0 &&
                !$this->bb->user->is_moderator($post['fid'], '', $post['uid'])
            ) {
                $this->setError('message_too_short', [$this->bb->settings['minmessagelength']]);
                return false;
            }
        }
        return true;
    }

    /**
     * Verifies the specified post options are correct.
     *
     * @return boolean True
     */
    private function verifyOptions()
    {
        $options = &$this->data['options'];

        // Verify yes/no options.
        $this->verify_yesno_option($options, 'signature', 0);
        $this->verify_yesno_option($options, 'disablesmilies', 0);

        return true;
    }

    /**
     * Verify that the user is not flooding the system.
     *
     * @return boolean
     */
    private function verifyPostFlooding()
    {
        $post = &$this->data;

        // Check if post flooding is enabled within MyBB or if the admin override option is specified.
        if ($this->bb->settings['postfloodcheck'] == 1 && $post['uid'] != 0 && $this->admin_override == false) {
            if ($this->verifyPostMerge(true) !== true) {
                return true;
            }

            // Fetch the user information for this post - used to check their last post date.
            $user = $this->bb->user->get_user($post['uid']);

            // A little bit of calculation magic and moderator status checking.
            if (TIME_NOW - $user->lastpost <= $this->bb->settings['postfloodsecs'] &&
                !$this->bb->user->is_moderator($post['fid'], '', $user->uid)
            ) {
                // Oops, user has been flooding - throw back error message.
                $time_to_wait = ($this->bb->settings['postfloodsecs'] - (TIME_NOW - $user->lastpost)) + 1;
                if ($time_to_wait == 1) {
                    $this->setError('post_flooding_one_second');
                } else {
                    $this->setError('post_flooding', [$time_to_wait]);
                }
                return false;
            }
        }
        // All is well that ends well - return true.
        return true;
    }

    /**
     * @param bool $simple_mode
     *
     * @return array|bool
     */
    private function verifyPostMerge($simple_mode = false)
    {
        $post = &$this->data;

        // Are we starting a new thread?
        if (empty($post['tid'])) {
            return true;
        }

        // Are we even turned on?
        if (empty($this->bb->settings['postmergemins'])) {
            return true;
        }

        // Assign a default separator if none is specified
        if (trim($this->bb->settings['postmergesep']) == '') {
            $this->bb->settings['postmergesep'] = '[hr]';
        }

        // Check to see if this person is in a usergroup that is excluded
        if ($this->bb->user->is_member($this->bb->settings['postmergeuignore'], $post['uid'])) {
            return true;
        }

        // Select the lastpost and fid information for this thread
        $query = $this->bb->db->simple_select(
            'threads',
            'lastpost,fid',
            "lastposteruid='" . $post['uid'] . "' AND tid='" . $post['tid'] . "'",
            ['limit' => '1']
        );
        $thread = $this->bb->db->fetch_array($query);

        // Check to see if the same author has posted within the merge post time limit
        if (((int)$this->bb->settings['postmergemins'] != 0 &&
                trim($this->bb->settings['postmergemins']) != '') &&
            (TIME_NOW - $thread['lastpost']) > ((int)$this->bb->settings['postmergemins'] * 60)
        ) {
            return true;
        }

        if ($this->bb->settings['postmergefignore'] == -1) {
            return true;
        } elseif ($this->bb->settings['postmergefignore'] != '') {
            $fids = explode(',', (string)$this->bb->settings['postmergefignore']);

            if (is_array($fids)) {
                foreach ($fids as &$fid) {
                    $fid = (int)$fid;
                }
                unset($fid);

                if (in_array($thread['fid'], $fids)) {
                    return true;
                }
            }
        }

        if ($simple_mode == true) {
            return false;
        }

        if ($post['uid']) {
            $user_check = "uid='" . $post['uid'] . "'";
        } else {
            $user_check = 'ipaddress=' . $this->bb->session->ipaddress;
        }

        $query = $this->bb->db->simple_select(
            'posts',
            'pid,message,visible',
            "{$user_check} AND tid='" . $post['tid'] . "' AND dateline='" . $thread['lastpost'] . "'",
            ['order_by' => 'pid', 'order_dir' => 'DESC', 'limit' => 1]
        );
        return $this->bb->db->fetch_array($query);
    }

    /**
     * Verifies the image count.
     *
     * @return boolean True when valid, false when not valid.
     */
    private function verifyImageCount()
    {
        $post = &$this->data;

        // Get the permissions of the user who is making this post or thread
        $permissions = $this->bb->user->user_permissions($post['uid']);

        // Fetch the forum this post is being made in
        if (!$post['fid']) {
            $query = $this->bb->db->simple_select('posts', 'fid', "pid = '{$post['pid']}'");
            $post['fid'] = $this->bb->db->fetch_field($query, 'fid');
        }
        $forum = $this->bb->forum->get_forum($post['fid']);

        // Check if this post contains more images than the forum allows
        if ((!isset($post['savedraft']) || $post['savedraft'] != 1) &&
            $this->bb->settings['maxpostimages'] != 0 && $permissions['cancp'] != 1
        ) {
            // Parse the message.
            $parser_options = [
                'allow_html' => $forum['allowhtml'],
                'allow_mycode' => $forum['allowmycode'],
                'allow_imgcode' => $forum['allowimgcode'],
                'allow_videocode' => $forum['allowvideocode'],
                'filter_badwords' => 1
            ];

            if ($post['options']['disablesmilies'] != 1) {
                $parser_options['allow_smilies'] = $forum['allowsmilies'];
            } else {
                $parser_options['allow_smilies'] = 0;
            }

            $image_check = $this->bb->parser->parse_message($post['message'], $parser_options);

            // And count the number of image tags in the message.
            $image_count = substr_count($image_check, '<img');
            if ($image_count > $this->bb->settings['maxpostimages']) {
                // Throw back a message if over the count with the number of images as well as
                // the maximum number of images per post.
                $this->setError('too_many_images', [1 => $image_count, 2 => $this->bb->settings['maxpostimages']]);
                return false;
            }
        }

        return true;
    }

    /**
     * Verifies the video count.
     *
     * @return boolean True when valid, false when not valid.
     */
    private function verifyVideoCount()
    {
        $post = &$this->data;

        // Get the permissions of the user who is making this post or thread
        $permissions = $this->bb->user->user_permissions($post['uid']);

        // Check if this post contains more videos than the forum allows
        if ((!isset($post['savedraft']) || $post['savedraft'] != 1) &&
            $this->bb->settings['maxpostvideos'] != 0 && $permissions['cancp'] != 1
        ) {
            // And count the number of video tags in the message.
            $video_count = substr_count($post['message'], '[video=');
            if ($video_count > $this->bb->settings['maxpostvideos']) {
                // Throw back a message if over the count with the number of images as well as
                // the maximum number of images per post.
                $this->setError('too_many_videos', [1 => $video_count, 2 => $this->bb->settings['maxpostvideos']]);
                return false;
            }
        }

        return true;
    }

    /**
     * Verify the reply-to post.
     *
     * @return boolean True when valid, false when not valid.
     */
    private function verifyReplyTo()
    {
        $post = &$this->data;

        // Check if the post being replied to actually exists in this thread.
        if ($post['replyto']) {
            $query = $this->bb->db->simple_select('posts', 'pid', "pid='" . (int)$post['replyto'] . "'");
            $valid_post = $this->bb->db->fetch_array($query);
            if (!$valid_post['pid']) {
                $post['replyto'] = 0;
            } else {
                return true;
            }
        }

        // If this post isn't a reply to a specific post, attach it to the first post.
        if (!$post['replyto']) {
            $options = [
                'limit_start' => 0,
                'limit' => 1,
                'order_by' => 'dateline',
                'order_dir' => 'asc'
            ];
            $query = $this->bb->db->simple_select('posts', 'pid', "tid='{$post['tid']}'", $options);
            $reply_to = $this->bb->db->fetch_array($query);
            $post['replyto'] = $reply_to['pid'];
        }

        return true;
    }

    /**
     * Verify the post icon.
     *
     * @return boolean True when valid, false when not valid.
     */
    private function verifyPostIcon()
    {
        //global $cache;

        $post = &$this->data;

        $posticons_cache = $this->bb->cache->read('posticons');

        // If we don't have a post icon assign it as 0.
        if (empty($post['icon']) || !isset($posticons_cache[$post['icon']])) {
            $post['icon'] = 0;
        }
        return true;
    }

    /**
     * Verify the dateline.
     *
     * @return boolean True when valid, false when not valid.
     */
    private function verifyDateline()
    {
        $dateline = &$this->data['dateline'];

        // The date has to be numeric and > 0.
        if ($dateline < 0 || is_numeric($dateline) == false) {
            $dateline = TIME_NOW;
        }
    }

    /**
     * Verify thread prefix.
     *
     * @return boolean True when valid, false when not valid.
     */
    private function verifyPrefix()
    {
        $prefix = &$this->data['prefix'];

        // If a valid prefix isn't supplied, don't assign one.
        if (empty($prefix)) {
            $prefix = 0;
        } else {
            if (!empty($this->data['tid'])) {
                // Fetch the thread
                $thread = $this->bb->thread->get_thread($this->data['tid']);
            }

            $prefix_cache = $this->bb->thread->build_prefixes($prefix);

            if (empty($prefix_cache)) {
                $this->setError('invalid_prefix');
                return false;
            }
            if ($prefix_cache['groups'] != '-1') {
                if (!empty($this->data['edit_uid'])) {
                    // Post is being edited
                    $user = $this->bb->user->get_user($this->data['edit_uid']);
                } else {
                    $user = $this->bb->user->get_user($this->data['uid']);
                }

                if (!$this->bb->user->is_member(
                    $prefix_cache['groups'],
                    ['usergroup' => $user->usergroup, 'additionalgroups' => $user->additionalgroups]
                ) &&
                    (empty($this->data['tid']) || $prefix != $thread['prefix'])
                ) {
                    $this->setError('invalid_prefix');
                    return false;
                }
            }
            if ($prefix_cache['forums'] != '-1') {
                // Decide whether this prefix can be used in our forum
                $forums = explode(',', $prefix_cache['forums']);

                if (!in_array($this->data['fid'], $forums) && (empty($this->data['tid']) ||
                        $prefix != $thread['prefix'])) {
                    $this->setError('invalid_prefix');
                    return false;
                }
            }
        }

        // Does this forum require a prefix?
        $forum = $this->bb->forum->get_forum($this->data['fid']);

        if ($forum['requireprefix'] == 1) {
            $num_prefixes = false;

            // Go through each of our prefixes and decide if there are any possible prefixes to use.
            if (!empty($this->data['edit_uid'])) {
                // Post is being edited
                $user = $this->bb->user->get_user($this->data['edit_uid']);
            } else {
                $user = $this->bb->user->get_user($this->data['uid']);
            }

            $prefix_cache = $this->bb->thread->build_prefixes();

            if (!empty($prefix_cache)) {
                foreach ($prefix_cache as $required) {
                    if ($required['forums'] != '-1') {
                        // Decide whether this prefix can be used in our forum
                        $forums = explode(',', $required['forums']);

                        if (!in_array($forum['fid'], $forums)) {
                            continue;
                        }
                    }

                    if ($this->bb->user->is_member(
                        $required['groups'],
                        ['usergroup' => $user->usergroup, 'additionalgroups' => $user->additionalgroups]
                    )
                    ) {
                        $num_prefixes = true;
                    }
                }
            }

            if ($prefix == 0 && $num_prefixes) {
                $this->setError('require_prefix');
                return false;
            }
        }

        return true;
    }

    /**
     * Validate a post.
     *
     * @return boolean True when valid, false when invalid.
     */
    public function validatePost()
    {
        $post = &$this->data;
        $time = TIME_NOW;

        $this->action = 'post';

        if ($this->method != 'update' && !$post['savedraft']) {
            $this->verifyPostFlooding();
        }

        // Are we editing an existing thread or post?
        if ($this->method == 'update') {
            if (empty($post['tid'])) {
                $query = $this->bb->db->simple_select('posts', 'tid', "pid='" . (int)$post['pid'] . "'");
                $post['tid'] = $this->bb->db->fetch_field($query, 'tid');
            }
            // Here we determine if we're editing the first post of a thread or not.
            $options = [
                'limit' => 1,
                'limit_start' => 0,
                'order_by' => 'dateline',
                'order_dir' => 'asc'
            ];
            $query = $this->bb->db->simple_select('posts', 'pid', "tid='" . $post['tid'] . "'", $options);
            $first_check = $this->bb->db->fetch_array($query);
            if ($first_check['pid'] == $post['pid']) {
                $this->first_post = true;
            }
        }

        // Verify all post assets.

        if ($this->method == 'insert' || array_key_exists('uid', $post)) {
            $this->verifyAuthor();
        }

        if ($this->method == 'insert' || array_key_exists('subject', $post)) {
            $this->verifySubject();
        }

        if ($this->method == 'insert' || array_key_exists('message', $post)) {
            $this->verifyMessage();
            $this->verifyImageCount();
            $this->verifyVideoCount();
        }

        if ($this->method == 'insert' || array_key_exists('dateline', $post)) {
            $this->verifyDateline();
        }

        if ($this->method == 'insert' || array_key_exists('replyto', $post)) {
            $this->verifyReplyTo();
        }

        if ($this->method == 'insert' || array_key_exists('icon', $post)) {
            $this->verifyPostIcon();
        }

        if ($this->method == 'insert' || array_key_exists('options', $post)) {
            $this->verifyOptions();
        }

        if ($this->method == 'update' && $this->first_post) {
            $this->verifyPrefix();
        }

        $this->bb->plugins->runHooks('datahandler_post_validate_post', $this);

        // We are done validating, return.
        $this->set_validated(true);
        if (count($this->get_errors()) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Insert a post into the database.
     *
     * @return array Array of new post details, pid and visibility.
     */
    public function insertPost()
    {
        $post = &$this->data;

        // Yes, validating is required.
        if (!$this->get_validated()) {
            die('The post needs to be validated before inserting it into the DB.');
        }
        if (count($this->get_errors()) > 0) {
            die('The post is not valid.');
        }

        // Fetch the thread
        $thread = $this->bb->thread->get_thread($post['tid']);

        $closed = $thread['closed'];

        // This post is being saved as a draft.
        if ($post['savedraft']) {
            $visible = -2;
        } // Otherwise this post is being made now and we have a bit to do.
        else {
            // Automatic subscription to the thread
            if ($post['options']['subscriptionmethod'] != '' && $post['uid'] > 0) {
                switch ($post['options']['subscriptionmethod']) {
                    case 'pm':
                        $notification = 2;
                        break;
                    case 'email':
                        $notification = 1;
                        break;
                    default:
                        $notification = 0;
                }
                $this->bb->user->add_subscribed_thread($post['tid'], $notification, $post['uid']);
            }

            // Perform any selected moderation tools.
            $ismod = $this->bb->user->is_moderator($post['fid'], '', $post['uid']);
            if ($ismod) {
                $this->bb->lang->load($this->language_file, true);

                $modoptions = $post['modoptions'];
                $modlogdata['fid'] = $thread['fid'];
                $modlogdata['tid'] = $thread['tid'];

                if (!isset($modoptions['closethread'])) {
                    $modoptions['closethread'] = $closed;
                }

                $modoptions_update = [];

                // Close the thread.
                if ($modoptions['closethread'] == 1 && $thread['closed'] != 1) {
                    $modoptions_update['closed'] = $closed = 0;
                    $this->bblogger->log_moderator_action($modlogdata, $this->bb->lang->thread_closed);
                }

                // Open the thread.
                if ($modoptions['closethread'] != 1 && $thread['closed'] == 1) {
                    $modoptions_update['closed'] = $closed = 1;
                    $this->bblogger->log_moderator_action($modlogdata, $this->bb->lang->thread_opened);
                }

                if (!isset($modoptions['stickthread'])) {
                    $modoptions['stickthread'] = $thread['sticky'];
                }

                // Stick the thread.
                if ($modoptions['stickthread'] == 1 && $thread['sticky'] != 1) {
                    $modoptions_update['sticky'] = 1;
                    $this->bblogger->log_moderator_action($modlogdata, $this->bb->lang->thread_stuck);
                }

                // Unstick the thread.
                if ($modoptions['stickthread'] != 1 && $thread['sticky']) {
                    $modoptions_update['sticky'] = 0;
                    $this->bblogger->log_moderator_action($modlogdata, $this->bb->lang->thread_unstuck);
                }

                // Execute moderation options.
                if ($modoptions_update) {
                    $this->bb->db->update_query('threads', $modoptions_update, "tid='{$thread['tid']}'");
                }
            }

            // Fetch the forum this post is being made in
            $forum = $this->bb->forum->get_forum($post['fid']);

            // Decide on the visibility of this post.
            $forumpermissions = $this->bb->forum->forum_permissions($post['fid'], $post['uid']);
            if ($forumpermissions['modposts'] == 1 && !$ismod) {
                $visible = 0;
            } else {
                $visible = 1;
            }

            // Are posts from this user being moderated? Change visibility
            if ($this->bb->user->uid == $post['uid'] && $this->bb->user->moderateposts == 1) {
                $visible = 0;
            }
        }

        if (!isset($post['pid'])) {
            $post['pid'] = 0;
        }

        $post['pid'] = (int)$post['pid'];
        $post['uid'] = (int)$post['uid'];

        if ($post['pid'] > 0) {
            $query = $this->bb->db->simple_select(
                'posts',
                'tid',
                "pid='{$post['pid']}' AND uid='{$post['uid']}' AND visible='-2'"
            );
            $draft_check = $this->bb->db->fetch_field($query, 'tid');
        } else {
            $draft_check = false;
        }

        if ($this->method != 'update' && $visible == 1) {
            $double_post = $this->verifyPostMerge();

            // Only combine if they are both invisible (mod queue'd forum) or both visible
            if ($double_post !== true && $double_post['visible'] == $visible) {
                $this->pid = $double_post['pid'];

                $post['message'] = $double_post['message'] .= "\n" . $this->bb->settings['postmergesep'] .
                    "\n" . $post['message'];
                $update_query = [
                    'message' => $this->bb->db->escape_string($double_post['message'])
                ];
                $update_query['edituid'] = (int)$post['uid'];
                $update_query['edittime'] = TIME_NOW;
                $this->bb->db->update_query('posts', $update_query, "pid='" . $double_post['pid'] . "'");

                if ($draft_check) {
                    $this->bb->db->delete_query('posts', "pid='" . $post['pid'] . "'");
                }

                if ($post['posthash']) {
                    // Assign any uploaded attachments with the specific posthash to the merged post.
                    $post['posthash'] = $this->bb->db->escape_string($post['posthash']);

                    $query = $this->bb->db->simple_select(
                        'attachments',
                        'COUNT(aid) AS attachmentcount',
                        "pid='0' AND visible='1' AND posthash='{$post['posthash']}'"
                    );
                    $attachmentcount = $this->bb->db->fetch_field($query, 'attachmentcount');

                    if ($attachmentcount > 0) {
                        // Update forum count
                        $this->bb->thread->update_thread_counters(
                            $post['tid'],
                            ['attachmentcount' => "+{$attachmentcount}"]
                        );
                    }

                    $attachmentassign = [
                        'pid' => $double_post['pid'],
                        'posthash' => ''
                    ];
                    $this->bb->db->update_query(
                        'attachments',
                        $attachmentassign,
                        "posthash='{$post['posthash']}' AND pid='0'"
                    );
                }

                // Return the post's pid and whether or not it is visible.
                $this->return_values = [
                    'pid' => $double_post['pid'],
                    'visible' => $visible,
                    'merge' => true
                ];

                $this->bb->plugins->runHooks('datahandler_post_insert_merge', $this);

                return $this->return_values;
            }
        }

        if ($visible == 1 && $thread['visible'] == 1) {
            $now = TIME_NOW;

            // Yes, the value to the lastpost key in this array has single quotes within double quotes. It's not a bug.
            $update_array = [
                'lastpost' => "'{$now}'"
            ];
            if ($forum['usepostcounts'] != 0) {
                $update_array['postnum'] = 'postnum+1';
            }

            $this->bb->db->update_query('users', $update_array, "uid='{$post['uid']}'", 1, true);
        }

        // Are we updating a post which is already a draft? Perhaps changing it into a visible post?
        if ($draft_check) {
            // Update a post that is a draft
            $this->post_update_data = [
                'subject' => $this->bb->db->escape_string($post['subject']),
                'icon' => (int)$post['icon'],
                'uid' => $post['uid'],
                'username' => $this->bb->db->escape_string($post['username']),
                'dateline' => (int)$post['dateline'],
                'message' => $this->bb->db->escape_string($post['message']),
                'ipaddress' => $post['ipaddress'],
                'includesig' => $post['options']['signature'],
                'smilieoff' => $post['options']['disablesmilies'],
                'visible' => $visible
            ];

            $this->bb->plugins->runHooks('datahandler_post_insert_post', $this);

            $this->bb->db->update_query('posts', $this->post_update_data, "pid='{$post['pid']}'");
            $this->pid = $post['pid'];
        } else {
            // Insert the post.
            $this->post_insert_data = [
                'tid' => (int)$post['tid'],
                'replyto' => (int)$post['replyto'],
                'fid' => (int)$post['fid'],
                'subject' => $this->bb->db->escape_string($post['subject']),
                'icon' => (int)$post['icon'],
                'uid' => $post['uid'],
                'username' => $this->bb->db->escape_string($post['username']),
                'dateline' => $post['dateline'],
                'message' => $this->bb->db->escape_string($post['message']),
                'ipaddress' => $post['ipaddress'],
                'includesig' => $post['options']['signature'],
                'smilieoff' => $post['options']['disablesmilies'],
                'visible' => $visible
            ];

            $this->bb->plugins->runHooks('datahandler_post_insert_post', $this);

            $this->pid = $this->bb->db->insert_query('posts', $this->post_insert_data);
        }

        // Assign any uploaded attachments with the specific posthash to the newly created post.
        if ($post['posthash']) {
            $post['posthash'] = $this->bb->db->escape_string($post['posthash']);
            $attachmentassign = [
                'pid' => $this->pid,
                'posthash' => ''
            ];
            $this->bb->db->update_query('attachments', $attachmentassign, "posthash='{$post['posthash']}' AND pid='0'");
        }

        $thread_update = [];
        if ($visible == 1 && $thread['visible'] == 1) {
            $thread = $this->bb->thread->get_thread($post['tid']);

            $done_users = [];

            $subject = $this->bb->parser->parse_badwords($thread['subject']);

            $parser_options = [
                'me_username' => $post['username'],
                'filter_badwords' => 1
            ];

            $excerpt = $this->bb->parser->text_parse_message($post['message'], $parser_options);
            $excerpt = my_substr($excerpt, 0, $this->bb->settings['subscribeexcerpt']) .
                $this->bb->lang->emailbit_viewthread;

            // Fetch any users subscribed to this thread receiving instant notification
            // and queue up their subscription notices
            $query = $this->bb->db->query('
				SELECT u.username, u.email, u.uid, u.language, u.loginkey, u.salt, u.regdate, s.notification
				FROM ' . TABLE_PREFIX . 'threadsubscriptions s
				LEFT JOIN ' . TABLE_PREFIX . "users u ON (u.uid=s.uid)
				WHERE (s.notification='1' OR s.notification='2') AND s.tid='{$post['tid']}'
				AND s.uid != '{$post['uid']}'
				AND u.lastactive>'{$thread['lastpost']}'
			");

            $args = [
                'this' => &$this,
                'done_users' => &$done_users,
                'users' => []
            ];

            while ($subscribedmember = $this->bb->db->fetch_array($query)) {
                if ($done_users[$subscribedmember['uid']]) {
                    continue;
                }

                $args['users'][$subscribedmember['uid']] = (int)$subscribedmember['uid'];

                $done_users[$subscribedmember['uid']] = 1;

                $forumpermissions = $this->bb->forum->forum_permissions($thread['fid'], $subscribedmember['uid']);
                if ($forumpermissions['canview'] == 0 || $forumpermissions['canviewthreads'] == 0) {
                    continue;
                }

                if ($thread['uid'] != $subscribedmember['uid'] && $forumpermissions['canonlyviewownthread'] == 1 &&
                    !$this->bb->user->is_moderator($thread['fid'], '', $subscribedmember['uid'])
                ) {
                    // User isn't a moderator or the author of the thread...
                    continue;
                }

                if ($subscribedmember['language'] != '' &&
                    $this->bb->lang->language_exists($subscribedmember['language'])) {
                    $uselang = $subscribedmember['language'];
                } elseif ($this->bb->settings['orig_bblanguage']) {
                    $uselang = $this->bb->settings['orig_bblanguage'];
                } else {
                    $uselang = 'english';
                }

                if ($uselang == $this->bb->settings['bblanguage']) {
                    if ($subscribedmember['notification'] == 1) {
                        $emailsubject = $this->bb->lang->emailsubject_subscription;
                        $emailmessage = $this->bb->lang->email_subscription;
                    }
                } else {
                    if ($subscribedmember['notification'] == 1) {
                        if (!isset($langcache[$uselang]['emailsubject_subscription'])) {
                            $userlang = new \RunBB\Core\Language($this->bb);
//                            $userlang->set_path(MYBB_ROOT . 'Languages');
                            $userlang->set_language($uselang);
                            $userlang->load('messages');
                            $langcache[$uselang]['emailsubject_subscription'] = $userlang->emailsubject_subscription;
                            $langcache[$uselang]['email_subscription'] = $userlang->email_subscription;
                            unset($userlang);
                        }
                        $emailsubject = $langcache[$uselang]['emailsubject_subscription'];
                        $emailmessage = $langcache[$uselang]['email_subscription'];
                    }
                }

                if ($subscribedmember['notification'] == 1) {
                    $emailsubject = $this->bb->lang->sprintf($emailsubject, $subject);

                    $post_code = md5($subscribedmember['loginkey'] .
                        $subscribedmember['salt'] .
                        $subscribedmember['regdate']);
                    $emailmessage = $this->bb->lang->sprintf(
                        $emailmessage,
                        $subscribedmember['username'],
                        $post['username'],
                        $this->bb->settings['bbname'],
                        $subject,
                        $excerpt,
                        $this->bb->settings['bburl'],
                        str_replace(
                            '&amp;',
                            '&',
                            get_thread_link($thread['tid'], 0, 'newpost')
                        ),
                        $thread['tid'],
                        $post_code
                    );
                    $new_email = [
                        'mailto' => $this->bb->db->escape_string($subscribedmember['email']),
                        'mailfrom' => '',
                        'subject' => $this->bb->db->escape_string($emailsubject),
                        'message' => $this->bb->db->escape_string($emailmessage),
                        'headers' => ''
                    ];
                    $this->bb->db->insert_query('mailqueue', $new_email);
                    unset($userlang);
                    $queued_email = 1;
                } elseif ($subscribedmember['notification'] == 2) {
                    $post_code = md5($subscribedmember['loginkey'] . $subscribedmember['salt'] .
                        $subscribedmember['regdate']);
                    $pm = [
                        'subject' => ['pmsubject_subscription', $subject],
                        'message' => ['pm_subscription', $subscribedmember['username'],
                            $post['username'], $subject, $excerpt, $this->bb->settings['bburl'],
                            str_replace('&amp;', '&', get_thread_link($thread['tid'], 0, 'newpost')),
                            $thread['tid'], $post_code],
                        'touid' => $subscribedmember['uid'],
                        'language' => $subscribedmember['language'],
                        'language_file' => 'messages'
                    ];
                    $this->pm->send_pm($pm, -1, true);
                }
            }

            $this->bb->plugins->runHooks('datahandler_post_insert_subscribed', $args);

            // Have one or more emails been queued? Update the queue count
            if (isset($queued_email) && $queued_email == 1) {
                $this->bb->cache->update_mailqueue();
            }

            $thread_update = ['replies' => '+1'];

            // Update forum count
            $this->bb->thread->update_last_post($post['tid']);
            $this->bb->forum->update_forum_counters($post['fid'], ['posts' => '+1']);
            $this->bb->forum->update_forum_lastpost($thread['fid']);
        } // Post is stuck in moderation queue
        elseif ($visible == 0) {
            // Update the unapproved posts count for the current thread and current forum
            $thread_update = ['unapprovedposts' => '+1'];
            $this->bb->thread->update_thread_counters($post['tid'], ['unapprovedposts' => '+1']);
            $this->bb->forum->update_forum_counters($post['fid'], ['unapprovedposts' => '+1']);
        } elseif ($thread['visible'] == 0) {
            // Update the unapproved posts count for the current forum
            $thread_update = ['replies' => '+1'];
            $this->bb->forum->update_forum_counters($post['fid'], ['unapprovedposts' => '+1']);
        } elseif ($thread['visible'] == -1) {
            // Update the unapproved posts count for the current forum
            $thread_update = ['replies' => '+1'];
            $this->bb->forum->update_forum_counters($post['fid'], ['deletedposts' => '+1']);
        }

        $query = $this->bb->db->simple_select(
            'attachments',
            'COUNT(aid) AS attachmentcount',
            "pid='{$this->pid}' AND visible='1'"
        );
        $attachmentcount = $this->bb->db->fetch_field($query, 'attachmentcount');
        if ($attachmentcount > 0) {
            $thread_update['attachmentcount'] = "+{$attachmentcount}";
        }
        $this->bb->thread->update_thread_counters($post['tid'], $thread_update);

        // Return the post's pid and whether or not it is visible.
        $this->return_values = [
            'pid' => $this->pid,
            'visible' => $visible,
            'closed' => $closed
        ];

        $this->bb->plugins->runHooks('datahandler_post_insert_post_end', $this);

        return $this->return_values;
    }

    /**
     * Validate a thread.
     *
     * @return boolean True when valid, false when invalid.
     */
    public function validateThread()
    {
        $thread = &$this->data;

        // Validate all thread assets.

        if (!$thread['savedraft']) {
            $this->verifyPostFlooding();
        }

        if ($this->method == 'insert' || array_key_exists('uid', $thread)) {
            $this->verifyAuthor();
        }

        if ($this->method == 'insert' || array_key_exists('prefix', $thread)) {
            $this->verifyPrefix();
        }

        if ($this->method == 'insert' || array_key_exists('subject', $thread)) {
            $this->verifySubject();
        }

        if ($this->method == 'insert' || array_key_exists('message', $thread)) {
            $this->verifyMessage();
            $this->verifyImageCount();
            $this->verifyVideoCount();
        }

        if ($this->method == 'insert' || array_key_exists('dateline', $thread)) {
            $this->verifyDateline();
        }

        if ($this->method == 'insert' || array_key_exists('icon', $thread)) {
            $this->verifyPostIcon();
        }

        if ($this->method == 'insert' || array_key_exists('options', $thread)) {
            $this->verifyOptions();
        }

        $this->bb->plugins->runHooks('datahandler_post_validate_thread', $this);

        // We are done validating, return.
        $this->set_validated(true);
        if (count($this->get_errors()) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Insert a thread into the database.
     *
     * @return array Array of new thread details, tid and visibility.
     */
    public function insertThread()
    {
        // Yes, validating is required.
        if (!$this->get_validated()) {
            die('The thread needs to be validated before inserting it into the DB.');
        }
        if (count($this->get_errors()) > 0) {
            die('The thread is not valid.');
        }

        $thread = &$this->data;

        // Fetch the forum this thread is being made in
        $forum = $this->bb->forum->get_forum($thread['fid']);

        // This thread is being saved as a draft.
        if ($thread['savedraft']) {
            $visible = -2;
        } // Thread is being made now and we have a bit to do.
        else {
            $forumpermissions = $this->bb->forum->forum_permissions($thread['fid'], $thread['uid']);
            // Decide on the visibility of this post.
            if ($forumpermissions['modthreads'] == 1 &&
                !$this->bb->user->is_moderator($thread['fid'], '', $thread['uid'])
            ) {
                $visible = 0;
            } else {
                $visible = 1;
            }

            // Are posts from this user being moderated? Change visibility
            if ($this->bb->user->uid == $thread['uid'] && $this->bb->user->moderateposts == 1) {
                $visible = 0;
            }
        }

        // Have a post ID but not a thread ID - fetch thread ID
        if (!empty($thread['pid']) && !$thread['tid']) {
            $query = $this->bb->db->simple_select('posts', 'tid', "pid='{$thread['pid']}");
            $thread['tid'] = $this->bb->db->fetch_field($query, 'tid');
        }

        if (isset($thread['pid']) && $thread['pid'] > 0) {
            $query = $this->bb->db->simple_select(
                'posts',
                'pid',
                "pid='{$thread['pid']}' AND uid='{$thread['uid']}' AND visible='-2'"
            );
            $draft_check = $this->bb->db->fetch_field($query, 'pid');
        } else {
            $draft_check = false;
        }

        // Are we updating a post which is already a draft? Perhaps changing it into a visible post?
        if ($draft_check) {
            $this->thread_insert_data = [
                'subject' => $this->bb->db->escape_string($thread['subject']),
                'icon' => (int)$thread['icon'],
                'username' => $this->bb->db->escape_string($thread['username']),
                'dateline' => (int)$thread['dateline'],
                'lastpost' => (int)$thread['dateline'],
                'lastposter' => $this->bb->db->escape_string($thread['username']),
                'visible' => $visible
            ];

            $this->bb->plugins->runHooks('datahandler_post_insert_thread', $this);

            $this->bb->db->update_query('threads', $this->thread_insert_data, "tid='{$thread['tid']}'");

            $this->post_insert_data = [
                'subject' => $this->bb->db->escape_string($thread['subject']),
                'icon' => (int)$thread['icon'],
                'username' => $this->bb->db->escape_string($thread['username']),
                'dateline' => (int)$thread['dateline'],
                'message' => $this->bb->db->escape_string($thread['message']),
                'ipaddress' => $this->bb->session->ipaddress,
                'includesig' => $thread['options']['signature'],
                'smilieoff' => $thread['options']['disablesmilies'],
                'visible' => $visible
            ];
            $this->bb->plugins->runHooks('datahandler_post_insert_thread_post', $this);

            $this->bb->db->update_query('posts', $this->post_insert_data, "pid='{$thread['pid']}'");
            $this->tid = $thread['tid'];
            $this->pid = $thread['pid'];
        } // Inserting a new thread into the database.
        else {
            $this->thread_insert_data = [
                'fid' => $thread['fid'],
                'subject' => $this->bb->db->escape_string($thread['subject']),
                'prefix' => (int)$thread['prefix'],
                'icon' => (int)$thread['icon'],
                'uid' => $thread['uid'],
                'username' => $this->bb->db->escape_string($thread['username']),
                'dateline' => (int)$thread['dateline'],
                'lastpost' => (int)$thread['dateline'],
                'lastposter' => $this->bb->db->escape_string($thread['username']),
                'lastposteruid' => $thread['uid'],
                'views' => 0,
                'replies' => 0,
                'visible' => $visible,
                'notes' => ''
            ];

            $this->bb->plugins->runHooks('datahandler_post_insert_thread', $this);

            $this->tid = $this->bb->db->insert_query('threads', $this->thread_insert_data);

            $this->post_insert_data = [
                'tid' => $this->tid,
                'fid' => $thread['fid'],
                'subject' => $this->bb->db->escape_string($thread['subject']),
                'icon' => (int)$thread['icon'],
                'uid' => $thread['uid'],
                'username' => $this->bb->db->escape_string($thread['username']),
                'dateline' => (int)$thread['dateline'],
                'message' => $this->bb->db->escape_string($thread['message']),
                'ipaddress' => $this->bb->session->ipaddress,
                'includesig' => $thread['options']['signature'],
                'smilieoff' => $thread['options']['disablesmilies'],
                'visible' => $visible
            ];
            $this->bb->plugins->runHooks('datahandler_post_insert_thread_post', $this);

            $this->pid = $this->bb->db->insert_query('posts', $this->post_insert_data);

            // Now that we have the post id for this first post, update the threads table.
            $firstpostup = ['firstpost' => $this->pid];
            $this->bb->db->update_query('threads', $firstpostup, "tid='{$this->tid}'");
        }

        // If we're not saving a draft there are some things we need to check now
        if (!$thread['savedraft']) {
            if ($thread['options']['subscriptionmethod'] != '' && $thread['uid'] > 0) {
                switch ($thread['options']['subscriptionmethod']) {
                    case 'pm':
                        $notification = 2;
                        break;
                    case 'email':
                        $notification = 1;
                        break;
                    default:
                        $notification = 0;
                }
                $this->bb->user->add_subscribed_thread($this->tid, $notification, $thread['uid']);
            }

            // Perform any selected moderation tools.
            if ($this->bb->user->is_moderator($thread['fid'], '', $thread['uid']) && is_array($thread['modoptions'])) {
                $this->bb->lang->load($this->language_file, true);

                $modoptions = $thread['modoptions'];
                $modlogdata['fid'] = $thread['fid'];
                if (isset($thread['tid'])) {
                    $modlogdata['tid'] = $thread['tid'];
                }

                $modoptions_update = [];

                // Close the thread.
                if (!empty($modoptions['closethread'])) {
                    $modoptions_update['closed'] = 1;
                    $this->bblogger->log_moderator_action($modlogdata, $this->bb->lang->thread_closed);
                }

                // Stick the thread.
                if (!empty($modoptions['stickthread'])) {
                    $modoptions_update['sticky'] = 1;
                    $this->bblogger->log_moderator_action($modlogdata, $this->bb->lang->thread_stuck);
                }

                // Execute moderation options.
                if ($modoptions_update) {
                    $this->bb->db->update_query('threads', $modoptions_update, "tid='{$this->tid}'");
                }
            }
            if ($visible == 1) {
                // If we have a registered user then update their post count and last post times.
                if ($thread['uid'] > 0) {
                    $user = $this->bb->user->get_user($thread['uid']);
                    $update_query = [];
                    // Only update the lastpost column of the user if the date of the thread
                    // is newer than their last post.
                    if ($thread['dateline'] > $user->lastpost) {
                        // Yes this has a single quote within a double quote. It's not a bug.
                        $update_query['lastpost'] = "'{$thread['dateline']}'";
                    }
                    // Update the post count if this forum allows post counts to be tracked
                    if ($forum['usepostcounts'] != 0) {
                        $update_query['postnum'] = 'postnum+1';
                    }
                    if ($forum['usethreadcounts'] != 0) {
                        $update_query['threadnum'] = 'threadnum+1';
                    }

                    // Only update the table if we need to.
                    if (!empty($update_query)) {
                        $this->bb->db->update_query('users', $update_query, "uid='{$thread['uid']}'", 1, true);
                    }
                }

                if (!isset($forum['lastpost'])) {
                    $forum['lastpost'] = 0;
                }

                $done_users = [];

                // Queue up any forum subscription notices to users who are subscribed to this forum.
                $excerpt = my_substr($thread['message'], 0, $this->bb->settings['subscribeexcerpt']) .
                    $this->bb->lang->emailbit_viewthread;

                // Parse badwords
                $excerpt = $this->bb->parser->parse_badwords($excerpt);

                $query = $this->bb->db->query('
					SELECT u.username, u.email, u.uid, u.language, u.loginkey, u.salt, u.regdate
					FROM ' . TABLE_PREFIX . 'forumsubscriptions fs
					LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=fs.uid)
					LEFT JOIN ' . TABLE_PREFIX . "usergroups g ON (g.gid=u.usergroup)
					WHERE fs.fid='" . (int)$thread['fid'] . "'
					AND fs.uid != '" . (int)$thread['uid'] . "'
					AND u.lastactive > '{$forum['lastpost']}'
					AND g.isbannedgroup != 1
				");
                while ($subscribedmember = $this->bb->db->fetch_array($query)) {
                    if ($done_users[$subscribedmember['uid']]) {
                        continue;
                    }
                    $done_users[$subscribedmember['uid']] = 1;

                    $forumpermissions = $this->bb->forum->forum_permissions($thread['fid'], $subscribedmember['uid']);
                    if ($forumpermissions['canview'] == 0 || $forumpermissions['canviewthreads'] == 0) {
                        continue;
                    }

                    if (!$this->bb->user->is_moderator($thread['fid'], '', $subscribedmember['uid']) &&
                        $forumpermissions['canonlyviewownthreads'] == 1
                    ) {
                        // In a 'view own only' forum and not a moderator
                        continue;
                    }

                    // Determine the language pack we'll be using to send this email in and load it if it isn't already.
                    if ($subscribedmember['language'] != '' &&
                        $this->bb->lang->language_exists($subscribedmember['language'])) {
                        $uselang = $subscribedmember['language'];
                    } elseif ($this->bb->settings['bblanguage']) {
                        $uselang = $this->bb->settings['bblanguage'];
                    } else {
                        $uselang = 'english';
                    }

                    if ($uselang == $this->bb->settings['bblanguage']) {
                        $emailsubject = $this->bb->lang->emailsubject_forumsubscription;
                        $emailmessage = $this->bb->lang->email_forumsubscription;
                    } else {
                        if (!isset($langcache[$uselang]['emailsubject_forumsubscription'])) {
                            $userlang = new \RunBB\Core\Language($this->bb);
//                            $userlang->set_path(MYBB_ROOT . 'Languages');
                            $userlang->set_language($uselang);
                            $userlang->load('messages');
                            $langcache[$uselang]['emailsubject_forumsubscription'] =
                                $userlang->emailsubject_forumsubscription;
                            $langcache[$uselang]['email_forumsubscription'] = $userlang->email_forumsubscription;
                            unset($userlang);
                        }
                        $emailsubject = $langcache[$uselang]['emailsubject_forumsubscription'];
                        $emailmessage = $langcache[$uselang]['email_forumsubscription'];
                    }
                    $emailsubject = $this->bb->lang->sprintf($emailsubject, $forum['name']);

                    $post_code = md5($subscribedmember['loginkey'] . $subscribedmember['salt'] .
                        $subscribedmember['regdate']);
                    $emailmessage = $this->bb->lang->sprintf(
                        $emailmessage,
                        $subscribedmember['username'],
                        $thread['username'],
                        $forum['name'],
                        $this->bb->settings['bbname'],
                        $thread['subject'],
                        $excerpt,
                        $this->bb->settings['bburl'],
                        get_thread_link($this->tid),
                        $thread['fid'],
                        $post_code
                    );
                    $new_email = [
                        'mailto' => $this->bb->db->escape_string($subscribedmember['email']),
                        'mailfrom' => '',
                        'subject' => $this->bb->db->escape_string($emailsubject),
                        'message' => $this->bb->db->escape_string($emailmessage),
                        'headers' => ''
                    ];
                    $this->bb->db->insert_query('mailqueue', $new_email);
                    unset($userlang);
                    $queued_email = 1;
                }
                // Have one or more emails been queued? Update the queue count
                if (isset($queued_email) && $queued_email == 1) {
                    $this->bb->cache->update_mailqueue();
                }
            }
        }

        // Assign any uploaded attachments with the specific posthash to the newly created post.
        if ($thread['posthash']) {
            $thread['posthash'] = $this->bb->db->escape_string($thread['posthash']);
            $attachmentassign = [
                'pid' => $this->pid,
                'posthash' => ''
            ];
            $this->bb->db->update_query(
                'attachments',
                $attachmentassign,
                "posthash='{$thread['posthash']}' AND pid='0'"
            );
        }

        if ($visible == 1) {
            $this->bb->thread->update_last_post($this->tid);
            $this->bb->forum->update_forum_counters($thread['fid'], ['threads' => '+1', 'posts' => '+1']);
            $this->bb->forum->update_forum_lastpost($thread['fid']);
        } elseif ($visible == 0) {
            $this->bb->forum->update_forum_counters(
                $thread['fid'],
                ['unapprovedthreads' => '+1', 'unapprovedposts' => '+1']
            );
        }

        $query = $this->bb->db->simple_select(
            'attachments',
            'COUNT(aid) AS attachmentcount',
            "pid='{$this->pid}' AND visible='1'"
        );
        $attachmentcount = $this->bb->db->fetch_field($query, 'attachmentcount');
        if ($attachmentcount > 0) {
            $this->bb->thread->update_thread_counters($this->tid, ['attachmentcount' => "+{$attachmentcount}"]);
        }

        // Return the post's pid and whether or not it is visible.
        $this->return_values = [
            'pid' => $this->pid,
            'tid' => $this->tid,
            'visible' => $visible
        ];

        $this->bb->plugins->runHooks('datahandler_post_insert_thread_end', $this);

        return $this->return_values;
    }

    /**
     * Updates a post that is already in the database.
     *
     * @return array
     */
    public function updatePost()
    {
        // Yes, validating is required.
        if ($this->get_validated() != true) {
            die('The post needs to be validated before inserting it into the DB.');
        }
        if (count($this->get_errors()) > 0) {
            die('The post is not valid.');
        }

        $post = &$this->data;

        $post['pid'] = (int)$post['pid'];

        $existing_post = $this->bb->post->get_post($post['pid']);
        $post['tid'] = $existing_post['tid'];
        $post['fid'] = $existing_post['fid'];

        $forum = $this->bb->forum->get_forum($post['fid']);
        $forumpermissions = $this->bb->forum->forum_permissions($post['fid'], $post['uid']);

        // Decide on the visibility of this post.
        $ismod = $this->bb->user->is_moderator($post['fid'], '', $post['uid']);

        // Keep visibility for unapproved and deleted posts
        if ($existing_post['visible'] == 0) {
            $visible = 0;
        } elseif ($existing_post['visible'] == -1) {
            $visible = -1;
        } elseif ($forumpermissions['mod_edit_posts'] == 1 && !$ismod) {
            $visible = 0;
            $this->moderation->unapprove_posts([$post['pid']]);
        } else {
            $visible = 1;
        }

        // Update the thread details that might have been changed first.
        if ($this->first_post) {
            $this->tid = $post['tid'];

            if (isset($post['prefix'])) {
                $this->thread_update_data['prefix'] = (int)$post['prefix'];
            }

            if (isset($post['subject'])) {
                $this->thread_update_data['subject'] = $this->bb->db->escape_string($post['subject']);
            }

            if (isset($post['icon'])) {
                $this->thread_update_data['icon'] = (int)$post['icon'];
            }
            if (count($this->thread_update_data) > 0) {
                $this->bb->plugins->runHooks('datahandler_post_update_thread', $this);

                $this->bb->db->update_query('threads', $this->thread_update_data, "tid='" . (int)$post['tid'] . "'");
            }
        }

        // Prepare array for post updating.

        $this->pid = $post['pid'];

        if (isset($post['subject'])) {
            $this->post_update_data['subject'] = $this->bb->db->escape_string($post['subject']);
        }

        if (isset($post['message'])) {
            $this->post_update_data['message'] = $this->bb->db->escape_string($post['message']);
        }

        if (isset($post['editreason']) && trim($post['editreason']) != '') {
            $this->post_update_data['editreason'] = $this->bb->db->escape_string(trim($post['editreason']));
        }

        if (isset($post['icon'])) {
            $this->post_update_data['icon'] = (int)$post['icon'];
        }

        if (isset($post['options'])) {
            if (isset($post['options']['disablesmilies'])) {
                $this->post_update_data['smilieoff'] = $this->bb->db->escape_string($post['options']['disablesmilies']);
            }
            if (isset($post['options']['signature'])) {
                $this->post_update_data['includesig'] = $this->bb->db->escape_string($post['options']['signature']);
            }
        }

        // If we need to show the edited by, let's do so.
        if (($this->bb->settings['showeditedby'] == 1 && !$this->bb->user->is_moderator(
            $post['fid'],
            'caneditposts',
            $post['edit_uid']
        )) || ($this->bb->settings['showeditedbyadmin'] == 1 &&
                $this->bb->user->is_moderator($post['fid'], 'caneditposts', $post['edit_uid']))
        ) {
            $this->post_update_data['edituid'] = (int)$post['edit_uid'];
            $this->post_update_data['edittime'] = TIME_NOW;
        }

        $this->bb->plugins->runHooks('datahandler_post_update', $this);

        $this->bb->db->update_query('posts', $this->post_update_data, "pid='" . (int)$post['pid'] . "'");

        // Automatic subscription to the thread
        if ($post['options']['subscriptionmethod'] != '' && $post['uid'] > 0) {
            switch ($post['options']['subscriptionmethod']) {
                case 'pm':
                    $notification = 2;
                    break;
                case 'email':
                    $notification = 1;
                    break;
                default:
                    $notification = 0;
            }
            $this->bb->user->add_subscribed_thread($post['tid'], $notification, $post['uid']);
        } else {
            $this->bb->db->delete_query(
                'threadsubscriptions',
                "uid='" . (int)$post['uid'] . "' AND tid='" . (int)$post['tid'] . "'"
            );
        }

        $this->bb->forum->update_forum_lastpost($post['fid']);
        $this->bb->thread->update_last_post($post['tid']);

        // Return the thread's first post id and whether or not it is visible.
        $this->return_values = [
            'visible' => $visible,
            'first_post' => $this->first_post
        ];

        $this->bb->plugins->runHooks('datahandler_post_update_end', $this);

        return $this->return_values;
    }
}
