<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\DataHandlers;

use RunBB\Core\DataHandler;

/**
 * User handling class, provides common structure to handle user data.
 *
 */
class UserDataHandler extends DataHandler
{
    /**
     * The language file used in the data handler.
     *
     * @var string
     */
    public $language_file = 'datahandler_user';

    /**
     * The prefix for the language variables used in the data handler.
     *
     * @var string
     */
    public $language_prefix = 'userdata';

    /**
     * Array of data inserted in to a user.
     *
     * @var array
     */
    public $user_insert_data = [];

    /**
     * Array of data used to update a user.
     *
     * @var array
     */
    public $user_update_data = [];

    /**
     * User ID currently being manipulated by the datahandlers.
     *
     * @var int
     */
    public $uid = 0;

    /**
     * Values to be returned after inserting/deleting an user.
     *
     * @var array
     */
    public $return_values = [];

    /**
     * @var array
     */
    protected $delete_uids = [];

    /**
     * @var int
     */
    protected $deleted_users = 0;

    /**
     * Verifies if a username is valid or invalid.
     *
     * @return boolean True when valid, false when invalid.
     */
    public function verify_username()
    {
        $username = &$this->data['username'];

        // Fix bad characters
        $username = trim_blank_chrs($username);
        $username = str_replace(
            [unichr(160), unichr(173), unichr(0xCA), dec_to_utf8(8238), dec_to_utf8(8237), dec_to_utf8(8203)],
            [' ', '-', '', '', '', ''],
            $username
        );

        // Remove multiple spaces from the username
        $username = preg_replace("#\s{2,}#", ' ', $username);

        // Check if the username is not empty.
        if ($username == '') {
            $this->setError('missing_username');
            return false;
        }

        // Check if the username belongs to the list of banned usernames.
        if ($this->bb->ban->is_banned_username($username, true)) {
            $this->setError('banned_username');
            return false;
        }

        // Check for certain characters in username (<, >, &, commas and slashes)
        if (strpos($username, '<') !== false ||
            strpos($username, '>') !== false ||
            strpos($username, '&') !== false ||
            my_strpos($username, '\\') !== false ||
            strpos($username, ';') !== false ||
            strpos($username, ',') !== false ||
            !validate_utf8_string($username, false, false)
        ) {
            $this->setError('bad_characters_username');
            return false;
        }

        // Check if the username is of the correct length.
        if (($this->bb->settings['maxnamelength'] != 0 && my_strlen($username) > $this->bb->settings['maxnamelength']) ||
            ($this->bb->settings['minnamelength'] != 0 && my_strlen($username) < $this->bb->settings['minnamelength'])
        ) {
            $this->setError('invalid_username_length', [$this->bb->settings['minnamelength'], $this->bb->settings['maxnamelength']]);
            return false;
        }

        return true;
    }

    /**
     * Verifies if a usertitle is valid or invalid.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_usertitle()
    {
        $usertitle = &$this->data['usertitle'];

        // Check if the usertitle is of the correct length.
        if ($this->bb->settings['customtitlemaxlength'] != 0 &&
            my_strlen($usertitle) > $this->bb->settings['customtitlemaxlength']
        ) {
            $this->setError('invalid_usertitle_length', $this->bb->settings['customtitlemaxlength']);
            return false;
        }

        return true;
    }

    /**
     * Verifies if a username is already in use or not.
     *
     * @return boolean False when the username is not in use, true when it is.
     */
    public function verify_username_exists()
    {
        $username = &$this->data['username'];

        $user = $this->bb->user->get_user_by_username(trim($username));

        if (!empty($this->data['uid']) && !empty($user['uid']) && $user['uid'] == $this->data['uid']) {
            unset($user);
        }

        if (!empty($user['uid'])) {
            $this->setError('username_exists', [$username]);
            return true;
        }

        return false;
    }

    /**
     * Verifies if a new password is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    function verify_password()
    {
        $user = &$this->data;

        // Always check for the length of the password.
        if (my_strlen($user['password']) < $this->bb->settings['minpasswordlength'] ||
            my_strlen($user['password']) > $this->bb->settings['maxpasswordlength']
        ) {
            $this->setError(
                'invalid_password_length',
                [$this->bb->settings['minpasswordlength'], $this->bb->settings['maxpasswordlength']]
            );
            return false;
        }

        // Has the user tried to use their email address or username as a password?
        if ($user['email'] === $user['password'] || $user['username'] === $user['password']) {
            $this->setError('bad_password_security');
            return false;
        }

        // See if the board has "require complex passwords" enabled.
        if ($this->bb->settings['requirecomplexpasswords'] == 1) {
            // Complex passwords required, do some extra checks.
            // First, see if there is one or more complex character(s) in the password.
            if (!preg_match("/^.*(?=.{" . $this->bb->settings['minpasswordlength'] . ",})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $user['password'])) {
                $this->setError('no_complex_characters', [$this->bb->settings['minpasswordlength']]);
                return false;
            }
        }

        // If we have a "password2" check if they both match
        if (isset($user['password2']) && $user['password'] !== $user['password2']) {
            $this->setError('passwords_dont_match');
            return false;
        }

        // MD5 the password
        $user['md5password'] = md5($user['password']);

        // Generate our salt
        $user['salt'] = $this->bb->user->generate_salt();

        // Combine the password and salt
        $user['saltedpw'] = $this->bb->user->salt_password($user['md5password'], $user['salt']);

        // Generate the user login key
        $user['loginkey'] = $this->bb->user->generate_loginkey();

        return true;
    }

    /**
     * Verifies usergroup selections and other group details.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_usergroup()
    {
        return true;
    }

    /**
     * Verifies if an email address is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_email()
    {
        $user = &$this->data;

        // Check if an email address has actually been entered.
        if (trim_blank_chrs($user['email']) == '') {
            $this->setError('missing_email');
            return false;
        }

        // Check if this is a proper email address.
        if (!validate_email_format($user['email'])) {
            $this->setError('invalid_email_format');
            return false;
        }

        // Check banned emails
        if ($this->bb->ban->is_banned_email($user['email'], true)) {
            $this->setError('banned_email');
            return false;
        }

        // Check signed up emails
        // Ignore the ACP because the Merge System sometimes produces users with duplicate email addresses (Not A Bug)
        if ($this->bb->settings['allowmultipleemails'] == 0 && !defined('IN_ADMINCP')) {
            $uid = 0;
            if (isset($user['uid'])) {
                $uid = $user['uid'];
            }
            if ($this->bb->ban->email_already_in_use($user['email'], $uid)) {
                $this->setError('email_already_in_use');
                return false;
            }
        }

        // If we have an "email2", verify it matches the existing email
        if (isset($user['email2']) && $user['email'] != $user['email2']) {
            $this->setError('emails_dont_match');
            return false;
        }

        return true;
    }

    /**
     * Verifies if a website is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_website()
    {
        $website = &$this->data['website'];

        if (empty($website) ||
            my_strtolower($website) == 'http://' ||
            my_strtolower($website) == 'https://'
        ) {
            $website = '';
            return true;
        }

        // Does the website start with http(s)://?
        if (my_strtolower(substr($website, 0, 4)) != 'http') {
            // Website does not start with http://, let's see if the user forgot.
            $website = 'http://' . $website;
        }

        if (!filter_var($website, FILTER_VALIDATE_URL)) {
            $this->setError('invalid_website');
            return false;
        }

        return true;
    }

    /**
     * Verifies if an ICQ number is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_icq()
    {
        $icq = &$this->data['icq'];

        if ($icq != '' && !is_numeric($icq)) {
            $this->setError('invalid_icq_number');
            return false;
        }
        $icq = (int)$icq;
        return true;
    }

    /**
     * Verifies if a birthday is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_birthday()
    {
        $user = &$this->data;
        $birthday = &$user['birthday'];

        if (!is_array($birthday)) {
            return true;
        }

        // Sanitize any input we have
        $birthday['day'] = (int)$birthday['day'];
        $birthday['month'] = (int)$birthday['month'];
        $birthday['year'] = (int)$birthday['year'];

        // Error if a day and month exists, and the birthday day and range is not in range
        if ($birthday['day'] != 0 || $birthday['month'] != 0) {
            if ($birthday['day'] < 1 || $birthday['day'] > 31 ||
                $birthday['month'] < 1 || $birthday['month'] > 12 ||
                ($birthday['month'] == 2 && $birthday['day'] > 29)
            ) {
                $this->setError('invalid_birthday');
                return false;
            }
        }

        // Check if the day actually exists.
        $months = get_bdays($birthday['year']);
        if ($birthday['month'] != 0 && $birthday['day'] > $months[$birthday['month'] - 1]) {
            $this->setError('invalid_birthday');
            return false;
        }

        // Error if a year exists and the year is out of range
        if ($birthday['year'] != 0 &&
            ($birthday['year'] < (date('Y') - 100)) ||
            $birthday['year'] > date('Y')
        ) {
            $this->setError('invalid_birthday');
            return false;
        } elseif ($birthday['year'] == date('Y')) {
            // Error if birth date is in future
            if ($birthday['month'] > date('m') || ($birthday['month'] == date('m') && $birthday['day'] > date('d'))) {
                $this->setError('invalid_birthday');
                return false;
            }
        }

        // Error if COPPA is on, and the user hasn't verified their age / under 13
        if ($this->bb->settings['coppa'] == 'enabled' && ($birthday['year'] == 0 || !$birthday['year'])) {
            $this->setError('invalid_birthday_coppa');
            return false;
        } elseif (($this->bb->settings['coppa'] == 'deny' && $birthday['year'] > (date('Y') - 13)) &&
            !$this->bb->user->is_moderator()
        ) {
            $this->setError('invalid_birthday_coppa2');
            return false;
        }

        // Make the user's birthday field
        if ($birthday['year'] != 0) {
            // If the year is specified, put together a d-m-y string
            $user['bday'] = $birthday['day'] . '-' . $birthday['month'] . '-' . $birthday['year'];
        } elseif ($birthday['day'] && $birthday['month']) {
            // If only a day and month are specified, put together a d-m string
            $user['bday'] = $birthday['day'] . '-' . $birthday['month'] . '-';
        } else {
            // No field is specified, so return an empty string for an unknown birthday
            $user['bday'] = '';
        }
        return true;
    }

    /**
     * Verifies if the birthday privacy option is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_birthday_privacy()
    {
        $birthdayprivacy = &$this->data['birthdayprivacy'];
        $accepted = [
            'none',
            'age',
            'all'
        ];

        if (!in_array($birthdayprivacy, $accepted)) {
            $this->setError('invalid_birthday_privacy');
            return false;
        }
        return true;
    }

    /**
     * Verifies if the post count field is filled in correctly.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_postnum()
    {
        $user = &$this->data;

        if (isset($user['postnum']) && $user['postnum'] < 0) {
            $this->setError('invalid_postnum');
            return false;
        }

        return true;
    }

    /**
     * Verifies if the thread count field is filled in correctly.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_threadnum()
    {
        $user = &$this->data;

        if (isset($user['threadnum']) && $user['threadnum'] < 0) {
            $this->setError('invalid_threadnum');
            return false;
        }

        return true;
    }

    /**
     * Verifies if a profile fields are filled in correctly.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_profile_fields()
    {
        $user = &$this->data;
        $profile_fields = &$this->data['profile_fields'];
        // Loop through profile fields checking if they exist or not and are filled in.

        // Fetch all profile fields first.
        $pfcache = $this->bb->cache->read('profilefields');

        if (is_array($pfcache)) {
            // Then loop through the profile fields.
            foreach ($pfcache as $profilefield) {
                if ((isset($this->data['profile_fields_editable']) ||
                        isset($this->data['registration'])) &&
                    ($profilefield['required'] == 1 || $profilefield['registration'] == 1)
                ) {
                    $profilefield['editableby'] = -1;
                }

                if (isset($user['usergroup'])) {
                    if (isset($user['additionalgroups'])) {
                        $checkVars = ['usergroup' => $user['usergroup'], 'additionalgroups' => $user['additionalgroups']];
                    } else {
                        $checkVars = ['usergroup' => $user['usergroup']];
                    }

                    if (!$this->bb->user->is_member($profilefield['editableby'], $checkVars)) {
                        continue;
                    }
                }

                // Does this field have a minimum post count?
                if (!isset($this->data['profile_fields_editable']) &&
                    !empty($profilefield['postnum']) &&
                    $profilefield['postnum'] > $user['postnum']
                ) {
                    continue;
                }

                $profilefield['type'] = htmlspecialchars_uni($profilefield['type']);
                $profilefield['name'] = htmlspecialchars_uni($profilefield['name']);
                $thing = explode("\n", $profilefield['type'], '2');
                $type = trim($thing[0]);
                $field = "fid{$profilefield['fid']}";

                if (!isset($profile_fields[$field])) {
                    $profile_fields[$field] = '';
                }

                // If the profile field is required, but not filled in, present error.
                if ($type != 'multiselect' && $type != 'checkbox') {
                    if (trim($profile_fields[$field]) == '' && $profilefield['required'] == 1 &&
                        !defined('IN_ADMINCP') && $this->bb->current_page !== 'modcp'
                    ) {
                        $this->setError('missing_required_profile_field', [$profilefield['name']]);
                    }
                } elseif (($type == 'multiselect' || $type == 'checkbox') && $profile_fields[$field] == '' &&
                    $profilefield['required'] == 1 && !defined('IN_ADMINCP') && $this->bb->current_page !== 'modcp'
                ) {
                    $this->setError('missing_required_profile_field', [$profilefield['name']]);
                }

                // Sort out multiselect/checkbox profile fields.
                $options = '';
                if (($type == 'multiselect' || $type == 'checkbox') && is_array($profile_fields[$field])) {
                    $expoptions = explode("\n", $thing[1]);
                    $expoptions = array_map('trim', $expoptions);
                    foreach ($profile_fields[$field] as $value) {
                        if (!in_array(htmlspecialchars_uni($value), $expoptions)) {
                            $this->setError('bad_profile_field_values', [$profilefield['name']]);
                        }
                        if ($options) {
                            $options .= "\n";
                        }
                        $options .= $this->bb->db->escape_string($value);
                    }
                } elseif ($type == 'select' || $type == 'radio') {
                    $expoptions = explode("\n", $thing[1]);
                    $expoptions = array_map('trim', $expoptions);
                    if (!in_array(htmlspecialchars_uni($profile_fields[$field]), $expoptions) && trim($profile_fields[$field]) != '') {
                        $this->setError('bad_profile_field_values', [$profilefield['name']]);
                    }
                    $options = $this->bb->db->escape_string($profile_fields[$field]);
                } else {
                    if ($profilefield['maxlength'] > 0 && my_strlen($profile_fields[$field]) > $profilefield['maxlength']) {
                        $this->setError('max_limit_reached', [$profilefield['name'], $profilefield['maxlength']]);
                    }

                    if (!empty($profilefield['regex']) && !preg_match('#' . $profilefield['regex'] . '#i', $profile_fields[$field])) {
                        $this->setError('bad_profile_field_value', [$profilefield['name']]);
                    }

                    $options = $this->bb->db->escape_string($profile_fields[$field]);
                }
                $user['user_fields'][$field] = $options;
            }
        }

        return true;
    }

    /**
     * Verifies if an optionally entered referrer exists or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_referrer()
    {
        $user = &$this->data;

        // Does the referrer exist or not?
//        if ($this->bb->settings['usereferrals'] == 1 && $user->referrer != '') {
        if ($this->bb->settings['usereferrals'] == 1 && isset($user['referrer'])) {
            $referrer = $this->bb->user->get_user_by_username($user['referrer']);

            if (empty($referrer['uid'])) {
                $this->setError('invalid_referrer', [$user['referrer']]);
                return false;
            }

            $user['referrer_uid'] = $referrer['uid'];
        } else {
            $user['referrer_uid'] = 0;
        }

        return true;
    }

    /**
     * Verifies user options.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verifyOptions()
    {
        $options = &$this->data['options'];

        // Verify yes/no options.
        $this->verify_yesno_option($options, 'allownotices', 1);
        $this->verify_yesno_option($options, 'hideemail', 0);
        $this->verify_yesno_option($options, 'receivepms', 1);
        $this->verify_yesno_option($options, 'receivefrombuddy', 0);
        $this->verify_yesno_option($options, 'pmnotice', 1);
        $this->verify_yesno_option($options, 'pmnotify', 1);
        $this->verify_yesno_option($options, 'invisible', 0);
        $this->verify_yesno_option($options, 'showimages', 1);
        $this->verify_yesno_option($options, 'showvideos', 1);
        $this->verify_yesno_option($options, 'showsigs', 1);
        $this->verify_yesno_option($options, 'showavatars', 1);
        $this->verify_yesno_option($options, 'showquickreply', 1);
        $this->verify_yesno_option($options, 'showredirect', 1);
        $this->verify_yesno_option($options, 'showcodebuttons', 1);
        $this->verify_yesno_option($options, 'sourceeditor', 0);
        $this->verify_yesno_option($options, 'buddyrequestspm', 1);
        $this->verify_yesno_option($options, 'buddyrequestsauto', 0);

        if ($this->bb->settings['postlayout'] == 'classic') {
            $this->verify_yesno_option($options, 'classicpostbit', 1);
        } else {
            $this->verify_yesno_option($options, 'classicpostbit', 0);
        }

        if (array_key_exists('subscriptionmethod', $options)) {
            // Value out of range
            $options['subscriptionmethod'] = (int)$options['subscriptionmethod'];
            if ($options['subscriptionmethod'] < 0 || $options['subscriptionmethod'] > 3) {
                $options['subscriptionmethod'] = 0;
            }
        }

        if (array_key_exists('dstcorrection', $options)) {
            // Value out of range
            $options['dstcorrection'] = (int)$options['dstcorrection'];
            if ($options['dstcorrection'] < 0 || $options['dstcorrection'] > 2) {
                $options['dstcorrection'] = 0;
            }
        }

        if (isset($options['dstcorrection'])) {
            $options['dst'] = ($options['dstcorrection'] == 1) ? 1 : 0;
        } else {
            $options['dst'] = 0;
        }

        if ($this->method == 'insert' || (isset($options['threadmode']) &&
                $options['threadmode'] != 'linear' && $options['threadmode'] != 'threaded')
        ) {
            if ($this->bb->settings['threadusenetstyle']) {
                $options['threadmode'] = 'threaded';
            } else {
                $options['threadmode'] = 'linear';
            }
        }

        // Verify the 'threads per page' option.
        if ($this->method == 'insert' || (array_key_exists('tpp', $options) && $this->bb->settings['usertppoptions'])) {
            if (!isset($options['tpp'])) {
                $options['tpp'] = 0;
            }
            $explodedtpp = explode(',', $this->bb->settings['usertppoptions']);
            if (is_array($explodedtpp)) {
                @asort($explodedtpp);
                $biggest = $explodedtpp[count($explodedtpp) - 1];
                // Is the selected option greater than the allowed options?
                if ($options['tpp'] > $biggest) {
                    $options['tpp'] = $biggest;
                }
            }
            $options['tpp'] = (int)$options['tpp'];
        }
        // Verify the 'posts per page' option.
        if ($this->method == 'insert' || (array_key_exists('ppp', $options) && $this->bb->settings['userpppoptions'])) {
            if (!isset($options['ppp'])) {
                $options['ppp'] = 0;
            }
            $explodedppp = explode(',', $this->bb->settings['userpppoptions']);
            if (is_array($explodedppp)) {
                @asort($explodedppp);
                $biggest = $explodedppp[count($explodedppp) - 1];
                // Is the selected option greater than the allowed options?
                if ($options['ppp'] > $biggest) {
                    $options['ppp'] = $biggest;
                }
            }
            $options['ppp'] = (int)$options['ppp'];
        }
        // Is our selected 'days prune' option valid or not?
        if ($this->method == 'insert' || array_key_exists('daysprune', $options)) {
            if (!isset($options['daysprune'])) {
                $options['daysprune'] = 0;
            }
            $options['daysprune'] = (int)$options['daysprune'];
            if ($options['daysprune'] < 0) {
                $options['daysprune'] = 0;
            }
        }
        $this->data['options'] = $options;
    }

    /**
     * Verifies if a registration date is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_regdate()
    {
        $regdate = &$this->data['regdate'];

        $regdate = (int)$regdate;
        // If the timestamp is below 0, set it to the current time.
        if ($regdate <= 0) {
            $regdate = TIME_NOW;
        }
        return true;
    }

    /**
     * Verifies if a last visit date is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_lastvisit()
    {
        $lastvisit = &$this->data['lastvisit'];

        $lastvisit = (int)$lastvisit;
        // If the timestamp is below 0, set it to the current time.
        if ($lastvisit <= 0) {
            $lastvisit = TIME_NOW;
        }
        return true;
    }

    /**
     * Verifies if a last active date is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_lastactive()
    {
        $lastactive = &$this->data['lastactive'];

        $lastactive = (int)$lastactive;
        // If the timestamp is below 0, set it to the current time.
        if ($lastactive <= 0) {
            $lastactive = TIME_NOW;
        }
        return true;
    }

    /**
     * Verifies if an away mode status is valid or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_away()
    {
        $user = &$this->data;
        // If the board does not allow 'away mode' or the user is marking as not away, set defaults.
        if ($this->bb->settings['allowaway'] == 0 || !isset($user['away']['away']) || $user['away']['away'] != 1) {
            $user['away']['away'] = 0;
            $user['away']['date'] = 0;
            $user['away']['returndate'] = 0;
            $user['away']['awayreason'] = '';
            return true;
        } elseif ($user['away']['returndate']) {
            list($returnday, $returnmonth, $returnyear) = explode('-', $user['away']['returndate']);
            if (!$returnday || !$returnmonth || !$returnyear) {
                $this->setError('missing_returndate');
                return false;
            }

            // Validate the return date lengths
            $user['away']['returndate'] = substr($returnday, 0, 2) . '-' . substr($returnmonth, 0, 2) . '-' . substr($returnyear, 0, 4);
        }
        return true;
    }

    /**
     * Verifies if a language is valid for this user or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_language()
    {
        $language = &$this->data['language'];

        // An invalid language has been specified?
        if ($language != '' && !$this->bb->lang->language_exists($language)) {
            $this->setError('invalid_language');
            return false;
        }
        return true;
    }

    /**
     * Verifies if a style is valid for this user or not.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_style()
    {
        $user = &$this->data;
        if (!empty($user['style'])) {
            $theme = $this->bb->themes->get_theme($user['style']);
            if (empty($theme) ||
                (!$this->bb->user->is_member($theme['allowedgroups'], $user) &&
                $theme['allowedgroups'] !== 'all')
            ) {
                $this->setError('invalid_style');
                return false;
            }
        }

        return true;
    }

    /**
     * Verifies if this is coming from a spam bot or not
     *
     * @return boolean True when valid, false when invalid.
     */
    private function verify_checkfields()
    {
        $user = &$this->data;

        // An invalid language has been specified?
        if ($user['regcheck1'] !== '' || $user['regcheck2'] !== 'true') {
            $this->setError('invalid_checkfield');
            return false;
        }
        return true;
    }

    /**
     * Verifies if the user timezone is valid.
     * If the timezone is invalid, the board default is used.
     *
     * @return boolean True when timezone was valid, false otherwise
     */
    private function verify_timezone()
    {
        $user = &$this->data;

        $timezones = $this->bb->time->getSupportedTimezones();

        if (!isset($user['timezone']) || !array_key_exists($user['timezone'], $timezones)) {
            $user['timezone'] = $this->bb->settings['timezoneoffset'];
            return false;
        }

        return true;
    }

    /**
     * Validate all user assets.
     *
     * @return boolean True when valid, false when invalid.
     */
    public function validate_user()
    {
        $user = &$this->data;

        // First, grab the old user details if this user exists
        if (!empty($user['uid'])) {
            $old_user = $this->bb->user->get_user($user['uid']);
        }

        if ($this->method == 'insert' || array_key_exists('username', $user)) {
            // If the username is the same - no need to verify
            if (!isset($old_user->username) || $user['username'] != $old_user->username) {
                $this->verify_username();
                $this->verify_username_exists();
            }
//TODO why unset name???
//      else {
//        unset($user->username);
//      }
        }
        if ($this->method == 'insert' || array_key_exists('usertitle', $user)) {
            $this->verify_usertitle();
        }
        if ($this->method == 'insert' || array_key_exists('password', $user)) {
            $this->verify_password();
        }
        if ($this->method == 'insert' || array_key_exists('usergroup', $user)) {
            $this->verify_usergroup();
        }
        if ($this->method == 'insert' || array_key_exists('email', $user)) {
            $this->verify_email();
        }
        if ($this->method == 'insert' || array_key_exists('website', $user)) {
            $this->verify_website();
        }
        if ($this->method == 'insert' || array_key_exists('icq', $user)) {
            $this->verify_icq();
        }
        if ($this->method == 'insert' || (isset($user['birthday']) && is_array($user['birthday']))) {
            $this->verify_birthday();
        }
        if ($this->method == 'insert' || array_key_exists('postnum', $user)) {
            $this->verify_postnum();
        }
        if ($this->method == 'insert' || array_key_exists('threadnum', $user)) {
            $this->verify_threadnum();
        }
        if ($this->method == 'insert' || array_key_exists('profile_fields', $user)) {
            $this->verify_profile_fields();
        }
        if ($this->method == 'insert' || array_key_exists('referrer', $user)) {
            $this->verify_referrer();
        }
        if ($this->method == 'insert' || array_key_exists('options', $user)) {
            $this->verifyOptions();
        }
        if ($this->method == 'insert' || array_key_exists('regdate', $user)) {
            $this->verify_regdate();
        }
        if ($this->method == 'insert' || array_key_exists('lastvisit', $user)) {
            $this->verify_lastvisit();
        }
        if ($this->method == 'insert' || array_key_exists('lastactive', $user)) {
            $this->verify_lastactive();
        }
        if ($this->method == 'insert' || array_key_exists('away', $user)) {
            $this->verify_away();
        }
        if ($this->method == 'insert' || array_key_exists('language', $user)) {
            $this->verify_language();
        }
        if ($this->method == 'insert' || array_key_exists('timezone', $user)) {
            $this->verify_timezone();
        }
        if ($this->method == 'insert' && array_key_exists('regcheck1', $user) && array_key_exists('regcheck2', $user)) {
            $this->verify_checkfields();
        }
        if (array_key_exists('birthdayprivacy', $user)) {
            $this->verify_birthday_privacy();
        }
        if ($this->method == 'insert' || array_key_exists('style', $user)) {
            $this->verify_style();
        }

        $this->bb->plugins->runHooks('datahandler_user_validate', $this);

        // We are done validating, return.
        $this->set_validated(true);
        if (count($this->get_errors()) > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Inserts a user into the database.
     *
     * @return array
     */
    public function insert_user()
    {
        // Yes, validating is required.
        if (!$this->get_validated()) {
            die('The user needs to be validated before inserting it into the DB.');
        }
        if (count($this->get_errors()) > 0) {
            die('The user is not valid.');
        }

        $user = &$this->data;

        $array = ['postnum', 'threadnum', 'avatar', 'avatartype', 'additionalgroups',
            'displaygroup', 'icq', 'aim', 'yahoo', 'skype', 'google', 'bday', 'signature',
            'style', 'dateformat', 'timeformat', 'notepad'];
        foreach ($array as $value) {
            if (!isset($user[$value])) {
                $user[$value] = '';
            }
        }

        $this->user_insert_data = [
            'username' => $this->bb->db->escape_string($user['username']),
            'password' => $user['saltedpw'],
            'salt' => $user['salt'],
            'loginkey' => $user['loginkey'],
            'email' => $this->bb->db->escape_string($user['email']),
            'postnum' => (int)$user['postnum'],
            'threadnum' => (int)$user['threadnum'],
            'avatar' => $this->bb->db->escape_string($user['avatar']),
            'avatartype' => $this->bb->db->escape_string($user['avatartype']),
            'usergroup' => (int)$user['usergroup'],
            'additionalgroups' => $this->bb->db->escape_string($user['additionalgroups']),
            'displaygroup' => (int)$user['displaygroup'],
            'usertitle' => $this->bb->db->escape_string(htmlspecialchars_uni($user['usertitle'])),
            'regdate' => (int)$user['regdate'],
            'lastactive' => (int)$user['lastactive'],
            'lastvisit' => (int)$user['lastvisit'],
            'website' => $this->bb->db->escape_string($user['website']),
            'icq' => (int)$user['icq'],
            'aim' => $this->bb->db->escape_string($user['aim']),
            'yahoo' => $this->bb->db->escape_string($user['yahoo']),
            'skype' => $this->bb->db->escape_string($user['skype']),
            'google' => $this->bb->db->escape_string($user['google']),
            'birthday' => $user['bday'],
            'signature' => $this->bb->db->escape_string($user['signature']),
            'allownotices' => (int)$user['options']['allownotices'],
            'hideemail' => (int)$user['options']['hideemail'],
            'subscriptionmethod' => (int)$user['options']['subscriptionmethod'],
            'receivepms' => (int)$user['options']['receivepms'],
            'receivefrombuddy' => (int)$user['options']['receivefrombuddy'],
            'pmnotice' => (int)$user['options']['pmnotice'],
            'pmnotify' => (int)$user['options']['pmnotify'],
            'showimages' => (int)$user['options']['showimages'],
            'showvideos' => (int)$user['options']['showvideos'],
            'showsigs' => (int)$user['options']['showsigs'],
            'showavatars' => (int)$user['options']['showavatars'],
            'showquickreply' => (int)$user['options']['showquickreply'],
            'showredirect' => (int)$user['options']['showredirect'],
            'tpp' => (int)$user['options']['tpp'],
            'ppp' => (int)$user['options']['ppp'],
            'invisible' => (int)$user['options']['invisible'],
            'style' => (int)$user['style'],
            'timezone' => $this->bb->db->escape_string($user['timezone']),
            'dstcorrection' => (int)$user['options']['dstcorrection'],
            'threadmode' => $user['options']['threadmode'],
            'daysprune' => (int)$user['options']['daysprune'],
            'dateformat' => $this->bb->db->escape_string($user['dateformat']),
            'timeformat' => $this->bb->db->escape_string($user['timeformat']),
            'regip' => $user['regip'],
            'language' => $this->bb->db->escape_string($user['language']),
            'showcodebuttons' => (int)$user['options']['showcodebuttons'],
            'sourceeditor' => (int)$user['options']['sourceeditor'],
            'buddyrequestspm' => (int)$user['options']['buddyrequestspm'],
            'buddyrequestsauto' => (int)$user['options']['buddyrequestsauto'],
            'away' => (int)$user['away']['away'],
            'awaydate' => (int)$user['away']['date'],
            'returndate' => $user['away']['returndate'],
            'awayreason' => $this->bb->db->escape_string($user['away']['awayreason']),
            'notepad' => $this->bb->db->escape_string($user['notepad']),
            'referrer' => (int)$user['referrer_uid'],
            'referrals' => 0,
            'buddylist' => '',
            'ignorelist' => '',
            'pmfolders' => '',
            'warningpoints' => 0,
            'moderateposts' => 0,
            'moderationtime' => 0,
            'suspendposting' => 0,
            'suspensiontime' => 0,
            'coppauser' => (int)$user['coppa_user'],
            'classicpostbit' => (int)$user['options']['classicpostbit'],
            'usernotes' => ''
        ];

        if ($user['options']['dstcorrection'] == 1) {
            $this->user_insert_data['dst'] = 1;
        } elseif ($user['options']['dstcorrection'] == 0) {
            $this->user_insert_data['dst'] = 0;
        }

        $this->bb->plugins->runHooks('datahandler_user_insert', $this);

        $this->uid = $this->bb->db->insert_query('users', $this->user_insert_data);

        $user['user_fields']['ufid'] = $this->uid;

        $pfcache = $this->bb->cache->read('profilefields');

        if (is_array($pfcache)) {
            foreach ($pfcache as $profile_field) {
                if (array_key_exists("fid{$profile_field['fid']}", $user['user_fields'])) {
                    continue;
                }
                $user['user_fields']["fid{$profile_field['fid']}"] = '';
            }
        }

        $this->bb->db->insert_query('userfields', $user['user_fields'], false);

        if ($this->user_insert_data['referrer'] != 0) {
            $this->bb->db->write_query('
				UPDATE ' . TABLE_PREFIX . "users
				SET referrals=referrals+1
				WHERE uid='{$this->user_insert_data['referrer']}'
			");
        }

        // Update forum stats
        $this->bb->cache->update_stats(['numusers' => '+1']);

        if ((int)$user['usergroup'] == 5) {
            $this->bb->cache->update_awaitingactivation();
        }

        $this->return_values = [
            'uid' => $this->uid,
            'username' => $user['username'],
            'loginkey' => $user['loginkey'],
            'email' => $user['email'],
            'password' => $user['password'],
            'usergroup' => $user['usergroup']
        ];

        $this->bb->plugins->runHooks('datahandler_user_insert_end', $this);

        return $this->return_values;
    }

    /**
     * Updates a user in the database.
     *
     * @return bool
     */
    public function update_user()
    {
        // Yes, validating is required.
        if (!$this->get_validated()) {
            die('The user needs to be validated before inserting it into the DB.');
        }
        if (count($this->get_errors()) > 0) {
            die('The user is not valid.');
        }

        $user = &$this->data;
        $user['uid'] = (int)$user['uid'];
        $this->uid = $user['uid'];

        // Set up the update data.
        if (isset($user['username'])) {
            $this->user_update_data['username'] = $this->bb->db->escape_string($user['username']);
        }
        if (isset($user['saltedpw'])) {
            $this->user_update_data['password'] = $user['saltedpw'];
            $this->user_update_data['salt'] = $user['salt'];
            $this->user_update_data['loginkey'] = $user['loginkey'];
        }
        if (isset($user['email'])) {
            $this->user_update_data['email'] = $user['email'];
        }
        if (isset($user['postnum'])) {
            $this->user_update_data['postnum'] = (int)$user['postnum'];
        }
        if (isset($user['threadnum'])) {
            $this->user_update_data['threadnum'] = (int)$user['threadnum'];
        }
        if (isset($user['avatar'])) {
            $this->user_update_data['avatar'] = $this->bb->db->escape_string($user['avatar']);
            $this->user_update_data['avatartype'] = $this->bb->db->escape_string($user['avatartype']);
        }
        if (isset($user['usergroup'])) {
            $this->user_update_data['usergroup'] = (int)$user['usergroup'];
        }
        if (isset($user['additionalgroups'])) {
            $this->user_update_data['additionalgroups'] = $this->bb->db->escape_string($user['additionalgroups']);
        }
        if (isset($user['displaygroup'])) {
            $this->user_update_data['displaygroup'] = (int)$user['displaygroup'];
        }
        if (isset($user['usertitle'])) {
            $this->user_update_data['usertitle'] = $this->bb->db->escape_string($user['usertitle']);
        }
        if (isset($user['regdate'])) {
            $this->user_update_data['regdate'] = (int)$user['regdate'];
        }
        if (isset($user['lastactive'])) {
            $this->user_update_data['lastactive'] = (int)$user['lastactive'];
        }
        if (isset($user['lastvisit'])) {
            $this->user_update_data['lastvisit'] = (int)$user['lastvisit'];
        }
        if (isset($user['signature'])) {
            $this->user_update_data['signature'] = $this->bb->db->escape_string($user['signature']);
        }
        if (isset($user['website'])) {
            $this->user_update_data['website'] = $this->bb->db->escape_string($user['website']);
        }
        if (isset($user['icq'])) {
            $this->user_update_data['icq'] = (int)$user['icq'];
        }
        if (isset($user['aim'])) {
            $this->user_update_data['aim'] = $this->bb->db->escape_string($user['aim']);
        }
        if (isset($user['yahoo'])) {
            $this->user_update_data['yahoo'] = $this->bb->db->escape_string($user['yahoo']);
        }
        if (isset($user['skype'])) {
            $this->user_update_data['skype'] = $this->bb->db->escape_string($user['skype']);
        }
        if (isset($user['google'])) {
            $this->user_update_data['google'] = $this->bb->db->escape_string($user['google']);
        }
        if (isset($user['bday'])) {
            $this->user_update_data['birthday'] = $user['bday'];
        }
        if (isset($user['birthdayprivacy'])) {
            $this->user_update_data['birthdayprivacy'] = $this->bb->db->escape_string($user['birthdayprivacy']);
        }
        if (isset($user['style'])) {
            $this->user_update_data['style'] = (int)$user['style'];
        }
        if (isset($user['timezone'])) {
            $this->user_update_data['timezone'] = $this->bb->db->escape_string($user['timezone']);
        }
        if (isset($user['dateformat'])) {
            $this->user_update_data['dateformat'] = $this->bb->db->escape_string($user['dateformat']);
        }
        if (isset($user['timeformat'])) {
            $this->user_update_data['timeformat'] = $this->bb->db->escape_string($user['timeformat']);
        }
        if (isset($user['regip'])) {
            $this->user_update_data['regip'] = $this->bb->db->escape_string($user['regip']);
        }
        if (isset($user['language'])) {
            $this->user_update_data['language'] = $this->bb->db->escape_string($user['language']);
        }
        if (isset($user['away'])) {
            $this->user_update_data['away'] = $user['away']['away'];
            $this->user_update_data['awaydate'] = $this->bb->db->escape_string($user['away']['date']);
            $this->user_update_data['returndate'] = $this->bb->db->escape_string($user['away']['returndate']);
            $this->user_update_data['awayreason'] = $this->bb->db->escape_string($user['away']['awayreason']);
        }
        if (isset($user['notepad'])) {
            $this->user_update_data['notepad'] = $this->bb->db->escape_string($user['notepad']);
        }
        if (isset($user['usernotes'])) {
            $this->user_update_data['usernotes'] = $this->bb->db->escape_string($user['usernotes']);
        }
        if (isset($user['options']) && is_array($user['options'])) {
            foreach ($user['options'] as $option => $value) {
                $this->user_update_data[$option] = $value;
            }
        }
        if (array_key_exists('coppa_user', $user)) {
            $this->user_update_data['coppauser'] = (int)$user['coppa_user'];
        }
        // First, grab the old user details for later use.
        $old_user = $this->bb->user->get_user($user['uid']);

        // If old user has new pmnotice and new user has = yes, keep old value
        if ($old_user->pmnotice == '2' &&
            (isset($this->user_update_data['pmnotice']) && $this->user_update_data['pmnotice'] == 1)
        ) {
            unset($this->user_update_data['pmnotice']);
        }

        $this->bb->plugins->runHooks('datahandler_user_update', $this);

        if (count($this->user_update_data) < 1 && empty($user['user_fields'])) {
            return false;
        }

        if (count($this->user_update_data) > 0) {
            // Actual updating happens here.
            $this->bb->db->update_query('users', $this->user_update_data, "uid='{$user['uid']}'");
        }

        $this->bb->cache->update_moderators();
        if (isset($user['bday']) || isset($user['username'])) {
            $this->bb->cache->update_birthdays();
        }

        if (isset($user['usergroup']) && (int)$user['usergroup'] == 5) {
            $this->bb->cache->update_awaitingactivation();
        }

        // Maybe some userfields need to be updated?
        if (isset($user['user_fields']) && is_array($user['user_fields'])) {
            $query = $this->bb->db->simple_select('userfields', '*', "ufid='{$user['uid']}'");
            $fields = $this->bb->db->fetch_array($query);
            if (!$fields['ufid']) {
                $user_fields = [
                    'ufid' => $user['uid']
                ];

                $fields_array = $this->bb->db->show_fields_from('userfields');
                foreach ($fields_array as $field) {
                    if ($field['Field'] == 'ufid') {
                        continue;
                    }
                    $user_fields[$field['Field']] = '';
                }
                $this->bb->db->insert_query('userfields', $user_fields);
            }
            $this->bb->db->update_query('userfields', $user['user_fields'], "ufid='{$user['uid']}'", false);
        }

        // Let's make sure the user's name gets changed everywhere in the db if it changed.
        if (!empty($this->user_update_data['username']) && $this->user_update_data['username'] != $old_user->username) {
            $username_update = [
                'username' => $this->user_update_data['username']
            ];
            $lastposter_update = [
                'lastposter' => $this->user_update_data['username']
            ];

            $this->bb->db->update_query('posts', $username_update, "uid='{$user['uid']}'");
            $this->bb->db->update_query('threads', $username_update, "uid='{$user['uid']}'");
            $this->bb->db->update_query('threads', $lastposter_update, "lastposteruid='{$user['uid']}'");
            $this->bb->db->update_query('forums', $lastposter_update, "lastposteruid='{$user['uid']}'");

            $stats = $this->bb->cache->read('stats');
            if ($stats['lastuid'] == $user['uid']) {
                // User was latest to register, update stats
                $this->bb->cache->update_stats(['numusers' => '+0']);
            }
        }

        return true;
    }

    /**
     * Provides a method to completely delete a user.
     *
     * @param array $delete_uids Array of user information
     * @param integer $prunecontent Whether if delete threads/posts or not
     * @return array
     */
    public function delete_user($delete_uids, $prunecontent = 0)
    {
        // Yes, validating is required.
        if (count($this->get_errors()) > 0) {
            die('The user is not valid.');
        }

        $this->delete_uids = array_map('intval', (array)$delete_uids);

        foreach ($this->delete_uids as $key => $uid) {
            if (!$uid || $this->bb->user->is_super_admin($uid) || $uid == $this->bb->user->uid) {
                // Remove super admins
                unset($this->delete_uids[$key]);
            }
        }

        $this->bb->plugins->runHooks('datahandler_user_delete_start', $this);

        $this->delete_uids = implode(',', $this->delete_uids);

        if (empty($this->delete_uids)) {
            $this->deleted_users = 0;
            $this->return_values = [
                'deleted_users' => $this->deleted_users
            ];

            return $this->return_values;
        }

        $this->delete_content();

        // Delete the user
        $query = $this->bb->db->delete_query('users', "uid IN({$this->delete_uids})");
        $this->deleted_users = $this->bb->db->affected_rows($query);

        // Are we removing the posts/threads of a user?
        if ((int)$prunecontent == 1) {
            $this->delete_posts();
            $this->bb->db->delete_query('announcements', "uid IN({$this->delete_uids})");
        } else {
            // We're just updating the UID
            $this->bb->db->update_query('pollvotes', ['uid' => 0], "uid IN({$this->delete_uids})");
            $this->bb->db->update_query('posts', ['uid' => 0], "uid IN({$this->delete_uids})");
            $this->bb->db->update_query('threads', ['uid' => 0], "uid IN({$this->delete_uids})");
            $this->bb->db->update_query('attachments', ['uid' => 0], "uid IN({$this->delete_uids})");
            $this->bb->db->update_query('announcements', ['uid' => 0], "uid IN({$this->delete_uids})");
        }

        $this->bb->db->update_query('privatemessages', ['fromid' => 0], "fromid IN({$this->delete_uids})");
        $this->bb->db->update_query('users', ['referrer' => 0], "referrer IN({$this->delete_uids})");

        // Update thread ratings
        $query = $this->bb->db->query('
			SELECT r.*, t.numratings, t.totalratings
			FROM ' . TABLE_PREFIX . 'threadratings r
			LEFT JOIN ' . TABLE_PREFIX . "threads t ON (t.tid=r.tid)
			WHERE r.uid IN({$this->delete_uids})
		");
        while ($rating = $this->bb->db->fetch_array($query)) {
            $update_thread = [
                'numratings' => $rating['numratings'] - 1,
                'totalratings' => $rating['totalratings'] - $rating['rating']
            ];
            $this->bb->db->update_query('threads', $update_thread, "tid='{$rating['tid']}'");
        }

        $this->bb->db->delete_query('threadratings', "uid IN({$this->delete_uids})");

        // Update forums & threads if user is the lastposter
        $this->bb->db->update_query('forums', ['lastposteruid' => 0], "lastposteruid IN({$this->delete_uids})");
        $this->bb->db->update_query('threads', ['lastposteruid' => 0], "lastposteruid IN({$this->delete_uids})");

        // Update forum stats
        $this->bb->cache->update_stats(['numusers' => '-' . $this->deleted_users]);

        $this->return_values = [
            'deleted_users' => $this->deleted_users
        ];

        $this->bb->plugins->runHooks('datahandler_user_delete_end', $this);

        // Update  cache
        $this->bb->cache->update_banned();
        $this->bb->cache->update_moderators();
        $this->bb->cache->update_forumsdisplay();
        $this->bb->cache->update_reportedcontent();
        $this->bb->cache->update_awaitingactivation();

        return $this->return_values;
    }

    /**
     * Provides a method to delete users' content
     *
     * @param array|bool $delete_uids Array of user ids, false if they're already set (eg when using the delete_user function)
     */
    public function delete_content($delete_uids = false)
    {
        if ($delete_uids !== false) {
            $this->delete_uids = array_map('intval', (array)$delete_uids);

            foreach ($this->delete_uids as $key => $uid) {
                if (!$uid || $this->bb->user->is_super_admin($uid) || $uid == $this->bb->user->uid) {
                    // Remove super admins
                    unset($this->delete_uids[$key]);
                }
            }

            $this->delete_uids = implode(',', $this->delete_uids);
        }

        $this->bb->plugins->runHooks('datahandler_user_delete_content', $this);

        if (empty($this->delete_uids)) {
            return;
        }

        $this->bb->db->delete_query('userfields', "ufid IN({$this->delete_uids})");
        $this->bb->db->delete_query('privatemessages', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('events', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('moderators', "id IN({$this->delete_uids}) AND isgroup = 0");
        $this->bb->db->delete_query('forumsubscriptions', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('threadsubscriptions', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('forumsread', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('threadsread', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('adminviews', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('adminoptions', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('adminsessions', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('sessions', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('banned', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('joinrequests', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('groupleaders', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('awaitingactivation', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('warnings', "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('reputation', "uid IN({$this->delete_uids}) OR adduid IN({$this->delete_uids})");
        $this->bb->db->delete_query('buddyrequests', "uid IN({$this->delete_uids}) OR touid IN({$this->delete_uids})");
        $this->bb->db->delete_query('posts', "uid IN({$this->delete_uids}) AND visible = -2");
        $this->bb->db->delete_query('threads', "uid IN({$this->delete_uids}) AND visible = -2");

        // Delete reports made to the profile or reputation of the deleted users (i.e. made by them)
        $this->bb->db->delete_query('reportedcontent', "type='reputation' AND id3 IN({$this->delete_uids}) OR type='reputation' AND id2 IN({$this->delete_uids})");
        $this->bb->db->delete_query('reportedcontent', "type='profile' AND id IN({$this->delete_uids})");

        // Update the reports made by the deleted users by setting the uid to 0
        $this->bb->db->update_query('reportedcontent', ['uid' => 0], "uid IN({$this->delete_uids})");

        // Remove any of the user(s) uploaded avatars
//    $this->upload = new \RunBB\Core\Upload($this);
        foreach (explode(',', $this->delete_uids) as $uid) {
            $this->upload->remove_avatars($uid);
        }
    }

    /**
     * Provides a method to delete an users posts and threads
     *
     * @param array|bool $delete_uids Array of user ids, false if they're already set (eg when using the delete_user function)
     */
    public function delete_posts($delete_uids = false)
    {
        if ($delete_uids != false) {
            $this->delete_uids = array_map('intval', (array)$delete_uids);

            foreach ($this->delete_uids as $key => $uid) {
                if (!$uid || $this->bb->user->is_super_admin($uid) || $uid == $this->bb->user->uid) {
                    // Remove super admins
                    unset($this->delete_uids[$key]);
                }
            }

            $this->delete_uids = implode(',', $this->delete_uids);
        }

        $this->bb->plugins->runHooks('datahandler_user_delete_posts', $this);

        if (empty($this->delete_uids)) {
            return;
        }

        // Threads
        $query = $this->bb->db->simple_select('threads', 'tid', "uid IN({$this->delete_uids})");
        while ($tid = $this->bb->db->fetch_field($query, 'tid')) {
            $this->moderation->delete_thread($tid);
        }

        // Posts
        $query = $this->bb->db->simple_select('posts', 'pid', "uid IN({$this->delete_uids})");
        while ($pid = $this->bb->db->fetch_field($query, 'pid')) {
            $this->moderation->delete_post($pid);
        }
    }

    /**
     * Provides a method to clear an users profile
     *
     * @param array|bool $delete_uids Array of user ids, false if they're already set (eg when using the delete_user function)
     * @param int $gid The new usergroup if the users should be moved (additional usergroups are always removed)
     */
    public function clear_profile($delete_uids = false, $gid = 0)
    {
        // delete_uids isn't a nice name, but it's used as the functions above use the same
        if ($delete_uids != false) {
            $this->delete_uids = array_map('intval', (array)$delete_uids);

            foreach ($this->delete_uids as $key => $uid) {
                if (!$uid || $this->bb->user->is_super_admin($uid) || $uid == $this->bb->user->uid) {
                    // Remove super admins
                    unset($this->delete_uids[$key]);
                }
            }

            $this->delete_uids = implode(',', $this->delete_uids);
        }

        $update = [
            'website' => '',
            'birthday' => '',
            'icq' => '',
            'aim' => '',
            'yahoo' => '',
            'skype' => '',
            'google' => '',
            'usertitle' => '',
            'away' => 0,
            'awaydate' => 0,
            'returndate' => '',
            'awayreason' => '',
            'additionalgroups' => '',
            'displaygroup' => 0,
            'signature' => '',
            'avatar' => '',
            'avatardimensions' => '',
            'avatartype' => ''
        ];

        if ($gid > 0) {
            $update['usergroup'] = (int)$gid;
        }

        $this->bb->plugins->runHooks('datahandler_user_clear_profile', $this);

        if (empty($this->delete_uids)) {
            return;
        }

        $this->bb->db->update_query('users', $update, "uid IN({$this->delete_uids})");
        $this->bb->db->delete_query('userfields', "ufid IN({$this->delete_uids})");

        // Remove any of the user(s) uploaded avatars
//    $this->upload = new \RunBB\Core\Upload($this);
        foreach (explode(',', $this->delete_uids) as $uid) {
            $this->upload->remove_avatars($uid);
        }
    }
}
