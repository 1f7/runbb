<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\DataHandlers;

use RunBB\Core\DataHandler;

/**
 * Login handling class, provides common structure to handle login events.
 *
 */
class WarningsHandler extends DataHandler
{
    /**
     * The language file used in the data handler.
     *
     * @var string
     */
    public $language_file = 'datahandler_warnings';

    /**
     * The prefix for the language variables used in the data handler.
     *
     * @var string
     */
    public $language_prefix = 'warnings';

    /**
     * The stored data for the warning being written.
     *
     * @var array
     */
    public $write_warning_data = [];

    /**
     * The stored data for the warning being retrieved.
     *
     * @var array
     */
    private $read_warning_data = [];

    /**
     * Friendly redirect action after inserting a new warning.
     *
     * @var string
     */
    public $friendly_action = '';

    /**
     * Validate a warning user assets.
     *
     * @return boolean True when valid, false when invalid.
     */
    function validate_user()
    {
        //global $mybb;

        $warning = &$this->data;

        $user = $this->user->get_user($warning['uid']);

        if (!$user['uid']) {
            $this->setError('error_invalid_user');
            return false;
        }

        if ($user['uid'] == $this->user->uid) {
            $this->setError('error_cannot_warn_self');
            return false;
        }

        if ($user['warningpoints'] >= $this->bb->settings['maxwarningpoints']) {
            $this->setError('error_user_reached_max_warning');
            return false;
        }

        return true;
    }

    /**
     * Validate a warning post.
     *
     * @return boolean True when valid, false when invalid.
     */
    private function validatePost()
    {
        $warning = &$this->data;

        $post = $this->bb->post->get_post($warning['pid']);

        if (!$post['pid']) {
            $this->setError('error_invalid_post');
            return false;
        }

        return true;
    }

    /**
     * Validate a warning notes.
     *
     * @return boolean True when valid, false when invalid.
     */
    function validate_notes()
    {
        $warning = &$this->data;

        if (!trim($warning['notes'])) {
            $this->setError('error_no_note');
            return false;
        }

        return true;
    }

    /**
     * Validate maximum warnings per day for current user.
     *
     * @return boolean True when valid, false when invalid.
     */
    function validate_maximum()
    {
        //global $mybb, $db, $lang;

        if ($this->bb->usergroup['maxwarningsday'] != 0) {
            $timecut = TIME_NOW - 60 * 60 * 24;
            $query = $this->bb->db->simple_select(
                'warnings',
                'COUNT(wid) AS given_today',
                "issuedby='{$this->user->uid}' AND dateline>'$timecut'"
            );
            $given_today = $this->bb->db->fetch_field($query, 'given_today');
            if ($given_today >= $this->bb->usergroup['maxwarningsday']) {
                $this->setError(
                    'reached_max_warnings_day',
                    [$this->bb->parser->formatNumber($this->bb->usergroup['maxwarningsday'])]
                );
                return false;
            }
        }

        return true;
    }

    /**
     * Validate warnings type.
     *
     * @return boolean True when valid, false when invalid.
     */
    function validate_type()
    {
        //global $mybb, $db;

        $warning = &$this->data;

        // Issuing a custom warning
        if ($warning['type'] == 'custom') {
            if ($this->bb->settings['allowcustomwarnings'] == 0) {
                $this->setError('error_cant_custom_warn');
                return false;
            }

            if (!$warning['custom_reason']) {
                $this->setError('error_no_custom_reason');
                return false;
            }

            $warning['title'] = $warning['custom_reason'];

            if (!$warning['custom_points'] ||
                $warning['custom_points'] > $this->bb->settings['maxwarningpoints'] ||
                $warning['custom_points'] < 0
            ) {
                $this->setError(
                    'error_invalid_custom_points',
                    [$this->bb->parser->formatNumber($this->bb->settings['maxwarningpoints'])]
                );
                return false;
            }

            $warning['points'] = round($warning['custom_points']);

            // Build expiry date
            if ($warning['expires_period'] == 'hours') {
                $warning['expires'] = $warning['expires'] * 3600 + TIME_NOW;
            } elseif ($warning['expires_period'] == 'days') {
                $warning['expires'] = $warning['expires'] * 86400 + TIME_NOW;
            } elseif ($warning['expires_period'] == 'weeks') {
                $warning['expires'] = $warning['expires'] * 604800 + TIME_NOW;
            } elseif ($warning['expires_period'] == 'months') {
                $warning['expires'] = $warning['expires'] * 2592000 + TIME_NOW;
            } elseif ($warning['expires_period'] == 'never') {
                $warning['expires'] = 0;
            } else {
                // unkown expires_period
                $this->setError('error_invalid_expires_period');
                return false;
            }
        } // Using a predefined warning type
        else {
            $query = $this->bb->db->simple_select('warningtypes', '*', "tid='" . (int)$warning['type'] . "'");
            $this->warning_type = $this->bb->db->fetch_array($query);

            if (!$this->warning_type) {
                $this->setError('error_invalid_type');
                return false;
            }

            $warning['points'] = $this->warning_type['points'];
            $warning['title'] = '';
            $warning['expires'] = 0;

            if ($this->warning_type['expirationtime']) {
                $warning['expires'] = TIME_NOW + $this->warning_type['expirationtime'];
            }
        }

        return true;
    }

    /**
     * Validate a warning.
     *
     * @return boolean True when valid, false when invalid.
     */
    function validate_warning()
    {
        //global $plugins;

        $warning = &$this->data;

        // Verify all warning assets.
        $this->validate_user();
        $this->validate_maximum();
        $this->validate_notes();

        if (array_key_exists('pid', $warning)) {
            $this->validatePost();
        }
        if (array_key_exists('type', $warning)) {
            $this->validate_type();
        }

        $this->bb->plugins->runHooks('datahandler_warnings_validate_warning', $this);

        // We are done validating, return.
        $this->set_validated(true);

        if (count($this->get_errors()) > 0) {
            return false;
        }

        return true;
    }

    /**
     * Gets a valid warning from the DB engine.
     *
     * @param int $wid
     * @return array|bool array when valid, boolean false when invalid.
     */
    function get($wid)
    {
        //global $db;

        $wid = (int)$wid;
        if ($wid <= 0) {
            return false;
        }

        $query = $this->bb->db->simple_select('warnings', '*', "wid='" . $wid . "'");
        $this->read_warning_data = $this->bb->db->fetch_array($query);

        if (!$this->read_warning_data['wid']) {
            return false;
        }

        return $this->read_warning_data;
    }

    /**
     * Expire old warnings in the database.
     *
     * @return boolean True when finished.
     */
    function expire_warnings()
    {
        //global $db;

        $users = [];

        $query = $this->bb->db->query('
			SELECT w.wid, w.uid, w.points, u.warningpoints
			FROM ' . TABLE_PREFIX . 'warnings w
			LEFT JOIN ' . TABLE_PREFIX . 'users u ON (u.uid=w.uid)
			WHERE expires<' . TIME_NOW . ' AND expires!=0 AND expired!=1
		');
        while ($warning = $this->bb->db->fetch_array($query)) {
            $updated_warning = [
                'expired' => 1
            ];
            $this->bb->db->update_query('warnings', $updated_warning, "wid='{$warning['wid']}'");

            if (array_key_exists($warning['uid'], $users)) {
                $users[$warning['uid']] -= $warning['points'];
            } else {
                $users[$warning['uid']] = $warning['warningpoints'] - $warning['points'];
            }
        }

        foreach ($users as $uid => $warningpoints) {
            if ($warningpoints < 0) {
                $warningpoints = 0;
            }

            $updated_user = [
                'warningpoints' => (int)$warningpoints
            ];
            $this->bb->db->update_query('users', $updated_user, "uid='" . (int)$uid . "'");
        }

        return true;
    }

    /**
     * Updates an user warning details.
     *
     * @return array Updated user details.
     */
    function update_user($method = 'insert')
    {
        if ($this->bb->settings['maxwarningpoints'] < 1) {
            $this->bb->settings['maxwarningpoints'] = 10;
        }

        if (!is_array($this->bb->groupscache)) {
            $this->bb->groupscache = $this->bb->cache->read('usergroups');
        }

        $warning = &$this->data;

        $user = $this->user->get_user($warning['uid']);

        if ($method == 'insert') {
            // Build warning level & ensure it doesn't go over 100.
            $current_level = round($user['warningpoints'] / $this->bb->settings['maxwarningpoints'] * 100);
            $this->new_warning_level = round(($user['warningpoints'] + $warning['points']) / $this->bb->settings['maxwarningpoints'] * 100);
            if ($this->new_warning_level > 100) {
                $this->new_warning_level = 100;
            }

            // Update user
            $this->updated_user = [
                'warningpoints' => $user['warningpoints'] + $warning['points']
            ];

            // Fetch warning level
            $query = $this->bb->db->simple_select(
                'warninglevels',
                '*',
                "percentage<={$this->new_warning_level}",
                ['order_by' => 'percentage', 'order_dir' => 'desc']
            );
            $new_level = $this->bb->db->fetch_array($query);

            if ($new_level['lid']) {
                $expiration = 0;
                $action = my_unserialize($new_level['action']);

                if ($action['length'] > 0) {
                    $expiration = TIME_NOW + $action['length'];
                }

                switch ($action['type']) {
                    // Ban the user for a specified time
                    case 1:
                        // Fetch any previous bans for this user
                        $query = $this->bb->db->simple_select('banned', '*', "uid='{$user['uid']}' AND gid='{$action['usergroup']}' AND lifted>" . TIME_NOW);
                        $existing_ban = $this->bb->db->fetch_array($query);

                        // Only perform if no previous ban or new ban expires later than existing ban
                        if (($expiration > $existing_ban['lifted'] && $existing_ban['lifted'] != 0) || $expiration == 0 || !$existing_ban['uid']) {
                            if (!$warning['title']) {
                                $warning['title'] = $this->warning_type['title'];
                            }

                            // Never lift the ban?
                            if ($action['length'] <= 0) {
                                $bantime = '---';
                            } else {
                                $bantimes = $this->bb->ban->fetch_ban_times();
                                foreach ($bantimes as $date => $string) {
                                    if ($date == '---') {
                                        continue;
                                    }

                                    $time = 0;
                                    list($day, $month, $year) = explode('-', $date);
                                    if ($day > 0) {
                                        $time += 60 * 60 * 24 * $day;
                                    }

                                    if ($month > 0) {
                                        $time += 60 * 60 * 24 * 30 * $month;
                                    }

                                    if ($year > 0) {
                                        $time += 60 * 60 * 24 * 365 * $year;
                                    }

                                    if ($time == $action['length']) {
                                        $bantime = $date;
                                        break;
                                    }
                                }
                            }

                            $new_ban = [
                                'uid' => $user['uid'],
                                'gid' => $action['usergroup'],
                                'oldgroup' => $user['usergroup'],
                                'oldadditionalgroups' => $user['additionalgroups'],
                                'olddisplaygroup' => $user['displaygroup'],
                                'admin' => $this->user->uid,
                                'dateline' => TIME_NOW,
                                'bantime' => $this->bb->db->escape_string($bantime),
                                'lifted' => $expiration,
                                'reason' => $this->bb->db->escape_string($warning['title'])
                            ];
                            // Delete old ban for this user, taking details
                            if ($existing_ban['uid']) {
                                $this->bb->db->delete_query('banned', "uid='{$user['uid']}' AND gid='{$action['usergroup']}'");
                                // Override new ban details with old group info
                                $new_ban['oldgroup'] = $existing_ban['oldgroup'];
                                $new_ban['oldadditionalgroups'] = $existing_ban['oldadditionalgroups'];
                                $new_ban['olddisplaygroup'] = $existing_ban['olddisplaygroup'];
                            }

                            $period = $this->bb->lang->expiration_never;
                            $ban_length = fetch_friendly_expiration($action['length']);

                            if ($ban_length['time']) {
                                $lang_str = 'expiration_' . $ban_length['period'];
                                $period = $this->bb->lang->sprintf($this->bb->lang->result_period, $ban_length['time'], $this->bb->lang->$lang_str);
                            }

                            $group_name = $this->bb->groupscache[$action['usergroup']]['title'];
                            $this->friendly_action = $this->bb->lang->sprintf($this->bb->lang->redirect_warned_banned, $group_name, $period);

                            $this->bb->db->insert_query('banned', $new_ban);
                            $this->updated_user['usergroup'] = $action['usergroup'];
                            $this->updated_user['additionalgroups'] = '';
                            $this->updated_user['displaygroup'] = 0;
                        }
                        break;
                    // Suspend posting privileges
                    case 2:
                        // Only perform if the expiration time is greater than the users current suspension period
                        if ($expiration == 0 || $expiration > $user['suspensiontime']) {
                            if (($user['suspensiontime'] != 0 && $user['suspendposting']) || !$user['suspendposting']) {
                                $period = $this->bb->lang->expiration_never;
                                $ban_length = fetch_friendly_expiration($action['length']);

                                if ($ban_length['time']) {
                                    $lang_str = 'expiration_' . $ban_length['period'];
                                    $period = $this->bb->lang->sprintf($this->bb->lang->result_period, $ban_length['time'], $this->bb->lang->$lang_str);
                                }

                                $this->friendly_action = $this->bb->lang->sprintf($this->bb->lang->redirect_warned_suspended, $period);

                                $this->updated_user['suspensiontime'] = $expiration;
                                $this->updated_user['suspendposting'] = 1;
                            }
                        }
                        break;
                    // Moderate new posts
                    case 3:
                        // Only perform if the expiration time is greater than the users current suspension period
                        if ($expiration == 0 || $expiration > $user['moderationtime']) {
                            if (($user['moderationtime'] != 0 && $user['moderateposts']) || !$user['suspendposting']) {
                                $period = $this->bb->lang->expiration_never;
                                $ban_length = fetch_friendly_expiration($action['length']);

                                if ($ban_length['time']) {
                                    $lang_str = 'expiration_' . $ban_length['period'];
                                    $period = $this->bb->lang->sprintf($this->bb->lang->result_period, $ban_length['time'], $this->bb->lang->$lang_str);
                                }

                                $this->friendly_action = $this->bb->lang->sprintf($this->bb->lang->redirect_warned_moderate, $period);

                                $this->updated_user['moderationtime'] = $expiration;
                                $this->updated_user['moderateposts'] = 1;
                            }
                        }
                        break;
                }
            }
        } else {
            // Warning is still active, lower users point count
            if ($warning['expired'] != 1) {
                $new_warning_points = $user['warningpoints'] - $warning['points'];
                if ($new_warning_points < 0) {
                    $new_warning_points = 0;
                }

                $this->updated_user = [
                    'warningpoints' => $new_warning_points
                ];


                // check if we need to revoke any consequences with this warning
                $current_level = round($user['warningpoints'] / $this->bb->settings['maxwarningpoints'] * 100);
                $this->new_warning_level = round($new_warning_points / $this->bb->settings['maxwarningpoints'] * 100);
                $query = $this->bb->db->simple_select('warninglevels', 'action', "percentage>{$this->new_warning_level} AND percentage<=$current_level");
                if ($this->bb->db->num_rows($query)) {
                    // we have some warning levels we need to revoke
                    $max_expiration_times = $check_levels = [];
                    find_warnlevels_to_check($query, $max_expiration_times, $check_levels);

                    // now check warning levels already applied to this user to see if we need to lower any expiration times
                    $query = $this->bb->db->simple_select('warninglevels', 'action', "percentage<={$this->new_warning_level}");
                    $lower_expiration_times = $lower_levels = [];
                    find_warnlevels_to_check($query, $lower_expiration_times, $lower_levels);

                    // now that we've got all the info, do necessary stuff
                    for ($i = 1; $i <= 3; ++$i) {
                        if ($check_levels[$i]) {
                            switch ($i) {
                                case 1: // Ban
                                    // we'll have to resort to letting the admin/mod remove the ban manually, since there's an issue if stacked bans are in force...
                                    continue;
                                case 2: // Revoke posting
                                    $current_expiry_field = 'suspensiontime';
                                    $current_inforce_field = 'suspendposting';
                                    break;
                                case 3:
                                    $current_expiry_field = 'moderationtime';
                                    $current_inforce_field = 'moderateposts';
                                    break;
                            }

                            // if the thing isn't in force, don't bother with trying to update anything
                            if (!$user[$current_inforce_field]) {
                                continue;
                            }

                            if ($lower_levels[$i]) {
                                // lessen the expiration time if necessary

                                if (!$lower_expiration_times[$i]) {
                                    // doesn't expire - enforce this
                                    $this->updated_user[$current_expiry_field] = 0;
                                    continue;
                                }

                                if ($max_expiration_times[$i]) {
                                    // if the old level did have an expiry time...
                                    if ($max_expiration_times[$i] <= $lower_expiration_times[$i]) {
                                        // if the lower expiration time is actually higher than the upper expiration time -> skip
                                        continue;
                                    }
                                    // both new and old max expiry times aren't infinite, so we can take a difference
                                    $expire_offset = ($lower_expiration_times[$i] - $max_expiration_times[$i]);
                                } else {
                                    // the old level never expired, not much we can do but try to estimate a new expiry time... which will just happen to be starting from today...
                                    $expire_offset = TIME_NOW + $lower_expiration_times[$i];
                                    // if the user's expiry time is already less than what we're going to set it to, skip
                                    if ($user[$current_expiry_field] <= $expire_offset) {
                                        continue;
                                    }
                                }

                                $this->updated_user[$current_expiry_field] = $user[$current_expiry_field] + $expire_offset;
                                // double-check if it's expired already
                                if ($this->updated_user[$current_expiry_field] < TIME_NOW) {
                                    $this->updated_user[$current_expiry_field] = 0;
                                    $this->updated_user[$current_inforce_field] = 0;
                                }
                            } else {
                                // there's no lower level for this type - remove the consequence entirely
                                $this->updated_user[$current_expiry_field] = 0;
                                $this->updated_user[$current_inforce_field] = 0;
                            }
                        }
                    }
                }
            }
        }

        // Save updated details
        $this->bb->db->update_query('users', $this->updated_user, "uid='{$user['uid']}'");

        $this->bb->cache->update_moderators();

        return $this->updated_user;
    }

    /**
     * Inserts a warning into the database
     *
     * @return array Warning database details.
     */
    function insert_warning()
    {
        //global $db, $mybb, $plugins;

        $warning = &$this->data;

        $this->write_warning_data = [
            'uid' => (int)$warning['uid'],
            'tid' => (int)$warning['type'],
            'pid' => (int)$warning['pid'],
            'title' => $this->bb->db->escape_string($warning['title']),
            'points' => (int)$warning['points'],
            'dateline' => TIME_NOW,
            'issuedby' => $this->user->uid,
            'expires' => (int)$warning['expires'],
            'expired' => 0,
            'revokereason' => '',
            'notes' => $this->bb->db->escape_string($warning['notes'])
        ];

        $this->write_warning_data['wid'] = $this->bb->db->insert_query('warnings', $this->write_warning_data);

        $this->update_user();

        $this->bb->plugins->runHooks('datahandler_warnings_insert_warning', $this);

        return $this->write_warning_data;
    }

    /**
     * Updates a warning in the database
     *
     * @return array Warning database details.
     */
    function update_warning()
    {
        //global $db, $mybb, $plugins;

        $warning = &$this->data;

        $warning['wid'] = (int)$warning['wid'];
        if ($warning['wid'] <= 0) {
            return false;
        }

        $this->write_warning_data = [
            'expired' => 1,
            'daterevoked' => TIME_NOW,
            'revokedby' => $this->user->uid,
            'revokereason' => $this->bb->db->escape_string($warning['reason'])
        ];

        $this->bb->plugins->runHooks('datahandler_warnings_update_warning', $this);

        $this->bb->db->update_query('warnings', $this->write_warning_data, "wid='{$warning['wid']}'");

        $this->update_user('update');

        return $this->write_warning_data;
    }
}
