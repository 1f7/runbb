<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Handlers\MailHandlers;

class MailHandler
{
    /**
     * Which email it should send to.
     *
     * @var string
     */
    public $to;

    /**
     * 1/0 value weather it should show errors or not.
     *
     * @var integer
     */
    public $show_errors = 1;

    /**
     * Who it is from.
     *
     * @var string
     */
    public $from;

    /**
     * Who the email should return to.
     *
     * @var string
     */
    public $return_email;

    /**
     * The subject of mail.
     *
     * @var string
     */
    public $subject;

    /**
     * The unaltered subject of mail.
     *
     * @var string
     */
    public $orig_subject;

    /**
     * The message of the mail.
     *
     * @var string
     */
    public $message;

    /**
     * The headers of the mail.
     *
     * @var string
     */
    public $headers;

    /**
     * The charset of the mail.
     *
     * @var string
     * @default utf-8
     */
    public $charset = 'utf-8';

    /**
     * The currently used delimiter new lines.
     *
     * @var string
     */
    public $delimiter = "\r\n";

    /**
     * How it should parse the email (HTML or plain text?)
     *
     * @var string
     */
    public $parse_format = 'text';

    /**
     * The last received response from the SMTP server.
     *
     * @var string
     */
    public $data = '';

    /**
     * The last received response code from the SMTP server.
     *
     * @var string
     */
    public $code = 0;

    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    /**
     * Selects between AdminEmail and ReturnEmail, dependant on if ReturnEmail is filled.
     *
     * @return string
     */
    private function getFromEmail()
    {
        if (trim($this->bb->settings['returnemail'])) {
            $email = $this->bb->settings['returnemail'];
        } else {
            $email = $this->bb->settings['adminemail'];
        }

        return $email;
    }

    /**
     * Builds the whole mail.
     * To be used by the different email classes later.
     *
     * @param string $to to email.
     * @param string $subject subject of email.
     * @param string $message message of email.
     * @param string $from from email.
     * @param string $charset charset of email.
     * @param string $headers headers of email.
     * @param string $format format of the email (HTML, plain text, or both?).
     * @param string $message_text plain text version of the email.
     * @param string $return_email the return email address.
     */
    public function buildMessage(
        $to,
        $subject,
        $message,
        $from = '',
        $charset = '',
        $headers = '',
        $format = 'text',
        $message_text = '',
        $return_email = ''
    ) {
    
        $this->message = '';
        $this->headers = $headers;

        if ($from) {
            $this->from = $from;
        } else {
            $this->from = '';
            if ($this->bb->settings['mail_handler'] == 'smtp') {
                $this->from = $this->getFromEmail();
            } else {
                //$this->from = '"'.$this->utf8_encode($this->settings['bbname']).'"';
                $this->from = '"' . utf8_encode($this->bb->settings['bbname']) . '"';
                $this->from .= ' <' . $this->getFromEmail() . '>';
            }
        }

        if ($return_email) {
            $this->return_email = $return_email;
        } else {
            $this->return_email = '';
            $this->return_email = $this->getFromEmail();
        }

        $this->setTo($to);
        $this->setSubject($subject);

        if ($charset) {
            $this->setCharset($charset);
        }

        $this->parse_format = $format;
        $this->setCommonHeaders();
        $this->setMessage($message, $message_text);
    }

    /**
     * Sets the charset.
     *
     * @param string $charset charset
     */
    private function setCharset($charset)
    {
        if (empty($charset)) {
            $this->charset = $this->lang->settings['charset'];
        } else {
            $this->charset = $charset;
        }
    }

    /**
     * Sets and formats the email message.
     *
     * @param string $message message
     * @param string $message_text
     */
    private function setMessage($message, $message_text = '')
    {
        $message = $this->cleanupCrlf($message);

        if ($message_text) {
            $message_text = $this->cleanupCrlf($message_text);
        }

        if ($this->parse_format == 'html' || $this->parse_format == 'both') {
            $this->setHtmlHeaders($message, $message_text);
        } else {
            $this->message = $message;
            $this->setPlainHeaders();
        }
    }

    /**
     * Sets and formats the email subject.
     *
     * @param string $subject
     */
    private function setSubject($subject)
    {
        $this->orig_subject = $this->cleanup($subject);
        //$this->subject = $this->utf8_encode($this->orig_subject);
        $this->subject = utf8_encode($this->orig_subject);
    }

    /**
     * Sets and formats the recipient address.
     *
     * @param string $to
     */
    private function setTo($to)
    {
        $to = $this->cleanup($to);

        $this->to = $this->cleanup($to);
    }

    /**
     * Sets the plain headers, text/plain
     */
    private function setPlainHeaders()
    {
        $this->headers .= "Content-Type: text/plain; charset={$this->charset}{$this->delimiter}";
    }

    /**
     * Sets the alternative headers, text/html and text/plain.
     *
     * @param string $message
     * @param string $message_text
     */
    private function setHtmlHeaders($message, $message_text = '')
    {
        if (!$message_text && $this->parse_format == 'both') {
            $message_text = strip_tags($message);
        }

        if ($this->parse_format == 'both') {
            $mime_boundary = '=_NextPart' . md5(TIME_NOW);

            $this->headers .= "Content-Type: multipart/alternative; boundary=\"{$mime_boundary}\"{$this->delimiter}";
            $this->message = "This is a multi-part message in MIME format.{$this->delimiter}{$this->delimiter}";

            $this->message .= "--{$mime_boundary}{$this->delimiter}";
            $this->message .= "Content-Type: text/plain; charset=\"{$this->charset}\"{$this->delimiter}";
            $this->message .= "Content-Transfer-Encoding: 8bit{$this->delimiter}{$this->delimiter}";
            $this->message .= $message_text . "{$this->delimiter}{$this->delimiter}";

            $this->message .= "--{$mime_boundary}{$this->delimiter}";

            $this->message .= "Content-Type: text/html; charset=\"{$this->charset}\"{$this->delimiter}";
            $this->message .= "Content-Transfer-Encoding: 8bit{$this->delimiter}{$this->delimiter}";
            $this->message .= $message . "{$this->delimiter}{$this->delimiter}";

            $this->message .= "--{$mime_boundary}--{$this->delimiter}{$this->delimiter}";
        } else {
            $this->headers .= "Content-Type: text/html; charset=\"{$this->charset}\"{$this->delimiter}";
            $this->headers .= "Content-Transfer-Encoding: 8bit{$this->delimiter}{$this->delimiter}";
            $this->message = $message . "{$this->delimiter}{$this->delimiter}";
        }
    }

    /**
     * Sets the common headers.
     */
    private function setCommonHeaders()
    {
        // Build mail headers
        $this->headers .= "From: {$this->from}{$this->delimiter}";

        if ($this->return_email) {
            $this->headers .= "Return-Path: {$this->return_email}{$this->delimiter}";
            $this->headers .= "Reply-To: {$this->return_email}{$this->delimiter}";
        }

        if (isset($_SERVER['SERVER_NAME'])) {
            $http_host = $_SERVER['SERVER_NAME'];
        } elseif (isset($_SERVER['HTTP_HOST'])) {
            $http_host = $_SERVER['HTTP_HOST'];
        } else {
            $http_host = 'unknown.local';
        }

        $msg_id = md5(uniqid(TIME_NOW, true)) . '@' . $http_host;

        if ($this->bb->settings['mail_message_id']) {
            $this->headers .= "Message-ID: <{$msg_id}>{$this->delimiter}";
        }
        $this->headers .= "Content-Transfer-Encoding: 8bit{$this->delimiter}";
        $this->headers .= "X-Priority: 3{$this->delimiter}";
        $this->headers .= "X-Mailer: MyBB{$this->delimiter}";
        $this->headers .= "MIME-Version: 1.0{$this->delimiter}";
    }

    /**
     * Log a fatal error message to the database.
     *
     * @param string $error The error message
     */
    protected function fatalError($error)
    {
        $mail_error = [
            'subject' => $this->bb->db->escape_string($this->orig_subject),
            'message' => $this->bb->db->escape_string($this->message),
            'toaddress' => $this->bb->db->escape_string($this->to),
            'fromaddress' => $this->bb->db->escape_string($this->from),
            'dateline' => TIME_NOW,
            'error' => $this->bb->db->escape_string($error),
            'smtperror' => $this->bb->db->escape_string($this->data),
            'smtpcode' => (int)$this->code
        ];
        $this->bb->db->insert_query('mailerrors', $mail_error);

        // Another neat feature would be the ability to notify the site administrator
        // via email - but wait, with email down, how do we do that? How about private
        // message and hope the admin checks their PMs?
    }

    /**
     * Rids pesky characters from subjects, recipients, from addresses etc (prevents mail injection too)
     *
     * @param string $string The string being checked
     * @return string The cleaned string
     */
    private function cleanup($string)
    {
        $string = str_replace(["\r", "\n", "\r\n"], '', $string);
        $string = trim($string);
        return $string;
    }

    /**
     * Converts message text to suit the correct delimiter
     * See dev.mybb.com/issues/1735 (Jorge Oliveira)
     *
     * @param string $text The text being converted
     * @return string The converted string
     */
    private function cleanupCrlf($text)
    {
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\r", "\n", $text);
        $text = str_replace("\n", "\r\n", $text);

        return $text;
    }

    /**
     * Encode a string based on the character set enabled. Used to encode subjects
     * and recipients in email messages going out so that they show up correctly
     * in email clients.
     *
     * @param string $string The string to be encoded.
     * @return string The encoded string.
     */
/*
    private function utf8_encode($string)
    {
        if (strtolower($this->charset) == 'utf-8' && preg_match('/[^\x20-\x7E]/', $string)) {
            $chunk_size = 47; // Derived from floor((75 - strlen('=?UTF-8?B??=')) * 0.75);
            $len = mb_strlen($string);
            $output = '';
            $pos = 0;
            while ($pos < $len) {
                $newpos = min($pos + $chunk_size, $len);
                while (ord($string[$newpos]) >= 0x80 && ord($string[$newpos]) < 0xC0) {
                    // Reduce len until it's safe to split UTF-8.
                    $newpos--;
                }
                $chunk = substr($string, $pos, $newpos - $pos);
                $pos = $newpos;
                $output .= " =?UTF-8?B?" . base64_encode($chunk) . "?=\n";
            }
            return trim($output);
        }
        return $string;
    }
*/
}
