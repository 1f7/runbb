<?php
/**
 * MyBB 1.8
 * Copyright 2014 MyBB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

namespace RunBB\Handlers\MailHandlers;

/**
 * PHP mail handler class.
 */
class PhpMail extends MailHandler
{
    /**
     * Additional parameters to pass to PHPs mail() function.
     *
     * @var string
    */
    public $additional_parameters = '';

    /**
     * Sends the email.
     *
     * @return bool whether or not the email got sent or not.
     */
    public function send()
    {
        // For some reason sendmail/qmail doesn't like \r\n
        $this->sendmail = @ini_get('sendmail_path');
        if ($this->sendmail) {
            $this->headers = str_replace("\r\n", "\n", $this->headers);
            $this->message = str_replace("\r\n", "\n", $this->message);
            $this->delimiter = "\n";
        }

        // Some mail providers ignore email's with incorrect return-to path's so try and fix that here
        $this->sendmail_from = @ini_get('sendmail_from');
        if ($this->sendmail_from != $this->bb->settings['adminemail']) {
            @ini_set("sendmail_from", $this->bb->settings['adminemail']);
        }

//		$dir = "/{$this->bb->config['admin_dir']}/";
//		$pos = strrpos($this->bb->currentRoute, $dir);
//		if(defined('IN_ADMINCP') && $pos !== false)
//		{
//			$temp_script_path = $_SERVER['PHP_SELF'];
//			$_SERVER['PHP_SELF'] = substr($_SERVER['PHP_SELF'], $pos + strlen($dir) - 1);
//		}

        // If safe mode is on, don't send the additional parameters as we're not allowed to
        if (SAFEMODE) {
            $sent = @mail($this->to, $this->subject, $this->message, trim($this->headers));
        } else {
            $sent = @mail($this->to, $this->subject, $this->message, trim($this->headers), $this->additionalParameters);
        }
        $function_used = 'mail()';

//		if(defined('IN_ADMINCP') && $pos !== false)
//		{
//			$_SERVER['PHP_SELF'] = $temp_script_path;
//		}

        if (!$sent) {
            $this->fatalError("MyBB was unable to send the email using the PHP {$function_used} function.");
            return false;
        }

        return true;
    }
}
