<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Helpers;

class DebugPage
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    public function show()
    {
        $totaltime = format_time_duration($this->bb->maintimer->totaltime);
        $phptime = $this->bb->maintimer->totaltime - $this->bb->db->query_time;
        $query_time = $this->bb->db->query_time;
        $globaltime = format_time_duration($this->bb->globaltime);

        $percentphp = number_format((($phptime / $this->bb->maintimer->totaltime) * 100), 2);
        $percentsql = number_format((($query_time / $this->bb->maintimer->totaltime) * 100), 2);

        $phptime = format_time_duration($this->bb->maintimer->totaltime - $this->bb->db->query_time);
        $query_time = format_time_duration($this->bb->db->query_time);

        $call_time = format_time_duration($this->bb->cache->call_time);

        $phpversion = PHP_VERSION;

        $serverload = get_server_load($this->bb->lang->unknown);

        if ($this->bb->settings['gzipoutput'] != 0) {
            $gzipen = 'Enabled';
        } else {
            $gzipen = 'Disabled';
        }

        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
        echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">";
        echo "<head>";
        echo "<meta name=\"robots\" content=\"noindex\" />";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />";
        echo "<title>BB Debug Information</title>";
        echo "</head>";
        echo "<body>";
        echo "<h1>BB Debug Information</h1>\n";
        echo "<h2>Page Generation</h2>\n";
        echo "<table bgcolor=\"#666666\" width=\"95%\" cellpadding=\"4\" cellspacing=\"1\" align=\"center\">\n";
        echo "<tr>\n";
        echo "<td bgcolor=\"#cccccc\" colspan=\"4\"><b><span style=\"size:2;\">Page Generation Statistics</span></b></td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">Page Generation Time:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$totaltime</span></td>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">No. DB Queries:</span></b></td>\n";
        echo '<td bgcolor="#fefefe" width="25%"><span style="font-family: tahoma; font-size: 12px;">' . $this->bb->db->query_count . '</span></td>' . "\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">PHP Processing Time:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$phptime ($percentphp%)</span></td>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">DB Processing Time:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$query_time ($percentsql%)</span></td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">Extensions Used:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">{$this->bb->config['database']['type']}, xml</span></td>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">initBB() Processing Time:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$globaltime</span></td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">PHP Version:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$phpversion</span></td>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">Server Load:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$serverload</span></td>\n";
        echo "</tr>\n";
        echo "<tr>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">GZip Encoding Status:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">$gzipen</span></td>\n";
        echo "<td bgcolor=\"#efefef\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">No. Templates Used:</span></b></td>\n";
        echo "<td bgcolor=\"#fefefe\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">" . count($this->bb->templates->cache) . " (" /* (int)count(explode(",", $this->bb->templatelist)) . " Cached / "*/ . (int)count($this->bb->templates->uncached_templates) . " Manually Loaded)</span></td>\n";
        echo "</tr>\n";

        $memory_usage = get_memory_usage();
        if (!$memory_usage) {
            $memory_usage = $this->bb->lang->unknown;
        } else {
            $memory_usage = $this->bb->parser->friendlySize($memory_usage) . " ({$memory_usage} bytes)";
        }
        $memory_limit = @ini_get("memory_limit");
        echo "<tr>\n";
        echo "<td bgcolor=\"#EFEFEF\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">Memory Usage:</span></b></td>\n";
        echo "<td bgcolor=\"#FEFEFE\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">{$memory_usage}</span></td>\n";
        echo "<td bgcolor=\"#EFEFEF\" width=\"25%\"><b><span style=\"font-family: tahoma; font-size: 12px;\">Memory Limit:</span></b></td>\n";
        echo "<td bgcolor=\"#FEFEFE\" width=\"25%\"><span style=\"font-family: tahoma; font-size: 12px;\">{$memory_limit}</span></td>\n";
        echo "</tr>\n";

        echo "</table>\n";

        echo "<h2>Database Connections (" . count($this->bb->db->connections) . " Total) </h2>\n";
        echo "<table style=\"background-color: #666;\" width=\"95%\" cellpadding=\"4\" cellspacing=\"1\" align=\"center\">\n";
        echo "<tr>\n";
        echo "<td style=\"background: #fff;\">" . implode("<br />", $this->bb->db->connections) . "</td>\n";
        echo "</tr>\n";
        echo "</table>\n";
        echo "<br />\n";

        echo "<h2>Database Queries (" . $this->bb->db->query_count . " Total) </h2>\n";
        echo $this->bb->db->explain;
        $total = 0;
        foreach ($this->bb->db->querylist as $var) {
            $total += $var['time'];
        }
        echo "<h2>Total Queries Time (" . format_time_duration($total) . ", DB: " . $this->bb->db->database . ") </h2>\n";
        if ($this->bb->cache->call_count > 0) {
            echo "<h2>Cache Calls (" . $this->bb->cache->call_count . " Total, " . $call_time . ") </h2>\n";
            echo $this->bb->cache->cache_debug;
        }

        echo "<h2>Template Statistics</h2>\n";

        if (count($this->bb->templates->cache) > 0) {
            echo "<table style=\"background-color: #666;\" width=\"95%\" cellpadding=\"4\" cellspacing=\"1\" align=\"center\">\n";
            echo "<tr>\n";
            echo "<td style=\"background-color: #ccc;\"><strong>Templates Used (Loaded for this Page) - " . count($this->bb->templates->cache) . " Total</strong></td>\n";
            echo "</tr>\n";
            echo "<tr>\n";
            echo "<td style=\"background: #fff;\">" . implode(", ", array_keys($this->bb->templates->cache)) . "</td>\n";
            echo "</tr>\n";
            echo "</table>\n";
            echo "<br />\n";
        }

        if (count($this->bb->templates->uncached_templates) > 0) {
            echo "<table style=\"background-color: #666;\" width=\"95%\" cellpadding=\"4\" cellspacing=\"1\" align=\"center\">\n";
            echo "<tr>\n";
            echo "<td style=\"background-color: #ccc;\"><strong>Templates Requiring Additional Calls (Not Cached at Startup) - " . count($this->bb->templates->uncached_templates) . " Total</strong></td>\n";
            echo "</tr>\n";
            echo "<tr>\n";
            echo "<td style=\"background: #fff;\">" . implode(", ", $this->bb->templates->uncached_templates) . "</td>\n";
            echo "</tr>\n";
            echo "</table>\n";
            echo "<br />\n";
        }
        echo "</body>";
        echo "</html>";
        exit;
    }
}
