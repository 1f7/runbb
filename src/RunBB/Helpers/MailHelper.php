<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Helpers;

use RunCMF\Core\AbstractController;

class MailHelper extends AbstractController
{
    /**
     * Build the mass email SQL query for the specified conditions.
     *
     * @param array $conditions Array of conditions to match users against.
     * @return string The generated search SQL
     */
    public function buildMassMailQuery($conditions)
    {
        if (!is_array($conditions)) {
            return '';
        }

        $search_sql = 'u.allownotices=1';

        // List of valid LIKE search fields
        $user_like_fields = ['username', 'email'];
        foreach ($user_like_fields as $search_field) {
            if ($conditions[$search_field]) {
                $search_sql .= " AND u.{$search_field} LIKE '%" .
                    $this->db->escape_string_like($conditions[$search_field]) . "%'";
            }
        }

        // LESS THAN or GREATER THAN
        $direction_fields = ['postnum'];
        foreach ($direction_fields as $search_field) {
            $direction_field = $search_field . '_dir';
            if (!empty($conditions[$search_field]) && $conditions[$direction_field]) {
                switch ($conditions[$direction_field]) {
                    case 'greater_than':
                        $direction = '>';
                        break;
                    case 'less_than':
                        $direction = '<';
                        break;
                    default:
                        $direction = '=';
                }
                $search_sql .= " AND u.{$search_field}{$direction}'" . (int)$conditions[$search_field] . "'";
            }
        }

        // Time-based search fields
        $time_fields = ['regdate', 'lastactive'];
        foreach ($time_fields as $search_field) {
            $time_field = $search_field . '_date';
            $direction_field = $search_field . '_dir';
            if (!empty($conditions[$search_field]) && $conditions[$time_field] && $conditions[$direction_field]) {
                switch ($conditions[$time_field]) {
                    case 'hours':
                        $date = $conditions[$search_field] * 60 * 60;
                        break;
                    case 'days':
                        $date = $conditions[$search_field] * 60 * 60 * 24;
                        break;
                    case 'weeks':
                        $date = $conditions[$search_field] * 60 * 60 * 24 * 7;
                        break;
                    case 'months':
                        $date = $conditions[$search_field] * 60 * 60 * 24 * 30;
                        break;
                    case 'years':
                        $date = $conditions[$search_field] * 60 * 60 * 24 * 365;
                        break;
                    default:
                        $date = $conditions[$search_field] * 60 * 60 * 24;
                }

                switch ($conditions[$direction_field]) {
                    case 'less_than':
                        $direction = '>';
                        break;
                    case 'more_than':
                        $direction = '<';
                        break;
                    default:
                        $direction = '<';
                }
                $search_sql .= " AND u.{$search_field}{$direction}'" . (TIME_NOW - $date) . "'";
            }
        }

        // Usergroup based searching
        if (isset($conditions['usergroup'])) {
            if (!is_array($conditions['usergroup'])) {
                $conditions['usergroup'] = [$conditions['usergroup']];
            }

            $conditions['usergroup'] = array_map('intval', $conditions['usergroup']);

            $additional_sql = '';
            foreach ($conditions['usergroup'] as $usergroup) {
                switch ($this->db->type) {
                    case 'pgsql':
                    case 'sqlite':
                        $additional_sql .= " OR ','||additionalgroups||',' LIKE '%,{$usergroup},%'";
                        break;
                    default:
                        $additional_sql .= " OR CONCAT(',',additionalgroups,',') LIKE '%,{$usergroup},%'";
                }
            }
            $search_sql .= ' AND (u.usergroup IN (' . implode(',', $conditions['usergroup']) . ") {$additional_sql})";
        }

        return $search_sql;
    }

    /**
     * Create a text based version of a HTML mass email.
     *
     * @param string $message The HTML version.
     * @return string The generated text based version.
     */
    public function createTextMessage($message)
    {
        // Cut out all current line breaks
        // Makes links CONTENT (link)
        $message = $this->makePrettyLinks($message);
        $message = str_replace(["\r\n", "\n"], "\n", $message);
        $message = preg_replace("#</p>#i", "\n\n", $message);
        $message = preg_replace("#<br( \/?)>#i", "\n", $message);
        $message = preg_replace("#<p[^>]*?>#i", "", $message);
        $message = preg_replace("#<hr[^>]*?>\s*#i", "-----------\n", $message);
        $message = html_entity_decode($message);
        $message = str_replace("\t", '', $message);
        do {
            $message = str_replace('  ', ' ', $message);
        } while (strpos($message, '  ') !== false);

        $search = ['@<script[^>]*?>.*?</script>@si',  // Strip out javascript
            '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
            '@<title[^>]*?>.*?</title>@siU',    // Strip title tags
            '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
            '@<![\s\S]*?--[ \t\n\r]*>@'        // Strip multi-line comments including CDATA
        ];
        $message = preg_replace($search, '', $message);
        $message = preg_replace("#\n\n+#", "\n\n", $message);
        $message = preg_replace("#^\s+#is", '', $message);
        return $message;
    }

    /**
     * Generates friendly links for a text based version of a mass email from the HTML version.
     *
     * @param string $message_html The HTML version.
     * @return string The version with the friendly links and all <a> tags stripped.
     */
    private function makePrettyLinks($message_html)
    {
        do {
            $start = stripos($message_html, '<a');
            if ($start === false) {
                break;
            }
            $end = stripos($message_html, '</a>', $start);
            if ($end === false) {
                break;
            }

            $a_href = substr($message_html, $start, ($end - $start));

            preg_match("#href=\"?([^\"> ]+)\"?#i", $a_href, $href_matches);
            if (!$href_matches[1]) {
                continue;
            }
            $link = $href_matches[1];

            $contents = strip_tags($a_href);
            if (!$contents) {
                preg_match("#alt=\"?([^\">]+)\"?#i", $a_href, $matches2);
                if ($matches2[1]) {
                    $contents = $matches2[1];
                }
                if (!$contents) {
                    preg_match("#title=\"?([^\">]+)\"?#i", $a_href, $matches2);
                    if ($matches2[1]) {
                        $contents = $matches2[1];
                    }
                }
            }

            $replaced_link = $contents . " ({$link}) ";

            $message_html = substr_replace($message_html, $replaced_link, $start, ($end - $start));
        } while (true);
        return $message_html;
    }

    /**
     * Sends an email using PHP's mail function, formatting it appropriately.
     *
     * @param string $to Address the email should be addressed to.
     * @param string $subject The subject of the email being sent.
     * @param string $message The message being sent.
     * @param string $from The from address of the email, if blank, the board name will be used.
     * @param string $charset The chracter set being used to send this email.
     * @param string $headers
     * @param boolean $keep_alive Do we wish to keep the connection to the mail server alive to send more
     *  than one message (SMTP only)
     * @param string $format The format of the email to be sent (text or html). text is default
     * @param string $message_text The text message of the email if being sent in html format,
     *  for email clients that don't support html
     * @param string $return_email The email address to return to. Defaults to admin return email address.
     * @return bool
     */
    public function send(
        $to,
        $subject,
        $message,
        $from = '',
        $charset = '',
        $headers = '',
        $keep_alive = false,
        $format = 'text',
        $message_text = '',
        $return_email = ''
    ) {
    
        static $mail;

        // Does our object not exist? Create it
        if (!is_object($mail)) {
            if ($this->bb->settings['mail_handler'] == 'smtp') {
                $mail = new \RunBB\Handlers\MailHandlers\SmtpMail($this);
            } else {
                $mail = new \RunBB\Handlers\MailHandlers\PhpMail($this);
            }
        }

        // Using SMTP based mail
        if ($this->bb->settings['mail_handler'] == 'smtp') {
            if ($keep_alive == true) {
                $mail->keep_alive = true;
            }
        } // Using PHP based mail()
        else {
            if ($this->bb->settings['mail_parameters'] != '') {
                $mail->additionalParameters = $this->bb->settings['mail_parameters'];
            }
        }

        // Build and send
        $mail->buildMessage($to, $subject, $message, $from, $charset, $headers, $format, $message_text, $return_email);
        return $mail->send();
    }

    /**
     * Sends a specified amount of messages from the mail queue
     *
     * @param int $count The number of messages to send (Defaults to 10)
     */
    public function sendMailQueue($count = 10)
    {
        $this->plugins->runHooks('send_mail_queue_start');

        // Check to see if the mail queue has messages needing to be sent
        $mailcache = $this->cache->read('mailqueue');
        if ($mailcache['queue_size'] > 0 && ($mailcache['locked'] == 0 || $mailcache['locked'] < TIME_NOW - 300)) {
            // Lock the queue so no other messages can be sent whilst these are (for popular boards)
            $this->cache->update_mailqueue(0, TIME_NOW);

            // Fetch emails for this page view - and send them
            $query = $this->db->simple_select(
                'mailqueue',
                '*',
                '',
                ['order_by' => 'mid', 'order_dir' => 'asc', 'limit_start' => 0, 'limit' => $count]
            );

            while ($email = $this->db->fetch_array($query)) {
                // Delete the message from the queue
                $this->db->delete_query('mailqueue', "mid='{$email['mid']}'");

                if ($this->db->affected_rows() == 1) {
                    $this->mail->send(
                        $email['mailto'],
                        $email['subject'],
                        $email['message'],
                        $email['mailfrom'],
                        '',
                        $email['headers'],
                        true
                    );
                }
            }
            // Update the mailqueue cache and remove the lock
            $this->cache->update_mailqueue(TIME_NOW, 0);
        }

        $this->plugins->runHooks('send_mail_queue_end');
    }
}
