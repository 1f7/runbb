<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Helpers;

class OnlineHelper
{
    private $uid_list = [];
    private $aid_list = [];
    private $pid_list = [];
    private $tid_list = [];
    private $fid_list = [];
    private $ann_list = [];
    private $eid_list = [];

    protected $bb;

    public function __construct($bb)
    {
        $this->bb = $bb;
    }

    /**
     * Fetch a users activity and any corresponding details from their location.
     *
     * @param string $location The location (URL) of the user.
     * @param bool $nopermission
     * @return array Array of location and activity information
     */
    public function fetchWolActivity($location, $nopermission = false)
    {
        $user_activity = [];
        $filename = $this->bb->current_page;

        if ($nopermission) {
            $filename = 'nopermission';
        }

        switch ($filename) {
            case 'announcements':
                if (!isset($parameters['aid'])) {
                    $parameters['aid'] = 0;
                }
                $parameters['aid'] = (int)$parameters['aid'];
                if ($parameters['aid'] > 0) {
                    $this->ann_list[$parameters['aid']] = $parameters['aid'];
                }
                $user_activity['activity'] = 'announcements';
                $user_activity['ann'] = $parameters['aid'];
                break;
            case 'attachment':
                if (!isset($parameters['aid'])) {
                    $parameters['aid'] = 0;
                }
                $parameters['aid'] = (int)$parameters['aid'];
                if ($parameters['aid'] > 0) {
                    $this->aid_list[] = $parameters['aid'];
                }
                $user_activity['activity'] = 'attachment';
                $user_activity['aid'] = $parameters['aid'];
                break;
            case 'calendar':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'event') {
                    if (!isset($parameters['eid'])) {
                        $parameters['eid'] = 0;
                    }
                    $parameters['eid'] = (int)$parameters['eid'];
                    if ($parameters['eid'] > 0) {
                        $this->eid_list[$parameters['eid']] = $parameters['eid'];
                    }
                    $user_activity['activity'] = 'calendar_event';
                    $user_activity['eid'] = $parameters['eid'];
                } elseif ($parameters['action'] == 'addevent' ||
                    $parameters['action'] == 'do_addevent'
                ) {
                    $user_activity['activity'] = 'calendar_addevent';
                } elseif ($parameters['action'] == 'editevent' ||
                    $parameters['action'] == 'do_editevent'
                ) {
                    $user_activity['activity'] = 'calendar_editevent';
                } else {
                    $user_activity['activity'] = 'calendar';
                }
                break;
            case 'contact':
                $user_activity['activity'] = 'contact';
                break;
            case 'editpost':
                $user_activity['activity'] = 'editpost';
                break;
            case 'forumdisplay':
                if (!isset($parameters['fid'])) {
                    $parameters['fid'] = 0;
                }
                $parameters['fid'] = (int)$parameters['fid'];
                if ($parameters['fid'] > 0) {
                    $this->fid_list[$parameters['fid']] = $parameters['fid'];
                }
                $user_activity['activity'] = 'forumdisplay';
                $user_activity['fid'] = $parameters['fid'];
                break;
            case 'index':
                $user_activity['activity'] = 'index';
                break;
            case 'managegroup':
                $user_activity['activity'] = 'managegroup';
                break;
            case 'member':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'activate') {
                    $user_activity['activity'] = 'member_activate';
                } elseif ($parameters['action'] == 'register' ||
                    $parameters['action'] == 'do_register'
                ) {
                    $user_activity['activity'] = 'member_register';
                } elseif ($parameters['action'] == 'login' /*|| $parameters['action'] == 'do_login'*/) {
                    $user_activity['activity'] = 'member_login';
                } elseif ($parameters['action'] == 'logout') {
                    $user_activity['activity'] = 'member_logout';
                } elseif ($parameters['action'] == 'profile') {
                    $user_activity['activity'] = 'member_profile';
                    if (!isset($parameters['uid'])) {
                        $parameters['uid'] = 0;
                    }
                    $parameters['uid'] = (int)$parameters['uid'];
                    if ($parameters['uid'] > 0) {
                        $this->uid_list[$parameters['uid']] = $parameters['uid'];
                    }
                    $user_activity['uid'] = $parameters['uid'];
                } elseif ($parameters['action'] == 'emailuser' ||
                    $parameters['action'] == 'do_emailuser'
                ) {
                    $user_activity['activity'] = 'member_emailuser';
                } elseif ($parameters['action'] == 'rate' ||
                    $parameters['action'] == 'do_rate'
                ) {
                    $user_activity['activity'] = 'member_rate';
                } elseif ($parameters['action'] == 'resendactivation' ||
                    $parameters['action'] == 'do_resendactivation'
                ) {
                    $user_activity['activity'] = 'member_resendactivation';
                } elseif ($parameters['action'] == 'lostpw' ||
                    $parameters['action'] == 'do_lostpw' ||
                    $parameters['action'] == 'resetpassword'
                ) {
                    $user_activity['activity'] = 'member_lostpw';
                } else {
                    $user_activity['activity'] = 'member';
                }
                break;
            case 'memberlist':
                $user_activity['activity'] = 'memberlist';
                break;
            case 'misc':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                $accepted_parameters = [
                    'markread',
                    'help',
                    'buddypopup',
                    'smilies',
                    'syndication',
                    'imcenter',
                    'dstswitch'
                ];
                if ($parameters['action'] == 'whoposted') {
                    if (!isset($parameters['tid'])) {
                        $parameters['tid'] = 0;
                    }
                    $parameters['tid'] = (int)$parameters['tid'];
                    if ($parameters['tid'] > 0) {
                        $this->tid_list[$parameters['tid']] = $parameters['tid'];
                    }
                    $user_activity['activity'] = 'misc_whoposted';
                    $user_activity['tid'] = $parameters['tid'];
                } elseif (in_array($parameters['action'], $accepted_parameters)) {
                    $user_activity['activity'] = 'misc_' . $parameters['action'];
                } else {
                    $user_activity['activity'] = 'misc';
                }
                break;
            case 'modcp':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = 0;
                }

                $accepted_parameters = ['modlogs', 'announcements', 'finduser', 'warninglogs', 'ipsearch'];

                foreach ($accepted_parameters as $action) {
                    if ($parameters['action'] == $action) {
                        $user_activity['activity'] = 'modcp_' . $action;
                        break;
                    }
                }

                $accepted_parameters = [];
                $accepted_parameters['report'] = ['do_reports', 'reports', 'allreports'];
                $accepted_parameters['new_announcement'] = ['do_new_announcement', 'new_announcement'];
                $accepted_parameters['delete_announcement'] = ['do_delete_announcement', 'delete_announcement'];
                $accepted_parameters['edit_announcement'] = ['do_edit_announcement', 'edit_announcement'];
                $accepted_parameters['mod_queue'] = ['do_modqueue', 'modqueue'];
                $accepted_parameters['editprofile'] = ['do_editprofile', 'editprofile'];
                $accepted_parameters['banning'] = ['do_banuser', 'banning', 'liftban', 'banuser'];

                foreach ($accepted_parameters as $name => $actions) {
                    if (in_array($parameters['action'], $actions)) {
                        $user_activity['activity'] = 'modcp_' . $name;
                        break;
                    }
                }

                if (empty($user_activity['activity'])) {
                    $user_activity['activity'] = 'modcp';
                }
                break;
            case 'moderation':
                $user_activity['activity'] = 'moderation';
                break;
            case 'newreply':
                if (!isset($parameters['tid'])) {
                    $parameters['tid'] = 0;
                }
                $parameters['tid'] = (int)$parameters['tid'];
                if ($parameters['tid'] > 0) {
                    $this->tid_list[$parameters['tid']] = $parameters['tid'];
                }
                $user_activity['activity'] = 'newreply';
                $user_activity['tid'] = $parameters['tid'];
                break;
            case 'newthread':
                if (!isset($parameters['fid'])) {
                    $parameters['fid'] = 0;
                }
                $parameters['fid'] = (int)$parameters['fid'];
                if ($parameters['fid'] > 0) {
                    $this->fid_list[$parameters['fid']] = $parameters['fid'];
                }
                $user_activity['activity'] = 'newthread';
                $user_activity['fid'] = $parameters['fid'];
                break;
            case 'online':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'today') {
                    $user_activity['activity'] = 'woltoday';
                } else {
                    $user_activity['activity'] = 'wol';
                }
                break;
            case 'polls':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                // Make the 'do' parts the same as the other one.
                if ($parameters['action'] == 'do_newpoll') {
                    $user_activity['activity'] = 'newpoll';
                } elseif ($parameters['action'] == 'do_editpoll') {
                    $user_activity['activity'] = 'editpoll';
                } else {
                    $accepted_parameters = ['do_editpoll', 'editpoll', 'newpoll', 'do_newpoll', 'showresults', 'vote'];

                    foreach ($accepted_parameters as $action) {
                        if ($parameters['action'] == $action) {
                            $user_activity['activity'] = $action;
                            break;
                        }
                    }

                    if (!$user_activity['activity']) {
                        $user_activity['activity'] = 'showresults';
                    }
                }
                break;
            case 'printthread':
                if (!isset($parameters['tid'])) {
                    $parameters['tid'] = 0;
                }
                $parameters['tid'] = (int)$parameters['tid'];
                if ($parameters['tid'] > 0) {
                    $this->tid_list[$parameters['tid']] = $parameters['tid'];
                }
                $user_activity['activity'] = 'printthread';
                $user_activity['tid'] = $parameters['tid'];
                break;
            case 'private':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'send' || $parameters['action'] == 'do_send') {
                    $user_activity['activity'] = 'private_send';
                } elseif ($parameters['action'] == 'read') {
                    $user_activity['activity'] = 'private_read';
                } elseif ($parameters['action'] == 'folders' || $parameters['action'] == 'do_folders') {
                    $user_activity['activity'] = 'private_folders';
                } else {
                    $user_activity['activity'] = 'private';
                }
                break;
            case 'ratethread':
                $user_activity['activity'] = 'ratethread';
                break;
            case 'report':
                $user_activity['activity'] = 'report';
                break;
            case 'reputation':
                if (!isset($parameters['uid'])) {
                    $parameters['uid'] = 0;
                }
                $parameters['uid'] = (int)$parameters['uid'];
                if ($parameters['uid'] > 0) {
                    $this->uid_list[$parameters['uid']] = $parameters['uid'];
                }
                $user_activity['uid'] = $parameters['uid'];

                if ($parameters['action'] == 'add') {
                    $user_activity['activity'] = 'reputation';
                } else {
                    $user_activity['activity'] = 'reputation_report';
                }
                break;
            case 'search':
                $user_activity['activity'] = 'search';
                break;
            case 'sendthread':
                if (!isset($parameters['tid'])) {
                    $parameters['tid'] = 0;
                }
                $parameters['tid'] = (int)$parameters['tid'];
                if ($parameters['tid'] > 0) {
                    $this->tid_list[$parameters['tid']] = $parameters['tid'];
                }
                $user_activity['activity'] = 'sendthread';
                $user_activity['tid'] = $parameters['tid'];
                break;
            case 'showteam':
                $user_activity['activity'] = 'showteam';
                break;
            case 'showthread':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = 0;
                }
                if (!isset($parameters['pid'])) {
                    $parameters['pid'] = 0;
                }
                $parameters['pid'] = (int)$parameters['pid'];
                if ($parameters['pid'] > 0 && $parameters['action'] == 'showpost') {
                    $this->pid_list[$parameters['pid']] = $parameters['pid'];
                    $user_activity['activity'] = 'showpost';
                    $user_activity['pid'] = $parameters['pid'];
                } else {
                    if (!isset($parameters['page'])) {
                        $parameters['page'] = 0;
                    }
                    $parameters['page'] = (int)$parameters['page'];
                    $user_activity['page'] = $parameters['page'];
                    if (!isset($parameters['tid'])) {
                        $parameters['tid'] = 0;
                    }
                    $parameters['tid'] = (int)$parameters['tid'];
                    if ($parameters['tid'] > 0) {
                        $this->tid_list[$parameters['tid']] = $parameters['tid'];
                    }
                    $user_activity['activity'] = 'showthread';
                    $user_activity['tid'] = $parameters['tid'];
                }
                break;
            case 'stats':
                $user_activity['activity'] = 'stats';
                break;
            case 'usercp':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'profile' || $parameters['action'] == 'do_profile') {
                    $user_activity['activity'] = 'usercp_profile';
                } elseif ($parameters['action'] == 'options' || $parameters['action'] == 'do_options') {
                    $user_activity['activity'] = 'usercp_options';
                } elseif ($parameters['action'] == 'password' || $parameters['action'] == 'do_password') {
                    $user_activity['activity'] = 'usercp_password';
                } elseif ($parameters['action'] == 'editsig' || $parameters['action'] == 'do_editsig') {
                    $user_activity['activity'] = 'usercp_editsig';
                } elseif ($parameters['action'] == 'avatar' || $parameters['action'] == 'do_avatar') {
                    $user_activity['activity'] = 'usercp_avatar';
                } elseif ($parameters['action'] == 'editlists' || $parameters['action'] == 'do_editlists') {
                    $user_activity['activity'] = 'usercp_editlists';
                } elseif ($parameters['action'] == 'favorites') {
                    $user_activity['activity'] = 'usercp_favorites';
                } elseif ($parameters['action'] == 'subscriptions') {
                    $user_activity['activity'] = 'usercp_subscriptions';
                } elseif ($parameters['action'] == 'notepad' || $parameters['action'] == 'do_notepad') {
                    $user_activity['activity'] = 'usercp_notepad';
                } else {
                    $user_activity['activity'] = 'usercp';
                }
                break;
            case 'usercp2':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'addfavorite' ||
                    $parameters['action'] == 'removefavorite' ||
                    $parameters['action'] == 'removefavorites'
                ) {
                    $user_activity['activity'] = 'usercp2_favorites';
                } elseif ($parameters['action'] == 'addsubscription' ||
                    $parameters['action'] == 'do_addsubscription' ||
                    $parameters['action'] == 'removesubscription' ||
                    $parameters['action'] == 'removesubscriptions'
                ) {
                    $user_activity['activity'] = 'usercp2_subscriptions';
                }
                break;
            case 'portal':
                $user_activity['activity'] = 'portal';
                break;
            case 'warnings':
                if (!isset($parameters['action'])) {
                    $parameters['action'] = '';
                }
                if ($parameters['action'] == 'warn' || $parameters['action'] == 'do_warn') {
                    $user_activity['activity'] = 'warnings_warn';
                } elseif ($parameters['action'] == 'do_revoke') {
                    $user_activity['activity'] = 'warnings_revoke';
                } elseif ($parameters['action'] == 'view') {
                    $user_activity['activity'] = 'warnings_view';
                } else {
                    $user_activity['activity'] = 'warnings';
                }
                break;
            case 'nopermission':
                $user_activity['activity'] = 'nopermission';
                $user_activity['nopermission'] = 1;
                break;
            default:
                $user_activity['activity'] = 'unknown';
                break;
        }

        // Expects $location to be passed through already sanitized
        $user_activity['location'] = $location;

        $user_activity = $this->bb->plugins->runHooks('fetch_wol_activity_end', $user_activity);

        return $user_activity;
    }

    /**
     * Builds a friendly named Who's Online location from an 'activity' and array of user data.
     * Assumes fetchWolActivity has already been called.
     *
     * @param array $user_activity Array containing activity and essential IDs.
     * @return string Location name for the activity being performed.
     */
    public function buildFriendlyWolLocation($user_activity)
    {
        // Fetch forum permissions for this user
        $unviewableforums = $this->bb->forum->get_unviewable_forums();
        $inactiveforums = $this->bb->forum->get_inactive_forums();
        $fidnot = '';
        $unviewablefids = $inactivefids = [];
        if ($unviewableforums) {
            $fidnot = ' AND fid NOT IN (' . implode(',', $unviewableforums) . ')';
            $unviewablefids = $unviewableforums;
        }
        if ($inactiveforums) {
            $fidnot .= ' AND fid NOT IN (' . implode(',', $inactiveforums) . ')';
            $inactivefids = $inactiveforums;
        }

        // Fetch any users
        if (!isset($usernames) && count($this->uid_list) > 0) {
            $uid_sql = implode(',', $this->uid_list);
            if ($uid_sql != $this->bb->user->uid) {
                $query = $this->bb->db->simple_select('users', 'uid,username', "uid IN ($uid_sql)");
                while ($user = $this->bb->db->fetch_array($query)) {
                    $usernames[$user['uid']] = $user['username'];
                }
            } else {
                $usernames[$this->bb->user->uid] = $this->bb->user->username;
            }
        }

        // Fetch any attachments
        if (!isset($this->bb->attachments) && count($this->aid_list) > 0) {
            $aid_sql = implode(',', $this->aid_list);
            $query = $this->bb->db->simple_select('attachments', 'aid,pid', "aid IN ($aid_sql)");
            while ($attachment = $this->bb->db->fetch_array($query)) {
                $attachments[$attachment['aid']] = $attachment['pid'];
                $this->pid_list[] = $attachment['pid'];
            }
        }

        // Fetch any announcements
        if (!isset($this->bb->announcements) && count($this->ann_list) > 0) {
            $aid_sql = implode(',', $this->ann_list);
            $query = $this->bb->db->simple_select('announcements', 'aid,subject', "aid IN ({$aid_sql}) {$fidnot}");
            while ($announcement = $this->bb->db->fetch_array($query)) {
                $announcement_title = htmlspecialchars_uni($this->parser->parse_badwords($announcement['subject']));
                $this->bb->announcements[$announcement['aid']] = $announcement_title;
            }
        }

        // Fetch any posts
        if (!isset($this->bb->posts) && count($this->pid_list) > 0) {
            $pid_sql = implode(',', $this->pid_list);
            $query = $this->bb->db->simple_select('posts', 'pid,tid', "pid IN ({$pid_sql}) {$fidnot}");
            while ($post = $this->bb->db->fetch_array($query)) {
                $this->bb->posts[$post['pid']] = $post['tid'];
                $this->tid_list[] = $post['tid'];
            }
        }

        // Fetch any threads
        if (!isset($this->bb->threads) && count($this->tid_list) > 0) {
            $perms = [];
            $tid_sql = implode(',', $this->tid_list);
            $query = $this->bb->db->simple_select(
                'threads',
                'uid, fid, tid, subject, visible, prefix',
                "tid IN({$tid_sql}) {$fidnot}"
            );

            $threadprefixes = $this->bb->thread->build_prefixes();

            while ($thread = $this->bb->db->fetch_array($query)) {
                $thread['threadprefix'] = '';
                if ($thread['prefix'] && !empty($threadprefixes[$thread['prefix']])) {
                    $thread['threadprefix'] = $threadprefixes[$thread['prefix']]['displaystyle'];
                }
                if (empty($perms[$thread['fid']])) {
                    $perms[$thread['fid']] = $this->bb->forum->forum_permissions($thread['fid']);
                }

                if (isset($perms[$thread['fid']]['canonlyviewownthreads']) &&
                    $perms[$thread['fid']]['canonlyviewownthreads'] == 1 &&
                    $thread['uid'] != $this->bb->user->uid &&
                    !$this->bb->user->is_moderator($thread['fid'])
                ) {
                    continue;
                }

                if ($this->bb->user->is_moderator($thread['fid']) || $thread['visible'] == 1) {
                    $thread_title = '';
                    if ($thread['threadprefix']) {
                        $thread_title = $thread['threadprefix'] . '&nbsp;';
                    }

                    $thread_title .= htmlspecialchars_uni($this->parser->parse_badwords($thread['subject']));

                    $threads[$thread['tid']] = $thread_title;
                    $this->fid_list[] = $thread['fid'];
                }
            }
        }

        // Fetch any forums
        if (!isset($this->bb->forums) && count($this->fid_list) > 0) {
            $fidnot = array_merge($unviewablefids, $inactivefids);

            foreach ($this->bb->forum_cache as $fid => $forum) {
                if (in_array($fid, $this->fid_list) && !in_array($fid, $fidnot)) {
                    $forums[$fid] = $forum['name'];
                    $forums_linkto[$fid] = $forum['linkto'];
                }
            }
        }

        // And finaly any events
        if (!isset($this->bb->events) && count($this->eid_list) > 0) {
            $eid_sql = implode(',', $this->eid_list);
            $query = $this->bb->db->simple_select('events', 'eid,name', "eid IN ($eid_sql)");
            while ($event = $this->bb->db->fetch_array($query)) {
                $events[$event['eid']] = htmlspecialchars_uni($this->parser->parse_badwords($event['name']));
            }
        }

        // Now we've got everything we need we can put a name to the location
        switch ($user_activity['activity']) {
            // announcement.php functions
            case 'announcements':
                if (!empty($this->bb->announcements[$user_activity['ann']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->viewing_announcements,
                        get_announcement_link($user_activity['ann']),
                        $this->bb->announcements[$user_activity['ann']]
                    );
                } else {
                    $location_name = $this->bb->lang->viewing_announcements2;
                }
                break;
            // attachment.php actions
            case 'attachment':
                $pid = $attachments[$user_activity['aid']];
                $tid = $this->bb->posts[$pid];
                if (!empty($threads[$tid])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->viewing_attachment2,
                        $user_activity['aid'],
                        $threads[$tid],
                        get_thread_link($tid)
                    );
                } else {
                    $location_name = $this->bb->lang->viewing_attachment;
                }
                break;
            // calendar.php functions
            case 'calendar':
                $location_name = $this->bb->lang->viewing_calendar;
                break;
            case 'calendar_event':
                if (!empty($events[$user_activity['eid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->viewing_event2,
                        get_event_link($user_activity['eid']),
                        $events[$user_activity['eid']]
                    );
                } else {
                    $location_name = $this->bb->lang->viewing_event;
                }
                break;
            case 'calendar_addevent':
                $location_name = $this->bb->lang->adding_event;
                break;
            case 'calendar_editevent':
                $location_name = $this->bb->lang->editing_event;
                break;
            case 'contact':
                $location_name = $this->bb->lang->viewing_contact_us;
                break;
            // editpost.php functions
            case 'editpost':
                $location_name = $this->bb->lang->editing_post;
                break;
            // forumdisplay.php functions
            case 'forumdisplay':
                if (!empty($forums[$user_activity['fid']])) {
                    if ($forums_linkto[$user_activity['fid']]) {
                        $location_name = $this->bb->lang->sprintf(
                            $this->bb->lang->forum_redirect_to,
                            $this->bb->forum->get_forum_link($user_activity['fid']),
                            $forums[$user_activity['fid']]
                        );
                    } else {
                        $location_name = $this->bb->lang->sprintf(
                            $this->bb->lang->viewing_forum2,
                            $this->bb->forum->get_forum_link($user_activity['fid']),
                            $forums[$user_activity['fid']]
                        );
                    }
                } else {
                    $location_name = $this->bb->lang->viewing_forum;
                }
                break;
            // index.php functions
            case 'index':
                $location_name = $this->bb->lang->sprintf(
                    $this->bb->lang->viewing_index,
                    $this->bb->settings['bbname']
                );
                break;
            // managegroup.php functions
            case 'managegroup':
                $location_name = $this->bb->lang->managing_group;
                break;
            // member.php functions
            case 'member_activate':
                $location_name = $this->bb->lang->activating_account;
                break;
            case 'member_profile':
                if (!empty($usernames[$user_activity['uid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->viewing_profile2,
                        get_profile_link($user_activity['uid']),
                        $usernames[$user_activity['uid']]
                    );
                } else {
                    $location_name = $this->bb->lang->viewing_profile;
                }
                break;
            case 'member_register':
                $location_name = $this->bb->lang->registering;
                break;
            case 'member':
            case 'member_login':
                // Guest or member?
                if ($this->bb->user->uid == 0) {
                    $location_name = $this->bb->lang->logging_in;
                } else {
                    $location_name = $this->bb->lang->logging_in_plain;
                }
                break;
            case 'member_logout':
                $location_name = $this->bb->lang->logging_out;
                break;
            case 'member_emailuser':
                $location_name = $this->bb->lang->emailing_user;
                break;
            case 'member_rate':
                $location_name = $this->bb->lang->rating_user;
                break;
            case 'member_resendactivation':
                $location_name = $this->bb->lang->member_resendactivation;
                break;
            case 'member_lostpw':
                $location_name = $this->bb->lang->member_lostpw;
                break;
            // memberlist.php functions
            case 'memberlist':
                $location_name = $this->bb->lang->viewing_memberlist;
                break;
            // misc.php functions
            case 'misc_dstswitch':
                $location_name = $this->bb->lang->changing_dst;
                break;
            case 'misc_whoposted':
                if (!empty($threads[$user_activity['tid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->viewing_whoposted2,
                        get_thread_link($user_activity['tid']),
                        $threads[$user_activity['tid']]
                    );
                } else {
                    $location_name = $this->bb->lang->viewing_whoposted;
                }
                break;
            case 'misc_markread':
                $location_name = $this->bb->lang->sprintf($this->bb->lang->marking_read, $this->bb->post_code);
                break;
            case 'misc_help':
                $location_name = $this->bb->lang->viewing_helpdocs;
                break;
            case 'misc_buddypopup':
                $location_name = $this->bb->lang->viewing_buddylist;
                break;
            case 'misc_smilies':
                $location_name = $this->bb->lang->viewing_smilies;
                break;
            case 'misc_syndication':
                $location_name = $this->bb->lang->viewing_syndication;
                break;
            case 'misc_imcenter':
                $location_name = $this->bb->lang->viewing_imcenter;
                break;
            // modcp.php functions
            case 'modcp_modlogs':
                $location_name = $this->bb->lang->viewing_modlogs;
                break;
            case 'modcp_announcements':
                $location_name = $this->bb->lang->managing_announcements;
                break;
            case 'modcp_finduser':
                $location_name = $this->bb->lang->search_for_user;
                break;
            case 'modcp_warninglogs':
                $location_name = $this->bb->lang->managing_warninglogs;
                break;
            case 'modcp_ipsearch':
                $location_name = $this->bb->lang->searching_ips;
                break;
            case 'modcp_report':
                $location_name = $this->bb->lang->viewing_reports;
                break;
            case 'modcp_new_announcement':
                $location_name = $this->bb->lang->adding_announcement;
                break;
            case 'modcp_delete_announcement':
                $location_name = $this->bb->lang->deleting_announcement;
                break;
            case 'modcp_edit_announcement':
                $location_name = $this->bb->lang->editing_announcement;
                break;
            case 'modcp_mod_queue':
                $location_name = $this->bb->lang->managing_modqueue;
                break;
            case 'modcp_editprofile':
                $location_name = $this->bb->lang->editing_user_profiles;
                break;
            case 'modcp_banning':
                $location_name = $this->bb->lang->managing_bans;
                break;
            case 'modcp':
                $location_name = $this->bb->lang->viewing_modcp;
                break;
            // moderation.php functions
            case 'moderation':
                $location_name = $this->bb->lang->using_modtools;
                break;
            // newreply.php functions
            case 'newreply':
                if (!empty($threads[$user_activity['tid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->replying_thread2,
                        get_thread_link($user_activity['tid']),
                        $threads[$user_activity['tid']]
                    );
                } else {
                    $location_name = $this->bb->lang->replying_thread;
                }
                break;
            // newthread.php functions
            case 'newthread':
                if (!empty($forums[$user_activity['fid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->posting_thread2,
                        $this->bb->forum->get_forum_link($user_activity['fid']),
                        $forums[$user_activity['fid']]
                    );
                } else {
                    $location_name = $this->bb->lang->posting_thread;
                }
                break;
            // online.php functions
            case 'wol':
                $location_name = $this->bb->lang->viewing_wol;
                break;
            case 'woltoday':
                $location_name = $this->bb->lang->viewing_woltoday;
                break;
            // polls.php functions
            case 'newpoll':
                $location_name = $this->bb->lang->creating_poll;
                break;
            case 'editpoll':
                $location_name = $this->bb->lang->editing_poll;
                break;
            case 'showresults':
                $location_name = $this->bb->lang->viewing_pollresults;
                break;
            case 'vote':
                $location_name = $this->bb->lang->voting_poll;
                break;
            // printthread.php functions
            case 'printthread':
                if (!empty($threads[$user_activity['tid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->printing_thread2,
                        get_thread_link($user_activity['tid']),
                        $threads[$user_activity['tid']]
                    );
                } else {
                    $location_name = $this->bb->lang->printing_thread;
                }
                break;
            // private.php functions
            case 'private_send':
                $location_name = $this->bb->lang->sending_pm;
                break;
            case 'private_read':
                $location_name = $this->bb->lang->reading_pm;
                break;
            case 'private_folders':
                $location_name = $this->bb->lang->editing_pmfolders;
                break;
            case 'private':
                $location_name = $this->bb->lang->using_pmsystem;
                break;
            /* Ratethread functions */
            case 'ratethread':
                $location_name = $this->bb->lang->rating_thread;
                break;
            // report.php functions
            case 'report':
                $location_name = $this->bb->lang->reporting_post;
                break;
            // reputation.php functions
            case 'reputation':
                $location_name = $this->bb->lang->sprintf(
                    $this->bb->lang->giving_reputation,
                    get_profile_link($user_activity['uid']),
                    $usernames[$user_activity['uid']]
                );
                break;
            case 'reputation_report':
                if (!empty($usernames[$user_activity['uid']])) {
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->viewing_reputation_report,
                        $this->bb->settings['bburl'] . "/reputation?uid={$user_activity['uid']}",
                        $usernames[$user_activity['uid']]
                    );
                } else {
                    $location_name = $this->bb->lang->sprintf($this->bb->lang->viewing_reputation_report2);
                }
                break;
            // search.php functions
            case 'search':
                $location_name = $this->bb->lang->sprintf(
                    $this->bb->lang->searching_forum,
                    $this->bb->settings['bbname']
                );
                break;
            // showthread.php functions
            case 'showthread':
                if (!empty($threads[$user_activity['tid']])) {
                    $pagenote = '';
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->reading_thread2,
                        get_thread_link($user_activity['tid']),
                        $threads[$user_activity['tid']],
                        $pagenote
                    );
                } else {
                    $location_name = $this->bb->lang->reading_thread;
                }
                break;
            case 'showpost':
                if (!empty($this->bb->posts[$user_activity['pid']]) &&
                    !empty($threads[$this->bb->posts[$user_activity['pid']]])) {
                    $pagenote = '';
                    $location_name = $this->bb->lang->sprintf(
                        $this->bb->lang->reading_thread2,
                        get_thread_link($this->bb->posts[$user_activity['pid']]),
                        $threads[$this->bb->posts[$user_activity['pid']]],
                        $pagenote
                    );
                } else {
                    $location_name = $this->bb->lang->reading_thread;
                }
                break;
            // showteam.php functions
            case 'showteam':
                $location_name = $this->bb->lang->viewing_team;
                break;
            // stats.php functions
            case 'stats':
                $location_name = $this->bb->lang->viewing_stats;
                break;
            // usercp.php functions
            case 'usercp_profile':
                $location_name = $this->bb->lang->updating_profile;
                break;
            case 'usercp_editlists':
                $location_name = $this->bb->lang->managing_buddyignorelist;
                break;
            case 'usercp_options':
                $location_name = $this->bb->lang->updating_options;
                break;
            case 'usercp_editsig':
                $location_name = $this->bb->lang->editing_signature;
                break;
            case 'usercp_avatar':
                $location_name = $this->bb->lang->changing_avatar;
                break;
            case 'usercp_subscriptions':
                $location_name = $this->bb->lang->viewing_subscriptions;
                break;
            case 'usercp_favorites':
                $location_name = $this->bb->lang->viewing_favorites;
                break;
            case 'usercp_notepad':
                $location_name = $this->bb->lang->editing_pad;
                break;
            case 'usercp_password':
                $location_name = $this->bb->lang->editing_password;
                break;
            case 'usercp':
                $location_name = $this->bb->lang->user_cp;
                break;
            case 'usercp2_favorites':
                $location_name = $this->bb->lang->managing_favorites;
                break;
            case 'usercp2_subscriptions':
                $location_name = $this->bb->lang->managing_subscriptions;
                break;
            case 'portal':
                $location_name = $this->bb->lang->viewing_portal;
                break;
            // sendthread.php functions
            case 'sendthread':
                $location_name = $this->bb->lang->sending_thread;
                break;
            // warnings.php functions
            case 'warnings_revoke':
                $location_name = $this->bb->lang->revoking_warning;
                break;
            case 'warnings_warn':
                $location_name = $this->bb->lang->warning_user;
                break;
            case 'warnings_view':
                $location_name = $this->bb->lang->viewing_warning;
                break;
            case 'warnings':
                $location_name = $this->bb->lang->managing_warnings;
                break;
        }

        $plugin_array = ['user_activity' => &$user_activity, 'location_name' => &$location_name];
        $this->bb->plugins->runHooks('build_friendly_wol_location_end', $plugin_array);

        if (isset($user_activity['nopermission']) && $user_activity['nopermission'] == 1) {
            $location_name = $this->bb->lang->viewing_noperms;
        }

        if (!$location_name) {
            $location_name = $this->bb->lang->sprintf($this->bb->lang->unknown_location, $user_activity['location']);
        }

        return $location_name;
    }

    /**
     * Build a Who's Online row for a specific user
     *
     * @param array $user Array of user information including activity information
     * @return string Formatted online row
     */
    public function buildWolRow($user)
    {
        // We have a registered user
        if ($user['uid'] > 0) {
            // Only those with 'canviewwolinvis' permissions can view invisible users
            if ($user['invisible'] != 1 ||
                $this->bb->usergroup['canviewwolinvis'] == 1 ||
                $user['uid'] == $this->bb->user->uid
            ) {
                // Append an invisible mark if the user is invisible
                if ($user['invisible'] == 1) {
                    $invisible_mark = '*';
                } else {
                    $invisible_mark = '';
                }

                $user['username'] = $this->bb->user->format_name(
                    $user['username'],
                    $user['usergroup'],
                    $user['displaygroup']
                );
                $online_name = $this->bb->user->build_profile_link($user['username'], $user['uid']) . $invisible_mark;
            }
        } // We have a bot
        elseif (!empty($user['bot'])) {
            $online_name = $this->bb->user->format_name($user['bot'], $user['usergroup']);
        } // Otherwise we've got a plain old guest
        else {
            $online_name = $this->bb->user->format_name($this->bb->lang->guest, 1);
        }

        $online_time = $this->bb->time->formatDate($this->bb->settings['timeformat'], $user['time']);

        // Fetch the location name for this users activity
        $location = $this->buildFriendlyWolLocation($user['activity']);

        // Can view IPs, then fetch the IP template
        if ($this->bb->usergroup['canviewonlineips'] == 1) {
            if ($this->bb->usergroup['canmodcp'] == 1 && $this->bb->usergroup['canuseipsearch'] == 1) {
                $lookup = "<a href=\"{$this->bb->settings['bburl']}/modcp/ipsearch?ipaddress={$user['ip']}
                &amp;search_users=1\">{$this->bb->lang->lookup}</a>";
            }
            $user_ip = "<br /><span class=\"smalltext\">{$this->bb->lang->ip} {$user['ip']} {$lookup}</span>";
        } else {
            $user_ip = $lookup = $user['ip'] = '';
        }

        $online_row = [];
        // And finally if we have permission to view this user, return the completed online row
        if ($user['invisible'] != 1 || $this->bb->usergroup['canviewwolinvis'] == 1 ||
            $user['uid'] == $this->bb->user->uid) {
            $online_row = [
                'online_name' => $online_name,
                'user_ip' => $user_ip,
                'online_time' => $online_time,
                'location' => $location
            ];
        }
        return $online_row;
    }
}
