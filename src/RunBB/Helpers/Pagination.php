<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Helpers;

use RunCMF\Core\AbstractController;

class Pagination extends AbstractController
{
    /**
     * Generate a listing of page - pagination
     *
     * @param int $count The number of items
     * @param int $perpage The number of items to be shown per page
     * @param int $page The current page number
     * @param string $url The URL to have page numbers tacked on to (If {page} is specified,
     *      the value will be replaced with the page #)
     * @param boolean $breadcrumb Whether or not the multipage is being shown in the navigation breadcrumb
     * @return string The generated pagination
     */
    public function multipage($count, $perpage, $page, $url, $breadcrumb = false)
    {
        //global $theme, $templates, $lang, $mybb;

        if ($count <= $perpage) {
            return '';
        }

        $url = str_replace('&amp;', '&', $url);
        $url = htmlspecialchars_uni($url);

        $pages = ceil($count / $perpage);

        $prevpage = '';
        if ($page > 1) {
            $prev = $page - 1;
            $page_url = $this->fetchPageUrl($url, $prev);
            $prevpage = "<a href=\"{$page_url}\" class=\"pagination_previous\">&laquo; {$this->lang->previous}</a>";
        }

        // Maximum number of 'page bits' to show
        if (!$this->bb->settings['maxmultipagelinks']) {
            $this->bb->settings['maxmultipagelinks'] = 5;
        }

        $from = $page - floor($this->bb->settings['maxmultipagelinks'] / 2);
        $to = $page + floor($this->bb->settings['maxmultipagelinks'] / 2);

        if ($from <= 0) {
            $from = 1;
            $to = $from + $this->bb->settings['maxmultipagelinks'] - 1;
        }

        if ($to > $pages) {
            $to = $pages;
            $from = $pages - $this->bb->settings['maxmultipagelinks'] + 1;
            if ($from <= 0) {
                $from = 1;
            }
        }

        if ($to == 0) {
            $to = $pages;
        }

        $start = '';
        if ($from > 1) {
            if ($from - 1 == 1) {
                $this->lang->multipage_link_start = '';
            }

            $page_url = $this->fetchPageUrl($url, 1);
            $start = "<a href=\"{$page_url}\" class=\"pagination_first\">1</a> {$this->lang->multipage_link_start}";
        }

        $mppage = '';
        for ($i = $from; $i <= $to; ++$i) {
            $page_url = $this->fetchPageUrl($url, $i);
            if ($page == $i) {
                if ($breadcrumb == true) {
                    $mppage .= " <a href=\"{$page_url}\" class=\"pagination_current\">{$i}</a>";
                } else {
                    $mppage .= " <span class=\"pagination_current\">{$i}</span>";
                }
            } else {
                $mppage .= "<a href=\"{$page_url}\" class=\"pagination_page\">{$i}</a>";
            }
        }

        $end = '';
        if ($to < $pages) {
            if ($to + 1 == $pages) {
                $this->lang->multipage_link_end = '';
            }

            $page_url = $this->fetchPageUrl($url, $pages);
            $end = "{$this->lang->multipage_link_end} <a href=\"{$page_url}\" class=\"pagination_last\">{$pages}</a>";
        }

        $nextpage = '';
        if ($page < $pages) {
            $next = $page + 1;
            $page_url = $this->fetchPageUrl($url, $next);
            $nextpage = "<a href=\"{$page_url}\" class=\"pagination_next\">{$this->lang->next} &raquo;</a>";
        }

        $jumptopage = '';
        if ($pages > ($this->bb->settings['maxmultipagelinks'] + 1) &&
            $this->bb->settings['jumptopagemultipage'] == 1) {
            // When the second parameter is set to 1, fetchPageUrl thinks it's the first page and removes it
            // from the URL as it's unnecessary
            $jump_url = $this->fetchPageUrl($url, 1);
            $jumptopage = "
            <div class=\"popup_menu drop_go_page\" style=\"display: none;\">
                <form action=\"{$jump_url}\" method=\"post\">
                <label for=\"page\">{$this->lang->multipage_jump}:</label> 
                <input type=\"text\" class=\"textbox\" name=\"page\" value=\"{$page}\" size=\"4\" />
                <input type=\"submit\" class=\"button\" value=\"{$this->lang->go}\" />
                </form>
            </div>
            <a href=\"javascript:;\" class=\"go_page\" title=\"{$this->lang->multipage_jump}\">
                <img src=\"{$this->bb->theme['imgdir']}/arrow_down.png\" alt=\"{$this->lang->multipage_jump}\" />
            </a>&nbsp;
            <script type=\"text/javascript\">
                var go_page = 'go_page_' + $(\".go_page\").length;
                $(\".go_page\").last().attr('id', go_page);
                $(\".drop_go_page\").last().attr('id', go_page + '_popup');
                $('#' + go_page).popupMenu(false).click(function() {
                var drop_go_page = $(this).prev('.drop_go_page');
                if (drop_go_page.is(':visible')) {
                drop_go_page.find('.textbox').focus();
                }
                });
            </script>";
        }

        $this->lang->multipage_pages = $this->lang->sprintf($this->lang->multipage_pages, $pages);

        if ($breadcrumb == true) {
            $multipage = "
            <div id=\"breadcrumb_multipage_popup\" class=\"pagination pagination_breadcrumb\" style=\"display: none;\">
            {$prevpage}{$start}{$mppage}{$end}{$nextpage}
            </div>
            <script type=\"text/javascript\">
            // <!--
            if(use_xmlhttprequest == \"1\")
            {
              $(\"#breadcrumb_multipage\").popupMenu();
            }
            // -->
            </script";
        } else {
            $multipage = "
              <div class=\"pagination\">
                <span class=\"pages\">{$this->lang->multipage_pages}</span>
                {$prevpage}{$start}{$mppage}{$end}{$nextpage}{$jumptopage}
              </div>";
            //ev al("\$multipage = \"" . $this->templates->get('multipage') . "\";");
        }

        return $multipage;
    }

    /**
     * Generate a page URL for use by the multipage function
     *
     * @param string $url The URL being passed
     * @param int $page The page number
     * @return string
     */
    public function fetchPageUrl($url, $page)
    {
        if ($page <= 1) {
            $find = [
                '-page-{page}',
                '&amp;page={page}',
                '{page}'
            ];

            // Remove 'Page 1' to the defacto URL
            $url = str_replace($find, ['', '', $page], $url);
            return $url;
        } elseif (strpos($url, '{page}') === false) {
            // If no page identifier is specified we tack it on to the end of the URL
            if (strpos($url, '?') === false) {
                $url .= '?';
            } else {
                $url .= '&amp;';
            }

            $url .= "page=$page";
        } else {
            $url = str_replace('{page}', $page, $url);
        }

        return $url;
    }
}
