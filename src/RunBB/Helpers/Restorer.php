<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Helpers;

class Restorer
{
    protected $bb;

    public function __construct($bb)
    {
        $this->bb = $bb;
    }

    /**
     * Completely recount the board statistics (useful if they become out of sync)
     */
    function rebuild_stats()
    {
        $query = $this->bb->db->simple_select(
            'forums',
            'SUM(threads) AS numthreads, 
            SUM(posts) AS numposts, 
            SUM(unapprovedthreads) AS numunapprovedthreads, 
            SUM(unapprovedposts) AS numunapprovedposts, 
            SUM(deletedthreads) AS numdeletedthreads, 
            SUM(deletedposts) AS numdeletedposts'
        );
        $stats = $this->bb->db->fetch_array($query);

        $query = $this->bb->db->simple_select('users', 'COUNT(uid) AS users');
        $stats['numusers'] = $this->bb->db->fetch_field($query, 'users');

        $this->update_stats($stats, true);
    }

    /**
     * Completely rebuild the counters for a particular forum (useful if they become out of sync)
     *
     * @param int $fid The forum ID
     */
    function rebuild_forum_counters($fid)
    {
        // Fetch the number of threads and replies in this forum (Approved only)
        $query = $this->bb->db->simple_select(
            'threads',
            'COUNT(tid) AS threads, 
            SUM(replies) AS replies, 
            SUM(unapprovedposts) AS unapprovedposts, 
            SUM(deletedposts) AS deletedposts',
            "fid='$fid' AND visible='1'"
        );
        $count = $this->bb->db->fetch_array($query);
        $count['posts'] = $count['threads'] + $count['replies'];

        // Fetch the number of threads and replies in this forum (Unapproved only)
        $query = $this->bb->db->simple_select(
            'threads',
            'COUNT(tid) AS threads, 
            SUM(replies)+SUM(unapprovedposts)+SUM(deletedposts) AS impliedunapproved',
            "fid='$fid' AND visible='0'"
        );
        $count2 = $this->bb->db->fetch_array($query);
        $count['unapprovedthreads'] = $count2['threads'];
        $count['unapprovedposts'] += $count2['impliedunapproved'] + $count2['threads'];

        // Fetch the number of threads and replies in this forum (Soft deleted only)
        $query = $this->bb->db->simple_select(
            'threads',
            'COUNT(tid) AS threads,
            SUM(replies)+SUM(unapprovedposts)+SUM(deletedposts) AS implieddeleted',
            "fid='$fid' AND visible='-1'"
        );
        $count3 = $this->bb->db->fetch_array($query);
        $count['deletedthreads'] = $count3['threads'];
        $count['deletedposts'] += $count3['implieddeleted'] + $count3['threads'];

        $this->bb->forum->update_forum_counters($fid, $count);
        $this->bb->forum->update_forum_lastpost($fid);
    }

    /**
     * Completely rebuild the counters for a particular thread (useful if they become out of sync)
     *
     * @param int $tid The thread ID
     */
    public function rebuild_thread_counters($tid)
    {
        $thread = $this->bb->thread->get_thread($tid);
        $count = [];

        $query = $this->bb->db->simple_select(
            'posts',
            'COUNT(pid) AS replies',
            "tid='{$tid}' AND pid!='{$thread['firstpost']}' AND visible='1'"
        );
        $count['replies'] = $this->bb->db->fetch_field($query, 'replies');

        // Unapproved posts
        $query = $this->bb->db->simple_select(
            'posts',
            'COUNT(pid) AS unapprovedposts',
            "tid='{$tid}' AND pid != '{$thread['firstpost']}' AND visible='0'"
        );
        $count['unapprovedposts'] = $this->bb->db->fetch_field($query, 'unapprovedposts');

        // Soft deleted posts
        $query = $this->bb->db->simple_select(
            'posts',
            'COUNT(pid) AS deletedposts',
            "tid='{$tid}' AND pid != '{$thread['firstpost']}' AND visible='-1'"
        );
        $count['deletedposts'] = $this->bb->db->fetch_field($query, 'deletedposts');

        // Attachment count
        $query = $this->bb->db->query('
			SELECT COUNT(aid) AS attachment_count
			FROM ' . TABLE_PREFIX . 'attachments a
			LEFT JOIN ' . TABLE_PREFIX . "posts p ON (a.pid=p.pid)
			WHERE p.tid='$tid' AND a.visible=1
	");
        $count['attachmentcount'] = $this->bb->db->fetch_field($query, 'attachment_count');

        $this->bb->thread->update_thread_counters($tid, $count);
        $this->bb->thread->update_thread_data($tid);
    }

    /**
     * Completely rebuild poll counters for a particular poll (useful if they become out of sync)
     *
     * @param int $pid The poll ID
     */
    function rebuild_poll_counters($pid)
    {
        $query = $this->bb->db->simple_select('polls', 'pid, numoptions', "pid='" . (int)$pid . "'");
        $poll = $this->bb->db->fetch_array($query);

        $votes = [];
        $query = $this->bb->db->simple_select(
            'pollvotes',
            'voteoption, COUNT(vid) AS vote_count',
            "pid='{$poll['pid']}'",
            ['group_by' => 'voteoption']
        );
        while ($vote = $this->bb->db->fetch_array($query)) {
            $votes[$vote['voteoption']] = $vote['vote_count'];
        }

        $voteslist = '';
        $numvotes = 0;
        for ($i = 1; $i <= $poll['numoptions']; ++$i) {
            if (trim($voteslist != '')) {
                $voteslist .= '||~|~||';
            }

            if (!isset($votes[$i]) || (int)$votes[$i] <= 0) {
                $votes[$i] = '0';
            }
            $voteslist .= $votes[$i];
            $numvotes = $numvotes + $votes[$i];
        }

        $updatedpoll = [
            'votes' => $this->bb->db->escape_string($voteslist),
            'numvotes' => (int)$numvotes
        ];
        $this->bb->db->update_query('polls', $updatedpoll, "pid='{$poll['pid']}'");
    }

    /**
     * Updates the forum statistics with specific values (or addition/subtraction of the previous value)
     *
     * @param array $changes Array of items being updated
     *  (numthreads,numposts,numusers,numunapprovedthreads,numunapprovedposts,numdeletedposts,numdeletedthreads)
     * @param boolean $force Force stats update?
     */
    function update_stats($changes = [], $force = false)
    {
        static $stats_changes;

        if (empty($stats_changes)) {
            // Update stats after all changes are done
            $this->bb->add_shutdown('update_stats', [[], true]);
        }

        if (empty($stats_changes) || $stats_changes['inserted']) {
            $stats_changes = [
                'numthreads' => '+0',
                'numposts' => '+0',
                'numusers' => '+0',
                'numunapprovedthreads' => '+0',
                'numunapprovedposts' => '+0',
                'numdeletedposts' => '+0',
                'numdeletedthreads' => '+0',
                'inserted' => false // Reset after changes are inserted into cache
            ];
            $stats = $stats_changes;
        }

        if ($force) { // Force writing to cache?
            if (!empty($changes)) {
                // Calculate before writing to cache
                $this->update_stats($changes);
            }
            $stats = $this->bb->cache->read('stats');
            $changes = $stats_changes;
        } else {
            $stats = $stats_changes;
        }

        $new_stats = [];
        $counters = ['numthreads', 'numunapprovedthreads', 'numposts', 'numunapprovedposts',
            'numusers', 'numdeletedposts', 'numdeletedthreads'];
        foreach ($counters as $counter) {
            if (array_key_exists($counter, $changes)) {
                if (substr($changes[$counter], 0, 2) == '+-') {
                    $changes[$counter] = substr($changes[$counter], 1);
                }
                // Adding or subtracting from previous value?
                if (substr($changes[$counter], 0, 1) == '+' || substr($changes[$counter], 0, 1) == '-') {
                    if ((int)$changes[$counter] != 0) {
                        $new_stats[$counter] = $stats[$counter] + $changes[$counter];
                        if (!$force && (substr($stats[$counter], 0, 1) == '+' ||
                                substr($stats[$counter], 0, 1) == '-')) {
                            // We had relative values? Then it is still relative
                            if ($new_stats[$counter] >= 0) {
                                $new_stats[$counter] = "+{$new_stats[$counter]}";
                            }
                        } // Less than 0? That's bad
                        elseif ($new_stats[$counter] < 0) {
                            $new_stats[$counter] = 0;
                        }
                    }
                } else {
                    $new_stats[$counter] = $changes[$counter];
                    // Less than 0? That's bad
                    if ($new_stats[$counter] < 0) {
                        $new_stats[$counter] = 0;
                    }
                }
            }
        }

        if (!$force) {
            $stats_changes = array_merge($stats, $new_stats); // Overwrite changed values
            return;
        }

        // Fetch latest user if the user count is changing
        if (array_key_exists('numusers', $changes)) {
            $query = $this->bb->db->simple_select(
                'users',
                'uid, username',
                '',
                ['order_by' => 'regdate', 'order_dir' => 'DESC', 'limit' => 1]
            );
            $lastmember = $this->bb->db->fetch_array($query);
            $new_stats['lastuid'] = $lastmember['uid'];
            $new_stats['lastusername'] = $lastmember['username'];
        }

        if (!empty($new_stats)) {
            if (is_array($stats)) {
                $stats = array_merge($stats, $new_stats); // Overwrite changed values
            } else {
                $stats = $new_stats;
            }
        }

        // Update stats row for today in the database
        $todays_stats = [
            'dateline' => mktime(0, 0, 0, date('m'), date('j'), date('Y')),
            'numusers' => (int)$stats['numusers'],
            'numthreads' => (int)$stats['numthreads'],
            'numposts' => (int)$stats['numposts']
        ];
        $this->bb->db->replace_query('stats', $todays_stats, 'dateline');

        $this->bb->cache->update('stats', $stats, 'dateline');
        $stats_changes['inserted'] = true;
    }

    /**
     * Rebuilds settings.php
     */
    public function rebuild_settings()
    {
        $file = DIR . 'var/cache/forum/settings.php';
        $s = \RunBB\Models\Setting::get(['value','name']);
        foreach ($s as $k => $v) {
            $this->bb->settings[$v->name] = $v->value;
        }
        $settings = '<' . "?php\n/*\n  DO NOT EDIT THIS FILE, PLEASE USE\n  ADMIN SETTINGS EDITOR\n*/\n";
        $settings .= 'return ' . var_export($this->bb->settings, true) . ';';
        $this->bb->files->put($file, $settings);
    }
}
