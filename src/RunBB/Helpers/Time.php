<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB\Helpers;

use RunCMF\Core\AbstractController;

class Time extends AbstractController
{
    /**
     * Fixes mktime for dates earlier than 1970
     *
     * @param string $format The date format to use
     * @param int $year The year of the date
     * @return string The correct date format
     */
    public function fixMktime($format, $year)
    {
        // Our little work around for the date < 1970 thing.
        // -2 idea provided by Matt Light (http://www.mephex.com)
        $format = str_replace('Y', $year, $format);
        $format = str_replace('y', my_substr($year, -2), $format);

        return $format;
    }

    /**
     * Fetch a friendly x days, y months etc date stamp from a timestamp
     *
     * @param int $stamp The timestamp
     * @param array $options Array of options
     * @return string The friendly formatted timestamp
     */
    public function niceTime($stamp, $options = [])
    {
        $ysecs = 365 * 24 * 60 * 60;
        $mosecs = 31 * 24 * 60 * 60;
        $wsecs = 7 * 24 * 60 * 60;
        $dsecs = 24 * 60 * 60;
        $hsecs = 60 * 60;
        $msecs = 60;

        if (isset($options['short'])) {
            $lang_year = $this->bb->lang->year_short;
            $lang_years = $this->bb->lang->years_short;
            $lang_month = $this->bb->lang->month_short;
            $lang_months = $this->bb->lang->months_short;
            $lang_week = $this->bb->lang->week_short;
            $lang_weeks = $this->bb->lang->weeks_short;
            $lang_day = $this->bb->lang->day_short;
            $lang_days = $this->bb->lang->days_short;
            $lang_hour = $this->bb->lang->hour_short;
            $lang_hours = $this->bb->lang->hours_short;
            $lang_minute = $this->bb->lang->minute_short;
            $lang_minutes = $this->bb->lang->minutes_short;
            $lang_second = $this->bb->lang->second_short;
            $lang_seconds = $this->bb->lang->seconds_short;
        } else {
            $lang_year = ' ' . $this->bb->lang->year;
            $lang_years = ' ' . $this->bb->lang->years;
            $lang_month = ' ' . $this->bb->lang->month;
            $lang_months = ' ' . $this->bb->lang->months;
            $lang_week = ' ' . $this->bb->lang->week;
            $lang_weeks = ' ' . $this->bb->lang->weeks;
            $lang_day = ' ' . $this->bb->lang->day;
            $lang_days = ' ' . $this->bb->lang->days;
            $lang_hour = ' ' . $this->bb->lang->hour;
            $lang_hours = ' ' . $this->bb->lang->hours;
            $lang_minute = ' ' . $this->bb->lang->minute;
            $lang_minutes = ' ' . $this->bb->lang->minutes;
            $lang_second = ' ' . $this->bb->lang->second;
            $lang_seconds = ' ' . $this->bb->lang->seconds;
        }

        $years = floor($stamp / $ysecs);
        $stamp %= $ysecs;
        $months = floor($stamp / $mosecs);
        $stamp %= $mosecs;
        $weeks = floor($stamp / $wsecs);
        $stamp %= $wsecs;
        $days = floor($stamp / $dsecs);
        $stamp %= $dsecs;
        $hours = floor($stamp / $hsecs);
        $stamp %= $hsecs;
        $minutes = floor($stamp / $msecs);
        $stamp %= $msecs;
        $seconds = $stamp;

        if ($years == 1) {
            $nicetime['years'] = '1' . $lang_year;
        } elseif ($years > 1) {
            $nicetime['years'] = $years . $lang_years;
        }

        if ($months == 1) {
            $nicetime['months'] = '1' . $lang_month;
        } elseif ($months > 1) {
            $nicetime['months'] = $months . $lang_months;
        }

        if ($weeks == 1) {
            $nicetime['weeks'] = '1' . $lang_week;
        } elseif ($weeks > 1) {
            $nicetime['weeks'] = $weeks . $lang_weeks;
        }

        if ($days == 1) {
            $nicetime['days'] = '1' . $lang_day;
        } elseif ($days > 1) {
            $nicetime['days'] = $days . $lang_days;
        }

        if (!isset($options['hours']) || $options['hours'] !== false) {
            if ($hours == 1) {
                $nicetime['hours'] = '1' . $lang_hour;
            } elseif ($hours > 1) {
                $nicetime['hours'] = $hours . $lang_hours;
            }
        }

        if (!isset($options['minutes']) || $options['minutes'] !== false) {
            if ($minutes == 1) {
                $nicetime['minutes'] = '1' . $lang_minute;
            } elseif ($minutes > 1) {
                $nicetime['minutes'] = $minutes . $lang_minutes;
            }
        }

        if (!isset($options['seconds']) || $options['seconds'] !== false) {
            if ($seconds == 1) {
                $nicetime['seconds'] = '1' . $lang_second;
            } elseif ($seconds > 1) {
                $nicetime['seconds'] = $seconds . $lang_seconds;
            }
        }

        if (!empty($nicetime)) {
            return implode(', ', $nicetime);
        }
    }

    /**
     * Turn a unix timestamp in to a "friendly" date/time format for the user.
     *
     * @param string $format A date format according to PHP's date structure.
     * @param int $stamp The unix timestamp the date should be generated for.
     * @param int|string $offset The offset in hours that should be applied to times.
     *          (timezones) Or an empty string to determine that automatically
     * @param int $ty Whether or not to use today/yesterday formatting.
     * @param boolean $adodb Whether or not to use the adodb time class for < 1970 or > 2038 times
     * @return string The formatted timestamp.
     */
    public function formatDate($format, $stamp = 0, $offset = '', $ty = 1, $adodb = false)
    {
        // If the stamp isn't set, use TIME_NOW
        if (empty($stamp)) {
            $stamp = TIME_NOW;
        }

        if (!$offset && $offset != '0') {
            if ($this->user->uid > 0 && $this->user->timezone > 0) {
                $offset = $this->user->timezone;
                $dstcorrection = $this->user->dst;
            } elseif (defined('IN_ADMINCP')) {//FIXME
//                $offset = $this->admin['timezone'];
//                $dstcorrection = $this->admin['dst'];
                $offset = $this->bb->settings['timezoneoffset'];
                $dstcorrection = $this->bb->settings['dstcorrection'];
            } else {
                $offset = $this->bb->settings['timezoneoffset'];
                $dstcorrection = $this->bb->settings['dstcorrection'];
            }

            // If DST correction is enabled, add an additional hour to the timezone.
            if ($dstcorrection == 1) {
                ++$offset;
                if (my_substr($offset, 0, 1) != "-") {
                    $offset = '+' . $offset;
                }
            }
        }

        if ($offset == '-') {
            $offset = 0;
        }

        $todaysdate = $yesterdaysdate = '';
        if ($ty && ($format == $this->bb->settings['dateformat'] || $format == 'relative')) {
            $_stamp = TIME_NOW;
            $date = gmdate($this->bb->settings['dateformat'], $stamp + ($offset * 3600));
            $todaysdate = gmdate($this->bb->settings['dateformat'], $_stamp + ($offset * 3600));
            $yesterdaysdate = gmdate($this->bb->settings['dateformat'], ($_stamp - 86400) + ($offset * 3600));
        }

        if ($format == 'relative') {
            // Relative formats both date and time
            if ($ty != 2 && abs(TIME_NOW - $stamp) < 3600) {
                $diff = TIME_NOW - $stamp;
                $relative = [
                    'prefix' => '', 'minute' => 0,
                    'plural' => $this->lang->rel_minutes_plural, 'suffix' => $this->lang->rel_ago
                ];

                if ($diff < 0) {
                    $diff = abs($diff);
                    $relative['suffix'] = '';
                    $relative['prefix'] = $this->lang->rel_in;
                }

                $relative['minute'] = floor($diff / 60);

                if ($relative['minute'] <= 1) {
                    $relative['minute'] = 1;
                    $relative['plural'] = $this->lang->rel_minutes_single;
                }

                if ($diff <= 60) {
                    // Less than a minute
                    $relative['prefix'] = $this->lang->rel_less_than;
                }

                $date = $this->lang->sprintf(
                    $this->lang->rel_time,
                    $relative['prefix'],
                    $relative['minute'],
                    $relative['plural'],
                    $relative['suffix']
                );
            } elseif ($ty != 2 && abs(TIME_NOW - $stamp) < 43200) {
                $diff = TIME_NOW - $stamp;
                $relative = [
                    'prefix' => '',
                    'hour' => 0,
                    'plural' => $this->lang->rel_hours_plural,
                    'suffix' => $this->lang->rel_ago
                ];

                if ($diff < 0) {
                    $diff = abs($diff);
                    $relative['suffix'] = '';
                    $relative['prefix'] = $this->lang->rel_in;
                }

                $relative['hour'] = floor($diff / 3600);

                if ($relative['hour'] <= 1) {
                    $relative['hour'] = 1;
                    $relative['plural'] = $this->lang->rel_hours_single;
                }

                $date = $this->lang->sprintf(
                    $this->lang->rel_time,
                    $relative['prefix'],
                    $relative['hour'],
                    $relative['plural'],
                    $relative['suffix']
                );
            } else {
                if ($ty) {
                    if ($todaysdate == $date) {
                        $date = $this->lang->today;
                    } elseif ($yesterdaysdate == $date) {
                        $date = $this->lang->yesterday;
                    }
                }

                $date .= $this->bb->settings['datetimesep'];
                $date .= gmdate($this->bb->settings['timeformat'], $stamp + ($offset * 3600));
            }
        } else {
            if ($ty && $format == $this->bb->settings['dateformat']) {
                if ($todaysdate == $date) {
                    $date = $this->lang->today;
                } elseif ($yesterdaysdate == $date) {
                    $date = $this->lang->yesterday;
                }
            } else {
                $date = gmdate($format, $stamp + ($offset * 3600));
            }
        }

        if (is_object($this->plugins)) {
            $date = $this->plugins->runHooks('my_date', $date);
        }

        return $date;
    }

    /**
     * Returns an array of supported timezones
     *
     * @return string[] Key is timezone offset, Value the language description
     */
    public function getSupportedTimezones()
    {
        $timezones = [
            '-12' => $this->bb->lang->timezone_gmt_minus_1200,
            '-11' => $this->bb->lang->timezone_gmt_minus_1100,
            '-10' => $this->bb->lang->timezone_gmt_minus_1000,
            '-9.5' => $this->bb->lang->timezone_gmt_minus_950,
            '-9' => $this->bb->lang->timezone_gmt_minus_900,
            '-8' => $this->bb->lang->timezone_gmt_minus_800,
            '-7' => $this->bb->lang->timezone_gmt_minus_700,
            '-6' => $this->bb->lang->timezone_gmt_minus_600,
            '-5' => $this->bb->lang->timezone_gmt_minus_500,
            '-4.5' => $this->bb->lang->timezone_gmt_minus_450,
            '-4' => $this->bb->lang->timezone_gmt_minus_400,
            '-3.5' => $this->bb->lang->timezone_gmt_minus_350,
            '-3' => $this->bb->lang->timezone_gmt_minus_300,
            '-2' => $this->bb->lang->timezone_gmt_minus_200,
            '-1' => $this->bb->lang->timezone_gmt_minus_100,
            '0' => $this->bb->lang->timezone_gmt,
            '1' => $this->bb->lang->timezone_gmt_100,
            '2' => $this->bb->lang->timezone_gmt_200,
            '3' => $this->bb->lang->timezone_gmt_300,
            '3.5' => $this->bb->lang->timezone_gmt_350,
            '4' => $this->bb->lang->timezone_gmt_400,
            '4.5' => $this->bb->lang->timezone_gmt_450,
            '5' => $this->bb->lang->timezone_gmt_500,
            '5.5' => $this->bb->lang->timezone_gmt_550,
            '5.75' => $this->bb->lang->timezone_gmt_575,
            '6' => $this->bb->lang->timezone_gmt_600,
            '6.5' => $this->bb->lang->timezone_gmt_650,
            '7' => $this->bb->lang->timezone_gmt_700,
            '8' => $this->bb->lang->timezone_gmt_800,
            '9' => $this->bb->lang->timezone_gmt_900,
            '9.5' => $this->bb->lang->timezone_gmt_950,
            '10' => $this->bb->lang->timezone_gmt_1000,
            '10.5' => $this->bb->lang->timezone_gmt_1050,
            '11' => $this->bb->lang->timezone_gmt_1100,
            '11.5' => $this->bb->lang->timezone_gmt_1150,
            '12' => $this->bb->lang->timezone_gmt_1200,
            '12.75' => $this->bb->lang->timezone_gmt_1275,
            '13' => $this->bb->lang->timezone_gmt_1300,
            '14' => $this->bb->lang->timezone_gmt_1400
        ];
        return $timezones;
    }

    /**
     * Build a time zone selection list.
     *
     * @param string $name The name of the select
     * @param int $selected The selected time zone (defaults to GMT)
     * @param boolean $short True to generate a "short" list with just timezone and current time
     * @return string
     */
    public function buildTimezoneSelect($name, $selected = 0, $short = false)
    {
        $timezones = $this->getSupportedTimezones();

        $timezone_option = '';
        $selected = str_replace('+', '', $selected);
        foreach ($timezones as $timezone => $label) {
            $selected_add = '';
            if ($selected == $timezone) {
                $selected_add = ' selected="selected"';
            }
            if ($short == true) {
                $label = '';
                if ($timezone != 0) {
                    $label = $timezone;
                    if ($timezone > 0) {
                        $label = "+{$label}";
                    }
                    if (strpos($timezone, '.') !== false) {
                        $label = str_replace('.', ':', $label);
                        $label = str_replace(':5', ':30', $label);
                        $label = str_replace(':75', ':45', $label);
                    } else {
                        $label .= ':00';
                    }
                }
                $time_in_zone = $this->formatDate($this->bb->settings['timeformat'], TIME_NOW, $timezone);
                $label = $this->bb->lang->sprintf($this->bb->lang->timezone_gmt_short, $label . ' ', $time_in_zone);
            }

            $timezone_option .= '<option value="' . $timezone . '"' . $selected_add . '>' . $label . '</option>';
        }

        $select = '<select class="form-control" name="'.$name.'" id="'.$name.'">'.$timezone_option.'</select>';

        return $select;
    }
}
