<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Helpers;

/*
  Example usage
  {{ getVar('subject', 'input') }}
  {{ getVar('bburl', 'set') }}
  {{ getVar('logo', 'theme') }}
  {{ getVar('forum_locked', 'lang') }}
  {{ getVar('asset_url', 'var') }}
  {{ getVar('logoutkey', 'user') }}
  {% if getVar('cansearch', 'group') == 1 %}
  {{ getVar('alt_trow', 'func') }}

  // func example
  {% if getVar('usereferrals', 'set') == 1 %}
    {% set bg = getVar('alt_trow', 'func') %}
  <tr>
      <td class="{{ bg }}"><strong>{{ getVar('members_referred', 'lang') }}</strong></td>
      <td class="{{ bg }}">{{ memprofile.referrals }}</td>
  </tr>
  {% endif %}
*/
class TwigGetVarHelper extends \Twig_Extension
{
    protected $bb;

    public function __construct(& $bb)
    {
        $this->bb = $bb;
    }

    public function getName()
    {
        return 'runbb_getvar';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getVar', [$this, 'getVar'], ['is_safe' => ['html']]),
        ];
    }

    public function getVar($var = '', $from = '')
    {
        if (empty($var) || empty($from)) {
            return 'No Var Or VarFrom';
        }
        switch ($from) {
            case 'input':
                $retval = $this->bb->input[$var];
                break;
            case 'lang':
                $retval = $this->bb->lang->$var;
                break;
            case 'set':
                $retval = $this->bb->settings[$var];
                break;
            case 'theme':
                $retval = $this->bb->theme[$var];
                break;
            case 'var':
                $retval = $this->bb->$var;
                break;
            case 'group':
                $retval = $this->bb->usergroup[$var];
                break;
            case 'user':
                $retval = $this->bb->user->$var;
                break;
            case 'config':
                $retval = $this->bb->config[$var];
                break;
            case 'func':
                $retval = $var();
                break;
            case 'debug':
                $retval = \Tracy\Debugger::timer();//$var();
                break;
        }
        return $retval;
    }
}
