<?php

/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunBB;

use RunBB\Middlewares\BBMiddleware;

class Init
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function init()
    {
        $this->registerMiddlewares();
        $this->registerViews();
        $this->registerUserRoute();
        $this->registerAdminRoute();
    }

    public function getModuleName()
    {
        return 'Forum';
    }

    public static function getModuleAccessor()
    {
        return 'forum';
    }

    public static function getAdminUrl()
    {
        return '/admin/forum';
    }

    public function registerUserMenu()
    {
        $v = $this->app->getContainer()->get('view');
        return $v->fetch('@forum/menuUser.html.twig');
    }

    public static function registerAdminMenu($menu, $url)
    {
        $forumMenu = $menu->createItem('forum', [
            'label' => 'Forum',
            'icon'  => 'comments',
            'url'   => '#'
        ]);
        $forumMenu->setAttribute('class', 'nav nav-second-level');

        $dashboardMenu = $menu->createItem('forum-dashboard', [
            'label' => 'Dashboard',
            'icon'  => 'dashboard',
            'url'   => $url.'/admin/forum'
        ]);

        $configMenu = $menu->createItem('forum-config', [
            'label' => 'Configuration',
            'icon'  => 'cog',
            'url'   => $url.'/admin/forum/config'
        ]);

        $forumsMenu = $menu->createItem('forum-forums', [
            'label' => 'Forums & Posts',
            'icon'  => 'comments-o',
            'url'   => $url.'/admin/forum/forum'
        ]);

        $usersMenu = $menu->createItem('forum-users', [
            'label' => 'Users & Groups',
            'icon'  => 'group',
            'url'   => $url.'/admin/forum/users'
        ]);

        $styleMenu = $menu->createItem('forum-style', [
            'label' => 'Templates',
            'icon'  => 'puzzle-piece',
            'url'   => $url.'/admin/forum/style'
        ]);

        $toolsMenu = $menu->createItem('forum-tools', [
            'label' => 'Maintenance',
            'icon'  => 'wrench',
            'url'   => $url.'/admin/forum/tools'
        ]);

        $forumMenu->addChildren('forum-dashboard', $dashboardMenu);
        $forumMenu->addChildren('forum-config', $configMenu);
        $forumMenu->addChildren('forum-forums', $forumsMenu);
        $forumMenu->addChildren('forum-users', $usersMenu);
        $forumMenu->addChildren('forum-style', $styleMenu);
        $forumMenu->addChildren('forum-tools', $toolsMenu);

        $menu->addItem('forum', $forumMenu);
    }

    private function registerMiddlewares()
    {
        $c = $this->app->getContainer();
        $this->app->add(new BBMiddleware($c));
    }

    private function registerViews()
    {
        // register template path & template alias
        $viewLoader = $this->app->getContainer()->get('view')->getLoader();
        $viewLoader->addPath(__DIR__ . '/Views', 'forum');
    }

    private function registerAdminRoute()
    {
        $this->app->group(self::getAdminUrl(), function () {
            $this->map(['GET', 'POST'], '', '\RunBB\Admin\Modules\Home\Index:index');
            $this->map(['GET', 'POST'], '/preferences', '\RunBB\Admin\Modules\Home\Preferences:index');
            $this->map(['GET', 'POST'], '/credits', '\RunBB\Admin\Modules\Home\Credits:index');
            // Forums
            $this->map(['GET', 'POST'], '/forum', '\RunBB\Admin\Modules\Forums\Management:index');
            $this->map(['GET', 'POST'], '/forum/announcements', '\RunBB\Admin\Modules\Forums\Announcements:index');
            $this->map(['GET', 'POST'], '/forum/mod_queue', '\RunBB\Admin\Modules\Forums\ModerationQueue:index');
            $this->map(['GET', 'POST'], '/forum/attachments', '\RunBB\Admin\Modules\Forums\Attachments:index');
            // Users
            $this->map(['GET', 'POST'], '/users', '\RunBB\Admin\Modules\Users\Users:index');
            $this->map(['GET', 'POST'], '/users/groups', '\RunBB\Admin\Modules\Users\Groups:index');
            $this->map(['GET', 'POST'], '/users/titles', '\RunBB\Admin\Modules\Users\Titles:index');
            $this->map(['GET', 'POST'], '/users/banning', '\RunBB\Admin\Modules\Users\Banning:index');
            $this->map(['GET', 'POST'], '/users/adminperm', '\RunBB\Admin\Modules\Users\AdminPermissions:index');
            $this->map(['GET', 'POST'], '/users/mass_mail', '\RunBB\Admin\Modules\Users\MassMail:index');
            $this->map(['GET', 'POST'], '/users/group_promotions', '\RunBB\Admin\Modules\Users\GroupPromotions:index');
            // Config
            $this->map(['GET', 'POST'], '/config', '\RunBB\Admin\Modules\Configs\Settings:index');
            $this->map(['GET', 'POST'], '/config/banning', '\RunBB\Admin\Modules\Configs\Banning:index');
            $this->map(['GET', 'POST'], '/config/profile_fields', '\RunBB\Admin\Modules\Configs\ProfileFields:index');
            $this->map(['GET', 'POST'], '/config/smilies', '\RunBB\Admin\Modules\Configs\Smilies:index');
            $this->map(['GET', 'POST'], '/config/badwords', '\RunBB\Admin\Modules\Configs\Badwords:index');
            $this->map(['GET', 'POST'], '/config/mycode', '\RunBB\Admin\Modules\Configs\MyCode:index');
            $this->map(['GET', 'POST'], '/config/languages', '\RunBB\Admin\Modules\Configs\Languages:index');
            $this->map(['GET', 'POST'], '/config/post_icons', '\RunBB\Admin\Modules\Configs\PostIcons:index');
            $this->map(['GET', 'POST'], '/config/help_documents', '\RunBB\Admin\Modules\Configs\HelpDocuments:index');
            $this->map(['GET', 'POST'], '/config/plugins', '\RunBB\Admin\Modules\Configs\Plugins:index');
            $this->map(['GET', 'POST'], '/config/attachment_types', '\RunBB\Admin\Modules\Configs\AttachmentTypes:index');
            $this->map(['GET', 'POST'], '/config/mod_tools', '\RunBB\Admin\Modules\Configs\ModTools:index');
            $this->map(['GET', 'POST'], '/config/spiders', '\RunBB\Admin\Modules\Configs\Spiders:index');
            $this->map(['GET', 'POST'], '/config/calendars', '\RunBB\Admin\Modules\Configs\Calendars:index');
            $this->map(['GET', 'POST'], '/config/warning', '\RunBB\Admin\Modules\Configs\Warning:index');
            $this->map(['GET', 'POST'], '/config/thread_prefixes', '\RunBB\Admin\Modules\Configs\ThreadPrefixes:index');
            $this->map(['GET', 'POST'], '/config/questions', '\RunBB\Admin\Modules\Configs\Questions:index');
            // Style
            $this->map(['GET', 'POST'], '/style', '\RunBB\Admin\Modules\Style\Themes:index');
            $this->map(['GET', 'POST'], '/style/templates', '\RunBB\Admin\Modules\Style\Templates:index');
            $this->post('/style/deltpl', '\RunBB\Admin\Modules\Style\Templates:delTpl');
            // Tools
            $this->map(['GET', 'POST'], '/tools', '\RunBB\Admin\Modules\Tools\Health:index');
            $this->map(['GET', 'POST'], '/tools/cache', '\RunBB\Admin\Modules\Tools\Cache:index');
            $this->map(['GET', 'POST'], '/tools/tasks', '\RunBB\Admin\Modules\Tools\Tasks:index');
            $this->map(['GET', 'POST'], '/tools/recount_rebuild', '\RunBB\Admin\Modules\Tools\Recount:index');
            $this->map(['GET', 'POST'], '/tools/php_info', '\RunBB\Admin\Modules\Tools\phpInfo:index');
            $this->map(['GET', 'POST'], '/tools/backupdb', '\RunBB\Admin\Modules\Tools\Backupdb:index');
            $this->map(['GET', 'POST'], '/tools/optimizedb', '\RunBB\Admin\Modules\Tools\Optimizedb:index');
            $this->map(['GET', 'POST'], '/tools/file_verification', '\RunBB\Admin\Modules\Tools\FileVerification:index');
            $this->map(['GET', 'POST'], '/tools/adminlog', '\RunBB\Admin\Modules\Tools\Adminlog:index');
            $this->map(['GET', 'POST'], '/tools/modlog', '\RunBB\Admin\Modules\Tools\Modlog:index');
            $this->map(['GET', 'POST'], '/tools/maillogs', '\RunBB\Admin\Modules\Tools\Maillog:index');
            $this->map(['GET', 'POST'], '/tools/mailerrors', '\RunBB\Admin\Modules\Tools\Mailerrors:index');
            $this->map(['GET', 'POST'], '/tools/warninglog', '\RunBB\Admin\Modules\Tools\Warninglog:index');
            $this->map(['GET', 'POST'], '/tools/spamlog', '\RunBB\Admin\Modules\Tools\Spamlog:index');
            $this->map(['GET', 'POST'], '/tools/statistics', '\RunBB\Admin\Modules\Tools\Statistics:index');
        });
    }

    private function registerUserRoute()
    {
        $this->app->group('/forum', function () {
            // BB root
            $this->get('', '\RunBB\Controllers\Index:index')->setName('forum');
            $this->get('/css', 'RunBB\Controllers\Css:index');
            $this->get('/task', 'RunBB\Controllers\Task:index');
            $this->get('/stats', 'RunBB\Controllers\Stats:index');
            $this->get('/captcha', 'RunBB\Controllers\Captcha:index');
            $this->map(['GET', 'POST'], '/xmlhttp', 'RunBB\Controllers\Xmlhttp:index');//FIXME split, rebuild
            $this->get('/online', 'RunBB\Controllers\Online:index');
            $this->map(['GET', 'POST'], '/report', 'RunBB\Controllers\Report:index');
            $this->get('/showteam', 'RunBB\Controllers\Showteam:index');
            $this->get('/forumdisplay', 'RunBB\Controllers\Forumdisplay:index');
            $this->get('/syndication', 'RunBB\Controllers\Syndication:index');
            $this->post('/ratethread', 'RunBB\Controllers\Ratethread:index');
            $this->map(['GET', 'POST'], '/showthread', 'RunBB\Controllers\Showthread:index');
            $this->get('/newreply', 'RunBB\Controllers\Newreply:index');
            $this->post('/newreply', 'RunBB\Controllers\Newreply:doNewreply');
            $this->get('/printthread', 'RunBB\Controllers\Printthread:index');
            $this->map(['GET', 'POST'], '/memberlist', 'RunBB\Controllers\Memberlist:index');
            $this->map(['GET', 'POST'], '/managegroup', 'RunBB\Controllers\Managegroup:index');
            $this->get('/portal', 'RunBB\Controllers\Portal:index');
            $this->get('/attachment', 'RunBB\Controllers\Attachment:index');
            $this->map(['GET', 'POST'], '/editpost', 'RunBB\Controllers\Editpost:index');
            $this->map(['GET', 'POST'], '/search', 'RunBB\Controllers\Search:index');
            $this->get('/announcements', 'RunBB\Controllers\Announcements:index');
            $this->get('/contact', 'RunBB\Controllers\Contact:index');
            $this->post('/contact', 'RunBB\Controllers\Contact:send');
            $this->get('/sendthread', 'RunBB\Controllers\Sendthread:index');
            $this->post('/sendthread', 'RunBB\Controllers\Sendthread:doSend');
            $this->get('/newthread', 'RunBB\Controllers\Newthread\Newthread:index');
            $this->get('/editdraft', 'RunBB\Controllers\Newthread\Newthread:index');
            $this->post('/newthread', 'RunBB\Controllers\Newthread\Newthread:doNewthread');
            $this->get('/archive', 'RunBB\Controllers\Archive\Index:index');//TODO fix url, rebuild to template

            // Member routes FIXME move to group???
            $this->get('/login', 'RunBB\Controllers\Member\Login:index');
            $this->post('/login', 'RunBB\Controllers\Member\Login:doLogin');
            $this->get('/logout', 'RunBB\Controllers\Member\Logout:index');
            $this->get('/register', 'RunBB\Controllers\Member\Register:index');
            $this->post('/register', 'RunBB\Controllers\Member\Register:doRegister');
            $this->map(['GET', 'POST'], '/activate', 'RunBB\Controllers\Member\Activate:index');
            $this->get('/lostpw', 'RunBB\Controllers\Member\Lostpw:index');
            $this->post('/lostpw', 'RunBB\Controllers\Member\Lostpw:doLostpw');
            $this->map(['GET', 'POST'], '/resetpw', 'RunBB\Controllers\Member\Resetpw:index');
            $this->get('/resendactivation', 'RunBB\Controllers\Member\ResendActivation:index');
            $this->post('/resendactivation', 'RunBB\Controllers\Member\ResendActivation:doResendActivation');
            $this->get('/emailuser', 'RunBB\Controllers\Member\Emailuser:index');
            $this->post('/emailuser', 'RunBB\Controllers\Member\Emailuser:doEmailuser');
            $this->get('/profile', 'RunBB\Controllers\Member\Profile:index');
            $this->get('/viewnotes', 'RunBB\Controllers\Member\Viewnotes:index');
            $this->get('/coppa_form', 'RunBB\Controllers\Member\CoppaForm:index');

            // UserCP
            $this->group('/usercp', function () {
                $this->get('', '\RunBB\Controllers\UserCP\Index:index');
                $this->get('/profile', '\RunBB\Controllers\UserCP\Profile:index');
                $this->post('/profile', '\RunBB\Controllers\UserCP\Profile:doProfile');
                $this->get('/changename', '\RunBB\Controllers\UserCP\ChangeName:index');
                $this->post('/changename', '\RunBB\Controllers\UserCP\ChangeName:doChangename');
                $this->get('/password', '\RunBB\Controllers\UserCP\Password:index');
                $this->post('/password', '\RunBB\Controllers\UserCP\Password:doPassword');
                $this->get('/email', '\RunBB\Controllers\UserCP\Email:index');
                $this->post('/email', '\RunBB\Controllers\UserCP\Email:doEmail');
                $this->get('/avatar', '\RunBB\Controllers\UserCP\Avatar:index');
                $this->post('/avatar', '\RunBB\Controllers\UserCP\Avatar:doAvatar');
                $this->get('/editsig', '\RunBB\Controllers\UserCP\Editsig:index');
                $this->post('/editsig', '\RunBB\Controllers\UserCP\Editsig:doEditsig');
                $this->get('/options', '\RunBB\Controllers\UserCP\Options:index');
                $this->post('/options', '\RunBB\Controllers\UserCP\Options:doOptions');
                $this->post('/notepad', '\RunBB\Controllers\UserCP\Notepad:index');

                //FIXME test acceptinvite, joingroup, leavegroup, displaygroup
                $this->get('/usergroups', '\RunBB\Controllers\UserCP\Usergroups:index');

                $this->get('/editlists', '\RunBB\Controllers\UserCP\Editlists:index');
                $this->post('/editlists', '\RunBB\Controllers\UserCP\Editlists:doEditlists');
                $this->get('/do_editlists', '\RunBB\Controllers\UserCP\Editlists:doEditlists');
                $this->get('/cancelrequest', '\RunBB\Controllers\UserCP\Cancelrequest:index');
                $this->get('/attachments', '\RunBB\Controllers\UserCP\Attachments:index');
                $this->post('/attachments', '\RunBB\Controllers\UserCP\Attachments:doAttachments');
                $this->get('/drafts', '\RunBB\Controllers\UserCP\Drafts:index');
                $this->post('drafts', '\RunBB\Controllers\UserCP\Drafts:doDrafts');
                $this->get('/subscriptions', '\RunBB\Controllers\UserCP\Subscriptions:index');
                $this->post('/subscriptions', '\RunBB\Controllers\UserCP\Subscriptions:doSubscriptions');
                $this->get('/addsubscription', '\RunBB\Controllers\UserCP\AddSubscription:index');
                $this->post('/addsubscription', '\RunBB\Controllers\UserCP\AddSubscription:doAddsubscription');
                $this->get('/removesubscription', '\RunBB\Controllers\UserCP\RemoveSubscription:removesubscription');
                $this->get('/removesubscriptions', '\RunBB\Controllers\UserCP\RemoveSubscription:removesubscriptions');
                $this->get('/forumsubscriptions', '\RunBB\Controllers\UserCP\Forumsubscriptions:index');
                $this->get('/declinerequest', '\RunBB\Controllers\UserCP\Declinerequest:index');
                $this->get('/acceptrequest', '\RunBB\Controllers\UserCP\Acceptrequest:index');
            });

            // ModCP
            $this->group('/modcp', function () {
                $this->get('', '\RunBB\Controllers\ModCP\Index:index');
                $this->post('/modnotes', '\RunBB\Controllers\ModCP\Modnotes:index');
                $this->get('/announcements', '\RunBB\Controllers\ModCP\Announcements:index');
                $this->get('/reports', '\RunBB\Controllers\ModCP\Reports:index');
                $this->post('/reports', '\RunBB\Controllers\ModCP\Reports:doReports');
                $this->get('/modqueue', '\RunBB\Controllers\ModCP\Modqueue:index');
                $this->post('/modqueue', '\RunBB\Controllers\ModCP\Modqueue:doModqueue');
                $this->map(['GET', 'POST'], '/modlogs', '\RunBB\Controllers\ModCP\Modlogs:index');// post/get = sorting
                $this->map(['GET', 'POST'], '/finduser', '\RunBB\Controllers\ModCP\Finduser:index');// post/get = sorting
                $this->get('/banning', '\RunBB\Controllers\ModCP\Banning:index');
                $this->get('/warninglogs', '\RunBB\Controllers\ModCP\Warninglogs:index');
                $this->map(['GET', 'POST'], '/ipsearch', '\RunBB\Controllers\ModCP\Ipsearch:index');// post/get = search
                $this->get('/iplookup', '\RunBB\Controllers\ModCP\Iplookup:index');
                $this->get('/banuser', '\RunBB\Controllers\ModCP\Banuser:index');
                $this->post('/banuser', '\RunBB\Controllers\ModCP\Banuser:doBanuser');
                $this->get('/liftban', '\RunBB\Controllers\ModCP\Liftban:index');
                $this->get('/allreports', '\RunBB\Controllers\ModCP\Allreports:index');
                $this->get('/edit_announcement', '\RunBB\Controllers\ModCP\EditAnnouncement:index');
                $this->post('/edit_announcement', '\RunBB\Controllers\ModCP\EditAnnouncement:doEdit');
                $this->get('/new_announcement', '\RunBB\Controllers\ModCP\NewAnnouncement:index');
                $this->post('/new_announcement', '\RunBB\Controllers\ModCP\NewAnnouncement:doNew');
                $this->get('/delete_announcement', '\RunBB\Controllers\ModCP\DeleteAnnouncement:index');
                $this->post('/delete_announcement', '\RunBB\Controllers\ModCP\DeleteAnnouncement:doDelete');
                $this->get('/editprofile', '\RunBB\Controllers\ModCP\Editprofile:index');
                $this->post('/editprofile', '\RunBB\Controllers\ModCP\Editprofile:doEdit');
            });

            // Private Messages (PM)
            $this->group('/private', function () {
                $this->map(['GET', 'POST'], '', '\RunBB\Controllers\PM\Index:index');// post from jumpto
                $this->get('/send', '\RunBB\Controllers\PM\Send:index');
                $this->post('/send', '\RunBB\Controllers\PM\Send:doSend');
                $this->get('/read', '\RunBB\Controllers\PM\Read:index');
                $this->get('/tracking', '\RunBB\Controllers\PM\Tracking:index');
                $this->post('/tracking', '\RunBB\Controllers\PM\Tracking:doTracking');
                $this->get('/folders', '\RunBB\Controllers\PM\Folders:index');
                $this->post('/folders', '\RunBB\Controllers\PM\Folders:doFolders');
                $this->get('/empty', '\RunBB\Controllers\PM\EmptyFolder:index');
                $this->post('/empty', '\RunBB\Controllers\PM\EmptyFolder:doEmpty');
                $this->get('/advanced_search', '\RunBB\Controllers\PM\AdvancedSearch:index');
                $this->post('/advanced_search', '\RunBB\Controllers\PM\AdvancedSearch:doSearch');
                $this->get('/export', '\RunBB\Controllers\PM\Export:index');
                $this->post('/export', '\RunBB\Controllers\PM\Export:doExport');
                $this->post('/dostuff', '\RunBB\Controllers\PM\DoStuff:index');
                $this->get('/results', '\RunBB\Controllers\PM\Results:index');
                $this->get('/delete', '\RunBB\Controllers\PM\Delete:index');
                $this->get('/stopalltracking', '\RunBB\Controllers\PM\StopAllTracking:index');
                $this->get('/dismiss_notice', '\RunBB\Controllers\PM\DismissNotice:index');
            });

            // Polls
            $this->group('/polls', function () {
                $this->get('/newpoll', '\RunBB\Controllers\Poll\Newpoll:index');
                $this->post('/newpoll', '\RunBB\Controllers\Poll\Newpoll:doNewpoll');
                $this->get('/editpoll', '\RunBB\Controllers\Poll\Editpoll:index');
                $this->post('/editpoll', '\RunBB\Controllers\Poll\Editpoll:doEditpoll');
                $this->get('/showresults', '\RunBB\Controllers\Poll\Showresults:index');
                $this->post('/vote', '\RunBB\Controllers\Poll\Vote:index');
                $this->get('/undovote', '\RunBB\Controllers\Poll\Undovote:index');
            });

            // Misc
            $this->group('/misc', function () {
                $this->get('/buddypopup', '\RunBB\Controllers\Misc\Buddypopup:index');
                $this->get('/smilies', '\RunBB\Controllers\Misc\Smilies:index');
                $this->map(['GET', 'POST'], '/syndication', '\RunBB\Controllers\Misc\Syndication:index');
                $this->get('/help', '\RunBB\Controllers\Misc\Help:index');
                $this->post('/help', '\RunBB\Controllers\Misc\Help:doHelpsearch');
                $this->get('/helpresults', '\RunBB\Controllers\Misc\Helpresults:index');
                $this->get('/clearcookies', '\RunBB\Controllers\Misc\Clearcookies:index');
                $this->get('/markread', '\RunBB\Controllers\Misc\Markread:index');
                $this->get('/imcenter', '\RunBB\Controllers\Misc\Imcenter:index');
                $this->get('/whoposted', '\RunBB\Controllers\Misc\WhoPosted:index');
                $this->get('/clearpass', '\RunBB\Controllers\Misc\Clearpass:index');
                $this->get('/rules', '\RunBB\Controllers\Misc\Rules:index');
                $this->post('/dstswitch', '\RunBB\Controllers\Misc\Dstswitch:index');
            });

            // Calendar
            $this->group('/calendar', function () {
                $this->map(['GET', 'POST'], '', '\RunBB\Controllers\Calendar\Index:index');
                $this->get('/dayview', '\RunBB\Controllers\Calendar\Dayview:index');
                $this->get('/addevent', '\RunBB\Controllers\Calendar\Addevent:index');
                $this->post('/addevent', '\RunBB\Controllers\Calendar\Addevent:doAddevent');
                $this->get('/event', '\RunBB\Controllers\Calendar\Event:index');
                $this->get('/editevent', '\RunBB\Controllers\Calendar\Editevent:index');
                $this->post('/editevent', '\RunBB\Controllers\Calendar\Editevent:doEditevent');
                $this->post('/deleteevent', '\RunBB\Controllers\Calendar\Deleteevent:index');
                $this->get('/weekview', '\RunBB\Controllers\Calendar\Weekview:index');
                $this->get('/move', '\RunBB\Controllers\Calendar\Move:index');
                $this->post('/move', '\RunBB\Controllers\Calendar\Move:doMove');
                $this->get('/unapprove', '\RunBB\Controllers\Calendar\Unapprove:index');
                $this->get('/approve', '\RunBB\Controllers\Calendar\Approve:index');
            });

            // Reputation
            $this->group('/reputation', function () {
                $this->get('', '\RunBB\Controllers\Reputation\Index:index');
                $this->get('/add', '\RunBB\Controllers\Reputation\Add:index');
                $this->post('/add', '\RunBB\Controllers\Reputation\Doadd:index');
                $this->post('/delete', '\RunBB\Controllers\Reputation\Delete:index');
            });

            // Warnings
            $this->group('/warnings', function () {
                $this->get('', '\RunBB\Controllers\Warnings\Index:index');
                $this->get('/warn', '\RunBB\Controllers\Warnings\Warn:index');
                $this->post('/warn', '\RunBB\Controllers\Warnings\Warn:doWarn');
                $this->get('/view', '\RunBB\Controllers\Warnings\View:index');
                $this->post('/revoke', '\RunBB\Controllers\Warnings\Revoke:index');
            });

            // Moderation
            $this->group('/moderation', function () {
                $this->map(['GET', 'POST'], '', '\RunBB\Controllers\Moderation\Index:index');
                $this->get('/getip', '\RunBB\Controllers\Moderation\GetIp:index');
                $this->get('/purgespammer', '\RunBB\Controllers\Moderation\Purgespammer:index');
                $this->post('/purgespammer', '\RunBB\Controllers\Moderation\Purgespammer:doPurgespammer');
            });
        });
    }
}
