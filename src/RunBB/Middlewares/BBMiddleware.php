<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Middlewares;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class BBMiddleware
{
    private $c;

    public function __construct(& $container)
    {
        $this->c = $container;
    }

    public function __invoke(Request $request, Response $response, $next)
    {
        $c = & $this->c;
        $c['bb'] = function ($c) {
            return new \RunBB\Core\BB($c);
        };
        $c['db'] = function ($c) {
            return new \RunBB\Core\DBMySQLi($c);
        };
        $c['templates'] = function ($c) {
            return new \RunBB\Core\Templates($c);
        };
        $c['themes'] = function ($c) {
            return new \RunBB\Core\Themes($c);
        };
        $c['cache'] = function ($c) {
            return new \RunBB\Core\DataCache($c);
        };
        $c['plugins'] = function ($c) {
            return new \RunBB\Core\Plugins($c);
        };
        $c['ban'] = function ($c) {
            return new \RunBB\Core\Ban($c);
        };
        $c['parser'] = function ($c) {
            return new \RunBB\Core\PostParser($c);
        };
        $c['forum'] = function ($c) {
            return new \RunBB\Core\Forum($c);
        };
        $c['thread'] = function ($c) {
            return new \RunBB\Core\Thread($c);
        };
        $c['lang'] = function ($c) {
            return new \RunBB\Core\Language($c);
        };
        $c['user'] = function ($c) {
            return new \RunBB\Core\User($c);
        };
        $c['group'] = function ($c) {
            return new \RunBB\Core\Group($c);
        };
        $c['session'] = function ($c) {
            return new \RunBB\Core\Session($c);
        };
        $c['bbErrorHandler'] = function ($c) {
            return new \RunBB\Core\ErrorHandler($c);
        };
        $c['upload'] = function ($c) {
            return new \RunBB\Core\Upload($c);
        };
        $c['bblogger'] = function ($c) {
            return new \RunBB\Core\Logger($c);
        };
        $c['pagination'] = function ($c) {
            return new \RunBB\Helpers\Pagination($c);
        };
        $c['page'] = function ($c) {
            return new \RunBB\Admin\Helpers\Page($c);
        };
        $c['adm'] = function ($c) {
            return new \RunBB\Admin\AdminCommon($c);
        };
        $c['task'] = function ($c) {
            return new \RunBB\Core\Task($c);
        };
        $c['indicator'] = function ($c) {
            return new \RunBB\Core\Indicators($c);
        };
        $c['post'] = function ($c) {
            return new \RunBB\Core\Post($c);
        };
        $c['editor'] = function ($c) {
            return new \RunBB\Core\Editor($c);
        };
        $c['modcp'] = function ($c) {
            return new \RunBB\Core\ModCP($c);
        };
        $c['search'] = function ($c) {
            return new \RunBB\Core\Search($c);
        };
        $c['moderation'] = function ($c) {
            return new \RunBB\Core\CustomModeration($c);
        };
        $c['pm'] = function ($c) {
            return new \RunBB\Core\PM($c);
        };
        $c['time'] = function ($c) {
            return new \RunBB\Helpers\Time($c);
        };
        $c['calendar'] = function ($c) {
            return new \RunBB\Core\Calendar($c);
        };
        $c['archive'] = function ($c) {
            return new \RunBB\Core\Archive($c);
        };
        $c['mail'] = function ($c) {
            return new \RunBB\Helpers\MailHelper($c);
        };

        return $next($request, $response);
    }
}
