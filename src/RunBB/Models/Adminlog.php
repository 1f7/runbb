<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MybbAdminlog
 */
class Adminlog extends Model
{
    protected $table = 'adminlog';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'ipaddress',
        'dateline',
        'module',
        'action',
        'data'
    ];

    protected $guarded = [];
}
