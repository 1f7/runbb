<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MybbAdminoption
 */
class Adminoption extends Model
{
    protected $table = 'adminoptions';

    protected $primaryKey = 'uid';

    public $timestamps = false;

    protected $fillable = [
        'cpstyle',
        'cplanguage',
        'codepress',
        'notes',
        'permissions',
        'defaultviews',
        'loginattempts',
        'loginlockoutexpiry',
        'authsecret',
        'recovery_codes'
    ];

    protected $guarded = [];
}
