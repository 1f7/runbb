<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MybbAdminsession
 */
class Adminsession extends Model
{
    protected $table = 'adminsessions';

    public $timestamps = false;

    protected $fillable = [
        'sid',
        'uid',
        'loginkey',
        'ip',
        'dateline',
        'lastactive',
        'data',
        'useragent',
        'authenticated'
    ];

    protected $guarded = [];
}
