<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MybbAdminview
 */
class Adminview extends Model
{
    protected $table = 'adminviews';

    protected $primaryKey = 'vid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'title',
        'type',
        'visibility',
        'fields',
        'conditions',
        'custom_profile_fields',
        'sortby',
        'sortorder',
        'perpage',
        'view_type'
    ];

    protected $guarded = [];
}
