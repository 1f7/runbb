<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MybbAnnouncement
 */
class Announcement extends Model
{
    protected $table = 'announcements';

    protected $primaryKey = 'aid';

    public $timestamps = false;

    protected $fillable = [
        'fid',
        'uid',
        'subject',
        'message',
        'startdate',
        'enddate',
        'allowhtml',
        'allowmycode',
        'allowsmilies'
    ];

    protected $guarded = [];
}
