<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attachment
 */
class Attachment extends Model
{
    protected $table = 'attachments';

    protected $primaryKey = 'aid';

    public $timestamps = false;

    protected $fillable = [
        'pid',
        'posthash',
        'uid',
        'filename',
        'filetype',
        'filesize',
        'attachname',
        'downloads',
        'dateuploaded',
        'visible',
        'thumbnail'
    ];

    protected $guarded = [];
}
