<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attachtype
 */
class Attachtype extends Model
{
    protected $table = 'attachtypes';

    protected $primaryKey = 'atid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'mimetype',
        'extension',
        'maxsize',
        'icon'
    ];

    protected $guarded = [];
}
