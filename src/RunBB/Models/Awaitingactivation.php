<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Awaitingactivation
 */
class Awaitingactivation extends Model
{
    protected $table = 'awaitingactivation';

    protected $primaryKey = 'aid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'dateline',
        'code',
        'type',
        'validated',
        'misc'
    ];

    protected $guarded = [];
}
