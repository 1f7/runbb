<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Badword
 */
class Badword extends Model
{
    protected $table = 'badwords';

    protected $primaryKey = 'bid';

    public $timestamps = false;

    protected $fillable = [
        'badword',
        'replacement'
    ];

    protected $guarded = [];
}
