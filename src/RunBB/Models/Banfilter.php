<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Banfilter
 */
class Banfilter extends Model
{
    protected $table = 'banfilters';

    protected $primaryKey = 'fid';

    public $timestamps = false;

    protected $fillable = [
        'filter',
        'type',
        'lastuse',
        'dateline'
    ];

    protected $guarded = [];
}
