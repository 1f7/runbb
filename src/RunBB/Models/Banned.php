<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Banned
 */
class Banned extends Model
{
    protected $table = 'banned';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'gid',
        'oldgroup',
        'oldadditionalgroups',
        'olddisplaygroup',
        'admin',
        'dateline',
        'bantime',
        'lifted',
        'reason'
    ];

    protected $guarded = [];
}
