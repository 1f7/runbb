<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Buddyrequest
 */
class Buddyrequest extends Model
{
    protected $table = 'buddyrequests';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'touid',
        'date'
    ];

    protected $guarded = [];
}
