<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Calendar
 */
class Calendar extends Model
{
    protected $table = 'calendars';

    protected $primaryKey = 'cid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'disporder',
        'startofweek',
        'showbirthdays',
        'eventlimit',
        'moderation',
        'allowhtml',
        'allowmycode',
        'allowimgcode',
        'allowvideocode',
        'allowsmilies'
    ];

    protected $guarded = [];
}
