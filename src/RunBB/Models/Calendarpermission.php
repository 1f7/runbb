<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Calendarpermission
 */
class Calendarpermission extends Model
{
    protected $table = 'calendarpermissions';

    public $timestamps = false;

    protected $fillable = [
        'cid',
        'gid',
        'canviewcalendar',
        'canaddevents',
        'canbypasseventmod',
        'canmoderateevents'
    ];

    protected $guarded = [];
}
