<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Captcha
 */
class Captcha extends Model
{
    protected $table = 'captcha';

    public $timestamps = false;

    protected $fillable = [
        'imagehash',
        'imagestring',
        'dateline',
        'used'
    ];

    protected $guarded = [];
}
