<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Datacache
 */
class Datacache extends Model
{
    protected $table = 'datacache';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'cache'
    ];

    protected $guarded = [];
}
