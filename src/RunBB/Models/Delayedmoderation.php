<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Delayedmoderation
 */
class Delayedmoderation extends Model
{
    protected $table = 'delayedmoderation';

    protected $primaryKey = 'did';

    public $timestamps = false;

    protected $fillable = [
        'type',
        'delaydateline',
        'uid',
        'fid',
        'tids',
        'dateline',
        'inputs'
    ];

    protected $guarded = [];
}
