<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 */
class Event extends Model
{
    protected $table = 'events';

    protected $primaryKey = 'eid';

    public $timestamps = false;

    protected $fillable = [
        'cid',
        'uid',
        'name',
        'description',
        'visible',
        'private',
        'dateline',
        'starttime',
        'endtime',
        'timezone',
        'ignoretimezone',
        'usingtime',
        'repeats'
    ];

    protected $guarded = [];
}
