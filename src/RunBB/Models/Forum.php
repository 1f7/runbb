<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class Forum
 */
class Forum extends Model
{
    protected $table = 'forums';

    protected $primaryKey = 'fid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'linkto',
        'type',
        'pid',
        'parentlist',
        'disporder',
        'active',
        'open',
        'threads',
        'posts',
        'lastpost',
        'lastposter',
        'lastposteruid',
        'lastposttid',
        'lastpostsubject',
        'allowhtml',
        'allowmycode',
        'allowsmilies',
        'allowimgcode',
        'allowvideocode',
        'allowpicons',
        'allowtratings',
        'usepostcounts',
        'usethreadcounts',
        'requireprefix',
        'password',
        'showinjump',
        'style',
        'overridestyle',
        'rulestype',
        'rulestitle',
        'rules',
        'unapprovedthreads',
        'unapprovedposts',
        'deletedthreads',
        'deletedposts',
        'defaultdatecut',
        'defaultsortby',
        'defaultsortorder'
    ];

    protected $guarded = [];

    /**
     * used in index | forumdisplay
     *
     * @return mixed Array of active forums
     */
    public static function getActiveForums()
    {
        // Build a forum cache.
//            $query = $this->db->simple_select('forums', '*', 'active!=0', array('order_by' => 'pid, disporder'));
//        $f =
        return self::where('forums.active', '!=', '0')
            ->orderBy('pid')
            ->orderBy('disporder')
            ->get()
            ->toArray();
    }

    /**
     * used in index | forumdisplay
     *
     * @param null $user_id
     * @return mixed
     */
    public static function getActiveForumsOnForumsread($user_id = null)
    {
        // Build a forum cache.
//            $query = $this->db->query("
//		SELECT f.*, fr.dateline AS lastread
//		FROM " . TABLE_PREFIX . "forums f
//		LEFT JOIN " . TABLE_PREFIX . "forumsread fr ON (fr.fid = f.fid AND fr.uid = '{$this->user->uid}')
//		WHERE f.active != 0
//		ORDER BY pid, disporder
//	");
//        $f =
        return self::where('forums.active', '!=', '0')
            ->select('forums.*', 'forumsread.dateline AS lastread')
            ->leftJoin('forumsread', function ($j) use ($user_id) {
                $j->on(function ($q) use ($user_id) {
                    $q->on('forumsread.fid', '=', 'forums.fid');
                    $q->on('forumsread.uid', '=', DB::raw($user_id));
                });
            })
            ->orderBy('pid')
            ->orderBy('disporder')
            ->get()
            ->toArray();
    }
}
