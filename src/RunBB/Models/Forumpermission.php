<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Forumpermission
 */
class Forumpermission extends Model
{
    protected $table = 'forumpermissions';

    protected $primaryKey = 'pid';

    public $timestamps = false;

    protected $fillable = [
        'fid',
        'gid',
        'canview',
        'canviewthreads',
        'canonlyviewownthreads',
        'candlattachments',
        'canpostthreads',
        'canpostreplys',
        'canonlyreplyownthreads',
        'canpostattachments',
        'canratethreads',
        'caneditposts',
        'candeleteposts',
        'candeletethreads',
        'caneditattachments',
        'modposts',
        'modthreads',
        'mod_edit_posts',
        'modattachments',
        'canpostpolls',
        'canvotepolls',
        'cansearch'
    ];

    protected $guarded = [];
}
