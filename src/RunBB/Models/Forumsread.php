<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Forumsread
 */
class Forumsread extends Model
{
    protected $table = 'forumsread';

    public $timestamps = false;

    protected $fillable = [
        'fid',
        'uid',
        'dateline'
    ];

    protected $guarded = [];
}
