<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Forumsubscription
 */
class Forumsubscription extends Model
{
    protected $table = 'forumsubscriptions';

    protected $primaryKey = 'fsid';

    public $timestamps = false;

    protected $fillable = [
        'fid',
        'uid'
    ];

    protected $guarded = [];
}
