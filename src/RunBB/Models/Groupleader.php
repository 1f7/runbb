<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Groupleader
 */
class Groupleader extends Model
{
    protected $table = 'groupleaders';

    protected $primaryKey = 'lid';

    public $timestamps = false;

    protected $fillable = [
        'gid',
        'uid',
        'canmanagemembers',
        'canmanagerequests',
        'caninvitemembers'
    ];

    protected $guarded = [];
}
