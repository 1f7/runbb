<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Helpdoc
 */
class Helpdoc extends Model
{
    protected $table = 'helpdocs';

    protected $primaryKey = 'hid';

    public $timestamps = false;

    protected $fillable = [
        'sid',
        'name',
        'description',
        'document',
        'usetranslation',
        'enabled',
        'disporder'
    ];

    protected $guarded = [];
}
