<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Helpsection
 */
class Helpsection extends Model
{
    protected $table = 'helpsections';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'usetranslation',
        'enabled',
        'disporder'
    ];

    protected $guarded = [];
}
