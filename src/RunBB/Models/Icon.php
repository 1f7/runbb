<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Icon
 */
class Icon extends Model
{
    protected $table = 'icons';

    protected $primaryKey = 'iid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'path'
    ];

    protected $guarded = [];
}
