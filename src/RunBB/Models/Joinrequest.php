<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Joinrequest
 */
class Joinrequest extends Model
{
    protected $table = 'joinrequests';

    protected $primaryKey = 'rid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'gid',
        'reason',
        'dateline',
        'invite'
    ];

    protected $guarded = [];
}
