<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

class LangRussian extends Model
{
    protected $table = 'lang_russian';

    protected $fillable = [
//        'id',
        'isadmin',
        'section',
        'var',
        'trans'
    ];

    protected $guarded = [];

    public static function countAll()
    {
        $ret = [];
        $ret['total'] = self::count();
        $ret['info'] = self::where('section', '=', 'langinfo')->count();
        $ret['user'] = self::where('isadmin', '=', 0)->where('section', '!=', 'langinfo')->count();
        $ret['admin'] = self::where('isadmin', '=', 1)->where('section', '!=', 'langinfo')->count();

        return $ret;
    }
}
