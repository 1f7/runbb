<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Mailerror
 */
class Mailerror extends Model
{
    protected $table = 'mailerrors';

    protected $primaryKey = 'eid';

    public $timestamps = false;

    protected $fillable = [
        'subject',
        'message',
        'toaddress',
        'fromaddress',
        'dateline',
        'error',
        'smtperror',
        'smtpcode'
    ];

    protected $guarded = [];
}
