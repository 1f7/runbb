<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Maillog
 */
class Maillog extends Model
{
    protected $table = 'maillogs';

    protected $primaryKey = 'mid';

    public $timestamps = false;

    protected $fillable = [
        'subject',
        'message',
        'dateline',
        'fromuid',
        'fromemail',
        'touid',
        'toemail',
        'tid',
        'ipaddress',
        'type'
    ];

    protected $guarded = [];
}
