<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Mailqueue
 */
class Mailqueue extends Model
{
    protected $table = 'mailqueue';

    protected $primaryKey = 'mid';

    public $timestamps = false;

    protected $fillable = [
        'mailto',
        'mailfrom',
        'subject',
        'message',
        'headers'
    ];

    protected $guarded = [];
}
