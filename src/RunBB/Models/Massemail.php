<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Massemail
 */
class Massemail extends Model
{
    protected $table = 'massemails';

    protected $primaryKey = 'mid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'subject',
        'message',
        'htmlmessage',
        'type',
        'format',
        'dateline',
        'senddate',
        'status',
        'sentcount',
        'totalcount',
        'conditions',
        'perpage'
    ];

    protected $guarded = [];
}
