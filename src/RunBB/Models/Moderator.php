<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Moderator
 */
class Moderator extends Model
{
    protected $table = 'moderators';

    protected $primaryKey = 'mid';

    public $timestamps = false;

    protected $fillable = [
        'fid',
        'isgroup',
        'caneditposts',
        'cansoftdeleteposts',
        'canrestoreposts',
        'candeleteposts',
        'cansoftdeletethreads',
        'canrestorethreads',
        'candeletethreads',
        'canviewips',
        'canviewunapprove',
        'canviewdeleted',
        'canopenclosethreads',
        'canstickunstickthreads',
        'canapproveunapprovethreads',
        'canapproveunapproveposts',
        'canapproveunapproveattachs',
        'canmanagethreads',
        'canmanagepolls',
        'canpostclosedthreads',
        'canmovetononmodforum',
        'canusecustomtools',
        'canmanageannouncements',
        'canmanagereportedposts',
        'canviewmodlog'
    ];

    protected $guarded = [];
}
