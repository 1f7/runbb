<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Moderatorlog
 */
class Moderatorlog extends Model
{
    protected $table = 'moderatorlog';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'dateline',
        'fid',
        'tid',
        'pid',
        'action',
        'data',
        'ipaddress'
    ];

    protected $guarded = [];
}
