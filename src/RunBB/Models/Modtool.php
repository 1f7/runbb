<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Modtool
 */
class Modtool extends Model
{
    protected $table = 'modtools';

    protected $primaryKey = 'tid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'forums',
        'groups',
        'type',
        'postoptions',
        'threadoptions'
    ];

    protected $guarded = [];
}
