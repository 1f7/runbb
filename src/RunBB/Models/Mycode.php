<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Mycode
 */
class Mycode extends Model
{
    protected $table = 'mycode';

    protected $primaryKey = 'cid';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'regex',
        'replacement',
        'active',
        'parseorder'
    ];

    protected $guarded = [];
}
