<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Poll
 */
class Poll extends Model
{
    protected $table = 'polls';

    protected $primaryKey = 'pid';

    public $timestamps = false;

    protected $fillable = [
        'tid',
        'question',
        'dateline',
        'options',
        'votes',
        'numoptions',
        'numvotes',
        'timeout',
        'closed',
        'multiple',
        'public',
        'maxoptions'
    ];

    protected $guarded = [];
}
