<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pollvote
 */
class Pollvote extends Model
{
    protected $table = 'pollvotes';

    protected $primaryKey = 'vid';

    public $timestamps = false;

    protected $fillable = [
        'pid',
        'uid',
        'voteoption',
        'dateline'
    ];

    protected $guarded = [];
}
