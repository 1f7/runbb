<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 */
class Post extends Model
{
    protected $table = 'posts';

    protected $primaryKey = 'pid';

    public $timestamps = false;

    protected $fillable = [
        'tid',
        'replyto',
        'fid',
        'subject',
        'icon',
        'uid',
        'username',
        'dateline',
        'message',
        'ipaddress',
        'includesig',
        'smilieoff',
        'edituid',
        'edittime',
        'editreason',
        'visible'
    ];

    protected $guarded = [];

    public static function draftCount($uid = null)
    {
        if ($uid === null) {
            return 0;
        } else {
            return self::where([
                ['visible', '=', '-2'],
                ['uid', '=', $uid]
            ])->count();
        }
    }

    public static function getAllUserDarfts($uid = null)
    {
        if ($uid === null) {
            return [];
        } else {
            return self::where([
                ['posts.uid', '=', $uid],
                ['posts.visible', '=', '-2']
            ])
                ->leftJoin('threads as t', 't.tid', '=', 'posts.tid')
                ->leftJoin('forums as f', 'f.fid', '=', 't.fid')
                ->orderBy('posts.dateline', 'desc')
                ->get(['posts.subject', 'posts.pid', 't.tid',
                    't.subject AS threadsubject', 't.fid',
                    'f.name AS forumname', 'posts.dateline',
                    't.visible AS threadvisible',
                    'posts.visible AS postvisible'])
                ->toArray();
        }
    }
}
