<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Privatemessage
 */
class Privatemessage extends Model
{
    protected $table = 'privatemessages';

    protected $primaryKey = 'pmid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'toid',
        'fromid',
        'recipients',
        'folder',
        'subject',
        'icon',
        'message',
        'dateline',
        'deletetime',
        'status',
        'statustime',
        'includesig',
        'smilieoff',
        'receipt',
        'readtime',
        'ipaddress'
    ];

    protected $guarded = [];
}
