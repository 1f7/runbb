<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Profilefield
 */
class Profilefield extends Model
{
    protected $table = 'profilefields';

    protected $primaryKey = 'fid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'disporder',
        'type',
        'regex',
        'length',
        'maxlength',
        'required',
        'registration',
        'profile',
        'postbit',
        'viewableby',
        'editableby',
        'postnum',
        'allowhtml',
        'allowmycode',
        'allowsmilies',
        'allowimgcode',
        'allowvideocode'
    ];

    protected $guarded = [];
}
