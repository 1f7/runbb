<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Promotion
 */
class Promotion extends Model
{
    protected $table = 'promotions';

    protected $primaryKey = 'pid';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'enabled',
        'logging',
        'posts',
        'posttype',
        'threads',
        'threadtype',
        'registered',
        'registeredtype',
        'online',
        'onlinetype',
        'reputations',
        'reputationtype',
        'referrals',
        'referralstype',
        'warnings',
        'warningstype',
        'requirements',
        'originalusergroup',
        'newusergroup',
        'usergrouptype'
    ];

    protected $guarded = [];
}
