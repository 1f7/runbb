<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Promotionlog
 */
class Promotionlog extends Model
{
    protected $table = 'promotionlogs';

    protected $primaryKey = 'plid';

    public $timestamps = false;

    protected $fillable = [
        'pid',
        'uid',
        'oldusergroup',
        'newusergroup',
        'dateline',
        'type'
    ];

    protected $guarded = [];
}
