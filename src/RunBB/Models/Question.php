<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 */
class Question extends Model
{
    protected $table = 'questions';

    protected $primaryKey = 'qid';

    public $timestamps = false;

    protected $fillable = [
        'question',
        'answer',
        'shown',
        'correct',
        'incorrect',
        'active'
    ];

    protected $guarded = [];
}
