<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Questionsession
 */
class Questionsession extends Model
{
    protected $table = 'questionsessions';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'qid',
        'dateline'
    ];

    protected $guarded = [];
}
