<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Reportedcontent
 */
class Reportedcontent extends Model
{
    protected $table = 'reportedcontent';

    protected $primaryKey = 'rid';

    public $timestamps = false;

    protected $fillable = [
        'id2',
        'id3',
        'uid',
        'reportstatus',
        'reason',
        'type',
        'reports',
        'reporters',
        'dateline',
        'lastreport'
    ];

    protected $guarded = [];
}
