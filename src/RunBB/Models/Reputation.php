<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Reputation
 */
class Reputation extends Model
{
    protected $table = 'reputation';

    protected $primaryKey = 'rid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'adduid',
        'pid',
        'reputation',
        'dateline',
        'comments'
    ];

    protected $guarded = [];
}
