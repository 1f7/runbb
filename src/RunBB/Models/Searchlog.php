<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Searchlog
 */
class Searchlog extends Model
{
    protected $table = 'searchlog';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'sid',
        'uid',
        'dateline',
        'ipaddress',
        'threads',
        'posts',
        'resulttype',
        'querycache',
        'keywords'
    ];

    protected $guarded = [];
}
