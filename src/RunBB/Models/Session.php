<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class Session
 */
class Session extends Model
{
    protected $table = 'sessions';
    protected $primaryKey = 'sid';

    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'sid',
        'uid',
        'ip',
        'time',
        'location',
        'useragent',
        'anonymous',
        'nopermission',
        'location1',
        'location2'
    ];

    protected $guarded = [];
}
