<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 */
class Setting extends Model
{
    protected $table = 'settings';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'title',
        'description',
        'optionscode',
        'value',
        'disporder',
        'gid',
        'isdefault'
    ];

    protected $guarded = [];
}
