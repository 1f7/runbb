<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Settinggroup
 */
class Settinggroup extends Model
{
    protected $table = 'settinggroups';

    protected $primaryKey = 'gid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'title',
        'description',
        'disporder',
        'isdefault'
    ];

    protected $guarded = [];
}
