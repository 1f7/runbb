<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Smily
 */
class Smily extends Model
{
    protected $table = 'smilies';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'find',
        'image',
        'disporder',
        'showclickable'
    ];

    protected $guarded = [];
}
