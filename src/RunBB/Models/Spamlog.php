<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Spamlog
 */
class Spamlog extends Model
{
    protected $table = 'spamlog';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'username',
        'email',
        'ipaddress',
        'dateline',
        'data'
    ];

    protected $guarded = [];
}
