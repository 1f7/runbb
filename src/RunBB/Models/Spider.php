<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Spider
 */
class Spider extends Model
{
    protected $table = 'spiders';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'theme',
        'language',
        'usergroup',
        'useragent',
        'lastvisit'
    ];

    protected $guarded = [];
}
