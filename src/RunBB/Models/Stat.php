<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Stat
 */
class Stat extends Model
{
    protected $table = 'stats';

    protected $primaryKey = 'dateline';

    public $timestamps = false;

    protected $fillable = [
        'numusers',
        'numthreads',
        'numposts'
    ];

    protected $guarded = [];
}
