<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 */
class Task extends Model
{
    protected $table = 'tasks';

    protected $primaryKey = 'tid';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'description',
        'file',
        'minute',
        'hour',
        'day',
        'month',
        'weekday',
        'nextrun',
        'lastrun',
        'enabled',
        'logging',
        'locked'
    ];

    protected $guarded = [];
}
