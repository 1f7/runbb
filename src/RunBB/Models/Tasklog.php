<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tasklog
 */
class Tasklog extends Model
{
    protected $table = 'tasklog';

    protected $primaryKey = 'lid';

    public $timestamps = false;

    protected $fillable = [
        'tid',
        'dateline',
        'data'
    ];

    protected $guarded = [];
}
