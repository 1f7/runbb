<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Template
 */
class Template extends Model
{
    protected $table = 'templates';

    protected $primaryKey = 'tid';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'template',
        'sid',
        'version',
        'status',
        'dateline'
    ];

    protected $guarded = [];
}
