<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Templategroup
 */
class Templategroup extends Model
{
    protected $table = 'templategroups';

    protected $primaryKey = 'gid';

    public $timestamps = false;

    protected $fillable = [
        'prefix',
        'title',
        'isdefault'
    ];

    protected $guarded = [];
}
