<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Templateset
 */
class Templateset extends Model
{
    protected $table = 'templatesets';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'title'
    ];

    protected $guarded = [];
}
