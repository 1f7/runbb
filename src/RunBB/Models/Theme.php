<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Theme
 */
class Theme extends Model
{
    protected $table = 'themes';

    protected $primaryKey = 'tid';

//    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'tid',
        'name',
        'pid',
        'def',
        'properties',
        'stylesheets',
        'allowedgroups'
    ];

    protected $guarded = [];
}
