<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Themestylesheet
 */
class Themestylesheet extends Model
{
    protected $table = 'themestylesheets';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'tid',
        'attachedto',
        'stylesheet',
        'cachefile',
        'lastmodified'
    ];

    protected $guarded = [];
}
