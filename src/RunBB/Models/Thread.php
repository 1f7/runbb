<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Thread
 */
class Thread extends Model
{
    protected $table = 'threads';

    protected $primaryKey = 'tid';

    public $timestamps = false;

    protected $fillable = [
        'fid',
        'subject',
        'prefix',
        'icon',
        'poll',
        'uid',
        'username',
        'dateline',
        'firstpost',
        'lastpost',
        'lastposter',
        'lastposteruid',
        'views',
        'replies',
        'closed',
        'sticky',
        'numratings',
        'totalratings',
        'notes',
        'visible',
        'unapprovedposts',
        'deletedposts',
        'attachmentcount',
        'deletetime'
    ];

    protected $guarded = [];
}
