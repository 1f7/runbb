<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Threadprefix
 */
class Threadprefix extends Model
{
    protected $table = 'threadprefixes';

    protected $primaryKey = 'pid';

    public $timestamps = false;

    protected $fillable = [
        'prefix',
        'displaystyle',
        'forums',
        'groups'
    ];

    protected $guarded = [];
}
