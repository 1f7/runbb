<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Threadrating
 */
class Threadrating extends Model
{
    protected $table = 'threadratings';

    protected $primaryKey = 'rid';

    public $timestamps = false;

    protected $fillable = [
        'tid',
        'uid',
        'rating',
        'ipaddress'
    ];

    protected $guarded = [];
}
