<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Threadsread
 */
class Threadsread extends Model
{
    protected $table = 'threadsread';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'tid',
        'uid',
        'dateline'
    ];

    protected $guarded = [];
}
