<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Threadsubscription
 */
class Threadsubscription extends Model
{
    protected $table = 'threadsubscriptions';

    protected $primaryKey = 'sid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'tid',
        'notification',
        'dateline'
    ];

    protected $guarded = [];
}
