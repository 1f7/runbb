<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Threadview
 */
class Threadview extends Model
{
    protected $table = 'threadviews';

    public $timestamps = false;

    protected $fillable = [
        'tid'
    ];

    protected $guarded = [];
}
