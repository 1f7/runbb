<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'uid';
    public $timestamps = false;

    protected $fillable = [
    'username',
    'password',
    'salt',
    'loginkey',
    'email',
    'postnum',
    'threadnum',
    'avatar',
    'avatardimensions',
    'avatartype',
    'usergroup',
    'additionalgroups',
    'displaygroup',
    'usertitle',
    'regdate',
    'lastactive',
    'lastvisit',
    'lastpost',
    'website',
    'icq',
    'aim',
    'yahoo',
    'skype',
    'google',
    'birthday',
    'birthdayprivacy',
    'signature',
    'allownotices',
    'hideemail',
    'subscriptionmethod',
    'invisible',
    'receivepms',
    'receivefrombuddy',
    'pmnotice',
    'pmnotify',
    'buddyrequestspm',
    'buddyrequestsauto',
    'threadmode',
    'showimages',
    'showvideos',
    'showsigs',
    'showavatars',
    'showquickreply',
    'showredirect',
    'ppp',
    'tpp',
    'daysprune',
    'dateformat',
    'timeformat',
    'timezone',
    'dst',
    'dstcorrection',
    'buddylist',
    'ignorelist',
    'style',
    'away',
    'awaydate',
    'returndate',
    'awayreason',
    'pmfolders',
    'notepad',
    'referrer',
    'referrals',
    'reputation',
    'regip',
    'lastip',
    'language',
    'timeonline',
    'showcodebuttons',
    'totalpms',
    'unreadpms',
    'warningpoints',
    'moderateposts',
    'moderationtime',
    'suspendposting',
    'suspensiontime',
    'suspendsignature',
    'suspendsigtime',
    'coppauser',
    'classicpostbit',
    'loginattempts',
    'usernotes',
    'sourceeditor'
    ];

    protected $guarded = [];
}
