<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Userfield
 */
class Userfield extends Model
{
    protected $table = 'userfields';

    protected $primaryKey = 'ufid';

    public $timestamps = false;

    protected $fillable = [
        'fid1',
        'fid2',
        'fid3'
    ];

    protected $guarded = [];
}
