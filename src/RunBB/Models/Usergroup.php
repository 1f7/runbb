<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Usergroup
 */
class Usergroup extends Model
{
    protected $table = 'usergroups';

    protected $primaryKey = 'gid';

    public $timestamps = false;

    protected $fillable = [
        'type',
        'title',
        'description',
        'namestyle',
        'usertitle',
        'stars',
        'starimage',
        'image',
        'disporder',
        'isbannedgroup',
        'canview',
        'canviewthreads',
        'canviewprofiles',
        'candlattachments',
        'canviewboardclosed',
        'canpostthreads',
        'canpostreplys',
        'canpostattachments',
        'canratethreads',
        'modposts',
        'modthreads',
        'mod_edit_posts',
        'modattachments',
        'caneditposts',
        'candeleteposts',
        'candeletethreads',
        'caneditattachments',
        'canpostpolls',
        'canvotepolls',
        'canundovotes',
        'canusepms',
        'cansendpms',
        'cantrackpms',
        'candenypmreceipts',
        'pmquota',
        'maxpmrecipients',
        'cansendemail',
        'cansendemailoverride',
        'maxemails',
        'emailfloodtime',
        'canviewmemberlist',
        'canviewcalendar',
        'canaddevents',
        'canbypasseventmod',
        'canmoderateevents',
        'canviewonline',
        'canviewwolinvis',
        'canviewonlineips',
        'cancp',
        'issupermod',
        'cansearch',
        'canusercp',
        'canuploadavatars',
        'canratemembers',
        'canchangename',
        'canbereported',
        'canchangewebsite',
        'showforumteam',
        'usereputationsystem',
        'cangivereputations',
        'candeletereputations',
        'reputationpower',
        'maxreputationsday',
        'maxreputationsperuser',
        'maxreputationsperthread',
        'candisplaygroup',
        'attachquota',
        'cancustomtitle',
        'canwarnusers',
        'canreceivewarnings',
        'maxwarningsday',
        'canmodcp',
        'showinbirthdaylist',
        'canoverridepm',
        'canusesig',
        'canusesigxposts',
        'signofollow',
        'edittimelimit',
        'maxposts',
        'showmemberlist',
        'canmanageannounce',
        'canmanagemodqueue',
        'canmanagereportedcontent',
        'canviewmodlogs',
        'caneditprofiles',
        'canbanusers',
        'canviewwarnlogs',
        'canuseipsearch'
    ];

    protected $guarded = [];
}
