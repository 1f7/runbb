<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Usertitle
 */
class Usertitle extends Model
{
    protected $table = 'usertitles';

    protected $primaryKey = 'utid';

    public $timestamps = false;

    protected $fillable = [
        'posts',
        'title',
        'stars',
        'starimage'
    ];

    protected $guarded = [];
}
