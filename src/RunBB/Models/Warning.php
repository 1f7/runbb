<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Warning
 */
class Warning extends Model
{
    protected $table = 'warnings';

    protected $primaryKey = 'wid';

    public $timestamps = false;

    protected $fillable = [
        'uid',
        'tid',
        'pid',
        'title',
        'points',
        'dateline',
        'issuedby',
        'expires',
        'expired',
        'daterevoked',
        'revokedby',
        'revokereason',
        'notes'
    ];

    protected $guarded = [];
}
