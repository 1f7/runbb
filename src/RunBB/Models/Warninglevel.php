<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Warninglevel
 */
class Warninglevel extends Model
{
    protected $table = 'warninglevels';

    protected $primaryKey = 'lid';

    public $timestamps = false;

    protected $fillable = [
        'percentage',
        'action'
    ];

    protected $guarded = [];
}
