<?php

namespace RunBB\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Warningtype
 */
class Warningtype extends Model
{
    protected $table = 'warningtypes';

    protected $primaryKey = 'tid';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'points',
        'expirationtime'
    ];

    protected $guarded = [];
}
