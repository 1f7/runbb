<?php
/**
 * BB 1.8
 * Copyright 2014 BB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */

/*
 * Arbitrary limits for _safe_unserialize()
 */
define('MAX_SERIALIZED_INPUT_LENGTH', 10240);
define('MAX_SERIALIZED_ARRAY_LENGTH', 256);
define('MAX_SERIALIZED_ARRAY_DEPTH', 5);

/**
 * Find and replace a string in a particular template through every template set.
 *
 * @param string $title The name of the template
 * @param string $find The regular expression to match in the template
 * @param string $replace The replacement string
 * @param int $autocreate Set to 1 to automatically create templates which do not exist for sets with SID > 0 (based off master) - defaults to 1
 * @param mixed $sid Template SID to modify, false for every SID > 0 and SID = -1
 * @param int $limit The maximum possible replacements for the regular expression
 * @return boolean true if updated one or more templates, false if not.
 */

function find_replace_templatesets($title, $find, $replace, $autocreate = 1, $sid = false, $limit = -1)
{
    $return = false;
    $template_sets = [-2, -1];

    // Select all templates with that title (including global) if not working on a specific template set
    $sqlwhere = '>0 OR sid=-1';
    $sqlwhere2 = '>0';

    // Otherwise select just templates from that specific set
    if ($sid !== false) {
        $sid = (int)$sid;
        $sqlwhere2 = $sqlwhere = "=$sid";
    }

    // Select all other modified templates with that title
    $query = $db->simple_select("templates", "tid, sid, template", "title = '".$db->escape_string($title)."' AND (sid{$sqlwhere})");
    while ($template = $db->fetch_array($query)) {
        // Keep track of which templates sets have a modified version of this template already
        $template_sets[] = $template['sid'];

        // Update the template if there is a replacement term or a change
        $new_template = preg_replace($find, $replace, $template['template'], $limit);
        if ($new_template == $template['template']) {
            continue;
        }

        // The template is a custom template.  Replace as normal.
        $updated_template = [
            "template" => $db->escape_string($new_template)
        ];
        $db->update_query("templates", $updated_template, "tid='{$template['tid']}'");

        $return = true;
    }

    // Add any new templates if we need to and are allowed to
    if ($autocreate != 0) {
        // Select our master template with that title
        $query = $db->simple_select("templates", "title, template", "title='".$db->escape_string($title)."' AND sid='-2'", ['limit' => 1]);
        $master_template = $db->fetch_array($query);
        $master_template['new_template'] = preg_replace($find, $replace, $master_template['template'], $limit);

        if ($master_template['new_template'] != $master_template['template']) {
            // Update the rest of our template sets that are currently inheriting this template from our master set
            $query = $db->simple_select("templatesets", "sid", "sid NOT IN (".implode(',', $template_sets).") AND (sid{$sqlwhere2})");
            while ($template = $db->fetch_array($query)) {
                $insert_template = [
                    "title" => $db->escape_string($master_template['title']),
                    "template" => $db->escape_string($master_template['new_template']),
                    "sid" => $template['sid'],
                    "version" => $mybb->version_code,
                    "status" => '',
                    "dateline" => TIME_NOW
                ];
                $db->insert_query("templates", $insert_template);

                $return = true;
            }
        }
    }

    return $return;
}

/**
 * Credits go to https://github.com/piwik
 * Safe unserialize() replacement
 * - accepts a strict subset of PHP's native my_serialized representation
 * - does not unserialize objects
 *
 * @param string $str
 * @return mixed
 * @throw Exception if $str is malformed or contains unsupported types (e.g., resources, objects)
 */
function _safe_unserialize($str)
{
    if (strlen($str) > MAX_SERIALIZED_INPUT_LENGTH) {
        // input exceeds MAX_SERIALIZED_INPUT_LENGTH
        return false;
    }

    if (empty($str) || !is_string($str)) {
        return false;
    }

    $stack = $expected = [];

    /*
       * states:
       *   0 - initial state, expecting a single value or array
       *   1 - terminal state
       *   2 - in array, expecting end of array or a key
       *   3 - in array, expecting value or another array
       */
    $state = 0;
    while ($state != 1) {
        $type = isset($str[0]) ? $str[0] : '';

        if ($type == '}') {
            $str = substr($str, 1);
        } elseif ($type == 'N' && $str[1] == ';') {
            $value = null;
            $str = substr($str, 2);
        } elseif ($type == 'b' && preg_match('/^b:([01]);/', $str, $matches)) {
            $value = $matches[1] == '1' ? true : false;
            $str = substr($str, 4);
        } elseif ($type == 'i' && preg_match('/^i:(-?[0-9]+);(.*)/s', $str, $matches)) {
            $value = (int)$matches[1];
            $str = $matches[2];
        } elseif ($type == 'd' && preg_match('/^d:(-?[0-9]+\.?[0-9]*(E[+-][0-9]+)?);(.*)/s', $str, $matches)) {
            $value = (float)$matches[1];
            $str = $matches[3];
        } elseif ($type == 's' && preg_match('/^s:([0-9]+):"(.*)/s', $str, $matches) && substr($matches[2], (int)$matches[1], 2) == '";') {
            $value = substr($matches[2], 0, (int)$matches[1]);
            $str = substr($matches[2], (int)$matches[1] + 2);
        } elseif ($type == 'a' && preg_match('/^a:([0-9]+):{(.*)/s', $str, $matches) && $matches[1] < MAX_SERIALIZED_ARRAY_LENGTH) {
            $expectedLength = (int)$matches[1];
            $str = $matches[2];
        } else {
            // object or unknown/malformed type
            return false;
        }

        switch ($state) {
            case 3: // in array, expecting value or another array
                if ($type == 'a') {
                    if (count($stack) >= MAX_SERIALIZED_ARRAY_DEPTH) {
                        // array nesting exceeds MAX_SERIALIZED_ARRAY_DEPTH
                        return false;
                    }

                    $stack[] = &$list;
                    $list[$key] = [];
                    $list = &$list[$key];
                    $expected[] = $expectedLength;
                    $state = 2;
                    break;
                }
                if ($type != '}') {
                    $list[$key] = $value;
                    $state = 2;
                    break;
                }

                // missing array value
                return false;

            case 2: // in array, expecting end of array or a key
                if ($type == '}') {
                    if (count($list) < end($expected)) {
                        // array size less than expected
                        return false;
                    }

                    unset($list);
                    $list = &$stack[count($stack) - 1];
                    array_pop($stack);

                    // go to terminal state if we're at the end of the root array
                    array_pop($expected);
                    if (count($expected) == 0) {
                        $state = 1;
                    }
                    break;
                }
                if ($type == 'i' || $type == 's') {
                    if (count($list) >= MAX_SERIALIZED_ARRAY_LENGTH) {
                        // array size exceeds MAX_SERIALIZED_ARRAY_LENGTH
                        return false;
                    }
                    if (count($list) >= end($expected)) {
                        // array size exceeds expected length
                        return false;
                    }

                    $key = $value;
                    $state = 3;
                    break;
                }

                // illegal array index type
                return false;

            case 0: // expecting array or value
                if ($type == 'a') {
                    if (count($stack) >= MAX_SERIALIZED_ARRAY_DEPTH) {
                        // array nesting exceeds MAX_SERIALIZED_ARRAY_DEPTH
                        return false;
                    }

                    $data = [];
                    $list = &$data;
                    $expected[] = $expectedLength;
                    $state = 2;
                    break;
                }
                if ($type != '}') {
                    $data = $value;
                    $state = 1;
                    break;
                }

                // not in array
                return false;
        }
    }

    if (!empty($str)) {
        // trailing data in input
        return false;
    }
    return $data;
}

/**
 * Credits go to https://github.com/piwik
 * Wrapper for _safe_unserialize() that handles exceptions and multibyte encoding issue
 *
 * @param string $str
 * @return mixed
 */
function my_unserialize($str)
{
    // Ensure we use the byte count for strings even when strlen() is overloaded by mb_strlen()
    if (function_exists('mb_internal_encoding') && (((int)ini_get('mbstring.func_overload')) & 2)) {
        $mbIntEnc = mb_internal_encoding();
        mb_internal_encoding('ASCII');
    }

    $out = _safe_unserialize($str);

    if (isset($mbIntEnc)) {
        mb_internal_encoding($mbIntEnc);
    }

    return $out;
}

/**
 * Credits go to https://github.com/piwik
 * Safe serialize() replacement
 * - output a strict subset of PHP's native serialized representation
 * - does not my_serialize objects
 *
 * @param mixed $value
 * @return string
 * @throw Exception if $value is malformed or contains unsupported types (e.g., resources, objects)
 */
function _safe_serialize($value)
{
    if (is_null($value)) {
        return 'N;';
    }

    if (is_bool($value)) {
        return 'b:' . (int)$value . ';';
    }

    if (is_int($value)) {
        return 'i:' . $value . ';';
    }

    if (is_float($value)) {
        return 'd:' . str_replace(',', '.', $value) . ';';
    }

    if (is_string($value)) {
        return 's:' . strlen($value) . ':"' . $value . '";';
    }

    if (is_array($value)) {
        $out = '';
        foreach ($value as $k => $v) {
            $out .= _safe_serialize($k) . _safe_serialize($v);
        }

        return 'a:' . count($value) . ':{' . $out . '}';
    }

    // safe_serialize cannot my_serialize resources or objects
    return false;
}

/**
 * Credits go to https://github.com/piwik
 * Wrapper for _safe_serialize() that handles exceptions and multibyte encoding issue
 *
 * @param mixed $value
 * @return string
 */
function my_serialize($value)
{
    // ensure we use the byte count for strings even when strlen() is overloaded by mb_strlen()
    if (function_exists('mb_internal_encoding') && (((int)ini_get('mbstring.func_overload')) & 2)) {
        $mbIntEnc = mb_internal_encoding();
        mb_internal_encoding('ASCII');
    }

    $out = _safe_serialize($value);
    if (isset($mbIntEnc)) {
        mb_internal_encoding($mbIntEnc);
    }

    return $out;
}

/**
 * Returns the serverload of the system.
 *
 * @return int The serverload of the system.
 */
function get_server_load($lang_unknown = 'Unknown')
{
    $serverload = [];

    // DIRECTORY_SEPARATOR checks if running windows
    if (DIRECTORY_SEPARATOR != '\\') {
        if (function_exists('sys_getloadavg')) {
            // sys_getloadavg() will return an array with [0] being load within the last minute.
            $serverload = sys_getloadavg();
            $serverload[0] = round($serverload[0], 4);
        } elseif (@file_exists('/proc/loadavg') && $load = @file_get_contents('/proc/loadavg')) {
            $serverload = explode(' ', $load);
            $serverload[0] = round($serverload[0], 4);
        }
        if (!is_numeric($serverload[0])) {
            if (SAFEMODE) {
                return $lang_unknown;
            }

            // Suhosin likes to throw a warning if exec is disabled then die - weird
            if ($func_blacklist = @ini_get('suhosin.executor.func.blacklist')) {
                if (strpos(',' . $func_blacklist . ',', 'exec') !== false) {
                    return $lang_unknown;
                }
            }
            // PHP disabled functions?
            if ($func_blacklist = @ini_get('disable_functions')) {
                if (strpos(',' . $func_blacklist . ',', 'exec') !== false) {
                    return $lang_unknown;
                }
            }

            $load = @exec('uptime');
            $load = explode('load average: ', $load);
            $serverload = explode(',', $load[1]);
            if (!is_array($serverload)) {
                return $lang_unknown;
            }
        }
    } else {
        return $lang_unknown;
    }

    $returnload = trim($serverload[0]);

    return $returnload;
}

/**
 * Returns the amount of memory allocated to the script.
 *
 * @return int The amount of memory allocated to the script.
 */
function get_memory_usage()
{
    if (function_exists('memory_get_peak_usage')) {
        return memory_get_peak_usage(true);
    } elseif (function_exists('memory_get_usage')) {
        return memory_get_usage(true);
    }
    return false;
}


/**
 * Returns the extension of a file.
 *
 * @param string $file The filename.
 * @return string The extension of the file.
 */
function get_extension($file)
{
    return my_strtolower(my_substr(strrchr($file, '.'), 1));
}

/**
 * Generates a random string.
 *
 * @param int $length The length of the string to generate.
 * @param bool $complex Whether to return complex string. Defaults to false
 * @return string The random string.
 */
function random_str($length = 8, $complex = false)
{
    $set = array_merge(range(0, 9), range('A', 'Z'), range('a', 'z'));
    $str = [];

    // Complex strings have always at least 3 characters, even if $length < 3
    if ($complex == true) {
        // At least one number
        $str[] = $set[my_rand(0, 9)];

        // At least one big letter
        $str[] = $set[my_rand(10, 35)];

        // At least one small letter
        $str[] = $set[my_rand(36, 61)];

        $length -= 3;
    }

    for ($i = 0; $i < $length; ++$i) {
        $str[] = $set[my_rand(0, 61)];
    }

    // Make sure they're in random order and convert them to a string
    shuffle($str);

    return implode($str);
}

/**
 * Fetch a color coded version of a warning level (based on it's percentage)
 *
 * @param int $level The warning level (percentage of 100)
 * @return string Formatted warning level
 */
function get_colored_warning_level($level)
{
    if ($level >= 80) {
        $warning_class = 'high_warning';
    } elseif ($level >= 50) {
        $warning_class = 'moderate_warning';
    } elseif ($level >= 25) {
        $warning_class = 'low_warning';
    } else {
        $warning_class = 'normal_warning';
    }

    return "<span class=\"{$warning_class}\">{$level}%</span>";
}

/**
 * Format a decimal number in to microseconds, milliseconds, or seconds.
 *
 * @param int $time The time in microseconds
 * @return string The friendly time duration
 */
function format_time_duration($time)
{
    if (!is_numeric($time)) {
        return 'N/A';//$lang->na;
    }

    if (round(1000000 * $time, 2) < 1000) {
        $time = number_format(round(1000000 * $time, 2)) . ' μs';
    } elseif (round(1000000 * $time, 2) >= 1000 && round(1000000 * $time, 2) < 1000000) {
        $time = number_format(round((1000 * $time), 2)) . ' ms';
    } else {
        $time = round($time, 3) . ' seconds';
    }

    return $time;
}


///**
// * Outputs the correct page headers.
// */
//function send_page_headers()
//{
//  //global $mybb;
//
//  if ($mybb->settings['nocacheheaders'] == 1) {
//    header('Expires: Sat, 1 Jan 2000 01:00:00 GMT');
//    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
//    header('Cache-Control: no-cache, must-revalidate');
//    header('Pragma: no-cache');
//  }
//}

/**
 * Select an alternating row colour based on the previous call to this function
 *
 * @param int $reset 1 to reset the row to trow1.
 * @return string trow1 or trow2 depending on the previous call
 */
function alt_trow($reset = 0)
{
    static $alttrow;

    if ($alttrow == 'trow1' && !$reset) {
        $trow = 'trow2';
    } else {
        $trow = 'trow1';
    }

    $alttrow = $trow;

    return $trow;
}

/**
 * Custom function for htmlspecialchars which takes in to account unicode
 *
 * @param string $message The string to format
 * @return string The string with htmlspecialchars applied
 */
function htmlspecialchars_uni($message)
{
    $message = preg_replace("#&(?!\#[0-9]+;)#si", "&amp;", $message); // Fix & but allow unicode
    $message = str_replace("<", "&lt;", $message);
    $message = str_replace(">", "&gt;", $message);
    $message = str_replace("\"", "&quot;", $message);
    return $message;
}


/**
 * Workaround for date limitation in PHP to establish the day of a birthday (Provided by meme)
 *
 * @param int $in The year.
 * @return array The number of days in each month of that year
 */
function get_bdays($in)
{
    return [
        31,
        ($in % 4 == 0 && ($in % 100 > 0 || $in % 400 == 0) ? 29 : 28),
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31
    ];
}


/**
 * Checks for the length of a string, mb strings accounted for
 *
 * @param string $string The string to check the length of.
 * @return int The length of the string.
 */
function my_strlen($string)
{
    $string = preg_replace("#&\#([0-9]+);#", "-", $string);

//FIXME set charset ???
//  if (strtolower($lang->settings['charset']) == "utf-8") {
    // Get rid of any excess RTL and LTR override for they are the workings of the devil
    $string = str_replace(dec_to_utf8(8238), '', $string);
    $string = str_replace(dec_to_utf8(8237), '', $string);

    // Remove dodgy whitespaces
    $string = str_replace(chr(0xCA), '', $string);
//  }
    $string = trim($string);

    if (function_exists('mb_strlen')) {
        $string_length = mb_strlen($string);
    } else {
        $string_length = strlen($string);
    }

    return $string_length;
}

/**
 * Cuts a string at a specified point, mb strings accounted for
 *
 * @param string $string The string to cut.
 * @param int $start Where to cut
 * @param int $length (optional) How much to cut
 * @param bool $handle_entities (optional) Properly handle HTML entities?
 * @return string The cut part of the string.
 */
function my_substr($string, $start, $length = null, $handle_entities = false)
{
    if ($handle_entities) {
        $string = unhtmlentities($string);
    }
    if (function_exists("mb_substr")) {
        if ($length != null) {
            $cut_string = mb_substr($string, $start, $length);
        } else {
            $cut_string = mb_substr($string, $start);
        }
    } else {
        if ($length != null) {
            $cut_string = substr($string, $start, $length);
        } else {
            $cut_string = substr($string, $start);
        }
    }

    if ($handle_entities) {
        $cut_string = htmlspecialchars_uni($cut_string);
    }
    return $cut_string;
}

/**
 * Lowers the case of a string, mb strings accounted for
 *
 * @param string $string The string to lower.
 * @return string The lowered string.
 */
function my_strtolower($string)
{
    if (function_exists("mb_strtolower")) {
        $string = mb_strtolower($string);
    } else {
        $string = strtolower($string);
    }

    return $string;
}

/**
 * Finds a needle in a haystack and returns it position, mb strings accounted for
 *
 * @param string $haystack String to look in (haystack)
 * @param string $needle What to look for (needle)
 * @param int $offset (optional) How much to offset
 * @return int|bool false on needle not found, integer position if found
 */
function my_strpos($haystack, $needle, $offset = 0)
{
    if ($needle == '') {
        return false;
    }

    if (function_exists('mb_strpos')) {
        $position = mb_strpos($haystack, $needle, $offset);
    } else {
        $position = strpos($haystack, $needle, $offset);
    }

    return $position;
}

/**
 * Ups the case of a string, mb strings accounted for
 *
 * @param string $string The string to up.
 * @return string The uped string.
 */
function my_strtoupper($string)
{
    if (function_exists("mb_strtoupper")) {
        $string = mb_strtoupper($string);
    } else {
        $string = strtoupper($string);
    }

    return $string;
}

/**
 * Returns any html entities to their original character
 *
 * @param string $string The string to un-htmlentitize.
 * @return string The un-htmlentitied' string.
 */
function unhtmlentities($string)
{
    // Replace numeric entities
    $string = preg_replace_callback('~&#x([0-9a-f]+);~i', create_function('$matches', 'return unichr(hexdec($matches[1]));'), $string);
    $string = preg_replace_callback('~&#([0-9]+);~', create_function('$matches', 'return unichr($matches[1]);'), $string);

    // Replace literal entities
    $trans_tbl = get_html_translation_table(HTML_ENTITIES);
    $trans_tbl = array_flip($trans_tbl);

    return strtr($string, $trans_tbl);
}

/**
 * Returns any ascii to it's character (utf-8 safe).
 *
 * @param int $c The ascii to characterize.
 * @return string|bool The characterized ascii. False on failure
 */
function unichr($c)
{
    if ($c <= 0x7F) {
        return chr($c);
    } elseif ($c <= 0x7FF) {
        return chr(0xC0 | $c >> 6) . chr(0x80 | $c & 0x3F);
    } elseif ($c <= 0xFFFF) {
        return chr(0xE0 | $c >> 12) . chr(0x80 | $c >> 6 & 0x3F)
            . chr(0x80 | $c & 0x3F);
    } elseif ($c <= 0x10FFFF) {
        return chr(0xF0 | $c >> 18) . chr(0x80 | $c >> 12 & 0x3F)
            . chr(0x80 | $c >> 6 & 0x3F)
            . chr(0x80 | $c & 0x3F);
    } else {
        return false;
    }
}

/**
 * Get the event poster.
 *
 * @param array $event The event data array.
 * @return string The link to the event poster.
 */
function get_event_poster($event)
{
    $event['username'] = $this->user->format_name($event['username'], $event['usergroup'], $event['displaygroup']);
    $event_poster = $this->user->build_profile_link($event['username'], $event['author']);
    return $event_poster;
}

/**
 * Get the event date.
 *
 * @param array $event The event data array.
 * @return string The event date.
 */
function get_event_date($event)
{
    $event_date = explode('-', $event['date']);
    $event_date = mktime(0, 0, 0, $event_date[1], $event_date[0], $event_date[2]);
    $event_date = $this->time->formatDate($mybb->settings['dateformat'], $event_date);

    return $event_date;
}

/**
 * Get the profile link.
 *
 * @param int $uid The user id of the profile.
 * @return string The url to the profile.
 */
function get_profile_link($uid = 0)
{
    $link = str_replace('{uid}', $uid, PROFILE_URL);
    return htmlspecialchars_uni($link);
}

/**
 * Get the announcement link.
 *
 * @param int $aid The announement id of the announcement.
 * @return string The url to the announcement.
 */
function get_announcement_link($aid = 0)
{
    $link = str_replace('{aid}', $aid, ANNOUNCEMENT_URL);
    return htmlspecialchars_uni($link);
}


/**
 * Build the thread link.
 *
 * @param int $tid The thread id of the thread.
 * @param int $page (Optional) The page number of the thread.
 * @param string $action (Optional) The action we're performing (ex, lastpost, newpost, etc)
 * @return string The url to the thread.
 */
function get_thread_link($tid, $page = 0, $action = '')
{
    if ($page > 1) {
        if ($action) {
            $link = THREAD_URL_ACTION;
            $link = str_replace('{action}', $action, $link);
        } else {
            $link = THREAD_URL_PAGED;
        }
        $link = str_replace('{tid}', $tid, $link);
        $link = str_replace('{page}', $page, $link);
        return htmlspecialchars_uni($link);
    } else {
        if ($action) {
            $link = THREAD_URL_ACTION;
            $link = str_replace('{action}', $action, $link);
        } else {
            $link = THREAD_URL;
        }
        $link = str_replace('{tid}', $tid, $link);
        return htmlspecialchars_uni($link);
    }
}

/**
 * Build the post link.
 *
 * @param int $pid The post ID of the post
 * @param int $tid The thread id of the post.
 * @return string The url to the post.
 */
function get_post_link($pid, $tid = 0)
{
    if ($tid > 0) {
        $link = str_replace('{tid}', $tid, THREAD_URL_POST);
        $link = str_replace('{pid}', $pid, $link);
        return htmlspecialchars_uni($link);
    } else {
        $link = str_replace('{pid}', $pid, POST_URL);
        return htmlspecialchars_uni($link);
    }
}

/**
 * Build the event link.
 *
 * @param int $eid The event ID of the event
 * @return string The URL of the event
 */
function get_event_link($eid)
{
    $link = str_replace('{eid}', $eid, EVENT_URL);
    return htmlspecialchars_uni($link);
}

/**
 * Build the link to a specified date on the calendar
 *
 * @param int $calendar The ID of the calendar
 * @param int $year The year
 * @param int $month The month
 * @param int $day The day (optional)
 * @return string The URL of the calendar
 */
function get_calendar_link($calendar, $year = 0, $month = 0, $day = 0)
{
    if ($day > 0) {
        $link = str_replace('{month}', $month, CALENDAR_URL_DAY);
        $link = str_replace('{year}', $year, $link);
        $link = str_replace('{day}', $day, $link);
        $link = str_replace('{calendar}', $calendar, $link);
        return htmlspecialchars_uni($link);
    } elseif ($month > 0) {
        $link = str_replace('{month}', $month, CALENDAR_URL_MONTH);
        $link = str_replace('{year}', $year, $link);
        $link = str_replace('{calendar}', $calendar, $link);
        return htmlspecialchars_uni($link);
    } /* Not implemented
	else if($year > 0)
	{
	}*/
    else {
        $link = str_replace('{calendar}', $calendar, CALENDAR_URL);
        return htmlspecialchars_uni($link);
    }
}

/**
 * Build the link to a specified week on the calendar
 *
 * @param int $calendar The ID of the calendar
 * @param int $week The week
 * @return string The URL of the calendar
 */
function get_calendar_week_link($calendar, $week)
{
    if ($week < 0) {
        $week = str_replace('-', 'n', $week);
    }
    $link = str_replace('{week}', $week, CALENDAR_URL_WEEK);
    $link = str_replace('{calendar}', $calendar, $link);
    return htmlspecialchars_uni($link);
}


/**
 * Validates the format of an email address.
 *
 * @param string $email The string to check.
 * @return boolean True when valid, false when invalid.
 */
function validate_email_format($email)
{
    if (strpos($email, ' ') !== false) {
        return false;
    }
    // Valid local characters for email addresses: http://www.remote.org/jochen/mail/info/chars.html
    return preg_match("/^[a-zA-Z0-9&*+\-_.{}~^\?=\/]+@[a-zA-Z0-9-]+\.([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]{2,}$/si", $email);
}

/**
 * Converts a decimal reference of a character to its UTF-8 equivalent
 * (Code by Anne van Kesteren, http://annevankesteren.nl/2005/05/character-references)
 *
 * @param int $src Decimal value of a character reference
 * @return string|bool
 */
function dec_to_utf8($src)
{
    $dest = '';

    if ($src < 0) {
        return false;
    } elseif ($src <= 0x007f) {
        $dest .= chr($src);
    } elseif ($src <= 0x07ff) {
        $dest .= chr(0xc0 | ($src >> 6));
        $dest .= chr(0x80 | ($src & 0x003f));
    } elseif ($src <= 0xffff) {
        $dest .= chr(0xe0 | ($src >> 12));
        $dest .= chr(0x80 | (($src >> 6) & 0x003f));
        $dest .= chr(0x80 | ($src & 0x003f));
    } elseif ($src <= 0x10ffff) {
        $dest .= chr(0xf0 | ($src >> 18));
        $dest .= chr(0x80 | (($src >> 12) & 0x3f));
        $dest .= chr(0x80 | (($src >> 6) & 0x3f));
        $dest .= chr(0x80 | ($src & 0x3f));
    } else {
        // Out of range
        return false;
    }

    return $dest;
}


/**
 * Fetch the contents of a remote file.
 *
 * @param string $url The URL of the remote file
 * @param array $post_data The array of post data
 * @param int $max_redirects Number of maximum redirects
 * @return string|bool The remote file contents. False on failure
 */
function fetch_remote_file($url, $post_data = [], $max_redirects = 20)
{
    $post_body = '';
    if (!empty($post_data)) {
        foreach ($post_data as $key => $val) {
            $post_body .= '&' . urlencode($key) . '=' . urlencode($val);
        }
        $post_body = ltrim($post_body, '&');
    }

    if (function_exists('curl_init')) {
        $can_followlocation = @ini_get('open_basedir') === '' && !SAFEMODE;

        $request_header = $max_redirects != 0 && !$can_followlocation;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, $request_header);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if ($max_redirects != 0 && $can_followlocation) {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_MAXREDIRS, $max_redirects);
        }

        if (!empty($post_body)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
        }

        $response = curl_exec($ch);

        if ($request_header) {
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $header_size);
            $body = substr($response, $header_size);

            if (in_array(curl_getinfo($ch, CURLINFO_HTTP_CODE), [301, 302])) {
                preg_match('/Location:(.*?)(?:\n|$)/', $header, $matches);

                if ($matches) {
                    $data = fetch_remote_file(trim(array_pop($matches)), $post_data, --$max_redirects);
                }
            } else {
                $data = $body;
            }
        } else {
            $data = $response;
        }

        curl_close($ch);
        return $data;
    } elseif (function_exists('fsockopen')) {
        $url = @parse_url($url);
        if (!$url['host']) {
            return false;
        }
        if (!isset($url['port'])) {
            $url['port'] = 80;
        }
        if (!isset($url['path'])) {
            $url['path'] = '/';
        }
        if (isset($url['query'])) {
            $url['path'] .= "?{$url['query']}";
        }

        $scheme = '';

        if ($url['scheme'] == 'https') {
            $scheme = 'ssl://';
            if ($url['port'] == 80) {
                $url['port'] = 443;
            }
        }

        $fp = @fsockopen($scheme . $url['host'], $url['port'], $error_no, $error, 10);
        @stream_set_timeout($fp, 10);
        if (!$fp) {
            return false;
        }
        $headers = [];
        if (!empty($post_body)) {
            $headers[] = "POST {$url['path']} HTTP/1.0";
            $headers[] = 'Content-Length: ' . strlen($post_body);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        } else {
            $headers[] = "GET {$url['path']} HTTP/1.0";
        }

        $headers[] = "Host: {$url['host']}";
        $headers[] = 'Connection: Close';
        $headers[] = '';

        if (!empty($post_body)) {
            $headers[] = $post_body;
        } else {
            // If we have no post body, we need to add an empty element to make sure we've got \r\n\r\n before the (non-existent) body starts
            $headers[] = '';
        }

        $headers = implode("\r\n", $headers);
        if (!@fwrite($fp, $headers)) {
            return false;
        }

        $data = null;

        while (!feof($fp)) {
            $data .= fgets($fp, 12800);
        }
        fclose($fp);

        $data = explode("\r\n\r\n", $data, 2);

        $header = $data[0];
        $status_line = current(explode("\n\n", $header, 1));
        $body = $data[1];

        if ($max_redirects != 0 && (strstr($status_line, ' 301 ') || strstr($status_line, ' 302 '))) {
            preg_match('/Location:(.*?)(?:\n|$)/', $header, $matches);

            if ($matches) {
                $data = fetch_remote_file(trim(array_pop($matches)), $post_data, --$max_redirects);
            }
        } else {
            $data = $body;
        }

        return $data;
    } elseif (empty($post_data)) {
        return @implode('', @file($url));
    } else {
        return false;
    }
}

/**
 * Custom chmod function to fix problems with hosts who's server configurations screw up umasks
 *
 * @param string $file The file to chmod
 * @param string $mode The mode to chmod(i.e. 0666)
 * @return bool
 */
function my_chmod($file, $mode)
{
    // Passing $mode as an octal number causes strlen and substr to return incorrect values. Instead pass as a string
    if (substr($mode, 0, 1) != '0' || strlen($mode) !== 4) {
        return false;
    }
    $old_umask = umask(0);

    // We convert the octal string to a decimal number because passing a octal string doesn't work with chmod
    // and type casting subsequently removes the prepended 0 which is needed for octal numbers
    $result = chmod($file, octdec($mode));
    umask($old_umask);
    return $result;
}

/**
 * Custom rmdir function to loop through an entire directory and delete all files/folders within
 *
 * @param string $path The path to the directory
 * @param array $ignore Any files you wish to ignore (optional)
 * @return bool
 */
//function my_rmdir_recursive($path, $ignore = array())
//{
//  //global $orig_dir;
//
//  if (!isset($orig_dir)) {
//    $orig_dir = $path;
//  }
//
//  if (@is_dir($path) && !@is_link($path)) {
//    if ($dh = @opendir($path)) {
//      while (($file = @readdir($dh)) !== false) {
//        if ($file == '.' || $file == '..' || $file == '.svn' || in_array($path . '/' . $file, $ignore) || !my_rmdir_recursive($path . '/' . $file)) {
//          continue;
//        }
//      }
//      @closedir($dh);
//    }
//
//    // Are we done? Don't delete the main folder too and return true
//    if ($path == $orig_dir) {
//      return true;
//    }
//
//    return @rmdir($path);
//  }
//
//  return @unlink($path);
//}

/**
 * Counts the number of subforums in a array([pid][disporder][fid]) starting from the pid
 *
 * @param array $array The array of forums
 * @return integer The number of sub forums
 */
function subforums_count($array)
{
    $count = 0;
    foreach ($array as $array2) {
        $count += count($array2);
    }

    return $count;
}

/**
 * Converts a human readable IP address to its packed in_addr representation
 *
 * @param string $ip The IP to convert
 * @return string IP in 32bit or 128bit binary format
 */
function my_inet_pton($ip)
{
    if (function_exists('inet_pton')) {
        return @inet_pton($ip);
    } else {
        /**
         * Replace inet_pton()
         *
         * @category    PHP
         * @package     PHP_Compat
         * @license     LGPL - http://www.gnu.org/licenses/lgpl.html
         * @copyright   2004-2007 Aidan Lister <aidan@php.net>, Arpad Ray <arpad@php.net>
         * @link        http://php.net/inet_pton
         * @author      Arpad Ray <arpad@php.net>
         * @version     $Revision: 269597 $
         */
        $r = ip2long($ip);
        if ($r !== false && $r != -1) {
            return pack('N', $r);
        }

        $delim_count = substr_count($ip, ':');
        if ($delim_count < 1 || $delim_count > 7) {
            return false;
        }

        $r = explode(':', $ip);
        $rcount = count($r);
        if (($doub = array_search('', $r, 1)) !== false) {
            $length = (!$doub || $doub == $rcount - 1 ? 2 : 1);
            array_splice($r, $doub, $length, array_fill(0, 8 + $length - $rcount, 0));
        }

        $r = array_map('hexdec', $r);
        array_unshift($r, 'n*');
        $r = call_user_func_array('pack', $r);

        return $r;
    }
}

/**
 * Converts a packed internet address to a human readable representation
 *
 * @param string $ip IP in 32bit or 128bit binary format
 * @return string IP in human readable format
 */
function my_inet_ntop($ip)
{
    if (function_exists('inet_ntop')) {
        return @inet_ntop($ip);
    } else {
        /**
         * Replace inet_ntop()
         *
         * @category    PHP
         * @package     PHP_Compat
         * @license     LGPL - http://www.gnu.org/licenses/lgpl.html
         * @copyright   2004-2007 Aidan Lister <aidan@php.net>, Arpad Ray <arpad@php.net>
         * @link        http://php.net/inet_ntop
         * @author      Arpad Ray <arpad@php.net>
         * @version     $Revision: 269597 $
         */
        switch (strlen($ip)) {
            case 4:
                list(, $r) = unpack('N', $ip);
                return long2ip($r);
            case 16:
                $r = substr(chunk_split(bin2hex($ip), 4, ':'), 0, -1);
                $r = preg_replace(
                    ['/(?::?\b0+\b:?){2,}/', '/\b0+([^0])/e'],
                    ['::', '(int)"$1"?"$1":"0$1"'],
                    $r
                );
                return $r;
        }
        return false;
    }
}

/**
 * Fetch an binary formatted range for searching IPv4 and IPv6 IP addresses.
 *
 * @param string $ipaddress The IP address to convert to a range
 * @return string|array|bool If a full IP address is provided, the in_addr representation, otherwise an array of the upper & lower extremities of the IP. False on failure
 */
function fetch_ip_range($ipaddress)
{
    // Wildcard
    if (strpos($ipaddress, '*') !== false) {
        if (strpos($ipaddress, ':') !== false) {
            // IPv6
            $upper = str_replace('*', 'ffff', $ipaddress);
            $lower = str_replace('*', '0', $ipaddress);
        } else {
            // IPv4
            $ip_bits = count(explode('.', $ipaddress));
            if ($ip_bits < 4) {
                // Support for 127.0.*
                $replacement = str_repeat('.*', 4 - $ip_bits);
                $ipaddress = substr_replace($ipaddress, $replacement, strrpos($ipaddress, '*') + 1, 0);
            }
            $upper = str_replace('*', '255', $ipaddress);
            $lower = str_replace('*', '0', $ipaddress);
        }
        $upper = my_inet_pton($upper);
        $lower = my_inet_pton($lower);
        if ($upper === false || $lower === false) {
            return false;
        }
        return [$lower, $upper];
    } // CIDR notation
    elseif (strpos($ipaddress, '/') !== false) {
        $ipaddress = explode('/', $ipaddress);
        $ip_address = $ipaddress[0];
        $ip_range = (int)$ipaddress[1];

        if (empty($ip_address) || empty($ip_range)) {
            // Invalid input
            return false;
        } else {
            $ip_address = my_inet_pton($ip_address);

            if (!$ip_address) {
                // Invalid IP address
                return false;
            }
        }

        /**
         * Taken from: https://github.com/NewEraCracker/php_work/blob/master/ipRangeCalculate.php
         * Author: NewEraCracker
         * License: Public Domain
         */

        // Pack IP, Set some vars
        $ip_pack = $ip_address;
        $ip_pack_size = strlen($ip_pack);
        $ip_bits_size = $ip_pack_size * 8;

        // IP bits (lots of 0's and 1's)
        $ip_bits = '';
        for ($i = 0; $i < $ip_pack_size; $i = $i + 1) {
            $bit = decbin(ord($ip_pack[$i]));
            $bit = str_pad($bit, 8, '0', STR_PAD_LEFT);
            $ip_bits .= $bit;
        }

        // Significative bits (from the ip range)
        $ip_bits = substr($ip_bits, 0, $ip_range);

        // Some calculations
        $ip_lower_bits = str_pad($ip_bits, $ip_bits_size, '0', STR_PAD_RIGHT);
        $ip_higher_bits = str_pad($ip_bits, $ip_bits_size, '1', STR_PAD_RIGHT);

        // Lower IP
        $ip_lower_pack = '';
        for ($i = 0; $i < $ip_bits_size; $i = $i + 8) {
            $chr = substr($ip_lower_bits, $i, 8);
            $chr = chr(bindec($chr));
            $ip_lower_pack .= $chr;
        }

        // Higher IP
        $ip_higher_pack = '';
        for ($i = 0; $i < $ip_bits_size; $i = $i + 8) {
            $chr = substr($ip_higher_bits, $i, 8);
            $chr = chr(bindec($chr));
            $ip_higher_pack .= $chr;
        }

        return [$ip_lower_pack, $ip_higher_pack];
    } // Just on IP address
    else {
        return my_inet_pton($ipaddress);
    }
}

/**
 * Time how long it takes for a particular piece of code to run. Place calls above & below the block of code.
 *
 * @return float The time taken
 */
function get_execution_time()
{
    static $time_start;

    $time = microtime(true);


    // Just starting timer, init and return
    if (!$time_start) {
        $time_start = $time;
        return;
    } // Timer has run, return execution time
    else {
        $total = $time - $time_start;
        if ($total < 0) {
            $total = 0;
        }
        $time_start = 0;
        return $total;
    }
}


/**
 * Returns a signed value equal to an integer
 *
 * @param int $int The integer
 * @return string The signed equivalent
 */
function signed($int)
{
    if ($int < 0) {
        return "$int";
    } else {
        return "+$int";
    }
}

/**
 * Returns a securely generated seed
 *
 * @return string A secure binary seed
 */
function secure_binary_seed_rng($bytes)
{
    $output = null;

    if (version_compare(PHP_VERSION, '7.0', '>=')) {
        try {
            $output = random_bytes($bytes);
        } catch (Exception $e) {
        }
    }

    if (strlen($output) < $bytes) {
        if (@is_readable('/dev/urandom') && ($handle = @fopen('/dev/urandom', 'rb'))) {
            $output = @fread($handle, $bytes);
            @fclose($handle);
        }
    } else {
        return $output;
    }

    if (strlen($output) < $bytes) {
        if (function_exists('mcrypt_create_iv')) {
            if (DIRECTORY_SEPARATOR == '/') {
                $source = MCRYPT_DEV_URANDOM;
            } else {
                $source = MCRYPT_RAND;
            }

            $output = @mcrypt_create_iv($bytes, $source);
        }
    } else {
        return $output;
    }

    if (strlen($output) < $bytes) {
        if (function_exists('openssl_random_pseudo_bytes')) {
            // PHP <5.3.4 had a bug which makes that function unusable on Windows
            if ((DIRECTORY_SEPARATOR == '/') || version_compare(PHP_VERSION, '5.3.4', '>=')) {
                $output = openssl_random_pseudo_bytes($bytes, $crypto_strong);
                if ($crypto_strong == false) {
                    $output = null;
                }
            }
        }
    } else {
        return $output;
    }

    if (strlen($output) < $bytes) {
        if (class_exists('COM')) {
            try {
                $CAPI_Util = new COM('CAPICOM.Utilities.1');
                if (is_callable([$CAPI_Util, 'GetRandom'])) {
                    $output = $CAPI_Util->GetRandom($bytes, 0);
                }
            } catch (Exception $e) {
            }
        }
    } else {
        return $output;
    }

    if (strlen($output) < $bytes) {
        // Close to what PHP basically uses internally to seed, but not quite.
        $unique_state = microtime() . @getmypid();

        $rounds = ceil($bytes / 16);

        for ($i = 0; $i < $rounds; $i++) {
            $unique_state = md5(microtime() . $unique_state);
            $output .= md5($unique_state);
        }

        $output = substr($output, 0, ($bytes * 2));

        $output = pack('H*', $output);

        return $output;
    } else {
        return $output;
    }
}

/**
 * Returns a securely generated seed integer
 *
 * @return int An integer equivalent of a secure hexadecimal seed
 */
function secure_seed_rng()
{
    $bytes = PHP_INT_SIZE;

    do {
        $output = secure_binary_seed_rng($bytes);

        // convert binary data to a decimal number
        if ($bytes == 4) {
            $elements = unpack('i', $output);
            $output = abs($elements[1]);
        } else {
            $elements = unpack('N2', $output);
            $output = abs($elements[1] << 32 | $elements[2]);
        }
    } while ($output > PHP_INT_MAX);

    return $output;
}

/**
 * Generates a cryptographically secure random number.
 *
 * @param int $min Optional lowest value to be returned (default: 0)
 * @param int $max Optional highest value to be returned (default: PHP_INT_MAX)
 */
function my_rand($min = 0, $max = PHP_INT_MAX)
{
    // backward compatibility
    if ($min === null || $max === null || $max < $min) {
        $min = 0;
        $max = PHP_INT_MAX;
    }

    if (version_compare(PHP_VERSION, '7.0', '>=')) {
        try {
            $result = random_int($min, $max);
        } catch (Exception $e) {
        }

        if (isset($result)) {
            return $result;
        }
    }

    $seed = secure_seed_rng();

    $distance = $max - $min;
    return $min + floor($distance * ($seed / PHP_INT_MAX));
}

/**
 * More robust version of PHP's trim() function. It includes a list of UTF-8 blank characters
 * from http://kb.mozillazine.org/Network.IDN.blacklist_chars
 *
 * @param string $string The string to trim from
 * @param string $charlist Optional. The stripped characters can also be specified using the charlist parameter
 * @return string The trimmed string
 */
function trim_blank_chrs($string, $charlist = '')
{
    $hex_chrs = [
        0x09 => 1, // \x{0009}
        0x0A => 1, // \x{000A}
        0x0B => 1, // \x{000B}
        0x0D => 1, // \x{000D}
        0x20 => 1, // \x{0020}
        0xC2 => [0x81 => 1, 0x8D => 1, 0x90 => 1, 0x9D => 1, 0xA0 => 1, 0xAD => 1], // \x{0081}, \x{008D}, \x{0090}, \x{009D}, \x{00A0}, \x{00AD}
        0xCC => [0xB7 => 1, 0xB8 => 1], // \x{0337}, \x{0338}
        0xE1 => [0x85 => [0x9F => 1, 0xA0 => 1], 0x9A => [0x80 => 1], 0xA0 => [0x8E => 1]], // \x{115F}, \x{1160}, \x{1680}, \x{180E}
        0xE2 => [0x80 => [0x80 => 1, 0x81 => 1, 0x82 => 1, 0x83 => 1, 0x84 => 1, 0x85 => 1, 0x86 => 1, 0x87 => 1, 0x88 => 1, 0x89 => 1, 0x8A => 1, 0x8B => 1, 0x8C => 1, 0x8D => 1, 0x8E => 1, 0x8F => 1, // \x{2000} - \x{200F}
            0xA8 => 1, 0xA9 => 1, 0xAA => 1, 0xAB => 1, 0xAC => 1, 0xAD => 1, 0xAE => 1, 0xAF => 1], // \x{2028} - \x{202F}
            0x81 => [0x9F => 1]], // \x{205F}
        0xE3 => [0x80 => [0x80 => 1], // \x{3000}
            0x85 => [0xA4 => 1]], // \x{3164}
        0xEF => [0xBB => [0xBF => 1], // \x{FEFF}
            0xBE => [0xA0 => 1], // \x{FFA0}
            0xBF => [0xB9 => 1, 0xBA => 1, 0xBB => 1]], // \x{FFF9} - \x{FFFB}
    ];

    $hex_chrs_rev = [
        0x09 => 1, // \x{0009}
        0x0A => 1, // \x{000A}
        0x0B => 1, // \x{000B}
        0x0D => 1, // \x{000D}
        0x20 => 1, // \x{0020}
        0x81 => [0xC2 => 1, 0x80 => [0xE2 => 1]], // \x{0081}, \x{2001}
        0x8D => [0xC2 => 1, 0x80 => [0xE2 => 1]], // \x{008D}, \x{200D}
        0x90 => [0xC2 => 1], // \x{0090}
        0x9D => [0xC2 => 1], // \x{009D}
        0xA0 => [0xC2 => 1, 0x85 => [0xE1 => 1], 0x81 => [0xE2 => 1], 0xBE => [0xEF => 1]], // \x{00A0}, \x{1160}, \x{2060}, \x{FFA0}
        0xAD => [0xC2 => 1, 0x80 => [0xE2 => 1]], // \x{00AD}, \x{202D}
        0xB8 => [0xCC => 1], // \x{0338}
        0xB7 => [0xCC => 1], // \x{0337}
        0x9F => [0x85 => [0xE1 => 1], 0x81 => [0xE2 => 1]], // \x{115F}, \x{205F}
        0x80 => [0x9A => [0xE1 => 1], 0x80 => [0xE2 => 1, 0xE3 => 1]], // \x{1680}, \x{2000}, \x{3000}
        0x8E => [0xA0 => [0xE1 => 1], 0x80 => [0xE2 => 1]], // \x{180E}, \x{200E}
        0x82 => [0x80 => [0xE2 => 1]], // \x{2002}
        0x83 => [0x80 => [0xE2 => 1]], // \x{2003}
        0x84 => [0x80 => [0xE2 => 1]], // \x{2004}
        0x85 => [0x80 => [0xE2 => 1]], // \x{2005}
        0x86 => [0x80 => [0xE2 => 1]], // \x{2006}
        0x87 => [0x80 => [0xE2 => 1]], // \x{2007}
        0x88 => [0x80 => [0xE2 => 1]], // \x{2008}
        0x89 => [0x80 => [0xE2 => 1]], // \x{2009}
        0x8A => [0x80 => [0xE2 => 1]], // \x{200A}
        0x8B => [0x80 => [0xE2 => 1]], // \x{200B}
        0x8C => [0x80 => [0xE2 => 1]], // \x{200C}
        0x8F => [0x80 => [0xE2 => 1]], // \x{200F}
        0xA8 => [0x80 => [0xE2 => 1]], // \x{2028}
        0xA9 => [0x80 => [0xE2 => 1]], // \x{2029}
        0xAA => [0x80 => [0xE2 => 1]], // \x{202A}
        0xAB => [0x80 => [0xE2 => 1]], // \x{202B}
        0xAC => [0x80 => [0xE2 => 1]], // \x{202C}
        0xAE => [0x80 => [0xE2 => 1]], // \x{202E}
        0xAF => [0x80 => [0xE2 => 1]], // \x{202F}
        0xA4 => [0x85 => [0xE3 => 1]], // \x{3164}
        0xBF => [0xBB => [0xEF => 1]], // \x{FEFF}
        0xB9 => [0xBF => [0xEF => 1]], // \x{FFF9}
        0xBA => [0xBF => [0xEF => 1]], // \x{FFFA}
        0xBB => [0xBF => [0xEF => 1]], // \x{FFFB}
    ];

    $i = 0;
    // Start from the beginning and work our way in
    do {
        // Check to see if we have matched a first character in our utf-8 array
        $offset = match_sequence($string, $hex_chrs);
        if (!$offset) {
            // If not, then we must have a "good" character and we don't need to do anymore processing
            break;
        }
        $string = substr($string, $offset);
    } while (++$i);

    // Start from the end and work our way in
    $string = strrev($string);
    do {
        // Check to see if we have matched a first character in our utf-8 array
        $offset = match_sequence($string, $hex_chrs_rev);
        if (!$offset) {
            // If not, then we must have a "good" character and we don't need to do anymore processing
            break;
        }
        $string = substr($string, $offset);
    } while (++$i);
    $string = strrev($string);

    if ($charlist) {
        $string = trim($string, $charlist);
    } else {
        $string = trim($string);
    }

    return $string;
}

/**
 * Match a sequence
 *
 * @param string $string The string to match from
 * @param array $array The array to match from
 * @param int $i Number in the string
 * @param int $n Number of matches
 * @return int The number matched
 */
function match_sequence($string, $array, $i = 0, $n = 0)
{
    if ($string === '') {
        return 0;
    }

    $ord = ord($string[$i]);
    if (array_key_exists($ord, $array)) {
        $level = $array[$ord];
        ++$n;
        if (is_array($level)) {
            ++$i;
            return match_sequence($string, $level, $i, $n);
        }
        return $n;
    }

    return 0;
}

/**
 * Obtain the version of GD installed.
 *
 * @return float Version of GD
 */
function gd_version()
{
    static $gd_version;

    if ($gd_version) {
        return $gd_version;
    }
    if (!extension_loaded('gd')) {
        return;
    }

    if (function_exists('gd_info')) {
        $gd_info = gd_info();
        preg_match('/\d/', $gd_info['GD Version'], $gd);
        $gd_version = $gd[0];
    } else {
        ob_start();
        phpinfo(8);
        $info = ob_get_contents();
        ob_end_clean();
        $info = stristr($info, 'gd version');
        preg_match('/\d/', $info, $gd);
        $gd_version = $gd[0];
    }

    return $gd_version;
}

/**
 * Validates an UTF-8 string.
 *
 * @param string $input The string to be checked
 * @param boolean $allow_mb4 Allow 4 byte UTF-8 characters?
 * @param boolean $return Return the cleaned string?
 * @return string|boolean Cleaned string or boolean
 */
function validate_utf8_string($input, $allow_mb4 = true, $return = true)
{
    // Valid UTF-8 sequence?
    if (!preg_match('##u', $input)) {
        $string = '';
        $len = strlen($input);
        for ($i = 0; $i < $len; $i++) {
            $c = ord($input[$i]);
            if ($c > 128) {
                if ($c > 247 || $c <= 191) {
                    if ($return) {
                        $string .= '?';
                        continue;
                    } else {
                        return false;
                    }
                } elseif ($c > 239) {
                    $bytes = 4;
                } elseif ($c > 223) {
                    $bytes = 3;
                } elseif ($c > 191) {
                    $bytes = 2;
                }
                if (($i + $bytes) > $len) {
                    if ($return) {
                        $string .= '?';
                        break;
                    } else {
                        return false;
                    }
                }
                $valid = true;
                $multibytes = $input[$i];
                while ($bytes > 1) {
                    $i++;
                    $b = ord($input[$i]);
                    if ($b < 128 || $b > 191) {
                        if ($return) {
                            $valid = false;
                            $string .= '?';
                            break;
                        } else {
                            return false;
                        }
                    } else {
                        $multibytes .= $input[$i];
                    }
                    $bytes--;
                }
                if ($valid) {
                    $string .= $multibytes;
                }
            } else {
                $string .= $input[$i];
            }
        }
        $input = $string;
    }
    if ($return) {
        if ($allow_mb4) {
            return $input;
        } else {
            return preg_replace("#[^\\x00-\\x7F][\\x80-\\xBF]{3,}#", '?', $input);
        }
    } else {
        if ($allow_mb4) {
            return true;
        } else {
            return !preg_match("#[^\\x00-\\x7F][\\x80-\\xBF]{3,}#", $input);
        }
    }
}
