<?php
/**
 * BB 1.8
 * Copyright 2014 BB Group, All Rights Reserved
 *
 * Website: http://www.mybb.com
 * License: http://www.mybb.com/about/license
 *
 */


// Begin!
switch ($this->bb->input['action']) {
    // Delayed Moderation
    case "cancel_delayedmoderation":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $this->bb->add_breadcrumb($lang->delayed_moderation);
        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks('moderation_cancel_delayedmoderation');

        $this->db->delete_query("delayedmoderation", "did='".$this->bb->getInput('did', 0)."'");

        if ($this->tid == 0) {
            return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_delayed_moderation_cancelled);
        } else {
            return $this->moderation_redirect("moderation.php?action=delayedmoderation&amp;tid={$this->tid}&amp;my_post_key={$this->bb->post_code}", $lang->redirect_delayed_moderation_cancelled);
        }
        break;

    case "do_delayedmoderation":
    case "delayedmoderation":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->bb->getInput('date_day', 0)) {
            $this->bb->input['date_day'] = date('d', TIME_NOW);
        }
        if (!$this->bb->getInput('date_month', 0)) {
            $this->bb->input['date_month'] = date('m', TIME_NOW);
        }

        // Assume in-line moderation if TID is not set
        if (!empty($this->bb->input['tid'])) {
            $this->bb->input['tids'] = $this->tid;
        } else {
            if ($this->bb->getInput('inlinetype', '') == 'search') {
                $tids = $this->getids($this->bb->getInput('searchid', 0), 'search');
            } else {
                $this->fid = $this->bb->getInput('fid', 0);
                $tids = $this->getids($this->fid, "forum");
            }
            if (count($tids) < 1) {
                $this->bb->error($lang->error_inline_nothreadsselected);
            }

            $this->bb->input['tids'] = $tids;
        }

        $this->bb->add_breadcrumb($lang->delayed_moderation);

        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $errors = [];
        $customthreadtools = "";

        $allowed_types = ['openclosethread', 'softdeleterestorethread', 'deletethread', 'move', 'stick', 'merge', 'removeredirects', 'removesubscriptions', 'approveunapprovethread'];

        $this->bb->input['type'] = $this->bb->getInput('type', '');

        switch ($this->db->type) {
            case "pgsql":
            case "sqlite":
                $query = $this->db->simple_select("modtools", 'tid, name', "(','||forums||',' LIKE '%,$this->fid,%' OR ','||forums||',' LIKE '%,-1,%' OR forums='') AND type = 't'");
                break;
            default:
                $query = $this->db->simple_select("modtools", 'tid, name', "(CONCAT(',',forums,',') LIKE '%,$this->fid,%' OR CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='') AND type = 't'");
        }
        while ($tool = $this->db->fetch_array($query)) {
            $allowed_types[] = "modtool_".$tool['tid'];

            $tool['name'] = htmlspecialchars_uni($tool['name']);

            $checked = "";
            if ($this->bb->input['type'] == "modtool_".$tool['tid']) {
                $checked = "checked=\"checked\"";
            }

            eval("\$customthreadtools .= \"".$templates->get("moderation_delayedmoderation_custommodtool")."\";");
        }

        $this->bb->input['delayedmoderation'] = $this->bb->getInput('delayedmoderation', []);

        if ($this->bb->input['action'] == "do_delayedmoderation" && $this->bb->request_method == "post") {
            if (!in_array($this->bb->input['type'], $allowed_types)) {
                $this->bb->input['type'] = '';
                $errors[] = $lang->error_delayedmoderation_unsupported_type;
            }

            if ($this->bb->input['type'] == 'move' &&
            (!isset($this->bb->input['delayedmoderation']['method']) ||
            !in_array(
                $this->bb->input['delayedmoderation']['method'],
                ['move', 'redirect', 'copy']
            ))) {
                $this->bb->input['delayedmoderation']['method'] = '';
                $errors[] = $lang->error_delayedmoderation_unsupported_method;
            }

            if ($this->bb->input['type'] == 'move') {
                $newforum = $this->forum->get_forum($this->fid);
                if (!$newforum || $newforum['type'] != "f" || ($newforum['type'] == "f" && $newforum['linkto'] != '')) {
                    $errors[] = $lang->error_invalidforum;
                }
            }

            if ($this->bb->input['date_day'] > 31 || $this->bb->input['date_day'] < 1) {
                $errors[] = $lang->error_delayedmoderation_invalid_date_day;
            }

            if ($this->bb->input['date_month'] > 12 || $this->bb->input['date_month'] < 1) {
                $errors[] = $lang->error_delayedmoderation_invalid_date_month;
            }

            if ($this->bb->input['date_year'] < gmdate('Y', TIME_NOW)) {
                $errors[] = $lang->error_delayedmoderation_invalid_date_year;
            }

            $date_time = explode(' ', $this->bb->getInput('date_time', ''));
            $date_time = explode(':', (string)$date_time[0]);

            if (stristr($this->bb->input['date_time'], 'pm')) {
                $date_time[0] = 12+$date_time[0];
                if ($date_time[0] >= 24) {
                    $date_time[0] = '00';
                }
            }

            $rundate = mktime((int)$date_time[0], (int)$date_time[1], date('s', TIME_NOW), $this->bb->getInput('date_month', 0), $this->bb->getInput('date_day', 0), $this->bb->getInput('date_year', 0));

            if (!$errors) {
                if (is_array($this->bb->input['tids'])) {
                    $this->bb->input['tids'] = implode(',', $this->bb->input['tids']);
                }

                $did = $this->db->insert_query("delayedmoderation", [
                    'type' => $this->db->escape_string($this->bb->input['type']),
                    'delaydateline' => (int)$rundate,
                    'uid' => $this->user->uid,
                    'tids' => $this->db->escape_string($this->bb->input['tids']),
                    'fid' => $this->fid,
                    'dateline' => TIME_NOW,
                    'inputs' => $this->db->escape_string(my_serialize($this->bb->input['delayedmoderation']))
                ]);

                $this->plugins->runHooks('moderation_do_delayedmoderation');

                $rundate_format = $this->time->formatDate('relative', $rundate, '', 2);
                $lang->redirect_delayed_moderation_thread = $lang->sprintf($lang->redirect_delayed_moderation_thread, $rundate_format);

                if (!empty($this->bb->input['tid'])) {
                    return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_delayed_moderation_thread);
                } else {
                    if ($this->bb->getInput('inlinetype', '') === 'search') {
                        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->sprintf($lang->redirect_delayed_moderation_search, $rundate_format));
                    } else {
                        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->sprintf($lang->redirect_delayed_moderation_forum, $rundate_format));
                    }
                }
            } else {
                $type_selected = [];
                foreach ($allowed_types as $type) {
                    $type_selected[$type] = '';
                }
                $type_selected[$this->bb->getInput('type', '')] = 'checked="checked"';
                $method_selected = ['move' => '', 'redirect' => '', 'copy' => ''];
                if (isset($this->bb->input['delayedmoderation']['method'])) {
                    $method_selected[$this->bb->input['delayedmoderation']['method']] = "checked=\"checked\"";
                }

                foreach (['redirect_expire', 'new_forum', 'subject', 'threadurl'] as $value) {
                    if (!isset($this->bb->input['delayedmoderation'][$value])) {
                        $this->bb->input['delayedmoderation'][$value] = '';
                    }
                }
                $this->bb->input['delayedmoderation']['redirect_expire'] = (int)$this->bb->input['delayedmoderation']['redirect_expire'];
                $this->bb->input['delayedmoderation']['new_forum'] = (int)$this->bb->input['delayedmoderation']['new_forum'];
                $this->bb->input['delayedmoderation']['subject'] = htmlspecialchars_uni($this->bb->input['delayedmoderation']['subject']);
                $this->bb->input['delayedmoderation']['threadurl'] = htmlspecialchars_uni($this->bb->input['delayedmoderation']['threadurl']);

                $forumselect = $this->forum->build_forum_jump("", $this->bb->input['delayedmoderation']['new_forum'], 1, '', 0, true, '', "delayedmoderation[new_forum]");
            }
        } else {
            $type_selected = [];
            foreach ($allowed_types as $type) {
                $type_selected[$type] = '';
            }
            $type_selected['openclosethread'] = "checked=\"checked\"";
            $method_selected = ['move' => 'checked="checked"', 'redirect' => '', 'copy' => ''];

            $this->bb->input['delayedmoderation']['redirect_expire'] = '';
            $this->bb->input['delayedmoderation']['subject'] = $thread['subject'];
            $this->bb->input['delayedmoderation']['threadurl'] = '';

            $forumselect = $this->forum->build_forum_jump("", $this->fid, 1, '', 0, true, '', "delayedmoderation[new_forum]");
        }

        if (isset($errors) && count($errors) > 0) {
            $display_errors = $this->bb->inline_error($errors);
        } else {
            $display_errors = '';
        }

        $this->bb->forum_cache = $cache->read("forums");

        $actions = [
            'openclosethread' => $lang->open_close_thread,
            'softdeleterestorethread' => $lang->softdelete_restore_thread,
            'deletethread' => $lang->delete_thread,
            'move' => $lang->move_copy_thread,
            'stick' => $lang->stick_unstick_thread,
            'merge' => $lang->merge_threads,
            'removeredirects' => $lang->remove_redirects,
            'removesubscriptions' => $lang->remove_subscriptions,
            'approveunapprovethread' => $lang->approve_unapprove_thread
        ];

        switch ($this->db->type) {
            case "pgsql":
            case "sqlite":
                $query = $this->db->simple_select("modtools", 'tid, name', "(','||forums||',' LIKE '%,$this->fid,%' OR ','||forums||',' LIKE '%,-1,%' OR forums='') AND type = 't'");
                break;
            default:
                $query = $this->db->simple_select("modtools", 'tid, name', "(CONCAT(',',forums,',') LIKE '%,$this->fid,%' OR CONCAT(',',forums,',') LIKE '%,-1,%' OR forums='') AND type = 't'");
        }
        while ($tool = $this->db->fetch_array($query)) {
            $actions['modtool_'.$tool['tid']] = htmlspecialchars_uni($tool['name']);
        }

        $delayedmods = '';
        $trow = alt_trow(1);
        if ($this->tid == 0) {
            // Inline thread moderation is used
            if ($this->bb->getInput('inlinetype', '') == 'search') {
                $tids = $this->getids($this->bb->getInput('searchid', ''), 'search');
            } else {
                $tids = $this->getids($this->fid, "forum");
            }
            $where_array = [];
            switch ($this->db->type) {
                case "pgsql":
                case "sqlite":
                    foreach ($tids as $like) {
                        $where_array[] = "','||d.tids||',' LIKE '%,".$this->db->escape_string($like).",%'";
                    }
                    $where_statement = implode(" OR ", $where_array);
                    break;
                default:
                    foreach ($tids as $like) {
                        $where_array[] = "CONCAT(',',d.tids,',') LIKE  '%,".$this->db->escape_string($like).",%'";
                    }
                    $where_statement = implode(" OR ", $where_array);
            }
            $query = $this->db->query("
				SELECT d.*, u.username, f.name AS fname
				FROM ".TABLE_PREFIX."delayedmoderation d
				LEFT JOIN ".TABLE_PREFIX."users u ON (u.uid=d.uid)
				LEFT JOIN ".TABLE_PREFIX."forums f ON (f.fid=d.fid)
				WHERE ".$where_statement."
				ORDER BY d.dateline DESC
				LIMIT  0, 20
			");
        } else {
            switch ($this->db->type) {
                case "pgsql":
                case "sqlite":
                    $query = $this->db->query("
						SELECT d.*, u.username, f.name AS fname
						FROM ".TABLE_PREFIX."delayedmoderation d
						LEFT JOIN ".TABLE_PREFIX."users u ON (u.uid=d.uid)
						LEFT JOIN ".TABLE_PREFIX."forums f ON (f.fid=d.fid)
						WHERE ','||d.tids||',' LIKE '%,{$this->tid},%'
						ORDER BY d.dateline DESC
						LIMIT  0, 20
					");
                    break;
                default:
                    $query = $this->db->query("
						SELECT d.*, u.username, f.name AS fname
						FROM ".TABLE_PREFIX."delayedmoderation d
						LEFT JOIN ".TABLE_PREFIX."users u ON (u.uid=d.uid)
						LEFT JOIN ".TABLE_PREFIX."forums f ON (f.fid=d.fid)
						WHERE CONCAT(',',d.tids,',') LIKE '%,{$this->tid},%'
						ORDER BY d.dateline DESC
						LIMIT  0, 20
					");
            }
        }
        while ($delayedmod = $this->db->fetch_array($query)) {
            $delayedmod['dateline'] = $this->time->formatDate("jS M Y, G:i", $delayedmod['delaydateline']);
            $delayedmod['profilelink'] = $this->user->build_profile_link($delayedmod['username'], $delayedmod['uid']);
            $delayedmod['action'] = $actions[$delayedmod['type']];
            $info = '';
            if (strpos($delayedmod['tids'], ',') === false) {
                $delayed_thread = $this->thread->get_thread($delayedmod['tids']);
                $info .= "<strong>{$lang->thread}</strong> <a href=\"".get_thread_link($delayedmod['tids'])."\">".htmlspecialchars_uni($delayed_thread['subject'])."</a><br />";
            } else {
                $info .= "<strong>{$lang->thread}</strong> {$lang->multiple_threads}<br />";
            }

            if ($delayedmod['fname']) {
                $info .= "<strong>{$lang->forum}</strong> <a href=\"".$this->forum->get_forum_link($delayedmod['fid'])."\">".htmlspecialchars_uni($delayedmod['fname'])."</a><br />";
            }
            $delayedmod['inputs'] = my_unserialize($delayedmod['inputs']);

            if ($delayedmod['type'] == 'move') {
                $info .= "<strong>{$lang->new_forum}</strong>  <a href=\"".$this->forum->get_forum_link($delayedmod['inputs']['new_forum'])."\">".htmlspecialchars_uni($this->bb->forum_cache[$delayedmod['inputs']['new_forum']]['name'])."</a><br />";
                if ($delayedmod['inputs']['method'] == "redirect") {
                    if ((int)$delayedmod['inputs']['redirect_expire'] == 0) {
                        $redirect_expire_bit = $lang->redirect_forever;
                    } else {
                        $redirect_expire_bit = (int)$delayedmod['inputs']['redirect_expire']." {$lang->days}";
                    }
                    $info .= "<strong>{$lang->leave_redirect_for}</strong> {$redirect_expire_bit}<br />";
                }
            } elseif ($delayedmod['type'] == 'merge') {
                $info .= "<strong>{$lang->new_subject}</strong> ".htmlspecialchars_uni($delayedmod['inputs']['subject'])."<br />";
                $info .= "<strong>{$lang->thread_to_merge_with}</strong> <a href=\"".htmlspecialchars_uni($delayedmod['inputs']['threadurl'])."\">".htmlspecialchars_uni($delayedmod['inputs']['threadurl'])."</a><br />";
            }

            eval("\$delayedmods .= \"".$templates->get("moderation_delayedmodaction_notes")."\";");
            $trow = alt_trow();
        }
        if (!$delayedmods) {
            $cols = 5;
            eval("\$delayedmods = \"".$templates->get("moderation_delayedmodaction_error")."\";");
        }

        $url = '';
        if ($this->bb->getInput('tid', 0)) {
            $lang->threads = $lang->thread;
            $threads = "<a href=\"".get_thread_link($this->tid)."\">{$thread['subject']}</a>";
            eval("\$moderation_delayedmoderation_merge = \"".$templates->get("moderation_delayedmoderation_merge")."\";");
        } else {
            if ($this->bb->getInput('inlinetype', '') === 'search') {
                $tids = $this->getids($this->bb->getInput('searchid', 0), 'search');
                $url = htmlspecialchars_uni($this->bb->getInput('url', ''));
            } else {
                $tids = $this->getids($this->fid, "forum");
            }
            if (count($tids) < 1) {
                $this->bb->error($lang->error_inline_nothreadsselected);
            }

            $threads = $lang->sprintf($lang->threads_selected, count($tids));
            $moderation_delayedmoderation_merge = '';
        }
        $this->bb->input['redirect_expire'] = $this->bb->getInput('redirect_expire', '');
        eval("\$moderation_delayedmoderation_move = \"".$templates->get("moderation_delayedmoderation_move")."\";");

        // Generate form elements for date form
        $dateday = '';
        for ($day = 1; $day <= 31; ++$day) {
            $selected = '';
            if ($this->bb->getInput('date_day', 0) == $day) {
                $selected = ' selected="selected"';
            }
            eval('$dateday .= "'.$templates->get('moderation_delayedmoderation_date_day').'";');
        }

        $datemonth = [];
        foreach (['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'] as $month) {
            $datemonth[$month] = '';
            if ($this->bb->getInput('date_month', 0) == (int)$month) {
                $datemonth[$month] = ' selected="selected"';
            }
        }


        eval('$datemonth = "'.$templates->get('moderation_delayedmoderation_date_month').'";');

        $dateyear = gmdate('Y', TIME_NOW);
        $datetime = gmdate('g:i a', TIME_NOW);

        $this->plugins->runHooks("moderation_delayedmoderation");

        eval("\$delayedmoderation = \"".$templates->get("moderation_delayedmoderation")."\";");
        output_page($delayedmoderation);
        break;
    // Open or close a thread
    case "openclosethread":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canopenclosethreads")) {
            return $this->bb->error_no_permission();
        }

        if ($thread['closed'] == 1) {
            $openclose = $lang->opened;
            $redirect = $lang->redirect_openthread;
            $this->moderation->open_threads($this->tid);
        } else {
            $openclose = $lang->closed;
            $redirect = $lang->redirect_closethread;
            $this->moderation->close_threads($this->tid);
        }

        $lang->mod_process = $lang->sprintf($lang->mod_process, $openclose);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->mod_process);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $redirect);
        break;

    // Stick or unstick that post to the top bab!
    case "stick":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canstickunstickthreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_stick");

        if ($thread['sticky'] == 1) {
            $stuckunstuck = $lang->unstuck;
            $redirect = $lang->redirect_unstickthread;
            $this->moderation->unstick_threads($this->tid);
        } else {
            $stuckunstuck = $lang->stuck;
            $redirect = $lang->redirect_stickthread;
            $this->moderation->stick_threads($this->tid);
        }

        $lang->mod_process = $lang->sprintf($lang->mod_process, $stuckunstuck);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->mod_process);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $redirect);
        break;

    // Remove redirects to a specific thread
    case "removeredirects":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_removeredirects");

        $this->moderation->remove_redirects($this->tid);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->redirects_removed);
        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_redirectsremoved);
        break;

    // Delete thread confirmation page
    case "deletethread":
        $this->bb->add_breadcrumb($lang->nav_deletethread);

        if (!$this->user->is_moderator($this->fid, "candeletethreads")) {
            if ($permissions['candeletethreads'] != 1 || $this->user->uid != $thread['uid']) {
                return $this->bb->error_no_permission();
            }
        }

        $this->plugins->runHooks("moderation_deletethread");

        eval("\$deletethread = \"".$templates->get("moderation_deletethread")."\";");
        output_page($deletethread);
        break;

    // Delete the actual thread here
    case "do_deletethread":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "candeletethreads")) {
            if ($permissions['candeletethreads'] != 1 || $this->user->uid != $thread['uid']) {
                return $this->bb->error_no_permission();
            }
        }

        $this->plugins->runHooks("moderation_do_deletethread");

        // Log the subject of the deleted thread
        $this->modlogdata['thread_subject'] = $thread['subject'];

        $thread['subject'] = $this->db->escape_string($thread['subject']);
        $lang->thread_deleted = $lang->sprintf($lang->thread_deleted, $thread['subject']);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_deleted);

        $this->moderation->delete_thread($this->tid);

        $this->moderation->mark_reports($this->tid, "thread");
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_threaddeleted);
        break;

    // Delete the poll from a thread confirmation page
    case "deletepoll":
        $this->bb->add_breadcrumb($lang->nav_deletepoll);

        if (!$this->user->is_moderator($this->fid, "canmanagepolls")) {
            if ($permissions['candeletethreads'] != 1 || $this->user->uid != $thread['uid']) {
                return $this->bb->error_no_permission();
            }
        }

        $this->plugins->runHooks("moderation_deletepoll");

        $query = $this->db->simple_select("polls", "pid", "tid='$this->tid'");
        $poll = $this->db->fetch_array($query);
        if (!$poll) {
            $this->bb->error($lang->error_invalidpoll);
        }

        eval("\$deletepoll = \"".$templates->get("moderation_deletepoll")."\";");
        output_page($deletepoll);
        break;

    // Delete the actual poll here!
    case "do_deletepoll":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!isset($this->bb->input['delete'])) {
            $this->bb->error($lang->redirect_pollnotdeleted);
        }
        if (!$this->user->is_moderator($this->fid, "canmanagepolls")) {
            if ($permissions['candeletethreads'] != 1 || $this->user->uid != $thread['uid']) {
                return $this->bb->error_no_permission();
            }
        }
        $query = $this->db->simple_select("polls", "pid", "tid = $this->tid");
        $poll = $this->db->fetch_array($query);
        if (!$poll) {
            $this->bb->error($lang->error_invalidpoll);
        }

        $this->plugins->runHooks("moderation_do_deletepoll");

        $lang->poll_deleted = $lang->sprintf($lang->poll_deleted, $thread['subject']);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->poll_deleted);

        $this->moderation->delete_poll($poll['pid']);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_polldeleted);
        break;

    // Approve a thread
    case "approvethread":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canapproveunapprovethreads")) {
            return $this->bb->error_no_permission();
        }
        $thread = $this->thread->get_thread($this->tid);

        $this->plugins->runHooks("moderation_approvethread");

        $lang->thread_approved = $lang->sprintf($lang->thread_approved, $thread['subject']);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_approved);

        $this->moderation->approve_threads($this->tid, $this->fid);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_threadapproved);
        break;

    // Unapprove a thread
    case "unapprovethread":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canapproveunapprovethreads")) {
            return $this->bb->error_no_permission();
        }
        $thread = $this->thread->get_thread($this->tid);

        $this->plugins->runHooks("moderation_unapprovethread");

        $lang->thread_unapproved = $lang->sprintf($lang->thread_unapproved, $thread['subject']);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_unapproved);

        $this->moderation->unapprove_threads($this->tid);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_threadunapproved);
        break;

    // Restore a thread
    case "restorethread":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canrestorethreads")) {
            return $this->bb->error_no_permission();
        }
        $thread = $this->thread->get_thread($this->tid);

        $this->plugins->runHooks("moderation_restorethread");

        $lang->thread_restored = $lang->sprintf($lang->thread_restored, $thread['subject']);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_restored);

        $this->moderation->restore_threads($this->tid);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_threadrestored);
        break;

    // Soft delete a thread
    case "softdeletethread":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "cansoftdeletethreads")) {
            return $this->bb->error_no_permission();
        }
        $thread = $this->thread->get_thread($this->tid);

        $this->plugins->runHooks("moderation_softdeletethread");

        $lang->thread_soft_deleted = $lang->sprintf($lang->thread_soft_deleted, $thread['subject']);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_soft_deleted);

        $this->moderation->soft_delete_threads($this->tid);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_threadsoftdeleted);
        break;

    // Move a thread
    case "move":
        $this->bb->add_breadcrumb($lang->nav_move);
        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_move");

        $forumselect = $this->forum->build_forum_jump("", '', 1, '', 0, true, '', "moveto");
        eval("\$movethread = \"".$templates->get("moderation_move")."\";");
        output_page($movethread);
        break;

    // Let's get this thing moving!
    case "do_move":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $moveto = $this->bb->getInput('moveto', 0);
        $method = $this->bb->getInput('method', '');

        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }
        // Check if user has moderator permission to move to destination
        if (!$this->user->is_moderator($moveto, "canmanagethreads") && !$this->user->is_moderator($this->fid, "canmovetononmodforum")) {
            return $this->bb->error_no_permission();
        }
        $newperms = $this->forum->forum_permissions($moveto);
        if ($newperms['canview'] == 0 && !$this->user->is_moderator($this->fid, "canmovetononmodforum")) {
            return $this->bb->error_no_permission();
        }

        $newforum = $this->forum->get_forum($moveto);
        if (!$newforum || $newforum['type'] != "f" || $newforum['type'] == "f" && $newforum['linkto'] != '') {
            $this->bb->error($lang->error_invalidforum);
        }
        if ($method != "copy" && $thread['fid'] == $moveto) {
            $this->bb->error($lang->error_movetosameforum);
        }

        $this->plugins->runHooks('moderation_do_move');

        $expire = 0;
        if ($this->bb->getInput('redirect_expire', 0) > 0) {
            $expire = TIME_NOW + ($this->bb->getInput('redirect_expire', 0) * 86400);
        }

        $the_thread = $this->tid;

        $newtid = $this->moderation->move_thread($this->tid, $moveto, $method, $expire);

        switch ($method) {
            case "copy":
                $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_copied);
                break;
            default:
            case "move":
            case "redirect":
                $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_moved);
                break;
        }

        return $this->moderation_redirect(get_thread_link($newtid), $lang->redirect_threadmoved);
        break;

    // Viewing thread notes
    case "viewthreadnotes":
        if (!$this->user->is_moderator($this->fid)) {
            return $this->bb->error_no_permission();
        }

        // Make sure we are looking at a real thread here.
        if (!$thread) {
            $this->bb->error($lang->error_nomember);
        }

        $this->plugins->runHooks('moderation_viewthreadnotes');

        $lang->view_notes_for = $lang->sprintf($lang->view_notes_for, $thread['subject']);

        $thread['notes'] = nl2br(htmlspecialchars_uni($thread['notes']));

        eval("\$viewthreadnotes = \"".$templates->get("moderation_viewthreadnotes", 1, 0)."\";");
        echo $viewthreadnotes;
        break;

    // Let's look up the ip address of a PM
    case "getpmip":
        if ($this->bb->pmid <= 0) {
            $this->bb->error($lang->error_invalidpm);
        }
        $this->bb->add_breadcrumb($lang->nav_pms, "private.php");
        $pm['subject'] = htmlspecialchars_uni($parser->parse_badwords($pm['subject']));
        $this->bb->add_breadcrumb($pm['subject'], "private/read?pmid={$this->bb->pmid}");
        $this->bb->add_breadcrumb($lang->nav_getpmip);
        if (!$this->bb->usergroup['issupermod']) {
            return $this->bb->error_no_permission();
        }

        $hostname = @gethostbyaddr($pm['ipaddress']);
        if (!$hostname || $hostname == $pm['ipaddress']) {
            $hostname = $lang->resolve_fail;
        }

        $name = $this->db->fetch_field($this->db->simple_select('users', 'username', "uid = {$pm['fromid']}"), 'username');
        $username = $this->user->build_profile_link($name, $pm['fromid']);

        // Moderator options
        $modoptions = "";
        if ($this->bb->usergroup['canmodcp'] == 1 && $this->bb->usergroup['canuseipsearch'] == 1) {
            $ipaddress = $pm['ipaddress'];
            eval("\$modoptions = \"".$templates->get("moderation_getip_modoptions")."\";");
        }

        $this->plugins->runHooks('moderation_getpmip');

        eval("\$getpmip = \"".$templates->get("moderation_getpmip")."\";");
        output_page($getpmip);
        break;

    // Merge threads
    case "merge":
        $this->bb->add_breadcrumb($lang->nav_merge);
        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_merge");

        eval("\$merge = \"".$templates->get("moderation_merge")."\";");
        output_page($merge);
        break;

    // Let's get those threads together baby! (Merge threads)
    case "do_merge":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_do_merge");

        // explode at # sign in a url (indicates a name reference) and reassign to the url
        $realurl = explode("#", $this->bb->getInput('threadurl', ''));
        $this->bb->input['threadurl'] = $realurl[0];

        // Are we using an SEO URL?
        if (substr($this->bb->input['threadurl'], -4) == "html") {
            // Get thread to merge's tid the SEO way
            preg_match("#thread-([0-9]+)?#i", $this->bb->input['threadurl'], $threadmatch);
            preg_match("#post-([0-9]+)?#i", $this->bb->input['threadurl'], $postmatch);

            if ($threadmatch[1]) {
                $parameters['tid'] = $threadmatch[1];
            }

            if ($postmatch[1]) {
                $parameters['pid'] = $postmatch[1];
            }
        } else {
            // Get thread to merge's tid the normal way
            $splitloc = explode(".php", $this->bb->input['threadurl']);
            $temp = explode("&", my_substr($splitloc[1], 1));

            if (!empty($temp)) {
                for ($i = 0; $i < count($temp); $i++) {
                    $temp2 = explode("=", $temp[$i], 2);
                    $parameters[$temp2[0]] = $temp2[1];
                }
            } else {
                $temp2 = explode("=", $splitloc[1], 2);
                $parameters[$temp2[0]] = $temp2[1];
            }
        }

        if (!empty($parameters['pid']) && empty($parameters['tid'])) {
            $post = get_post($parameters['pid']);
            $mergetid = (int)$post['tid'];
        } elseif (!empty($parameters['tid'])) {
            $mergetid = (int)$parameters['tid'];
        } else {
            $mergetid = 0;
        }
        $mergethread = $this->thread->get_thread($mergetid);
        if (!$mergethread) {
            $this->bb->error($lang->error_badmergeurl);
        }
        if ($mergetid == $this->tid) { // sanity check
            $this->bb->error($lang->error_mergewithself);
        }
        if (!$this->user->is_moderator($mergethread['fid'], "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }
        if (isset($this->bb->input['subject'])) {
            $subject = $this->bb->getInput('subject', '');
        } else {
            $subject = $thread['subject'];
        }

        $this->moderation->merge_threads($mergetid, $this->tid, $subject);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_merged);

        return $this->moderation_redirect(get_thread_link($this->tid), $lang->redirect_threadsmerged);
        break;

    // Divorce the posts in this thread (Split!)
    case "split":
        $this->bb->add_breadcrumb($lang->nav_split);
        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }
        $query = $this->db->query("
			SELECT p.*, u.*
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."users u ON (p.uid=u.uid)
			WHERE tid='$this->tid'
			ORDER BY dateline ASC
		");
        $numposts = $this->db->num_rows($query);
        if ($numposts <= 1) {
            $this->bb->error($lang->error_cantsplitonepost);
        }

        $altbg = "trow1";
        $posts = '';
        while ($post = $this->db->fetch_array($query)) {
            $postdate = $this->time->formatDate('relative', $post['dateline']);

            $parser_options = [
                "allow_html" => $forum['allowhtml'],
                "allow_mycode" => $forum['allowmycode'],
                "allow_smilies" => $forum['allowsmilies'],
                "allow_imgcode" => $forum['allowimgcode'],
                "allow_videocode" => $forum['allowvideocode'],
                "filter_badwords" => 1
            ];
            if ($post['smilieoff'] == 1) {
                $parser_options['allow_smilies'] = 0;
            }

            $message = $parser->parse_message($post['message'], $parser_options);
            eval("\$posts .= \"".$templates->get("moderation_split_post")."\";");
            $altbg = alt_trow();
        }
        $forumselect = $this->forum->build_forum_jump("", $this->fid, 1, '', 0, true, '', "moveto");

        $this->plugins->runHooks("moderation_split");

        eval("\$split = \"".$templates->get("moderation_split")."\";");
        output_page($split);
        break;

    // Let's break them up buddy! (Do the split)
    case "do_split":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_do_split");

        $this->bb->input['splitpost'] = $this->bb->getInput('splitpost', []);
        if (empty($this->bb->input['splitpost'])) {
            $this->bb->error($lang->error_nosplitposts);
        }
        $query = $this->db->simple_select("posts", "COUNT(*) AS totalposts", "tid='{$this->tid}'");
        $count = $this->db->fetch_array($query);

        if ($count['totalposts'] == 1) {
            $this->bb->error($lang->error_cantsplitonepost);
        }

        if ($count['totalposts'] == count($this->bb->input['splitpost'])) {
            $this->bb->error($lang->error_cantsplitall);
        }

        if (!empty($this->bb->input['moveto'])) {
            $moveto = $this->bb->getInput('moveto', 0);
        } else {
            $moveto = $this->fid;
        }

        $newforum = $this->forum->get_forum($moveto);
        if (!$newforum || $newforum['type'] != "f" || $newforum['type'] == "f" && $newforum['linkto'] != '') {
            $this->bb->error($lang->error_invalidforum);
        }

        $pids = [];

        // move the selected posts over
        $query = $this->db->simple_select("posts", "pid", "tid='$this->tid'");
        while ($post = $this->db->fetch_array($query)) {
            if (isset($this->bb->input['splitpost'][$post['pid']]) && $this->bb->input['splitpost'][$post['pid']] == 1) {
                $pids[] = $post['pid'];
            }
            $this->moderation->mark_reports($post['pid'], "post");
        }

        $newtid = $this->moderation->split_posts($pids, $this->tid, $moveto, $this->bb->getInput('newsubject', ''));

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->thread_split);

        return $this->moderation_redirect(get_thread_link($newtid), $lang->redirect_threadsplit);
        break;

    // Delete Thread Subscriptions
    case "removesubscriptions":
        if (!$this->user->is_moderator($this->fid, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $this->plugins->runHooks("moderation_removesubscriptions");

        $this->moderation->remove_thread_subscriptions($this->tid, true);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->removed_subscriptions);

        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_removed_subscriptions);
        break;

    // Open threads - Inline moderation
    case "multiopenthreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canopenclosethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canopenclosethreads')) {
                return $this->bb->error_no_permission();
            }
        }

        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->open_threads($threads);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_opened_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadsopened);
        break;

    // Approve threads - Inline moderation
    case "multiapprovethreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canapproveunapprovethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canapproveunapprovethreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->approve_threads($threads, $this->fid);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_approved_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        $this->cache->update_stats();
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadsapproved);
        break;

    // Unapprove threads - Inline moderation
    case "multiunapprovethreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canapproveunapprovethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canapproveunapprovethreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->unapprove_threads($threads, $this->fid);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_unapproved_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        $this->cache->update_stats();
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadsunapproved);
        break;

    // Restore threads - Inline moderation
    case "multirestorethreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canrestorethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canrestorethreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->restore_threads($threads);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_restored_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        $this->cache->update_stats();
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadsrestored);
        break;

    // Soft delete threads - Inline moderation
    case "multisoftdeletethreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'cansoftdeletethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'cansoftdeletethreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->soft_delete_threads($threads);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_soft_deleted_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        $this->cache->update_stats();
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadssoftdeleted);
        break;

    // Stick threads - Inline moderation
    case "multistickthreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canstickunstickthreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canstickunstickthreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->stick_threads($threads);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_stuck_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadsstuck);
        break;

    // Unstick threads - Inline moderaton
    case "multiunstickthreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canstickunstickthreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canstickunstickthreads')) {
                return $this->bb->error_no_permission();
            }
        }
        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }

        $this->moderation->unstick_threads($threads);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_unstuck_threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        return $this->moderation_redirect($this->forum->get_forum_link($this->fid), $lang->redirect_inline_threadsunstuck);
        break;

    // Move threads - Inline moderation
    case "multimovethreads":
        $this->bb->add_breadcrumb($lang->nav_multi_movethreads);

        if (!empty($this->bb->input['searchid'])) {
            // From search page
            $threads = $this->getids($this->bb->getInput('searchid', ''), 'search');
            if (!$this->is_moderator_by_tids($threads, 'canmanagethreads')) {
                return $this->bb->error_no_permission();
            }
        } else {
            $threads = $this->getids($this->fid, 'forum');
            if (!$this->user->is_moderator($this->fid, 'canmanagethreads')) {
                return $this->bb->error_no_permission();
            }
        }

        if (count($threads) < 1) {
            $this->bb->error($lang->error_inline_nothreadsselected);
        }
        $inlineids = implode("|", $threads);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->fid, 'forum');
        }
        $forumselect = $this->forum->build_forum_jump("", '', 1, '', 0, true, '', "moveto");
        $return_url = htmlspecialchars_uni($this->bb->getInput('url', ''));
        eval("\$movethread = \"".$templates->get("moderation_inline_movethreads")."\";");
        output_page();
        break;

    // Actually move the threads in Inline moderation
    case "do_multimovethreads":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $moveto = $this->bb->getInput('moveto', 0);
        $threadlist = explode("|", $this->bb->getInput('threads', ''));
        if (!$this->is_moderator_by_tids($threadlist, 'canmanagethreads')) {
            return $this->bb->error_no_permission();
        }
        foreach ($threadlist as $this->tid) {
            $tids[] = (int)$this->tid;
        }
        // Make sure moderator has permission to move to the new forum
        $newperms = $this->forum->forum_permissions($moveto);
        if (($newperms['canview'] == 0 ||
        !$this->user->is_moderator($moveto, 'canmanagethreads')) &&
        !$this->is_moderator_by_tids($tids, 'canmovetononmodforum')) {
            return $this->bb->error_no_permission();
        }

        $newforum = $this->forum->get_forum($moveto);
        if (!$newforum || $newforum['type'] != "f" || ($newforum['type'] == "f" && $newforum['linkto'] != '')) {
            $this->bb->error($lang->error_invalidforum);
        }

        $this->moderation->move_threads($tids, $moveto);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_moved_threads);

        return $this->moderation_redirect($this->forum->get_forum_link($moveto), $lang->redirect_inline_threadsmoved);
        break;

    // Merge posts - Inline moderation
    case "multimergeposts":
        $this->bb->add_breadcrumb($lang->nav_multi_mergeposts);

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }

        // Add the selected posts from other threads
        foreach ($this->bb->cookies as $key => $value) {
            if (strpos($key, "inlinemod_thread") !== false && $key != "inlinemod_thread$tid") {
                $inlinepostlist = explode("|", $this->bb->cookies[$key]);
                foreach ($inlinepostlist as $p) {
                    $p = (int)$p;

                    if (!empty($p)) {
                        $posts[] = (int)$p;
                    }
                }
                // Remove the cookie once its data is retrieved
                $this->bb->my_unsetcookie($key);
            }
        }

        if (empty($posts)) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        if (!$this->is_moderator_by_pids($posts, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $postlist = "";
        $query = $this->db->query("
			SELECT p.*, u.*
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."users u ON (p.uid=u.uid)
			WHERE pid IN (".implode($posts, ",").")
			ORDER BY dateline ASC
		");
        $altbg = "trow1";
        while ($post = $this->db->fetch_array($query)) {
            $postdate = $this->time->formatDate('relative', $post['dateline']);

            $parser_options = [
                "allow_html" => $forum['allowhtml'],
                "allow_mycode" => $forum['allowmycode'],
                "allow_smilies" => $forum['allowsmilies'],
                "allow_imgcode" => $forum['allowimgcode'],
                "allow_videocode" => $forum['allowvideocode'],
                "filter_badwords" => 1
            ];
            if ($post['smilieoff'] == 1) {
                $parser_options['allow_smilies'] = 0;
            }

            $message = $parser->parse_message($post['message'], $parser_options);
            eval("\$postlist .= \"".$templates->get("moderation_mergeposts_post")."\";");
            $altbg = alt_trow();
        }

        $inlineids = implode("|", $posts);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }

        $return_url = htmlspecialchars_uni($this->bb->getInput('url', ''));

        eval("\$multimerge = \"".$templates->get("moderation_inline_mergeposts")."\";");
        output_page($multimerge);
        break;

    // Actually merge the posts - Inline moderation
    case "do_multimergeposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $mergepost = $this->bb->getInput('mergepost', []);
        if (count($mergepost) <= 1) {
            $this->bb->error($this->lang->error_nomergeposts);
        }

        foreach ($mergepost as $this->pid => $yes) {
            $postlist[] = (int)$this->pid;
        }

        if (!$this->is_moderator_by_pids($postlist, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        $masterpid = $this->moderation->merge_posts($postlist, $this->tid, $this->bb->input['sep']);

        $this->moderation->mark_reports($postlist, "posts");
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->merged_selective_posts);
        return $this->moderation_redirect(get_post_link($masterpid)."#pid$masterpid", $lang->redirect_inline_postsmerged);
        break;

    // Split posts - Inline moderation
    case "multisplitposts":
        $this->bb->add_breadcrumb($lang->nav_multi_splitposts);

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }

        if (count($posts) < 1) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        if (!$this->is_moderator_by_pids($posts, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }
        $posts = array_map('intval', $posts);
        $pidin = implode(',', $posts);

        // Make sure that we are not splitting a thread with one post
        // Select number of posts in each thread that the splitted post is in
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $threads = $pcheck = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ((int)$tcheck['count'] <= 1) {
                $this->bb->error($lang->error_cantsplitonepost);
            }
            $threads[] = $pcheck[] = $tcheck['tid']; // Save tids for below
        }

        // Make sure that we are not splitting all posts in the thread
        // The query does not return a row when the count is 0, so find if some threads are missing (i.e. 0 posts after removal)
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin) AND q.pid NOT IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $pcheck2 = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ($tcheck['count'] > 0) {
                $pcheck2[] = $tcheck['tid'];
            }
        }
        if (count($pcheck2) != count($pcheck)) {
            // One or more threads do not have posts after splitting
            $this->bb->error($lang->error_cantsplitall);
        }

        $inlineids = implode("|", $posts);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }
        $forumselect = $this->forum->build_forum_jump("", $this->fid, 1, '', 0, true, '', "moveto");

        $return_url = htmlspecialchars_uni($this->bb->getInput('url', ''));

        eval("\$splitposts = \"".$templates->get("moderation_inline_splitposts")."\";");
        output_page($splitposts);
        break;

    // Actually split the posts - Inline moderation
    case "do_multisplitposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        $plist = [];
        $postlist = explode("|", $this->bb->getInput('posts', ''));
        foreach ($postlist as $this->pid) {
            $this->pid = (int)$this->pid;
            $plist[] = $this->pid;
        }

        if (!$this->is_moderator_by_pids($plist, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        // Ensure all posts exist
        $posts = [];
        if (!empty($plist)) {
            $query = $this->db->simple_select('posts', 'pid', 'pid IN ('.implode(',', $plist).')');
            while ($this->pid = $this->db->fetch_field($query, 'pid')) {
                $posts[] = $this->pid;
            }
        }

        if (empty($posts)) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        $pidin = implode(',', $posts);

        // Make sure that we are not splitting a thread with one post
        // Select number of posts in each thread that the splitted post is in
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $pcheck = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ((int)$tcheck['count'] <= 1) {
                $this->bb->error($lang->error_cantsplitonepost);
            }
            $pcheck[] = $tcheck['tid']; // Save tids for below
        }

        // Make sure that we are not splitting all posts in the thread
        // The query does not return a row when the count is 0, so find if some threads are missing (i.e. 0 posts after removal)
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin) AND q.pid NOT IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $pcheck2 = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ($tcheck['count'] > 0) {
                $pcheck2[] = $tcheck['tid'];
            }
        }
        if (count($pcheck2) != count($pcheck)) {
            // One or more threads do not have posts after splitting
            $this->bb->error($lang->error_cantsplitall);
        }

        if (isset($this->bb->input['moveto'])) {
            $moveto = $this->bb->getInput('moveto', 0);
        } else {
            $moveto = $this->fid;
        }

        $newforum = $this->forum->get_forum($moveto);
        if (!$newforum || $newforum['type'] != "f" || $newforum['type'] == "f" && $newforum['linkto'] != '') {
            $this->bb->error($lang->error_invalidforum);
        }

        $newsubject = $this->bb->getInput('newsubject', '', true);
        $newtid = $this->moderation->split_posts($posts, $this->tid, $moveto, $newsubject);

        $pid_list = implode(', ', $posts);
        $lang->split_selective_posts = $lang->sprintf($lang->split_selective_posts, $pid_list, $newtid);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->split_selective_posts);

        return $this->moderation_redirect(get_thread_link($newtid), $lang->redirect_threadsplit);
        break;

    // Move posts - Inline moderation
    case "multimoveposts":
        $this->bb->add_breadcrumb($lang->nav_multi_moveposts);

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }

        if (count($posts) < 1) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        if (!$this->is_moderator_by_pids($posts, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }
        $posts = array_map('intval', $posts);
        $pidin = implode(',', $posts);

        // Make sure that we are not moving posts in a thread with one post
        // Select number of posts in each thread that the moved post is in
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $threads = $pcheck = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ((int)$tcheck['count'] <= 1) {
                $this->bb->error($lang->error_cantsplitonepost);
            }
            $threads[] = $pcheck[] = $tcheck['tid']; // Save tids for below
        }

        // Make sure that we are not moving all posts in the thread
        // The query does not return a row when the count is 0, so find if some threads are missing (i.e. 0 posts after removal)
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin) AND q.pid NOT IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $pcheck2 = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ($tcheck['count'] > 0) {
                $pcheck2[] = $tcheck['tid'];
            }
        }
        if (count($pcheck2) != count($pcheck)) {
            // One or more threads do not have posts after splitting
            $this->bb->error($lang->error_cantmoveall);
        }

        $inlineids = implode("|", $posts);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }
        $forumselect = $this->forum->build_forum_jump("", $this->fid, 1, '', 0, true, '', "moveto");
        eval("\$moveposts = \"".$templates->get("moderation_inline_moveposts")."\";");
        output_page($moveposts);
        break;

    // Actually split the posts - Inline moderation
    case "do_multimoveposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        // explode at # sign in a url (indicates a name reference) and reassign to the url
        $realurl = explode("#", $this->bb->getInput('threadurl', ''));
        $this->bb->input['threadurl'] = $realurl[0];

        // Are we using an SEO URL?
        if (substr($this->bb->input['threadurl'], -4) == "html") {
            // Get thread to merge's tid the SEO way
            preg_match("#thread-([0-9]+)?#i", $this->bb->input['threadurl'], $threadmatch);
            preg_match("#post-([0-9]+)?#i", $this->bb->input['threadurl'], $postmatch);

            if ($threadmatch[1]) {
                $parameters['tid'] = $threadmatch[1];
            }

            if ($postmatch[1]) {
                $parameters['pid'] = $postmatch[1];
            }
        } else {
            // Get thread to merge's tid the normal way
            $splitloc = explode(".php", $this->bb->input['threadurl']);
            $temp = explode("&", my_substr($splitloc[1], 1));

            if (!empty($temp)) {
                for ($i = 0; $i < count($temp); $i++) {
                    $temp2 = explode("=", $temp[$i], 2);
                    $parameters[$temp2[0]] = $temp2[1];
                }
            } else {
                $temp2 = explode("=", $splitloc[1], 2);
                $parameters[$temp2[0]] = $temp2[1];
            }
        }

        if (!empty($parameters['pid']) && empty($parameters['tid'])) {
            $query = $this->db->simple_select("posts", "tid", "pid='".(int)$parameters['pid']."'");
            $post = $this->db->fetch_array($query);
            $newtid = $post['tid'];
        } elseif (!empty($parameters['tid'])) {
            $newtid = $parameters['tid'];
        } else {
            $newtid = 0;
        }
        $newtid = (int)$newtid;
        $newthread = $this->thread->get_thread($newtid);
        if (!$newthread) {
            $this->bb->error($lang->error_badmovepostsurl);
        }
        if ($newtid == $this->tid) { // sanity check
            $this->bb->error($lang->error_movetoself);
        }

        $postlist = explode("|", $this->bb->getInput('posts', ''));
        $plist = [];
        foreach ($postlist as $this->pid) {
            $this->pid = (int)$this->pid;
            $plist[] = $this->pid;
        }

        if (!$this->is_moderator_by_pids($plist, "canmanagethreads")) {
            return $this->bb->error_no_permission();
        }

        // Ensure all posts exist
        $posts = [];
        if (!empty($plist)) {
            $query = $this->db->simple_select('posts', 'pid', 'pid IN ('.implode(',', $plist).')');
            while ($this->pid = $this->db->fetch_field($query, 'pid')) {
                $posts[] = $this->pid;
            }
        }

        if (empty($posts)) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        $pidin = implode(',', $posts);

        // Make sure that we are not moving posts in a thread with one post
        // Select number of posts in each thread that the moved post is in
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $threads = $pcheck = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ((int)$tcheck['count'] <= 1) {
                $this->bb->error($lang->error_cantsplitonepost);
            }
            $threads[] = $pcheck[] = $tcheck['tid']; // Save tids for below
        }

        // Make sure that we are not moving all posts in the thread
        // The query does not return a row when the count is 0, so find if some threads are missing (i.e. 0 posts after removal)
        $query = $this->db->query("
			SELECT DISTINCT p.tid, COUNT(q.pid) as count
			FROM ".TABLE_PREFIX."posts p
			LEFT JOIN ".TABLE_PREFIX."posts q ON (p.tid=q.tid)
			WHERE p.pid IN ($pidin) AND q.pid NOT IN ($pidin)
			GROUP BY p.tid, p.pid
		");
        $pcheck2 = [];
        while ($tcheck = $this->db->fetch_array($query)) {
            if ($tcheck['count'] > 0) {
                $pcheck2[] = $tcheck['tid'];
            }
        }
        if (count($pcheck2) != count($pcheck)) {
            // One or more threads do not have posts after splitting
            $this->bb->error($lang->error_cantmoveall);
        }

        $newtid = $this->moderation->split_posts($posts, $this->tid, $newthread['fid'], $this->db->escape_string($newthread['subject']), $newtid);

        $pid_list = implode(', ', $posts);
        $lang->move_selective_posts = $lang->sprintf($lang->move_selective_posts, $pid_list, $newtid);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->move_selective_posts);

        return $this->moderation_redirect(get_thread_link($newtid), $lang->redirect_moveposts);
        break;

    // Approve posts - Inline moderation
    case "multiapproveposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }
        if (count($posts) < 1) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        if (!$this->is_moderator_by_pids($posts, "canapproveunapproveposts")) {
            return $this->bb->error_no_permission();
        }

        $pids = [];
        foreach ($posts as $this->pid) {
            $pids[] = (int)$this->pid;
        }

        $this->moderation->approve_posts($pids);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_approve_posts);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }
        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_inline_postsapproved);
        break;

    // Unapprove posts - Inline moderation
    case "multiunapproveposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }

        if (count($posts) < 1) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }
        $pids = [];

        if (!$this->is_moderator_by_pids($posts, "canapproveunapproveposts")) {
            return $this->bb->error_no_permission();
        }
        foreach ($posts as $this->pid) {
            $pids[] = (int)$this->pid;
        }

        $this->moderation->unapprove_posts($pids);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_unapprove_posts);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }
        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_inline_postsunapproved);
        break;

    // Restore posts - Inline moderation
    case "multirestoreposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }
        if (count($posts) < 1) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }

        if (!$this->is_moderator_by_pids($posts, "canrestoreposts")) {
            return $this->bb->error_no_permission();
        }

        $pids = [];
        foreach ($posts as $this->pid) {
            $pids[] = (int)$this->pid;
        }

        $this->moderation->restore_posts($pids);

        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_restore_posts);
        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }
        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_inline_postsrestored);
        break;

    // Soft delete posts - Inline moderation
    case "multisoftdeleteposts":
        // Verify incoming POST request
        $this->bb->verify_post_check($this->bb->getInput('my_post_key', ''));

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $posts = $this->getids($this->bb->getInput('searchid', ''), 'search');
        } else {
            $posts = $this->getids($this->tid, 'thread');
        }

        if (count($posts) < 1) {
            $this->bb->error($lang->error_inline_nopostsselected);
        }
        $pids = [];

        if (!$this->is_moderator_by_pids($posts, "cansoftdeleteposts")) {
            return $this->bb->error_no_permission();
        }
        foreach ($posts as $this->pid) {
            $pids[] = (int)$this->pid;
        }

        $this->moderation->soft_delete_posts($pids);
        $this->bblogger->log_moderator_action($this->modlogdata, $lang->multi_soft_delete_posts);

        if ($this->bb->getInput('inlinetype', '') === 'search') {
            $this->clearinline($this->bb->getInput('searchid', 0), 'search');
        } else {
            $this->clearinline($this->tid, 'thread');
        }
        return $this->moderation_redirect(get_thread_link($thread['tid']), $lang->redirect_inline_postssoftdeleted);
        break;
}
