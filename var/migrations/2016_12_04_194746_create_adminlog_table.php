<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAdminlogTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('adminlog', function($table)
        {
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->integer('dateline')->unsigned()->default(0);
            $table->string('module', 50)->default('');
            $table->string('action', 50)->default('');
            $table->text('data', 65535);
            $table->index(['module','action'], 'module');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('adminlog');
    }
}