<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAdminoptionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('adminoptions', function($table)
        {
            $table->integer('uid')->default(0)->primary();
            $table->string('cpstyle', 50)->default('');
            $table->string('cplanguage', 50)->default('');
            $table->boolean('codepress')->default(1);
            $table->text('notes', 65535);
            $table->text('permissions', 65535);
            $table->text('defaultviews', 65535);
            $table->smallInteger('loginattempts')->unsigned()->default(0);
            $table->integer('loginlockoutexpiry')->unsigned()->default(0);
            $table->string('authsecret', 16)->default('');
            $table->string('recovery_codes', 177)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('adminoptions');
    }
}