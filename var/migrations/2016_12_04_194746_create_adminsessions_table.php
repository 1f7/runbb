<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAdminsessionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('adminsessions', function($table)
        {
            $table->string('sid', 32)->default('');
            $table->integer('uid')->unsigned()->default(0);
            $table->string('loginkey', 50)->default('');
            $table->string('ip', 45)->default('0')->index('ip_idx');
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('lastactive')->unsigned()->default(0);
            $table->text('data', 65535);
            $table->string('useragent', 200)->default('');
            $table->boolean('authenticated')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('adminsessions');
    }
}