<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAdminviewsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('adminviews', function($table)
        {
            $table->increments('vid');
            $table->integer('uid')->unsigned()->default(0);
            $table->string('title', 100)->default('');
            $table->string('type', 6)->default('');
            $table->boolean('visibility')->default(0);
            $table->text('fields', 65535);
            $table->text('conditions', 65535);
            $table->text('custom_profile_fields', 65535);
            $table->string('sortby', 20)->default('');
            $table->string('sortorder', 4)->default('');
            $table->smallInteger('perpage')->unsigned()->default(0);
            $table->string('view_type', 6)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('adminviews');
    }
}