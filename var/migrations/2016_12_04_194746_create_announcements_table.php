<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAnnouncementsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('announcements', function($table)
        {
            $table->increments('aid');
            $table->integer('fid')->default(0)->index('fid');
            $table->integer('uid')->unsigned()->default(0);
            $table->string('subject', 120)->default('');
            $table->text('message', 65535);
            $table->integer('startdate')->unsigned()->default(0);
            $table->integer('enddate')->unsigned()->default(0);
            $table->boolean('allowhtml')->default(0);
            $table->boolean('allowmycode')->default(0);
            $table->boolean('allowsmilies')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('announcements');
    }
}