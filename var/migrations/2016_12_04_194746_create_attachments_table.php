<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAttachmentsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('attachments', function($table)
        {
            $table->increments('aid');
            $table->integer('pid')->unsigned()->default(0);
            $table->string('posthash', 50)->default('');
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->string('filename', 120)->default('');
            $table->string('filetype', 120)->default('');
            $table->integer('filesize')->unsigned()->default(0);
            $table->string('attachname', 120)->default('');
            $table->integer('downloads')->unsigned()->default(0);
            $table->integer('dateuploaded')->unsigned()->default(0);
            $table->boolean('visible')->default(0);
            $table->string('thumbnail', 120)->default('');
            $table->index(['pid','visible'], 'pid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('attachments');
    }
}