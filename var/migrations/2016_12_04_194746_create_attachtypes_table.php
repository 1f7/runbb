<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAttachtypesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('attachtypes', function($table)
        {
            $table->increments('atid');
            $table->string('name', 120)->default('');
            $table->string('mimetype', 120)->default('');
            $table->string('extension', 10)->default('');
            $table->integer('maxsize')->unsigned()->default(0);
            $table->string('icon', 100)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('attachtypes');
    }
}