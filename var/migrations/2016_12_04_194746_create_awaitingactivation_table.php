<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateAwaitingactivationTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('awaitingactivation', function($table)
        {
            $table->increments('aid');
            $table->integer('uid')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->string('code', 100)->default('');
            $table->char('type', 1)->default('');
            $table->boolean('validated')->default(0);
            $table->string('misc')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('awaitingactivation');
    }
}