<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateBadwordsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('badwords', function($table)
        {
            $table->increments('bid');
            $table->string('badword', 100)->default('');
            $table->string('replacement', 100)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('badwords');
    }
}