<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateBanfiltersTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('banfilters', function($table)
        {
            $table->increments('fid');
            $table->string('filter', 200)->default('');
            $table->boolean('type')->default(0)->index('type');
            $table->integer('lastuse')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('banfilters');
    }
}