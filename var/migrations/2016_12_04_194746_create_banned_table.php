<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateBannedTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('banned', function($table)
        {
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->integer('gid')->unsigned()->default(0);
            $table->integer('oldgroup')->unsigned()->default(0);
            $table->text('oldadditionalgroups', 65535);
            $table->integer('olddisplaygroup')->unsigned()->default(0);
            $table->integer('admin')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0)->index('dateline');
            $table->string('bantime', 50)->default('');
            $table->integer('lifted')->unsigned()->default(0);
            $table->string('reason')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('banned');
    }
}