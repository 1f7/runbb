<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateBuddyrequestsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('buddyrequests', function($table)
        {
            $table->increments('id');
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->integer('touid')->unsigned()->default(0)->index('touid');
            $table->integer('date')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('buddyrequests');
    }
}