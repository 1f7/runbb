<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateCalendarpermissionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('calendarpermissions', function($table)
        {
            $table->integer('cid')->unsigned()->default(0);
            $table->integer('gid')->unsigned()->default(0);
            $table->boolean('canviewcalendar')->default(0);
            $table->boolean('canaddevents')->default(0);
            $table->boolean('canbypasseventmod')->default(0);
            $table->boolean('canmoderateevents')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('calendarpermissions');
    }
}