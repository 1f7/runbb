<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateCalendarsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('calendars', function($table)
        {
            $table->increments('cid');
            $table->string('name', 100)->default('');
            $table->smallInteger('disporder')->unsigned()->default(0);
            $table->boolean('startofweek')->default(0);
            $table->boolean('showbirthdays')->default(0);
            $table->smallInteger('eventlimit')->unsigned()->default(0);
            $table->boolean('moderation')->default(0);
            $table->boolean('allowhtml')->default(0);
            $table->boolean('allowmycode')->default(0);
            $table->boolean('allowimgcode')->default(0);
            $table->boolean('allowvideocode')->default(0);
            $table->boolean('allowsmilies')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('calendars');
    }
}