<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateCaptchaTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('captcha', function($table)
        {
            $table->string('imagehash', 32)->default('')->index('imagehash');
            $table->string('imagestring', 8)->default('');
            $table->integer('dateline')->unsigned()->default(0)->index('dateline');
            $table->boolean('used')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('captcha');
    }
}