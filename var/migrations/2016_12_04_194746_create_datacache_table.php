<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateDatacacheTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('datacache', function($table)
        {
            $table->increments('id');
            $table->string('title', 50)->default('');
            $table->text('cache', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('datacache');
    }
}