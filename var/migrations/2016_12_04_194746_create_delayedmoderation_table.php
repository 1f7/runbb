<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateDelayedmoderationTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('delayedmoderation', function($table)
        {
            $table->increments('did');
            $table->string('type', 30)->default('');
            $table->integer('delaydateline')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->smallInteger('fid')->unsigned()->default(0);
            $table->text('tids', 65535);
            $table->integer('dateline')->unsigned()->default(0);
            $table->text('inputs', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('delayedmoderation');
    }
}