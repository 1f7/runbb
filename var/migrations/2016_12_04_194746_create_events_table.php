<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateEventsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('events', function($table)
        {
            $table->increments('eid');
            $table->integer('cid')->unsigned()->default(0)->index('cid');
            $table->integer('uid')->unsigned()->default(0);
            $table->string('name', 120)->default('');
            $table->text('description', 65535);
            $table->boolean('visible')->default(0);
            $table->boolean('private')->default(0)->index('private');
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('starttime')->unsigned()->default(0);
            $table->integer('endtime')->unsigned()->default(0);
            $table->string('timezone', 5)->default('');
            $table->boolean('ignoretimezone')->default(0);
            $table->boolean('usingtime')->default(0);
            $table->text('repeats', 65535);
            $table->index(['starttime','endtime'], 'daterange');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('events');
    }
}