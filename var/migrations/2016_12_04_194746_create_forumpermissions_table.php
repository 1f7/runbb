<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateForumpermissionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('forumpermissions', function($table)
        {
            $table->increments('pid');
            $table->integer('fid')->unsigned()->default(0);
            $table->integer('gid')->unsigned()->default(0);
            $table->boolean('canview')->default(0);
            $table->boolean('canviewthreads')->default(0);
            $table->boolean('canonlyviewownthreads')->default(0);
            $table->boolean('candlattachments')->default(0);
            $table->boolean('canpostthreads')->default(0);
            $table->boolean('canpostreplys')->default(0);
            $table->boolean('canonlyreplyownthreads')->default(0);
            $table->boolean('canpostattachments')->default(0);
            $table->boolean('canratethreads')->default(0);
            $table->boolean('caneditposts')->default(0);
            $table->boolean('candeleteposts')->default(0);
            $table->boolean('candeletethreads')->default(0);
            $table->boolean('caneditattachments')->default(0);
            $table->boolean('modposts')->default(0);
            $table->boolean('modthreads')->default(0);
            $table->boolean('mod_edit_posts')->default(0);
            $table->boolean('modattachments')->default(0);
            $table->boolean('canpostpolls')->default(0);
            $table->boolean('canvotepolls')->default(0);
            $table->boolean('cansearch')->default(0);
            $table->index(['fid','gid'], 'fid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('forumpermissions');
    }
}