<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateForumsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('forums', function($table)
        {
            $table->increments('fid');
            $table->string('name', 120)->default('');
            $table->text('description', 65535);
            $table->string('linkto', 180)->default('');
            $table->char('type', 1)->default('');
            $table->smallInteger('pid')->unsigned()->default(0);
            $table->text('parentlist', 65535);
            $table->smallInteger('disporder')->unsigned()->default(0);
            $table->boolean('active')->default(0);
            $table->boolean('open')->default(0);
            $table->integer('threads')->unsigned()->default(0);
            $table->integer('posts')->unsigned()->default(0);
            $table->integer('lastpost')->unsigned()->default(0);
            $table->string('lastposter', 120)->default('');
            $table->integer('lastposteruid')->unsigned()->default(0);
            $table->integer('lastposttid')->unsigned()->default(0);
            $table->string('lastpostsubject', 120)->default('');
            $table->boolean('allowhtml')->default(0);
            $table->boolean('allowmycode')->default(0);
            $table->boolean('allowsmilies')->default(0);
            $table->boolean('allowimgcode')->default(0);
            $table->boolean('allowvideocode')->default(0);
            $table->boolean('allowpicons')->default(0);
            $table->boolean('allowtratings')->default(0);
            $table->boolean('usepostcounts')->default(0);
            $table->boolean('usethreadcounts')->default(0);
            $table->boolean('requireprefix')->default(0);
            $table->string('password', 50)->default('');
            $table->boolean('showinjump')->default(0);
            $table->smallInteger('style')->unsigned()->default(0);
            $table->boolean('overridestyle')->default(0);
            $table->boolean('rulestype')->default(0);
            $table->string('rulestitle', 200)->default('');
            $table->text('rules', 65535);
            $table->integer('unapprovedthreads')->unsigned()->default(0);
            $table->integer('unapprovedposts')->unsigned()->default(0);
            $table->integer('deletedthreads')->unsigned()->default(0);
            $table->integer('deletedposts')->unsigned()->default(0);
            $table->smallInteger('defaultdatecut')->unsigned()->default(0);
            $table->string('defaultsortby', 10)->default('');
            $table->string('defaultsortorder', 4)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('forums');
    }
}