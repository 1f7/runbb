<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateForumsreadTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('forumsread', function($table)
        {
            $table->integer('fid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0)->index('dateline');
            $table->unique(['fid','uid'], 'fid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('forumsread');
    }
}