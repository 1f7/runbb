<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateForumsubscriptionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('forumsubscriptions', function($table)
        {
            $table->increments('fsid');
            $table->smallInteger('fid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0)->index('uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('forumsubscriptions');
    }
}