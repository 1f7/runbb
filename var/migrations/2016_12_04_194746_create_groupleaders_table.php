<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateGroupleadersTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('groupleaders', function($table)
        {
            $table->increments('lid');
            $table->smallInteger('gid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->boolean('canmanagemembers')->default(0);
            $table->boolean('canmanagerequests')->default(0);
            $table->boolean('caninvitemembers')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('groupleaders');
    }
}