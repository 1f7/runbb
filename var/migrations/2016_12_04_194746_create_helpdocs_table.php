<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateHelpdocsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('helpdocs', function($table)
        {
            $table->increments('hid');
            $table->smallInteger('sid')->unsigned()->default(0);
            $table->string('name', 120)->default('');
            $table->text('description', 65535);
            $table->text('document', 65535);
            $table->boolean('usetranslation')->default(0);
            $table->boolean('enabled')->default(0);
            $table->smallInteger('disporder')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('helpdocs');
    }
}