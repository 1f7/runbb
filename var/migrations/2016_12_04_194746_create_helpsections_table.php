<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateHelpsectionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('helpsections', function($table)
        {
            $table->increments('sid');
            $table->string('name', 120)->default('');
            $table->text('description', 65535);
            $table->boolean('usetranslation')->default(0);
            $table->boolean('enabled')->default(0);
            $table->smallInteger('disporder')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('helpsections');
    }
}