<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateIconsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('icons', function($table)
        {
            $table->increments('iid');
            $table->string('name', 120)->default('');
            $table->string('path', 220)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('icons');
    }
}