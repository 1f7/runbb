<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateJoinrequestsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('joinrequests', function($table)
        {
            $table->increments('rid');
            $table->integer('uid')->unsigned()->default(0);
            $table->smallInteger('gid')->unsigned()->default(0);
            $table->string('reason', 250)->default('');
            $table->integer('dateline')->unsigned()->default(0);
            $table->boolean('invite')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('joinrequests');
    }
}