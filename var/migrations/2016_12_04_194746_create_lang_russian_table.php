<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateLangRussianTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('lang_russian', function($table)
        {
            $table->increments('id');
            $table->boolean('isadmin')->default(0);
            $table->string('section')->default('');
            $table->string('var')->default('');
            $table->text('trans', 65535);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('lang_russian');
    }
}