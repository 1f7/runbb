<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateLanguagesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('languages', function($table)
        {
            $table->increments('id');
            $table->smallInteger('installed')->default(0);
            $table->smallInteger('used')->default(0);
            $table->string('code')->default('');
            $table->string('locale')->default('');
            $table->string('name')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('languages');
    }
}