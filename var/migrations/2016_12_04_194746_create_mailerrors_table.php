<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateMailerrorsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('mailerrors', function($table)
        {
            $table->increments('eid');
            $table->string('subject', 200)->default('');
            $table->text('message', 65535);
            $table->string('toaddress', 150)->default('');
            $table->string('fromaddress', 150)->default('');
            $table->integer('dateline')->unsigned()->default(0);
            $table->text('error', 65535);
            $table->string('smtperror', 200)->default('');
            $table->smallInteger('smtpcode')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('mailerrors');
    }
}