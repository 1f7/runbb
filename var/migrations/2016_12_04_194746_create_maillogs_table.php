<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateMaillogsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('maillogs', function($table)
        {
            $table->increments('mid');
            $table->string('subject', 200)->default('');
            $table->text('message', 65535);
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('fromuid')->unsigned()->default(0);
            $table->string('fromemail', 200)->default('');
            $table->integer('touid')->unsigned()->default(0);
            $table->string('toemail', 200)->default('');
            $table->integer('tid')->unsigned()->default(0);
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->boolean('type')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('maillogs');
    }
}