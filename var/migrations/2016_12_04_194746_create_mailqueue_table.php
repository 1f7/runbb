<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateMailqueueTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('mailqueue', function($table)
        {
            $table->increments('mid');
            $table->string('mailto', 200);
            $table->string('mailfrom', 200);
            $table->string('subject', 200);
            $table->text('message', 65535);
            $table->text('headers', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('mailqueue');
    }
}