<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateMassemailsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('massemails', function($table)
        {
            $table->increments('mid');
            $table->integer('uid')->unsigned()->default(0);
            $table->string('subject', 200)->default('');
            $table->text('message', 65535);
            $table->text('htmlmessage', 65535);
            $table->boolean('type')->default(0);
            $table->boolean('format')->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('senddate')->unsigned()->default(0);
            $table->boolean('status')->default(0);
            $table->integer('sentcount')->unsigned()->default(0);
            $table->integer('totalcount')->unsigned()->default(0);
            $table->text('conditions', 65535);
            $table->smallInteger('perpage')->unsigned()->default(50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('massemails');
    }
}