<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateModeratorlogTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('moderatorlog', function($table)
        {
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->integer('dateline')->unsigned()->default(0);
            $table->smallInteger('fid')->unsigned()->default(0)->index('fid');
            $table->integer('tid')->unsigned()->default(0)->index('tid');
            $table->integer('pid')->unsigned()->default(0);
            $table->text('action', 65535);
            $table->text('data', 65535);
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('moderatorlog');
    }
}