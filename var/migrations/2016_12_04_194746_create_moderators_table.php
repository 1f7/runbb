<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateModeratorsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('moderators', function($table)
        {
            $table->increments('mid');
            $table->smallInteger('fid')->unsigned()->default(0);
            $table->integer('id')->unsigned()->default(0);
            $table->boolean('isgroup')->default(0);
            $table->boolean('caneditposts')->default(0);
            $table->boolean('cansoftdeleteposts')->default(0);
            $table->boolean('canrestoreposts')->default(0);
            $table->boolean('candeleteposts')->default(0);
            $table->boolean('cansoftdeletethreads')->default(0);
            $table->boolean('canrestorethreads')->default(0);
            $table->boolean('candeletethreads')->default(0);
            $table->boolean('canviewips')->default(0);
            $table->boolean('canviewunapprove')->default(0);
            $table->boolean('canviewdeleted')->default(0);
            $table->boolean('canopenclosethreads')->default(0);
            $table->boolean('canstickunstickthreads')->default(0);
            $table->boolean('canapproveunapprovethreads')->default(0);
            $table->boolean('canapproveunapproveposts')->default(0);
            $table->boolean('canapproveunapproveattachs')->default(0);
            $table->boolean('canmanagethreads')->default(0);
            $table->boolean('canmanagepolls')->default(0);
            $table->boolean('canpostclosedthreads')->default(0);
            $table->boolean('canmovetononmodforum')->default(0);
            $table->boolean('canusecustomtools')->default(0);
            $table->boolean('canmanageannouncements')->default(0);
            $table->boolean('canmanagereportedposts')->default(0);
            $table->boolean('canviewmodlog')->default(0);
            $table->index(['id','fid'], 'uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('moderators');
    }
}