<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateModtoolsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('modtools', function($table)
        {
            $table->increments('tid');
            $table->string('name', 200);
            $table->text('description', 65535);
            $table->text('forums', 65535);
            $table->text('groups', 65535);
            $table->char('type', 1)->default('');
            $table->text('postoptions', 65535);
            $table->text('threadoptions', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('modtools');
    }
}