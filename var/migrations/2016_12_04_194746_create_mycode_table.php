<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateMycodeTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('mycode', function($table)
        {
            $table->increments('cid');
            $table->string('title', 100)->default('');
            $table->text('description', 65535);
            $table->text('regex', 65535);
            $table->text('replacement', 65535);
            $table->boolean('active')->default(0);
            $table->smallInteger('parseorder')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('mycode');
    }
}