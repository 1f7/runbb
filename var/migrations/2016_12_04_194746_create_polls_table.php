<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePollsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('polls', function($table)
        {
            $table->increments('pid');
            $table->integer('tid')->unsigned()->default(0)->index('tid');
            $table->string('question', 200)->default('');
            $table->integer('dateline')->unsigned()->default(0);
            $table->text('options', 65535);
            $table->text('votes', 65535);
            $table->smallInteger('numoptions')->unsigned()->default(0);
            $table->integer('numvotes')->unsigned()->default(0);
            $table->integer('timeout')->unsigned()->default(0);
            $table->boolean('closed')->default(0);
            $table->boolean('multiple')->default(0);
            $table->boolean('public')->default(0);
            $table->smallInteger('maxoptions')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('polls');
    }
}