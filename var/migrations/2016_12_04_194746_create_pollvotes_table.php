<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePollvotesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('pollvotes', function($table)
        {
            $table->increments('vid');
            $table->integer('pid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->smallInteger('voteoption')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->index(['pid','uid'], 'pid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('pollvotes');
    }
}