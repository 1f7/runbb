<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePostsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('posts', function($table)
        {
            $table->increments('pid');
            $table->integer('tid')->unsigned()->default(0);
            $table->integer('replyto')->unsigned()->default(0);
            $table->smallInteger('fid')->unsigned()->default(0);
            $table->string('subject', 120)->default('');
            $table->smallInteger('icon')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->string('username', 80)->default('');
            $table->integer('dateline')->unsigned()->default(0)->index('dateline');
            $table->text('message', 65535);//->index('message');
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->boolean('includesig')->default(0);
            $table->boolean('smilieoff')->default(0);
            $table->integer('edituid')->unsigned()->default(0);
            $table->integer('edittime')->unsigned()->default(0);
            $table->string('editreason', 150)->default('');
            $table->boolean('visible')->default(0)->index('visible');
            $table->index(['tid','uid'], 'tid');
            $table->index(['tid','dateline'], 'tiddate');
        });
        DB::statement('ALTER TABLE '.DB::getTablePrefix().'posts ADD FULLTEXT message(message)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->table('posts', function ($table) {
            $table->dropIndex('message');
        });
        DB::schema()->drop('posts');
    }
}