<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePrivatemessagesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('privatemessages', function($table)
        {
            $table->increments('pmid');
            $table->integer('uid')->unsigned()->default(0);
            $table->integer('toid')->unsigned()->default(0)->index('toid');
            $table->integer('fromid')->unsigned()->default(0);
            $table->text('recipients', 65535);
            $table->smallInteger('folder')->unsigned()->default(1);
            $table->string('subject', 120)->default('');
            $table->smallInteger('icon')->unsigned()->default(0);
            $table->text('message', 65535);
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('deletetime')->unsigned()->default(0);
            $table->boolean('status')->default(0);
            $table->integer('statustime')->unsigned()->default(0);
            $table->boolean('includesig')->default(0);
            $table->boolean('smilieoff')->default(0);
            $table->boolean('receipt')->default(0);
            $table->integer('readtime')->unsigned()->default(0);
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->index(['uid','folder'], 'uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('privatemessages');
    }
}