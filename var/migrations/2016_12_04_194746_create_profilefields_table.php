<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateProfilefieldsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('profilefields', function($table)
        {
            $table->increments('fid');
            $table->string('name', 100)->default('');
            $table->text('description', 65535);
            $table->smallInteger('disporder')->unsigned()->default(0);
            $table->text('type', 65535);
            $table->text('regex', 65535);
            $table->smallInteger('length')->unsigned()->default(0);
            $table->smallInteger('maxlength')->unsigned()->default(0);
            $table->boolean('required')->default(0);
            $table->boolean('registration')->default(0);
            $table->boolean('profile')->default(0);
            $table->boolean('postbit')->default(0);
            $table->text('viewableby', 65535);
            $table->text('editableby', 65535);
            $table->smallInteger('postnum')->unsigned()->default(0);
            $table->boolean('allowhtml')->default(0);
            $table->boolean('allowmycode')->default(0);
            $table->boolean('allowsmilies')->default(0);
            $table->boolean('allowimgcode')->default(0);
            $table->boolean('allowvideocode')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('profilefields');
    }
}