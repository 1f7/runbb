<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePromotionlogsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('promotionlogs', function($table)
        {
            $table->increments('plid');
            $table->integer('pid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->string('oldusergroup', 200)->default('0');
            $table->smallInteger('newusergroup')->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->string('type', 9)->default('primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('promotionlogs');
    }
}