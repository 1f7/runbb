<?php use Illuminate\Database\Capsule\Manager as DB;

class CreatePromotionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('promotions', function($table)
        {
            $table->increments('pid');
            $table->string('title', 120)->default('');
            $table->text('description', 65535);
            $table->boolean('enabled')->default(1);
            $table->boolean('logging')->default(0);
            $table->integer('posts')->unsigned()->default(0);
            $table->char('posttype', 2)->default('');
            $table->integer('threads')->unsigned()->default(0);
            $table->char('threadtype', 2)->default('');
            $table->integer('registered')->unsigned()->default(0);
            $table->string('registeredtype', 20)->default('');
            $table->integer('online')->unsigned()->default(0);
            $table->string('onlinetype', 20)->default('');
            $table->integer('reputations')->default(0);
            $table->char('reputationtype', 2)->default('');
            $table->integer('referrals')->unsigned()->default(0);
            $table->char('referralstype', 2)->default('');
            $table->integer('warnings')->unsigned()->default(0);
            $table->char('warningstype', 2)->default('');
            $table->string('requirements', 200)->default('');
            $table->string('originalusergroup', 120)->default('0');
            $table->smallInteger('newusergroup')->unsigned()->default(0);
            $table->string('usergrouptype', 120)->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('promotions');
    }
}