<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateQuestionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('questions', function($table)
        {
            $table->increments('qid');
            $table->string('question', 200)->default('');
            $table->string('answer', 150)->default('');
            $table->integer('shown')->unsigned()->default(0);
            $table->integer('correct')->unsigned()->default(0);
            $table->integer('incorrect')->unsigned()->default(0);
            $table->boolean('active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('questions');
    }
}