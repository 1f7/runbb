<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateQuestionsessionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('questionsessions', function($table)
        {
            $table->string('sid', 32)->default('')->primary();
            $table->integer('qid')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('questionsessions');
    }
}