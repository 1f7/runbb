<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateReportedcontentTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('reportedcontent', function($table)
        {
            $table->increments('rid');
            $table->integer('id')->unsigned()->default(0);
            $table->integer('id2')->unsigned()->default(0);
            $table->integer('id3')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->boolean('reportstatus')->default(0)->index('reportstatus');
            $table->string('reason', 250)->default('');
            $table->string('type', 50)->default('');
            $table->integer('reports')->unsigned()->default(0);
            $table->text('reporters', 65535);
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('lastreport')->unsigned()->default(0)->index('lastreport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('reportedcontent');
    }
}