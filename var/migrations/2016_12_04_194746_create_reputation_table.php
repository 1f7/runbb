<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateReputationTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('reputation', function($table)
        {
            $table->increments('rid');
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->integer('adduid')->unsigned()->default(0);
            $table->integer('pid')->unsigned()->default(0);
            $table->smallInteger('reputation')->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->text('comments', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('reputation');
    }
}