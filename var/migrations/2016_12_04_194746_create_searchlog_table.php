<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSearchlogTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('searchlog', function($table)
        {
            $table->increments('id');
            $table->string('sid', 32)->default('')->unique('sid');
            $table->integer('uid')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->text('threads', 65535);
            $table->text('posts', 65535);
            $table->string('resulttype', 10)->default('');
            $table->text('querycache', 65535);
            $table->text('keywords', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('searchlog');
    }
}