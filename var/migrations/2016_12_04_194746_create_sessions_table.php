<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSessionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('sessions', function($table)
        {
            $table->string('sid', 32)->default('')->primary();
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->string('ip', 45)->default('0')->index('ip_idx');
            $table->integer('time')->unsigned()->default(0)->index('time');
            $table->string('location', 150)->default('');
            $table->string('useragent', 200)->default('');
            $table->boolean('anonymous')->default(0);
            $table->boolean('nopermission')->default(0);
            $table->integer('location1')->unsigned()->default(0);
            $table->integer('location2')->unsigned()->default(0);
            $table->index(['location1','location2'], 'location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('sessions');
    }
}