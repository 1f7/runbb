<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSettinggroupsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('settinggroups', function($table)
        {
            $table->increments('gid');
            $table->string('name', 100)->default('');
            $table->string('title', 220)->default('');
            $table->text('description', 65535);
            $table->smallInteger('disporder')->unsigned()->default(0);
            $table->boolean('isdefault')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('settinggroups');
    }
}