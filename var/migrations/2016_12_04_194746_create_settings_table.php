<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSettingsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('settings', function($table)
        {
            $table->increments('sid');
            $table->string('name', 120)->default('');
            $table->string('title', 120)->default('');
            $table->text('description', 65535);
            $table->text('optionscode', 65535);
            $table->text('value', 65535);
            $table->smallInteger('disporder')->unsigned()->default(0);
            $table->smallInteger('gid')->unsigned()->default(0)->index('gid');
            $table->boolean('isdefault')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('settings');
    }
}