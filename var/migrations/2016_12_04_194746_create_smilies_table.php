<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSmiliesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('smilies', function($table)
        {
            $table->increments('sid');
            $table->string('name', 120)->default('');
            $table->text('find', 65535);
            $table->string('image', 220)->default('');
            $table->smallInteger('disporder')->unsigned()->default(0);
            $table->boolean('showclickable')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('smilies');
    }
}