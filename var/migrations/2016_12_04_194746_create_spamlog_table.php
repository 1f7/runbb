<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSpamlogTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('spamlog', function($table)
        {
            $table->increments('sid');
            $table->string('username', 120)->default('');
            $table->string('email', 220)->default('');
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->integer('dateline')->unsigned()->default(0);
            $table->text('data', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('spamlog');
    }
}