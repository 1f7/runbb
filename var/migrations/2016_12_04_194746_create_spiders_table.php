<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateSpidersTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('spiders', function($table)
        {
            $table->increments('sid');
            $table->string('name', 100)->default('');
            $table->smallInteger('theme')->unsigned()->default(0);
            $table->string('language', 20)->default('');
            $table->smallInteger('usergroup')->unsigned()->default(0);
            $table->string('useragent', 200)->default('');
            $table->integer('lastvisit')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('spiders');
    }
}