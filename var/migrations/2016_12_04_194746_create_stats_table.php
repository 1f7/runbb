<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateStatsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('stats', function($table)
        {
            $table->integer('dateline')->unsigned()->default(0)->primary();
            $table->integer('numusers')->unsigned()->default(0);
            $table->integer('numthreads')->unsigned()->default(0);
            $table->integer('numposts')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('stats');
    }
}