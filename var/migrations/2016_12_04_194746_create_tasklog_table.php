<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateTasklogTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('tasklog', function($table)
        {
            $table->increments('lid');
            $table->integer('tid')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->text('data', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('tasklog');
    }
}