<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateTasksTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('tasks', function($table)
        {
            $table->increments('tid');
            $table->string('title', 120)->default('');
            $table->text('description', 65535);
            $table->string('file', 30)->default('');
            $table->string('minute', 200)->default('');
            $table->string('hour', 200)->default('');
            $table->string('day', 100)->default('');
            $table->string('month', 30)->default('');
            $table->string('weekday', 15)->default('');
            $table->integer('nextrun')->unsigned()->default(0);
            $table->integer('lastrun')->unsigned()->default(0);
            $table->boolean('enabled')->default(1);
            $table->boolean('logging')->default(0);
            $table->integer('locked')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('tasks');
    }
}