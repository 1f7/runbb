<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateTemplategroupsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('templategroups', function($table)
        {
            $table->increments('gid');
            $table->string('prefix', 50)->default('');
            $table->string('title', 100)->default('');
            $table->boolean('isdefault')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('templategroups');
    }
}