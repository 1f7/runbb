<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateTemplatesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('templates', function($table)
        {
            $table->increments('tid');
            $table->string('title', 120)->default('');
            $table->text('template', 65535);
            $table->smallInteger('sid')->default(0);
            $table->string('version', 20)->default('0');
            $table->string('status', 10)->default('');
            $table->integer('dateline')->unsigned()->default(0);
            $table->index(['sid','title'], 'sid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('templates');
    }
}