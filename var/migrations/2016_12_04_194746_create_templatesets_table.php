<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateTemplatesetsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('templatesets', function($table)
        {
            $table->increments('sid');
            $table->string('title', 120)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('templatesets');
    }
}