<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThemesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('themes', function($table)
        {
            $table->increments('tid');
            $table->string('name', 100)->default('');
            $table->smallInteger('pid')->unsigned()->default(0);
            $table->boolean('def')->default(0);
            $table->text('properties', 65535);
            $table->text('stylesheets', 65535);
            $table->text('allowedgroups', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('themes');
    }
}