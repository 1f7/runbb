<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThemestylesheetsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('themestylesheets', function($table)
        {
            $table->increments('sid');
            $table->string('name', 30)->default('');
            $table->smallInteger('tid')->unsigned()->default(0)->index('tid');
            $table->text('attachedto', 65535);
            $table->text('stylesheet', 65535);
            $table->string('cachefile', 100)->default('');
            $table->integer('lastmodified')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('themestylesheets');
    }
}