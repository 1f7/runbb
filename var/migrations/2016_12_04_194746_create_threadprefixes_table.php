<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThreadprefixesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('threadprefixes', function($table)
        {
            $table->increments('pid');
            $table->string('prefix', 120)->default('');
            $table->string('displaystyle', 200)->default('');
            $table->text('forums', 65535);
            $table->text('groups', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('threadprefixes');
    }
}