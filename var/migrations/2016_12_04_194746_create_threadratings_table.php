<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThreadratingsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('threadratings', function($table)
        {
            $table->increments('rid');
            $table->integer('tid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->boolean('rating')->default(0);
            $table->string('ipaddress', 45)->default('0')->index('ipaddress_idx');
            $table->index(['tid','uid'], 'tid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('threadratings');
    }
}