<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThreadsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('threads', function($table)
        {
            $table->increments('tid');
            $table->smallInteger('fid')->unsigned()->default(0);
            $table->string('subject', 120)->default('')->index('subject');
            $table->smallInteger('prefix')->unsigned()->default(0);
            $table->smallInteger('icon')->unsigned()->default(0);
            $table->integer('poll')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->string('username', 80)->default('');
            $table->integer('dateline')->unsigned()->default(0)->index('dateline');
            $table->integer('firstpost')->unsigned()->default(0)->index('firstpost');
            $table->integer('lastpost')->unsigned()->default(0);
            $table->string('lastposter', 120)->default('');
            $table->integer('lastposteruid')->unsigned()->default(0);
            $table->integer('views')->unsigned()->default(0);
            $table->integer('replies')->unsigned()->default(0);
            $table->string('closed', 30)->default('');
            $table->boolean('sticky')->default(0);
            $table->smallInteger('numratings')->unsigned()->default(0);
            $table->smallInteger('totalratings')->unsigned()->default(0);
            $table->text('notes', 65535);
            $table->boolean('visible')->default(0);
            $table->integer('unapprovedposts')->unsigned()->default(0);
            $table->integer('deletedposts')->unsigned()->default(0);
            $table->integer('attachmentcount')->unsigned()->default(0);
            $table->integer('deletetime')->unsigned()->default(0);
            $table->index(['fid','visible','sticky'], 'fid');
            $table->index(['lastpost','fid'], 'lastpost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('threads');
    }
}