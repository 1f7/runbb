<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThreadsreadTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('threadsread', function($table)
        {
            $table->increments('id');
            $table->integer('tid')->unsigned()->default(0);
            $table->integer('uid')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0)->index('dateline');
            $table->unique(['tid','uid'], 'tid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('threadsread');
    }
}