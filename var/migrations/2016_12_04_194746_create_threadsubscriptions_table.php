<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThreadsubscriptionsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('threadsubscriptions', function($table)
        {
            $table->increments('sid');
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->integer('tid')->unsigned()->default(0);
            $table->boolean('notification')->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->index(['tid','notification'], 'tid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('threadsubscriptions');
    }
}