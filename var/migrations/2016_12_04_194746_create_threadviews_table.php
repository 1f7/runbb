<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateThreadviewsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('threadviews', function($table)
        {
            $table->integer('tid')->unsigned()->default(0)->index('tid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('threadviews');
    }
}