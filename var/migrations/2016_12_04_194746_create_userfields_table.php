<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateUserfieldsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('userfields', function($table)
        {
            $table->integer('ufid')->unsigned()->default(0)->primary();
            $table->text('fid1', 65535);
            $table->text('fid2', 65535);
            $table->text('fid3', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('userfields');
    }
}