<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateUsergroupsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('usergroups', function($table)
        {
            $table->increments('gid');
            $table->boolean('type')->default(2);
            $table->string('title', 120)->default('');
            $table->text('description', 65535);
            $table->string('namestyle', 200)->default('{username}');
            $table->string('usertitle', 120)->default('');
            $table->smallInteger('stars')->unsigned()->default(0);
            $table->string('starimage', 120)->default('');
            $table->string('image', 120)->default('');
            $table->smallInteger('disporder')->unsigned();
            $table->boolean('isbannedgroup')->default(0);
            $table->boolean('canview')->default(0);
            $table->boolean('canviewthreads')->default(0);
            $table->boolean('canviewprofiles')->default(0);
            $table->boolean('candlattachments')->default(0);
            $table->boolean('canviewboardclosed')->default(0);
            $table->boolean('canpostthreads')->default(0);
            $table->boolean('canpostreplys')->default(0);
            $table->boolean('canpostattachments')->default(0);
            $table->boolean('canratethreads')->default(0);
            $table->boolean('modposts')->default(0);
            $table->boolean('modthreads')->default(0);
            $table->boolean('mod_edit_posts')->default(0);
            $table->boolean('modattachments')->default(0);
            $table->boolean('caneditposts')->default(0);
            $table->boolean('candeleteposts')->default(0);
            $table->boolean('candeletethreads')->default(0);
            $table->boolean('caneditattachments')->default(0);
            $table->boolean('canpostpolls')->default(0);
            $table->boolean('canvotepolls')->default(0);
            $table->boolean('canundovotes')->default(0);
            $table->boolean('canusepms')->default(0);
            $table->boolean('cansendpms')->default(0);
            $table->boolean('cantrackpms')->default(0);
            $table->boolean('candenypmreceipts')->default(0);
            $table->integer('pmquota')->unsigned()->default(0);
            $table->integer('maxpmrecipients')->unsigned()->default(5);
            $table->boolean('cansendemail')->default(0);
            $table->boolean('cansendemailoverride')->default(0);
            $table->integer('maxemails')->unsigned()->default(5);
            $table->integer('emailfloodtime')->unsigned()->default(5);
            $table->boolean('canviewmemberlist')->default(0);
            $table->boolean('canviewcalendar')->default(0);
            $table->boolean('canaddevents')->default(0);
            $table->boolean('canbypasseventmod')->default(0);
            $table->boolean('canmoderateevents')->default(0);
            $table->boolean('canviewonline')->default(0);
            $table->boolean('canviewwolinvis')->default(0);
            $table->boolean('canviewonlineips')->default(0);
            $table->boolean('cancp')->default(0);
            $table->boolean('issupermod')->default(0);
            $table->boolean('cansearch')->default(0);
            $table->boolean('canusercp')->default(0);
            $table->boolean('canuploadavatars')->default(0);
            $table->boolean('canratemembers')->default(0);
            $table->boolean('canchangename')->default(0);
            $table->boolean('canbereported')->default(0);
            $table->boolean('canchangewebsite')->default(1);
            $table->boolean('showforumteam')->default(0);
            $table->boolean('usereputationsystem')->default(0);
            $table->boolean('cangivereputations')->default(0);
            $table->boolean('candeletereputations')->default(0);
            $table->integer('reputationpower')->unsigned()->default(0);
            $table->integer('maxreputationsday')->unsigned()->default(0);
            $table->integer('maxreputationsperuser')->unsigned()->default(0);
            $table->integer('maxreputationsperthread')->unsigned()->default(0);
            $table->boolean('candisplaygroup')->default(0);
            $table->integer('attachquota')->unsigned()->default(0);
            $table->boolean('cancustomtitle')->default(0);
            $table->boolean('canwarnusers')->default(0);
            $table->boolean('canreceivewarnings')->default(0);
            $table->integer('maxwarningsday')->unsigned()->default(3);
            $table->boolean('canmodcp')->default(0);
            $table->boolean('showinbirthdaylist')->default(0);
            $table->boolean('canoverridepm')->default(0);
            $table->boolean('canusesig')->default(0);
            $table->smallInteger('canusesigxposts')->unsigned()->default(0);
            $table->boolean('signofollow')->default(0);
            $table->integer('edittimelimit')->unsigned()->default(0);
            $table->integer('maxposts')->unsigned()->default(0);
            $table->boolean('showmemberlist')->default(1);
            $table->boolean('canmanageannounce')->default(0);
            $table->boolean('canmanagemodqueue')->default(0);
            $table->boolean('canmanagereportedcontent')->default(0);
            $table->boolean('canviewmodlogs')->default(0);
            $table->boolean('caneditprofiles')->default(0);
            $table->boolean('canbanusers')->default(0);
            $table->boolean('canviewwarnlogs')->default(0);
            $table->boolean('canuseipsearch')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('usergroups');
    }
}