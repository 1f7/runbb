<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateUsersTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('users', function($table)
        {
            $table->increments('uid');
            $table->string('username', 120)->default('')->unique('username');
            $table->string('password', 120)->default('');
            $table->string('salt', 10)->default('');
            $table->string('loginkey', 50)->default('');
            $table->string('email', 220)->default('');
            $table->integer('postnum')->unsigned()->default(0);
            $table->integer('threadnum')->unsigned()->default(0);
            $table->string('avatar', 200)->default('');
            $table->string('avatardimensions', 10)->default('');
            $table->string('avatartype', 10)->default('0');
            $table->smallInteger('usergroup')->unsigned()->default(0)->index('usergroup');
            $table->string('additionalgroups', 200)->default('');
            $table->smallInteger('displaygroup')->unsigned()->default(0);
            $table->string('usertitle', 250)->default('');
            $table->integer('regdate')->unsigned()->default(0);
            $table->integer('lastactive')->unsigned()->default(0);
            $table->integer('lastvisit')->unsigned()->default(0);
            $table->integer('lastpost')->unsigned()->default(0);
            $table->string('website', 200)->default('');
            $table->string('icq', 10)->default('');
            $table->string('aim', 50)->default('');
            $table->string('yahoo', 50)->default('');
            $table->string('skype', 75)->default('');
            $table->string('google', 75)->default('');
            $table->string('birthday', 15)->default('');
            $table->string('birthdayprivacy', 4)->default('all');
            $table->text('signature', 65535);
            $table->boolean('allownotices')->default(0);
            $table->boolean('hideemail')->default(0);
            $table->boolean('subscriptionmethod')->default(0);
            $table->boolean('invisible')->default(0);
            $table->boolean('receivepms')->default(0);
            $table->boolean('receivefrombuddy')->default(0);
            $table->boolean('pmnotice')->default(0);
            $table->boolean('pmnotify')->default(0);
            $table->boolean('buddyrequestspm')->default(1);
            $table->boolean('buddyrequestsauto')->default(0);
            $table->string('threadmode', 8)->default('');
            $table->boolean('showimages')->default(0);
            $table->boolean('showvideos')->default(0);
            $table->boolean('showsigs')->default(0);
            $table->boolean('showavatars')->default(0);
            $table->boolean('showquickreply')->default(0);
            $table->boolean('showredirect')->default(0);
            $table->smallInteger('ppp')->unsigned()->default(0);
            $table->smallInteger('tpp')->unsigned()->default(0);
            $table->smallInteger('daysprune')->unsigned()->default(0);
            $table->string('dateformat', 4)->default('');
            $table->string('timeformat', 4)->default('');
            $table->string('timezone', 5)->default('');
            $table->boolean('dst')->default(0);
            $table->boolean('dstcorrection')->default(0);
            $table->text('buddylist', 65535);
            $table->text('ignorelist', 65535);
            $table->smallInteger('style')->unsigned()->default(0);
            $table->boolean('away')->default(0);
            $table->integer('awaydate')->unsigned()->default(0);
            $table->string('returndate', 15)->default('');
            $table->string('awayreason', 200)->default('');
            $table->text('pmfolders', 65535);
            $table->text('notepad', 65535);
            $table->integer('referrer')->unsigned()->default(0);
            $table->integer('referrals')->unsigned()->default(0);
            $table->integer('reputation')->default(0);
            $table->string('regip', 45)->default('0')->index('regip_idx');
            $table->string('lastip', 45)->default('0')->index('lastip_idx');
            $table->string('language', 50)->default('');
            $table->integer('timeonline')->unsigned()->default(0);
            $table->boolean('showcodebuttons')->default(1);
            $table->integer('totalpms')->unsigned()->default(0);
            $table->integer('unreadpms')->unsigned()->default(0);
            $table->integer('warningpoints')->unsigned()->default(0);
            $table->boolean('moderateposts')->default(0);
            $table->integer('moderationtime')->unsigned()->default(0);
            $table->boolean('suspendposting')->default(0);
            $table->integer('suspensiontime')->unsigned()->default(0);
            $table->boolean('suspendsignature')->default(0);
            $table->integer('suspendsigtime')->unsigned()->default(0);
            $table->boolean('coppauser')->default(0);
            $table->boolean('classicpostbit')->default(0);
            $table->smallInteger('loginattempts')->unsigned()->default(1);
            $table->text('usernotes', 65535);
            $table->boolean('sourceeditor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('users');
    }
}