<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateUsertitlesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('usertitles', function($table)
        {
            $table->increments('utid');
            $table->integer('posts')->unsigned()->default(0);
            $table->string('title', 250)->default('');
            $table->smallInteger('stars')->unsigned()->default(0);
            $table->string('starimage', 120)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('usertitles');
    }
}