<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateWarninglevelsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('warninglevels', function($table)
        {
            $table->increments('lid');
            $table->smallInteger('percentage')->unsigned()->default(0);
            $table->text('action', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('warninglevels');
    }
}