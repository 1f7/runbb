<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateWarningsTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('warnings', function($table)
        {
            $table->increments('wid');
            $table->integer('uid')->unsigned()->default(0)->index('uid');
            $table->integer('tid')->unsigned()->default(0);
            $table->integer('pid')->unsigned()->default(0);
            $table->string('title', 120)->default('');
            $table->smallInteger('points')->unsigned()->default(0);
            $table->integer('dateline')->unsigned()->default(0);
            $table->integer('issuedby')->unsigned()->default(0);
            $table->integer('expires')->unsigned()->default(0);
            $table->boolean('expired')->default(0);
            $table->integer('daterevoked')->unsigned()->default(0);
            $table->integer('revokedby')->unsigned()->default(0);
            $table->text('revokereason', 65535);
            $table->text('notes', 65535);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('warnings');
    }
}