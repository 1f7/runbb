<?php use Illuminate\Database\Capsule\Manager as DB;

class CreateWarningtypesTable
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::schema()->create('warningtypes', function($table)
        {
            $table->increments('tid');
            $table->string('title', 120)->default('');
            $table->smallInteger('points')->unsigned()->default(0);
            $table->integer('expirationtime')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::schema()->drop('warningtypes');
    }
}