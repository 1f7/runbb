<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class AdminlogTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('adminlog')->delete();
        
        DB::table('adminlog')->insert(array (
            0 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713474,
                'module' => '',
                'action' => 'prune',
                'data' => 'a:4:{i:0;i:1;i:1;i:0;i:2;s:0:"";i:3;i:123;}',
            ),
            1 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713500,
                'module' => '',
                'action' => 'prune',
                'data' => 'a:5:{i:0;i:1;i:1;i:0;i:2;i:0;i:3;i:71;i:4;N;}',
            ),
            2 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713529,
                'module' => '',
                'action' => 'prune',
                'data' => 'a:1:{i:0;i:13;}',
            ),
            3 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713542,
                'module' => '',
                'action' => 'prune',
                'data' => 'a:1:{i:0;i:9;}',
            ),
            4 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713898,
                'module' => '',
                'action' => 'delete',
                'data' => 'a:2:{i:0;s:1:"1";i:1;s:12:"test annonce";}',
            ),
            5 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713902,
                'module' => '',
                'action' => 'delete',
                'data' => 'a:2:{i:0;s:1:"4";i:1;s:10:"3132323313";}',
            ),
            6 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713905,
                'module' => '',
                'action' => 'delete',
                'data' => 'a:2:{i:0;s:2:"10";i:1;s:56:"Lorem ipsum dolor sit amet, consectetur adipiscing elit.";}',
            ),
            7 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713908,
                'module' => '',
                'action' => 'delete',
                'data' => 'a:2:{i:0;s:1:"8";i:1;s:14:"dshdfhhdhffdfh";}',
            ),
            8 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713911,
                'module' => '',
                'action' => 'delete',
                'data' => 'a:2:{i:0;s:1:"2";i:1;s:15:"global annonuce";}',
            ),
            9 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480713933,
                'module' => '',
                'action' => 'delete_orphans',
                'data' => 'a:0:{}',
            ),
            10 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480714028,
                'module' => '',
                'action' => 'prune',
                'data' => 'a:4:{i:0;i:1;i:1;i:0;i:2;s:0:"";i:3;i:0;}',
            ),
            11 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480720606,
                'module' => '',
                'action' => 'delete_prefix',
                'data' => 'a:2:{i:0;s:1:"1";i:1;s:8:"bu-bu-bu";}',
            ),
            12 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480720929,
                'module' => '',
                'action' => 'add',
                'data' => 'a:2:{i:0;i:1;i:1;s:14:"First Category";}',
            ),
            13 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480721012,
                'module' => '',
                'action' => 'add',
                'data' => 'a:2:{i:0;i:2;i:1;s:10:"Lets Begin";}',
            ),
            14 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480721048,
                'module' => '',
                'action' => '',
                'data' => 'a:5:{i:0;s:6:"addmod";i:1;i:1;i:2;s:10:"Moderators";i:3;i:2;i:4;s:10:"Lets Begin";}',
            ),
            15 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480721057,
                'module' => '',
                'action' => 'editmod',
                'data' => 'a:4:{i:0;i:2;i:1;s:10:"Lets Begin";i:2;i:1;i:3;s:10:"Moderators";}',
            ),
            16 => 
            array (
                'uid' => 1,
                'ipaddress' => '192.168.56.1',
                'dateline' => 1480876238,
                'module' => '',
                'action' => 'delete_template',
                'data' => 'a:4:{i:0;s:3:"869";i:1;s:19:"forumbit_depth1_cat";i:2;s:2:"-1";i:3;N;}',
            ),
        ));

        
    }
}
