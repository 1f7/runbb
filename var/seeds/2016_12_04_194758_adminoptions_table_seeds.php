<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class AdminoptionsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('adminoptions')->delete();
        
        DB::table('adminoptions')->insert(array (
            0 => 
            array (
                'uid' => -4,
                'cpstyle' => '',
                'cplanguage' => '',
                'codepress' => 1,
                'notes' => '',
                'permissions' => 'a:5:{s:7:"configs";a:18:{s:3:"tab";i:1;s:8:"settings";i:1;s:7:"banning";i:1;s:14:"profile_fields";i:1;s:7:"smilies";i:1;s:8:"badwords";i:1;s:6:"mycode";i:1;s:9:"languages";i:1;s:10:"post_icons";i:1;s:14:"help_documents";i:1;s:7:"plugins";i:1;s:16:"attachment_types";i:1;s:9:"mod_tools";i:1;s:7:"spiders";i:1;s:9:"calendars";i:1;s:7:"warning";i:1;s:15:"thread_prefixes";i:1;s:9:"questions";i:1;}s:6:"forums";a:5:{s:3:"tab";i:1;s:10:"management";i:1;s:13:"announcements";i:1;s:16:"moderation_queue";i:1;s:11:"attachments";i:1;}s:5:"style";a:3:{s:3:"tab";i:1;s:6:"themes";i:1;s:9:"templates";i:1;}s:5:"tools";a:16:{s:3:"tab";i:1;s:13:"system_health";i:1;s:5:"cache";i:1;s:5:"tasks";i:1;s:8:"backupdb";i:1;s:10:"optimizedb";i:1;s:15:"recount_rebuild";i:1;s:8:"php_info";i:1;s:8:"adminlog";i:1;s:6:"modlog";i:1;s:7:"spamlog";i:1;s:8:"maillogs";i:1;s:10:"mailerrors";i:1;s:10:"warninglog";i:1;s:10:"statistics";i:1;s:17:"file_verification";i:1;}s:5:"users";a:8:{s:3:"tab";i:1;s:5:"users";i:1;s:6:"groups";i:1;s:6:"titles";i:1;s:7:"banning";i:1;s:9:"adminperm";i:1;s:9:"mass_mail";i:1;s:16:"group_promotions";i:1;}}',
                'defaultviews' => 'a:1:{s:5:"users";s:1:"1";}',
                'loginattempts' => 0,
                'loginlockoutexpiry' => 0,
                'authsecret' => '',
                'recovery_codes' => '',
            ),
            1 => 
            array (
                'uid' => 0,
                'cpstyle' => '',
                'cplanguage' => '',
                'codepress' => 1,
                'notes' => '',
                'permissions' => 'a:5:{s:7:"configs";a:18:{s:3:"tab";i:1;s:8:"settings";i:1;s:7:"banning";i:1;s:14:"profile_fields";i:1;s:7:"smilies";i:1;s:8:"badwords";i:1;s:6:"mycode";i:1;s:9:"languages";i:1;s:10:"post_icons";i:1;s:14:"help_documents";i:1;s:7:"plugins";i:1;s:16:"attachment_types";i:1;s:9:"mod_tools";i:1;s:7:"spiders";i:1;s:9:"calendars";i:1;s:7:"warning";i:1;s:15:"thread_prefixes";i:1;s:9:"questions";i:1;}s:6:"forums";a:5:{s:3:"tab";i:1;s:10:"management";i:1;s:13:"announcements";i:1;s:16:"moderation_queue";i:1;s:11:"attachments";i:1;}s:5:"style";a:3:{s:3:"tab";i:1;s:6:"themes";i:1;s:9:"templates";i:1;}s:5:"tools";a:16:{s:3:"tab";i:1;s:13:"system_health";i:1;s:5:"cache";i:1;s:5:"tasks";i:1;s:8:"backupdb";i:1;s:10:"optimizedb";i:1;s:15:"recount_rebuild";i:1;s:8:"php_info";i:1;s:8:"adminlog";i:1;s:6:"modlog";i:1;s:7:"spamlog";i:1;s:8:"maillogs";i:1;s:10:"mailerrors";i:1;s:10:"warninglog";i:1;s:10:"statistics";i:1;s:17:"file_verification";i:1;}s:5:"users";a:8:{s:3:"tab";i:1;s:5:"users";i:1;s:6:"groups";i:1;s:6:"titles";i:1;s:7:"banning";i:1;s:9:"adminperm";i:1;s:9:"mass_mail";i:1;s:16:"group_promotions";i:1;}}',
                'defaultviews' => 'a:1:{s:5:"users";s:1:"1";}',
                'loginattempts' => 0,
                'loginlockoutexpiry' => 0,
                'authsecret' => '',
                'recovery_codes' => '',
            ),
            2 => 
            array (
                'uid' => 1,
                'cpstyle' => '',
                'cplanguage' => '',
                'codepress' => 1,
            'notes' => 'test note :)',
            'permissions' => 'a:5:{s:7:"configs";a:18:{s:3:"tab";i:1;s:8:"settings";i:1;s:7:"banning";i:1;s:14:"profile_fields";i:1;s:7:"smilies";i:1;s:8:"badwords";i:1;s:6:"mycode";i:1;s:9:"languages";i:1;s:10:"post_icons";i:1;s:14:"help_documents";i:1;s:7:"plugins";i:1;s:16:"attachment_types";i:1;s:9:"mod_tools";i:1;s:7:"spiders";i:1;s:9:"calendars";i:1;s:7:"warning";i:1;s:15:"thread_prefixes";i:1;s:9:"questions";i:1;}s:6:"forums";a:5:{s:3:"tab";i:1;s:10:"management";i:1;s:13:"announcements";i:1;s:16:"moderation_queue";i:1;s:11:"attachments";i:1;}s:5:"style";a:3:{s:3:"tab";i:1;s:6:"themes";i:1;s:9:"templates";i:1;}s:5:"tools";a:16:{s:3:"tab";i:1;s:13:"system_health";i:1;s:5:"cache";i:1;s:5:"tasks";i:1;s:8:"backupdb";i:1;s:10:"optimizedb";i:1;s:15:"recount_rebuild";i:1;s:8:"php_info";i:1;s:8:"adminlog";i:1;s:6:"modlog";i:1;s:7:"spamlog";i:1;s:8:"maillogs";i:1;s:10:"mailerrors";i:1;s:10:"warninglog";i:1;s:10:"statistics";i:1;s:17:"file_verification";i:1;}s:5:"users";a:8:{s:3:"tab";i:1;s:5:"users";i:1;s:6:"groups";i:1;s:6:"titles";i:1;s:7:"banning";i:1;s:9:"adminperm";i:1;s:9:"mass_mail";i:1;s:16:"group_promotions";i:1;}}',
            'defaultviews' => 'a:1:{s:5:"users";s:1:"1";}',
            'loginattempts' => 0,
            'loginlockoutexpiry' => 0,
            'authsecret' => '',
            'recovery_codes' => '',
        ),
        3 => 
        array (
            'uid' => 2,
            'cpstyle' => 'default',
            'cplanguage' => 'english',
            'codepress' => 0,
            'notes' => '',
            'permissions' => '',
            'defaultviews' => '',
            'loginattempts' => 0,
            'loginlockoutexpiry' => 0,
            'authsecret' => '',
            'recovery_codes' => '',
        ),
    ));

        
    }
}
