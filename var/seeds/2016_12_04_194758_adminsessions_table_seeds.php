<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class AdminsessionsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('adminsessions')->delete();
        
        DB::table('adminsessions')->insert(array (
            0 => 
            array (
                'sid' => 'd3777c2743c44a281434cc627b68202f',
                'uid' => 1,
                'loginkey' => 'QWzbfL28BzD5Inl8kmWaN4b5gwsfph88qLVlv3HYKScDbOhDZq',
                'ip' => '192.168.56.1',
                'dateline' => 1480875828,
                'lastactive' => 1480878143,
                'data' => 'a:1:{s:13:"flash_message";s:0:"";}',
            'useragent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0',
                'authenticated' => 0,
            ),
        ));

        
    }
}
