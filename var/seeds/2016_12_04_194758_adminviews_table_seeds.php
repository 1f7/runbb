<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class AdminviewsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('adminviews')->delete();
        
        DB::table('adminviews')->insert(array (
            0 => 
            array (
                'vid' => 1,
                'uid' => 0,
                'title' => 'All Users',
                'type' => 'user',
                'visibility' => 2,
                'fields' => 'a:7:{i:0;s:6:"avatar";i:1;s:8:"username";i:2;s:5:"email";i:3;s:7:"regdate";i:4;s:10:"lastactive";i:5;s:7:"postnum";i:6;s:8:"controls";}',
                'conditions' => 'a:0:{}',
                'custom_profile_fields' => 'a:0:{}',
                'sortby' => 'username',
                'sortorder' => 'asc',
                'perpage' => 20,
                'view_type' => 'card',
            ),
        ));

        
    }
}
