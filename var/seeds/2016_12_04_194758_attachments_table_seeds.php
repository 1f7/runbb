<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class AttachmentsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('attachments')->delete();
        
        DB::table('attachments')->insert(array (
            0 => 
            array (
                'aid' => 2,
                'pid' => 1,
                'posthash' => '',
                'uid' => 1,
                'filename' => 'pC_FtpasY2c.jpg',
                'filetype' => 'image/jpeg',
                'filesize' => 54841,
                'attachname' => '201612/post_1_1480723883_ba6ad68ed4dedd93a4284a503c04714f.attach',
                'downloads' => 2,
                'dateuploaded' => 1480723883,
                'visible' => 1,
                'thumbnail' => '201612/post_1_1480723883_ba6ad68ed4dedd93a4284a503c04714f_thumb.jpg',
            ),
        ));

        
    }
}
