<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class AttachtypesTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('attachtypes')->delete();
        
        DB::table('attachtypes')->insert(array (
            0 => 
            array (
                'atid' => 1,
                'name' => 'ZIP File',
                'mimetype' => 'application/zip',
                'extension' => 'zip',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/zip.png',
            ),
            1 => 
            array (
                'atid' => 2,
                'name' => 'JPG Image',
                'mimetype' => 'image/jpeg',
                'extension' => 'jpg',
                'maxsize' => 500,
                'icon' => 'images/attachtypes/image.png',
            ),
            2 => 
            array (
                'atid' => 3,
                'name' => 'Text Document',
                'mimetype' => 'text/plain',
                'extension' => 'txt',
                'maxsize' => 200,
                'icon' => 'images/attachtypes/txt.png',
            ),
            3 => 
            array (
                'atid' => 4,
                'name' => 'GIF Image',
                'mimetype' => 'image/gif',
                'extension' => 'gif',
                'maxsize' => 500,
                'icon' => 'images/attachtypes/image.png',
            ),
            4 => 
            array (
                'atid' => 5,
                'name' => 'Adobe Photoshop File',
                'mimetype' => 'application/x-photoshop',
                'extension' => 'psd',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/psd.png',
            ),
            5 => 
            array (
                'atid' => 6,
                'name' => 'PHP File',
                'mimetype' => 'application/x-httpd-php',
                'extension' => 'php',
                'maxsize' => 500,
                'icon' => 'images/attachtypes/php.png',
            ),
            6 => 
            array (
                'atid' => 7,
                'name' => 'PNG Image',
                'mimetype' => 'image/png',
                'extension' => 'png',
                'maxsize' => 500,
                'icon' => 'images/attachtypes/image.png',
            ),
            7 => 
            array (
                'atid' => 8,
                'name' => 'Microsoft Word Document',
                'mimetype' => 'application/msword',
                'extension' => 'doc',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/doc.png',
            ),
            8 => 
            array (
                'atid' => 9,
                'name' => 'HTM File',
                'mimetype' => 'text/html',
                'extension' => 'htm',
                'maxsize' => 100,
                'icon' => 'images/attachtypes/html.png',
            ),
            9 => 
            array (
                'atid' => 10,
                'name' => 'HTML File',
                'mimetype' => 'text/html',
                'extension' => 'html',
                'maxsize' => 100,
                'icon' => 'images/attachtypes/html.png',
            ),
            10 => 
            array (
                'atid' => 11,
                'name' => 'JPEG Image',
                'mimetype' => 'image/jpeg',
                'extension' => 'jpeg',
                'maxsize' => 500,
                'icon' => 'images/attachtypes/image.png',
            ),
            11 => 
            array (
                'atid' => 12,
                'name' => 'GZIP Compressed File',
                'mimetype' => 'application/x-gzip',
                'extension' => 'gz',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/tar.png',
            ),
            12 => 
            array (
                'atid' => 13,
                'name' => 'TAR Compressed File',
                'mimetype' => 'application/x-tar',
                'extension' => 'tar',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/tar.png',
            ),
            13 => 
            array (
                'atid' => 14,
                'name' => 'CSS Stylesheet',
                'mimetype' => 'text/css',
                'extension' => 'css',
                'maxsize' => 100,
                'icon' => 'images/attachtypes/css.png',
            ),
            14 => 
            array (
                'atid' => 15,
                'name' => 'Adobe Acrobat PDF',
                'mimetype' => 'application/pdf',
                'extension' => 'pdf',
                'maxsize' => 2048,
                'icon' => 'images/attachtypes/pdf.png',
            ),
            15 => 
            array (
                'atid' => 16,
                'name' => 'Bitmap Image',
                'mimetype' => 'image/bmp',
                'extension' => 'bmp',
                'maxsize' => 500,
                'icon' => 'images/attachtypes/image.png',
            ),
            16 => 
            array (
                'atid' => 17,
                'name' => 'Microsoft Word 2007 Document',
                'mimetype' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'extension' => 'docx',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/doc.png',
            ),
            17 => 
            array (
                'atid' => 18,
                'name' => 'Microsoft Excel Document',
                'mimetype' => 'application/vnd.ms-excel',
                'extension' => 'xls',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/xls.png',
            ),
            18 => 
            array (
                'atid' => 19,
                'name' => 'Microsoft Excel 2007 Document',
                'mimetype' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'extension' => 'xlsx',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/xls.png',
            ),
            19 => 
            array (
                'atid' => 20,
                'name' => 'Microsoft PowerPoint Document',
                'mimetype' => 'application/vnd.ms-powerpoint',
                'extension' => 'ppt',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/ppt.png',
            ),
            20 => 
            array (
                'atid' => 21,
                'name' => 'Microsoft PowerPoint 2007 Document',
                'mimetype' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'extension' => 'pptx',
                'maxsize' => 1024,
                'icon' => 'images/attachtypes/ppt.png',
            ),
        ));

        
    }
}
