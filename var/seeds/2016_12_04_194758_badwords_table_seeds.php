<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class BadwordsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('badwords')->delete();
        
        DB::table('badwords')->insert(array (
            0 => 
            array (
                'bid' => 1,
                'badword' => 'fuck*',
                'replacement' => '[censored]',
            ),
        ));

        
    }
}
