<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class BanfiltersTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('banfilters')->delete();
        
        DB::table('banfilters')->insert(array (
            0 => 
            array (
                'fid' => 1,
                'filter' => '',
                'type' => 1,
                'lastuse' => 0,
                'dateline' => 1469355665,
            ),
        ));

        
    }
}
