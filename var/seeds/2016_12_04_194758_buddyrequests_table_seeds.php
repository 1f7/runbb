<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class BuddyrequestsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('buddyrequests')->delete();
        
        DB::table('buddyrequests')->insert(array (
            0 => 
            array (
                'id' => 21,
                'uid' => 4,
                'touid' => 3,
                'date' => 1467133538,
            ),
        ));

        
    }
}
