<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class CalendarsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('calendars')->delete();
        
        DB::table('calendars')->insert(array (
            0 => 
            array (
                'cid' => 1,
                'name' => 'Default Calendar',
                'disporder' => 1,
                'startofweek' => 1,
                'showbirthdays' => 1,
                'eventlimit' => 4,
                'moderation' => 0,
                'allowhtml' => 0,
                'allowmycode' => 1,
                'allowimgcode' => 1,
                'allowvideocode' => 1,
                'allowsmilies' => 1,
            ),
        ));

        
    }
}
