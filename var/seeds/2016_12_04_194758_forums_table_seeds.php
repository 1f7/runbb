<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ForumsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('forums')->delete();
        
        DB::table('forums')->insert(array (
            0 => 
            array (
                'fid' => 1,
                'name' => 'First Category',
                'description' => 'First Test Category',
                'linkto' => '',
                'type' => 'c',
                'pid' => 0,
                'parentlist' => '1',
                'disporder' => 1,
                'active' => 1,
                'open' => 1,
                'threads' => 0,
                'posts' => 0,
                'lastpost' => 0,
                'lastposter' => '',
                'lastposteruid' => 0,
                'lastposttid' => 0,
                'lastpostsubject' => '',
                'allowhtml' => 0,
                'allowmycode' => 1,
                'allowsmilies' => 1,
                'allowimgcode' => 1,
                'allowvideocode' => 1,
                'allowpicons' => 1,
                'allowtratings' => 1,
                'usepostcounts' => 1,
                'usethreadcounts' => 1,
                'requireprefix' => 0,
                'password' => '',
                'showinjump' => 1,
                'style' => 0,
                'overridestyle' => 0,
                'rulestype' => 0,
                'rulestitle' => '',
                'rules' => '',
                'unapprovedthreads' => 0,
                'unapprovedposts' => 0,
                'deletedthreads' => 0,
                'deletedposts' => 0,
                'defaultdatecut' => 0,
                'defaultsortby' => '',
                'defaultsortorder' => '',
            ),
            1 => 
            array (
                'fid' => 2,
                'name' => 'Lets Begin',
                'description' => 'Begin tests',
                'linkto' => '',
                'type' => 'f',
                'pid' => 1,
                'parentlist' => '1,2',
                'disporder' => 1,
                'active' => 1,
                'open' => 1,
                'threads' => 1,
                'posts' => 1,
                'lastpost' => 1480723899,
                'lastposter' => 'admin',
                'lastposteruid' => 1,
                'lastposttid' => 1,
                'lastpostsubject' => 'qweqweqe',
                'allowhtml' => 0,
                'allowmycode' => 1,
                'allowsmilies' => 1,
                'allowimgcode' => 1,
                'allowvideocode' => 1,
                'allowpicons' => 1,
                'allowtratings' => 1,
                'usepostcounts' => 1,
                'usethreadcounts' => 1,
                'requireprefix' => 0,
                'password' => '',
                'showinjump' => 1,
                'style' => 0,
                'overridestyle' => 0,
                'rulestype' => 0,
                'rulestitle' => '',
                'rules' => '',
                'unapprovedthreads' => 0,
                'unapprovedposts' => 0,
                'deletedthreads' => 0,
                'deletedposts' => 0,
                'defaultdatecut' => 0,
                'defaultsortby' => '',
                'defaultsortorder' => '',
            ),
        ));

        
    }
}
