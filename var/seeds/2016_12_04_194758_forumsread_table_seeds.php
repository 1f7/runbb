<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ForumsreadTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('forumsread')->delete();
        
        DB::table('forumsread')->insert(array (
            0 => 
            array (
                'fid' => 2,
                'uid' => 1,
                'dateline' => 1480723834,
            ),
        ));

        
    }
}
