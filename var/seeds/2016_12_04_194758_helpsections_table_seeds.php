<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class HelpsectionsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('helpsections')->delete();
        
        DB::table('helpsections')->insert(array (
            0 => 
            array (
                'sid' => 1,
                'name' => 'User Maintenance',
                'description' => 'Basic instructions for maintaining a forum account.',
                'usetranslation' => 1,
                'enabled' => 1,
                'disporder' => 1,
            ),
            1 => 
            array (
                'sid' => 2,
                'name' => 'Posting',
                'description' => 'Posting, replying, and basic usage of forum.',
                'usetranslation' => 1,
                'enabled' => 1,
                'disporder' => 2,
            ),
        ));

        
    }
}
