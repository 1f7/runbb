<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class IconsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('icons')->delete();
        
        DB::table('icons')->insert(array (
            0 => 
            array (
                'iid' => 1,
                'name' => 'Bug',
                'path' => 'images/icons/bug.png',
            ),
            1 => 
            array (
                'iid' => 2,
                'name' => 'Exclamation',
                'path' => 'images/icons/exclamation.png',
            ),
            2 => 
            array (
                'iid' => 3,
                'name' => 'Question',
                'path' => 'images/icons/question.png',
            ),
            3 => 
            array (
                'iid' => 4,
                'name' => 'Smile',
                'path' => 'images/icons/smile.png',
            ),
            4 => 
            array (
                'iid' => 5,
                'name' => 'Sad',
                'path' => 'images/icons/sad.png',
            ),
            5 => 
            array (
                'iid' => 6,
                'name' => 'Wink',
                'path' => 'images/icons/wink.png',
            ),
            6 => 
            array (
                'iid' => 7,
                'name' => 'Big Grin',
                'path' => 'images/icons/biggrin.png',
            ),
            7 => 
            array (
                'iid' => 8,
                'name' => 'Tongue',
                'path' => 'images/icons/tongue.png',
            ),
            8 => 
            array (
                'iid' => 9,
                'name' => 'Brick',
                'path' => 'images/icons/brick.png',
            ),
            9 => 
            array (
                'iid' => 10,
                'name' => 'Heart',
                'path' => 'images/icons/heart.png',
            ),
            10 => 
            array (
                'iid' => 11,
                'name' => 'Information',
                'path' => 'images/icons/information.png',
            ),
            11 => 
            array (
                'iid' => 12,
                'name' => 'Lightbulb',
                'path' => 'images/icons/lightbulb.png',
            ),
            12 => 
            array (
                'iid' => 13,
                'name' => 'Music',
                'path' => 'images/icons/music.png',
            ),
            13 => 
            array (
                'iid' => 14,
                'name' => 'Photo',
                'path' => 'images/icons/photo.png',
            ),
            14 => 
            array (
                'iid' => 15,
                'name' => 'Rainbow',
                'path' => 'images/icons/rainbow.png',
            ),
            15 => 
            array (
                'iid' => 16,
                'name' => 'Shocked',
                'path' => 'images/icons/shocked.png',
            ),
            16 => 
            array (
                'iid' => 17,
                'name' => 'Star',
                'path' => 'images/icons/star.png',
            ),
            17 => 
            array (
                'iid' => 18,
                'name' => 'Thumbs Down',
                'path' => 'images/icons/thumbsdown.png',
            ),
            18 => 
            array (
                'iid' => 19,
                'name' => 'Thumbs Up',
                'path' => 'images/icons/thumbsup.png',
            ),
            19 => 
            array (
                'iid' => 20,
                'name' => 'Video',
                'path' => 'images/icons/video.png',
            ),
        ));

        
    }
}
