<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ModeratorsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('moderators')->delete();
        
        DB::table('moderators')->insert(array (
            0 => 
            array (
                'mid' => 1,
                'fid' => 2,
                'id' => 6,
                'isgroup' => 1,
                'caneditposts' => 1,
                'cansoftdeleteposts' => 1,
                'canrestoreposts' => 1,
                'candeleteposts' => 1,
                'cansoftdeletethreads' => 1,
                'canrestorethreads' => 1,
                'candeletethreads' => 1,
                'canviewips' => 1,
                'canviewunapprove' => 1,
                'canviewdeleted' => 1,
                'canopenclosethreads' => 1,
                'canstickunstickthreads' => 1,
                'canapproveunapprovethreads' => 1,
                'canapproveunapproveposts' => 1,
                'canapproveunapproveattachs' => 1,
                'canmanagethreads' => 1,
                'canmanagepolls' => 1,
                'canpostclosedthreads' => 1,
                'canmovetononmodforum' => 1,
                'canusecustomtools' => 1,
                'canmanageannouncements' => 1,
                'canmanagereportedposts' => 1,
                'canviewmodlog' => 1,
            ),
        ));

        
    }
}
