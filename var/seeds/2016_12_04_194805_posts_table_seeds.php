<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class PostsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('posts')->delete();
        
        DB::table('posts')->insert(array (
            0 => 
            array (
                'pid' => 1,
                'tid' => 1,
                'replyto' => 0,
                'fid' => 2,
                'subject' => 'qweqweqe',
                'icon' => 1,
                'uid' => 1,
                'username' => 'admin',
                'dateline' => 1480723899,
                'message' => 'qeqweqeqeqwqwe
qweqweqweqeqwew
qeqw 
:D

[attachment=2]',
                'ipaddress' => '192.168.56.1',
                'includesig' => 0,
                'smilieoff' => 0,
                'edituid' => 1,
                'edittime' => 1480874425,
                'editreason' => '',
                'visible' => 1,
            ),
        ));

        
    }
}
