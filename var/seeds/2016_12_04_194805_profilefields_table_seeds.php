<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ProfilefieldsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('profilefields')->delete();
        
        DB::table('profilefields')->insert(array (
            0 => 
            array (
                'fid' => 1,
                'name' => 'Location',
                'description' => 'Where in the world do you live?',
                'disporder' => 1,
                'type' => 'text',
                'regex' => '',
                'length' => 0,
                'maxlength' => 255,
                'required' => 0,
                'registration' => 0,
                'profile' => 1,
                'postbit' => 0,
                'viewableby' => '-1',
                'editableby' => '-1',
                'postnum' => 0,
                'allowhtml' => 0,
                'allowmycode' => 0,
                'allowsmilies' => 0,
                'allowimgcode' => 0,
                'allowvideocode' => 0,
            ),
            1 => 
            array (
                'fid' => 2,
                'name' => 'Bio',
                'description' => 'Enter a few short details about yourself, your life story etc.',
                'disporder' => 2,
                'type' => 'textarea',
                'regex' => '',
                'length' => 0,
                'maxlength' => 0,
                'required' => 0,
                'registration' => 0,
                'profile' => 1,
                'postbit' => 0,
                'viewableby' => '-1',
                'editableby' => '-1',
                'postnum' => 0,
                'allowhtml' => 0,
                'allowmycode' => 0,
                'allowsmilies' => 0,
                'allowimgcode' => 0,
                'allowvideocode' => 0,
            ),
            2 => 
            array (
                'fid' => 3,
                'name' => 'Sex',
                'description' => 'Please select your sex from the list below.',
                'disporder' => 3,
                'type' => 'select
Undisclosed
Male
Female
Other',
                'regex' => '',
                'length' => 0,
                'maxlength' => 0,
                'required' => 0,
                'registration' => 0,
                'profile' => 1,
                'postbit' => 0,
                'viewableby' => '-1',
                'editableby' => '-1',
                'postnum' => 0,
                'allowhtml' => 0,
                'allowmycode' => 0,
                'allowsmilies' => 0,
                'allowimgcode' => 0,
                'allowvideocode' => 0,
            ),
        ));

        
    }
}
