<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class QuestionsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('questions')->delete();
        
        DB::table('questions')->insert(array (
            0 => 
            array (
                'qid' => 1,
                'question' => 'What does 2 + 2 equal?',
                'answer' => '4
Four',
                'shown' => 43,
                'correct' => 6,
                'incorrect' => 0,
                'active' => 1,
            ),
            1 => 
            array (
                'qid' => 2,
                'question' => 'What does 3 - 2 equal?',
                'answer' => '1
one',
                'shown' => 3,
                'correct' => 1,
                'incorrect' => 0,
                'active' => 1,
            ),
        ));

        
    }
}
