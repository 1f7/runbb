<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class SessionsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('sessions')->delete();
        
        DB::table('sessions')->insert(array (
            0 => 
            array (
                'sid' => '6f3fb5cf00563d1948ad480d34d1b3ef',
                'uid' => 1,
                'ip' => '192.168.56.1',
                'time' => 1480878154,
                'location' => '/forum/task?',
            'useragent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:50.0) Gecko/20100101 Firefox/50.0',
                'anonymous' => 0,
                'nopermission' => 0,
                'location1' => 0,
                'location2' => 0,
            ),
        ));

        
    }
}
