<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class SettinggroupsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('settinggroups')->delete();
        
        DB::table('settinggroups')->insert(array (
            0 => 
            array (
                'gid' => 1,
                'name' => 'onlineoffline',
                'title' => 'Board Online / Offline',
                'description' => 'These settings allow you to globally turn your forums online or offline, and allow you to specify a reason for turning them off.',
                'disporder' => 1,
                'isdefault' => 1,
            ),
            1 => 
            array (
                'gid' => 2,
                'name' => 'details',
                'title' => 'Site Details',
                'description' => 'This section contains various settings such as your board name and url, as well as your website name and url.',
                'disporder' => 2,
                'isdefault' => 1,
            ),
            2 => 
            array (
                'gid' => 3,
                'name' => 'general',
                'title' => 'General Configuration',
                'description' => 'This section allows you to manage the general aspects of your board.',
                'disporder' => 3,
                'isdefault' => 1,
            ),
            3 => 
            array (
                'gid' => 4,
                'name' => 'server',
                'title' => 'Server and Optimization Options',
                'description' => 'These options allow you to set various server and optimization preferences allowing you to reduce the load on your server, and gain better performance on your board.',
                'disporder' => 4,
                'isdefault' => 1,
            ),
            4 => 
            array (
                'gid' => 5,
                'name' => 'datetime',
                'title' => 'Date and Time Formats',
                'description' => 'Here you can specify the different date and time formats used to display dates and times on the forums.',
                'disporder' => 5,
                'isdefault' => 1,
            ),
            5 => 
            array (
                'gid' => 6,
                'name' => 'forumhome',
                'title' => 'Forum Home Options',
            'description' => 'This section allows you to manage the various settings used on the forum home (index.php) of your boards such as enabling and disabling different features.',
                'disporder' => 6,
                'isdefault' => 1,
            ),
            6 => 
            array (
                'gid' => 7,
                'name' => 'forumdisplay',
                'title' => 'Forum Display Options',
            'description' => 'This section allows you to manage the various settings used on the forum display (forumdisplay.php) of your boards such as enabling and disabling different features.',
                'disporder' => 7,
                'isdefault' => 1,
            ),
            7 => 
            array (
                'gid' => 8,
                'name' => 'showthread',
                'title' => 'Show Thread Options',
            'description' => 'This section allows you to manage the various settings used on the thread display page (showthread.php) of your boards such as enabling and disabling different features.',
                'disporder' => 8,
                'isdefault' => 1,
            ),
            8 => 
            array (
                'gid' => 9,
                'name' => 'member',
                'title' => 'Login and Registration Options',
                'description' => 'Here you can control various settings with relation to user account registration and login management.',
                'disporder' => 9,
                'isdefault' => 1,
            ),
            9 => 
            array (
                'gid' => 10,
                'name' => 'profile',
                'title' => 'Profile Options',
                'description' => 'The options here control the various settings of user profiles around the forums.',
                'disporder' => 10,
                'isdefault' => 1,
            ),
            10 => 
            array (
                'gid' => 11,
                'name' => 'posting',
                'title' => 'Posting',
                'description' => 'These options control the various elements in relation to posting messages on the forums.',
                'disporder' => 11,
                'isdefault' => 1,
            ),
            11 => 
            array (
                'gid' => 12,
                'name' => 'attachments',
                'title' => 'Attachments',
                'description' => 'Various options related to the attachment system can be managed and set here.',
                'disporder' => 12,
                'isdefault' => 1,
            ),
            12 => 
            array (
                'gid' => 13,
                'name' => 'memberlist',
                'title' => 'Member List',
            'description' => 'This section allows you to control various aspects of the board member listing (memberlist.php), such as how many members to show per page, and which features to enable or disable.',
                'disporder' => 13,
                'isdefault' => 1,
            ),
            13 => 
            array (
                'gid' => 14,
                'name' => 'reputation',
                'title' => 'Reputation',
            'description' => 'The reputation system allows your users to rate others and leave a comment on the user. This section has settings to disable and change other aspects of the reputation page (reputation.php).',
                'disporder' => 14,
                'isdefault' => 1,
            ),
            14 => 
            array (
                'gid' => 15,
                'name' => 'warning',
                'title' => 'Warning System Settings',
            'description' => 'The warning system allows forum staff to warn users for rule violations. Here you can manage the settings that control the warning system (warnings.php).',
                'disporder' => 15,
                'isdefault' => 1,
            ),
            15 => 
            array (
                'gid' => 16,
                'name' => 'privatemessaging',
                'title' => 'Private Messaging',
            'description' => 'Various options with relation to the MyBB Private Messaging system (private.php) can be managed and set here.',
                'disporder' => 16,
                'isdefault' => 1,
            ),
            16 => 
            array (
                'gid' => 17,
                'name' => 'calendar',
                'title' => 'Calendar',
            'description' => 'The board calendar allows the public and private listing of events and members\' birthdays. This section allows you to control and manage the settings for the Calendar (calendar.php).',
                'disporder' => 17,
                'isdefault' => 1,
            ),
            17 => 
            array (
                'gid' => 18,
                'name' => 'whosonline',
                'title' => 'Who\'s Online',
            'description' => 'Various settings regarding the Who is Online functionality (online.php).',
                'disporder' => 18,
                'isdefault' => 1,
            ),
            18 => 
            array (
                'gid' => 19,
                'name' => 'userpruning',
                'title' => 'User Pruning',
                'description' => 'User Pruning allows you to remove users from your forum meeting certain criteria. Here you can configure that criteria.',
                'disporder' => 19,
                'isdefault' => 1,
            ),
            19 => 
            array (
                'gid' => 20,
                'name' => 'portal',
                'title' => 'Portal Settings',
            'description' => 'The portal page compiles several different pieces of information about your forum, including latest posts, who\'s online, forum stats, announcements, and more. This section has settings to control the aspects of the portal page (portal.php).',
                'disporder' => 20,
                'isdefault' => 1,
            ),
            20 => 
            array (
                'gid' => 21,
                'name' => 'search',
                'title' => 'Search System',
                'description' => 'The various settings in this group allow you to make changes to the built in search mechanism for threads, posts and help documents in MyBB.',
                'disporder' => 21,
                'isdefault' => 1,
            ),
            21 => 
            array (
                'gid' => 22,
                'name' => 'clickablecode',
                'title' => 'Clickable Smilies and BB Code',
                'description' => 'This section allows you to change how the clickable smilies inserter appears and control the default MyCodes.',
                'disporder' => 22,
                'isdefault' => 1,
            ),
            22 => 
            array (
                'gid' => 23,
                'name' => 'cpprefs',
            'title' => 'Control Panel Preferences (Global)',
                'description' => 'This section allows you to set the global preferences for the Admin Control Panel.',
                'disporder' => 23,
                'isdefault' => 1,
            ),
            23 => 
            array (
                'gid' => 24,
                'name' => 'mailsettings',
                'title' => 'Mail Settings',
                'description' => 'This section allows you to control various aspects of the MyBB mail system, such as sending with PHP mail or with a off server SMTP server.',
                'disporder' => 24,
                'isdefault' => 1,
            ),
            24 => 
            array (
                'gid' => 25,
                'name' => 'contactsettings',
                'title' => 'Contact Settings',
            'description' => 'The various options to change the behaviour of the contact page (contact.php).',
                'disporder' => 25,
                'isdefault' => 1,
            ),
            25 => 
            array (
                'gid' => 26,
                'name' => 'purgespammer',
                'title' => 'Purge Spammer',
                'description' => 'This section allows you to change the settings of the Purge Spammer feature.',
                'disporder' => 26,
                'isdefault' => 1,
            ),
            26 => 
            array (
                'gid' => 27,
                'name' => 'stopforumspam',
                'title' => 'Stop Forum Spam',
                'description' => 'This section allows you to change the settings used for the integration with StopForumSpam.com',
                'disporder' => 27,
                'isdefault' => 1,
            ),
            27 => 
            array (
                'gid' => 28,
                'name' => 'contactdetails',
                'title' => 'Contact Details',
                'description' => 'This section allows you to change the settings of the contact fields feature.',
                'disporder' => 28,
                'isdefault' => 1,
            ),
            28 => 
            array (
                'gid' => 29,
                'name' => 'statspage',
                'title' => 'Statistics Page',
                'description' => 'This section allows you to change the settings of the statistics page.',
                'disporder' => 28,
                'isdefault' => 1,
            ),
        ));

        
    }
}
