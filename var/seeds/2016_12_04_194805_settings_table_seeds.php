<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class SettingsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('settings')->delete();
        
        DB::table('settings')->insert(array (
            0 => 
            array (
                'sid' => 1,
                'name' => 'boardclosed',
                'title' => 'Board Closed',
                'description' => 'If you need to close your forums to make some changes or perform an upgrade, this is the global switch. Viewers will not be able to view your forums, however, they will see a message with the reason you specify below. Administrators will still be able to view the forums.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 1,
                'gid' => 1,
                'isdefault' => 1,
            ),
            1 => 
            array (
                'sid' => 2,
                'name' => 'boardclosed_reason',
                'title' => 'Board Closed Reason',
                'description' => 'If your forum is closed, you can set a message here that your visitors will be able to see when they visit your forums. You can leave this empty to use the language files.',
                'optionscode' => 'textarea',
                'value' => 'These forums are currently closed for maintenance. Please check back later.',
                'disporder' => 2,
                'gid' => 1,
                'isdefault' => 1,
            ),
            2 => 
            array (
                'sid' => 3,
                'name' => 'bbname',
                'title' => 'Board Name',
                'description' => 'The name of your community. We recommend that it is not over 75 characters.',
                'optionscode' => 'text',
                'value' => 'Forums',
                'disporder' => 1,
                'gid' => 2,
                'isdefault' => 1,
            ),
            3 => 
            array (
                'sid' => 4,
                'name' => 'bburl',
                'title' => 'Board URL',
                'description' => 'The url to your forums.<br />Include the http://. Do NOT include a trailing slash.',
                'optionscode' => 'text',
                'value' => '/forum',
                'disporder' => 2,
                'gid' => 2,
                'isdefault' => 1,
            ),
            4 => 
            array (
                'sid' => 5,
                'name' => 'homename',
                'title' => 'Homepage Name',
                'description' => 'The name of your homepage. This will appear in the footer with a link to it.',
                'optionscode' => 'text',
                'value' => 'Your Website',
                'disporder' => 3,
                'gid' => 2,
                'isdefault' => 1,
            ),
            5 => 
            array (
                'sid' => 6,
                'name' => 'homeurl',
                'title' => 'Homepage URL',
                'description' => 'The full URL of your homepage. This will be linked to in the footer along with its name.',
                'optionscode' => 'text',
                'value' => 'http://run2.dev/',
                'disporder' => 4,
                'gid' => 2,
                'isdefault' => 1,
            ),
            6 => 
            array (
                'sid' => 7,
                'name' => 'adminemail',
                'title' => 'Admin Email',
                'description' => 'The administrator\'s email address. This will be used for outgoing emails sent via the forums.',
                'optionscode' => 'text',
                'value' => 'you@example.com',
                'disporder' => 5,
                'gid' => 2,
                'isdefault' => 1,
            ),
            7 => 
            array (
                'sid' => 8,
                'name' => 'returnemail',
                'title' => 'Return Email',
                'description' => 'The email address for incoming replies to outgoing emails sent via the forums and is useful for no-reply accounts. Leave blank to use the admin email address instead.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 6,
                'gid' => 2,
                'isdefault' => 1,
            ),
            8 => 
            array (
                'sid' => 9,
                'name' => 'contactemail',
                'title' => 'Contact Email',
            'description' => 'The email address that messages sent via the contact form (contact.php) will be sent to. Leave blank to use the admin email address instead.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 7,
                'gid' => 2,
                'isdefault' => 1,
            ),
            9 => 
            array (
                'sid' => 10,
                'name' => 'contactlink',
                'title' => 'Contact Us Link',
            'description' => 'This will be used for the Contact Us link on the bottom of all the forum pages. Can either be an email address (using mailto:email@website.com) or a hyperlink.',
                'optionscode' => 'text',
                'value' => 'contact',
                'disporder' => 8,
                'gid' => 2,
                'isdefault' => 1,
            ),
            10 => 
            array (
                'sid' => 11,
                'name' => 'cookiedomain',
                'title' => 'Cookie Domain',
                'description' => 'The domain which cookies should be set to. This can remain blank. It should also start with a . so it covers all subdomains.',
                'optionscode' => 'text',
                'value' => '.run2.dev',
                'disporder' => 9,
                'gid' => 2,
                'isdefault' => 1,
            ),
            11 => 
            array (
                'sid' => 12,
                'name' => 'cookiepath',
                'title' => 'Cookie Path',
                'description' => 'The path which cookies are set to. We recommend setting this to the full directory path to your forums with a trailing slash.',
                'optionscode' => 'text',
                'value' => '/',
                'disporder' => 10,
                'gid' => 2,
                'isdefault' => 1,
            ),
            12 => 
            array (
                'sid' => 13,
                'name' => 'cookieprefix',
                'title' => 'Cookie Prefix',
                'description' => 'A prefix to append to all cookies set by MyBB. This is useful if you wish to install multiple copies of MyBB on the one domain or have other software installed which conflicts with the names of the cookies in MyBB. If not specified, no prefix will be used.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 11,
                'gid' => 2,
                'isdefault' => 1,
            ),
            13 => 
            array (
                'sid' => 14,
                'name' => 'showvernum',
                'title' => 'Show Version Numbers',
                'description' => 'Allows you to turn off the public display of version numbers in MyBB.',
                'optionscode' => 'onoff',
                'value' => '0',
                'disporder' => 12,
                'gid' => 2,
                'isdefault' => 1,
            ),
            14 => 
            array (
                'sid' => 15,
                'name' => 'mailingaddress',
                'title' => 'Mailing Address',
                'description' => 'If you have a mailing address, enter it here. This is shown on the COPPA compliance form.',
                'optionscode' => 'textarea',
                'value' => '',
                'disporder' => 13,
                'gid' => 2,
                'isdefault' => 1,
            ),
            15 => 
            array (
                'sid' => 16,
                'name' => 'faxno',
                'title' => 'Contact Fax No',
                'description' => 'If you have a fax number, enter it here. This is shown on the COPPA compliance form.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 14,
                'gid' => 2,
                'isdefault' => 1,
            ),
            16 => 
            array (
                'sid' => 17,
                'name' => 'bblanguage',
                'title' => 'Default Language',
                'description' => 'The default language that MyBB should use for guests and for users without a selected language in their user control panel.',
                'optionscode' => 'language',
                'value' => 'english',
                'disporder' => 1,
                'gid' => 3,
                'isdefault' => 1,
            ),
            17 => 
            array (
                'sid' => 18,
                'name' => 'captchaimage',
                'title' => 'CAPTCHA Images for Registration & Posting',
                'description' => 'To help prevent automated registrations and postings, you can choose a type of image verification for the user to complete. Please see the MyBB Docs for more information on CAPTCHAs.',
                'optionscode' => 'select
0=No CAPTCHA
1=MyBB Default CAPTCHA
2=reCAPTCHA
4=NoCAPTCHA reCAPTCHA',
                'value' => '1',
                'disporder' => 2,
                'gid' => 3,
                'isdefault' => 1,
            ),
            18 => 
            array (
                'sid' => 19,
                'name' => 'captchapublickey',
                'title' => 'reCAPTCHA Public Key',
                'description' => 'Your reCAPTCHA public key.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 3,
                'gid' => 3,
                'isdefault' => 1,
            ),
            19 => 
            array (
                'sid' => 20,
                'name' => 'captchaprivatekey',
                'title' => 'reCAPTCHA Private Key',
                'description' => 'Your reCAPTCHA private key.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 4,
                'gid' => 3,
                'isdefault' => 1,
            ),
            20 => 
            array (
                'sid' => 21,
                'name' => 'reportmethod',
                'title' => 'Reported Posts Medium',
                'description' => 'Please select from the list how you want reported posts to be dealt with. Storing them in the database is probably the better of the options listed.',
                'optionscode' => 'radio
db=Stored in the Database
pms=Sent as Private Messages
email=Sent via Email',
                'value' => 'db',
                'disporder' => 5,
                'gid' => 3,
                'isdefault' => 1,
            ),
            21 => 
            array (
                'sid' => 22,
                'name' => 'decpoint',
                'title' => 'Decimal Point',
                'description' => 'The decimal point you use in your region.',
                'optionscode' => 'text',
                'value' => '.',
                'disporder' => 6,
                'gid' => 3,
                'isdefault' => 1,
            ),
            22 => 
            array (
                'sid' => 23,
                'name' => 'thousandssep',
                'title' => 'Thousands Numeric Separator',
                'description' => 'The punctuation you want to use. For example, a setting of \',\' with the number 1200 will give you 1,200.',
                'optionscode' => 'text',
                'value' => ',',
                'disporder' => 7,
                'gid' => 3,
                'isdefault' => 1,
            ),
            23 => 
            array (
                'sid' => 24,
                'name' => 'showlanguageselect',
                'title' => 'Show Language Selector in Footer',
                'description' => 'Set to no if you do not want to show the language selection area in the footer of all pages in the board. If you only have one language installed this setting will be ignored.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 8,
                'gid' => 3,
                'isdefault' => 1,
            ),
            24 => 
            array (
                'sid' => 25,
                'name' => 'showthemeselect',
                'title' => 'Show Theme Selector in Footer',
                'description' => 'Set to no if you do not want to show the theme selection area in the footer of all pages in the board.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 9,
                'gid' => 3,
                'isdefault' => 1,
            ),
            25 => 
            array (
                'sid' => 26,
                'name' => 'maxmultipagelinks',
                'title' => 'Maximum Page Links in Pagination',
                'description' => 'Here you can set the number of next and previous page links to show in the pagination for threads or forums with more than one page of results.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 10,
                'gid' => 3,
                'isdefault' => 1,
            ),
            26 => 
            array (
                'sid' => 27,
                'name' => 'jumptopagemultipage',
                'title' => 'Show Jump To Page form in Pagination',
                'description' => 'Do you want to show a "jump to page" form in pagination if number of pages exceeds "Maximum Page Links in Pagination"?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 11,
                'gid' => 3,
                'isdefault' => 1,
            ),
            27 => 
            array (
                'sid' => 28,
                'name' => 'no_plugins',
                'title' => 'Disable All Plugins',
                'description' => 'Setting this to yes will disable all plugins without deactivating or uninstalling them. This is equivalent of manually defining NO_PLUGINS at the top of ./inc/init.php.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 12,
                'gid' => 3,
                'isdefault' => 1,
            ),
            28 => 
            array (
                'sid' => 29,
                'name' => 'deleteinvites',
                'title' => 'Expire Old Group Invites',
                'description' => 'The number of days until any pending invites to user groups expire. Set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '180',
                'disporder' => 13,
                'gid' => 3,
                'isdefault' => 1,
            ),
            29 => 
            array (
                'sid' => 30,
                'name' => 'redirects',
                'title' => 'Friendly Redirection Pages',
                'description' => 'This will enable friendly redirection pages instead of bumping the user directly to the page.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 14,
                'gid' => 3,
                'isdefault' => 1,
            ),
            30 => 
            array (
                'sid' => 31,
                'name' => 'enableforumjump',
                'title' => 'Enable Forum Jump Menu?',
                'description' => 'The forum jump menu is shown on the forum and thread view pages. It can add significant load to your forums if you have a large amount of forums. Set to \'No\' to disable it.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 15,
                'gid' => 3,
                'isdefault' => 1,
            ),
            31 => 
            array (
                'sid' => 32,
                'name' => 'seourls',
                'title' => 'Enable search engine friendly URLs?',
            'description' => 'Search engine friendly URLs change the MyBB links to shorter URLs which search engines prefer and are easier to type. showthread.php?tid=1 becomes thread-1.html. <strong>Once this setting is enabled you need to make sure you have the MyBB .htaccess in your MyBB root directory (or the equivalent for your web server). Automatic detection may not work on all servers.</strong> Please see the <a href="http://docs.mybb.com/SEF_URLs.html">MyBB Docs</a> for assistance.',
                'optionscode' => 'select
auto=Automatic Detection
yes=Enabled
no=Disabled',
                'value' => 'auto',
                'disporder' => 1,
                'gid' => 4,
                'isdefault' => 1,
            ),
            32 => 
            array (
                'sid' => 33,
                'name' => 'seourls_archive',
                'title' => 'Enable search engine friendly URLs in Archive?',
                'description' => 'Search engine friendly URLs can be used in the archive. <strong>Once this setting is enabled ensure the archive still works as expected.</strong>',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 2,
                'gid' => 4,
                'isdefault' => 1,
            ),
            33 => 
            array (
                'sid' => 34,
                'name' => 'gzipoutput',
                'title' => 'Use GZip Page Compression?',
                'description' => 'Do you want to compress pages in GZip format when they are sent to the browser? This means quicker downloads for your visitors, and less traffic usage for you.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 3,
                'gid' => 4,
                'isdefault' => 1,
            ),
            34 => 
            array (
                'sid' => 35,
                'name' => 'gziplevel',
                'title' => 'GZip Page Compression Level',
            'description' => 'Set the level for GZip Page Compression from 0-9.  (0=no compression, 9=maximum compression). A level of 4 is recommended for most installations.',
                'optionscode' => 'numeric',
                'value' => '4',
                'disporder' => 4,
                'gid' => 4,
                'isdefault' => 1,
            ),
            35 => 
            array (
                'sid' => 36,
                'name' => 'nocacheheaders',
                'title' => 'Send No Cache Headers',
                'description' => 'With this option you can prevent caching of the page by the browser.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 5,
                'gid' => 4,
                'isdefault' => 1,
            ),
            36 => 
            array (
                'sid' => 37,
                'name' => 'load',
                'title' => '*NIX Load Limiting',
                'description' => 'Limit the maximum server load before MyBB rejects people. 0 for none. Recommended limit is 5.0.',
                'optionscode' => 'text',
                'value' => '0',
                'disporder' => 6,
                'gid' => 4,
                'isdefault' => 1,
            ),
            37 => 
            array (
                'sid' => 38,
                'name' => 'tplhtmlcomments',
                'title' => 'Output template start/end comments?',
                'description' => 'This will enable or disable the output of template start/end comments in the HTML.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 7,
                'gid' => 4,
                'isdefault' => 1,
            ),
            38 => 
            array (
                'sid' => 39,
                'name' => 'use_xmlhttprequest',
                'title' => 'Enable XMLHttp request features?',
                'description' => 'This will enable or disable the XMLHttp request features.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 8,
                'gid' => 4,
                'isdefault' => 1,
            ),
            39 => 
            array (
                'sid' => 40,
                'name' => 'extraadmininfo',
                'title' => 'Advanced Stats / Debug information',
                'description' => 'Shows Server load, generation time, memory usage, etc on the bottom of all pages in the root folder. Please note that only administrators see this information.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 9,
                'gid' => 4,
                'isdefault' => 1,
            ),
            40 => 
            array (
                'sid' => 41,
                'name' => 'uploadspath',
                'title' => 'Uploads Path',
            'description' => 'The path used for all board uploads. It <b>must be chmod 777</b> (on *nix servers).',
                'optionscode' => 'text',
                'value' => '/assets/runbb/uploads',
                'disporder' => 10,
                'gid' => 4,
                'isdefault' => 1,
            ),
            41 => 
            array (
                'sid' => 42,
                'name' => 'useerrorhandling',
                'title' => 'Use Error Handling',
                'description' => 'If you do not wish to use the integrated error handling for MyBB, you may turn this option off. However, it is recommended that it stay on',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 11,
                'gid' => 4,
                'isdefault' => 1,
            ),
            42 => 
            array (
                'sid' => 43,
                'name' => 'errorlogmedium',
                'title' => 'Error Logging Medium',
                'description' => 'The type of the error handling to use.',
                'optionscode' => 'select
none=Neither
log=Log errors
email=Email errors
both=Log and email errors
',
                'value' => 'none',
                'disporder' => 12,
                'gid' => 4,
                'isdefault' => 1,
            ),
            43 => 
            array (
                'sid' => 44,
                'name' => 'errortypemedium',
                'title' => 'Error Type Medium',
                'description' => 'The type of errors to show. It is recommended you hide errors and warnings on a production forum and log them instead.',
                'optionscode' => 'select
warning=Warnings
error=Errors
both=Warnings and Errors
none=Hide Errors and Warnings
',
                'value' => 'both',
                'disporder' => 13,
                'gid' => 4,
                'isdefault' => 1,
            ),
            44 => 
            array (
                'sid' => 45,
                'name' => 'errorloglocation',
                'title' => 'Error Logging Location',
                'description' => 'The location of the log to send errors to, if specified.',
                'optionscode' => 'text',
                'value' => './error.log',
                'disporder' => 14,
                'gid' => 4,
                'isdefault' => 1,
            ),
            45 => 
            array (
                'sid' => 46,
                'name' => 'ip_forwarded_check',
                'title' => 'Scrutinize User\'s IP address?',
                'description' => 'Do you want to check a user\'s IP address for HTTP_X_FORWARDED_FOR or HTTP_X_REAL_IP headers? If you\'re unsure, set this to no.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 15,
                'gid' => 4,
                'isdefault' => 1,
            ),
            46 => 
            array (
                'sid' => 47,
                'name' => 'minifycss',
                'title' => 'Minify Stylesheets?',
                'description' => 'Do you want to minify stylesheets? Choosing yes can save bandwidth and overall provide faster page loads.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 16,
                'gid' => 4,
                'isdefault' => 1,
            ),
            47 => 
            array (
                'sid' => 48,
                'name' => 'usecdn',
                'title' => 'Use a CDN?',
            'description' => 'You can utilise a CDN (Content Delivery Network) to offload the loading of static files such as Stylesheets, JavaScript and Images. Do you wish to enable this functionality?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 17,
                'gid' => 4,
                'isdefault' => 1,
            ),
            48 => 
            array (
                'sid' => 49,
                'name' => 'cdnurl',
                'title' => 'URL to use for static files',
                'description' => 'If you have enabled the CDN option above, please enter the base URL to serve static content from. This should be a valid URL without a trailing slash.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 18,
                'gid' => 4,
                'isdefault' => 1,
            ),
            49 => 
            array (
                'sid' => 50,
                'name' => 'cdnpath',
                'title' => 'Path to store static files',
                'description' => 'If you have enabled the CDN option above, please optionally enter a full path to store static files. This is only useful for "Push" type CDNs or local sub-domain setups. This path should not have a trailing slash.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 19,
                'gid' => 4,
                'isdefault' => 1,
            ),
            50 => 
            array (
                'sid' => 51,
                'name' => 'dateformat',
                'title' => 'Date Format',
            'description' => 'The format of the dates used on the forum. This format uses the PHP date() function. We recommend not changing this unless you know what you\'re doing.',
                'optionscode' => 'text',
                'value' => 'Y-m-d',
                'disporder' => 1,
                'gid' => 5,
                'isdefault' => 1,
            ),
            51 => 
            array (
                'sid' => 52,
                'name' => 'timeformat',
                'title' => 'Time Format',
            'description' => 'The format of the times used on the forum. This format uses PHP\'s date() function. We recommend not changing this unless you know what you\'re doing.',
                'optionscode' => 'text',
                'value' => 'h:i A',
                'disporder' => 2,
                'gid' => 5,
                'isdefault' => 1,
            ),
            52 => 
            array (
                'sid' => 53,
                'name' => 'datetimesep',
                'title' => 'Date/Time Separator',
            'description' => 'Where MyBB joins date and time formats this setting is used to separate them (typically a space or comma).',
                'optionscode' => 'text',
                'value' => ', ',
                'disporder' => 3,
                'gid' => 5,
                'isdefault' => 1,
            ),
            53 => 
            array (
                'sid' => 54,
                'name' => 'regdateformat',
                'title' => 'Registered Date Format',
                'description' => 'The format used on showthread where it shows when the user registered.',
                'optionscode' => 'text',
                'value' => 'M Y',
                'disporder' => 4,
                'gid' => 5,
                'isdefault' => 1,
            ),
            54 => 
            array (
                'sid' => 55,
                'name' => 'timezoneoffset',
                'title' => 'Default Timezone Offset',
                'description' => 'Here you can set the default timezone offset for guests and members using the default offset.',
                'optionscode' => 'php
<select name=\\"upsetting[{$setting[\'name\']}]\\">
<option value=\\"-12\\" ".($setting[\'value\'] == -12?"selected=\\"selected\\"":"").">GMT -12:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -12).")</option>
<option value=\\"-11\\" ".($setting[\'value\'] == -11?"selected=\\"selected\\"":"").">GMT -11:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -11).")</option>
<option value=\\"-10\\" ".($setting[\'value\'] == -10?"selected=\\"selected\\"":"").">GMT -10:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -10).")</option>
<option value=\\"-9.5\\" ".($setting[\'value\'] == -9.5?"selected=\\"selected\\"":"").">GMT -9:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -9.5).")</option>
<option value=\\"-9\\" ".($setting[\'value\'] == -9?"selected=\\"selected\\"":"").">GMT -9:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -9).")</option>
<option value=\\"-8\\" ".($setting[\'value\'] == -8?"selected=\\"selected\\"":"").">GMT -8:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -8).")</option>
<option value=\\"-7\\" ".($setting[\'value\'] == -7?"selected=\\"selected\\"":"").">GMT -7:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -7).")</option>
<option value=\\"-6\\" ".($setting[\'value\'] == -6?"selected=\\"selected\\"":"").">GMT -6:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -6).")</option>
<option value=\\"-5\\" ".($setting[\'value\'] == -5?"selected=\\"selected\\"":"").">GMT -5:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -5).")</option>
<option value=\\"-4.5\\" ".($setting[\'value\'] == -4.5?"selected=\\"selected\\"":"").">GMT -4:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -4.5).")</option>
<option value=\\"-4\\" ".($setting[\'value\'] == -4?"selected=\\"selected\\"":"").">GMT -4:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -4).")</option>
<option value=\\"-3.5\\" ".($setting[\'value\'] == -3.5?"selected=\\"selected\\"":"").">GMT -3:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -3.5).")</option>
<option value=\\"-3\\" ".($setting[\'value\'] == -3?"selected=\\"selected\\"":"").">GMT -3:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -3).")</option>
<option value=\\"-2\\" ".($setting[\'value\'] == -2?"selected=\\"selected\\"":"").">GMT -2:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -2).")</option>
<option value=\\"-1\\" ".($setting[\'value\'] == -1?"selected=\\"selected\\"":"").">GMT -1:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, -1).")</option>
<option value=\\"0\\" ".($setting[\'value\'] == 0?"selected=\\"selected\\"":"").">GMT (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 0).")</option>
<option value=\\"+1\\" ".($setting[\'value\'] == 1?"selected=\\"selected\\"":"").">GMT +1:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 1).")</option>
<option value=\\"+2\\" ".($setting[\'value\'] == 2?"selected=\\"selected\\"":"").">GMT +2:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 2).")</option>
<option value=\\"+3\\" ".($setting[\'value\'] == 3?"selected=\\"selected\\"":"").">GMT +3:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 3).")</option>
<option value=\\"+3.5\\" ".($setting[\'value\'] == 3.5?"selected=\\"selected\\"":"").">GMT +3:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 3.5).")</option>
<option value=\\"+4\\" ".($setting[\'value\'] == 4?"selected=\\"selected\\"":"").">GMT +4:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 4).")</option>
<option value=\\"+4.5\\" ".($setting[\'value\'] == 4.5?"selected=\\"selected\\"":"").">GMT +4:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 4.5).")</option>
<option value=\\"+5\\" ".($setting[\'value\'] == 5?"selected=\\"selected\\"":"").">GMT +5:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 5).")</option>
<option value=\\"+5.5\\" ".($setting[\'value\'] == 5.5?"selected=\\"selected\\"":"").">GMT +5:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 5.5).")</option>
<option value=\\"+5.75\\" ".($setting[\'value\'] == 5.75?"selected=\\"selected\\"":"").">GMT +5:45 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 5.75).")</option>
<option value=\\"+6\\" ".($setting[\'value\'] == 6?"selected=\\"selected\\"":"").">GMT +6:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 6).")</option>
<option value=\\"+6.5\\" ".($setting[\'value\'] == 6.5?"selected=\\"selected\\"":"").">GMT +6:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 6.5).")</option>
<option value=\\"+7\\" ".($setting[\'value\'] == 7?"selected=\\"selected\\"":"").">GMT +7:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 7).")</option>
<option value=\\"+8\\" ".($setting[\'value\'] == 8?"selected=\\"selected\\"":"").">GMT +8:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 8).")</option>
<option value=\\"+9\\" ".($setting[\'value\'] == 9?"selected=\\"selected\\"":"").">GMT +9:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 9).")</option>
<option value=\\"+9.5\\" ".($setting[\'value\'] == 9.5?"selected=\\"selected\\"":"").">GMT +9:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 9.5).")</option>
<option value=\\"+10\\" ".($setting[\'value\'] == 10?"selected=\\"selected\\"":"").">GMT +10:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 10).")</option>
<option value=\\"+10.5\\" ".($setting[\'value\'] == 10.5?"selected=\\"selected\\"":"").">GMT +10:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 10.5).")</option>
<option value=\\"+11\\" ".($setting[\'value\'] == 11?"selected=\\"selected\\"":"").">GMT +11:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 11).")</option>
<option value=\\"+11.5\\" ".($setting[\'value\'] == 11.5?"selected=\\"selected\\"":"").">GMT +11:30 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 11.5).")</option>
<option value=\\"+12\\" ".($setting[\'value\'] == 12?"selected=\\"selected\\"":"").">GMT +12:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 12).")</option>
<option value=\\"+12.75\\" ".($setting[\'value\'] == 12.75?"selected=\\"selected\\"":"").">GMT +12:45 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 12.75).")</option>
<option value=\\"+13\\" ".($setting[\'value\'] == 13?"selected=\\"selected\\"":"").">GMT +13:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 13).")</option>
<option value=\\"+14\\" ".($setting[\'value\'] == 14?"selected=\\"selected\\"":"").">GMT +14:00 Hours (".$this->time->formatDate($this->bb->settings[\'timeformat\'], TIME_NOW, 14).")</option>
</select>',
                'value' => '0',
                'disporder' => 5,
                'gid' => 5,
                'isdefault' => 1,
            ),
            55 => 
            array (
                'sid' => 56,
                'name' => 'dstcorrection',
                'title' => 'Day Light Savings Time',
                'description' => 'If times are an hour out above and your timezone is selected correctly, enable day light savings time correction.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 6,
                'gid' => 5,
                'isdefault' => 1,
            ),
            56 => 
            array (
                'sid' => 57,
                'name' => 'showdescriptions',
                'title' => 'Show Forum Descriptions?',
                'description' => 'This option will allow you to turn off showing the descriptions for forums.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 6,
                'isdefault' => 1,
            ),
            57 => 
            array (
                'sid' => 58,
                'name' => 'subforumsindex',
                'title' => 'Subforums to show on Index listing',
                'description' => 'The number of subforums that you wish to show inside forums on the index and forumdisplay pages. Set to 0 to not show the subforum list',
                'optionscode' => 'numeric',
                'value' => '2',
                'disporder' => 2,
                'gid' => 6,
                'isdefault' => 1,
            ),
            58 => 
            array (
                'sid' => 59,
                'name' => 'subforumsstatusicons',
                'title' => 'Show Subforum Status Icons?',
                'description' => 'Show icons indicating whether or not a subforum contains new posts or not?  This won\'t have any effect unless you enabled subforums display on the index.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 6,
                'isdefault' => 1,
            ),
            59 => 
            array (
                'sid' => 60,
                'name' => 'hideprivateforums',
                'title' => 'Hide Private Forums?',
                'description' => 'You can hide private forums by turning this option on. This option also hides forums on the forum jump and all subforums.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 4,
                'gid' => 6,
                'isdefault' => 1,
            ),
            60 => 
            array (
                'sid' => 61,
                'name' => 'modlist',
                'title' => 'Forums\' Moderator Listing',
                'description' => 'Here you can turn on or off the listing of moderators for each forum on index.php and forumdisplay.php',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 5,
                'gid' => 6,
                'isdefault' => 1,
            ),
            61 => 
            array (
                'sid' => 62,
                'name' => 'showbirthdays',
                'title' => 'Show Today\'s Birthdays?',
                'description' => 'Do you want to show today\'s birthdays on the forum homepage?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 6,
                'gid' => 6,
                'isdefault' => 1,
            ),
            62 => 
            array (
                'sid' => 63,
                'name' => 'showbirthdayspostlimit',
                'title' => 'Only Show Birthdays with x Posts',
                'description' => 'You can choose to only display users with at least the following amount of posts. A setting of 0 will show all users who have a birthday today.',
                'optionscode' => 'numeric',
                'value' => '0',
                'disporder' => 7,
                'gid' => 6,
                'isdefault' => 1,
            ),
            63 => 
            array (
                'sid' => 64,
                'name' => 'showwol',
                'title' => 'Show Who\'s Online?',
                'description' => 'Display the currently active users on the forum home page.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 8,
                'gid' => 6,
                'isdefault' => 1,
            ),
            64 => 
            array (
                'sid' => 65,
                'name' => 'showindexstats',
                'title' => 'Show Small Stats Section',
                'description' => 'Do you want to show the total number of threads, posts, members, and the last member on the forum home?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 9,
                'gid' => 6,
                'isdefault' => 1,
            ),
            65 => 
            array (
                'sid' => 66,
                'name' => 'showforumviewing',
                'title' => 'Show x viewing forum',
                'description' => 'Displays the currently active users viewing each forum.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 10,
                'gid' => 6,
                'isdefault' => 1,
            ),
            66 => 
            array (
                'sid' => 67,
                'name' => 'threadsperpage',
                'title' => 'Threads Per Page',
                'description' => 'The number of threads to display per page on the forum display',
                'optionscode' => 'numeric',
                'value' => '20',
                'disporder' => 1,
                'gid' => 7,
                'isdefault' => 1,
            ),
            67 => 
            array (
                'sid' => 68,
                'name' => 'hottopic',
                'title' => 'Replies For Hot Topic',
                'description' => 'The number of replies that is needed for a topic to be considered \'hot\'.',
                'optionscode' => 'numeric',
                'value' => '20',
                'disporder' => 2,
                'gid' => 7,
                'isdefault' => 1,
            ),
            68 => 
            array (
                'sid' => 69,
                'name' => 'hottopicviews',
                'title' => 'Views For Hot Topic',
                'description' => 'The number of views a thread can have before it is considered \'hot\'.',
                'optionscode' => 'numeric',
                'value' => '150',
                'disporder' => 3,
                'gid' => 7,
                'isdefault' => 1,
            ),
            69 => 
            array (
                'sid' => 70,
                'name' => 'usertppoptions',
                'title' => 'User Selectable Threads Per Page',
                'description' => 'If you would like to allow users to select how many threads per page are shown in a forum, enter the options they should be able to select separated with commas. If this is left blank they will not be able to choose how many threads are shown per page.',
                'optionscode' => 'text',
                'value' => '10,15,20,25,30,40,50',
                'disporder' => 4,
                'gid' => 7,
                'isdefault' => 1,
            ),
            70 => 
            array (
                'sid' => 71,
                'name' => 'dotfolders',
                'title' => 'Use \'dot\' Icons',
                'description' => 'Do you want to show dots on the thread indicators of threads users have participated in.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 5,
                'gid' => 7,
                'isdefault' => 1,
            ),
            71 => 
            array (
                'sid' => 72,
                'name' => 'allowthreadratings',
                'title' => 'Use Thread Ratings?',
                'description' => 'Allow users to rate threads?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 6,
                'gid' => 7,
                'isdefault' => 1,
            ),
            72 => 
            array (
                'sid' => 73,
                'name' => 'browsingthisforum',
                'title' => 'Users Browsing this Forum',
                'description' => 'Here you can turn off the \'users browsing this forum\' feature.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 7,
                'gid' => 7,
                'isdefault' => 1,
            ),
            73 => 
            array (
                'sid' => 74,
                'name' => 'announcementlimit',
                'title' => 'Announcements Limit',
                'description' => 'The number of forum announcements to  show in the thread listing on the forum display pages. Set to 0 to disable announcements altogether.',
                'optionscode' => 'numeric',
                'value' => '2',
                'disporder' => 8,
                'gid' => 7,
                'isdefault' => 1,
            ),
            74 => 
            array (
                'sid' => 75,
                'name' => 'readparentforums',
                'title' => 'Attempt to Mark Parent Forums as Read',
                'description' => 'When set to yes, this setting will attempt to mark the parent forums of a sub-forum as read if there are no more unread posts. Please note that this setting can lead to a decrease in performance and multiple database queries and therefore must be treated as experimental. See the <a href="http://docs.mybb.com/165.html">MyBB Docs</a> for more information regarding this change.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 9,
                'gid' => 7,
                'isdefault' => 1,
            ),
            75 => 
            array (
                'sid' => 76,
                'name' => 'postlayout',
                'title' => 'Post Layout',
                'description' => 'Allows you to switch between the classic and new horizontal layout modes. Classic mode shows the author information to the left of the post, horizontal shows the author information above the post.',
                'optionscode' => 'radio
horizontal=Display posts using the horizontal post layout
classic=Display posts using the classic layout',
                'value' => 'horizontal',
                'disporder' => 1,
                'gid' => 8,
                'isdefault' => 1,
            ),
            76 => 
            array (
                'sid' => 77,
                'name' => 'postsperpage',
                'title' => 'Posts Per Page:',
                'description' => 'The number of posts to display per page. We recommend its not higher than 20 for people with slower connections.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 2,
                'gid' => 8,
                'isdefault' => 1,
            ),
            77 => 
            array (
                'sid' => 78,
                'name' => 'userpppoptions',
                'title' => 'User Selectable Posts Per Page',
                'description' => 'If you would like to allow users to select how many posts are shown per page in a thread, enter the options they should be able to select separated with commas. If this is left blank they will not be able to choose how many posts are shown per page.',
                'optionscode' => 'text',
                'value' => '5,10,15,20,25,30,40,50',
                'disporder' => 3,
                'gid' => 8,
                'isdefault' => 1,
            ),
            78 => 
            array (
                'sid' => 79,
                'name' => 'postmaxavatarsize',
                'title' => 'Maximum Avatar Dimensions in Posts',
                'description' => 'The maximum dimensions for avatars when being displayed in a post. If an avatar is too large, it will automatically be scaled down.',
                'optionscode' => 'text',
                'value' => '100x100',
                'disporder' => 4,
                'gid' => 8,
                'isdefault' => 1,
            ),
            79 => 
            array (
                'sid' => 80,
                'name' => 'threadreadcut',
            'title' => 'Read Threads in Database (Days)',
                'description' => 'The number of days that you wish to keep thread read information in the database. For large boards, we do not recommend a high number as the board will become slower. Set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '7',
                'disporder' => 5,
                'gid' => 8,
                'isdefault' => 1,
            ),
            80 => 
            array (
                'sid' => 81,
                'name' => 'threadusenetstyle',
                'title' => 'Usenet Style Thread View',
                'description' => 'Selecting yes will cause posts to look similar to how posts look in USENET. No will cause posts to look the modern way.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 6,
                'gid' => 8,
                'isdefault' => 1,
            ),
            81 => 
            array (
                'sid' => 82,
                'name' => 'quickreply',
                'title' => 'Show Quick Reply Form',
                'description' => 'Allows you to set whether or not the quick reply form will be shown at the bottom of threads.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 7,
                'gid' => 8,
                'isdefault' => 1,
            ),
            82 => 
            array (
                'sid' => 83,
                'name' => 'multiquote',
                'title' => 'Show Multi-quote Buttons',
                'description' => 'The multi-quote button allows users to select a series of posts then click Reply and have those posts quoted in their message.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 8,
                'gid' => 8,
                'isdefault' => 1,
            ),
            83 => 
            array (
                'sid' => 84,
                'name' => 'showsimilarthreads',
                'title' => 'Show \'Similar Threads\' Table',
                'description' => 'The Similar Threads table shows threads that are relevant to the thread being read. You can set the relevancy below.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 9,
                'gid' => 8,
                'isdefault' => 1,
            ),
            84 => 
            array (
                'sid' => 85,
                'name' => 'similarityrating',
                'title' => 'Similar Threads Relevancy Rating',
            'description' => 'This allows you to limit similar threads to ones more relevant (0 being not relevant). This number should not be over 10 and should not be set low (<5) for large forums. Does not apply to PgSQL databases.',
                'optionscode' => 'numeric',
                'value' => '1',
                'disporder' => 10,
                'gid' => 8,
                'isdefault' => 1,
            ),
            85 => 
            array (
                'sid' => 86,
                'name' => 'similarlimit',
                'title' => 'Similar Threads Limit',
                'description' => 'Here you can change the total amount of similar threads to be shown in the similar threads table. It is recommended that it is not over 15 for 56k users.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 11,
                'gid' => 8,
                'isdefault' => 1,
            ),
            86 => 
            array (
                'sid' => 87,
                'name' => 'showforumpagesbreadcrumb',
                'title' => 'Show Forum Multipage Dropdown',
                'description' => 'If the forum has more than 1 page of threads, do you want to display a dropdown pagination menu in the breadcrumbs?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 12,
                'gid' => 8,
                'isdefault' => 1,
            ),
            87 => 
            array (
                'sid' => 88,
                'name' => 'browsingthisthread',
                'title' => 'Users Browsing this Thread',
                'description' => 'Here you can turn off the \'users browsing this thread\' feature.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 13,
                'gid' => 8,
                'isdefault' => 1,
            ),
            88 => 
            array (
                'sid' => 89,
                'name' => 'delayedthreadviews',
                'title' => 'Delayed Thread View Updates',
                'description' => 'If this setting is enabled, the number of thread views for threads will be updated in the background by the task schedule system. If not enabled, thread view counters are incremented instantly.',
                'optionscode' => 'onoff',
                'value' => '0',
                'disporder' => 14,
                'gid' => 8,
                'isdefault' => 1,
            ),
            89 => 
            array (
                'sid' => 90,
                'name' => 'disableregs',
                'title' => 'Disable Registrations',
                'description' => 'Allows you to turn off the capability for users to register with one click.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 1,
                'gid' => 9,
                'isdefault' => 1,
            ),
            90 => 
            array (
                'sid' => 91,
                'name' => 'regtype',
                'title' => 'Registration Method',
                'description' => 'Please select the method of registration to use when users register.',
                'optionscode' => 'select
instant=Instant Activation
verify=Send Email Verification
randompass=Send Random Password
admin=Administrator Activation
both=Email Verification & Administrator Activation',
                'value' => 'verify',
                'disporder' => 2,
                'gid' => 9,
                'isdefault' => 1,
            ),
            91 => 
            array (
                'sid' => 92,
                'name' => 'awactialert',
                'title' => 'Awaiting Activation Notification',
                'description' => 'Do you want to show a notification to administrators about accounts awaiting activation?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 9,
                'isdefault' => 1,
            ),
            92 => 
            array (
                'sid' => 93,
                'name' => 'forcelogin',
                'title' => 'Force Users to Login',
                'description' => 'Setting this to yes will force guests to login or register in order to access the board.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 4,
                'gid' => 9,
                'isdefault' => 1,
            ),
            93 => 
            array (
                'sid' => 94,
                'name' => 'minnamelength',
                'title' => 'Minimum Username Length',
                'description' => 'The minimum number of characters a username can be when a user registers.',
                'optionscode' => 'numeric',
                'value' => '3',
                'disporder' => 5,
                'gid' => 9,
                'isdefault' => 1,
            ),
            94 => 
            array (
                'sid' => 95,
                'name' => 'maxnamelength',
                'title' => 'Maximum Username Length',
                'description' => 'The maximum number of characters a username can be when a user registers.',
                'optionscode' => 'numeric',
                'value' => '30',
                'disporder' => 6,
                'gid' => 9,
                'isdefault' => 1,
            ),
            95 => 
            array (
                'sid' => 96,
                'name' => 'minpasswordlength',
                'title' => 'Minimum Password Length',
                'description' => 'The minimum number of characters a password should contain.',
                'optionscode' => 'numeric',
                'value' => '6',
                'disporder' => 7,
                'gid' => 9,
                'isdefault' => 1,
            ),
            96 => 
            array (
                'sid' => 97,
                'name' => 'maxpasswordlength',
                'title' => 'Maximum Password Length',
                'description' => 'The maximum number of characters a password should contain.',
                'optionscode' => 'numeric',
                'value' => '30',
                'disporder' => 8,
                'gid' => 9,
                'isdefault' => 1,
            ),
            97 => 
            array (
                'sid' => 98,
                'name' => 'requirecomplexpasswords',
                'title' => 'Require a complex password?',
                'description' => 'Do you want users to use complex passwords? Complex passwords require an upper case letter, lower case letter and a number.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 9,
                'gid' => 9,
                'isdefault' => 1,
            ),
            98 => 
            array (
                'sid' => 99,
                'name' => 'betweenregstime',
                'title' => 'Time Between Registrations',
            'description' => 'The amount of time (in hours) to disallow registrations for users who have already registered an account under the same ip address. 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '24',
                'disporder' => 10,
                'gid' => 9,
                'isdefault' => 1,
            ),
            99 => 
            array (
                'sid' => 100,
                'name' => 'maxregsbetweentime',
                'title' => 'Maximum Registrations Per IP Address',
                'description' => 'This option allows you to set the maximum amount of times a certain user can register within the timeframe specified above.',
                'optionscode' => 'numeric',
                'value' => '2',
                'disporder' => 11,
                'gid' => 9,
                'isdefault' => 1,
            ),
            100 => 
            array (
                'sid' => 101,
                'name' => 'allowmultipleemails',
                'title' => 'Allow emails to be registered multiple times?',
                'description' => 'Select yes if you wish to allow users to sign up with the same email more than once otherwise select no.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 12,
                'gid' => 9,
                'isdefault' => 1,
            ),
            101 => 
            array (
                'sid' => 102,
                'name' => 'emailkeep',
                'title' => 'Users Keep Email',
                'description' => 'If a current user has an email already registered in your banned list, should they be allowed to keep it?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 13,
                'gid' => 9,
                'isdefault' => 1,
            ),
            102 => 
            array (
                'sid' => 103,
                'name' => 'hiddencaptchaimage',
                'title' => 'Display a hidden CAPTCHA',
                'description' => 'Do you want to display a hidden CAPTCHA field when a user registers? This may prevent spambots from registering on your forum.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 14,
                'gid' => 9,
                'isdefault' => 1,
            ),
            103 => 
            array (
                'sid' => 104,
                'name' => 'hiddencaptchaimagefield',
                'title' => 'Hidden CAPTCHA field',
                'description' => 'You can choose a name for your hidden CAPTCHA field below.',
                'optionscode' => 'text',
                'value' => 'email3',
                'disporder' => 15,
                'gid' => 9,
                'isdefault' => 1,
            ),
            104 => 
            array (
                'sid' => 105,
                'name' => 'usereferrals',
                'title' => 'Use Referrals System',
                'description' => 'Do you want to use the user referrals system on these forums?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 16,
                'gid' => 9,
                'isdefault' => 1,
            ),
            105 => 
            array (
                'sid' => 106,
                'name' => 'coppa',
                'title' => 'COPPA Compliance',
                'description' => 'If you wish to enable <a href="http://www.coppa.org/comply.htm">COPPA</a> support on your forums, please select the registration allowance below.',
                'optionscode' => 'select
enabled=Enabled
deny=Deny users under the age of 13
disabled=Disable this feature',
                'value' => 'disabled',
                'disporder' => 17,
                'gid' => 9,
                'isdefault' => 1,
            ),
            106 => 
            array (
                'sid' => 107,
                'name' => 'username_method',
                'title' => 'Allowed Login Methods',
                'description' => 'The login methods you wish to allow for the username field. Username only, Email only, or allow both.',
                'optionscode' => 'select
0=Username Only
1=Email Only
2=Both Username and Email',
                'value' => '0',
                'disporder' => 18,
                'gid' => 9,
                'isdefault' => 1,
            ),
            107 => 
            array (
                'sid' => 108,
                'name' => 'failedcaptchalogincount',
                'title' => 'Number of failed logins before verification required',
                'description' => 'The number of times to allow someone to attempt to login before required to enter a CAPTCHA verification. 0 to disable',
                'optionscode' => 'numeric',
                'value' => '3',
                'disporder' => 19,
                'gid' => 9,
                'isdefault' => 1,
            ),
            108 => 
            array (
                'sid' => 109,
                'name' => 'failedlogincount',
                'title' => 'Number of times to allow failed logins',
                'description' => 'The number of times to allow someone to attempt to login. 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 20,
                'gid' => 9,
                'isdefault' => 1,
            ),
            109 => 
            array (
                'sid' => 110,
                'name' => 'failedlogintime',
                'title' => 'Time between failed logins',
            'description' => 'The amount of time (in minutes) before someone can try to login again, after they have failed to login the first time. Used if value above is not 0.',
                'optionscode' => 'numeric',
                'value' => '15',
                'disporder' => 21,
                'gid' => 9,
                'isdefault' => 1,
            ),
            110 => 
            array (
                'sid' => 111,
                'name' => 'failedlogintext',
                'title' => 'Display number of failed logins',
                'description' => 'Do you wish to display a line of text telling the user how many more login attempts they have?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 22,
                'gid' => 9,
                'isdefault' => 1,
            ),
            111 => 
            array (
                'sid' => 112,
                'name' => 'regtime',
                'title' => 'Minimum Registration Time',
                'description' => 'The minimum time in seconds a user must take to fill out the registration form.  Used to prevent automated bot signups.  Setting this to 0 to disables the function.',
                'optionscode' => 'numeric',
                'value' => '15',
                'disporder' => 23,
                'gid' => 9,
                'isdefault' => 1,
            ),
            112 => 
            array (
                'sid' => 113,
                'name' => 'securityquestion',
                'title' => 'Show Security Question',
                'description' => 'Do you wish to show a question that users must answer when registering?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 24,
                'gid' => 9,
                'isdefault' => 1,
            ),
            113 => 
            array (
                'sid' => 114,
                'name' => 'sigmycode',
                'title' => 'Allow MyCode in Signatures',
                'description' => 'Do you want to allow MyCode to be used in users\' signatures?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 10,
                'isdefault' => 1,
            ),
            114 => 
            array (
                'sid' => 115,
                'name' => 'sigcountmycode',
                'title' => 'MyCode affects signature length',
                'description' => 'Do you want MyCode to be counted as part of the limit when users use MyCode in their signature?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 2,
                'gid' => 10,
                'isdefault' => 1,
            ),
            115 => 
            array (
                'sid' => 116,
                'name' => 'sigsmilies',
                'title' => 'Allow Smilies in Signatures',
                'description' => 'Do you want to allow smilies to be used in users\' signatures?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 10,
                'isdefault' => 1,
            ),
            116 => 
            array (
                'sid' => 117,
                'name' => 'sightml',
                'title' => 'Allow HTML in Signatures',
                'description' => 'Do you want to allow HTML to be used in users\' signatures?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 4,
                'gid' => 10,
                'isdefault' => 1,
            ),
            117 => 
            array (
                'sid' => 118,
                'name' => 'sigimgcode',
                'title' => 'Allow [img] Code in Signatures',
                'description' => 'Do you want to allow [img] code to be used in users\' signatures?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 5,
                'gid' => 10,
                'isdefault' => 1,
            ),
            118 => 
            array (
                'sid' => 119,
                'name' => 'maxsigimages',
                'title' => 'Maximum Number of Images per Signature',
            'description' => 'Enter the maximum number of images (including smilies) a user can put in their signature. Set to 0 to disable images in signatures altogether.',
                'optionscode' => 'numeric',
                'value' => '2',
                'disporder' => 6,
                'gid' => 10,
                'isdefault' => 1,
            ),
            119 => 
            array (
                'sid' => 120,
                'name' => 'siglength',
                'title' => 'Length limit in Signatures',
                'description' => 'The maximum number of characters a user can place in a signature.',
                'optionscode' => 'numeric',
                'value' => '255',
                'disporder' => 7,
                'gid' => 10,
                'isdefault' => 1,
            ),
            120 => 
            array (
                'sid' => 121,
                'name' => 'hidesignatures',
                'title' => 'Hide Signatures To Groups',
                'description' => 'Select the usergroups you wish to hide signatures from.',
                'optionscode' => 'groupselect',
                'value' => '',
                'disporder' => 8,
                'gid' => 10,
                'isdefault' => 1,
            ),
            121 => 
            array (
                'sid' => 122,
                'name' => 'hidewebsite',
                'title' => 'Hide Websites To Groups',
                'description' => 'Select the usergroups you wish to hide websites from.',
                'optionscode' => 'groupselect',
                'value' => '',
                'disporder' => 9,
                'gid' => 10,
                'isdefault' => 1,
            ),
            122 => 
            array (
                'sid' => 123,
                'name' => 'useravatar',
                'title' => 'Default User Avatar',
                'description' => 'If the user does not set a custom avatar this image will be used instead.',
                'optionscode' => 'text',
                'value' => 'images/default_avatar.png',
                'disporder' => 10,
                'gid' => 10,
                'isdefault' => 1,
            ),
            123 => 
            array (
                'sid' => 124,
                'name' => 'useravatardims',
                'title' => 'Default Avatar Dimensions',
            'description' => 'The dimensions of the default avatar; width by height (e.g. 40|40).',
                'optionscode' => 'text',
                'value' => '100|100',
                'disporder' => 11,
                'gid' => 10,
                'isdefault' => 1,
            ),
            124 => 
            array (
                'sid' => 125,
                'name' => 'useravatarrating',
                'title' => 'Gravatar Rating',
                'description' => 'Allows you to set the maximum rating for Gravatars if a user chooses to use one. If a user avatar is higher than this rating a default avatar will be used. The ratings are:

<ul>
<li><strong>G</strong>: suitable for display on all websites with any audience type</li>
<li><strong>PG</strong>: may contain rude gestures, provocatively dressed individuals, the lesser swear words or mild violence</li>
<li><strong>R</strong>: may contain such things as harsh profanity, intense violence, nudity or hard drug use</li>
<li><strong>X</strong>: may contain hardcore sexual imagery or extremely disturbing violence</li>
</ul>',
                'optionscode' => 'select
g=G
pg=PG
r=R
x=X',
                'value' => 'g',
                'disporder' => 12,
                'gid' => 10,
                'isdefault' => 1,
            ),
            125 => 
            array (
                'sid' => 126,
                'name' => 'maxavatardims',
                'title' => 'Maximum Avatar Dimensions',
                'description' => 'The maximum dimensions that an avatar can be, in the format of width<b>x</b>height. If this is left blank then there will be no dimension restriction.',
                'optionscode' => 'text',
                'value' => '100x100',
                'disporder' => 13,
                'gid' => 10,
                'isdefault' => 1,
            ),
            126 => 
            array (
                'sid' => 127,
                'name' => 'avatarsize',
                'title' => 'Max Uploaded Avatar Size',
            'description' => 'Maximum file size (in kilobytes) of uploaded avatars.',
                'optionscode' => 'numeric',
                'value' => '25',
                'disporder' => 14,
                'gid' => 10,
                'isdefault' => 1,
            ),
            127 => 
            array (
                'sid' => 128,
                'name' => 'avatarresizing',
                'title' => 'Avatar Resizing Mode',
                'description' => 'If you wish to automatically resize all large avatars, provide users the option of resizing their avatar, or not resize avatars at all you can change this setting.',
                'optionscode' => 'select
auto=Automatically resize large avatars
user=Give users the choice of resizing large avatars
disabled=Disable this feature',
                'value' => 'auto',
                'disporder' => 15,
                'gid' => 10,
                'isdefault' => 1,
            ),
            128 => 
            array (
                'sid' => 129,
                'name' => 'avataruploadpath',
                'title' => 'Avatar Upload Path',
            'description' => 'This is the path where custom avatars will be uploaded to. This directory <b>must be chmod 777</b> (writable) for uploads to work.',
                'optionscode' => 'text',
                'value' => '/assets/runbb/uploads/avatars',
                'disporder' => 16,
                'gid' => 10,
                'isdefault' => 1,
            ),
            129 => 
            array (
                'sid' => 130,
                'name' => 'customtitlemaxlength',
                'title' => 'Custom User Title Maximum Length',
                'description' => 'Maximum length a user can enter for the custom user title.',
                'optionscode' => 'numeric',
                'value' => '40',
                'disporder' => 17,
                'gid' => 10,
                'isdefault' => 1,
            ),
            130 => 
            array (
                'sid' => 131,
                'name' => 'allowaway',
                'title' => 'Allow Away Statuses?',
                'description' => 'Should users be allowed to set their status to \'Away\' with a custom reason & return date?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 18,
                'gid' => 10,
                'isdefault' => 1,
            ),
            131 => 
            array (
                'sid' => 132,
                'name' => 'allowbuddyonly',
                'title' => 'Allow Buddy-Only Messaging?',
                'description' => 'Allow users to send private messages only to people on their buddy list?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 19,
                'gid' => 10,
                'isdefault' => 1,
            ),
            132 => 
            array (
                'sid' => 133,
                'name' => 'minmessagelength',
                'title' => 'Minimum Message Length',
                'description' => 'The minimum number of characters to post.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 1,
                'gid' => 11,
                'isdefault' => 1,
            ),
            133 => 
            array (
                'sid' => 134,
                'name' => 'maxmessagelength',
                'title' => 'Maximum Message Length',
            'description' => 'The maximum number of characters (bytes) to allow in a message. A setting of 0 allows an unlimited length.
<br />This should correlate with the message column type in the posts database table, and adjust accordingly. Below are the maximum lengths for each column type.
<ul>
<li>TEXT: 65535 (default)</li>
<li>MEDIUMTEXT: 16777215</li>
<li>LONGTEXT: 4294967295</li>
</ul>',
                'optionscode' => 'numeric',
                'value' => '65535',
                'disporder' => 2,
                'gid' => 11,
                'isdefault' => 1,
            ),
            134 => 
            array (
                'sid' => 135,
                'name' => 'mycodemessagelength',
                'title' => 'MyCode Affects Minimum Message Length?',
                'description' => 'Do you want MyCode to be counted as part of the minimum limit when users use MyCode in messages?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 11,
                'isdefault' => 1,
            ),
            135 => 
            array (
                'sid' => 136,
                'name' => 'postfloodcheck',
                'title' => 'Post Flood Checking',
                'description' => 'Set to on if you want to enable flood checking for posts. Specify the time between posts below.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 4,
                'gid' => 11,
                'isdefault' => 1,
            ),
            136 => 
            array (
                'sid' => 137,
                'name' => 'postfloodsecs',
                'title' => 'Post Flood Time',
            'description' => 'Set the time (in seconds) users have to wait between posting, to be in effect; the option above must be on.',
                'optionscode' => 'numeric',
                'value' => '60',
                'disporder' => 5,
                'gid' => 11,
                'isdefault' => 1,
            ),
            137 => 
            array (
                'sid' => 138,
                'name' => 'postmergemins',
                'title' => 'Post Merge Time',
            'description' => 'With this enabled, posts posted within x minutes by the same author right after each other, will be merged. Set the time limit (in minutes) to merge posts. Set to 0 or leave blank to disable this feature. Default: 60',
                'optionscode' => 'numeric',
                'value' => '60',
                'disporder' => 6,
                'gid' => 11,
                'isdefault' => 1,
            ),
            138 => 
            array (
                'sid' => 139,
                'name' => 'postmergefignore',
                'title' => 'Merge Forums to Ignore',
                'description' => 'Forums to exclude from the auto merge feature. Leave blank to disable.',
                'optionscode' => 'forumselect',
                'value' => '',
                'disporder' => 7,
                'gid' => 11,
                'isdefault' => 1,
            ),
            139 => 
            array (
                'sid' => 140,
                'name' => 'postmergeuignore',
                'title' => 'Merge User Groups to Ignore',
                'description' => 'Select the usergroups which should be excluded from the merge feature.',
                'optionscode' => 'groupselect',
                'value' => '4',
                'disporder' => 8,
                'gid' => 11,
                'isdefault' => 1,
            ),
            140 => 
            array (
                'sid' => 141,
                'name' => 'postmergesep',
                'title' => 'Merge Separator',
                'description' => 'The Separator to be used when merging two message Default: \'[hr]\'',
                'optionscode' => 'text',
                'value' => '[hr]',
                'disporder' => 9,
                'gid' => 11,
                'isdefault' => 1,
            ),
            141 => 
            array (
                'sid' => 142,
                'name' => 'logip',
                'title' => 'Show Posting IP Addresses',
                'description' => 'Do you wish to show ip addresses of users who post, and who you want to show ip addresses to?',
                'optionscode' => 'radio
no=Do not show IP
hide=Show to Admins & Mods
show=Show to all Users',
                'value' => 'hide',
                'disporder' => 10,
                'gid' => 11,
                'isdefault' => 1,
            ),
            142 => 
            array (
                'sid' => 143,
                'name' => 'showeditedby',
                'title' => 'Show \'edited by\' Messages',
                'description' => 'Once a post is edited by a regular user, do you want to show the edited by message?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 11,
                'gid' => 11,
                'isdefault' => 1,
            ),
            143 => 
            array (
                'sid' => 144,
                'name' => 'showeditedbyadmin',
                'title' => 'Show \'edited by\' Message for Administrators',
                'description' => 'Do you want to show edited by messages for posts created by administrators?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 12,
                'gid' => 11,
                'isdefault' => 1,
            ),
            144 => 
            array (
                'sid' => 145,
                'name' => 'maxpostimages',
                'title' => 'Maximum Images per Post',
            'description' => 'Enter the maximum number of images (including smilies) a user can put in their post. Set to 0 to disable this.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 13,
                'gid' => 11,
                'isdefault' => 1,
            ),
            145 => 
            array (
                'sid' => 146,
                'name' => 'maxpostvideos',
                'title' => 'Maximum Videos per Post',
                'description' => 'Enter the maximum number of videos a user can put in their post. Set to 0 to disable this.',
                'optionscode' => 'numeric',
                'value' => '1',
                'disporder' => 14,
                'gid' => 11,
                'isdefault' => 1,
            ),
            146 => 
            array (
                'sid' => 147,
                'name' => 'subscribeexcerpt',
                'title' => 'Amount of Characters for Subscription Previews',
                'description' => 'How many characters of the post do you want to send with the email notification of a new reply?',
                'optionscode' => 'numeric',
                'value' => '100',
                'disporder' => 15,
                'gid' => 11,
                'isdefault' => 1,
            ),
            147 => 
            array (
                'sid' => 148,
                'name' => 'maxquotedepth',
                'title' => 'Maximum Nested Quote Tags',
                'description' => 'The maximum depth level for quote tags. When quote-replying to a post, all nested quotes which exceed this threshold are automatically removed. Note that this value is only enforced when quote-replying - it is still possible to manually enter quotes to exceed this limit, and this setting will not affect posts already submitted. Set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 16,
                'gid' => 11,
                'isdefault' => 1,
            ),
            148 => 
            array (
                'sid' => 149,
                'name' => 'polloptionlimit',
                'title' => 'Maximum Poll Option Length',
            'description' => 'The maximum length that each poll option can be. (Set to 0 to disable).',
                'optionscode' => 'numeric',
                'value' => '250',
                'disporder' => 17,
                'gid' => 11,
                'isdefault' => 1,
            ),
            149 => 
            array (
                'sid' => 150,
                'name' => 'maxpolloptions',
                'title' => 'Maximum Number of Poll Options',
                'description' => 'The maximum number of options for polls that users can post.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 18,
                'gid' => 11,
                'isdefault' => 1,
            ),
            150 => 
            array (
                'sid' => 151,
                'name' => 'polltimelimit',
                'title' => 'Poll Time Limit',
            'description' => 'The number of hours until regular users cannot add a poll to their own threads (if they have the permission). Enter 0 (zero) for no limit.',
                'optionscode' => 'numeric',
                'value' => '12',
                'disporder' => 19,
                'gid' => 11,
                'isdefault' => 1,
            ),
            151 => 
            array (
                'sid' => 152,
                'name' => 'threadreview',
                'title' => 'Show Thread Review',
                'description' => 'Show recent posts when creating a new reply?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 20,
                'gid' => 11,
                'isdefault' => 1,
            ),
            152 => 
            array (
                'sid' => 153,
                'name' => 'alloweditreason',
                'title' => 'Allow Edit Reason',
                'description' => 'Do you want to allow users the ability to add an optional reason why they\'ve edited their post?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 21,
                'gid' => 11,
                'isdefault' => 1,
            ),
            153 => 
            array (
                'sid' => 154,
                'name' => 'soft_delete',
                'title' => 'Enable Soft Delete for Users',
                'description' => 'If enabled, posts and threads deleted by users will be hidden and can be restored by moderators. Otherwise, these posts and threads will be deleted permanently.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 22,
                'gid' => 11,
                'isdefault' => 1,
            ),
            154 => 
            array (
                'sid' => 155,
                'name' => 'enableattachments',
                'title' => 'Enable Attachment Functionality',
                'description' => 'If you wish to disable attachments on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 12,
                'isdefault' => 1,
            ),
            155 => 
            array (
                'sid' => 156,
                'name' => 'maxattachments',
                'title' => 'Maximum Attachments Per Post',
                'description' => 'The maximum number of attachments a user is allowed to upload per post.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 2,
                'gid' => 12,
                'isdefault' => 1,
            ),
            156 => 
            array (
                'sid' => 157,
                'name' => 'attachthumbnails',
                'title' => 'Show Attached Thumbnails in Posts',
                'description' => 'How do you want images to be shown in posts?',
                'optionscode' => 'radio
yes=Thumbnail
no=Full Size Image
download=As Download Link',
                'value' => 'yes',
                'disporder' => 3,
                'gid' => 12,
                'isdefault' => 1,
            ),
            157 => 
            array (
                'sid' => 158,
                'name' => 'attachthumbh',
                'title' => 'Attached Thumbnail Maximum Height',
                'description' => 'Enter the height that attached thumbnails should be generated at.',
                'optionscode' => 'numeric',
                'value' => '200',
                'disporder' => 4,
                'gid' => 12,
                'isdefault' => 1,
            ),
            158 => 
            array (
                'sid' => 159,
                'name' => 'attachthumbw',
                'title' => 'Attached Thumbnail Maximum Width',
                'description' => 'Enter the width that attached thumbnails should be generated at.',
                'optionscode' => 'numeric',
                'value' => '200',
                'disporder' => 5,
                'gid' => 12,
                'isdefault' => 1,
            ),
            159 => 
            array (
                'sid' => 160,
                'name' => 'enablememberlist',
                'title' => 'Enable Member List Functionality',
                'description' => 'If you wish to disable the member list on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 13,
                'isdefault' => 1,
            ),
            160 => 
            array (
                'sid' => 161,
                'name' => 'membersperpage',
                'title' => 'Members Per Page',
                'description' => 'The number of members to show per page on the member list.',
                'optionscode' => 'numeric',
                'value' => '20',
                'disporder' => 2,
                'gid' => 13,
                'isdefault' => 1,
            ),
            161 => 
            array (
                'sid' => 162,
                'name' => 'default_memberlist_sortby',
                'title' => 'Default Sort Field',
                'description' => 'Select the field that you want members to be sorted by default.',
                'optionscode' => 'select
regdate=Registration Date
postnum=Post Count
username=Username
lastvisit=Last Visit',
                'value' => 'regdate',
                'disporder' => 3,
                'gid' => 13,
                'isdefault' => 1,
            ),
            162 => 
            array (
                'sid' => 163,
                'name' => 'default_memberlist_order',
                'title' => 'Default Sort Order',
                'description' => 'Select the order that you want members to be sorted by default.<br />Ascending: A-Z / beginning-end<br />Descending: Z-A / end-beginning',
                'optionscode' => 'select
ascending=Ascending
descending=Descending',
                'value' => 'ascending',
                'disporder' => 4,
                'gid' => 13,
                'isdefault' => 1,
            ),
            163 => 
            array (
                'sid' => 164,
                'name' => 'memberlistmaxavatarsize',
                'title' => 'Maximum Display Avatar Dimensions',
                'description' => 'The maximum dimensions for avatars when being displayed in the member list. If an avatar is too large, it will automatically be scaled down.',
                'optionscode' => 'text',
                'value' => '70x70',
                'disporder' => 5,
                'gid' => 13,
                'isdefault' => 1,
            ),
            164 => 
            array (
                'sid' => 165,
                'name' => 'enablereputation',
                'title' => 'Enable Reputation Functionality',
                'description' => 'If you wish to disable the reputation system on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 14,
                'isdefault' => 1,
            ),
            165 => 
            array (
                'sid' => 166,
                'name' => 'posrep',
                'title' => 'Allow Positive Reputation',
                'description' => 'Allow users to give others positive reputation?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 2,
                'gid' => 14,
                'isdefault' => 1,
            ),
            166 => 
            array (
                'sid' => 167,
                'name' => 'negrep',
                'title' => 'Allow Negative Reputation',
                'description' => 'Allow users to give others negative reputation?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 14,
                'isdefault' => 1,
            ),
            167 => 
            array (
                'sid' => 168,
                'name' => 'neurep',
                'title' => 'Allow Neutral Reputation',
                'description' => 'Allow users to give others neutral reputation?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 4,
                'gid' => 14,
                'isdefault' => 1,
            ),
            168 => 
            array (
                'sid' => 169,
                'name' => 'multirep',
                'title' => 'Allow Multiple Reputation',
                'description' => 'Can users give multiple reputations to the same user?<br />Note: Does not effect "Post" reputation',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 5,
                'gid' => 14,
                'isdefault' => 1,
            ),
            169 => 
            array (
                'sid' => 170,
                'name' => 'postrep',
                'title' => 'Allow Post Reputations',
                'description' => 'Enable reputation to be linked to posts?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 6,
                'gid' => 14,
                'isdefault' => 1,
            ),
            170 => 
            array (
                'sid' => 171,
                'name' => 'repsperpage',
                'title' => 'Reputation Comments Per Page',
                'description' => 'Here you can enter the number of reputation comments to show per page on the reputation system',
                'optionscode' => 'numeric',
                'value' => '15',
                'disporder' => 7,
                'gid' => 14,
                'isdefault' => 1,
            ),
            171 => 
            array (
                'sid' => 172,
                'name' => 'maxreplength',
                'title' => 'Maximum Reputation Length',
                'description' => 'The maximum number of characters a reputation can be.',
                'optionscode' => 'numeric',
                'value' => '300',
                'disporder' => 8,
                'gid' => 14,
                'isdefault' => 1,
            ),
            172 => 
            array (
                'sid' => 173,
                'name' => 'minreplength',
                'title' => 'Minimum Reputation Length',
                'description' => 'The minimum number of characters a reputation can be.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 9,
                'gid' => 14,
                'isdefault' => 1,
            ),
            173 => 
            array (
                'sid' => 174,
                'name' => 'enablewarningsystem',
                'title' => 'Enable Warning System?',
                'description' => 'Set to no to completely disable the warning system.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 15,
                'isdefault' => 1,
            ),
            174 => 
            array (
                'sid' => 175,
                'name' => 'allowcustomwarnings',
                'title' => 'Allow Custom Warning Types?',
                'description' => 'Allow a custom reason and amount of points to be specified by those with permissions to warn users.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 2,
                'gid' => 15,
                'isdefault' => 1,
            ),
            175 => 
            array (
                'sid' => 176,
                'name' => 'canviewownwarning',
                'title' => 'Can Users View Own Warnings?',
                'description' => 'Set to yes to allow users to view recent warnings in their User CP and show their warning level to them in their profile.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 15,
                'isdefault' => 1,
            ),
            176 => 
            array (
                'sid' => 177,
                'name' => 'allowanonwarningpms',
                'title' => 'Allow Anonymous Warning PMs',
                'description' => 'Allow moderators to send anonymous warning PMs.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 4,
                'gid' => 15,
                'isdefault' => 1,
            ),
            177 => 
            array (
                'sid' => 178,
                'name' => 'maxwarningpoints',
                'title' => 'Maximum Warning Points',
                'description' => 'The maximum warning points that can be given to a user before it is considered a warning level of 100%.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 5,
                'gid' => 15,
                'isdefault' => 1,
            ),
            178 => 
            array (
                'sid' => 179,
                'name' => 'enablepms',
                'title' => 'Enable Private Messaging Functionality',
                'description' => 'If you wish to disable the private messaging system on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 16,
                'isdefault' => 1,
            ),
            179 => 
            array (
                'sid' => 180,
                'name' => 'pmquickreply',
                'title' => 'Show Quick Reply Form',
                'description' => 'Allows you to set whether or not the quick reply form will be shown at the bottom of PMs.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 2,
                'gid' => 16,
                'isdefault' => 1,
            ),
            180 => 
            array (
                'sid' => 181,
                'name' => 'pmsallowhtml',
                'title' => 'Allow HTML',
                'description' => 'Selecting yes will allow HTML to be used in private messages.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 3,
                'gid' => 16,
                'isdefault' => 1,
            ),
            181 => 
            array (
                'sid' => 182,
                'name' => 'pmsallowmycode',
                'title' => 'Allow MyCode',
                'description' => 'Selecting yes will allow MyCode to be used in private messages.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 4,
                'gid' => 16,
                'isdefault' => 1,
            ),
            182 => 
            array (
                'sid' => 183,
                'name' => 'pmsallowsmilies',
                'title' => 'Allow Smilies',
                'description' => 'Selecting yes will allow Smilies to be used in private messages.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 5,
                'gid' => 16,
                'isdefault' => 1,
            ),
            183 => 
            array (
                'sid' => 184,
                'name' => 'pmsallowimgcode',
                'title' => 'Allow [img] Code',
                'description' => 'Selecting yes will allow [img] Code to be used in private messages.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 6,
                'gid' => 16,
                'isdefault' => 1,
            ),
            184 => 
            array (
                'sid' => 185,
                'name' => 'pmsallowvideocode',
                'title' => 'Allow [video] Code',
                'description' => 'Selecting yes will allow [video] Code to be used in private messages.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 7,
                'gid' => 16,
                'isdefault' => 1,
            ),
            185 => 
            array (
                'sid' => 186,
                'name' => 'pmfloodsecs',
                'title' => 'Private Message Flood Time',
            'description' => 'Set the time (in seconds) users have to wait between sending messages, to be in effect; set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '60',
                'disporder' => 8,
                'gid' => 16,
                'isdefault' => 1,
            ),
            186 => 
            array (
                'sid' => 187,
                'name' => 'showpmip',
                'title' => 'Show Private Message IP Addresses',
                'description' => 'Do you wish to show ip addresses of users who send private messages, and who you want to show ip addresses to?',
                'optionscode' => 'radio
no=Do not show IP
hide=Show to Admins & Mods
show=Show to all Users',
                'value' => 'hide',
                'disporder' => 9,
                'gid' => 16,
                'isdefault' => 1,
            ),
            187 => 
            array (
                'sid' => 188,
                'name' => 'maxpmquotedepth',
                'title' => 'Maximum PM Nested Quote Tags',
                'description' => 'The maximum depth level for quote tags. When quote-replying to a PM, all nested quotes which exceed this threshold are automatically removed. Note that this value is only enforced when quote-replying - it is still possible to manually enter quotes to exceed this limit, and this setting will not affect PMs already sent. Set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 10,
                'gid' => 16,
                'isdefault' => 1,
            ),
            188 => 
            array (
                'sid' => 189,
                'name' => 'enablecalendar',
                'title' => 'Enable Calendar Functionality',
                'description' => 'If you wish to disable the calendar on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 17,
                'isdefault' => 1,
            ),
            189 => 
            array (
                'sid' => 190,
                'name' => 'wolcutoffmins',
            'title' => 'Cut-off Time (mins)',
                'description' => 'The number of minutes before a user is marked offline. Recommended: 15.',
                'optionscode' => 'numeric',
                'value' => '15',
                'disporder' => 1,
                'gid' => 18,
                'isdefault' => 1,
            ),
            190 => 
            array (
                'sid' => 191,
                'name' => 'refreshwol',
            'title' => 'Refresh Who\'s online page Time (mins)',
                'description' => 'The number of minutes before the "Who\'s online" page refreshes. 0 is disabled.',
                'optionscode' => 'numeric',
                'value' => '1',
                'disporder' => 2,
                'gid' => 18,
                'isdefault' => 1,
            ),
            191 => 
            array (
                'sid' => 192,
                'name' => 'wolorder',
                'title' => 'Who\'s Online Order',
                'description' => 'List the online users by username or last activity. Note: This setting only takes effect on the portal and index pages.',
                'optionscode' => 'select
username=Order By Username (ASC)
activity=Order By Last Activity (DESC)',
                'value' => 'username',
                'disporder' => 3,
                'gid' => 18,
                'isdefault' => 1,
            ),
            192 => 
            array (
                'sid' => 193,
                'name' => 'enablepruning',
                'title' => 'Enable user pruning?',
                'description' => 'Set to on to prune users by the specified criteria below.',
                'optionscode' => 'onoff',
                'value' => '0',
                'disporder' => 1,
                'gid' => 19,
                'isdefault' => 1,
            ),
            193 => 
            array (
                'sid' => 194,
                'name' => 'enableprunebyposts',
                'title' => 'Prune user by post count?',
                'description' => 'Set to on to prune users by their number of posts.',
                'optionscode' => 'onoff',
                'value' => '0',
                'disporder' => 2,
                'gid' => 19,
                'isdefault' => 1,
            ),
            194 => 
            array (
                'sid' => 195,
                'name' => 'prunepostcount',
                'title' => 'Post count to prune by',
                'description' => 'Set to prune users less then a specified number of posts.',
                'optionscode' => 'numeric',
                'value' => '0',
                'disporder' => 3,
                'gid' => 19,
                'isdefault' => 1,
            ),
            195 => 
            array (
                'sid' => 196,
                'name' => 'prunepostcountall',
                'title' => 'Count all posts?',
                'description' => 'If set to yes, posts from forums which don\'t increase users\' post count will also be considered while pruning.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 4,
                'gid' => 19,
                'isdefault' => 1,
            ),
            196 => 
            array (
                'sid' => 197,
                'name' => 'dayspruneregistered',
                'title' => 'Days registered before pruning by post count',
                'description' => 'Set to prune users whom have been registered for at least the given amount of days.',
                'optionscode' => 'numeric',
                'value' => '90',
                'disporder' => 5,
                'gid' => 19,
                'isdefault' => 1,
            ),
            197 => 
            array (
                'sid' => 198,
                'name' => 'pruneunactived',
                'title' => 'Prune unactivated users?',
                'description' => 'Set to on to prune users whom are unactived.',
                'optionscode' => 'onoff',
                'value' => '0',
                'disporder' => 6,
                'gid' => 19,
                'isdefault' => 1,
            ),
            198 => 
            array (
                'sid' => 199,
                'name' => 'dayspruneunactivated',
                'title' => 'Days registered before pruning unactivated users',
                'description' => 'Set to prune users whom have been unactived for at least the given amount of days.',
                'optionscode' => 'numeric',
                'value' => '90',
                'disporder' => 7,
                'gid' => 19,
                'isdefault' => 1,
            ),
            199 => 
            array (
                'sid' => 200,
                'name' => 'prunethreads',
                'title' => 'Prune User Posts/Threads',
                'description' => 'For the users that are going to be removed, do you want to remove all of their posts/threads?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 8,
                'gid' => 19,
                'isdefault' => 1,
            ),
            200 => 
            array (
                'sid' => 201,
                'name' => 'portal',
                'title' => 'Enable Portal',
                'description' => 'If you wish to disable the portal on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 20,
                'isdefault' => 1,
            ),
            201 => 
            array (
                'sid' => 202,
                'name' => 'portal_announcementsfid',
                'title' => 'Forums to pull announcements from',
                'description' => 'Please select the forums you wish to pull the announcements from.',
                'optionscode' => 'forumselect',
                'value' => '-1',
                'disporder' => 2,
                'gid' => 20,
                'isdefault' => 1,
            ),
            202 => 
            array (
                'sid' => 203,
                'name' => 'portal_numannouncements',
                'title' => 'Number of announcements to show',
                'description' => 'Please enter the number of announcements to show per page on the main page.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 3,
                'gid' => 20,
                'isdefault' => 1,
            ),
            203 => 
            array (
                'sid' => 204,
                'name' => 'portal_showwelcome',
                'title' => 'Show the Welcome box',
                'description' => 'Do you want to show the welcome box to visitors / users.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 4,
                'gid' => 20,
                'isdefault' => 1,
            ),
            204 => 
            array (
                'sid' => 205,
                'name' => 'portal_showpms',
                'title' => 'Show the number of PMs to users',
                'description' => 'Do you want to show the number of private messages the current user has in their pm system.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 5,
                'gid' => 20,
                'isdefault' => 1,
            ),
            205 => 
            array (
                'sid' => 206,
                'name' => 'portal_showstats',
                'title' => 'Show forum statistics',
                'description' => 'Do you want to show the total number of posts, threads, members and the last registered member on the portal page?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 6,
                'gid' => 20,
                'isdefault' => 1,
            ),
            206 => 
            array (
                'sid' => 207,
                'name' => 'portal_showwol',
                'title' => 'Show Who\'s Online',
                'description' => 'Do you want to show the \'Who\'s online\' information to users when they visit the portal page?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 7,
                'gid' => 20,
                'isdefault' => 1,
            ),
            207 => 
            array (
                'sid' => 208,
                'name' => 'portal_showsearch',
                'title' => 'Show Search Box',
                'description' => 'Do you want to show the search box, allowing users to quickly search the forums on the portal?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 8,
                'gid' => 20,
                'isdefault' => 1,
            ),
            208 => 
            array (
                'sid' => 209,
                'name' => 'portal_showdiscussions',
                'title' => 'Show Latest Discussions',
                'description' => 'Do you wish to show the current forum discussions on the portal page?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 9,
                'gid' => 20,
                'isdefault' => 1,
            ),
            209 => 
            array (
                'sid' => 210,
                'name' => 'portal_showdiscussionsnum',
                'title' => 'Number of latest discussions to show',
                'description' => 'Please enter the number of current forum discussions to show on the portal page.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 10,
                'gid' => 20,
                'isdefault' => 1,
            ),
            210 => 
            array (
                'sid' => 211,
                'name' => 'portal_excludediscussion',
                'title' => 'Forums to exclude latest discussions from',
                'description' => 'Please select the forums you wish to exclude from the current forum discussions box.',
                'optionscode' => 'forumselect',
                'value' => '',
                'disporder' => 11,
                'gid' => 20,
                'isdefault' => 1,
            ),
            211 => 
            array (
                'sid' => 212,
                'name' => 'searchtype',
                'title' => 'Search Type',
            'description' => 'Please select the type of search system you wish to use. You can either chose between "Standard", or "Full Text" (depending on your database). Fulltext searching is more powerful than the standard MyBB searching and quicker too.',
                'optionscode' => 'php
<select name=\\"upsetting[{$setting[\'name\']}]\\"><option value=\\"standard\\">".($this->lang->setting_searchtype_standard?$this->lang->setting_searchtype_standard:"Standard")."</option>".($this->db->supports_fulltext("threads") && $this->db->supports_fulltext_boolean("posts")?"<option value=\\"fulltext\\"".($setting[\'value\']=="fulltext"?" selected=\\"selected\\"":"").">".($this->lang->setting_searchtype_fulltext?$this->lang->setting_searchtype_fulltext:"Full Text")."</option>":"")."</select>',
                'value' => 'standard',
                'disporder' => 1,
                'gid' => 21,
                'isdefault' => 1,
            ),
            212 => 
            array (
                'sid' => 213,
                'name' => 'searchfloodtime',
            'title' => 'Search Flood Time (seconds)',
                'description' => 'Enter the time in seconds for the minimum allowed interval for searching. This prevents users from overloading your server by constantly performing searches. Set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '30',
                'disporder' => 2,
                'gid' => 21,
                'isdefault' => 1,
            ),
            213 => 
            array (
                'sid' => 214,
                'name' => 'minsearchword',
                'title' => 'Minimum Search Word Length',
            'description' => 'Enter the minimum number of characters an individual word in a search query can be. Set to 0 to disable (and accept the hard limit default of 3 for standard searching and 4 for MySQL fulltext searching). If you use MySQL fulltext searching and set this lower than the MySQL setting - MySQL will override it.',
                'optionscode' => 'numeric',
                'value' => '0',
                'disporder' => 3,
                'gid' => 21,
                'isdefault' => 1,
            ),
            214 => 
            array (
                'sid' => 215,
                'name' => 'searchhardlimit',
                'title' => 'Hard Limit for Maximum Search Results',
            'description' => 'Enter the maximum amount of results to be processed. Set to 0 to disable. On larger boards (more than 1 million posts) this should be set to no more than 1000.',
                'optionscode' => 'numeric',
                'value' => '0',
                'disporder' => 4,
                'gid' => 21,
                'isdefault' => 1,
            ),
            215 => 
            array (
                'sid' => 216,
                'name' => 'helpsearch',
                'title' => 'Enable Help Documents Search',
                'description' => 'If enabled it will allow users to search through the help documents.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 5,
                'gid' => 21,
                'isdefault' => 1,
            ),
            216 => 
            array (
                'sid' => 217,
                'name' => 'bbcodeinserter',
                'title' => 'Clickable MyCode Editor',
                'description' => 'Set this option to On to show the clickable code buttons editor on posting pages. Switching this off will also disable the Smilies Inserter.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 1,
                'gid' => 22,
                'isdefault' => 1,
            ),
            217 => 
            array (
                'sid' => 218,
                'name' => 'partialmode',
                'title' => 'Clickable MyCode Editor in Partial Mode',
                'description' => 'Editor will be in partial mode if this option is set to \'yes\'. Several MyCodes, such as [quote] and [img], will be inserted into it as plain text tags.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 2,
                'gid' => 22,
                'isdefault' => 1,
            ),
            218 => 
            array (
                'sid' => 219,
                'name' => 'smilieinserter',
                'title' => 'Clickable Smilies Inserter',
                'description' => 'Clickable smilies will appear on the posting pages if this option is set to \'on\'.',
                'optionscode' => 'onoff',
                'value' => '1',
                'disporder' => 3,
                'gid' => 22,
                'isdefault' => 1,
            ),
            219 => 
            array (
                'sid' => 220,
                'name' => 'smilieinsertertot',
                'title' => 'No. of Smilies to show',
                'description' => 'Enter the total number of smilies to show on the clickable smilie inserter.',
                'optionscode' => 'numeric',
                'value' => '20',
                'disporder' => 4,
                'gid' => 22,
                'isdefault' => 1,
            ),
            220 => 
            array (
                'sid' => 221,
                'name' => 'smilieinsertercols',
                'title' => 'No. of Smilie Cols to Show',
                'description' => 'Enter the number of columns you wish to show on the clickable smilie inserter.',
                'optionscode' => 'numeric',
                'value' => '4',
                'disporder' => 5,
                'gid' => 22,
                'isdefault' => 1,
            ),
            221 => 
            array (
                'sid' => 222,
                'name' => 'allowbasicmycode',
                'title' => 'Allow Basic MyCodes',
                'description' => 'Setting this to yes allows users to use the basic MyCodes, such as Bold, Italic, Underline, Strike-through and HR.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 6,
                'gid' => 22,
                'isdefault' => 1,
            ),
            222 => 
            array (
                'sid' => 223,
                'name' => 'allowcolormycode',
                'title' => 'Allow Color MyCode',
                'description' => 'Setting this to yes allows users to use the Color MyCode.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 7,
                'gid' => 22,
                'isdefault' => 1,
            ),
            223 => 
            array (
                'sid' => 224,
                'name' => 'allowsizemycode',
                'title' => 'Allow Size MyCode',
                'description' => 'Setting this to yes allows users to use the Size MyCode',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 8,
                'gid' => 22,
                'isdefault' => 1,
            ),
            224 => 
            array (
                'sid' => 225,
                'name' => 'allowfontmycode',
                'title' => 'Allow Font MyCode',
                'description' => 'Setting this to yes allows users to use the Font MyCode.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 9,
                'gid' => 22,
                'isdefault' => 1,
            ),
            225 => 
            array (
                'sid' => 226,
                'name' => 'allowlinkmycode',
                'title' => 'Allow Link MyCode',
                'description' => 'Setting this to yes allows users to use the URL MyCode.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 10,
                'gid' => 22,
                'isdefault' => 1,
            ),
            226 => 
            array (
                'sid' => 227,
                'name' => 'allowemailmycode',
                'title' => 'Allow Email MyCode',
                'description' => 'Setting this to yes allows users to use the Email MyCode.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 11,
                'gid' => 22,
                'isdefault' => 1,
            ),
            227 => 
            array (
                'sid' => 228,
                'name' => 'allowalignmycode',
                'title' => 'Allow Alignment MyCode',
                'description' => 'Setting this to yes allows users to use the Align MyCode.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 12,
                'gid' => 22,
                'isdefault' => 1,
            ),
            228 => 
            array (
                'sid' => 229,
                'name' => 'allowlistmycode',
                'title' => 'Allow List MyCode',
                'description' => 'Setting this to yes allows users to use the List MyCode.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 13,
                'gid' => 22,
                'isdefault' => 1,
            ),
            229 => 
            array (
                'sid' => 230,
                'name' => 'allowcodemycode',
                'title' => 'Allow Code MyCodes',
                'description' => 'Setting this to yes allows users to use the Code and PHP MyCodes.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 14,
                'gid' => 22,
                'isdefault' => 1,
            ),
            230 => 
            array (
                'sid' => 231,
                'name' => 'allowsymbolmycode',
                'title' => 'Allow Symbol MyCodes',
            'description' => 'Setting this to yes allows users to use the (tm), (c) and (r) MyCodes.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 15,
                'gid' => 22,
                'isdefault' => 1,
            ),
            231 => 
            array (
                'sid' => 232,
                'name' => 'allowmemycode',
                'title' => 'Allow Me MyCodes',
                'description' => 'Setting this to yes allows users to use the /me and /slap MyCodes.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 16,
                'gid' => 22,
                'isdefault' => 1,
            ),
            232 => 
            array (
                'sid' => 233,
                'name' => 'guestimages',
                'title' => 'Parse [img] MyCode To Guests',
                'description' => 'Setting this to yes will parse posted images to guests. If no, a link will be shown instead.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 17,
                'gid' => 22,
                'isdefault' => 1,
            ),
            233 => 
            array (
                'sid' => 234,
                'name' => 'guestvideos',
                'title' => 'Parse [video] MyCode To Guests',
                'description' => 'Setting this to yes will parse posted videos to guests. If no, a link will be shown instead.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 18,
                'gid' => 22,
                'isdefault' => 1,
            ),
            234 => 
            array (
                'sid' => 235,
                'name' => 'cplanguage',
                'title' => 'Control Panel Language',
                'description' => 'The language of the control panel.',
                'optionscode' => 'adminlanguage',
                'value' => 'english',
                'disporder' => 1,
                'gid' => 23,
                'isdefault' => 1,
            ),
            235 => 
            array (
                'sid' => 236,
                'name' => 'cpstyle',
                'title' => 'Control Panel Style',
                'description' => 'The Default style that the control panel will use. Styles are inside the styles folder. A folder name inside that folder becomes the style title and style.css inside the style title folder is the css style file.',
                'optionscode' => 'cpstyle',
                'value' => '',
                'disporder' => 2,
                'gid' => 23,
                'isdefault' => 1,
            ),
            236 => 
            array (
                'sid' => 237,
                'name' => 'maxloginattempts',
                'title' => 'Max Number of Login Attempts',
                'description' => 'The max number of attempts to login before being locked out. Set to 0 to disable.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 3,
                'gid' => 23,
                'isdefault' => 1,
            ),
            237 => 
            array (
                'sid' => 238,
                'name' => 'loginattemptstimeout',
                'title' => 'Login Attempts Timeout',
            'description' => 'If the person trying to login reaches the max number of attempts, how long should they have to wait before being able to login again? (Set in minutes)',
                'optionscode' => 'numeric',
                'value' => '15',
                'disporder' => 4,
                'gid' => 23,
                'isdefault' => 1,
            ),
            238 => 
            array (
                'sid' => 239,
                'name' => 'mail_handler',
                'title' => 'Mail handler',
                'description' => 'The medium through which MyBB will send outgoing emails.',
                'optionscode' => 'select
mail=PHP mail
smtp=SMTP mail',
                'value' => 'mail',
                'disporder' => 1,
                'gid' => 24,
                'isdefault' => 1,
            ),
            239 => 
            array (
                'sid' => 240,
                'name' => 'smtp_host',
                'title' => 'SMTP hostname',
                'description' => 'The hostname of the SMTP server through which mail should be sent.<br />Only required if SMTP Mail is selected as the Mail Handler.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 2,
                'gid' => 24,
                'isdefault' => 1,
            ),
            240 => 
            array (
                'sid' => 241,
                'name' => 'smtp_port',
                'title' => 'SMTP port',
                'description' => 'The port number of the SMTP server through which mail should be sent.<br />Only required if SMTP Mail is selected as the Mail Handler.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 3,
                'gid' => 24,
                'isdefault' => 1,
            ),
            241 => 
            array (
                'sid' => 242,
                'name' => 'smtp_user',
                'title' => 'SMTP username',
                'description' => 'The username used to authenticate with the SMTP server.<br />Only required if SMTP Mail is selected as the Mail Handler, and the SMTP server requires authentication.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 4,
                'gid' => 24,
                'isdefault' => 1,
            ),
            242 => 
            array (
                'sid' => 243,
                'name' => 'smtp_pass',
                'title' => 'SMTP password',
                'description' => 'The corresponding password used to authenticate with the SMTP server.<br />Only required if SMTP Mail is selected as the Mail Handler, and the SMTP server requires authentication.',
                'optionscode' => 'passwordbox',
                'value' => '',
                'disporder' => 5,
                'gid' => 24,
                'isdefault' => 1,
            ),
            243 => 
            array (
                'sid' => 244,
                'name' => 'secure_smtp',
                'title' => 'SMTP Encryption Mode',
                'description' => 'Select the encryption required to communicate with the SMTP server.<br />Only required if SMTP Mail is selected as the Mail Handler.',
                'optionscode' => 'select
0=No encryption
1=SSL encryption
2=TLS encryption',
                'value' => '0',
                'disporder' => 6,
                'gid' => 24,
                'isdefault' => 1,
            ),
            244 => 
            array (
                'sid' => 245,
                'name' => 'mail_parameters',
            'title' => 'Additional Parameters for PHP\'s mail()',
            'description' => 'This setting allows you to set additional parameters for the PHP mail() function. Only used when \'PHP mail\' is selected as Mail Handler. <a href="http://php.net/function.mail" target="_blank">More information</a>',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 7,
                'gid' => 24,
                'isdefault' => 1,
            ),
            245 => 
            array (
                'sid' => 246,
                'name' => 'mail_logging',
                'title' => 'Mail Logging',
                'description' => 'This setting allows you to set how to log outgoing emails sent via the \'Send Thread to a Friend\' feature. In some countries it is illegal to log all content.',
                'optionscode' => 'select
0=Disable email logging
1=Log emails without content
2=Log everything',
                'value' => '2',
                'disporder' => 8,
                'gid' => 24,
                'isdefault' => 1,
            ),
            246 => 
            array (
                'sid' => 247,
                'name' => 'mail_message_id',
                'title' => 'Add message ID in mail headers',
                'description' => 'Disabling this option on some shared hosts resolves issues with forum emails being marked as spam.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 9,
                'gid' => 24,
                'isdefault' => 1,
            ),
            247 => 
            array (
                'sid' => 248,
                'name' => 'contact',
                'title' => 'Enable Contact Page',
                'description' => 'Setting this to yes will allow access to the contact page.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 25,
                'isdefault' => 1,
            ),
            248 => 
            array (
                'sid' => 249,
                'name' => 'contact_guests',
                'title' => 'Disable Contact Page to Guests',
                'description' => 'Setting this to yes will disallow access to the contact page for guests.',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 2,
                'gid' => 25,
                'isdefault' => 1,
            ),
            249 => 
            array (
                'sid' => 250,
                'name' => 'contact_badwords',
                'title' => 'Enable Word Filter on Contact Page',
                'description' => 'Setting this to yes will will send the subject and message fields through the word filter before sending.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 3,
                'gid' => 25,
                'isdefault' => 1,
            ),
            250 => 
            array (
                'sid' => 251,
                'name' => 'contact_maxsubjectlength',
                'title' => 'Maximum Subject Length on Contact Page',
            'description' => 'The maximum number of characters (bytes) to allow in the subject. A setting of 0 allows an unlimited length.',
                'optionscode' => 'numeric',
                'value' => '85',
                'disporder' => 4,
                'gid' => 25,
                'isdefault' => 1,
            ),
            251 => 
            array (
                'sid' => 252,
                'name' => 'contact_minmessagelength',
                'title' => 'Minimum Message Length on Contact Page',
                'description' => 'The minimum number of characters in order to send the message. A value of 0 disables this setting.',
                'optionscode' => 'numeric',
                'value' => '5',
                'disporder' => 5,
                'gid' => 25,
                'isdefault' => 1,
            ),
            252 => 
            array (
                'sid' => 253,
                'name' => 'contact_maxmessagelength',
                'title' => 'Maximum Message Length on Contact Page',
            'description' => 'The maximum number of characters (bytes) to allow in the message. A setting of 0 allows an unlimited length.',
                'optionscode' => 'numeric',
                'value' => '65535',
                'disporder' => 6,
                'gid' => 25,
                'isdefault' => 1,
            ),
            253 => 
            array (
                'sid' => 254,
                'name' => 'purgespammergroups',
                'title' => 'Allowed Usergroups',
                'description' => 'Select the usergroups which should be allowed to use Purge Spammer.',
                'optionscode' => 'groupselect',
                'value' => '3,4,6',
                'disporder' => 1,
                'gid' => 26,
                'isdefault' => 1,
            ),
            254 => 
            array (
                'sid' => 255,
                'name' => 'purgespammerpostlimit',
                'title' => 'Post Limit',
                'description' => 'This setting stops this tool being used on users who have more than a certain amount of posts, to prevent it being used on active members. Setting the value to 0 will disable the post check, however this is not recommended.',
                'optionscode' => 'numeric',
                'value' => '10',
                'disporder' => 2,
                'gid' => 26,
                'isdefault' => 1,
            ),
            255 => 
            array (
                'sid' => 256,
                'name' => 'purgespammerbandelete',
                'title' => 'Ban or Delete Spammers',
                'description' => 'Do you want to ban or delete spammers with this tool?',
                'optionscode' => 'radio
ban=Ban (Permanent)
delete=Delete',
                'value' => 'ban',
                'disporder' => 3,
                'gid' => 26,
                'isdefault' => 1,
            ),
            256 => 
            array (
                'sid' => 257,
                'name' => 'purgespammerbangroup',
                'title' => 'Ban Usergroup',
                'description' => 'Choose the usergroup to put users into when they get purged. Defaults to the fundamental Banned usergroup.',
                'optionscode' => 'groupselectsingle',
                'value' => '7',
                'disporder' => 4,
                'gid' => 26,
                'isdefault' => 1,
            ),
            257 => 
            array (
                'sid' => 258,
                'name' => 'purgespammerbanreason',
                'title' => 'Ban Reason',
                'description' => 'The reason which is used to ban the spammer.',
                'optionscode' => 'text',
                'value' => 'Spam',
                'disporder' => 5,
                'gid' => 26,
                'isdefault' => 1,
            ),
            258 => 
            array (
                'sid' => 259,
                'name' => 'purgespammerapikey',
                'title' => 'Stop Forum Spam API Key',
                'description' => 'In order to be able to submit information on a spammer to the Stop Forum Spam database, you need an API key. You can get one of these <a href="http://stopforumspam.com/signup" target="_blank">here</a>. When you have your key, paste it into the box below.',
                'optionscode' => 'text',
                'value' => '',
                'disporder' => 6,
                'gid' => 26,
                'isdefault' => 1,
            ),
            259 => 
            array (
                'sid' => 260,
                'name' => 'enablestopforumspam_on_register',
                'title' => 'Check Registrations Against StopForumSpam.com?',
                'description' => '
Do you wish to check all new registrations against the StopForumSpam.com database?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 1,
                'gid' => 27,
                'isdefault' => 1,
            ),
            260 => 
            array (
                'sid' => 261,
                'name' => 'stopforumspam_on_contact',
                'title' => 'Check Guest Contact Submissions Against StopForumSpam?',
                'description' => '
Should guest email and IP addresses be checked against StopForumSpam when using the contact page?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 2,
                'gid' => 27,
                'isdefault' => 1,
            ),
            261 => 
            array (
                'sid' => 262,
                'name' => 'stopforumspam_on_newreply',
                'title' => 'Check Guest Replies Against StopForumSpam?',
                'description' => '
Should guest usernames and IP addresses be checked against StopForumSpam when creating new replies?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 3,
                'gid' => 27,
                'isdefault' => 1,
            ),
            262 => 
            array (
                'sid' => 263,
                'name' => 'stopforumspam_on_newthread',
                'title' => 'Check Guest Threads Against StopForumSpam?',
                'description' => '
Should guest usernames and IP addresses be checked against StopForumSpam when creating new threads?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 4,
                'gid' => 27,
                'isdefault' => 1,
            ),
            263 => 
            array (
                'sid' => 264,
                'name' => 'stopforumspam_min_weighting_before_spam',
                'title' => 'Minimum stop forum spam weighting?',
                'description' => '
The minimum weighting received from StopForumSpam before deciding a user is a spammer. Should be a number between 0 and 100.',
                'optionscode' => 'text',
                'value' => '50.00',
                'disporder' => 5,
                'gid' => 27,
                'isdefault' => 1,
            ),
            264 => 
            array (
                'sid' => 265,
                'name' => 'stopforumspam_check_usernames',
                'title' => 'Check usernames?',
                'description' => '
Should the user usernames be checked against the StopForumSpam database?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 6,
                'gid' => 27,
                'isdefault' => 1,
            ),
            265 => 
            array (
                'sid' => 266,
                'name' => 'stopforumspam_check_emails',
                'title' => 'Check email addresses?',
                'description' => '
Should the user email addresses be checked against the StopForumSpam database?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 7,
                'gid' => 27,
                'isdefault' => 1,
            ),
            266 => 
            array (
                'sid' => 267,
                'name' => 'stopforumspam_check_ips',
                'title' => 'Check IP addresses?',
                'description' => '
Should the user IP addresses be checked against the StopForumSpam database?',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 8,
                'gid' => 27,
                'isdefault' => 1,
            ),
            267 => 
            array (
                'sid' => 268,
                'name' => 'stopforumspam_block_on_error',
                'title' => 'Block on StopForumSpam error?',
                'description' => '
If there is an error retrieving information from the StopForumSpam API, should the user be blocked?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 9,
                'gid' => 27,
                'isdefault' => 1,
            ),
            268 => 
            array (
                'sid' => 269,
                'name' => 'stopforumspam_log_blocks',
                'title' => 'Log StopForumSpam blocks?',
                'description' => '
Should every StopForumSpam block be logged?',
                'optionscode' => 'yesno',
                'value' => '0',
                'disporder' => 10,
                'gid' => 27,
                'isdefault' => 1,
            ),
            269 => 
            array (
                'sid' => 270,
                'name' => 'allowicqfield',
                'title' => 'Allow ICQ Number Field To Usergroups',
                'description' => 'Select the usergroups which should be allowed to use the ICQ Number contact field.',
                'optionscode' => 'groupselect',
                'value' => '-1',
                'disporder' => 1,
                'gid' => 28,
                'isdefault' => 1,
            ),
            270 => 
            array (
                'sid' => 271,
                'name' => 'allowaimfield',
                'title' => 'Allow AIM Screen Name To Usergroups',
                'description' => 'Select the usergroups which should be allowed to use the AIM Screen Name contact field.',
                'optionscode' => 'groupselect',
                'value' => '-1',
                'disporder' => 2,
                'gid' => 28,
                'isdefault' => 1,
            ),
            271 => 
            array (
                'sid' => 272,
                'name' => 'allowyahoofield',
                'title' => 'Allow Yahoo ID Field To Usergroups',
                'description' => 'Select the usergroups which should be allowed to use the Yahoo ID contact field.',
                'optionscode' => 'groupselect',
                'value' => '-1',
                'disporder' => 3,
                'gid' => 28,
                'isdefault' => 1,
            ),
            272 => 
            array (
                'sid' => 273,
                'name' => 'allowskypefield',
                'title' => 'Allow Skype ID Field To Usergroups',
                'description' => 'Select the usergroups which should be allowed to use the Skype ID contact field.',
                'optionscode' => 'groupselect',
                'value' => '-1',
                'disporder' => 4,
                'gid' => 28,
                'isdefault' => 1,
            ),
            273 => 
            array (
                'sid' => 274,
                'name' => 'allowgooglefield',
                'title' => 'Allow Google Talk ID Field To Usergroups',
                'description' => 'Select the usergroups which should be allowed to use the Google Talk ID contact field.',
                'optionscode' => 'groupselect',
                'value' => '-1',
                'disporder' => 5,
                'gid' => 28,
                'isdefault' => 1,
            ),
            274 => 
            array (
                'sid' => 275,
                'name' => 'statsenabled',
                'title' => 'Enable Statistics Pages',
                'description' => 'If you wish to disable the statistics page on your board, set this option to no.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 1,
                'gid' => 29,
                'isdefault' => 1,
            ),
            275 => 
            array (
                'sid' => 276,
                'name' => 'statscachetime',
                'title' => 'Statistics Cache Time',
            'description' => 'Insert the interval of time to refresh the statistics page cache (in hours). 0 to disable caching.',
                'optionscode' => 'numeric',
                'value' => '0',
                'disporder' => 2,
                'gid' => 29,
                'isdefault' => 1,
            ),
            276 => 
            array (
                'sid' => 277,
                'name' => 'statslimit',
                'title' => 'Stats Limit',
                'description' => 'The number of threads to show on the stats page for most replies and most views.',
                'optionscode' => 'numeric',
                'value' => '15',
                'disporder' => 3,
                'gid' => 29,
                'isdefault' => 1,
            ),
            277 => 
            array (
                'sid' => 278,
                'name' => 'statstopreferrer',
                'title' => 'Show Top Referrer on Statistics Page',
                'description' => 'Do you want to show the top referrer on the stats.php page? This adds an additional query.',
                'optionscode' => 'yesno',
                'value' => '1',
                'disporder' => 4,
                'gid' => 29,
                'isdefault' => 1,
            ),
        ));

        
    }
}
