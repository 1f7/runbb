<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class SmiliesTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('smilies')->delete();
        
        DB::table('smilies')->insert(array (
            0 => 
            array (
                'sid' => 1,
                'name' => 'Smile',
            'find' => ':)',
            'image' => 'images/smilies/smile.png',
            'disporder' => 1,
            'showclickable' => 1,
        ),
        1 => 
        array (
            'sid' => 2,
            'name' => 'Wink',
        'find' => ';)',
        'image' => 'images/smilies/wink.png',
        'disporder' => 2,
        'showclickable' => 1,
    ),
    2 => 
    array (
        'sid' => 3,
        'name' => 'Cool',
        'find' => ':cool:',
        'image' => 'images/smilies/cool.png',
        'disporder' => 3,
        'showclickable' => 1,
    ),
    3 => 
    array (
        'sid' => 4,
        'name' => 'Big Grin',
        'find' => ':D',
        'image' => 'images/smilies/biggrin.png',
        'disporder' => 4,
        'showclickable' => 1,
    ),
    4 => 
    array (
        'sid' => 5,
        'name' => 'Tongue',
        'find' => ':P',
        'image' => 'images/smilies/tongue.png',
        'disporder' => 5,
        'showclickable' => 1,
    ),
    5 => 
    array (
        'sid' => 6,
        'name' => 'Rolleyes',
        'find' => ':rolleyes:',
        'image' => 'images/smilies/rolleyes.png',
        'disporder' => 6,
        'showclickable' => 1,
    ),
    6 => 
    array (
        'sid' => 7,
        'name' => 'Shy',
        'find' => ':shy:',
        'image' => 'images/smilies/shy.png',
        'disporder' => 7,
        'showclickable' => 1,
    ),
    7 => 
    array (
        'sid' => 8,
        'name' => 'Sad',
        'find' => ':(',
            'image' => 'images/smilies/sad.png',
            'disporder' => 8,
            'showclickable' => 1,
        ),
        8 => 
        array (
            'sid' => 9,
            'name' => 'At',
            'find' => ':at:',
            'image' => 'images/smilies/at.png',
            'disporder' => 9,
            'showclickable' => 1,
        ),
        9 => 
        array (
            'sid' => 10,
            'name' => 'Angel',
            'find' => ':angel:',
            'image' => 'images/smilies/angel.png',
            'disporder' => 10,
            'showclickable' => 1,
        ),
        10 => 
        array (
            'sid' => 11,
            'name' => 'Angry',
            'find' => ':@',
            'image' => 'images/smilies/angry.png',
            'disporder' => 11,
            'showclickable' => 1,
        ),
        11 => 
        array (
            'sid' => 12,
            'name' => 'Blush',
            'find' => ':blush:',
            'image' => 'images/smilies/blush.png',
            'disporder' => 12,
            'showclickable' => 1,
        ),
        12 => 
        array (
            'sid' => 13,
            'name' => 'Confused',
            'find' => ':s',
            'image' => 'images/smilies/confused.png',
            'disporder' => 13,
            'showclickable' => 1,
        ),
        13 => 
        array (
            'sid' => 14,
            'name' => 'Dodgy',
            'find' => ':dodgy:',
            'image' => 'images/smilies/dodgy.png',
            'disporder' => 14,
            'showclickable' => 1,
        ),
        14 => 
        array (
            'sid' => 15,
            'name' => 'Exclamation',
            'find' => ':exclamation:',
            'image' => 'images/smilies/exclamation.png',
            'disporder' => 15,
            'showclickable' => 1,
        ),
        15 => 
        array (
            'sid' => 16,
            'name' => 'Heart',
            'find' => ':heart:',
            'image' => 'images/smilies/heart.png',
            'disporder' => 16,
            'showclickable' => 1,
        ),
        16 => 
        array (
            'sid' => 17,
            'name' => 'Huh',
            'find' => ':huh:',
            'image' => 'images/smilies/huh.png',
            'disporder' => 17,
            'showclickable' => 1,
        ),
        17 => 
        array (
            'sid' => 18,
            'name' => 'Idea',
            'find' => ':idea:',
            'image' => 'images/smilies/lightbulb.png',
            'disporder' => 18,
            'showclickable' => 1,
        ),
        18 => 
        array (
            'sid' => 19,
            'name' => 'Sleepy',
            'find' => ':sleepy:',
            'image' => 'images/smilies/sleepy.png',
            'disporder' => 19,
            'showclickable' => 1,
        ),
        19 => 
        array (
            'sid' => 20,
            'name' => 'Undecided',
            'find' => ':-/',
            'image' => 'images/smilies/undecided.png',
            'disporder' => 20,
            'showclickable' => 1,
        ),
        20 => 
        array (
            'sid' => 21,
            'name' => 'Cry',
            'find' => ':cry:',
            'image' => 'images/smilies/cry.png',
            'disporder' => 21,
            'showclickable' => 1,
        ),
        21 => 
        array (
            'sid' => 22,
            'name' => 'Sick',
            'find' => ':sick:',
            'image' => 'images/smilies/sick.png',
            'disporder' => 22,
            'showclickable' => 1,
        ),
        22 => 
        array (
            'sid' => 23,
            'name' => 'Arrow',
            'find' => ':arrow:',
            'image' => 'images/smilies/arrow.png',
            'disporder' => 23,
            'showclickable' => 1,
        ),
        23 => 
        array (
            'sid' => 24,
            'name' => 'My',
            'find' => ':my:',
            'image' => 'images/smilies/my.png',
            'disporder' => 24,
            'showclickable' => 1,
        ),
    ));

        
    }
}
