<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class SpidersTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('spiders')->delete();
        
        DB::table('spiders')->insert(array (
            0 => 
            array (
                'sid' => 1,
                'name' => 'Google',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'Googlebot',
                'lastvisit' => 0,
            ),
            1 => 
            array (
                'sid' => 2,
                'name' => 'Bing',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'bingbot',
                'lastvisit' => 0,
            ),
            2 => 
            array (
                'sid' => 3,
                'name' => 'Alexa Internet',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'ia_archiver',
                'lastvisit' => 0,
            ),
            3 => 
            array (
                'sid' => 4,
                'name' => 'Ask.com',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'Teoma',
                'lastvisit' => 0,
            ),
            4 => 
            array (
                'sid' => 5,
                'name' => 'Baidu',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'Baiduspider',
                'lastvisit' => 0,
            ),
            5 => 
            array (
                'sid' => 6,
                'name' => 'Yandex',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'YandexBot',
                'lastvisit' => 0,
            ),
            6 => 
            array (
                'sid' => 7,
                'name' => 'Blekko',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'Blekkobot',
                'lastvisit' => 0,
            ),
            7 => 
            array (
                'sid' => 8,
                'name' => 'Facebook',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'facebookexternalhit',
                'lastvisit' => 0,
            ),
            8 => 
            array (
                'sid' => 9,
                'name' => 'Twitter',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'Twitterbot',
                'lastvisit' => 0,
            ),
            9 => 
            array (
                'sid' => 10,
                'name' => 'Internet Archive',
                'theme' => 0,
                'language' => '',
                'usergroup' => 0,
                'useragent' => 'archive.org_bot',
                'lastvisit' => 0,
            ),
        ));

        
    }
}
