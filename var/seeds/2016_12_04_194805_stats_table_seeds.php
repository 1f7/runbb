<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class StatsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('stats')->delete();
        
        DB::table('stats')->insert(array (
            0 => 
            array (
                'dateline' => 1480723200,
                'numusers' => 5,
                'numthreads' => 1,
                'numposts' => 1,
            ),
        ));

        
    }
}
