<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class TasksTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('tasks')->delete();
        
        DB::table('tasks')->insert(array (
            0 => 
            array (
                'tid' => 1,
                'title' => 'Hourly Cleanup',
                'description' => 'Cleans out old searches, captcha images, registration questions and expires redirects.',
                'file' => 'hourlycleanup',
                'minute' => '0',
                'hour' => '*',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1466676000,
                'lastrun' => 1466673131,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            1 => 
            array (
                'tid' => 2,
                'title' => 'Daily Cleanup',
                'description' => 'Cleans out old sessions and read threads.',
                'file' => 'dailycleanup',
                'minute' => '0',
                'hour' => '0',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1469311200,
                'lastrun' => 1466651897,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 1469233659,
            ),
            2 => 
            array (
                'tid' => 3,
                'title' => 'Half-hourly User Cleanup',
                'description' => 'Automatically expires bans, warnings and posting suspension and moderation times for users.',
                'file' => 'usercleanup',
                'minute' => '30,59',
                'hour' => '*',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1468623540,
                'lastrun' => 1467365795,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 1468622453,
            ),
            3 => 
            array (
                'tid' => 4,
                'title' => 'Weekly Backup',
                'description' => 'Automatically backups your MyBB tables to your backups directory.',
                'file' => 'backupdb',
                'minute' => '0',
                'hour' => '0',
                'day' => '*',
                'month' => '*',
                'weekday' => '0',
                'nextrun' => 1466892000,
                'lastrun' => 0,
                'enabled' => 0,
                'logging' => 1,
                'locked' => 0,
            ),
            4 => 
            array (
                'tid' => 5,
                'title' => 'Promotion System',
                'description' => 'Automatically runs the promotion system every 20 minutes.',
                'file' => 'promotions',
                'minute' => '5,25,45',
                'hour' => '*',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1467309900,
                'lastrun' => 1467308999,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            5 => 
            array (
                'tid' => 6,
                'title' => 'Thread Views',
                'description' => 'Automatically updates thread views every 15 minutes. This task will automatically be enabled or disabled when changing the \'Delayed Thread Views\' setting.',
                'file' => 'threadviews',
                'minute' => '10,25,40,55',
                'hour' => '*',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1466357100,
                'lastrun' => 0,
                'enabled' => 0,
                'logging' => 1,
                'locked' => 0,
            ),
            6 => 
            array (
                'tid' => 7,
                'title' => 'Tables Check',
                'description' => 'Automatically runs a table check and attempts to repair any crashed tables.',
                'file' => 'checktables',
                'minute' => '0',
                'hour' => '*',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1466359200,
                'lastrun' => 0,
                'enabled' => 0,
                'logging' => 1,
                'locked' => 0,
            ),
            7 => 
            array (
                'tid' => 8,
                'title' => 'Log Pruning',
            'description' => 'Automatically cleans up old MyBB log files (administrator, moderator, task, promotion, and mail logs.)',
                'file' => 'logcleanup',
                'minute' => '21',
                'hour' => '5',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1466738460,
                'lastrun' => 1466652079,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            8 => 
            array (
                'tid' => 9,
                'title' => 'Mass Mail',
                'description' => 'Automatically sends any queued mass mailings every 15 minutes.',
                'file' => 'massmail',
                'minute' => '10,25,40,55',
                'hour' => '*',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1467366000,
                'lastrun' => 1467365794,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            9 => 
            array (
                'tid' => 10,
                'title' => 'User Pruning',
                'description' => 'Automatically prunes users based on criteria set in the settings every day.',
                'file' => 'userpruning',
                'minute' => '0',
                'hour' => '2',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1466726400,
                'lastrun' => 1466652018,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            10 => 
            array (
                'tid' => 11,
                'title' => 'Delayed Moderation',
                'description' => 'Automatically performs delayed moderation actions every 24 hours.',
                'file' => 'delayedmoderation',
                'minute' => '0',
                'hour' => '0',
                'day' => '*',
                'month' => '*',
                'weekday' => '*',
                'nextrun' => 1466719200,
                'lastrun' => 1466651958,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            11 => 
            array (
                'tid' => 12,
                'title' => 'Version Check',
                'description' => 'Automatically checks for news and new versions of MyBB every day.',
                'file' => 'versioncheck',
                'minute' => '0',
                'hour' => '13',
                'day' => '*',
                'month' => '*',
                'weekday' => '0',
                'nextrun' => 1466938800,
                'lastrun' => 1466356705,
                'enabled' => 1,
                'logging' => 1,
                'locked' => 0,
            ),
            12 => 
            array (
                'tid' => 13,
                'title' => 'Stylesheet Re-Cache',
                'description' => 'Re-caches all stylesheets to the filesystem, including minifying the styles.',
                'file' => 'recachestylesheets',
                'minute' => '0',
                'hour' => '0',
                'day' => '1',
                'month' => '0',
                'weekday' => '0',
                'nextrun' => 1483225200,
                'lastrun' => 0,
                'enabled' => 0,
                'logging' => 1,
                'locked' => 1466920135,
            ),
        ));

        
    }
}
