<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class TemplategroupsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('templategroups')->delete();
        
        DB::table('templategroups')->insert(array (
            0 => 
            array (
                'gid' => 1,
                'prefix' => 'calendar',
                'title' => '<lang:group_calendar>',
                'isdefault' => 1,
            ),
            1 => 
            array (
                'gid' => 3,
                'prefix' => 'forumbit',
                'title' => '<lang:group_forumbit>',
                'isdefault' => 1,
            ),
            2 => 
            array (
                'gid' => 4,
                'prefix' => 'forumjump',
                'title' => '<lang:group_forumjump>',
                'isdefault' => 1,
            ),
            3 => 
            array (
                'gid' => 5,
                'prefix' => 'forumdisplay',
                'title' => '<lang:group_forumdisplay>',
                'isdefault' => 1,
            ),
            4 => 
            array (
                'gid' => 7,
                'prefix' => 'error',
                'title' => '<lang:group_error>',
                'isdefault' => 1,
            ),
            5 => 
            array (
                'gid' => 8,
                'prefix' => 'memberlist',
                'title' => '<lang:group_memberlist>',
                'isdefault' => 1,
            ),
            6 => 
            array (
                'gid' => 9,
                'prefix' => 'multipage',
                'title' => '<lang:group_multipage>',
                'isdefault' => 1,
            ),
            7 => 
            array (
                'gid' => 10,
                'prefix' => 'private',
                'title' => '<lang:group_private>',
                'isdefault' => 1,
            ),
            8 => 
            array (
                'gid' => 11,
                'prefix' => 'portal',
                'title' => '<lang:group_portal>',
                'isdefault' => 1,
            ),
            9 => 
            array (
                'gid' => 12,
                'prefix' => 'postbit',
                'title' => '<lang:group_postbit>',
                'isdefault' => 1,
            ),
            10 => 
            array (
                'gid' => 13,
                'prefix' => 'posticons',
                'title' => '<lang:group_posticons>',
                'isdefault' => 1,
            ),
            11 => 
            array (
                'gid' => 14,
                'prefix' => 'showthread',
                'title' => '<lang:group_showthread>',
                'isdefault' => 1,
            ),
            12 => 
            array (
                'gid' => 15,
                'prefix' => 'usercp',
                'title' => '<lang:group_usercp>',
                'isdefault' => 1,
            ),
            13 => 
            array (
                'gid' => 17,
                'prefix' => 'moderation',
                'title' => '<lang:group_moderation>',
                'isdefault' => 1,
            ),
            14 => 
            array (
                'gid' => 18,
                'prefix' => 'nav',
                'title' => '<lang:group_nav>',
                'isdefault' => 1,
            ),
            15 => 
            array (
                'gid' => 20,
                'prefix' => 'showteam',
                'title' => '<lang:group_showteam>',
                'isdefault' => 1,
            ),
            16 => 
            array (
                'gid' => 21,
                'prefix' => 'reputation',
                'title' => '<lang:group_reputation>',
                'isdefault' => 1,
            ),
            17 => 
            array (
                'gid' => 24,
                'prefix' => 'member',
                'title' => '<lang:group_member>',
                'isdefault' => 1,
            ),
            18 => 
            array (
                'gid' => 25,
                'prefix' => 'warnings',
                'title' => '<lang:group_warning>',
                'isdefault' => 1,
            ),
            19 => 
            array (
                'gid' => 26,
                'prefix' => 'global',
                'title' => '<lang:group_global>',
                'isdefault' => 1,
            ),
            20 => 
            array (
                'gid' => 29,
                'prefix' => 'misc',
                'title' => '<lang:group_misc>',
                'isdefault' => 1,
            ),
            21 => 
            array (
                'gid' => 30,
                'prefix' => 'modcp',
                'title' => '<lang:group_modcp>',
                'isdefault' => 1,
            ),
            22 => 
            array (
                'gid' => 33,
                'prefix' => 'post',
                'title' => '<lang:group_post>',
                'isdefault' => 1,
            ),
            23 => 
            array (
                'gid' => 35,
                'prefix' => 'report',
                'title' => '<lang:group_report>',
                'isdefault' => 1,
            ),
            24 => 
            array (
                'gid' => 40,
                'prefix' => 'video',
                'title' => '<lang:group_video>',
                'isdefault' => 1,
            ),
            25 => 
            array (
                'gid' => 41,
                'prefix' => 'sendthread',
                'title' => '<lang:group_sendthread>',
                'isdefault' => 1,
            ),
        ));

        
    }
}
