<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class TemplatesTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('templates')->delete();
        
        DB::table('templates')->insert(array (
            0 => 
            array (
                'tid' => 4,
                'title' => 'calendar',
                'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->calendar}</title>
{$headerinclude}
</head>
<body>
{$header}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<thead>
<tr>
<td class="thead" colspan="8">
<div class="float_right">
<a href="{$prev_link}">&laquo; {$prev_month[\'name\']} {$prev_month[\'year\']}</a> | <a href="{$next_link}">{$next_month[\'name\']} {$next_month[\'year\']} &raquo;</a>
</div>
<div><strong>{$monthnames[$month]} {$year}</strong></div>
</td>
</tr>
<tr>
<td class="tcat">&nbsp;</td>
{$weekday_headers}
</tr>
</thead>
<tbody>
{$calendar_rows}
</tbody>
</table>
<br />
<form action="calendar.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="trow1">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td valign="top">{$addevent}</td>
<td align="right">
<span class="smalltext"><strong>{$lang->jump_month}</strong></span>
<select name="month">
<option value="{$month}">{$monthnames[$month]}</option>
<option value="{$month}">----------</option>
<option value="1">{$lang->alt_month_1}</option>
<option value="2">{$lang->alt_month_2}</option>
<option value="3">{$lang->alt_month_3}</option>
<option value="4">{$lang->alt_month_4}</option>
<option value="5">{$lang->alt_month_5}</option>
<option value="6">{$lang->alt_month_6}</option>
<option value="7">{$lang->alt_month_7}</option>
<option value="8">{$lang->alt_month_8}</option>
<option value="9">{$lang->alt_month_9}</option>
<option value="10">{$lang->alt_month_10}</option>
<option value="11">{$lang->alt_month_11}</option>
<option value="12">{$lang->alt_month_12}</option>
</select>
<select name="year">
<option value="{$year}">{$year}</option>
<option value="{$year}">----------</option>
{$yearsel}
</select>
{$gobutton}
<br />
{$calendar_jump}
</td>
</tr>
</table>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
                'sid' => -2,
                'version' => '1800',
                'status' => '',
                'dateline' => 1466356529,
            ),
            1 => 
            array (
                'tid' => 5,
                'title' => 'calendar_addevent',
                'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->calendar} - {$lang->add_event}</title>
{$headerinclude}
</head>
<body>
{$header}
{$event_errors}
<form action="calendar.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder clear">
<tr>
<td colspan="2" width="100%" class="thead"><strong>{$lang->add_event}</strong></td>
</tr>
<tr>
<td width="20%" class="trow1"><strong>{$lang->event_name}</strong></td>
<td class="trow1"><input type="text" class="textbox" size="50" name="name" maxlength="120" value="{$name}"/></td>
</tr>
{$calendar_select}
<tr>
<td class="trow2" style="vertical-align: top;"><strong>{$lang->event_date}</strong></td>
<td class="trow1">
<script type="text/javascript">
<!--
function toggleType()
{
if($("#type_single").prop("checked"))
{
$("#ranged_selects").hide();
$("#single_selects").show();
}
else
{
$("#single_selects").hide();
$("#ranged_selects").show();
toggleRepeats();
}
}

function toggleRepeats()
{
var repeats = $("#repeats").get(0).selectedIndex;
$(".repeats_type").each(function()
{
$(this).hide();
});
if($("#repeats_type_"+repeats).length > 0)
{
$("#repeats_type_"+repeats).show();
}
}
// -->
</script>
<dl style="margin-top: 0; margin-bottom: 0; width: 100%;">
<dt><input type="radio" name="type" value="single" {$type_single} id="type_single" onclick="toggleType();" /> <label for="type_single"><strong>{$lang->type_single}</strong></label></dt>
<dd style="margin-top: 4px;" id="single_selects">
<select name="single_day">
{$single_days}
</select>
&nbsp;
<select name="single_month">
<option value="1"{$single_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$single_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$single_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$single_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$single_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$single_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$single_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$single_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$single_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$single_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$single_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$single_month[\'12\']}>{$lang->month_12}</option>
</select>
&nbsp;
<select name="single_year">
{$single_years}
</select>
</dd>
<dt><input type="radio" name="type" value="ranged" {$type_ranged} id="type_ranged" onclick="toggleType();" /> <label for="type_ranged"><strong>{$lang->type_ranged}</strong></label></dt>
<dd style="margin-top: 4px; width: 100%;" id="ranged_selects">
<table width="100%">
<tr>
<td class="smalltext" style="padding: 4px; text-align: right;">{$lang->start_time}</td>
<td style="padding: 4px;">
<select name="start_day">
{$start_days}
</select>
<select name="start_month">
<option value="1"{$start_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$start_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$start_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$start_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$start_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$start_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$start_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$start_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$start_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$start_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$start_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$start_month[\'12\']}>{$lang->month_12}</option>
</select>
<select name="start_year">
{$start_years}
</select>
<span class="smalltext">&nbsp;&nbsp;&nbsp;{$lang->enter_time}</span>
<input type="text" class="textbox" name="start_time" value="{$start_time}" size="7" />
</td>
</tr>
<tr>
<td class="smalltext" style="padding: 4px; text-align: right;">{$lang->end_time}</td>
<td style="padding: 4px;">
<select name="end_day">
{$end_days}
</select>
<select name="end_month">
<option value="1"{$end_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$end_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$end_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$end_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$end_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$end_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$end_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$end_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$end_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$end_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$end_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$end_month[\'12\']}>{$lang->month_12}</option>
</select>
<select name="end_year">
{$end_years}
</select>
<span class="smalltext">&nbsp;&nbsp;&nbsp;{$lang->enter_time}</span>
<input type="text" class="textbox" name="end_time" value="{$end_time}" size="7" />
</td>
</tr>
<tr>
<td class="smalltext" style="padding: 4px; text-align: right; vertical-align: top;">{$lang->timezone}</td>
<td style="padding: 4px;">
{$timezones}<br />
<label class="smalltext" style="display: block; padding-top: 4px;"><input type="checkbox" name="ignoretimezone" value="1" {$ignore_timezone} style="vertical-align: middle;" /> {$lang->ignore_timezone} </label>
</td>
</tr>
<tr>
<td class="smalltext" style="padding: 4px; text-align: right; vertical-align:top;">{$lang->repeats}</td>
<td class="smalltext" style="padding: 4px;">
<select name="repeats" id="repeats" onchange="toggleRepeats();">
<option value="0">{$lang->does_not_repeat}</option>
<option value="1"{$repeats_sel[1]}>{$lang->repeats_daily}</option>
<option value="2"{$repeats_sel[2]}>{$lang->repeats_weekdays}</option>
<option value="3"{$repeats_sel[3]}>{$lang->repeats_weekly}</option>
<option value="4"{$repeats_sel[4]}>{$lang->repeats_monthly}</option>
<option value="5"{$repeats_sel[5]}>{$lang->repeats_yearly}</option>
</select>
<div class="repeats_type" id="repeats_type_1" style="padding: 4px;">
{$lang->repeats_every}
<input type="text" class="textbox" name="repeats_1_days" value="{$repeats_1_days}" size="2" /> {$lang->day_or_days}
</div>
<div class="repeats_type" id="repeats_type_3" style="padding: 4px;">
{$lang->repeats_every}
<input type="text" class="textbox" name="repeats_3_weeks" value="{$repeats_3_weeks}" size="2" /> {$lang->week_or_weeks_on}
<br />
<table style="padding: 4px;">
<tr>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[0]" value="1" {$repeats_3_days[0]} style="vertical-align: middle;" /> {$lang->sunday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[1]" value="1" {$repeats_3_days[1]} style="vertical-align: middle;" /> {$lang->monday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[2]" value="1" {$repeats_3_days[2]} style="vertical-align: middle;" /> {$lang->tuesday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[3]" value="1" {$repeats_3_days[3]} style="vertical-align: middle;" /> {$lang->wednesday}</label></td>
</tr>
<tr>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[4]" value="1" {$repeats_3_days[4]} style="vertical-align: middle;" /> {$lang->thursday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[5]" value="1" {$repeats_3_days[5]} style="vertical-align: middle;" /> {$lang->friday}</label></td>
<td style="padding: 2px;" class="smalltext" colspan="2"><label><input type="checkbox" name="repeats_3_days[6]" value="1" {$repeats_3_days[6]} style="vertical-align: middle;" /> {$lang->saturday}</label></td>
</tr>
</table>
</div>
<div class="repeats_type" id="repeats_type_4" style="padding: 4px;">
<div class="smalltext"><label>
<input type="radio" name="repeats_4_type" id="repeats_4_type_1" value="1" style="vertical-align: middle;" {$repeats_4_type[1]} />
{$lang->repeats_on_day}</label>
<input type="text" class="textbox" name="repeats_4_day" value="{$repeats_4_day}" onfocus="$(\'#repeats_4_type_1\').prop(\'checked\', true);" size="2" /> {$lang->of_every} <input type="text" class="textbox" name="repeats_4_months" value="{$repeats_4_months}" size="2" onfocus="$(\'#repeats_4_type_1\').prop(\'checked\', true);" /> {$lang->month_or_months}
</div>
<div class="smalltext" style="margin-top: 5px;"><label>
<input type="radio" name="repeats_4_type" id="repeats_4_type_2" value="2" style="vertical-align: middle;" {$repeats_4_type[2]} />
{$lang->repeats_on_the}</label>
<select name="repeats_4_occurance" onfocus="$(\'#repeats_4_type_2\').prop(\'checked\', true);">
<option value="1"{$repeats_4_occurance[1]}>{$lang->first}</option>
<option value="2"{$repeats_4_occurance[2]}>{$lang->second}</option>
<option value="3"{$repeats_4_occurance[3]}>{$lang->third}</option>
<option value="4"{$repeats_4_occurance[4]}>{$lang->fourth}</option>
<option value="last"{$repeats_4_occurance[\'last\']}>{$lang->last}</option>
</select>
<select name="repeats_4_weekday" onfocus="$(\'#repeats_4_type_2\').prop(\'checked\', true);">
<option value="0"{$repeats_4_weekday[0]}>{$lang->sunday}</option>
<option value="1"{$repeats_4_weekday[1]}>{$lang->monday}</option>
<option value="2"{$repeats_4_weekday[2]}>{$lang->tuesday}</option>
<option value="3"{$repeats_4_weekday[3]}>{$lang->wednesday}</option>
<option value="4"{$repeats_4_weekday[4]}>{$lang->thursday}</option>
<option value="5"{$repeats_4_weekday[5]}>{$lang->friday}</option>
<option value="6"{$repeats_4_weekday[6]}>{$lang->saturday}</option>
</select>
{$lang->of_every}
<input type="text" class="textbox" name="repeats_4_months2" value="{$repeats_4_months2}" size="2" onfocus="$(\'#repeats_4_type_2\').prop(\'checked\', true);" /> {$lang->month_or_months}
</div>
</div>
<div class="repeats_type" id="repeats_type_5" style="padding: 4px;">
<div class="smalltext"><label>
<input type="radio" name="repeats_5_type" value="1" id="repeats_5_type_1" style="vertical-align: middle;" {$repeats_5_type[1]} />
{$lang->repeats_on}</label>
<input type="text" class="textbox" name="repeats_5_day" onfocus="$(\'#repeats_5_type_1\').prop(\'checked\', true);" value="{$repeats_5_day}" size="2" />
<select name="repeats_5_month" onfocus="$(\'#repeats_5_type_1\').prop(\'checked\', true);" >
<option value="1"{$repeats_5_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$repeats_5_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$repeats_5_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$repeats_5_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$repeats_5_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$repeats_5_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$repeats_5_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$repeats_5_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$repeats_5_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$repeats_5_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$repeats_5_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$repeats_5_month[\'12\']}>{$lang->month_12}</option>
</select>
{$lang->every}
<input type="text" class="textbox" name="repeats_5_years" value="{$repeats_5_years}" onfocus="$(\'#repeats_5_type_1\').prop(\'checked\', true);" size="2" /> {$lang->year_or_years}
</div>
<div class="smalltext" style="margin-top: 5px;"><label>
<input type="radio" name="repeats_5_type" value="2" id="repeats_5_type_2" style="vertical-align: middle;" {$repeats_5_type[2]} />
{$lang->repeats_on_the}</label>
<select name="repeats_5_occurance" onfocus="$(\'#repeats_5_type_2\').prop(\'checked\', true);" >
<option value="1"{$repeats_5_occurance[1]}>{$lang->first}</option>
<option value="2"{$repeats_5_occurance[2]}>{$lang->second}</option>
<option value="3"{$repeats_5_occurance[3]}>{$lang->third}</option>
<option value="4"{$repeats_5_occurance[4]}>{$lang->fourth}</option>
<option value="last"{$repeats_4_occurance[\'last\']}>{$lang->last}</option>
</select>
<select name="repeats_5_weekday" onfocus="$(\'#repeats_5_type_2\').prop(\'checked\', true);" >
<option value="0"{$repeats_5_weekday[0]}>{$lang->sunday}</option>
<option value="1"{$repeats_5_weekday[1]}>{$lang->monday}</option>
<option value="2"{$repeats_5_weekday[2]}>{$lang->tuesday}</option>
<option value="3"{$repeats_5_weekday[3]}>{$lang->wednesday}</option>
<option value="4"{$repeats_5_weekday[4]}>{$lang->thursday}</option>
<option value="5"{$repeats_5_weekday[5]}>{$lang->friday}</option>
<option value="6"{$repeats_5_weekday[6]}>{$lang->saturday}</option>
</select>
{$lang->of}
<select name="repeats_5_month2" onfocus="$(\'#repeats_5_type_2\').prop(\'checked\', true);" >
<option value="1"{$repeats_5_month2[\'1\']}>{$lang->month_1}</option>
<option value="2"{$repeats_5_month2[\'2\']}>{$lang->month_2}</option>
<option value="3"{$repeats_5_month2[\'3\']}>{$lang->month_3}</option>
<option value="4"{$repeats_5_month2[\'4\']}>{$lang->month_4}</option>
<option value="5"{$repeats_5_month2[\'5\']}>{$lang->month_5}</option>
<option value="6"{$repeats_5_month2[\'6\']}>{$lang->month_6}</option>
<option value="7"{$repeats_5_month2[\'7\']}>{$lang->month_7}</option>
<option value="8"{$repeats_5_month2[\'8\']}>{$lang->month_8}</option>
<option value="9"{$repeats_5_month2[\'9\']}>{$lang->month_9}</option>
<option value="10"{$repeats_5_month2[\'10\']}>{$lang->month_10}</option>
<option value="11"{$repeats_5_month2[\'11\']}>{$lang->month_11}</option>
<option value="12"{$repeats_5_month2[\'12\']}>{$lang->month_12}</option>
</select>
{$lang->every}
<input type="text" class="textbox" name="repeats_5_years2" value="{$repeats_5_years}" size="2" onfocus="$(\'#repeats_5_type_2\').prop(\'checked\', true);" /> {$lang->year_or_years}
</div>
</div>
</td>
</tr>
</table>
</dd>
</dl>
<script type="text/javascript">
<!--
toggleType();
toggleRepeats();
// -->
</script>
</td>
</tr>
<tr>
<td valign="top" width="20%" class="trow1"><strong>{$lang->event_details}</strong>{$smilieinserter}</td>
<td class="trow1"><textarea name="description" id="message" rows="20" cols="70">{$description}</textarea>
{$codebuttons}</td>
</tr>
<tr>
<td width="20%" class="trow2"><strong>{$lang->event_options}</strong></td>
<td class="trow2">
<input type="checkbox" class="checkbox" name="private" value="1"{$privatecheck} /><span class="smalltext">{$lang->private_option}</span><br />
</td>
</tr>
</table>
<br />
<input type="hidden" name="action" value="do_addevent" />
<div align="center"><input type="submit" class="button" value="{$lang->post_event}" /></div>
</form>
{$footer}
</body>
</html>',
            'sid' => -2,
            'version' => '1800',
            'status' => '',
            'dateline' => 1466356529,
        ),
        2 => 
        array (
            'tid' => 6,
            'title' => 'calendar_addevent_calendarselect',
            'template' => '		<tr>
<td width="20%" class="trow1"><strong>{$lang->select_calendar}</strong></td>
<td class="trow1">
<select name="calendar">
{$select_calendar}
</select>
</td>
</tr>',
            'sid' => -2,
            'version' => '1800',
            'status' => '',
            'dateline' => 1466356529,
        ),
        3 => 
        array (
            'tid' => 7,
            'title' => 'calendar_addevent_calendarselect_hidden',
            'template' => '<input type="hidden" name="calendar" value="{$calendar[\'cid\']}" />',
            'sid' => -2,
            'version' => '1800',
            'status' => '',
            'dateline' => 1466356529,
        ),
        4 => 
        array (
            'tid' => 9,
            'title' => 'calendar_addprivateevent',
            'template' => '<a href="calendar.php?action=addevent&amp;calendar={$calendar[\'cid\']}&amp;private=1">{$lang->add_private_event}</a>',
            'sid' => -2,
            'version' => '1400',
            'status' => '',
            'dateline' => 1466356529,
        ),
        5 => 
        array (
            'tid' => 10,
            'title' => 'calendar_addpublicevent',
            'template' => '<a href="calendar.php?action=addevent&amp;calendar={$calendar[\'cid\']}">{$lang->add_public_event}</a>',
            'sid' => -2,
            'version' => '1400',
            'status' => '',
            'dateline' => 1466356529,
        ),
        6 => 
        array (
            'tid' => 11,
            'title' => 'calendar_day',
            'template' => '<option value="{$day}" {$selected}>{$day}</option>',
            'sid' => -2,
            'version' => '1800',
            'status' => '',
            'dateline' => 1466356529,
        ),
        7 => 
        array (
            'tid' => 13,
            'title' => 'calendar_dayview_birthdays',
            'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->birthdays_on_day}</strong></td>
</tr>
<tr>
<td class="trow1"><span class="smalltext">{$birthday_list}</span></td>
</tr>
</table>
<br />',
            'sid' => -2,
            'version' => '1400',
            'status' => '',
            'dateline' => 1466356529,
        ),
        8 => 
        array (
            'tid' => 14,
            'title' => 'calendar_dayview_birthdays_bday',
            'template' => '{$comma}{$birthday[\'profilelink\']}{$age}',
            'sid' => -2,
            'version' => '1400',
            'status' => '',
            'dateline' => 1466356529,
        ),
        9 => 
        array (
            'tid' => 17,
            'title' => 'calendar_editevent',
            'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->calendar} - {$lang->edit_event}</title>
{$headerinclude}
</head>
<body>
{$header}
{$event_errors}
<form action="calendar.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="3"><strong>{$lang->delete_event}</strong></td>
</tr>
<tr>
<td class="trow1" style="white-space: nowrap"><input type="checkbox" class="checkbox" name="delete" value="1" tabindex="9" /> <strong>{$lang->delete_q}</strong></td>
<td class="trow1" width="100%">{$lang->delete_1}<br /><span class="smalltext">{$lang->delete_2}</span></td>
<td class="trow1"><input type="submit" class="button" name="submit" value="{$lang->delete_now}" tabindex="10" /></td>
</tr>
</table>
<input type="hidden" name="action" value="do_deleteevent" />
<input type="hidden" name="eid" value="{$event[\'eid\']}" />
</form>
<br />
<form action="calendar.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder clear">
<tr>
<td colspan="2" width="100%" class="thead"><strong>{$lang->edit_event}</strong></td>
</tr>
<tr>
<td width="20%" class="trow1"><strong>{$lang->event_name}</strong></td>
<td class="trow1"><input type="text" class="textbox" size="50" name="name" maxlength="120" value="{$name}"/></td>
</tr>
<tr>
<td class="trow2" style="vertical-align: top;"><strong>{$lang->event_date}</strong></td>
<td class="trow1">
<script type="text/javascript">
<!--
function toggleType()
{
if($("#type_single").prop("checked"))
{
$("#ranged_selects").hide();
$("#single_selects").show();
}
else
{
$("#single_selects").hide();
$("#ranged_selects").show();
toggleRepeats();
}
}

function toggleRepeats()
{
var repeats = $("#repeats").get(0).selectedIndex;
$(".repeats_type").each(function()
{
$(this).hide();
});
if($("#repeats_type_"+repeats).length > 0)
{
$("#repeats_type_"+repeats).show();
}
}
// -->
</script>
<dl style="margin-top: 0; margin-bottom: 0; width: 100%;">
<dt><input type="radio" name="type" value="single" {$type_single} id="type_single" onclick="toggleType();" /> <label for="type_single"><strong>{$lang->type_single}</strong></label></dt>
<dd style="margin-top: 4px;" id="single_selects">
<select name="single_day">
{$single_days}
</select>
&nbsp;
<select name="single_month">
<option value="1"{$single_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$single_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$single_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$single_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$single_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$single_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$single_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$single_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$single_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$single_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$single_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$single_month[\'12\']}>{$lang->month_12}</option>
</select>
&nbsp;
<select name="single_year">
{$single_years}
</select>
</dd>
<dt><input type="radio" name="type" value="ranged" {$type_ranged} id="type_ranged" onclick="toggleType();" /> <label for="type_ranged"><strong>{$lang->type_ranged}</strong></label></dt>
<dd style="margin-top: 4px; width: 100%;" id="ranged_selects">
<table width="100%">
<tr>
<td class="smalltext" style="padding: 4px; text-align: right;">{$lang->start_time}</td>
<td style="padding: 4px;">
<select name="start_day">
{$start_days}
</select>
<select name="start_month">
<option value="1"{$start_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$start_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$start_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$start_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$start_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$start_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$start_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$start_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$start_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$start_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$start_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$start_month[\'12\']}>{$lang->month_12}</option>
</select>
<select name="start_year">
{$start_years}
</select>
<span class="smalltext">&nbsp;&nbsp;&nbsp;{$lang->enter_time}</span>
<input type="text" class="textbox" name="start_time" value="{$start_time}" size="7" />
</td>
</tr>
<tr>
<td class="smalltext" style="padding: 4px; text-align: right;">{$lang->end_time}</td>
<td style="padding: 4px;">
<select name="end_day">
{$end_days}
</select>
<select name="end_month">
<option value="1"{$end_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$end_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$end_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$end_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$end_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$end_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$end_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$end_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$end_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$end_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$end_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$end_month[\'12\']}>{$lang->month_12}</option>
</select>
<select name="end_year">
{$end_years}
</select>
<span class="smalltext">&nbsp;&nbsp;&nbsp;{$lang->enter_time}</span>
<input type="text" class="textbox" name="end_time" value="{$end_time}" size="7" />
</td>
</tr>
<tr>
<td class="smalltext" style="padding: 4px; text-align: right; vertical-align: top;">{$lang->timezone}</td>
<td style="padding: 4px;">
{$timezones}<br />
<label class="smalltext" style="display: block; padding-top: 4px;"><input type="checkbox" name="ignoretimezone" value="1" {$ignore_timezone} style="vertical-align: middle;" /> {$lang->ignore_timezone}</label>
</td>
</tr>
<tr>
<td class="smalltext" style="padding: 4px; text-align: right; vertical-align:top;">{$lang->repeats}</td>
<td class="smalltext" style="padding: 4px;">
<select name="repeats" id="repeats" onchange="toggleRepeats();">
<option value="0">{$lang->does_not_repeat}</option>
<option value="1"{$repeats_sel[1]}>{$lang->repeats_daily}</option>
<option value="2"{$repeats_sel[2]}>{$lang->repeats_weekdays}</option>
<option value="3"{$repeats_sel[3]}>{$lang->repeats_weekly}</option>
<option value="4"{$repeats_sel[4]}>{$lang->repeats_monthly}</option>
<option value="5"{$repeats_sel[5]}>{$lang->repeats_yearly}</option>
</select>
<div class="repeats_type" id="repeats_type_1" style="padding: 4px;">
{$lang->repeats_every}
<input type="text" class="textbox" name="repeats_1_days" value="{$repeats_1_days}" size="2" /> {$lang->day_or_days}
</div>
<div class="repeats_type" id="repeats_type_3" style="padding: 4px;">
{$lang->repeats_every}
<input type="text" class="textbox" name="repeats_3_weeks" value="{$repeats_3_weeks}" size="2" /> {$lang->week_or_weeks_on}
<br />
<table style="padding: 4px;">
<tr>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[0]" value="1" {$repeats_3_days[0]} style="vertical-align: middle;" /> {$lang->sunday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[1]" value="1" {$repeats_3_days[1]} style="vertical-align: middle;" /> {$lang->monday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[2]" value="1" {$repeats_3_days[2]} style="vertical-align: middle;" /> {$lang->tuesday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[3]" value="1" {$repeats_3_days[3]} style="vertical-align: middle;" /> {$lang->wednesday}</label></td>
</tr>
<tr>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[4]" value="1" {$repeats_3_days[4]} style="vertical-align: middle;" /> {$lang->thursday}</label></td>
<td style="padding: 2px;" class="smalltext"><label><input type="checkbox" name="repeats_3_days[5]" value="1" {$repeats_3_days[5]} style="vertical-align: middle;" /> {$lang->friday}</label></td>
<td style="padding: 2px;" class="smalltext" colspan="2"><label><input type="checkbox" name="repeats_3_days[6]" value="1" {$repeats_3_days[6]} style="vertical-align: middle;" /> {$lang->saturday}</label></td>
</tr>
</table>
</div>
<div class="repeats_type" id="repeats_type_4" style="padding: 4px;">
<div class="smalltext"><label>
<input type="radio" name="repeats_4_type" id="repeats_4_type_1" value="1" style="vertical-align: middle;" {$repeats_4_type[1]} />
{$lang->repeats_on_day}</label>
<input type="text" class="textbox" name="repeats_4_day" value="{$repeats_4_day}" onfocus="$(\'repeats_4_type_1\').checked=true;" size="2" /> {$lang->of_every} <input type="text" class="textbox" name="repeats_4_months" value="{$repeats_4_months}" size="2" onfocus="$(\'repeats_4_type_1\').checked=true;" /> {$lang->month_or_months}
</div>
<div class="smalltext" style="margin-top: 5px;"><label>
<input type="radio" name="repeats_4_type" id="repeats_4_type_2" value="2" style="vertical-align: middle;" {$repeats_4_type[2]} />
{$lang->repeats_on_the}</label>
<select name="repeats_4_occurance" onfocus="$(\'repeats_4_type_2\').checked=true;">
<option value="1"{$repeats_4_occurance[1]}>{$lang->first}</option>
<option value="2"{$repeats_4_occurance[2]}>{$lang->second}</option>
<option value="3"{$repeats_4_occurance[3]}>{$lang->third}</option>
<option value="4"{$repeats_4_occurance[4]}>{$lang->fourth}</option>
<option value="last"{$repeats_4_occurance[\'last\']}>{$lang->last}</option>
</select>
<select name="repeats_4_weekday" onfocus="$(\'repeats_4_type_2\').checked=true;">
<option value="0"{$repeats_4_weekday[0]}>{$lang->sunday}</option>
<option value="1"{$repeats_4_weekday[1]}>{$lang->monday}</option>
<option value="2"{$repeats_4_weekday[2]}>{$lang->tuesday}</option>
<option value="3"{$repeats_4_weekday[3]}>{$lang->wednesday}</option>
<option value="4"{$repeats_4_weekday[4]}>{$lang->thursday}</option>
<option value="5"{$repeats_4_weekday[5]}>{$lang->friday}</option>
<option value="6"{$repeats_4_weekday[6]}>{$lang->saturday}</option>
</select>
{$lang->of_every}
<input type="text" class="textbox" name="repeats_4_months2" value="{$repeats_4_months2}" size="2" onfocus="$(\'repeats_4_type_2\').checked=true;" /> {$lang->month_or_months}
</div>
</div>
<div class="repeats_type" id="repeats_type_5" style="padding: 4px;">
<div class="smalltext"><label>
<input type="radio" name="repeats_5_type" value="1" id="repeats_5_type_1" style="vertical-align: middle;" {$repeats_5_type[1]} />
{$lang->repeats_on}</label>
<input type="text" class="textbox" name="repeats_5_day" onfocus="$(\'repeats_5_type_1\').checked=true;" value="{$repeats_5_day}" size="2" />
<select name="repeats_5_month" onfocus="$(\'repeats_5_type_1\').checked=true;" >
<option value="1"{$repeats_5_month[\'1\']}>{$lang->month_1}</option>
<option value="2"{$repeats_5_month[\'2\']}>{$lang->month_2}</option>
<option value="3"{$repeats_5_month[\'3\']}>{$lang->month_3}</option>
<option value="4"{$repeats_5_month[\'4\']}>{$lang->month_4}</option>
<option value="5"{$repeats_5_month[\'5\']}>{$lang->month_5}</option>
<option value="6"{$repeats_5_month[\'6\']}>{$lang->month_6}</option>
<option value="7"{$repeats_5_month[\'7\']}>{$lang->month_7}</option>
<option value="8"{$repeats_5_month[\'8\']}>{$lang->month_8}</option>
<option value="9"{$repeats_5_month[\'9\']}>{$lang->month_9}</option>
<option value="10"{$repeats_5_month[\'10\']}>{$lang->month_10}</option>
<option value="11"{$repeats_5_month[\'11\']}>{$lang->month_11}</option>
<option value="12"{$repeats_5_month[\'12\']}>{$lang->month_12}</option>
</select>
{$lang->every}
<input type="text" class="textbox" name="repeats_5_years" value="{$repeats_5_years}" onfocus="$(\'repeats_5_type_1\').checked=true;" size="2" /> {$lang->year_or_years}
</div>
<div class="smalltext" style="margin-top: 5px;"><label>
<input type="radio" name="repeats_5_type" value="2" id="repeats_5_type_2" style="vertical-align: middle;" {$repeats_5_type[2]} />
{$lang->repeats_on_the}</label>
<select name="repeats_5_occurance" onfocus="$(\'repeats_5_type_2\').checked=true;" >
<option value="1"{$repeats_5_occurance[1]}>{$lang->first}</option>
<option value="2"{$repeats_5_occurance[2]}>{$lang->second}</option>
<option value="3"{$repeats_5_occurance[3]}>{$lang->third}</option>
<option value="4"{$repeats_5_occurance[4]}>{$lang->fourth}</option>
<option value="last"{$repeats_4_occurance[\'last\']}>{$lang->last}</option>
</select>
<select name="repeats_5_weekday" onfocus="$(\'repeats_5_type_2\').checked=true;" >
<option value="0"{$repeats_5_weekday[0]}>{$lang->sunday}</option>
<option value="1"{$repeats_5_weekday[1]}>{$lang->monday}</option>
<option value="2"{$repeats_5_weekday[2]}>{$lang->tuesday}</option>
<option value="3"{$repeats_5_weekday[3]}>{$lang->wednesday}</option>
<option value="4"{$repeats_5_weekday[4]}>{$lang->thursday}</option>
<option value="5"{$repeats_5_weekday[5]}>{$lang->friday}</option>
<option value="6"{$repeats_5_weekday[6]}>{$lang->saturday}</option>
</select>
{$lang->of}
<select name="repeats_5_month2" onfocus="$(\'repeats_5_type_2\').checked=true;" >
<option value="1"{$repeats_5_month2[\'1\']}>{$lang->month_1}</option>
<option value="2"{$repeats_5_month2[\'2\']}>{$lang->month_2}</option>
<option value="3"{$repeats_5_month2[\'3\']}>{$lang->month_3}</option>
<option value="4"{$repeats_5_month2[\'4\']}>{$lang->month_4}</option>
<option value="5"{$repeats_5_month2[\'5\']}>{$lang->month_5}</option>
<option value="6"{$repeats_5_month2[\'6\']}>{$lang->month_6}</option>
<option value="7"{$repeats_5_month2[\'7\']}>{$lang->month_7}</option>
<option value="8"{$repeats_5_month2[\'8\']}>{$lang->month_8}</option>
<option value="9"{$repeats_5_month2[\'9\']}>{$lang->month_9}</option>
<option value="10"{$repeats_5_month2[\'10\']}>{$lang->month_10}</option>
<option value="11"{$repeats_5_month2[\'11\']}>{$lang->month_11}</option>
<option value="12"{$repeats_5_month2[\'12\']}>{$lang->month_12}</option>
</select>
{$lang->every}
<input type="text" class="textbox" name="repeats_5_years2" value="{$repeats_5_years}" size="2" onfocus="$(\'repeats_5_type_2\').checked=true;" /> {$lang->year_or_years}
</div>
</div>
</td>
</tr>
</table>
</dd>
</dl>
<script type="text/javascript">
<!--
toggleType();
toggleRepeats();
// -->
</script>
</td>
</tr>
<tr>
<td valign="top" width="20%" class="trow1"><strong>{$lang->event_details}</strong>{$smilieinserter}</td>
<td class="trow1"><textarea name="description" id="message" rows="20" cols="70">{$description}</textarea>
{$codebuttons}</td>
</tr>
<tr>
<td width="20%" class="trow2"><strong>{$lang->event_options}</strong></td>
<td class="trow2">
<input type="checkbox" class="checkbox" name="private" value="1"{$privatecheck} /><span class="smalltext">{$lang->private_option}</span><br />
</td>
</tr>
</table>
<br />
<input type="hidden" name="action" value="do_editevent" />
<input type="hidden" name="eid" value="{$event[\'eid\']}" />
<div align="center"><input type="submit" class="button" value="{$lang->edit_event}" /></div>
</form>
{$footer}
</body>
</html>',
        'sid' => -2,
        'version' => '1806',
        'status' => '',
        'dateline' => 1466356529,
    ),
    10 => 
    array (
        'tid' => 22,
        'title' => 'calendar_eventbit',
        'template' => '<div style="margin-bottom: 4px;" class="smalltext {$event_class}"><a href="{$event[\'eventlink\']}" title="{$event[\'fullname\']}">{$event[\'name\']}</a></div>',
        'sid' => -2,
        'version' => '1402',
        'status' => '',
        'dateline' => 1466356529,
    ),
    11 => 
    array (
        'tid' => 23,
        'title' => 'calendar_jump',
        'template' => '<br />
<span class="smalltext"><strong>{$lang->jump_to_calendar}</strong></span>
<select name="calendar">
{$jump_options}
</select>
{$gobutton}',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    12 => 
    array (
        'tid' => 24,
        'title' => 'calendar_jump_option',
        'template' => '<option value="{$calendar[\'cid\']}" {$sel}>{$calendar[\'name\']}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    13 => 
    array (
        'tid' => 29,
        'title' => 'calendar_move',
        'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->calendar} - {$lang->move_event}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="calendar.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder clear">
<tr>
<td colspan="2" width="100%" class="thead"><strong>{$lang->move_event}: {$event[\'name\']}</strong></td>
</tr>
<tr>
<td width="20%" class="trow1"><strong>{$lang->move_to_calendar}</strong></td>
<td class="trow1">
<select name="new_calendar">
{$calendar_select}
</select>
</td>
</tr>
</table>
<br />
<input type="hidden" name="action" value="do_move" />
<input type="hidden" name="eid" value="{$event[\'eid\']}" />
<div align="center"><input type="submit" class="button" value="{$lang->move_event}" /></div>
</form>
{$footer}
</body>
</html>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    14 => 
    array (
        'tid' => 31,
        'title' => 'calendar_select',
        'template' => '<option value="{$calendar_option[\'cid\']}"{$selected}>{$calendar_option[\'name\']}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    15 => 
    array (
        'tid' => 32,
        'title' => 'calendar_weekdayheader',
        'template' => '				<td class="tcat" align="center" width="14%"><strong>{$weekday_name}</strong></td>',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    16 => 
    array (
        'tid' => 33,
        'title' => 'calendar_weekrow',
        'template' => '			<tr>
<td class="tcat" align="center" width="1"><a href="{$week_link}">&raquo;</a></td>
{$day_bits}
</tr>',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    17 => 
    array (
        'tid' => 34,
        'title' => 'calendar_weekrow_day',
        'template' => '				<td class="{$day_class}" style="vertical-align: top; height: 100px;">
<div class="float_right smalltext"><a href="{$day_link}">{$day}</a></div>
<div class="clear">
{$day_birthdays}
{$day_events}
</div>
</td>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    18 => 
    array (
        'tid' => 35,
        'title' => 'calendar_weekrow_day_birthdays',
        'template' => '<div style="margin-bottom: 4px;"><a href="{$calendar[\'link\']}" class="smalltext">{$bday_count} {$birthday_lang}</a></div>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    19 => 
    array (
        'tid' => 36,
        'title' => 'calendar_weekrow_day_external',
        'template' => '				<td class="calendar_daybit_external">
<div class="float_right smalltext"><a href="{$day_link}">{$day}</a></div>
<div>
{$day_events}
{$day_birthdays}
</div>
</td>',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    20 => 
    array (
        'tid' => 37,
        'title' => 'calendar_weekrow_day_today',
        'template' => '				<td class="calendar_daybit_today">
<div class="float_right smalltext"><a href="{$day_link}">{$day}</a></div>
<div>
{$day_events}
{$day_birthdays}
</div>
</td>',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    21 => 
    array (
        'tid' => 39,
        'title' => 'calendar_weekview_day',
        'template' => '			<tr>
<td class="trow_sep" colspan="2">{$weekday_name}</td>
</tr>
<tr>
<td class="trow2{$day_shaded}" style="text-align: center; padding: 8px;"><span class="largetext">{$weekday_day}</span></td>
<td class="trow1" width="100%">{$day_birthdays}{$day_events}</td>
</tr>',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    22 => 
    array (
        'tid' => 41,
        'title' => 'calendar_weekview_day_event',
        'template' => '<a href="{$event[\'eventlink\']}" class="{$event_class}" title="{$event[\'fullname\']}">{$event[\'fullname\']}</a>{$event_time}<br />',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    23 => 
    array (
        'tid' => 42,
        'title' => 'calendar_weekview_day_event_time',
    'template' => '<span class="smalltext"> ({$time_period})</span>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    24 => 
    array (
        'tid' => 44,
        'title' => 'calendar_year',
        'template' => '<option value="{$year}" {$selected}>{$year}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    25 => 
    array (
        'tid' => 51,
        'title' => 'error',
        'template' => '<html>
<head>
<title>{$title}</title>
{$headerinclude}
</head>
<body>
{$header}
<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><span class="smalltext"><strong>{$title}</strong></span></td>
</tr>
<tr>
<td class="trow1">{$error}</td>
</tr>
</table>
{$footer}
</body>
</html>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    26 => 
    array (
        'tid' => 53,
        'title' => 'error_inline',
        'template' => '<div class="error">
<p><em>{$title}</em></p>
<ul>
{$errorlist}
</ul>
</div>
<br />',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    27 => 
    array (
        'tid' => 54,
        'title' => 'error_nopermission',
        'template' => '{$lang->error_nopermission_guest_1}
<ol>
<li>{$lang->error_nopermission_guest_2}</li>
<li>{$lang->error_nopermission_guest_3}</li>
<li>{$lang->error_nopermission_guest_4}</li>
<li>{$lang->error_nopermission_guest_5}</li>
</ol>
<form action="member.php" method="post">
<input type="hidden" name="action" value="do_login" />
<input type="hidden" name="url" value="{$redirect_url}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><span class="smalltext"><strong>{$lang->login}</strong></span></td>
</tr>
<tr>
<td class="trow1"><strong>{$lang_username}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="username" tabindex="1" /></td>
</tr>
<tr>
<td class="trow2"><strong>{$lang->password}</strong></td>
<td class="trow2"><input type="password" class="textbox" name="password" tabindex="2" /></td>
</tr>
<tr>
<td class="trow2" colspan="2"><span class="smalltext float_right" style="padding-top: 3px;"><a href="member.php?action=register">{$lang->need_reg}</a> | <a href="member.php?action=lostpw">{$lang->forgot_password}</a>&nbsp;</span>&nbsp;<input type="submit" class="button" value="{$lang->login}" tabindex="3" /></td>
</tr>
</table>
</form>
<br />',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    28 => 
    array (
        'tid' => 55,
        'title' => 'error_nopermission_loggedin',
        'template' => '{$lang->error_nopermission_user_1}
<ol>
<li>{$lang->error_nopermission_user_2}</li>
<li>{$lang->error_nopermission_user_3}</li>
<li>{$lang->error_nopermission_user_4} (<a href="member.php?action=resendactivation">{$lang->error_nopermission_user_resendactivation}</a>)</li>
<li>{$lang->error_nopermission_user_5}</li>
</ol>
<br />
{$lang->error_nopermission_user_username}',
        'sid' => -2,
        'version' => '1600',
        'status' => '',
        'dateline' => 1466356529,
    ),
    29 => 
    array (
        'tid' => 62,
        'title' => 'forumbit_depth1_cat',
        'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<thead>
<tr>
<td class="thead{$expthead}" colspan="5">
<div class="expcolimage"><img src="{$theme[\'imgdir\']}/{$expcolimage}" id="cat_{$forum[\'fid\']}_img" class="expander" alt="{$expaltext}" title="{$expaltext}" /></div>
<div><strong><a href="{$forum_url}">{$forum[\'name\']}</a></strong><br /><div class="smalltext">{$forum[\'description\']}</div></div>
</td>
</tr>
</thead>
<tbody style="{$expdisplay}" id="cat_{$forum[\'fid\']}_e">
<tr>
<td class="tcat" colspan="2"><span class="smalltext"><strong>{$lang->forumbit_forum}</strong></span></td>
<td class="tcat" width="85" align="center" style="white-space: nowrap"><span class="smalltext"><strong>{$lang->forumbit_threads}</strong></span></td>
<td class="tcat" width="85" align="center" style="white-space: nowrap"><span class="smalltext"><strong>{$lang->forumbit_posts}</strong></span></td>
<td class="tcat" width="200" align="center"><span class="smalltext"><strong>{$lang->forumbit_lastpost}</strong></span></td>
</tr>
{$sub_forums}
</tbody>
</table>
<br />',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    30 => 
    array (
        'tid' => 63,
        'title' => 'forumbit_depth1_cat_subforum',
        'template' => '<tr>
<td class="tcat" colspan="6"><strong>&raquo;&nbsp;&nbsp;<a href="{$forum_url}">{$forum[\'name\']}</a></strong><br /><span class="smalltext">{$forum[\'description\']}</span></td></tr>{$sub_forums}',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    31 => 
    array (
        'tid' => 64,
        'title' => 'forumbit_depth1_forum_lastpost',
        'template' => '<span class="smalltext">
<a href="{$lastpost_link}" title="{$full_lastpost_subject}"><strong>{$lastpost_subject}</strong></a>
<br />{$lastpost_date}<br />{$lang->by} {$lastpost_profilelink}</span>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    32 => 
    array (
        'tid' => 65,
        'title' => 'forumbit_depth2_cat',
        'template' => '<tr>
<td class="{$bgcolor}" align="center" width="1"><span class="forum_status forum_{$lightbulb[\'folder\']} ajax_mark_read" title="{$lightbulb[\'altonoff\']}" id="mark_read_{$forum[\'fid\']}"></span></td>
<td class="{$bgcolor}">
<strong><a href="{$forum_url}">{$forum[\'name\']}</a></strong>{$forum_viewers_text}<div class="smalltext">{$forum[\'description\']}{$subforums}</div>
</td>
<td class="{$bgcolor}" align="center" style="white-space: nowrap">{$threads}{$unapproved[\'unapproved_threads\']}</td>
<td class="{$bgcolor}" align="center" style="white-space: nowrap">{$posts}{$unapproved[\'unapproved_posts\']}</td>
<td class="{$bgcolor}" align="right" style="white-space: nowrap">{$lastpost}</td>
</tr>',
        'sid' => -2,
        'version' => '1804',
        'status' => '',
        'dateline' => 1466356529,
    ),
    33 => 
    array (
        'tid' => 66,
        'title' => 'forumbit_depth2_forum',
        'template' => '<tr>
<td class="{$bgcolor}" align="center" width="1"><span class="forum_status forum_{$lightbulb[\'folder\']} ajax_mark_read" title="{$lightbulb[\'altonoff\']}" id="mark_read_{$forum[\'fid\']}"></span></td>
<td class="{$bgcolor}">
<strong><a href="{$forum_url}">{$forum[\'name\']}</a></strong>{$forum_viewers_text}<div class="smalltext">{$forum[\'description\']}{$modlist}{$subforums}</div>
</td>
<td class="{$bgcolor}" align="center" style="white-space: nowrap">{$threads}{$unapproved[\'unapproved_threads\']}</td>
<td class="{$bgcolor}" align="center" style="white-space: nowrap">{$posts}{$unapproved[\'unapproved_posts\']}</td>
<td class="{$bgcolor}" align="right" style="white-space: nowrap">{$lastpost}</td>
</tr>',
        'sid' => -2,
        'version' => '1804',
        'status' => '',
        'dateline' => 1466356529,
    ),
    34 => 
    array (
        'tid' => 70,
        'title' => 'forumbit_depth2_forum_unapproved_posts',
    'template' => ' <span title="{$unapproved_posts_count}">({$forum[\'unapprovedposts\']})</span>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    35 => 
    array (
        'tid' => 71,
        'title' => 'forumbit_depth2_forum_unapproved_threads',
    'template' => ' <span title="{$unapproved_threads_count}">({$forum[\'unapprovedthreads\']})</span>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    36 => 
    array (
        'tid' => 72,
        'title' => 'forumbit_depth2_forum_viewers',
        'template' => '<span class="smalltext">{$forum_viewers_text}</span>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    37 => 
    array (
        'tid' => 73,
        'title' => 'forumbit_depth3',
        'template' => '{$comma}{$statusicon}<a href="{$forum_url}" title="{$forum_viewers_text_plain}">{$forum[\'name\']}</a>',
        'sid' => -2,
        'version' => '1404',
        'status' => '',
        'dateline' => 1466356529,
    ),
    38 => 
    array (
        'tid' => 74,
        'title' => 'forumbit_depth3_statusicon',
        'template' => '<div title="{$lightbulb[\'altonoff\']}" class="subforumicon subforum_{$lightbulb[\'folder\']} ajax_mark_read" id="mark_read_{$forum[\'fid\']}"></div>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    39 => 
    array (
        'tid' => 78,
        'title' => 'forumbit_subforums',
        'template' => '<br />{$lang->subforums} {$sub_forums}',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    40 => 
    array (
        'tid' => 80,
        'title' => 'forumdisplay_announcement_rating',
        'template' => '	<td class="{$bgcolor} forumdisplay_announcement" align="center">-</td>',
        'sid' => -2,
        'version' => '1801',
        'status' => '',
        'dateline' => 1466356529,
    ),
    41 => 
    array (
        'tid' => 81,
        'title' => 'forumdisplay_announcements',
        'template' => '<tr>
<td class="trow_sep" colspan="{$colspan}">{$lang->forum_announcements}</td>
</tr>
{$announcements}',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    42 => 
    array (
        'tid' => 85,
        'title' => 'forumdisplay_inlinemoderation_approveunapprove',
        'template' => '<option value="multiapprovethreads">{$lang->approve_threads}</option>
<option value="multiunapprovethreads">{$lang->unapprove_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    43 => 
    array (
        'tid' => 86,
        'title' => 'forumdisplay_inlinemoderation_col',
    'template' => '<td class="tcat" align="center" width="1"><input type="checkbox" name="allbox" onclick="inlineModeration.checkAll(this)" /></td>',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    44 => 
    array (
        'tid' => 87,
        'title' => 'forumdisplay_inlinemoderation_custom',
        'template' => '<optgroup label="{$lang->custom_mod_tools}">{$customthreadtools}</optgroup>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    45 => 
    array (
        'tid' => 88,
        'title' => 'forumdisplay_inlinemoderation_custom_tool',
        'template' => '<option value="{$tool[\'tid\']}">{$tool[\'name\']}</option>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    46 => 
    array (
        'tid' => 89,
        'title' => 'forumdisplay_inlinemoderation_delete',
        'template' => '<option value="multideletethreads">{$lang->delete_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    47 => 
    array (
        'tid' => 90,
        'title' => 'forumdisplay_inlinemoderation_manage',
        'template' => '<option value="multimovethreads">{$lang->move_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    48 => 
    array (
        'tid' => 91,
        'title' => 'forumdisplay_inlinemoderation_openclose',
        'template' => '<option value="multiclosethreads">{$lang->close_threads}</option>
<option value="multiopenthreads">{$lang->open_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    49 => 
    array (
        'tid' => 92,
        'title' => 'forumdisplay_inlinemoderation_restore',
        'template' => '<option value="multirestorethreads">{$lang->restore_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    50 => 
    array (
        'tid' => 93,
        'title' => 'forumdisplay_inlinemoderation_selectall',
        'template' => '<tr id="selectAllrow" class="hiddenrow">
<td colspan="8" class="selectall">{$lang->page_selected} <a href="#" onclick=\'inlineModeration.selectAll();\'>{$lang->select_all}</a></td>
</tr>
<tr id="allSelectedrow" class="hiddenrow">
<td colspan="8" class="selectall">{$lang->all_selected} <a href="#" onclick=\'inlineModeration.clearChecked();\'>{$lang->clear_selection}</a></td>
</tr>',
        'sid' => -2,
        'version' => '1600',
        'status' => '',
        'dateline' => 1466356529,
    ),
    51 => 
    array (
        'tid' => 94,
        'title' => 'forumdisplay_inlinemoderation_softdelete',
        'template' => '<option value="multisoftdeletethreads">{$lang->soft_delete_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    52 => 
    array (
        'tid' => 96,
        'title' => 'forumdisplay_inlinemoderation_stickunstick',
        'template' => '<option value="multistickthreads">{$lang->stick_threads}</option>
<option value="multiunstickthreads">{$lang->unstick_threads}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    53 => 
    array (
        'tid' => 98,
        'title' => 'forumdisplay_newthread',
        'template' => '<a href="newthread.php?fid={$fid}" class="button new_thread_button"><span>{$lang->post_thread}</span></a>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    54 => 
    array (
        'tid' => 99,
        'title' => 'forumdisplay_nopermission',
        'template' => '<tr>
<td colspan="{$colspan}" class="trow1">{$lang->nopermission}</td>
</tr>',
        'sid' => -2,
        'version' => '1807',
        'status' => '',
        'dateline' => 1466356529,
    ),
    55 => 
    array (
        'tid' => 100,
        'title' => 'forumdisplay_nothreads',
        'template' => '<tr>
<td colspan="{$colspan}" class="trow1">{$lang->nothreads}</td>
</tr>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    56 => 
    array (
        'tid' => 101,
        'title' => 'forumdisplay_orderarrow',
        'template' => '<span class="smalltext">[<a href="{$sorturl}&amp;sortby={$sortby}&amp;order={$oppsortnext}">{$oppsort}</a>]</span>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    57 => 
    array (
        'tid' => 102,
        'title' => 'forumdisplay_password',
        'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->password_required} </title>
{$headerinclude}
</head>
<body>
{$header}
<form action="{$_SERVER[\'REQUEST_URI\']}" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="2"><strong>{$lang->password_required}</strong></td>
</tr>
<tr>
<td class="trow2" colspan="2">{$lang->forum_password_note}</td>
</tr>
<tr>
<td class="tcat" colspan="2"><strong>{$lang->enter_password_below}</strong></td>
</tr>
{$pwnote}
<tr>
<td class="trow1" align="center" colspan="2"><input type="password" class="textbox" name="pwverify" size="50" value="" /></td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->verify_forum_password}" /></div>
</form>
{$footer}
</body>
</html>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    58 => 
    array (
        'tid' => 103,
        'title' => 'forumdisplay_password_wrongpass',
        'template' => '<tr>
<td class="trow1" align="center" colspan="2"><strong>{$lang->wrong_forum_password}</strong></td>
</tr>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    59 => 
    array (
        'tid' => 107,
        'title' => 'forumdisplay_searchforum',
        'template' => '<form action="search.php" method="post">
<span class="smalltext"><strong>{$lang->search_forum}</strong></span>
<input type="text" class="textbox" name="keywords" /> {$gobutton}
<input type="hidden" name="action" value="do_search" />
<input type="hidden" name="forums" value="{$fid}" />
<input type="hidden" name="postthread" value="1" />
</form><br />',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    60 => 
    array (
        'tid' => 108,
        'title' => 'forumdisplay_sticky_sep',
        'template' => '<tr>
<td class="trow_sep" colspan="{$colspan}">{$lang->sticky_threads}</td>
</tr>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    61 => 
    array (
        'tid' => 109,
        'title' => 'forumdisplay_subforums',
        'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="5" align="center"><strong>{$lang->sub_forums_in}</strong></td>
</tr>
<tr>
<td class="tcat" width="2%">&nbsp;</td>
<td class="tcat" width="59%"><span class="smalltext"><strong>{$lang->forumbit_forum}</strong></span></td>
<td class="tcat" width="7%" align="center" style="white-space: nowrap"><span class="smalltext"><strong>{$lang->forumbit_threads}</strong></span></td>
<td class="tcat" width="7%" align="center" style="white-space: nowrap"><span class="smalltext"><strong>{$lang->forumbit_posts}</strong></span></td>
<td class="tcat" width="15%" align="center"><span class="smalltext"><strong>{$lang->forumbit_lastpost}</strong></span></td>
</tr>
{$forums}
</table>
<br />',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    62 => 
    array (
        'tid' => 110,
        'title' => 'forumdisplay_thread',
        'template' => '<tr class="inline_row">
<td align="center" class="{$bgcolor}{$thread_type_class}" width="2%"><span class="thread_status {$folder}" title="{$folder_label}">&nbsp;</span></td>
<td align="center" class="{$bgcolor}{$thread_type_class}" width="2%">{$icon}</td>
<td class="{$bgcolor}{$thread_type_class}">
{$attachment_count}
<div>
<span>{$prefix} {$gotounread}{$thread[\'threadprefix\']}<span class="{$inline_edit_class} {$new_class}" id="tid_{$inline_edit_tid}"><a href="{$thread[\'threadlink\']}">{$thread[\'subject\']}</a></span>{$thread[\'multipage\']}</span>
<div class="author smalltext">{$thread[\'profilelink\']}</div>
</div>
</td>
<td align="center" class="{$bgcolor}{$thread_type_class}"><a href="javascript:MyBB.whoPosted({$thread[\'tid\']});">{$thread[\'replies\']}</a>{$unapproved_posts}</td>
<td align="center" class="{$bgcolor}{$thread_type_class}">{$thread[\'views\']}</td>
{$rating}
<td class="{$bgcolor}{$thread_type_class}" style="white-space: nowrap; text-align: right;">
<span class="lastpost smalltext">{$lastpostdate}<br />
<a href="{$thread[\'lastpostlink\']}">{$lang->lastpost}</a>: {$lastposterlink}</span>
</td>
{$modbit}
</tr>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    63 => 
    array (
        'tid' => 111,
        'title' => 'forumdisplay_thread_attachment_count',
        'template' => '<div class="float_right"><img src="{$theme[\'imgdir\']}/paperclip.png" alt="" title="{$attachment_count}" /></div>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    64 => 
    array (
        'tid' => 114,
        'title' => 'forumdisplay_thread_modbit',
        'template' => '<td class="{$bgcolor}{$thread_type_class}" align="center" style="white-space: nowrap"><input type="checkbox" class="checkbox" name="inlinemod_{$multitid}" id="inlinemod_{$multitid}" value="1" {$inlinecheck}  /></td>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    65 => 
    array (
        'tid' => 115,
        'title' => 'forumdisplay_thread_multipage',
    'template' => ' <span class="smalltext">({$lang->pages} {$threadpages}{$morelink})</span>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    66 => 
    array (
        'tid' => 116,
        'title' => 'forumdisplay_thread_multipage_more',
        'template' => '... <a href="{$page_link}">{$thread[\'pages\']}</a>',
        'sid' => -2,
        'version' => '1600',
        'status' => '',
        'dateline' => 1466356529,
    ),
    67 => 
    array (
        'tid' => 117,
        'title' => 'forumdisplay_thread_multipage_page',
        'template' => '<a href="{$page_link}">{$i}</a> ',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    68 => 
    array (
        'tid' => 118,
        'title' => 'forumdisplay_thread_rating',
        'template' => '<td align="center" class="{$bgcolor}{$thread_type_class}" id="rating_table_{$thread[\'tid\']}">
<ul class="star_rating{$not_rated}" id="rating_thread_{$thread[\'tid\']}">
<li style="width: {$thread[\'width\']}%" class="current_rating" id="current_rating_{$thread[\'tid\']}">{$ratingvotesav}</li>
</ul>
<script type="text/javascript">
<!--
Rating.build_forumdisplay({$thread[\'tid\']}, { width: \'{$thread[\'width\']}\', extra_class: \'{$not_rated}\', current_average: \'{$ratingvotesav}\' });
// -->
</script>
</td>',
        'sid' => -2,
        'version' => '1600',
        'status' => '',
        'dateline' => 1466356529,
    ),
    69 => 
    array (
        'tid' => 119,
        'title' => 'forumdisplay_thread_rating_moved',
        'template' => '<td class="{$bgcolor}" style="text-align: center;">-</td>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    70 => 
    array (
        'tid' => 120,
        'title' => 'forumdisplay_thread_unapproved_posts',
    'template' => ' <span title="{$unapproved_posts_count}">({$thread[\'unapprovedposts\']})</span>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    71 => 
    array (
        'tid' => 123,
        'title' => 'forumdisplay_threadlist_prefixes',
        'template' => '<select name="prefix">
<option value="-2"{$default_selected[\'any\']}>{$lang->prefix_any}</option>
<option value="-1"{$default_selected[\'none\']}>{$lang->prefix_none}</option>
<option value="0"{$default_selected[\'all\']}>{$lang->prefix_all}</option>
{$prefixselect_prefix}
</select>',
        'sid' => -2,
        'version' => '1801',
        'status' => '',
        'dateline' => 1466356529,
    ),
    72 => 
    array (
        'tid' => 124,
        'title' => 'forumdisplay_threadlist_prefixes_prefix',
        'template' => '<option value="{$prefix[\'pid\']}"{$selected}>{$lang->prefix} {$prefix[\'prefix\']}</option>',
        'sid' => -2,
        'version' => '1800',
        'status' => '',
        'dateline' => 1466356529,
    ),
    73 => 
    array (
        'tid' => 125,
        'title' => 'forumdisplay_threadlist_rating',
        'template' => '	<td class="tcat" align="center" width="80">
<span class="smalltext"><strong><a href="{$sorturl}&amp;sortby=rating&amp;order=desc">{$lang->rating}</a> {$orderarrow[\'rating\']}</strong></span>
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/rating.js?ver=1804"></script>
<script type="text/javascript">
<!--
lang.stars = new Array();
lang.stars[1] = "{$lang->one_star}";
lang.stars[2] = "{$lang->two_stars}";
lang.stars[3] = "{$lang->three_stars}";
lang.stars[4] = "{$lang->four_stars}";
lang.stars[5] = "{$lang->five_stars}";
lang.ratings_update_error = "{$lang->ratings_update_error}";
// -->
</script>
</td>',
        'sid' => -2,
        'version' => '1804',
        'status' => '',
        'dateline' => 1466356529,
    ),
    74 => 
    array (
        'tid' => 126,
        'title' => 'forumdisplay_threadlist_sortrating',
        'template' => '<option value="rating" {$sortsel[\'rating\']}>{$lang->sort_by_rating}</option>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    75 => 
    array (
        'tid' => 127,
        'title' => 'forumdisplay_threads_sep',
        'template' => '<tr>
<td class="trow_sep" colspan="{$colspan}">{$lang->normal_threads}</td>
</tr>',
        'sid' => -2,
        'version' => '120',
        'status' => '',
        'dateline' => 1466356529,
    ),
    76 => 
    array (
        'tid' => 128,
        'title' => 'forumdisplay_usersbrowsing',
        'template' => '<span class="smalltext">{$lang->users_browsing_forum} {$onlinemembers}{$onlinesep}{$invisonline}{$onlinesep2}{$guestsonline}</span><br />',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    77 => 
    array (
        'tid' => 129,
        'title' => 'forumdisplay_usersbrowsing_user',
        'template' => '{$comma}{$user[\'profilelink\']}{$invisiblemark}',
        'sid' => -2,
        'version' => '1400',
        'status' => '',
        'dateline' => 1466356529,
    ),
    78 => 
    array (
        'tid' => 130,
        'title' => 'forumjump_advanced',
        'template' => '<form action="forumdisplay.php" method="get">
<span class="smalltext"><strong>{$lang->forumjump}</strong></span>
<select name="{$name}" class="forumjump">
<option value="-4">{$lang->forumjump_pms}</option>
<option value="-3">{$lang->forumjump_usercp}</option>
<option value="-5">{$lang->forumjump_wol}</option>
<option value="-2">{$lang->forumjump_search}</option>
<option value="-1">{$lang->forumjump_home}</option>
{$forumjumpbits}
</select>
{$gobutton}
</form>
<script type="text/javascript">
$(".forumjump").change(function() {
var option = $(this).val();

if(option < 0)
{
window.location = \'forumdisplay.php?fid=\'+option;
}
else
{
window.location = {$forum_link};
}
});
</script>',
    'sid' => -2,
    'version' => '1804',
    'status' => '',
    'dateline' => 1466356529,
),
79 => 
array (
    'tid' => 131,
    'title' => 'forumjump_bit',
    'template' => '<option value="{$forum[\'fid\']}" {$optionselected}>{$depth} {$forum[\'name\']}</option>',
    'sid' => -2,
    'version' => '120',
    'status' => '',
    'dateline' => 1466356529,
),
80 => 
array (
    'tid' => 132,
    'title' => 'forumjump_special',
    'template' => '<select name="{$name}">
{$forumjumpbits}
</select>',
    'sid' => -2,
    'version' => '1804',
    'status' => '',
    'dateline' => 1466356529,
),
81 => 
array (
    'tid' => 171,
    'title' => 'member_activate',
    'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->account_activation}</title>
{$headerinclude}
</head>
<body>
{$header}
<br />
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->activate_account}</strong></td>
</tr>
<tr>
<td class="trow1" width="30%"><strong>{$lang->username}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="username" value="{$user[\'username\']}" /></td>
</tr>
<tr>
<td class="trow1" width="30%"><strong>{$lang->activation_code}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="code" value="{$code}" /></td>
</tr>
</table>
<br />
<div align="center"><input type="hidden" name="action" value="activate" /><input type="submit" class="button" name="regsubmit" value="{$lang->activate_account}" /></div>
</form>
{$footer}
</body>
</html>',
    'sid' => -2,
    'version' => '120',
    'status' => '',
    'dateline' => 1466356529,
),
82 => 
array (
    'tid' => 172,
    'title' => 'member_coppa_form',
    'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->coppa_compliance}</title>
<style type="text/css">
body { font-family: Verdana, Arial, sans-serif; font-size: 13px; }
.largetext, h1 { font-family: Verdana, Arial, sans-serif; font-size: medium; font-weight: bold; margin: 0; }
.grey { color: #ccc; }
dl { margin-left: 40px; }
dt { width: 150px; float: left; clear: left; font-weight: bold; }
dd { margin-bottom: 20px; margin-left: 150px; }
dd.single { margin-left: 0; }
input.textbox { border: 0; border-bottom: 1px solid #000; font-size: 16px; width: 350px; }
input.date { width: 80px; }
</style>
</head>
<body>
<div class="largetext float_right" style="text-align: center; padding-bottom: 10px;">
{$mybb->settings[\'bbname\']}<br /><span class="grey">{$lang->coppa_registration}</span>
</div>
<div style="padding-bottom: 10px;"><a href="index.php"><img src="{$theme[\'logo\']}" alt="{$mybb->settings[\'bbname\']}" border="0" /></a></div>
<br class="clear" />

<p>{$lang->coppa_form_instructions}</p>

<dl>
<dt>{$lang->fax_number}</dt>
<dd>{$mybb->settings[\'faxno\']}</dd>

<dt>{$lang->mailing_address}</dt>
<dd>{$mybb->settings[\'mailingaddress\']}</dd>
</dl>

<h1 class="clear" />{$lang->account_information}</h1>
<dl>
<dt><label for="username">{$lang->username}</label></dt>
<dd><input type="text" class="textbox" id="username" /></dd>

<dt><label for="email">{$lang->email}</label></dt>
<dd><input type="text" class="textbox" id="email" /></dd>
</dl>
<h1 class="clear" />{$lang->parent_details}</h1>
<dl>
<dt><label for="name">{$lang->full_name}</label></dt>
<dd><input type="text" class="textbox" id="name" /></dd>

<dt><label for="relation">{$lang->relation}</label></dt>
<dd><input type="text" class="textbox" id="relation" /></dd>

<dt><label for="phone">{$lang->phone_no}</label></dt>
<dd><input type="text" class="textbox" id="phone" /></dd>

<dt><label for="parent_email">{$lang->email}</label></dt>
<dd><input type="text" class="textbox" id="parent_email" /></dd>

<dd class="single">{$lang->coppa_parent_agreement}</dd>

<dt><label for="signature">{$lang->signature}</label></dt>
<dd><input type="text" class="textbox" id="signature" /> {$lang->date}: <input type="text" class="textbox date" id="date" /></dd>
</dl>
</body>
</html>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
83 => 
array (
    'tid' => 173,
    'title' => 'member_emailuser',
    'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->email_user}</title>
{$headerinclude}
</head>
<body>
{$header}
{$errors}
<form action="member.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td colspan="2" width="100%" class="thead"><strong>{$lang->email_user}</strong></td>
</tr>
{$from_email}
<tr>
<td width="40%" class="trow2"><strong>{$lang->email_subject}</strong></td>
<td width="60%" class="trow2"><input type="text" class="textbox" size="50" name="subject" value="{$subject}" /></td>
</tr>
<tr>
<td valign="top" width="40%" class="trow1"><strong>{$lang->email_message}</strong></td>
<td width="60%" class="trow1"><textarea cols="50" rows="10" name="message">{$message}</textarea></td>
</tr>
{$captcha}
</table>
<br />
<input type="hidden" name="action" value="do_emailuser" />
<input type="hidden" name="uid" value="{$to_user[\'uid\']}" />
<div align="center"><input type="submit" class="button" value="{$lang->send_email}" /></div>
</form>
{$footer}
</body>
</html>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
84 => 
array (
    'tid' => 174,
    'title' => 'member_emailuser_guest',
    'template' => '<tr>
<td width="40%" class="trow2"><strong>{$lang->your_name}</strong><br /><span class="smalltext">{$lang->name_note}</span></td>
<td width="60%" class="trow2"><input type="text" class="textbox" size="50" name="fromname" value="{$fromname}" /></td>
</tr>
<tr>
<td width="40%" class="trow1"><strong>{$lang->your_email}</strong><br /><span class="smalltext">{$lang->email_note}</span></td>
<td width="60%" class="trow1"><input type="text" class="textbox" size="50" name="fromemail"  value="{$fromemail}" /></td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
85 => 
array (
    'tid' => 175,
    'title' => 'member_loggedin_notice',
    'template' => '<fieldset class="align_right smalltext">{$lang->already_logged_in}</fieldset>
<br />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
86 => 
array (
    'tid' => 176,
    'title' => 'member_login',
    'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->login}</title>
{$headerinclude}
</head>
<body>
{$header}
<br />
{$inline_errors}
{$member_loggedin_notice}
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->login}</strong></td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->username}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="username" size="25" style="width: 200px;" value="{$username}" /></td>
</tr>
<tr>
<td class="trow2"><strong>{$lang->password}</strong><br /><span class="smalltext">{$lang->pw_note}</span></td>
<td class="trow2"><input type="password" class="textbox" name="password" size="25" style="width: 200px;" value="{$password}" /> (<a href="member.php?action=lostpw">{$lang->lostpw_note}</a>)</td>
</tr>
<tr>
<td class="trow1" colspan="2" align="center"><label title="{$lang->remember_me_desc}"><input type="checkbox" class="checkbox" name="remember" checked="checked" value="yes" /> {$lang->remember_me}</label></td>
</tr>
{$captcha}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->login}" /></div>
<input type="hidden" name="action" value="do_login" />
<input type="hidden" name="url" value="{$redirect_url}" />
</form>
{$footer}
</body>
</html>',
    'sid' => -2,
    'version' => '1611',
    'status' => '',
    'dateline' => 1466356529,
),
87 => 
array (
    'tid' => 177,
    'title' => 'member_lostpw',
    'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->lost_pw}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->lost_pw_form}</strong></td>
</tr>
<tr>
<td class="trow1" width="40%"><strong>{$lang->email_address}</strong></td>
<td class="trow1" width="60%"><input type="text" class="textbox" name="email" /></td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" value="{$lang->request_user_pass}" /></div>
<input type="hidden" name="action" value="do_lostpw" />
</form>
{$footer}
</body>
</html>',
    'sid' => -2,
    'version' => '120',
    'status' => '',
    'dateline' => 1466356529,
),
88 => 
array (
    'tid' => 179,
    'title' => 'member_profile_addremove',
    'template' => '<a href="{$add_remove_options[\'url\']}" class="button small_button {$add_remove_options[\'class\']}"><span>{$add_remove_options[\'lang\']}</span></a>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
89 => 
array (
    'tid' => 180,
    'title' => 'member_profile_adminoptions',
    'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" width="100%" class="tborder">
<tr>
<td colspan="2" class="thead"><strong>{$lang->admin_options}</strong></td>
</tr>
<tr>
<td class="trow1">
<ul>
<li><a href="{$mybb->settings[\'bburl\']}/{$admin_dir}/index.php?module=user-users&amp;action=edit&amp;uid={$uid}">{$lang->admin_edit_in_acp}</a></li>
<li><a href="{$mybb->settings[\'bburl\']}/{$admin_dir}/index.php?module=user-banning&amp;uid={$uid}">{$lang->admin_ban_in_acp}</a></li>
</ul>
</td>
</tr>
</table>
<br />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
90 => 
array (
    'tid' => 181,
    'title' => 'member_profile_avatar',
    'template' => '<img src="{$useravatar[\'image\']}" alt="" {$useravatar[\'width_height\']} />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
91 => 
array (
    'tid' => 182,
    'title' => 'member_profile_away',
    'template' => '<fieldset>
<legend><strong>{$lang->away_note}</strong></legend>
<em>{$lang->away_reason} {$awayreason}</em><br />
<span class="smalltext">{$lang->away_since} {$awaydate} &mdash; {$lang->away_returns} {$returndate}</span>
</fieldset>
<br />',
    'sid' => -2,
    'version' => '1801',
    'status' => '',
    'dateline' => 1466356529,
),
92 => 
array (
    'tid' => 183,
    'title' => 'member_profile_banned',
    'template' => '<fieldset>
<legend><strong>{$lang->ban_note}</strong></legend>
<em>{$lang->banned_warning2}: {$memban[\'reason\']}</em><br />
<span class="smalltext"><strong>{$lang->ban_by}:</strong> {$memban[\'adminuser\']} &mdash; <strong>{$lang->ban_length}:</strong> {$banlength} <span class="smalltext">{$timeremaining}</span></span>
</fieldset>
<br />',
    'sid' => -2,
    'version' => '1801',
    'status' => '',
    'dateline' => 1466356529,
),
93 => 
array (
    'tid' => 184,
    'title' => 'member_profile_contact_details',
    'template' => '<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<colgroup>
<col style="width: 30%;" />
</colgroup>
<tr>
<td colspan="2" class="thead"><strong>{$lang->users_contact_details}</strong></td>
</tr>
{$website}
{$sendemail}
{$sendpm}
{$contact_fields[\'icq\']}
{$contact_fields[\'aim\']}
{$contact_fields[\'yahoo\']}
{$contact_fields[\'skype\']}
{$contact_fields[\'google\']}
</table>',
    'sid' => -2,
    'version' => '1804',
    'status' => '',
    'dateline' => 1466356529,
),
94 => 
array (
    'tid' => 185,
    'title' => 'member_profile_contact_fields_aim',
    'template' => '<tr>
<td class="{$bgcolors[\'aim\']}"><strong>{$lang->aim_screenname}</strong></td>
<td class="{$bgcolors[\'aim\']}"><a href="javascript:;" onclick="MyBB.popupWindow(\'/misc.php?action=imcenter&amp;imtype=aim&amp;uid={$uid}&amp;modal=1\'); return false;">{$memprofile[\'aim\']}</a></td>
</tr>',
    'sid' => -2,
    'version' => '1804',
    'status' => '',
    'dateline' => 1466356529,
),
95 => 
array (
    'tid' => 186,
    'title' => 'member_profile_contact_fields_google',
    'template' => '<tr>
<td class="{$bgcolors[\'google\']}"><strong>{$lang->google_id}</strong></td>
<td class="{$bgcolors[\'google\']}">{$memprofile[\'google\']}</td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
96 => 
array (
    'tid' => 187,
    'title' => 'member_profile_contact_fields_icq',
    'template' => '<tr>
<td class="{$bgcolors[\'icq\']}"><strong>{$lang->icq_number}</strong></td>
<td class="{$bgcolors[\'icq\']}">{$memprofile[\'icq\']}</td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
97 => 
array (
    'tid' => 188,
    'title' => 'member_profile_contact_fields_skype',
    'template' => '<tr>
<td class="{$bgcolors[\'skype\']}"><strong>{$lang->skype_id}</strong></td>
<td class="{$bgcolors[\'skype\']}"><a href="javascript:;" onclick="MyBB.popupWindow(\'/misc.php?action=imcenter&amp;imtype=skype&amp;uid={$uid}&amp;modal=1\'); return false;">{$memprofile[\'skype\']}</a></td>
</tr>',
    'sid' => -2,
    'version' => '1804',
    'status' => '',
    'dateline' => 1466356529,
),
98 => 
array (
    'tid' => 189,
    'title' => 'member_profile_contact_fields_yahoo',
    'template' => '<tr>
<td class="{$bgcolors[\'yahoo\']}"><strong>{$lang->yahoo_id}</strong></td>
<td class="{$bgcolors[\'yahoo\']}"><a href="javascript:;" onclick="MyBB.popupWindow(\'/misc.php?action=imcenter&amp;imtype=yahoo&amp;uid={$uid}&amp;modal=1\'); return false;">{$memprofile[\'yahoo\']}</a></td>
</tr>',
    'sid' => -2,
    'version' => '1804',
    'status' => '',
    'dateline' => 1466356529,
),
99 => 
array (
    'tid' => 190,
    'title' => 'member_profile_customfields',
    'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<colgroup>
<col style="width: 30%;" />
</colgroup>
<tr>
<td colspan="2" class="thead"><strong>{$lang->users_additional_info}</strong></td>
</tr>
{$customfields}
</table>
<br />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
100 => 
array (
    'tid' => 191,
    'title' => 'member_profile_customfields_field',
    'template' => '<tr>
<td class="{$bgcolor}"><strong>{$customfield[\'name\']}:</strong></td>
<td class="{$bgcolor} scaleimages">{$customfieldval}</td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
101 => 
array (
    'tid' => 192,
    'title' => 'member_profile_customfields_field_multi',
    'template' => '<ul style="margin: 0; padding-left: 15px;">
{$customfield_val}
</ul>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
102 => 
array (
    'tid' => 193,
    'title' => 'member_profile_customfields_field_multi_item',
    'template' => '<li style="margin-left: 0;">{$val}</li>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
103 => 
array (
    'tid' => 194,
    'title' => 'member_profile_email',
    'template' => '<tr>
<td class="{$bgcolor}"><strong>{$lang->email}</strong></td>
<td class="{$bgcolor}"><a href="member.php?action=emailuser&amp;uid={$memprofile[\'uid\']}">{$lang->send_user_email}</a></td>
</tr>',
    'sid' => -2,
    'version' => '120',
    'status' => '',
    'dateline' => 1466356529,
),
104 => 
array (
    'tid' => 195,
    'title' => 'member_profile_findposts',
'template' => '<br /><span class="smalltext">(<a href="search.php?action=finduser&amp;uid={$uid}">{$lang->find_posts}</a>)</span>',
    'sid' => -2,
    'version' => '1807',
    'status' => '',
    'dateline' => 1466356529,
),
105 => 
array (
    'tid' => 196,
    'title' => 'member_profile_findthreads',
'template' => '<br /><span class="smalltext">(<a href="search.php?action=finduserthreads&amp;uid={$uid}">{$lang->find_threads}</a>)</span>',
    'sid' => -2,
    'version' => '1807',
    'status' => '',
    'dateline' => 1466356529,
),
106 => 
array (
    'tid' => 197,
    'title' => 'member_profile_groupimage',
    'template' => '<img src="{$displaygroup[\'image\']}" alt="{$usertitle}" title="{$usertitle}" /><br />',
    'sid' => -2,
    'version' => '120',
    'status' => '',
    'dateline' => 1466356529,
),
107 => 
array (
    'tid' => 198,
    'title' => 'member_profile_modoptions',
    'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" width="100%" class="tborder">
<tr>
<td colspan="2" class="thead"><strong>{$lang->mod_options}</strong></td>
</tr>
{$manageuser}
<tr>
<td class="trow2">
{$memprofile[\'usernotes\']}<br />
{$editnotes}
</td>
</tr>
</table>
<br />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
108 => 
array (
    'tid' => 199,
    'title' => 'member_profile_modoptions_banuser',
    'template' => '<li><a href="{$mybb->settings[\'bburl\']}/modcp/banuser?uid={$uid}">{$lang->ban_in_mcp}</a></li>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
109 => 
array (
    'tid' => 200,
    'title' => 'member_profile_modoptions_editnotes',
    'template' => '<a href="{$mybb->settings[\'bburl\']}/modcp/editprofile?uid={$uid}#usernotes">{$lang->edit_usernotes}</a>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
110 => 
array (
    'tid' => 201,
    'title' => 'member_profile_modoptions_editprofile',
    'template' => '<li><a href="{$mybb->settings[\'bburl\']}/modcp/editprofile?uid={$uid}">{$lang->edit_in_mcp}</a></li>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
111 => 
array (
    'tid' => 202,
    'title' => 'member_profile_modoptions_purgespammer',
    'template' => '<li><a href="{$mybb->settings[\'bburl\']}/moderation.php?action=purgespammer&amp;uid={$uid}">{$lang->purgespammer}</a></li>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
112 => 
array (
    'tid' => 203,
    'title' => 'member_profile_modoptions_manageuser',
    'template' => '<tr>
<td class="trow1">
<ul>
{$editprofile}
{$banuser}
{$purgespammer}
</ul>
</td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
113 => 
array (
    'tid' => 204,
    'title' => 'member_profile_modoptions_viewnotes',
'template' => '[<a href="javascript:MyBB.viewNotes({$memprofile[\'uid\']});">{$lang->view_all_notes}</a>]',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
114 => 
array (
    'tid' => 207,
    'title' => 'member_profile_pm',
    'template' => '<tr>
<td class="{$bgcolor}"><strong>{$lang->pm}</strong></td>
<td class="{$bgcolor}"><a href="private.php?action=send&amp;uid={$memprofile[\'uid\']}">{$lang->send_pm}</a></td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
115 => 
array (
    'tid' => 208,
    'title' => 'member_profile_referrals',
    'template' => '<tr>
<td class="{$bg_color}"><strong>{$lang->members_referred}</strong></td>
<td class="{$bg_color}">{$memprofile[\'referrals\']}</td>
</tr>',
    'sid' => -2,
    'version' => '1600',
    'status' => '',
    'dateline' => 1466356529,
),
116 => 
array (
    'tid' => 209,
    'title' => 'member_profile_reputation',
    'template' => '<tr>
<td class="{$bg_color}"><strong>{$lang->reputation}</strong></td>
<td class="{$bg_color}">{$reputation} [<a href="reputation.php?uid={$memprofile[\'uid\']}">{$lang->reputation_details}</a>] {$vote_link}</td>
</tr>',
    'sid' => -2,
    'version' => '1600',
    'status' => '',
    'dateline' => 1466356529,
),
117 => 
array (
    'tid' => 210,
    'title' => 'member_profile_reputation_vote',
'template' => '[<a href="javascript:MyBB.reputation({$memprofile[\'uid\']});">{$lang->reputation_vote}</a>]',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
118 => 
array (
    'tid' => 211,
    'title' => 'member_profile_signature',
    'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<tr>
<td class="thead"><strong>{$lang->users_signature}</strong></td>
</tr>
<tr>
<td class="trow1 scaleimages">{$memprofile[\'signature\']}</td>
</tr>
</table>
<br />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
119 => 
array (
    'tid' => 212,
    'title' => 'member_profile_userstar',
    'template' => '<img src="{$starimage}" border="0" alt="*" />',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
120 => 
array (
    'tid' => 213,
    'title' => 'member_profile_warn',
    'template' => '[<a href="warnings.php?action=warn&amp;uid={$memprofile[\'uid\']}">{$lang->warn}</a>]',
    'sid' => -2,
    'version' => '1400',
    'status' => '',
    'dateline' => 1466356529,
),
121 => 
array (
    'tid' => 214,
    'title' => 'member_profile_warninglevel',
    'template' => '<tr>
<td class="{$bg_color}"><strong>{$lang->warning_level}</strong></td>
<td class="{$bg_color}"><a href="{$warning_link}">{$warning_level}</a> {$warn_user}</td>
</tr>',
    'sid' => -2,
    'version' => '1600',
    'status' => '',
    'dateline' => 1466356529,
),
122 => 
array (
    'tid' => 215,
    'title' => 'member_profile_website',
    'template' => '<tr>
<td class="{$bgcolor}"><strong>{$lang->homepage}</strong></td>
<td class="{$bgcolor}"><a href="{$memprofile[\'website\']}" target="_blank">{$memprofile[\'website\']}</a></td>
</tr>',
    'sid' => -2,
    'version' => '1800',
    'status' => '',
    'dateline' => 1466356529,
),
123 => 
array (
    'tid' => 216,
    'title' => 'member_register',
    'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->registration}</title>
{$headerinclude}
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/validate/jquery.validate.min.js?ver=1804"></script>
</head>
<body>
{$header}
<form action="member.php" method="post" id="registration_form"><input type="text" style="visibility: hidden;" value="" name="regcheck1" /><input type="text" style="visibility: hidden;" value="true" name="regcheck2" />
{$regerrors}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->registration}</strong></td>
</tr>
<tr>
<td width="50%" class="trow1" valign="top">
<fieldset class="trow2">
<legend><strong>{$lang->account_details}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}" width="100%">
<tr>
<td colspan="2"><span class="smalltext"><label for="username">{$lang->username}</label></span></td>
</tr>
<tr>
<td colspan="2"><input type="text" class="textbox" name="username" id="username" style="width: 100%" value="{$username}" /></td>
</tr>
{$passboxes}
<tr>
<td width="50%" valign="top"><span class="smalltext"><label for="email">{$lang->email}</label></span></td>
<td width="50%" valign="top"><span class="smalltext"><label for="email2">{$lang->confirm_email}</label></span></td>
</tr>
<tr>
<td width="50%" valign="top"><input type="text" class="textbox" name="email" id="email" style="width: 100%" maxlength="50" value="{$email}" /></td>
<td width="50%" valign="top"><input type="text" class="textbox" name="email2" id="email2" style="width: 100%" maxlength="50" value="{$email2}" /></td>
</tr>
<tr>
<td colspan="2" style="display: none;" id="email_status">&nbsp;</td>
</tr>
{$hiddencaptcha}
</table>
</fieldset>
{$requiredfields}
{$customfields}
{$referrer}
{$regimage}
{$questionbox}
</td>
<td width="50%" class="trow1" valign="top">
<fieldset class="trow2">
<legend><strong>{$lang->account_prefs}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}" width="100%">
<tr>
<td valign="top" width="1"><input type="checkbox" class="checkbox" name="allownotices" id="allownotices" value="1" {$allownoticescheck} /></td>
<td valign="top"><span class="smalltext"><label for="allownotices">{$lang->allow_notices}</label></span></td>
</tr>
<tr>
<td valign="top" width="1"><input type="checkbox" class="checkbox" name="hideemail" id="hideemail" value="1" {$hideemailcheck} /></td>
<td valign="top"><span class="smalltext"><label for="hideemail">{$lang->hide_email}</label></span></td>
</tr>
<tr>
<td valign="top" width="1"><input type="checkbox" class="checkbox" name="receivepms" id="receivepms" value="1" {$receivepmscheck} /></td>
<td valign="top"><span class="smalltext"><label for="receivepms">{$lang->receive_pms}</label></span></td>
</tr>
<tr>
<td valign="top" width="1"><input type="checkbox" class="checkbox" name="pmnotice" id="pmnotice" value="1"{$pmnoticecheck} /></td>
<td valign="top"><span class="smalltext"><label for="pmnotice">{$lang->pm_notice}</label></span></td>
</tr>
<tr>
<td valign="top" width="1"><input type="checkbox" class="checkbox" name="pmnotify" id="pmnotify" value="1" {$pmnotifycheck} /></td>
<td valign="top"><span class="smalltext"><label for="pmnotify">{$lang->email_notify_newpm}</label></span></td>
</tr>
<tr>
<td valign="top" width="1"><input type="checkbox" class="checkbox" name="invisible" id="invisible" value="1" {$invisiblecheck} /></td>
<td valign="top"><span class="smalltext"><label for="invisible">{$lang->invisible_mode}</label></span></td>
</tr>
<tr>
<td colspan="2"><span class="smalltext"><label for="subscriptionmethod">{$lang->subscription_method}</label></span></td>
</tr>
<tr>
<td colspan="2">
<select name="subscriptionmethod" id="subscriptionmethod">
<option value="0" {$no_auto_subscribe_selected}>{$lang->no_auto_subscribe}</option>
<option value="1" {$no_subscribe_selected}>{$lang->no_subscribe}</option>
<option value="2" {$instant_email_subscribe_selected}>{$lang->instant_email_subscribe}</option>
<option value="3" {$instant_pm_subscribe_selected}>{$lang->instant_pm_subscribe}</option>
</select>
</td>
</tr>
</table>
</fieldset>
<br />
<fieldset class="trow2">
<legend><strong><label for="timezone">{$lang->time_offset}</label></strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}" width="100%">
<tr>
<td><span class="smalltext">{$lang->time_offset_desc}</span></td>
</tr>
<tr>
<td>{$tzselect}</td>
</tr>
<tr>
<td><span class="smalltext">{$lang->dst_correction}</span></td>
</tr>
<tr>
<td>
<select name="dstcorrection">
<option value="2" {$dst_auto_selected}>{$lang->dst_correction_auto}</option>
<option value="1" {$dst_enabled_selected}>{$lang->dst_correction_enabled}</option>
<option value="0" {$dst_disabled_selected}>{$lang->dst_correction_disabled}</option>
</select>
</td>
</tr>
</table>
</fieldset>
{$boardlanguage}
</td>
</tr>
</table>
<br />
<div align="center">
<input type="hidden" name="regtime" value="{$time}" />
<input type="hidden" name="step" value="registration" />
<input type="hidden" name="action" value="do_register" />
<input type="submit" class="button" name="regsubmit" value="{$lang->submit_registration}" />
</div>
</form>
<script type="text/javascript">
$(document).ready(function() {
$("#registration_form").validate({
rules: {
username: {
required: true,
minlength: {$mybb->settings[\'minnamelength\']},
maxlength: {$mybb->settings[\'maxnamelength\']},
remote: {
url: "xmlhttp.php?action=username_availability",
type: "post",
dataType: "json",
data:
{
my_post_key: my_post_key
},
},
},
email: {
required: true,
email: true,
},
email2: {
required: true,
email: true,
equalTo: "#email"
},
},
messages: {
username: {
minlength: "{$lang->js_validator_username_length}",
maxlength: "{$lang->js_validator_username_length}",
},
email: "{$lang->js_validator_invalid_email}",
email2: "{$lang->js_validator_email_match}",
},
errorPlacement: function(error, element) {
if(element.is(\':checkbox\') || element.is(\':radio\'))
error.insertAfter($(\'input[name="\' + element.attr(\'name\') + \'"]\').last().next(\'span\'));
else
error.insertAfter(element);
}
});

{$validator_extra}
});
</script>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
124 => 
array (
'tid' => 217,
'title' => 'member_register_additionalfields',
'template' => '<br />
<fieldset class="trow2">
<legend><strong>{$lang->additional_info}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
{$customfields}
</table>
</fieldset>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
125 => 
array (
'tid' => 218,
'title' => 'member_register_agreement',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->agreement}</title>
{$headerinclude}
</head>
<body>
{$header}
<br />
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$mybb->settings[\'bbname\']} - {$lang->agreement}</strong></td>
</tr>
{$coppa_agreement}
<tr>
<td class="trow1">
<p>{$lang->agreement_1}</p>
<p>{$lang->agreement_2}</p>
<p>{$lang->agreement_3}</p>
<p>{$lang->agreement_4}</p>
<p><strong>{$lang->agreement_5}</strong></p>
</td>
</tr>
</table>

<br />
<div align="center">
<input type="hidden" name="step" value="agreement" />
<input type="hidden" name="action" value="register" />
<input type="submit" class="button" name="agree" value="{$lang->i_agree}" />
</div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '123',
'status' => '',
'dateline' => 1466356529,
),
126 => 
array (
'tid' => 219,
'title' => 'member_register_agreement_coppa',
'template' => '<tr>
<td class="tcat"><strong>{$lang->coppa_compliance}</strong></td>
</tr>
<tr>
<td class="trow1">
<p>{$lang->coppa_agreement_1}</p>
<p>{$lang->coppa_agreement_2}</p>
<p>{$lang->coppa_agreement_3}</p>
</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
127 => 
array (
'tid' => 220,
'title' => 'member_register_coppa',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->coppa_compliance}</title>
{$headerinclude}
</head>
<body>
{$header}
<br />
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$mybb->settings[\'bbname\']} - {$lang->coppa_compliance}</strong></td>
</tr>
<tr>
<td class="trow1">
{$lang->coppa_desc}
<div style="width: 400px; margin: auto; margin-bottom: 15px;">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td colspan="1" class="thead"><strong>{$lang->date_of_birth}</strong></td>
</tr>
<tr>
<td class="trow1">
<table cellpadding="4" border="0" align="center">
<tr>
<td class="smalltext"><label for="day">{$lang->day}:</label></td>
<td class="smalltext"><label for="month">{$lang->month}:</label></td>
<td class="smalltext"><label for="year">{$lang->year}:</label></td>
</tr>
<tr>
<td>
<select name="bday1" id="day">
<option value="">&nbsp;</option>
{$bdaysel}
</select>
</td>
<td>
<select name="bday2" id="month">
<option value="">&nbsp;</option>
<option value="1" {$bdaymonthsel[\'1\']}>{$lang->month_1}</option>
<option value="2" {$bdaymonthsel[\'2\']}>{$lang->month_2}</option>
<option value="3" {$bdaymonthsel[\'3\']}>{$lang->month_3}</option>
<option value="4" {$bdaymonthsel[\'4\']}>{$lang->month_4}</option>
<option value="5" {$bdaymonthsel[\'5\']}>{$lang->month_5}</option>
<option value="6" {$bdaymonthsel[\'6\']}>{$lang->month_6}</option>
<option value="7" {$bdaymonthsel[\'7\']}>{$lang->month_7}</option>
<option value="8" {$bdaymonthsel[\'8\']}>{$lang->month_8}</option>
<option value="9" {$bdaymonthsel[\'9\']}>{$lang->month_9}</option>
<option value="10" {$bdaymonthsel[\'10\']}>{$lang->month_10}</option>
<option value="11" {$bdaymonthsel[\'11\']}>{$lang->month_11}</option>
<option value="12" {$bdaymonthsel[\'12\']}>{$lang->month_12}</option>
</select>
</td>
<td>
<input type="text" class="textbox" size="4" maxlength="4" name="bday3" id="year" value="{$mybb->input[\'bday3\']}" />
</td>
</tr>
</table>
<div class="smalltext" style="text-align: center;">{$lang->hide_dob}</div>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<br />
<div align="center">
<input type="hidden" name="action" value="register" />
<input type="submit" class="button" name="agree" value="{$lang->continue_registration}" />
</div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
128 => 
array (
'tid' => 221,
'title' => 'member_register_customfield',
'template' => '<tr>
<td>
{$profilefield[\'name\']}
<br />
<span class="smalltext">{$profilefield[\'description\']}</span>
</td>
</tr>
<tr>
<td>{$code}</td>
</tr>',
'sid' => -2,
'version' => '1605',
'status' => '',
'dateline' => 1466356529,
),
129 => 
array (
'tid' => 222,
'title' => 'member_register_day',
'template' => '<option value="{$day}"{$selected}>{$day}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
130 => 
array (
'tid' => 223,
'title' => 'member_register_hiddencaptcha',
'template' => '<tr style="display: none;">
<td colspan="2" id="{$captcha_field}_status">{$lang->leave_this_field_empty} <input type="text" class="textbox" name="{$captcha_field}" id="{$captcha_field}" style="width: 100%" maxlength="50" value="" /></td>
</tr>',
'sid' => -2,
'version' => '1605',
'status' => '',
'dateline' => 1466356529,
),
131 => 
array (
'tid' => 224,
'title' => 'member_register_language',
'template' => '<br />
<fieldset class="trow2">
<legend><strong><label for="language">{$lang->lang_select}</label></strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}" width="100%">
<tr>
<td colspan="2"><span class="smalltext">{$lang->lang_select_desc}</span></td>
</tr>
<tr>
<td><select name="language" id="language">
<option value="">{$lang->lang_select_default}</option>
<option value="">-----------</option>
{$langoptions}
</select></td>
</tr>
</table>
</fieldset>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
132 => 
array (
'tid' => 225,
'title' => 'member_register_password',
'template' => '<tr>
<td width="50%" valign="top"><span class="smalltext">{$lang->password}</span></td>
<td width="50%" valign="top"><span class="smalltext">{$lang->confirm_password}</span></td>
</tr>
<tr>
<td width="50%" valign="top"><input type="password" class="textbox" name="password" id="password" style="width: 100%" /></td>
<td width="50%" valign="top"><input type="password" class="textbox" name="password2" id="password2" style="width: 100%" /></td>
</tr>
<tr>
<td colspan="2" style="display: none;" id="password_status">&nbsp;</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
133 => 
array (
'tid' => 226,
'title' => 'member_register_question',
'template' => '<br />
<fieldset class="trow2">
<script type="text/javascript">
<!--
lang.question_fetch_failure = "{$lang->question_fetch_failure}";
// -->
</script>
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/question.js?ver=1804"></script>
<legend><strong>{$lang->security_question}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td colspan="2"><span class="smalltext">{$lang->question_note}</span></td>
</tr>
<tr>
<td colspan="2"><br /><span class="smalltext" id="question" style="font-weight:bold;">{$question[\'question\']}</span></td>
</tr>
<tr>
<td width="60%">
<input type="text" class="textbox" name="answer" value="" id="answer" style="width: 70%;" />
<input type="hidden" name="question_id" value="{$sid}" id="question_id" />
</td>
<td width="40%">
{$refresh}
</td>
</tr>
<tr>
<td id="answer_status"  style="display: none;" colspan="2">&nbsp;</td>
</tr>
</table>
</fieldset>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
134 => 
array (
'tid' => 227,
'title' => 'member_register_question_refresh',
'template' => '
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
document.write(\'<input type="button" class="button" tabindex="10000" name="refresh" value="{$lang->refresh}" onclick="return question.refresh();" \\/>\');
}
// -->
</script>
',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
135 => 
array (
'tid' => 228,
'title' => 'member_register_referrer',
'template' => '<br />
<fieldset class="trow2">
<legend><strong>{$lang->referrer}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td><span class="smalltext"><label for="referrer">{$lang->referrer_desc}</label></span></td>
</tr>
<tr>
<td>
<input type="text" class="textbox" name="referrername" id="referrer" value="{$referrername}" style="width: 100%;" />
</td>
</tr></table>
</fieldset>
<link rel="stylesheet" href="{$mybb->asset_url}/jscripts/select2/select2.css?ver=1807">
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/select2/select2.min.js?ver=1806"></script>
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
MyBB.select2();
$("#referrer").select2({
placeholder: "{$lang->search_user}",
minimumInputLength: 2,
multiple: false,
ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
url: "xmlhttp.php?action=get_users",
dataType: \'json\',
data: function (term, page) {
return {
query: term, // search term
};
},
results: function (data, page) { // parse the results into the format expected by Select2.
// since we are using custom formatting functions we do not need to alter remote JSON data
return {results: data};
}
},
initSelection: function(element, callback) {
var value = $(element).val();
if (value !== "") {
callback({
id: value,
text: value
});
}
},
});
}
// -->
</script>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
136 => 
array (
'tid' => 229,
'title' => 'member_register_regimage',
'template' => '<br />
<fieldset class="trow2">
<script type="text/javascript">
<!--
lang.captcha_fetch_failure = "{$lang->captcha_fetch_failure}";
// -->
</script>
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/captcha.js?ver=1804"></script>
<legend><strong>{$lang->image_verification}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td><span class="smalltext">{$lang->verification_note}</span></td>
<td rowspan="2" align="center"><img src="captcha.php?action=regimage&amp;imagehash={$imagehash}" alt="{$lang->image_verification}" title="{$lang->image_verification}" id="captcha_img" /><br /><span style="color: red;" class="smalltext">{$lang->verification_subnote}</span>
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
document.write(\'<br \\/><br \\/><input type="button" class="button" tabindex="10000" name="refresh" value="{$lang->refresh}" onclick="return captcha.refresh();" \\/>\');
}
// -->
</script>
</td>
</tr>
<tr>
<td><input type="text" class="textbox" name="imagestring" value="" id="imagestring" style="width: 100%;" /><input type="hidden" name="imagehash" value="{$imagehash}" id="imagehash" /></td>
</tr>
<tr>
<td id="imagestring_status"  style="display: none;" colspan="2">&nbsp;</td>
</tr>
</table>
</fieldset>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
137 => 
array (
'tid' => 230,
'title' => 'member_register_regimage_recaptcha',
'template' => '<br />
<script type="text/javascript">
<!--
var RecaptchaOptions = {
theme: \'clean\'
};
// -->
</script>
<fieldset class="trow2">
<legend><strong>{$lang->image_verification}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td><span class="smalltext">{$lang->verification_note}</span></td>
</tr>
<tr>
<td><script type="text/javascript" src="{$server}/challenge?k={$public_key}"></script></td>
</tr>
</table>
</fieldset>',
'sid' => -2,
'version' => '1605',
'status' => '',
'dateline' => 1466356529,
),
138 => 
array (
'tid' => 231,
'title' => 'member_register_regimage_nocaptcha',
'template' => '<br />
<fieldset class="trow2">
<legend><strong>{$lang->human_verification}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td><span class="smalltext">{$lang->verification_note_nocaptcha}</span></td>
</tr>
<tr>
<td><script type="text/javascript" src="{$server}"></script><div class="g-recaptcha" data-sitekey="{$public_key}"></div></td>
</tr>
</table>
</fieldset>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
139 => 
array (
'tid' => 232,
'title' => 'member_register_requiredfields',
'template' => '<br />
<fieldset class="trow2">
<legend><strong>{$lang->required_info}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
{$requiredfields}
</table>
</fieldset>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
140 => 
array (
'tid' => 233,
'title' => 'member_register_stylebit',
'template' => '<option value="{$style[\'sid\']}">{$style[\'name\']}</option>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
141 => 
array (
'tid' => 234,
'title' => 'member_resendactivation',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->resend_activation}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->resend_activation}</strong></td>
</tr>
<tr>
<td class="trow1" width="40%"><strong>{$lang->email_address}</strong></td>
<td class="trow1" width="60%"><input type="text" class="textbox" name="email" /></td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" value="{$lang->request_activation}" /></div>
<input type="hidden" name="action" value="do_resendactivation" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
142 => 
array (
'tid' => 235,
'title' => 'member_resetpassword',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->reset_password}</title>
{$headerinclude}
</head>
<body>
{$header}
<br />
<form action="member.php" method="post">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->reset_password}</strong></td>
</tr>
<tr>
<td class="trow1" width="30%"><strong>{$lang_username}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="username" value="{$user[\'username\']}" /></td>
</tr>
<tr>
<td class="trow1" width="30%"><strong>{$lang->activation_code}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="code" value="{$code}" /></td>
</tr>
</table>
<br />
<div align="center"><input type="hidden" name="action" value="resetpassword" /><input type="submit" class="button" name="regsubmit" value="{$lang->send_password}" /></div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1607',
'status' => '',
'dateline' => 1466356529,
),
143 => 
array (
'tid' => 237,
'title' => 'memberlist',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->member_list}</title>
{$headerinclude}
</head>
<body>
{$header}
{$multipage}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="7">
<div class="float_right">
<a href="memberlist.php?username_match=begins&amp;username={$lang->a}" style="padding: 5px;">{$lang->a}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->b}" style="padding: 5px;">{$lang->b}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->c}" style="padding: 5px;">{$lang->c}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->d}" style="padding: 5px;">{$lang->d}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->e}" style="padding: 5px;">{$lang->e}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->f}" style="padding: 5px;">{$lang->f}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->g}" style="padding: 5px;">{$lang->g}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->h}" style="padding: 5px;">{$lang->h}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->i}" style="padding: 5px;">{$lang->i}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->j}" style="padding: 5px;">{$lang->j}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->k}" style="padding: 5px;">{$lang->k}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->l}" style="padding: 5px;">{$lang->l}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->m}" style="padding: 5px;">{$lang->m}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->n}" style="padding: 5px;">{$lang->n}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->o}" style="padding: 5px;">{$lang->o}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->p}" style="padding: 5px;">{$lang->p}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->q}" style="padding: 5px;">{$lang->q}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->r}" style="padding: 5px;">{$lang->r}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->s}" style="padding: 5px;">{$lang->s}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->t}" style="padding: 5px;">{$lang->t}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->u}" style="padding: 5px;">{$lang->u}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->v}" style="padding: 5px;">{$lang->v}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->w}" style="padding: 5px;">{$lang->w}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->x}" style="padding: 5px;">{$lang->x}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->y}" style="padding: 5px;">{$lang->y}</a>
<a href="memberlist.php?username_match=begins&amp;username={$lang->z}" style="padding: 5px;">{$lang->z}</a>
</div>
<div><strong>{$lang->member_list}</strong></div></td>
</tr>
<tr>
<td class="tcat" width="1%"><span class="smalltext"><strong>{$lang->avatar}</strong></span></td>
<td class="tcat"><span class="smalltext"><a href="{$sorturl}&amp;sort=username&amp;order=ascending"><strong>{$lang->username}</strong></a> {$orderarrow[\'username\']}</span></td>
<td class="tcat" width="15%" align="center"><span class="smalltext"><a href="{$sorturl}&amp;sort=regdate&amp;order=ascending"><strong>{$lang->joined}</strong></a> {$orderarrow[\'regdate\']}</span></td>
<td class="tcat" width="15%" align="center"><span class="smalltext"><a href="{$sorturl}&amp;sort=lastvisit&amp;order=descending"><strong>{$lang->lastvisit}</strong></a> {$orderarrow[\'lastvisit\']}</span></td>
<td class="tcat" width="10%" align="center"><span class="smalltext"><a href="{$sorturl}&amp;sort=postnum&amp;order=descending"><strong>{$lang->posts}</strong></a> {$orderarrow[\'postnum\']}</span></td>
<td class="tcat" width="10%" align="center"><span class="smalltext"><a href="{$sorturl}&amp;sort=threadnum&amp;order=descending"><strong>{$lang->threads}</strong></a> {$orderarrow[\'threadnum\']}</span></td>
{$referral_header}
</tr>
{$users}
</table>
<div class="float_right" style="padding-top: 4px;">
<a href="showteam.php"><strong>{$lang->forumteam}</strong></a>
</div>
{$multipage}
<br class="clear" />
<br />
<form method="post" action="memberlist.php">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="3">
<div class="float_right">
<strong><a href="memberlist.php?action=search">{$lang->advanced_search}</a></strong>
</div>
<div><strong>{$lang->search_members}</strong></div>
</td>
</tr>
<tr>
<td class="tcat"><strong><label for="username">{$lang->username}</label></strong></td>
<td class="tcat"><strong><label for="website">{$lang->website}</label></strong></td>
<td class="tcat"><strong><label for="sort">{$lang->sort_by}</label></strong></td>
</tr>
<tr>
<td class="trow1" width="33%" style="vertical-align: top;">
{$lang->contains}<br />
<input type="text" class="textbox" name="username" id="username" style="width: 99%; margin-top: 4px;" value="{$search_username}" />
</td>
<td class="trow1" width="33%" style="vertical-align: top;">
{$lang->contains}<br />
<input type="text" class="textbox" name="website" id="website" style="width: 99%; margin-top: 4px;" value="{$search_website}" />
</td>
<td class="trow1" width="33%">
<div class="smalltext" style="margin-bottom: 3px;">
<input type="radio" class="radio" name="order" id="order_asc" value="ascending"{$order_check[\'ascending\']} /> <label for="order_asc">{$lang->order_asc}</label>&nbsp;
<input type="radio" class="radio" name="order" id="order_desc" value="descending"{$order_check[\'descending\']} /> <label for="order_desc">{$lang->order_desc}</label>
</div>
<select name="sort" id="sort" style="width: 99%;">
<option value="username"{$sort_selected[\'username\']}>{$lang->sort_by_username}</option>
<option value="regdate"{$sort_selected[\'regdate\']}>{$lang->sort_by_regdate}</option>
<option value="lastvisit"{$sort_selected[\'lastvisit\']}>{$lang->sort_by_lastvisit}</option>
<option value="postnum"{$sort_selected[\'postnum\']}>{$lang->sort_by_posts}</option>
<option value="threadnum"{$sort_selected[\'threadnum\']}>{$lang->sort_by_threads}</option>
<option value="referrals"{$sort_selected[\'referrals\']}>{$lang->sort_by_referrals}</option>
</select>
</td>
</tr>
</table>
<div align="center"><br /><input type="submit" class="button" name="submit" value="{$lang->search}" /></div>
</form>
{$footer}
<link rel="stylesheet" href="{$mybb->asset_url}/jscripts/select2/select2.css?ver=1807">
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/select2/select2.min.js?ver=1806"></script>
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
MyBB.select2();
$("#username").select2({
placeholder: "{$lang->search_user}",
minimumInputLength: 2,
multiple: false,
ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
url: "xmlhttp.php?action=get_users",
dataType: \'json\',
data: function (term, page) {
return {
query: term, // search term
};
},
results: function (data, page) { // parse the results into the format expected by Select2.
// since we are using custom formatting functions we do not need to alter remote JSON data
return {results: data};
}
},
initSelection: function(element, callback) {
var value = $(element).val();
if (value !== "") {
callback({
id: value,
text: value
});
}
},
// Allow the user entered text to be selected as well
createSearchChoice:function(term, data) {
if ( $(data).filter( function() {
return this.text.localeCompare(term)===0;
}).length===0) {
return {id:term, text:term};
}
},
});

$(\'[for=username]\').click(function(){
$("#username").select2(\'open\');
return false;
});
}
// -->
</script>
</body>
</html>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
144 => 
array (
'tid' => 238,
'title' => 'memberlist_referrals',
'template' => '<td class="tcat" width="10%" align="center"><span class="smalltext"><a href="{$sorturl}&amp;sort=referrals&amp;order=descending"><strong>{$lang->referrals}</strong></a> {$orderarrow[\'referrals\']}</span></td>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
145 => 
array (
'tid' => 239,
'title' => 'memberlist_referrals_bit',
'template' => '<td class="{$alt_bg}" align="center">{$user[\'referrals\']}</td>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
146 => 
array (
'tid' => 240,
'title' => 'memberlist_search',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->search_member_list}</title>
{$headerinclude}
</head>
<body>
{$header}
<form method="post" action="memberlist.php">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->search_member_list}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2"><strong>{$lang->search_criteria}</strong></td>
</tr>
<tr>
<td class="trow1" style="vertical-align: top;" width="20%"><strong><label for="username">{$lang->username}</label></strong></td>
<td class="trow1">
<select name="username_match">
<option value="begins">{$lang->begins_with}</option>
<option value="contains">{$lang->username_contains}</option>
</select>
&nbsp;
<input type="text" class="textbox" name="username" id="username" />
</td>
</tr>
<tr>
<td class="trow2" width="20%"><strong><label for="website">{$lang->search_website}</label></strong></td>
<td class="trow2">
<input type="text" class="textbox" name="website" id="website" />
</td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="aim">{$lang->search_aim}</label></strong></td>
<td class="trow1">
<input type="text" class="textbox" name="aim" id="aim" />
</td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="skype">{$lang->search_skype}</label></strong></td>
<td class="trow1">
<input type="text" class="textbox" name="skype" id="skype" />
</td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="google">{$lang->search_google}</label></strong></td>
<td class="trow1">
<input type="text" class="textbox" name="google" id="google" />
</td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="yahoo">{$lang->search_yahoo}</label></strong></td>
<td class="trow1">
<input type="text" class="textbox" name="yahoo" id="yahoo" />
</td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="icq">{$lang->search_icq}</label></strong></td>
<td class="trow1">
<input type="text" class="textbox" name="icq" id="icq" />
</td>
</tr>
<tr>
<td class="tcat" colspan="2"><strong>{$lang->search_options}</strong></td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="sort">{$lang->sort_by}</label></strong></td>
<td class="trow1">
<select name="sort" id="sort">
<option value="username">{$lang->sort_by_username}</option>
<option value="regdate">{$lang->sort_by_regdate}</option>
<option value="lastvisit">{$lang->sort_by_lastvisit}</option>
<option value="postnum">{$lang->sort_by_posts}</option>
<option value="threadnum">{$lang->sort_by_threads}</option>
<option value="referrals">{$lang->sort_by_referrals}</option>
</select><br />
<span class="smalltext">
<input type="radio" class="radio" name="order" id="order_asc" value="asc" /> <label for="order_asc">{$lang->order_asc}</label><br />
<input type="radio" class="radio" name="order" id="order_desc" value="desc" checked="checked" /> <label for="order_desc">{$lang->order_desc}</label>
</span>
</td>
</tr>
<tr>
<td class="trow1" width="20%"><strong><label for="perpage">{$lang->per_page}</label></strong></td>
<td class="trow1">
<input type="text" class="textbox" size="4" name="perpage" id="perpage" value="15" />
</td>
</tr>
</table>
<div align="center"><br /><input type="submit" class="button" name="submit" value="{$lang->search}" /></div>
</form>
{$footer}
<link rel="stylesheet" href="{$mybb->asset_url}/jscripts/select2/select2.css?ver=1807">
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/select2/select2.min.js?ver=1806"></script>
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
MyBB.select2();
$("#username").select2({
placeholder: "{$lang->search_user}",
minimumInputLength: 2,
multiple: false,
ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
url: "xmlhttp.php?action=get_users",
dataType: \'json\',
data: function (term, page) {
return {
query: term, // search term
};
},
results: function (data, page) { // parse the results into the format expected by Select2.
// since we are using custom formatting functions we do not need to alter remote JSON data
return {results: data};
}
},
initSelection: function(element, callback) {
var value = $(element).val();
if (value !== "") {
callback({
id: value,
text: value
});
}
},
// Allow the user entered text to be selected as well
createSearchChoice:function(term, data) {
if ( $(data).filter( function() {
return this.text.localeCompare(term)===0;
}).length===0) {
return {id:term, text:term};
}
},
});

$(\'[for=username]\').click(function(){
$("#username").select2(\'open\');
return false;
});
}
// -->
</script>
</body>
</html>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
147 => 
array (
'tid' => 241,
'title' => 'memberlist_user',
'template' => '<tr>
<td class="{$alt_bg}" align="center">{$user[\'avatar\']}</td>
<td class="{$alt_bg}">{$user[\'profilelink\']}<br />
<span class="smalltext">
{$user[\'usertitle\']}<br />
{$usergroup[\'groupimage\']}
{$user[\'userstars\']}
</span></td>
<td class="{$alt_bg}" align="center">{$user[\'regdate\']}</td>
<td class="{$alt_bg}" align="center">{$user[\'lastvisit\']}</td>
<td class="{$alt_bg}" align="center">{$user[\'postnum\']}</td>
<td class="{$alt_bg}" align="center">{$user[\'threadnum\']}</td>
{$referral_bit}
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
148 => 
array (
'tid' => 242,
'title' => 'memberlist_user_userstar',
'template' => '<img src="{$starimage}" border="0" alt="*" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
149 => 
array (
'tid' => 243,
'title' => 'memberlist_error',
'template' => '<tr>
<td colspan="{$colspan}" align="center" class="trow1">{$lang->error_no_members}</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
150 => 
array (
'tid' => 244,
'title' => 'memberlist_user_avatar',
'template' => '<img src="{$useravatar[\'image\']}" alt="" {$useravatar[\'width_height\']} />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
151 => 
array (
'tid' => 245,
'title' => 'memberlist_user_groupimage',
'template' => '<img src="{$usergroup[\'image\']}" alt="{$usergroup[\'title\']}" title="{$usergroup[\'title\']}" />',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
152 => 
array (
'tid' => 246,
'title' => 'memberlist_orderarrow',
'template' => '<span class="smalltext">[<a href="{$sorturl}&amp;sort={$sort}&amp;order={$oppsortnext}">{$oppsort}</a>]</span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
153 => 
array (
'tid' => 247,
'title' => 'misc_buddypopup',
'template' => '<div class="modal">
<div style="overflow-y: auto; max-height: 400px;">
<table cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"{$colspan}>
<div><strong>{$lang->buddy_list}</strong></div>
</td>
</tr>
{$buddies}
</table>
</div>
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
154 => 
array (
'tid' => 248,
'title' => 'misc_buddypopup_user',
'template' => '	<tr>
<td class="tcat"{$colspan}><strong>{$lang->online}</strong></td>
{$buddys[\'online\']}
</tr>
<tr>
<td class="tcat"{$colspan}><strong>{$lang->offline}</strong></td>
{$buddys[\'offline\']}
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
155 => 
array (
'tid' => 249,
'title' => 'misc_buddypopup_user_none',
'template' => '					<tr>
<td class="trow1"{$colspan}>{$error}</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
156 => 
array (
'tid' => 250,
'title' => 'misc_buddypopup_user_offline',
'template' => '					<tr>
<td class="{$boffline_alt}" width="1%">
<div class="buddy_avatar float_left"><img src="{$buddy[\'avatar\'][\'image\']}" alt="" {$buddy[\'avatar\'][\'width_height\']} style="margin-top: 3px;" /></div>
</td>
<td class="{$boffline_alt}">
{$profile_link}
<div class="buddy_action">
<span class="smalltext">{$last_active}<br />{$send_pm}</span>
</div>
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
157 => 
array (
'tid' => 251,
'title' => 'misc_buddypopup_user_online',
'template' => '					<tr>
<td class="{$bonline_alt}" width="1%">
<div class="buddy_avatar float_left"><img src="{$buddy[\'avatar\'][\'image\']}" alt="" {$buddy[\'avatar\'][\'width_height\']} style="margin-top: 3px;" /></div>
</td>
<td class="{$bonline_alt}">
{$profile_link}
<div class="buddy_action">
<span class="smalltext">{$last_active}<br />{$send_pm}</span>
</div>
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
158 => 
array (
'tid' => 252,
'title' => 'misc_buddypopup_user_sendpm',
'template' => '<a href="private.php?action=send&amp;uid={$buddy[\'uid\']}" target="_blank" onclick="window.opener.location.href=this.href; return false;">{$lang->pm_buddy}</a>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
159 => 
array (
'tid' => 253,
'title' => 'misc_help',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->help_docs}</title>
{$headerinclude}
</head>
<body>
{$header}
{$sections}
{$search}
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
160 => 
array (
'tid' => 254,
'title' => 'misc_help_helpdoc',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->help_docs}</title>
{$headerinclude}
</head>
<body>
{$header}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<tr>
<td class="thead"><strong>{$helpdoc[\'name\']}</strong></td>
</tr>
<tr>
<td class="trow1 scaleimages">{$helpdoc[\'document\']}</td>
</tr>
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
161 => 
array (
'tid' => 255,
'title' => 'misc_help_search',
'template' => '<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="3"><strong>{$lang->search}</strong></td>
</tr>
<tr>
<td class="trow1">
<form action="misc.php" method="post">
<div class="float_left" style="padding-top: 5px;">
<input type="text" name="keywords" value="{$lang->enter_keywords}" onfocus="if(this.value == \'{$lang->enter_keywords}\') { this.value = \'\'; }" onblur="if(this.value==\'\') { this.value=\'{$lang->enter_keywords}\'; }" class="textbox" size="25" />
</div>
<div class="float_left" style="padding-left: 20px;">
<span class="smalltext">
<label><input name="name" checked="checked" value="1" type="checkbox"> {$lang->search_by_name}</label><br />
<label><input name="document" checked="checked" value="1" type="checkbox"> {$lang->search_by_document}</label>
</span>
</div>
<div class="float_left" style="padding-left: 20px; padding-top: 5px;">
<input type="hidden" name="action" value="do_helpsearch" />
<input type="submit" class="button" name="submit" value="{$lang->search_help_documents}" />
</div>
</form>
</td>
</tr>
</table>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
162 => 
array (
'tid' => 256,
'title' => 'misc_help_section',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder clear">
<thead>
<tr>
<td class="thead{$expthead}"><div class="expcolimage"><img src="{$theme[\'imgdir\']}/{$expcolimage}" id="sid_{$section[\'sid\']}_img" class="expander" alt="[-]" title="[-]" /></div><div><strong>{$section[\'name\']}</strong></div></td>
</tr>
</thead>
<tbody style="{$expdisplay}" id="sid_{$section[\'sid\']}_e">
<tr>
<td class="tcat"><span class="smalltext"><strong>{$section[\'description\']}</strong></span></td>
</tr>
{$helpbits}
</tbody>
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
163 => 
array (
'tid' => 257,
'title' => 'misc_help_section_bit',
'template' => '<tr>
<td width="100%" class="{$altbg}"><strong><a href="misc.php?action=help&amp;hid={$helpdoc[\'hid\']}">{$helpdoc[\'name\']}</a></strong><br /><span class="smalltext">{$helpdoc[\'description\']}</span></td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
164 => 
array (
'tid' => 265,
'title' => 'misc_rules_forum',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$forum[\'rulestitle\']}</title>
{$headerinclude}
</head>
<body>
{$header}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<tr>
<td class="thead"><strong>{$forum[\'rulestitle\']}</strong></td>
</tr>
<tr>
<td class="trow1 scaleimages">{$forum[\'rules\']}</td>
</tr>
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
165 => 
array (
'tid' => 271,
'title' => 'misc_syndication',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->syndication}</title>
{$headerinclude}
</head>
<body>
{$header}
{$feedurl}
<table border="0" cellspacing="0" cellpadding="{$theme[\'tablespace\']}" width="100%">
<tr><td>{$lang->syndication_note}</td></tr>
</table>
<br />
<form method="post" action="misc.php?action=syndication">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->syndication}</strong></td>
</tr>
<tr>
<td class="trow1" width="40%" valign="top"><strong>{$lang->syndication_forum}</strong><br /><span class="smalltext">{$lang->syndication_forum_desc}</span></td>
<td class="trow1" width="60%">{$forumselect}</td>
</tr>
<tr>
<td class="trow2" width="40%" valign="top"><strong>{$lang->syndication_version}</strong><br /><span class="smalltext">{$lang->syndication_version_desc}</span></td>
<td class="trow2" width="60%"><input type="radio" class="radio" name="version" value="rss2.0" {$rss2check} />&nbsp;{$lang->syndication_version_rss2}<br /><input type="radio" class="radio" name="version" value="atom1.0" {$atom1check} />&nbsp;{$lang->syndication_version_atom1}</td>
</tr>
<tr>
<td class="trow1" width="40%" valign="top"><strong>{$lang->syndication_limit}</strong><br /><span class="smalltext">{$lang->syndication_limit_desc}</span></td>
<td class="trow1" width="60%"><input type="text" class="textbox" name="limit" value="{$limit}" size="3" /> {$lang->syndication_threads_time}</td>
</tr></table>
<br />
<div align="center"><input type="submit" class="button" name="make" value="{$lang->syndication_generate}" /></div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
166 => 
array (
'tid' => 272,
'title' => 'misc_syndication_feedurl',
'template' => '<table border="0" cellspacing="0" cellpadding="{$theme[\'tablespace\']}" width="100%">
<tr><td>
<strong>{$lang->syndication_generated_url}</strong><blockquote>
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder"><tr><td class="trow1">
{$url}
</td></tr></table>
</blockquote>
</td></tr>
</table>
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
167 => 
array (
'tid' => 273,
'title' => 'misc_syndication_forumlist',
'template' => '<select name="forums[]" size="10" multiple="multiple">
<option value="all" {$addsel}>{$lang->syndicate_all_forums}</option>
<option value="all">----------------------</option>
{$forumlistbits}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
168 => 
array (
'tid' => 274,
'title' => 'misc_whoposted',
'template' => '<div class="modal">
<div style="overflow-y: auto; max-height: 400px;">
<table width="100%" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" border="0" align="center" class="tborder">
<tr>
<td colspan="2" class="thead"><strong>{$lang->total_posts} {$numposts}</strong></td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong><a href="misc.php?action=whoposted&amp;tid={$tid}&amp;sort=username">{$lang->user}</a></strong></span></td>
<td class="tcat"><span class="smalltext"><strong><a href="misc.php?action=whoposted&amp;tid={$tid}&amp;sort=numposts">{$lang->num_posts}</a></strong></span></td>
</tr>
{$whoposted}
</table>
</div>
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
169 => 
array (
'tid' => 275,
'title' => 'misc_whoposted_poster',
'template' => '<tr>
<td class="{$altbg}">{$profile_link}</td>
<td class="{$altbg}">{$poster[\'posts\']}</td>
</tr>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
170 => 
array (
'tid' => 276,
'title' => 'modcp',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->modcp}</title>
{$headerinclude}
</head>
<body>
{$header}
<table width="100%" border="0" align="center">
<tr>
{$modcp_nav}
<td valign="top">
{$awaitingmoderation}
{$latestfivemodactions}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="4"><strong>{$lang->bans_ending_soon}</strong></td>
</tr>
<tr>
<td class="tcat" width="25%"><span class="smalltext"><strong>{$lang->username}</strong></span></td>
<td class="tcat" align="center" width="30%"><span class="smalltext"><strong>{$lang->reason}</strong></span></td>
<td class="tcat" align="center" width="25%"><span class="smalltext"><strong>{$lang->ban_length}</strong></span></td>
<td class="tcat" align="center" width="20%"><span class="smalltext"><strong>{$lang->ban_bannedby}</strong></span></td>
</tr>
{$bannedusers}
</table>
<br />
<form action="modcp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_modnotes" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="1"><strong>{$lang->moderator_notes}</strong></td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->notes_public_all}</strong></span></td>
</tr>
<tr>
<td class="trow1"><textarea name="modnotes" style="width: 99%; height: 200px;" rows="5" cols="45">{$modnotes}</textarea></td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="notessubmit" value="{$lang->save_notes}" /></div>
</form>
</td>
</tr>
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1805',
'status' => '',
'dateline' => 1466356529,
),
171 => 
array (
'tid' => 283,
'title' => 'modcp_announcements_delete',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->delete_announcement}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="modcp.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="do_delete_announcement" />
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table width="100%" border="0" align="center">
<tr>
{$modcp_nav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$announcement[\'subject\']} - {$lang->delete_announcement}</strong></td>
</tr>
<tr>
<td class="trow1" colspan="2" align="center">{$lang->confirm_delete_announcement}</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->delete_announcement}" /></div>
<input type="hidden" name="aid" value="{$aid}" />
</form>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
172 => 
array (
'tid' => 291,
'title' => 'modcp_awaitingattachments',
'template' => '<tr>
<td class="{$bgcolor}"><span class="smalltext"><strong>{$lang->attachments}</strong></span></td>
<td class="{$bgcolor}" align="center"><span class="smalltext">{$unapproved_attachments}</span></td>
<td class="{$bgcolor}" align="center"><span class="smalltext">{$latest_attachment}</span></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
173 => 
array (
'tid' => 292,
'title' => 'modcp_awaitingposts',
'template' => '<tr>
<td class="trow2"><span class="smalltext"><strong>{$lang->posts}</strong></span></td>
<td class="trow2" align="center"><span class="smalltext">{$unapproved_posts}</span></td>
<td class="trow2" align="center"><span class="smalltext">{$latest_post}</span></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
174 => 
array (
'tid' => 293,
'title' => 'modcp_awaitingmoderation',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="3"><strong>{$lang->awaiting_moderation}</strong></td>
</tr>
<tr>
<td class="tcat" width="23%"><span class="smalltext"><strong>{$lang->type}</strong></span></td>
<td class="tcat" align="center" width="33%"><span class="smalltext"><strong>{$lang->number_awaiting}</strong></span></td>
<td class="tcat" align="center" width="44%"><span class="smalltext"><strong>{$lang->latest}</strong></span></td>
</tr>
{$awaitingthreads}
{$awaitingposts}
{$awaitingattachments}
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
175 => 
array (
'tid' => 294,
'title' => 'modcp_awaitingmoderation_none',
'template' => '<span style="text-align: center;">{$lang->lastpost_never}</span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
176 => 
array (
'tid' => 295,
'title' => 'modcp_awaitingthreads',
'template' => '<tr>
<td class="trow1"><span class="smalltext"><strong>{$lang->threads}</strong></span></td>
<td class="trow1" align="center"><span class="smalltext">{$unapproved_threads}</span></td>
<td class="trow1" align="center"><span class="smalltext">{$latest_thread}</span></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
177 => 
array (
'tid' => 297,
'title' => 'modcp_banning_ban',
'template' => '<tr>
<td class="{$trow}">{$profile_link}{$edit_link}</td>
<td class="{$trow}">{$banned[\'reason\']}</td>
<td class="{$trow}" align="center">{$banlength}<br /><span class="smalltext">{$timeremaining}</span></td>
<td class="{$trow}" align="center">{$admin_profile}</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
178 => 
array (
'tid' => 298,
'title' => 'modcp_banning_edit',
'template' => '<br /><span class="smalltext"><a href="modcp/banuser?uid={$banned[\'uid\']}">{$lang->edit_ban}</a> | <a href="modcp.php?action=liftban&amp;uid={$banned[\'uid\']}&amp;my_post_key={$mybb->post_code}">{$lang->lift_ban}</a></span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
179 => 
array (
'tid' => 309,
'title' => 'modcp_editprofile_away',
'template' => '<br />
<fieldset class="trow2">
<legend><strong>{$lang->away_information}</strong></legend>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td colspan="2"><span class="smalltext">{$lang->away_status}</span></td>
</tr>
<tr>
<td><span class="smalltext"><input type="radio" class="radio" name="away" value="1" {$awaycheck[\'1\']} /> {$lang->im_away}</span></td>
<td><span class="smalltext"><input type="radio" class="radio" name="away" value="0" {$awaycheck[\'0\']} /> {$lang->im_here}</span></td>
</tr>
<tr>
<td colspan="2"><span class="smalltext">{$lang->away_reason}</span></td>
</tr>
<tr>
<td colspan="2"><input type="text" class="textbox" name="awayreason" value="{$user[\'awayreason\']}" size="25" /></td>
</tr>
</table>
<table cellspacing="0" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td colspan="3"><span class="smalltext">{$lang->return_date}</span></td>
</tr>
<tr>
<td>
<select name="awayday">
<option value="">&nbsp;</option>
{$returndatesel}
</select>
</td>
<td>
<select name="awaymonth">
<option value="">&nbsp;</option>
<option value="1" {$returndatemonthsel[\'1\']}>{$lang->month_1}</option>
<option value="2" {$returndatemonthsel[\'2\']}>{$lang->month_2}</option>
<option value="3" {$returndatemonthsel[\'3\']}>{$lang->month_3}</option>
<option value="4" {$returndatemonthsel[\'4\']}>{$lang->month_4}</option>
<option value="5" {$returndatemonthsel[\'5\']}>{$lang->month_5}</option>
<option value="6" {$returndatemonthsel[\'6\']}>{$lang->month_6}</option>
<option value="7" {$returndatemonthsel[\'7\']}>{$lang->month_7}</option>
<option value="8" {$returndatemonthsel[\'8\']}>{$lang->month_8}</option>
<option value="9" {$returndatemonthsel[\'9\']}>{$lang->month_9}</option>
<option value="10" {$returndatemonthsel[\'10\']}>{$lang->month_10}</option>
<option value="11" {$returndatemonthsel[\'11\']}>{$lang->month_11}</option>
<option value="12" {$returndatemonthsel[\'12\']}>{$lang->month_12}</option>
</select>
</td>
<td>
<input type="text" class="textbox" size="4" maxlength="4" name="awayyear" value="{$returndate[\'2\']}" />
</td>
</tr>
</table>
</fieldset>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
180 => 
array (
'tid' => 317,
'title' => 'modcp_ipsearch',
'template' => '
<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->ipsearch}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="modcp.php?action=ipsearch" method="post">
<table width="100%" border="0" align="center">
<tr>
{$modcp_nav}
<td valign="top">
{$ipsearch_results}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->ipaddress_search}</strong></td>
</tr>
<tr>
<td class="trow1" width="25%"><strong>{$lang->ip_address}</strong></td>
<td class="trow1" width="75%"><input type="text" class="textbox" name="ipaddress" value="{$ipaddressvalue}" size="30" /></td>
</tr>
<tr>
<td class="trow1" width="25%" valign="top"><strong>{$lang->options}</strong></td>
<td class="trow1" width="75%">
<label><input type="checkbox" class="checkbox" name="search_users" value="1" {$usersearchselect} />{$lang->search_users}</label><br />
<label><input type="checkbox" class="checkbox" name="search_posts" value="1" {$postsearchselect} />{$lang->search_posts}</label>
</td>
</tr>
</table>
<br />
<div align="center">
<input type="submit" class="button" value="{$lang->find}" />
</div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
181 => 
array (
'tid' => 318,
'title' => 'modcp_ipsearch_misc_info',
'template' => '<div class="modal">
<div style="overflow-y: auto; max-height: 400px;">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="2">
<strong>{$lang->ipaddress_misc_info}</strong>
</td>
</tr>
<tr>
<td class="trow1" width="25%" align="center">{$lang->ipaddress_host_name}</td>
<td class="trow1">{$ipaddress_host_name}</td>
</tr>
<tr>
<td class="trow2" width="25%" align="center">{$lang->ipaddress_location}</td>
<td class="trow2">{$ipaddress_location}</td>
</tr>
</table>
</div>
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
182 => 
array (
'tid' => 319,
'title' => 'modcp_ipsearch_noresults',
'template' => '<tr>
<td class="trow1" align="center" colspan="2">{$lang->error_no_results}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
183 => 
array (
'tid' => 320,
'title' => 'modcp_ipsearch_result',
'template' => '<tr>
<td class="{$trow}" align="center">{$ip}</td>
<td class="{$trow}">{$subject}</td>
</tr>',
'sid' => -2,
'version' => '1408',
'status' => '',
'dateline' => 1466356529,
),
184 => 
array (
'tid' => 321,
'title' => 'modcp_ipsearch_result_lastip',
'template' => '<strong>{$lang->ipresult_lastip}</strong> {$profile_link}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
185 => 
array (
'tid' => 322,
'title' => 'modcp_ipsearch_result_post',
'template' => '<strong>{$lang->ipresult_post}</strong> <a href="{$ipaddress[\'postlink\']}">{$ipaddress[\'subject\']}</a> {$lang->by} {$ipaddress[\'profilelink\']}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
186 => 
array (
'tid' => 323,
'title' => 'modcp_ipsearch_result_regip',
'template' => '<strong>{$lang->ipresult_regip}</strong> {$profile_link}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
187 => 
array (
'tid' => 324,
'title' => 'modcp_ipsearch_results',
'template' => '					<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="2">
{$misc_info_link}<div><strong>{$lang->ipsearch_results}</strong></div>
</td>
</tr>
<tr>
<td class="tcat" width="15%" align="center"><span class="smalltext"><strong>{$lang->ipaddress}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->result}</strong></span></td>
</tr>
{$results}
</table>
{$multipage}
<br />',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
188 => 
array (
'tid' => 325,
'title' => 'modcp_ipsearch_results_information',
'template' => '<div class="float_right">(<a href="modcp.php?action=iplookup&ipaddress={$ipaddress}" onclick="MyBB.popupWindow(\'/modcp.php?action=iplookup&ipaddress={$ipaddress_url}&modal=1\'); return false;">{$lang->info_on_ip}</a>)</div>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
189 => 
array (
'tid' => 326,
'title' => 'modcp_lastattachment',
'template' => '<span class="smalltext">
<a href="{$attachment[\'link\']}#pid{$attachment[\'pid\']}"><strong>{$attachment[\'filename\']}</strong></a>
<br />{$attachment[\'date\']}<br />{$lang->by} {$attachment[\'profilelink\']}</span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
190 => 
array (
'tid' => 327,
'title' => 'modcp_lastpost',
'template' => '<span class="smalltext">
<a href="{$post[\'link\']}#pid{$post[\'pid\']}" title="{$post[\'fullsubject\']}"><strong>{$post[\'subject\']}</strong></a>
<br />{$post[\'date\']}<br />{$lang->by} {$post[\'profilelink\']}</span>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
191 => 
array (
'tid' => 328,
'title' => 'modcp_lastthread',
'template' => '<span class="smalltext">
<a href="{$thread[\'link\']}" title="{$thread[\'fullsubject\']}"><strong>{$thread[\'subject\']}</strong></a>
<br />{$thread[\'date\']}<br />{$lang->by} {$thread[\'profilelink\']}</span>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
192 => 
array (
'tid' => 329,
'title' => 'modcp_latestfivemodactions',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" align="center" colspan="5"><strong>{$lang->latest_5_modactions}</strong></td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->username}</strong></span></td>
<td class="tcat" align="center"><span class="smalltext"><strong>{$lang->date}</strong></span></td>
<td class="tcat" align="center"><span class="smalltext"><strong>{$lang->action}</strong></span></td>
<td class="tcat" align="center"><span class="smalltext"><strong>{$lang->information}</strong></span></td>
<td class="tcat" align="center"><span class="smalltext"><strong>{$lang->ip}</strong></span></td>
</tr>
{$modlogresults}
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
193 => 
array (
'tid' => 331,
'title' => 'modcp_modlogs_multipage',
'template' => '<tr>
<td class="tfoot" colspan="6"><span class="smalltext"> {$multipage}</span></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
194 => 
array (
'tid' => 333,
'title' => 'modcp_modlogs_nologs',
'template' => '<tr>
<td class="trow1" align="center" colspan="5">{$lang->no_logs}</td>

</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
195 => 
array (
'tid' => 335,
'title' => 'modcp_modlogs_result_announcement',
'template' => '<strong>{$lang->announcement}:</strong> <a href="{$data[\'announcement\']}" target="_blank">{$data[\'subject\']}</a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
196 => 
array (
'tid' => 336,
'title' => 'modcp_modlogs_result_forum',
'template' => '<strong>{$lang->forum}:</strong> <a href="{$logitem[\'forum\']}" target="_blank">{$logitem[\'fname\']}</a><br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
197 => 
array (
'tid' => 337,
'title' => 'modcp_modlogs_result_post',
'template' => '<strong>{$lang->post}:</strong> <a href="{$logitem[\'post\']}#pid{$logitem[\'pid\']}">{$logitem[\'psubject\']}</a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
198 => 
array (
'tid' => 338,
'title' => 'modcp_modlogs_result_thread',
'template' => '<strong>{$lang->thread}:</strong> <a href="{$logitem[\'thread\']}" target="_blank">{$logitem[\'tsubject\']}</a><br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
199 => 
array (
'tid' => 340,
'title' => 'modcp_modqueue_attachments',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->attachments_awaiting_moderation}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="modcp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_modqueue" />
<table width="100%" border="0" align="center">
<tr>
{$modcp_nav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="6">
<div class="float_right">
{$thread_link}
{$post_link}
{$navsep}<strong>{$lang->attachments}</strong>
</div>
<strong>{$lang->attachments_awaiting_moderation}</strong>
</td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->filename}</strong></span></td>
<td class="tcat" align="center" width="20%"><span class="smalltext"><strong>{$lang->author}</strong></span></td>
<td class="tcat" align="center" width="20%"><span class="smalltext"><strong>{$lang->date}</strong></span></td>
<td class="tcat" align="center" colspan="3"><span class="smalltext"><strong>{$lang->controls}</strong></span></td>
</tr>
{$attachments}
</table>
{$multipage}
{$mass_controls}
<br />
<div align="center"><input type="submit" class="button" name="reportsubmit" value="{$lang->perform_actions}" /></div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
200 => 
array (
'tid' => 341,
'title' => 'modcp_modqueue_attachments_attachment',
'template' => '			<tr>
<td class="{$altbg}"><a href="attachment.php?aid={$attachment[\'aid\']}" target="_blank">{$attachment[\'filename\']}</a> ({$attachment[\'filesize\']})<br /><small class="modqueue_meta">{$lang->post}: <a href="{$link}">{$attachment[\'postsubject\']}</a></small></td>
<td class="{$altbg}" align="center">{$profile_link}</td>
<td align="center" class="{$altbg}">{$attachdate}</td>
<td class="{$altbg}" align="center"><label class="label_radio_ignore"><input type="radio" class="radio radio_ignore" name="attachments[{$attachment[\'aid\']}]" value="ignore" checked="checked" /> {$lang->ignore}</label></td>
<td class="{$altbg}" align="center"><label class="label_radio_delete"><input type="radio" class="radio radio_delete" name="attachments[{$attachment[\'aid\']}]" value="delete" /> {$lang->delete}</label></td>
<td class="{$altbg}" align="center"><label class="label_radio_approve"><input type="radio" class="radio radio_approve" name="attachments[{$attachment[\'aid\']}]" value="approve" /> {$lang->approve}</label></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
201 => 
array (
'tid' => 342,
'title' => 'modcp_modqueue_attachments_empty',
'template' => '<tr>
<td class="trow1" align="center" colspan="6">{$lang->mod_queue_attachments_empty}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
202 => 
array (
'tid' => 343,
'title' => 'modcp_modqueue_attachment_link',
'template' => '{$navsep}<a href="modcp.php?action=modqueue&amp;type=attachments">{$lang->attachments}</a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
203 => 
array (
'tid' => 345,
'title' => 'modcp_modqueue_link_forum',
'template' => '<strong>{$lang->meta_forum} <a href="{$forum_link}">{$forum_name}</a></strong><br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
204 => 
array (
'tid' => 346,
'title' => 'modcp_modqueue_link_thread',
'template' => '<strong>{$lang->meta_thread} <a href="{$post[\'threadlink\']}">{$post[\'threadsubject\']}</a></strong>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
205 => 
array (
'tid' => 347,
'title' => 'modcp_modqueue_masscontrols',
'template' => '<ul class="modqueue_mass">
<li><a href="#" class="mass_ignore" onclick="$(\'input.radio_ignore\').each(function(){ $(this).prop(\'checked\', true); }); return false">{$lang->mark_all_ignored}</a></li>
<li><a href="#" class="mass_delete" onclick="$(\'input.radio_delete\').each(function(){ $(this).prop(\'checked\', true); }); return false;">{$lang->mark_all_deletion}</a></li>
<li><a href="#" class="mass_approve" onclick="$(\'input.radio_approve\').each(function(){ $(this).prop(\'checked\', true); }); return false;">{$lang->mark_all_approved}</a></li>
</ul>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
206 => 
array (
'tid' => 348,
'title' => 'modcp_modqueue_posts',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->posts_awaiting_moderation}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="modcp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_modqueue" />
<table width="100%" border="0" align="center">
<tr>
{$modcp_nav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="3">
<div class="float_right">
{$thread_link}
<strong>{$lang->posts}</strong>
{$attachment_link}
</div>
<strong>{$lang->posts_awaiting_moderation}</strong>
</td>
</tr>
<tr>
<td class="tcat" width="50%"><span class="smalltext"><strong>{$lang->subject}</strong></span></td>
<td class="tcat" align="center" width="25%"><span class="smalltext"><strong>{$lang->author}</strong></span></td>
<td class="tcat" align="center" width="25%"><span class="smalltext"><strong>{$lang->date}</strong></span></td>
</tr>
{$posts}
</table>
{$multipage}
{$mass_controls}
<br />
<div align="center"><input type="submit" class="button" name="reportsubmit" value="{$lang->perform_actions}" /></div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
207 => 
array (
'tid' => 349,
'title' => 'modcp_modqueue_posts_empty',
'template' => '<tr>
<td class="trow1" align="center" colspan="3">{$lang->mod_queue_posts_empty}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
208 => 
array (
'tid' => 350,
'title' => 'modcp_modqueue_posts_post',
'template' => '			<tr>
<td class="{$altbg}"><a href="{$post[\'postlink\']}#pid{$post[\'pid\']}">{$post[\'subject\']}</a></td>
<td class="{$altbg}" align="center">{$profile_link}</td>
<td align="center" class="{$altbg}">{$postdate}</td>
</tr>
<tr>
<td class="{$altbg}" colspan="3">
<div class="modqueue_message">
<div class="float_right modqueue_controls">
<label class="label_radio_ignore"><input type="radio" class="radio radio_ignore" name="posts[{$post[\'pid\']}]" value="ignore" checked="checked" /> {$lang->ignore}</label>
<label class="label_radio_delete"><input type="radio" class="radio radio_delete" name="posts[{$post[\'pid\']}]" value="delete" /> {$lang->delete}</label>
<label class="label_radio_approve"><input type="radio" class="radio radio_approve" name="posts[{$post[\'pid\']}]" value="approve" /> {$lang->approve}</label>
</div>
<div class="modqueue_meta">
{$forum}{$thread}
</div>
{$post[\'message\']}
</div>
</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
209 => 
array (
'tid' => 351,
'title' => 'modcp_modqueue_post_link',
'template' => '{$navsep}<a href="modcp.php?action=modqueue&amp;type=posts">{$lang->posts}</a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
210 => 
array (
'tid' => 352,
'title' => 'modcp_modqueue_threads',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->threads_awaiting_moderation}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="modcp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_modqueue" />
<table width="100%" border="0" align="center">
<tr>
{$modcp_nav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="3">
<div class="float_right">
<strong>{$lang->threads}</strong>
{$post_link}
{$attachment_link}
</div>
<strong>{$lang->threads_awaiting_moderation}</strong>
</td>
</tr>
<tr>
<td class="tcat" width="50%"><span class="smalltext"><strong>{$lang->subject}</strong></span></td>
<td class="tcat" align="center" width="25%"><span class="smalltext"><strong>{$lang->author}</strong></span></td>
<td class="tcat" align="center" width="25%"><span class="smalltext"><strong>{$lang->date}</strong></span></td>
</tr>
{$threads}
</table>
{$multipage}
{$mass_controls}
<br />
<div align="center"><input type="submit" class="button" name="reportsubmit" value="{$lang->perform_actions}" /></div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
211 => 
array (
'tid' => 353,
'title' => 'modcp_modqueue_threads_empty',
'template' => '<tr>
<td class="trow1" align="center" colspan="3">{$lang->mod_queue_threads_empty}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
212 => 
array (
'tid' => 354,
'title' => 'modcp_modqueue_threads_thread',
'template' => '			<tr>
<td class="{$altbg}"><a href="{$thread[\'threadlink\']}">{$thread[\'subject\']}</a></td>
<td class="{$altbg}" align="center">{$profile_link}</td>
<td align="center" class="{$altbg}">{$threaddate}</td>
</tr>
<tr>
<td class="{$altbg}" colspan="3">
<div class="modqueue_message">
<div class="float_right modqueue_controls">
<label class="label_radio_ignore"><input type="radio" class="radio radio_ignore" name="threads[{$thread[\'tid\']}]" value="ignore" checked="checked" /> {$lang->ignore}</label>
<label class="label_radio_delete"><input type="radio" class="radio radio_delete" name="threads[{$thread[\'tid\']}]" value="delete" /> {$lang->delete}</label>
<label class="label_radio_approve"><input type="radio" class="radio radio_approve" name="threads[{$thread[\'tid\']}]" value="approve" /> {$lang->approve}</label>
</div>
<div class="modqueue_meta">
{$forum}
</div>
{$thread[\'postmessage\']}
</div>
</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
213 => 
array (
'tid' => 355,
'title' => 'modcp_modqueue_thread_link',
'template' => '<a href="modcp.php?action=modqueue&amp;type=threads">{$lang->threads}</a>{$navsep}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
214 => 
array (
'tid' => 356,
'title' => 'modcp_nav',
'template' => '<td width="{$lang->nav_width}" valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->nav_menu}</strong></td>
</tr>
<tr><td class="trow1 smalltext"><a href="modcp.php" class="modcp_nav_item modcp_nav_home">{$lang->mcp_nav_home}</a></td></tr>
{$modcp_nav_forums_posts}
{$modcp_nav_users}
</table>
</td>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
215 => 
array (
'tid' => 357,
'title' => 'modcp_nav_announcements',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=announcements" class="modcp_nav_item modcp_nav_announcements">{$lang->mcp_nav_announcements}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
216 => 
array (
'tid' => 358,
'title' => 'modcp_nav_banning',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=banning" class="modcp_nav_item modcp_nav_banning">{$lang->mcp_nav_banning}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
217 => 
array (
'tid' => 359,
'title' => 'modcp_nav_forums_posts',
'template' => '	<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'modcpforums\']}">
<div class="expcolimage"><img src="{$theme[\'imgdir\']}/collapse{$collapsedimg[\'modcpforums\']}.png" id="modcpforums_img" class="expander" alt="[-]" title="[-]" /></div>
<div><span class="smalltext"><strong>{$lang->mcp_nav_forums}</strong></span></div>
</td>
</tr>
<tbody style="{$collapsed[\'modcpforums_e\']}" id="modcpforums_e">
{$nav_announcements}
{$nav_modqueue}
{$nav_reportcenter}
{$nav_modlogs}
</tbody>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
218 => 
array (
'tid' => 360,
'title' => 'modcp_nav_editprofile',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=finduser" class="modcp_nav_item modcp_nav_editprofile">{$lang->mcp_nav_editprofile}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
219 => 
array (
'tid' => 361,
'title' => 'modcp_nav_ipsearch',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=ipsearch" class="modcp_nav_item modcp_nav_ipsearch">{$lang->mcp_nav_ipsearch}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
220 => 
array (
'tid' => 362,
'title' => 'modcp_nav_modlogs',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=modlogs" class="modcp_nav_item modcp_nav_modlogs">{$lang->mcp_nav_modlogs}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
221 => 
array (
'tid' => 363,
'title' => 'modcp_nav_modqueue',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=modqueue" class="modcp_nav_item modcp_nav_modqueue">{$lang->mcp_nav_modqueue}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
222 => 
array (
'tid' => 364,
'title' => 'modcp_nav_reportcenter',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=reports" class="modcp_nav_item modcp_nav_reports">{$lang->mcp_nav_report_center}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
223 => 
array (
'tid' => 365,
'title' => 'modcp_nav_users',
'template' => '	<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'modcpusers\']}">
<div class="expcolimage"><img src="{$theme[\'imgdir\']}/collapse{$collapsedimg[\'modcpusers\']}.png" id="modcpusers_img" class="expander" alt="[-]" title="[-]" /></div>
<div><span class="smalltext"><strong>{$lang->mcp_nav_users}</strong></span></div>
</td>
</tr>
<tbody style="{$collapsed[\'modcpusers_e\']}" id="modcpusers_e">
{$nav_editprofile}
{$nav_banning}
{$nav_warninglogs}
{$nav_ipsearch}
</tbody>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
224 => 
array (
'tid' => 366,
'title' => 'modcp_nav_warninglogs',
'template' => '<tr><td class="trow1 smalltext"><a href="modcp.php?action=warninglogs" class="modcp_nav_item modcp_nav_warninglogs">{$lang->mcp_nav_warninglogs}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
225 => 
array (
'tid' => 376,
'title' => 'modcp_nobanned',
'template' => '<tr>
<td class="trow1" align="center" colspan="4">{$lang->no_bans_ending}</td>
</tr>',
'sid' => -2,
'version' => '1613',
'status' => '',
'dateline' => 1466356529,
),
226 => 
array (
'tid' => 381,
'title' => 'moderation_delayedmodaction_notes',
'template' => '<tr>
<td class="{$trow}" align="center">{$delayedmod[\'profilelink\']}</td>
<td class="{$trow}" align="center">{$delayedmod[\'dateline\']}</td>
<td class="{$trow}" align="center">{$delayedmod[\'action\']}</td>
<td class="{$trow}">{$info}</td>
<td class="{$trow}" align="center"><a href="moderation.php?action=cancel_delayedmoderation&amp;tid={$tid}&amp;fid={$fid}&amp;did={$delayedmod[\'did\']}&amp;my_post_key={$mybb->post_code}">{$lang->cancel}</a></td>
</tr>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
227 => 
array (
'tid' => 382,
'title' => 'moderation_delayedmoderation',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->delayed_moderation}</title>
{$headerinclude}
</head>
<body>
{$header}
{$display_errors}
<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="5"><strong>{$lang->delayed_mod_queue}</strong></td>
</tr>
<tr>
<td class="tcat" align="center"><strong>{$lang->mod_username}</strong></td>
<td class="tcat" align="center"><strong>{$lang->days_to_perform_action}</strong></td>
<td class="tcat" align="center"><strong>{$lang->mod_actions}</strong></td>
<td class="tcat" align="center"><strong>{$lang->mod_information}</strong></td>
<td class="tcat" align="center"><strong>{$lang->actions}</strong></td>
</tr>
{$delayedmods}
</table>
<br />
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_delayedmoderation" />
<input type="hidden" name="url" value="{$url}" />
<input type="hidden" name="tid" value="{$tid}" />
<input type="hidden" name="fid" value="{$fid}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->delayed_moderation}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2"><strong>{$lang->delayed_moderation_desc}</strong></td>
</tr>
{$loginbox}
<tr>
<td class="trow1"><strong>{$lang->threads}</strong></td>
<td class="trow2">{$threads}</td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->run_moderation_time}</strong></td>
<td class="trow2">
<select name="date_day">
{$dateday}
</select>
&nbsp;
<select name="date_month">
{$datemonth}
</select>
&nbsp;
<input type="text" name="date_year" value="{$dateyear}" size="4" maxlength="4" class="textbox" />
- {$lang->time} <input type="text" name="date_time" value="{$datetime}" size="10" class="textbox" />
</td>
</tr>
<tr>
<td class="trow1" style="vertical-align: top;"><strong>{$lang->moderation_action}</strong></td>
<td class="trow2">
<script type="text/javascript">
<!--
function toggleType()
{
if($(\'#type_merge\'))
{
if($(\'#type_movecopythread\').prop(\'checked\') == true)
{
$(\'#type_merge_expanded\').hide();
$(\'#type_movecopythread_expanded\').show();
}
else if($(\'#type_merge\').prop(\'checked\') == true)
{
$(\'#type_movecopythread_expanded\').hide();
$(\'#type_merge_expanded\').show();
}
else
{
$(\'#type_movecopythread_expanded\').hide();
$(\'#type_merge_expanded\').hide();
}
}
else
{
if($(\'#type_movecopythread\').prop(\'checked\') == true)
{
$(\'#type_movecopythread_expanded\').show();
}
else
{
$(\'#type_movecopythread_expanded\').hide();
}
}
}
// -->
</script>
<dl style="margin-top: 0; margin-bottom: 0; width: 100%;">
<dt><input type="radio" name="type" value="openclosethread" {$type_selected[\'openclosethread\']} id="type_openclosethread" onclick="toggleType();" /> <label for="type_openclosethread"><strong>{$lang->open_close_thread}</strong></label></dt>
<dt><input type="radio" name="type" value="softdeleterestorethread" {$type_selected[\'softdeleterestorethread\']} id="type_softdeleterestorethread" onclick="toggleType();" /> <label for="type_softdeleterestorethread"><strong>{$lang->softdelete_restore_thread}</strong></label></dt>
<dt><input type="radio" name="type" value="deletethread" {$type_selected[\'deletethread\']} id="type_deletethread" onclick="toggleType();" /> <label for="type_deletethread"><strong>{$lang->delete_thread}</strong></label></dt>
<dt><input type="radio" name="type" value="move" {$type_selected[\'move\']} id="type_movecopythread" onclick="toggleType();" /> <label for="type_movecopythread"><strong>{$lang->move_copy_thread}</strong></label></dt>
<dd style="margin-top: 4px;" id="type_movecopythread_expanded">
{$lang->new_forum}<br />
{$forumselect}
{$moderation_delayedmoderation_move}
</dd>
<dt><input type="radio" name="type" value="stick" {$type_selected[\'stick\']} id="type_stick_unstick_thread" onclick="toggleType();" /> <label for="type_stick_unstick_thread"><strong>{$lang->stick_unstick_thread}</strong></label></dt>
{$moderation_delayedmoderation_merge}
<dt><input type="radio" name="type" value="removeredirects" {$type_selected[\'removeredirects\']} id="type_removeredirects" onclick="toggleType();" /> <label for="type_removeredirects"><strong>{$lang->remove_redirects}</strong></label></dt>
<dt><input type="radio" name="type" value="removesubscriptions" {$type_selected[\'removesubscriptions\']} id="type_removesubscriptions" onclick="toggleType();" /> <label for="type_removesubscriptions"><strong>{$lang->remove_subscriptions}</strong></label></dt>
<dt><input type="radio" name="type" value="approveunapprovethread" {$type_selected[\'approveunapprovethread\']} id="type_approveunapprovethread" onclick="toggleType();" /> <label for="type_approveunapprovethread"><strong>{$lang->approve_unapprove_thread}</strong></label></dt>
{$customthreadtools}
</dl>
<script type="text/javascript">
<!--
toggleType();
// -->
</script>
</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" value="{$lang->save_delayed_moderation}" /></div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
228 => 
array (
'tid' => 383,
'title' => 'moderation_delayedmoderation_date_month',
'template' => '<option value="01" {$datemonth[\'01\']}>{$lang->january}</option>
<option value="02" {$datemonth[\'02\']}>{$lang->february}</option>
<option value="03" {$datemonth[\'03\']}>{$lang->march}</option>
<option value="04" {$datemonth[\'04\']}>{$lang->april}</option>
<option value="05" {$datemonth[\'05\']}>{$lang->may}</option>
<option value="06" {$datemonth[\'06\']}>{$lang->june}</option>
<option value="07" {$datemonth[\'07\']}>{$lang->july}</option>
<option value="08" {$datemonth[\'08\']}>{$lang->august}</option>
<option value="09" {$datemonth[\'09\']}>{$lang->september}</option>
<option value="10" {$datemonth[\'10\']}>{$lang->october}</option>
<option value="11" {$datemonth[\'11\']}>{$lang->november}</option>
<option value="12" {$datemonth[\'12\']}>{$lang->december}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
229 => 
array (
'tid' => 384,
'title' => 'moderation_delayedmoderation_date_day',
'template' => '<option value="{$day}"{$selected}">{$day}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
230 => 
array (
'tid' => 385,
'title' => 'moderation_delayedmoderation_custommodtool',
'template' => '<dt><input type="radio" name="type" value="modtool_{$tool[\'tid\']}" {$checked} id="type_modtool_{$tool[\'tid\']}" onclick="toggleType();" /> <label for="type_modtool_{$tool[\'tid\']}"><strong>{$tool[\'name\']}</strong> <small>({$lang->custom})</small></label></dt>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
231 => 
array (
'tid' => 386,
'title' => 'moderation_delayedmoderation_merge',
'template' => '<dt><input type="radio" name="type" value="merge" {$type_selected[\'merge\']} id="type_merge" onclick="toggleType();" /> <label for="type_merge"><strong>{$lang->merge_threads}</strong></label></dt>
<dd style="margin-top: 4px; width: 100%;" id="type_merge_expanded">
{$lang->new_subject}<br />
<input type="text" class="textbox" name="delayedmoderation[subject]" value="{$mybb->input[\'delayedmoderation\'][\'subject\']}" size="40" /><br />
{$lang->thread_to_merge_with}<br />
<input type="text" class="textbox" name="delayedmoderation[threadurl]" value="{$mybb->input[\'delayedmoderation\'][\'threadurl\']}" size="40" />
<br /><span class="smalltext">{$lang->merge_with_note}</span>
</dd>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
232 => 
array (
'tid' => 387,
'title' => 'moderation_delayedmoderation_move',
'template' => '<br />
{$lang->method}:<br />
<label><input type="radio" class="radio" name="delayedmoderation[method]" value="move" {$method_selected[\'move\']} />{$lang->method_move}</label><br />
<label><input type="radio" class="radio" name="delayedmoderation[method]" value="redirect" {$method_selected[\'redirect\']} />{$lang->method_move_redirect}</label> <input type="text" class="textbox" name="delayedmoderation[redirect_expire]" value="{$mybb->input[\'redirect_expire\']}" size="3" /> {$lang->redirect_expire_note}<br />
<label><input type="radio" class="radio" name="delayedmoderation[method]" value="copy" {$method_selected[\'copy\']} />{$lang->method_copy}</label><br />',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
233 => 
array (
'tid' => 388,
'title' => 'moderation_confirmation',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->confirm_execute_tool}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="{$mybb->input[\'action\']}" />
<input type="hidden" name="modtype" value="{$modtype}" />
<input type="hidden" name="tid" value="{$tid}" />
<input type="hidden" name="pid" value="{$pid}" />
<input type="hidden" name="fid" value="{$fid}" />
<input type="hidden" name="pmid" value="{$pmid}" />
<input type="hidden" name="confirm" value="1" />
<input type="hidden" name="inlinetype" value="{$inlinetype}" />
<input type="hidden" name="searchid" value="{$searchid}" />
<input type="hidden" name="url" value="{$url}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$thread[\'subject\']} - {$lang->confirm_execute_tool}</strong></td>
</tr>
<tr>
<td class="trow1" colspan="2" align="center">{$lang->confirm_execute_tool_desc}</td>
</tr>
{$loginbox}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->confirm_execute_tool}" /></div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
234 => 
array (
'tid' => 390,
'title' => 'moderation_deletepoll',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->delete_poll}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->delete_poll}</strong></td>
</tr>
{$loginbox}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->delete_poll}" /></div>
<input type="hidden" name="action" value="do_deletepoll" />
<input type="hidden" name="tid" value="{$tid}" />
<input type="hidden" name="delete" value="1" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
235 => 
array (
'tid' => 391,
'title' => 'moderation_deletethread',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->delete_thread}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$thread[\'subject\']} - {$lang->delete_thread}</strong></td>
</tr>
<tr>
<td class="trow1" colspan="2" align="center">{$lang->confirm_delete_threads}</td>
</tr>
{$loginbox}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->delete_thread}" /></div>
<input type="hidden" name="action" value="do_deletethread" />
<input type="hidden" name="tid" value="{$tid}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
236 => 
array (
'tid' => 392,
'title' => 'moderation_getip',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->get_post_ip}</title>
{$headerinclude}
</head>
<body>
{$header}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->get_post_ip}</strong></td>
</tr>
<tr>
<td class="trow1" width="20%"><strong>{$lang->username}</strong></td>
<td class="trow1">{$username}</td>
</tr>
<tr>
<td class="trow2"><strong>{$lang->ip_address}</strong></td>
<td class="trow2">{$post[\'ipaddress\']}</td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->hostname}<br /><span class="smalltext">{$lang->if_resolvable}</span></strong></td>
<td class="trow2">{$hostname}</td>
</tr>
{$modoptions}
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1608',
'status' => '',
'dateline' => 1466356529,
),
237 => 
array (
'tid' => 393,
'title' => 'moderation_getpmip',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->get_pm_ip}</title>
{$headerinclude}
</head>
<body>
{$header}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->get_pm_ip}</strong></td>
</tr>
<tr>
<td class="trow1" width="20%"><strong>{$lang->username}</strong></td>
<td class="trow1">{$username}</td>
</tr>
<tr>
<td class="trow2"><strong>{$lang->ip_address}</strong></td>
<td class="trow2">{$pm[\'ipaddress\']}</td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->hostname}<br /><span class="smalltext">{$lang->if_resolvable}</span></strong></td>
<td class="trow2">{$hostname}</td>
</tr>
{$modoptions}
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
238 => 
array (
'tid' => 394,
'title' => 'moderation_getip_modoptions',
'template' => '<tr>
<td class="trow2"><strong>{$lang->mod_options}</strong></td>
<td class="trow2">
<a href="modcp.php?action=ipsearch&amp;ipaddress={$ipaddress}&amp;search_users=1">{$lang->search_ip_users}</a><br />
<a href="modcp.php?action=ipsearch&amp;ipaddress={$ipaddress}&amp;search_posts=1">{$lang->search_ip_posts}</a><br />
<a href="modcp.php?action=iplookup&ipaddress={$ipaddress}" onclick="MyBB.popupWindow(\'/modcp.php?action=iplookup&ipaddress={$ipaddress}&modal=1\'); return false;">{$lang->info_on_ip}</a>
</td>
</tr>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
239 => 
array (
'tid' => 396,
'title' => 'moderation_inline_deletethreads',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->delete_threads}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->delete_threads}</strong></td>
</tr>
<tr>
<td class="trow1" colspan="2" align="center">{$lang->confirm_delete_threads}</td>
</tr>
{$loginbox}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->delete_threads}" /></div>
<input type="hidden" name="action" value="do_multideletethreads" />
<input type="hidden" name="fid" value="{$fid}" />
<input type="hidden" name="threads" value="{$inlineids}" />
<input type="hidden" name="url" value="{$return_url}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
240 => 
array (
'tid' => 397,
'title' => 'moderation_inline_mergeposts',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->merge_posts}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->merge_posts}</strong></td>
</tr>
<tr>
{$loginbox}
<tr>
<td class="trow2"><strong>{$lang->post_separator}</strong></td>
<td class="trow2"><label><input type="radio" class="radio" name="sep" value="hr" checked="checked" />&nbsp;{$lang->horizontal_rule}</label><br /><label><input type="radio" class="radio" name="sep" value="new_line" />&nbsp;{$lang->new_line}</label></td>
</tr>
{$postlist}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->merge_posts}" /></div>
<input type="hidden" name="action" value="do_multimergeposts" />
<input type="hidden" name="tid" value="{$tid}" />
<input type="hidden" name="url" value="{$return_url}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
241 => 
array (
'tid' => 398,
'title' => 'moderation_inline_movethreads',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->move_threads}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->move_threads}</strong></td>
</tr>
{$loginbox}
<tr>
<td class="trow1"><strong>{$lang->new_forum}</strong></td>
<td class="trow2">{$forumselect}</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->move_threads}" /></div>
<input type="hidden" name="action" value="do_multimovethreads" />
<input type="hidden" name="fid" value="{$fid}" />
<input type="hidden" name="threads" value="{$inlineids}" />
<input type="hidden" name="url" value="{$return_url}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
242 => 
array (
'tid' => 399,
'title' => 'moderation_inline_splitposts',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->split_thread}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->split_thread}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2"><span class="smalltext"><strong>{$lang->new_thread_info}</strong></span></td>
</tr>
{$loginbox}
<tr>
<td class="trow2"><strong>{$lang->new_subject}</strong></td>
<td class="trow2"><input type="text" class="textbox" name="newsubject" value="{$lang->split_thread_subject} {$thread[\'subject\']}" size="50" /></td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->new_forum}</strong></td>
<td class="trow1">{$forumselect}</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->split_thread}" /></div>
<input type="hidden" name="action" value="do_multisplitposts" />
<input type="hidden" name="tid" value="{$tid}" />
<input type="hidden" name="posts" value="{$inlineids}" />
<input type="hidden" name="url" value="{$return_url}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1607',
'status' => '',
'dateline' => 1466356529,
),
243 => 
array (
'tid' => 400,
'title' => 'moderation_inline_moveposts',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->move_posts}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->move_posts}</strong></td>
</tr>
{$loginbox}
<tr>
<td class="trow1"><strong>{$lang->thread_to_move_to}</strong><br /><span class="smalltext">{$lang->move_post_note}</span></td>
<td class="trow1" width="60%"><input type="text" class="textbox" name="threadurl" size="40" />
</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->move_posts}" /></div>
<input type="hidden" name="action" value="do_multimoveposts" />
<input type="hidden" name="tid" value="{$tid}" />
<input type="hidden" name="posts" value="{$inlineids}" />
<input type="hidden" name="url" value="{$return_url}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
244 => 
array (
'tid' => 401,
'title' => 'moderation_merge',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->merge_threads}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->merge_threads}</strong></td>
</tr>
{$loginbox}
<tr>
<td class="trow2"><strong>{$lang->new_subject}</strong></td>
<td class="trow2"><input type="text" class="textbox" name="subject" value="{$thread[\'subject\']}" size="40" /></td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->thread_to_merge_with}</strong><br /><span class="smalltext">{$lang->merge_with_note}</span></td>
<td class="trow1" width="60%"><input type="text" class="textbox" name="threadurl" size="40" />
</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->merge_threads}" /></div>
<input type="hidden" name="action" value="do_merge" />
<input type="hidden" name="tid" value="{$tid}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
245 => 
array (
'tid' => 402,
'title' => 'moderation_mergeposts_post',
'template' => '<tr>
<td class="tcat" colspan="2"><span class="smalltext"><strong>{$lang->posted_by} {$post[\'username\']} - {$postdate}</strong></span></td>
</tr>
<tr>
<td class="{$altbg}" valign="top" align="center" width="1"><input type="checkbox" class="checkbox" checked="checked" name="mergepost[{$post[\'pid\']}]" value="1" /></td>
<td class="{$altbg}">{$message}</td>
</tr>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
246 => 
array (
'tid' => 403,
'title' => 'moderation_move',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->move_copy_thread}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->move_copy_thread}</strong></td>
</tr>
{$loginbox}
<tr>
<td class="trow1"><strong>{$lang->new_forum}</strong></td>
<td class="trow2">{$forumselect}</td>
</tr>
<tr>
<td class="trow1" valign="top"><strong>{$lang->method}</strong></td>
<td class="trow2">
<label><input type="radio" class="radio" name="method" value="move" />{$lang->method_move}</label><br />
<label><input type="radio" class="radio" name="method" value="redirect" checked="checked" />{$lang->method_move_redirect}</label> <input type="text" class="textbox" name="redirect_expire" size="3" /> {$lang->redirect_expire_note}<br />
<label><input type="radio" class="radio" name="method" value="copy" />{$lang->method_copy}</label><br />
</td>
</tr>
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->move_copy_thread}" /></div>
<input type="hidden" name="action" value="do_move" />
<input type="hidden" name="tid" value="{$tid}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
247 => 
array (
'tid' => 404,
'title' => 'moderation_purgespammer',
'template' => '<html>
<head>
<title>{$lang->purgespammer}</title>
{$headerinclude}
</head>
<body>
{$header}
<form method="post" action="moderation.php">
<input type="hidden" name="action" value="do_purgespammer" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead">
<strong>{$lang->purgespammer_purge}</strong>
</td>
</tr>
<tr>
<td class="trow1">{$lang->purgespammer_purge_desc}</td>
</tr>
<tr>
<td class="tfoot" align="center"><input class="button" type="submit" value="{$lang->purgespammer_submit}" /></td>
</tr>
</table>
<input type="hidden" name="uid" value="{$mybb->input[\'uid\']}" />
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
248 => 
array (
'tid' => 405,
'title' => 'moderation_split',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->split_thread}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="moderation.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->split_thread}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2"><span class="smalltext"><strong>{$lang->new_thread_info}</strong></span></td>
</tr>
{$loginbox}
<tr>
<td class="trow2"><strong>{$lang->new_subject}</strong></td>
<td class="trow2"><input type="text" class="textbox" name="newsubject" value="{$lang->split_thread_subject} {$thread[\'subject\']}" size="50" /></td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->new_forum}</strong></td>
<td class="trow1">{$forumselect}</td>
</tr>
</table>
<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->posts_to_split}</strong></td>
</tr>
{$posts}
</table>
<br />
<div align="center"><input type="submit" class="button" name="submit" value="{$lang->split_thread}" /></div>
<input type="hidden" name="action" value="do_split" />
<input type="hidden" name="tid" value="{$tid}" />
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1607',
'status' => '',
'dateline' => 1466356529,
),
249 => 
array (
'tid' => 406,
'title' => 'moderation_split_post',
'template' => '<tr>
<td class="tcat" colspan="2"><span class="smalltext"><strong>{$lang->posted_by} {$post[\'username\']} - {$postdate}</strong></span></td>
</tr>
<tr>
<td class="{$altbg}" valign="top" align="center" width="1%"><input type="checkbox" class="checkbox" name="splitpost[{$post[\'pid\']}]" value="1" /></td>
<td class="{$altbg}">{$message}</td>
</tr>',
'sid' => -2,
'version' => '127',
'status' => '',
'dateline' => 1466356529,
),
250 => 
array (
'tid' => 410,
'title' => 'moderation_viewthreadnotes',
'template' => '<div class="modal">
<div style="overflow-y: auto; max-height: 400px;">
<table width="100%" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" border="0" align="center" class="tborder">
<tr>
<td colspan="2" class="thead"><strong>{$lang->view_notes_for}</strong></td>
</tr>
<tr>
<td class="trow1">{$thread[\'notes\']}</td>
</tr>
</table>
</div>
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
251 => 
array (
'tid' => 412,
'title' => 'multipage',
'template' => '<div class="pagination">
<span class="pages">{$lang->multipage_pages}</span>
{$prevpage}{$start}{$mppage}{$end}{$nextpage}{$jumptopage}
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
252 => 
array (
'tid' => 413,
'title' => 'multipage_breadcrumb',
'template' => '<div id="breadcrumb_multipage_popup" class="pagination pagination_breadcrumb" style="display: none;">
{$prevpage}{$start}{$mppage}{$end}{$nextpage}
</div>
<script type="text/javascript">
// <!--
if(use_xmlhttprequest == "1")
{
$("#breadcrumb_multipage").popupMenu();
}
// -->
</script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
253 => 
array (
'tid' => 414,
'title' => 'multipage_end',
'template' => '{$lang->multipage_link_end} <a href="{$page_url}" class="pagination_last">{$pages}</a>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
254 => 
array (
'tid' => 415,
'title' => 'multipage_jump_page',
'template' => '<div class="popup_menu drop_go_page" style="display: none;">
<form action="{$jump_url}" method="post">
<label for="page">{$lang->multipage_jump}:</label> <input type="text" class="textbox" name="page" value="{$page}" size="4" />
<input type="submit" class="button" value="{$lang->go}" />
</form>
</div>
<a href="javascript:;" class="go_page" title="{$lang->multipage_jump}"><img src="{$theme[\'imgdir\']}/arrow_down.png" alt="{$lang->multipage_jump}" /></a>&nbsp;
<script type="text/javascript">
var go_page = \'go_page_\' + $(".go_page").length;
$(".go_page").last().attr(\'id\', go_page);
$(".drop_go_page").last().attr(\'id\', go_page + \'_popup\');
$(\'#\' + go_page).popupMenu(false).click(function() {
var drop_go_page = $(this).prev(\'.drop_go_page\');
if (drop_go_page.is(\':visible\')) {
drop_go_page.find(\'.textbox\').focus();
}
});
</script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
255 => 
array (
'tid' => 416,
'title' => 'multipage_nextpage',
'template' => '<a href="{$page_url}" class="pagination_next">{$lang->next} &raquo;</a>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
256 => 
array (
'tid' => 417,
'title' => 'multipage_page',
'template' => '<a href="{$page_url}" class="pagination_page">{$i}</a>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
257 => 
array (
'tid' => 418,
'title' => 'multipage_page_current',
'template' => ' <span class="pagination_current">{$i}</span>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
258 => 
array (
'tid' => 419,
'title' => 'multipage_page_link_current',
'template' => ' <a href="{$page_url}" class="pagination_current">{$i}</a>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
259 => 
array (
'tid' => 420,
'title' => 'multipage_prevpage',
'template' => '<a href="{$page_url}" class="pagination_previous">&laquo; {$lang->previous}</a>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
260 => 
array (
'tid' => 421,
'title' => 'multipage_start',
'template' => '<a href="{$page_url}" class="pagination_first">1</a> {$lang->multipage_link_start}',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
261 => 
array (
'tid' => 422,
'title' => 'nav',
'template' => '
<div class="navigation">
{$nav}{$activesep}{$activebit}
</div>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
262 => 
array (
'tid' => 423,
'title' => 'nav_bit',
'template' => '<a href="{$navbit[\'url\']}">{$navbit[\'name\']}</a>{$sep}',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
263 => 
array (
'tid' => 424,
'title' => 'nav_bit_active',
'template' => '<span class="active">{$navbit[\'name\']}</span>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
264 => 
array (
'tid' => 425,
'title' => 'nav_dropdown',
'template' => ' <img src="{$theme[\'imgdir\']}/arrow_down.png" alt="v" title="" class="pagination_breadcrumb_link" id="breadcrumb_multipage" />{$multipage}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
265 => 
array (
'tid' => 426,
'title' => 'nav_sep',
'template' => '&rsaquo;',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
266 => 
array (
'tid' => 427,
'title' => 'nav_sep_active',
'template' => '<br /><img src="{$theme[\'imgdir\']}/nav_bit.png" alt="" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
267 => 
array (
'tid' => 447,
'title' => 'php_warnings',
'template' => '<table border="0" cellspacing="1" cellpadding="4" align="center" class="tborder">
<tr>
<td class="tcat">
<strong>{$lang->warnings}</strong>
</td>
</tr>
<tr>
<td class="trow1"><span class="smalltext">{$this->warnings}</span><br /></td>
</tr>
</table><br /><br />',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
268 => 
array (
'tid' => 454,
'title' => 'portal',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']}</title>
<link rel="alternate" type="application/rss+xml" title="{$lang->latest_announcements} (RSS 2.0)" href="{$mybb->settings[\'bburl\']}/syndication.php?portal=1" />
<link rel="alternate" type="application/atom+xml" title="{$lang->latest_announcements} (Atom 1.0)" href="{$mybb->settings[\'bburl\']}/syndication.php?portal=1&type=atom1.0" />
{$headerinclude}
</head>
<body>
{$header}
<table width="100%" cellspacing="0" cellpadding="{$theme[\'tablespace\']}" border="0" align="center">
<tr><td valign="top" width="200">
{$welcome}
{$pms}
{$search}
{$stats}
{$whosonline}
{$latestthreads}
</td>
<td>&nbsp;</td>
<td valign="top">
{$announcements}
{$multipage}
</td>
</tr>
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
269 => 
array (
'tid' => 455,
'title' => 'portal_announcement',
'template' => '<table cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$icon} <a href="{$mybb->settings[\'bburl\']}/{$announcement[\'threadlink\']}">{$announcement[\'subject\']}</a></strong></td>
</tr>
<tr>
<td class="trow2" align="right">
<span class="smalltext">{$lang->posted_by} {$profilelink}  - {$anndate} - {$lang->forum} <a href="{$mybb->settings[\'bburl\']}/{$announcement[\'forumlink\']}">{$announcement[\'forumname\']}</a> {$numcomments}</span>
</td>
</tr>
<tr>
<td class="trow1">
<table border="0" cellpadding="{$theme[\'tablespace\']}" class="tfixed" style="width: 100%;">
<tr>
{$avatar}
<td class="trow1 scaleimages">
<p>
{$message}
</p>
{$post[\'attachments\']}
</td>
</tr>
<tr>
<td align="right" colspan="2" valign="bottom">
<span class="smalltext">
<a href="{$mybb->settings[\'bburl\']}/printthread.php?tid={$announcement[\'tid\']}"><img src="{$theme[\'imgdir\']}/printable.png" alt="{$lang->print_this_item}" title="{$lang->print_this_item}" /></a>{$senditem}
</span>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
270 => 
array (
'tid' => 456,
'title' => 'portal_announcement_avatar',
'template' => '<td class="trow1" style="text-align: center; vertical-align: top;" {$useravatar[\'width_height\']}><img src="{$useravatar[\'image\']}" alt="" {$useravatar[\'width_height\']} /></td>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
271 => 
array (
'tid' => 457,
'title' => 'portal_announcement_icon',
'template' => '<img src="{$icon[\'path\']}" alt="{$icon[\'name\']}" title="{$icon[\'name\']}" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
272 => 
array (
'tid' => 458,
'title' => 'portal_announcement_numcomments',
'template' => '- <a href="{$mybb->settings[\'bburl\']}/{$announcement[\'threadlink\']}">{$lang->replies}</a> ({$announcement[\'replies\']})',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
273 => 
array (
'tid' => 459,
'title' => 'portal_announcement_numcomments_no',
'template' => '- {$lang->no_replies}',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
274 => 
array (
'tid' => 460,
'title' => 'portal_announcement_send_item',
'template' => '&nbsp;<a href="{$mybb->settings[\'bburl\']}/sendthread.php?tid={$announcement[\'tid\']}"><img src="{$theme[\'imgdir\']}/send.png" alt="{$lang->send_to_friend}" title="{$lang->send_to_friend}" /></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
275 => 
array (
'tid' => 461,
'title' => 'portal_latestthreads',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->latest_threads}</strong></td>
</tr>
{$threadlist}
</table>
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
276 => 
array (
'tid' => 462,
'title' => 'portal_latestthreads_thread',
'template' => '<tr>
<td class="{$altbg}">
<strong><a href="{$mybb->settings[\'bburl\']}/{$thread[\'threadlink\']}">{$thread[\'subject\']}</a></strong>
<span class="smalltext"><br />
{$lang->forum} <a href="{$thread[\'forumlink\']}">{$thread[\'forumname\']}</a><br />
<a href="{$thread[\'lastpostlink\']}">{$lang->latest_threads_lastpost}</a> {$lastposterlink}<br />
{$lastpostdate}<br />
<strong>&raquo; </strong>{$lang->latest_threads_replies} {$thread[\'replies\']}<br />
<strong>&raquo; </strong>{$lang->latest_threads_views} {$thread[\'views\']}
</span>
</td>
</tr>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
277 => 
array (
'tid' => 463,
'title' => 'portal_pms',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong><a href="{$mybb->settings[\'bburl\']}/private.php">{$lang->private_messages}</a></strong></td>
</tr>
<tr>
<td class="trow1">
<span class="smalltext">{$lang->pms_received_new}<br /><br />
<strong>&raquo; </strong> <strong>{$messages[\'pms_unread\']}</strong> {$lang->pms_unread}<br />
<strong>&raquo; </strong> <strong>{$messages[\'pms_total\']}</strong> {$lang->pms_total}</span>
</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
278 => 
array (
'tid' => 464,
'title' => 'portal_search',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->search_forums}</strong></td>
</tr>
<tr>
<td class="trow1" align="center">
<form method="post" action="{$mybb->settings[\'bburl\']}/search.php">
<input type="hidden" name="action" value="do_search" />
<input type="hidden" name="postthread" value="1" />
<input type="hidden" name="forums" value="all" />
<input type="hidden" name="showresults" value="threads" />
<input type="text" class="textbox" name="keywords" value="" />
{$gobutton}
</form><br />
<span class="smalltext">
(<a href="{$mybb->settings[\'bburl\']}/search.php">{$lang->advanced_search}</a>)
</span>
</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
279 => 
array (
'tid' => 465,
'title' => 'portal_stats',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->forum_stats}</strong></td>
</tr>
<tr>
<td class="trow1">
<span class="smalltext">
<strong>&raquo; </strong>{$lang->num_members} {$stats[\'numusers\']}<br />
<strong>&raquo; </strong>{$lang->latest_member} {$newestmember}<br />
<strong>&raquo; </strong>{$lang->num_threads} {$stats[\'numthreads\']}<br />
<strong>&raquo; </strong>{$lang->num_posts} {$stats[\'numposts\']}
<br /><br /><a href="{$mybb->settings[\'bburl\']}/stats.php">{$lang->full_stats}</a>
</span>
</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
280 => 
array (
'tid' => 466,
'title' => 'portal_welcome',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->welcome}</strong></td>
</tr>
<tr>
<td class="trow1">
{$welcometext}
</td>
</tr>
</table><br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
281 => 
array (
'tid' => 467,
'title' => 'portal_welcome_guesttext',
'template' => '<span class="smalltext">{$lang->guest_welcome_registration}</span><br />
<br />
<form method="post" action="{$mybb->settings[\'bburl\']}/member.php"><input type="hidden" name="action" value="do_login" /><input type="hidden" name="url" value="{$portal_url}" />
{$username}<br />&nbsp;&nbsp;<input type="text" class="textbox" name="username" value="" /><br /><br />
{$lang->password}<br />&nbsp;&nbsp;<input type="password" class="textbox" name="password" value="" /><br /><br />
<label title="{$lang->remember_me_desc}"><input type="checkbox" class="checkbox" name="remember" value="yes" /> {$lang->remember_me}</label><br /><br />
<br /><input type="submit" class="button" name="loginsubmit" value="{$lang->login}" /></form>',
'sid' => -2,
'version' => '1609',
'status' => '',
'dateline' => 1466356529,
),
282 => 
array (
'tid' => 468,
'title' => 'portal_welcome_membertext',
'template' => '<span class="smalltext"><em>{$lang->member_welcome_lastvisit}</em> {$lastvisit}<br />
{$lang->since_then}<br />
<strong>&raquo;</strong> {$lang->new_announcements}<br />
<strong>&raquo;</strong> {$lang->new_threads}<br />
<strong>&raquo;</strong> {$lang->new_posts}<br /><br />
<a href="{$mybb->settings[\'bburl\']}/search.php?action=getnew">{$lang->view_new}</a><br /><a href="{$mybb->settings[\'bburl\']}/search.php?action=getdaily">{$lang->view_todays}</a>
</span>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
283 => 
array (
'tid' => 469,
'title' => 'portal_whosonline',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->online}</strong></td>
</tr>
<tr>
<td class="trow1">
<span class="smalltext">
{$lang->online_users}<br /><strong>&raquo;</strong> {$lang->online_counts}<br />{$onlinemembers}
</span>
</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
284 => 
array (
'tid' => 470,
'title' => 'portal_whosonline_memberbit',
'template' => '{$comma}<a href="{$mybb->settings[\'bburl\']}/{$user[\'profilelink\']}">{$user[\'username\']}</a>{$invisiblemark}',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
285 => 
array (
'tid' => 481,
'title' => 'post_captcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top" width="350"><strong>{$lang->image_verification}</strong>
<br/>
<span class="smalltext">{$lang->verification_note}</span>
</td>
<td class="trow1">
<script type="text/javascript">
<!--
lang.captcha_fetch_failure = "{$lang->captcha_fetch_failure}";
// -->
</script>
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/captcha.js?ver=1804"></script>
<table style="width: 300px; padding: 4px;">
<tr>
<td style="vertical-align: middle;"><img src="{$mybb->settings[\'bburl\']}/captcha.php?imagehash={$imagehash}" alt="{$lang->image_verification}" title="{$lang->image_verification}" id="captcha_img" /><br /><span style="color: red;" class="smalltext">{$lang->verification_subnote}</span>
</td>
</tr>
<tr>
<td>
<input type="text" class="textbox" name="imagestring" value="" id="imagestring" size="16" /><input type="hidden" name="imagehash" value="{$imagehash}" id="imagehash" />
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
document.write(\'<input type="button" class="button" name="refresh" value="{$lang->refresh}" onclick="return captcha.refresh();" \\/>\');
}
// -->
</script>
</td>
</tr>
</table>
</td>
</tr>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
286 => 
array (
'tid' => 482,
'title' => 'post_captcha_hidden',
'template' => '<input type="hidden" name="{$hash}" value="{$field[\'hash\']}" />
<input type="hidden" name="{$string}" value="{$field[\'string\']}" />',
'sid' => -2,
'version' => '1605',
'status' => '',
'dateline' => 1466356529,
),
287 => 
array (
'tid' => 483,
'title' => 'post_captcha_recaptcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top"><strong>{$lang->image_verification}</strong></td>
<td class="trow1">
<script type="text/javascript">
<!--
var RecaptchaOptions = {
theme: \'clean\'
};
// -->
</script>
<table style="width: 300px; padding: 4px;">
<tr>
<td><span class="smalltext">{$lang->verification_note}</span></td>
</tr>
<tr>
<td><script type="text/javascript" src="{$server}/challenge?k={$public_key}"></script></td>
</tr>
</table>
</td>
</tr>',
'sid' => -2,
'version' => '1605',
'status' => '',
'dateline' => 1466356529,
),
288 => 
array (
'tid' => 484,
'title' => 'post_captcha_nocaptcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top"><strong>{$lang->human_verification}</strong></td>
<td class="trow1">
<table style="width: 300px; padding: 4px;">
<tr>
<td><span class="smalltext">{$lang->verification_note_nocaptcha}</span></td>
</tr>
<tr>
<td><script type="text/javascript" src="{$server}"></script><div class="g-recaptcha" data-sitekey="{$public_key}"></div></td>
</tr>
</table>
</td>
</tr>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
289 => 
array (
'tid' => 485,
'title' => 'post_prefixselect_multiple',
'template' => '<select name="threadprefix[]" multiple="multiple" size="5">
<option value="any"{$any_selected}>{$lang->any_prefix}</option>
<option value="0"{$default_selected}>{$lang->no_prefix}</option>
{$prefixselect_prefix}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
290 => 
array (
'tid' => 486,
'title' => 'post_prefixselect_prefix',
'template' => '<option value="{$prefix[\'pid\']}"{$selected}>{$prefix[\'prefix\']}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
291 => 
array (
'tid' => 487,
'title' => 'post_prefixselect_single',
'template' => '<select name="threadprefix">
<option value="0"{$default_selected}>{$lang->no_prefix}</option>
{$prefixselect_prefix}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
292 => 
array (
'tid' => 490,
'title' => 'postbit',
'template' => '{$ignore_bit}
<a name="pid{$post[\'pid\']}" id="pid{$post[\'pid\']}"></a>
<div class="post {$unapproved_shade}" style="{$post_visibility}" id="post_{$post[\'pid\']}">
<div class="post_author">
{$post[\'useravatar\']}
<div class="author_information">
<strong><span class="largetext">{$post[\'profilelink\']}</span></strong> {$post[\'onlinestatus\']}<br />
<span class="smalltext">
{$post[\'usertitle\']}<br />
{$post[\'userstars\']}
{$post[\'groupimage\']}
</span>
</div>
<div class="author_statistics">
{$post[\'user_details\']}
</div>
</div>
<div class="post_content">
<div class="post_head">
{$post[\'posturl\']}
{$post[\'icon\']}
<span class="post_date">{$post[\'postdate\']} <span class="post_edit" id="edited_by_{$post[\'pid\']}">{$post[\'editedmsg\']}</span></span>
{$post[\'subject_extra\']}
</div>
<div class="post_body scaleimages" id="pid_{$post[\'pid\']}">
{$post[\'message\']}
</div>
{$post[\'attachments\']}
{$post[\'signature\']}
<div class="post_meta" id="post_meta_{$post[\'pid\']}">
{$post[\'iplogged\']}
</div>
</div>
<div class="post_controls">
<div class="postbit_buttons author_buttons float_left">
{$post[\'button_email\']}{$post[\'button_pm\']}{$post[\'button_www\']}{$post[\'button_find\']}{$post[\'button_rep\']}
</div>
<div class="postbit_buttons post_management_buttons float_right">
{$post[\'button_edit\']}{$post[\'button_quickdelete\']}{$post[\'button_quickrestore\']}{$post[\'button_quote\']}{$post[\'button_multiquote\']}{$post[\'button_report\']}{$post[\'button_warn\']}{$post[\'button_purgespammer\']}{$post[\'button_reply_pm\']}{$post[\'button_replyall_pm\']}{$post[\'button_forward_pm\']}{$post[\'button_delete_pm\']}
</div>
</div>
</div>',
'sid' => -2,
'version' => '1801',
'status' => '',
'dateline' => 1466356529,
),
293 => 
array (
'tid' => 491,
'title' => 'postbit_attachments',
'template' => '<br />
<br />
<fieldset>
<legend><strong>{$lang->postbit_attachments}</strong></legend>
{$post[\'attachedthumbs\']}
{$post[\'attachedimages\']}
{$post[\'attachmentlist\']}
</fieldset>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
294 => 
array (
'tid' => 492,
'title' => 'postbit_attachments_attachment',
'template' => '<br />{$attachment[\'icon\']}&nbsp;&nbsp;<a href="attachment.php?aid={$attachment[\'aid\']}" target="_blank" title="{$attachdate}">{$attachment[\'filename\']}</a> ({$lang->postbit_attachment_size} {$attachment[\'filesize\']} / {$lang->postbit_attachment_downloads} {$attachment[\'downloads\']})',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
295 => 
array (
'tid' => 493,
'title' => 'postbit_attachments_attachment_unapproved',
'template' => '<br /><strong>{$postbit_unapproved_attachments}</strong>',
'sid' => -2,
'version' => '1402',
'status' => '',
'dateline' => 1466356529,
),
296 => 
array (
'tid' => 494,
'title' => 'postbit_attachments_images',
'template' => '<span class="smalltext"><strong>{$lang->postbit_attachments_images}</strong></span><br />
{$post[\'imagelist\']}
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
297 => 
array (
'tid' => 495,
'title' => 'postbit_attachments_images_image',
'template' => '<img src="attachment.php?aid={$attachment[\'aid\']}" class="attachment" alt="" title="{$lang->postbit_attachment_filename} {$attachment[\'filename\']}&#13;{$lang->postbit_attachment_size} {$attachment[\'filesize\']}&#13;{$attachdate}" />&nbsp;&nbsp;&nbsp;',
'sid' => -2,
'version' => '1805',
'status' => '',
'dateline' => 1466356529,
),
298 => 
array (
'tid' => 496,
'title' => 'postbit_attachments_thumbnails',
'template' => '<span class="smalltext"><strong>{$lang->postbit_attachments_thumbnails}</strong></span><br />
{$post[\'thumblist\']}
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
299 => 
array (
'tid' => 497,
'title' => 'postbit_attachments_thumbnails_thumbnail',
'template' => '<a href="attachment.php?aid={$attachment[\'aid\']}" target="_blank"><img src="attachment.php?thumbnail={$attachment[\'aid\']}" class="attachment" alt="" title="{$lang->postbit_attachment_filename} {$attachment[\'filename\']}&#13;{$lang->postbit_attachment_size} {$attachment[\'filesize\']}&#13;{$attachdate}" /></a>&nbsp;&nbsp;&nbsp;',
'sid' => -2,
'version' => '1805',
'status' => '',
'dateline' => 1466356529,
),
300 => 
array (
'tid' => 498,
'title' => 'postbit_author_guest',
'template' => '&nbsp;',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
301 => 
array (
'tid' => 499,
'title' => 'postbit_author_user',
'template' => '
{$lang->postbit_posts} {$post[\'postnum\']}<br />
{$lang->postbit_threads} {$post[\'threadnum\']}<br />
{$lang->postbit_joined} {$post[\'userregdate\']}
{$post[\'replink\']}{$post[\'profilefield\']}{$post[\'warninglevel\']}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
302 => 
array (
'tid' => 500,
'title' => 'postbit_avatar',
'template' => '<div class="author_avatar"><a href="{$post[\'profilelink_plain\']}"><img src="{$useravatar[\'image\']}" alt="" {$useravatar[\'width_height\']} /></a></div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
303 => 
array (
'tid' => 501,
'title' => 'postbit_away',
'template' => '<a href="{$post[\'profilelink_plain\']}" title="{$lang->postbit_status_away}"><img src="{$theme[\'imgdir\']}/buddy_away.png" border="0" alt="{$lang->postbit_status_away}" class="buddy_status" /></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
304 => 
array (
'tid' => 502,
'title' => 'postbit_classic',
'template' => '{$ignore_bit}
<a name="pid{$post[\'pid\']}" id="pid{$post[\'pid\']}"></a>
<div class="post classic {$unapproved_shade}" style="{$post_visibility}" id="post_{$post[\'pid\']}">
<div class="post_author scaleimages">
{$post[\'useravatar\']}
<div class="author_information">
<strong><span class="largetext">{$post[\'profilelink\']}</span></strong> {$post[\'onlinestatus\']}<br />
<span class="smalltext">
{$post[\'usertitle\']}<br />
{$post[\'userstars\']}
{$post[\'groupimage\']}
</span>
</div>
<div class="author_statistics">
{$post[\'user_details\']}
</div>
</div>
<div class="post_content">
<div class="post_head">
{$post[\'posturl\']}
{$post[\'icon\']}
<span class="post_date">{$post[\'postdate\']} <span class="post_edit" id="edited_by_{$post[\'pid\']}">{$post[\'editedmsg\']}</span></span>
{$post[\'subject_extra\']}
</div>
<div class="post_body scaleimages" id="pid_{$post[\'pid\']}">
{$post[\'message\']}
</div>
{$post[\'attachments\']}
{$post[\'signature\']}
<div class="post_meta" id="post_meta_{$post[\'pid\']}">
{$post[\'iplogged\']}
</div>
</div>
<div class="post_controls">
<div class="postbit_buttons author_buttons float_left">
{$post[\'button_email\']}{$post[\'button_pm\']}{$post[\'button_www\']}{$post[\'button_find\']}{$post[\'button_rep\']}
</div>
<div class="postbit_buttons post_management_buttons float_right">
{$post[\'button_edit\']}{$post[\'button_quickdelete\']}{$post[\'button_quickrestore\']}{$post[\'button_quote\']}{$post[\'button_multiquote\']}{$post[\'button_report\']}{$post[\'button_warn\']}{$post[\'button_purgespammer\']}{$post[\'button_reply_pm\']}{$post[\'button_replyall_pm\']}{$post[\'button_forward_pm\']}{$post[\'button_delete_pm\']}
</div>
</div>
</div>',
'sid' => -2,
'version' => '1801',
'status' => '',
'dateline' => 1466356529,
),
305 => 
array (
'tid' => 504,
'title' => 'postbit_edit',
'template' => '<a href="editpost.php?pid={$post[\'pid\']}" id="edit_post_{$post[\'pid\']}" title="{$lang->postbit_edit}" class="postbit_edit"><span>{$lang->postbit_button_edit}</span></a>
<div id="edit_post_{$post[\'pid\']}_popup" class="popup_menu" style="display: none;"><div class="popup_item_container"><a href="javascript:;" class="popup_item quick_edit_button" id="quick_edit_post_{$post[\'pid\']}">{$lang->postbit_quick_edit}</a></div><div class="popup_item_container"><a href="editpost.php?pid={$post[\'pid\']}" class="popup_item">{$lang->postbit_full_edit}</a></div></div>
<script type="text/javascript">
// <!--
if(use_xmlhttprequest == "1")
{
$("#edit_post_{$post[\'pid\']}").popupMenu();
}
// -->
</script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
306 => 
array (
'tid' => 505,
'title' => 'postbit_editedby',
'template' => '<span class="edited_post">({$post[\'editnote\']} {$post[\'editedprofilelink\']}.{$editreason})</span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
307 => 
array (
'tid' => 506,
'title' => 'postbit_editedby_editreason',
'template' => ' <em>{$lang->postbit_editreason}: {$post[\'editreason\']}</em>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
308 => 
array (
'tid' => 507,
'title' => 'postbit_email',
'template' => '<a href="member.php?action=emailuser&amp;uid={$post[\'uid\']}" title="{$lang->postbit_email}" class="postbit_email"><span>{$lang->postbit_button_email}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
309 => 
array (
'tid' => 508,
'title' => 'postbit_find',
'template' => '<a href="search.php?action=finduser&amp;uid={$post[\'uid\']}" title="{$lang->postbit_find}" class="postbit_find"><span>{$lang->postbit_button_find}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
310 => 
array (
'tid' => 510,
'title' => 'postbit_purgespammer',
'template' => '<a href="moderation.php?action=purgespammer&amp;uid={$post[\'uid\']}" title="{$lang->postbit_purgespammer}" class="postbit_purgespammer"><span>{$lang->postbit_button_purgespammer}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
311 => 
array (
'tid' => 511,
'title' => 'postbit_gotopost',
'template' => ' <a href="{$url}" class="quick_jump"></a>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
312 => 
array (
'tid' => 512,
'title' => 'postbit_groupimage',
'template' => '<img src="{$usergroup[\'image\']}" alt="{$usergroup[\'title\']}" title="{$usergroup[\'title\']}" />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
313 => 
array (
'tid' => 513,
'title' => 'postbit_icon',
'template' => '<img src="{$icon[\'path\']}" alt="{$icon[\'name\']}" title="{$icon[\'name\']}" style="vertical-align: middle;" />&nbsp;',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
314 => 
array (
'tid' => 514,
'title' => 'postbit_ignored',
'template' => '<div class="ignored_post" id="ignored_post_{$post[\'pid\']}">
<div class="ignored_post_author"><strong><span class="largetext">{$post[\'profilelink\']}</span></strong></div>
<div class="ignored_post_message">
<div class="show_ignored_post float_right" style="vertical-align: top;" id="show_ignored_link_{$post[\'pid\']}"><a href="#" onclick="Thread.showIgnoredPost({$post[\'pid\']}); return false;" class="button small_button"><span>{$lang->postbit_show_ignored_post}</span></a></div>
<script type="text/javascript">
<!--
$(\'#show_ignored_link_{$post[\'pid\']}\').show();
// -->
</script>
{$ignored_message}
</div>
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
315 => 
array (
'tid' => 515,
'title' => 'postbit_inlinecheck',
'template' => '<input type="checkbox" class="checkbox" name="inlinemod_{$post[\'pid\']}" id="inlinemod_{$post[\'pid\']}" value="1" style="vertical-align: middle; margin: -2px 0 0 5px;" {$inlinecheck}  />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
316 => 
array (
'tid' => 516,
'title' => 'postbit_iplogged_hiden',
'template' => '{$lang->postbit_ipaddress} <a href="moderation.php?action={$action}&amp;{$idtype}={$post[$idtype]}">{$lang->postbit_ipaddress_logged}</a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
317 => 
array (
'tid' => 517,
'title' => 'postbit_iplogged_show',
'template' => '{$lang->postbit_ipaddress} {$ipaddress}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
318 => 
array (
'tid' => 518,
'title' => 'postbit_multiquote',
'template' => '<a href="javascript:Thread.multiQuote({$post[\'pid\']});" style="display: none;" id="multiquote_link_{$post[\'pid\']}" title="{$lang->postbit_multiquote}" class="postbit_multiquote"><span id="multiquote_{$post[\'pid\']}">{$lang->postbit_button_multiquote}</span></a>
<script type="text/javascript">
//<!--
$(\'#multiquote_link_{$post[\'pid\']}\').css("display", "");
// -->
</script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
319 => 
array (
'tid' => 519,
'title' => 'postbit_offline',
'template' => '<img src="{$theme[\'imgdir\']}/buddy_offline.png" title="{$lang->postbit_status_offline}" alt="{$lang->postbit_status_offline}" class="buddy_status" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
320 => 
array (
'tid' => 520,
'title' => 'postbit_online',
'template' => '<a href="online.php" title="{$lang->postbit_status_online}"><img src="{$theme[\'imgdir\']}/buddy_online.png" border="0" alt="{$lang->postbit_status_online}" class="buddy_status" /></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
321 => 
array (
'tid' => 521,
'title' => 'postbit_pm',
'template' => '<a href="private.php?action=send&amp;uid={$post[\'uid\']}" title="{$lang->postbit_pm}" class="postbit_pm"><span>{$lang->postbit_button_pm}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
322 => 
array (
'tid' => 522,
'title' => 'postbit_posturl',
'template' => '<div class="float_right" style="vertical-align: top">
<strong><a href="{$post[\'postlink\']}#pid{$post[\'pid\']}" title="{$post[\'subject_title\']}">#{$post_number}</a></strong>
{$post[\'inlinecheck\']}
</div>',
'sid' => -2,
'version' => '1801',
'status' => '',
'dateline' => 1466356529,
),
323 => 
array (
'tid' => 523,
'title' => 'postbit_profilefield',
'template' => '<br />{$post[\'fieldname\']}: {$post[\'fieldvalue\']}',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
324 => 
array (
'tid' => 524,
'title' => 'postbit_profilefield_multiselect',
'template' => '<ul style="margin: 0; padding-left: 15px;">
{$post[\'fieldvalue_option\']}
</ul>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
325 => 
array (
'tid' => 525,
'title' => 'postbit_profilefield_multiselect_value',
'template' => '<li style="margin-left: 0;">{$val}</li>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
326 => 
array (
'tid' => 526,
'title' => 'postbit_quickdelete',
'template' => '<a href="editpost.php?pid={$post[\'pid\']}" onclick="Thread.deletePost({$post[\'pid\']}); return false;" style="display: none;" id="quick_delete_{$post[\'pid\']}" title="{$postbit_qdelete}" class="postbit_qdelete"><span>{$lang->postbit_button_qdelete}</span></a>
<script type="text/javascript">
// <!--
$(\'#quick_delete_{$post[\'pid\']}\').css(\'display\', \'{$display}\');
// -->
</script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
327 => 
array (
'tid' => 527,
'title' => 'postbit_quickrestore',
'template' => '<a href="editpost.php?pid={$post[\'pid\']}" onclick="Thread.restorePost({$post[\'pid\']}); return false;" style="display: none;" id="quick_restore_{$post[\'pid\']}" title="{$postbit_qrestore}" class="postbit_qrestore"><span>{$lang->postbit_button_qrestore}</span></a>
<script type="text/javascript">
// <!--
$(\'#quick_restore_{$post[\'pid\']}\').css(\'display\', \'{$display}\');
// -->
</script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
328 => 
array (
'tid' => 528,
'title' => 'postbit_quote',
'template' => '<a href="newreply.php?tid={$tid}&amp;replyto={$post[\'pid\']}" title="{$lang->postbit_quote}" class="postbit_quote"><span>{$lang->postbit_button_quote}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
329 => 
array (
'tid' => 529,
'title' => 'postbit_rep_button',
'template' => '<a href="javascript:MyBB.reputation({$post[\'uid\']},{$post[\'pid\']});" title="{$lang->postbit_reputation_add}" class="postbit_reputation_add"><span>{$lang->postbit_button_reputation_add}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
330 => 
array (
'tid' => 532,
'title' => 'postbit_report',
'template' => '<a href="javascript:Report.reportPost({$post[\'pid\']});" title="{$lang->postbit_report}" class="postbit_report"><span>{$lang->postbit_button_report}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
331 => 
array (
'tid' => 533,
'title' => 'postbit_reputation',
'template' => '<br />{$lang->postbit_reputation} {$post[\'userreputation\']}',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
332 => 
array (
'tid' => 534,
'title' => 'postbit_reputation_formatted',
'template' => '<strong class="{$reputation_class}">{$reputation}</strong>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
333 => 
array (
'tid' => 535,
'title' => 'postbit_reputation_formatted_link',
'template' => '<a href="reputation.php?uid={$uid}"><strong class="{$reputation_class}">{$reputation}</strong></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
334 => 
array (
'tid' => 536,
'title' => 'postbit_signature',
'template' => '<div class="signature scaleimages">
{$post[\'signature\']}
</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
335 => 
array (
'tid' => 537,
'title' => 'postbit_userstar',
'template' => '<img src="{$post[\'starimage\']}" border="0" alt="*" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
336 => 
array (
'tid' => 538,
'title' => 'postbit_warn',
'template' => '<a href="warnings.php?action=warn&amp;uid={$post[\'uid\']}&amp;pid={$post[\'pid\']}" title="{$lang->postbit_warn}" class="postbit_warn"><span>{$lang->postbit_button_warn}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
337 => 
array (
'tid' => 539,
'title' => 'postbit_warninglevel',
'template' => '<br />{$lang->postbit_warning_level} <a href="{$warning_link}">{$warning_level}</a>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
338 => 
array (
'tid' => 540,
'title' => 'postbit_warninglevel_formatted',
'template' => '<span class="{$warning_class}">{$level}%</span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
339 => 
array (
'tid' => 541,
'title' => 'postbit_www',
'template' => '<a href="{$post[\'website\']}" target="_blank" title="{$lang->postbit_website}" class="postbit_website"><span>{$lang->postbit_button_website}</span></a>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
340 => 
array (
'tid' => 542,
'title' => 'posticons',
'template' => '<tr>
<td class="trow1" style="vertical-align: top"><strong>{$lang->post_icon}</strong><span class="smalltext"><label class="posticons_label"><input type="radio" class="radio" name="icon" value="-1"{$no_icons_checked} />{$lang->no_post_icon}</label></span></td>
<td class="trow1" valign="top">{$iconlist}</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
341 => 
array (
'tid' => 543,
'title' => 'posticons_icon',
'template' => '<label class="posticons_label"><input type="radio" name="icon" value="{$dbicon[\'iid\']}"{$checked} /> <img src="{$dbicon[\'path\']}" alt="{$dbicon[\'name\']}" title="{$dbicon[\'name\']}" /></label>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
342 => 
array (
'tid' => 551,
'title' => 'private_advanced_search_folders',
'template' => '<select multiple="multiple" name="folder[]" id="folder">
<option selected="selected">{$lang->all_folders}</option>
{$foldersearch_folder}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
343 => 
array (
'tid' => 553,
'title' => 'private_archive_csv',
'template' => '{$lang->export_date_sent},{$lang->export_folder},{$lang->export_subject},{$lang->export_to},{$lang->export_from},{$lang->export_message}
{$pmsdownload}',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
344 => 
array (
'tid' => 554,
'title' => 'private_archive_csv_message',
'template' => '{$senddate},"{$foldername}","{$message[\'subject\']}","{$message[\'tousername\']}","{$message[\'fromusername\']}","{$message[\'message\']}"
',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
345 => 
array (
'tid' => 557,
'title' => 'private_archive_html',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->private_messages_for}</title>
<style type="text/css">{$css}
* {text-align: left}</style>
</head>
<body>
<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="1"><span class="largetext"><strong>{$lang->private_messages_for}</strong></span></td>
</tr>
{$pmsdownload}
</table>
<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="trow1" colspan="1">{$lang->exported_date}<br /><a href="{$mybb->settings[\'bburl\']}">{$mybb->settings[\'bbname\']}</a></td>
</tr>
</table>
</body>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
346 => 
array (
'tid' => 558,
'title' => 'private_archive_html_folderhead',
'template' => '</table>
<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="tcat" colspan="2"><strong>{$lang->folder} {$foldername}</strong></td>
</tr>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
347 => 
array (
'tid' => 559,
'title' => 'private_archive_html_message',
'template' => '<tr>
<td class="trow1"><strong>{$lang->subject} {$message[\'subject\']}</strong><br /><em>{$lang->to} {$message[\'tousername\']}<br />{$lang->from} {$message[\'fromusername\']}<br />{$lang->sent} {$senddate}</em></td>
</tr>
<tr>
<td class="trow2">{$message[\'message\']}</td>
</tr>
<tr>
<td class="tcat" height="3"> </td>
</tr>',
'sid' => -2,
'version' => '1801',
'status' => '',
'dateline' => 1466356529,
),
348 => 
array (
'tid' => 560,
'title' => 'private_archive_txt',
'template' => '{$lang->private_messages_for}
({$lang->exported_date})

{$pmsdownload}',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
349 => 
array (
'tid' => 561,
'title' => 'private_archive_txt_folderhead',
'template' => '#######################################################################
{$lang->folder} {$foldername}
#######################################################################
',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
350 => 
array (
'tid' => 562,
'title' => 'private_archive_txt_message',
'template' => '{$lang->subject} {$message[\'subject\']}
{$tofrom} {$tofromusername}
{$lang->sent} {$senddate}
------------------------------------------------------------------------
{$message[\'message\']}
------------------------------------------------------------------------

',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
351 => 
array (
'tid' => 570,
'title' => 'private_jump_folders',
'template' => '<select name="jumpto">
{$folderjump_folder}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
352 => 
array (
'tid' => 571,
'title' => 'private_jump_folders_folder',
'template' => '<option value="{$folder_id}"{$sel}>{$folder_name}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
353 => 
array (
'tid' => 576,
'title' => 'private_messagebit_sep',
'template' => '<tr>
<td class="tcat" align="center" colspan="6" height="1"> </td>
</tr>
',
'sid' => -2,
'version' => '1801',
'status' => '',
'dateline' => 1466356529,
),
354 => 
array (
'tid' => 577,
'title' => 'private_move',
'template' => '<input type="hidden" value="{$mybb->input[\'fid\']}" name="fromfid" />
<select name="fid">
{$folderoplist_folder}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
355 => 
array (
'tid' => 582,
'title' => 'private_orderarrow',
'template' => '<span class="smalltext">[<a href="private.php?fid={$folder}&amp;sortby={$sortby}&amp;order={$oppsortnext}">{$oppsort}</a>]</span>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
356 => 
array (
'tid' => 589,
'title' => 'private_search_messagebit',
'template' => '<tr>
<td align="center" class="trow1" width="1%"><img src="{$theme[\'imgdir\']}/{$msgfolder}" alt="{$msgalt}" title="{$msgalt}" /></td>
<td align="center" class="trow2" width="1%">{$icon}</td>
<td class="trow1" width="35%">{$msgprefix}<a href="private.php?action=read&amp;pmid={$message[\'pmid\']}">{$message[\'subject\']}</a>{$msgsuffix}{$denyreceipt}
<br />{$message[\'message\']}</td>
<td align="center" class="trow2">{$tofromusername}</td>
<td align="center" class="trow1"><a href="private.php?fid={$message[\'folder\']}">{$foldername}</a></td>
<td class="trow2" align="center" style="white-space: nowrap"><span class="smalltext">{$senddate}</span></td>
<td class="trow1" align="center"><input type="checkbox" class="checkbox" name="check[{$message[\'pmid\']}]" value="1" /></td>
</tr>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
357 => 
array (
'tid' => 591,
'title' => 'private_search_results_nomessages',
'template' => '<tr>
<td colspan="7" class="trow1">{$lang->nomessages}</td>
</tr>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
358 => 
array (
'tid' => 592,
'title' => 'private_send',
'template' => '<html>
<head>
<title>{$lang->compose_pm}</title>
{$headerinclude}
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/usercp.js?ver=1804"></script>
</head>
<body>
{$header}
<form action="private.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table width="100%" border="0" align="center">
<tr>
{$usercpnav}
<td valign="top">
{$preview}
{$send_errors}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->compose_pm}</strong></td>
</tr>
<tr>
<td class="trow1" valign="top" width="200"><strong>{$lang->compose_to}</strong>
<script type="text/javascript">
<!--
document.write(\'(<a href="#" onclick="showBcc(); return false;" title="{$lang->compose_bcc_show_title}">{$lang->compose_bcc_show}<\\/a>)\');
// -->
</script>
<br /><span class="smalltext">{$lang->separate_names}{$buddy_select_to}</span></td>
<td class="trow1" valign="top"><textarea name="to" id="to" rows="2" cols="38" tabindex="1" style="width: 450px;">{$to}</textarea>{$max_recipients}</td>
</tr>
<tr id="bcc_area">
<td class="trow2" valign="top"><strong>{$lang->compose_bcc}</strong>{$buddy_select_bcc}</td>
<td class="trow2"><textarea name="bcc" id="bcc" rows="2" cols="38" tabindex="1" style="width: 450px;">{$bcc}</textarea></td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->compose_subject}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="subject" size="40" maxlength="85" value="{$subject}" tabindex="3" /></td>
</tr>
{$posticons}
<tr>
<td class="trow2" valign="top"><strong>{$lang->compose_message}</strong>{$smilieinserter}</td>
<td class="trow2">
<textarea name="message" id="message" rows="20" cols="70" tabindex="4">{$message}</textarea>
{$codebuttons}
</td>
</tr>
<tr>
<td class="trow1" valign="top"><strong>{$lang->compose_options}</strong></td>
<td class="trow1"><span class="smalltext">
<label><input type="checkbox" class="checkbox" name="options[signature]" value="1" tabindex="5" {$optionschecked[\'signature\']} />{$lang->options_sig}</label><br />
<label><input type="checkbox" class="checkbox" name="options[disablesmilies]" value="1" tabindex="6" {$optionschecked[\'disablesmilies\']} />{$lang->options_disable_smilies}</label><br />
<label><input type="checkbox" class="checkbox" name="options[savecopy]" value="1" tabindex="7" {$optionschecked[\'savecopy\']} />{$lang->options_save_copy}</label>
{$private_send_tracking}
</span></td>
</tr>
</table>
<br />
<input type="hidden" name="action" value="do_send" />
<input type="hidden" name="pmid" value="{$pmid}" />
<input type="hidden" name="do" value="{$do}" />
<div style="text-align: center;">
<input type="submit" class="button" name="submit" value="{$lang->send_message}" tabindex="9" accesskey="s" />
<input type="submit" class="button" name="saveasdraft" value="{$lang->save_draft}" tabindex="10" />
<input type="submit" class="button" name="preview" value="{$lang->preview}" tabindex="11" />
</div>
</td>
</tr>
</table>
</form>
{$footer}
<script type="text/javascript">
<!--
if($("#bcc_area").length > 0 && $("#bcc").val() == "")
{
$("#bcc_area").css("display", "none");
}

function showBcc()
{
if($("#bcc_area").css("display") == \'none\')
{
$("#bcc_area").css("display", "");
}
else
{
$("#bcc_area").css("display", "none");
}
}
// -->
</script>
{$autocompletejs}
<script type="text/javascript">
$(".author_avatar img").error(function () {
$(this).unbind("error").closest(\'.author_avatar\').remove();
});
</script>
</body>
</html>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
359 => 
array (
'tid' => 593,
'title' => 'private_send_autocomplete',
'template' => '<link rel="stylesheet" href="{$mybb->asset_url}/jscripts/select2/select2.css?ver=1807">
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/select2/select2.min.js?ver=1806"></script>
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
MyBB.select2();
$("#to").select2({
placeholder: "{$lang->search_user}",
minimumInputLength: 2,
maximumSelectionSize: {$mybb->usergroup[\'maxpmrecipients\']},
multiple: true,
ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
url: "xmlhttp.php?action=get_users",
dataType: \'json\',
data: function (term, page) {
return {
query: term, // search term
};
},
results: function (data, page) { // parse the results into the format expected by Select2.
// since we are using custom formatting functions we do not need to alter remote JSON data
return {results: data};
}
},
initSelection: function(element, callback) {
var query = $(element).val();
if (query !== "") {
var newqueries = [];
exp_queries = query.split(",");
$.each(exp_queries, function(index, value ){
if(value.replace(/\\s/g, \'\') != "")
{
var newquery = {
id: value.replace(/,\\s?/g, ", "),
text: value.replace(/,\\s?/g, ", ")
};
newqueries.push(newquery);
}
});
callback(newqueries);
}
}
});

$("#bcc").select2({
placeholder: "{$lang->search_user}",
minimumInputLength: 2,
maximumSelectionSize: {$mybb->usergroup[\'maxpmrecipients\']},
multiple: true,
ajax: { // instead of writing the function to execute the request we use Select2\'s convenient helper
url: "xmlhttp.php?action=get_users",
dataType: \'json\',
data: function (term, page) {
return {
query: term, // search term
};
},
results: function (data, page) { // parse the results into the format expected by Select2.
// since we are using custom formatting functions we do not need to alter remote JSON data
return {results: data};
}
},
initSelection: function(element, callback) {
var query = $(element).val();
if (query !== "") {
var newqueries = [];
exp_queries = query.split(",");
$.each(exp_queries, function(index, value ){
if(value.replace(/\\s/g, \'\') != "")
{
var newquery = {
id: value.replace(/,\\s?/g, ", "),
text: value.replace(/,\\s?/g, ", ")
};
newqueries.push(newquery);
}
});
callback(newqueries);
}
}
});
}
// -->
</script>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
360 => 
array (
'tid' => 594,
'title' => 'private_send_buddyselect',
'template' => '<script type="text/javascript"><!--
document.write(\'<br /><span class="smalltext"><a href="#" onclick="UserCP.openBuddySelect(\\\'{$buddy_select}\\\');"><img src="{$theme[\'imgdir\']}/buddies.png" alt="" style="vertical-align: middle;" alt="" title="{$lang->select_from_buddies}" /> {$lang->select_from_buddies}</a></span>\');
// --></script>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
361 => 
array (
'tid' => 602,
'title' => 'report',
'template' => '<div class="modal">
<div style="overflow-y: auto; max-height: 400px;" class="modal_{$id}">
<form action="report.php" method="post" class="reportData_{$id}" onsubmit="javascript: return Report.submitReport({$id});">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_report" />
<input type="hidden" name="type" value="{$report_type}" />
<input type="hidden" name="pid" value="{$id}" />
<input type="hidden" name="no_modal" value="1" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$report_title}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2">{$lang->report_to_mod}</td>
</tr>
{$report_reasons}
</table>
</form>

<script type="text/javascript">
<!--
if($("#report_reason option:selected").val() != \'other\')
{
$("#reason").hide();
}

$("#report_reason").change(function()
{
if($(this).find("option:selected").val() == \'other\')
{
$("#reason").show();
return;
}

$("#reason").hide();
});
// -->
</script>
</div>
</div>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
362 => 
array (
'tid' => 603,
'title' => 'report_duplicate',
'template' => '<tr>
<td class="trow1">{$lang->error_report_duplicate}</td>
</tr>
<tr>
<td class="tfoot"><input type="submit" class="button" value="{$report_title}" /></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
363 => 
array (
'tid' => 604,
'title' => 'report_error',
'template' => '<tr>
<td colspan="2" class="trow1">{$error}</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
364 => 
array (
'tid' => 605,
'title' => 'report_error_nomodal',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$report_title}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2">{$lang->report_to_mod}</td>
</tr>
<tr>
<td colspan="2" class="trow1">{$error}</td>
</tr>
</table>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
365 => 
array (
'tid' => 606,
'title' => 'report_reasons',
'template' => '<tr>
<td class="trow1" align="left" style="width: 25%"><span class="smalltext"><strong>{$lang->report_reason}</strong></span></td>
<td class="trow1" align="left">
<select name="reason" id="report_reason">
<option value="rules">{$lang->report_reason_rules}</option>
<option value="bad">{$lang->report_reason_bad}</option>
<option value="spam">{$lang->report_reason_spam}</option>
<option value="wrong">{$lang->report_reason_wrong}</option>
<option value="other">{$lang->report_reason_other}</option>
</select>
</td>
</tr>
<tr id="reason">
<td class="trow2">&nbsp;</td>
<td class="trow2" align="left">
<div>{$lang->report_reason_other_description}</div>
<input type="text" class="textbox" name="comment" size="40" maxlength="250" />
</td>
</tr>
<tr>
<td colspan="2" class="tfoot"><input type="submit" class="button" value="{$report_title}" /></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
366 => 
array (
'tid' => 607,
'title' => 'report_thanks',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$report_title}</strong></td>
</tr>
<tr>
<td class="tcat">{$lang->report_to_mod}</td>
</tr>
<tr>
<td class="trow1">{$lang->success_report_voted}</td>
</tr>
</table>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
367 => 
array (
'tid' => 616,
'title' => 'reputation_added',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="trow1" style="padding: 20px">
<strong>{$lang->vote_added}</strong><br /><br />
<blockquote>{$lang->vote_added_message}</blockquote>
</td>
</tr>
</table>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
368 => 
array (
'tid' => 620,
'title' => 'reputation_vote',
'template' => '<tr>
<td class="trow1 {$status_class}" id="rid{$reputation_vote[\'rid\']}">
{$report_link}{$delete_link}
{$reputation_vote[\'username\']} <span class="smalltext">{$reputation_vote[\'user_reputation\']} - {$last_updated}<br />{$postrep_given}</span>
<br />
<strong class="{$vote_type_class}">{$vote_type} {$vote_reputation}:</strong> {$reputation_vote[\'comments\']}
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
369 => 
array (
'tid' => 646,
'title' => 'showteam',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->forum_team}</title>
{$headerinclude}
</head>
<body>
{$header}
{$grouplist}
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
370 => 
array (
'tid' => 647,
'title' => 'showteam_moderators',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="5"><strong>{$lang->moderators}</strong></td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->username}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->mod_forums}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->lastvisit}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->email}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->pm}</strong></span></td>
</tr>
{$modrows}
</table>
<br/>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
371 => 
array (
'tid' => 648,
'title' => 'showteam_moderators_forum',
'template' => '<a href="{$forum_url}">{$forum[\'name\']}</a><br/>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
372 => 
array (
'tid' => 649,
'title' => 'showteam_moderators_mod',
'template' => '<tr>
<td width="45%" class="{$bgcolor}"><img src="{$theme[\'imgdir\']}/buddy_{$status}.png" title="{$lang->$status}" alt="{$lang->$status}" style="vertical-align: middle;" /><a href="{$user[\'profilelink\']}"><strong>{$user[\'username\']}</strong></a></td>
<td width="30%" class="{$bgcolor}"><span class="smalltext">{$forumslist}</span></td>
<td width="15%" class="{$bgcolor}"><div class="postbit_buttons">{$user[\'lastvisit\']}</div></td>
<td width="5%" class="{$bgcolor}"><div class="postbit_buttons">{$emailcode}</div></td>
<td width="5%" class="{$bgcolor}"><div class="postbit_buttons">{$pmcode}</div></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
373 => 
array (
'tid' => 650,
'title' => 'showteam_usergroup',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="4"><strong>{$usergroup[\'title\']}</strong></td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->username}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->lastvisit}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->email}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->pm}</strong></span></td>
</tr>
{$usergrouprows}
</table>
<br />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
374 => 
array (
'tid' => 651,
'title' => 'showteam_usergroup_user',
'template' => '<tr>
<td width="75%" class="{$bgcolor}"><img src="{$theme[\'imgdir\']}/buddy_{$status}.png" title="{$lang->$status}" alt="{$lang->$status}" style="vertical-align: middle;" /><a href="{$user[\'profilelink\']}"><strong>{$user[\'username\']}</strong></a></td>
<td width="15%" class="{$bgcolor}">{$user[\'lastvisit\']}</td>
<td width="5%" class="{$bgcolor}"><div class="postbit_buttons">{$emailcode}</div></td>
<td width="5%" class="{$bgcolor}"><div class="postbit_buttons">{$pmcode}</div></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
375 => 
array (
'tid' => 653,
'title' => 'showthread_add_poll',
'template' => '<li class="poll"><a href="polls.php?action=newpoll&amp;tid={$tid}">{$lang->add_poll_to_thread}</a></li>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
376 => 
array (
'tid' => 654,
'title' => 'showthread_classic_header',
'template' => '<tr>
<td class="tcat" width="15%"><span class="smalltext"><strong>{$lang->author}</strong></span></td>
<td class="tcat"><span class="smalltext"><strong>{$lang->message}</strong></span></td>
</tr>
',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
377 => 
array (
'tid' => 692,
'title' => 'showthread_send_thread',
'template' => '<li class="sendthread"><a href="sendthread.php?tid={$tid}">{$lang->send_thread}</a></li>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
378 => 
array (
'tid' => 699,
'title' => 'showthread_threadnotes_viewnotes',
'template' => '[<a href="javascript:Thread.viewNotes({$thread[\'tid\']});">{$lang->view_all_notes}</a>]',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
379 => 
array (
'tid' => 710,
'title' => 'usercp_addsubscription_thread',
'template' => '<html>
<head>
<title>{$lang->subscribe_to_thread}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="usercp2.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_addsubscription" />
<input type="hidden" name="tid" value="{$thread[\'tid\']}" />
<table width="100%" border="0" align="center">
<tr>
{$usercpnav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->subscribe_to_thread}</strong></td>
</tr>
<tr>
<td class="trow1" valign="top" width="200"><strong>{$lang->notification_method}</strong></td>
<td class="trow1" valign="top">
<table width="100%" border="0" align="center">
<tr>
<td width="1"><input type="radio" name="notification" id="notification_none" value="0" {$notification_none_checked} /></td>
<td><strong><label for="notification_none">{$lang->no_notification}</label></strong><br /><span class="smalltext">{$lang->no_notification_desc}</span></td>
</tr>
<tr>
<td width="1"><input type="radio" name="notification" id="notification_email" value="1" {$notification_email_checked} /></td>
<td><strong><label for="notification_email">{$lang->email_notification}</label></strong><br /><span class="smalltext">{$lang->email_notification_desc}</span></td>
</tr>
<tr>
<td width="1"><input type="radio" name="notification" id="notification_pm" value="2" {$notification_pm_checked} /></td>
<td><strong><label for="notification_pm">{$lang->pm_notification}</label></strong><br /><span class="smalltext">{$lang->pm_notification_desc}</span></td>
</tr>
</table>
</td>
</tr>
</table>
<br />
<div style="text-align: center;">
<input type="submit" class="button" name="submit" value="{$lang->do_subscribe}" tabindex="3" accesskey="s" />
</div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
380 => 
array (
'tid' => 721,
'title' => 'usercp_currentavatar',
'template' => '<td class="trow1" valign="top" align="center" width="1"><div class="usercp_container"><img src="{$useravatar[\'image\']}" alt="{$avatar_username}" title="{$avatar_username}" {$useravatar[\'width_height\']} /></div></td>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
381 => 
array (
'tid' => 736,
'title' => 'usercp_editsig',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->edit_sig}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="usercp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table width="100%" border="0" align="center">
<tr>
{$usercpnav}
<td valign="top">
{$error}
{$signature}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->edit_sig}</strong></td>
</tr>
<tr>
<td class="trow1" valign="top" width="20%"><span class="smalltext">{$lang->edit_sig_note}</span>
{$smilieinserter}</td>
<td class="trow1" width="80%">
<textarea rows="15" cols="70" id="signature" name="signature">{$sig}</textarea>
{$codebuttons}
</td>
</tr>
<tr>
<td class="trow2">
<span class="smalltext">{$lang->edit_sig_note2}</span>
</td>
<td class="trow2">
<span class="smalltext">
<label><input type="radio" class="radio" name="updateposts" value="enable" />&nbsp;{$lang->enable_sig_posts}</label><br />
<label><input type="radio" class="radio" name="updateposts" value="disable" />&nbsp;{$lang->disable_sig_posts}</label><br />
<label><input type="radio" class="radio" name="updateposts" value="0" checked="checked" />&nbsp;{$lang->leave_sig_settings}</label></span>
</td>
</tr>
</table>
<br />
<div align="center">
<input type="hidden" name="action" value="do_editsig" />
<input type="submit" class="button" name="submit" value="{$lang->update_sig}" />
<input type="submit" class="button" name="preview" value="{$lang->preview}" />
</div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
382 => 
array (
'tid' => 737,
'title' => 'usercp_editsig_current',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<tr>
<td class="thead"><strong>{$lang->current_sig}</strong></td>
</tr>
<tr>
<td class="trow1 scaleimages">{$sigpreview}</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
383 => 
array (
'tid' => 738,
'title' => 'usercp_editsig_preview',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed">
<tr>
<td class="thead"><strong>{$lang->sig_preview}</strong></td>
</tr>
<tr>
<td class="trow1 scaleimages">{$sigpreview}</td>
</tr>
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
384 => 
array (
'tid' => 739,
'title' => 'usercp_editsig_suspended',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->edit_sig}</title>
{$headerinclude}
</head>
<body>
{$header}
<table width="100%" border="0" align="center">
<tr>
{$usercpnav}
<td valign="top">
<div class="error">
<p><em>{$lang->edit_sig_error_title}</em></p>
<ul>
<li>{$lang->edit_sig_no_permission}</li>
</ul>
</div>
<br />
{$signature}
</td>
</tr>
</table>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
385 => 
array (
'tid' => 744,
'title' => 'usercp_latest_subscribed',
'template' => '<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="6"><div class="float_right"><a href="usercp.php?action=subscriptions"><strong>{$lang->view_all_subscriptions}</strong></a></div><strong>{$lang->new_thread_subscriptions}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="3"><span class="smalltext"><strong>{$lang->thread} / {$lang->author}</strong></span></td>
<td class="tcat" align="center" width="75"><span class="smalltext"><strong>{$lang->replies}</strong></span></td>
<td class="tcat" align="center" width="75"><span class="smalltext"><strong>{$lang->views}</strong></span></td>
<td class="tcat" align="center" width="200"><span class="smalltext"><strong>{$lang->lastpost}</strong></span></td>
</tr>
{$latest_subscribed_threads}
</table>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
386 => 
array (
'tid' => 745,
'title' => 'usercp_latest_subscribed_threads',
'template' => '<tr>
<td align="center" class="{$bgcolor}" width="2%"><span class="thread_status {$folder}" title="{$folder_label}">&nbsp;</span></td>
<td align="center" class="{$bgcolor}" width="2%">{$icon}</td>
<td class="{$bgcolor}">{$gotounread}{$thread[\'displayprefix\']}<a href="{$thread[\'threadlink\']}" class="{$new_class}">{$thread[\'subject\']}</a><br /><span class="smalltext">{$thread[\'author\']}</span></td>
<td align="center" class="{$bgcolor}"><a href="javascript:MyBB.whoPosted({$thread[\'tid\']});">{$thread[\'replies\']}</a></td>
<td align="center" class="{$bgcolor}">{$thread[\'views\']}</td>
<td class="{$bgcolor}" style="white-space: nowrap"><span class="smalltext">{$lastpostdate}
<br /><a href="{$thread[\'lastpostlink\']}">{$lang->lastpost}</a>: {$lastposterlink}</span>
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
387 => 
array (
'tid' => 746,
'title' => 'usercp_latest_threads',
'template' => '<br />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="6"><div class="float_right"><a href="search.php?action=finduserthreads&amp;uid={$this->user->uid}"><strong>{$lang->find_all_threads}</strong></a></div><strong>{$lang->latest_threads}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="3"><span class="smalltext"><strong>{$lang->thread} / {$lang->author}</strong></span></td>
<td class="tcat" align="center" width="75"><span class="smalltext"><strong>{$lang->replies}</strong></span></td>
<td class="tcat" align="center" width="75"><span class="smalltext"><strong>{$lang->views}</strong></span></td>
<td class="tcat" align="center" width="200"><span class="smalltext"><strong>{$lang->lastpost}</strong></span></td>
</tr>
{$latest_threads_threads}
</table>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
388 => 
array (
'tid' => 747,
'title' => 'usercp_latest_threads_threads',
'template' => '<tr>
<td align="center" class="{$bgcolor}" width="2%"><span class="thread_status {$folder}" title="{$folder_label}">&nbsp;</span></td>
<td align="center" class="{$bgcolor}" width="2%">{$icon}</td>
<td class="{$bgcolor}">{$gotounread}{$thread[\'displayprefix\']}<a href="{$thread[\'threadlink\']}" class="{$new_class}">{$thread[\'subject\']}</a><br /><span class="smalltext">{$thread[\'author\']}</span></td>
<td align="center" class="{$bgcolor}"><a href="javascript:MyBB.whoPosted({$thread[\'tid\']});">{$thread[\'replies\']}</a></td>
<td align="center" class="{$bgcolor}">{$thread[\'views\']}</td>
<td class="{$bgcolor}" style="white-space: nowrap"><span class="smalltext">{$lastpostdate}
<br /><a href="{$thread[\'lastpostlink\']}">{$lang->lastpost}</a>: {$lastposterlink}</span>
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
389 => 
array (
'tid' => 748,
'title' => 'usercp_nav',
'template' => '<td width="{$lang->ucp_nav_width}" valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tbody>
<tr>
<td class="thead"><strong>{$lang->ucp_nav_menu}</strong></td>
</tr>
<tr>
<td class="trow1 smalltext"><a href="usercp.php" class="usercp_nav_item usercp_nav_home">{$lang->ucp_nav_home}</a></td>
</tr>
</tbody>
{$usercpmenu}
</table>
</td>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
390 => 
array (
'tid' => 749,
'title' => 'usercp_nav_attachments',
'template' => '<tr><td class="trow1 smalltext"><a href="usercp.php?action=attachments" class="usercp_nav_item usercp_nav_attachments">{$lang->ucp_nav_attachments}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
391 => 
array (
'tid' => 750,
'title' => 'usercp_nav_changename',
'template' => '<div><a href="usercp.php?action=changename" class="usercp_nav_item usercp_nav_username">{$lang->ucp_nav_change_username}</a></div>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
392 => 
array (
'tid' => 751,
'title' => 'usercp_nav_editsignature',
'template' => '<div><a href="usercp.php?action=editsig" class="usercp_nav_item usercp_nav_editsig">{$lang->ucp_nav_edit_sig}</a></div>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
393 => 
array (
'tid' => 752,
'title' => 'usercp_nav_messenger',
'template' => '<tbody>
<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'usercppms\']}">
<div class="expcolimage"><img src="{$theme[\'imgdir\']}/collapse{$collapsedimg[\'usercppms\']}.png" id="usercppms_img" class="expander" alt="[-]"/></div>
<div><span class="smalltext"><strong>{$lang->ucp_nav_messenger}</strong></span></div>
</td>
</tr>
</tbody>
<tbody style="{$collapsed[\'usercppms_e\']}" id="usercppms_e">
{$ucp_nav_compose}
<tr>
<td class="trow1 smalltext">
{$folderlinks}
</td>
</tr>
{$ucp_nav_tracking}
<tr><td class="trow1 smalltext"><a href="private.php?action=folders" class="usercp_nav_item usercp_nav_pmfolders">{$lang->ucp_nav_edit_folders}</a></td></tr>
</tbody>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
394 => 
array (
'tid' => 753,
'title' => 'usercp_nav_messenger_compose',
'template' => '<tr><td class="trow1 smalltext"><a href="private.php?action=send" class="usercp_nav_item usercp_nav_composepm">{$lang->ucp_nav_compose}</a></td></tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
395 => 
array (
'tid' => 754,
'title' => 'usercp_nav_messenger_folder',
'template' => '<div><a href="private.php?fid={$folder_id}" class="usercp_nav_item {$class}">{$folder_name}</a></div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
396 => 
array (
'tid' => 755,
'title' => 'usercp_nav_messenger_tracking',
'template' => '<tr><td class="trow1 smalltext"><a href="private.php?action=tracking" class="usercp_nav_item usercp_nav_pmtracking">{$lang->ucp_nav_tracking}</a></td></tr>',
'sid' => -2,
'version' => '1610',
'status' => '',
'dateline' => 1466356529,
),
397 => 
array (
'tid' => 756,
'title' => 'usercp_nav_misc',
'template' => '<tbody>
<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'usercpmisc\']}">
<div class="expcolimage"><img src="{$theme[\'imgdir\']}/collapse{$collapsedimg[\'usercpmisc\']}.png" id="usercpmisc_img" class="expander" alt="[-]" title="[-]" /></div>
<div><span class="smalltext"><strong>{$lang->ucp_nav_misc}</strong></span></div>
</td>
</tr>
</tbody>
<tbody style="{$collapsed[\'usercpmisc_e\']}" id="usercpmisc_e">
<tr><td class="trow1 smalltext"><a href="usercp.php?action=usergroups" class="usercp_nav_item usercp_nav_usergroups">{$lang->ucp_nav_usergroups}</a></td></tr>
<tr><td class="trow1 smalltext"><a href="usercp.php?action=editlists" class="usercp_nav_item usercp_nav_editlists">{$lang->ucp_nav_editlists}</a></td></tr>
{$attachmentop}
<tr><td class="trow1 smalltext"><a href="usercp.php?action=drafts" class="usercp_nav_item usercp_nav_drafts">{$draftcount}</a></td></tr>
<tr><td class="trow1 smalltext"><a href="usercp.php?action=subscriptions" class="usercp_nav_item usercp_nav_subscriptions">{$lang->ucp_nav_subscribed_threads}</a></td></tr>
<tr><td class="trow1 smalltext"><a href="usercp.php?action=forumsubscriptions" class="usercp_nav_item usercp_nav_fsubscriptions">{$lang->ucp_nav_forum_subscriptions}</a></td></tr>
<tr><td class="trow1 smalltext"><a href="{$profile_link}" class="usercp_nav_item usercp_nav_viewprofile">{$lang->ucp_nav_view_profile}</a></td></tr>
</tbody>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
398 => 
array (
'tid' => 757,
'title' => 'usercp_nav_profile',
'template' => '<tbody>
<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'usercpprofile\']}">
<div class="expcolimage"><img src="{$theme[\'imgdir\']}/collapse{$collapsedimg[\'usercpprofile\']}.png" id="usercpprofile_img" class="expander" alt="[-]" title="[-]" /></div>
<div><span class="smalltext"><strong>{$lang->ucp_nav_profile}</strong></span></div>
</td>
</tr>
</tbody>
<tbody style="{$collapsed[\'usercpprofile_e\']}" id="usercpprofile_e">
<tr><td class="trow1 smalltext">
<div><a href="usercp.php?action=profile" class="usercp_nav_item usercp_nav_profile">{$lang->ucp_nav_edit_profile}</a></div>
{$changenameop}
<div><a href="usercp.php?action=password" class="usercp_nav_item usercp_nav_password">{$lang->ucp_nav_change_pass}</a></div>
<div><a href="usercp.php?action=email" class="usercp_nav_item usercp_nav_email">{$lang->ucp_nav_change_email}</a></div>
<div><a href="usercp.php?action=avatar" class="usercp_nav_item usercp_nav_avatar">{$lang->ucp_nav_change_avatar}</a></div>
{$changesigop}
</td></tr>
<tr><td class="trow1 smalltext"><a href="usercp.php?action=options" class="usercp_nav_item usercp_nav_options">{$lang->ucp_nav_edit_options}</a></td></tr>
</tbody>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
399 => 
array (
'tid' => 758,
'title' => 'usercp_notepad',
'template' => '<br />
<form action="usercp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead"><strong>{$lang->personal_notepad}</strong></td>
</tr>
<tr>
<td align="center" class="trow1" width="100%">
<textarea name="notepad" cols="1" rows="10" class="usercp_notepad">{$this->user->notepad}</textarea>
</td>
</tr>
</table>
<br />
<div align="center">
<input type="hidden" name="action" value="do_notepad" />
<input type="submit" class="button" name="submit" value="{$lang->update_notepad}" />
</div>
</form>',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
400 => 
array (
'tid' => 761,
'title' => 'usercp_options_language',
'template' => '<tr>
<td colspan="2"><span class="smalltext">{$lang->board_language}:</span></td>
</tr>
<tr>
<td colspan="2">
<select name="language">
<option value="">{$lang->use_default}</option>
<option value="0">-----------</option>
{$langoptions}
</select>
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
401 => 
array (
'tid' => 762,
'title' => 'usercp_options_language_option',
'template' => '<option value="{$name}"{$sel}>{$language}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
402 => 
array (
'tid' => 764,
'title' => 'usercp_options_pppselect',
'template' => '<tr>
<td colspan="2"><span class="smalltext">{$lang->ppp}</span></td>
</tr>
<tr>
<td colspan="2">
<select name="ppp">
<option value="">{$lang->use_default}</option>
{$pppoptions}
</select>
</td>
</tr>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
403 => 
array (
'tid' => 768,
'title' => 'usercp_options_timezone',
'template' => '<select name="{$name}" id="{$name}">
{$timezone_option}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
404 => 
array (
'tid' => 769,
'title' => 'usercp_options_timezone_option',
'template' => '<option value="{$timezone}"{$selected_add}>{$label}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
405 => 
array (
'tid' => 770,
'title' => 'usercp_options_tppselect',
'template' => '<tr>
<td colspan="2"><span class="smalltext">{$lang->tpp}</span></td>
</tr>
<tr>
<td colspan="2">
<select name="tpp">
<option value="">{$lang->use_default}</option>
{$tppoptions}
</select>
</td>
</tr>',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
406 => 
array (
'tid' => 771,
'title' => 'usercp_options_tppselect_option',
'template' => '<option value="{$val}"{$selected}>{$tpp_option}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
407 => 
array (
'tid' => 791,
'title' => 'usercp_referrals',
'template' => '<strong>{$lang->members_referred}</strong> {$this->user->referrals}<br />
{$referral_link}',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
408 => 
array (
'tid' => 792,
'title' => 'usercp_reputation',
'template' => '<strong>{$lang->reputation}</strong> {$reputation_link} [<a href="reputation.php?uid={$this->user->uid}">{$lang->details}</a>]<br />',
'sid' => -2,
'version' => '1600',
'status' => '',
'dateline' => 1466356529,
),
409 => 
array (
'tid' => 793,
'title' => 'usercp_resendactivation',
'template' => '<br />(<a href="member.php?action=resendactivation">{$lang->resend_activation}</a>)',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
410 => 
array (
'tid' => 799,
'title' => 'usercp_themeselector',
'template' => '<select name="{$name}">
<option value="0">{$lang->use_default}</option>
<option value="0">-----------</option>
{$themeselect_option}
</select>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
411 => 
array (
'tid' => 800,
'title' => 'usercp_themeselector_option',
'template' => '<option value="{$theme[\'tid\']}"{$sel}>{$depth}{$theme[\'name\']}</option>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
412 => 
array (
'tid' => 802,
'title' => 'usercp_usergroups_joinable',
'template' => '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="3"><strong>{$lang->usergroups_joinable}</strong></td>
</tr>
<tr>
<td class="tcat" width="36%"><strong><span class="smalltext">{$lang->usergroups_usergroup}</span></strong></td>
<td class="tcat" align="center" width="32%"><strong><span class="smalltext">{$lang->join_conditions}</span></strong></td>
<td class="tcat" align="center" width="32%"><strong><span class="smalltext">{$lang->join_group}</span></strong></td>
</tr>
{$joinablegrouplist}
</table>
<br />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
413 => 
array (
'tid' => 806,
'title' => 'usercp_usergroups_joingroup',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->request_join_usergroup}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="usercp.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table width="100%" border="0" align="center">
<tr>
{$usercpnav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->request_join_usergroup}</strong></td>
</tr>
<tr>
<td class="trow1" align="center" colspan="2">{$lang->join_group_moderate_note}</td>
</tr>
<tr>
<td class="trow2"><strong>{$lang->user_group}</strong></td>
<td class="trow2">{$usergroup[\'title\']}</td>
</tr>
<tr>
<td class="trow1"><strong>{$lang->join_reason}</strong></td>
<td class="trow1"><input type="text" class="textbox" name="reason" value="" size="50" /></td>
</tr>
</table>
<br />
<div align="center">
<input type="hidden" name="action" value="usergroups" />
<input type="hidden" name="joingroup" value="{$joingroup}" />
<input type="hidden" name="do" value="joingroup" />
<input type="submit" class="button" value="{$lang->send_join_request}" />
</div>
</td>
</tr>
</table>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
414 => 
array (
'tid' => 823,
'title' => 'video_metacafe_embed',
'template' => '<iframe src="http://www.metacafe.com/embed/{$id}/" width="440" height="248" allowFullScreen frameborder=0></iframe>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
415 => 
array (
'tid' => 824,
'title' => 'video_dailymotion_embed',
'template' => '<iframe frameborder="0" width="480" height="270" src="//www.dailymotion.com/embed/video/{$id}" allowfullscreen></iframe>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
416 => 
array (
'tid' => 825,
'title' => 'video_facebook_embed',
'template' => '<iframe src="https://www.facebook.com/video/embed?video_id={$id}" width="625" height="350" frameborder="0"></iframe>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
417 => 
array (
'tid' => 826,
'title' => 'video_liveleak_embed',
'template' => '<iframe width="500" height="300" src="http://www.liveleak.com/ll_embed?i={$id}" frameborder="0" allowfullscreen></iframe>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
418 => 
array (
'tid' => 827,
'title' => 'video_myspacetv_embed',
'template' => '<iframe width="480" height="270" src="//media.myspace.com/play/video/{$id}" frameborder="0" allowtransparency="true" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
419 => 
array (
'tid' => 828,
'title' => 'video_youtube_embed',
'template' => '<iframe width="560" height="315" src="//www.youtube.com/embed/{$id}" frameborder="0" allowfullscreen></iframe>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
420 => 
array (
'tid' => 829,
'title' => 'video_veoh_embed',
'template' => '<object width="410" height="341" id="veohFlashPlayer" name="veohFlashPlayer"><param name="movie" value="http://www.veoh.com/swf/webplayer/WebPlayer.swf?version=AFrontend.5.7.0.1446&permalinkId={$id}&player=videodetailsembedded&videoAutoPlay=0&id=anonymous"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.veoh.com/swf/webplayer/WebPlayer.swf?version=AFrontend.5.7.0.1446&permalinkId={$id}&player=videodetailsembedded&videoAutoPlay=0&id=anonymous" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="410" height="341" id="veohFlashPlayerEmbed" name="veohFlashPlayerEmbed"></embed></object>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
421 => 
array (
'tid' => 830,
'title' => 'video_vimeo_embed',
'template' => '<iframe src="//player.vimeo.com/video/{$id}" width="500" height="375" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
422 => 
array (
'tid' => 831,
'title' => 'video_yahoo_embed',
'template' => '<iframe width="640" height="360" scrolling="no" frameborder="0" src="//{$local}screen.yahoo.com/{$id}?format=embed" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true" allowtransparency="true"></iframe>',
'sid' => -2,
'version' => '1807',
'status' => '',
'dateline' => 1466356529,
),
423 => 
array (
'tid' => 832,
'title' => 'warnings',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->warning_log}</title>
{$headerinclude}
</head>
<body>
{$header}

{$multipage}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<thead>
<tr>
<td class="thead" colspan="5">
<div class="float_right">{$lang->current_warning_level}</div>
<div><strong>{$lang->warning_log}</strong></div>
</td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->warning}</strong></span></td>
<td class="tcat" width="20%" align="center"><span class="smalltext"><strong>{$lang->date_issued}</strong></span></td>
<td class="tcat" width="20%" align="center"><span class="smalltext"><strong>{$lang->expiry_date}</strong></span></td>
<td class="tcat" width="20%" align="center"><span class="smalltext"><strong>{$lang->issued_by}</strong></span></td>
<td class="tcat" width="1" align="center"><span class="smalltext"><strong>{$lang->details}</strong></span></td>
</tr>
</thead>
<tbody>
{$warnings}
</tbody>
</table>
{$multipage}

{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
424 => 
array (
'tid' => 833,
'title' => 'warnings_active_header',
'template' => '			<tr>
<td class="trow_sep" colspan="5">{$lang->active_warnings}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
425 => 
array (
'tid' => 834,
'title' => 'warnings_expired_header',
'template' => '			<tr>
<td class="trow_sep" colspan="5">{$lang->expired_warnings}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
426 => 
array (
'tid' => 835,
'title' => 'warnings_no_warnings',
'template' => '			<tr>
<td class="trow1" colspan="5" align="center">{$lang->no_warnings}</td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
427 => 
array (
'tid' => 841,
'title' => 'warnings_warn',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->warn_user}</title>
{$headerinclude}
</head>
<body>
{$header}
{$existing_warnings}
{$warn_errors}
<form action="warnings.php" method="post" name="input">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<input type="hidden" name="action" value="do_warn" />
<input type="hidden" name="uid" value="{$user[\'uid\']}" />

<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder clear">
<tr>
<td class="thead" colspan="2"><strong>{$lang->warn_user}</strong></td>
</tr>
<tr>
<td class="tcat" colspan="2"><span class="smalltext"><strong>{$lang->warn_user_desc}</strong></span></td>
</tr>
<tr>
<td class="trow1" style="width: 20%;"><strong>{$lang->username}</strong></td>
<td class="trow1">{$user_link}</td>
</tr>
{$post}
<tr>
<td class="trow1" style="width: 20%; vertical-align: top;"><strong>{$lang->warning_note}</strong></td>
<td class="trow1"><textarea name="notes" cols="60" rows="4">{$notes}</textarea></td>
</tr>
<tr>
<td class="trow2" style="width: 20%; vertical-align: top;"><strong>{$lang->warning_type}</strong></td>
<td class="trow2">
<script type="text/javascript">
// <!--
function checkType(id)
{
var checked = \'\';

$(\'.types_check\').each(function(e, val)
{
if($(this).prop(\'checked\') == true)
{
checked = $(this).val();
}
});
$(\'.types\').each(function(e)
{
$(this).hide();
});
if($(\'#type_\'+checked))
{
$(\'#type_\'+checked).show();
}
}
// -->
</script>
<dl style="margin-top: 0; margin-bottom: 0; width: 100%;">
{$types}
{$custom_warning}
</dl>
<script type="text/javascript">
// <!--
checkType();
// -->
</script>
</td>
</tr>
{$pm_notify}
</table>
<br />
<div style="text-align: center;"><input type="submit" class="button" value="{$lang->warn_user}" /></div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
428 => 
array (
'tid' => 842,
'title' => 'warnings_warn_custom',
'template' => '					<dt><label style="display: block;"><input type="radio" name="type" value="custom" {$type_checked[\'custom\']} class="types_check" onclick="checkType();" style="vertical-align: middle;" /> <strong>{$lang->custom}</strong></label></dt>
<dd style="margin-top: 4px;" id="type_custom" class="types">
<table>
<tr>
<td class="smalltext">{$lang->reason}</td>
<td class="smalltext">{$lang->points}</td>
</tr>
<tr>
<td><input type="text" class="textbox" name="custom_reason" size="50" maxlength="120" value="{$custom_reason}" /></td>
<td><input type="text" class="textbox" name="custom_points" size="2" value="{$custom_points}" /></td>
</tr>
<tr>
<td class="smalltext" colspan="2" style="padding-top: 4px;">{$lang->expires}</td>
</tr>
<tr>
<td colspan="2">
<input type="text" class="textbox" name="expires" size="2" value="{$expires}" />
<select name="expires_period">
<option value="hours" {$expires_period[\'hours\']}>{$lang->hour_or_hours}</option>
<option value="days" {$expires_period[\'days\']}>{$lang->day_or_days}</option>
<option value="weeks" {$expires_period[\'weeks\']}>{$lang->week_or_weeks}</option>
<option value="months" {$expires_period[\'months\']}>{$lang->month_or_months}</option>
<option value="never" {$expires_period[\'never\']}>{$lang->never}</option>
</select>
</td>
</tr>
</table>
</dd>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
429 => 
array (
'tid' => 843,
'title' => 'warnings_warn_existing',
'template' => '	<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<thead>
<tr>
<td class="thead" colspan="5">
<div><strong>{$lang->existing_post_warnings}</strong></div>
</td>
</tr>
<tr>
<td class="tcat"><span class="smalltext"><strong>{$lang->warning}</strong></span></td>
<td class="tcat" width="20%" align="center"><span class="smalltext"><strong>{$lang->date_issued}</strong></span></td>
<td class="tcat" width="20%" align="center"><span class="smalltext"><strong>{$lang->expiry_date}</strong></span></td>
<td class="tcat" width="20%" align="center"><span class="smalltext"><strong>{$lang->issued_by}</strong></span></td>
<td class="tcat" width="1" align="center"><span class="smalltext"><strong>{$lang->details}</strong></span></td>
</tr>
</thead>
<tbody>
{$warnings}
</tbody>
</table><br />',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
430 => 
array (
'tid' => 844,
'title' => 'warnings_warn_pm',
'template' => '<tr>
<td width="20%" class="trow1" valign="top">
<strong>{$lang->send_pm}</strong>
</td>
<td class="trow1">
<script type="text/javascript">
// <!--
function togglePM()
{
if($(\'#send_pm\').prop(\'checked\') == true)
{
$(\'#pm_input\').show();
$(\'#pm_smilie_insert\').show();
}
else
{
$(\'#pm_input\').hide();
}
}
// -->
</script>
<label style="display: block;"><input type="checkbox" class="checkbox" value="1" name="send_pm" id="send_pm" onclick="togglePM();" style="vertical-align: middle;" {$send_pm_checked} /> {$lang->send_user_warning_pm}</label>
<table id="pm_input" cellpadding="{$theme[\'tablespace\']}">
<tr>
<td><strong>{$lang->send_pm_subject}</strong></td>
<td><input type="text" class="textbox" name="pm_subject" size="40" maxlength="85" value="{$pm_subject}" /></td>
</tr>
<tr>
<td valign="top"><strong>{$lang->send_pm_message}</strong></td>
<td><textarea name="pm_message" id="message" rows="20" cols="70">{$message}</textarea>
{$codebuttons}</td>
</tr>
{$anonymous_pm}
</table>
<script type="text/javascript">
<!--
$(document).ready(function() {
togglePM();
});
// -->
</script>
</td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
431 => 
array (
'tid' => 845,
'title' => 'warnings_warn_pm_anonymous',
'template' => '<tr>
<td><strong>{$lang->send_pm_options}</strong></td>
<td><label><input type="checkbox" class="checkbox" name="pm_anonymous" value="1"{$checked} />&nbsp;{$lang->send_pm_options_anonymous}</label></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
432 => 
array (
'tid' => 846,
'title' => 'warnings_warn_post',
'template' => '		<tr>
<td width="20%" class="trow2"><strong>{$lang->post}</strong></td>
<td class="trow2"><input type="hidden" name="pid" value="{$post[\'pid\']}" /><a href="{$post_link}">{$post[\'subject\']}</a></td>
</tr>',
'sid' => -2,
'version' => '1400',
'status' => '',
'dateline' => 1466356529,
),
433 => 
array (
'tid' => 847,
'title' => 'warnings_warn_type',
'template' => '					<dt><label style="display: block;"><input type="radio" name="type" value="{$type[\'tid\']}" {$checked} class="types_check" onclick="checkType();" style="vertical-align: middle;" /> <strong>{$type[\'title\']}</strong> {$points}</label></dt>
<dd style="margin-top: 4px;" id="type_{$type[\'tid\']}" class="types">
<div class="smalltext">{$lang->new_warning_level}</div>
<div class="tborder float_left" style="width: 150px; margin: 0; padding: 1px;">
<div class="trow1 float_left" style="width: {$current_level}%;">&nbsp;</div>
<div class="trow2 float_left" style="width: {$level_diff}%;">&nbsp;</div>
</div>
<div class="float_left" style="padding-left: 10px; font-weight: bold;">{$new_warning_level}%</div><br style="clear: left;" />
{$result}
</dd>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
434 => 
array (
'tid' => 848,
'title' => 'warnings_warn_type_result',
'template' => '<div class="smalltext" style="clear: left; padding-top: 4px;">{$lang->result}<br />{$new_level[\'friendly_action\']}</div>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
435 => 
array (
'tid' => 849,
'title' => 'warnings_warning',
'template' => '			<tr>
<td class="{$alt_bg}">{$warning_type} {$points}{$post_link}</td>
<td class="{$alt_bg}" style="text-align: center;">{$date_issued}</td>
<td class="{$alt_bg}" style="text-align: center;">{$expires}</td>
<td class="{$alt_bg}" style="text-align: center;">{$issuedby}</td>
<td class="{$alt_bg}" style="text-align: center;"><a href="warnings.php?action=view&amp;wid={$warning[\'wid\']}">{$lang->view}</a></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
436 => 
array (
'tid' => 853,
'title' => 'attachment_icon',
'template' => '<img src="{$icon}" title="{$name}" border="0" alt=".{$ext}" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
437 => 
array (
'tid' => 855,
'title' => 'codebuttons',
'template' => '<link rel="stylesheet" href="{$mybb->asset_url}/jscripts/sceditor/editor_themes/{$theme[\'editortheme\']}" type="text/css" media="all" />
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/sceditor/jquery.sceditor.bbcode.min.js?ver=1805"></script>
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/bbcodes_sceditor.js?ver=1804"></script>
<script type="text/javascript" src="{$mybb->asset_url}/jscripts/sceditor/editor_plugins/undo.js?ver=1805"></script>
<script type="text/javascript">
var partialmode = {$mybb->settings[\'partialmode\']},
opt_editor = {
plugins: "bbcode,undo",
style: "{$mybb->asset_url}/jscripts/sceditor/textarea_styles/jquery.sceditor.{$theme[\'editortheme\']}",
rtl: {$lang->settings[\'rtl\']},
locale: "mybblang",
enablePasteFiltering: true,
autoUpdate: true,
emoticonsEnabled: {$emoticons_enabled},
emoticons: {
// Emoticons to be included in the dropdown
dropdown: {
{$dropdownsmilies}
},
// Emoticons to be included in the more section
more: {
{$moresmilies}
},
// Emoticons that are not shown in the dropdown but will still be converted. Can be used for things like aliases
hidden: {
{$hiddensmilies}
}
},
emoticonsCompat: true,
toolbar: "{$basic1}{$align}{$font}{$size}{$color}{$removeformat}{$basic2}image,{$email}{$link}|video{$emoticon}|{$list}{$code}quote|maximize,source",
};
{$editor_language}
$(function() {
$("#{$bind}").sceditor(opt_editor);

MyBBEditor = $("#{$bind}").sceditor("instance");
{$sourcemode}
});
</script>',
'sid' => -2,
'version' => '1806',
'status' => '',
'dateline' => 1466356529,
),
438 => 
array (
'tid' => 856,
'title' => 'contact',
'template' => '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - {$lang->contact}</title>
{$headerinclude}
</head>
<body>
{$header}
<form action="contact.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
{$errors}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2"><strong>{$lang->contact}</strong></td>
</tr>
<tr>
<td class="trow1" valign="top"><strong>{$lang->contact_subject}:</strong><br /><span class="smalltext">{$lang->contact_subject_desc}</span></td>
<td class="trow1"><input type="text" name="subject" class="textbox" size="49" value="{$mybb->input[\'subject\']}" /></td>
</tr>
<tr>
<td class="trow2" valign="top"><strong>{$lang->contact_message}:</strong><br /><span class="smalltext">{$lang->contact_message_desc}</span></td>
<td class="trow2"><textarea cols="50" rows="10" name="message" class="textarea" >{$mybb->input[\'message\']}</textarea></td>
</tr>
<tr>
<td class="trow1" valign="top"><strong>{$lang->contact_email}:</strong><br /><span class="smalltext">{$lang->contact_email_desc}</span></td>
<td class="trow1"><input type="text" name="email" class="textbox" size="49" value="{$mybb->input[\'email\']}" /></td>
</tr>
{$captcha}
</table>
<br />
<div align="center">
<input type="submit" class="button" name="submit" value="{$lang->contact_send}" />
</div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
439 => 
array (
'tid' => 858,
'title' => 'gobutton',
'template' => '<input type="submit" class="button" value="{$lang->go}" />',
'sid' => -2,
'version' => '120',
'status' => '',
'dateline' => 1466356529,
),
440 => 
array (
'tid' => 864,
'title' => 'sendthread',
'template' => '<html>
<head>
<title>{$threadprefix[\'prefix\']} {$thread[\'subject\']} - {$lang->send_thread}</title>
{$headerinclude}
</head>
<body>
{$header}
{$errors}
<form action="sendthread.php" method="post">
<input type="hidden" name="my_post_key" value="{$mybb->post_code}" />
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td colspan="2" width="100%" class="thead"><strong>{$lang->send_thread}</strong></td>
</tr>
{$from_email}
<tr>
<td width="40%" class="trow1"><strong>{$lang->recipient}</strong><br /><span class="smalltext">{$lang->recipient_note}</span></td>
<td width="60%" class="trow1"><input type="text" class="textbox" size="50" name="email" value="{$email}" /></td>
</tr>
<tr>
<td width="40%" class="trow2"><strong>{$lang->subject}</strong></td>
<td width="60%" class="trow2"><input type="text" class="textbox" size="50" name="subject" value="{$subject}" /></td>
</tr>
<tr>
<td valign="top" width="40%" class="trow1"><strong>{$lang->message}</strong></td>
<td width="60%" class="trow1"><textarea cols="50" rows="10" name="message">{$message}</textarea></td>
</tr>
{$captcha}
</table>
<br />
<input type="hidden" name="action" value="do_sendtofriend" />
<input type="hidden" name="tid" value="{$tid}" />
<div align="center"><input type="submit" class="button" value="{$lang->send_thread}" /></div>
</form>
{$footer}
</body>
</html>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
441 => 
array (
'tid' => 865,
'title' => 'sendthread_fromemail',
'template' => '<tr>
<td width="40%" class="trow2"><strong>{$lang->your_name}</strong><br /><span class="smalltext">{$lang->name_note}</span></td>
<td width="60%" class="trow2"><input type="text" class="textbox" size="50" name="fromname" value="{$fromname}" /></td>
</tr>
<tr>
<td width="40%" class="trow1"><strong>{$lang->your_email}</strong><br /><span class="smalltext">{$lang->email_note}</span></td>
<td width="60%" class="trow1"><input type="text" class="textbox" size="50" name="fromemail" value="{$fromemail}" /></td>
</tr>',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
442 => 
array (
'tid' => 866,
'title' => 'task_image',
'template' => '<img src="{$mybb->settings[\'bburl\']}/task.php" width="1" height="1" alt="" />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
443 => 
array (
'tid' => 867,
'title' => 'smilie',
'template' => '<img src="{$smilie[\'image\']}" alt="{$smilie[\'name\']}" title="{$smilie[\'name\']}" class="smilie smilie_{$smilie[\'sid\']}{$extra_class}"{$onclick} />',
'sid' => -2,
'version' => '1800',
'status' => '',
'dateline' => 1466356529,
),
444 => 
array (
'tid' => 868,
'title' => 'global_board_offline_modal',
'template' => '<div class="modal">
<div style="overflow-y: auto; max-height: 400px;">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead">
<strong>{$mybb->settings[\'bbname\']}</strong>
</td>
</tr>
<tr>
<td class="trow1">{$lang->error_boardclosed}</td>
</tr>
</table>
</div>
</div>',
'sid' => -2,
'version' => '1804',
'status' => '',
'dateline' => 1466356529,
),
445 => 
array (
'tid' => 870,
'title' => 'post_captcha_recaptcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top"><strong>{$this->bb->lang->image_verification}</strong></td>
<td class="trow1">
<script type="text/javascript">
<!--
var RecaptchaOptions = {
theme: \'clean\'
};
// -->
</script>
<table style="width: 300px; padding: 4px;">
<tr>
<td><span class="smalltext">{$this->bb->lang->verification_note}</span></td>
</tr>
<tr>
<td><script type="text/javascript" src="{$server}/challenge?k={$public_key}"></script></td>
</tr>
</table>
</td>
</tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1466630212,
),
446 => 
array (
'tid' => 871,
'title' => 'post_captcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top" width="350"><strong>{$this->bb->lang->image_verification}</strong>
<br/>
<span class="smalltext">{$this->bb->lang->verification_note}</span>
</td>
<td class="trow1">
<script type="text/javascript">
<!--
lang.captcha_fetch_failure = "{$this->bb->lang->captcha_fetch_failure}";
// -->
</script>
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/captcha.js?ver=1804"></script>
<table style="width: 300px; padding: 4px;">
<tr>
<td style="vertical-align: middle;"><img src="{$this->bb->settings[\'bburl\']}/captcha?imagehash={$imagehash}" alt="{$this->bb->lang->image_verification}" title="{$this->bb->lang->image_verification}" id="captcha_img" /><br /><span style="color: red;" class="smalltext">{$this->bb->lang->verification_subnote}</span>
</td>
</tr>
<tr>
<td>
<input type="text" class="textbox" name="imagestring" value="" id="imagestring" size="16" /><input type="hidden" name="imagehash" value="{$imagehash}" id="imagehash" />
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
document.write(\'<input type="button" class="button" name="refresh" value="{$this->bb->lang->refresh}" onclick="return captcha.refresh();" \\/>\');
}
// -->
</script>
</td>
</tr>
</table>
</td>
</tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1466636776,
),
447 => 
array (
'tid' => 873,
'title' => 'usercp_nav_messenger_tracking',
'template' => '<tr><td class="trow1 smalltext"><a href="private/tracking" class="usercp_nav_item usercp_nav_pmtracking">{$this->bb->lang->ucp_nav_tracking}</a></td></tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1466896140,
),
448 => 
array (
'tid' => 874,
'title' => 'usercp_nav_messenger_compose',
'template' => '<tr><td class="trow1 smalltext"><a href="private/send" class="usercp_nav_item usercp_nav_composepm">{$this->bb->lang->ucp_nav_compose}</a></td></tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1466896193,
),
449 => 
array (
'tid' => 875,
'title' => 'usercp_nav_messenger',
'template' => '<tbody>
<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'usercppms\']}">
<div class="expcolimage"><img src="{$this->bb->theme[\'imgdir\']}/collapse{$collapsedimg[\'usercppms\']}.png" id="usercppms_img" class="expander" alt="[-]"/></div>
<div><span class="smalltext"><strong>{$this->bb->lang->ucp_nav_messenger}</strong></span></div>
</td>
</tr>
</tbody>
<tbody style="{$collapsed[\'usercppms_e\']}" id="usercppms_e">
{$ucp_nav_compose}
<tr>
<td class="trow1 smalltext">
{$folderlinks}
</td>
</tr>
{$ucp_nav_tracking}
<tr><td class="trow1 smalltext"><a href="private/folders" class="usercp_nav_item usercp_nav_pmfolders">{$this->bb->lang->ucp_nav_edit_folders}</a></td></tr>
</tbody>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1466896367,
),
450 => 
array (
'tid' => 876,
'title' => 'report_reasons',
'template' => '<tr>
<td class="trow1" align="left" style="width: 25%"><span class="smalltext"><strong>{$this->lang->report_reason}</strong></span></td>
<td class="trow1" align="left">
<select name="reason" id="report_reason">
<option value="rules">{$this->lang->report_reason_rules}</option>
<option value="bad">{$this->lang->report_reason_bad}</option>
<option value="spam">{$this->lang->report_reason_spam}</option>
<option value="wrong">{$this->lang->report_reason_wrong}</option>
<option value="other">{$this->lang->report_reason_other}</option>
</select>
</td>
</tr>
<tr id="reason">
<td class="trow2">&nbsp;</td>
<td class="trow2" align="left">
<div>{$this->lang->report_reason_other_description}</div>
<input type="text" class="textbox" name="comment" size="40" maxlength="250" />
</td>
</tr>
<tr>
<td colspan="2" class="tfoot"><input type="submit" class="button" value="{$report_title}" /></td>
</tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1467329477,
),
451 => 
array (
'tid' => 877,
'title' => 'private_archive_html_folderhead',
'template' => '</table>
<br />
<table border="0" cellspacing="{$this->bb->theme[\'borderwidth\']}" cellpadding="{$this->bb->theme[\'tablespace\']}" class="tborder">
<tr>
<td class="tcat" colspan="2"><strong>{$this->lang->folder} {$foldername}</strong></td>
</tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932251,
),
452 => 
array (
'tid' => 878,
'title' => 'private_archive_html_message',
'template' => '<tr>
<td class="trow1"><strong>{$this->lang->subject} {$message[\'subject\']}</strong><br /><em>{$this->lang->to} {$message[\'tousername\']}<br />{$this->lang->from} {$message[\'fromusername\']}<br />{$this->lang->sent} {$senddate}</em></td>
</tr>
<tr>
<td class="trow2">{$message[\'message\']}</td>
</tr>
<tr>
<td class="tcat" height="3"> </td>
</tr>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932313,
),
453 => 
array (
'tid' => 879,
'title' => 'private_archive_html',
'template' => '<html>
<head>
<title>{$this->bb->settings[\'bbname\']} - {$this->lang->private_messages_for}</title>
<style type="text/css">{$css}
* {text-align: left}</style>
</head>
<body>
<br />
<table border="0" cellspacing="{$this->bb->theme[\'borderwidth\']}" cellpadding="{$this->bb->theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="1"><span class="largetext"><strong>{$this->lang->private_messages_for}</strong></span></td>
</tr>
{$pmsdownload}
</table>
<br />
<table border="0" cellspacing="{$this->bb->theme[\'borderwidth\']}" cellpadding="{$this->bb->theme[\'tablespace\']}" class="tborder">
<tr>
<td class="trow1" colspan="1">{$this->lang->exported_date}<br /><a href="{$this->bb->settings[\'bburl\']}">{$this->bb->settings[\'bbname\']}</a></td>
</tr>
</table>
</body>',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932414,
),
454 => 
array (
'tid' => 880,
'title' => 'private_archive_txt',
'template' => '{$this->lang->private_messages_for}
({$this->lang->exported_date})

{$pmsdownload}',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932460,
),
455 => 
array (
'tid' => 881,
'title' => 'private_archive_txt_folderhead',
'template' => '
#######################################################################
{$this->lang->folder} {$foldername}
#######################################################################',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932553,
),
456 => 
array (
'tid' => 882,
'title' => 'private_archive_txt_message',
'template' => '\\n{$this->lang->subject} {$message[\'subject\']}
{$tofrom} {$tofromusername}
{$this->lang->sent} {$senddate}
------------------------------------------------------------------------
{$message[\'message\']}
------------------------------------------------------------------------',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932604,
),
457 => 
array (
'tid' => 883,
'title' => 'private_archive_csv',
'template' => '{$this->lang->export_date_sent},{$this->lang->export_folder},{$this->lang->export_subject},{$this->lang->export_to},{$this->lang->export_from},{$this->lang->export_message}
{$pmsdownload}',
'sid' => 1,
'version' => '1807',
'status' => '',
'dateline' => 1472932809,
),
458 => 
array (
'tid' => 1080,
'title' => 'forumbit_depth2_forum_lastpost',
'template' => '<span class="smalltext">
<a href="{$lastpost_link}" title="{$full_lastpost_subject}"><strong>{$lastpost_subject}</strong></a>
<br />{$lastpost_date}<br />{$this->bb->lang->by} {$lastpost_profilelink}</span>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
459 => 
array (
'tid' => 1081,
'title' => 'post_captcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top" width="350"><strong>{$this->bb->lang->image_verification}</strong>
<br/>
<span class="smalltext">{$this->bb->lang->verification_note}</span>
</td>
<td class="trow1">
<script type="text/javascript">
<!--
lang.captcha_fetch_failure = "{$this->bb->lang->captcha_fetch_failure}";
// -->
</script>
<script type="text/javascript" src="{$this->bb->asset_url}/jscripts/captcha.js?ver=1804"></script>
<table style="width: 300px; padding: 4px;">
<tr>
<td style="vertical-align: middle;"><img src="{$this->bb->settings[\'bburl\']}/captcha?imagehash={$imagehash}" alt="{$this->bb->lang->image_verification}" title="{$this->bb->lang->image_verification}" id="captcha_img" /><br /><span style="color: red;" class="smalltext">{$this->bb->lang->verification_subnote}</span>
</td>
</tr>
<tr>
<td>
<input type="text" class="textbox" name="imagestring" value="" id="imagestring" size="16" /><input type="hidden" name="imagehash" value="{$imagehash}" id="imagehash" />
<script type="text/javascript">
<!--
if(use_xmlhttprequest == "1")
{
document.write(\'<input type="button" class="button" name="refresh" value="{$this->bb->lang->refresh}" onclick="return captcha.refresh();" \\/>\');
}
// -->
</script>
</td>
</tr>
</table>
</td>
</tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
460 => 
array (
'tid' => 1082,
'title' => 'post_captcha_recaptcha',
'template' => '<tr id="captcha_trow">
<td class="trow1" valign="top"><strong>{$this->bb->lang->image_verification}</strong></td>
<td class="trow1">
<script type="text/javascript">
<!--
var RecaptchaOptions = {
theme: \'clean\'
};
// -->
</script>
<table style="width: 300px; padding: 4px;">
<tr>
<td><span class="smalltext">{$this->bb->lang->verification_note}</span></td>
</tr>
<tr>
<td><script type="text/javascript" src="{$server}/challenge?k={$public_key}"></script></td>
</tr>
</table>
</td>
</tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
461 => 
array (
'tid' => 1083,
'title' => 'private_archive_csv',
'template' => '{$this->lang->export_date_sent},{$this->lang->export_folder},{$this->lang->export_subject},{$this->lang->export_to},{$this->lang->export_from},{$this->lang->export_message}
{$pmsdownload}',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
462 => 
array (
'tid' => 1084,
'title' => 'private_archive_html',
'template' => '<html>
<head>
<title>{$this->bb->settings[\'bbname\']} - {$this->lang->private_messages_for}</title>
<style type="text/css">{$css}
* {text-align: left}</style>
</head>
<body>
<br />
<table border="0" cellspacing="{$this->bb->theme[\'borderwidth\']}" cellpadding="{$this->bb->theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="1"><span class="largetext"><strong>{$this->lang->private_messages_for}</strong></span></td>
</tr>
{$pmsdownload}
</table>
<br />
<table border="0" cellspacing="{$this->bb->theme[\'borderwidth\']}" cellpadding="{$this->bb->theme[\'tablespace\']}" class="tborder">
<tr>
<td class="trow1" colspan="1">{$this->lang->exported_date}<br /><a href="{$this->bb->settings[\'bburl\']}">{$this->bb->settings[\'bbname\']}</a></td>
</tr>
</table>
</body>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
463 => 
array (
'tid' => 1085,
'title' => 'private_archive_html_folderhead',
'template' => '</table>
<br />
<table border="0" cellspacing="{$this->bb->theme[\'borderwidth\']}" cellpadding="{$this->bb->theme[\'tablespace\']}" class="tborder">
<tr>
<td class="tcat" colspan="2"><strong>{$this->lang->folder} {$foldername}</strong></td>
</tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
464 => 
array (
'tid' => 1086,
'title' => 'private_archive_html_message',
'template' => '<tr>
<td class="trow1"><strong>{$this->lang->subject} {$message[\'subject\']}</strong><br /><em>{$this->lang->to} {$message[\'tousername\']}<br />{$this->lang->from} {$message[\'fromusername\']}<br />{$this->lang->sent} {$senddate}</em></td>
</tr>
<tr>
<td class="trow2">{$message[\'message\']}</td>
</tr>
<tr>
<td class="tcat" height="3"> </td>
</tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
465 => 
array (
'tid' => 1087,
'title' => 'private_archive_txt',
'template' => '{$this->lang->private_messages_for}
({$this->lang->exported_date})

{$pmsdownload}',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
466 => 
array (
'tid' => 1088,
'title' => 'private_archive_txt_folderhead',
'template' => '
#######################################################################
{$this->lang->folder} {$foldername}
#######################################################################',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
467 => 
array (
'tid' => 1089,
'title' => 'private_archive_txt_message',
'template' => '\\n{$this->lang->subject} {$message[\'subject\']}
{$tofrom} {$tofromusername}
{$this->lang->sent} {$senddate}
------------------------------------------------------------------------
{$message[\'message\']}
------------------------------------------------------------------------',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
468 => 
array (
'tid' => 1090,
'title' => 'report_reasons',
'template' => '<tr>
<td class="trow1" align="left" style="width: 25%"><span class="smalltext"><strong>{$this->lang->report_reason}</strong></span></td>
<td class="trow1" align="left">
<select name="reason" id="report_reason">
<option value="rules">{$this->lang->report_reason_rules}</option>
<option value="bad">{$this->lang->report_reason_bad}</option>
<option value="spam">{$this->lang->report_reason_spam}</option>
<option value="wrong">{$this->lang->report_reason_wrong}</option>
<option value="other">{$this->lang->report_reason_other}</option>
</select>
</td>
</tr>
<tr id="reason">
<td class="trow2">&nbsp;</td>
<td class="trow2" align="left">
<div>{$this->lang->report_reason_other_description}</div>
<input type="text" class="textbox" name="comment" size="40" maxlength="250" />
</td>
</tr>
<tr>
<td colspan="2" class="tfoot"><input type="submit" class="button" value="{$report_title}" /></td>
</tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
469 => 
array (
'tid' => 1091,
'title' => 'usercp_nav_messenger',
'template' => '<tbody>
<tr>
<td class="tcat tcat_menu tcat_collapse{$collapsedimg[\'usercppms\']}">
<div class="expcolimage"><img src="{$this->bb->theme[\'imgdir\']}/collapse{$collapsedimg[\'usercppms\']}.png" id="usercppms_img" class="expander" alt="[-]"/></div>
<div><span class="smalltext"><strong>{$this->bb->lang->ucp_nav_messenger}</strong></span></div>
</td>
</tr>
</tbody>
<tbody style="{$collapsed[\'usercppms_e\']}" id="usercppms_e">
{$ucp_nav_compose}
<tr>
<td class="trow1 smalltext">
{$folderlinks}
</td>
</tr>
{$ucp_nav_tracking}
<tr><td class="trow1 smalltext"><a href="private/folders" class="usercp_nav_item usercp_nav_pmfolders">{$this->bb->lang->ucp_nav_edit_folders}</a></td></tr>
</tbody>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
470 => 
array (
'tid' => 1092,
'title' => 'usercp_nav_messenger_compose',
'template' => '<tr><td class="trow1 smalltext"><a href="private/send" class="usercp_nav_item usercp_nav_composepm">{$this->bb->lang->ucp_nav_compose}</a></td></tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
471 => 
array (
'tid' => 1093,
'title' => 'usercp_nav_messenger_tracking',
'template' => '<tr><td class="trow1 smalltext"><a href="private/tracking" class="usercp_nav_item usercp_nav_pmtracking">{$this->bb->lang->ucp_nav_tracking}</a></td></tr>',
'sid' => 10,
'version' => '1807',
'status' => '',
'dateline' => 1479950054,
),
));

        
    }
}
