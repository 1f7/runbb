<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class TemplatesetsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('templatesets')->delete();
        
        DB::table('templatesets')->insert(array (
            0 => 
            array (
                'sid' => 1,
                'title' => 'Default Templates',
            ),
            1 => 
            array (
                'sid' => 10,
                'title' => 'Default Water Templates',
            ),
        ));

        
    }
}
