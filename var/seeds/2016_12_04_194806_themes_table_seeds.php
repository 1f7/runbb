<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ThemesTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('themes')->delete();
        
        DB::table('themes')->insert(array (
            0 => 
            array (
                'tid' => 1,
                'name' => 'MyBB Master Style',
                'pid' => 0,
                'def' => 0,
                'properties' => 'a:7:{s:11:"templateset";i:-2;s:6:"imgdir";s:6:"images";s:4:"logo";s:15:"images/logo.png";s:10:"tablespace";s:1:"5";s:11:"borderwidth";s:1:"0";s:11:"editortheme";s:8:"mybb.css";s:9:"disporder";a:7:{s:10:"global.css";i:1;s:10:"usercp.css";i:2;s:9:"modcp.css";i:3;s:16:"star_ratings.css";i:4;s:14:"showthread.css";i:5;s:17:"thread_status.css";i:6;s:8:"css3.css";i:7;}}',
                'stylesheets' => 'a:8:{s:6:"global";a:1:{s:6:"global";a:2:{i:0;s:20:"css.php?stylesheet=1";i:1;s:20:"css.php?stylesheet=7";}}s:10:"usercp.php";a:1:{s:6:"global";a:2:{i:0;s:20:"css.php?stylesheet=2";i:1;s:20:"css.php?stylesheet=6";}}s:11:"usercp2.php";a:1:{s:6:"global";a:1:{i:0;s:20:"css.php?stylesheet=2";}}s:11:"private.php";a:1:{s:6:"global";a:1:{i:0;s:20:"css.php?stylesheet=2";}}s:9:"modcp.php";a:1:{s:6:"global";a:1:{i:0;s:20:"css.php?stylesheet=3";}}s:16:"forumdisplay.php";a:1:{s:6:"global";a:2:{i:0;s:20:"css.php?stylesheet=4";i:1;s:20:"css.php?stylesheet=6";}}s:14:"showthread.php";a:1:{s:6:"global";a:2:{i:0;s:20:"css.php?stylesheet=4";i:1;s:20:"css.php?stylesheet=5";}}s:10:"search.php";a:1:{s:6:"global";a:1:{i:0;s:20:"css.php?stylesheet=6";}}}',
                'allowedgroups' => 'all',
            ),
            1 => 
            array (
                'tid' => 2,
                'name' => 'Default',
                'pid' => 1,
                'def' => 1,
                'properties' => 'a:8:{s:11:"templateset";i:1;s:11:"editortheme";s:8:"mybb.css";s:6:"imgdir";s:6:"images";s:4:"logo";s:21:"images/logo_runbb.png";s:10:"tablespace";i:5;s:11:"borderwidth";i:0;s:6:"colors";a:10:{s:5:"black";s:6:"Black";s:4:"calm";s:5:"Calm";s:4:"dawn";s:5:"Dawn";s:5:"earth";s:6:"Earth";s:5:"flame";s:6:"Flame";s:4:"leaf";s:5:"Leaf";s:5:"night";s:6:"Night";s:3:"sun";s:4:"Sun";s:8:"twilight";s:9:"Twilight";s:5:"water";s:5:"Water";}s:9:"disporder";a:17:{s:10:"global.css";i:1;s:10:"usercp.css";i:2;s:9:"modcp.css";i:3;s:16:"star_ratings.css";i:4;s:14:"showthread.css";i:5;s:17:"thread_status.css";i:6;s:8:"css3.css";i:7;s:15:"color_black.css";i:8;s:14:"color_calm.css";i:9;s:14:"color_dawn.css";i:10;s:15:"color_earth.css";i:11;s:15:"color_flame.css";i:12;s:14:"color_leaf.css";i:13;s:15:"color_night.css";i:14;s:13:"color_sun.css";i:15;s:18:"color_twilight.css";i:16;s:15:"color_water.css";i:17;}}',
                'stylesheets' => 'a:18:{s:5:"black";a:1:{s:6:"global";a:1:{i:0;s:29:"themes/theme2/color_black.css";}}s:4:"calm";a:1:{s:6:"global";a:1:{i:0;s:28:"themes/theme2/color_calm.css";}}s:4:"dawn";a:1:{s:6:"global";a:1:{i:0;s:28:"themes/theme2/color_dawn.css";}}s:5:"earth";a:1:{s:6:"global";a:1:{i:0;s:29:"themes/theme2/color_earth.css";}}s:5:"flame";a:1:{s:6:"global";a:1:{i:0;s:29:"themes/theme2/color_flame.css";}}s:4:"leaf";a:1:{s:6:"global";a:1:{i:0;s:28:"themes/theme2/color_leaf.css";}}s:5:"night";a:1:{s:6:"global";a:1:{i:0;s:29:"themes/theme2/color_night.css";}}s:3:"sun";a:1:{s:6:"global";a:1:{i:0;s:17:"css?stylesheet=15";}}s:8:"twilight";a:1:{s:6:"global";a:1:{i:0;s:32:"themes/theme2/color_twilight.css";}}s:5:"water";a:1:{s:6:"global";a:1:{i:0;s:29:"themes/theme2/color_water.css";}}s:6:"global";a:1:{s:6:"global";a:2:{i:0;s:24:"themes/theme2/global.css";i:1;s:22:"themes/theme2/css3.css";}}s:6:"usercp";a:1:{s:6:"global";a:2:{i:0;s:24:"themes/theme2/usercp.css";i:1;s:31:"themes/theme2/thread_status.css";}}s:7:"usercp2";a:1:{s:6:"global";a:1:{i:0;s:24:"themes/theme2/usercp.css";}}s:7:"private";a:1:{s:6:"global";a:1:{i:0;s:24:"themes/theme2/usercp.css";}}s:5:"modcp";a:1:{s:6:"global";a:1:{i:0;s:23:"themes/theme2/modcp.css";}}s:12:"forumdisplay";a:1:{s:6:"global";a:2:{i:0;s:30:"themes/theme2/star_ratings.css";i:1;s:31:"themes/theme2/thread_status.css";}}s:10:"showthread";a:1:{s:6:"global";a:2:{i:0;s:30:"themes/theme2/star_ratings.css";i:1;s:28:"themes/theme2/showthread.css";}}s:6:"search";a:1:{s:6:"global";a:1:{i:0;s:31:"themes/theme2/thread_status.css";}}}',
                'allowedgroups' => 'all',
            ),
            2 => 
            array (
                'tid' => 3,
                'name' => 'Default Water',
                'pid' => 1,
                'def' => 0,
                'properties' => 'a:9:{s:11:"templateset";i:10;s:11:"editortheme";s:8:"mybb.css";s:6:"imgdir";s:6:"images";s:4:"logo";s:21:"images/logo_runbb.png";s:10:"tablespace";i:5;s:11:"borderwidth";i:0;s:5:"color";s:5:"water";s:6:"colors";a:1:{s:5:"water";s:5:"Water";}s:9:"disporder";a:8:{s:10:"global.css";i:1;s:10:"usercp.css";i:2;s:9:"modcp.css";i:3;s:16:"star_ratings.css";i:4;s:14:"showthread.css";i:5;s:17:"thread_status.css";i:6;s:8:"css3.css";i:7;s:15:"color_water.css";i:8;}}',
                'stylesheets' => 'a:8:{s:6:"global";a:1:{s:6:"global";a:3:{i:0;s:24:"themes/theme3/global.css";i:1;s:22:"themes/theme3/css3.css";i:2;s:29:"themes/theme3/color_water.css";}}s:6:"usercp";a:1:{s:6:"global";a:2:{i:0;s:24:"themes/theme3/usercp.css";i:1;s:31:"themes/theme3/thread_status.css";}}s:7:"usercp2";a:1:{s:6:"global";a:1:{i:0;s:24:"themes/theme3/usercp.css";}}s:7:"private";a:1:{s:6:"global";a:1:{i:0;s:24:"themes/theme3/usercp.css";}}s:5:"modcp";a:1:{s:6:"global";a:1:{i:0;s:23:"themes/theme3/modcp.css";}}s:12:"forumdisplay";a:1:{s:6:"global";a:2:{i:0;s:30:"themes/theme3/star_ratings.css";i:1;s:31:"themes/theme3/thread_status.css";}}s:10:"showthread";a:1:{s:6:"global";a:2:{i:0;s:30:"themes/theme3/star_ratings.css";i:1;s:28:"themes/theme3/showthread.css";}}s:6:"search";a:1:{s:6:"global";a:1:{i:0;s:31:"themes/theme3/thread_status.css";}}}',
                'allowedgroups' => 'all',
            ),
        ));

        
    }
}
