<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ThemestylesheetsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('themestylesheets')->delete();
        
        DB::table('themestylesheets')->insert(array (
            0 => 
            array (
                'sid' => 1,
                'name' => 'global.css',
                'tid' => 1,
                'attachedto' => '',
                'stylesheet' => 'body {
background: #fff;
color: #333;
text-align: center;
line-height: 1.4;
margin: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
overflow-y: scroll;
}

a:link {
color: #0072BC;
text-decoration: none;
}

a:visited {
color: #0072BC;
text-decoration: none;
}

a:hover,
a:active {
color: #0072BC;
text-decoration: underline;
}

#container {
color: #333;
text-align: left;
line-height: 1.4;
margin: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
min-width: 990px;
}

.wrapper {
width: 85%;
min-width: 970px;
max-width: 1500px;
margin: auto auto;
}

#logo {
background: #fff;
padding: 10px 0;
border-bottom: 1px solid #263c30;
}

#content {
background: #fff;
width: auto !important;
padding: 20px 10px;
overflow: hidden;
}

#header ul.menu {
margin: 0;
padding: 0;
list-style: none;
}

#header ul.menu li {
margin: 0 7px;
display: inline;
}

#header ul.menu li a {
padding-left: 20px;
background-image: url(/assets/runbb/images/headerlinks_sprite.png);
background-repeat: no-repeat;
display: inline-block;
line-height: 16px;
}

#logo ul.top_links {
font-weight: bold;
text-align: right;
margin: -10px 5px 0 0;
}

#logo ul.top_links a.search {
background-position: 0 0;
}

#logo ul.top_links a.memberlist {
background-position: 0 -20px;
}

#logo ul.top_links a.calendar {
background-position: 0 -40px;
}

#logo ul.top_links a.help {
background-position: 0 -60px;
}

#logo ul.top_links a.portal {
background-position: 0 -180px;
}

#panel .upper a.logout {
font-weight: bold;
background: url(/assets/runbb/images/headerlinks_sprite.png) right -80px no-repeat;
padding-right: 20px;
margin-left: 10px;
}

#panel .upper a.login,
#panel .upper a.lost_password {
background: url(/assets/runbb/images/headerlinks_sprite.png) 0 -100px no-repeat;
padding-left: 20px;
margin-left: 10px;
font-weight: bold;
}

#panel .upper a.register {
background: url(/assets/runbb/images/headerlinks_sprite.png) right -80px no-repeat;
padding-right: 20px;
margin-left: 10px;
font-weight: bold;
}

#panel .lower ul.panel_links {
float: left;
}

#panel .lower ul.panel_links a.usercp {
background-position: 0 -120px;
}

#panel .lower ul.panel_links a.modcp {
background-position: 0 -140px;
}

#panel .lower ul.panel_links a.admincp {
background-position: 0 -160px;
}

#panel .lower ul.user_links {
float: right;
}

#panel .lower ul.user_links li a {
padding: 0;
background-image: none;
}

#panel .upper {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
border-top: 1px solid #444;
border-bottom: 1px solid #000;
padding: 7px;
clear: both;
}

#panel .upper a:link,
#panel .upper a:visited,
#panel .upper a:hover,
#panel .upper a:active {
color: #fff;
}

#panel .lower {
background: #efefef;
color: #999;
border-top: 1px solid #fff;
border-bottom: 1px solid #ccc;
padding: 5px;
}

#panel .lower a:link,
#panel .lower a:visited,
#panel .lower a:hover,
#panel .lower a:active {
color: #666;
}

#search {
border: 0;
padding: 0;
margin: 0;
float: right;
vertical-align: middle;
}

#search input.button,
#search input.textbox {
border-color: #000;
}

#search input.button {
background: #0066a2 url(/assets/runbb/images/thead.png) top left repeat-x;
color: #fff;
}

#search input {
margin: -3px 0;
}

#quick_login .remember_me input {
vertical-align: middle;
margin: -3px 0 0 5px;
}

#footer {
clear: both;
}

#footer ul.menu {
margin: 0;
padding: 0;
list-style: none;
}

#footer ul.menu li {
margin: 0 5px;
display: inline;
}

#footer .upper {
background: #efefef;
border-top: 1px solid #bbb;
border-bottom: 1px solid #bbb;
padding: 6px;
font-size: 12px;
overflow: hidden;
}

#footer a:link,
#footer a:visited,
#footer a:hover,
#footer a:active {
color: #777;
}

#footer .upper .language {
float: right;
margin: -1px;
margin-left: 15px;
}

#footer .upper .language select {
border-color: #ccc;
}

#footer .upper .theme {
float: right;
margin: -1px;
margin-left: 15px;
}

#footer .upper .theme select {
border-color: #ccc;
}

#footer .upper ul.bottom_links {
float: left;
margin: 4px 0 0 0;
}

#footer .lower {
color: #666;
padding: 6px 6px 12px 6px;
overflow: hidden;
font-size: 11px;
}

#footer .lower a:link,
#footer .lower a:visited {
color: #444;
font-weight: bold;
}

#footer .lower a:hover,
#footer .lower a:active {
color: #333;
text-decoration: underline;
font-weight: bold;
}

#footer .lower #current_time {
float: right;
color: #888;
}

#debug {
float: right;
text-align: right;
margin-top: 20px;
font-size: 11px;
}

.scaleimages img {
max-width: 100%;
}

.forum_status {
height: 30px;
width: 30px;
background: url(/assets/runbb/images/forum_icon_sprite.png) no-repeat 0 0;
display: inline-block;
}

.forum_on {
background-position: 0 0;
}

.forum_off {
background-position: 0 -30px;
}

.forum_offlock {
background-position: 0 -60px;
}

.forum_offlink {
background-position: 0 -90px;
}

.subforumicon {
height: 10px;
width: 10px;
display: inline-block;
margin: 0 5px;
background: url(/assets/runbb/images/mini_status_sprite.png) no-repeat 0 0;
}

.subforum_minion {
background-position: 0 0;
}

.subforum_minioff {
background-position: 0 -10px;
}

.subforum_miniofflock {
background-position: 0 -20px;
}

.subforum_miniofflink {
background-position: 0 -30px;
}

table {
color: #333;
font-size: 13px;
}

.tborder {
background: #fff;
width: 100%;
margin: auto auto;
border: 1px solid #ccc;
padding: 1px;
}

.tfixed {
table-layout: fixed;
word-wrap: break-word;
}

.thead {
background: #0066a2 url(/assets/runbb/images/thead.png) top left repeat-x;
color: #ffffff;
border-bottom: 1px solid #263c30;
padding: 8px;
}

.thead a:link {
color: #ffffff;
text-decoration: none;
}

.thead a:visited {
color: #ffffff;
text-decoration: none;
}

.thead a:hover,
.thead a:active {
color: #ffffff;
text-decoration: underline;
}

.tcat {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
border-top: 1px solid #444;
border-bottom: 1px solid #000;
padding: 6px;
font-size: 12px;
}

.tcat a:link {
color: #fff;
}

.tcat a:visited {
color: #fff;
}

.tcat a:hover,
.tcat a:active {
color: #fff;
}

.trow1 {
background: #f5f5f5;
border: 1px solid;
border-color: #fff #ddd #ddd #fff;
}

.trow2 {
background: #efefef;
border: 1px solid;
border-color: #fff #ddd #ddd #fff;
}

.trow_shaded {
background: #ffdde0;
border: 1px solid;
border-color: #fff #ffb8be #ffb8be #fff;
}

.no_bottom_border {
border-bottom: 0;
}

.post.unapproved_post {
background: #ffdde0;
}

.post.unapproved_post .post_author {
border-bottom-color: #ffb8be;
}

.post.classic.unapproved_post .post_author {
border-color: #ffb8be;
}

.post.unapproved_post .post_controls {
border-top-color: #ffb8be;
}

.trow_deleted,
.post.deleted_post {
background: #E8DEFF;
}

.trow_selected,
tr.trow_selected td {
background: #FFFBD9;
color: #333;
border-right-color: #F7E86A;
border-bottom-color: #F7E86A;
}

.trow_selected a:link,
.trow_selected a:visited,
.trow_selected a:hover,
.trow_selected a:active {
color: #333;
}

.trow_sep {
background: #ddd;
color: #333;
border-bottom: 1px solid #c5c5c5;
padding: 6px;
font-size: 12px;
font-weight: bold;
}

.tfoot {
border-top: 1px solid #fff;
padding: 6px;
background: #ddd;
color: #666;
}

.tfoot a:link {
color: #444;
text-decoration: none;
}

.tfoot a:visited {
color: #444;
text-decoration: none;
}

.tfoot a:hover,
.tfoot a:active {
color: #444;
text-decoration: underline;
}

.thead input.textbox,
.thead select {
border: 1px solid #263c30;
}

.bottommenu {
background: #efefef;
color: #333;
border: 1px solid #4874a3;
padding: 10px;
}

.navigation {
color: #333;
font-size: 12px;
}

.navigation a:link {
text-decoration: none;
}

.navigation a:visited {
text-decoration: none;
}

.navigation a:hover,
.navigation a:active {
text-decoration: underline;
}

.navigation .active {
color: #333;
font-size: small;
font-weight: bold;
}

.smalltext {
font-size: 11px;
}

.largetext {
font-size: 16px;
font-weight: bold;
}

fieldset {
padding: 12px;
border: 1px solid #ddd;
margin: 0;
}

fieldset.trow1,
fieldset.trow2 {
border-color: #bbb;
}

fieldset.align_right {
text-align: right;
}

input.textbox {
background: #ffffff;
color: #333;
border: 1px solid #ccc;
padding: 3px;
outline: 0;
font-size: 13px;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
}

textarea {
background: #ffffff;
color: #333;
border: 1px solid #ccc;
padding: 2px;
line-height: 1.4;
outline: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
}

select {
background: #ffffff;
padding: 3px;
border: 1px solid #ccc;
outline: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
}

button,
input.button {
padding: 3px 8px;
cursor: pointer;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
background: #eee url(/assets/runbb/images/buttons_bg.png) repeat-x;
border: 1px solid #bbb;
color: #333;
outline: 0;
}

button:hover,
input.button:hover {
border-color: #aaa;
}

form {
margin: 0;
padding: 0;
}

input.error, textarea.error, select.error {
border: 1px solid #f30;
color: #f30;
}

input.valid, textarea.valid, select.valid {
border: 1px solid #0c0;
}

label.error {
color: #f30;
margin: 5px;
padding: 0px;
display: block;
font-weight: bold;
font-size: 11px;
}

form #message {
width: 500px;
}

.editor {
background: #f1f1f1;
border: 1px solid #ccc;
}

.editor_control_bar {
background: #fff;
border: 1px solid #ccc;
}

.post .editor_control_bar {
background: #f5f5f5;
}

.popup_menu {
background: #fff;
border: 1px solid #ccc;
}

.popup_menu .popup_item {
background: #efefef;
color: #333;
}

.popup_menu .popup_item:hover {
background: #0072BC;
color: #fff;
}

.trow_reputation_positive {
background: #ccffcc;
}

.trow_reputation_negative {
background: #ffcccc;
}

.reputation_positive {
color: green;
}

.reputation_neutral {
color: #444;
}

.reputation_negative {
color: red;
}

.repbox {
font-size:16px;
font-weight: bold;
padding:5px 7px 5px 7px;
}

._neutral {
background-color:#FAFAFA;
color: #999999;
border:1px solid #CCCCCC;
}

._minus {
background-color: #FDD2D1;
color: #CB0200;
border:1px solid #980201;
}

._plus {
background-color:#E8FCDC;
color: #008800;
border:1px solid #008800;
}

img {
border: none;
}

img.attachment {
border: 1px solid #E9E5D7;
padding: 2px;
}

hr {
background-color: #000000;
color: #000000;
height: 1px;
border: 0px;
}

.clear {
clear: both;
}

.float_left {
float: left;
}

.float_right {
float: right;
}

.hidden {
display: none;
float: none;
width: 1%;
}

.hiddenrow {
display: none;
}

.selectall {
background: #FFFBD9;
border-bottom: 1px solid #F7E86A;
color: #333;
text-align: center;
}

.expcolimage {
float: right;
width: auto;
vertical-align: middle;
margin-top: 3px;
}

.tcat_menu > .expcolimage {
margin-top: 0;
}

blockquote {
border: 1px solid #ccc;
margin: 0;
background: #fff;
padding: 10px;
}

blockquote cite {
font-weight: bold;
border-bottom: 1px solid #ccc;
font-style: normal;
display: block;
padding-bottom: 3px;
margin: 0 0 10px 0;
}

blockquote cite span {
float: right;
font-weight: normal;
font-size: 12px;
color: #666;
}

blockquote cite span.highlight {
float: none;
font-weight: bold;
padding-bottom: 0;
}

.codeblock {
background: #fff;
border: 1px solid #ccc;
padding: 10px;
}

.codeblock .title {
border-bottom: 1px solid #ccc;
font-weight: bold;
padding-bottom: 3px;
margin: 0 0 10px 0;
}

.codeblock code {
overflow: auto;
height: auto;
max-height: 200px;
display: block;
font-family: Monaco, Consolas, Courier, monospace;
font-size: 13px;
}

.smilie {
vertical-align: middle;
}

.smilie_pointer {
cursor: pointer;
}

.separator {
margin: 5px;
padding: 0;
height: 0px;
font-size: 1px;
list-style-type: none;
}

.popup_menu .popup_item_container {
margin: 1px;
text-align: left;
}

.popup_menu .popup_item {
display: block;
padding: 4px;
white-space: nowrap;
text-decoration: none;
}

.popup_menu a.popup_item:hover {
text-decoration: none;
}

.subject_new {
font-weight: bold;
}

.highlight {
background: #FFFFCC;
padding-top: 3px;
padding-bottom: 3px;
}

.pm_alert {
background: #FFF6BF;
border: 1px solid #FFD324;
text-align: center;
padding: 5px 20px;
margin-bottom: 15px;
font-size: 11px;
}

.red_alert {
background: #FBE3E4;
border: 1px solid #A5161A;
color: #A5161A;
text-align: center;
padding: 5px 20px;
margin-bottom: 15px;
font-size: 11px;
word-wrap: break-word;
}

.red_alert a:link,
.red_alert a:visited,
.red_alert a:hover,
.red_alert  a:active  {
color: #A5161A;
}

.high_warning {
color: #CC0000;
}

.moderate_warning {
color: #F3611B;
}

.low_warning {
color: #AE5700;
}

.online {
color: #15A018;
}

.offline {
color: #C7C7C7;
}

div.error {
padding: 5px 10px;
border-top: 2px solid #FFD324;
border-bottom: 2px solid #FFD324;
background: #FFF6BF;
font-size: 12px;
}

div.error p {
margin: 0;
color: #333;
font-weight: normal;
}

div.error p em {
font-style: normal;
font-weight: bold;
padding-left: 24px;
display: block;
color: #C00;
background: url(/assets/runbb/images/error.png) no-repeat 0;
}

div.error ul {
margin-left: 24px;
}

.pagination {
font-size: 11px;
padding-top: 10px;
margin-bottom: 5px;
}

.tfoot .pagination,
.tcat .pagination {
padding-top: 0;
}

.pagination .pages {
font-weight: bold;
}

.pagination .pagination_current,
.pagination a {
padding: 3px 6px;
margin-bottom: 3px;
}

.pagination a {
background: #f5f5f5;
border: 1px solid #ccc;
}

.pagination .pagination_current {
background: none;
color: #333;
border: none;
font-weight: bold;
}

.pagination a:hover {
background: #0072BC;
color: #fff;
border-color: #263c30;
text-decoration: none;
}

.pagination .go_page img {
margin-bottom: -4px;
}

.drop_go_page {
background: #f5f5f5;
padding: 4px;
}

.pagination_breadcrumb {
background-color: #efefef;
border: 1px solid #fff;
outline: 1px solid #ccc;
padding: 5px;
margin-top: 5px;
font-weight: normal;
}

.pagination_breadcrumb_link {
vertical-align: middle;
cursor: pointer;
}

.thread_legend,
.thread_legend dd {
margin: 0;
padding: 0;
}

.thread_legend dd {
padding-bottom: 4px;
margin-right: 15px;
}

.thread_legend img {
margin-right: 4px;
vertical-align: bottom;
}

.forum_legend,
.forum_legend dt,
.forum_legend dd {
margin: 0;
padding: 0;
}

.forum_legend dd {
float: left;
margin-right: 10px;
margin-top: 7px;
}

.forum_legend dt {
margin-right: 10px;
float: left;
}

.success_message {
color: #00b200;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

.error_message {
color: #C00;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

#posts_container {
padding: 0;
}

.ignored_post {
border-top: 3px solid #333;
padding: 15px;
}

.ignored_post .show_ignored_post {
margin-top: -15px;
}

.ignored_post .show_ignored_post a.button span {
background-position: 0 -400px;
}

.post {
overflow: hidden;
}

.post.classic {
padding-top: 15px;
}

.post .post_author {
border-bottom: 1px solid #ccc;
border-top: 2px solid #ccc;
background: #f5f5f5;
padding: 5px;
overflow: hidden;
}

.post.classic .post_author {
border: 1px solid #ddd;
float: left;
width: 15%;
margin: 0 1% 15px 0;
border-left: 0;
padding: 5px 1%;
}

.post .post_author .buddy_status {
vertical-align: middle;
margin-top: -4px;
}

.post .post_author div.author_avatar {
float: left;
margin-right: 3px;
}

.post.classic .post_author div.author_avatar {
float: none;
text-align: center;
margin-bottom: 8px;
}

.post .post_author div.author_avatar img {
padding: 5px;
border: 1px solid #ddd;
background: #fff;
}

.post .post_author div.author_information {
float: left;
padding: 6px 8px;
}

.post.classic .post_author div.author_information {
float: none;
padding: 0;
text-align: center;
}

.post .post_author div.author_statistics {
float: right;
font-size: 11px;
padding: 3px 10px 3px 5px;
color: #666;
line-height: 1.3;
}

.post.classic .post_author div.author_statistics {
border-top: 1px dotted #ccc;
margin: 6px 0 0 0;
padding: 6px 6px 3px 6px;
float: none;
}

.post .post_head {
font-size: 11px;
padding-bottom: 4px;
border-bottom: 1px dotted #ddd;
margin-bottom: 4px;
}

.post .post_head span.post_date {
color: #666;
}

.post .post_head span.edited_post {
font-size: 10px;
color: #999;
}

.post .post_head span.edited_post a {
color: #666;
}

.post_body {
font-size: 14px;
padding: 12px 0;
}

.post.classic .post_content {
float: left;
width: 79%;
padding: 0 1% 5px 1%;
}

.post_content {
padding: 9px 10px 5px 10px;
}

.post_content .signature {
margin-top: 5px;
border-top: 1px dotted #ddd;
padding: 10px 0 4px 0;
}

.post .post_meta {
margin: 4px 0;
font-size: 11px;
color: #999;
}

.post .post_meta a:link,
.post .post_meta a:visited {
color: #777;
}

.post .post_meta a:hover,
.post .post_meta a:active {
color: #777;
}

.post_controls {
clear: both;
background: #f5f5f5;
border-bottom: 1px solid #ccc;
padding: 5px;
overflow: hidden;
}

.postbit_buttons > a:link,
.postbit_buttons > a:hover,
.postbit_buttons > a:visited,
.postbit_buttons > a:active {
display: inline-block;
padding: 2px 5px;
margin: 2px;
font-size: 11px;
background: #eee url(/assets/runbb/images/buttons_bg.png) repeat-x;
border: 1px solid #ccc;
color: #555;
}

.postbit_buttons > a:hover {
border-color: #bbb;
}

.postbit_buttons a span {
padding-left: 20px;
display: inline-block;
height: 16px;
background-image: url(/assets/runbb/images/buttons_sprite.png);
background-repeat: no-repeat;
}

.postbit_buttons a.postbit_find span {
background-position: 0 0;
}

.postbit_buttons a.postbit_reputation_add span {
background-position: 0 -20px;
}

.postbit_buttons a.postbit_email span {
background-position: 0 -40px;
}

.postbit_buttons a.postbit_website span {
background-position: 0 -60px;
}

.postbit_buttons a.postbit_pm span {
background-position: 0 -80px;
}

.postbit_buttons a.postbit_quote span {
background-position: 0 -100px;
}

.postbit_buttons a.postbit_multiquote span {
background-position: 0 -120px;
}

.postbit_buttons a.postbit_multiquote_on span {
background-position: 0 -140px;
}

.postbit_buttons a.postbit_edit span {
background-position: 0 -160px;
}

.postbit_buttons a.postbit_qdelete span {
background-position: 0 -180px;
}

.postbit_buttons a.postbit_qrestore span {
background-position: 0 -200px;
}

.postbit_buttons a.postbit_report span {
background-position: 0 -220px;
}

.postbit_buttons a.postbit_warn span {
background-position: 0 -240px;
}

.postbit_buttons a.postbit_purgespammer span {
background-position: 0 -540px;
}

.postbit_buttons a.postbit_reply_pm span {
background-position: 0 -260px;
}

.postbit_buttons a.postbit_reply_all span {
background-position: 0 -280px;
}

.postbit_buttons a.postbit_forward_pm span {
background-position: 0 -300px;
}

.postbit_buttons a.postbit_delete_pm span {
background-position: 0 -320px;
}

a.button:link,
a.button:hover,
a.button:visited,
a.button:active {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
display: inline-block;
padding: 4px 8px;
margin: 2px 2px 6px 2px;
border: 1px solid #000;
font-size: 14px;
}

a.button.small_button {
font-size: 13px;
margin: 0;
padding: 3px 6px;
}

a.button span {
padding-left: 20px;
display: inline-block;
background-image: url(/assets/runbb/images/buttons_sprite.png);
background-repeat: no-repeat;
}

a.button.new_thread_button span {
background-position: 0 -340px;
}

a.button.new_reply_button span {
background-position: 0 -360px;
}

a.button.closed_button span {
background-position: 0 -380px;
}

a.button.rate_user_button span {
background-position: 0 -400px;
}

a.button.add_buddy_button span {
background-position: 0 -440px;
}

a.button.remove_buddy_button span {
background-position: 0 -480px;
}

a.button.add_ignore_button span {
background-position: 0 -460px;
}

a.button.remove_ignore_button span {
background-position: 0 -500px;
}

a.button.report_user_button span {
background-position: 0 -520px;
}

.quick_jump {
background: url(/assets/runbb/images/jump.png) no-repeat 0;
width: 13px;
height: 13px;
padding-left: 13px; /* amount of padding needed for image to fully show */
margin-top: -3px;
border: none;
}

.pollbar {
background: url(/assets/runbb/images/pollbar.png) top left repeat-x;
border: 1px solid #3f3f3f;
height: 10px;
}

.pollbar .percent {
display: none;
}

.posticons_label {
white-space: nowrap;
}

/** jGrowl Start **/

/** Special IE6 Style Positioning **/
.ie6 {
position: absolute;
}

.ie6.top-right {
right: auto;
bottom: auto;
left: expression( ( 0 - jGrowl.offsetWidth + ( document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth ) + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + \'px\' );
top: expression( ( 0 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + \'px\' );
}

.ie6.top-left {
left: expression( ( 0 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + \'px\' );
top: expression( ( 0 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + \'px\' );
}

.ie6.bottom-right {
left: expression( ( 0 - jGrowl.offsetWidth + ( document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth ) + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + \'px\' );
top: expression( ( 0 - jGrowl.offsetHeight + ( document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight ) + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + \'px\' );
}

.ie6.bottom-left {
left: expression( ( 0 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + \'px\' );
top: expression( ( 0 - jGrowl.offsetHeight + ( document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight ) + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + \'px\' );
}

.ie6.center {
left: expression( ( 0 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + \'px\' );
top: expression( ( 0 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + \'px\' );
width: 100%;
}

/** jGrowl Styling **/
.jGrowl {
z-index: 9999;
color: #ffffff;
font-size: 12px;
font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
position: fixed;
}
.jGrowl.top-left {
left: 0px;
top: 0px;
}
.jGrowl.top-right {
right: 0px;
top: 0px;
}
.jGrowl.bottom-left {
left: 0px;
bottom: 0px;
}
.jGrowl.bottom-right {
right: 0px;
bottom: 0px;
}
.jGrowl.center {
top: 0px;
width: 50%;
left: 25%;
}

/** Cross Browser Styling **/

.jGrowl.center .jGrowl-notification,
.jGrowl.center .jGrowl-closer {
margin-left: auto;
margin-right: auto;
}
.jGrowl-notification {
background-color: #000000;
opacity: 0.9;
filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=(0.9*100));
-ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=(0.9*100));
zoom: 1;
width: 250px;
padding: 10px;
margin: 10px;
text-align: left;
display: none;
border-radius: 5px;
word-break: break-all;
min-height: 40px;
}
.jGrowl-notification .ui-state-highlight,
.jGrowl-notification .ui-widget-content .ui-state-highlight,
.jGrowl-notification .ui-widget-header .ui-state-highlight {
border: 1px solid #000;
background: #000;
color: #fff;
}
.jGrowl-notification .jGrowl-header {
font-weight: bold;
font-size: .85em;
}
.jGrowl-notification .jGrowl-close {
background-color: transparent;
color: inherit;
border: none;
z-index: 99;
float: right;
font-weight: bold;
font-size: 1em;
cursor: pointer;
}
.jGrowl-closer {
background-color: #000000;
opacity: 0.9;
filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=(0.9*100));
-ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=(0.9*100));
zoom: 1;
width: 250px;
padding: 10px;
margin: 10px;
text-align: left;
display: none;
border-radius: 5px;
word-break: break-all;
padding-top: 4px;
padding-bottom: 4px;
cursor: pointer;
font-size: .9em;
font-weight: bold;
text-align: center;
}
.jGrowl-closer .ui-state-highlight,
.jGrowl-closer .ui-widget-content .ui-state-highlight,
.jGrowl-closer .ui-widget-header .ui-state-highlight {
border: 1px solid #000;
background: #000;
color: #fff;
}

/** Hide jGrowl when printing **/
@media print {
.jGrowl {
display: none;
}
}

/** jGrowl End **/

/** Modal Start **/

.modal {
display: none;
width: 400px;
text-align: left;
background: #fff;
-webkit-border-radius: 8px;
-moz-border-radius: 8px;
-o-border-radius: 8px;
-ms-border-radius: 8px;
border-radius: 8px;
-webkit-box-shadow: 0 0 10px #000;
-moz-box-shadow: 0 0 10px #000;
-o-box-shadow: 0 0 10px #000;
-ms-box-shadow: 0 0 10px #000;
box-shadow: 0 0 10px #000;
}

.modal a.close-modal {
position: absolute;
top: -12.5px;
right: -12.5px;
display: block;
width: 30px;
height: 30px;
text-indent: -9999px;
background: url(/assets/runbb/images/close.png) no-repeat 0 0;
}

.modal-spinner {
display: none;
width: 64px;
height: 64px;
position: fixed;
top: 50%;
left: 50%;
margin-right: -32px;
margin-top: -32px;
background: url(/assets/runbb/images/spinner_big.gif) no-repeat center center;
-webkit-border-radius: 8px;
-moz-border-radius: 8px;
-o-border-radius: 8px;
-ms-border-radius: 8px;
border-radius: 8px;
}

/** Modal End **/

/** Impromptu Start **/

/*! jQuery-Impromptu - v6.2.1 - 2015-05-10
* http://trentrichardson.com/Impromptu
* Copyright (c) 2015 Trent Richardson; Licensed MIT */

.jqifade{
position: absolute; 
background-color: #777777; 
}
iframe.jqifade{
display:block;
z-index:-1;
}
div.jqi{ 
width: 400px; 
max-width:90%;
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; 
position: absolute; 
background-color: #ffffff; 
font-size: 11px; 
text-align: left; 
border: solid 1px #eeeeee;
border-radius: 6px;
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
padding: 7px;
}
div.jqi .jqicontainer{ 
}
div.jqi .jqiclose{ 
position: absolute;
top: 4px; right: -2px; 
width: 18px; 
cursor: default; 
color: #bbbbbb; 
font-weight: bold; 
}
div.jqi .jqistate{
background-color: #fff;
}
div.jqi .jqititle{
padding: 5px 10px;
font-size: 16px; 
line-height: 20px; 
border-bottom: solid 1px #eeeeee;
}
div.jqi .jqimessage{ 
padding: 10px; 
line-height: 20px; 
color: #444444; 
overflow: auto;
}
div.jqi .jqibuttonshide{
display: none;
} 
div.jqi .jqibuttons{ 
text-align: right; 
margin: 0 -7px -7px -7px;
border-top: solid 1px #e4e4e4; 
background-color: #f4f4f4;
border-radius: 0 0 6px 6px;
-moz-border-radius: 0 0 6px 6px;
-webkit-border-radius: 0 0 6px 6px;
}
div.jqi .jqibuttons button{ 
margin: 0;
padding: 15px 20px;
background-color: transparent;
font-weight: normal; 
border: none;
border-left: solid 1px #e4e4e4; 
color: #777; 
font-weight: bold; 
font-size: 12px; 
}
div.jqi .jqibuttons button.jqidefaultbutton{
color: #489afe;
}
div.jqi .jqibuttons button:hover,
div.jqi .jqibuttons button:focus{
color: #287ade;
outline: none;
}
div.jqi .jqibuttons button[disabled]{
color: #aaa;
}
.jqiwarning .jqi .jqibuttons{ 
background-color: #b95656;
}

/* sub states */
div.jqi .jqiparentstate::after{ 
background-color: #777;
opacity: 0.6;
filter: alpha(opacity=60);
content: \'\';
position: absolute;
top:0;left:0;bottom:0;right:0;
border-radius: 6px;
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
}
div.jqi .jqisubstate{
position: absolute;
top:0;
left: 20%;
width: 60%;
padding: 7px;
border: solid 1px #eeeeee;
border-top: none;
border-radius: 0 0 6px 6px;
-moz-border-radius: 0 0 6px 6px;
-webkit-border-radius: 0 0 6px 6px;
}
div.jqi .jqisubstate .jqibuttons button{
padding: 10px 18px;
}

/* arrows for tooltips/tours */
.jqi .jqiarrow{ position: absolute; height: 0; width:0; line-height: 0; font-size: 0; border: solid 10px transparent;}

.jqi .jqiarrowtl{ left: 10px; top: -20px; border-bottom-color: #ffffff; }
.jqi .jqiarrowtc{ left: 50%; top: -20px; border-bottom-color: #ffffff; margin-left: -10px; }
.jqi .jqiarrowtr{ right: 10px; top: -20px; border-bottom-color: #ffffff; }

.jqi .jqiarrowbl{ left: 10px; bottom: -20px; border-top-color: #ffffff; }
.jqi .jqiarrowbc{ left: 50%; bottom: -20px; border-top-color: #ffffff; margin-left: -10px; }
.jqi .jqiarrowbr{ right: 10px; bottom: -20px; border-top-color: #ffffff; }

.jqi .jqiarrowlt{ left: -20px; top: 10px; border-right-color: #ffffff; }
.jqi .jqiarrowlm{ left: -20px; top: 50%; border-right-color: #ffffff; margin-top: -10px; }
.jqi .jqiarrowlb{ left: -20px; bottom: 10px; border-right-color: #ffffff; }

.jqi .jqiarrowrt{ right: -20px; top: 10px; border-left-color: #ffffff; }
.jqi .jqiarrowrm{ right: -20px; top: 50%; border-left-color: #ffffff; margin-top: -10px; }
.jqi .jqiarrowrb{ right: -20px; bottom: 10px; border-left-color: #ffffff; }

/** Impromptu End */
',
                'cachefile' => 'global.css',
                'lastmodified' => 1466356529,
            ),
            1 => 
            array (
                'sid' => 2,
                'name' => 'usercp.css',
                'tid' => 1,
                'attachedto' => 'usercp|usercp2|private',
                'stylesheet' => '.usercp_nav_item {
display: block;
padding: 1px 0 1px 23px;
background-image: url(/assets/runbb/images/usercp_sprite.png);
background-repeat: no-repeat;
}
.usercp_nav_composepm {
background-position: 0 0;
}

.usercp_nav_pmfolder {
background-position: 0 -20px;
}

.usercp_nav_sub_pmfolder {
padding-left: 40px;
background-position: 0 -40px;
}

.usercp_nav_trash_pmfolder {
padding-left: 40px;
background-position: 0 -60px;
}

.usercp_nav_pmtracking {
background-position: 0 -80px;
}

.usercp_nav_pmfolders {
background-position: 0 -100px;
}

.usercp_nav_profile {
background-position: 0 -120px;
}

.usercp_nav_email {
padding-left: 40px;
background-position: 0 -140px;
}

.usercp_nav_password {
padding-left: 40px;
background-position: 0 -160px;
}

.usercp_nav_username {
padding-left: 40px;
background-position: 0 -180px;
}

.usercp_nav_editsig {
padding-left: 40px;
background-position: 0 -200px;
}

.usercp_nav_avatar {
padding-left: 40px;
background-position: 0 -220px;
}

.usercp_nav_options {
background-position: 0 -240px;
}

.usercp_nav_usergroups {
background-position: 0 -260px;
}

.usercp_nav_editlists {
background-position: 0 -280px;
}

.usercp_nav_attachments {
background-position: 0 -300px;
}

.usercp_nav_drafts {
background-position: 0 -320px;
}

.usercp_nav_subscriptions {
background-position: 0 -340px;
}

.usercp_nav_fsubscriptions {
background-position: 0 -360px;
}

.usercp_nav_viewprofile {
background-position: 0 -380px;
}

.usercp_nav_home {
background-position: 0 -400px;
}

.usercp_notepad {
width: 99%;
}

.usercp_container {
margin: 5px;
padding: 8px;
border:1px solid #CCCCCC;
}

.pmspace {
float: right;
margin: -3px 5px;
}

.pmspace_container {
background: #fff;
border: 1px solid #ccc;
width: 100px;
display: inline-block;
}

.pmspace_used {
display: inline-block;
color: #fff;
margin: -1px;
}

.pmspace_used.low {
border: 1px solid #0c5a01;
background: #167203;
}

.pmspace_used.medium {
background: #ff862b;
border: 1px solid #d7611e;
}

.pmspace_used.high {
background: #e73c3c;
border: 1px solid #c00;
}

.pmspace_unused {
display: inline-block;
}

.pmspace_text {
padding: 1px 5px;
display: inline-block;
}',
                'cachefile' => 'usercp.css',
                'lastmodified' => 1466356529,
            ),
            2 => 
            array (
                'sid' => 3,
                'name' => 'modcp.css',
                'tid' => 1,
                'attachedto' => 'modcp',
                'stylesheet' => '.modcp_nav_item {
display: block;
padding: 1px 0 1px 23px;
background-image: url(/assets/runbb/images/modcp_sprite.png);
background-repeat: no-repeat;
}

.modcp_nav_home {
background-position: 0 0;
}

.modcp_nav_announcements {
background-position: 0 -20px;
}

.modcp_nav_reports {
background-position: 0 -40px;
}

.modcp_nav_modqueue {
background-position: 0 -60px;
}

.modcp_nav_modlogs {
background-position: 0 -80px;
}

.modcp_nav_editprofile {
background-position: 0 -100px;
}

.modcp_nav_banning {
background-position: 0 -120px;
}

.modcp_nav_warninglogs {
background-position: 0 -140px;
}

.modcp_nav_ipsearch {
background-position: 0 -160px;
}

.modqueue_message {
overflow: auto;
max-height: 250px;
}

.modqueue_controls {
width: 270px;
float: right;
text-align: center;
border: 1px solid #ccc;
background: #fff;
padding: 6px;
font-weight: bold;
}

.modqueue_controls label {
margin-right: 8px;
}

.label_radio_ignore,
.label_radio_delete,
.label_radio_approve {
font-weight: bold;
}

.modqueue_meta {
color: #444;
font-size: 95%;
margin-bottom: 8px;
}

.modqueue_mass {
list-style: none;
margin: 0;
width: 150px;
padding: 0;
}

.modqueue_mass li {
margin-bottom: 4px;
padding: 0;
}

.modqueue_mass li a {
display: block;
padding: 4px;
border: 1px solid transparent;
}

.modqueue_mass li a:hover {
background: #efefef;
border: 1px solid #ccc;
text-decoration: none;
}',
                'cachefile' => 'modcp.css',
                'lastmodified' => 1466356529,
            ),
            3 => 
            array (
                'sid' => 4,
                'name' => 'star_ratings.css',
                'tid' => 1,
                'attachedto' => 'forumdisplay|showthread',
                'stylesheet' => '.star_rating,
.star_rating li a:hover,
.star_rating .current_rating {
background: url(/assets/runbb/images/star_rating.png) left -1000px repeat-x;
vertical-align: middle;
}

.star_rating {
position: relative;
width:80px;
height:16px;
overflow: hidden;
list-style: none;
margin: 0;
padding: 0;
background-position: left top;
}

td .star_rating {
margin: auto;
}

.star_rating li {
display: inline;
}

.star_rating li a,
.star_rating .current_rating {
position: absolute;
text-indent: -1000px;
height: 16px;
line-height: 16px;
outline: none;
overflow: hidden;
border: none;
top:0;
left:0;
}

.star_rating_notrated li a:hover {
background-position: left bottom;
}

.star_rating li a.one_star {
width:20%;
z-index:6;
}

.star_rating li a.two_stars {
width:40%;
z-index:5;
}

.star_rating li a.three_stars {
width:60%;
z-index:4;
}

.star_rating li a.four_stars {
width:80%;
z-index:3;
}

.star_rating li a.five_stars {
width:100%;
z-index:2;
}

.star_rating .current_rating {
z-index:1;
background-position: left center;
}

.star_rating_success,
.success_message {
color: #00b200;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

.inline_rating {
background: url(/assets/runbb/images/star_rating.png) left -1000px repeat-x;
float: left;
vertical-align: middle;
padding-right: 5px;
}',
                'cachefile' => 'star_ratings.css',
                'lastmodified' => 1466356529,
            ),
            4 => 
            array (
                'sid' => 5,
                'name' => 'showthread.css',
                'tid' => 1,
                'attachedto' => 'showthread',
                'stylesheet' => 'ul.thread_tools,
ul.thread_tools li {
list-style: none;
padding: 0;
margin: 0;
}

ul.thread_tools li {
padding-left: 24px;
padding-bottom: 4px;
margin-bottom: 3px;
font-size: 11px;
background-image: url(/assets/runbb/images/showthread_sprite.png);
background-repeat: no-repeat;
}

ul.thread_tools li.printable {
background-position: 0 0;
}

ul.thread_tools li.sendthread {
background-position: 0 -20px;
}

ul.thread_tools li.subscription_add {
background-position: 0 -40px;
}

ul.thread_tools li.subscription_remove {
background-position: 0 -60px;
}

ul.thread_tools li.poll {
background-position: 0 -80px;
}

.showthread_spinner {
width: 100%;
margin: 0 auto;
display: block;
text-align: center;
padding: 20px;
}

',
                'cachefile' => 'showthread.css',
                'lastmodified' => 1466356529,
            ),
            5 => 
            array (
                'sid' => 6,
                'name' => 'thread_status.css',
                'tid' => 1,
                'attachedto' => 'forumdisplay|usercp|search',
                'stylesheet' => '.thread_status {
display: inline-block;
width: 16px;
height: 16px;
background-image: url(/assets/runbb/images/folders_sprite.png);
background-repeat: no-repeat;
}

.thread_status.dot_folder {
background-position: 0 0;
}

.thread_status.dot_hotfolder {
background-position: 0 -20px;
}

.thread_status.dot_hotlockfolder {
background-position: 0 -40px;
}

.thread_status.dot_lockfolder {
background-position: 0 -60px;
}

.thread_status.dot_newfolder {
background-position: 0 -80px;
}

.thread_status.dot_newhotfolder {
background-position: 0 -100px;
}

.thread_status.dot_newhotlockfolder {
background-position: 0 -120px;
}

.thread_status.dot_newlockfolder {
background-position: 0 -140px;
}

.thread_status.folder {
background-position: 0 -160px;
}

.thread_status.hotfolder {
background-position: 0 -180px;
}

.thread_status.hotlockfolder {
background-position: 0 -200px;
}

.thread_status.lockfolder {
background-position: 0 -220px;
}

.thread_status.movefolder {
background-position: 0 -240px;
}

.thread_status.newfolder {
background-position: 0 -260px;
}

.thread_status.newhotfolder {
background-position: 0 -280px;
}

.thread_status.newhotlockfolder {
background-position: 0 -300px;
}

.thread_status.newlockfolder {
background-position: 0 -320px;
}',
                'cachefile' => 'thread_status.css',
                'lastmodified' => 1466356529,
            ),
            6 => 
            array (
                'sid' => 7,
                'name' => 'css3.css',
                'tid' => 1,
                'attachedto' => '',
                'stylesheet' => 'tr td.trow1:first-child,
tr td.trow2:first-child,
tr td.trow_shaded:first-child {
border-left: 0;
}

tr td.trow1:last-child,
tr td.trow2:last-child,
tr td.trow_shaded:last-child {
border-right: 0;
}

.tborder {
-moz-border-radius: 7px;
-webkit-border-radius: 7px;
border-radius: 7px;
}

.tborder tbody tr:last-child > td {
border-bottom: 0;
}

.tborder tbody tr:last-child > td:first-child {
-moz-border-radius-bottomleft: 6px;
-webkit-border-bottom-left-radius: 6px;
border-bottom-left-radius: 6px;
}

.tborder tbody tr:last-child > td:last-child {
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-right-radius: 6px;
}

.thead {
-moz-border-radius-topleft: 6px;
-moz-border-radius-topright: 6px;
-webkit-border-top-left-radius: 6px;
-webkit-border-top-right-radius: 6px;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.thead_collapsed {
-moz-border-radius-bottomleft: 6px;
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-left-radius: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-left-radius: 6px;
border-bottom-right-radius: 6px;
}

.thead_left {
-moz-border-radius-topright: 0;
-webkit-border-top-right-radius: 0;
border-top-right-radius: 0;
}

.thead_right {
-moz-border-radius-topleft: 0;
-webkit-border-top-left-radius: 0;
border-top-left-radius: 0;
}

.tcat_menu {
-moz-border-radius: 0 !important;
-webkit-border-radius: 0 !important;
border-radius: 0 !important;
}

.tborder tbody:nth-last-child(2) .tcat_collapse_collapsed {
-moz-border-radius-bottomleft: 6px !important;
-moz-border-radius-bottomright: 6px !important;
-webkit-border-bottom-left-radius: 6px !important;
-webkit-border-bottom-right-radius: 6px !important;
border-bottom-left-radius: 6px !important;
border-bottom-right-radius: 6px !important;
}

button,
input.button,
input.textbox,
input.invalid_field,
input.valid_field,
select,
textarea,
.editor_control_bar,
blockquote,
.codeblock,
fieldset,
.pm_alert,
.red_alert,
.popup_menu,
.postbit_buttons > a,
a.button {
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
}

.post.classic .post_author {
-moz-border-radius: 0 6px 6px 0;
-webkit-border-radius: 0 6px 6px 0;
border-radius: 0 6px 6px 0;
}

.popup_menu .popup_item_container:first-child .popup_item {
-moz-border-radius-topleft: 6px;
-moz-border-radius-topright: 6px;
-webkit-border-top-left-radius: 6px;
-webkit-border-top-right-radius: 6px;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.popup_menu .popup_item_container:last-child .popup_item {
-moz-border-radius-bottomleft: 6px;
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-left-radius: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-left-radius: 6px;
border-bottom-right-radius: 6px;
}

.pagination a {
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
}

.pollbar {
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
border-radius: 3px;
}',
                'cachefile' => 'css3.css',
                'lastmodified' => 1466356529,
            ),
            7 => 
            array (
                'sid' => 8,
                'name' => 'color_black.css',
                'tid' => 2,
                'attachedto' => 'black',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #555;
}

#logo {
background: #202020 url(/assets/runbb/admin/images/colors/black_header.png) top left repeat-x;
border-bottom: 1px solid #000;
}

#header ul.menu li a {
color: #fff;
}

#panel .upper {
background: #dcdbdc url(/assets/runbb/admin/images/colors/black_tcat.png) repeat-x;
color: #000;
border-top: 1px solid #fff;
border-bottom: 1px solid #bbb;
}

#panel .upper a:link,
#panel .upper a:visited,
#panel .upper a:hover,
#panel .upper a:active {
color: #000;
}

#panel input.textbox {
border-color: #ccc;
}

#panel input.button {
background: #202121 url(/assets/runbb/admin/images/colors/black_thead.png) top left repeat-x;
color: #fff;
border-color: #000;
}

.thead {
background: #202121 url(/assets/runbb/admin/images/colors/black_thead.png) top left repeat-x;
border-bottom: 1px solid #000;
}

.tcat {
background: #dcdbdc url(/assets/runbb/admin/images/colors/black_tcat.png) repeat-x;
color: #000000;
border-top: 1px solid #fff;
border-bottom: 1px solid #bbb;
}

.tcat a:link,
.tcat a:visited,
.tcat a:hover,
.tcat a:active {
color: #000000;
}

.thead input.textbox,
.thead select {
border: 1px solid #111;
}

.popup_menu .popup_item:hover {
background: #333;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #333;
color: #fff;
}

.pagination a:hover {
background-color: #333;
color: #fff;
border-color: #000;
}

',
                'cachefile' => 'color_black.css',
                'lastmodified' => 1479803433,
            ),
            8 => 
            array (
                'sid' => 9,
                'name' => 'color_calm.css',
                'tid' => 2,
                'attachedto' => 'calm',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #48715b;
}

#logo {
background: #385947 url(/assets/runbb/admin/images/colors/calm_header.png) top left repeat-x;
border-bottom: 1px solid #263c30;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #385947 url(/assets/runbb/admin/images/colors/calm_thead.png) top left repeat-x;
}

.thead {
background: #385947 url(/assets/runbb/admin/images/colors/calm_thead.png) top left repeat-x;
color: #ffffff;
border-bottom: 1px solid #263c30;
}

.thead input.textbox,
.thead select {
border: 1px solid #263c30;
}

.popup_menu .popup_item:hover {
background: #48715b;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #48715b;
color: #fff;
}

.pagination a:hover {
background-color: #48715b;
color: #fff;
border-color: #263c30;
}

',
                'cachefile' => 'color_calm.css',
                'lastmodified' => 1479803439,
            ),
            9 => 
            array (
                'sid' => 10,
                'name' => 'color_dawn.css',
                'tid' => 2,
                'attachedto' => 'dawn',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #d56c2c;
}

#logo {
background: #d56c2c url(/assets/runbb/admin/images/colors/dawn_header.png) top left repeat-x;
border-bottom: 1px solid #011929;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #d56c2c url(/assets/runbb/admin/images/colors/dawn_thead.png) top left repeat-x;
}

.thead {
background: #d56c2c url(/assets/runbb/admin/images/colors/dawn_thead.png) top left repeat-x;
border-bottom: 1px solid #9a4e20;
}

.thead input.textbox,
.thead select {
border: 1px solid #9a4e20;
}

.popup_menu .popup_item:hover {
background: #f2a555;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #f2a555;
color: #fff;
}

.pagination a:hover {
background-color: #f2a555;
color: #fff;
border-color: #9a4e20;
}

',
                'cachefile' => 'color_dawn.css',
                'lastmodified' => 1479803446,
            ),
            10 => 
            array (
                'sid' => 11,
                'name' => 'color_earth.css',
                'tid' => 2,
                'attachedto' => 'earth',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #6d5545;
}

#logo {
background: #4a3a2e url(/assets/runbb/admin/images/colors/earth_header.png) top left repeat-x;
border-bottom: 1px solid #31261e;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #4a3b2f url(/assets/runbb/admin/images/colors/earth_thead.png) top left repeat-x;
}

.thead {
background: #4a3b2f url(/assets/runbb/admin/images/colors/earth_thead.png) top left repeat-x;
border-bottom: 1px solid #31261e;
}

.thead input.textbox,
.thead select {
border: 1px solid #31261e;
}

.popup_menu .popup_item:hover {
background: #6d5545;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #6d5545;
color: #fff;
}

.pagination a:hover {
background-color: #6d5545;
color: #fff;
border-color: #31261e;
}

',
                'cachefile' => 'color_earth.css',
                'lastmodified' => 1479803453,
            ),
            11 => 
            array (
                'sid' => 12,
                'name' => 'color_flame.css',
                'tid' => 2,
                'attachedto' => 'flame',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #a20000;
}

#logo {
background: #750000 url(/assets/runbb/admin/images/colors/flame_header.png) top left repeat-x;
border-bottom: 1px solid #550000;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #750000 url(/assets/runbb/admin/images/colors/flame_thead.png) top left repeat-x;
}

.thead {
background: #750000 url(/assets/runbb/admin/images/colors/flame_thead.png) top left repeat-x;
border-bottom: 1px solid #550000;
}

.thead input.textbox,
.thead select {
border: 1px solid #550000;
}

.popup_menu .popup_item:hover {
background: #a20000;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #a20000;
color: #fff;
}

.pagination a:hover {
background-color: #a20000;
color: #fff;
border-color: #550000;
}

',
                'cachefile' => 'color_flame.css',
                'lastmodified' => 1479803461,
            ),
            12 => 
            array (
                'sid' => 13,
                'name' => 'color_leaf.css',
                'tid' => 2,
                'attachedto' => 'leaf',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #5aaf33;
}

#logo {
background: #386e21 url(/assets/runbb/admin/images/colors/leaf_header.png) top left repeat-x;
border-bottom: 1px solid #234615;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #386e21 url(/assets/runbb/admin/images/colors/leaf_thead.png) top left repeat-x;
}

.thead {
background: #386e21 url(/assets/runbb/admin/images/colors/leaf_thead.png) top left repeat-x;
border-bottom: 1px solid #234615;
}

.thead input.textbox,
.thead select {
border: 1px solid #234615;
}

.popup_menu .popup_item:hover {
background: #5aaf33;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #5aaf33;
color: #fff;
}

.pagination a:hover {
background-color: #5aaf33;
color: #fff;
border-color: #234615;
}

',
                'cachefile' => 'color_leaf.css',
                'lastmodified' => 1479803469,
            ),
            13 => 
            array (
                'sid' => 14,
                'name' => 'color_night.css',
                'tid' => 2,
                'attachedto' => 'night',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #023a61;
}

#logo {
background: #022338 url(/assets/runbb/admin/images/colors/night_header.png) top left repeat-x;
border-bottom: 1px solid #011929;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #022338 url(/assets/runbb/admin/images/colors/night_thead.png) top left repeat-x;
}

.thead {
background: #022338 url(/assets/runbb/admin/images/colors/night_thead.png) top left repeat-x;
border-bottom: 1px solid #011929;
}

.thead input.textbox,
.thead select {
border: 1px solid #011929;
}

.popup_menu .popup_item:hover {
background: #023a61;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #023a61;
color: #fff;
}

.pagination a:hover {
background-color: #023a61;
color: #fff;
border-color: #011929;
}

',
                'cachefile' => 'color_night.css',
                'lastmodified' => 1479803477,
            ),
            14 => 
            array (
                'sid' => 15,
                'name' => 'color_sun.css',
                'tid' => 2,
                'attachedto' => 'sun',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #555;
}

#logo {
background: #ffdd4d url(/assets/runbb/admin/images/colors/sun_header.png) top left repeat-x;
border-bottom: 1px solid #111;
}

#header ul.menu li a {
color: #000;
}

#panel input.button {
background: #ffdd4d url(/assets/runbb/admin/images/colors/sun_thead.png) top left repeat-x;
color: #000;
}

.thead {
background: #ffdd4d url(/assets/runbb/admin/images/colors/sun_thead.png) top left repeat-x;
border-bottom: 1px solid #ffaf1a;
color: #000;
}

.thead a:link,
.thead a:visited,
.thead a:hover,
.thead a:active {
color: #000;
}

.thead input.textbox,
.thead select {
border: 1px solid #ffaf1a;
}

.popup_menu .popup_item:hover {
background: #ffea9c;
color: #000;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #ffea9c;
color: #fff;
}

.pagination a:hover {
background-color: #ffea9c;
color: #000;
border-color: #ffaf1a;
}

',
                'cachefile' => 'color_sun.css',
                'lastmodified' => 1479803495,
            ),
            15 => 
            array (
                'sid' => 16,
                'name' => 'color_twilight.css',
                'tid' => 2,
                'attachedto' => 'twilight',
                'stylesheet' => '
a:link,
a:visited,
a:hover,
a:active {
color: #426276;
}

#logo {
background: #273a46 url(/assets/runbb/admin/images/colors/twilight_header.png) top left repeat-x;
border-bottom: 1px solid #1d2b34;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #273a46 url(/assets/runbb/admin/images/colors/twilight_thead.png) top left repeat-x;
}

.thead {
background: #273a46 url(/assets/runbb/admin/images/colors/twilight_thead.png) top left repeat-x;
border-bottom: 1px solid #1d2b34;
}

.thead input.textbox,
.thead select {
border: 1px solid #1d2b34;
}

.popup_menu .popup_item:hover {
background: #426276;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #426276;
color: #fff;
}

.pagination a:hover {
background-color: #426276;
color: #fff;
border-color: #1d2b34;
}

',
                'cachefile' => 'color_twilight.css',
                'lastmodified' => 1479803501,
            ),
            16 => 
            array (
                'sid' => 17,
                'name' => 'color_water.css',
                'tid' => 2,
                'attachedto' => 'water',
                'stylesheet' => 'a:link,
a:visited,
a:hover,
a:active {
color: #2d9595;
}

#logo {
background: #1e6365 url(/assets/runbb/admin/images/colors/water_header.png) top left repeat-x;
border-bottom: 1px solid #133f41;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #1e6365 url(/assets/runbb/admin/images/colors/water_thead.png) top left repeat-x;
}

.thead {
background: #1e6365 url(/assets/runbb/admin/images/colors/water_thead.png) top left repeat-x;
border-bottom: 1px solid #133f41;
}

.thead input.textbox,
.thead select {
border: 1px solid #133f41;
}

.popup_menu .popup_item:hover {
background: #2d9595;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #2d9595;
color: #fff;
}

.pagination a:hover {
background-color: #2d9595;
color: #fff;
border-color: #133f41;
}

.panel-heading, .navbar {
/*background-color: #2d9595;*/
background-image: linear-gradient(to bottom, #009594 0%, #006564 100%) !important;
}

#panel .upper {
background-image: linear-gradient(to bottom, #009594 0%, #006564 100%) !important;
/*background: #045754 !important;*/
/*border-top: 1px solid #444;
border-bottom: 1px solid #000;*/
}',
                'cachefile' => 'color_water.css',
                'lastmodified' => 1479815181,
            ),
            17 => 
            array (
                'sid' => 36,
                'name' => 'global.css',
                'tid' => 2,
                'attachedto' => '',
                'stylesheet' => 'body {
background: #fff;
color: #333;
/*text-align: center;*/
/*line-height: 1.4;*/
margin: 0;
/*font-family: Tahoma, Verdana, Arial, Sans-Serif;*/
/*font-size: 13px;*/
overflow-y: scroll;
}

.panel-title a {
color: #FFF !important;
}
/*
a:link {
color: #0072BC;
text-decoration: none;
}

a:visited {
color: #0072BC;
text-decoration: none;
}

a:hover,
a:active {
color: #0072BC;
text-decoration: underline;
}
*/
#container {
color: #333;
text-align: left;
line-height: 1.4;
margin: 0;
min-width: 990px;

font-family: Tahoma, Verdana, Arial, Sans-Serif;
/*font-size: 13px;*/
}

.wrapper {
/*width: 85%;*/
min-width: 970px;
max-width: 1500px;
margin: auto auto;
}

#logo {
background: #fff;
padding: 10px 0;
border-bottom: 1px solid #263c30;
}

#content {
background: #fff;
width: auto !important;
padding: 20px 10px;
overflow: hidden;
}

#header ul.menu {
margin: 0;
padding: 0;
list-style: none;
}

#header ul.menu li {
margin: 0 7px;
display: inline;
}

#header ul.menu li a {
padding-left: 20px;
background-image: url(/assets/runbb/images/headerlinks_sprite.png);
background-repeat: no-repeat;
display: inline-block;
line-height: 16px;
}

#logo ul.top_links {
font-weight: bold;
text-align: right;
margin: -10px 5px 0 0;
}

#logo ul.top_links a.search {
background-position: 0 0;
}

#logo ul.top_links a.memberlist {
background-position: 0 -20px;
}

#logo ul.top_links a.calendar {
background-position: 0 -40px;
}

#logo ul.top_links a.help {
background-position: 0 -60px;
}

#logo ul.top_links a.portal {
background-position: 0 -180px;
}

#panel .upper a.logout {
font-weight: bold;
background: url(/assets/runbb/images/headerlinks_sprite.png) right -80px no-repeat;
padding-right: 20px;
margin-left: 10px;
}

#panel .upper a.login,
#panel .upper a.lost_password {
background: url(/assets/runbb/images/headerlinks_sprite.png) 0 -100px no-repeat;
padding-left: 20px;
margin-left: 10px;
font-weight: bold;
}

#panel .upper a.register {
background: url(/assets/runbb/images/headerlinks_sprite.png) right -80px no-repeat;
padding-right: 20px;
margin-left: 10px;
font-weight: bold;
}

#panel .lower ul.panel_links {
float: left;
}

#panel .lower ul.panel_links a.usercp {
background-position: 0 -120px;
}

#panel .lower ul.panel_links a.modcp {
background-position: 0 -140px;
}

#panel .lower ul.panel_links a.admincp {
background-position: 0 -160px;
}

#panel .lower ul.user_links {
float: right;
}

#panel .lower ul.user_links li a {
padding: 0;
background-image: none;
}

#panel .upper {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
border-top: 1px solid #444;
border-bottom: 1px solid #000;
padding: 7px;
clear: both;
}

#panel .upper a:link,
#panel .upper a:visited,
#panel .upper a:hover,
#panel .upper a:active {
color: #fff;
}

#panel .lower {
background: #efefef;
color: #999;
border-top: 1px solid #fff;
border-bottom: 1px solid #ccc;
padding: 5px;
}

#panel .lower a:link,
#panel .lower a:visited,
#panel .lower a:hover,
#panel .lower a:active {
color: #666;
}

#search {
border: 0;
padding: 0;
margin: 0;
float: right;
vertical-align: middle;
}

#search input.button,
#search input.textbox {
border-color: #000;
}

#search input.button {
background: #0066a2 url(/assets/runbb/images/thead.png) top left repeat-x;
color: #fff;
}

#search input {
margin: -3px 0;
}

#quick_login .remember_me input {
vertical-align: middle;
margin: -3px 0 0 5px;
}

#footer {
clear: both;
}

#footer ul.menu {
margin: 0;
padding: 0;
list-style: none;
}

#footer ul.menu li {
margin: 0 5px;
display: inline;
}

#footer .upper {
background: #efefef;
border-top: 1px solid #bbb;
border-bottom: 1px solid #bbb;
padding: 6px;
font-size: 12px;
overflow: hidden;
}

#footer a:link,
#footer a:visited,
#footer a:hover,
#footer a:active {
color: #777;
}

#footer .upper .language {
float: right;
margin: -1px;
margin-left: 15px;
}

#footer .upper .language select {
border-color: #ccc;
}

#footer .upper .theme {
float: right;
margin: -1px;
margin-left: 15px;
}

#footer .upper .theme select {
border-color: #ccc;
}

#footer .upper ul.bottom_links {
float: left;
margin: 4px 0 0 0;
}

#footer .lower {
color: #666;
padding: 6px 6px 12px 6px;
overflow: hidden;
font-size: 11px;
}

#footer .lower a:link,
#footer .lower a:visited {
color: #444;
font-weight: bold;
}

#footer .lower a:hover,
#footer .lower a:active {
color: #333;
text-decoration: underline;
font-weight: bold;
}

#footer .lower #current_time {
float: right;
color: #888;
}

#debug {
float: right;
text-align: right;
margin-top: 20px;
font-size: 11px;
}

.scaleimages img {
max-width: 100%;
}

.forum_status {
height: 30px;
width: 30px;
background: url(/assets/runbb/images/forum_icon_sprite.png) no-repeat 0 0;
display: inline-block;
}

.forum_on {
background-position: 0 0;
}

.forum_off {
background-position: 0 -30px;
}

.forum_offlock {
background-position: 0 -60px;
}

.forum_offlink {
background-position: 0 -90px;
}

.subforumicon {
height: 10px;
width: 10px;
display: inline-block;
margin: 0 5px;
background: url(/assets/runbb/images/mini_status_sprite.png) no-repeat 0 0;
}

.subforum_minion {
background-position: 0 0;
}

.subforum_minioff {
background-position: 0 -10px;
}

.subforum_miniofflock {
background-position: 0 -20px;
}

.subforum_miniofflink {
background-position: 0 -30px;
}

.tborder {
background: #fff;
width: 100%;
margin: auto auto;
border: 1px solid #ccc;
padding: 1px;
}

.tfixed {
table-layout: fixed;
word-wrap: break-word;
}

.thead {
background: #0066a2 url(/assets/runbb/images/thead.png) top left repeat-x;
color: #ffffff;
border-bottom: 1px solid #263c30;
padding: 8px;
}

.thead a:link {
color: #ffffff;
text-decoration: none;
}

.thead a:visited {
color: #ffffff;
text-decoration: none;
}

.thead a:hover,
.thead a:active {
color: #ffffff;
text-decoration: underline;
}

.tcat {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
border-top: 1px solid #444;
border-bottom: 1px solid #000;
padding: 6px;
font-size: 12px;
}

.tcat a:link {
color: #fff;
}

.tcat a:visited {
color: #fff;
}

.tcat a:hover,
.tcat a:active {
color: #fff;
}

.trow1 {
background: #f5f5f5;
border: 1px solid;
border-color: #fff #ddd #ddd #fff;
}

.trow2 {
background: #efefef;
border: 1px solid;
border-color: #fff #ddd #ddd #fff;
}

.trow_shaded {
background: #ffdde0;
border: 1px solid;
border-color: #fff #ffb8be #ffb8be #fff;
}

.no_bottom_border {
border-bottom: 0;
}

.post.unapproved_post {
background: #ffdde0;
}

.post.unapproved_post .post_author {
border-bottom-color: #ffb8be;
}

.post.classic.unapproved_post .post_author {
border-color: #ffb8be;
}

.post.unapproved_post .post_controls {
border-top-color: #ffb8be;
}

.trow_deleted,
.post.deleted_post {
background: #E8DEFF;
}

.trow_selected,
tr.trow_selected td {
background: #FFFBD9;
color: #333;
border-right-color: #F7E86A;
border-bottom-color: #F7E86A;
}

.trow_selected a:link,
.trow_selected a:visited,
.trow_selected a:hover,
.trow_selected a:active {
color: #333;
}

.trow_sep {
background: #ddd;
color: #333;
border-bottom: 1px solid #c5c5c5;
padding: 6px;
font-size: 12px;
font-weight: bold;
}

.tfoot {
border-top: 1px solid #fff;
padding: 6px;
background: #ddd;
color: #666;
}

.tfoot a:link {
color: #444;
text-decoration: none;
}

.tfoot a:visited {
color: #444;
text-decoration: none;
}

.tfoot a:hover,
.tfoot a:active {
color: #444;
text-decoration: underline;
}

.thead input.textbox,
.thead select {
border: 1px solid #263c30;
}

.bottommenu {
background: #efefef;
color: #333;
border: 1px solid #4874a3;
padding: 10px;
}

.navigation {
color: #333;
font-size: 12px;
}

.navigation a:link {
text-decoration: none;
}

.navigation a:visited {
text-decoration: none;
}

.navigation a:hover,
.navigation a:active {
text-decoration: underline;
}

.navigation .active {
color: #333;
font-size: small;
font-weight: bold;
}

.smalltext {
font-size: 11px;
}

.largetext {
font-size: 16px;
font-weight: bold;
}

fieldset {
padding: 12px;
border: 1px solid #ddd;
margin: 0;
}

fieldset.trow1,
fieldset.trow2 {
border-color: #bbb;
}

fieldset.align_right {
text-align: right;
}

input.textbox {
background: #ffffff;
color: #333;
border: 1px solid #ccc;
padding: 3px;
outline: 0;
font-size: 13px;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
}

textarea {
background: #ffffff;
color: #333;
border: 1px solid #ccc;
padding: 2px;
line-height: 1.4;
outline: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
}

select {
background: #ffffff;
padding: 3px;
border: 1px solid #ccc;
outline: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
}

button,
input.button {
padding: 3px 8px;
cursor: pointer;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
background: #eee url(/assets/runbb/images/buttons_bg.png) repeat-x;
border: 1px solid #bbb;
color: #333;
outline: 0;
}

button:hover,
input.button:hover {
border-color: #aaa;
}

form {
margin: 0;
padding: 0;
}

input.error, textarea.error, select.error {
border: 1px solid #f30;
color: #f30;
}

input.valid, textarea.valid, select.valid {
border: 1px solid #0c0;
}

label.error {
color: #f30;
margin: 5px;
padding: 0px;
display: block;
font-weight: bold;
font-size: 11px;
}

form #message {
width: 500px;
}

.editor {
background: #f1f1f1;
border: 1px solid #ccc;
}

.editor_control_bar {
background: #fff;
border: 1px solid #ccc;
}

.post .editor_control_bar {
background: #f5f5f5;
}

.popup_menu {
background: #fff;
border: 1px solid #ccc;
}

.popup_menu .popup_item {
background: #efefef;
color: #333;
}

.popup_menu .popup_item:hover {
background: #0072BC;
color: #fff;
}

.trow_reputation_positive {
background: #ccffcc;
}

.trow_reputation_negative {
background: #ffcccc;
}

.reputation_positive {
color: green;
}

.reputation_neutral {
color: #444;
}

.reputation_negative {
color: red;
}

.repbox {
font-size:16px;
font-weight: bold;
padding:5px 7px 5px 7px;
}

._neutral {
background-color:#FAFAFA;
color: #999999;
border:1px solid #CCCCCC;
}

._minus {
background-color: #FDD2D1;
color: #CB0200;
border:1px solid #980201;
}

._plus {
background-color:#E8FCDC;
color: #008800;
border:1px solid #008800;
}

img {
border: none;
}

img.attachment {
border: 1px solid #E9E5D7;
padding: 2px;
}

hr {
background-color: #000000;
color: #000000;
height: 1px;
border: 0px;
}

.clear {
clear: both;
}

.float_left {
float: left;
}

.float_right {
float: right;
}

.hidden {
display: none;
float: none;
width: 1%;
}

.hiddenrow {
display: none;
}

.selectall {
background: #FFFBD9;
border-bottom: 1px solid #F7E86A;
color: #333;
text-align: center;
}

.expcolimage {
float: right;
width: auto;
vertical-align: middle;
margin-top: 3px;
}

.tcat_menu > .expcolimage {
margin-top: 0;
}

blockquote {
border: 1px solid #ccc;
margin: 0;
background: #fff;
padding: 10px;
}

blockquote cite {
font-weight: bold;
border-bottom: 1px solid #ccc;
font-style: normal;
display: block;
padding-bottom: 3px;
margin: 0 0 10px 0;
}

blockquote cite span {
float: right;
font-weight: normal;
font-size: 12px;
color: #666;
}

blockquote cite span.highlight {
float: none;
font-weight: bold;
padding-bottom: 0;
}

.codeblock {
background: #fff;
border: 1px solid #ccc;
padding: 10px;
}

.codeblock .title {
border-bottom: 1px solid #ccc;
font-weight: bold;
padding-bottom: 3px;
margin: 0 0 10px 0;
}

.codeblock code {
overflow: auto;
height: auto;
max-height: 200px;
display: block;
font-family: Monaco, Consolas, Courier, monospace;
font-size: 13px;
}

.smilie {
vertical-align: middle;
}

.smilie_pointer {
cursor: pointer;
}

.separator {
margin: 5px;
padding: 0;
height: 0px;
font-size: 1px;
list-style-type: none;
}

.popup_menu .popup_item_container {
margin: 1px;
text-align: left;
}

.popup_menu .popup_item {
display: block;
padding: 4px;
white-space: nowrap;
text-decoration: none;
}

.popup_menu a.popup_item:hover {
text-decoration: none;
}

.subject_new {
font-weight: bold;
}

.highlight {
background: #FFFFCC;
padding-top: 3px;
padding-bottom: 3px;
}

.pm_alert {
background: #FFF6BF;
border: 1px solid #FFD324;
text-align: center;
padding: 5px 20px;
margin-bottom: 15px;
font-size: 11px;
}

.red_alert {
background: #FBE3E4;
border: 1px solid #A5161A;
color: #A5161A;
text-align: center;
padding: 5px 20px;
margin-bottom: 15px;
font-size: 11px;
word-wrap: break-word;
}

.red_alert a:link,
.red_alert a:visited,
.red_alert a:hover,
.red_alert  a:active {
color: #A5161A;
}

.high_warning {
color: #CC0000;
}

.moderate_warning {
color: #F3611B;
}

.low_warning {
color: #AE5700;
}

.online {
color: #15A018;
}

.offline {
color: #C7C7C7;
}

div.error {
padding: 5px 10px;
border-top: 2px solid #FFD324;
border-bottom: 2px solid #FFD324;
background: #FFF6BF;
font-size: 12px;
}

div.error p {
margin: 0;
color: #333;
font-weight: normal;
}

div.error p em {
font-style: normal;
font-weight: bold;
padding-left: 24px;
display: block;
color: #C00;
background: url(/assets/runbb/images/error.png) no-repeat 0;
}

div.error ul {
margin-left: 24px;
}

.pagination {
/*font-size: 11px;
padding-top: 10px;
margin-bottom: 5px;*/
margin: 7px;
}

.tfoot .pagination,
.tcat .pagination {
padding-top: 0;
}

.pagination .pages {
font-weight: bold;
}

.pagination .pagination_current,
.pagination a {
padding: 3px 6px;
margin-bottom: 3px;
}

.pagination a {
background: #f5f5f5;
border: 1px solid #ccc;
}

.pagination .pagination_current {
background: none;
color: #333;
border: none;
font-weight: bold;
}

.pagination a:hover {
background: #0072BC;
color: #fff;
border-color: #263c30;
text-decoration: none;
}

.pagination .go_page img {
margin-bottom: -4px;
}

.drop_go_page {
background: #f5f5f5;
padding: 4px;
}

.pagination_breadcrumb {
background-color: #efefef;
border: 1px solid #fff;
outline: 1px solid #ccc;
padding: 5px;
margin-top: 5px;
font-weight: normal;
}

.pagination_breadcrumb_link {
vertical-align: middle;
cursor: pointer;
}

.thread_legend,
.thread_legend dd {
margin: 0;
padding: 0;
}

.thread_legend dd {
padding-bottom: 4px;
margin-right: 15px;
}

.thread_legend img {
margin-right: 4px;
vertical-align: bottom;
}

.forum_legend,
.forum_legend dt,
.forum_legend dd {
margin: 0;
padding: 0;
}

.forum_legend dd {
float: left;
margin-right: 10px;
margin-top: 7px;
}

.forum_legend dt {
margin-right: 10px;
float: left;
}

.success_message {
color: #00b200;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

.error_message {
color: #C00;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

#posts_container {
padding: 0;
}

.ignored_post {
border-top: 3px solid #333;
padding: 15px;
}

.ignored_post .show_ignored_post {
margin-top: -15px;
}

.ignored_post .show_ignored_post a.button span {
background-position: 0 -400px;
}

.post {
overflow: hidden;
}

.post.classic {
padding-top: 15px;
}

.post .post_author {
border-bottom: 1px solid #ccc;
border-top: 2px solid #ccc;
background: #f5f5f5;
padding: 5px;
overflow: hidden;
}

.post.classic .post_author {
border: 1px solid #ddd;
float: left;
width: 15%;
margin: 0 1% 15px 0;
border-left: 0;
padding: 5px 1%;
}

.post .post_author .buddy_status {
vertical-align: middle;
margin-top: -4px;
}

.post .post_author div.author_avatar {
float: left;
margin-right: 3px;
}

.post.classic .post_author div.author_avatar {
float: none;
text-align: center;
margin-bottom: 8px;
}

.post .post_author div.author_avatar img {
padding: 5px;
border: 1px solid #ddd;
background: #fff;
}

.post .post_author div.author_information {
float: left;
padding: 6px 8px;
}

.post.classic .post_author div.author_information {
float: none;
padding: 0;
text-align: center;
}

.post .post_author div.author_statistics {
float: right;
font-size: 11px;
padding: 3px 10px 3px 5px;
color: #666;
line-height: 1.3;
}

.post.classic .post_author div.author_statistics {
border-top: 1px dotted #ccc;
margin: 6px 0 0 0;
padding: 6px 6px 3px 6px;
float: none;
}

.post .post_head {
font-size: 11px;
padding-bottom: 4px;
border-bottom: 1px dotted #ddd;
margin-bottom: 4px;
}

.post .post_head span.post_date {
color: #666;
}

.post .post_head span.edited_post {
font-size: 10px;
color: #999;
}

.post .post_head span.edited_post a {
color: #666;
}

.post_body {
font-size: 14px;
padding: 12px 0;
}

.post.classic .post_content {
float: left;
width: 79%;
padding: 0 1% 5px 1%;
}

.post_content {
padding: 9px 10px 5px 10px;
}

.post_content .signature {
margin-top: 5px;
border-top: 1px dotted #ddd;
padding: 10px 0 4px 0;
}

.post .post_meta {
margin: 4px 0;
font-size: 11px;
color: #999;
}

.post .post_meta a:link,
.post .post_meta a:visited {
color: #777;
}

.post .post_meta a:hover,
.post .post_meta a:active {
color: #777;
}

.post_controls {
clear: both;
background: #f5f5f5;
border-bottom: 1px solid #ccc;
padding: 5px;
overflow: hidden;
}

.postbit_buttons > a:link,
.postbit_buttons > a:hover,
.postbit_buttons > a:visited,
.postbit_buttons > a:active {
display: inline-block;
padding: 2px 5px;
margin: 2px;
font-size: 11px;
background: #eee url(/assets/runbb/images/buttons_bg.png) repeat-x;
border: 1px solid #ccc;
color: #555;
}

.postbit_buttons > a:hover {
border-color: #bbb;
}

.postbit_buttons a span {
padding-left: 20px;
display: inline-block;
height: 16px;
background-image: url(/assets/runbb/images/buttons_sprite.png);
background-repeat: no-repeat;
}

.postbit_buttons a.postbit_find span {
background-position: 0 0;
}

.postbit_buttons a.postbit_reputation_add span {
background-position: 0 -20px;
}

.postbit_buttons a.postbit_email span {
background-position: 0 -40px;
}

.postbit_buttons a.postbit_website span {
background-position: 0 -60px;
}

.postbit_buttons a.postbit_pm span {
background-position: 0 -80px;
}

.postbit_buttons a.postbit_quote span {
background-position: 0 -100px;
}

.postbit_buttons a.postbit_multiquote span {
background-position: 0 -120px;
}

.postbit_buttons a.postbit_multiquote_on span {
background-position: 0 -140px;
}

.postbit_buttons a.postbit_edit span {
background-position: 0 -160px;
}

.postbit_buttons a.postbit_qdelete span {
background-position: 0 -180px;
}

.postbit_buttons a.postbit_qrestore span {
background-position: 0 -200px;
}

.postbit_buttons a.postbit_report span {
background-position: 0 -220px;
}

.postbit_buttons a.postbit_warn span {
background-position: 0 -240px;
}

.postbit_buttons a.postbit_purgespammer span {
background-position: 0 -540px;
}

.postbit_buttons a.postbit_reply_pm span {
background-position: 0 -260px;
}

.postbit_buttons a.postbit_reply_all span {
background-position: 0 -280px;
}

.postbit_buttons a.postbit_forward_pm span {
background-position: 0 -300px;
}

.postbit_buttons a.postbit_delete_pm span {
background-position: 0 -320px;
}

a.button:link,
a.button:hover,
a.button:visited,
a.button:active {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
display: inline-block;
padding: 4px 8px;
margin: 2px 2px 6px 2px;
border: 1px solid #000;
font-size: 14px;
}

a.button.small_button {
font-size: 13px;
margin: 0;
padding: 3px 6px;
}

a.button span {
padding-left: 20px;
display: inline-block;
background-image: url(/assets/runbb/images/buttons_sprite.png);
background-repeat: no-repeat;
}

a.button.new_thread_button span {
background-position: 0 -340px;
}

a.button.new_reply_button span {
background-position: 0 -360px;
}

a.button.closed_button span {
background-position: 0 -380px;
}

a.button.rate_user_button span {
background-position: 0 -400px;
}

a.button.add_buddy_button span {
background-position: 0 -440px;
}

a.button.remove_buddy_button span {
background-position: 0 -480px;
}

a.button.add_ignore_button span {
background-position: 0 -460px;
}

a.button.remove_ignore_button span {
background-position: 0 -500px;
}

a.button.report_user_button span {
background-position: 0 -520px;
}

.quick_jump {
background: url(/assets/runbb/images/jump.png) no-repeat 0;
width: 13px;
height: 13px;
padding-left: 13px; /* amount of padding needed for image to fully show */
margin-top: -3px;
border: none;
}

.pollbar {
background: url(/assets/runbb/images/pollbar.png) top left repeat-x;
border: 1px solid #3f3f3f;
height: 10px;
}

.pollbar .percent {
display: none;
}

.posticons_label {
white-space: nowrap;
}

.expander.collapse,
.expander.collapse_collapsed {
font: normal normal normal 21px FontAwesome;
display: inline-block;
text-decoration: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
line-height: 0.7;
}

.expander.collapse:before {
content: \'\\f205\';
}

.expander.collapse_collapsed:before {
content: \'\\f204\';
}

[data-toggle=collapse] .fa.toggle:before {
content: "\\f205";
}

[data-toggle=collapse].collapsed .fa.toggle:before {
content: "\\f204";
}

/* sceditor buttons in bootstrap
https://github.com/samclarke/SCEditor/issues/308
*/
.sceditor-container { font-weight: normal }
.sceditor-button { box-sizing: content-box; -webkit-box-sizing: content-box; -moz-box-sizing: content-box; }
',
                'cachefile' => 'global.css',
                'lastmodified' => 1479815023,
            ),
            18 => 
            array (
                'sid' => 37,
                'name' => 'usercp.css',
                'tid' => 2,
                'attachedto' => 'usercp|usercp2|private',
                'stylesheet' => '
.usercp_nav_item {
display: block;
padding: 1px 0 1px 23px;
background-image: url(/assets/runbb/images/usercp_sprite.png);
background-repeat: no-repeat;
}

.usercp_nav_composepm {
background-position: 0 0;
}

.usercp_nav_pmfolder {
background-position: 0 -20px;
}

.usercp_nav_sub_pmfolder {
padding-left: 40px;
background-position: 0 -40px;
}

.usercp_nav_trash_pmfolder {
padding-left: 40px;
background-position: 0 -60px;
}

.usercp_nav_pmtracking {
background-position: 0 -80px;
}

.usercp_nav_pmfolders {
background-position: 0 -100px;
}

.usercp_nav_profile {
background-position: 0 -120px;
}

.usercp_nav_email {
padding-left: 40px;
background-position: 0 -140px;
}

.usercp_nav_password {
padding-left: 40px;
background-position: 0 -160px;
}

.usercp_nav_username {
padding-left: 40px;
background-position: 0 -180px;
}

.usercp_nav_editsig {
padding-left: 40px;
background-position: 0 -200px;
}

.usercp_nav_avatar {
padding-left: 40px;
background-position: 0 -220px;
}

.usercp_nav_options {
background-position: 0 -240px;
}

.usercp_nav_usergroups {
background-position: 0 -260px;
}

.usercp_nav_editlists {
background-position: 0 -280px;
}

.usercp_nav_attachments {
background-position: 0 -300px;
}

.usercp_nav_drafts {
background-position: 0 -320px;
}

.usercp_nav_subscriptions {
background-position: 0 -340px;
}

.usercp_nav_fsubscriptions {
background-position: 0 -360px;
}

.usercp_nav_viewprofile {
background-position: 0 -380px;
}

.usercp_nav_home {
background-position: 0 -400px;
}

.usercp_notepad {
width: 99%;
}

.usercp_container {
margin: 5px;
padding: 8px;
border:1px solid #CCCCCC;
}

.pmspace {
float: right;
margin: -3px 5px;
}

.pmspace_container {
background: #fff;
border: 1px solid #ccc;
width: 100px;
display: inline-block;
}

.pmspace_used {
display: inline-block;
color: #fff;
margin: -1px;
}

.pmspace_used.low {
border: 1px solid #0c5a01;
background: #167203;
}

.pmspace_used.medium {
background: #ff862b;
border: 1px solid #d7611e;
}

.pmspace_used.high {
background: #e73c3c;
border: 1px solid #c00;
}

.pmspace_unused {
display: inline-block;
}

.pmspace_text {
padding: 1px 5px;
display: inline-block;
}

',
                'cachefile' => 'usercp.css',
                'lastmodified' => 1479803389,
            ),
            19 => 
            array (
                'sid' => 38,
                'name' => 'modcp.css',
                'tid' => 2,
                'attachedto' => 'modcp',
                'stylesheet' => '
.modcp_nav_item {
display: block;
padding: 1px 0 1px 23px;
background-image: url(/assets/runbb/images/modcp_sprite.png);
background-repeat: no-repeat;
}

.modcp_nav_home {
background-position: 0 0;
}

.modcp_nav_announcements {
background-position: 0 -20px;
}

.modcp_nav_reports {
background-position: 0 -40px;
}

.modcp_nav_modqueue {
background-position: 0 -60px;
}

.modcp_nav_modlogs {
background-position: 0 -80px;
}

.modcp_nav_editprofile {
background-position: 0 -100px;
}

.modcp_nav_banning {
background-position: 0 -120px;
}

.modcp_nav_warninglogs {
background-position: 0 -140px;
}

.modcp_nav_ipsearch {
background-position: 0 -160px;
}

.modqueue_message {
overflow: auto;
max-height: 250px;
}

.modqueue_controls {
width: 270px;
float: right;
text-align: center;
border: 1px solid #ccc;
background: #fff;
padding: 6px;
font-weight: bold;
}

.modqueue_controls label {
margin-right: 8px;
}

.label_radio_ignore,
.label_radio_delete,
.label_radio_approve {
font-weight: bold;
}

.modqueue_meta {
color: #444;
font-size: 95%;
margin-bottom: 8px;
}

.modqueue_mass {
list-style: none;
margin: 0;
width: 150px;
padding: 0;
}

.modqueue_mass li {
margin-bottom: 4px;
padding: 0;
}

.modqueue_mass li a {
display: block;
padding: 4px;
border: 1px solid transparent;
}

.modqueue_mass li a:hover {
background: #efefef;
border: 1px solid #ccc;
text-decoration: none;
}

',
                'cachefile' => 'modcp.css',
                'lastmodified' => 1479803395,
            ),
            20 => 
            array (
                'sid' => 39,
                'name' => 'star_ratings.css',
                'tid' => 2,
                'attachedto' => 'forumdisplay|showthread',
                'stylesheet' => '.star_rating,
.star_rating li a:hover,
.star_rating .current_rating {
background: url(/assets/runbb/images/star_rating.png) left -1000px repeat-x;
vertical-align: middle;
}

.star_rating {
position: relative;
width:80px;
height:16px;
overflow: hidden;
list-style: none;
/*margin: 0;*/
padding: 0;
background-position: left top;

display: inline-block;
margin: 0 auto !important;
}

td .star_rating {
margin: auto;
}

.star_rating li {
display: inline;
}

.star_rating li a,
.star_rating .current_rating {
position: absolute;
text-indent: -1000px;
height: 16px;
line-height: 16px;
outline: none;
overflow: hidden;
border: none;
top:0;
left:0;
}

.star_rating_notrated li a:hover {
background-position: left bottom;
}

.star_rating li a.one_star {
width:20%;
z-index:6;
}

.star_rating li a.two_stars {
width:40%;
z-index:5;
}

.star_rating li a.three_stars {
width:60%;
z-index:4;
}

.star_rating li a.four_stars {
width:80%;
z-index:3;
}

.star_rating li a.five_stars {
width:100%;
z-index:2;
}

.star_rating .current_rating {
z-index:1;
background-position: left center;
}

.star_rating_success,
.success_message {
color: #00b200;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

.inline_rating {
background: url(/assets/runbb/images/star_rating.png) left -1000px repeat-x;
float: left;
vertical-align: middle;
padding-right: 5px;
}',
                'cachefile' => 'star_ratings.css',
                'lastmodified' => 1479804174,
            ),
            21 => 
            array (
                'sid' => 40,
                'name' => 'showthread.css',
                'tid' => 2,
                'attachedto' => 'showthread',
                'stylesheet' => '
ul.thread_tools,
ul.thread_tools li {
list-style: none;
padding: 0;
margin: 0;
}

ul.thread_tools li {
padding-left: 24px;
padding-bottom: 4px;
margin-bottom: 3px;
font-size: 11px;
background-image: url(/assets/runbb/images/showthread_sprite.png);
background-repeat: no-repeat;
}

ul.thread_tools li.printable {
background-position: 0 0;
}

ul.thread_tools li.sendthread {
background-position: 0 -20px;
}

ul.thread_tools li.subscription_add {
background-position: 0 -40px;
}

ul.thread_tools li.subscription_remove {
background-position: 0 -60px;
}

ul.thread_tools li.poll {
background-position: 0 -80px;
}

.showthread_spinner {
margin: 0 auto;
display: block;
text-align: center;
padding: 20px;

width: 100%;
}

',
                'cachefile' => 'showthread.css',
                'lastmodified' => 1479803408,
            ),
            22 => 
            array (
                'sid' => 41,
                'name' => 'thread_status.css',
                'tid' => 2,
                'attachedto' => 'forumdisplay|usercp|search',
                'stylesheet' => '.thread_status {
display: inline-block;
width: 16px;
height: 16px;
background-image: url(/assets/runbb/images/folders_sprite.png);
background-repeat: no-repeat;
}

.thread_status.dot_folder {
background-position: 0 0;
}

.thread_status.dot_hotfolder {
background-position: 0 -20px;
}

.thread_status.dot_hotlockfolder {
background-position: 0 -40px;
}

.thread_status.dot_lockfolder {
background-position: 0 -60px;
}

.thread_status.dot_newfolder {
background-position: 0 -80px;
}

.thread_status.dot_newhotfolder {
background-position: 0 -100px;
}

.thread_status.dot_newhotlockfolder {
background-position: 0 -120px;
}

.thread_status.dot_newlockfolder {
background-position: 0 -140px;
}

.thread_status.folder {
background-position: 0 -160px;
}

.thread_status.hotfolder {
background-position: 0 -180px;
}

.thread_status.hotlockfolder {
background-position: 0 -200px;
}

.thread_status.lockfolder {
background-position: 0 -220px;
}

.thread_status.movefolder {
background-position: 0 -240px;
}

.thread_status.newfolder {
background-position: 0 -260px;
}

.thread_status.newhotfolder {
background-position: 0 -280px;
}

.thread_status.newhotlockfolder {
background-position: 0 -300px;
}

.thread_status.newlockfolder {
background-position: 0 -320px;
}',
                'cachefile' => 'thread_status.css',
                'lastmodified' => 1479804261,
            ),
            23 => 
            array (
                'sid' => 42,
                'name' => 'css3.css',
                'tid' => 2,
                'attachedto' => '',
                'stylesheet' => '
tr td.trow1:first-child,
tr td.trow2:first-child,
tr td.trow_shaded:first-child {
border-left: 0;
}

tr td.trow1:last-child,
tr td.trow2:last-child,
tr td.trow_shaded:last-child {
border-right: 0;
}

.tborder {
-moz-border-radius: 7px;
-webkit-border-radius: 7px;
border-radius: 7px;
}

.tborder tbody tr:last-child > td {
border-bottom: 0;
}

.tborder tbody tr:last-child > td:first-child {
-moz-border-radius-bottomleft: 6px;
-webkit-border-bottom-left-radius: 6px;
border-bottom-left-radius: 6px;
}

.tborder tbody tr:last-child > td:last-child {
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-right-radius: 6px;
}

.thead {
-moz-border-radius-topleft: 6px;
-moz-border-radius-topright: 6px;
-webkit-border-top-left-radius: 6px;
-webkit-border-top-right-radius: 6px;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.thead_collapsed {
-moz-border-radius-bottomleft: 6px;
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-left-radius: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-left-radius: 6px;
border-bottom-right-radius: 6px;
}

.thead_left {
-moz-border-radius-topright: 0;
-webkit-border-top-right-radius: 0;
border-top-right-radius: 0;
}

.thead_right {
-moz-border-radius-topleft: 0;
-webkit-border-top-left-radius: 0;
border-top-left-radius: 0;
}

.tcat_menu {
-moz-border-radius: 0 !important;
-webkit-border-radius: 0 !important;
border-radius: 0 !important;
}

.tborder tbody:nth-last-child(2) .tcat_collapse_collapsed {
-moz-border-radius-bottomleft: 6px !important;
-moz-border-radius-bottomright: 6px !important;
-webkit-border-bottom-left-radius: 6px !important;
-webkit-border-bottom-right-radius: 6px !important;
border-bottom-left-radius: 6px !important;
border-bottom-right-radius: 6px !important;
}

button,
input.button,
input.textbox,
input.invalid_field,
input.valid_field,
select,
textarea,
.editor_control_bar,
blockquote,
.codeblock,
fieldset,
.pm_alert,
.red_alert,
.popup_menu,
.postbit_buttons > a,
a.button {
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
}

.post.classic .post_author {
-moz-border-radius: 0 6px 6px 0;
-webkit-border-radius: 0 6px 6px 0;
border-radius: 0 6px 6px 0;
}

.popup_menu .popup_item_container:first-child .popup_item {
-moz-border-radius-topleft: 6px;
-moz-border-radius-topright: 6px;
-webkit-border-top-left-radius: 6px;
-webkit-border-top-right-radius: 6px;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.popup_menu .popup_item_container:last-child .popup_item {
-moz-border-radius-bottomleft: 6px;
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-left-radius: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-left-radius: 6px;
border-bottom-right-radius: 6px;
}

.pagination a {
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
}

.pollbar {
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
border-radius: 3px;
}

',
                'cachefile' => 'css3.css',
                'lastmodified' => 1479803423,
            ),
            24 => 
            array (
                'sid' => 43,
                'name' => 'global.css',
                'tid' => 3,
                'attachedto' => '',
                'stylesheet' => 'body {
background: #fff;
color: #333;
/*text-align: center;*/
/*line-height: 1.4;*/
margin: 0;
/*font-family: Tahoma, Verdana, Arial, Sans-Serif;*/
/*font-size: 13px;*/
overflow-y: scroll;
}

.panel-title a {
color: #FFF !important;
}
/*
a:link {
color: #0072BC;
text-decoration: none;
}

a:visited {
color: #0072BC;
text-decoration: none;
}

a:hover,
a:active {
color: #0072BC;
text-decoration: underline;
}
*/
#container {
color: #333;
text-align: left;
line-height: 1.4;
margin: 0;
min-width: 990px;

font-family: Tahoma, Verdana, Arial, Sans-Serif;
/*font-size: 13px;*/
}

.wrapper {
/*width: 85%;*/
min-width: 970px;
max-width: 1500px;
margin: auto auto;
}

#logo {
background: #fff;
padding: 10px 0;
border-bottom: 1px solid #263c30;
}

#content {
background: #fff;
width: auto !important;
padding: 20px 10px;
overflow: hidden;
}

#header ul.menu {
margin: 0;
padding: 0;
list-style: none;
}

#header ul.menu li {
margin: 0 7px;
display: inline;
}

#header ul.menu li a {
padding-left: 20px;
background-image: url(/assets/runbb/images/headerlinks_sprite.png);
background-repeat: no-repeat;
display: inline-block;
line-height: 16px;
}

#logo ul.top_links {
font-weight: bold;
text-align: right;
margin: -10px 5px 0 0;
}

#logo ul.top_links a.search {
background-position: 0 0;
}

#logo ul.top_links a.memberlist {
background-position: 0 -20px;
}

#logo ul.top_links a.calendar {
background-position: 0 -40px;
}

#logo ul.top_links a.help {
background-position: 0 -60px;
}

#logo ul.top_links a.portal {
background-position: 0 -180px;
}

#panel .upper a.logout {
font-weight: bold;
background: url(/assets/runbb/images/headerlinks_sprite.png) right -80px no-repeat;
padding-right: 20px;
margin-left: 10px;
}

#panel .upper a.login,
#panel .upper a.lost_password {
background: url(/assets/runbb/images/headerlinks_sprite.png) 0 -100px no-repeat;
padding-left: 20px;
margin-left: 10px;
font-weight: bold;
}

#panel .upper a.register {
background: url(/assets/runbb/images/headerlinks_sprite.png) right -80px no-repeat;
padding-right: 20px;
margin-left: 10px;
font-weight: bold;
}

#panel .lower ul.panel_links {
float: left;
}

#panel .lower ul.panel_links a.usercp {
background-position: 0 -120px;
}

#panel .lower ul.panel_links a.modcp {
background-position: 0 -140px;
}

#panel .lower ul.panel_links a.admincp {
background-position: 0 -160px;
}

#panel .lower ul.user_links {
float: right;
}

#panel .lower ul.user_links li a {
padding: 0;
background-image: none;
}

#panel .upper {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
border-top: 1px solid #444;
border-bottom: 1px solid #000;
padding: 7px;
clear: both;
}

#panel .upper a:link,
#panel .upper a:visited,
#panel .upper a:hover,
#panel .upper a:active {
color: #fff;
}

#panel .lower {
background: #efefef;
color: #999;
border-top: 1px solid #fff;
border-bottom: 1px solid #ccc;
padding: 5px;
}

#panel .lower a:link,
#panel .lower a:visited,
#panel .lower a:hover,
#panel .lower a:active {
color: #666;
}

#search {
border: 0;
padding: 0;
margin: 0;
float: right;
vertical-align: middle;
}

#search input.button,
#search input.textbox {
border-color: #000;
}

#search input.button {
background: #0066a2 url(/assets/runbb/images/thead.png) top left repeat-x;
color: #fff;
}

#search input {
margin: -3px 0;
}

#quick_login .remember_me input {
vertical-align: middle;
margin: -3px 0 0 5px;
}

#footer {
clear: both;
}

#footer ul.menu {
margin: 0;
padding: 0;
list-style: none;
}

#footer ul.menu li {
margin: 0 5px;
display: inline;
}

#footer .upper {
background: #efefef;
border-top: 1px solid #bbb;
border-bottom: 1px solid #bbb;
padding: 6px;
font-size: 12px;
overflow: hidden;
}

#footer a:link,
#footer a:visited,
#footer a:hover,
#footer a:active {
color: #777;
}

#footer .upper .language {
float: right;
margin: -1px;
margin-left: 15px;
}

#footer .upper .language select {
border-color: #ccc;
}

#footer .upper .theme {
float: right;
margin: -1px;
margin-left: 15px;
}

#footer .upper .theme select {
border-color: #ccc;
}

#footer .upper ul.bottom_links {
float: left;
margin: 4px 0 0 0;
}

#footer .lower {
color: #666;
padding: 6px 6px 12px 6px;
overflow: hidden;
font-size: 11px;
}

#footer .lower a:link,
#footer .lower a:visited {
color: #444;
font-weight: bold;
}

#footer .lower a:hover,
#footer .lower a:active {
color: #333;
text-decoration: underline;
font-weight: bold;
}

#footer .lower #current_time {
float: right;
color: #888;
}

#debug {
float: right;
text-align: right;
margin-top: 20px;
font-size: 11px;
}

.scaleimages img {
max-width: 100%;
}

.forum_status {
height: 30px;
width: 30px;
background: url(/assets/runbb/images/forum_icon_sprite.png) no-repeat 0 0;
display: inline-block;
}

.forum_on {
background-position: 0 0;
}

.forum_off {
background-position: 0 -30px;
}

.forum_offlock {
background-position: 0 -60px;
}

.forum_offlink {
background-position: 0 -90px;
}

.subforumicon {
height: 10px;
width: 10px;
display: inline-block;
margin: 0 5px;
background: url(/assets/runbb/images/mini_status_sprite.png) no-repeat 0 0;
}

.subforum_minion {
background-position: 0 0;
}

.subforum_minioff {
background-position: 0 -10px;
}

.subforum_miniofflock {
background-position: 0 -20px;
}

.subforum_miniofflink {
background-position: 0 -30px;
}

.tborder {
background: #fff;
width: 100%;
margin: auto auto;
border: 1px solid #ccc;
padding: 1px;
}

.tfixed {
table-layout: fixed;
word-wrap: break-word;
}

.thead {
background: #0066a2 url(/assets/runbb/images/thead.png) top left repeat-x;
color: #ffffff;
border-bottom: 1px solid #263c30;
padding: 8px;
}

.thead a:link {
color: #ffffff;
text-decoration: none;
}

.thead a:visited {
color: #ffffff;
text-decoration: none;
}

.thead a:hover,
.thead a:active {
color: #ffffff;
text-decoration: underline;
}

.tcat {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
border-top: 1px solid #444;
border-bottom: 1px solid #000;
padding: 6px;
font-size: 12px;
}

.tcat a:link {
color: #fff;
}

.tcat a:visited {
color: #fff;
}

.tcat a:hover,
.tcat a:active {
color: #fff;
}

.trow1 {
background: #f5f5f5;
border: 1px solid;
border-color: #fff #ddd #ddd #fff;
}

.trow2 {
background: #efefef;
border: 1px solid;
border-color: #fff #ddd #ddd #fff;
}

.trow_shaded {
background: #ffdde0;
border: 1px solid;
border-color: #fff #ffb8be #ffb8be #fff;
}

.no_bottom_border {
border-bottom: 0;
}

.post.unapproved_post {
background: #ffdde0;
}

.post.unapproved_post .post_author {
border-bottom-color: #ffb8be;
}

.post.classic.unapproved_post .post_author {
border-color: #ffb8be;
}

.post.unapproved_post .post_controls {
border-top-color: #ffb8be;
}

.trow_deleted,
.post.deleted_post {
background: #E8DEFF;
}

.trow_selected,
tr.trow_selected td {
background: #FFFBD9;
color: #333;
border-right-color: #F7E86A;
border-bottom-color: #F7E86A;
}

.trow_selected a:link,
.trow_selected a:visited,
.trow_selected a:hover,
.trow_selected a:active {
color: #333;
}

.trow_sep {
background: #ddd;
color: #333;
border-bottom: 1px solid #c5c5c5;
padding: 6px;
font-size: 12px;
font-weight: bold;
}

.tfoot {
border-top: 1px solid #fff;
padding: 6px;
background: #ddd;
color: #666;
}

.tfoot a:link {
color: #444;
text-decoration: none;
}

.tfoot a:visited {
color: #444;
text-decoration: none;
}

.tfoot a:hover,
.tfoot a:active {
color: #444;
text-decoration: underline;
}

.thead input.textbox,
.thead select {
border: 1px solid #263c30;
}

.bottommenu {
background: #efefef;
color: #333;
border: 1px solid #4874a3;
padding: 10px;
}

.navigation {
color: #333;
font-size: 12px;
}

.navigation a:link {
text-decoration: none;
}

.navigation a:visited {
text-decoration: none;
}

.navigation a:hover,
.navigation a:active {
text-decoration: underline;
}

.navigation .active {
color: #333;
font-size: small;
font-weight: bold;
}

.smalltext {
font-size: 11px;
}

.largetext {
font-size: 16px;
font-weight: bold;
}

fieldset {
padding: 12px;
border: 1px solid #ddd;
margin: 0;
}

fieldset.trow1,
fieldset.trow2 {
border-color: #bbb;
}

fieldset.align_right {
text-align: right;
}

input.textbox {
background: #ffffff;
color: #333;
border: 1px solid #ccc;
padding: 3px;
outline: 0;
font-size: 13px;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
}

textarea {
background: #ffffff;
color: #333;
border: 1px solid #ccc;
padding: 2px;
line-height: 1.4;
outline: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
}

select {
background: #ffffff;
padding: 3px;
border: 1px solid #ccc;
outline: 0;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
}

button,
input.button {
padding: 3px 8px;
cursor: pointer;
font-family: Tahoma, Verdana, Arial, Sans-Serif;
font-size: 13px;
background: #eee url(/assets/runbb/images/buttons_bg.png) repeat-x;
border: 1px solid #bbb;
color: #333;
outline: 0;
}

button:hover,
input.button:hover {
border-color: #aaa;
}

form {
margin: 0;
padding: 0;
}

input.error, textarea.error, select.error {
border: 1px solid #f30;
color: #f30;
}

input.valid, textarea.valid, select.valid {
border: 1px solid #0c0;
}

label.error {
color: #f30;
margin: 5px;
padding: 0px;
display: block;
font-weight: bold;
font-size: 11px;
}

form #message {
width: 500px;
}

.editor {
background: #f1f1f1;
border: 1px solid #ccc;
}

.editor_control_bar {
background: #fff;
border: 1px solid #ccc;
}

.post .editor_control_bar {
background: #f5f5f5;
}

.popup_menu {
background: #fff;
border: 1px solid #ccc;
}

.popup_menu .popup_item {
background: #efefef;
color: #333;
}

.popup_menu .popup_item:hover {
background: #0072BC;
color: #fff;
}

.trow_reputation_positive {
background: #ccffcc;
}

.trow_reputation_negative {
background: #ffcccc;
}

.reputation_positive {
color: green;
}

.reputation_neutral {
color: #444;
}

.reputation_negative {
color: red;
}

.repbox {
font-size:16px;
font-weight: bold;
padding:5px 7px 5px 7px;
}

._neutral {
background-color:#FAFAFA;
color: #999999;
border:1px solid #CCCCCC;
}

._minus {
background-color: #FDD2D1;
color: #CB0200;
border:1px solid #980201;
}

._plus {
background-color:#E8FCDC;
color: #008800;
border:1px solid #008800;
}

img {
border: none;
}

img.attachment {
border: 1px solid #E9E5D7;
padding: 2px;
}

hr {
background-color: #000000;
color: #000000;
height: 1px;
border: 0px;
}

.clear {
clear: both;
}

.float_left {
float: left;
}

.float_right {
float: right;
}

.hidden {
display: none;
float: none;
width: 1%;
}

.hiddenrow {
display: none;
}

.selectall {
background: #FFFBD9;
border-bottom: 1px solid #F7E86A;
color: #333;
text-align: center;
}

.expcolimage {
float: right;
width: auto;
vertical-align: middle;
margin-top: 3px;
}

.tcat_menu > .expcolimage {
margin-top: 0;
}

blockquote {
border: 1px solid #ccc;
margin: 0;
background: #fff;
padding: 10px;
}

blockquote cite {
font-weight: bold;
border-bottom: 1px solid #ccc;
font-style: normal;
display: block;
padding-bottom: 3px;
margin: 0 0 10px 0;
}

blockquote cite span {
float: right;
font-weight: normal;
font-size: 12px;
color: #666;
}

blockquote cite span.highlight {
float: none;
font-weight: bold;
padding-bottom: 0;
}

.codeblock {
background: #fff;
border: 1px solid #ccc;
padding: 10px;
}

.codeblock .title {
border-bottom: 1px solid #ccc;
font-weight: bold;
padding-bottom: 3px;
margin: 0 0 10px 0;
}

.codeblock code {
overflow: auto;
height: auto;
max-height: 200px;
display: block;
font-family: Monaco, Consolas, Courier, monospace;
font-size: 13px;
}

.smilie {
vertical-align: middle;
}

.smilie_pointer {
cursor: pointer;
}

.separator {
margin: 5px;
padding: 0;
height: 0px;
font-size: 1px;
list-style-type: none;
}

.popup_menu .popup_item_container {
margin: 1px;
text-align: left;
}

.popup_menu .popup_item {
display: block;
padding: 4px;
white-space: nowrap;
text-decoration: none;
}

.popup_menu a.popup_item:hover {
text-decoration: none;
}

.subject_new {
font-weight: bold;
}

.highlight {
background: #FFFFCC;
padding-top: 3px;
padding-bottom: 3px;
}

.pm_alert {
background: #FFF6BF;
border: 1px solid #FFD324;
text-align: center;
padding: 5px 20px;
margin-bottom: 15px;
font-size: 11px;
}

.red_alert {
background: #FBE3E4;
border: 1px solid #A5161A;
color: #A5161A;
text-align: center;
padding: 5px 20px;
margin-bottom: 15px;
font-size: 11px;
word-wrap: break-word;
}

.red_alert a:link,
.red_alert a:visited,
.red_alert a:hover,
.red_alert  a:active {
color: #A5161A;
}

.high_warning {
color: #CC0000;
}

.moderate_warning {
color: #F3611B;
}

.low_warning {
color: #AE5700;
}

.online {
color: #15A018;
}

.offline {
color: #C7C7C7;
}

div.error {
padding: 5px 10px;
border-top: 2px solid #FFD324;
border-bottom: 2px solid #FFD324;
background: #FFF6BF;
font-size: 12px;
}

div.error p {
margin: 0;
color: #333;
font-weight: normal;
}

div.error p em {
font-style: normal;
font-weight: bold;
padding-left: 24px;
display: block;
color: #C00;
background: url(/assets/runbb/images/error.png) no-repeat 0;
}

div.error ul {
margin-left: 24px;
}

.pagination {
/*font-size: 11px;
padding-top: 10px;
margin-bottom: 5px;*/
margin: 7px;
}

.tfoot .pagination,
.tcat .pagination {
padding-top: 0;
}

.pagination .pages {
font-weight: bold;
}

.pagination .pagination_current,
.pagination a {
padding: 3px 6px;
margin-bottom: 3px;
}

.pagination a {
background: #f5f5f5;
border: 1px solid #ccc;
}

.pagination .pagination_current {
background: none;
color: #333;
border: none;
font-weight: bold;
}

.pagination a:hover {
background: #0072BC;
color: #fff;
border-color: #263c30;
text-decoration: none;
}

.pagination .go_page img {
margin-bottom: -4px;
}

.drop_go_page {
background: #f5f5f5;
padding: 4px;
}

.pagination_breadcrumb {
background-color: #efefef;
border: 1px solid #fff;
outline: 1px solid #ccc;
padding: 5px;
margin-top: 5px;
font-weight: normal;
}

.pagination_breadcrumb_link {
vertical-align: middle;
cursor: pointer;
}

.thread_legend,
.thread_legend dd {
margin: 0;
padding: 0;
}

.thread_legend dd {
padding-bottom: 4px;
margin-right: 15px;
}

.thread_legend img {
margin-right: 4px;
vertical-align: bottom;
}

.forum_legend,
.forum_legend dt,
.forum_legend dd {
margin: 0;
padding: 0;
}

.forum_legend dd {
float: left;
margin-right: 10px;
margin-top: 7px;
}

.forum_legend dt {
margin-right: 10px;
float: left;
}

.success_message {
color: #00b200;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

.error_message {
color: #C00;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

#posts_container {
padding: 0;
}

.ignored_post {
border-top: 3px solid #333;
padding: 15px;
}

.ignored_post .show_ignored_post {
margin-top: -15px;
}

.ignored_post .show_ignored_post a.button span {
background-position: 0 -400px;
}

.post {
overflow: hidden;
}

.post.classic {
padding-top: 15px;
}

.post .post_author {
border-bottom: 1px solid #ccc;
border-top: 2px solid #ccc;
background: #f5f5f5;
padding: 5px;
overflow: hidden;
}

.post.classic .post_author {
border: 1px solid #ddd;
float: left;
width: 15%;
margin: 0 1% 15px 0;
border-left: 0;
padding: 5px 1%;
}

.post .post_author .buddy_status {
vertical-align: middle;
margin-top: -4px;
}

.post .post_author div.author_avatar {
float: left;
margin-right: 3px;
}

.post.classic .post_author div.author_avatar {
float: none;
text-align: center;
margin-bottom: 8px;
}

.post .post_author div.author_avatar img {
padding: 5px;
border: 1px solid #ddd;
background: #fff;
}

.post .post_author div.author_information {
float: left;
padding: 6px 8px;
}

.post.classic .post_author div.author_information {
float: none;
padding: 0;
text-align: center;
}

.post .post_author div.author_statistics {
float: right;
font-size: 11px;
padding: 3px 10px 3px 5px;
color: #666;
line-height: 1.3;
}

.post.classic .post_author div.author_statistics {
border-top: 1px dotted #ccc;
margin: 6px 0 0 0;
padding: 6px 6px 3px 6px;
float: none;
}

.post .post_head {
font-size: 11px;
padding-bottom: 4px;
border-bottom: 1px dotted #ddd;
margin-bottom: 4px;
}

.post .post_head span.post_date {
color: #666;
}

.post .post_head span.edited_post {
font-size: 10px;
color: #999;
}

.post .post_head span.edited_post a {
color: #666;
}

.post_body {
font-size: 14px;
padding: 12px 0;
}

.post.classic .post_content {
float: left;
width: 79%;
padding: 0 1% 5px 1%;
}

.post_content {
padding: 9px 10px 5px 10px;
}

.post_content .signature {
margin-top: 5px;
border-top: 1px dotted #ddd;
padding: 10px 0 4px 0;
}

.post .post_meta {
margin: 4px 0;
font-size: 11px;
color: #999;
}

.post .post_meta a:link,
.post .post_meta a:visited {
color: #777;
}

.post .post_meta a:hover,
.post .post_meta a:active {
color: #777;
}

.post_controls {
clear: both;
background: #f5f5f5;
border-bottom: 1px solid #ccc;
padding: 5px;
overflow: hidden;
}

.postbit_buttons > a:link,
.postbit_buttons > a:hover,
.postbit_buttons > a:visited,
.postbit_buttons > a:active {
display: inline-block;
padding: 2px 5px;
margin: 2px;
font-size: 11px;
background: #eee url(/assets/runbb/images/buttons_bg.png) repeat-x;
border: 1px solid #ccc;
color: #555;
}

.postbit_buttons > a:hover {
border-color: #bbb;
}

.postbit_buttons a span {
padding-left: 20px;
display: inline-block;
height: 16px;
background-image: url(/assets/runbb/images/buttons_sprite.png);
background-repeat: no-repeat;
}

.postbit_buttons a.postbit_find span {
background-position: 0 0;
}

.postbit_buttons a.postbit_reputation_add span {
background-position: 0 -20px;
}

.postbit_buttons a.postbit_email span {
background-position: 0 -40px;
}

.postbit_buttons a.postbit_website span {
background-position: 0 -60px;
}

.postbit_buttons a.postbit_pm span {
background-position: 0 -80px;
}

.postbit_buttons a.postbit_quote span {
background-position: 0 -100px;
}

.postbit_buttons a.postbit_multiquote span {
background-position: 0 -120px;
}

.postbit_buttons a.postbit_multiquote_on span {
background-position: 0 -140px;
}

.postbit_buttons a.postbit_edit span {
background-position: 0 -160px;
}

.postbit_buttons a.postbit_qdelete span {
background-position: 0 -180px;
}

.postbit_buttons a.postbit_qrestore span {
background-position: 0 -200px;
}

.postbit_buttons a.postbit_report span {
background-position: 0 -220px;
}

.postbit_buttons a.postbit_warn span {
background-position: 0 -240px;
}

.postbit_buttons a.postbit_purgespammer span {
background-position: 0 -540px;
}

.postbit_buttons a.postbit_reply_pm span {
background-position: 0 -260px;
}

.postbit_buttons a.postbit_reply_all span {
background-position: 0 -280px;
}

.postbit_buttons a.postbit_forward_pm span {
background-position: 0 -300px;
}

.postbit_buttons a.postbit_delete_pm span {
background-position: 0 -320px;
}

a.button:link,
a.button:hover,
a.button:visited,
a.button:active {
background: #0f0f0f url(/assets/runbb/images/tcat.png) repeat-x;
color: #fff;
display: inline-block;
padding: 4px 8px;
margin: 2px 2px 6px 2px;
border: 1px solid #000;
font-size: 14px;
}

a.button.small_button {
font-size: 13px;
margin: 0;
padding: 3px 6px;
}

a.button span {
padding-left: 20px;
display: inline-block;
background-image: url(/assets/runbb/images/buttons_sprite.png);
background-repeat: no-repeat;
}

a.button.new_thread_button span {
background-position: 0 -340px;
}

a.button.new_reply_button span {
background-position: 0 -360px;
}

a.button.closed_button span {
background-position: 0 -380px;
}

a.button.rate_user_button span {
background-position: 0 -400px;
}

a.button.add_buddy_button span {
background-position: 0 -440px;
}

a.button.remove_buddy_button span {
background-position: 0 -480px;
}

a.button.add_ignore_button span {
background-position: 0 -460px;
}

a.button.remove_ignore_button span {
background-position: 0 -500px;
}

a.button.report_user_button span {
background-position: 0 -520px;
}

.quick_jump {
background: url(/assets/runbb/images/jump.png) no-repeat 0;
width: 13px;
height: 13px;
padding-left: 13px; /* amount of padding needed for image to fully show */
margin-top: -3px;
border: none;
}

.pollbar {
background: url(/assets/runbb/images/pollbar.png) top left repeat-x;
border: 1px solid #3f3f3f;
height: 10px;
}

.pollbar .percent {
display: none;
}

.posticons_label {
white-space: nowrap;
}

.expander.collapse,
.expander.collapse_collapsed {
font: normal normal normal 21px FontAwesome;
display: inline-block;
text-decoration: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
line-height: 0.7;
}

.expander.collapse:before {
content: \'\\f205\';
}

.expander.collapse_collapsed:before {
content: \'\\f204\';
}

[data-toggle=collapse] .fa.toggle:before {
content: "\\f205";
}

[data-toggle=collapse].collapsed .fa.toggle:before {
content: "\\f204";
}

/* sceditor buttons in bootstrap
https://github.com/samclarke/SCEditor/issues/308
*/
.sceditor-container { font-weight: normal }
.sceditor-button { box-sizing: content-box; -webkit-box-sizing: content-box; -moz-box-sizing: content-box; }
',
                'cachefile' => 'global.css',
                'lastmodified' => 1479950494,
            ),
            25 => 
            array (
                'sid' => 44,
                'name' => 'usercp.css',
                'tid' => 3,
                'attachedto' => 'usercp|usercp2|private',
                'stylesheet' => '
.usercp_nav_item {
display: block;
padding: 1px 0 1px 23px;
background-image: url(/assets/runbb/images/usercp_sprite.png);
background-repeat: no-repeat;
}

.usercp_nav_composepm {
background-position: 0 0;
}

.usercp_nav_pmfolder {
background-position: 0 -20px;
}

.usercp_nav_sub_pmfolder {
padding-left: 40px;
background-position: 0 -40px;
}

.usercp_nav_trash_pmfolder {
padding-left: 40px;
background-position: 0 -60px;
}

.usercp_nav_pmtracking {
background-position: 0 -80px;
}

.usercp_nav_pmfolders {
background-position: 0 -100px;
}

.usercp_nav_profile {
background-position: 0 -120px;
}

.usercp_nav_email {
padding-left: 40px;
background-position: 0 -140px;
}

.usercp_nav_password {
padding-left: 40px;
background-position: 0 -160px;
}

.usercp_nav_username {
padding-left: 40px;
background-position: 0 -180px;
}

.usercp_nav_editsig {
padding-left: 40px;
background-position: 0 -200px;
}

.usercp_nav_avatar {
padding-left: 40px;
background-position: 0 -220px;
}

.usercp_nav_options {
background-position: 0 -240px;
}

.usercp_nav_usergroups {
background-position: 0 -260px;
}

.usercp_nav_editlists {
background-position: 0 -280px;
}

.usercp_nav_attachments {
background-position: 0 -300px;
}

.usercp_nav_drafts {
background-position: 0 -320px;
}

.usercp_nav_subscriptions {
background-position: 0 -340px;
}

.usercp_nav_fsubscriptions {
background-position: 0 -360px;
}

.usercp_nav_viewprofile {
background-position: 0 -380px;
}

.usercp_nav_home {
background-position: 0 -400px;
}

.usercp_notepad {
width: 99%;
}

.usercp_container {
margin: 5px;
padding: 8px;
border:1px solid #CCCCCC;
}

.pmspace {
float: right;
margin: -3px 5px;
}

.pmspace_container {
background: #fff;
border: 1px solid #ccc;
width: 100px;
display: inline-block;
}

.pmspace_used {
display: inline-block;
color: #fff;
margin: -1px;
}

.pmspace_used.low {
border: 1px solid #0c5a01;
background: #167203;
}

.pmspace_used.medium {
background: #ff862b;
border: 1px solid #d7611e;
}

.pmspace_used.high {
background: #e73c3c;
border: 1px solid #c00;
}

.pmspace_unused {
display: inline-block;
}

.pmspace_text {
padding: 1px 5px;
display: inline-block;
}

',
                'cachefile' => 'usercp.css',
                'lastmodified' => 1479950160,
            ),
            26 => 
            array (
                'sid' => 45,
                'name' => 'modcp.css',
                'tid' => 3,
                'attachedto' => 'modcp',
                'stylesheet' => '
.modcp_nav_item {
display: block;
padding: 1px 0 1px 23px;
background-image: url(/assets/runbb/images/modcp_sprite.png);
background-repeat: no-repeat;
}

.modcp_nav_home {
background-position: 0 0;
}

.modcp_nav_announcements {
background-position: 0 -20px;
}

.modcp_nav_reports {
background-position: 0 -40px;
}

.modcp_nav_modqueue {
background-position: 0 -60px;
}

.modcp_nav_modlogs {
background-position: 0 -80px;
}

.modcp_nav_editprofile {
background-position: 0 -100px;
}

.modcp_nav_banning {
background-position: 0 -120px;
}

.modcp_nav_warninglogs {
background-position: 0 -140px;
}

.modcp_nav_ipsearch {
background-position: 0 -160px;
}

.modqueue_message {
overflow: auto;
max-height: 250px;
}

.modqueue_controls {
width: 270px;
float: right;
text-align: center;
border: 1px solid #ccc;
background: #fff;
padding: 6px;
font-weight: bold;
}

.modqueue_controls label {
margin-right: 8px;
}

.label_radio_ignore,
.label_radio_delete,
.label_radio_approve {
font-weight: bold;
}

.modqueue_meta {
color: #444;
font-size: 95%;
margin-bottom: 8px;
}

.modqueue_mass {
list-style: none;
margin: 0;
width: 150px;
padding: 0;
}

.modqueue_mass li {
margin-bottom: 4px;
padding: 0;
}

.modqueue_mass li a {
display: block;
padding: 4px;
border: 1px solid transparent;
}

.modqueue_mass li a:hover {
background: #efefef;
border: 1px solid #ccc;
text-decoration: none;
}

',
                'cachefile' => 'modcp.css',
                'lastmodified' => 1479950173,
            ),
            27 => 
            array (
                'sid' => 46,
                'name' => 'star_ratings.css',
                'tid' => 3,
                'attachedto' => 'forumdisplay|showthread',
                'stylesheet' => '
.star_rating,
.star_rating li a:hover,
.star_rating .current_rating {
background: url(/assets/runbb/images/star_rating.png) left -1000px repeat-x;
vertical-align: middle;
}

.star_rating {
position: relative;
width:80px;
height:16px;
overflow: hidden;
list-style: none;
margin: 0;
padding: 0;
background-position: left top;
}

td .star_rating {
margin: auto;
}

.star_rating li {
display: inline;
}

.star_rating li a,
.star_rating .current_rating {
position: absolute;
text-indent: -1000px;
height: 16px;
line-height: 16px;
outline: none;
overflow: hidden;
border: none;
top:0;
left:0;
}

.star_rating_notrated li a:hover {
background-position: left bottom;
}

.star_rating li a.one_star {
width:20%;
z-index:6;
}

.star_rating li a.two_stars {
width:40%;
z-index:5;
}

.star_rating li a.three_stars {
width:60%;
z-index:4;
}

.star_rating li a.four_stars {
width:80%;
z-index:3;
}

.star_rating li a.five_stars {
width:100%;
z-index:2;
}

.star_rating .current_rating {
z-index:1;
background-position: left center;
}

.star_rating_success,
.success_message {
color: #00b200;
font-weight: bold;
font-size: 10px;
margin-bottom: 10px;
}

.inline_rating {
background: url(/assets/runbb/images/star_rating.png) left -1000px repeat-x;
float: left;
vertical-align: middle;
padding-right: 5px;
}

',
                'cachefile' => 'star_ratings.css',
                'lastmodified' => 1479950179,
            ),
            28 => 
            array (
                'sid' => 47,
                'name' => 'showthread.css',
                'tid' => 3,
                'attachedto' => 'showthread',
                'stylesheet' => '
ul.thread_tools,
ul.thread_tools li {
list-style: none;
padding: 0;
margin: 0;
}

ul.thread_tools li {
padding-left: 24px;
padding-bottom: 4px;
margin-bottom: 3px;
font-size: 11px;
background-image: url(/assets/runbb/images/showthread_sprite.png);
background-repeat: no-repeat;
}

ul.thread_tools li.printable {
background-position: 0 0;
}

ul.thread_tools li.sendthread {
background-position: 0 -20px;
}

ul.thread_tools li.subscription_add {
background-position: 0 -40px;
}

ul.thread_tools li.subscription_remove {
background-position: 0 -60px;
}

ul.thread_tools li.poll {
background-position: 0 -80px;
}

.showthread_spinner {
margin: 0 auto;
display: block;
text-align: center;
padding: 20px;

width: 100%;
}

',
                'cachefile' => 'showthread.css',
                'lastmodified' => 1479950185,
            ),
            29 => 
            array (
                'sid' => 48,
                'name' => 'thread_status.css',
                'tid' => 3,
                'attachedto' => 'forumdisplay|usercp|search',
                'stylesheet' => '
.thread_status {
display: inline-block;
height: 16px;
background-image: url(/assets/runbb/images/folders_sprite.png);
background-repeat: no-repeat;

width: 16px;
}

.thread_status.dot_folder {
background-position: 0 0;
}

.thread_status.dot_hotfolder {
background-position: 0 -20px;
}

.thread_status.dot_hotlockfolder {
background-position: 0 -40px;
}

.thread_status.dot_lockfolder {
background-position: 0 -60px;
}

.thread_status.dot_newfolder {
background-position: 0 -80px;
}

.thread_status.dot_newhotfolder {
background-position: 0 -100px;
}

.thread_status.dot_newhotlockfolder {
background-position: 0 -120px;
}

.thread_status.dot_newlockfolder {
background-position: 0 -140px;
}

.thread_status.folder {
background-position: 0 -160px;
}

.thread_status.hotfolder {
background-position: 0 -180px;
}

.thread_status.hotlockfolder {
background-position: 0 -200px;
}

.thread_status.lockfolder {
background-position: 0 -220px;
}

.thread_status.movefolder {
background-position: 0 -240px;
}

.thread_status.newfolder {
background-position: 0 -260px;
}

.thread_status.newhotfolder {
background-position: 0 -280px;
}

.thread_status.newhotlockfolder {
background-position: 0 -300px;
}

.thread_status.newlockfolder {
background-position: 0 -320px;
}

',
                'cachefile' => 'thread_status.css',
                'lastmodified' => 1479950191,
            ),
            30 => 
            array (
                'sid' => 49,
                'name' => 'css3.css',
                'tid' => 3,
                'attachedto' => '',
                'stylesheet' => '
tr td.trow1:first-child,
tr td.trow2:first-child,
tr td.trow_shaded:first-child {
border-left: 0;
}

tr td.trow1:last-child,
tr td.trow2:last-child,
tr td.trow_shaded:last-child {
border-right: 0;
}

.tborder {
-moz-border-radius: 7px;
-webkit-border-radius: 7px;
border-radius: 7px;
}

.tborder tbody tr:last-child > td {
border-bottom: 0;
}

.tborder tbody tr:last-child > td:first-child {
-moz-border-radius-bottomleft: 6px;
-webkit-border-bottom-left-radius: 6px;
border-bottom-left-radius: 6px;
}

.tborder tbody tr:last-child > td:last-child {
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-right-radius: 6px;
}

.thead {
-moz-border-radius-topleft: 6px;
-moz-border-radius-topright: 6px;
-webkit-border-top-left-radius: 6px;
-webkit-border-top-right-radius: 6px;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.thead_collapsed {
-moz-border-radius-bottomleft: 6px;
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-left-radius: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-left-radius: 6px;
border-bottom-right-radius: 6px;
}

.thead_left {
-moz-border-radius-topright: 0;
-webkit-border-top-right-radius: 0;
border-top-right-radius: 0;
}

.thead_right {
-moz-border-radius-topleft: 0;
-webkit-border-top-left-radius: 0;
border-top-left-radius: 0;
}

.tcat_menu {
-moz-border-radius: 0 !important;
-webkit-border-radius: 0 !important;
border-radius: 0 !important;
}

.tborder tbody:nth-last-child(2) .tcat_collapse_collapsed {
-moz-border-radius-bottomleft: 6px !important;
-moz-border-radius-bottomright: 6px !important;
-webkit-border-bottom-left-radius: 6px !important;
-webkit-border-bottom-right-radius: 6px !important;
border-bottom-left-radius: 6px !important;
border-bottom-right-radius: 6px !important;
}

button,
input.button,
input.textbox,
input.invalid_field,
input.valid_field,
select,
textarea,
.editor_control_bar,
blockquote,
.codeblock,
fieldset,
.pm_alert,
.red_alert,
.popup_menu,
.postbit_buttons > a,
a.button {
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
}

.post.classic .post_author {
-moz-border-radius: 0 6px 6px 0;
-webkit-border-radius: 0 6px 6px 0;
border-radius: 0 6px 6px 0;
}

.popup_menu .popup_item_container:first-child .popup_item {
-moz-border-radius-topleft: 6px;
-moz-border-radius-topright: 6px;
-webkit-border-top-left-radius: 6px;
-webkit-border-top-right-radius: 6px;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.popup_menu .popup_item_container:last-child .popup_item {
-moz-border-radius-bottomleft: 6px;
-moz-border-radius-bottomright: 6px;
-webkit-border-bottom-left-radius: 6px;
-webkit-border-bottom-right-radius: 6px;
border-bottom-left-radius: 6px;
border-bottom-right-radius: 6px;
}

.pagination a {
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
}

.pollbar {
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
border-radius: 3px;
}

',
                'cachefile' => 'css3.css',
                'lastmodified' => 1479950198,
            ),
            31 => 
            array (
                'sid' => 50,
                'name' => 'color_water.css',
                'tid' => 3,
                'attachedto' => '',
                'stylesheet' => 'a:link,
a:visited,
a:hover,
a:active {
color: #2d9595;
}

#logo {
background: #1e6365 url(/assets/runbb/admin/images/colors/water_header.png) top left repeat-x;
border-bottom: 1px solid #133f41;
}

#header ul.menu li a {
color: #fff;
}

#panel input.button {
background: #1e6365 url(/assets/runbb/admin/images/colors/water_thead.png) top left repeat-x;
}

.thead {
background: #1e6365 url(/assets/runbb/admin/images/colors/water_thead.png) top left repeat-x;
border-bottom: 1px solid #133f41;
}

.thead input.textbox,
.thead select {
border: 1px solid #133f41;
}

.popup_menu .popup_item:hover {
background: #2d9595;
color: #fff;
}

.tt-suggestion.tt-is-under-cursor {
background-color: #2d9595;
color: #fff;
}

.pagination a:hover {
background-color: #2d9595;
color: #fff;
border-color: #133f41;
}

.panel-heading, .navbar {
/*background-color: #2d9595;*/
background-image: linear-gradient(to bottom, #009594 0%, #006564 100%) !important;
}

#panel .upper {
background-image: linear-gradient(to bottom, #009594 0%, #006564 100%) !important;
/*background: #045754 !important;*/
/*border-top: 1px solid #444;
border-bottom: 1px solid #000;*/
}',
                'cachefile' => 'color_water.css',
                'lastmodified' => 1479950965,
            ),
        ));

        
    }
}
