<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ThreadsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('threads')->delete();
        
        DB::table('threads')->insert(array (
            0 => 
            array (
                'tid' => 1,
                'fid' => 2,
                'subject' => 'qweqweqe',
                'prefix' => 0,
                'icon' => 1,
                'poll' => 0,
                'uid' => 1,
                'username' => 'admin',
                'dateline' => 1480723899,
                'firstpost' => 1,
                'lastpost' => 1480723899,
                'lastposter' => 'admin',
                'lastposteruid' => 1,
                'views' => 7,
                'replies' => 0,
                'closed' => '',
                'sticky' => 0,
                'numratings' => 0,
                'totalratings' => 0,
                'notes' => '',
                'visible' => 1,
                'unapprovedposts' => 0,
                'deletedposts' => 0,
                'attachmentcount' => 1,
                'deletetime' => 0,
            ),
        ));

        
    }
}
