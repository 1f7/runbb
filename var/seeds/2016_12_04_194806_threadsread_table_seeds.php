<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class ThreadsreadTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('threadsread')->delete();
        
        DB::table('threadsread')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tid' => 1,
                'uid' => 1,
                'dateline' => 1480877748,
            ),
        ));

        
    }
}
