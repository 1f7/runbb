<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class UsertitlesTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('usertitles')->delete();
        
        DB::table('usertitles')->insert(array (
            0 => 
            array (
                'utid' => 1,
                'posts' => 0,
                'title' => 'Newbie',
                'stars' => 1,
                'starimage' => '',
            ),
            1 => 
            array (
                'utid' => 2,
                'posts' => 1,
                'title' => 'Junior Member',
                'stars' => 2,
                'starimage' => '',
            ),
            2 => 
            array (
                'utid' => 3,
                'posts' => 50,
                'title' => 'Member',
                'stars' => 3,
                'starimage' => '',
            ),
            3 => 
            array (
                'utid' => 4,
                'posts' => 250,
                'title' => 'Senior Member',
                'stars' => 4,
                'starimage' => '',
            ),
            4 => 
            array (
                'utid' => 5,
                'posts' => 750,
                'title' => 'Posting Freak',
                'stars' => 5,
                'starimage' => '',
            ),
        ));

        
    }
}
