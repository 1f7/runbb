<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Capsule\Manager as DB;

class WarninglevelsTableSeeds extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('warninglevels')->delete();
        
        DB::table('warninglevels')->insert(array (
            0 => 
            array (
                'lid' => 1,
                'percentage' => 50,
                'action' => 'a:3:{s:4:"type";i:1;s:9:"usergroup";i:7;s:6:"length";i:2592000;}',
            ),
        ));

        
    }
}
